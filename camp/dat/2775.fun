if { $R < 600000.0 && $R >= 155000.0 } {# Using oxford calibrated as ref
  set T [expr {2975.2*pow((1/($R-1040.5))+1.785E-5,1.08380)}]
} elseif { $R < 155000.0 && $R >= 1965.0 } {
  set T [expr {2400.0*pow((1/($R-1040.5))+2.34E-5,1.08380)}]
} elseif { $R < 2000.0 && $R >= 1375.0 } {
  set T [expr {1441.1*pow((1/($R-1040.6))-6.63E-5,0.99613)}]
} elseif { $R < 1400.0 && $R >= 1080.0 } {
  set T [expr {421.2*pow((1/($R-1035.4))-1.12E-3,0.7270)}]
} else {
  return -code error "Out of range"
}
return $T
