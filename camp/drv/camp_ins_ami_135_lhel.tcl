# camp_ins_ami_135_lhel.tcl
# Camp Tcl instrument driver for American Magnetics Inc Model 135 Helium level meter
# Donald Arseneau,  TRIUMF
# Last revised   26-Nov-2002
# 
# Use "read" to send command because instrument replies (with null string)
# Error return from instrument is "-8" (strange).

CAMP_INSTRUMENT /~ -D -T "AMI 135 Liq He" \
    -H "American Magnetics Inc Model 135 Liquid Helium Level Instrument" \
    -d on \
    -initProc ami135_init     -deleteProc ami135_delete \
    -onlineProc ami135_online -offlineProc ami135_offline
proc ami135_init { ins } {
    insSet /${ins} -if rs232 0.2 2 none 9600 8 none 1 CRLF CRLF
}
proc ami135_delete { ins } {
    insSet /${ins} -line off
}
proc ami135_online { ins } {
    insIfOn /${ins}
    if { [catch { 
        varRead /${ins}/He_level
        varRead /${ins}/setup/low_level
        varDoSet /${ins}/setup/warn_level -v [expr int( [varGetVal /${ins}/setup/low_level] + 5 )]
    } ] } {
        insIfOff /${ins}
        return -code error "failed ID query, check interface definition and connections"
    }
    catch { varRead /${ins}/setup/units }
    varDoSet /${ins}/He_level -p on -p_int 120.0 -a on
}
proc ami135_offline { ins } {
    insIfOff /${ins}
}

proc ami135_insReadNum { ins cmd } {
    if { [catch { ami135_insRead "$ins" "$cmd" } buf] || \
	     [scan $buf { %f} val] != 1 } {
        return -code error "Invalid readback. $buf"
    }
    return $val
}
    
proc ami135_insRead { ins cmd } {
    set val [string trim [insIfRead /${ins} $cmd 32]]
    if { $val == -8 } {
        return -code error "Unrecognized instrument command: $cmd"
    }
    return $val
}

CAMP_FLOAT /~/He_level -D -R -P -L -A -T "Helium Level" \
        -tol 0.0 -d on -r on -units {%} -readProc ami135_He_read_r
proc ami135_He_read_r { ins } {
    set val [ami135_insReadNum $ins LEVEL ]
    set w [varGetVal /${ins}/setup/warn_level]
    if { $w > $val } { 
        set a on
    } else {
        set a off
    }
    varDoSet /${ins}/He_level -v $val -alert $a
}

CAMP_SELECT /~/measure -D -S -T "measure level" \
        -d on -s on -selections MEASURE HOLD \
        -writeProc ami135_measure_w
proc ami135_measure_w { ins i } {
    set cmd [lindex {MEASURE HOLD} $i]
    ami135_insRead $ins $cmd
    varDoSet /${ins}/measure -v $i
}

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on 

CAMP_SELECT /~/setup/units -D -R -S -T "Level units" \
        -d on -r on -s on -selections {%} cm inch \
        -readProc ami135_units_r -writeProc ami135_units_w
# Instrument might give a null response instead of % when units are percent.
proc ami135_units_r { ins } {
    set val [ami135_insRead $ins UNIT]
    if { $val == "" } { set val "%" }
    set i [lsearch -exact {% C I} $val]
    varDoSet /${ins}/setup/units -v $i
    set u [lindex {% cm inch} $i]
    varDoSet /${ins}/He_level -units $u
    varRead /${ins}/He_level
    varDoSet /${ins}/setup/warn_level -units $u
    varDoSet /${ins}/setup/low_level -units $u
    varDoSet /${ins}/setup/high_level -units $u
}
proc ami135_units_w { ins i } {
    set cmd [lindex {PERCENT CM INCH} $i]
    ami135_insRead $ins $cmd
    varRead /${ins}/setup/units
}

CAMP_FLOAT /~/setup/warn_level -D -S -T "Warn at Level" \
        -d on -s on -units {%} -writeProc ami135_warn_w
proc ami135_warn_w { ins val } {
    varDoSet /${ins}/setup/warn_level -v $val
    varRead /${ins}/He_level
}

CAMP_FLOAT /~/setup/low_level -D -S -R -P -T "low trip level" \
        -d on -s on -r on -units {%} \
        -readProc ami135_low_r -writeProc ami135_low_w
proc ami135_low_r { ins } {
    set val [ami135_insReadNum $ins LO ]
    varDoSet /${ins}/setup/low_level -v $val
}
proc ami135_low_w { ins val } {
    ami135_insRead $ins "LO=$val"
    varRead /${ins}/setup/low_level
}

CAMP_FLOAT /~/setup/high_level -D -S -R -P -T "high trip level" \
        -d on -s on -r on -units {%} \
        -readProc ami135_high_r -writeProc ami135_high_w
proc ami135_high_r { ins } {
    set val [ami135_insReadNum $ins HI ]
    varDoSet /${ins}/setup/high_level -v $val
}
proc ami135_high_w { ins val } {
    ami135_insRead $ins "HI=$val"
    varRead /${ins}/setup/high_level
}

CAMP_FLOAT /~/setup/interval -D -S -R -T "Sample interval" \
        -d on -s on -r on -units {min} \
        -readProc ami135_interval_r -writeProc ami135_interval_w
proc ami135_interval_r { ins } {
    set val [ami135_insReadNum $ins INTERVAL ]
    varDoSet /${ins}/setup/interval -v $val
}
proc ami135_interval_w { ins val } {
    ami135_insRead $ins "INTERVAL=$val"
    varRead /${ins}/setup/interval
}

