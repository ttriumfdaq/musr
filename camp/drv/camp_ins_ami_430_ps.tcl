 # camp_ins_ami_430_ps.tcl or camp_ins_ami_430_ps_doc.tcl
 # Camp Tcl instrument driver for American Magnetics model 430 superconducting magnet 
 # power supply programmer.
 # Donald Arseneau,  TRIUMF
 # Last revised July 29, 2015
 # 
 # If there are no following comments, then this is camp_ins_ami_430.tcl.
 # Do not edit camp_ins_ami_430_ps.tcl !!  Edit camp_ins_ami_430_ps_doc.tcl
 # instead; it has lots of comments.

CAMP_INSTRUMENT /~ -D -T "AMI Model 430 ps" \
    -H "AMI Model 430 power supply programmer" \
    -d on \
    -initProc ami_430_init \
    -deleteProc ami_430_delete \
    -onlineProc ami_430_online \
    -offlineProc ami_430_offline

proc amiWriteVerify { pins setc readc buff var fmt ntry target } {
  if { [catch { insIfWriteVerify $pins "*CLS; $setc" $readc $buff $var $fmt $ntry $target } msg] } {
      varDoSet $var -m "writeverify err: $msg"
      if { [catch {insIfRead $pins "SYST:ERR?" 80} err] == 0} {
	  if { [scan $err "%d,\"%\[^\"\]" n m ] == 2 } {
	      if { $n < 0 } { return -code error $m }
	  }
      }
      return -code error "Failed amiWriteVerify: $msg"
  }
}

proc amiReadVerify { pins readc buff var fmt ntry } {
  if { [catch { insIfReadVerify $pins "*CLS; $readc" $buff $var $fmt $ntry } msg] } {
      varDoSet $var -m "readverify err: $msg"
      if { [catch {insIfRead $pins "SYST:ERR?" 80} err] == 0} {
	  if { [scan $err "%d,\"%\[^\"\]" n m ] == 2 } {
	      if { $n < 0 } { return -code error $m }
	  }
      }
      return -code error "Failed amiReadVerify: $msg"
  }
}

if { [catch { set MAG(names) } n ] == 1 || $n == ""} {
    source "drv/magnet_procs.tcl"
}

proc ami_430_init { ins } {
    global MAG
    mag_var_init $ins
    set MAG($ins,PS) AMI_430
    set MAG($ins,tol) 0.008
    set MAG($ins,bigtol) 0.08
    #insSet /$ins -if rs232 0.1 2 none 115200 8 none 1 CRLF CRLF
    insSet /$ins -if tcpip 0.35 -2 142.90.154.127 7180 CRLF CRLF
    # Netbook as server:
    #insSet /$ins -if tcpip 0.1 1 142.90.126.39 7180 CRLF CRLF

}
proc ami_430_delete { ins } {
    insSet /$ins -line off
}
proc ami_430_online { ins } {
    global MAG
    if { [insGetIfDelay /$ins] < 0.1 } {
	set c "\[insSet /$ins -if [insGetIfTypeIdent /$ins] 0.1 [insGetIfTimeout /$ins] [insGetIf /$ins] \]"
	expr $c
    }
    insIfOn /$ins
    varRead /$ins/setup/id
    if { [varGetVal /$ins/setup/id] == 0 } {
	insIfOff /$ins
	return -code error "failed ID query, check interface definition and connections"
    }
    if { [catch {varRead /$ins/setup/supply}] } {
	insIfOff /$ins
	return -code error "Bad Power Supply configuration"
    }

    insIfWrite /$ins "CONF:FIELD:UNITS 1; CONF:RAMP:RATE:UNITS 0"
    if { [catch {
	varRead /$ins/setup/magnet
	ami_430_all_status_r $ins
    } ] } {
	return -code error "some magnet setup failed; check magnet parameters"
    }
    varDoSet /$ins/mag_field -p on -p_int 15
}
proc ami_430_offline { ins } {
    insIfOff /$ins
}


CAMP_FLOAT /~/mag_set -D -S -L -T "Set magnet current" \
    -H "Set magnet current and ramp to setpoint." \
    -d on -s on -units A \
    -writeProc mag_set_w

CAMP_FLOAT /~/mag_read -D -R -P -L -A -T "Read output current" \
    -H "Read power supply output current" \
    -d on -r on -units A -tol 0.003 -readProc ami_430_mag_read_r
proc ami_430_mag_read_r { ins } {
    amiReadVerify /$ins "CURR:SUPP?" 80 /$ins/mag_read " %f" 2
    varRead /$ins/heat_status
    if { [varGetVal /$ins/heat_status] != 1 } {
	varTestAlert /$ins/mag_read [varGetVal /$ins/mag_set]
    }
}

CAMP_FLOAT /~/mag_field -D -S -R -P -L -T "Nominal Magnetic Field" \
    -H "Nominal Magnetic Field, using magnet calibration constant (read and set)" \
    -d on -s on -r on -units T -tol 0.001 \
    -readProc ami_430_field_r -writeProc ami_430_field_w
proc ami_430_field_r { ins } {
    amiReadVerify /$ins "FIELD:MAG?" 80 /$ins/mag_field " %f" 2
    varRead /$ins/mag_read
}
proc ami_430_field_w { ins target } {
    varSet /$ins/mag_set -v [ expr ( $target + 0.0 ) / \
				    ([varGetVal /$ins/setup/calibration]) ]
    varDoSet /$ins/degauss/set_field -v $target
}

CAMP_FLOAT /~/degauss_field -D -S -T "Set with Degauss" \
    -H "Sets nominal magnetic field using Degauss procedures" \
    -d on -s on -units T -writeProc mag_degauss_set_field_w

CAMP_SELECT /~/ramp_status -D -S -T "Ramp status" -d on -s on \
    -H "Indicator for ramp-control sequencer" \
    -selections Holding Ramping Settling Persistent "Ramp leads +" "Ramp leads -" \
    "Heat switch" "Cool switch" "Turn on Heat" "Turn off Heat" "Degauss" "QUENCH" \
    -writeProc ami_430_ramp_status_w
proc ami_430_ramp_status_w { ins target } {
}

CAMP_SELECT /~/heat_status -D -S -R -P -L -A -T "Heater status" \
    -H "Superconducting switch heater status" \
    -d on -s on -r on -selections none off on force_on \
    -readProc ami_430_heat_status_r -writeProc ami_430_heat_status_w
proc ami_430_heat_status_r { ins } {
    varRead /$ins/controls/heat_onoff
    set h [varGetVal /$ins/controls/heat_onoff]
    if { $h == 0 && [varGetVal /$ins/heat_status] == 0 } { set h 3 }
    varDoSet /$ins/heat_status -v [lindex {1 2 0 0 0} $h]
}
proc ami_430_heat_status_w { ins target } {
    varRead /$ins/heat_status
    set h [varGetVal /$ins/heat_status]
    if { ($h==0) == ($target==0) } {
	if { $target == 2 && $h == 1 } {
	    varRead /$ins/controls/i_persist
	    set pers [varGetVal /$ins/controls/i_persist]
	    set buf [insIfRead /$ins "CURR:SUPP?" 80]
	    if { [scan $buf %f curr] != 1 || abs($pers-$curr) > 0.1 } {
		return -code error "Persistent current mismatch"
	    }
	}
	set h [lindex {0 0 1 1} $target]
	varSet /$ins/controls/heat_onoff -v $h
	varRead /$ins/heat_status
    } else {
	return -code error "define magnet to declare presence/absence of switch"
    }
}

CAMP_FLOAT /~/fast_set -D -S -R -L -P -T "Fast magnet set, no checks" \
    -H "Set magnet current. No smart control; no checks. (Read PS setpoint.)" \
    -d on -s off -r on -units A \
    -readProc ami_430_fast_set_r -writeProc ami_430_fast_set_w
proc ami_430_fast_set_w { ins target } {
    set val [format "%.5f" $target]
    insIfWrite /$ins "CONF:CURR:TARG $val"
    varDoSet /$ins/fast_set -v $val
    varSet /$ins/controls/activity -v 0
}
proc ami_430_fast_set_r { ins } {
    amiReadVerify /$ins "CURR:TARG?" 80 /$ins/fast_set " %f" 2
}

CAMP_FLOAT /~/settle_set -D -S -T "Set and Settle" \
    -H "Sets magnet and waits until stable" \
    -d on -s on -units A -writeProc mag_settle_w

CAMP_FLOAT /~/volts -D -R -P -L -T "Output voltage" \
    -d on -r on -units V -p_int 13 -readProc ami_430_volts_r
proc ami_430_volts_r { ins } {
    amiReadVerify /$ins "VOLT:SUPPLY?" 80 /$ins/volts " %f" 2
}

CAMP_SELECT /~/abort -D -S -T "Abort or Pause Ramp" \
    -H "Abort, pause, or continue a magnet change" \
    -d on -s on -v 0 -selections Abort Pause Resume "Never_mind" " " \
    -writeProc mag_abort_w

CAMP_SELECT /~/refresh -D -S -R -P -T "Refresh Persistent Current" \
    -H "Set or poll this to rejuvenate persistent field in magnet" \
    -d on -s on -r off -v 0 -selections Refresh \
    -readProc mag_refresh_r -writeProc mag_refresh_w

CAMP_STRUCT /~/degauss -D -T "Degauss magnet" -d on 

CAMP_FLOAT /~/degauss/amplitude -D -S -T "degauss delta-B" \
    -H "Set initial magnitude of field oscillations to use for degaussing magnet" \
    -units T -d on -s on \
    -writeProc mag_deg_ampl_w

CAMP_FLOAT /~/degauss/decrement -D -S -T "degauss % decrement" \
    -H "The percentage decrease in field for each stage of degaussing" \
    -d on -s on -units "%" -v 7.0 \
    -writeProc mag_deg_decrement_w

CAMP_FLOAT /~/degauss/set_field -D -S -T "degauss set B" \
    -H "Set the final field for after degaussing magnet" \
    -units T -d on -s on \
    -writeProc mag_deg_setf_w

CAMP_SELECT /~/degauss/degauss -D -R -P -S -T "degauss magnet" \
    -d on -s on -r off -p off -selections "FINISHED" "DEGAUSS NOW" \
    -readProc mag_degauss_r -writeProc mag_degauss_w

CAMP_STRUCT /~/setup -D -d on -T "Setup variables" \
    -H "Set magnet and power supply parameters here"
CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections false true -readProc ami_430_id_r
proc ami_430_id_r { ins } {
    set n 0
    set id 0
    while { $id == 0 && [incr n] <= 3 } {
	set status [catch {insIfRead /$ins "*IDN?" 80} buf]
	if { $status == 0 } {
	    set id [scan $buf " AMERICAN MAGNETICS INC.,MODEL %*d,%f" val]
	}
    }
    varDoSet /$ins/setup/id -v $id
}


CAMP_SELECT /~/setup/supply -D -R -T "Power Supply" \
    -H "Identify Power Supply model" \
    -d on -r on -selections  AMI_12100PS AMI_12200PS AMI_4Q05100PS AMI_4Q06125PS AMI_4Q06250PS AMI_4Q12125PS \
    AMI_10100PS AMI_10200PS HP_6260B Kepco_20-5M Kepco_20-10M XantrexXFR7.5-140 Custom AMI05100PS-430-601 \
    AMI05200PS-430-601 AMI05300PS-430-601 AMI05400PS-430-601 AMI05500PS-430-601 \
    -readProc ami_430_supply_r 


proc ami_430_supply_r { ins } {
    set buf [insIfRead /$ins SUPP:TYPE? 32]
    if { [scan $buf " %d" type] != 1 } {
	return -code error "Failed to read Power Supply Type; got \"$buf\""
    }
    if { $type < 0 || $type > 17 } {
	return -code error "Power supply type is unrecognized or incorrect"
    }
    varDoSet /$ins/setup/supply -v $type
}


CAMP_SELECT /~/setup/magnet -D -S -R -T "Select magnet" \
    -H "Identify superconducting magnet" \
    -d on -s on -r on -selections None Helios DR Belle bNMR hiTime nuTime \
    -readProc ami_430_magnet_r -writeProc ami_430_magnet_w
proc ami_430_magnet_r { ins } {
    global MAG
    set cal [varRead /$ins/setup/calibration; varGetVal /$ins/setup/calibration]
    set i -1
    set target 0
    foreach c $MAG(callist) {
	incr i
	if { abs($cal-$c) <= 0.0001 } { set target $i }
    }
    varDoSet /$ins/setup/magnet -v $target
    if { [catch {set MAG($ins,name)}] } {
	set MAG($ins,name) "--"
    }
    if { [string tolower $MAG($ins,name)] != [string tolower [lindex $MAG(namelist) $target]] } {
	ami_430_magnet_w $ins $target
    }
}
proc ami_430_magnet_w { ins target } {
    global MAG
    varRead /$ins/controls/ramp_status
    if { $target <= [llength $MAG(namelist)] } {
	varDoSet /$ins/setup/magnet -v $target
	set mag_S [lindex [string tolower $MAG(namelist)] $target ]
	source "drv/magnet_${mag_S}.ini"
	set i [lindex $MAG(indlist) $target]
	set s [expr 1.0/( 0.01 + pow(0.16745*$i,4) + pow(0.178*$i,6) ) ]
	insIfWrite /$ins "CONF:STAB [format %.1f $s]"
	set MAG($ins,currents)      [list 999.]
	set MAG($ins,ramps)	 [list 0.1 ]
    }
}

CAMP_FLOAT /~/setup/mag_max -D -S -R -L -T "Upper current limit" \
    -d on -s on -r on -units A \
    -readProc ami_430_mag_max_r -writeProc ami_430_mag_max_w
proc ami_430_mag_max_r { ins } {
    amiReadVerify /$ins "CURR:LIM?" 80 /$ins/setup/mag_max " %f" 2
    if { [varGetVal /$ins/setup/mag_min] < 0.0 } {
	varDoSet /$ins/setup/mag_min -v [expr -[varGetVal /$ins/setup/mag_max]]
    }
}
proc ami_430_mag_max_w { ins target } {
    varDoSet /$ins/setup/mag_max -v $target
    ami_430_apply_mag_m $ins
}

CAMP_FLOAT /~/setup/mag_min -D -S -R -L -T "Lower current limit" \
    -d on -s on -r on -units A \
    -readProc ami_430_mag_min_r -writeProc ami_430_mag_min_w
proc ami_430_mag_min_r { ins } {
    varDoSet /$ins/setup/mag_min -v -1.0
    ami_430_mag_max_r $ins
}
proc ami_430_mag_min_w { ins target } {
    varDoSet /$ins/setup/mag_min -v $target
    ami_430_apply_mag_m $ins
}
proc ami_430_apply_mag_m { ins } {
    set mmax [varGetVal /$ins/setup/mag_max]
    set mmin [varGetVal /$ins/setup/mag_min]
    if { abs($mmax) < abs($mmin) } {set mmax $mmin }
    insIfWrite /$ins "CONF:CURR:LIM [expr abs($mmax)]"
}

CAMP_FLOAT /~/setup/volt_max -D -S -R -L -T "Voltage limit" \
    -d on -s on -r on \
    -readProc ami_430_volt_max_r -writeProc ami_430_volt_max_w
proc ami_430_volt_max_r { ins } {
    amiReadVerify /$ins "VOLT:LIM?" 80 /$ins/setup/volt_max " %f" 2
}
proc ami_430_volt_max_w { ins target } {
    amiWriteVerify /$ins "CONF:VOLT:LIM $target" "VOLT:LIM?" 80 \
	/$ins/setup/volt_max " %f" 2 $target
}


CAMP_SELECT /~/setup/ramp_mode -D -S -R -T "Ramp mode" \
    -H "Set this to choose method of magnet operation. Read to choose mode automatically." \
    -d on -r off -s on -selections non-persistant persistant -v 0 \
    -readProc mag_ramp_mode_r -writeProc mag_ramp_mode_w


CAMP_FLOAT /~/setup/heat_i -D -S -R -P -L -T "Heater current (mA)" \
    -d on -s on -r on -units mA \
    -readProc ami_430_heat_i_r -writeProc ami_430_heat_i_w
proc ami_430_heat_i_r { ins } {
    amiReadVerify /$ins "PSWITCH:CURR?" 80 /$ins/setup/heat_i " %f" 2
}
proc ami_430_heat_i_w { ins target } {
    amiWriteVerify /$ins "CONF:PSWITCH:CURR $target" "PSWITCH:CURR?" 80 \
	/$ins/setup/heat_i " %f" 2 $target
}

CAMP_FLOAT /~/setup/heat_time -D -S -R -P -T "Time to heat switch" \
    -H "Time for SC switch to change state after heater turned on/off." \
    -d on -r on -s on -units s \
    -readProc  ami_430_heat_time_r -writeProc ami_430_heat_time_w
proc ami_430_heat_time_r { ins } {
    amiReadVerify /$ins "PSWITCH:HTIME?" 80 /$ins/setup/heat_time " %f" 2
}
proc ami_430_heat_time_w { ins target } {
    amiWriteVerify /$ins "CONF:PSWITCH:HEATTIME $target" "PSWITCH:HEATTIME?" 80 \
	/$ins/setup/heat_time " %f" 2 $target
    amiWriteVerify /$ins "CONF:PSWITCH:COOLTIME $target" "PSWITCH:COOLTIME?" 80 \
	/$ins/setup/heat_time " %f" 2 $target
}

CAMP_FLOAT /~/setup/settle_time -D -S -T "Magnet settling time" \
    -H "Magnet settling time (for settle_set)" \
    -d on -s on -units s -v 20.0 -writeProc mag_settle_time_w

CAMP_SELECT /~/setup/pers_leads -D -S -T "Manage persistent leads" \
    -H "Say whether to zero leads in persistent mode, or hold current" \
    -selections Zero_curr Hold_curr \
    -v 0 -d on -s on -writeProc mag_pers_leads_w

CAMP_FLOAT /~/setup/calibration -D -S -R -L -T "Tesla per ampere" \
    -H "Magnet calibration, Tesla per Amp, used for setting mag_field" \
    -d on -s on -r on -units "T/A" -tol 0.0001 \
    -readProc ami_430_cal_r -writeProc ami_430_cal_w
proc ami_430_cal_r { ins } {
    amiReadVerify /$ins "COIL?" 80 /$ins/setup/calibration " %f" 2
}
proc ami_430_cal_w { ins target } {
    amiWriteVerify /$ins "CONF:COIL $target" "COIL?" 80 \
	/$ins/setup/calibration " %f" 2 $target
}

CAMP_STRUCT /~/setup/ramps -D -d on -T "ramp rates" \
    -H "Set magnet ramp rates by regions"

CAMP_INT /~/setup/ramps/num_seg -D -S -R -T "Num segments" \
    -d on -s on -r on -units "" \
    -H "The number of ramping segments" \
    -readProc ami_430_numseg_r -writeProc ami_430_numseg_w
proc ami_430_numseg_r { ins } {
    amiReadVerify /$ins "RAMP:RATE:SEG?" 40 /$ins/setup/ramps/num_seg " %d" 2
    ami_430_show_segments $ins [varGetVal /$ins/setup/ramps/num_seg]
}
proc ami_430_numseg_w { ins target } {
    amiWriteVerify /$ins "CONF:RAMP:RATE:SEG $target" "RAMP:RATE:SEG?" 80 \
	/$ins/setup/ramps/num_seg " %d" 2 $target
    ami_430_show_segments $ins $target
}
proc ami_430_show_segments { ins num } {
    for { set i 1 } { $i <= 10 } { incr i } {
	if { $i <= $num } {
	    varDoSet /$ins/setup/ramps/segment_$i -d on -s on -r on
	} else {
	    varDoSet /$ins/setup/ramps/segment_$i -d off -s off -r off
	}
    }
}

for { set __i 1 } { $__i <= 10 } { incr __i } {

CAMP_STRING /~/setup/ramps/segment_$__i -D -S -R -T "Ramp segment $__i" \
    -d off -s off -r off \
    -H "Ramp rate (A/s) and upper bound (A) for segment $__i" \
    -readProc [list ami_430_segment_r $__i] -writeProc [list ami_430_segment_w $__i]

}
unset __i

proc ami_430_segment_r { i ins } {
    catch {
	set buf [insIfRead /$ins "RAMP:RATE:CURR:$i?" 80]
	varDoSet /$ins/setup/ramps/segment_$i -v [string trim $buf]
    }
}
proc ami_430_segment_w { i ins value } {
    set value [string trim $value]
    if { $i < 1 || $i > [varGetVal /$ins/setup/ramps/num_seg] } {
	return -code error "Segment $i is not enabled currently"
    }
    if { [llength [split $value ,]] != 2 && [llength $value] == 2 } {
	set value [join $value ","]
    }
    if { [scan $value "%f, %f %c" r u x] != 2 } {
	return -code error "Improper format. Must be two numbers separated by a comma"
    }
    insIfWrite /$ins "CONF:RAMP:RATE:CURR ${i},$value"
    varRead /$ins/setup/ramps/segment_$i
}

CAMP_STRUCT /~/controls -D -d on -T "Control variables" \
    -H "Internal control variables: Don't set them manually."

CAMP_SELECT /~/controls/activity -D -R -S -T "Zero or setpoint" \
    -H "Sets PS to zero or setpoint" \
    -d on -r on -s on -selections Setpoint Zero Pause Quench  \
    -readProc ami_430_activity_r -writeProc ami_430_activity_w
proc ami_430_activity_r { ins } {
    set state [insIfRead /$ins STATE? 80]
    varDoSet /$ins/controls/activity -v [lindex {0 0 0 2 0 0 1 3 1 0 1} $state]
    set r [lindex {0 1 0 0 1 1 1 2 0 0 0} $state]
    varDoSet /$ins/controls/ramp_status -v $r
}
proc ami_430_activity_w { ins target } {
    set a [lindex {{QU 0; RAMP} {QU 0; ZERO} {QU 0; PAUSE} {PAUSE; QU 1}} $target]
    insIfWrite /$ins $a
    varDoSet /$ins/controls/activity -v $target
}

CAMP_SELECT /~/controls/ramp_status -D -R -P -T "Instrument Ramp status" \
    -d on -r on -selections Holding Ramping Quench \
    -readProc ami_430_activity_r

CAMP_SELECT /~/controls/do_ramp -D -P -T "Ramping Sequencer" \
    -H "Ramping Sequencer (polled when in operation)" \
    -d on -selections "DO RAMP" -readProc mag_do_ramp_r

CAMP_SELECT /~/controls/fast_leads -D -R -P -S -T "Fast or slow" \
    -H "This is a required variable, but setting it has no effect" \
    -d on -r on -s on -selections "Slow Mag" "Fast leads" \
    -readProc ami_430_leads_r -writeProc ami_430_leads_w
proc ami_430_leads_r { ins } {
    amiReadVerify /$ins "PERS?" 40 /$ins/controls/fast_leads " %d" 2
}
proc ami_430_leads_w { ins target } {
    varDoSet /$ins/controls/fast_leads -v $target
}

CAMP_FLOAT /~/controls/ramp_rate -D -S -R -T "Bogus ramp rate" \
    -d on -s on -r on -units "A/s" \
    -H "This is a required variable, but setting it has no effect" \
    -readProc ami_430_con_rr_r -writeProc ami_430_con_rr_w
proc ami_430_con_rr_r { ins } {
}
proc ami_430_con_rr_w { ins target } {
}

CAMP_FLOAT /~/controls/i_persist -D -R -L -T "Persistant magnet current" \
    -d on -r on -units A \
    -readProc ami_430_i_persist_r
proc ami_430_i_persist_r { ins } {
    amiReadVerify /$ins "CURR:MAG?" 80 /$ins/controls/i_persist " %f" 2
}

CAMP_SELECT /~/controls/heat_onoff -D -S -R -P -L -T "Instrument heater status" \
    -d on -r on -s on -selections OFF ON \
    -readProc ami_430_heat_onoff_r -writeProc ami_430_heat_onoff_w
proc ami_430_heat_onoff_r { ins } {
    amiReadVerify /$ins "PSWITCH?" 80 /$ins/controls/heat_onoff " %d" 2
}
proc ami_430_heat_onoff_w { ins target } {
    amiWriteVerify /$ins "PSWITCH $target" "PSWITCH?" 80 \
	/$ins/controls/heat_onoff " %d" 2 $target
}

proc ami_430_all_status_r { ins } {
    varRead /$ins/controls/activity
    varRead /$ins/controls/i_persist
    varRead /$ins/mag_field
    varRead /$ins/setup/mag_max
    varRead /$ins/setup/mag_min
    set rs [varGetVal /$ins/controls/ramp_status]
    set hs [varGetVal /$ins/heat_status]
    set act [varGetVal /$ins/controls/activity]
    if { $rs > 1 || $act == 3 } {
	varDoSet /$ins/ramp_status -v "QUENCH"
	return
    }
    set rs [lindex [list $rs [expr {3+$rs*($act==0)}] 1] $hs]
    varDoSet /$ins/ramp_status -v $rs
    if { [varGetVal /$ins/mag_set] == 0.0 && [varNumGetNum /$ins/mag_set] == 0 } {
	varDoSet /$ins/mag_set -v [varGetVal /$ins/fast_set]
    }
    if { $rs==0 } {
	varDoSet /$ins/mag_set -v [varGetVal /$ins/mag_read]
    } elseif { $act==1 && $hs != 1 } {
	varDoSet /$ins/mag_set -v 0.0
    }
    if { $rs == 0 || $rs == 3 } {
	varDoSet /$ins/controls/do_ramp -p off
    } else {
	varSet /$ins/mag_set -v [varGetVal /$ins/mag_set]
    }
}


CAMP_STRUCT /~/controls/ps_config -D -T "PS Configuration" \
  -H "Parameters that define the power supply model" -d on

CAMP_FLOAT /~/controls/ps_config/volt_max -D -R -T "Max Voltage" -d on -r on -readProc {ami_430_cfg_r volt max}
CAMP_FLOAT /~/controls/ps_config/volt_min -D -R -T "Min Voltage" -d on -r on -readProc {ami_430_cfg_r volt min}
CAMP_FLOAT /~/controls/ps_config/curr_max -D -R -T "Max Current" -d on -r on -readProc {ami_430_cfg_r curr max}
CAMP_FLOAT /~/controls/ps_config/curr_min -D -R -T "Min Current" -d on -r on -readProc {ami_430_cfg_r curr min}
CAMP_SELECT /~/controls/ps_config/absorber -D -R -T "Absorber" \
    -d on -r on -selections NO YES -readProc ami_430_abs_r

proc ami_430_cfg_r { par lim ins } {
    amiReadVerify /$ins "SUPP:${par}:${lim}?" 80 /$ins/controls/ps_config/${par}_${lim} " %f" 2
}

proc ami_430_abs_r { ins } {
    amiReadVerify /$ins "AB?" 80 /$ins/controls/ps_config/absorber " %d" 2
}

