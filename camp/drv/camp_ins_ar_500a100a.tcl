# camp_ins_ar_500a100a.tcl
# Amplifier Research model 500A100A RF amplifier
#
# $Log: camp_ins_ar_500a100a.tcl,v $
# Revision 1.7  2017/06/29 03:16:42  asnd
# Show recent max power (if RF is on long enough with each pulse)
#
# Revision 1.6  2017/06/29 03:13:36  asnd
# Display unscaled value.
#
# Revision 1.5  2015/11/29 04:08:10  asnd
# Recover version 1.2
#
# Revision 1.4  2015/03/15 00:51:54  suz
# Ted's 201404 interface changes
#
# Revision 1.2  2010/09/01 01:11:40  asnd
# Suggest cycling main power for connection failure
#
# Revision 1.1  2008/04/03 03:36:24  asnd
# New instrument: Amplifier Research model 500A100A RF amplifier (bNMR RF Amp)
#
CAMP_INSTRUMENT /~ -D -T "ar 500A100A" \
  -H "AR 500A100A RF Amplifier" -d on \
  -initProc ar_init \
  -deleteProc ar_delete \
  -onlineProc ar_online \
  -offlineProc ar_offline

# The DIP switches on the back should have switches 3 and 6 on (to the
# left) for rs232 9600 baud.  Change switch 6 (the topmost) to off for
# gpib using channel 4 (since switch 3 is on).
#
# The amplifier stops paying attention to the rs232 port as soon as it
# ever loses DCD (carrier detect, on pin 1), requiring the main power
# (breaker switch on back) to be turned off-then-on to reestablish 
# connection.  So for convenience/sanity it is best to use an rs232
# adapter unit that shorts DTR to DCD (pins 1 and 4).
	
proc ar_init { ins } {
  upvar \#0 ${ins}_plog plog
  set plog(fwd) {}
  set plog(refl) {}
  insSet /${ins} -if rs232 0.33 1 /tyCo/1 9600 8 none 1 LF LF
}
proc ar_delete { ins } {
  global ${ins}_plog
  catch { unset ${ins}_plog }
  insSet /${ins} -line off
}
proc ar_online { ins } {
  upvar \#0 ${ins}_plog plog
  set plog(fwd) {}
  set plog(refl) {}
  insIfOn /${ins}
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == 0) } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections, Switch KEY TO REMOTE and CYCLE MAIN POWER ON BACK OF AMPLIFIER"
  }
  # No status readback! Default operation when instrument key is switched to "remote"
  # is power off, so assume that,
  varDoSet /${ins}/state -v Off
}
proc ar_offline { ins } {
  insIfOff /${ins}
}

CAMP_SELECT /~/state -D -S -T "Operation state" \
  -H "Set the operation On (operate) or Standby or Off" \
  -selections Operate Standby Off \
  -d on -s on \
  -writeProc ar_state_w

proc ar_state_w { ins target } {
  if { $target < 2 } {
    insIfWrite /$ins P1
    set cmd [lindex {O S} $target]
    insIfWrite /$ins $cmd
  } else {
    insIfWrite /$ins P0
  }
  varDoSet /${ins}/state -v $target
}


CAMP_SELECT /~/mode -D -S -T "Operation mode" \
  -H "Set the operation mode" \
  -selections Manual Pulse "ALC (internal)" "ALC (external)" \
  -d on -s on \
  -writeProc ar_mode_w

proc ar_mode_w { ins target } {
  upvar \#0 ${ins}_plog plog
  set plog(fwd) {}
  set plog(refl) {}
  set cmd [lindex {MM MP MA MA} $target]
  insIfWrite /$ins $cmd
  if { $target < 2 } {
    varDoSet /${ins}/setup/ALC_threshold -d off -r off
    varDoSet /${ins}/setup/detector_gain -d off -r off
    varDoSet /${ins}/setup/ALC_response -d off -r off
  } else {
    set cmd [lindex {x x DI DE} $target]
    insIfWrite /$ins $cmd
    varDoSet /${ins}/setup/ALC_threshold -d on -r on
    varDoSet /${ins}/setup/detector_gain -d on -r on
    varDoSet /${ins}/setup/ALC_response -d on -r on
    varRead /${ins}/setup/ALC_threshold
    varRead /${ins}/setup/detector_gain
    varRead /${ins}/setup/ALC_response
  }
  varDoSet /${ins}/mode -v $target
}

CAMP_SELECT /~/reset -D -S -T "Reset" \
  -selections Reset \
  -H "Reset any fault conditions" \
  -d on -s on \
  -writeProc ar_reset_w

proc ar_reset_w { ins target } {
  upvar \#0 ${ins}_plog plog
  set plog(fwd) {}
  set plog(refl) {}
  insIfWrite /$ins R
}

CAMP_FLOAT /~/RF_gain -D -S -R -P -L -T "RF Gain" \
  -d on -s on -r on -p off -p_int 10 -units {%} \
  -H "RF Gain setting, as percentage of maximum possible" \
  -readProc ar_rfgain_r -writeProc ar_rfgain_w

proc ar_rfgain_r { ins } {
  set buf [insIfRead /${ins} "G?" 16]
  if { [scan $buf " G%d" val] == 1 } {
    set val [format "%.1f" [expr { 100.0*$val/4095.0 }] ]
    varDoSet /${ins}/RF_gain -v $val
  } else {
    return -code error "Invalid response $buf"
  }
}

proc ar_rfgain_w { ins target } {
  upvar \#0 ${ins}_plog plog
  set plog(fwd) {}
  set plog(refl) {}
  if { $target < 0.0 || $target > 100.0 } {
    return -code error "requested setting $target out of range (0-100)"
  }
  insIfWrite /$ins [format "G%4.4d" [expr { round($target/100.0*4095.0) }] ]
  varDoSet /${ins}/RF_gain -v $target
}


CAMP_FLOAT /~/fwd_power -D -R -P -L -T "Forward power" \
  -H "Reads forward transmitted power" \
  -d on -r on -p off -p_int 10 -units {W} \
  -readProc ar_fwd_r

proc ar_fwd_r { ins } {
  set buf [insIfRead /$ins "FP?" 16]
  if { [scan $buf " FP%d" i] < 1 } {
    return -code error "invalid number: $buf"
  }
  set pwr [expr {double($i)/2048.0*750.0}]
  varDoSet /${ins}/fwd_power -v [format {%.1f} $pwr]
  ar_max_r $ins "fwd"
}

CAMP_FLOAT /~/refl_power -D -R -P -L -T "Reflected power" \
  -H "Reads reflected RF power" \
  -d on -r on -p off -p_int 10 -units {W} \
  -readProc ar_refl_r

proc ar_refl_r { ins } {
  set buf [insIfRead /$ins "RP?" 16]
  if { [scan $buf " RP%d" i] < 1 } {
    return -code error "invalid number: $buf"
  }
  set pwr [expr {double($i)/2048.0*750.0}]
  varDoSet /${ins}/refl_power -v [format {%.1f} $pwr]
  ar_max_r $ins "refl"
}

CAMP_FLOAT /~/fwd_max -D -R -P -L -T "Peak forward power" \
  -H "Records maximum recent forward transmitted RF power" \
  -d on -r on -p off -p_int 10 -units {W} \
  -readProc ar_fwd_r

CAMP_FLOAT /~/refl_max -D -R -P -L -T "Peak reflected power" \
  -H "Records maximum recent reflected RF power" \
  -d on -r on -p off -p_int 10 -units {W} \
  -readProc ar_refl_r

proc ar_max_r { ins var } {
  set p [varGetVal /$ins/${var}_power]
  upvar \#0 ${ins}_plog plog
  # update list
  set num [expr [varGetVal /${ins}/setup/num_to_max] - 2]
  set lis [linsert [lrange $plog($var) 0 $num] 0 $p]
  set ${ins}(listfc) $lis
  # select maximum
  set val [lindex [lsort -decreasing -real $lis] 0]
  varDoSet /${ins}/${var}_max -v $val
}

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
  -selections FALSE TRUE -readProc ar_id_r
 proc ar_id_r { ins } {
   set id 0
   set buf x
   catch {insIfRead /${ins} "I?" 100} buf
   set id [expr {[scan $buf " IAR%dA%dASW" i1 i2] == 2}]
   varDoSet /${ins}/setup/id -v $id
 }

CAMP_INT /~/setup/num_to_max -D -S -T "Number of power reads" \
  -d on -s on -v 10 \
  -H "Enter the number of power readings to keep for selecting maximum value" \
  -writeProc ar_npr
proc ar_npr { ins num } {
  upvar \#0 ${ins}_plog plog
  if { $num<1 || $num > 100 } { return -code error "Number out of range 1..100" }
  varDoSet /$ins/setup/num_to_max -v $num
}

CAMP_FLOAT /~/setup/ALC_threshold -D -S -R -P -L -T "ALC Threshold" \
  -H "Automatic Level Control threshold" \
  -d on -s on -r on -p off -p_int 10 -units {%} \
  -readProc ar_thresh_r -writeProc ar_thresh_w

proc ar_thresh_r { ins } {
  set buf [insIfRead /${ins} "T?" 16]
  if { [scan $buf "T%d" val] == 1 } {
    set val [format "%.1f" [expr { 100.0*$val/4095.0 }] ]
    varDoSet /${ins}/setup/ALC_threshold -v $val
  } else {
    return -code error "Invalid response $buf"
  }
}

proc ar_thresh_w { ins target } {
  if { $target < 0.0 || $target > 100.0 } {
    return -code error "requested setting $target out of range (0-100)"
  }
  set val [format "T%4.4d" [expr { round($val/100.0*4095.0) }] ]
  varDoSet /${ins}/setup/ALC_threshold -v $val
}

CAMP_FLOAT /~/setup/detector_gain -D -S -R -P -L -T "Detector Gain" \
  -H "Gain of detector used for ALC, in percent of full" \
  -d on -s on -r on -p off -p_int 10 -units {%} \
  -readProc ar_detgain_r -writeProc ar_detgain_w

proc ar_detgain_r { ins } {
  set buf [insIfRead /${ins} "D?" 16]
  if { [scan $buf " D%d" val] == 1 } {
    set val [format "%.1f" [expr { 100.0*$val/255.0 }] ]
    varDoSet /${ins}/setup/detector_gain -v $val
  } else {
    return -code error "Invalid response $buf"
  }
}

proc ar_detgain_w { ins target } {
  if { $target < 0.0 || $target > 100.0 } {
    return -code error "requested setting $target out of range (0-100)"
  }
  set val [format "D%3.3d" [expr { round($val/100.0*255.0) }] ]
  varDoSet /${ins}/setup/detector_gain -v $val
}

CAMP_SELECT /~/setup/ALC_response -D -S -R -P -T "ALC Response" \
  -H "ALC time constant setting" \
  -selections "0 fastest" 1 2 3 4 5 "6 slowest" \
  -d on -r on -s on -p off \
  -readProc ar_response_r -writeProc ar_response_w

proc ar_response_r { ins } {
  set buf [insIfRead /${ins} "LR?" 16]
    if { [scan $buf " LR%d" val] == 1 } {
    varDoSet /${ins}/setup/ALC_response -v $val
  } else {
    return -code error "Invalid response $buf"
  }
}

proc ar_response_w { ins target } {
  insIfWrite /$ins "LR$target"
  varDoSet /${ins}/setup/ALC_response -v $target
}

