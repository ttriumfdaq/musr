# camp_ins_asd_gagemux.tcl
# Camp driver for ASD GageMux / Mitutoyo absolute position gauge
# $Log: camp_ins_asd_gagemux.tcl,v $
# Revision 1.3  2015/03/15 00:51:05  suz
# Ted's 201404 interface changes
#
# Revision 1.2  2006/07/20 02:13:54  asnd
# Spelling
#
 
CAMP_INSTRUMENT /~ -D -T "ASD GageMux" \
    -H "ASD GageMux / Mitutoyo absolute position gauge" -d on \
    -initProc asdgm_init -deleteProc asdgm_delete \
    -onlineProc asdgm_online -offlineProc asdgm_offline
proc asdgm_init { ins } {
    #insSet /${ins} -if gpib 0.5 2 12 LF LF
    insSet /${ins} -if rs232 0.5 2 tt: 9600 8 none 1 CRLF CR
}
proc asdgm_delete { ins } {
    insSet /${ins} -line off
}
proc asdgm_online { ins } {
    insIfOn /${ins}
    set i 0
    if { [catch {
#        insIfWrite /$ins "XX"
        varRead /${ins}/read_x
    } msg] } {
        insIfOff /${ins}
        return -code error "Online failure: $msg"
    }
    varSet /${ins}/num_chan -v [varGetVal /${ins}/num_chan]
}
proc asdgm_offline { ins } {
    insIfOff /${ins}
}

CAMP_FLOAT /~/read_x -D -R -P -L -T "Ch x reading" \
    -d on -r on -p off -p_int 11 -units mm \
    -readProc [list asdgm_read_r x]

CAMP_FLOAT /~/read_y -D -R -P -L -T "Ch y reading" \
    -d on -r on -p off -p_int 11 -units mm \
    -readProc [list asdgm_read_r y]

CAMP_FLOAT /~/read_z -D -R -P -L -T "Ch z reading" \
    -d on -r on -p off -p_int 11 -units mm \
    -readProc [list asdgm_read_r z]

CAMP_FLOAT /~/read_t -D -R -P -L -T "Ch t reading" \
    -d on -r on -p off -p_int 11 -units mm \
    -readProc [list asdgm_read_r t]

proc asdgm_read_r { ch ins } {
    set chn [string first $ch "0xyzt"]
    if { $chn < 1 || $chn > [varGetVal /${ins}/num_chan] } {
         return -code error "Channel $ch is not active (please verify num_chan)"
    }
    set retries 0
    while { [catch {insIfRead /$ins R[format "%2.2i" $chn] 32} buf] } {
        # Read failures persist until communications are reset, so reset them on first retry
        if { $retries == 0} { insIfWrite /$ins "XX" }
        if { [incr retries] > 5 } { 
            insIfWrite /$ins "XX" 
            return -code error "Probably disconnected channel $ch ($chn): $buf"
        }
        sleep 1.0
    }
    if { [scan $buf {%d, %f, %[^,], %d} n r m c] != 4 } {
        return -code error "Invalid readback: $buf"
    }
    if { $c != $chn } {
        return -code error "Reading mixup: got reading for channel $c instead of $chn"
    }
    varDoSet /${ins}/read_$ch -v $r
}


CAMP_INT /~/num_chan -D -S -T "Number of channels" \
    -H "Set the number of connected channels (nust be connected starting at 1)" \
    -d on -s on -v 2 \
    -writeProc asdgm_num_chan_w

proc asdgm_num_chan_w { ins num } {
    if { $num < 0 || $num > 4 } {
        return -code error "Invalid number.  Must be in range 0 to 4."
    }
    varDoSet /${ins}/num_chan -v $num
    for { set i 1 } { $i <= 4 } { incr i } {
        set c [lindex { ? x y z t } $i]
        if { $i <= $num } {
            varDoSet /${ins}/read_$c -d on -r on
        } else {
            varDoSet /${ins}/read_$c -d off -r off -p off
        }
    }
}
