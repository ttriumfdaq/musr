# camp_ins_bnc625a.tcl
# BNC 625A / BNC625lex SG-100/A  Arbitrary Signal Generator
#
# This driver uses only CW sinewave and arbitrary function modes.
#
# DC functions have been eliminated in favor of direct control of the
# offset voltage.
#
CAMP_INSTRUMENT /~ -D -T "BNC 625A Signal Generator" \
    -H "BNC 625/A Arbitrary Signal Generator" -d on \
    -initProc BNC625_init \
    -deleteProc BNC625_delete \
    -onlineProc BNC625_online \
    -offlineProc BNC625_offline

global BNC625Names
global BNC625Functions
# BNC625Functions($name) is a list: check trig t0 t1 mag sl_coeff function_string
set BNC625Names "Sine Sine_1_3 Gauss sech_AM Hermite {sint/t} Re_sech Im_sech"
set BNC625Functions(Sine)     {1 1 0 6.2832 2047.3 0.0 {sin($t)}}
set BNC625Functions(Sine_1_3) {2 1 0 6.2832 1329.5 0.0 {sin($t)+sin(3.0*$t)}}
set BNC625Functions(Gauss)    {3 0 -2.9 2.9 2047.3 0.0 {exp(-($t*$t))}}
set BNC625Functions(sech_AM)  {4 0 -9. 9. 2047.3 0.0 {1.0/cosh($t)}}
set BNC625Functions(Hermite)  {5 0 -3.2 3.2 2047.3 0.18 {(1.0-0.957*$t*$t)*exp(-$t*$t)}}
set BNC625Functions(sint/t)   {6 0 -31.4159265 31.4159265 2047.3 .1 {sin($t)/$t}}
set BNC625Functions(Re_sech)  {7 0 -5. 5. 2047.3 0.796 {cos(-5.0*log(cosh($t)))/cosh($t)}}
set BNC625Functions(Im_sech)  {8 0 -5. 5. 2047.3 0.796 {sin(-5.0*log(cosh($t)))/cosh($t)}}

# trig is the trigger mode appropriate for the function: 0 (triggered)
#      for pulses and 1 (continuous) repeating functions
# sl_coeff (slice coefficient) converts between duration and frequency slice.
# slice = sl_coeff * beta = sl_coeff * (f_last_t-f_first_t) / duration
# The provided sl_coeff is only meaningful for the hermite and the 
# two-part of the complex sech pulse; values for the others are just 
# filler.

proc BNC625_init { ins } {
   insSet /${ins} -if rs232 0.33 1 /tyCo/1 9600 8 none 1 none none
}
proc BNC625_delete { ins } {
  insSet /${ins} -line off
}
proc BNC625_online { ins } {
  insIfOn /${ins}
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == 0) } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections"
  }
  varRead /${ins}/setup/mode
  if { [varGetVal /${ins}/setup/mode] == 1 } {
    # start up in "arbitrary function mode"
    insIfWrite /${ins} "MLF40F0"
    varDoSet /${ins}/function -v 0
  } else {
    varDoSet /${ins}/function -v 1
    varRead /${ins}/frequency
  }
}
proc BNC625_offline { ins } {
  insIfOff /${ins}
}

CAMP_FLOAT /~/amplitude -D -S -R -P -L -T "Amplitude" \
    -d on -s on -r on -p off -p_int 10 -units V \
    -readProc BNC625_par_r -writeProc BNC625_ampl_w
  proc BNC625_ampl_w { ins val } {
      set pp [expr { abs(2.0*$val)}]
      if { [varGetVal /${ins}/function] == 1 } {
          insIfWrite /${ins} "M0 F4 $pp Y F0"
          varDoSet /${ins}/setup/mode -v Sine
      } else {# mode arb, choose level, give value p-p, Volts
          insIfWrite /${ins} "ML F3 $pp Y F0"
          varDoSet /${ins}/setup/mode -v Arb
      }
      varDoSet /${ins}/amplitude -v $val
  }

CAMP_FLOAT /~/offset -D -S -R -P -L -T "Voltage Offset" \
    -d on -s on -r on -p off -p_int 10 -units V \
    -readProc BNC625_offset_r -writeProc BNC625_offset_w
  proc BNC625_offset_r { ins } {
      # Get screen for Offset
      set buf [ insIfRead /${ins} {O E1 E0 O} 112 ]
      set v 0
      set n [ scan $buf { %*[^""]"  Offset:  %[^m]%s "} v u ]
      # remove comma and spaces from number 
      set v [join [split $v {, }] {}]
      # convert mV to V
      set v [expr {$v*0.001}]
      varDoSet /${ins}/offset -v $v
  }
  proc BNC625_offset_w { ins val } {
      if { abs($val) > 6.0 } {
          return -code error "out of range -6..6"
      }
      # Write value as integer mV
      set mV [expr {round(1000.0*$val)}]
      insIfWrite /${ins} "O $mV X O"
      varDoSet /${ins}/offset -v $val
  }

CAMP_SELECT /~/trigger -D -S -R -T "Triggering Mode" \
    -H "Triggering mode: continuous or single trigger" \
    -d on -s on -r on -selections Triggered Continuous -v 0 \
    -readProc BNC625_par_r -writeProc BNC625_tmode_w
  proc BNC625_tmode_w { ins val } {
      if { [varGetVal /${ins}/function] != 1 } {
          insIfWrite /${ins} "ML F1 $val F0"
      }
      varDoSet /${ins}/trigger -v $val
  }

  proc BNC625_par_r { ins } {
      # None Sine Sine_1_3 Gauss Hermite {sint/t} sech_AM Re_sech Im_sech Other_Func
      if { [varGetVal /${ins}/function] == 1 } {
          BNC625_CW_par_r $ins
      } else {
          BNC625_arb_par_r $ins
      }
  }
  # Read parameters in arb-function mode.  Data comes from a screen dump of exactly
  # 115 characters, and no terminator.
  proc BNC625_arb_par_r { ins } {
      set buf [ insIfRead /${ins} {ML F0 E1 E0} 115 ]
      set n [ scan $buf { %*[^"]"Arb Mode Int Clock " "%s Clock: %s Hz %s mV %c} t f v c ]
      if { $n < 4 } { return -code error "Failed parse of screen buffer" }
      switch $t {
        Trig { varDoSet /${ins}/trigger -v Triggered }
        Cont { varDoSet /${ins}/trigger -v Continuous }
        default {}
      }
  #   remove commas from number:
  #   varDoSet /${ins}/ticks -v [join [split $f ,] {}]
      varDoSet /${ins}/amplitude -v [expr {0.0005 * [join [split $v {, }] {}] } ]
      set n [varGetVal /${ins}/setup/num_pt]
      set f [join [split $f {, }] {}]
      if { $n < 1 } { set n 1 }
      varDoSet /${ins}/duration -v [expr {$n/$f} ]
      varDoSet /${ins}/frequency -v [format {%.3f} [expr {$f/$n} ]]
  }
  # Read parameters in CW mode
  proc BNC625_CW_par_r { ins } {
      set buf [ insIfRead /${ins} {M0 F0 E1 E0} 115 ]
      varDoSet /${ins}/trigger -v Continuous 
      set n [ scan $buf { %*[^"]"Sine " " %s Hz %c } f c ]
      if { $n < 2 } { return -code error "Failed parse of frequency" }
  #   remove commas from number:
      set f [join [split $f ,] {}]
      varDoSet /${ins}/frequency -v [format {%.3f} $f]
      set n [ scan $buf { %*[^"]"Sine " " %s Hz %s mV %c } f v c ]
      if { $n < 3 } { return -code error "Failed parse of amplitude" }
      varDoSet /${ins}/amplitude -v [expr {0.0005 * [join [split $v ,] {}] } ]
  }

CAMP_FLOAT /~/duration  -D -S -R -L -T "Duration of function" \
    -H "Time, in seconds, to output functional shape (reciprocal of frequency)" \
    -d on -r on -s on -units "s" -v 0.01 \
    -readProc BNC625_par_r -writeProc BNC625_duration_w
  proc BNC625_duration_w { ins val } {
      if { [varGetVal /${ins}/function] == 1 } {# CW sinewave
          varSet /${ins}/frequency -v [expr {1.0/$val}]
      } else {# Arb functions
          set slc [varGetVal /${ins}/sl_coeff]
          if { $val > 0. && $slc > 0.0 } {
              set fsl [ expr { $slc * ([varGetVal /${ins}/f_last_t]-[varGetVal /${ins}/f_first_t]) / $val } ]
              if { $fsl > 0.1 } {
                  varDoSet /${ins}/freq_slice -v $fsl
              }
          }
          set n [varGetVal /${ins}/setup/num_pt]
          set val [expr $val/double($n) ]
#         set tick frequency as num_points/duration (maximum 40e6 Hz)
          if { $val < 0.25e-7 } { set val 0.25e-7 }
          varDoSet /${ins}/setup/mode -v "Arb"
          insIfWrite /${ins} "ML F2 [expr {1.0/$val}] X F0"
          varDoSet /${ins}/duration -v [ expr {$val*$n} ]
          varDoSet /${ins}/frequency -v [format {%.4f} [ expr {1.0/($val*$n)} ] ]
      }
  }

CAMP_FLOAT /~/frequency  -D -S -R -P -L -T "Output frequency" \
    -H "Output frequency (reciprocal of duration)" \
    -d on -r on -p off -p_int 10 -s on -units "Hz" -v 100.0 \
    -readProc BNC625_par_r -writeProc BNC625_frequency_w
  proc BNC625_frequency_w { ins val } {
      set val [expr { abs($val) }]
      if { [varGetVal /${ins}/function] == 1 } {# CW functions
          varDoSet /${ins}/setup/mode -v "Sine"
          if { $val > 21.5e6 } { set val 21.5e6 }
          insIfWrite /${ins} "M0 F1 $val X F0"
          varDoSet /${ins}/duration -v [ expr {1.0/$val} ]
          varDoSet /${ins}/frequency -v [ format {%.3f} $val ]
      } else {# Arb functions
          varDoSet /${ins}/setup/mode -v "Arb"
          set n [varGetVal /${ins}/setup/num_pt]
          set val [expr {$val*double($n)} ]
          #  set tick frequency as num_points/duration (maximum 40e6 Hz)
          if { $val > 40.0e6 } { set val 40.0e6 }
          insIfWrite /${ins} "ML F2 [format {%.2f} $val] X F0"
          varDoSet /${ins}/duration -v [ expr {$n/$val} ]
          varDoSet /${ins}/frequency -v [ format {%.3f} [ expr {$val/$n} ] ]
      }
  }

CAMP_SELECT /~/function -D -S -T "Functional Form" -d on -s on \
    -selections None Sine Sine_1_3 Gauss Hermite {sint/t} sech_AM Re_sech Im_sech Other_Func \
    -v 0 -writeProc BNC625_func_form_w 
  proc BNC625_func_form_w { ins val } {
      global BNC625Functions
      varDoSet /${ins}/function -v $val
      set fn [ varSelGetValLabel /${ins}/function ]
      switch [ lindex [list 0 1 2 2 2 2 2 2 2 3] $val ] {
	  0 {# none
              varDoSet /${ins}/setup/mode -v Arb
	      insIfWrite /${ins} "MLWI 0 0 0 X F0"
              varDoSet /${ins}/function -m ""
	      return
	    }
          1 {# CW sinewave
              varDoSet /${ins}/setup/mode -v Sine
	      insIfWrite /${ins} "M0 F0"
              varSet /${ins}/trigger -v Continuous
              varDoSet /${ins}/function -m ""
          }
	  2 {# pre-defined soft functions
              varRead /${ins}/setup/max_freq
	      set npt [varGetVal /${ins}/setup/num_pt]
	      set fpar $BNC625Functions(${fn})
              set tm  [ lindex $fpar 1 ]
              if { $tm != [varGetVal /${ins}/trigger -v $tm] } {
                  varSet /${ins}/trigger -v $tm
              }
              set fp1 [ lindex $fpar 2 ]
              set fp2 [ lindex $fpar 3 ]
              set fp3 [ lindex $fpar 4 ]
              set sl_coeff [ lindex $fpar 5 ]
              set fp5 [ lindex $fpar 6 ]
              set s [ BNC625_load_func $ins $npt $fp1 $fp2 $fp3 $fp5 ]
              if { $s == 1 } {# success 
                  varDoSet /${ins}/f_first_t -v $fp1
                  varDoSet /${ins}/f_last_t -v $fp2
                  varDoSet /${ins}/sl_coeff -v $sl_coeff
                  varDoSet /${ins}/other_func -v $fp5
                  set fsl [ varGetVal /${ins}/freq_slice ]
                  if { $fsl > 0.01 && $sl_coeff > 0.01 } {
                      varSet /${ins}/duration -v [ expr {$sl_coeff * ($fp2-$fp1) / $fsl }]
                  }
              } else {# failure
                  return -code error "Failed to load function $fn"
              }
	    }
	  3 {# Arbitrary, user-entered function
              varRead /${ins}/setup/max_freq
	      set npt [varGetVal /${ins}/setup/num_pt]
	      set s [ BNC625_load_func $ins $npt [varGetVal /${ins}/f_first_t] \
                  [varGetVal /${ins}/f_last_t] 0.0 [varGetVal /${ins}/other_func] ]
	      if { $s != 1 } {
		  return -code error "Failed to load function"
	      }
          }
      }
      # update frequency/duration, amplitude because they are saved separately
      # within the instrument for different modes.
      varSet /${ins}/frequency -v [varGetVal /${ins}/frequency]
      varSet /${ins}/amplitude -v [varGetVal /${ins}/amplitude]
  }

# Here we evaluate the function to produce a list of $npt values and load them
# into the BNC SG.  we evaluate $mag*function($t) for $t from $t0 to $t1.
# Returns logical 1=success, 0=failure
#
# For continuous waveforms, indicated by a trigger mode "Continuous", the 
# function is sampled at times corresponding to the *end* of each time bin.
# For pulses, as indicated by a "triggered" trigger mode, the function is 
# sampled at times corresponding to the time-center of each point.  For example, 
# with a continuous function whose min,max are 0,1, and npt=5, the points are 
# evaluated at times 0.2,0.4,0.6,0.8,1.0; but in individual-trigger mode the
# points are taken at times 0.1,0.3,0.5,0.7,0.9.
#
  proc BNC625_load_func { ins npt t0 t1 mag fun } {
      # suppress output while loading (level=5mV):
      varDoSet /${ins}/setup/mode -v "Arb"
      insIfWrite /${ins} "ML F3 5 X F0"
      set tm [ varGetVal /${ins}/trigger ]
      set dt [ expr { (0.0+$t1-$t0)/($npt) } ]
      set init [ expr { $t0 + $dt*($tm ? 1.0 : 0.5 ) } ]
      if { $mag <= 0.0 } {# determine magnitude automatically
	  for { set i 0 } { $i < $npt } { incr i } {
	      set t [ expr {$init+$dt*$i} ]
	      set val [ expr abs($fun) ]
	      if { $val > $mag } { set mag $val }
	  }
	  set mag [ expr 2047.0/$mag ]
      }
      if { $mag <= 0.0 } { 
	  return -code error "Function does not depend on \$t"
      }
      set vstr "MLWI"
      set f_of_t "round( [ expr double($mag) ]*($fun) )"
      for { set i 0 } { $i < $npt } { incr i } {
	  if { [ string length $vstr ] > 200 } {# write long strings in pieces
	      insIfWrite /${ins} "$vstr"
	      set vstr ""
	  }
	  set t [ expr {$init+$dt*$i} ]
	  append vstr " [ expr $f_of_t ]"
      }
      insIfWrite /${ins} "$vstr X F0"
      varDoSet /${ins}/function -m "Currently: $fun"
      return 1
  }

CAMP_STRING /~/other_func -D -S -T "Arbitrary function" \
    -H "For functions not in the menu. Enter any arbitrary function of \$t, plus t-range, then set function to Other_Func" \
    -d on -s on -writeProc BNC625_other_func_s
  proc BNC625_other_func_s { ins val } {
      if { [string first "\$t" $val] == -1 } {
	  return -code error "Expression is not a function of \"\$t\""
      }
      varDoSet /${ins}/other_func -v $val
  }

CAMP_FLOAT /~/f_first_t -D -S -T "Function's first \$t" \
    -H "First value for \$t in arbitrary \"other function\"." \
    -d on -s on -writeProc BNC625_firstt_s
  proc BNC625_firstt_s { ins val } {
      varDoSet /${ins}/f_first_t -v $val
  }

CAMP_FLOAT /~/f_last_t -D -S -T "Function's last \$t value" \
    -H "Last value for \$t in arbitrary \"other function\"." \
    -d on -s on -writeProc BNC625_lastt_s
  proc BNC625_lastt_s { ins val } {
      varDoSet /${ins}/f_last_t -v $val
  }

CAMP_FLOAT /~/freq_slice  -D -S -T "Frequency slice" \
    -H "Desired irradiation frequncy slice" \
    -d on -s on -units "Hz" -v 1000.0 \
    -writeProc BNC625_freq_slice_w
  proc BNC625_freq_slice_w { ins val } {
      if { $val > 0.1 } {
          set d [ expr { [varGetVal /${ins}/sl_coeff] * \
                  ([varGetVal /${ins}/f_last_t]-[varGetVal /${ins}/f_first_t]) / $val } ]
          if { $d > 1.0e-6 } { varSet /${ins}/duration -v $d }
      }
      varDoSet /${ins}/freq_slice -v $val
  }

CAMP_FLOAT /~/sl_coeff -D -S -T "Freq slice coeff" \
    -H "Frequency slice coefficient: slice = sl_coeff * beta" \
    -d on -s on -v 1.0 \
    -writeProc BNC625_sl_coeff_w
  proc BNC625_sl_coeff_w { ins val } {
      varDoSet /${ins}/sl_coeff -v $val
  }

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc BNC625_id_r
  proc BNC625_id_r { ins } {
      set id 0
      set buf x
      catch {insIfRead /${ins} "V" 100} buf
      set id [scan $buf " BNC model: %d" val ]
      if { $id } { set id [expr $val == 625 ] }
      varDoSet /${ins}/setup/id -v $id
  }

CAMP_SELECT /~/setup/init -D -R -P -T "Initialization" \
    -d on -r off -p on -p_int 5 -selections init \
    -readProc BNC625_init_r
  proc BNC625_init_r { ins } {
      varRead /${ins}/amplitude
      if { [varGetVal /${ins}/function] == 0 } { varSet /${ins}/function -v 0 }
      varDoSet /${ins}/setup/init -p off
      varRead /${ins}/setup/max_freq
  }

CAMP_INT /~/setup/num_pt -D -S -T "Number of points" \
    -H "Number of points used to generate functions" \
    -d on -s on -v 128 -writeProc BNC625_num_pt_w
  proc BNC625_num_pt_w { ins val } {
      if { $val > 32766 || $val < 1 } {
          return -code error "Number of points must be in range 1 to 32766"
      }
      varDoSet /${ins}/function -v None
      varDoSet /${ins}/setup/num_pt -v $val
      varRead /${ins}/setup/max_freq
  }

CAMP_INT /~/setup/max_freq -D -R -T "Max Frequency" \
    -H "Maximum possible Arb synthesis frequency, based on number of points" \
    -d on -r on -readProc BNC625_max_freq_r
  proc BNC625_max_freq_r { ins } {
      catch {
          set n [varGetVal /${ins}/setup/num_pt]
          varDoSet /${ins}/setup/max_freq -v [expr {int( (40.0e6 / $n) + .01 )}]
  }   }

CAMP_SELECT /~/setup/mode -D -R -T "Operating mode" \
    -H "Current device operating mode" \
    -selections "Sine" "Arb" "Function" "Int AM" "External AM" "Int FM" "External FM" "Other" \
    -d on -v 0 -r on -readProc BNC625_op_mode_r
  proc BNC625_op_mode_r { ins } {
      set buf [ insIfRead /${ins} {E1 E0} 110 ]
      foreach m [list "Sine" "Arb" "Function" "Int AM" "External AM" "Int FM" "External FM" "Other"] {
          if { [string match "*\"${m}*" $buf ] } { break }
      }
      varDoSet /${ins}/setup/mode -v $m
  }
