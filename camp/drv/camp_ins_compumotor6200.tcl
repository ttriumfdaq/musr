# camp_ins_compumotor6200.tcl
#
# $Log: camp_ins_compumotor6200.tcl,v $
# Revision 1.5  2015/11/29 04:26:42  asnd
# Recover lost version 1.2
#
# Revision 1.4  2015/03/15 00:51:05  suz
# Ted's 201404 interface changes
#
# Revision 1.2  2013/05/21 04:09:23  asnd
# Add set-and-move vars, more velocity checks.
#
# Revision 1.1  2007/10/10 04:30:42  asnd
# Compumotor system -- used M9b cryostat position control
#
#

CAMP_INSTRUMENT /~ -D -T "Compumotor 6200 Cryostat position" \
    -H "Parker Compumotor 6200 cryostat positioning system" -d on \
    -initProc CM6k_init -deleteProc CM6k_delete \
    -onlineProc CM6k_online -offlineProc CM6k_offline

# Many of the readbacks from his device are formatted on multiple 
# lines, which are very difficult to read.  The trick used in the 
# OMS motor driver doesn't work here because only one command in a
# train can give output.  Specifically, the command echo is followed
# by CRLF, uncontrollably.  The characters specified by EOT, plus
# CRLF are put at the end of a response.  The characters specified 
# by EOL are inserted between lines of a multiline response.  Thus
# it is possible to read multiline responses by setting EOL 32,0,0
# We will set ECHO0 to prevent command echoing.  Error messages are
# still preceded by CRLF, so we don't get to see them; set ERRLVL2
# to suppress them.  

proc CM6k_init { ins } {
    insSet /${ins} -if rs232 0.4 2 /tyCo/1 9600 8 none 1 CRLF CRLF
}

proc CM6k_delete { ins } { insSet /${ins} -line off }

proc CM6k_online { ins } {
    if { [string compare [insGetIfTypeIdent /$ins] rs232] } {
        return -code error "Interface must be rs232"
    }
    insIfOn /${ins}

    # to keep responses on a single line, set EOL to be <space><semicolon>
    # and set command echoing off.  Set low verbosity because the error 
    # messages get lost anyway.

    insIfWrite /$ins "EOT13,10,0:EOL32,59,0:ECHO0:ERRLVL4"

    if { [catch { insIfRead /$ins "TSTAT" 600} buf ] } {
        insIfOff /$ins
        return -code error "Failed to read status $buf"
    }

    if { 3 != [scan $buf {*%*d Revision %*d-%*d-%*d-%f %d ;* Participating Axes %d} ver model nax] } {
        insIfOff /$ins
        return -code error "Failed identity check $buf"
    }
    if { $model != 6200 } {
        insIfOff /$ins
        return -code error "Wrong model number: $model"
    }
    if { $nax != 2 } {
        insIfOff /$ins
        return -code error "Wrong number of axes: $nax (should be 2)"
    }

    # Set absolute positioning on, scaling on, hard limits on,
    # (status) inputs enabled, drive fault level = 1, 
    # no encoders.
    insIfRead /$ins "@MA1 : SCALE1 : @LH1 : !DRFLVL11 : INFEN1 : @ENC0" 40

    # @ESTALL1   -- detect motor stall; needs encoders
    # @ESK1      -- kill move on stall;

    # Perform more thorough initialization hidden in the background
    # (else the online operation would time out).
    varDoSet /${ins}/movement -p on -p_int 20 -v WAIT \
        -m "Please wait for full initialization"
    varDoSet /${ins}/internal/initial -p on -p_int 2.0
}

proc CM6k_offline { ins } { insIfOff /${ins} }

CAMP_SELECT /~/movement -D -R -P -S -T "Movement status" \
        -d on -r on -s on -p off -p_int 6 \
        -H {Set or read movement status. Set this to start or stop a move.} \
        -selections STOP MOVE STUCK LIMIT FAULT WAIT \
        -readProc CM6k_movement_r -writeProc CM6k_movement_w

proc CM6k_movement_r { ins } {
    set prev [varGetVal /${ins}/movement]
    set p 4
    set ii [varGetVal /${ins}/internal/merr]
    varDoSet /${ins}/internal/merr -v [incr ii] -m "m_r A"

    # Read positions (read gagemux before motors to reduce discrepancies)
    if { [string length /${ins}/absolute/abs_sensor_dev] } {
        if { [catch {
            varRead /${ins}/abs_pos_v
            varRead /${ins}/abs_pos_h
        } buf ] } {
            set msg "Sensor failure $buf"
        }
    }
    CM6k_motorpos_r $ins

    varDoSet /${ins}/internal/merr -m "m_r B"
    set buf [insIfRead /$ins "!TAS" 90]
#       moving dir shutdn fault hwhilim hwlolim swhilim swlolim indb inpos
    if { [scan $buf \
       " *TAS%1d%1d%*2d_%*d_%*d_%1d%1d%1d%1d_%1d%1d%1d%1d_%*d_%*d_%*d ;* %1d%1d%*2d_%*d_%*d_%1d%1d%1d%1d_%1d%1d%1d%1d_" \
       v1 v2 v13 v14 v15 v16 v17 v18 v19 v20 h1 h2 h13 h14 h15 h16 h17 h18 h19 h20 \
       ] != 20 } {
          return -code error "Bad movement status: $buf"
    }
    varDoSet /${ins}/internal/merr -m "m_r C"
    set moving [expr {$v1 || $h1}]
    set fault [expr {$v13 || $v14 || $h13 || $h14}]
    set hwlim [expr {$v15 || $v16 || $h15 || $h16}]
    set swlim [expr {$v17 || $v18 || $h17 || $h18}]

    if { $moving } {
        set vdir [lindex {"" "v+" "v-"} [expr {$v1*(1+$v2)}]]
        set hdir [lindex {"" "h+" "h-"} [expr {$h1*(1+$h2)}]]
        set msg "Moving $vdir $hdir  "
        set p 1
    } else {
        set msg "Stopped. "
        set p 12
    }

    varDoSet /${ins}/internal/merr -m "m_r D"
    if { [string length /${ins}/absolute/abs_sensor_dev] } {
        if { [CM6k_abs_in_tol $ins] == 0 } {
            set moving 2
            set p 6
            append msg "Gauge disagreement - STUCK "
        }
    }

    varDoSet /${ins}/internal/merr -m "m_r E"
    if { $fault } {
        append msg "FAULT! "
        set moving 4
    }
    varDoSet /${ins}/internal/merr -m "m_r F"
    if { $hwlim } {
        set msg  [join [list "HIT" \
		      [lindex {{} { v+}} $v15] [lindex {{} { v-}} $v16] \
		      [lindex {{} { h+}} $h15] [lindex {{} { h-}} $h16] \
			    " LIMIT SWITCH! "] ""]
    }
    if { $swlim } { append msg "At limit. " }
    if { $hwlim || $swlim } { set moving 3 }

    varDoSet /${ins}/internal/merr -m "m_r G"

    varDoSet /${ins}/internal/merr -m "m_r H"
    #  Apply new state, message, and poll interval
    varDoSet /${ins}/movement -v $moving -m $msg -p_int $p

    #  Detect the beginning of a fault condition,
    if { $prev <= 2 && $moving >= 2 } {
    varDoSet /${ins}/internal/merr -m "m_r I"
        # STOP -- abort movement
        insIfWrite /$ins "!S"
    }
    varDoSet /${ins}/internal/merr -v 0 -m "m_r J"
  }

  proc CM6k_movement_w { ins target } {
    #varRead /${ins}/movement
    set prev [varGetVal /${ins}/movement]
    # STOP -- abort movement
    insIfWrite /$ins "!S"
    varDoSet /${ins}/movement -v 0 -p on -p_int 12
    if { $target == 1 } {# MOVE -- initiate movement
        set v [varGetVal /${ins}/destin_v]
        set h [varGetVal /${ins}/destin_h]
        if { $v < [varGetVal /${ins}/limits/v_low] || \
             $v > [varGetVal /${ins}/limits/v_high] || \
             $h < [varGetVal /${ins}/limits/h_low] || \
             $h > [varGetVal /${ins}/limits/h_high] } {
                return -code error "Destination is outside limits"
        }
        if { $prev > 1 } {
            # After hitting a limit, the limit status must be cleared,
            set lhv [varGetVal /${ins}/limits/v_hard_lim]
            set lhh [varGetVal /${ins}/limits/h_hard_lim]
            insIfWrite /$ins "LS0,0 : LH0,0 : LS3,3 : LH$lhv,$lhh"
        }
        set hv [varGetVal /${ins}/setup/hysteresis_v]
        set hh [varGetVal /${ins}/setup/hysteresis_h]
        if { $hv == 0.0 && $hh == 0.0 } {
            insIfWrite /$ins "D${v},${h} : GO"
        } else {
            # Calculate waypoint
            set v1 [expr {$v+[varGetVal /${ins}/setup/hysteresis_v]}]
            set h1 [expr {$h+[varGetVal /${ins}/setup/hysteresis_h]}]
            # Initate two-stage move
            insIfWrite /$ins "D${v1},${h1} : GO : D${v},${h} : GO"
        }
	set vel [varGetVal /${ins}/setup/velocity]
	if { $vel < 0.01 } {
	    set vel 0.01
	    varSet /${ins}/setup/velocity -v $vel
	}
        varDoSet /${ins}/movement -v 1 -p off
        varDoSet /${ins}/movement -p on -p_int [expr { 0.1/$vel > 1 ? 0.1/$vel : 1 }]
        #CM6k_check_error $ins
    }
 }

# The destination gets applied to the unit when it is set, and also when we GO.
# It is also readable, but that is intended for debugging or synchronization,
# not for everyday use.  (The reading is correct when hysteresis is zero.)

CAMP_FLOAT /~/destin_v -D -S -L -T "Set v position" \
	-d on -s on -units "mm" \
        -H "To move apparatus, set both destin_v and destin_h, then set movement to MOVE." \
	-writeProc CM6k_destin_v_w

  proc CM6k_destin_v_w { ins target } {
    insIfWrite /$ins "D[format %.4f $target],"
    varDoSet /${ins}/destin_v -v $target
  }

CAMP_FLOAT /~/destin_h -D -S -L -T "Set h position" \
	-d on -s on -units "mm" \
        -H "To move apparatus, set both destin_v and destin_h, then set movement to MOVE." \
	-writeProc CM6k_destin_h_w

  proc CM6k_destin_h_w { ins target } {
    insIfWrite /$ins "D,[format %.4f $target]"
    varDoSet /${ins}/destin_h -v $target
  }

CAMP_FLOAT /~/motor_pos_v -D -R -P -L -A -T "Read v position" \
	-d on -r on -tol 0.1 -units "mm" \
	-readProc CM6k_motorpos_r

CAMP_FLOAT /~/motor_pos_h -D -R -P -L -A -T "Read h position" \
	-d on -r on -tol 0.1 -units "mm" \
	-readProc CM6k_motorpos_r

  proc CM6k_motorpos_r { ins } {
    set buf [insIfRead /$ins "!TPM" 80]
    if { [scan $buf "*TPM%f,%f" v h] != 2 } {
        return -code error "Bad response: $buf"
    }
    varDoSet /${ins}/motor_pos_v -v [format %.4f $v]
    varTestAlert /${ins}/motor_pos_v [varGetVal /${ins}/destin_v]
    varDoSet /${ins}/motor_pos_h -v [format %.4f $h]
    varTestAlert /${ins}/motor_pos_h [varGetVal /${ins}/destin_h]
  }

CAMP_FLOAT /~/abs_pos_v -D -R -P -L -T "Absolute v position" \
    -H {v-axis position as read by external position encoder} \
    -d on -r on -units "mm" \
    -readProc {CM6k_abspos_r v}

CAMP_FLOAT /~/abs_pos_h -D -R -P -L -T "Absolute h position" \
    -H {h-axis position as read by external position encoder} \
    -d on -r on -units "mm" \
    -readProc {CM6k_abspos_r h}

  proc CM6k_abspos_r { ax ins } {
    set sens_dev [string trim [varGetVal /${ins}/absolute/abs_sensor_dev] "/ "]
    if { $sens_dev == "" ||
         [catch {insGetTypeIdent /$sens_dev} type] ||
         [string compare $type "asd_gagemux"] } {
        varDoSet /${ins}/abs_pos_$ax -r off -p off
        return -code error "External sensor device not set up properly. Check /${ins}/absolute/abs_sensor_dev."
    }

    if { [varGetVal /${ins}/absolute/vertical_chan] == 0 } { # v = x
        set map(v) x ; set map(h) y
    } else { # v = y
        set map(v) y ; set map(h) x
    }

    set sign [expr { [varGetVal /${ins}/absolute/polarity_$ax] ? -1 : 1 }]
    set off [varGetVal /${ins}/absolute/offset_$ax]
    varRead /${sens_dev}/read_$map($ax)
    set val [expr { $sign*[varGetVal /${sens_dev}/read_$map($ax)] + $off } ]
    varDoSet /${ins}/abs_pos_$ax -v $val -m "$ax $map($ax) $sign $off /${sens_dev}/read_$map($ax)"
  }


CAMP_FLOAT /~/goto_v -D -S -T "set and go vertical" \
    -H {Shortcut to set vertical destination and then begin movement} \
    -d on -s on -units "mm" \
    -writeProc {CM6k_goto_w v}

CAMP_FLOAT /~/goto_h -D -S -T "set and go horizontal" \
    -H {Shortcut to set horizontal destination and then begin movement} \
    -d on -s on -units "mm" \
    -writeProc {CM6k_goto_w h}

proc CM6k_goto_w {ax ins dest} {
    varSet /${ins}/destin_$ax -v $dest
    varSet /${ins}/movement -v MOVE
    varDoSet /${ins}/goto_$ax -v $dest
}

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

CAMP_SELECT /~/setup/define_pos -D -S -T "Define position" \
    -d on -s on  \
    -selections {Do nothing} {Declare Zero} {Equate to Destin} {Equate to abs_pos} \
    -H {Set this to define the present position to be some value. No movement involved.} \
    -writeProc CM6k_def_pos_w

proc CM6k_def_pos_w { ins target } {
    if {$target == 0 } {# Do nothing
        return
    }

    CM6K_no_moving $ins "Defining position"

    if { $target == 1 } {# Declare Zero
        varDoSet /${ins}/destin_v -v 0.0
        varDoSet /${ins}/destin_h -v 0.0
        set target 2
    }
    if { $target == 2 } {# Equate to (Zero or) Destin
        set v [format %.3f [varGetVal /${ins}/destin_v]]
        set h [format %.3f [varGetVal /${ins}/destin_h]]
        insIfWrite /$ins "PSET$v,$h"
        catch {# Adjust the offset (origin) of the absolute position readback 
            # ($ap-$ao is raw external absolute position reading)
            varRead /${ins}/abs_pos_v
            set ap [varGetVal /${ins}/abs_pos_v]
            set ao [varGetVal /${ins}/absolute/offset_v]
            varSet /${ins}/absolute/offset_v -v [expr {$v-($ap+$ao)}]
            varDoSet /${ins}/abs_pos_v -v $v
            #
            varRead /${ins}/abs_pos_h
            set ap [varGetVal /${ins}/abs_pos_h]
            set ao [varGetVal /${ins}/absolute/offset_h]
            varSet /${ins}/absolute/offset_h -v [expr {$h-($ap+$ao)}]
            varDoSet /${ins}/abs_pos_h -v $h
        }
    }
    if { $target == 3 } {# Equate to (raw or offset) absolute sensor reading
        varRead /${ins}/abs_pos_v
        varRead /${ins}/abs_pos_h
        set v [format %.3f [varGetVal /${ins}/abs_pos_v]]
        set h [format %.3f [varGetVal /${ins}/abs_pos_h]]
        insIfWrite /$ins "PSET$v,$h"
    }
    varDoSet /${ins}/destin_v -v $v
    varDoSet /${ins}/destin_h -v $h

    varRead /${ins}/movement
}

CAMP_FLOAT /~/setup/velocity -D -S -R -T "Move velocity" \
        -d on -s on -r on -units {mm/s} -v 0.2 \
        -readProc CM6k_vel_r -writeProc CM6k_vel_w
  # For reading the velocity, let's average both axes
  proc CM6k_vel_r { ins } {
    set buf [insIfRead /$ins V 48]
    if { [scan $buf "*V%f,%f" v1 v2] < 2 } {
      return -code error "Invalid readback: $buf"
    }
    set v [expr {0.5*($v1+$v2)}]
    varDoSet /${ins}/setup/velocity -v $v
  }
  proc CM6k_vel_w { ins v } {
    insIfWriteVerify /$ins "!@V$v" "V" 48 /${ins}/setup/velocity " *V%f," 1 $v 0.05
  }

CAMP_FLOAT /~/setup/acceleration -D -S -R -T "Move acceleration" \
        -d on -s on -r on -units {mm/s^2} \
        -readProc CM6k_accel_r -writeProc CM6k_accel_w
  # For reading the acceleration, let's average both axes
  proc CM6k_accel_r { ins } {
    set buf [insIfRead /$ins A 48]
    if { [scan $buf "*A%f,%f" a1 a2] < 2 } {
      return -code error "Invalid readback: $buf"
    }
    set a [expr {0.5*($a1+$a2)}]
    varDoSet /${ins}/setup/acceleration -v $a
  }
  proc CM6k_accel_w { ins a } {
    insIfWriteVerify /$ins "!@A$a" "A" 60 /${ins}/setup/acceleration " *A%f," 1 $a 0.1
  }

CAMP_INT /~/setup/calib_v -D -S -R -T "v calibration" -d on -s on -r on \
        -H "Distance calibration for v-axis movement" \
        -v 838000 -units "step/mm" \
        -readProc CM6k_calib_r -writeProc {CM6k_calib_w v 1 {}}

CAMP_INT /~/setup/calib_h -D -S -R -T "h calibration" -d on -s on -r on \
        -H "Distance calibration for h-axis movement" \
        -v 19685 -units "step/mm" \
        -readProc CM6k_calib_r -writeProc {CM6k_calib_w h 2 ,}

# When setting the scaling, use the same scaling for distance, velocity, and acceleration.
#                          If DRES = 200     DRES=25000
# h scale = DRES * 2rev/2.54mm  =  157.48     19685
# v scale = DRES * 50rev/1.57mm = 6369.43    796178
proc CM6k_calib_w { ax n comma ins cal } {
    CM6K_no_moving $ins "Changing calibration"
    set ccal "$comma$cal,"
    insIfWrite /$ins "SCLA${ccal}:SCLV${ccal}"
    insIfWriteVerify /$ins "SCLD${ccal}" "${n}SCLD" 48 /${ins}/setup/calib_${ax} " *%*dSCLD%d" 1 $cal
}

proc CM6k_calib_r { ins } {
    set buf [insIfRead /$ins "SCLD" 48]
    if { [scan $buf " *SCLD%d,%d" v h] != 2 } {
        return -code error "Invalid scale readback: $buf"
    }
    varDoSet /${ins}/setup/calib_v -v $v
    varDoSet /${ins}/setup/calib_h -v $h
}

CAMP_FLOAT /~/setup/hysteresis_v -D -S -T "Hysteresis for v" \
        -H {Hysteresis or backlash when moving v axis} \
        -d on -s on -v 0 -units mm \
        -writeProc {CM6k_hysteresis_w v}

CAMP_FLOAT /~/setup/hysteresis_h -D -S -T "Hysteresis for h" \
        -H {Hysteresis or backlash when moving h axis} \
        -d on -s on -v 0 -units mm \
        -writeProc {CM6k_hysteresis_w h}

  proc CM6k_hysteresis_w { ax ins val } {
    varDoSet /${ins}/setup/hysteresis_${ax} -v $val
  }


# Motors have labelled 1.8deg/step, so DRES = 200
# Ah ha! Dave says they can work at 25000!
CAMP_INT /~/setup/drive_res -D -S -R -T "Drive resolution" \
    -H "Drive resolution for stepper motors (same for both)" \
    -d on -s on -r on -v 25000 -units "steps/rev" \
    -readProc CM6k_driveres_r -writeProc CM6k_driveres_w

  proc CM6k_driveres_r { ins } {
    insIfReadVerify /$ins "DRES" 48 /${ins}/setup/drive_res "*DRES%d," 1
  }

  proc CM6k_driveres_w { ins val } {
    CM6K_no_moving $ins "Setting drive resolution"
    insIfWriteVerify /$ins "@DRES$val" "DRES" 48 /${ins}/setup/drive_res " *DRES%d," 1 $val
  }


CAMP_STRUCT /~/absolute -D -T "Absolute setup" -d on \
    -H "Configuration for readback from external absolute position sensor"

CAMP_STRING /~/absolute/abs_sensor_dev -D -S -R -T "Abs pos device" \
    -H "The Camp instrument name for the GageMux position sensor readback device" \
    -d on -r on -s on \
    -readProc CM6k_sens_dev_r -writeProc CM6k_sens_dev_w

# Set the sensor device.  If it is explicitly set blank, then disable and hide the absolute
# position variables.  If the device name is wrong, then merely disable them.
  proc CM6k_sens_dev_w { ins target } {
    set dev [string trim $target "/ "]
    if { [string length $dev] == 0 } {
        varDoSet /${ins}/abs_pos_v -r off -p off -d off
        varDoSet /${ins}/abs_pos_h -r off -p off -d off
        varDoSet /${ins}/absolute/abs_sensor_dev -v $dev
    } else {
        set dev [lindex [split $dev "/"] 0]
        varDoSet /${ins}/absolute/abs_sensor_dev -v $dev
        if { [catch {insGetTypeIdent /$dev} type] ||
             [string compare $type "asd_gagemux"] } {
            varDoSet /${ins}/abs_pos_v -r off -p off
            varDoSet /${ins}/abs_pos_h -r off -p off
            return -code error "position device must be a gagemux sensor readback unit"
        }
        if { [catch {
            varRead /${dev}/read_x
            varRead /${dev}/read_y
        } ] } {
            varDoSet /${ins}/abs_pos_v -r off -p off
            varDoSet /${ins}/abs_pos_h -r off -p off
            return -code error "Absolute readback failed: check sensors are plugged in"
        }
        varDoSet /${ins}/abs_pos_v -r on -d on
        varDoSet /${ins}/abs_pos_h -r on -d on
    }
  }
  proc CM6k_sens_dev_r { ins } {
    foreach dev [sysGetInsNames] {
        if { [string compare [insGetTypeIdent /$dev] "asd_gagemux"] == 0 } {
            varSet /${ins}/absolute/abs_sensor_dev -v $dev
            return
        }
    }
    return -code error "No asd_gagemux instrument is present"
  }

CAMP_SELECT /~/absolute/vertical_chan -S -D -T "V channel" \
    -H "Select which Gagemux channel is vertical" \
    -selections x y \
    -s on -d on -v 0 \
    -writeProc CM6k_abs_vchan_w

  proc CM6k_abs_vchan_w { ins val } {
    varDoSet /${ins}/absolute/vertical_chan -v $val
  }


CAMP_SELECT /~/absolute/polarity_v -S -D -T "V polarity" \
    -H "Polarity of vertical sensor readback" \
    -selections normal reverse \
    -s on -d on -v 1 \
    -writeProc {CM6k_abs_pol_w v}

CAMP_SELECT /~/absolute/polarity_h -S -D -T "H polarity" \
    -H "Polarity of horizontal sensor readback" \
    -selections normal reverse \
    -s on -d on -v 0 \
    -writeProc {CM6k_abs_pol_w h}

  proc CM6k_abs_pol_w { ax ins val } {
    varDoSet /${ins}/absolute/polarity_${ax} -v $val
  }

CAMP_FLOAT /~/absolute/offset_v -S -D -T "Offset for v" \
    -H "Offset for external readback of v-position, defining origin" \
    -s on -d on -v 0.0 -units "mm" \
    -writeProc {CM6k_abs_offset_w v}

CAMP_FLOAT /~/absolute/offset_h -S -D -T "Offset for h" \
    -H "Offset for external readback of h-position, defining origin" \
    -s on -d on -v 0.0 -units "mm" \
    -writeProc {CM6k_abs_offset_w h}

  proc CM6k_abs_offset_w { ax ins target } {
    varDoSet /${ins}/absolute/offset_$ax -v $target
  }


#  Procedure to check if external encoder values agree with motor position.
#  Returns boolean.

  proc CM6k_abs_in_tol { ins } {
    set t [varGetVal /${ins}/absolute/tolerance]
    set a [varGetVal /${ins}/abs_pos_v]
    set m [varGetVal /${ins}/motor_pos_v]
    if { abs( $a - $m ) > $t } { return 0 }
    set a [varGetVal /${ins}/abs_pos_h]
    set m [varGetVal /${ins}/motor_pos_h]
    if { abs( $a - $m ) > $t } { return 0 }
    return 1
  }

CAMP_FLOAT /~/absolute/tolerance -S -D -T "Abs tolerance" \
    -H "Tolerance for disagreement between motor and external encoders." \
    -d on -s on -v 0.8 -units mm -writeProc CM6k_abs_toler
  proc CM6k_abs_toler { ins target } {
    if { $target < 0.05 } {
        return -code error "Tolerance too small: Must be > 0.05 mm."
    }
    varDoSet /$ins/absolute/tolerance -v $target
  }


CAMP_STRUCT /~/limits -D -T "Limits" -d on


CAMP_SELECT /~/limits/h_hard_lim -D -S -R -T "H hard limits" \
    -H "Enabling of horizontal hard-limit switches." \
    -d on -s on -r on -selections None {Low only} {High only} {Low & High} \
    -readProc CM6k_hhardlim_r -writeProc CM6k_hhardlim_w

  proc CM6k_hhardlim_r { ins } {
    insIfReadVerify /$ins "2LH" 48 /${ins}/limits/h_hard_lim " *2LH%d" 1
  }

  proc CM6k_hhardlim_w { ins val } {
    insIfWriteVerify /$ins "LH,$val" "2LH" 48 /${ins}/limits/h_hard_lim " *2LH%d" 1 $val
  }

CAMP_SELECT /~/limits/v_hard_lim -D -S -R -T "V hard limits" \
    -H "Enabling of vertical hard-limit switches." \
    -d on -s on -r on -selections None {Low only} {High only} {Low & High} \
    -readProc CM6k_vhardlim_r -writeProc CM6k_vhardlim_w

  proc CM6k_vhardlim_r { ins } {
    insIfReadVerify /$ins "1LH" 48 /${ins}/limits/v_hard_lim " *1LH%d" 1
  }

  proc CM6k_vhardlim_w { ins val } {
    insIfWriteVerify /$ins "LH$val," "1LH" 48 /${ins}/limits/v_hard_lim " *1LH%d" 1 $val
  }


CAMP_FLOAT /~/limits/h_low -D -S -R -T "H low limit" \
        -d on -s on -r on -units {mm} -v -10 \
        -readProc CM6k_vhlow_r -writeProc CM6k_hlow_w

CAMP_FLOAT /~/limits/h_high -D -S -R -T "H high limit" \
        -d on -s on -r on -units {mm} -v 10 \
        -readProc CM6k_vhhigh_r -writeProc CM6k_hhigh_w

CAMP_FLOAT /~/limits/v_low -D -S -R -T "V low limit" \
        -d on -s on -r on -units {mm} -v -10 \
        -readProc CM6k_vhlow_r -writeProc CM6k_vlow_w

CAMP_FLOAT /~/limits/v_high -D -S -R -T "V high limit" \
        -d on -s on -r on -units {mm} -v 10 \
        -readProc CM6k_vhhigh_r -writeProc CM6k_vhigh_w

  proc CM6k_vhlow_r { ins } {
    set buf [insIfRead /$ins "LSNEG" 48]
    if { [scan $buf " *LSNEG%f,%f" v1 v2] < 2 } {
      return -code error "Invalid readback: $buf"
    }
    varDoSet /${ins}/limits/v_low -v $v1
    varDoSet /${ins}/limits/h_low -v $v2
  }
  proc CM6k_vlow_w { ins v } {
    insIfWriteVerify /$ins "LSNEG$v," "1LSNEG" 48 /${ins}/limits/v_low " *1LSNEG%f," 1 $v 0.05
  }
  proc CM6k_hlow_w { ins v } {
    insIfWriteVerify /$ins "LSNEG,$v" "2LSNEG" 48 /${ins}/limits/h_low " *2LSNEG%f," 1 $v 0.05
  }

  proc CM6k_vhhigh_r { ins } {
    set buf [insIfRead /$ins "LSPOS" 48]
    if { [scan $buf " *LSPOS%f,%f" v1 v2] < 2 } {
      return -code error "Invalid readback: $buf"
    }
    varDoSet /${ins}/limits/v_high -v $v1
    varDoSet /${ins}/limits/h_high -v $v2
  }
  proc CM6k_vhigh_w { ins v } {
    insIfWriteVerify /$ins "LSPOS$v," "1LSPOS" 48 /${ins}/limits/v_high " *1LSPOS%f," 1 $v 0.05
  }
  proc CM6k_hhigh_w { ins v } {
    insIfWriteVerify /$ins "LSPOS,$v" "2LSPOS" 48 /${ins}/limits/h_high " *2LSPOS%f," 1 $v 0.05
  }


# A procedure to read multi-line responses by temporarily setting the read
# terminator to "none".  Such reads MUST specify a buffer length less than 
# or equal to the actual instrument response (response may be truncated).

proc CM6k_multiline_read { ins cmd buflen } {
    # First, set interface to have no read terminator
    set iface [concat \[ insSet /$ins -if rs232 [insGetIfDelay /$ins] [insGetIfTimeout /$ins] [insGetIf /$ins] \]]
    # set readTerm to none
    set newface [lreplace $iface 12 12 none]
    # set timeout to 0
    set newface [lreplace $newface 6 6 0]
    insIfOff /$ins
    expr $newface
    insIfOn /$ins
    # perform multi-line read, into buffer
    set err [catch {insIfRead /$ins $cmd $buflen} buf]
    # restore interface to normal (terminated) settings
    insIfOff /$ins
    expr $iface
    insIfOn /$ins
    # finally, return buffer or error
    if { $err } {
        return -code error $errorinfo
    } else {
        return $buf
    }
}

# A procedure to check for errors.  Unfortunately, there seems no way to 
# eliminate the CRLF that comes before the error message, and no way to
# retrieve the previous error message separately.  But this doesn't work
# unless the buff length truncates the message
#
#proc CM6k_check_error { ins } {
#    set cmd [string trim [insIfRead /$ins TCMDER 60] "* "]
#    if { [string compare $cmd "NO ERRORS"] } {
#        # There was an error, so repeat the erroneous command
#        catch { CM6k_multiline_read $ins $cmd 300 } err
#        set err [join [split [string trim $err] "\r\n"] "; "]
#        return -code error $err
#    }
#}

proc CM6k_check_error { ins } {
    set cmd [string trim [insIfRead /$ins TCMDER 60] "* "]
    if { [string compare $cmd "NO ERRORS"] } {
        # There was an error
        scan $cmd {%[A-Z]} cmdword
        set buf [insIfRead /$ins TER 80]
        scan $buf {*TER%1d%1d%1d%1d_%*d_%1d%1d%*d_} stall hl sl df pcu peg 
        set err "Error executing \"$cmd\"...  "
        if { $stall } { append err "Stall detected. " }
        if { $hl } { append err "Hard limit. " }
        if { $sl } { append err "Soft limit. " }
        if { $df } { append err "Drive fault. " }
        if { $pcu } { append err "Pulse cutoff activated. " }
        if { $peg } { append err "Pre-emptive GO impossible. " }
        return -code error $err
    }
}

proc CM6K_no_moving { ins what } {
    set buf [insIfRead /$ins "!TAS.1" 12]
#   bit 1 = "moving"
    if { [scan $buf " *%d" moving] != 1 } {
        return -code error "Failed to test movement status"
    }
    if { $moving } {
        return -code error "$what is not possible when drive is moving"
    }
}

CAMP_STRUCT /~/internal -D -T "Internal controls" -d on 

CAMP_INT /~/internal/initial -D -R -P -T "Initialization" \
    -d on -r on -p_int 2.0 -v 0 \
    -readProc CM6k_initial_r

# Do some initialization in a poll, so it doesn't take too long to come online
proc CM6k_initial_r { ins } {
    # Set polling on with a long poll interval, in case we error-out
    varDoSet /${ins}/internal/initial -p off -v 0
    varDoSet /${ins}/internal/initial -p on -p_int 10
    # Apply setup.
    varSet /${ins}/setup/drive_res -v [varGetVal /${ins}/setup/drive_res]
    varSet /${ins}/setup/calib_v -v [varGetVal /${ins}/setup/calib_v]
    varSet /${ins}/setup/calib_h -v [varGetVal /${ins}/setup/calib_h]
    varSet /${ins}/setup/velocity -v [varGetVal /${ins}/setup/velocity]
    varSet /${ins}/setup/acceleration -v [varGetVal /${ins}/setup/acceleration]
    #
    if { [string length [varGetVal /${ins}/absolute/abs_sensor_dev]] == 0 } {
        catch { varRead /${ins}/absolute/abs_sensor_dev }
    }
    if { [string length [varGetVal /${ins}/absolute/abs_sensor_dev]] > 0 } {
        if { [ catch {# Define position from gagemux 
            varSet /${ins}/setup/define_pos -v 3
        } msg ] } {
            varDoSet /${ins}/internal/initial -m "define_pos failed with message $msg"
            return -code error "define_pos failed with message $msg"
        }
    }
    # Read movement status
    varRead /${ins}/movement
    if { [varGetVal /${ins}/movement] != 1 } {
        varRead /${ins}/motor_pos_v ;# also reads motor_pos_h
        varDoSet /${ins}/destin_v -v [varGetVal /${ins}/motor_pos_v]
        varDoSet /${ins}/destin_h -v [varGetVal /${ins}/motor_pos_h]
    }
    # Check limits.  If they are set, use them; if unset, apply mine.
    set v_low [varGetVal /${ins}/limits/v_low]
    set v_high [varGetVal /${ins}/limits/v_high]
    set h_low [varGetVal /${ins}/limits/h_low]
    set h_high [varGetVal /${ins}/limits/h_high]
    # reads both h and v
    varRead /${ins}/limits/v_low
    varRead /${ins}/limits/v_high
    if { [varGetVal /${ins}/limits/v_low] >= [varGetVal /${ins}/limits/v_high] || \
             [varGetVal /${ins}/limits/h_low] >= [varGetVal /${ins}/limits/h_high] } {
        varSet /${ins}/limits/v_low  -v $v_low
        varSet /${ins}/limits/v_high -v $v_high
        varSet /${ins}/limits/h_low  -v $h_low
        varSet /${ins}/limits/h_high -v $h_high
    }
    # Success, so turn off polling
    varDoSet /${ins}/internal/initial -p off -v 1
    varDoSet /${ins}/movement -p on -p_int 6
}

CAMP_INT /~/internal/merr -D -T "Watcher" \
    -d on -v 0 

