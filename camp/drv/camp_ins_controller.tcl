# camp_ins_controller.tcl  --  Generic Active Control
# A processor for active PID control of other Camp devices.
#
# Donald Arseneau                last modified    25-Sep-2015
# (contains debugging messages still)

CAMP_INSTRUMENT /~ -D -T "Generic Active Control" \
    -H "Processor for active PID control of other Camp devices." \
    -d on \
    -initProc cont_init -deleteProc cont_delete \
    -onlineProc cont_online -offlineProc cont_offline

  proc cont_init { ins } {
	global ${ins}_CON
        insSet /${ins} -if none 0.0 0.0
	set ${ins}_CON(type) unknown
	set ${ins}_CON(lstage) 0
        set ${ins}_CON(invf) {$x}
        set ${ins}_CON(outf) {$x}
        insSet /${ins} -line on
  }
  proc cont_delete { ins } {
	global ${ins}_CON
        insSet /${ins} -line off
	destroy ${ins}_CON
  }
  proc cont_online { ins } {
	insIfOn /${ins}
        varSet /${ins}/internal/init -p on -p_int 2
  }
  proc cont_offline { ins } {
        insIfOff /${ins}
  }

  CAMP_FLOAT /~/setpoint -D -S -T "Setpoint" \
	-d on -s on -units " " -writeProc cont_setpoint_w
    proc cont_setpoint_w { ins target } {
	global ${ins}_CON
	varDoSet /${ins}/setpoint -v $target
        # DO NOT re-initialize the integration of the I term
#	set ${ins}_CON(sum_I) {($oldout - $offby * ($P+$I))}
    }

  CAMP_FLOAT /~/readback -D -R -L -P -A -T "Readback" \
	-H "Readback" \
	-d on -r on -units " " -readProc cont_readback_r
    proc cont_readback_r { ins } {
        set d [varGetVal /${ins}/setup_read/device]
        set nta [varGetVal /${ins}/setup_read/num_ave]
	if { $nta < 1 } { set $nta 1 }
	set sum 0.0
	set vmax -9.9e20
	set vmin 9.9e20
	for { set i 0 } { $i < $nta } { incr i } {
	    varRead $d
	    set v [ expr [varGetVal $d] - 0.0 ]
	    if { $v < $vmin } { set vmin $v }
	    if { $v > $vmax } { set vmax $v }
	    set sum [ expr $sum + $v ]
	}
	set v [expr $sum/$nta]
        if { $vmax - $vmin > [varGetVal /${ins}/setup_read/stable_tol] } {
            varDoSet /${ins}/status -v 4 ;# "unstable"
        } elseif { ( $v > [varGetVal /${ins}/setup_read/max] ) || \
                   ( $v < [varGetVal /${ins}/setup_read/min] ) } {
            varDoSet /${ins}/status -v 3 ;# "Out of range"
        } else {
            varDoSet /${ins}/status -v [varGetVal /${ins}/function]
        }
        # Do I want to perform any action when unstable or out of range?
	varDoSet /${ins}/readback -v $v
	varTestAlert /${ins}/readback [varGetVal /${ins}/setpoint]
    }

  CAMP_FLOAT /~/output -D -S -R -L -T "Driving output" \
	-H "Setting of output device" \
	-d on -s on -r on -units " " -readProc cont_output_r -writeProc cont_output_w
    proc cont_output_r { ins } {
        global ${ins}_CON
	set d [varGetVal /${ins}/setup_out/device]
        if { [catch { varGetVal $d } x] == 0 } {
            varDoSet /${ins}/output -v [expr 0.0+[set ${ins}_CON(invf)]]
	}
    }
    proc cont_output_w { ins x } {
	global ${ins}_CON
	set d [varGetVal /${ins}/setup_out/device]
        set v [expr 0.0+[set ${ins}_CON(outf)]]
        if { [set ${ins}_CON(type)] == "integer" } {
	    set v [expr round($v)]
	}
	varSet $d -v $v
        varDoSet /${ins}/output -v $x
    }

  CAMP_SELECT /~/function -D -S -T "Control Function" \
        -d on -s on \
	-H "Turn active control on or off" \
        -selections Stop Monitor Control Learn \
        -writeProc cont_function_w

    proc cont_function_w { ins target } {
	global ${ins}_CON
        # Start by turning off everything; then turn on what's desired
        cont_nothing $ins
        if { $target == 0 } { return }
	# Check validity of parameters before enabling monitor or control
        varSet /${ins}/setup_read/interval -v [ varGetVal /${ins}/setup_read/interval ]
        varSet /${ins}/setup_read/device -v [ varGetVal /${ins}/setup_read/device ]
        if { [varGetVal /${ins}/setup_read/min] >=  [ varGetVal /${ins}/setup_read/max ] } {
            return -code error "Readback min/max limits are invalid"
        }
        varSet /${ins}/setup_read/num_ave -v [ varGetVal /${ins}/setup_read/num_ave ]
        varSet /${ins}/setup_read/stable_tol -v [ varGetVal /${ins}/setup_read/stable_tol ]
        varDoSet /${ins}/readback -m "Readback from device [ varGetVal /${ins}/setup_read/device ]"
        if { $target == 1 } {# Monitor
            varDoSet /${ins}/readback -p on -p_int [ varGetVal /${ins}/setup_read/interval ] 
            varRead /${ins}/readback
            varDoSet /${ins}/status -v 1
            varDoSet /${ins}/function -v $target
            return
        }
        # check Output/Control parameters now, if necessary
        varSet /${ins}/setup_out/device -v [ varGetVal /${ins}/setup_out/device ]
        set v X
        set status [ catch {varGetVal /${ins}/setup_out/device} od ]
        if { $status == 0 } {
            catch { varRead $od }
            set status [ catch { varGetVal $od } v]
        }
        if { $status == 0 } {
            set status [ catch {expr 1.5 + $v} ]
        }
        if { $status == 0 } {
            if { [string first "." $v] < 0 } {
                set ${ins}_CON(type) integer
            } else {
                set ${ins}_CON(type) float
            }
        }
        if { $status == 0 } {
            set status [ catch {varSet $od -v $v}]
        }
        if { $status } {
            set ${ins}_CON(type) bad
            return -code error [ cont_badvar $od output ]
        }
        set x [ varGetVal /${ins}/setup_out/scale_func ]
        if { [ string length "$x" ] == 0 } { set x {$x} }
        varSet /${ins}/setup_out/scale_func -v $x
        set x "output"
        expr "\[set y [set ${ins}_CON(outf)]\]"
        if { [string compare $y $x] == 0 } {
            varDoSet /${ins}/output -m "Setpoint for device $od"
        } else {
            varDoSet /${ins}/output -m "Device $od is set to $y"
        }
        set x [ varGetVal /${ins}/setup_out/inverse_func ]
        if { [ string length "$x" ] == 0 } { set x {$x} }
        varSet /${ins}/setup_out/inverse_func -v $x
	set mn [varGetVal /${ins}/setup_out/min] 
	set mx [varGetVal /${ins}/setup_out/max]
        if { $mn >= $mx } {
            return -code error "Output min/max limits are invalid"
        }
        set st 0
        set status [ catch {varGetVal /${ins}/setup_out/max_change} st ]
        if { $st <= 0.0 } {
            varDoSet /${ins}/setup_out/max_change -v [ expr ( $mx - $mn ) / 5 + .1 ]
        }
        # Initialize output value
	varRead /${ins}/output
        # We initialize the "previous readback" to be a reference to the *new* read,
        # and the difference will be zero.  See use in the roller procedure.
        set ${ins}_CON(prev_rb) {$rb}
        # also initialize the integration of the I term for a smooth start.
	set ${ins}_CON(sum_I) {($oldout - $offby * ($P+$I))}
	#
        varDoSet /${ins}/readback -p off
        if { $target == 3 } { # learn
            if { [set ${ins}_CON(lstage)] == 1 } {
	        varDoSet /${ins}/function -v $target
                return
            } else {
                return -code error "Please use /${ins}/setup/out/learn to start learning"
            }
        }
	varDoSet /${ins}/function -v $target
        varRead  /${ins}/internal/roller
        varDoSet /${ins}/internal/roller -p on -p_int [ varGetVal /${ins}/setup_out/interval ]
    }


# This list of status must correspond to the list of functions, where they
# overlap.  This allows alarms. Alarm testing is done in the roller process.
  CAMP_SELECT /~/status -D -A -T "Status" -d on \
        -selections idle reading controlling out-of-range unstable control-limit learning

CAMP_STRUCT /~/setup_read -D -T "Setup readbacks" -d on \
        -H "Set readback device and parameters"

  CAMP_STRING /~/setup_read/device -D -S -T "Readback device" \
        -H "Identify Camp Variable to use for readback" \
        -d on -s on -writeProc cont_sr_device_w
    proc cont_sr_device_w { ins target } {
        if { [ string first "/" $target ] != 0 } {
	    if { [string length $target ] > 0 } { set target "/${target}" }
	}
        set st [ catch { varRead $target } ]
#	varDoSet /${ins}/setup_read/device -m "read it $st: varRead $target "
        if { $st == 0 } {
            set st [ catch {expr [varGetVal $target] + 0.0 } ]
#	    varDoSet /${ins}/setup_read/device -m "got it $st: expr [varGetVal $target] + 0.0  "
        }
        if { $st != 0 } {
            cont_nothing $ins
            return -code error "[ cont_badvar $target readback ]"
        }
        varDoSet /${ins}/setup_read/device -v $target
        cont_set_units $ins
    }

    proc cont_badvar { target how } {
	return "${how} device \"${target}\" does not exist or is inappropriate for ${how}."
    }
    proc cont_set_units { ins } {
	if { [ catch { varNumGetUnits [ varGetVal /${ins}/setup_read/device ] } ru ] } {
	    set ru ?
	}
	if { [ catch { varNumGetUnits [ varGetVal /${ins}/setup_out/device ] } ou ] } {
	    set ou ?
	}
	varDoSet /${ins}/setpoint -units $ru
	varDoSet /${ins}/readback -units $ru
	varDoSet /${ins}/output -units $ou
    }

  CAMP_FLOAT /~/setup_read/min -D -S -T "Minimum readback" \
        -H "Minimum acceptable readback value" \
        -d on -s on -writeProc cont_sr_min_w
    proc cont_sr_min_w { ins target } {
        varDoSet /${ins}/setup_read/min -v $target
    }

  CAMP_FLOAT /~/setup_read/max -D -S -T "Maximum readback" \
        -H "Maximum acceptable readback value" \
        -d on -s on -writeProc cont_sr_max_w
    proc cont_sr_max_w { ins target } {
        varDoSet /${ins}/setup_read/max -v $target
    }

  CAMP_INT /~/setup_read/interval -D -S -T "Poll interval" \
        -H "Number of seconds between each readback/control iteration" \
        -d on -s on -units s -v 20 -writeProc cont_sr_interval_w
    proc cont_sr_interval_w { ins target } {
        if {$target < 1} {set target 5}
	set v [varGetVal /${ins}/setup_read/interval]
        varDoSet /${ins}/setup_read/interval -v $target
        varDoSet /${ins}/setup_out/interval -v $target
        varDoSet /${ins}/internal/roller -p_int $target
        varDoSet /${ins}/readback -p_int $target
	if { $v < .1 } { return }
        set P [varGetVal /${ins}/setup_out/Kp]
        set I [varGetVal /${ins}/setup_out/Ki]
        set D [varGetVal /${ins}/setup_out/Kd]
        # if P,I,D are finite fixed values, modify them to match interval change
        if { [ string first {$} "$P $I $D" ] < 0 } { return }
        if { abs($P+$I+$D) < 1.0e-8 } { return }
        varDoSet /${ins}/setup_out/Ki -v [expr $I * ( ($target/$v)*($P+$D) + $I )/($P+$I+$D) ]
        varDoSet /${ins}/setup_out/Kd -v [expr $D*$v/$target]
    }

  CAMP_INT /~/setup_read/num_ave -D -S -T "Number to average" \
        -H "Number of instrument readings to average for each readback iteration" \
        -d on -s on -v 1 -writeProc cont_sr_num_w
    proc cont_sr_num_w { ins target } {
        if {$target < 1} {set target 2}
        varDoSet /${ins}/setup_read/num_ave -v $target
    }

  CAMP_FLOAT /~/setup_read/stable_tol -D -S -T "Req. readback stability" \
        -H "The maximum range of readings in each iteration for the readback to be declared \"stable\" (when num_ave > 1)" \
        -d on -s on -v 999999. -writeProc cont_sr_stable_tol_w
    proc cont_sr_stable_tol_w { ins target } {
        if {$target <= 0} {set target 1}
        varDoSet /${ins}/setup_read/stable_tol -v $target
    }

  CAMP_FLOAT /~/setup_read/unstable_fuzz -D -S -T "Reading uncertainty" \
        -H "The typical meaningless variability in readings or the readback uncertainty" \
        -d on -s on -v 0.0 -writeProc cont_sr_fuzz_w
    proc cont_sr_fuzz_w { ins target } {
        varDoSet /${ins}/setup_read/unstable_fuzz -v [expr {abs($target)}]
    }

CAMP_STRUCT /~/setup_out -D -T "Setup output" -d on \
        -H "Set up control of output device"

  CAMP_STRING /~/setup_out/device -D -S -T "Output device" \
        -H "Identify Camp Variable to set for output" \
        -d on -s on -writeProc cont_so_device_w
    proc cont_so_device_w { ins target } {
        if { [ string first "/" $target ] != 0 } {
	    if { [string length $target ] > 0 } { set target "/${target}" }
	}
        set st [ catch {expr [varGetVal $target] + 0.0 } v ]
        if { $st == 0 } {
            set st [ catch { varSet $target -v $v } ]
        }
        if { $st != 0 } {
            cont_nothing $ins
            return -code error "[ cont_badvar $target outut ]"
        }
        varDoSet /${ins}/setup_out/device -v $target
        cont_set_units $ins
    }

  CAMP_FLOAT /~/setup_out/min -D -S -T "Minimum output" \
        -H "Minimum allowed output setting" \
        -d on -s on -writeProc cont_so_min_w
    proc cont_so_min_w { ins target } {
        varDoSet /${ins}/setup_out/min -v $target
    }

  CAMP_FLOAT /~/setup_out/max -D -S -T "Maximum output" \
        -H "Maximum allowed output setting" \
        -d on -s on -writeProc cont_so_max_w
    proc cont_so_max_w { ins target } {
        varDoSet /${ins}/setup_out/max -v $target
    }

  CAMP_FLOAT /~/setup_out/max_change -D -S -T "Max change of output" \
        -H "The maximum allowed change in output at each control iteration" \
        -d on -s on -v 9999.9999 -writeProc cont_so_max_change_w
    proc cont_so_max_change_w { ins target } {
        if {$target <= 0} {set target 9999.9}
        varDoSet /${ins}/setup_out/max_change -v $target
    }

# This is a copy of /~/setup_read/interval !
  CAMP_INT /~/setup_out/interval -D -S -T "Control interval" \
        -H "Number of seconds between each readback/control iteration" \
        -d on -s on -units s -v 20 -writeProc cont_sr_interval_w

  CAMP_FLOAT /~/setup_out/max_deviation -D -S -T "Max deviation" \
        -H "Limit the deviation (reading-setpoint) used for PID control (set 0.0 for no limit)" \
        -d on -s on -v 0. -writeProc cont_so_max_dev_w
    proc cont_so_max_dev_w { ins target } {
        varDoSet /${ins}/setup_out/max_deviation -v [expr {abs($target)}]
    }

  CAMP_STRING /~/setup_out/Kp -D -S -T "Proportional Gain" \
        -H "Proportional Gain: Coefficient for P term (0 is off) (may be function of \$sp or \$rb)" \
        -d on -s on -v 0.0 -writeProc cont_sc_Kp_w
    proc cont_sc_Kp_w { ins target } {
        cont_so_setPID $ins $target Kp
    }

  CAMP_STRING /~/setup_out/Ki -D -S -T "Integral coeff" \
        -H "Coefficient for I term (0 is off) (may be function of \$sp or \$rb)" \
        -d on -s on -v 0.0 -writeProc cont_sc_Ki_w
    proc cont_sc_Ki_w { ins target } {
        cont_so_setPID $ins $target Ki
    }

  CAMP_STRING /~/setup_out/Kd -D -S -T "Differential coeff" \
        -H "Coefficient for D term (0 is off) (may be function of readback \$sp or \$rb)" \
        -d on -s on -v 0.0 -writeProc cont_sc_Kd_w
    proc cont_sc_Kd_w { ins target } {
        cont_so_setPID $ins $target Kd
    }
    proc cont_so_setPID { ins val par } {
        set rb 42.0
	set sp 43.0
        if { [ catch {expr $val + 1.0} ] } {
            return -code error "value ${val} is not a legal number or function of \$sp or \$rb"
        } else {
            varDoSet /${ins}/setup_out/${par} -v $val
        }
    }

  CAMP_STRING /~/setup_out/scale_func -D -S -T "Output scaling" \
        -H "Function of \$x to rescale the control output, e.g. \"sqrt(\$x)\". May contain </camp/var> references." \
        -d on -s on -writeProc cont_so_scale_w
#       Need braces around value to restore config properly!
    proc cont_so_scale_w { ins target } {
        global ${ins}_CON
        set f $target
	regsub -all {<(/[A-Za-z0-9_/-]+)>} $target {[varGetVal \1]} f
        if { [scan "$f;1" { ;%d} x] == 1 } {set f {$x}}
        set x 1.23
        set s1 [catch {expr 1.0+$f}]
        set x bad
        set s2 [catch {expr 1.0+$f}]
        if { $s1==1 || $s2==0 } {
            return -code error "scale_func not a valid function of \$x"
        }
        varDoSet /${ins}/setup_out/scale_func -v $target
        set ${ins}_CON(outf) $f
    }

  CAMP_STRING /~/setup_out/inverse_func -D -S -T "Inverse of scale_func" \
        -H "Enter the inverse of the scale_func function, e.g. \"\$x*\$x\"" \
        -d on -s on -writeProc cont_so_inverse_w
    proc cont_so_inverse_w { ins target } {
        global ${ins}_CON
	regsub -all {<(/[A-Za-z0-9_/-]+)>} $target {[varGetVal \1]} i
        if { [scan "$i;1" { ;%d} x] == 1 } {set i {$x}}
        set f [set ${ins}_CON(outf)]
        set x 1.23
        set s [catch {expr 0.0+$f} x]
        if { $s==0 } { set s [catch {expr 0.0+$i} x] }
        if { $s==1 || abs($x-1.23)>.001 } {
            return -code error "inverse_func $i not the inverse function of $f"
        }
        varDoSet /${ins}/setup_out/inverse_func -v $target
        set ${ins}_CON(invf) $i
    }

  CAMP_SELECT /~/setup_out/learn -D -S -T "Measure parameters" \
        -H "Automatically determine (tune) control parameters." \
        -d on -s on -v 0 \
        -selections { } {I} {I & interval} {P} {P I} {P I D} Fail \
        -writeProc cont_so_learn_w
    proc cont_so_learn_w { ins target } {
	global ${ins}_CON
        if { $target == 6 } { set target 0 }
        if { $target == 0 } {# turn off learning; do control
            varDoSet /${ins}/internal/learner -p off -v 0
            if { [ varGetVal /${ins}/setup_out/learn ] > 0 } {
                catch { varSet /${ins}/function -v 2 }
            }
            varDoSet /${ins}/setup_out/learn -v 0 -m {}
	    return
	}
        # Make sure control is possible, but be sure not to change output
	set ${ins}_CON(lstage) 1
        set stat [ catch { varSet /${ins}/function -v 3 } ]
	set ${ins}_CON(lstage) 0
	if { $stat } { return -code error "learning is not possible when control is not" }
        # set parameters controlling learning
	set ${ins}_CON(values) ""
	set ${ins}_CON(maxdif) 0.0
	set ${ins}_CON(learnoff) 0.0
	set ${ins}_CON(inislope) 0.0
        set ns [ varGetVal /${ins}/setup_out/lconfig/num_tail ] ;# 4.0
        if { $target == 2 } {# I & interval -- variable interval
            set most 8
            set ${ins}_CON(maxinterval) 180
            set inter 2
        } else {
            set inter [ varGetVal /${ins}/setup_out/interval ]
            set ${ins}_CON(maxinterval) $inter
            set most [expr 16 * ( 1 + ($inter<40) + ($inter<13) + ($inter<5) ) ]
        }
        set ${ins}_CON(numstable) $ns
        set ${ins}_CON(mostvals) $most
	set ${ins}_CON(postmax) 0.0
        # now get the disturbance size, with many heuristics:
        # use specified learning step, else max_change, else whole range/10 in direction of more room
        set s [varGetVal /${ins}/setup_out/lconfig/lstep]
        if { $s == 0.0 } {
            set s [varGetVal /${ins}/setup_out/max_change]
            set x [varGetVal /${ins}/setup_out/max]
            set n [varGetVal /${ins}/setup_out/min]
            if { abs($s) > 0.4*($x - $n) } { set s 0.0 }
            if { $s == 0.0 } {
                set o [varGetVal /${ins}/output]
                set s [expr ( 2.0*$o > $x + $n ? -0.3 : 0.3 ) * ($x - $n) ]
            }
        }
        set ${ins}_CON(lstep) [ expr $s ]
        #
        # start the learning process, at stage 1, do first iter immediately, then every $inter
        set ${ins}_CON(lstage) 2
        varDoSet /${ins}/setup_out/learn -v $target -m {}
        cont_c_learner_r $ins
        varDoSet /${ins}/internal/learner -p on -p_int $inter
    }

CAMP_STRUCT /~/setup_out/lconfig -D -T "learning config" -d on \
        -H "Configure details of learning process"

  CAMP_FLOAT /~/setup_out/lconfig/lstep -D -S -T "Output step" \
        -d on -s on -v 0.0 \
        -H "The output will be changed by this (if non-zero) for learning" \
        -writeProc cont_l_lstep_w
    proc cont_l_lstep_w { ins target } {
        varDoSet /${ins}/setup_out/lconfig/lstep -v $target
    }

  CAMP_FLOAT /~/setup_out/lconfig/xgain -D -S -T "Gain multiplier" \
        -d on -s on -v 1.0 \
        -H "Values > 1 give faster response; < 1 slower" \
        -writeProc cont_l_xgain_w
    proc cont_l_xgain_w { ins target } {
        varDoSet /${ins}/setup_out/lconfig/xgain -v [bounded $target 0.1 25.0]
    }

   CAMP_INT /~/setup_out/lconfig/num_tail -D -S -T "Number in tail" \
        -d on -s on -v 4 \
        -H "The number of iterations to measure asmptote at end" \
        -writeProc cont_l_num_tail_w
    proc cont_l_num_tail_w { ins target } {
        varDoSet /${ins}/setup_out/lconfig/num_tail -v [bounded $target 2 20]
    }

CAMP_STRUCT /~/internal -D -T "internal" -d on \
        -H "Internal control variables"


  CAMP_SELECT /~/internal/init -D -R -P -T "Initialization" \
          -d on -r off -p on -p_int 2 -readProc cont_c_init_r
#   This initialization variable is only polled once soon after loading --
#   after parameters have been restored after a camp restart.  This allows
#   setting parameters up consistently.
    proc cont_c_init_r { ins } {
        varDoSet /${ins}/internal/init -p off
	varSet /${ins}/function -v [varGetVal /${ins}/function]
    }

  CAMP_INT /~/internal/roller -D -R -P -T "Controller engine" \
	  -d on -r on -p on -readProc cont_roller_r
    proc cont_roller_r { ins } {
        global ${ins}_CON
	if { [ varGetVal /${ins}/function ] != 2 } { # Inexplicable invocation
	    varDoSet /${ins}/internal/roller -p off
	    return
	}
	varRead /${ins}/readback
	set st [varGetVal /${ins}/status] 
	if { $st > 2 } { # bad reading; retry once
	    varRead /${ins}/readback
	    set st [varGetVal /${ins}/status] 
	}
	set rb [varGetVal /${ins}/readback]
	switch $st {
	    3 { # out of range; treat as if at limit
		set rb [bounded $rb [varGetVal /${ins}/setup_read/min] [varGetVal /${ins}/setup_read/max]]
	    }
	    1 { # change "reading" to "controlling"
                varDoSet /${ins}/status -v 2
	    }
	}
	# we must set variables oldout, offby, I, and P for references by
	# startup meta-values; see /~/function and /~/setpoint.
#	set ${ins}_CON(sum_I) {($oldout - $offby * ($P+$I))}
	set amin [varGetVal /${ins}/setup_out/min]
	set amax [varGetVal /${ins}/setup_out/max]
	set oldout [varGetVal /${ins}/output]
	set oldsum [set ${ins}_CON(sum_I)]
	set prb [expr [set ${ins}_CON(prev_rb)]]
	set sp [varGetVal /${ins}/setpoint]
        set fuzz [varGetVal /${ins}/setup_read/unstable_fuzz]
	set fuzz [expr {$fuzz<0.0? 0.0 : $fuzz}]
	set offby [expr ($sp-$rb)]
        set maxd  [varGetVal /${ins}/setup_out/max_deviation]
	if { $maxd > 0.0 } {
	    set offby [bounded $offby -$maxd $maxd]
	}
	set P [expr "([varGetVal /${ins}/setup_out/Kp])"]
	set Dterm [expr "([varGetVal /${ins}/setup_out/Kd]) * ($prb-$rb)" ]
	set I [expr "([varGetVal /${ins}/setup_out/Ki])" ]
	set sum [bounded [expr "$oldsum + $offby * $I" ] $amin $amax]
	set oldsum [bounded [expr $oldsum] $amin $amax]
	set offby [reduced $offby $fuzz]
	set newout 0.0
	set stat [ catch { expr "$offby * $P + $sum + $Dterm" } newout ]
	set ${ins}_CON(prev_rb) $rb
	if { $stat } {
	    set newout $oldout
	    set stat 5
	    set msg [format {Retained %.2f. Error in %.2f * %.2f + %.2f + %.2f ***} $oldout $offby $P $sum $Dterm]
	} else {
	    set msg [format {%.2f + (%.2f%+.2f) + %.2f} [expr $offby*$P] [expr $oldsum] [expr $offby*$I] $Dterm ]
        }
        # Now check for out-of control conditions; apply limits.
        # When limits apply, correct the integration to reflect true settings
        set mch [varGetVal /${ins}/setup_out/max_change]
	set min [expr "$amin < $oldout - $mch ?  $oldout - $mch : $amin"]
	set max [expr "$amax > $oldout + $mch ?  $oldout + $mch : $amax"]
	if { $newout > $max } { # above max
	    append msg [format {; MAX %.2f; SUM %.2f -> %.2f -> %.2f} \
			    $max $oldsum $sum [bounded "$sum-$newout+$max" $oldsum $max]]
	    if { $sum > $oldsum } { set sum [bounded "$sum-$newout+$max" $oldsum $max] }
	    set newout $max
	    set stat 5
	} elseif { $newout < $min } { # below min
	    append msg [format {; MIN %.2f; SUM %.2f -> %.2f -> %.2f} \
			    $min $oldsum $sum [bounded "$sum-$newout+$min" $min $oldsum]]
	    if { $sum < $oldsum } { set sum [bounded "$sum-$newout+$min" $min $oldsum] }
	    set newout $min
	    set stat 5
	} 
	if { [catch { varSet /${ins}/output -v $newout }] || ($stat == 5) } {
	    varDoSet /${ins}/status -v 5
	    varDoSet /${ins}/internal/roller -m $msg -units "($newout)"
        } else {
	    varDoSet /${ins}/internal/roller -m $msg -units $newout
	}
	set ${ins}_CON(sum_I) $sum
	varDoSet /${ins}/internal/Pterm -v [expr $offby*$P]
	varDoSet /${ins}/internal/Iterm -v $sum
	varDoSet /${ins}/internal/Dterm -v $Dterm
	varTestAlert /${ins}/status [ varGetVal /${ins}/function ]
    }

# These three for debugging (can strip-chart the terms)
  CAMP_FLOAT /~/internal/Pterm -D -L -T "P term" -d on
  CAMP_FLOAT /~/internal/Iterm -D -L -T "I term (sum)" -d on
  CAMP_FLOAT /~/internal/Dterm -D -L -T "D term" -d on

  CAMP_INT /~/internal/learner -D -R -P -T "Learning engine" \
        -d on -r off -p off -v 0 \
        -readProc cont_c_learner_r

    proc cont_c_learner_r { ins } {# this is the learning procedure, by polling
        global ${ins}_CON
        # do the work in a sub-routine; easier access to ${ins}_CON array
        cont_learner $ins
        varDoSet /${ins}/internal/learner -v [ set ${ins}_CON(lstage) ]
    }

proc cont_learner { ins } {
    # allow easy access to array name, instead of using global
    upvar ${ins}_CON CON
    varRead /${ins}/readback
    set rb [ varGetVal /${ins}/readback ]
    set v [expr $rb - [ set CON(learnoff) [expr $CON(learnoff) + $CON(inislope)] ] ]
set d 0.0
    set nv [ llength [ lappend CON(values) $v ] ]
varDoSet /${ins}/internal/learner -units $nv/$CON(maxinterval)
    if { $nv > 1 } {# get difference and update max differ
        # Tcl on mvme does not accept:  set p [ lindex $CON(values) end ]
        set p [ lindex $CON(values) [expr $nv - 2] ]
        set d [ expr 0.0 + $v - $p ]
        if { abs( $d ) > abs( $CON(maxdif) ) } {# new max diff
            set CON(maxdif) $d
            set CON(postmax) 0.0
        } else {
            set CON(postmax) [ expr $CON(postmax) + \
                    ( ( abs( $d ) < abs( 0.3 * $CON(maxdif) ) ) ? 1 : .3 ) ]
        }
    }
    if { $nv > $CON(mostvals) } {
        # lots of points; double poll interval, if it is variable
        set p [ varGetPollInterval /${ins}/internal/learner ]
        if { $p > $CON(maxinterval) } {
            cont_none $ins
            varDoSet /${ins}/setup_out/learn -p off -v 6 -m "LEARNING FAILED!"
            varSet /${ins}/function -v 1
            return
        }
        varDoSet /${ins}/internal/learner -p off
        varDoSet /${ins}/internal/learner -p on -p_int [expr $p * 2]
        # remove every second entry from list of values
        # (p is previous value)
        set lv ""
        set p [ lindex $CON(values) 0 ]
        set CON(maxdif) 0.0
        # Tcl on mvme does not understand:  foreach { e o } $CON(values)
        set i 0
        foreach e "$CON(values)" {
            if { $i/2 == ($i+1)/2 } { # even entry
                lappend lv $e
                if { abs( 0.0 + $e - $p ) > abs( $CON(maxdif) ) } {
                    set CON(maxdif) [ expr 0.0 + $e - $p ]
                }
                set p $e
            }
            incr i
        }
        set CON(values) $lv
    }
    switch $CON(lstage) {
      2 {# Find slope before disturbance
          if { [varGetVal /${ins}/status ] == 4 } {# unstable; restart
              set CON(values) ""
              set CON(maxdif) 0.0
          }
          set l $CON(values)
          varDoSet /${ins}/setup_out/learn -m "prelim vals: $l "
          if { [llength $l] > 3 } {# then apply step to output
              set CON(learnoff) [lindex $l 3]
              set CON(inislope) [expr $CON(learnoff) - [lindex $l 2] ]
              set CON(values) 0.0
              set CON(maxdif) 0.0
              # Apply step change to output:
              varSet /${ins}/output -v [expr [varGetVal /${ins}/output] + $CON(lstep) ]
              incr CON(lstage)
              varDoSet /${ins}/setup_out/learn -m "Step output by $CON(lstep)"
          }
        }
      3 {
            if { $CON(postmax) > $CON(numstable) } {
                incr CON(lstage)
            }
            varDoSet /${ins}/setup_out/learn -m \
		[format "read %.4g, diff %.4g, maxdif %.4g, postmax %.4g" $rb $d $CON(maxdif) $CON(postmax)]
        }
      4 {
            varDoSet /${ins}/setup_out/learn -m \
		[format "read %.4g, diff %.4g, maxdif %.4g, postmax %.4g" $rb $d $CON(maxdif) $CON(postmax)]
            set l $CON(values)
            set n [llength $l]
            set v3 [lindex $l [incr n -1] ]
            set v2 [lindex $l [incr n -1] ]
            set v1 [lindex $l [incr n -1] ]
            if { abs( 0.5*($v3 + $v1) - $v2 ) <= [varGetVal /${ins}/setup_read/stable_tol] } {
                incr CON(lstage)
            }
        }
      default { }
    }
    if { $CON(lstage) < 5 } { return }
    #
    # Done recording, now interpret record of values based on the type
    # of learning:  { } {I} {I interval} {P} {P I} {P I D}
    set type [ varGetVal /${ins}/setup_out/learn ]
    #   v = readback value from list
    #   r = response: v - previous v         
    set mxv 0.0 ; # max value; used as full response
    set mxr 0.0 ; # max response (change) for an iteration
    set vmr 0.0 ; # value at max response
    set imx 0   ; # index of max response
    set ihx 0   ; # index of half max response
    set vp  0.0 ; # previous value in list
    set i   -2  ; # counter
    foreach  v $CON(values) {
        incr i
        set r [expr $v - $vp]
        if { abs( $r ) > abs( $mxr ) } {
            set mxr [expr $v - $vp ]
            set imx $i
            set vmr $vp
        }
	if { abs( $r ) >= 0.5*abs( $mxr ) } {
	    set ihx $i
	}
        if { abs( $v ) > abs( $mxv ) } {
            set mxv $v
        }
        set vp $v
    }
    set P 0.0
    set I 0.0
    set D 0.0
varDoSet /${ins}/internal/init -m "lstep $CON(lstep), mxv $mxv, mxr $mxr "
    if { $type == 2 } { # variable interval
        set i 0
        foreach v $CON(values) {
            if { abs( $v ) < 0.9 * abs( $mxv ) } {
                incr i
                set mxr $v
            }
        }
        varSet /${ins}/setup_out/interval -v [expr $i * [ varGetPollInterval /${ins}/internal/learner ] ]
    }
    if { $type != 3 } { # all except P
        set I [expr $CON(lstep) / $mxv * $mxr / $mxv ]
    }
    if { $type > 2 } { # any with P
        set P [expr $CON(lstep) / $mxv - $I ]
    }
    if { $type > 4 } {# with D; get latency (in intervals)
        set lat [ bounded [ expr $imx - $vmr / $mxr ] 0.0 $imx ]
        set fac [ bounded [expr $r/$mxr] 0.0 1.0]
varDoSet /${ins}/internal/init -m "lstep $CON(lstep), lat $lat, mxv $mxv,"
varDoSet /${ins}/internal/learner -m "mxr $mxr, imx $imx, vmr $vmr, fac $fac"
        set P [ expr ($CON(lstep)/$mxr) / (1.0 + $lat + 2.0*$fac) ]
        set I [ expr $P / ($mxv/$mxr + $lat) ]
        set D [ expr $P * 0.5 * $lat ]
    }
    set m [expr ($type > 2) ? [varGetVal /${ins}/setup_out/lconfig/xgain ] : 1.0 ]
    varSet /${ins}/setup_out/Ki -v [ expr $I ]
    varSet /${ins}/setup_out/Kp -v [ expr $P * $m ]
    varSet /${ins}/setup_out/Kd -v [ expr $D * $m ]
    set CON(lstage) 0
    varSet /${ins}/function -v 2
}

proc cont_nothing { ins } {
    varDoSet /${ins}/readback -p off
    varDoSet /${ins}/internal/roller -p off
    varDoSet /${ins}/setup_out/learn -v 0 -m {}
    varDoSet /${ins}/internal/learner -p off -v 0
    varDoSet /${ins}/function -v 0
    varDoSet /${ins}/status -v 0
}

proc bounded { x lo hi } {
    expr "($x)<($lo) ? ($lo) : ( ($x)>($hi) ? ($hi) : ($x) )"
}

proc reduced { x r } {
    expr "($r)<=0.0 ? ($x) : (($x)>($r) ? ($x)-($r) : (($x)<-($r) ? ($x)+($r) : 0.0) )"
}
