# camp_ins_converter.tcl
#
# $Log: camp_ins_converter.tcl,v $
# Revision 1.4  2018/06/15 04:39:53  asnd
# Tiny change.
#
# Revision 1.3  2016/06/02 08:47:54  asnd
# Introduce minimum change for applied output. Other small changes.
#
# Revision 1.2  2015/04/25 00:13:27  asnd
# Add fourth readable var and one writable (result) var
#
# Revision 1.1  2009/09/30 08:23:21  asnd
# Add target value
#
#

CAMP_INSTRUMENT /~ -D -T "Generic converter" \
    -H "Pseudo-instrument to apply conversion functions to other Camp variable(s)" \
    -d on \
    -initProc Conv_init -deleteProc Conv_delete \
    -onlineProc Conv_online -offlineProc Conv_offline

proc Conv_init { ins } {
    insSet /$ins -if none 0 0
    insIfOn /$ins
}

proc Conv_delete { ins } { insSet /$ins -line off }

proc Conv_online { ins } { insIfOn /$ins }

proc Conv_offline { ins } { insIfOff /$ins }

CAMP_FLOAT /~/value -D -R -P -L -A -T "Calculated value" \
        -d on -r on -units "" -tol 0.0 \
        -readProc Conv_value_r

proc Conv_value_r { ins } {
    set func [varGetVal /${ins}/setup/function]
    if { [string length $func] == 0 } {
        return -code error "No function defined yet"
    }
    foreach v {w x y z} {
        catch {
            set cv [varGetVal /${ins}/setup/${v}_var_path]
            if { [string length $cv] } {
                set $v [varGetVal $cv]
            }
        }
    }
    if { [catch { expr $func } result] } {
        return -code error "Conversion failure: $result"
    }
    varDoSet /${ins}/value -v $result
    varTestAlert /${ins}/value [varGetVal /${ins}/target_value]
    set cv [varGetVal /${ins}/setup/result_var_path]
    if { [string match {/*} $cv] } {
	set ic [varGetVal /${ins}/setup/insig_change]
        set prev [varGetVal $cv]
        if { abs($result-$prev) > $ic } {
            varSet $cv -v $result
        }
    }
}

CAMP_FLOAT /~/target_value -D -S -T "Target value" \
        -H "The target value, used to test if the converted (read) value is within tolerance for alarm purposes" \
        -d on -s on  -units "" \
        -writeProc Conv_target_w

proc  Conv_target_w { ins target } {
    varDoSet /${ins}/target_value -v $target
}

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

CAMP_STRING /~/setup/function -D -S -T "Conversion function" \
    -H "Enter the conversion function here, using Tcl expr syntax, with variables \$w, \$x, \$y, \$z." \
    -d on -s on -writeProc Conv_function_w

proc Conv_function_w { ins func } {
    if { [llength [split $func {[];{}} ]] > 1 } {
        return -code error "Illegal function elements."
    }
    varDoSet /${ins}/setup/function -v $func
}

CAMP_STRING /~/setup/w_var_path -D -S -T "Camp var for \$w" \
    -H "Enter the Camp variable path for the \$w value." \
    -d on -s on -writeProc {Conv_varpath_w w}
    
CAMP_STRING /~/setup/x_var_path -D -S -T "Camp var for \$x" \
    -H "Enter the Camp variable path for the \$x value." \
    -d on -s on -writeProc {Conv_varpath_w x}
    
CAMP_STRING /~/setup/y_var_path -D -S -T "Camp var for \$y" \
    -H "Enter the Camp variable path for the \$y value." \
    -d on -s on -writeProc {Conv_varpath_w y}
    
CAMP_STRING /~/setup/z_var_path -D -S -T "Camp var for \$z" \
    -H "Enter the Camp variable path for the \$z value." \
    -d on -s on -writeProc {Conv_varpath_w z}

# Test if camp var exists and is numeric; or the string is blank.
proc Conv_varpath_w { which ins cvar } {
    global CAMP_VAR_TYPE_NUMERIC
    set cvar [string trim $cvar " \t/"]
    if { [string length $cvar] } {
        set cvar "/$cvar"
        set val [varGetVal $cvar]
        if { ([varGetVarType $cvar] & $CAMP_VAR_TYPE_NUMERIC) == 0 && [scan $val {%f %c} vv cc] == 0 } {
            return -code error "Camp variable $cvar is not numeric"
        }
    }
    varDoSet /${ins}/setup/${which}_var_path -v $cvar
}

CAMP_STRING /~/setup/units -D -S -T "Units of value" \
    -H "Set this if you care about the \"units\" display for the value" \
    -d on -s on -writeProc Conv_units_w

proc Conv_units_w { ins unit } {
    varDoSet /${ins}/value -units $unit
    varDoSet /${ins}/setup/units -v $unit
}

CAMP_STRING /~/setup/result_var_path -D -S -T "Camp var to set" \
    -H "If the calculated result should set another Camp variable, declare it here" \
    -d on -s on -writeProc {Conv_varpath_w result}

CAMP_FLOAT /~/setup/insig_change -D -S -T "Insignificant change" \
    -H "Do not apply calculated values to the result var if the change would be less than this" \
    -v 0.0 -d on -s on -writeProc Conv_insig_w

proc Conv_insig_w { ins val } {
    set val [expr {abs($val)}]
    varDoSet /${ins}/setup/insig_change -v $val
}
