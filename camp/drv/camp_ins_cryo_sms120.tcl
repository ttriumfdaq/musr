 # camp_ins_cryo_sms120.tcl or camp_ins_cryo_sms120_doc.tcl
 # Camp Tcl instrument driver for Cryogenic SMS120C superconducting magnet power supply.
 # Donald Arseneau,  TRIUMF
 # Last revised June 10, 2019
 # 
 # If there are no following comments, then this is camp_ins_cryo_sms120.tcl.
 # Do not edit camp_ins_cryo_sms120.tcl !!  Edit camp_ins_cryo_sms120_doc.tcl
 # instead; it has lots of comments.

CAMP_INSTRUMENT /~ -D -T "Cryogenics SMS120C-H" \
    -H "Cryogenics SMS120C-H Superconducting Magnet Power Supply" \
    -d on \
    -initProc cryo_ps120_init \
    -deleteProc cryo_ps120_delete \
    -onlineProc cryo_ps120_online \
    -offlineProc cryo_ps120_offline


if { [catch { set MAG(names) } n ] == 1 || $n == ""} {
    source "drv/magnet_procs.tcl"
}

proc cryo_ps120_init { ins } {
    global MAG
    mag_var_init $ins
    set MAG($ins,PS) cryo_sms120
    set MAG($ins,tol) 0.015
    set MAG($ins,bigtol) 0.08
    insSet /$ins -if rs232 0.5 2 none 9600 8 none 1 CRLF CRLF
}
proc cryo_ps120_delete { ins } {
    insSet /$ins -line off
}
proc cryo_ps120_online { ins } {
    global MAG
    if { [insGetIfDelay /$ins] < 0.4 } {
        set c "\[insSet /$ins -if [insGetIfTypeIdent /$ins] 0.4 [insGetIfTimeout /$ins] [insGetIf /$ins]\]"
        expr $c
    }
    insIfOn /$ins
    varRead /$ins/setup/id
    if { [varGetVal /$ins/setup/id] == 0 } {
        insIfOff /$ins
        return -code error "failed ID query, check interface definition and connections"
    }
    insIfRead /$ins "TESLA OFF" 128
    varDoSet /$ins/controls/do_ramp -p off
    varDoSet /$ins/setup/magnet -p on -p_int 0.5
    varDoSet /$ins/mag_field -p on -p_int 30
}
proc cryo_ps120_offline { ins } {
    insIfOff /$ins
}

proc cryo_readSET { path req resp un ins } {
    for {set try 0} {$try<4} {incr try} {
	set buf ""
	if { [catch { insIfRead /$ins "SET $req" 128 } buf] } { continue }
	set u ""
	set n [scan $buf " %s $resp: %f %s" p v u]
	if { $n > 1 && [string match $un $u] } {
	    varDoSet /$ins/$path -v $v
	    return
	}
	if { $n == 3 && $u == "TESLA" && $un == "AMPS" } {# BAD! So fix!
	    insIfRead /$ins "TESLA OFF" 128
	}
    }
    return -code error "Failed $ins/$path read: $buf"
}

proc cryo_setSET { path req dig resp un ins val } {
    set n 0
    set val [expr {[format %.7f $val]}]
    for {set try 0} {$try<4} {incr try} {
        set buf ""
        if { [catch { insIfRead /$ins "SET $req $val" 128 } buf] } { continue }
        if { [set i [string first "--->" $buf]] >= 0 } {
            return -code error "Failed set $ins/$path $val: [string range $buf [incr i 4] 128]"
        }
        set u ""
        set n [scan $buf " %s $resp: %f %s" p v u]
        if { $n == 3 && $u == "TESLA" && $un == "AMPS" } {# Bad setting! Fix it!
            insIfRead /$ins "TESLA OFF" 128
        }
        if { $n > 1 && [string match $un $u] } {
            set fp "00000"
            scan $buf " %*s $resp: %\[^. ].%\[0-9]" ip fp
            set fd [string length $fp]
            if { $fd > $dig } { set fd $dig }
            if { abs($v-$val)/(0.001+abs($val)) <= 3*pow(10,-$dig) || abs($v-$val) <= 2*pow(10,-$fd)} {
                varDoSet /$ins/$path -v $v
                return
            }
        }
    }
    return -code error "Failed set $ins/$path $val: $buf"
}

CAMP_FLOAT /~/mag_set -D -S -P -L -T "Set magnet current" \
    -H "Set magnet current and ramp to setpoint." \
    -d on -s on -units A \
    -writeProc mag_set_w
CAMP_FLOAT /~/mag_read -D -R -P -L -A -T "Read output current" \
    -H "Read power supply output current" \
    -d on -r on -units A -tol 0.002 -readProc cryo_ps120_get_output
proc cryo_ps120_get_output { ins } {
    for {set try 0} {$try<4} {incr try} {
        set buf ""
        if { [catch { insIfRead /$ins "GET OUTPUT" 128 } buf] } { continue }
        set n [scan $buf " %s OUTPUT: %f %s AT %f %s" p I Iun V Vun]
        if { $n < 5 } { continue }
        if { $Iun == "AMPS" && $Vun == "VOLTS" } {
            varDoSet /$ins/mag_read -v $I
            varDoSet /$ins/volts -v $V
            varTestAlert /$ins/mag_read [varGetVal /$ins/mag_set]
            return
        }
        if { $Iun == "TESLA" } {# BAD! FIX!
            insIfRead /$ins "TESLA OFF" 128
        }
    }
    return -code error "Bad $ins output: $buf"
}

CAMP_FLOAT /~/mag_field -D -S -R -P -L -A -T "Nominal Magnetic Field" \
    -H "Nominal Magnetic Field, using magnet calibration constant (read and set)" \
    -d on -s on -r on -units T -tol 0.001 \
    -readProc cryo_ps120_field_r -writeProc cryo_ps120_field_w
proc cryo_ps120_field_r { ins } {
    if { [varGetVal /$ins/heat_status] == 1 } {
        varRead /$ins/controls/i_persist
        set target [varGetVal /$ins/controls/i_persist]
    } else {
        varRead /$ins/mag_read
        set target [varGetVal /$ins/mag_read]
    }
    varDoSet /$ins/mag_field -v [ expr ( $target + 0.0 ) \
                    * ([varGetVal /$ins/setup/calibration]) ]
}
proc cryo_ps120_field_w { ins target } {
    varSet /$ins/mag_set -v [ expr ( $target + 0.0 ) / ([varGetVal /$ins/setup/calibration]) ]
    varDoSet /$ins/degauss/set_field -v $target
}

CAMP_FLOAT /~/degauss_field -D -S -T "Set with Degauss" \
    -H "Sets nominal magnetic field using Degauss procedures" \
    -d on -s on -units T -writeProc mag_degauss_set_field_w

CAMP_SELECT /~/ramp_status -D -S -T "Ramp status" -d on -s on \
    -H "Indicator for ramp-control sequencer" \
    -selections Holding Ramping Settling Persistent "Ramp leads +" "Ramp leads -" \
    "Heat switch" "Cool switch" "Turn on Heat" "Turn off Heat" "Degauss" "QUENCH" \
    -writeProc cryo_ps120_ramp_status_w
proc cryo_ps120_ramp_status_w { ins target } {
}

CAMP_SELECT /~/heat_status -D -S -R -P -A -T "Heater status" \
    -H "Superconducting switch heater status" \
    -d on -s on -r on -selections none off on \
    -readProc cryo_ps120_heat_status_r -writeProc cryo_ps120_heat_status_w
proc cryo_ps120_heat_status_r { ins } {
    varRead /$ins/controls/heat_onoff
    set h [varGetVal /$ins/controls/heat_onoff]
    if { $h == 0 && [varGetVal /$ins/heat_status] == 0 } { set h 3 }
    varDoSet /$ins/heat_status -v [lindex {1 2 0 0 0} $h]
}
proc cryo_ps120_heat_status_w { ins target } {
    set h [varGetVal /$ins/heat_status]
    if { ($h==0) == ($target==0) } {
        set h [lindex {0 0 1} $target]
        varSet /$ins/controls/heat_onoff -v $h
    } else {
        return -code error "define magnet to declare presence/absence of switch"
    }
}

CAMP_FLOAT /~/fast_set -D -S -R -L -P -T "Fast magnet set, no checks" \
    -H "Set magnet current. No smart control; no checks. (Read PS setpoint.)" \
    -d on -s on -r on -units A \
    -readProc {cryo_readSET fast_set "MID" "MID SETTING" "AMPS"} \
    -writeProc cryo_ps120_fast_set_w
proc cryo_ps120_fast_set_w { ins target } {
    cryo_setSET fast_set "MID" 4 "MID SETTING" "AMPS" $ins $target
    varSet /$ins/controls/activity -v 0
}

CAMP_FLOAT /~/settle_set -D -S -T "Set and Settle" \
    -H "Sets magnet and waits until stable" \
    -d on -s on -units A -writeProc mag_settle_w

CAMP_FLOAT /~/volts -D -R -P -L -T "Output voltage" \
    -d on -r on -units V -p_int 33 -readProc cryo_ps120_get_output
CAMP_SELECT /~/abort -D -S -T "Abort or Pause Ramp" \
    -H "Abort, pause, or continue a magnet change" \
    -d on -s on -v 0 -selections Abort Pause Resume "Never_mind" \
    -writeProc mag_abort_w

CAMP_SELECT /~/refresh -D -S -R -P -T "Refresh Persistent Current" \
    -H "Set or poll this to rejuvenate persistent field in magnet" \
    -d on -s on -r off -v 0 -selections Refresh \
    -readProc mag_refresh_r -writeProc mag_refresh_w


CAMP_STRUCT /~/degauss -D -T "Degauss magnet" -d on 

CAMP_FLOAT /~/degauss/amplitude -D -S -T "degauss delta-B" \
    -H "Set magnitude of field oscillations to use for degaussing magnet" \
    -units T -d on -s on \
    -writeProc mag_deg_ampl_w

CAMP_FLOAT /~/degauss/decrement -D -S -T "degauss % decrement" \
    -H "The percentage decrease in field for each stage of degaussing" \
    -d on -s on -units "%" -v 10.0 \
    -writeProc mag_deg_decrement_w

CAMP_FLOAT /~/degauss/set_field -D -S -T "degauss set B" \
    -H "Set the final field for after degaussing magnet" \
    -units T -d on -s on \
    -writeProc mag_deg_setf_w

CAMP_SELECT /~/degauss/degauss -D -R -P -S -T "degauss magnet" \
    -d on -s on -r off -p off -selections "FINISHED" "DEGAUSS NOW" \
    -readProc mag_degauss_r -writeProc mag_degauss_w

CAMP_STRUCT /~/setup -D -d on -T "Setup variables" \
    -H "Set magnet and power supply parameters here"
CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections false true -readProc cryo_ps120_id_r
proc cryo_ps120_id_r { ins } {
    set id 0
    set status [catch {insIfRead /$ins "SET RAMP" 128} buf]
    if { $status == 0 } {
        set id [scan $buf "%*s RAMP RATE: %f A/SEC" val]
        if { $id != 1 } { set id 0 }
    }
    varDoSet /$ins/setup/id -v $id
}

CAMP_SELECT /~/setup/magnet -D -S -R -P -T "Select magnet" \
    -H "Identify superconducting magnet" \
    -d on -s on -r on -p off -selections None Helios DR Belle bNMR hiTime nutime \
    -readProc cryo_ps120_magnet_r -writeProc cryo_ps120_magnet_w
proc cryo_ps120_magnet_r { ins } {
    global MAG
    varDoSet /$ins/setup/magnet -p off
    set cal [varRead /$ins/setup/calibration; varGetVal /$ins/setup/calibration]
    set i -1
    set target 0
    foreach c $MAG(callist) {
        incr i
        if { abs($cal-$c) <= 0.0001 } { set target $i }
    }
    varDoSet /$ins/setup/magnet -v $target
    if { [catch {set MAG($ins,name)}] } {
        set MAG($ins,name) "set-up"
    }
    if { [string tolower $MAG($ins,name)] != [string tolower [lindex $MAG(namelist) $target]] } {
        varDoSet /$ins/setup/magnet -m "$MAG($ins,name) not [lindex $MAG(namelist) $target], so re-load"
        cryo_ps120_magnet_w $ins $target
    }
}
proc cryo_ps120_magnet_w { ins target } {
    global MAG
    varRead /$ins/controls/ramp_status
    if { $target <= [llength $MAG(namelist)] } {
        varDoSet /$ins/setup/magnet -v $target
        set mag_S [lindex [string tolower $MAG(namelist)] $target ]
        source "drv/magnet_${mag_S}.ini"
    }
    cryo_ps120_all_status_r $ins
}

CAMP_FLOAT /~/setup/mag_max -D -S -R -L -T "Upper current limit" \
    -d on -s on -r on -units A \
    -readProc {cryo_readSET setup/mag_max "MAX" "MAX SETTING" "AMPS"} \
    -writeProc {cryo_setSET setup/mag_max "MAX" 3 "MAX SETTING" "AMPS"}

CAMP_FLOAT /~/setup/mag_min -D -T "Lower current limit (0)" \
    -d on -units A -v 0.0

CAMP_FLOAT /~/setup/volt_max -D -S -R -L -T "Voltage limit" \
    -d on -s on -r on \
    -readProc {cryo_readSET setup/volt_max "LIMIT" "VOLTAGE LIMIT" "VOLTS"} \
    -writeProc {cryo_setSET setup/volt_max "LIMIT" 2 "VOLTAGE LIMIT" "VOLTS"}

CAMP_FLOAT /~/setup/ramp_rate -D -S -L -T "Ramp rate setting (A/s)" \
    -H "Requested MAXIMUM ramp rate for magnet. Lower rates may be chosen automatically. Zero means no limit." \
    -d on -s on -units "A/s" \
    -writeProc cryo_ps120_ramp_rate_w
proc cryo_ps120_ramp_rate_w { ins target } {
    varDoSet /$ins/setup/ramp_rate -v $target
    varSet /$ins/controls/ramp_rate -v [varGetVal /$ins/controls/ramp_rate]
}
CAMP_SELECT /~/setup/ramp_mode -D -S -R -T "Ramp mode" \
    -H "Set this to choose method of magnet operation. Read to choose mode automatically." \
    -d on -r on -s on -selections non-persistant persistant -v 0 \
    -readProc mag_ramp_mode_r -writeProc mag_ramp_mode_w

CAMP_FLOAT /~/setup/heat_v -D -S -R -P -L -T "Heater voltage (V)" \
    -d on -s on -r on -units V \
    -readProc {cryo_readSET setup/heat_v "HEATER" "HEATER OUTPUT" "VOLTS"} \
    -writeProc {cryo_setSET setup/heat_v "HEATER" 2 "HEATER OUTPUT" "VOLTS"}

CAMP_FLOAT /~/setup/heat_time -D -S -T "Time to heat switch" \
    -H "Time for SC switch to change state after heater turned on/off." \
    -d on -s on -units s -v 0.0 -writeProc mag_heat_time_w

CAMP_FLOAT /~/setup/settle_time -D -S -T "Magnet settling time" \
    -H "Magnet settling time (for settle_set)" \
    -d on -s on -units s -v 20.0 -writeProc mag_settle_time_w

CAMP_SELECT /~/setup/pers_leads -D -S -T "Manage persistent leads" \
    -H "Say whether to zero leads in persistent mode, or hold current" \
    -selections Zero_curr Hold_curr \
    -v 0 -d on -s on -writeProc mag_pers_leads_w

CAMP_FLOAT /~/setup/calibration -D -S -R -L -T "Tesla per ampere" \
    -H "Magnet calibration, Tesla per Amp, used for setting mag_field" \
    -d on -s on -r on -units "T/A" -tol 0.0001 \
    -readProc {cryo_readSET setup/calibration "TPA" "FIELD CONSTANT" "T/A"} \
    -writeProc {cryo_setSET setup/calibration "TPA" 4 "FIELD CONSTANT" "T/A"}


CAMP_STRUCT /~/controls -D -d on -T "Control variables" \
    -H "Internal control variables: Don't set them manually."

CAMP_SELECT /~/controls/activity -D -R -S -T "Zero or setpoint" \
    -H "Sets PS to zero or setpoint" \
    -d on -r on -s on -selections MID ZERO HOLD \
    -readProc cryo_ps120_activity_r -writeProc cryo_ps120_activity_w
proc cryo_ps120_activity_r { ins } {
    varRead /$ins/controls/pause
    varRead /$ins/controls/ramp_target
    set target 2
    if { [varGetVal /$ins/controls/pause] == 0 } {
        set target [expr ([varGetVal /$ins/controls/ramp_target] > 0) ? 0 : 1 ]
    }
    varDoSet /$ins/controls/activity -v $target
}
proc cryo_ps120_activity_w { ins target } {
    if { $target >= 2 } {
        varSet /$ins/controls/pause -v ON
    } else {
        varSet /$ins/controls/ramp_target -v [lindex {MID ZERO} $target]
        varSet /$ins/controls/pause -v OFF
    }
    varDoSet /$ins/controls/activity -v $target
}

CAMP_SELECT /~/controls/ramp_status -D -R -P -T "Instrument Ramp status" \
    -d on -r on -selections HOLDING RAMPING QUENCH EXTERNAL \
    -readProc cryo_ps120_con_ramp_status_r
proc cryo_ps120_con_ramp_status_r { ins } {
    insIfReadVerify /$ins "RAMP STATUS" 128 /$ins/controls/ramp_status { %*[.] RAMP STATUS: %s %*s} 3
}

CAMP_SELECT /~/controls/do_ramp -D -P -T "Ramping Sequencer" \
    -H "Ramping Sequencer (polled when in operation)" \
    -d on -selections "DO RAMP" -readProc mag_do_ramp_r

CAMP_SELECT /~/controls/fast_leads -D -S -T "Select Ramp Mode" \
    -H "Selects a fast ramp rate for ramping leads" \
    -d on -s on -selections "Slow Mag" "Fast leads" \
    -writeProc cryo_ps120_leads_w
proc cryo_ps120_leads_w { ins target } {
    global MAG
    set r [varGetVal /$ins/setup/ramp_rate]
    if {($target == 0) || ([catch {set MAG($ins,leadsramp)}] == 1) } {
        if { ($r > 0.0) && ($r < [varGetVal /$ins/controls/ramp_rate]) } {
            varSet /$ins/controls/ramp_rate -v $r
        }
    } else {
        varRead /$ins/heat_status
        if { [varGetVal /$ins/heat_status]==1 } {
            set r $MAG($ins,leadsramp)
        }
        varSet /$ins/controls/ramp_rate -v $r
    }
    varDoSet /$ins/controls/fast_leads -v $target
}

CAMP_FLOAT /~/controls/ramp_rate -D -S -R -T "Instrument actual ramp rate" \
    -d on -s on -r on -units "A/s" \
    -readProc {cryo_readSET controls/ramp_rate "RAMP" "RAMP RATE" "A/SEC"} \
    -writeProc cryo_ps120_con_rr_w
proc cryo_ps120_con_rr_w { ins target } {
    set r1 [varGetVal /$ins/setup/ramp_rate]
    if { $r1 > 0.0 } {
        set r [expr $r1<$target ? $r1 : $target]
    } else {
        set r $target
    }
    cryo_setSET controls/ramp_rate "RAMP" 1 "RAMP RATE" "A/SEC" $ins $r
}

CAMP_FLOAT /~/controls/i_persist -D -R -L -T "Persistant magnet current" \
    -d on -r on -units A -readProc cryo_ps120_heat_onoff_r
CAMP_SELECT /~/controls/heat_onoff -D -S -R -P -T "Instrument heater status" \
    -d on -r on -s on -selections OFF ON \
    -readProc cryo_ps120_heat_onoff_r -writeProc cryo_ps120_heat_onoff_w
proc cryo_ps120_heat_onoff_r { ins } {
    set onoff "OFF"
    set buf [insIfRead /$ins "HEATER" 128]
    if { [scan $buf "%*s HEATER STATUS: SWITCHED OFF AT %f %*s" ip] != 1 } {
        set ip 0.0
        scan $buf "%*s HEATER STATUS: %s" onoff
    }
    varDoSet /$ins/controls/i_persist -v $ip
    varDoSet /$ins/controls/heat_onoff -v $onoff
}
proc cryo_ps120_heat_onoff_w { ins target } {
    set buf [insIfRead /$ins "HEATER $target" 128]
    if { [scan $buf "%*s HEATER STATUS: %s" onoff ] != 1 } {
        set buf [insIfRead /$ins "HEATER $target" 128]
    }
    set onoff nil
    scan "$buf " "%*s HEATER STATUS: %s " onoff
    switch [string first $onoff "ON OFF SWITCHED"] {
        -1 { return -code error "failed to set heater" }
        0 { }
        default { set onoff OFF }
    }
    varDoSet /$ins/controls/heat_onoff -v $onoff
}

CAMP_SELECT /~/controls/ramp_target -D -S -R -T "Ramp target setting" \
    -d on -s on -r on -selections ZERO MID MAX \
    -readProc cryo_ps120_ramp_r -writeProc cryo_ps120_ramp_w
proc cryo_ps120_ramp_r { ins } {
    insIfReadVerify /$ins "RAMP" 128 /$ins/controls/ramp_target "%*s RAMP TARGET: %s" 3
}
proc cryo_ps120_ramp_w { ins target } {
    set ramp [lindex {ZERO MID MAX} $target]
    insIfReadVerify /$ins "RAMP $ramp" 128 /$ins/controls/ramp_target "%*s RAMP TARGET: %s" 4
}

CAMP_SELECT /~/controls/pause -D -S -R -T "Pause" \
    -d on -s on -r on -selections OFF ON \
    -readProc cryo_ps120_pause_r -writeProc cryo_ps120_pause_w
proc cryo_ps120_pause_r { ins } {
    cryo_ps120_pause_w $ins ""
}
proc cryo_ps120_pause_w { ins target } {
    insIfReadVerify /$ins "PAUSE $target" 128 /$ins/controls/pause " %*s PAUSE STATUS: %s" 4
}

CAMP_SELECT /~/controls/xtrip -D -S -R -T "External trip" \
    -d on -s on -r on -selections DISABLED ENABLED ACTIVE \
    -readProc cryo_ps120_xtrip_r -writeProc cryo_ps120_xtrip_w
proc cryo_ps120_xtrip_r { ins } {
    insIfReadVerify /$ins "XTRIP" 128 /$ins/controls/xtrip "%*s EXTERNAL TRIP: %s" 3
}
proc cryo_ps120_xtrip_w { ins target } {
    if { $target > 1 } { set target 1 }
    insIfReadVerify  /$ins "XTRIP $target" 128 /$ins/controls/xtrip "%*s EXTERNAL TRIP: %s" 4
}

CAMP_SELECT /~/controls/lock -D -S -R -T "Lock" \
    -d on -s on -r on -selections OFF ON \
    -readProc cryo_ps120_lock_r -writeProc cryo_ps120_lock_w
proc cryo_ps120_lock_r { ins } {
    insIfReadVerify /$ins "LOCK" 128 /$ins/controls/lock "%*s LOCK: %s" 3
}
proc cryo_ps120_lock_w { ins target } {
    insIfReadVerify /$ins "LOCK $target" 128 /$ins/controls/lock "%*s LOCK: %s" 4
}

proc cryo_ps120_all_status_r { ins } {
	varRead /$ins/controls/activity
	varRead /$ins/heat_status
	varRead /$ins/fast_set
	varRead /$ins/mag_field
	varRead /$ins/setup/mag_max
	varRead /$ins/controls/ramp_status
	set rs [varGetVal /$ins/controls/ramp_status]
	if { $rs > 1 } {
	    varDoSet /$ins/ramp_status -v "QUENCH"
	    return 
	}
	set hs [varGetVal /$ins/heat_status]
	set act [varGetVal /$ins/controls/activity]
	set rs [lindex [list $rs [expr {3+$rs*($act==0)}] 1] $hs]
	varDoSet /$ins/ramp_status -v $rs
        if { [varGetVal /$ins/mag_set] == 0.0 && [varNumGetNum /$ins/mag_set] == 0 } {
            if { $hs==1 } {
                varDoSet /$ins/mag_set -v [varGetVal /$ins/controls/i_persist]
            } else {
                varDoSet /$ins/mag_set -v [varGetVal /$ins/fast_set]
            }
        }
        if { $rs==0 } {
            varDoSet /$ins/mag_set -v [varGetVal /$ins/mag_read]
        } elseif { $act==1 && $hs != 1 } {
            varDoSet /$ins/mag_set -v 0.0
        }
        if { $rs == 0 || $rs == 3 } {
            varDoSet /$ins/controls/do_ramp -p off
        } else {
            varSet /$ins/mag_set -v [varGetVal /$ins/mag_set]
        }
}
