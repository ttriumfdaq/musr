# camp_ins_dr_flow.tcl
# 
# This is the camp device driver for the flow control in the DR cabinet, consisting of an
# Omega Mass Flow controller plus a pressure transducer. As such it is acombination of
# two ADCs and one DAC.

CAMP_INSTRUMENT /~ -D -T "DR Flow" \
    -H "DR Flow and pressure control" -d on \
    -initProc drf_init -deleteProc drf_delete \
    -onlineProc drf_online -offlineProc drf_offline

proc drf_init { ins } {
    insSet /$ins -if none 0.0 0.0
    varDoSet /$ins/setup/IPPos -v 3
    insIfOn /$ins
}
proc drf_delete { ins } {
    insSet /$ins -line off
}
proc drf_online { ins } {
    insIfOn /$ins
    varSet /$ins/setup/flow_gain -v 1
    varSet /$ins/setup/press_gain -v 1
}
proc drf_offline { ins } {
    insIfOff /$ins
}

CAMP_FLOAT /~/read_pressure -D -R -P -L -A -T "Read Pressure" \
    -d on -r on -p off -p_int 8 -units mBar \
    -H "He Mixture pressure, in mBar." \
    -readProc drf_read_press_r

proc drf_read_press_r { ins } {
    global CAMP_VAR_ATTR_ALARM
    set pos [varGetVal /$ins/setup/IPPos]
    set chan [varGetVal /$ins/setup/press_chan]
    if { $chan < 0 } {
        return -code error "Select a channel first (setup/press_chan)"
    }
    set full [varGetVal /$ins/setup/press_scale]
    set offset [varGetVal /$ins/setup/press_offset]
    set maxi [varGetVal /$ins/setup/press_full_adc]
    insIfRead /$ins "AdcRead $pos $chan adc_read" 10
    set value [format %.1f [expr {0.0 + ($adc_read * $full / $maxi) - $offset}]]
    set limit [varGetVal /$ins/control/press_limit]
    set a [expr {[varGetStatus /$ins/read_pressure] & $CAMP_VAR_ATTR_ALARM}]
    set alert [expr {(($a!=0)&&($value>$limit)) ? "on" : "off"}]
    varDoSet /$ins/read_pressure -v $value -alert $alert
}

CAMP_FLOAT /~/read_flow -D -R -P -L -A -T "Read Flow" \
    -d on -r on -p off -p_int 12 -units "slpm" \
    -H "Measured flow form the flow controller, in percent." \
    -readProc drf_read_flow_r

proc drf_read_flow_r { ins } {
    set pos [varGetVal /$ins/setup/IPPos]
    set chan [varGetVal /$ins/setup/flow_chan]
    if { $chan < 0 || $chan > 3 } {
        return -code error "Select a channel first (setup/flow_chan)"
    }
    set full [varGetVal /$ins/setup/flow_scale]
    set offset [varGetVal /$ins/setup/flow_offset]
    set maxi [varGetVal /$ins/setup/flow_full_adc]
    insIfRead /$ins "AdcRead $pos $chan adc_read" 10
    set value [format %.3f [expr {0.0 + $adc_read * ( $full / $maxi ) - $offset}]]
    varDoSet /$ins/read_flow -v $value
    varTestAlert /$ins/read_flow [varGetVal /$ins/ctrl_flow]
}

CAMP_FLOAT /~/set_flow -D -S -L -T "Set Desired Flow" \
    -d on -s on -units "slpm" \
    -H "Set the desired flow. The applied setting may be lower (see ctrl_flow)" \
    -writeProc drf_set_flow_w

proc drf_set_flow_w { ins target } {
    set full [varGetVal /$ins/setup/flow_scale]
    if { $full <= 0.0 } { set full 40.0 }
    if { ( $target < 0 ) || ( $target > $full+0.000001 ) } {
        return -code error "Setting $target out of range 0 to $full"
    }
    varDoSet /$ins/set_flow -v $target
    varDoSet /$ins/control/iter_sec -p on -p_int [varGetVal /$ins/control/iter_sec]
    varRead  /$ins/control/iter_sec
}

CAMP_FLOAT /~/ctrl_flow -D -S -L -T "Applied Flow Setting" \
    -d on -s off -units "slpm" \
    -H "This is the instantaneous flow setting, adjusted to prevent pressure trips." \
    -writeProc drf_ctrl_flow_w

proc drf_ctrl_flow_w { ins target } {
    set full [varGetVal /$ins/setup/flow_scale]
    if { $full <= 0.0 } { set full 40.0 }
    set pos [varGetVal /$ins/setup/IPPos]
    set chan [varGetVal /$ins/setup/flow_chan]
    if { $chan < 0 } {
        return -code error "Select a channel!"
    }
    set gain [lindex {1.0 2.0 4.0 8.0} [varGetVal /$ins/setup/flow_gain]]
    set maxi [varGetVal /$ins/setup/flow_full_adc]
    # Dac has fixed gain of 1, so convert from ADC gain
    set dac_set [expr { round( ($target/$full) * ($maxi/$gain) ) }]
    set dac_set [expr { $dac_set < 0 ? 0 : $dac_set > 2047 ? 2047 : $dac_set }]
    insIfWrite /$ins "DacSet $pos $chan dac_set"
    varDoSet /$ins/ctrl_flow -v $target
}

CAMP_SELECT /~/op_mode -D -S -T "Operation Mode" \
    -selections "Manual" "P limited" "P & T control" \
    -H "Select how actual flow setting (ctrl_flow) is determined (see parameters under control/)" \
    -d on -s on -v 1 -writeProc drf_op_mode_w

proc drf_op_mode_w { ins target } {
    varDoSet /$ins/op_mode -v $target
    drf_roller $ins
}

CAMP_STRUCT /~/control -D -T "Control variables" -d on \
    -H "Configuration of flow adjustment based on pressure and temperature"

CAMP_FLOAT /~/control/press_limit -D -S -T "Pressure limit" \
    -d on -s on -v 800.0 -units mBar \
    -H "Maximum pressure allowed. Please set less than the pressure trip setting on G4." \
    -writeProc {drf_ctrlpar_w press_limit 0 1000}

CAMP_FLOAT /~/control/repulsion -D -S -T "Pressure limit repulsion" \
    -d on -s on -v 30.0 -units mBar \
    -H "Repulsion range holding pressure below limit" \
    -writeProc {drf_ctrlpar_w repulsion 0 1000}

CAMP_FLOAT /~/control/flow_limit -D -S -T "Max flow" \
    -d on -s on -v 4.0 -units "slpm" \
    -H "Maximum automatic flow setting" \
    -writeProc {drf_ctrlpar_w flow_limit 0 40}

CAMP_FLOAT /~/control/flow_change -D -S -T "Max flow incr" \
    -d on -s on -v 40.0 -units "slpm/min" \
    -H "Maximum increase in flow setting per minute" \
    -writeProc {drf_ctrlpar_w flow_change 0 10000}

CAMP_FLOAT /~/control/flow_diff -D -S -T "Max flow diff" \
    -d on -s on -v 1.0 -units "slpm" \
    -H "Maximum amount flow setting may be above the flow reading" \
    -writeProc {drf_ctrlpar_w flow_diff 0 40}

CAMP_FLOAT /~/control/iter_sec -D -S -R -P -T "Iteration time" \
    -d on -s on -r on -p off -p_int 5.0 -v 5.0 -units "sec" \
    -H "Time between adjustments to the flow setting" \
    -readProc drf_roller -writeProc drf_iter_w

CAMP_STRING /~/control/T_set_var -D -S -T "Temperature setpoint variable" \
    -d on -s on -v "/DR_temp/control_set" \
    -H "Enter the full Camp variable path for the DR (mixing chamber) set-point" \
    -writeProc {drf_ctrlvar_w T_set_var}

CAMP_STRING /~/control/T_read_var -D -S -T "Temperature reading variable" \
    -d on -s on -v "/DR_temp/read_mix_cham" \
    -H "Enter the full Camp variable path for the DR (mixing chamber) temperature reading" \
    -writeProc {drf_ctrlvar_w T_read_var}

proc drf_ctrlpar_w {which min max ins val} {
    if { $val < $min || $val > $max } {
        return -code error "Value for $ins $which must be in range $min to $max"
    }
    varDoSet /$ins/control/$which -v $val
}

proc drf_iter_w {ins val} {
    if { $val<0.5 } { set val 1.0 }
    varDoSet /$ins/control/iter_sec -v $val -p on -p_int $val
}

proc drf_ctrlvar_w {which ins var} {
    set var [string trim $var]
    if { [string length $var] == 0 } {
        varDoSet /$ins/control/$which -v $var
        if { [varGetVal /$ins/op_mode] == 2 } {
            varDoSet /$ins/op_mode -v 1
        }
    } else {
        if { [string first "/" $var] != 0 } {
            set var "/$var"
        }
        if { [catch {set val [varGetVal $var]; expr {0.0+$val}}] } {
            return -code error "$which setting ($val) is not a proper Camp variable"
        }
        varDoSet /$ins/control/$which -v $var
    }
}

# The control loop procedure.
# In mode 0, just apply requested flow to the flow controller.
# In mode 1, adjust applied flow setting to avoid over-pressure.
# In mode 2, set requested flow based on temperature, and continue as if mode 1.
# (Use expr with varGetVal to shorten numbers for the debug message.)
proc drf_roller { ins } {
    set sec [varGetVal /$ins/control/iter_sec]
    varDoSet /$ins/control/iter_sec -v $sec -p on -p_int $sec
    set prevp [varGetVal /$ins/read_pressure]
    if { [catch {
        varRead /$ins/read_pressure; set press [varGetVal /$ins/read_pressure]
        varRead /$ins/read_flow ;    set readf [varGetVal /$ins/read_flow]
    } msg ] } {
        varDoSet /$ins/control/iter_sec -m "Failed iteration at read: $msg"
        return -code error $msg
    }
    set mode [varGetVal /$ins/op_mode]
    set setf [expr [varGetVal /$ins/set_flow]]
    if { $mode == 0 } {
        catch { varSet /$ins/ctrl_flow -v $setf }
        return
    }
    set mf [varGetVal /$ins/control/flow_limit]
    set maxf [varGetVal /$ins/setup/flow_scale]
    set maxf [expr { $mf>$maxf ? $maxf : $mf }]
    set dif [expr [varGetVal /$ins/control/flow_diff]]
    set prev [expr [varGetVal /$ins/ctrl_flow]]
    if { $mode == 2 } {
        set cc 0.35
        set bb 1.2477
        set aa -0.08071
        set ee 2.0
        if { [catch {
            set st [varGetVal [varGetVal /$ins/control/T_set_var]]
            set rt [varGetVal [varGetVal /$ins/control/T_read_var]]
	    set td [expr { $ee*($st-$rt) > 3.0 ? 3.0 : $ee*($st-$rt) < -3.0 ? -3.0 : $ee*($st-$rt) }]
            set xt [expr { $st + $td < 0.3 ? 0.3 : $st + $td }]
            set setf [expr { $cc+$bb/($xt+$aa) }]
	    if { $setf < 1.5 && $td < -0.2 } {# want more than threshold flow rate when warm
		set setf 1.5
	    }
	    set mf [expr {$maxf > $prev+$dif ? $prev+$dif : $maxf }]
            set setf [expr { $setf > $mf ? $mf : ($setf < 0.0 ? 0.0 : $setf) }]
            set setf [format %.2f $setf]
            varDoSet /$ins/set_flow -v $setf
            varDoSet /$ins/control/T_set_var -m "td $td, xt $xt, sf [expr { $cc+$bb/($xt+$aa) }]"
        } msg] } {
            varDoSet /$ins/control/T_set_var -m "Failed iteration for T adj: $msg"
        }
    }
    set lim [expr [varGetVal /$ins/control/press_limit]]
    set stp [expr {[varGetVal /$ins/control/flow_change]*$sec/60.0}]
    set del [expr [varGetVal /$ins/control/repulsion]]
    if {[catch {
	set px [expr {$press>$prevp ? $press+(20.0+$sec)/(1.0+$sec)*($press-$prevp) : $press}]
	if { $px<$lim } {
	    set ff [expr {1.0 - ($del + $del*$del/$lim)*(1.0/($lim+$del-$px) + 1.0/($lim+$del))}]
	} else {
	    set ff 0.0
	}
        set new [expr {($ff<0.8 ? 1.25*$ff : 1.0) * $setf}]
        set new [expr {$new > $maxf ? $maxf : ($new < 0.0 ? 0.0 : $new)}]
        set new [expr {$new > $readf+$dif*$ff ? $readf+$dif*$ff : $new}]
        set new [expr {$new > $prev+$stp*$ff ? $prev+$stp*$ff : $new}]
        varDoSet /$ins/control/repulsion -m "ff = $ff = 1.0 - ($del + $del*$del/$lim)*(1.0/($lim+$del-$px) + 1.0/($lim+$del))"
        varSet /$ins/ctrl_flow -v $new 
    } msg]} {
        varDoSet /$ins/control/iter_sec -m "Failed iteration at ctrl: $msg"
	return -code error $msg
    }
}

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on \
    -H "Variables for the hardware definition and calibration"

CAMP_SELECT /~/setup/IPPos -D -S -T "IP Position" \
    -d on -s off -v 3 -selections 0 1 2 3 \
    -H "Industry Pack slot occupied by ADC/DAC board" \
    -writeProc drf_IPPos_w

proc drf_IPPos_w { ins target } {
    varDoSet /$ins/setup/IPPos -v $target
}

# This is tricky.  Because the interface goes "online" immediately, perhaps
# with the wrong channel number, we do not apply the adc gain parameter
# at startup, but only when the channel is selected.  The readback checks
# for an invalid channel number before trying to read.  When the server
# is restarted, the channel is "polled" to apply the restored flow setting.
#
CAMP_INT /~/setup/flow_chan -D -S -R -P -T "Flow DAC \& ADC Channel" \
    -d on -s on -r off -p on -p_int 3 -v "-1" \
    -H "Flow DAC and ADC channel number (use the same for both)" \
    -readProc {drf_channel_r flow} -writeProc {drf_channel_w flow}

proc drf_channel_r { which ins } {
    set c [varGetVal /$ins/setup/${which}_chan]
    if { $c >= 0 } {
        varSet /$ins/setup/${which}_chan -p off -v $c
    }
}
proc drf_channel_w { which ins target } {
    if { ( $target < 0 ) || ( $target > 3 ) } {
        return -code error "Channel number out of range: 0 to 3"
    }
    varDoSet /$ins/setup/${which}_chan -v $target
    varSet /$ins/setup/${which}_gain -v [varGetVal /$ins/setup/${which}_gain]
    varRead /$ins/control/iter_sec
}

CAMP_FLOAT /~/setup/flow_scale -D -S -T "Full Scale" \
    -d on -s on -v 40.0 \
    -H "Calibration: full scale corresponds to this flow rate" \
    -writeProc {drf_full_scale_w flow}

CAMP_FLOAT /~/setup/flow_full_adc -D -S -T "Full Scale ADC" \
    -d on -s on -v 2047 -units "" \
    -H "ADC value corresponding to full scale flow." \
    -writeProc {drf_full_adc_w flow}

CAMP_FLOAT /~/setup/flow_offset -D -S -T "Readback Offset" \
    -d on -s on -v 0.0 \
    -H "Set this if you need to correct for a non-zeroed flow readback" \
    -writeProc {drf_offset_w flow}

CAMP_SELECT /~/setup/flow_gain -D -S -T "Flow ADC Gain" \
    -d on -s on -v 1 -selections x1 x2 x4 x8 \
    -writeProc {drf_gain_w flow}

proc drf_full_scale_w { which ins target } {
    varDoSet /$ins/setup/${which}_scale -v $target
}

proc drf_full_adc_w { which ins target } {
    varDoSet /$ins/setup/${which}_full_adc -v $target
}

proc drf_offset_w { which ins target } {
    varDoSet /$ins/setup/${which}_offset -v $target
}

proc drf_gain_w { which ins target } {
    set gain $target
    set pos [varGetVal /$ins/setup/IPPos]
    set chan [varGetVal /$ins/setup/${which}_chan]
    insIfWrite /$ins "GainSet $pos $chan gain"
    varDoSet /$ins/setup/${which}_gain -v $gain
}

CAMP_INT /~/setup/press_chan -D -S -R -P -T "Pressure ADC Chan" \
    -d on -s on -r off -p on -p_int 2.5 -v "-1" \
    -H "Pressure ADC channel number" \
    -readProc {drf_channel_r press} -writeProc {drf_channel_w press}

CAMP_FLOAT /~/setup/press_scale -D -S -T "Full Scale P" \
    -d on -s on -v 1034.21 -units mBar \
    -H "Calibration: full scale corresponds to this pressure" \
    -writeProc {drf_full_scale_w press}

CAMP_FLOAT /~/setup/press_full_adc -D -S -T "Full Scale ADC" \
    -d on -s on -v 2047 -units "" \
    -H "ADC value corresponding to full scale pressure." \
    -writeProc {drf_full_adc_w press}

CAMP_FLOAT /~/setup/press_offset -D -S -T "P Offset" \
    -d on -s on -v 0.0 -units mBar \
    -H "Set this if you need to correct for a non-zeroed pressure reading" \
    -writeProc {drf_offset_w press}

CAMP_SELECT /~/setup/press_gain -D -S -T "Pressure ADC Gain" \
    -d on -s on -v 1 -selections x1 x2 x4 x8 \
    -writeProc {drf_gain_w press}

