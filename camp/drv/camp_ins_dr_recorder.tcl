# camp_ins_dr_recorder.tcl
#
# A specific monitor/converter non-device instrument that monitors the
# voltage measurement from the DR bridge "Recorder" output, and reports
# ohms and temperature.  The recorder output gives only the mantissa 
# of the resistance measurement, so the power of 10 must be entered
# manually.  A facility is provided to track wrap-arounds of the raw
# reading and adjust the power of ten automatically.
#
# $Log: camp_ins_dr_recorder.tcl,v $
# Revision 1.5  2015/11/05 05:29:59  asnd
# Add range selector that mirrors exponent.
#
# Revision 1.4  2015/05/18 21:34:17  asnd
# Better handling of range changes; alarms.
#
# Revision 1.3  2015/04/25 00:11:27  asnd
# Try to track range changes better, and alert when unsure.
#
# Revision 1.2  2015/04/17 05:24:47  asnd
# Better, but not great, handling of range changes.
#
#
# prevV     The previous voltage reading, but skipping zero values
# prevN     The previous varNumGetNum for the voltage variable (no skipping)
# logV      Log of voltage values, skipping values that changed a lot (including zeros)
# prevExp   The exponent that applied to the entire log (changing exponent clears log)
# loglen    maximum length of V log
# unstable  count of ubstable (skipped) voltage readings 
# quituns   discard log after this many unstable readings (20)
# sensor    copy of /~/setup/sensor
# caltype   calibration type: fun, spl, or dat
# caltable  copy of the curve calibration table
# calfunc   function (Tcl proc) name used to compute the calibration (of type caltype)
# prevint   cache the interval used previously for table interpolation

# $Log: camp_ins_dr_recorder.tcl,v $
# Revision 1.5  2015/11/05 05:29:59  asnd
# Add range selector that mirrors exponent.
#
# Revision 1.4  2015/05/18 21:34:17  asnd
# Better handling of range changes; alarms.
#
# Revision 1.3  2015/04/25 00:11:27  asnd
# Try to track range changes better, and alert when unsure.
#

CAMP_INSTRUMENT /~ -D -T "DR Recorder" \
    -H "Pseudo-instrument to convert DR ohms to temperature. Use a separate voltmeter instrument to measure the recorder signal." \
    -d on \
    -initProc DRR_init -deleteProc DRR_delete \
    -onlineProc DRR_online -offlineProc DRR_offline

proc DRR_init { ins } {
    global DRR
    set DRR(prevV) {}
    set DRR(prevN) 0
    set DRR(logV) {}
    set DRR(prevExp) -1
    set DRR(loglen) 5
    set DRR(unstable) 0
    set DRR(quituns) 20
    set DRR(sensor) {}
    set DRR(caltype) {}
    set DRR(caltable) {}
    set DRR(calfunc) {}
    set DRR(prevint) {}
    insSet /$ins -if none 0.0 0.0
    insSet /$ins -line on
}

proc DRR_delete { ins } {
    global DRR
    insSet /$ins -line off
    unset DRR
}

proc DRR_online { ins } { 
    global DRR
    insIfOn /$ins
    if { [string length $DRR(sensor)] == 0 && [string length [varGetVal /${ins}/setup/sensor]] > 0 } {
        if { [catch {varSet /${ins}/setup/sensor -v [varGetVal /${ins}/setup/sensor]}] } {
            varSet /${ins}/setup/sensor -v ""
        }
    }
    varDoSet /${ins}/resistance -a on
    varDoSet /${ins}/exponent -a on
}

proc DRR_offline { ins } {
    insIfOff /$ins
}

CAMP_FLOAT /~/temperature -D -R -P -L -A -T "Temperature" \
        -d on -r on -p_int 10 -a on -a_act just_beep -tol 0.05 -units "K" \
        -H "Temperature as calculated from the bridge resistance" \
        -readProc DRR_temperature_r

CAMP_FLOAT /~/target_temp -D -S -L -T "Target Temperature" \
        -d on -s on -units "K" \
        -H "Set the desired temperature here for the purposes of alarms.  This does NOT control the setpoint." \
        -writeProc DRR_target_t_w

CAMP_FLOAT /~/resistance -D -R -L -A -T "Bridge resistance" \
        -d on -r on -a on -a_act just_beep -tol 1 -units "Ohm" \
        -H "Bridge resistance determined from the raw recorder voltage, offset, and exponent" \
        -readProc DRR_resistance_r

CAMP_FLOAT /~/raw_V -D -R -L -T "Recorder voltage" \
        -d on -r on -units "V" \
        -H "Raw voltage measured on the bridge's recorder output (using another instrument)" \
        -readProc DRR_raw_v_r

CAMP_INT /~/exponent -D -S -R -L -A -T "Exponent of 10" \
        -d on -s on -r on -a on -a_act just_beep -v 0 -units "" -tol 1 \
        -H "Power-of-ten to convert raw voltage mantissa into actual resistance reading in ohms, based on R_range" \
        -writeProc DRR_exponent_w -readProc DRR_exponent_r

# Note that selection index = exponent, so we can use the same writeProc.
CAMP_SELECT /~/R_range -D -S -L -T "Bridge range" \
        -d on -s on -selections "" 20 200 2k 20k 200k 2M \
        -H "Manually set this to match the true range setting on the resistance bridge; gives exponent" \
        -writeProc DRR_exponent_w

CAMP_FLOAT /~/offset_V -D -S -L -T "Offset voltage" \
        -d on -s on -units "V" \
        -H "Correction that will be added to the raw recorder voltage to offset a constant measurement error" \
        -writeProc DRR_offset_v_w


proc DRR_temperature_r { ins } {
    global DRR
    if { [catch {varRead /${ins}/resistance} msg] } {
	varTestAlert /${ins}/temperature 999.9
	varDoSet /${ins}/temperature -m "Invalid Temperature: $msg"
	return -code error "Invalid Temperature: $msg"
    }
    set R [varGetVal /${ins}/resistance]
    if { [string length $DRR(calfunc)] == 0 } {
	varDoSet /${ins}/temperature -m "Invalid Temperature: no sensor yet"
        return -code error "Sensor calibration is not configured yet."
    }
    if { [catch "$DRR(calfunc) $R" T] } {
	varDoSet /${ins}/temperature -v 0 -m "Invalid Temperature conversion: $T"
	varTestAlert /${ins}/temperature 999.9
    } else {
	varDoSet /${ins}/temperature -v $T -m ""
	varTestAlert /${ins}/temperature [varGetVal /${ins}/target_temp]
    }
}

proc DRR_target_t_w { ins val } {
    varDoSet /${ins}/target_temp -v $val
}

# Calculate resistance from voltage reading.
# Track sudden changes and adjust exponent accordingly

proc DRR_resistance_r { ins } {
    global DRR
    varRead /${ins}/raw_V
    set V [varGetVal /${ins}/raw_V]
    if { $V < 0.0005 } { # zero voltage mean no lock, invalid reading
        varDoSet /${ins}/resistance -a on -m "Bad voltage value: $V. Check bridge lock"
	varTestAlert /${ins}/resistance -999
        incr DRR(unstable)
        return -code error "Bad voltage value: $V. Check bridge lock."
    }
    set e [varGetVal /${ins}/exponent]
    set mes ""

    if { $e != $DRR(prevExp) } {
        DRR_reset_Vlog $e
        set DRR(unstable) 0
    }
    if { [string length $DRR(prevV)] == 0 } { set DRR(prevV) $V }

    if { $DRR(unstable) > $DRR(quituns) } {
        # Resetting the log prevents stupid adjustments to the exponent
        DRR_reset_Vlog $e
        varDoSet /${ins}/exponent -a on -m "Prolonged unstable voltage; exponent and R_range are likely wrong"
	varTestAlert /${ins}/exponent 99
    }

    if { abs($DRR(prevV) - $V)/($DRR(prevV) + $V + 0.001) > 0.2 } { # large V change
        incr DRR(unstable)
        varTestAlert /${ins}/exponent 99
    } else { # small V change = "stable" now.
        if { $DRR(unstable) } { # if we were previously unstable, check values against log ...
            if { [llength $DRR(logV)] > 3 } { # ... if there is a log
                # Take average of log for comparison. Maybe we should do extrapolation instead.
                set mean [expr "double([join $DRR(logV) +])/[llength $DRR(logV)]"]
                if { abs($mean - $V) / ($mean + $V + 0.01) < 0.4 } { # Transient instability returning to same range
                    varDoSet /${ins}/exponent -v $e -a on -m ""
		    varTestAlert /${ins}/exponent $e
                } elseif { abs(100.0*$mean - $V)/(100.0*$mean + $V + 0.02) < 0.3 } {
                    incr e -2
                    DRR_reset_Vlog $e
                    varDoSet /${ins}/exponent -v $e -a on -m "Automatically decreased exponent to $e. Please check R_range!" 
		    varTestAlert /${ins}/exponent $e
                } elseif { abs($mean - 100.0*$V)/($mean + 100.0*$V + 0.02) < 0.3 } {
                    incr e 2
                    DRR_reset_Vlog $e
                    varDoSet /${ins}/exponent -v $e -a on -m "Automatically increased exponent to $e. Please check R_range!"
		    varTestAlert /${ins}/exponent $e
                } elseif { abs(10.0*$mean - $V)/(10.0*$mean + $V + 0.02) < 0.4 } {
                    incr e -1
                    DRR_reset_Vlog $e
                    varDoSet /${ins}/exponent -v $e -a on -m "Automatically decreased exponent to $e. Please check R_range!" 
		    varTestAlert /${ins}/exponent $e
                } elseif { abs($mean - 10.0*$V)/($mean + 10.0*$V + 0.02) < 0.4 } {
                    incr e
                    DRR_reset_Vlog $e
                    varDoSet /${ins}/exponent -v $e -a on -m "Automatically increased exponent to $e. Please check R_range!"
		    varTestAlert /${ins}/exponent $e
                } else { # no match: can't handle
		    varDoSet /${ins}/exponent -a on -m "Unhandled large change of V. Check and set R_range manually!" 
		    varTestAlert /${ins}/exponent 99
		}
		set DRR(logV) [linsert $DRR(logV) 0 $DRR(prevV)]
	    } else { # no log: can't handle
		varDoSet /${ins}/exponent -a on -m "Unhandled large change of V. Check and set R_range manually!" 
		varTestAlert /${ins}/exponent 99
	    }
        }
        set DRR(logV) [lrange [linsert $DRR(logV) 0 $V] 0 $DRR(loglen)]
        set DRR(unstable) 0
    }
    varDoSet /${ins}/R_range -v $e

    set DRR(prevV) $V

    set R [expr { ($V+[varGetVal /${ins}/offset_V])*pow(10,$e) } ]

    varDoSet /${ins}/resistance -v $R -m ""
    varTestAlert /${ins}/resistance $R
}

# "Read" the raw voltage from another instrument.  Force a fresh
# reading of the voltage by the other instrument unless the count
# of reads is 1 to 5 greater than the previous count.

proc DRR_raw_v_r { ins } {
    global DRR
    set cv [varGetVal /${ins}/setup/voltage_var]
    if { [string length $cv] == 0 } {
        return -code error "No path to voltage reading (/${ins}/setup/voltage_var)"
    }
    if { abs([varNumGetNum $cv]-$DRR(prevN)-3) > 2 } {
        varRead $cv
    }
    set DRR(prevN) [varNumGetNum $cv]
    varDoSet /${ins}/raw_V -v [varGetVal $cv]
}

# For manually setting the exponent.  We want to immediately
# update the temperature (and resistance) reading to reflect
# the new exponent, and we want to suppress any automatic
# exponent change for that reading.

proc DRR_exponent_w { ins val } {
    global DRR
    if { $val < 1 || $val > 6 } {
        return -code error "Valid R_range from 20 to 2M. Exponent value should be in range 1 to 6"
    }
    varDoSet /${ins}/exponent -v $val -m ""
    varTestAlert /${ins}/exponent $val
    varDoSet /${ins}/R_range -v $val 
    set DRR(prevV) {}
    set DRR(logV) {}
    set DRR(prevExp) $val
    set DRR(unstable) 0
    catch { varRead /${ins}/temperature }
}

proc DRR_exponent_r { ins } { return }

proc DRR_offset_v_w { ins val } {
    if { abs($val) > 0.1 } {
        return -code error "Value $val is unreasonably large"
    }
    varDoSet /${ins}/offset_V -v $val
    catch { varRead /${ins}/temperature }
}


# spl files will probably not be used, but they could be used for the raw calibration
# tables as printed: columns are T and R

proc DRR_cal_spl { R } {
    return [interpolate_curve $R]
}

# dat files make more sense, and are the usual for resistance thermometers.
# columns are T and log R

proc DRR_cal_dat { R } {
    return [interpolate_curve [expr { log10($R) }]]
}


# interpolate_curve: 
#
# This is used when a calibration is loaded as a table.
# It takes the ohms (or whatever) reading and returns temperature
#
# Curves are lists of pairs saved in global DRR(caltable), and pairs are
# lists of two elements: converted read value (ohms) and temperature (K).
# Assumption: curves are monotonic; this is enforced when the curve is read in.
# Even if they monotonically decrease in the curve file, they are stored in the
# DRR(caltable) list monotonically increasing.
# global DRR(prevint) holds the four values (two pairs) used for the previous
# interpolation; used as a cache in case the current reading falls in the same
# interval, so avoiding a search through the full table.

proc interpolate_curve { x } {
    global DRR
    set curve $DRR(caltable)
    set prev $DRR(prevint)
    set Y2 -1
    if { [llength $prev] == 4 } {
        # try between previous points in curve
        set X1 [lindex $prev 0]
        set X2 [lindex $prev 2]
        # interpolate using previous pair, if good
        if { ($x >= $X1) && ($x <= $X2) } {
            set Y1 [lindex $prev 1]
            set Y2 [lindex $prev 3]
        }
    }
    if { $Y2 <= 0.0 } {
        # Not in previous interval.  Check if in entire range. 
        # (vxWorks Tcl does not accept [lindex {...} end] or [foreach {a b} ...])
        set index_last [expr {[llength $curve] - 1}]
        set X1 [lindex [lindex $curve 0] 0]
        set Y1 [lindex [lindex $curve 0] 1]
        set Xn [lindex [lindex $curve $index_last] 0]
        # if x not inside [X1,Xn], return error
        if { $x < $X1 || $Xn < $x } { return -code error "Reading out of range" }
        # Check each interval in curve, and interpolate the proper one.
        foreach pair [lrange $curve 1 end] {
            set X2 [lindex $pair 0]
            set Y2 [lindex $pair 1]
            if { $x <= $X2 } {
                set DRR(prevint) [list $X1 $Y1 $X2 $Y2]
                break
            } else {
                set X1 $X2
                set Y1 $Y2
            }
	}
    }
    return [expr { ( $Y2*($x-$X1) + $Y1*($X2-$x) ) / ($X2-$X1) } ]
}

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

CAMP_STRING /~/setup/sensor -D -S -R -P -T "Sensor ID" \
    -H "Enter the sensor's ID number.  This should load the appropriate conversion function or curve, if known." \
    -d on -s on -r off -p on -p_int 1 -v "2775" \
    -writeProc DRR_sensor_w -readProc DRR_sensor_r

# readproc is just for a one-shot poll
proc DRR_sensor_r { ins } {
    varDoSet /${ins}/setup/sensor -p off
    set s [varGetVal /${ins}/setup/sensor]
    if { [string length $s] } {
        DRR_sensor_w $ins $s
    }
}

# Set sensor ID including loading calibration information.

proc DRR_sensor_w { ins sens } {
    global DRR
    set buf ""
    foreach ext {fun spl dat} {
        if { ! [catch { DRR_read_file "./dat/${sens}.${ext}" } buf] } {
            break
        }
    }
    if { [string length $buf] < 5 } {
        return -code error "Could not read calibration for sensor $sens"
    }
    set DRR(calfunc) ""
    switch $ext {
        fun {
            proc DRR_cal_function { R } "$buf"
            set DRR(calfunc) "DRR_cal_function"
        }
        spl {
            set DRR(caltable) [DRR_parse_table $buf]
            set DRR(calfunc) "DRR_cal_spl"
        }
        dat {
            set DRR(caltable) [DRR_parse_table $buf]
            set DRR(calfunc) "DRR_cal_dat"
        }
    }
    set DRR(sensor) $sens
    set DRR(caltype) $ext
    varDoSet /${ins}/setup/sensor -v $sens
    set DRR(prevV) {}
}

# Read specified file.  Return contents on success, return error on failure.
proc DRR_read_file { file } {
    set buf ""
    set fh [open $file r]
    catch {set buf [read $fh]}
    close $fh
    if { [string length $buf] < 5 } { return -code error "No data" }
    return $buf
}

proc DRR_parse_table { buf } {
    set curve [list]
    set reverse 0
    set lines [split $buf "\n"]
    set np1 0
    set np2 0
    foreach line $lines {
        set n [scan $line " %f %f" t v]
        if { $n == 1 } {
            set np1 $t
        } elseif { $n == 2 } {
            if { [incr np2] == 1 } {
                set curve [list [list $v $t]]
                set max $v
                set min $v
            } else {
                if { $v > $max } {
                    lappend curve [list $v $t]
                } elseif { $v < $min } {
                    set curve [linsert $curve 0 [list $v $t]]
                } else {
                    return -code error "Calibration curve is disordered"
                }
            }
        }
    }
    if { $np1 > 0 && $np1 != $np2 } {
        return -code error "Number of points was wrong in calibration file ($np1:$np2)"
    }
    return $curve
}


CAMP_STRING /~/setup/voltage_var -D -S -T "Voltage variable" \
    -H "Enter the Camp variable path for the voltage reading" \
    -d on -s on -writeProc DRR_varpath_w

#  Unless the string is blank, test if the specified camp var exists and is numeric before applying it.
proc DRR_varpath_w { ins cvar } {
    global DRR CAMP_VAR_TYPE_NUMERIC 
    set cvar [string trim [join [split $cvar " \t\n\r(\[\{\]\})"]] " /"]
    if { [string length $cvar] } {
        set cvar "/$cvar"
        set val [varGetVal $cvar]
        if { ([varGetVarType $cvar]&$CAMP_VAR_TYPE_NUMERIC) == 0 || [scan $val {%f %c} vv cc] != 1 } {
            return -code error "Camp variable $cvar is not numeric"
        }
    }
    varDoSet /${ins}/setup/voltage_var -v $cvar
    set DRR(prevV) {}
}

proc DRR_reset_Vlog { e } {
    global DRR
    set DRR(logV) {}
    set DRR(prevExp) $e
}
