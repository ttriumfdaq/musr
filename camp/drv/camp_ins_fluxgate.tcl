##  FluxGate Magnetometer
#   ---------------------
#   Driver written  May 1998  SD
#   Update Oct 2002, DA.
#   See end of file for instruction manual
#
# Due to the peculiar labelling of this unit, have called the channels
# A B and C from left to right looking at the unit. The data is readout
# in this order, and internally A,B,C correspond to 1,2,3. I have called
# then A B and C to avoid confusion. 
#


CAMP_INSTRUMENT /~ -D -T "Fluxgate Magnetometer" \
    -H "Fluxgate Magnetometer" -d on \
    -initProc Fluxgate_init \
    -deleteProc Fluxgate_delete \
    -onlineProc Fluxgate_online \
    -offlineProc Fluxgate_offline
  proc Fluxgate_init { ins } {
	insSet /${ins} -if rs232 0.1 2 /tyCo/2 9600 8 none 1 LF LF
  }
  proc Fluxgate_delete { ins } {
	insSet /${ins} -line off
  }
  proc Fluxgate_online { ins } {
	insIfOn /${ins}
	catch { varRead /${ins}/setup/id }
	if { [varGetVal /${ins}/setup/id] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
        }
  }
  proc Fluxgate_offline { ins } {
	insIfOff /${ins}
  }



#   ***************   Channel A  **********************

    CAMP_FLOAT /~/A_Flux_read -D -R -P -L -A -T "Read Field A" \
        -d on -p on -p_int 12 -r on -tol 50.0 \
        -units  "mG" \
        -H "Readback Channel A Flux in milligauss" \
    	-readProc fg_read_all

    CAMP_INT /~/A_Flux_set -D -S -T "Set Field A" \
        -d on -s on \
        -units  "mG" \
        -H "Setpoint (in mG) to control channel A output" \
        -writeProc {fg_Flux_w A 1}
        
    CAMP_INT /~/A_dac -D -S -R -P -L -T "Raw Output A" \
        -d on -s on -r on \
        -H "Set or Read the DAC for channel A (setting this disables feedback control)" \
        -units "(DAC)" \
    	-readProc fg_read_all -writeProc {fg_dac_w A 1}
         
#   ***************   Channel B  **********************

    CAMP_FLOAT /~/B_Flux_read -D -R -P -L -A -T "Read Field B" \
        -d on -p off -r on -tol 50.0 \
        -units  "mG" \
        -H "Readback Channel B Flux in milligauss" \
    	-readProc fg_read_all

    CAMP_INT /~/B_Flux_set -D -S -T "Set Field B" \
        -d on -s on \
        -units  "mG" \
        -H "Setpoint (in mG) to control channel B output" \
        -writeProc {fg_Flux_w B 2}
         
    CAMP_INT /~/B_dac -D -S -R -P -L -T "Raw Output B" \
        -d on -s on -r on \
        -H "Set or Read the DAC for channel B (setting this disables feedback control)" \
        -units "(DAC)" \
    	-readProc fg_read_all -writeProc {fg_dac_w B 2}

#   ***************   Channel C  **********************

    CAMP_FLOAT /~/C_Flux_read -D -R -P -L -A -T "Read Field C" \
        -d on -p off -r on -tol 50.0 \
        -units  "mG" \
        -H "Readback Channel C Flux in milligauss" \
    	-readProc fg_read_all

    CAMP_INT /~/C_Flux_set -D -S -T "Set Field C" \
        -d on -s on \
        -units  "mG" \
        -H "Setpoint (in mG) to control channel C output" \
        -writeProc {fg_Flux_w C 3}
          
    CAMP_INT /~/C_dac -D -S -R -P -L -T "Raw Output C" \
        -d on -s on -r on \
        -H "Set or Read the DAC for channel C (setting this disables feedback control)" \
        -units "(DAC)" \
    	-readProc fg_read_all -writeProc {fg_dac_w C 3}


#      *************   All Channels   *****************

    proc fg_dac_w { chan chan_num ins target } {
         insIfWrite /${ins} [format "%-2s %-2s %-2s %+5.4d" S $chan_num V $target]
	 varDoSet /${ins}/${chan}_dac -v $target
      }

    proc fg_Flux_w { chan chan_num ins target } {
         insIfWrite /${ins} [format "%-2s %-2s %-2s %+5.4d" S $chan_num F $target]
         fg_read_all $ins
      }

    proc fg_read_all { ins } {
         set status 0
         set buf {}
         if { [catch {insIfRead /${ins} "?" 80} buf] == 0 } {
              set status [scan $buf \
                      " %s %d %d %d %s %d %d %d %s %d %d %d  "\
                        uA sA fA vA uB sB fB vB uC sC fC vC ]
          }
          if { $status != 12 } {
              return -code error "Unintelligible readback: $buf"
          }

          foreach c [list A B C] {
              varDoSet /${ins}/${c}_Flux_read -v [set f$c]
              varDoSet /${ins}/${c}_dac -v [set v$c]
              if { [set u$c] == "F" } {
                  varDoSet /${ins}/${c}_Flux_set -v [set s$c] -units "mG" \
                          -m "Setpoint (in mG) controlling channel $c"
                  varTestAlert /${ins}/${c}_Flux_read [set s$c]
              } else {
                  varDoSet /${ins}/${c}_Flux_set -units "manual" \
                          -m "Setpoint (in mG) to begin controlling channel $c"
              }
          }
      }

#        *************   Setup   *****************

    CAMP_STRUCT /~/setup -D -d on
	CAMP_SELECT /~/setup/id -D -R -T "Query" -d on -r on \
            -v 0 -selections BAD "0897 FluxGate" \
	    -readProc Fluxgate_id_r
          proc Fluxgate_id_r { ins } {
		set str 0
                varDoSet /${ins}/setup/id -v "BAD" 
		set status [catch {insIfRead /${ins} "?" 32} buf]
		if { $status != 0 } { return -code error "error reading Fluxgate module " }
#		(match either F or V ; scan should make 1 conversion) 
                set status [scan $buf { %[FV] } str]
                if { $status != 1 } { return -code error "unexpected value read " }

                varDoSet /${ins}/setup/id -v 1
	  }

#########################################################################
#   Module Instruction Manual:
#   -------------------------
#   This "manual" written by SD.
#
#   Module built by  W. Roberts.
#
#   RS232  9600 baud 8 bits No parity Read term LF Write term LF
# 
#   To read instrument:
#   ------------------
#   send the following string (without the quotes and <LF> is a linefeed):
#   "?<LF>"
#
#   Instrument replies with a string of the form
#   "V +0010 +0066 +0010 F +0100 -0155 -0419 V +0012 -0235 +0012"
#
#   i.e. 4 parameters per channel, in order channel A, B, C
#   Param 1 : a character            V or F   set point is in dac or mG (Flux)
#   Param 2 : signed 4 digit integer          set point value
#   Param 3 : signed 4 digit integer          flux reading (mG)
#   Param 4 : signed 4 digit integer          voltage reading (V)
#
#   To change parameters:
#   ---------------------
#   send a string of the form (without the quotes and <LF> is a linefeed) :
#   "S 1 F +0032<LF>"
#
#   i.e. 4 parameters,
#   Param 1 : 1 character           S        UPPER CASE
#   Param 2 : 1 character        1,2 or 3    channel no. (for A,B or C)
#   Param 3 : 1 character          F or V    UPPER CASE desired units  (V=volt or F=mG)
#   Param 4 : signed 4 digit integer         set point in desired units
#   
#   The parameters must be separated by a space. Upper Case only.
#########################################################################
