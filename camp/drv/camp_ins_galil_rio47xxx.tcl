# CAMP driver for Galil RIO 47xxx programmable I/O controller.
#
# This may be later revised to split each feature into a separate 
# Camp device, and run the communication as a specific camp "interface".
#
# Incomplete coverage of features.  Only first 4 channels used.
# No differential inputs.  No digital IO. No dac_incr or ramp.
# No conversion of AIN readings.

CAMP_INSTRUMENT /~ -D -T "Galil RIO" -d on \
    -H "Galil RIO 47xxx programmable I/O controller portable PLC" \
    -initProc rio_init -deleteProc rio_delete \
    -onlineProc rio_online -offlineProc rio_offline

proc rio_init { ins } {
    insSet /${ins} -if tcpip 0.05 1 142.90.126.151 1047 CR CR
}

proc rio_delete { ins } { insSet /${ins} -line off }

proc rio_online { ins } {
    insIfOn /${ins}
    if { [catch {varRead /${ins}/setup/id} msg] } {
        insIfOff /${ins}
        return -code error $msg
    }
    # Further initialization:
    varDoSet /${ins}/setup/id -p on -p_int 0.5
}

proc rio_offline { ins } {
    insIfOff /${ins}
}

for {set _i 0} {$_i<4} {incr _i} {

CAMP_FLOAT /~/output$_i -D -S -R -L -T "Voltage output $_i" \
    -d on -s on -r on -units "V" \
    -readProc [list rio_aout_r $_i] -writeProc [list rio_aout_w $_i]

}

proc rio_aout_r { io ins } {
    insIfReadVerify /$ins "MG@AO\[$io\]" 16 /${ins}/output$io " %f" 1
}

proc rio_aout_w { io ins set } {
    set ri [varGetVal /${ins}/setup/AOutputs/range$io]
    set min [lindex {-99 0.0 0.0 -5.0 -10.0} $ri]
    set max [lindex {99 5.0 10.0 5.0 10.0} $ri]
    if { $set < $min || $set > $max } {
        return -code error "Setting out of range ($min,$max)"
    }
    insIfWriteVerify /$ins "AO $io,$set" "MG@AO\[$io\]" 24 /${ins}/output$io " %f" 2 $set 0.002
}


for {set _i 0} {$_i<4} {incr _i} {

CAMP_FLOAT /~/input$_i -D -R -P -L -T "Voltage input $_i" \
    -d on -r on -units "V" \
    -readProc [list rio_ainput_r $_i] 
}

proc rio_ainput_r { io ins } {
    insIfReadVerify /$ins "MG AV\[$io\]" 16 /${ins}/input$io " %f" 1
}


# setup:   structure for holding setup parameters which follow

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

# id reads identification directly, and recovers more when polled (once)

CAMP_STRING /~/setup/id -D -R -P -T "ID query" -d on -r on -p off \
    -v "" -H "Model identification" -readProc rio_id_r

proc rio_id_r { ins } {
    global CAMP_VAR_ATTR_POLL
    set id ""
    if { [catch { insIfRead /$ins "\022\026" 80 } id] } {
        if { [catch { insIfRead /$ins "\022\026" 80 } id] } {
            return -code error "Failed identity check: $id"
    }   }
    varDoSet /${ins}/setup/id -v [string trim $id]
    if { [varGetStatus /${ins}/setup/id] & $CAMP_VAR_ATTR_POLL } { # polled: do init
        for { set io 0 } { $io < 4 } { incr io } {
            if { [varGetVal /${ins}/setup/AOutputs/range$io] > 0 } { # recovery: apply my ranges
                varSet /${ins}/setup/AOutputs/range$io -v [varGetVal /${ins}/setup/AOutputs/range$io]
                varSet /${ins}/setup/AInputs/in${io}/range -v [varGetVal /${ins}/setup/AInputs/in${io}/range]
            } else { # new instrument: read ranges from rio
                varRead /${ins}/setup/AOutputs/range$io
                varRead /${ins}/output$io
                varRead /${ins}/setup/AInputs/in${io}/range
            }
        }
        varDoSet /${ins}/setup/id -p off
    }
}

CAMP_STRUCT /~/setup/AOutputs -D -T "Setup Analog Outputs" -d on

for {set _i 0} {$_i<4} {incr _i} {

# Note space at beginning of each selection to hide "-", or else value
# is interpreted as a bad option.

CAMP_SELECT /~/setup/AOutputs/range$_i -D -S -R -L -T "Output $_i range" \
    -selections " " " 0 to 5V" " 0 to 10V" " -5 to 5V" " -10 to 10V" \
    -d on -s on -r on -v 0 \
    -readProc [list rio_aoutrange_r $_i] -writeProc [list rio_aoutrange_w $_i]

}

# Yes, the range settings are indexed differently for Analog In and Out.

proc rio_aoutrange_r { io ins } {
    set buf [insIfRead /$ins "MG _DQ$io" 16]
    if { [scan $buf " %f %c" r x] == 1 } {
        varDoSet /${ins}/setup/AOutputs/range$io -v [expr {round($r)}]
    }
}

proc rio_aoutrange_w { io ins range } {
    if { $range == 0 } { return }
    insIfWrite /$ins "DQ $io,$range"
    varDoSet /${ins}/setup/AOutputs/range$io -v $range
    varSet /${ins}/output$io -v [varGetVal /${ins}/output$io]
}

CAMP_STRUCT /~/setup/AInputs -D -T "Setup Analog Inputs" -d on

for {set _i 0} {$_i<4} {incr _i} {

CAMP_STRUCT /~/setup/AInputs/in$_i -D -T "Setup Analog Input $_i" -d on

# Note space at beginning of each selection, to hide "-"
CAMP_SELECT /~/setup/AInputs/in${_i}/range -D -S -R -L -T "Input $_i range" \
    -selections " " " -5 to 5V" " -10 to 10V" " 0 to 5V" " 0 to 10V" \
    -d on -s on -r on -v 0 \
    -readProc [list rio_ainrange_r $_i] -writeProc [list rio_ainrange_w $_i]

CAMP_INT /~/setup/AInputs/in${_i}/F_num -D -S -R -L -T "In $_i filter number" \
    -H "Filtering number for Analog Input ${_i}" \
    -d on -s on -r on -units "" \
    -readProc [list rio_ainnum_r $_i] -writeProc [list rio_ainnum_w $_i]

CAMP_FLOAT /~/setup/AInputs/in${_i}/window -D -S -R -L -T "In $_i filter window" \
    -H "Filtering window for Analog Input ${_i}" \
    -d on -s on -r on -units "V" \
    -readProc [list rio_ainwin_r $_i] -writeProc [list rio_ainwin_w $_i]

}

proc rio_ainrange_r { io ins } {
    set buf [insIfRead /$ins "MG _AQ$io" 16]
    if { [scan $buf " %f %c" r x] == 1 } {
        varDoSet /${ins}/setup/AInputs/in${io}/range -v [expr {round($r)}]
    }
}

proc rio_ainrange_w { io ins range } {
    if { $range == 0 } { return }
    insIfWrite /$ins "AQ $io,$range"
    varDoSet /${ins}/setup/AInputs/in${io}/range -v $range
}

proc rio_ainnum_r { io ins } {
    set buf [insIfRead /$ins "MG F\[$io\]" 16]
    if { [scan $buf " %f %c" n x] == 1 } {
        varDoSet /${ins}/setup/AInputs/in${io}/F_num -v [expr {round($n)}]
    }
}

proc rio_ainnum_w { io ins num } {
    if { $num < 1 } { set num 1 }
    insIfWrite /$ins "F\[$io\]=$num"
    varDoSet /${ins}/setup/AInputs/in${io}/F_num -v $num
}

proc rio_ainwin_r { io ins } {
    set buf [insIfRead /$ins "MG W\[$io\]" 16]
    if { [scan $buf " %f %c" w x] == 1 } {
        varDoSet /${ins}/setup/AInputs/in${io}/window -v $w
    }
}

proc rio_ainwin_w { io ins win } {
    if { $win < 0 } { set win 0.0 }
    insIfWrite /$ins "W\[$io\]=$win"
    varDoSet /${ins}/setup/AInputs/in${io}/window -v $win
}


