CAMP_INSTRUMENT /~ -D -T "Group3 DTM-141" -d on \
    -initProc dtm141_init \
    -deleteProc dtm141_delete \
    -onlineProc dtm141_online \
    -offlineProc dtm141_offline
    proc dtm141_init { ins } {
	insSet /${ins} -if rs232 0.3 2 none 9600 8 none 1 CRLF none
    }
    proc dtm141_delete { ins } {
	insSet /${ins} -line off
    }
    proc dtm141_online { ins } {
	insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
        # set units to Gauss
    	varSet /${ins}/units -v 0  
    	# insIfWrite /${ins} "BCAMPING"
    }
    proc dtm141_offline { ins } { insIfOff /${ins} }


    CAMP_FLOAT /~/field -D -R -P -L -T "Field reading" -d on -r on \
	-readProc dtm141_field_r 
      proc dtm141_field_r { ins } {
            # Do not use insIfReadVerify because manual scan lets us read 
	    # both the field and the units.  (The "insIfReadVerify error: "
	    # bug has been fixed.)
	    set buf [insIfRead /${ins} "F" 32]
	    set status [scan $buf " %f%s" val uni]
	    if { $status != 2 } {
		set buf [insIfRead /${ins} "F" 32]
		set status [scan $buf " %f%s" val uni]
	    }
#	    Possible response "OVER RANGE"
	    if { $status != 2 } { return -code error "$buf" }
	    set pol [varGetVal /${ins}/setup/polarity]
	    set val [expr { ($pol ? -$val : $val ) }]
	    varDoSet /${ins}/field -v $val -units $uni
	}
    CAMP_FLOAT /~/temp -D -R -P -L -T "Temperature reading" -d on -r on \
	-units C  -readProc dtm141_temp_r 
      proc dtm141_temp_r { ins } {
	    insIfReadVerify /${ins} "T" 32 /${ins}/temp " %fC" 2
	}
    CAMP_SELECT /~/units -D -S -T "Field units" -d on -s on \
    	-selections G T \
    	-writeProc dtm141_units_w
      proc dtm141_units_w { ins target } { 
            set units $target
    	    switch $units {
    		0 { insIfWrite /${ins} "UFG" }
    		1 { insIfWrite /${ins} "UFT" }
    	    }
    	    varDoSet /${ins}/units -v $units
	} 
    CAMP_SELECT /~/range -D -S -R -P -T "Field range" -d on -s on -r on \
    	-selections 0.3T 0.6T 1.2T 3.0T \
        -readProc dtm141_range_r -writeProc dtm141_range_w
      proc dtm141_range_w { ins target } {
    	    insIfWrite /${ins} "R$target"
            varRead /${ins}/range
	}
      proc dtm141_range_r { ins } {
            insIfReadVerify /${ins} "IR" 32 /${ins}/range " %d" 2
        }
    CAMP_SELECT /~/acdc -D -S -T "Field measure mode" -d on -s on \
    	-selections AC DC -v 1 -writeProc dtm141_acdc_w
      proc dtm141_acdc_w { ins target } {
    	    switch $target {
    		0 { insIfWrite /${ins} "GA" }
    		1 { insIfWrite /${ins} "GD" }
    	    }
    	    varDoSet /${ins}/acdc -v $target
	} 


    CAMP_STRUCT /~/filter -D -T "Filtering" -d on
          CAMP_SELECT /~/filter/dfilt -D -S -R -T "Digital filtering" \
            -d on -s on -r on -selections OFF ON \
            -readProc dtm141_dfilt_r -writeProc dtm141_dfilt_w
            proc dtm141_dfilt_w { ins target } {
    	       insIfWrite /${ins} "D$target"
               varRead /${ins}/filter/dfilt
	    }
            proc dtm141_dfilt_r { ins } {
               insIfReadVerify /${ins} "ID" 32 /${ins}/filter/dfilt " %d" 2
            }


          CAMP_FLOAT /~/filter/window -D -R -S -L -T "Filter window" \
	    -d on -r on -s on -units G \
            -H "Filter only when measurements are within this window" \
            -readProc group3_window_r -writeProc group3_window_w
            proc group3_window_r { ins } {
	       insIfReadVerify /${ins} "IY" 32 /${ins}/filter/window " %f" 2
            }
            proc group3_window_w { ins target } {
               set window $target
	       if { $window < 0.0 } { set window 0.0 }
	       if { $window > 65534.0 } { set window 65534.0 }
	       set target [format "%7.1f" $window]
               insIfWriteVerify /${ins} "Y$target" "IY" 32 \
                    /${ins}/filter/window  " %f" 2  $target 
            }

         CAMP_FLOAT /~/filter/filtfact -D -R -S -L -T "Filter factor" \
	   -d on -r on -s on \
	   -H "Digital filter strength (41 gives 4 sec)" \
           -readProc group3_filtfact_r -writeProc group3_filtfact_w
           proc group3_filtfact_r { ins } {
	      insIfReadVerify /${ins} "IJ" 32 /${ins}/filter/filtfact " %f" 2
            }
           proc group3_filtfact_w { ins target } {
               set filtfact $target
	       if { $filtfact < 0.0 } { set filtfact 0.0 }
	       if { $filtfact > 65534.0 } { set filtfact 65534.0 }
	       set target [format "%7.1f" $filtfact]
               insIfWriteVerify /${ins} "J$target" "IJ" 32 \
		 /${ins}/filter/filtfact  " %f" 2  $target 
            }


    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE \
	    -readProc dtm141_id_r
          proc dtm141_id_r { ins } {
		set id 0
		set status [catch {insIfRead /${ins} "F" 32} buf]
		if { $status == 0 } {
		    set id [scan $buf " %fG" val]
		    if { $id != 1 } { set id  [string match " OVER RANG*" $buf]}
		    if { $id != 1 } { set id 0 }
		}
		varDoSet /${ins}/setup/id -v $id
	    }

	CAMP_SELECT /~/setup/polarity -D -S -T "Polarity" -d on -s on \
	    -selections NORMAL REVERSE -v 0 \
	    -writeProc dtm141_polarity_w
          proc dtm141_polarity_w { ins target } {
		varDoSet /${ins}/setup/polarity -v $target
	    }
