# camp_ins_hp34401a.tcl: 
# Camp driver for the Hewlett-Packard 34401a multimeter.
#
# Donald Arseneau      15-Nov-2015 


CAMP_INSTRUMENT /~ -D -T "HP 34401a multimeter" \
    -H "Hewlett-Packard 34401a multimeter" -d on \
    -initProc HP34401a_init \
    -deleteProc HP34401a_delete \
    -onlineProc HP34401a_online \
    -offlineProc HP34401a_offline
proc HP34401a_init { ins } {
#   insSet /${ins} -if rs232 0.5 /tyCo/2 9600 8 none 2 CRLF LF 10
    insSet /${ins} -if gpib 0.1 2.0 4 LF LF
}
proc HP34401a_delete { ins } {
    insSet /${ins} -line off
}
proc HP34401a_online { ins } {
    global HPvm
    insIfOn /${ins}
    #  Record interface type: rs232 or gpib
    set type [insGetIfTypeIdent /${ins}]
    set HPvm(${ins},itype) $type
    set st [catch {
	switch -glob $type {
	    rs232 {
		insIfWrite /${ins} "SYSTEM:REMOTE"
		set HPvm(${ins},clear) "\[ insIfWrite /${ins} \"\03\" \]"
		expr $HPvm(${ins},clear)
	    }
	    gpib* {
		set ad [insGetIfGpibAddr /${ins}]
		set HPvm(${ins},clear) "\[ gpibClear $type $ad \]"
		expr $HPvm(${ins},clear)
	    }
	    default { return -code error "Did not expect interface type ${type}"}
	}
    }]
    varRead /${ins}/setup/id
    if { $st != 0 || [varGetVal /${ins}/setup/id] == 0 } {
	insIfOff /${ins}
	return -code error "failed ID query, check interface definition and connections"
    }
    if { [set st [varGetVal /${ins}/setup/function]] == 0 } {
	catch { varRead /${ins}/setup/function }
	set st [varGetVal /${ins}/setup/function]
    }
    if { $st > 0 } { 
	varSet /${ins}/setup/function -v $st 
    }
}
proc HP34401a_offline { ins } {
    insIfOff /${ins}
}

CAMP_FLOAT /~/reading -D -d on -R -r off -P -L -A \
   -T "reading" -units "function?" \
   -H "Instrument Reading, of selected function and units." \
   -readProc HP34401a_reading_r
proc HP34401a_reading_r { ins } {
    HP34401a_checktype $ins 
    set code [catch { insIfReadVerify /${ins} "READ?" 80 /${ins}/reading " %g" 2 } ]
    if { $code } { 
	set err [HP34401a_err_mes ${ins}]
	return -code error $err
    }
}
# The HP34401a meter does not return error codes or messages unless prompted,
# but will queue error messages waiting for retrieval.  HP34401a_err_mes asks 
# for the messages until it gets the most recent.
proc HP34401a_err_mes { ins } {
    set err "No response"
    while { ([scan [set em [insIfRead /${ins} {:SYST:ERR?} 80] ] " %g" ec] == 1) ? 0 : $ec } {
	set err $em
    }
    return $err
}
# HP34401a_checktype just generates an error message if a measurement type has not 
# been selected.
proc HP34401a_checktype { ins } {
    if { [varGetVal /${ins}/setup/function] < 1 } {
	return -code error "You must choose a measurement type first"
    }
}
# Cancel an infinite trigger.  Uses Gpib clear for gpib or control-C for RS232
proc HP34401a_cancel { ins } {
    global HPvm
    expr $HPvm(${ins},clear)
}

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc HP34401a_id_r
proc HP34401a_id_r {ins} {
    set id 0
    if { [catch {
        insIfWrite /$ins "*CLS"
        insIfRead /${ins} "*IDN?" 80
    } buf] == 0} {
	set id [ expr [scan $buf " HEWLETT-PACKARD,34401A,%d," val] == 1 ]
    }
    if { $id } {
	set id [ expr [string first "\r" $buf ] < 0 ]
    }
    varDoSet /${ins}/setup/id -v $id
}

CAMP_SELECT /~/setup/function -D -S -R \
    -d on -s on -r on \
    -H "Select the measurement function, the type of reading." \
    -selections NONE Volt_DC Volt_AC Curr_DC Curr_AC Resist Res_4_wire Freq \
    -writeProc HP34401a_function_w -readProc HP34401a_function_r

proc HP34401a_function_w { ins val } {
    global HPvm
    #
    HP34401a_cancel $ins
    varDoSet /${ins}/setup/function -v $val
    switch $val {
	0 {# NONE
	    varDoSet /${ins}/reading -units "function?" -r off
	    set HPvm($ins,type) ""
	}
	1 {# Voltage_DC
	    insIfWrite /${ins} "SENSE:FUNC \"VOLT:DC\""
	    insIfWrite /${ins} "CONF:VOLT:DC DEF"
	    varDoSet /${ins}/reading -units "V" -r on -z
            set HPvm($ins,type) "VOLT:DC"
	}
	2 {# Voltage_AC
	    insIfWrite /${ins} "SENSE:FUNC \"VOLT:AC\""
	    insIfWrite /${ins} "CONF:VOLT:AC DEF"
	    varDoSet /${ins}/reading -units "V ac" -r on -z
            set HPvm($ins,type) "VOLT:AC"
	}
	3 {# Current_DC
	    insIfWrite /${ins} "SENSE:FUNC \"CURR:DC\""
	    insIfWrite /${ins} "CONF:CURR:DC DEF"
	    varDoSet /${ins}/reading -units "A" -r on -z
            set HPvm($ins,type) "CURR:DC"
	}
	4 {# Current_AC
	    insIfWrite /${ins} "SENSE:FUNC \"CURR:AC\""
	    insIfWrite /${ins} "CONF:CURR:AC DEF"
	    varDoSet /${ins}/reading -units "A ac" -r on -z
            set HPvm($ins,type) "CURR:AC"
	}
	5 {# Resistance
	    insIfWrite /${ins} "SENSE:FUNC \"RES\""
	    insIfWrite /${ins} "CONF:RES DEF"
	    varDoSet /${ins}/reading -units "Ohm" -r on -z
            set HPvm($ins,type) "RES"
	}
	6 {# FourWireResistor
	    insIfWrite /${ins} "SENSE:FUNC \"FRES\""
	    insIfWrite /${ins} "CONF:FRES DEF"
	    varDoSet /${ins}/reading -units "Ohm" -r on -z
            set HPvm($ins,type) "FRES"
	}
	7 {# Frequency
	    insIfWrite /${ins} "SENSE:FUNC \"FREQ\""
	    insIfWrite /${ins} "CONF:FREQ DEF"
	    varDoSet /${ins}/reading -units "Hz" -r on -z
            set HPvm($ins,type) "FREQ"
	}
    }
}

proc HP34401a_function_r { ins } {
    HP34401a_cancel $ins
    set buf [string trim [insIfRead /$ins "SENSE:FUNC?" 80]]
    set val [lsearch {{"VOLT"} {"VOLT:AC"} {"CURR"} {"CURR:AC"} {"RES"} {"FRES"} {"FREQ"}} $buf]
    varDoSet /${ins}/setup/function -v [incr val]
    if { $val == 0 } {
	return -code error "Unknown measurement function: $buf"
    }
}

# Note the use of float numbers to avoid confusion when setting
# values based on label: 1 -> 0.2 but 1.0 -> 1.0 
CAMP_SELECT /~/setup/int_time -T "Integration time" \
    -D -S -R -d on -s on -r on -v 4  \
    -selections 0.02 0.2 1.0 10.0 100.0 \
    -H "Measurement integration time in units of 1/60 second (NPLC)" \
    -readProc HP34401a_int_time_r -writeProc HP34401a_int_time_w
#
proc HP34401a_int_time_w { ins val } {
    global HPvm
    HP34401a_checktype $ins 
    HP34401a_cancel $ins
    varDoSet /${ins}/setup/int_time -v $val
    set func [varGetVal /${ins}/setup/function]
    if { [lsearch {1 3 5 6} $func] >= 0 } {
	insIfWrite /${ins} "SENSE:$HPvm($ins,type):NPLC [varSelGetValLabel /${ins}/setup/int_time]"
    }
    set err "[HP34401a_err_mes ${ins}]" 
}
proc HP34401a_int_time_r { ins } {
    global HPvm
    set func [varGetVal /${ins}/setup/function]
    if { [lsearch {1 3 5 6} $func] >= 0 } {
        set itim 2
	scan [insIfRead /${ins} "SENSE:$HPvm($ins,type):NPLC?" 40] " %g" itim
	varDoSet /${ins}/setup/int_time -v $itim
    }
}
