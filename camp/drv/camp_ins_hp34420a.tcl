# camp_ins_hp34420a.tcl: 
# Camp driver for the Hewlett-Packard 34420A nanovoltmeter.
#
# Donald Arseneau      revised     18-Oct-2000
#
# I want to have multiple sensor types, but most of them are disabled.
# There are two reading modes -- triggering on demand and auto triggering.  
# Do I want to support curves in software al-la Solartron?  I think not.
#
# Only a single channel is supported.  All the four-wire sensor types
# use both channels, so it is not worth the vast bookkeeping to use 
# separate channels just for raw voltage or current.
#
# Question: For which sensor types do we want to use low power excitation
# and/or a fixed range?  What was generic FourWireResistor specifically for?
# Question: Should we just drop the PTR and thermocouple functions?
#
# Continuous measurement mode is a means to allow the instrument to
# read continuously without the driver waiting for each response, which
# causes timeouts for precise measurements.  Instead, the meter is 
# told to keep statistics and given an "infinite" trigger.  Some time 
# later, when the `reading' is polled, the measurement is interrupted
# by sending a "clear" signal.  The average reading is read out, and
# the continuous measurement is restarted.  This leads to a lot of 
# finicky management:
#  o the "infinite trigger" must be interrupted before any communication
#    with the meter.
#  o The reading must be only polled, not read directly (so -r off)
#  o The poll interval must be long enough to take the measurement(s)

CAMP_INSTRUMENT /~ -D -T "HP 34420A nanovoltmeter" \
    -H "Hewlett-Packard 34420A nanovolt / microOhm meter" -d on \
    -initProc HP34420A_init \
    -deleteProc HP34420A_delete \
    -onlineProc HP34420A_online \
    -offlineProc HP34420A_offline
proc HP34420A_init { ins } {
# insSet /${ins} -if rs232 0.5 10 /tyCo/2 9600 8 none 2 CRLF CRLF
  insSet /${ins} -if gpib 0.1 10 22 LF LF
}
proc HP34420A_delete { ins } {
  insSet /${ins} -line off
}
proc HP34420A_online { ins } {
  global HPnVm
  insIfOn /${ins}
  #  Record interface type: rs232 or gpib
  set iftype [insGetIfTypeIdent /${ins}]
  set HPnVm(${ins},itype) $iftype
  #  20140408  TW  rearranged this code a little to allow debugging
  set status 0
  set error_message ""
  switch -glob $iftype {
      rs232* {
	  set HPnVm(${ins},clear) "\[ insIfWrite /${ins} \"\03\" \]"
	  set status [catch {expr $HPnVm(${ins},clear)} error_message]
	  if { $status == 0 } {
	      set status [catch {insIfWrite /${ins} "SYSTEM:REMOTE"} error_message]
	  }
      }
      gpib* {
	  set addr [insGetIfGpibAddr /${ins}]
	  set timeout [insGetIfTimeout /${ins}]
	  # note: gpibClear not tied to an instrument, so call gpibTimeout to give it a timeout
	  # note: this timeout does not have the resolution of the read/write timeout
	  set HPnVm(${ins},timeout) "\[ gpibTimeout $iftype $timeout \]"
	  set HPnVm(${ins},clear) "\[ gpibClear $iftype $addr \]"
	  set status [catch {expr $HPnVm(${ins},timeout)} error_message]
	  if { $status == 0 } {
	      set status [catch {expr $HPnVm(${ins},clear)} error_message]
	  }
      }
      default { 
	  set status 1
	  set error_message "Did not expect interface type ${iftype}"
      }
  }
  if { $status == 0 } {
    varRead /${ins}/setup/id
  }
  # puts "HP34420A_online: type:$iftype error_message:'$error_message'"
  if { $status != 0 || [varGetVal /${ins}/setup/id] == 0 } {
    insIfOff /${ins}
    return -code error "$error_message | failed ID query, check interface definition and connections"
  }
  set st [varGetVal /${ins}/setup/sensor_type]
  if { $st > 0 } { varSet /${ins}/setup/sensor_type -v $st }
}
proc HP34420A_offline { ins } {
  insIfOff /${ins}
}

CAMP_FLOAT /~/reading -D -d on -R -r off -P -L -A \
   -T "reading" -units "sensor?" \
   -H "Volts/ohms/etc Reading" \
   -readProc HP34420A_reading_r
proc HP34420A_reading_r { ins } {
    HP34420A_checksensor $ins 
    if { [varGetVal /${ins}/setup/how_read] > 0 } {
	# Average since previous reading; require at least $na readings in ave
        HP34420A_cancel $ins
	set code [catch {insIfRead /${ins} "CALC:AVER:COUNT?" 80} nm ]
	set err "[ HP34420A_err_mes ${ins} ]" 
	if { $code } { return -code error $err }
        # $nm is number of measurements, $na is requested number to average
        set nm [format " %g" $nm]
	set na [varGetVal /${ins}/setup/num_aver]
	if { $nm >= $na } {# Got enough measurements
	    set val 0
	    set code [catch { insIfRead /${ins} "CALC:AVER:AVER?" 80 } val ]
	    # turn off/on statistics to zero them:
	    insIfWrite /${ins} ":CALC:STATE OFF;STATE ON"
	    HP34420A_resume $ins
	    if { $code } { return -code error $err }
	    if { abs( $val ) > 1.11e+11 } { 
		varDoSet /${ins}/reading -v 9.999999e+9 -m "Overload"
	    } else {
		varDoSet /${ins}/reading -v $val -m [format "Reading is average of %.0f measurements." $nm]
	    }
	} else {# Not enough samples since last polled
	    HP34420A_resume $ins
	    # Increase Poll interval to allow enough readings, but don't 
	    # increase to more than 1.25*(poll_interval + 10s) and never
            # more than 100s * requested_num
	    set pi [varGetPollInterval /${ins}/reading]
	    set np [expr (2.0+$na)*$pi/($nm+1.0)]
	    set pi [expr 1.25*($pi + 10.0) ]
            if { $np > $pi } { set np $pi }
            if { $np > 100.0*$na } { set np [expr 100.0*na] }
	    varDoSet /${ins}/reading -p_int $np -m [format "Only %.0f readings; need %d." $nm $na]
	}
    } else {
	# Single reading
	set code [catch { insIfReadVerify /${ins} "READ?" 80 /${ins}/reading " %g" 2 }]
	if { $code } { 
	    set err [HP34420A_err_mes ${ins}]
	    return -code error $err
	}
    }
}
# Cancel an infinite trigger.  Uses Gpib clear for gpib or control-C for RS232
proc HP34420A_cancel { ins } {
    global HPnVm
    expr $HPnVm(${ins},timeout)
    expr $HPnVm(${ins},clear)
}
# Restart the infinite trigger used for continuous read mode.
proc HP34420A_resume { ins } {
    global HPnVm
    if { $HPnVm(${ins},contin) } { insIfWrite /${ins} ":INIT" }
}
# The HP34420A meter does not return error codes or messages unless prompted,
# but will queue error messages waiting for retrieval.  HP34420A_err_mes asks 
# for the messages until it gets the most recent.
proc HP34420A_err_mes { ins } {
    set err "No response"
    while { ([scan [set em [insIfRead /${ins} {:SYST:ERR?} 80] ] " %g" ec] == 1) ? 0 : $ec } {
	set err $em
    }
    return $err
}
# HP34420A_checksensor just generates an error message if a sensor type has not 
# been selected.
proc HP34420A_checksensor { ins } {
    if { [varGetVal /${ins}/setup/sensor_type] < 1 } {
	return -code error "You must choose a sensor type first"
    }
}

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc HP34420A_id_r
  proc HP34420A_id_r {ins} {
      set id 0
      if { [catch {insIfRead /${ins} "*IDN?" 80} buf] == 0} {
	  set id [ expr [scan $buf " HEWLETT-PACKARD,34420A,%d," val] == 1 ]
      }
      if { $id } {
          set id [ expr [string first "\r" $buf ] < 0 ]
      }
      varDoSet /${ins}/setup/id -v $id
  }

CAMP_SELECT /~/setup/sensor_type -D -S \
      -d on -s on \
      -selections NONE Diode FourWireResistor PTR Thermocouple HallProbe V_Raw R_Raw \
      -writeProc HP34420A_sensor_type_w
  proc HP34420A_sensor_type_w { ins val } {
      global HPnVm
      # REMINDER: set readability and units of "reading" and the
      # setability of other setup things based on the selection here.
      #
      # I want to configure a range and resolution for each of the sensor 
      # types, but I have to find out what makes sense first!  The integration
      # time (nplc) is automatically set by setting the resolution.
      #
      HP34420A_cancel $ins
      if { [varGetVal /${ins}/setup/how_read] > 0 } { varSet /${ins}/setup/how_read -v 0 }
      varDoSet /${ins}/setup/sensor_type -v $val
      varDoSet /${ins}/setup/PTR_resist -s off
      varDoSet /${ins}/setup/TC_type -s off
      switch $val {
	0 {# NONE
	    varDoSet /${ins}/reading -units "sensor?" -r off
	  }
	1 {# Diode
	    varDoSet /${ins}/reading -units "V" -r on -z
            set HPnVm($ins,sense) "VOLT"
	  }
	2 {# FourWireResistor
	    varDoSet /${ins}/reading -units "Ohm" -r on -z
            set HPnVm($ins,sense) "FRES"
	  }
	3 {# PTR
	    varDoSet /${ins}/reading -units "K" -r on -z
	    varDoSet /${ins}/setup/PTR_resist -s on
            set HPnVm($ins,sense) "TEMP"
	  }
	4 {# Thermocouple
	    insIfWrite /${ins} "SENSE:FUNC \"TEMP\""
	    set HPnVm(${ins},func) TEMP
	    insIfWrite /${ins} "CONF:TEMP TC,[ varSelGetValLabel /${ins}/setup/TC_type ],1,DEF"
	    insIfWrite /${ins} "UNIT:TEMP K"
	    varDoSet /${ins}/reading -units "K" -r on -z
	    varDoSet /${ins}/setup/TC_type -s on
            set HPnVm($ins,sense) "TEMP"
	  }
	5 {# HallProbe: Four wire RESistor; 
	    # DEFault range, MINimum uncertainty ( = best precision);
	    # using OffsetCOMpensation;
	    # Units Ohms; Read using continuous trigger
	    insIfWrite /${ins} "SENSE:FUNC \"FRES\""
	    set HPnVm(${ins},func) FRES
	    insIfWrite /${ins} "CONF:FRES DEF,MIN"
	    insIfWrite /${ins} "SENSE:FRES:OCOM ON"
	    varDoSet /${ins}/reading -units "Ohm" -r on -z
	    varSet /${ins}/setup/how_read -v "continuously"
            set HPnVm($ins,sense) "FRES"
	  }
	6 {# V_Raw
	    insIfWrite /${ins} "CONF:VOLT:DC "
	    varDoSet /${ins}/reading -units "V" -r on -z
            set HPnVm($ins,sense) "VOLT"
	  }
	7 {# R_Raw
	    insIfWrite /${ins} "SENSE:FUNC \"FRES\""
	    insIfWrite /${ins} "CONF:FRES "
	    varDoSet /${ins}/reading -units "Ohm" -r on -z
            set HPnVm($ins,sense) "FRES"
	  }
      }
  }

CAMP_SELECT /~/setup/TC_type -D -S \
      -d on -s off \
      -selections B E J K R S T \
      -writeProc HP34420A_TC_type_w
  proc HP34420A_TC_type_w { ins val } {
  }

CAMP_FLOAT /~/setup/PTR_resist -D -S \
      -d on -s on -v 100. -units Ohms \
      -writeProc HP34420A_TC_type_w
  proc HP34420A_PTR_resist_w { ins val } {
  }

#  Setting the resolution this way is not useful.
#  It doesn't work when autoranging, and other problems
#
#CAMP_FLOAT /~/setup/resolution -T "Measurement resolution" \
#      -D -S -R -d on -s on -r on \
#      -H "Set desired measurement uncertainty (0 is most precise)" \
#      -readProc HP34420A_resolution_r -writeProc HP34420A_resolution_w
#  proc HP34420A_resolution_w { ins val } {
#    global HPnVm
#    HP34420A_checksensor $ins
#    HP34420A_cancel $ins
#    set mres [format " %g" [ insIfRead /${ins} ":$HPnVm($ins,sense):RES? MAX" 40 ] ]
#    if { $val <= 0.0 } { set val MIN }
#    if { $val >= $mres } { set val MAX }
#    insIfWrite /${ins} ":$HPnVm($ins,sense):RES $val "
#    varRead /${ins}/setup/int_time
#    varRead /${ins}/setup/resolution
#    HP34420A_resume $ins
#  }
#  proc HP34420A_resolution_r { ins } {
#    global HPnVm
#    set res [format " %g" [ insIfRead /${ins} ":$HPnVm($ins,sense):RES?" 40 ] ]
#    varDoSet /${ins}/setup/resolution -v $res
#  }

CAMP_SELECT /~/setup/int_time -T "Integration time" \
      -D -S -R -d on -s on -r on -v 4  \
      -selections 0.02 0.2 1 2 10 20 100 200 \
      -H "Integration time for readings; in 1/60 sec units" \
      -readProc HP34420A_int_time_r -writeProc HP34420A_int_time_w
  proc HP34420A_int_time_w { ins val } {
      global HPnVm
      HP34420A_checksensor $ins 
      HP34420A_cancel $ins
      varDoSet /${ins}/setup/int_time -v $val
      insIfWrite /${ins} ":$HPnVm($ins,sense):NPLC [varSelGetValLabel /${ins}/setup/int_time]"
      set err "[ HP34420A_err_mes ${ins} ]" 
      varRead /${ins}/setup/resolution
      HP34420A_resume $ins
  }
  proc HP34420A_int_time_r { ins } {
      global HPnVm
      set itim [format " %g" [ insIfRead /${ins} ":$HPnVm($ins,sense):NPLC?" 40 ] ]
      varDoSet /${ins}/setup/int_time -v $itim
  }
  
CAMP_SELECT /~/setup/how_read -D -S -d on -s on -v 0 \
      -H "Select read on demand or read continuously" \
      -selections on_demand continuously \
      -writeProc HP34420A_how_read
  proc HP34420A_how_read { ins val } {
      global HPnVm
      HP34420A_checksensor $ins 
      # cancel continuous trigger using "clear" (control-C or Gpib clear)
      HP34420A_cancel $ins
      set HPnVm(${ins},contin) [expr $val > 0 ]
      set pi [varGetPollInterval /${ins}/reading]
      if { $HPnVm(${ins},contin) } {
	  # Set reading by poll only; minimum 10 sec poll interval
	  varDoSet /${ins}/reading -p off
	  if { $pi < 10 } { set pi 10 }
	  varDoSet /${ins}/reading -r off -p on -p_int $pi
	  # Start continuous (INFinite) trigger
	  insIfWrite /${ins} ":TRIG:SOUR IMM;:TRIG:COUNT INF;DEL:AUTO ON"
	  # enable statistics
	  insIfWrite /${ins} ":CALC:FUNC AVER;STATE ON;:DATA:FEED RDG_STORE,\"\""
	  # Start readings (if contin)
	  HP34420A_resume $ins
      } else {
	  # set up single trigger on demand
	  insIfWrite /${ins} ":TRIG:SOUR IMM;COUNT 1;DEL 0;DEL:AUTO OFF;:SAMP:COUNT 1"
	  # turn off averaging
	  insIfWrite /${ins} ":CALC:STATE OFF;:DATA:FEED RDG_STORE,\"CALC\""
	  varDoSet /${ins}/reading -r on -p on -p_int $pi
      }
      varDoSet /${ins}/setup/how_read -v $val
  }

CAMP_INT /~/setup/num_aver -D -S \
      -d on -s on -v 1 \
      -H "For continuous measurement; set Minimum number of readings to average; 0 for read-on-demand" \
      -writeProc HP34420A_num_aver_w
  proc HP34420A_num_aver_w { ins val } {
      set hr [expr ($val>0) ]
      # only apply number if > 0
      if { $hr } { varDoSet /${ins}/setup/num_aver -v $val }
      # but select proper measurement method based on number
      varSet /${ins}/setup/how_read -v $hr
  }
