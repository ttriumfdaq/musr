CAMP_INSTRUMENT /~ -D -T "HP 3458A DVM" \
    -H "Hewlett Packard 3458A Voltmeter" \
    -d on \
    -initProc hp3458a_init -deleteProc hp3458a_delete \
    -onlineProc hp3458a_online -offlineProc hp3458a_offline
  proc hp3458a_init { ins } {
	insSet /${ins} -if gpib 0.2 5 22 CRLF LF
  }
  proc hp3458a_delete { ins } {
	insSet /${ins} -line off
  }
  proc hp3458a_online { ins } {
	insIfOn /${ins}
	set status [catch {varRead /${ins}/setup/id}]
	if { ( $status != 0 ) || ( [varGetVal /${ins}/setup/id] == 0 ) } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
	catch {insIfWrite /${ins} "TRIG AUTO"}
  }
  proc hp3458a_offline { ins } {
        insIfOff /${ins}
  }
    CAMP_FLOAT /~/ohms_read -D -R -P -L -T "Resistance reading" \
	-H "Read Resistance" \
	-d on -r on -units Ohms -readProc hp3458a_ohms_read_r
      proc hp3458a_ohms_read_r { ins } {
	set u x1
	set buf [ insIfRead /${ins} "" 125 ]
	if { [catch {lrange $buf 1 5} vals] == 0 } {
	    # Average 5 readings
	    set val [expr ([join $vals "+"])/5.0]
	    set u x5
	} else { # One reading
	    if { [catch {lrange $buf 1 1} val] != 0 } {
		return -code error "Bad reading"
	    }
	}
	varDoSet /${ins}/ohms_read -v $val -units $u
      }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on \
	-H "Set magnet and power supply parameters"

	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections false true -readProc hp3458a_setup_id_r
          proc hp3458a_setup_id_r { ins } {
	    set id 0
	    catch {insIfRead /${ins} "TRIG HOLD;TARM HOLD;MEM LIFO;NPLC 60;NRDGS 1" $buf 1000 }
	    if { [catch {insIfRead /${ins} "ID?" 30} buf] == 0} {
		set id [ expr [string first "HP3458A" $buf]  != -1 ]
	    }
            varDoSet /${ins}/setup/id -v 1
	  }
