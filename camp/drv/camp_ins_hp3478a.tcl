# camp_ins_hp3478a.tcl: 
# Camp driver for the old Hewlett-Packard 3478a multimeter.
#
# Donald Arseneau      25-Jun-2019


CAMP_INSTRUMENT /~ -D -T "HP 3478a multimeter" \
    -H "Hewlett-Packard 3478a multimeter" -d on \
    -initProc HP3478a_init \
    -deleteProc HP3478a_delete \
    -onlineProc HP3478a_online \
    -offlineProc HP3478a_offline
proc HP3478a_init { ins } {
#   insSet /$ins -if rs232 0.5 3.0 /tyCo/2 9600 8 none 2 CRLF LF
    insSet /$ins -if gpib 0.2 2.0 23 CRLF LF
}
proc HP3478a_delete { ins } {
    insSet /$ins -line off
}
proc HP3478a_online { ins } {
    insIfOn /$ins
    #  Test interface type: only gpib
    set type [insGetIfTypeIdent /$ins]
    switch -glob $type {
        gpib* {
            set ad [insGetIfGpibAddr /$ins]
        }
        default { return -code error "Did not expect interface type ${type}"}
    }
    varDoSet /$ins/setup/id -v 0
    for { set i 0 } { $i < 3 } { incr i } {
        if { [catch {varRead /$ins/setup/id} msg] == 0 } {
            set msg "$i tries"
            if { [varGetVal /$ins/setup/id] == 1 } { break }
            if { $i == 1 } { gpibClear $type $ad ; sleep 0.5 }
            sleep 0.5
        }
    }
    if { [varGetVal /$ins/setup/id] == 0 } {
	insIfOff /$ins
	return -code error "failed ID query, check interface definition and connections ($msg)"
    }
    set st [varGetVal /$ins/function]
    if { $st > 0 } {# Old instrument
	varSet /$ins/function -v $st
        varSet /$ins/range -v [varGetVal /$ins/range]
    }
    catch { varRead /$ins/function }
}
proc HP3478a_offline { ins } {
    insIfOff /$ins
}

CAMP_FLOAT /~/reading -D -d on -R -r off -P -p off -p_int 10 -L \
   -T "reading" -units "function?" \
   -H "Instrument Reading, of selected function and units." \
   -readProc HP3478a_reading_r
proc HP3478a_reading_r { ins } {
    set f [HP3478a_checktype $ins]
    set req [lindex {"" "" "T3" "GET" "T5"} [varGetVal /$ins/setup/triggering]]
    insIfReadVerify /$ins $req 16 /$ins/reading " %g" 2
}
# HP3478a_checktype generates an error message if a measurement type has not 
# been selected, and returns the function number otherwise.
proc HP3478a_checktype { ins } {
    set f [varGetVal /$ins/function]
    if { [varGetVal /$ins/function] < 1 } {
	return -code error "You must choose a measurement type first"
    } else {
        return $f
    }
}

CAMP_SELECT /~/function -D -S -R \
    -d on -s on -r on -v 0 \
    -H "Select the measurement function, the type of reading." \
    -selections NONE Volt_DC Volt_AC Ohm_2_wire Ohm_4_wire Curr_DC Curr_AC \
    -writeProc HP3478a_function_w -readProc HP3478a_status_r

proc HP3478a_function_w { ins val } {
    varDoSet /$ins/function -v $val
    set req "F$val"
    if { $val == 0 } {# NONE
        varDoSet /$ins/reading -units "function?" -r off -p off
        return
    }
    set req "F$val"
    set un [lindex {"" "V" "V" "Ohm" "Ohm" "A" "A" "Ohm"} $val]
    insIfWrite /$ins $req
    varDoSet /$ins/reading -units $un -r on -p on -z
}

CAMP_SELECT /~/range -D -S -R \
    -d on -s on -r on \
    -selections "Auto" "0.03 VDC" "0.3 V,A" "3 V,A" "30 V,Ohm" "300 V,Ohm" "3000 Ohm" "30 kOhm" "300 kOhm" "3 MOhm" "30 MOhm" \
    -writeProc HP3478a_range_w -readProc HP3478a_status_r

proc HP3478a_range_w { ins val } {
    varDoSet /$ins/range -v $val
    if { $val == 0 } {
        set req "RA"
    } else {        
        set req "R[expr $val-3]"
    }
    insIfWrite /$ins $req
}

# Note that we often get a garbage LF byte before any read-back,
# left from a previous <eoi><LF> pair. 
proc HP3478a_status_r { ins } {
    set S [insIfRead /$ins "S" 32]
    set b [insIfRead /$ins "B" 7]
    # clean up residual binary junk
    insIfRead /$ins "S" 32
    set b1 0; set b2 0; set b3 0; set b4 0
    scan $b %c%c%c%c%c b0 b1 b2 b3 b4
    #varDoSet /$ins/function -m "Status bytes $b0 $b1 $b2"
    if { $b0 != 10 } { return }
    set F [expr {($b1>>5)&7}]
    set R [expr {($b1>>2)&7}]
    set P [expr {$b1&3}]
    if { $F == 0 || $R == 0 || $P == 0 } {
        return -code error "Invalid status reading"
    }
    if { $F != [varGetVal /$ins/function] } {
        varSet /$ins/function -v $F
    } else {
        varDoSet /$ins/function -v $F
    }
    set r [varGetVal /$ins/range]
    if { ($b2>>1)&1 } {# Autorange
        set R 0
    } else {
        set off [lindex {0 0 1 3 3 1 1} $F]
        set R [expr {$R + [lindex {0 0 1 3 3 1 1} $F]}]
    }
    varDoSet /$ins/range -v $R
    varDoSet /$ins/setup/precision -v [lindex {0 2 1 0} $P]
    if { $b2 == 0 } { return }
    varDoSet /$ins/setup/auto_zero -v [expr {($b2>>2)&1}]
    set T [expr {($b2&1) ? 0 : ((($b2>>6)&1) ? 1 : 2)}]
    set t [varGetVal /$ins/setup/triggering]
    if { $T == ($t>1 ? 2 : $t) } {
        varDoSet /$ins/setup/triggering -v $T
    } else {
        varSet /$ins/setup/triggering -v $T
    }
}

CAMP_SELECT /~/clear -D -S -T "Clear" -d on -s on \
    -selections "CLEAR" -writeProc HP3478a_clear_w

proc HP3478a_clear_w { ins c } {
    set ad [insGetIfGpibAddr /$ins]
    set type [insGetIfTypeIdent /$ins]
    gpibClear $type $ad
    sleep 0.9
    catch { varRead /$ins/function }
}

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc HP3478a_id_r
proc HP3478a_id_r {ins} {
    set id 0
    if { [catch { insIfRead /$ins "KS" 80 } buf] == 0 } {
        set val -1
	set id [ expr {[scan $buf " %d" val] == 1 && ($val == 0 || $val == 1)} ]
    }
    varDoSet /$ins/setup/id -v $id
}

CAMP_SELECT /~/setup/triggering -D -R -S -T "Triggering" -d on -r on -s on \
    -selections "Continuous" "External" "On-Demand" "Demand Fast" \
    -writeProc  HP3478a_trig_w -readProc HP3478a_status_r

proc HP3478a_trig_w { ins val } {
    set req [lindex {T1 T2 T3 T5} $val]
    insIfWrite /$ins $req
    set mes [lindex {"continuous, internal, auto" "external, physical" "single, on-demand" "single, on-demand, without delay"} $val]
    varDoSet /$ins/setup/triggering -v $val -m "Triggering mode is $mes"
}

CAMP_SELECT /~/setup/auto_zero -D -R -S -T "Auto-zeroing" -d on -r on -s on \
    -selections OFF ON \
    -H "Auto-zeroing should be ON except in unusual circumstances." \
    -writeProc  HP3478a_autozero_w -readProc HP3478a_status_r

proc HP3478a_autozero_w { ins val } {
    insIfWrite /$ins "Z$val"
    varDoSet /$ins/setup/auto_zero -v $val
}

CAMP_SELECT /~/setup/precision -D -R -S -T "Precision" -d on -r on -s on \
    -selections "3.5 digit" "4.5 digit" "5.5 digit" \
    -H "Measurement and display precision, expressed as number of digits" \
    -writeProc  HP3478a_precision_w -readProc HP3478a_status_r

proc HP3478a_precision_w { ins val } {
    insIfWrite /$ins "N[expr $val+3]"
    varDoSet /$ins/setup/precision -v $val
}


