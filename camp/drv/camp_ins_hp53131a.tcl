# Hewlett-Packard 53131A Universal Counter
#

CAMP_INSTRUMENT /~ -D -T "HP 53131A Universal Counter" \
    -H "Hewlett-Packard 53131A Universal Counter" -d on \
    -initProc HP53131A_init \
    -deleteProc HP53131A_delete \
    -onlineProc HP53131A_online \
    -offlineProc HP53131A_offline
proc HP53131A_init { ins } {
  insSet /${ins} -if gpib 0.05 5 3 LF LF
}
proc HP53131A_delete { ins } {
  insSet /${ins} -line off
}
proc HP53131A_online { ins } {
  insIfOn /${ins}
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == 0) } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections"
  }
}
proc HP53131A_offline { ins } {
  insIfOff /${ins}  # mandatory call
}

CAMP_FLOAT /~/reading -D -d on -R -r on -P -L -A \
   -T "reading" -units "func?" \
   -H "Measurement of whatever function is selected" \
   -readProc HP53131A_reading_r
proc HP53131A_reading_r { ins } {
# For an auto-triggering measurement... not set up yet
    insIfReadVerify /$ins {:SENSE:DATA?} 50 /${ins}/reading { %f} 2
}

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc HP53131A_id_r
  proc HP53131A_id_r {ins} {
      set id 0
      if { [catch {insIfRead /${ins} "*IDN?" 80} buf] == 0} {
	  set id [ expr [scan $buf " HEWLETT-PACKARD,53131A,%d," val] == 1 ]
      }
      varDoSet /${ins}/setup/id -v $id
  }

