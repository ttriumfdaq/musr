CAMP_INSTRUMENT /~ -D -T "HP 59303a DAC" -d on \
    -initProc hp59303a_init \
    -deleteProc hp59303a_delete \
    -onlineProc hp59303a_online \
    -offlineProc hp59303a_offline
  proc hp59303a_init { ins } { insSet /${ins} -if gpib 0.5 5 16 LF LF } 
  proc hp59303a_delete { ins } { insSet /${ins} -line off }
  proc hp59303a_online { ins } { 
    insIfOn /${ins} 
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc hp59303a_offline { ins } { insIfOff /${ins} }
    CAMP_INT /~/dac_set -D -S -T "DAC setting" -d on -s on \
	-writeProc hp59303a_dac_set_w
      proc hp59303a_dac_set_w { ins target } {
	    set dac_set [expr { round($target) }]
	    if { ( $dac_set < -999 ) || ( $dac_set > 999 ) } {
		return -code error "dac_set out of range -999<=dac_set<=999"
	    }
            insIfWrite /${ins} $dac_set
	    varDoSet /${ins}/dac_set -v $dac_set
	}
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
        CAMP_SELECT /~/setup/id -D -R -T "ID query" -d on -r on \
            -selections FALSE TRUE \
            -readProc hp59303a_id_r
          proc hp59303a_id_r { ins } {
                # To do: read something back for a check
                varDoSet /${ins}/setup/id -v 1
            }
