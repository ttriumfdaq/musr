# iseg NHQ _0__ series HV DC power supply (NIM module)
#
# This device has a settable delay between characters in the range 0...255 ms,
# and the default setting is 3ms, which ruins ordinary undelayed communication.
# Since Camp has no such facility, it can't communicate even to change the
# parameter.  So use a stand-alone program to command W1 setting 1ms delay,
# which enables ordinary communication.
#
# Camp may be upgraded to provide inter-character delays, but it is doubtful
# that it can work on Vxworks due to coarse-grained delays.
#
# 
CAMP_INSTRUMENT /~ -D -T "iseg NHQ HV Power Supply" \
    -d on \
    -initProc NHQ_init \
    -deleteProc NHQ_delete \
    -onlineProc NHQ_online \
    -offlineProc NHQ_offline

# Replies have CRLF terminators in the middle, so set read_terminator = none
# and use specific character counts to delimit reads.

proc NHQ_init { ins } {
  insSet /${ins} -if rs232 0.1 1 /tyCo/1 9600 8 none 1 none CRLF
}
proc NHQ_delete { ins } {
  insSet /${ins} -line off
}
proc NHQ_online { ins } {
  insIfOn /${ins}
  if { [catch {
        varRead /${ins}/setup/id
        varRead /${ins}/setup/polarity_1
        varRead /${ins}/setup/polarity_2
     }] || ! [string match "NHQ*" [varGetVal /${ins}/setup/id]] 
   } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
   }
}
proc NHQ_offline { ins } {
  insIfOff /${ins}
}

CAMP_FLOAT /~/volt_read_1 -D -d on -R -r on -P -L -A \
    -T "Chan 1 Voltage" -units "V" \
    -readProc {NHQ_volt_read_r 1}

CAMP_INT /~/volt_set_1 -D -d on -R -r on -S -s on -L \
    -T "Chan 1 Set Voltage" -units "V" \
    -readProc {NHQ_volt_set_r 1} -writeProc {NHQ_volt_set_w 1} 

CAMP_FLOAT /~/curr_read_1 -D -d on -R -r on -P -L \
    -T "Chan 1 Current" -units "mA" \
    -readProc {NHQ_curr_read_r 1}

CAMP_SELECT /~/state_1 -D -d on -R -r on -P \
    -T "Chan 1 status" \
    -selections "On" "Off" "On-manual" "Too high" "Inhibited" "Poor quality" "Ramp up" "Ramp down" "Status..." "Current trip" "????" \
    -readProc {NHQ_state_r 1}

CAMP_FLOAT /~/volt_read_2 -D -d on -R -r on -P -L -A \
    -T "Chan 2 Voltage" -units "V" \
    -readProc {NHQ_volt_read_r 2}

CAMP_INT /~/volt_set_2 -D -d on -R -r on -S -s on -L \
    -T "Chan 2 Set Voltage" -units "V" \
    -readProc {NHQ_volt_set_r 2} -writeProc {NHQ_volt_set_w 2} 

CAMP_FLOAT /~/curr_read_2 -D -d on -R -r on -P -L \
    -T "Chan 2 Current" -units "mA" \
    -readProc {NHQ_curr_read_r 2}

CAMP_SELECT /~/state_2 -D -d on -R -r on -P \
    -T "Chan 2 status" \
    -selections "On" "Off" "On-manual" "Too high" "Inhibited" "Poor quality" "Ramp up" "Ramp down" "Status..." "Current trip" "????" \
    -readProc {NHQ_state_r 2}

proc NHQ_volt_read_r { ch ins } {
    insIfReadVerify /$ins "U$ch" 9 /${ins}/volt_read_$ch " U$ch %f" 2
    varTestAlert /${ins}/volt_read_$ch [varGetVal /${ins}/volt_set_$ch]
}

proc NHQ_volt_set_r { ch ins } {
    for {set i 0} {$i < 2} {incr i} {
        set buf [insIfRead /$ins "D$ch" 10]
        if {[scan $buf " D${ch} %d" vr] == 1} {
            set vr [expr { $vr * ([varGetVal /${ins}/setup/polarity_$ch] ? 1 : -1 ) }]
            varDoSet /${ins}/volt_set_$ch -v "$vr"
            return
        }
    }
    return -code error [NHQ_err_msg $buf]
}

# Voltage setting value is normally a positive integer. This driver will
# display the proper sign to match the read-back, and accept values of
# either sign.
proc NHQ_volt_set_w { ch ins val } {
    set vs [format %4.4d [expr {abs($val)}]] 
    for {set i 0} {$i < 2} {incr i} {
        set buf [insIfRead /$ins "D$ch=$vs" 11]
        if {[scan $buf " D${ch}=%d" vr] == 1} {
            # apply correct polarity to value:
            set vr [expr { $vr * ([varGetVal /${ins}/setup/polarity_$ch] ? 1 : -1 ) }]
            varDoSet /${ins}/volt_set_$ch -v "$vr"
            # execute GO command:
            set buf [insIfRead /$ins "G$ch" 12]
            if {[scan $buf " G$ch S${ch}=%s " stat] == 1} {
                NHQ_set_status $ch $ins $stat
                return
            }
        }
    }
    return -code error [NHQ_err_msg $buf]
}

proc NHQ_curr_read_r { ch ins } {
    for {set i 0} {$i < 2} {incr i} {
        set buf [insIfRead /$ins "I$ch" 12]
        if {[scan $buf " I$ch %d%d " m e] == 2} {
            incr e 3 ;# A->mA
            varDoSet /${ins}/curr_read_$ch -v "${m}e${e}"
            return
        }
    }
    return -code error [NHQ_err_msg $buf]
}

proc NHQ_state_r { ch ins } {
    for {set i 0} {$i < 2} {incr i} {
        set buf [insIfRead /$ins "S$ch" 12]
        if {[scan $buf " S${ch} S${ch}=%s " stat] == 1} {
            NHQ_set_status $ch $ins $stat
            return
        }
    }
    return -code error [NHQ_err_msg $buf]
}

proc NHQ_set_status { ch ins stat } {
    set tags [list ON OFF MAN ERR INH QUA L2H H2L LAS TRP]
    set i [lsearch -exact $tags $stat]
    if {$i<0} { set i 10 }
    varDoSet /${ins}/state_$ch -v $i
    if {$i==9} {varDoSet/${ins}/volt_set_$ch -v 0}
}

CAMP_STRUCT /~/setup -D -d on

CAMP_STRING /~/setup/id -D -R -T "ID Query" -d on -r on \
    -readProc NHQ_id_r

CAMP_FLOAT /~/setup/max_volt -D -d on -R -r on \
    -T "Max Voltage" -units "V" \
    -readProc NHQ_id_r

CAMP_FLOAT /~/setup/max_curr -D -d on -R -r on \
    -T "Max Current" -units "mA" \
    -readProc NHQ_id_r

CAMP_SELECT /~/setup/polarity_1 -D -d on -R -r on \
    -T "Ch 1 Polarity" -selections Negative Positive \
    -readProc {NHQ_polarity_r 1}

CAMP_FLOAT /~/setup/volt_limit_1 -D -d on -R -r on \
    -T "Ch 1 Voltage Limit" -units "V" \
    -readProc {NHQ_limit_r volt M 1}

CAMP_FLOAT /~/setup/curr_limit_1 -D -d on -R -r on \
    -T "Ch 1 Current Limit" -units "mA" \
    -H "Chan 1 Current Limit. May also act as a trip level if the Kill switch is set." \
    -readProc {NHQ_limit_r curr N 1}

CAMP_INT /~/setup/ramp_speed_1 -D -d on -R -r on -S -s on \
    -T "Ch 1 Voltage Ramp Speed" -units "V/s" \
    -readProc {NHQ_ramp_r 1} -writeProc {NHQ_ramp_w 1}

CAMP_INT /~/setup/trip_curr_1 -D -d on -R -r on -S -s on \
    -T "Ch 1 Trip Current" -units "microamp" \
    -H "Current limit to trip off chan 1. Value 0 means no limit. To clear a trip, set volt_set_1 again." \
    -readProc {NHQ_trip_r 1} -writeProc {NHQ_trip_w 1}

CAMP_SELECT /~/setup/polarity_2 -D -d on -R -r on \
    -T "Ch 2 Polarity" -selections Negative Positive \
    -readProc {NHQ_polarity_r 2}

CAMP_FLOAT /~/setup/volt_limit_2 -D -d on -R -r on \
    -T "Ch 2 Voltage Limit" -units "V" \
    -readProc {NHQ_limit_r volt M 2}

CAMP_FLOAT /~/setup/curr_limit_2 -D -d on -R -r on \
    -T "Ch 2 Current Limit" -units "mA" \
    -H "Chan 2 Current Limit. May also act as a trip level if the Kill switch is set." \
    -readProc {NHQ_limit_r curr N 2}

CAMP_INT /~/setup/ramp_speed_2 -D -d on -R -r on -S -s on \
    -T "Ch 2 Voltage Ramp Speed" -units "V/s" \
    -readProc {NHQ_ramp_r 2} -writeProc {NHQ_ramp_w 2}

CAMP_INT /~/setup/trip_curr_2 -D -d on -R -r on -S -s on \
    -T "Ch 2 Trip Current" -units "microamp" \
    -H "Current limit to trip off chan 2. Value 0 means no limit. To clear a trip, set volt_set_2 again." \
    -readProc {NHQ_trip_r 2} -writeProc {NHQ_trip_w 2}

proc NHQ_id_r {ins} {
    set id ""
#   write initial CRLF
    insIfWrite /$ins ""
    if { [catch {insIfRead /${ins} "#" 25} buf] == 0} {
        if { [scan $buf "# %d;%f;%fV;%fmA" num ver vmax imax] == 4 } {
            set id "NHQ $num;$ver"
            varDoSet /${ins}/setup/max_volt -v $vmax
            varDoSet /${ins}/setup/max_curr -v $imax
        }
    }
    varDoSet /${ins}/setup/id -v $id
}

proc NHQ_limit_r { what cmd ch ins } {
    set max [varGetVal /${ins}/setup/max_$what]
    for {set i 0} {$i < 2} {incr i} {
        set buf [insIfRead /$ins "${cmd}${ch}" 9]
        if {[scan $buf " ${cmd}${ch} %d " pct] == 1} {
            set lim [expr {$max*$pct/100.0}]
            varDoSet /${ins}/setup/${what}_limit_$ch -v $lim
            return
        }
    }
    return -code error [NHQ_err_msg $buf]
}

proc NHQ_ramp_r { ch ins } {
    insIfReadVerify /$ins "V$ch" 9 /${ins}/setup/ramp_speed_$ch " V$ch %d " 2
}

proc NHQ_ramp_w { ch ins val } {
    if { $val < 2 || $val > 255 } {
        return -code error "value $val out of range 2...255"
    }
    insIfReadVerify /$ins "V${ch}=[format %3.3d $val]" 11 /${ins}/setup/ramp_speed_$ch " V${ch}=%d" 2
}

proc NHQ_trip_r { ch ins } {
    insIfReadVerify /$ins "L$ch" 10 /${ins}/setup/trip_curr_${ch} " L$ch %d " 2
}

proc NHQ_trip_w { ch ins val } {
    if { $val < 0 || $val > 9999 } {
        return -code error "value $val out of range"
    }
    insIfReadVerify /$ins "L${ch}=[format %4.4d $val]" 12 /${ins}/setup/trip_curr_$ch " L${ch}=%d" 2
}

proc NHQ_polarity_r { ch ins } {
    for {set i 0} {$i < 2} {incr i} {
        set buf [insIfRead /$ins "T${ch}" 9]
        if {[scan $buf " T${ch} %d " bits] == 1} {
            varDoSet /${ins}/setup/polarity_$ch -v [expr {($bits & 4) / 4}]
            return
        }
    }
    return -code error [NHQ_err_msg $buf]
}

proc NHQ_err_msg { buf } {
    if { [string first "????" $buf] >= 0 } { return "Syntax error or command corruption" }
    if { [string first "?WCN" $buf] >= 0 } { return "Wrong channel number" }
    if { [string first "?TOT" $buf] >= 0 } { return "Timeout error" }
    if { [string first "UMAX=" $buf] >= 0 } { return "Set voltage exceeds voltage limit" }
    # sanitize buf a bit
    set b2 ""
    foreach c [split $buf {}] {
        if { [scan $c %c nc]==1 && ( $nc<=31 || ($nc>=127&&$nc<=129) || ($nc>=141&&$nc<=145) ) } {
            set c *
        }
        append b2 $c
    }
    return "Parsing error on $b2"
}