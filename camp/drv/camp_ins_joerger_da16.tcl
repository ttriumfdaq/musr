CAMP_INSTRUMENT /~ -D -T "Joerger D/A-16" -d on \
    -initProc joerger_da16_init \
    -deleteProc joerger_da16_delete \
    -onlineProc joerger_da16_online \
    -offlineProc joerger_da16_offline
  proc joerger_da16_init { ins } { insSet /${ins} -if camac 0.0 0.0 0 0 0 }
  proc joerger_da16_delete { ins } { insSet /${ins} -line off }
  proc joerger_da16_online { ins } { 
    insIfOn /${ins} 
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc joerger_da16_offline { ins } { insIfOff /${ins} }
    CAMP_INT /~/dac_set -D -S -L -T "Set DAC" -d on -s on \
	-writeProc joerger_da16_dac_set_w
      proc joerger_da16_dac_set_w { ins target } {
	    set dac_set [expr { round($target) }]
	    if { ( $dac_set < 0 ) || ( $dac_set > 65535 ) } {
		return -code error "dac_set out of range 0<=dac_set<=65535"
	    }
	    set b [insGetIfCamacB /${ins}]
	    set c [insGetIfCamacC /${ins}]
	    set n [insGetIfCamacN /${ins}]
	    set a [varGetVal /${ins}/setup/output_chan]
	    cdreg ext $b $c $n $a
	    insIfWrite /${ins} "cfsa 16 $ext dac_set q"
	    varDoSet /${ins}/dac_set -v $dac_set
	}
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
        CAMP_SELECT /~/setup/id -D -R -T "ID query" -d on -r on \
            -selections FALSE TRUE \
            -readProc joerger_da16_id_r
          proc joerger_da16_id_r { ins } {
            set id 0
            if {[catch {varSet /${ins}/dac_set -v 0}] == 0} {set id 1}
            varDoSet /${ins}/setup/id -v $id
          }
	CAMP_INT /~/setup/output_chan -D -S -T "Output channel" \
	    -d on -s on -v 0 \
	    -writeProc joerger_da16_output_chan_w
          proc joerger_da16_output_chan_w { ins target } {
	    set chan $target
	    if { ( $chan < 0 ) || ( $chan > 1 ) } {
	      return -code error "Output channel out of range 0 to 1"
	    }
	    varDoSet /${ins}/setup/output_chan -v $chan
	  }
