CAMP_INSTRUMENT /~ -D -T "LakeShore 218" \
    -H "LakeShore 218 Temperature Monitor" -d on \
    -initProc lake218_init -deleteProc lake218_delete \
    -onlineProc lake218_online -offlineProc lake218_offline
proc lake218_init { ins } {
    #insSet /${ins} -if gpib 0.5 5 12 LF LF
    insSet /${ins} -if rs232 0.15 2 tty: 9600 7 odd 1 CRLF CRLF
}
proc lake218_delete { ins } {
    insSet /${ins} -line off
}
proc lake218_online { ins } {
    insIfOn /${ins}
    set i 0
    catch {
        varRead /${ins}/setup/id
        varDoSet /${ins}/internal/initialize -v 0 -p on -p_int 1
        set i 1 
    } msg
    if { $i == 0 } {
        insIfOff /${ins}
        return -code error $msg
    }
}
proc lake218_offline { ins } {
    insIfOff /${ins}
}

for { set ch 1 } { $ch <= 8 } { incr ch } {

    CAMP_FLOAT /~/read_$ch -D -R -P -L -A -T "Ch $ch reading" \
            -d on -r on -p off -p_int 11 -units K \
            -readProc [list lake218_read_r $ch]
}

proc lake218_read_r { ch ins } {
    # global CAMP_VAR_ATTR_ALARM == 16
    if { [string compare [varNumGetUnits /${ins}/read_$ch] "K"] } {
        insIfReadVerify /${ins} "SRDG? $ch" 32 /${ins}/read_$ch " %f" 2
    } else {
        insIfReadVerify /${ins} "KRDG? $ch" 32 /${ins}/read_$ch " %f" 2
    }
    if { ([varGetStatus /${ins}/read_$ch] & 16) && [varGetVal /${ins}/setup/chan_${ch}/alarm_enable] } {
        set lo 0
        set hi 0
        scan [insIfRead /$ins "ALARMST? $ch" 80] " %d,%d" lo hi
        if { $lo || $hi } {
            varDoSet /${ins}/read_$ch -alert on
        } else {
            varDoSet /${ins}/read_$ch -alert off
        }
    }
}

CAMP_SELECT /~/alarm_reset -D -S -T "Reset alarms" \
         -H "Use this to reset latched alarms (normal alarms are self-resetting)" \
         -d on -s on -selections RESET \
         -writeProc lake218_alarm_reset_w

 proc lake218_alarm_reset_w { ins val } {
     insIfWrite /${ins} "ALMRST"
 }

CAMP_STRING /~/relay_status -D -R -P -d on -r on -T "Relay status" \
        -H "Relay activation status: relay 1 is on the left, up to relay 8 on the right." \
        -readProc lake218_relaystat_r

proc lake218_relaystat_r { ins } {
    set val 0
    scan [insIfRead /$ins "RELAYST?" 16] " %d" val
    # Why is there no %b format directive??!!
    set l [list]
    for { set b 0 } { $b < 8 } { incr b } {
	lappend l [expr { (($val>>$b)&1) }]
    }
    varDoSet /${ins}/relay_status -v [join $l " "]
}



CAMP_STRUCT /~/setup -D -d on -T "Configuration"

CAMP_STRING /~/setup/id -D -R -T "ID Query" -d on -r on \
        -v "FAIL" -readProc lake218_id_r
proc lake218_id_r { ins } {
    set id 0
    set status [catch {insIfRead /${ins} "*IDN?" 32} buf]
    if { $status == 0 } {
        set id [scan $buf { LSCI,MODEL%[^,],%*d,%d} mod rev]
        if { $id != 2 } { set id 0 }
    }
    if { $id } {
        varDoSet /${ins}/setup/id -v "Model $mod"
    } else {
        varDoSet /${ins}/setup/id -v "FAIL"
        return -code error "FAIL"
    }
}

CAMP_SELECT /~/setup/sensor_1-4 -D -R -S -T "Sensor for ch 1-4" \
        -d on -r on -s on \
        -H "Sensor type for input channels 1 through 4" \
        -selections {0 Si Diode} {1 GaAlAs Diode} {2 PT-100 (low T)} {3 PT-100 (high T)} {4 PT-1000} {5 Cernox or CGR} \
        -readProc lake218_sensor14_r -writeProc lake218_sensor14_w

proc lake218_sensor14_r { ins } {
    insIfReadVerify /${ins} "INTYPE? A" 32 /${ins}/setup/sensor_1-4 " %d" 2
}

proc lake218_sensor14_w { ins val } {
    insIfWriteVerify /${ins} "INTYPE A,$val" "INTYPE? A" 32 /${ins}/setup/sensor_1-4 " %d" 2 $val
}

CAMP_SELECT /~/setup/sensor_5-8 -D -R -S -T "Sensor for ch 5-8" \
        -d on -r on -s on \
        -H "Sensor type for input channels 5 through 8" \
        -selections {0 Si Diode} {1 GaAlAs Diode} {2 PT-100 (low T)} {3 PT-100 (high T)} {4 PT-1000} {5 Cernox or CGR} \
        -readProc lake218_sensor58_r -writeProc lake218_sensor58_w

proc lake218_sensor58_r { ins } {
    insIfReadVerify /${ins} "INTYPE? B" 32 /${ins}/setup/sensor_5-8 " %d" 2
}

proc lake218_sensor58_w { ins val } {
    insIfWriteVerify /${ins} "INTYPE B,$val" "INTYPE? B" 32 /${ins}/setup/sensor_5-8 " %d" 2 $val
}

for { set ch 1 } { $ch <= 8 } { incr ch } {

 CAMP_STRUCT /~/setup/chan_${ch} -D -d on -T "Config Ch $ch" \
         -H "Set up channel $ch"
    
 CAMP_SELECT /~/setup/chan_${ch}/enable -D -R -S -T "Chan $ch Enable" \
        -d on -r on -s on -selections NO YES \
        -readProc [list lake218_enable_r $ch] -writeProc [list lake218_enable_w $ch]
    
 CAMP_SELECT /~/setup/chan_${ch}/curve_id -D -R -S -T "Curve num" \
        -d on -r on -s on -selections \
        {0 None} {1 DT-470} {2 DT-500} {3 CTI} {4 DT-670} {6 PT-100} {7 PT-1000} "[expr 20+$ch] User" \
        -readProc [list lake218_cid_r $ch] -writeProc [list lake218_cid_w $ch]
    
 CAMP_STRING /~/setup/chan_${ch}/user_curve -D -R -S -T "Curve name" \
        -d on -r on -s on \
        -readProc [list lake218_curve_r $ch] -writeProc [list lake218_curve_w $ch]

 CAMP_SELECT /~/setup/chan_${ch}/alarm_enable -D -R -S -T "C$ch Alarm Enable" \
        -d on -r on -s on -selections OFF ON LATCH \
        -readProc [list lake218_alarm_r $ch] -writeProc [list lake218_alarm_enab_w $ch]

 CAMP_FLOAT /~/setup/chan_${ch}/high_trip -D -R -S -T "C$ch high limit" \
        -H "Upper trip point for Channel $ch alarm" \
        -d on -r on -s on -units K -v 399.9999 \
        -readProc [list lake218_alarm_r $ch] -writeProc [list lake218_hightrip_w $ch]

 CAMP_FLOAT /~/setup/chan_${ch}/low_trip -D -R -S -T "C$ch low limit" \
        -H "Lower trip point for Channel $ch alarm" \
        -d on -r on -s on -units K -v 0.0 \
        -readProc [list lake218_alarm_r $ch] -writeProc [list lake218_lowtrip_w $ch]

 CAMP_FLOAT /~/setup/chan_${ch}/deadband -D -R -S -T "C$ch deadband" \
        -H "Deadband for cancelling alarm trips: readback must come this far inside limits before alarm gets cancelled." \
        -d on -r on -s on -units K -v 0.0 \
        -readProc [list lake218_alarm_r $ch] -writeProc [list lake218_deadband_w $ch]

 CAMP_SELECT /~/setup/chan_${ch}/relay -D -S -R -T "C$ch Alarm Relay" \
        -H "Select a relay to be activated when this alarm is triggered." \
        -d on -s on -r on -v 0 -selections "none" " 1" " 2" " 3" " 4" " 5" " 6" " 7" " 8" \
        -writeProc [list lake218_alarm_relay_w $ch]

}

proc lake218_enable_r { ch ins } {
    insIfReadVerify /${ins} "INPUT? $ch" 32 /${ins}/setup/chan_${ch}/enable " %d" 2
}

# Besides enabling an input channel, this tuns on polling and reads alarm setup
proc lake218_enable_w { ch ins val } {
    insIfWriteVerify /${ins} "INPUT ${ch},$val" "INPUT? $ch" 32 /${ins}/setup/chan_${ch}/enable " %d" 2 $val
    varDoSet /${ins}/read_$ch -p [lindex {off on} $val]
    if { $val } { catch { 
        lake218_alarm_r $ch $ins
        varRead /${ins}/setup/chan_${ch}/curve_id
        if { [varGetVal /${ins}/setup/chan_${ch}/curve_id] == 7 } {
            catch { varRead /${ins}/setup/chan_${ch}/user_curve }
        }
    } }
}

# note: each input channel (1-8) corresponds to a particular user-curve number (21-28)
proc lake218_cid_r { ch ins } {
    set buf [insIfRead /${ins} "INCRV? $ch" 32]
    if { [scan $buf " %d" ic] < 1 } {
        return -code error "Failed read of ch $ch curve number: $buf"
    }
    set val -1
    if { $ic == 20 + $ch } {
        set val 7
    } elseif { $ic < 10 } {
        set val [lindex [list 0 1 2 3 4 -1 5 6 -1 -1] $ic]
    }
    if { $val < 0 } {
        varDoSet /${ins}/setup/chan_${ch}/curve_id -v 0
        return -code error "Invalid internal curve number for ch ${ch}: $ic"
    }
    varDoSet /${ins}/setup/chan_${ch}/curve_id -v $val
    lake218_units_adjust $ch $ins
}

# {0: None} {1: DT-470} {2: DT-500} {3: CTI} {4: DT-670} {6: PT-100} {7: PT-1000} "[expr 20+$ch]: User"
proc lake218_cid_w { ch ins val } {
    set ic [lindex [list 0 1 2 3 4 6 7 [expr 20+$ch]] $val]
    insIfWrite /${ins} "INCRV ${ch},${ic}"
    varDoSet /${ins}/setup/chan_${ch}/curve_id -v $val
    if { [catch { varRead /${ins}/setup/chan_${ch}/curve_id }] ||
	 $val != [varGetVal /${ins}/setup/chan_${ch}/curve_id] } {
        return -code error "Curve was not accepted"
    }
    #  already done by varRead: lake218_units_adjust $ch $ins
    # now handle companion user_curve
    if { $val == 7 } {
        if { [catch {varRead /${ins}/setup/chan_${ch}/user_curve}] } {
            set cn ""
        } else {
            set cn [varGetVal /${ins}/setup/chan_${ch}/user_curve]
        }
        if { [string length $cn] == 0 } {
            return -code error "Please set user_curve first (or instead)"
        }
    }
}

# Reading back the curve name from the curve header is a problem
# because the header is UPPERCASE only.  Go through contortions to
# achieve a proper-cased curve name.  In vxworks there is a problem
# that [file exists foo] and [glob foo] both fail, but [glob foo*] 
# succeeds.

proc lake218_curve_r { ch ins } {
    set name [varGetVal /${ins}/setup/chan_${ch}/user_curve]
    set sn ""
    if { [catch { insIfRead /${ins} "CRVHDR? [expr {20+$ch}]" 64 } hdr] } {
        set cn ""
    } elseif { [scan $hdr " User Curve %d" cn] == 1 } {
	set cn ""
    } elseif { [scan $hdr { %[^,],} cn] < 1 } {
	set cn ""
    } else {
	set cn [string trim $cn]
	set lcn [string tolower $cn]
	if { [string match [string tolower $name].* $lcn] } {
	    set sn $name
	} else {
	    if { [catch {glob "./dat/$cn*"}] } {
		if { [catch {glob "./dat/$lcn*"}] } {
		    if { ![catch {glob "./dat/*"} allfiles] } {
			foreach f $allfiles {
			    if { 0 = [string compare [string tolower [file tail $f]] $lcn] } {
				set cn [file tail $f]
				break
			    }
			}
		    }
		} else {
		    set cn $lcn
		}
	    }
	    set sn [file root $cn]
	}
    }
    if { $sn == "" } {
	varDoSet /${ins}/setup/chan_${ch}/user_curve -v $sn -m "No curve loaded"
    } else {
	varDoSet /${ins}/setup/chan_${ch}/user_curve -v $sn -m \
	    "User curve $ch ([expr {20+$ch}]) is $sn loaded from $cn"
    }
}

proc lake218_curve_w { ch ins cnam } {
    # prevent nasty users from accessing other directories
    set cnam [file tail $cnam]
    # Ignore possibility of curve name with "*" in it, but allow for names
    # with and without the extension.
    set ext [string tolower [file extension $cnam]]
    if { $ext == "" } {
	set ext ".*"
    }
    set base [file root $cnam]
    varDoSet /${ins}/setup/chan_${ch}/user_curve -v $base -m \
	"Load ${base}${ext} as curve [expr {20+$ch}]"
    #
    if { [catch { glob ./dat/${base}${ext} } ffound ] } {
	varDoSet /${ins}/setup/chan_${ch}/user_curve -m \
	    "Failed to load ${base}${ext} -- no file found"
	return -code error "No such file: ${base}${ext}"
    }
    set file [lindex $ffound 0]
    set ext [string tolower [file extension $file]]
    set type [expr [lsearch -exact ".tc .spl .tr .dat" $ext] + 1]
    if { $type < 2 } {
	varDoSet /${ins}/setup/chan_${ch}/user_curve -v -m \
	    "Failed to load ${base}${ext} -- unsupported type \"$ext\""
	return -code error "Unsupported file type \"$ext\""
    }
    varDoSet /${ins}/setup/chan_${ch}/user_curve -v $base -m \
	"Load ${base}${ext} as curve [expr {20+$ch}]"
    # Could duplicate some of the initial file-reading operations here
    # Begin loading
    lake218_do_load_w $ins $ch [file tail $file]
}

# Set the units on several related variables.  Fix any discrepancy with 
# alarm units used by the unit.
proc lake218_units_adjust { ch ins } {
    if { [varGetVal /${ins}/setup/chan_${ch}/curve_id] } {
        varDoSet /${ins}/read_$ch -units "K"
        varDoSet /${ins}/setup/chan_${ch}/high_trip -units "K"
        varDoSet /${ins}/setup/chan_${ch}/low_trip -units "K"
        varDoSet /${ins}/setup/chan_${ch}/deadband -units "K"
	set source 1
    } else {
        set bank [lindex {1-4 5-8} [expr { $ch/5 }]]
        varRead /${ins}/setup/sensor_$bank
        set sens [varGetVal /${ins}/setup/sensor_$bank]
        set u [lindex {V V Ohm Ohm Ohm Ohm} $sens]
        varDoSet /${ins}/read_$ch -units $u
        varDoSet /${ins}/setup/chan_${ch}/high_trip -units $u
        varDoSet /${ins}/setup/chan_${ch}/low_trip -units $u
        varDoSet /${ins}/setup/chan_${ch}/deadband -units $u
	set source 3
    }
    # Fix any discrepancy in alarm (source) units
    set buf [insIfRead /$ins "ALARM? $ch" 80]
    if { [scan $buf {%d,%d,%f,%f,%f,%d} enab sour high low dead latch] != 6 } {
	return -code error "Invalid alarm status: $buf"
    }
    if { $sour != $source } {
        insIfWrite /$ins "ALARM $ch,$enab,$source,$high,$low,$dead,$latch"
    }
}

proc lake218_alarm_r { ch ins } {
    set buf [insIfRead /$ins "ALARM? $ch" 80]
    if { [scan $buf {%d,%d,%f,%f,%f,%d} enab source high low dead latch] != 6 } {
	return -code error "Invalid alarm status: $buf"
    }
    if { $enab } { incr enab $latch }
    varDoSet /${ins}/setup/chan_${ch}/alarm_enable -v $enab
    varDoSet /${ins}/setup/chan_${ch}/high_trip -v $high
    varDoSet /${ins}/setup/chan_${ch}/low_trip -v $low
    varDoSet /${ins}/setup/chan_${ch}/deadband -v $dead
}

proc lake218_alarm_apply { ch ins } {
    set enab [varGetVal /${ins}/setup/chan_${ch}/alarm_enable]
    set latch [expr {$enab > 1}]
    incr enab -$latch
    if { [string compare [varNumGetUnits /${ins}/read_$ch] "K"] } {
	set source 3
    } else {
	set source 1
    }
    set high [varGetVal /${ins}/setup/chan_${ch}/high_trip]
    set low  [varGetVal /${ins}/setup/chan_${ch}/low_trip]
    set dead [varGetVal /${ins}/setup/chan_${ch}/deadband]
    insIfWrite /$ins "ALARM $ch,$enab,$source,$high,$low,$dead,$latch"
}

proc lake218_alarm_enab_w { ch ins val } {
    varDoSet /${ins}/setup/chan_${ch}/alarm_enable -v $val
    varDoSet /${ins}/read_${ch} -a [lindex {off on} $val]
    lake218_alarm_apply $ch $ins
}

proc lake218_hightrip_w { ch ins val } {
    varDoSet /${ins}/setup/chan_${ch}/high_trip -v $val
    lake218_alarm_apply $ch $ins
}

proc lake218_lowtrip_w { ch ins val } {
    varDoSet /${ins}/setup/chan_${ch}/low_trip -v $val
    lake218_alarm_apply $ch $ins
}

proc lake218_deadband_w { ch ins val } {
    varDoSet /${ins}/setup/chan_${ch}/deadband -v $val
    lake218_alarm_apply $ch $ins
}

proc lake218_alarm_relay_w { ch ins rel } {
    set buf [insIfRead /$ins "RELAY? $rel" 80]
    if { [scan $buf {%d,%d,%d} mode input type] != 3 } {
	return -code error "Invalid alarm status: $buf"
    }
    if { $mode == 2 && $input != $ch } {# That relay is being used...
	varRead /${ins}/setup/chan_${input}/enable
	varRead /${ins}/setup/chan_${input}/alarm_enable
	if { [varGetVal /${ins}/setup/chan_${input}/alarm_enable] && \
		 [varGetVal /${ins}/setup/chan_${input}/enable] } {# ...by an enabled alarm
	    return -code error "Relay $rel is already being used for input channel $input."
	}
    }
    insIfWriteVerify /$ins "RELAY $rel,2,$ch,2" "RELAY? $rel" 32 /${ins}/setup/chan_${ch}/relay "%*d,%d,%*d," 1 $ch
}

# check all relays looking for one that responds to this input channel
proc lake218_alarm_relay_r { ch ins } {
    for {set rel 1} {$rel <= 8 } {incr rel} {
        set buf [insIfRead /$ins "RELAY? $rel" 80]
        if { [scan $buf {%d,%d,%d} mode input type] == 3 && $mode == 2 && $input == $ch } {
            # That relay is being used for this input
            varDoSet /${ins}/setup/chan_${ch}/relay -v $rel
            return
        }
    }
    varDoSet /${ins}/setup/chan_${ch}/relay -v 0
}


CAMP_STRUCT /~/internal -D -d on -T "Internal variables"

# Do reads of many things as part of initialization.  To maintain responsiveness
# of other higher-priority operations, do initialization incremenatally by polling.
# At stage 0 we check sensor types and stop do_load
# At stages 1-8 we read the setup for that numbered channel and that relay
# At stage 9 we read relay status and finish.

 CAMP_INT /~/internal/initialize -D -R -P -T "Initialize" \
         -d on -r on -v 0 \
         -readProc lake218_initialize_r
 proc lake218_initialize_r { ins } {
     set v [varGetVal /${ins}/internal/initialize]
     if { $v == 0 } {
	 varDoSet /${ins}/internal/do_load -p off -v ""
	 varRead /${ins}/setup/sensor_1-4
	 varRead /${ins}/setup/sensor_5-8
	 varDoSet /${ins}/internal/initialize -v 1 -p on -p_int 1
     } elseif { $v > 8 } {
	 varDoSet /${ins}/internal/initialize -v 0 -p off
	 varRead /${ins}/relay_status
     } else {
	 set ch $v
         varRead /${ins}/setup/chan_${ch}/enable
         if { [varGetVal /${ins}/setup/chan_${ch}/enable] } {
             varRead /${ins}/setup/chan_${ch}/curve_id
             if { [varGetVal /${ins}/setup/chan_${ch}/curve_id] == 7 } {
                 catch { varRead /${ins}/setup/chan_${ch}/user_curve }
             }
             varDoSet /${ins}/read_$ch -p on
         }
         catch { lake218_alarm_r $ch $ins }
	 set rel $v
         set buf [insIfRead /$ins "RELAY? $rel" 80]
         if { [scan $buf {%d,%d,%d} mode input type] == 3 && $mode == 2 && $input > 0 } {
             varDoSet /${ins}/setup/chan_${input}/relay -v $rel
         }
	 varDoSet /${ins}/internal/initialize -v [incr v]
     }
 }

# String contains list of <channel_num> <curve_file> acting
# as a queue for loading curves.

CAMP_STRING /~/internal/do_load -D -R -P -T "curves to load" \
    -d on -r on -p off -v "" \
    -readProc lake218_do_load_r

# ch should be the channel number for which to load the curve
# (this has an extra parameter, so is invoked directly)
proc lake218_do_load_w { ins ch file } {
    set curves [varGetVal /${ins}/internal/do_load]
    if { $ch < 1 || $ch > 8 || [string length file] < 1 } {
	return -code error "Improper use of do_load"
    }
    lappend curves $ch $file
    varDoSet /${ins}/internal/do_load -v $curves -p on -p_int 0.5
#    lake218_do_load_r $ins
}


# Curve formats table:
# Sensor         exten   "type"   coefft  units     order (in file)
# thermocouple   .tc     1        +       K,mV      increasing V (increasing T)
# Diode          .spl    2        -       K,V       decreasing V (increasing T)
# Thermister     .tr     3        +       K,Ohm     increasing R (increasing T)
# CGR,Cernox     .dat    4        -       K,log Ohm increasing log(R) (decreasing T)

# The Lakeshore218 (and others) require curve points in order of increasing
# raw-readback (V,Ohm,...).  Therefore, all the above should be loaded in file 
# order, except for spl files which should be reversed.
 
# This is invoked (by polling) once per curve to load
proc lake218_do_load_r { ins } {
    set plan [varGetVal /${ins}/internal/do_load]
    if { [llength $plan] < 2 } {
	varDoSet /${ins}/internal/do_load -v "" -p off -m ""
	return
    }
    set ch [lindex $plan 0]
    set file [file tail [lindex $plan 1]]
    # determine header info
    set num [expr {$ch+20}]
    set cnam [file root $file]
    set ext [string tolower [file extension $file]]
    set type [expr [lsearch -exact ".tc .spl .tr .dat" $ext] + 1]
    set fmt [lindex [list 0 0 2 3 4] $type]
    set cft [lindex [list 0 2 1 2 1] $type]
    set fspec [format "./dat/%s" $file]
    varDoSet /${ins}/internal/do_load -v [lrange $plan 2 end] -p on \
	-m "Loading curve $cnam from $fspec for channel $ch"
    varDoSet /${ins}/setup/chan_${ch}/user_curve \
	-m "Loading curve $cnam from $fspec for channel $ch"
    # get file
    set f [open $fspec r]
    # read first line (has the number of data points)
    if { [gets $f line] == -1 } {
	close $f
	lake218_abort_load $ins $ch "unexpected end of file"
    }
    if { [scan $line " %d" npt] < 1 || $npt < 2 || $npt > 198 } {
	close $f
	lake218_abort_load $ins $ch "bad number of points on first line"
    }
    # read ALL the points (so we can close the file)
    set lines [split [string trim [read $f]] "\n"]
    close $f
    if { [llength $lines] != $npt } {
	lake218_abort_load $ins $ch "incorrect number of points on first line"
    }
    set last [expr $npt-1]
    # Examine first two points
    set t0 0; set v0 0; set t1 0; set v1 0
    for { set i 0 } { $i < 2 } { incr i } {
	if { [scan [lindex $lines $i] " %f %f" t$i v$i] < 2 } {
	    lake218_abort_load $ins $ch "bad file format"
	}
    }
    # Determine whether ascending and maximum temperature
    if { $t0 < $t1 } {
	set asc 1
	if { [scan [lindex $lines $last] " %f %f" tmax v] < 2 } {
	    lake218_abort_load $ins $ch "bad file format"
	}
    } else {
	set asc -1
	set tmax $t0
    }
    # Determine ordering of raw-unit numbers
    if { $v0 < $v1 } {
	set idx 1
	set step 1
    } else {
	set idx $npt
	set step -1
    }
    # check ordering against sensor coefficient
    if { $step*$asc != ( $cft == 2 ? 1 : -1 ) } {
	lake218_abort_load $ins $ch "improper coefficient"
    }
    # disable curve while loading
    varSet /${ins}/setup/chan_${ch}/curve_id -v 0
    # delete the curve in memory 
    insIfWrite /${ins} [format "CRVDEL %2d" $num]
    # Set the curve header
    #set lf [open "./m15vw/log/curve.log" w]
    insIfWrite /${ins} [format "CRVHDR %2d,%15.15s,%10.10s,%1d,%.1f,%1d" \
			    $num $file $cnam $fmt $tmax $cft]
    #puts $lf [format "CRVHDR %2d,%15.15s,%10.10s,%1d,%.1f,%1d" \
    #		    $num $file $cnam $fmt $tmax $cft]
    # compile and set curve data
    set curve_data ""
    set sep ""
    foreach line $lines {
	if { [scan $line " %f %f" t v] < 2 } {
	    lake218_abort_load $ins $ch "bad file format"
	}
	append curve_data $sep [format "CRVPT %2d,%d,%s,%s" $num $idx [sixDigits $v] [sixDigits $t]]
	set sep ";"
	# This appends as many as 26 chars. lake218 buffer is only 64 characters.
	if { [string length $curve_data] > 64-27 } {
	    insIfWrite /${ins} $curve_data
	    #puts $lf $curve_data
	    set curve_data ""
	    set sep ""
	}
	incr idx $step
    }
    # May still have a point unwritten
    if { [string length $curve_data] } {
	    insIfWrite /${ins} $curve_data
	    #puts $lf $curve_data
    }
    #close $lf
    # Finished downloading curve. Try switching to it (The user curve is Camp selection 7)
    if { [catch {varSet /${ins}/setup/chan_${ch}/curve_id -v 7} msg ] } {
	lake218_abort_load $ins $ch $msg
    }
    # Could verify all downloaded points, but leave for some other time
    # Done load.
    varDoSet /${ins}/internal/do_load -m ""
    return
}

proc lake218_abort_load { ins ch msg } {
    varDoSet /${ins}/internal/do_load -m $msg
    varDoSet /${ins}/setup/chan_${ch}/user_curve -m "Failed to load curve: $msg"
    return -code error $msg
}

proc sixDigits { v } {
    if { abs($v)<9.99 } {
	return [format %.5f $v]
    } else {
	return [expr [format %.5e $v]]
    }
}
