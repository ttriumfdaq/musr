CAMP_INSTRUMENT /~ -D -T "LakeShore 330" \
    -H "LakeShore 330 Autotuning Temperature Controller" -d on \
    -initProc lake330_init -deleteProc lake330_delete \
    -onlineProc lake330_online -offlineProc lake330_offline
  proc lake330_init { ins } {
	insSet /${ins} -if gpib 0.5 5 12 LF LF
#	insSet /${ins} -if rs232 0.9 5 /tyCo/2 1200 7 odd 1 LF LF
  }
  proc lake330_delete { ins } {
	insSet /${ins} -line off
  }
  proc lake330_online { ins } {
	insIfOn /${ins}
	set i 0
	catch {
	    varRead /${ins}/setup/id
	    set i [varGetVal /${ins}/setup/id]
	    if { $i } {
		varRead /${ins}/setup/sample_units
		varRead /${ins}/setup/control_units
	    }
	}
	if { $i == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
  }
  proc lake330_offline { ins } {
	insIfOff /${ins}
  }

    CAMP_FLOAT /~/sample_read -D -R -P -L -T "Sample reading" \
	-d on -r on \
	-readProc lake330_sample_read_r
      proc lake330_sample_read_r { ins } {
	set s [catch {insIfReadVerify /${ins} "SDAT?" 32 /${ins}/sample_read " %f" 2} res]
	if { [string first ER $res] != -1 } { 
	  varDoSet /${ins}/sample_read -v 0.0
	  return -code error "Signal error"
	}
	if { $s != 0 } { return -code error "failed reading instrument $ins" }
      }
    CAMP_FLOAT /~/control_read -D -R -P -L -A -T "Control reading" \
	-d on -r on \
	-readProc lake330_control_read_r
      proc lake330_control_read_r { ins } {
	set s [catch {insIfReadVerify /${ins} "CDAT?" 32 /${ins}/control_read " %f" 2} res]
	if { [string first ER $res] != -1 } { 
	  varDoSet /${ins}/control_read -v 0.0
	  return -code error "Signal error"
	}
	if { $s != 0 } { return -code error "failed reading instrument $ins" }
	varTestAlert /${ins}/control_read [varGetVal /${ins}/control_set]
      }
    CAMP_FLOAT /~/control_set -D -R -S -L -T "Control set point" \
	-d on -r on -s on \
	-readProc lake330_control_set_r \
	-writeProc lake330_control_set_w
      proc lake330_control_set_r { ins } {
	    insIfReadVerify /${ins} "SETP?" 32 /${ins}/control_set " %f" 2
      }
      proc lake330_control_set_w { ins target } {
	    set control_set $target
	    varRead /${ins}/setup/control_units
	    switch [varGetVal /${ins}/setup/control_units] {
		0 -
		1 {
		    if { $control_set < 0.0 } { set control_set 0.0 }
		    if { $control_set > 799.9 } { set control_set 799.9 }
		    set target [format "%6.2f" $control_set]
		}
		2 {
		    if { $control_set < 0.0 } { set control_set 0.0 }
		    if { $control_set > 2.4990 } { set control_set 2.4990 }
		    set target [format "%6.4f" $control_set]
		}
		3 {
		    if { $control_set < 0.0 } { set control_set 0.0 }
		    if { $control_set > 299.99 } { set control_set 299.99 }
		    set target [format "%6.2f" $control_set]
		}
		4 {
		    if { $control_set < -15.0 } { set control_set -15.0 }
		    if { $control_set > 15.0 } { set control_set 15.0 }
		    set target [format "%6.3f" $control_set]
		}
	    }
	    set tol 0.0
	    if { [catch {
		insIfWriteVerify /${ins} "SETP $target" "SETP?" \
			32 /${ins}/control_set " %f" 2 $target $tol
	    } msg ] } {
		varRead /${ins}/ramp
		if { [varGetVal /${ins}/ramp] } {
		    varDoSet /${ins}/ramp_status -p on -p_int 5
		} else {
		    return -code error $msg
		}
	    }
      }
    CAMP_SELECT /~/heat_range -D -R -P -L -S -T "Heater range" \
	-d on -r on -s on -selections OFF LOW MED HIGH \
	-readProc lake330_heat_range_r \
	-writeProc lake330_heat_range_w
      proc lake330_heat_range_r { ins } {
	    insIfReadVerify /${ins} "RANG?" 32 /${ins}/heat_range " %d" 2
      }
      proc lake330_heat_range_w { ins target } {
	    insIfWriteVerify /${ins} "RANG $target" "RANG?" 32 /${ins}/heat_range " %d" 2 $target
      }
    CAMP_FLOAT /~/current_read -D -R -P -L -T "Output current read" \
	-d on -r on -p off -units A \
	-H "Heater output in Amps.  (Full output on High range is 1.0 A.)" \
	-readProc lake330_current_read_r 
      proc lake330_current_read_r { ins } {
	    varRead /${ins}/heat_range
	    set heat_range [lindex "0.0 0.1 0.3 1.0" [varGetVal /${ins}/heat_range] ]
	    set buf [insIfRead /${ins} "HEAT?" 32]
	    if { [scan $buf " %d" percent ] != 1 } { set percent -1.0 }
	    if { ( $percent < 0.0 ) || ( $percent > 100.0 ) } {
		return -code error "failed parsing \"$buf\" from HEAT? command"
	    }
	    varDoSet /${ins}/current_read -v [expr {$heat_range*$percent/100.0}]
      }
    CAMP_SELECT /~/ramp -D -S -R -P -T "Ramp" \
	-d on -s on -r on -selections DISABLED ENABLED \
	-H "Enable or disable slow ramping of setpoint" \
	-readProc lake330_ramp_r -writeProc lake330_ramp_w
      proc lake330_ramp_r { ins } {
	    insIfReadVerify /${ins} "RAMP?" 32 /${ins}/ramp " %d" 2
	}
      proc lake330_ramp_w { ins target } {
	    insIfWriteVerify /${ins} "RAMP $target" "RAMP?" 32 /${ins}/ramp " %d" 2 $target
	}
    CAMP_SELECT /~/ramp_status -D -S -R -P -T "Ramp status" \
	-d on -s on -r on -p off -p_int 5 -selections DONE RAMPING \
	-readProc lake330_ramp_status_r -writeProc lake330_ramp_status_w
      proc lake330_ramp_status_r { ins } {
	    insIfReadVerify /${ins} "RAMPS?" 32 /${ins}/ramp_status " %d" 2
	    varRead /${ins}/control_set
	    if { [varGetVal /${ins}/ramp_status] } {
		varDoSet /${ins}/ramp_status -p on -p_int 5
	    } else {
		varDoSet /${ins}/ramp_status -p off
	    }
	}
      proc lake330_ramp_status_w { ins target } {
	    insIfWriteVerify /${ins} "RAMPS $target" "RAMPS?" 32 /${ins}/ramp_status " %d" 2 $target
	}
    CAMP_FLOAT /~/ramp_rate -D -S -R -L -P -T "Ramp rate" \
	-d on -s on -r on -p off -units {K/min} \
	-H "Temperature (setpoint) ramp rate (if ramp is enabled) in Kelvin per minute (from 0.1 to 99.9, or 0 for off)" \
	-readProc lake330_ramp_rate_r -writeProc lake330_ramp_rate_w
      proc lake330_ramp_rate_r { ins } {
	insIfReadVerify /${ins} "RAMPR?" 32 /${ins}/ramp_rate " %f" 2
      }
      proc lake330_ramp_rate_w { ins target } {
	if { $target < 0.0 || $target > 99.91 } { 
	    return -code error "Invalid ramp rate.  Must be 0 to 99.9 (K/min)"
	}
	set target [format "%5.1f" $target]
	insIfWrite /${ins} "RAMPR $target"
	varRead /${ins}/ramp_rate
	varDoSet /${ins}/ramp -v [expr {$target > 0.0}]
      }

    CAMP_STRUCT /~/setup -D -d on
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE \
	    -readProc lake330_id_r
	  proc lake330_id_r { ins } {
	    set id 0
	    set status [catch {insIfRead /${ins} "*IDN?" 32} buf]
	    if { $status == 0 } {
		set id [scan $buf " LSCI,MODEL330,%*d,%d" val]
		if { $id != 1 } { set id 0 }
	    }
	    varDoSet /${ins}/setup/id -v $id
	  }
	CAMP_SELECT /~/setup/control_units -D -R -S -T "Control units" \
	    -d on -r on -s on -selections K C V R M \
	    -readProc lake330_control_units_r \
	    -writeProc lake330_control_units_w
	  proc lake330_control_units_r { ins } {
	    insIfReadVerify /${ins} "CUNI?" 32 /${ins}/setup/control_units " %s" 2
	    varDoSet /${ins}/control_set -units \
		   [varSelGetValLabel /${ins}/setup/control_units]
	    varDoSet /${ins}/control_read -units \
		   [varSelGetValLabel /${ins}/setup/control_units]
	  } 
	  proc lake330_control_units_w { ins target } {
	    insIfWrite /${ins} "CUNI [lindex {K C S S S} $target]"
	    varRead /${ins}/setup/control_units
	  }
	CAMP_SELECT /~/setup/sample_units -D -R -S -T "Sample units" \
	    -d on -r on -s on -selections K C V R M \
	    -readProc lake330_sample_units_r \
	    -writeProc lake330_sample_units_w
	  proc lake330_sample_units_r { ins } {
	    insIfReadVerify /${ins} "SUNI?" 32 /${ins}/setup/sample_units " %s" 2
	    varDoSet /${ins}/sample_read -units \
		[varSelGetValLabel /${ins}/setup/sample_units]
	  }
	  proc lake330_sample_units_w { ins target } {
	    insIfWrite /${ins} "SUNI [lindex {K C S S S} $target]"
	    varRead /${ins}/setup/sample_units
	  }
	CAMP_SELECT /~/setup/control_chan -D -R -S -T "Control channel" \
	    -d on -r on -s on -selections A B \
	    -readProc lake330_control_chan_r \
	    -writeProc lake330_control_chan_w
	  proc lake330_control_chan_r { ins } {
	    insIfReadVerify /${ins} "CCHN?" 32 /${ins}/setup/control_chan " %s" 2
	  }
	  proc lake330_control_chan_w { ins target } {
	    set target [lindex {A B} $target]
	    insIfWriteVerify /${ins} "CCHN $target" "CCHN?" 32 /${ins}/setup/control_chan " %s" 2 $target
	  }
	CAMP_SELECT /~/setup/sample_chan -D -R -S -T "Sample channel" \
	    -d on -r on -s on -selections A B \
	    -readProc lake330_sample_chan_r \
	    -writeProc lake330_sample_chan_w
	  proc lake330_sample_chan_r { ins } {
	    insIfReadVerify /${ins} "SCHN?" 32 /${ins}/setup/sample_chan " %s" 2
	  }
	  proc lake330_sample_chan_w { ins target } {
	    set target [lindex {A B} $target]
	    insIfWriteVerify /${ins} "SCHN $target" "SCHN?" 32 /${ins}/setup/sample_chan " %s" 2 $target
	  }
	CAMP_INT /~/setup/A_curve -D -R -S -T "Channel A curve" \
	    -d on -r on -s on \
	    -H "Load or read the A-channel calibration curve" \
	    -readProc {lake330_AB_curve_r A} -writeProc {lake330_AB_curve_w A}
	CAMP_INT /~/setup/B_curve -D -R -S -T "Channel B curve" \
	    -d on -r on -s on \
	    -H "Load or read the B-channel calibration curve" \
            -readProc {lake330_AB_curve_r B} -writeProc {lake330_AB_curve_w B}
	  proc lake330_AB_curve_r { channel ins } {
            varDoSet /${ins}/setup/${channel}_curve -m "Reading and verifying curve for channel $channel"
            varDoSet /${ins}/controls/${channel}_read -p on -p_int 0.1
	  }
	  proc lake330_AB_curve_w { channel ins target } { 
            varDoSet /${ins}/setup/${channel}_curve -v $target -units START -m "Prepare to load curve $target"
            varDoSet /${ins}/controls/${channel}_load -p on -p_int 0.1
	  }
	CAMP_SELECT /~/setup/A_input -D -R -T "Channel A input type" \
	    -d on -r on -selections SI PT AS TC ER \
	    -readProc lake330_A_input_r
	  proc lake330_A_input_r { ins } {
		insIfReadVerify /${ins} "ATYPE?" 32 /${ins}/setup/A_input " %s" 2
	  }
	CAMP_SELECT /~/setup/B_input -D -R -T "Channel B input type" \
	    -d on -r on -selections SI PT AS TC ER \
	    -readProc lake330_B_input_r
	  proc lake330_B_input_r { ins } {
		insIfReadVerify /${ins} "BTYPE?" 32 /${ins}/setup/B_input " %s" 2
	  }
	CAMP_SELECT /~/setup/autotune -D -R -S -T "Autotuning type" \
	    -d on -r on -s on -selections MANUAL P PI PID ZONE \
	    -readProc lake330_autotune_r -writeProc lake330_autotune_w
	  proc lake330_autotune_r { ins } {
		insIfReadVerify /${ins} "TUNE?" 32 /${ins}/setup/autotune " %d" 2
	  } 
	  proc lake330_autotune_w { ins target } {
		insIfWriteVerify /${ins} "TUNE $target" "TUNE?" 32 /${ins}/setup/autotune " %d" 2 $target
	  }
	CAMP_INT /~/setup/P -D -R -P -S -L -T "Gain setting (P)" \
	    -d on -r on -s on \
            -H "Proportional Gain setting (P), range 0-999. Use higher values when thermal response is sluggish or thermometer is insensitive." \
	    -readProc lake330_P_r \
	    -writeProc lake330_P_w
	  proc lake330_P_r { ins } {
		insIfReadVerify /${ins} "GAIN?" 32 /${ins}/setup/P " %d" 2
	  } 
	  proc lake330_P_w { ins target } {
		if { $target < 0 || $target > 999 } {
		    return -code error "invalid value \"$target\""
		}
		insIfWriteVerify /${ins} "GAIN $target" "GAIN?" 32 /${ins}/setup/P " %d" 2 $target
	  }
	CAMP_INT /~/setup/I -D -R -P -S -L -T "Reset setting (I)" \
	    -d on -r on -s on \
            -H "Number of Reset Intervals (I), range 0-999. Reset time (seconds) = 999/I" \
	    -readProc lake330_I_r \
	    -writeProc lake330_I_w
	  proc lake330_I_r { ins } {
		insIfReadVerify /${ins} "RSET?" 32 /${ins}/setup/I " %d" 2
	  } 
	  proc lake330_I_w { ins target } {
		if { $target < 0 || $target > 999 } {
		    return -code error "invalid value \"$target\""
		}
		insIfWriteVerify /${ins} "RSET $target" "RSET?" 32 /${ins}/setup/I " %d" 2 $target
	  }
	CAMP_INT /~/setup/D -D -R -P -S -L -T "Rate setting (D)" \
	    -d on -r on -s on \
	    -H "Rate setting (D) expressed as percentage of one-quarter of the Reset Time (250/I).  Use 0 for off" \
	    -readProc lake330_D_r \
	    -writeProc lake330_D_w
	  proc lake330_D_r { ins } {
		insIfReadVerify /${ins} "RATE?" 32 /${ins}/setup/D " %d" 2
	  } 
	  proc lake330_D_w { ins target } {
		if { $target < 0 || $target > 180 } {
		    return -code error "invalid value \"$target\""
		}
		insIfWriteVerify /${ins} "RATE $target" "RATE?" 32 /${ins}/setup/D " %d" 2 $target
	  }

	CAMP_STRUCT /~/setup/zone -D -d on -T "PID Zone configuration"
	  CAMP_SELECT /~/setup/zone/read_zones -D -R -T "Read zones" -d on -r on \
		  -H "Read all zone settings" -selections READ \
		  -readProc lake330_read_zones_r
	  proc lake330_read_zones_r { ins } {
	      for { set i 1 } { $i <= 10 } { incr i } {
		  lake330_zone_r $i $ins
	      }
	  }
	  CAMP_SELECT /~/setup/zone/apply_zones -D -S -T "Apply zones" -d on -s on \
		  -H "Check and apply all zone settings" -selections APPLY \
		  -writeProc lake330_apply_zones_w
	  proc lake330_apply_zones_w { ins target } {
	      set max 0.0
	      set adj 0
	      for { set i 1 } { $i <= 10 } { incr i } {
		  set z [varGetVal /${ins}/setup/zone/zone${i}]
                  # If this zone is blank, copy from previous.
		  if { [llength $z] == 0 && $i > 1 } {
                      set z [varGetVal /${ins}/setup/zone/zone[expr {$i-1}]]
                  }
		  if { [llength $z] >= 5 } {
		      set T [lindex $z 0]
		      if { $T < $max } {
			  if { $max < 300.0 } {
			      return -code error "Invalid sequence of temperatures: must increase from zone1 to zone10"
			  }
			  set T [string trim [format %6.1f $max]]
			  set z [lreplace $z 0 0 $T]
			  varDoSet /${ins}/setup/zone/zone${i} -v $z
		      }
		      set max $T
		      insIfWrite /$ins "ZONE $i,[join $z {,}]"
		  } else {
		      return -code error "Invalid values for zone $i."
		  }
	      }
	  }
	  for { set i 1 } { $i <= 10 } { incr i } {
	      CAMP_STRING /~/setup/zone/zone$i -D -S -R -T "Zone $i info" \
		      -d on -s on -r on \
		      -H "List of zone $i parameters: Max (set) Temp, Heat range, P, I, D" \
		      -readProc "lake330_zone_r $i" -writeProc "lake330_zone_w $i"
	  }
	  proc lake330_zone_w { i ins target } {
	      set z [join [split $target ,] " "]
	      if { [llength $z] == 5 } {
		  varDoSet /${ins}/setup/zone/zone${i} -v $z
	      } else {
		  return -code error "Invalid zone data!  Should be 5 numbers separated by spaces or commas."
	      }
	  }
	  proc lake330_zone_r { i ins } {
	      set z [insIfRead /$ins "ZONE? $i" 64]
	      if { [scan $z " %f,%d,%d,%d,%d" T H P I D] == 5 } {
		  varDoSet /${ins}/setup/zone/zone${i} -v [list $T $H $P $I $D]
	      } else {
		  return -code error "Invalid zone readback: \"$z\""
	      }
	  }

	CAMP_STRUCT /~/setup/curve -D -d on
	    CAMP_INT /~/setup/curve/number -D -S -T "Curve number" -d on -s on \
		-writeProc lake330_curve_number_w
	      proc lake330_curve_number_w { ins target } {
		    if { ( $target < 11 ) || ( $target > 31 ) } {
			return -code error "invalid curve number $target"
		    }
		    varDoSet /${ins}/setup/curve/number -v $target
		}
	    CAMP_STRING /~/setup/curve/descrip -D -S -T "Curve description" -d on -s on \
		-writeProc lake330_curve_descrip_w
	      proc lake330_curve_descrip_w { ins target } {
		    varDoSet /${ins}/setup/curve/descrip -v $target
	      }
	    CAMP_STRING /~/setup/curve/file -D -S -T "Curve datafile" -d on -s on \
		-writeProc lake330_curve_file_w
	      proc lake330_curve_file_w { ins target } {
		    set file $target
		    if { [string match "*.spl" $file] != 1 } {
			return -code error "invalid file type $file"
		    }
		    varDoSet /${ins}/setup/curve/file -v $file
	      }
	    CAMP_SELECT /~/setup/curve/do_load -D -S -T "Do curve load" \
		-H "Start loading the curve" \
		-d on -s on -selections DONE LOADING \
		-writeProc lake330_curve_do_w
	      proc lake330_curve_do_w { ins target } {
		varDoSet /${ins}/setup/curve/do_load -v $target
		if { $target == 1 } {
		  # Do asynchronously by polling controls/do_load
		  varDoSet /${ins}/controls/do_load -p on -p_int 0.1
		}
	      }
    CAMP_STRUCT /~/controls -D -d on
	CAMP_SELECT /~/controls/A_load -D -P -R -T "Load A curve" \
	    -H "Called (polled) asynchronously by setup/A_curve" \
	    -d on -r on -selections DONE LOADING \
	    -readProc {lake330_controls_AB_load_r A}
	  proc lake330_controls_AB_load_r { channel ins } {
	    varDoSet /${ins}/controls/${channel}_load -v 1 -p off
	    set status [catch {lake330_set_curve $ins $channel [varGetVal /${ins}/setup/${channel}_curve]} msg]
	    varDoSet /${ins}/controls/${channel}_load -v 0
	    if { $status == 0 } {
	      varDoSet /${ins}/controls/${channel}_read -p on -p_int 0.1
            }
	  }
	CAMP_SELECT /~/controls/A_read -D -P -T "Read A curve" \
	    -H "Readback check of A curve" \
	    -d on -selections DONE READING \
	    -readProc {lake330_controls_AB_read_r A}
	  proc lake330_controls_AB_read_r { channel ins } {	
	    varDoSet /${ins}/controls/${channel}_read -v 1 -p off
	    if { [catch {lake330_read_curve $ins $channel} mes] } {
		varDoSet /${ins}/setup/${channel}_curve -m "Curve Read Failure: $mes"
	    } else {
		varDoSet /${ins}/setup/${channel}_curve -m ""
	    }
	    varDoSet /${ins}/controls/${channel}_read -v 0
	  }
	CAMP_SELECT /~/controls/B_load -D -P -T "Load B curve" \
	    -H "Called (polled) asynchronously by setup/B_curve" \
	    -d on -selections DONE LOADING \
	    -readProc {lake330_controls_AB_load_r B}
	CAMP_SELECT /~/controls/B_read -D -P -T "Read B curve" \
	    -H "Readback check of B curve" \
	    -d on -selections DONE READING \
	    -readProc {lake330_controls_AB_read_r B}

	CAMP_SELECT /~/controls/do_load -D -P -R -T "Do load curve" \
	    -H "Performs curve loading asynchronously" \
	    -d on -r on -selections DONE LOADING \
	    -readProc lake330_controls_do_load_r
	  proc lake330_controls_do_load_r { ins } {
	    varDoSet /${ins}/setup/curve/do_load -v 1
	    set file [varGetVal /${ins}/setup/curve/file]
	    set f [open $file r]
	    set curve_data ""
	    # read first line (has the number of data points)
	    if { [gets $f line] == -1 } {
		close $f
		return -code error "unexpected end of file"
	    }
	    if { [scan $line " %d" npt] < 1 || $npt < 2 || $npt > 97 } {
		close $f
		return -code error "bad num points: $line"
	    }
	    # read ALL the points (so we can close the file)
	    set lines [split [string trim [read $f]] "\n"]
	    close $f
	    if { [llength $lines] != $npt } {
		return -code error "incorrect number of points: [llength $lines] vs $npt"
	    }
	    set last [expr {$npt-1}]
	    # Examine first two points
	    set t0 0; set v0 0; set t1 0; set v1 0
	    for { set i 0 } { $i < 2 } { incr i } {
		if { [scan [lindex $lines $i] " %f %f" t$i v$i] < 2 } {
		    return -code error "bad file format"
		}
	    }
	    # Determine whether ascending and maximum temperature
	    if { $t0 < $t1 } {
		set asc 1
		if { [scan [lindex $lines $last] " %f %f" tmax v] < 2 } {
		    return -code error "bad file format"
		}
	    } else {
		set asc -1
		set tmax $t0
	    }
	    # Determine max-temp code
	    set code 0
	    foreach t {330 380 480 810 999} {
		if { $t<$tmax } {
		    incr code
		}
	    }
	    if { $v0 < $v1 } {
		return -code error "bad ordering in curve file"
	    } else {
		set line [format "%.5f,%.1f,%.5f,%.1f" $v1 $t1 $v0 $t0]
	    }
	    set num [varGetVal /${ins}/setup/curve/number]
	    set descrip [varGetVal /${ins}/setup/curve/descrip]
	    # delete the curve in memory
	    insIfWrite /${ins} [format "KCUR%2d" $num]
	    # send the first two points
	    set cmd [format "CURV%2d,S%1d0 %s,%s*" $num $code $descrip $line]
	    insIfWrite /${ins} $cmd
	    set cmd_line ""
	    set s ""
            switch -glob [insGetIfTypeIdent /$ins] {
              gpib* { set clen 22 }
              default { set clen 22 }
            }
	    foreach line [lrange $lines 2 $last] {
		if { [scan $line " %f %f" t v] < 2 || $v>=$v0 || $asc*$t<$asc*$t0 } {
		  insIfWrite /${ins} [format "KCUR%2d" $num]
		  return -code error "bad file format"
		}
		set t0 $t
		set v0 $v
		append cmd_line $s [format "ECUR%2d,%.5f,%.1f" $num $v $t]
		set s ";"
		if { [string length $cmd_line] > $clen } {
		  insIfWrite /${ins} $cmd_line
		  set cmd_line ""
		  set s ""
		}
	    }
	    if { [string length $cmd_line] } { insIfWrite /${ins} $cmd_line }
	    varDoSet /${ins}/controls/do_load -v 0
	    varDoSet /${ins}/setup/curve/do_load -v 0
	  }
    CAMP_STRUCT /~/panic -D -d on
	CAMP_SELECT /~/panic/reset -D -S -T "Reset" -d on -s on \
	    -selections DO \
	    -writeProc lake330_reset_w
	  proc lake330_reset_w { ins target } {	insIfWrite /${ins} "*RST" }
	CAMP_SELECT /~/panic/test -D -R -T "Self-test" -d on -r on \
	    -selections OK ERROR UNKNOWN \
	    -readProc lake330_test_r
	  proc lake330_test_r { ins } {
		set test [insIfRead /${ins} "*TST?" 16]
		switch $test {
		    0 {}
		    1 {}
		    default { set test 2 }
		}
		varDoSet /${ins}/panic/test -v $test
		if { $test != 0 } { insSet /${ins} -line off }
	    }

proc lake330_set_curve { ins channel curve } {
    varDoSet /${ins}/setup/${channel}_curve -m "Read curve in preparation of loading"
    if { $curve < 0 } {
	varDoSet /${ins}/setup/${channel}_curve -units "FAIL \"$curve\"" \
		-m "bad curve number \"$curve\""
	return -code error "bad value \"$curve\""
    } elseif { $curve <= 31 } {
	set number $curve
    } else {
	set buf [glob ./dat/*.spl]
	set found 0
	foreach file $buf {
	    set root [file rootname [file tail $file]]
	    if { $root == $curve } { set found 1; break }
	}
	if { $found == 0 } {
	  varDoSet /${ins}/setup/${channel}_curve -units "NO FILE" -m \
		"Failed to find a data file for curve \"$curve\". Try again."
	  return -code error "couldn't find curve \"$curve\"" 
	}
	varDoSet /${ins}/setup/${channel}_curve -units "LOAD" -m \
		"Loading curve \"$curve\""
	set number 11
	set other_channel A
	if { $channel == $other_channel } { set other_channel B }
	set buf [insIfRead /${ins} "${other_channel}CUR?" 32]
	set buf [string trim $buf " \t\r\n"]
	set other_curve [string trimleft $buf "0"]
	if { $number == $other_curve } { incr number }
	varDoSet /${ins}/setup/${channel}_curve -units "LOAD" -m \
		"Loading curve \"$curve\" as number ${number}"
	varSet /${ins}/setup/curve/number -v $number
	varSet /${ins}/setup/curve/descrip -v [format "%d" $curve]
	set file [format "./dat/%d.spl" $curve]
	varSet /${ins}/setup/curve/file -v $file
	#
	# Reading controls/do_load actually loads the curve!
	#
	if { [catch { lake330_controls_do_load_r ${ins} } mes] } {
	    varDoSet /${ins}/setup/${channel}_curve -units "ERROR" -m "Load Failure: $mes"
	    return -code error "Load Failure $mes"
	}
    }
#   Verify setting of curve number.  Since the Lakeshore330 will not switch 
#   to a defective curve, this check also verifies the load.  We will later
#   read out the curve to check more fully.
    varDoSet /${ins}/setup/${channel}_curve -units "VERIFY" -m \
		"verifying the download..."
    insIfWrite /${ins} "${channel}CUR$number"
    set buf [insIfRead /${ins} "${channel}CUR?" 32]
    if { [scan $buf " %d %c" other_curve addn] != 1 || $number != $other_curve } {
	varDoSet /${ins}/setup/${channel}_curve -units "FAIL" -m \
		"failed to select curve number \"$number\" for channel \"$channel\""
	return -code error "failed to select curve number \"$number\" for channel \"$channel\""
    }
}
proc lake330_read_curve { ins channel } {
    set number -1
    set buf [insIfRead /${ins} "${channel}CUR?" 32]
    catch { set number [expr "$buf"] }
    if { !( ($number >= 0) && ($number <= 31) ) } {
	varDoSet /${ins}/setup/${channel}_curve -units "FAIL"
	return -code error "failed to read curve number \"$number\" for channel \"$channel\""
    } elseif { $number < 11 } {
	set curve $number
    } else {
	set buf [insIfRead /${ins} "CURV?$number" 2000]
	set buf [string trim $buf]
	if { [string length $buf]%14 != 12 } {
	  varDoSet /${ins}/setup/${channel}_curve -v -1
	  return -code error "curve readback truncated"
	}
	if { [scan $buf {%d,%*s %d ,%[NP],%d} nn curve tc np] != 4 } { 
	  varDoSet /${ins}/setup/${channel}_curve -v -1
          return -code error "failed reading back curve header"
	}
	#if { $nn != $number } { 
	#  varDoSet /${ins}/setup/${channel}_curve -v -1
        #  return -code error "mismatched curve number $nn vs $number"
	#}
	set file [format "./dat/%d.spl" $curve]
	set f [open $file r]
	set curve_data ""
	# read first line (has the number of data points)
	if { [gets $f line] == -1 } {
	  close $f
	  varDoSet /${ins}/setup/${channel}_curve -v $number
	  return -code error "unexpected end of file in $curve.spl"
	}
	set n 0
	if { [scan $line " %d" n] != 1 } {
	  close $f
	  varDoSet /${ins}/setup/${channel}_curve -v $number
	  return -code error "invalid $curve.spl format: no number of points"
	}
        if { $n != $np-2 } {
          close $f
	  varDoSet /${ins}/setup/${channel}_curve -v $number -units "FAIL"
          return -code error "curve $curve has $n points, but readback header says ${np}-2"
	}
	set tf 0; set vf 0
	set r0 [expr {$n*14 + 27}]
        if { [string length $buf] < [expr {$r0+12}] } {
          close $f
	  varDoSet /${ins}/setup/${channel}_curve -v $number -units "FAIL"
          return -code error "curve data in memory is too short; expected $n points for curve $curve"
	}
	for { set i 0 } { $i < $n } { incr i } {
	  if { [scan [string range $buf $r0 [expr {$r0+12}]] "%f, %f" v t] < 2 } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v $number -units "FAIL"
	    return -code error "failed parse of curve readback point $i"
	  }
	  incr r0 -14
	  if { [gets $f line] == -1 } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v $number -units "FAIL"
	    return -code error "unexpected end of file in $curve.spl"
	  }
	  if { [scan $line " %f %f" tf vf] < 2 } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v $number -units "FAIL"
	    return -code error "bad file format: while scanning data line $line"
	  }
	  set vf [format "%.5f" $vf]
	  set tf [format "%.1f" $tf]
	  if { $vf != $v || $tf != $t } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v $number -units "FAIL"
	    return -code error [format "inconsistent point at %d: %.5f,%.1f vs file %.5f,%.1f" \
		    [incr i] $v $t $vf $tf
	  }
	}
	close $f
    }
    varDoSet /${ins}/setup/${channel}_curve -v $curve -units "Num.$number" -m ""
}
