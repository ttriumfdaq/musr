CAMP_INSTRUMENT /~ -D -T "LakeShore 331" \
    -H "LakeShore 331 Autotuning Temperature Controller" -d on \
    -initProc lake331_init -deleteProc lake331_delete \
    -onlineProc lake331_online -offlineProc lake331_offline
  proc lake331_init { ins } {
	insSet /${ins} -if gpib 0.3 2 16 CRLF CRLF
#       insSet /${ins} -if rs232 0.5 2 /tyCo/2 9600 7 odd 1 CRLF CRLF
  }
  proc lake331_delete { ins } {
	insSet /${ins} -line off
  }

# For the RS232 interface, only the baud is adjustable, so try forcing the
# correct 7 bits, odd parity, CRLF, CRLF settings before failing. (We can't 
# set good defaults for rs232 because gpib is the default.) 
# For gpib, force the termination to match what the instrument tells.
# Only certain reads and writes fail with wrong terminator settings, so
# check one readback of a single built-in curve point (see table D-1).
# ACK! Individual units have different points, not listed in table D-1 !
# The same for other curves!  Must compare to multiple points.  If
# the terminator test fails, leave instrument online because it will 
# *mostly* work.
 
  proc lake331_online { ins } {
      insIfOn /${ins}
      varRead /${ins}/setup/id
      set iftype [insGetIfTypeIdent /${ins}]
      set iface  [insGetIf /${ins}]
      if { [string match rs232* $iftype] } {
          if { [varGetVal /${ins}/setup/id] == 0 || \
               [lindex $iface 2] != 7 || [lindex $iface 3] != "odd" || \
               [lindex $iface 4] != "CRLF" || [lindex $iface 5] != "CRLF" } {
              insIfOff /${ins}
              set iface [lreplace $iface 2 5 7 odd CRLF CRLF]
              set icmd [concat insSet /$ins -if $iftype [insGetIfDelay /$ins] [insGetIfTimeout /$ins] $iface]
              expr "\[$icmd\]"
              insIfOn /${ins}
              varRead /${ins}/setup/id
              # Would like to warn that interface was auto-corrected, but don't want
              # to raise an error.
          }
      }
      if { [varGetVal /${ins}/setup/id] == 0 } {
          insIfOff /${ins}
          return -code error "failed ID query, check interface definition and connections"
      }
      varRead /${ins}/setup/control_mode
# 20140411  TEMP: need control of terminator setting
#if { 0 } {
    if { [string match gpib* $iftype] } {
          catch {
              scan [insIfRead /${ins} {IEEE?} 32] " %d,%d,%d" t e a
              set ts [lindex {CRLF LFCR LF none} $t]
              if { $ts != [lindex $iface 1] || $ts != [lindex $iface 2] } {
                  insIfOff /${ins}
                  set iface [lreplace $iface 1 2 $ts $ts]
                  set icmd [concat insSet /$ins -if $iftype [insGetIfDelay /$ins] [insGetIfTimeout /$ins] $iface]
                  expr "\[$icmd\]"
                  insIfOn /${ins}
                  varRead /${ins}/setup/id
                  # Would like to warn that interface was auto-corrected, but don't want
                  # to raise an error.
              }
          }
      }
#}
      # Do a final check with CRVPT
      if { [catch {insIfRead /${ins} {CRVPT? 1,3} 32} buf] || \
           [scan $buf " %f, %f" V T] != 2 || \
           ! ( ($T == 475.0 && abs($V-0.09062) < 0.0001) || \
               ($T == 470.0 && abs($V-0.10191) < 0.0001) || \
               ($T == 465.0 && abs($V-0.11356) < 0.0001) ) } {
          # insIfOff /${ins}
          return -code error "probable terminator mismatch, check interface definition"
      }
  }
  proc lake331_offline { ins } {
	insIfOff /${ins}
  }
    CAMP_FLOAT /~/read_A -D -R -P -L -A -T "Channel A reading" \
	-d on -r on -units K -tol 0.5 \
	-readProc lake331_read_A_r
      proc lake331_read_A_r { ins } {
	set u [lindex {K C S} [varGetVal /${ins}/setup/A_units]]
	insIfReadVerify /${ins} "${u}RDG? A" 48 /${ins}/read_A " %f" 2
	if { [varGetVal /${ins}/setup/control_chan] == 0 } {
	    varTestAlert /${ins}/read_A [varGetVal /${ins}/setpoint]
	}
      }
    CAMP_FLOAT /~/read_B -D -R -P -L -A -T "Channel B reading" \
	-d on -r on -units K -tol 0.5 \
	-readProc lake331_read_B_r
      proc lake331_read_B_r { ins } {
	set u [lindex {K C S} [varGetVal /${ins}/setup/B_units]]
	insIfReadVerify /${ins} "${u}RDG? B" 48 /${ins}/read_B " %f" 2
	if { [varGetVal /${ins}/setup/control_chan] == 1 } {
	    varTestAlert /${ins}/read_B [varGetVal /${ins}/setpoint]
	}
      }
    CAMP_FLOAT /~/setpoint -D -R -S -L -T "Control set point" \
	-d on -r on -s on -units K \
	-readProc lake331_setpoint_r -writeProc lake331_setpoint_w
      proc lake331_setpoint_r { ins } {
	insIfReadVerify /${ins} "SETP? 1" 48 /${ins}/setpoint " %f" 2
      }
      proc lake331_setpoint_w { ins target } {
	  set msg ""
        if { [catch {
	    insIfWriteVerify /${ins} "SETP 1,$target" "SETP? 1" 48 \
		    /${ins}/setpoint " %f" 2 $target [expr {abs($target)/900.0}]
        } msg ] } {
            varRead /${ins}/ramp
            if { [varGetVal /${ins}/ramp] } {
                varDoSet /${ins}/ramp_status -p on -p_int 5
            } else {
                return -code error $msg
            }
        }
      }
    CAMP_SELECT /~/heat_range -D -R -S -P -T "Heater range" \
	-d on -r on -s on -p off -selections OFF LOW MED HIGH \
	-readProc lake331_heat_range_r -writeProc lake331_heat_range_w
      proc lake331_heat_range_r { ins } {
	    varDoSet /${ins}/heat_range -p off
	    insIfReadVerify /${ins} "RANGE?" 48 /${ins}/heat_range " %d" 2
      }
      # The 331 takes 3 seconds to detect a heater error after setting
      # the heater range, so we can't confirm a setting directly.  Therefore, 
      # check heater status *before* setting range (to check for connection
      # problems), then confirm the setting by a single poll of heat_range
      # If we are setting current manually, re-apply setting for new heater 
      # range.
      proc lake331_heat_range_w { ins target } {
          set err 0
          scan [insIfRead /${ins} {HTRST?} 48] { %d} err
          insIfWrite /${ins} "RANGE $target"
          varDoSet /${ins}/heat_range -p on -p_int 4
          if { $err } {
              return -code error [lindex {{} {Heater open load} {Heater Short}} $err]
          }
          varDoSet /${ins}/heat_range -v $target
          if { [varGetVal /${ins}/setup/control_mode] == 3 } {
              set sc [varGetVal /${ins}/set_current]
              if { $sc > [lindex {0.0 0.1 0.3 1.0} $target] } {
                  return -code error "Warning: set_current cannot be maintained in this heat_range"
              } else {
                  catch { lake331_setcurr_int $target $ins $sc }
              }
          }
      }
    CAMP_FLOAT /~/current_read -D -R -L -P -A -T "Output current" \
	-d on -r on -p off -units A -tol 0.001 \
	-H "Heater output in Amps.  (Full output on High range is 1.0 A)" \
	-readProc lake331_current_read_r 
      proc lake331_current_read_r { ins } {
          varRead /${ins}/heat_range
          set heat_range [lindex {0.0 0.1 0.3 1.0} [varGetVal /${ins}/heat_range] ]
          set buf [insIfRead /${ins} "HTR?" 48]
          if { [scan $buf " %d" percent] != 1 } { set percent -1.0 }
          if { ( $percent < 0.0 ) || ( $percent > 100.0 ) } {
              return -code error "failed parsing \"$buf\" from HTR? command"
          }
          varDoSet /${ins}/current_read -v [expr {$heat_range*$percent/100.0}]
          if { [varGetVal /${ins}/setup/control_mode] == 3 } {
              varTestAlert /${ins}/current_read [varGetVal /${ins}/set_current]
          }
      }

#  IMPORTANT NOTE:
#  The Lakeshore 331 "manual heater power" (MHP) is supposed to be set by the MOUT 
#  command, but testing shows that MOUT actually sets the output CURRENT (as a 
#  percentage of full range).

    CAMP_FLOAT /~/set_current -D -R -S -L -T "Set Output Current" \
	-d off -r on -s on -units "A" \
        -H "Constant heater current setting (only when not controlling temperature; see setup/control_mode)." \
	-readProc lake331_setcurr_r -writeProc lake331_setcurr_w
      proc lake331_setcurr_r { ins } {
          varRead /${ins}/heat_range
          set hr [varGetVal /${ins}/heat_range]
          set buf [insIfRead /${ins} "MOUT? 1" 48]
          if { [scan $buf " %f" pct] != 1 } {
              return -code error "Invalid percent: $buf"
          }
          set curr [expr { [lindex {0 0.1 0.3 1.0} $hr] * $pct/100.0}]
          varDoSet /${ins}/set_current -v $curr
      }
      proc lake331_setcurr_w { ins target } {
          if { $target < 0.0 || $target > 1.0 } {
              return -code error "Output current must be in range 0.0 to 1.0 A"
          }
          varRead /${ins}/heat_range
          lake331_setcurr_int [varGetVal /${ins}/heat_range] $ins $target
      }
      proc lake331_setcurr_int { hr ins target } {
          set max [lindex {0.1e-08 0.1 0.3 1.0} $hr]
          if { $target > $max } {
              return -code error "Requested current is beyond the '[lindex {OFF LOW MED HIGH} $hr]' heat range."
          }
          set pct [expr { 100.0 * $target / $max }]
          set rb -2
          scan [insIfRead /${ins} "MOUT 1,$pct;MOUT? 1" 48] " %f" rb
          if { abs($rb-$pct) > 0.1 } {
              scan [insIfRead /${ins} "MOUT 1,$pct;MOUT? 1" 48] " %f" rb
          }
          if { abs($rb-$pct) > 0.1 } {
              return -code error "Setting did not stick: $rb vs $pct %"
          }
          varDoSet /${ins}/set_current -v $target
      }
    CAMP_SELECT /~/ramp -D -S -R -P -T "Ramp" \
	-d on -s on -r on -selections DISABLED ENABLED \
	-H "Enable or disable slow ramping of setpoint" \
	-readProc lake331_ramp_r -writeProc lake331_ramp_w
      proc lake331_ramp_r { ins } {
          set buf [insIfRead /${ins} "RAMP? 1" 48]
          if { [scan $buf " %d, %f" en rate] < 2 } {
              return -code error "Invalid Ramp reading $msg"
          }
          varDoSet /${ins}/ramp -v $en
          varDoSet /${ins}/ramp_rate -v $rate
        }
      proc lake331_ramp_w { ins target } {
          lake331_ramp_r $ins
          set cmd [format "RAMP 1,%d,%.1f" $target [varGetVal /${ins}/ramp_rate]]
          insIfWriteVerify /${ins} $cmd "RAMP? 1" 48 /${ins}/ramp " %d," 2 $target
	}
    CAMP_SELECT /~/ramp_status -D -R -P -T "Ramp status" \
	-d on -r on -p off -p_int 5 -selections DONE RAMPING \
	-readProc lake331_ramp_status_r
      proc lake331_ramp_status_r { ins } {
	    insIfReadVerify /${ins} "RAMPST? 1" 32 /${ins}/ramp_status " %d" 2
	    varRead /${ins}/setpoint
	    if { [varGetVal /${ins}/ramp_status] } {
		varDoSet /${ins}/ramp_status -p on -p_int 5
	    } else {
		varDoSet /${ins}/ramp_status -p off
	    }
	}
    CAMP_FLOAT /~/ramp_rate -D -S -R -L -P -T "Ramp rate" \
	-d on -s on -r on -p off -units {K/min} \
	-H "Temperature (setpoint) ramp rate (if ramp is enabled) in Kelvin per minute (from 0.1 to 99.9, or 0 for off)" \
	-readProc lake331_ramp_r -writeProc lake331_ramp_rate_w
      proc lake331_ramp_rate_w { ins target } {
          if { $target < 0.0 || $target > 99.91 } { 
              return -code error "Invalid ramp rate.  Must be 0 to 99.9 (K/min)"
          }
          set en [expr {$target > 0.0}]
          set target [format "%5.1f" $target]
          set cmd [format "RAMP 1,%d,%.1f" $en $target]
          insIfWriteVerify /${ins} $cmd "RAMP? 1" 48 /${ins}/ramp_rate " %*d, %f" 2 $target
          varDoSet /${ins}/ramp -v $en
      }


################################################################################
    CAMP_STRUCT /~/alarms -D -d on -T "Alarms and Relays"

        CAMP_SELECT /~/alarms/alarm_A -D -R -P -A -T "A Alarm state" \
                -d on -r on -p off -p_int 5 -a off \
                -H "Alarm state for input A" \
                -selections OK Low_Trip Hi_Trip Both_Trip \
                -v 0 -readProc {lake331_alarmst_r A}

        CAMP_SELECT /~/alarms/alarm_B -D -R -P -A -T "B Alarm state" \
                -d on -r on -p off -p_int 5 -a off \
                -H "Alarm state for input B" \
                -selections OK Low_Trip Hi_Trip Both_Trip \
                -v 0 -readProc {lake331_alarmst_r B}

             proc lake331_alarmst_r { ch ins } {
                 set buf [insIfRead /$ins "ALARMST? $ch" 16]
                 if { [scan $buf " %d, %d" hi lo] != 2 } {
                     return -code error "invalid status $buf"
                 }
                 varDoSet /${ins}/alarms/alarm_$ch -v [expr {$lo + 2*$hi}]
                 varTestAlert /${ins}/alarms/alarm_$ch 0
             }

        CAMP_SELECT /~/alarms/reset -D -S -T "Reset alarms" \
                -d on -s on -selections "" RESET -v 0 \
                -writeProc lake331_alarmreset_w

             proc lake331_alarmreset_w { ins target } {
                 if { $target } {
                     insIfWrite /$ins "ALMRST"
                 }
             }

        CAMP_SELECT /~/alarms/beep -D -S -T "Alarm beeps" \
                -d on -s on -selections Quiet Beep \
                -readProc lake331_alarmbeep_r -writeProc lake331_alarmbeep_w

             proc lake331_alarmbeep_w { ins target } {
                 insIfWriteVerify /${ins} "BEEP $target" "BEEP?" 48 \
		    /${ins}/alarms/beep " %d" 2 $target 
             }

    foreach inch {A B} {

        CAMP_STRUCT /~/alarms/cfg_${inch} -D -d on -T "Configure Alarm ${inch}"

            CAMP_SELECT /~/alarms/cfg_${inch}/enable -D -S -R -T "Enable ${inch} Alarm" \
                -d on -s on -r on -selections Disable Enable \
                -readProc [list lake331_almcfg_r ${inch}] -writeProc [list lake331_almcfg_w ${inch} enable]
            CAMP_FLOAT /~/alarms/cfg_${inch}/low_limit -D -S -R -T "${inch} low limit" -d on -s on -r on \
                -readProc [list lake331_almcfg_r ${inch}] -writeProc [list lake331_almcfg_w ${inch} low_limit]
            CAMP_FLOAT /~/alarms/cfg_${inch}/high_limit -D -S -R -T "${inch} high limit" -d on -s on -r on \
                -readProc [list lake331_almcfg_r ${inch}] -writeProc [list lake331_almcfg_w ${inch} high_limit]
            CAMP_FLOAT /~/alarms/cfg_${inch}/deadband -D -S -R -T "${inch} deadband" -d on -s on -r on \
                -readProc [list lake331_almcfg_r ${inch}] -writeProc [list lake331_almcfg_w ${inch} deadband]
            CAMP_SELECT /~/alarms/cfg_${inch}/latching -D -S -R -T "${inch} alarm latching" \
                -d on -s on -r on -selections reset latch \
                -readProc [list lake331_almcfg_r ${inch}] -writeProc [list lake331_almcfg_w ${inch} latching]
    }

            proc lake331_almcfg_r { ch ins } {
                set buf [insIfRead /$ins "ALARM? $ch" 80]
                if { [scan $buf " %d,%d,%f,%f,%f,%d" enable source high low dead latch] != 6 } {
                    return -code error "invalid alarms config $buf"
                }
                varDoSet /${ins}/alarms/cfg_${ch}/enable -v $enable
                varDoSet /${ins}/alarms/cfg_${ch}/low_limit -v $low
                varDoSet /${ins}/alarms/cfg_${ch}/high_limit -v $high
                varDoSet /${ins}/alarms/cfg_${ch}/deadband -v $dead
                varDoSet /${ins}/alarms/cfg_${ch}/latching -v $latch
            }
            proc lake331_almcfg_w { ch var ins val } {
                set source [expr { [varGetVal /${ins}/setup/${ch}_units] + 1 }]
                set enable [varGetVal /${ins}/alarms/cfg_${ch}/enable]
                set low_limit [varGetVal /${ins}/alarms/cfg_${ch}/low_limit]
                set high_limit [varGetVal /${ins}/alarms/cfg_${ch}/high_limit]
                set deadband [varGetVal /${ins}/alarms/cfg_${ch}/deadband]
                set latching [varGetVal /${ins}/alarms/cfg_${ch}/latching]
                set $var $val
                insIfWrite /${ins} "ALARM $ch,$enable,$source,$high_limit,$low_limit,$deadband,$latching"
                varRead /${ins}/alarms/cfg_${ch}/$var
            }

	CAMP_SELECT /~/alarms/relay_1_set -D -R -S -T "Relay 1 setup" \
             -d on -s on -r on -selections Off On A_low A_high A_alarm B_low B_high B_alarm \
             -readProc {lake331_relayact_r 1} -writeProc {lake331_relayact_w 1}
	CAMP_SELECT /~/alarms/relay_2_set -D -R -S -T "Relay 2 setup" \
             -d on -s on -r on -selections Off On A_low A_high A_alarm B_low B_high B_alarm \
             -readProc {lake331_relayact_r 2} -writeProc {lake331_relayact_w 2}

         proc lake331_relayact_r { rel ins } {
             set buf [insIfRead /$ins "RELAY? $rel" 48]
             if { [scan $buf { %d,%[AB],%d} mode input type] != 3 } {
                    return -code error "invalid relay config $buf"
             }
             if { $mode == 2 } {
                 incr mode $type
                 if { $input == "B" } { incr mode 3 }
             }
             varDoSet /${ins}/alarms/relay_${rel}_set -v $mode
         }
         proc lake331_relayact_w { rel ins target } {
             if { $target < 2 } {
                 set buf [insIfRead /$ins "RELAY? $rel" 48]
                 if { [scan $buf { %d,%[AB],%d} mode input type] != 3 } {
                     return -code error "invalid relay config $buf"
                 }
                 set mode $target
             } else {
                 set mode 2
                 set input [expr { $target < 5 ? "A" : "B" }]
                 set type [expr { ($target-2) % 3 }]
             }
             insIfWrite /$ins "RELAY $rel,$mode,$input,$type"
             varDoSet /${ins}/alarms/relay_${rel}_set -v $target
         }

################################################################################
    CAMP_STRUCT /~/setup -D -d on -T "Setup parameters"
	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE \
	    -readProc lake331_id_r
	  proc lake331_id_r { ins } {
		set id 0
		set status [catch {insIfRead /${ins} "*IDN?" 48} buf]
		if { $status == 0 } {
		    set id [scan $buf { LSCI,MODEL33%*[1S],%*d,%d} val]
		    if { $id != 1 } { set id 0 }
		}
		varDoSet /${ins}/setup/id -v $id
	  }
	CAMP_SELECT /~/setup/control_chan -D -R -S -T "Control channel" \
	    -d on -r on -s on -selections A B \
	    -readProc lake331_control_chan_r -writeProc lake331_control_chan_w
	  proc lake331_control_chan_r { ins } {
                set buf [insIfRead /${ins} {CSET? 1} 48]
                if { [scan $buf { %[AB], %d,} cch cun] != 2 } {
                    return -code error $buf
                }
                varDoSet /${ins}/setup/control_chan -v $cch
                set run [varGetVal /${ins}/setup/${cch}_units]
                if { $run+1 != $cun } {
                    varSet /${ins}/heat_range -v 0
                    return -code error "DANGER: Setpoint units do not match channel $cch units"
                }
		varDoSet /${ins}/setpoint -m "Setpoint, controlling on channel $cch" \
                    -units [varNumGetUnits /${ins}/read_${cch}]
	  }
	  proc lake331_control_chan_w { ins target } {
		set chan [lindex {A B} $target]
		set unit [varGetVal /${ins}/setup/${chan}_units]
                insIfReadVerify /${ins} "CSET 1,${chan},[incr unit],0,1; CSET? 1" 48 \
		    /${ins}/setup/control_chan { %[AB]} 2 $chan
		varDoSet /${ins}/setpoint -m "Setpoint, controlling on channel $target" \
                    -units [varNumGetUnits /${ins}/read_${chan}]

	  }
	CAMP_SELECT /~/setup/control_mode -D -R -S -T "Control mode" \
	    -d on -r on -s on -selections "" "Manual PID" "Zone PID" "Constant Out" "Tune PID" "Tune PI" "Tune P" \
	    -readProc lake331_control_mode_r -writeProc lake331_control_mode_w
	  proc lake331_control_mode_r { ins } {
		insIfReadVerify /${ins} "CMODE? 1" 48 /${ins}/setup/control_mode " %d" 2
		if { [varGetVal /${ins}/setup/control_mode] == 3 } {
		    varDoSet /${ins}/set_current -d on
		} else {
		    varDoSet /${ins}/set_current -d off
		}
	  }
	  proc lake331_control_mode_w { ins target } {
		if { $target == 0 } { return }
		insIfWriteVerify /${ins} "CMODE 1,${target}" "CMODE? 1" 48 /${ins}/setup/control_mode " %d" 2 $target
		if { [varGetVal /${ins}/setup/control_mode] == 3 } {
		    varDoSet /${ins}/set_current -d on
		} else {
		    varDoSet /${ins}/set_current -d off
		}
	  }
	CAMP_FLOAT /~/setup/P -D -R -S -T "Gain setting (P)" \
	    -d on -r on -s on \
	    -readProc lake331_PID_r -writeProc lake331_P_w
	  proc lake331_PID_r { ins } {
		set buf [insIfRead /${ins} "PID? 1" 48]
		if { [scan $buf { %f,%f,%f} p i d ] < 3 } {
		    return -code error "Bad PID Readback \"$buf\""
		}
		varDoSet /${ins}/setup/P -v $p
		varDoSet /${ins}/setup/I -v $i
		varDoSet /${ins}/setup/D -v $d
	  } 
	  proc lake331_P_w { ins target } {
		if { $target < 0 || $target > 999 } {
		    return -code error "invalid value \"$target\""
		}
		lake331_PID_r $ins
		insIfWrite /${ins} "PID 1,${target},[varGetVal /${ins}/setup/I],[varGetVal /${ins}/setup/D]"
		lake331_PID_r $ins
	  }
	CAMP_FLOAT /~/setup/I -D -R -S -T "Reset setting (I)" \
	    -d on -r on -s on \
	    -readProc lake331_PID_r -writeProc lake331_I_w
	  proc lake331_I_w { ins target } {
		if { $target < 0 || $target > 999 } {
		    return -code error "invalid value \"$target\""
		}
		lake331_PID_r $ins
		insIfWrite /${ins} "PID 1,[varGetVal /${ins}/setup/P],${target},[varGetVal /${ins}/setup/D]"
		lake331_PID_r $ins
	  }
	CAMP_FLOAT /~/setup/D -D -R -S -T "Rate setting (D)" \
	    -d on -r on -s on \
	    -readProc lake331_PID_r -writeProc lake331_D_w
	  proc lake331_D_w { ins target } {
		if { $target < 0 || $target > 200 } {
		    return -code error "invalid value \"$target\""
		}
		lake331_PID_r $ins
		insIfWrite /${ins} "PID 1,[varGetVal /${ins}/setup/P],[varGetVal /${ins}/setup/I],${target}"
		lake331_PID_r $ins
	  }
	CAMP_SELECT /~/setup/A_units -D -S -T "Channel A units" \
	    -d on -s on -v 0 -selections K C Raw \
	    -writeProc {lake331_units_w A}
	CAMP_SELECT /~/setup/B_units -D -S -T "Channel B units" \
	    -d on -s on -v 0 -selections K C Raw \
	    -writeProc {lake331_units_w B}
	  proc lake331_units_w { chan ins target } {
	    set u [lindex {K C Raw} $target]
	    if { $target == 2 } {# Raw = sensor units
		varRead /${ins}/setup/${chan}_sensor
		switch [varGetVal /${ins}/setup/${chan}_sensor] {
		    0 -
		    1 { set u V }
		    default { set u Ohms }
		}
	    }
	    varDoSet /${ins}/setup/${chan}_units -v $target
	    varDoSet /${ins}/read_${chan} -units $u -z
	    if { [varSelGetValLabel /${ins}/setup/control_chan] == $chan } {
		varDoSet /${ins}/setpoint -units $u
		insIfWrite /${ins} "CSET 1,${chan},[incr target],0,2"
	    }
	  }
	CAMP_SELECT /~/setup/A_sensor -D -S -R -T "Channel A input type" \
	    -d on -s on -r on -selections "Si" "GaAlAs" "PtR100" "PtR1000" "Cernox/CGR" "TC" \
	    -readProc {lake331_sensor_r A} -writeProc {lake331_sensor_w A}
	CAMP_SELECT /~/setup/B_sensor -D -S -R -T "Channel B input type" \
	    -d on -s on -r on -selections "Si" "GaAlAs" "PtR100" "PtR1000" "Cernox/CGR" "TC" \
	    -readProc {lake331_sensor_r B} -writeProc {lake331_sensor_w B}
	  proc lake331_sensor_r { chan ins } {
		set buf [insIfRead /${ins} "INTYPE? ${chan}" 48]
		if { [scan $buf { %d} it] } {
		    varDoSet /${ins}/setup/${chan}_sensor -v [lindex {0 1 2 2 3 4 5 5} $it]
		}
	    }
	  proc lake331_sensor_w { chan ins target } {
	      lake331_sensor_r $chan $ins
	      set it [lindex {0 1 3 4 5 7} $target]
	      if { $it >= 2 && $it <= 4 } {
		  # Platinum selected -- first check with a low excitation (pretend Si)
		  set volts 9.
		  set okv [scan [insIfRead /${ins} "INTYPE ${chan},0,0; SRDG? ${chan}" 48] { %f} volts ]
		  if { $volts == 0.0 || $volts > 5.0 || $okv < 1 } {
		      set okv [scan [insIfRead /${ins} "INTYPE ${chan},0,0; SRDG? ${chan}" 48] { %f} volts ]
		  }
		  set stat [insIfRead /${ins} "RDGST? ${chan}" 48]
		  if { $volts > 0.03 || $volts == 0.0 } {# MORE TESTS HERE!!!!
		      return -code error "Platinum Resistor Rejected.  Volts = $volts"
		      lake331_sensor_r $chan $ins
		  }
	      }
	      insIfWrite /${ins} "INTYPE ${chan},${it},[expr { ($it >= 2) && ($it <= 5) }]"
	      lake331_sensor_r $chan $ins
              # Select curve automatically for some sensors
              set cn [lindex {2 0 6 7 0 12} $target]
              if { $cn } {
                  varSet /${ins}/setup/${chan}_curve -v $cn
              }
	  }
	CAMP_INT /~/setup/A_curve -D -R -S -T "Channel A curve" \
	    -d on -r on -s on \
	    -H "Load or read the A-channel calibration curve" \
	    -readProc lake331_A_curve_r -writeProc lake331_A_curve_w
	  proc lake331_A_curve_r { ins } { 
	    varDoSet /${ins}/controls/A_read -p on -p_int 1
	  }
	  proc lake331_A_curve_w { ins target } { 
	    varDoSet /${ins}/setup/A_curve -v $target
	    varDoSet /${ins}/controls/A_load -p on -p_int 1
	  }
	CAMP_INT /~/setup/B_curve -D -R -S -T "Channel B curve" \
	    -d on -r on -s on \
	    -H "Load or read the B-channel calibration curve" \
	    -readProc lake331_B_curve_r -writeProc lake331_B_curve_w
	  proc lake331_B_curve_r { ins } { 
	    varDoSet /${ins}/controls/B_read -p on -p_int 1
	  }
	  proc lake331_B_curve_w { ins target } { 
	    varDoSet /${ins}/setup/B_curve -v $target
	    varDoSet /${ins}/controls/B_load -p on -p_int 1
	  }
############################################################################

	CAMP_STRUCT /~/setup/filter -D -d on -T "Filtering"

	foreach inch {A B} {
	    CAMP_SELECT /~/setup/filter/${inch}_filter -D -R -S -T "${inch}-Filtering" \
		-H "Enable $inch channel filtering" \
		-d on -s on -r on -selections Off On \
		-readProc [list lake331_filter_r $inch] -writeProc [list lake331_filter_w $inch filter]
	    CAMP_INT /~/setup/filter/${inch}_points -D -R -S -T "${inch}-Points" \
		-H "Number of points for $inch channel filtering (2-64)" \
		-d on -s on -r on -units "" \
		-readProc [list lake331_filter_r $inch] -writeProc [list lake331_filter_w $inch points]
	    CAMP_INT /~/setup/filter/${inch}_window -D -R -S -T "${inch}-Window" \
		-H "Window for $inch channel filtering, as percentage of full scale (1-10%)" \
		-d on -s on -r on -units "%" \
		-readProc [list lake331_filter_r $inch] -writeProc [list lake331_filter_w $inch window]
	}
	proc lake331_filter_r { ch ins } {
	    set buf [insIfRead /$ins "FILTER? $ch" 48]
	    if {[scan $buf " %d, %d, %d" o p w] != 3} {
		return -code error "Invalid filter readback: $buf"
	    }
	    varDoSet /${ins}/setup/filter/${ch}_filter -v $o
	    varDoSet /${ins}/setup/filter/${ch}_points -v $p
	    varDoSet /${ins}/setup/filter/${ch}_window -v $w
	}
	proc lake331_filter_w { ch par ins val } {
	    set buf [insIfRead /$ins "FILTER? $ch" 48]
	    if {[scan $buf " %d, %d, %d" o p w] != 3} {
		return -code error "Invalid filter readback: $buf"
	    }
	    switch $par {
		filter {
		    set o $val
		}
		points {
		    if {$val < 2 || $val > 64 } {
			return -code error "Invalid number of points (must be 2-64)"
		    }
		    set p $val
		}
		window {
		    if {$val < 1 || $val > 10 } {
			return -code error "Invalid window (must be 1-10%)"
		    }
		    set w $val
		}
	    }
	    insIfWrite /$ins "FILTER $ch,$o,$p,$w"
	    lake331_filter_r $ch $ins
	}

############################################################################

	CAMP_STRUCT /~/setup/zone -D -d on -T "PID Zone configuration"
	    CAMP_SELECT /~/setup/zone/apply_zones -D -S -T "Apply zones" -d on -s on \
		  -H "Check and apply all zone settings" -selections APPLY \
		  -writeProc lake331_apply_zones_w
	    proc lake331_apply_zones_w { ins target } {
		set max 0.0
		set adj 0
		for { set i 1 } { $i <= 10 } { incr i } {
		    set z [varGetVal /${ins}/setup/zone/zone${i}]
		    # If this zone is blank, copy from previous.
		    if { [llength $z] == 0 && $i > 1 } {
			set z [varGetVal /${ins}/setup/zone/zone[expr {$i-1}]]
		    }
		    if { [llength $z] == 4 } { lappend z 0 }
		    if { [llength $z] == 5 } { lappend z 0 }
		    if { [llength $z] >= 6 } {
			set T [lindex $z 0]
			if { $T < $max && $T > 0.0 } {
			    if { $max < 300.0 } {
				return -code error "Invalid sequence of temperatures: must increase from zone1 to zone10"
			    }
			    set T [string trim [format %6.1f $max]]
			    set z [lreplace $z 0 0 $T]
			    varDoSet /${ins}/setup/zone/zone${i} -v $z
			}
			set max $T
			insIfWrite /$ins "ZONE 1,$i,$T,[join [lrange $z 2 5] {,}],[lindex $z 1]"
		    } else {
			return -code error "Invalid values for zone $i."
		    }
		}
	    }
	    for { set i 1 } { $i <= 10 } { incr i } {
		CAMP_STRING /~/setup/zone/zone$i -D -S -R -T "Zone $i info" \
		    -d on -s on -r on \
		    -H "List of zone $i parameters: Top Temp, Heat range, P, I, D, Manual out%" \
		    -readProc "lake331_zone_r $i" -writeProc "lake331_zone_w $i"
	    }
	    proc lake331_zone_w { i ins target } {
		set z [join [split $target ,] " "]
		if { [llength $z] == 4 } { lappend z 0 }
		if { [llength $z] == 5 } { lappend z 0 }
		if { [llength $z] >= 5 } {
		    varDoSet /${ins}/setup/zone/zone${i} -v [lrange $z 0 5]
		} else {
		    return -code error "Invalid zone data.  Should be 6 numbers separated by spaces or commas."
		}
	    }
	    proc lake331_zone_r { i ins } {
		set z [insIfRead /$ins "ZONE? 1,$i" 80]
		if { [scan $z " %f,%f,%f,%f,%f,%d" T P I D M H] == 6 } {
		    foreach v {T P I D M} { # reduce round numbers to integers
			set vv [set $v]
			set $v [expr {($vv==round($vv)?round($vv):$vv)}]
		    }
		    varDoSet /${ins}/setup/zone/zone${i} -v [list $T $H $P $I $D $M]
		} else {
		    return -code error "Invalid zone readback: \"$z\""
		}
	    }

############################################################################
	CAMP_STRUCT /~/setup/curve -D -d on
	    CAMP_INT /~/setup/curve/number -D -S -T "Curve number" -d on -s on \
		-writeProc lake331_curve_number_w
	      proc lake331_curve_number_w { ins target } {
		    if { ( $target < 21 ) || ( $target > 41 ) } {
			return -code error "invalid curve number $target"
		    }
		    varDoSet /${ins}/setup/curve/number -v $target
	      }
	    CAMP_STRING /~/setup/curve/file -D -S -T "Curve datafile" -d on -s on \
		-writeProc lake331_curve_file_w
	      proc lake331_curve_file_w { ins target } {
		    varDoSet /${ins}/setup/curve/file -v $target
	      }
	    CAMP_SELECT /~/setup/curve/format -D -S -R -T "Curve format" -d on -s on -r on \
		-selections "" {K,mV} {K,V} {K,Ohm} {K,log Ohm} \
		-readProc lake331_curve_header_r -writeProc lake331_curve_format_w
	      proc lake331_curve_format_w { ins target } {
		    varDoSet /${ins}/setup/curve/format -v $target
	      }
	    CAMP_STRING /~/setup/curve/id -D -S -R -T "Curve ID (serial num)" -d on -s on -r on \
		-readProc lake331_curve_header_r -writeProc lake331_curve_id_w
	      proc lake331_curve_id_w { ins target } {
		    varDoSet /${ins}/setup/curve/id -v [string trim [string tolower $target]]
	      }
	      proc lake331_curve_header_r { ins } {
		  set num [varGetVal /${ins}/setup/curve/number]
		  set buf [insIfRead /${ins} "CRVHDR? $num" 80]
		  if { [scan $buf {%[^,],%[^,],%d} name id fmt ] != 3 } {
		      return -code error "Could not read header for curve $num"
		  }
		  varDoSet /${ins}/setup/curve/file -v [string trim [string tolower $name]]
		  varDoSet /${ins}/setup/curve/id -v [string trim [string tolower $id]]
		  varDoSet /${ins}/setup/curve/format -v $fmt
	      }
	    CAMP_SELECT /~/setup/curve/do_load -D -S -T "Do curve load" \
		-H "Start loading the curve" \
		-d on -s on -selections DONE LOADING \
		-writeProc lake331_curve_do_w
	      proc lake331_curve_do_w { ins target } {
		varDoSet /${ins}/setup/curve/do_load -v $target
		if { $target == 1 } {
		  # Do asynchronously by polling controls/do_load
		  varDoSet /${ins}/controls/do_load -p on -p_int 1
		}
	      }

############################################################################
    CAMP_STRUCT /~/controls -D -d on
	CAMP_SELECT /~/controls/A_load -D -P -R -T "Load A curve" \
	    -H "Called (polled) asynchronously by setup/A_curve" \
	    -d on -r on -selections DONE LOADING \
	    -readProc lake331_controls_A_load_r
	  proc lake331_controls_A_load_r { ins } { 
	    varDoSet /${ins}/controls/A_load -v 1 -p off
	    set status [catch {lake331_set_curve $ins A [varGetVal /${ins}/setup/A_curve]}]
	    varDoSet /${ins}/controls/A_load -v 0
	    if { $status == 0 } {
	      varDoSet /${ins}/controls/A_read -p on -p_int 1
	    }
	  }
	CAMP_SELECT /~/controls/A_read -D -P -R -T "Read A curve" \
	    -H "Readback check of A curve" \
	    -d on -r on -selections DONE READING \
	    -readProc lake331_controls_A_read_r
	  proc lake331_controls_A_read_r { ins } {      
	    varDoSet /${ins}/controls/A_read -v 1 -p off
	    lake331_read_curve $ins A
	    varDoSet /${ins}/controls/A_read -v 0
	  }
	CAMP_SELECT /~/controls/B_load -D -P -T "Load B curve" \
	    -H "Called (polled) asynchronously by setup/B_curve" \
	    -d on -selections DONE LOADING \
	    -readProc lake331_controls_B_load_r
	  proc lake331_controls_B_load_r { ins } { 
	    varDoSet /${ins}/controls/B_load -v 1 -p off
	    set status [catch {lake331_set_curve $ins B [varGetVal /${ins}/setup/B_curve]}]
	    varDoSet /${ins}/controls/B_load -v 0
	    if { $status == 0 } {
	       varDoSet /${ins}/controls/B_read -p on -p_int 1
	    }
	  }
	CAMP_SELECT /~/controls/B_read -D -P -T "Read B curve" \
	    -H "Readback check of B curve" \
	    -d on -selections DONE READING \
	    -readProc lake331_controls_B_read_r
	  proc lake331_controls_B_read_r { ins } {      
	    varDoSet /${ins}/controls/B_read -v 1 -p off
	    set status [catch {lake331_read_curve $ins B}]
	    varDoSet /${ins}/controls/B_read -v 0
	  }
	CAMP_SELECT /~/controls/do_load -D -P -R -T "Do load curve" \
	    -H "Performs curve loading asynchronously" \
	    -d on -r on -selections DONE LOADING \
	    -readProc lake331_controls_do_load_r
	  proc lake331_controls_do_load_r { ins } {     
	      varDoSet /${ins}/setup/curve/do_load -v 1
	      varDoSet /${ins}/controls/do_load -v 1 -p off
	      set file [varGetVal /${ins}/setup/curve/file]
	      set f [open $file r]
	      set curve_data ""
	      # read first line (has the number of data points)
	      if { [gets $f line] == -1 } {
		  close $f
		  return -code error "unexpected end of file"
	      }
	      set num [varGetVal /${ins}/setup/curve/number]
	      set fmt [varGetVal /${ins}/setup/curve/format]
	      set coef [lindex "0 2 1 2 1" $fmt]
	      set sn [varGetVal /${ins}/setup/curve/id]
	      # delete the curve in memory 
	      insIfWrite /${ins} [format "CRVDEL %2d" $num]
	      # send the new header
	      insIfWrite /${ins} [format "CRVHDR %2d,%s,%s,%d,999,%d" $num $file $sn $fmt $coef]
	      set cmd_line ""
	      set i 0
	      set fp 0
	      set maxt -1.0
	      while { [gets $f line] >= 0 } {
		  if { [scan $line " %f %f" t v] < 2 } {
		      close $f
		      return -code error "bad file format"
		  }
		  incr i
		  if { $t > $maxt } { set maxt $t }
		  if { $i > 1 } {
		      if { ( $t != $tp ) && ( $v != $vp ) } {
			  set s [expr ($v - $vp ) / ( $t - $tp )]
			  if { ($s > 0.0) != ($coef - 1) } {
			      close $f
			      return -code error "bad file: slope changed sign at $i, $coef, $s = ($v - $vp ) / ( $t - $tp )"
			  }
		      }
		  }
		  set tp $t ; set vp $v
		  set cmd [format "CRVPT%d,%d,%.5f,%.3f" $num $i $v $t]
		  set len_cmd [string length $cmd]
		  set len_cmd_line [string length $cmd_line]
		  if { ( $len_cmd + $len_cmd_line ) > 64 } {
		      insIfWrite /${ins} $cmd_line
		      set cmd_line ""
		  }
		  append cmd_line ";" $cmd 
	      }
	      if { $cmd_line != "" } { insIfWrite /${ins} $cmd_line }
	      close $f
	      insIfWrite /${ins} [format "CRVHDR %2d,%s,%s,%d,%.1f,%d" $num $file $sn $fmt $maxt $coef]
	      varDoSet /${ins}/controls/do_load -v 0 
	      varDoSet /${ins}/setup/curve/do_load -v 0
	  }
    CAMP_STRUCT /~/panic -D -d on
	CAMP_SELECT /~/panic/reset -D -S -T "Reset" -d on -s on \
	    -selections DO_RESET \
	    -writeProc lake331_reset_w
	  proc lake331_reset_w { ins target } { insIfWrite /${ins} "*RST" }
	CAMP_SELECT /~/panic/test -D -R -T "Self-test" -d on -r on \
	    -selections OK ERROR UNKNOWN \
	    -readProc lake331_test_r
	  proc lake331_test_r { ins } {
		set test [insIfRead /${ins} "*TST?" 16]
		switch $test {
		    0 {}
		    1 {}
		    default { set test 2 }
		}
		varDoSet /${ins}/panic/test -v $test
		if { $test != 0 } { insSet /${ins} -line off }
	    }
proc lake331_set_curve { ins channel curve } {
    varDoSet /${ins}/setup/${channel}_curve -m ""
    if { $curve < 0 } {
	varDoSet /${ins}/setup/${channel}_curve -units "FAIL \"$curve\""
	return -code error "bad value \"$curve\""
    } elseif { $curve <= 41 } {
	set number $curve
    } else {
	set buf [glob ./dat/*.*]
	set found 0
	foreach file $buf {
	    set root [file rootname [file tail $file]]
	    if { $root == $curve } { set found 1; break }
	}
	if { $found == 0 } { 
	  varDoSet /${ins}/setup/${channel}_curve -units "NO FILE" -m \
		"Failed to find a data file for curve \"$curve\". Try again."
	  return -code error "couldn't find curve \"$curve\"" 
	}
	varDoSet /${ins}/setup/${channel}_curve -units "LOAD" -m \
		"Loading curve \"$curve\""
	set number 21
	set other_channel A
	if { $channel == $other_channel } { set other_channel B }
	set other_curve 0
	scan [insIfRead /${ins} "INCRV? ${other_channel}" 48] { %d} other_curve
	if { $number == $other_curve } { incr number }
	set ext [string tolower [file extension $file]]
	set type [expr [lsearch -exact ".tc .spl .tr .dat" $ext] + 1]
	varDoSet /${ins}/setup/${channel}_curve -units "LOAD" -m \
		"Loading curve \"$curve\" as number ${number}"
	varSet /${ins}/setup/curve/number -v $number
	varSet /${ins}/setup/curve/format -v $type
	varSet /${ins}/setup/curve/id -v $curve
	set file [format "./dat/%s" [file tail $file]]
	varSet /${ins}/setup/curve/file -v $file
	#
	# This actually does the file load!
	#
	varRead /${ins}/controls/do_load
    }
    insIfWrite /${ins} "INCRV ${channel},$number"
#   Verify setting of curve number.  Since the Lakeshore331 will not switch 
#   to a defective curve, this check also verifies the load.  We will later
#   read out the curve to check more fully.
    set other_curve 0
    set buf [insIfRead /${ins} "INCRV ${channel},$number; INCRV? ${channel}" 48] 
    set ie [scan $buf { %d} other_curve]
    if { $number != $other_curve } {
	set e "failed to load curve number $number for channel $channel"
	varDoSet /${ins}/setup/${channel}_curve -units "FAIL" -m $e
	return -code error $e
    }
    varDoSet /${ins}/setup/${channel}_curve -units "VERIFY" -m \
		"verifying the download..."
}
proc lake331_read_curve { ins channel } {
    set number -1
    set buf [insIfRead /${ins} "INCRV? ${channel}" 48]
    if { [catch { expr "$buf" }] == 0 } {
	set number [ expr "$buf" ]
    } else {
	set number -99
    }
    if { !( ($number >= 0) && ($number <= 41) ) } {
	set e "failed to read curve number $number for channel $channel"
	varDoSet /${ins}/setup/${channel}_curve -units "FAIL" -m $e
	return -code error $e
    } elseif { $number < 21 } {
	set curve $number
    } else {
	set buf [insIfRead /${ins} "CRVHDR?$number" 48]
	if { [scan $buf {%[^,],%[^,],%d} file id fmt ] != 3 } {
	  varDoSet /${ins}/setup/${channel}_curve -v -1
	  return -code error "failed parsing curve data"
	}
	set curve [string trim [string tolower $id]]
	set file [string trim [string tolower $file]]
	set f [open $file r]
	set curve_data ""
	# read first line (has the number of data points)
	if { [gets $f line] == -1 } {
	  close $f
	  varDoSet /${ins}/setup/${channel}_curve -v -1
	  return -code error "unexpected end of file"
	}
	set n 0
	if { [scan $line " %d" n] != 1 } {
	  close $f
	  varDoSet /${ins}/setup/${channel}_curve -v -1
	  return -code error "invalid file format: no number of points"
	}
	set tf 0; set vf 0
	for { set i 1 } { $i <= $n } { incr i } {
	  if { [gets $f line] == -1 } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v -1
	    return -code error "unexpected end of file"
	  }
	  if { [scan $line " %f %f" tf vf] < 2 } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v -1 -units "FAIL" -m \
		"bad file format: while scanning data line"
	    return -code error "bad file format: while scanning data line"
	  }
	  set vf [format "%.5f" $vf]
	  set tf [format "%.3f" $tf]
	  set buf [insIfRead /${ins} "CRVPT?$number,$i" 64]
	  if { [scan $buf { %f,%f} vi ti] != 2 } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v -1 -units "FAIL" -m \
		"invalid readback of curve $number point $i "
	    return -code error "invalid readback of curve $number point $i "
	  }
	  set vi [format %.5f $vi]
	  set ti [format %.3f $ti]
	  if { $vf != $vi || $tf != $ti } {
	    close $f
	    varDoSet /${ins}/setup/${channel}_curve -v -1  -units "FAIL" -m \
		"Failed load: inconsistent readback"
	    return -code error "inconsistent readback: Vfile=$vf Vins=$vi Tfile=$tf Tins=$ti" 
	  }
	}
	close $f
    }
    varDoSet /${ins}/setup/${channel}_curve -v $curve -units "Num.$number" -m ""
}
