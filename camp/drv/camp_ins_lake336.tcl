# -*- mode: tcl; tab-width: 4; -*- 
# camp_ins_lake336.tcl: driver for Lakeshore model 336 temperature controller
# with the 3462 option installed giving 2 extra inputs, for A,B,C,D in all.
#
# This instrument driver resembles the Lakeshore 331 and a bit of 340.
# Many FLOAT settings fail if the value has too many digits, so there are [format]s used.
#
# $Log: camp_ins_lake336.tcl,v $
# Revision 1.11  2018/06/13 01:40:59  asnd
# Rewrite curve loading/validation to yield. Many edits for size. Missing var bugfix.
#
# Revision 1.10  2017/06/21 04:36:22  asnd
# Accumulated changes
#
# Revision 1.9  2015/11/29 04:51:22  asnd
# Create Aliases
#
# Revision 1.8  2015/06/05 05:19:50  asnd
# Bugs fixed in apply zones
#
# Revision 1.6  2015/05/06 06:39:58  asnd
# Remove debugging messages
#
# Revision 1.5  2015/05/06 06:35:25  asnd
# Fix more vxworks issues (files)
#
# Revision 1.4  2015/05/06 05:36:28  asnd
# Fix array initialize

CAMP_INSTRUMENT /~ -D -T "LakeShore 336" \
	-H "LakeShore 336 Temperature Controller" -d on \
	-initProc L336_init -deleteProc L336_delete \
	-onlineProc L336_online -offlineProc L336_offline

# no [array set ...] in Tcl7
proc L336_init { ins } {
	global ${ins}_cache
	set ${ins}_cache(v:fid) ""
	foreach c {A B C D} {set ${ins}_cache(${c}curve) 999}
	insSet /$ins -if tcpip 0.15 -2 142.90.154.131 7777 CRLF CRLF
}
#	insSet /$ins -if gpib 0.1 2 16 CRLF CRLF
#	insSet /$ins -if rs232 0.2 2 /tyCo/2 9600 7 odd 1 CRLF CRLF

proc L336_delete { ins } {
	global ${ins}_cache
	insSet /$ins -line off
	unset ${ins}_cache
}

proc L336_online { ins } {
	varDoSet /$ins/setup/id -p off -v 0
	insIfOn /$ins
	varRead /$ins/setup/id
	if { [varGetVal /$ins/setup/id] == 0 } {
		insIfOff /$ins
		return -code error "failed ID query, check interface definition and connections"
	}
	varDoSet /$ins/setup/id -p on -p_int 0.125
}
# More initialization performed by polling ~/setup/id; 0.125 is signal value

proc L336_offline { ins } {
	insIfOff /$ins
}

# insIfRead and insIfWrite with a simple retry, for problem of flaky network.

proc insIfRRT {path req len} {
	if {[catch {insIfRead $path $req $len} msg]} {
		#sleep 0.1
		if { [catch {insIfRead $path $req $len} msg] } {return -code error $msg}
	}
	return $msg
}

proc insIfWRT {path req} {
	if {[catch {insIfWrite $path $req} msg]} {
		#sleep 0.1
		if {[catch {insIfWrite $path $req} msg]} {return -code error $msg}
	}
	return $msg
}

foreach _ich {A B C D} {
CAMP_FLOAT /~/read_$_ich -D -R -P -L -A -T "Channel $_ich reading" \
	-d on -r on -tol 0.5 \
	-readProc [list L336_read_r $_ich]
}

proc L336_read_r { ch ins } {
	set u [lindex {K K C S} [varGetVal /$ins/inputs/$ch/units]]
	insIfReadVerify /$ins "${u}RDG? $ch" 48 /$ins/read_$ch " %f" 2
	foreach io {1 2} {
		if { ! [string compare [varSelGetValLabel /$ins/output_$io/control_chan] $ch] } {
			varTestAlert /$ins/read_$ch [varGetVal /$ins/setpoint_$io]
		}
	}
}

foreach _io {1 2} {
# There are actually 4 setpoints and 4 control loops, but only 2 heater outputs.
# In the future we may want control loops driving the two auxilliary (voltage) outputs,
# particularly for flow regulators!

CAMP_FLOAT /~/setpoint_$_io -D -R -S -L -T "Output $_io set point" \
	-d on -r on -s on -units K \
	-readProc [list L336_setpoint_r $_io] -writeProc [list L336_setpoint_w $_io]

CAMP_SELECT /~/heat_range_$_io -D -R -S -P -T "Heater $_io range" \
	-d on -r on -s on -p off -selections OFF LOW MEDIUM HIGH \
	-H "Heater $_io range" \
	-readProc [list L336_heat_range_r $_io] -writeProc [list L336_heat_range_w $_io]

CAMP_FLOAT /~/current_read_$_io -D -R -L -P -A -T "Output $_io current" \
	-d on -r on -p off -units A -tol 0.01 \
	-H "Heater $_io output in Amps.  (Limited by both heat_range and max_current)" \
	-readProc [list L336_current_read_r $_io]

CAMP_FLOAT /~/set_current_$_io -D -R -S -L -T "Set Output $_io Current" \
	-d off -r off -s on -units "A" \
	-H "Constant heater current setting (only when not controlling temperature; see output_${_io}/control_mode)." \
	-readProc [list L336_setcurr_r $_io] -writeProc [list L336_setcurr_w $_io]

}

proc L336_setpoint_r { io ins } {
	insIfReadVerify /$ins "SETP? $io" 48 /$ins/setpoint_$io " %f" 2
}

# Enforce a 1K buffer against hitting temperature limit with setpoint.
proc L336_setpoint_w { io ins val } {
	set lim 0.0
	set ch [varSelGetValLabel /$ins/output_$io/control_chan]
	catch { scan [insIfRRT /$ins "TLIMIT? $ch" 16] " %f" lim }
	if { $lim > 0.0 && $val > $lim-0.9 } {
	return -code error "Setpoint exceeds channel $ch limit of [expr $lim-1.0]"
	}
	set err [expr {abs($val)/900.0}]
	if { [catch {
		insIfWriteVerify /$ins "SETP $io,$val" "SETP? $io" 48 /$ins/setpoint_$io " %g" 2 $val $err
	} msg ] } {
		varRead /$ins/output_$io/ramp
		if { [varGetVal /$ins/output_$io/ramp] } {
			#varDoSet  /$ins/setpoint_$io -m "Caught error $msg while ramping"
			varDoSet /$ins/output_$io/ramp_status -p on -p_int 5
		} else {
			return -code error $msg
		}
	}
}

proc L336_heat_range_r { io ins } {
	insIfReadVerify /$ins "RANGE? $io" 48 /$ins/heat_range_$io " %d" 2
}
#  ??? To be checked: how heater load checking happens.  In manual mode, an open
#  circuit is detected after a long delay.

#  Maybe hide heat range when in manual mode: Adjust it silently to match the
#  set current.  Or maybe don't even support manual setting.


proc L336_heat_range_w { io ins target } {
	if { [catch {
		insIfWriteVerify /$ins "RANGE $io,$target" "RANGE? $io" 48 /$ins/heat_range_$io " %d" 2 $target
	} buff ] } {
		set err 0
		scan [insIfRRT /$ins "HTRST? $io" 48] { %d} err
		if { $err } {
			return -code error [lindex {{} "Heater $io open load" "Heater $io short"} $err]
		}
		set range [varGetVal /$ins/heat_range_$io]
		if { $range == 0 && $target > 0 } {
			return -code error "Failed heater setting (open load or bad sensor?). $buff"
		} else {
			return -code error "Failed heater setting. $buff"
		}
	}
	set range [varGetVal /$ins/heat_range_$io]
	if { [varGetVal /$ins/output_$io/control_mode] == 3 } {# manual current
		set sc [varGetVal /$ins/set_current_$io]
		varRead /$ins/output_$io/heater/max_current
		set mc [varGetVal /$ins/output_$io/heater/max_current]
		if { $sc > $mc * [lindex {0.0 0.1 0.31623 1.0} $range] } {
			return -code error "Warning: set_current cannot be maintained in this heat_range"
		} else {
			catch { L336_setcurr_int $io $target $ins $sc }
		}
	}
}

proc L336_current_read_r { io ins } {
	varRead /$ins/heat_range_$io
	set range_factor [lindex {0.0 0.1 0.31623 1.0} [varGetVal /$ins/heat_range_$io] ]
	set buf [insIfRRT /$ins "HTR? $io" 48]
	if { [scan $buf " %f" percent] != 1 } { set percent -1.0 }
	if { ( $percent < 0.0 ) || ( $percent > 100.0 ) } {
		return -code error "failed parsing \"$buf\" from HTR? command"
	}
	set max_curr [varGetVal /$ins/output_$io/heater/max_current]
	varDoSet /$ins/current_read_$io -v [expr { $max_curr * $range_factor * $percent/100.0}]
	if { [varGetVal /$ins/output_$io/control_mode] == 3 } {
		varTestAlert /$ins/current_read_$io [varGetVal /$ins/set_current_$io]
	}
}

# NOTE: lake 336 MOUT is documented as unspecified "manual output value" as percentage, 
# not "power" as erroneously stated for the 331, but it appears to be percentage of
# full current in both cases.

# This is only displayed in camp_cui in manual output mode.

proc L336_setcurr_r { io ins } {
	varRead /$ins/heat_range_$io
	varRead /$ins/output_$io/heater/max_current
	set range_factor [lindex {0.0 0.1 0.31623 1.0} [varGetVal /$ins/heat_range_$io]]
	set max_curr [varGetVal /$ins/output_$io/heater/max_current]
	set buf [insIfRRT /$ins "MOUT? $io" 48]
	if { [scan $buf " %f" percent] != 1 } {
		return -code error "Invalid percent readback: $buf"
	}
	varDoSet /$ins/set_current_$io -v [expr { $max_curr * $range_factor * $percent/100.0}]
}

proc L336_setcurr_w { io ins target } {
	varRead /$ins/output_$io/heater/max_current
	set max_curr [varGetVal /$ins/output_$io/heater/max_current]
	if { $target < 0.0 || $target > $max_curr } {
		return -code error "Current setting must be in range 0.0 to $max_curr A (see output_$io/heater/max_current)"
	}
	varRead /$ins/heat_range_$io
	L336_setcurr_int $io [varGetVal /$ins/heat_range_$io] $ins $target
}

proc L336_setcurr_int { io hr ins target } {
	set max_curr [varGetVal /$ins/output_$io/heater/max_current]
	set range_factor [lindex {0.1e-08 0.1 0.31623 1.0} $hr]
	set pct [format %.3f [expr { 100.0 * $target / ($range_factor * $max_curr) }]]
	if { $pct > 100.001 } {
		return -code error "Exceeds max current for '[lindex {OFF LOW MED HIGH} $hr]' range."
	}
	#varDoSet /$ins/set_current -m "pct $pct, hr $hr, mc $mc, max $max "
	set rb -2
	for {set i 0} {$i < 3} {incr i} {
		insIfWRT /$ins "MOUT $io,$pct"
		scan [insIfRRT /$ins "MOUT? $io" 48] " %f" rb
		if { abs($rb-$pct) <= 0.1 } { break }
	}
	if { abs($rb-$pct) > 0.1 } {
		return -code error "Setting did not apply: $rb vs $pct %"
	}
	varDoSet /$ins/set_current_$io -v $target
}

CAMP_SELECT /~/ramp_status -D -R -P -T "Ramp status" \
	-d on -r on -p off -p_int 5 -selections "done" "ramping 1" "ramping 2" "ramp 1&2" \
	-readProc L336_ramp_status2_r

# Displayed ramp status for both outputs
proc L336_ramp_status2_r { ins } {
	varRead /$ins/output_1/ramp_status
	varRead /$ins/output_2/ramp_status
	set i [expr {[varGetVal /$ins/output_1/ramp_status] + 2*[varGetVal /$ins/output_1/ramp_status]}]
	if { $i } {
		varDoSet /$ins/ramp_status -v $i -p off
	} else {
		varDoSet /$ins/ramp_status -v $i -p on -p_int 6
	}
}

#########################################################################################

CAMP_STRUCT /~/alarms -D -d on -T "Alarms and Relays"

foreach _ich {A B C D} {

	CAMP_SELECT /~/alarms/alarm_$_ich -D -R -P -A -T "$_ich Alarm state" \
		-d on -r on -p off -p_int 8 -a off \
		-H "Alarm state for input $_ich" \
		-selections OK Low_Trip Hi_Trip Both_Trip \
		-v 0 -readProc [list L336_alarmst_r $_ich]
}

proc L336_alarmst_r { ch ins } {
	set buf [insIfRRT /$ins "ALARMST? $ch" 16]
	if { [scan $buf " %d, %d" hi lo] != 2 } {
		return -code error "invalid status $buf"
	}
	varDoSet /$ins/alarms/alarm_$ch -v [expr {$lo + 2*$hi}]
	varTestAlert /$ins/alarms/alarm_$ch 0
}

CAMP_SELECT /~/alarms/reset -D -S -T "Reset alarms" \
	-d on -s on -selections "" RESET -v 0 \
	-writeProc L336_alarmreset_w

proc L336_alarmreset_w { ins target } {
	if { $target } {
		insIfWRT /$ins "ALMRST"
	}
}


foreach _ich {A B C D} {

	CAMP_STRUCT /~/alarms/config_${_ich} -D -d on -T "Configure Alarm ${_ich}"

	CAMP_SELECT /~/alarms/config_${_ich}/enable -D -S -R -T "Enable ${_ich} Alarm" \
		-d on -s on -r on -selections Disable Enable \
		-readProc [list L336_almcfg_r ${_ich}] -writeProc [list L336_almcfg_w ${_ich} enable]
	CAMP_FLOAT /~/alarms/config_${_ich}/low_limit -D -S -R -T "${_ich} low limit" -d on -s on -r on \
		-readProc [list L336_almcfg_r ${_ich}] -writeProc [list L336_almcfg_w ${_ich} low_limit]
	CAMP_FLOAT /~/alarms/config_${_ich}/high_limit -D -S -R -T "${_ich} high limit" -d on -s on -r on \
		-readProc [list L336_almcfg_r ${_ich}] -writeProc [list L336_almcfg_w ${_ich} high_limit]
	CAMP_FLOAT /~/alarms/config_${_ich}/deadband -D -S -R -T "${_ich} deadband" -d on -s on -r on \
		-readProc [list L336_almcfg_r ${_ich}] -writeProc [list L336_almcfg_w ${_ich} deadband]
	CAMP_SELECT /~/alarms/config_${_ich}/latch -D -S -R -T "${_ich} alarm latching" \
		-d on -s on -r on -selections reset latch \
		-readProc [list L336_almcfg_r ${_ich}] -writeProc [list L336_almcfg_w ${_ich} latch]
}

proc L336_almcfg_r { ch ins } {
	set buf [insIfRRT /$ins "ALARM? $ch" 80]
	if { [scan $buf " %d,%g,%g,%g,%d,%d,%d" enable high low deadband latch audible visible] != 7 } {
		return -code error "invalid alarms config $buf"
	}
	varDoSet /$ins/alarms/config_$ch/enable -v $enable
	varDoSet /$ins/alarms/config_$ch/low_limit -v $low
	varDoSet /$ins/alarms/config_$ch/high_limit -v $high
	varDoSet /$ins/alarms/config_$ch/deadband -v $deadband
	varDoSet /$ins/alarms/config_$ch/latch -v $latch
}

proc L336_almcfg_w { ch var ins val } {
	set buf [insIfRRT /$ins "ALARM? $ch" 80]
	if { [scan $buf " %d,%g,%g,%g,%d,%d,%d" enable high low deadband latch audible visible] != 7 } {
		return -code error "invalid alarms config $buf"
	}
	set $var $val
	insIfWRT /$ins "ALARM $ch,$enable,$high,$low,$deadband,$latch,$audible,$visible"
	varRead /$ins/alarms/config_$ch/$var
}

CAMP_SELECT /~/alarms/relay_1_set -D -R -S -T "Relay 1 setting" \
	-d on -s on -r on -selections Off On Low_Alarm High_Alarm Any_Alarm \
	-readProc {L336_relayact_r 1} -writeProc {L336_relayact_w 1}

CAMP_SELECT /~/alarms/relay_2_set -D -R -S -T "Relay 2 setting" \
	-d on -s on -r on -selections Off On Low_Alarm High_Alarm Any_Alarm \
	-readProc {L336_relayact_r 2} -writeProc {L336_relayact_w 2}

#   On the 340, relays are not very configurable. One is always for the high alarm
#   and one for the low alarm.  The 336 is more flexible, and matches the 331.

proc L336_relayact_r { id ins } {
	set buf [insIfRRT /$ins "RELAY? $id" 48]
	if { [scan $buf { %d,%[ABCD],%d} mode chan type] != 3 } {
		return -code error "invalid relay config $buf"
	}
	if { $mode == 2 } {# alarms
		set mode [expr {$mode+$type}]
	}
	varDoSet /$ins/alarms/relay_${id}_set -v $mode
}

proc L336_relayact_w { id ins target } {
	set buf [insIfRRT /$ins "RELAY? $id" 48]
	if { [scan $buf { %d,%[ABCD],%d} mode chan type] != 3 } {
		return -code error "invalid relay config $buf"
	}
	if { $target >= 2 } {# alarm
		set type [expr {$target-2}]
		set mode 2
	} else {# manual
		set mode $target
		set type 0
	}
	insIfWRT /$ins "RELAY $id,$mode,$chan,$type"
	varDoSet /$ins/alarms/relay_${id}_set -v $target
}

#############################################################################################
foreach _io { 1 2 } {

CAMP_STRUCT /~/output_${_io}  -D -d on -T "Output loop $_io controls"

CAMP_SELECT /~/output_${_io}/control_chan -D -R -S -T "Control channel" \
	-d on -r on -s on -selections " " A B C D \
	-H "Sensor Channel controlling heater output $_io" \
	-readProc [list L336_control_chan_r $_io] -writeProc [list L336_control_chan_w $_io]

CAMP_SELECT /~/output_${_io}/control_mode -D -R -S -T "Control mode" \
	-d on -r on -s on  -selections "off" "Manual PID" "Zone PID" "Constant Out" \
	-readProc [list L336_control_mode_r $_io] -writeProc [list L336_control_mode_w $_io]

CAMP_SELECT /~/output_${_io}/autotune -D -R -P -S -T "Autotune $_io" \
	-d on -r on -p off -s on -selections "Idle" "P" "PI" "PID" "Busy" "Failed" \
	-H "Autotune of P,I,D works on only one control loop at a time and does not set the heater range. Note that it causes temperature variations." \
	-readProc [list L336_autotune_r $_io] -writeProc [list L336_autotune_w $_io]

CAMP_SELECT /~/output_${_io}/ramp -D -S -R -P -T "Ramp $_io" \
	-d on -s on -r on -selections DISABLED ENABLED \
	-H "Enable or disable slow ramping of setpoint" \
	-readProc [list L336_ramp_r $_io] -writeProc [list L336_ramp_w $_io]

CAMP_SELECT /~/output_${_io}/ramp_status -D -R -P -T "Ramp status $_io" \
	-d on -r on -p off -p_int 5 -selections DONE RAMPING \
	-readProc [list L336_ramp_status_r $_io]

CAMP_FLOAT /~/output_${_io}/ramp_rate -D -S -R -L -P -T "Ramp rate $_io" \
	-d on -s on -r on -p off -units {K/min} \
	-H "Temperature (setpoint) ramp rate (if ramp is enabled) in Kelvin per minute (from 0.1 to 99.9, or 0 for off)" \
	-readProc [list L336_ramp_r $_io] -writeProc [list L336_ramp_rate_w $_io]

CAMP_FLOAT /~/output_${_io}/P -D -R -S -L -T "Gain setting (P)" \
	-d on -r on -s on \
	-H "Proportional gain parameter; use high values when response is slow" \
	-readProc [list L336_PID_r $_io] -writeProc [list L336_PID_w $_io p]

CAMP_FLOAT /~/output_${_io}/I -D -R -S -L -T "Reset setting (I)" \
	-d on -r on -s on \
	-H "Integral reset parameter, relative to P (I = Ki/P = 1000sec/time_constant); use low values when response is slow" \
	-readProc [list L336_PID_r $_io] -writeProc [list L336_PID_w $_io i]

CAMP_FLOAT /~/output_${_io}/D -D -R -S -L -T "Rate setting (D)" \
	-d on -r on -s on \
	-H "Derivative rate parameter, relative to P (D=Kd/P)" \
	-readProc [list L336_PID_r $_io] -writeProc [list L336_PID_w $_io d]


#####################################################################################

CAMP_STRUCT /~/output_${_io}/zone -D -d on -T "PID Zone configuration"

CAMP_SELECT /~/output_${_io}/zone/apply_zones -D -S -T "Apply zones" -d on -s on \
	-H "Check and apply all zone settings" -selections APPLY \
	-writeProc [list L336_apply_zones_w $_io]

for { set _i 1 } { $_i <= 10 } { incr _i } {
	CAMP_STRING /~/output_${_io}/zone/zone${_i} -D -S -R -T "Zone $_i info" \
		-d on -s on -r on \
		-H "List of zone $_i parameters: Top Temp, Heat range, P, I, D, Manual out%" \
		-readProc [list L336_zone_r $_io $_i] -writeProc [list L336_zone_w $_io $_i]
}
unset _i

#####################################################################################

CAMP_STRUCT /~/output_${_io}/heater -D -d on -T "Heater configuration"

CAMP_FLOAT /~/output_${_io}/heater/max_current -D -S -R -L -T "Maximum Current $_io" \
	-d on -s on -r on -v [expr {($_io==1 ? 2.0 : 1.0)}] -units A \
	-H "100% current on HI range; other ranges scale proportionately. Max [expr {($_io==1 ? 2.0 : 1.0)}] A." \
	-readProc [list L336_heater_r $_io] -writeProc [list L336_maxcurr_w $_io]

CAMP_SELECT /~/output_${_io}/heater/resistance -D -S -R -L -T "Heater $_io resistance" \
	-d on -s on -r on -selections " " "25 ohm" "50 ohm" \
	-H "Heater resistance definition (not actually measured)" \
	-readProc [list L336_heater_r $_io] -writeProc [list L336_resistance_w $_io]

}
# end of _io loop over outputs 1&2
 
#####################################################################################

proc L336_control_chan_r { io ins } {
	set buf [insIfRRT /$ins "OUTMODE? $io" 48]
	if { [scan $buf { %d,%d,} mode inch] != 2 } {
		return -code error $buf
	}
	varDoSet /$ins/output_$io/control_chan -v $inch
	if { $inch > 0 } {
		varDoSet /$ins/output_$io/control_mode -v $mode
		set chan [lindex {"" A B C D} $inch]
		set units [varGetVal /$ins/inputs/$chan/units]
		set title [string trim [varGetVal /$ins/inputs/$chan/title]]
		varDoSet /$ins/setpoint_$io -m "Setpoint for $title, input $chan output $io" \
			-units [varNumGetUnits /$ins/read_$chan]
	} else {
		set title ""
		varDoSet /$ins/setpoint_$io -m ""
	}
	if { [string length $title] && [varGetVal /$ins/setup/names] } {
		set tt [join [split $title " /{}()!@\#%^&*"] {}]
		varDoSet /$ins/setpoint_$io -T "$title set point" -units [varNumGetUnits /$ins/read_$chan] \
			-m "alias: [string range set_$tt 0 14] Setpoint for $title, input $chan output $io"
		varDoSet /$ins/heat_range_$io -T "$title heat range" \
			-m "alias: [string range heat_range_$tt 0 14] Heat range for $title, input $chan output $io"
		varDoSet /$ins/current_read_$io -T "$title heater current" \
			-m "alias: [string range current_$tt 0 14] Heater current for $title, input $chan output $io"
	} elseif { [string length $title] } {
		varDoSet /$ins/setpoint_$io -T "$title set point" -units [varNumGetUnits /$ins/read_$chan] \
			-m "Setpoint for output $io controlling on input $chan"
		varDoSet /$ins/heat_range_$io -T "$title heat range" -m ""
		varDoSet /$ins/current_read_$io -T "$title heater current" -m ""
	} else {
		varDoSet /$ins/setpoint_$io -T "Control $io set point" -m ""
		varDoSet /$ins/heat_range_$io -T "Heater $io range" -m ""
		varDoSet /$ins/current_read_$io -T "Output $io current" -m ""
	}
}

proc L336_control_chan_w { io ins target } {
	set buf [insIfRRT /$ins "OUTMODE? $io" 64]
	if { [scan $buf { %d,%d,%d} mode inch p] != 3 } {
		return -code error "bad outmode: $buf"
	}
	insIfWRT /$ins "OUTMODE $io,$mode,$target,$p"
	L336_control_chan_r $io $ins
}

# selections "off" "Manual PID" "Zone PID" "Constant Out". Autotune modes are separate.
proc L336_control_mode_r { io ins } {
	insIfReadVerify /$ins "OUTMODE? $io" 64 /$ins/output_$io/control_mode { %d,} 2
	if { [varGetVal /$ins/output_$io/control_mode] == 3 } {
		varDoSet /$ins/setpoint_$io -d off -r off
		varDoSet /$ins/set_current_$io -d on -r on
	} else {
		varDoSet /$ins/setpoint_$io -d on -r on
		varDoSet /$ins/set_current_$io -d off -r off
	}
}

proc L336_control_mode_w { io ins target } {
	set buf [insIfRRT /$ins "OUTMODE? $io" 64]
	if { [scan $buf { %d,%d,} mode inch] != 2 } {
		return -code error $buf
	}
	set pup [expr {$target>0 && $inch>0}]
	insIfWRT /$ins "OUTMODE $io,$target,$inch,$pup"
	L336_control_mode_r $io $ins
}

# autotune selections "Idle" "P" "PI" "PID" "Busy" 
# Tuning can only be active on one output at a time. It runs once, then stops. There is no way 
# to read back what type of autotune has been started, so use sticky value or "busy".
proc L336_autotune_r { io ins } {
	set prev [varGetVal /$ins/output_$io/autotune]
	set buf [insIfRRT /$ins "TUNEST?" 64]
	if { [scan $buf { %d,%d,%d,%d} act out err stage ] < 4 } {
		return -code error "Bad autotune readback \"$buf\""
	}
	set v [expr {$act? ($out==$io && $prev>0 ? $prev : 4) : 0}]
	if { $v } {
		varDoSet /$ins/output_$io/autotune -v $v -p on -p_int 11 -m "Busy tuning"
	} elseif { $err } {
		varDoSet /$ins/output_$io/autotune -v $v -p off -m "Failed autotuning at stage $stage"
	} else {
		varDoSet /$ins/output_$io/autotune -v $v -p off -m ""
	}
}

proc L336_autotune_w { io ins target } {
	set prev [varGetVal /$ins/output_$io/autotune]
	set buf [insIfRRT /$ins "TUNEST?" 64]
	if { [scan $buf { %d,%d,%d,%d} act out err stage] < 3 } {
		return -code error "Bad autotune readback \"$buf\""
	}
	if { $act } {
		varRead /$ins/output_$io/autotune
		return -code error "Autotuning is busy"
	}
	if { $target > 0 && $target < 4 } {
		insIfWRT /$ins "ATUNE $io,[expr {$target - 1}]"
		varDoSet /$ins/output_$io/autotune -v $target -p on -p_int 5
	}
}

# Unlike other Lakeshore instruments, all settings in a list must be provided,
# so the list must be read first in order to set one value.
proc L336_PID_r { io ins } {
	set buf [insIfRRT /$ins "PID? $io" 48]
	if { [scan $buf { %f,%f,%f} p i d ] < 3 } {
		return -code error "Bad PID Readback \"$buf\""
	}
	varDoSet /$ins/output_$io/P -v $p
	varDoSet /$ins/output_$io/I -v $i
	varDoSet /$ins/output_$io/D -v $d
}

# $which should be p, i, or d, matching the variable names
proc L336_PID_w { io which ins target } {
	if { $target < 0 || $target > 999 } {
		return -code error "invalid value \"$target\""
	}
	set val [format %.4f $target]
	set buf [insIfRRT /$ins "PID? $io" 48]
	if { [scan $buf { %f,%f,%f} p i d ] < 3 } {
		return -code error "Bad PID Readback \"$buf\""
	}
	set $which $val
	insIfWRT /$ins "PID $io,$p,$i,$d"
	L336_PID_r $io $ins
}

proc L336_heater_r { io ins } {
	set buf [insIfRRT /$ins "HTRSET? $io" 64]
	if { [scan $buf { %d,%d,%f,%d} rcode icode iuser ip] != 4 } {
		return -code error "Invalid heater readback: $buf"
	}
	varDoSet /$ins/output_$io/heater/resistance -v $rcode
	varDoSet /$ins/output_$io/heater/max_current -v [lindex [list $iuser 0.707 1.0 1.141 2.0] $icode]
}


#  Only for output ($io) 1 or 2
#  All output is scaled relative to this maximum. 
#  max-max-current is context (resistance, channel) dependent
proc L336_maxcurr_w { io ins target } {
	set buf [insIfRRT /$ins "HTRSET? $io" 64]
	if { [scan $buf { %d,%d,%f,%d} rcode icode iuser ip] != 4 } {
		return -code error "Invalid heater readback: $buf"
	}
	set icode 0
	for {set i 1} {$i<=4} {incr i} {
		if { abs($target - [lindex {0.0 0.707 1.0 1.141 2.0} $i]) < 0.015 } {
			set icode $i
		}
	}
	insIfWRT /$ins [format {HTRSET %d,%d,%d,%.3f,%d} $io $rcode $icode $target 1]
	L336_heater_r $io $ins
	set result [varGetVal /$ins/output_$io/heater/max_current]
	if { abs($target-$result) > 0.05 } {
		return -code error "Setting was reduced to limit of $result"
	}
}

proc L336_resistance_w { io ins target } {
	set buf [insIfRRT /$ins "HTRSET? $io" 64]
	if { [scan $buf { %d,%d,%f,%d} rcode icode iuser ip] != 4 } {
		return -code error "Invalid heater readback: $buf"
	}
	insIfWRT /$ins [format {HTRSET %d,%d,%d,%.3f,%d} $io $target $icode $iuser 1]
	L336_heater_r $io $ins
}

proc L336_apply_zones_w { io ins target } {
	set max 0.0
	set adj 0
	for { set i 1 } { $i <= 10 } { incr i } {
		set z [varGetVal /$ins/output_$io/zone/zone$i]
		# If this zone is blank, copy from previous.
		if { [llength $z] == 0 && $i > 1 } {
			set z [varGetVal /$ins/output_$io/zone/zone[expr {$i-1}]]
		}
		while { [llength $z] < 6 } { lappend z 0 }
		set T [lindex $z 0]
		if { $T < $max && $T > 0.0 } {
			if { $max < 300.0 } {
				return -code error "Invalid sequence of temperatures: must increase from zone1 to zone10"
			}
			set T [string trim [format %6.1f $max]]
			set z [lreplace $z 0 0 $T]
			varDoSet /$ins/output_$io/zone/zone$i -v $z
		}
		set max $T
		insIfWRT /$ins "ZONE $io,$i,$T,[join [lrange $z 2 5] {,}],[lindex $z 1]"
	}
}

proc L336_zone_w { io i ins target } {
	set z [join [split $target ,] " "]
	while { [llength $z] < 5 } { lappend z 0 }
	varDoSet /$ins/output_$io/zone/zone$i -v [lrange $z 0 5]
}

proc L336_zone_r { io i ins } {
	set z [insIfRRT /$ins "ZONE? $io,$i" 80]
	if { [scan $z " %f,%f,%f,%f,%f,%d" T P I D M H] == 6 } {
		foreach v {T P I D M} { # reduce round numbers to integers
			set vv [set $v]
			set $v [expr {($vv==round($vv)?round($vv):$vv)}]
		}
		varDoSet /$ins/output_$io/zone/zone$i -v [list $T $H $P $I $D $M]
	} else {
		return -code error "Invalid zone readback: \"$z\""
	}
}

#####################################################################################

proc L336_ramp_r { io ins } {
	set buf [insIfRRT /$ins "RAMP? $io" 48]
	if { [scan $buf " %d, %f" en rate] < 2 } {
		return -code error "Invalid Ramp reading $buf"
	}
	varDoSet /$ins/output_$io/ramp -v $en
	varDoSet /$ins/output_$io/ramp_rate -v $rate
}

proc L336_ramp_w { io ins target } {
	L336_ramp_r $io $ins
	set cmd [format "RAMP %d,%d,%.1f" $io $target [varGetVal /$ins/output_$io/ramp_rate]]
	insIfWriteVerify /$ins $cmd "RAMP? $io" 48 /$ins/output_$io/ramp " %d," 2 $target
}

proc L336_ramp_status_r { io ins } {
	insIfReadVerify /$ins "RAMPST? $io" 32 /$ins/output_$io/ramp_status " %d" 2
	varRead /$ins/setpoint_$io
}

proc L336_ramp_rate_w { io ins target } {
	if { $target < 0.0 || $target > 99.91 } { 
		return -code error "Invalid ramp rate.  Must be 0 to 99.9 (K/min)"
	}
	set en [expr {$target > 0.0}]
	set cmd [format "RAMP $io,%d,%.1f" $en $target]
	insIfWriteVerify /$ins $cmd "RAMP? $io" 48 /$ins/output_$io/ramp_rate " %*d, %f" 2 $target 0.2
	varDoSet /$ins/output_$io/ramp -v $en
}

########################################################################################

CAMP_STRUCT /~/inputs -D -d on -T "Configure inputs"

foreach _ich {A B C D} {

	CAMP_STRUCT /~/inputs/$_ich -D -d on -T "Configure input $_ich"

	CAMP_STRING /~/inputs/$_ich/title -D -R -S -d on -T "Channel $_ich title" \
		-r on -s on \
		-readProc [list L336_title_r $_ich] -writeProc [list L336_title_w $_ich]

	CAMP_SELECT /~/inputs/$_ich/units -D -R -S -T "Channel $_ich units" \
		-d on -r on -s on -v 0 -selections "" K C Raw \
		-readProc [list L336_sensor_r $_ich] -writeProc [list L336_units_w $_ich]

	CAMP_SELECT /~/inputs/$_ich/sensor -D -S -R -T "Channel $_ich input type" \
		-d on -s on -r on -selections " " Si GaAlAs PTCR_Pt NTCR_Cernox TC \
		-H "Types are: Si diode, GaAlAs diode, Positive coefficient resistor (Pt, RhFe), Negative (Cernox, CGR, Ge, ROX), and thermocouple" \
		-readProc [list L336_sensor_r $_ich] -writeProc [list L336_sensor_w $_ich]

	CAMP_INT /~/inputs/$_ich/curve -D -R -P -S -T "Channel $_ich curve" \
		-d on -r on -s on -p off -p_int 0.1 \
		-H "Load or read the ${_ich}-channel calibration curve" \
		-readProc [list L336_curve_r $_ich] -writeProc [list L336_set_curve $_ich]

	CAMP_SELECT /~/inputs/$_ich/filter -D -R -S -T "${_ich}-Filtering" \
		-H "Enable $_ich channel filtering" \
		-d on -s on -r on -selections Off On \
		-readProc [list L336_filter_r $_ich] -writeProc [list L336_filter_w $_ich filter]

	CAMP_INT /~/inputs/$_ich/f_points -D -R -S -T "${_ich}-Points" \
		-H "Number of points for $_ich channel filtering (2-64)" \
		-d on -s on -r on -units " " \
		-readProc [list L336_filter_r $_ich] -writeProc [list L336_filter_w $_ich points]

	CAMP_INT /~/inputs/$_ich/f_window -D -R -S -T "${_ich}-Window" \
		-H "Window for $_ich channel filtering, as percentage of full scale (1-10%)" \
		-d on -s on -r on -units "%" \
		-readProc [list L336_filter_r $_ich] -writeProc [list L336_filter_w $_ich window]
}

proc L336_title_r { ch ins } {
	set buf [string trim [insIfRRT /$ins "INNAME? $ch" 64]]
	varDoSet /$ins/inputs/$ch/title -v "$buf"
	set what "reading"
	set w "_read"
	catch {
	L336_sensor_r $ch $ins
	set su [varGetVal /$ins/inputs/$ch/units]
	if { $su == 1 || $su == 2 } { 
			set what "temperature"
			set w "_temp"
		}
	}
	if { [string length $buf] } {
		set title "$buf $what"
	} else {
		set title "$ch $what"
	}
	varDoSet /$ins/read_$ch -T $title
	if { [string length $buf] && [varGetVal /$ins/setup/names] } {
		set tt [join [split $buf " /{}()!@\#%^&*"] {}]
		varDoSet /$ins/read_$ch -m "alias: [string range $tt 0 9]$w Chan $ch $title "
	} else {
		varDoSet /$ins/read_$ch -m ""
	}

	# These will propagate title to setpoints and heater:
	varRead /$ins/output_1/control_chan
	varRead /$ins/output_2/control_chan
}

proc L336_title_w { ch ins target} {
	set title [string trim [join [split $target ",;\"\n"]]]
	if { [string length $title] > 15 } {
		return -code error "Title is too long -- 15 characters max"
	}
	insIfWRT /$ins "INNAME $ch, \"$title\""
	varRead /$ins/inputs/$ch/title
}

proc L336_units_w { ch ins target } {
	if { $target==0 } {return }
	set buf [insIfRRT /$ins "INTYPE? $ch" 64]
	if { [scan $buf { %d,%d,%d,%d,%d} t ar r c u] < 5 } {
		return -code error "Bad input type readback"
	}
	insIfWRT /$ins "INTYPE $ch,$t,$ar,$r,$c,$target"
	varRead /$ins/inputs/$ch/title
	#L336_sensor_r $ch $ins
	#varRead /$ins/output_1/control_chan
	#varRead /$ins/output_2/control_chan
}

proc L336_sensor_r { ch ins } {
	set buf [insIfRRT /$ins "INTYPE? $ch" 64]
	if { [scan $buf { %d,%d,%d,%d,%d} t ar r c u] == 5 } {
		varDoSet /$ins/inputs/$ch/sensor -v [expr {$t + ($t==1? $r:0) + ($t>1? 1:0)}]
		set units [lindex [list "" K C [lindex {"" V Ohm Ohm mV} $t]] $u]
		varDoSet /$ins/inputs/$ch/units -v $u
		varDoSet /$ins/read_$ch -units $units
	}
}

proc L336_sensor_w { ch ins target } {
	L336_sensor_r $ch $ins
	set it [lindex {0 1 1 2 3 4} $target]
	set ar [lindex {0 0 0 1 1 0} $target]
	set r  [lindex {0 0 1 0 0 0} $target]
	set c  [lindex {0 0 0 1 1 0} $target]
	set u  [varGetVal /$ins/inputs/$ch/units]
	if { $target == 3 } {
		# Platinum selected -- first check with a low excitation (pretend Si)
		set volts 9.
		set okv [scan [insIfRRT /$ins "INTYPE $ch,1,0,0,0,1; SRDG? $ch" 48] { %f} volts ]
		if { $volts == 0.0 || $volts > 5.0 || $okv < 1 } {# Double check before rejecting
			set okv [scan [insIfRRT /$ins "INTYPE $ch,1,0,0,0,1; SRDG? $ch" 48] { %f} volts ]
		}
		# *** Instead of using silicon, maybe use Pt but with manually-low excitation")
		if { $volts > 0.03 || $volts == 0.0 } {# MORE TESTS HERE!!!!
			L336_sensor_r $ch $ins
			return -code error "Platinum Resistor Rejected.  Volts = $volts"
		}
	}
	insIfWRT /$ins "INTYPE $ch,$it,$ar,$r,$c,$u"
	L336_sensor_r $ch $ins
	# Select curve automatically for some sensors
	set cn [lindex {0 1 0 6 0 0} $target]
	if { $cn } {
		varSet /$ins/inputs/$ch/curve -v $cn
	}
	varRead /$ins/output_1/control_chan
	varRead /$ins/output_2/control_chan
}

proc L336_filter_r { ch ins } {
	set buf [insIfRRT /$ins "FILTER? $ch" 48]
	if {[scan $buf " %d, %d, %d" o p w] != 3} {
		return -code error "Invalid filter readback: $buf"
	}
	varDoSet /$ins/inputs/$ch/filter -v $o
	varDoSet /$ins/inputs/$ch/f_points -v $p
	varDoSet /$ins/inputs/$ch/f_window -v $w
}

proc L336_filter_w { ch par ins val } {
	set buf [insIfRRT /$ins "FILTER? $ch" 48]
	if {[scan $buf " %d, %d, %d" o p w] != 3} {
		return -code error "Invalid filter readback: $buf"
	}
	switch $par {
		filter {
			set o $val
		}
		points {
			if {$val < 2 || $val > 64 } {
				return -code error "Invalid number of points (must be 2-64)"
			}
			set p $val
		}
		window {
			if {$val < 1 || $val > 10 } {
				return -code error "Invalid window (must be 1-10%)"
			}
			set w $val
		}
	}
	insIfWRT /$ins "FILTER $ch,$o,$p,$w"
	L336_filter_r $ch $ins
}

########################################################################################

proc L336_curve_r { ch ins } {
	global CAMP_VAR_ATTR_POLL
	if { [varGetStatus /$ins/inputs/$ch/curve] & $CAMP_VAR_ATTR_POLL } {# read and validate
		varDoSet /$ins/inputs/$ch/curve -p off
		L336_read_curve $ch 2 $ins
	} else {
		L336_read_curve $ch 0 $ins
	}
}

# For setting [ABCD]_curve. $curve must be an integer. May initiate polling of inputs/curve/do_load
proc L336_set_curve { ch ins curve } {
	upvar \#0 ${ins}_cache cache
	if { $curve < 0 } {
		varDoSet /$ins/inputs/$ch/curve -units "FAIL \"$curve\""
		return -code error "bad value \"$curve\""
	} elseif { $curve <= 59 } {# max curve index in the 336
		set sn ""
		# always update cached header
		set cache(crvhdr_$curve) [insIfRRT /$ins "CRVHDR? $curve" 80]
		scan $cache(crvhdr_$curve) {%[^,],%[^,],} name sn
		set sn [string trim $sn]
		if { $sn == "" } {
			varDoSet /$ins/inputs/$ch/curve -units "EMPTY" -m "Curve $curve is corrupt or empty"
			return -code error "Curve $curve is corrupt or empty"
		}
		regexp -nocase {^[a-z]*([0-9]+)$} $sn sid sn
		set num $curve
		set curve $sn
	} else {# A sensor serial number
		# First see if it is already loaded, in range 21-35 (also locate unused in that range)
		set num 0; set unused 0; set read 0
		for {set i 21} {$i<=35} {incr i} {
			if { ! [info exists cache(crvhdr_$i)] } {
				set cache(crvhdr_$i) [insIfRRT /$ins "CRVHDR? $i" 80]
				set read $i
			}
			if { [scan $cache(crvhdr_$i) {%[^,],%[^,],%d} name sn fmt ] == 3 } {
				set sn [string trim $sn]
				if { $read != $i && [string match "*$curve" $sn] } {
					# curve may be loaded, according to old cache, but recheck before believing.
					set cache(crvhdr_$i) [insIfRRT /$ins "CRVHDR? $i" 80]
					scan $cache(crvhdr_$i) {%[^,],%[^,],%d} name sn fmt
					set sn [string trim $sn]
				}
				if { [string match "*$curve" $sn] && [regexp -nocase {^[a-z]*([0-9]+)$} $sn sid sn]} {
					set num $i
					break
				}
				if {$sn == "" && $unused == 0} { set unused $i }
			}
		}
		if { $num == 0 } {# User curve not already loaded
			if { $unused > 0 } {# Use unoccupied slot when possible
				set num $unused
			} else {# Pick a curve slot not currently referenced, and not the previous, in range 21-35
				foreach i {A B C D} {
					if { $cache(${i}curve) == 999 } {
						scan [insIfRRT /$ins "INCRV? $i" 48] { %d} cache(${i}curve)
					}
					set ${i}curve $cache(${i}curve)
				}
				set prev [varGetVal /$ins/inputs/curve/number]
				set num [expr { ($prev<21 || $prev>33) ? 25 : $prev+1 }]
				while { $num==$Acurve || $num==$Bcurve || $num==$Ccurve || $num==$Dcurve } {
					set num [expr { $num>=35 ? 21 : $num+1 }]
				}
			}
			# now have number to load into.
			varDoSet /$ins/inputs/$ch/curve -units "LOAD" -m "Loading curve \"$curve\" as number $num"
			# Initiate the actual loading (asynchronously)
			L336_setup_load $ins $ch $curve $num
			return
		}
	}
	# Select the curve (which we did not load)
	set n [L336_select_curve $ins $ch $num]
	if { $num != $n } {# Failed to select curve
		set e "Could not select curve $num for channel $ch"
		varDoSet /$ins/inputs/$ch/curve -units "FAIL" -m $e
		if { $num >= 21 } { # A user curve - try reloading
			varDoSet /$ins/inputs/$ch/curve -units "LOAD" -m "Reloading corrupt curve \"$curve\" as number $num"
			# Initiate asynchronous reloading
			L336_setup_load $ins $ch $curve $num
			return
		}
		return -code error $e
	}
	# read curve info without validation (sets curve/file to the name in the curve header)
	L336_read_curve $ch 0 $ins
	# Put curve to use
	if {$num>0} {varSet /$ins/inputs/$ch/units -v "K"}
	# NO LONGER ... if file available, then schedule asynchronous validation
}

# Select the curve for the channel. This also verifies the integrity, somewhat.  
# There is some delayed response to the selection, so do some retries. 
# Return the proper number on success.

proc L336_select_curve { ins ch num } {
	upvar \#0 ${ins}_cache cache
	insIfWRT /$ins "INCRV $ch,$num"
	set i -5
	set n 0
	while { [incr i] < 0 && $n != $num } {
		catch {
			sleep 0.2
			scan [insIfRRT /$ins "INCRV? $ch" 48] { %d} n
			set cache(${ch}curve) $n
		}
	}
	return $n
}

# Set up the asynchronous loading of a curve.  Try to take the "best" file
# type, and allow for several types for the same sensor. The silly glob-all
# followed by lsearch is because file name from header is uppercased, and
# some problem on vxworks.  $curve is a number, but could be extended to
# other strings.
proc L336_setup_load { ins ch curve num} {
	set matches {}
	set lcc [string tolower $curve]
	catch { set matches [glob "./dat/${lcc}.*"] }
	set ei 0
	set fi -1
	foreach ext [list 340 spl 330 dat tr tc tbl] {
		incr ei
		set fi [lsearch -glob $matches "*.$ext"]
		if { $fi >= 0 } { break }
	}
	if { $fi < 0 } {
		varDoSet /$ins/inputs/$ch/curve -units "NO FILE" -m \
			"Failed to find a data file for curve \"$curve\"."
		return -code error "curve \"$curve\" not found" 
	}
	set file [file tail [lindex $matches $fi]]
	# default format may be changed later by information in the file
	set format [lindex { 0 2 2 2 4 3 1 2 0} $ei]
	# Put parameters in place to perform the loading asynchronously
	set iic "/$ins/inputs/curve"
	varSet $iic/number -v $num
	varSet $iic/format -v $format
	varSet $iic/id -v $curve
	varSet $iic/file -v $file
	varDoSet $iic/chan -v $ch
	varDoSet $iic/do_load -p on -p_int 0.1
}
	
# ch is A, B, C, D, or "", 
# validate is:
#   0 just read header, 
#NA 1 just read header now, but schedule read and validation asynchronously
#   2 read header and start validate now -- validation completes asynchronously

proc L336_read_curve { ch validate ins } {
	global CAMP_VAR_ATTR_POLL
	upvar \#0 ${ins}_cache cache
	set iic "/$ins/inputs/curve"
	varDoSet $iic/validate -m ""
	set num -1
	if { [string match {[ABCD]} $ch] } {
		set curvar "/$ins/inputs/$ch/curve"
		set buf [insIfRRT /$ins "INCRV? $ch" 48]
		set num -99
		scan $buf " %d" num
	} else {
		set curvar "$iic/number"
		set num [varGetVal $curvar]
	}
	if {$num<0 || $num>59} {
		set e "Invalid curve number $num"
		varDoSet $curvar -units "FAIL" -m $e
		return -code error $e
	}
	set cache(${ch}curve) $num
	# Preliminary:
	varDoSet $curvar -v $num -units "Num.$num" -m \
		[lindex [list "No curve" "Standard curve $num" "User curve $num"] [expr {($num>0)+($num>20)}]]
	if { $num==0 } { return } 
	set curve $num
	# Fetch curve header
	set buf [insIfRRT /$ins "CRVHDR? $num" 80]
	if { [scan $buf {%[^,],%[^,],%d} name sn fmt ] != 3 } {
		set e "failed read of curve $num"
		varDoSet $curvar -v $num -m $e
		return -code error $e
	}
	set cache(crvhdr_$num) $buf
	set units [lindex {{} {mV/K} {V/K} {Ohm/K} {logOhm/K} {logOhm/logK}} $fmt]
	set name [string trim $name "\" "]
	set sn [string trim $sn "\" "]
	set curve [string tolower $sn]
	if { $num < 21 } {
		varDoSet $curvar -units $units -m "Standard curve $name"
		return
	}
	set file [string tolower $name]
	if { [string length $curve] && [string compare $name "User Curve"] } {
		set curvenum 0
		regexp -nocase {^[a-z]*([0-9]+)$} $curve sid curvenum
		if { [string first "/" $file] < 0 } { set file "./dat/$file" }
		set dir [file dirname $file]
		set tail [file tail $file]
		set root [file rootname $tail]
		if { [string compare [string toupper $root] $root] } {# name might be wrong case
			set flist [glob "$dir/*.*"]
		} else {
			set flist [glob "$dir/$root.*"]
		}
		set matches {}
		foreach ff $flist {
			set lcf [string tolower [file tail $ff]]
			if { ! [string compare $lcf $tail] } {# case-insensitive match
				lappend matches $ff
			}
		}
		if { [llength $matches] == 0 } {
			set file ""
		} else {
			set file [lindex $matches 0]
			if { [string match "./dat/*.*" [string tolower $file]] } {
				set file [file tail $file]
				set name $file
			}
		}
		# get integer identifier, either serial number or curve (slot) number
		if { $curvenum == 0 } {
			if { [scan $root "%d %c" curvenum i] != 1 } {
				set curvenum $num
			}
		}
	} else {
		varDoSet $curvar -v $num -m "Empty User Curve $num"
		return
	}
	set uvcd "Curve $num containing $curve ($name)"
	varDoSet $iic/number -v $num
	varDoSet $iic/file -v $file
	varDoSet $iic/id -v $curve
	varDoSet $iic/format -v $fmt
	varDoSet $iic/chan -v $ch
	if { [string match {[ABCD]} $ch] } {
		varDoSet /$ins/inputs/$ch/curve -v $curvenum -m $uvcd -units $units
		set curve $curvenum
	}
	if { $validate == 0 } { return }
	if { [string length $file] == 0 || \
		  ([varGetStatus $iic/validate] & $CAMP_VAR_ATTR_POLL) || \
		  ([varGetStatus $iic/do_load] & $CAMP_VAR_ATTR_POLL) } {
		varDoSet $curvar -m "$uvcd (unvalidated)"
		return
	}
	if { $validate == 1 && [string match {[ABCD]} $ch] } {
		varDoSet $curvar -p on -p_int 1 -m "$uvcd, (to be validated)"
		return
	}
	# else,  validate == 2 : validate starting now
	varDoSet $curvar -m "$uvcd, validating..."
	set cache(v:chan) $ch
	set cache(v:msg) $uvcd
	if { [catch {L336_curve_validate $ins } msg] } {
		varDoSet $curvar -m "$uvcd. Validation failed: $msg"
		varDoSet $iic/validate -m "Validation failed: $msg"
		set cache(crvhdr_$num) ""
		return -code error $msg
	}
}

# Start validation of curve data against file. This is invoked by read curve polled after 
# loading, or by setting curve/validate.  Validation of points is carried out in chunks by 
# polling curve/validate
proc L336_curve_validate { ins } {
	upvar \#0 ${ins}_cache cache
	set iic "/$ins/inputs/curve"
	set num [varGetVal $iic/number]
	set file [varGetVal $iic/file]
	varDoSet $iic/validate -m "Open file $file"
	if { [string first "/" $file] < 0 } { set file "./dat/$file" }
	if { [catch {set fid [open $file r]}] } {
		return -code error "Could not open file $file"
	}
	set npt 0
	set ncol 2
	# read header lines until we get to the curve data
	varDoSet $iic/validate -v 1 -m "Read header of file $file"
	set nln 0
	while { 1 } {
		incr nln
		if { [gets $fid line] == -1 } {
			close $fid
			return -code error "Failed to read a line ($nln)"
		}
		set line [string trim $line]
		set ns [scan $line "%d %c" v1 xx]
		if { $ns == 1 || ($ns > 1 && $xx == 44) } {# allow comma after number
			set npt $v1
		} elseif { [scan $line {%d %f %f %c} v1 v2 v3 xx] == 3 && $v1 == 1 } {
			set ncol 3
			break
		} elseif { [scan $line {%f %f %f %c} v1 v2 v3 xx] == 3 } {
			set ncol 2
			break
		} elseif { [scan $line {%f %f %c} v1 v2 xx] == 2 } {
			set ncol 2
			break
		} elseif { [scan $line "Serial Number: %s %c" v1 xx] == 1 } {
			set sn $v1
		} elseif { [scan $line "Data Format: %d" v1] == 1 } {
			if { [varGetVal $iic/format] != $v1 } {
				return -code error "mismatched format codes"
			}
		} elseif { [scan $line "Number of Breakpoints: %d" v1] == 1 } {
			set npt $v1
		}
	}
	# Collect data lines, but we already have the first in $line
	set cache(v:lines) [linsert [split [string trim [read $fid]] "\n"] 0 $line]
	close $fid
	set ll [llength $cache(v:lines)]
	if { $npt > 0 } {
		if { $npt != $ll } {
			set cache(v:lines) {}
			return -code error "Bad file: expected $npt data points, but found $ll"
		}
	} else {
		set npt $ll
	}
	# We made it to the data. Do the rest later by polling.
	set cache(v:num) $num
	set cache(v:npt) $npt
	set cache(v:ncol) $ncol
	set cache(v:i) 0
	varDoSet $iic/validate -p on -p_int 0.1 -v 1 -m "Validate against $file with $npt points"
}

# This is the asynchronous part, so put errors in messages
proc L336_validate_more { ins } {
	upvar \#0 ${ins}_cache cache
	set iic "/$ins/inputs/curve"
	set npt $cache(v:npt)
	set ini $cache(v:i)
	set num $cache(v:num)
	set pt [expr {$ini+1}]
	set ch $cache(v:chan)
	set scancmd [lindex { {} {}
		{[scan $line " %f %f" tf vf]}
		{[scan $line " %*d %f %f" vf tf]}
	} $cache(v:ncol)]
	set err ""
	for { set i $ini } { $i < $npt } { incr i; incr pt } {
		set line [lindex $cache(v:lines) $i]
		if "$scancmd < 2" {# quotes for double substitution in expr
			set err "bad data format in $file at point $pt"; break
		}
		if { [catch {insIfRRT /$ins "CRVPT? $num,$pt" 64} buf] || \
			 [scan $buf { %f,%f} vi ti] != 2 } {
			set err "invalid readback of point $pt: $buf"; break
		}
		if { abs($vf-$vi) > 0.00001 || abs($tf-$ti) > 0.001 } {
			set err "discrepancy at $pt: '$line' vs '$buf'"; break
		}
		if { $i % 10 == 9 && $i+1 < $npt } {
			set cache(v:i) [incr i]
			varDoSet $iic/validate -m "Validated $pt of $npt points"
			if { [string match {[ABCD]} $ch] } {
				varDoSet /$ins/inputs/$ch/curve -m "$cache(v:msg). Validated $pt of $npt points"
			}
			return
		}
	}
	if { [string length $err] } {
		set err "Validation failed: $err"
		varDoSet $iic/validate -p off -v 0 -m $err
		varDoSet $iic/do_load -m $err
		if { [string match {[ABCD]} $ch] } {
			varDoSet /$ins/inputs/$ch/curve -m "$cache(v:msg). $err"
		}
		set cache(v:lines) {}
		return -code error $err
	}
	varDoSet $iic/validate -p off -v 0 -m "Validated $npt points"
	varDoSet $iic/do_load -m ""
	if { [string match {[ABCD]} $ch] } {
		varDoSet /$ins/inputs/$ch/curve -m "$cache(v:msg). Validated."
	}
	set cache(v:lines) {}
}

############################################################################
#  Variables for loading curves.
#  Any curve file may be loaded into any curve number using these variables.
#  Curve loading and validation are done asynchronously by polling variables here.

CAMP_STRUCT /~/inputs/curve -D -d on -T "Curve Loading" \
	-H "Variables for loading curves (calibration tables)"

CAMP_INT /~/inputs/curve/number -D -S -T "Curve number" -d on -s on -units " " \
	-H "Identifies the internal curve number. Uploaded curves generally use 21-35, but the maximum is 59." \
	-writeProc L336_curve_number_w

proc L336_curve_number_w { ins target } {
	if { ($target < 21) || ($target > 59) } {
		return -code error "invalid curve number $target (expect 21-59)"
	}
	varDoSet /$ins/inputs/curve/number -v $target
}

CAMP_STRING /~/inputs/curve/file -D -S -R -T "Curve datafile" -d on -s on -r on \
	-readProc L336_curve_header_r -writeProc L336_curve_file_w

proc L336_curve_file_w { ins target } {
	varDoSet /$ins/inputs/curve/file -v $target
}

CAMP_SELECT /~/inputs/curve/format -D -S -R -T "Curve format" -d on -s on -r on \
	-selections "" {K,mV} {K,V} {K,Ohm} {K,logOhm} {logK,logOhm} \
	-readProc L336_curve_header_r -writeProc L336_curve_format_w

proc L336_curve_format_w { ins target } {
	varDoSet /$ins/inputs/curve/format -v $target
}

# Here the id is a string (not integer) for more generality.
CAMP_STRING /~/inputs/curve/id -D -S -R -T "Curve ID (serial num)" -d on -s on -r on \
	-H "The sensor curve's serial number or other identifier." \
	-readProc L336_curve_header_r -writeProc L336_curve_id_w

proc L336_curve_id_w { ins target } {
	varDoSet /$ins/inputs/curve/id -v [string trim [string tolower $target]]
}

proc L336_curve_header_r { ins } {
	L336_read_curve "" 0 $ins
}

CAMP_SELECT /~/inputs/curve/chan -D -S -T "Channel loading" \
	-H "Sensor channel to load curve for (A, B, C, D, or blank to load curves for future use)." \
	-s on -d on -selections "" A B C D \
	-writeProc L336_curve_chan_w

proc L336_curve_chan_w { ins ch } {
	varDoSet /$ins/inputs/curve/chan -v $ch
}

CAMP_SELECT /~/inputs/curve/do_load -D -S -P -T "Do curve load" \
	-H "Load the curve now" \
	-d on -s on -selections  "" "LOAD" \
	-readProc L336_curve_doload_r -writeProc L336_curve_doload_w

# Since do_load is activated by polling, capture errors and emit them as messages
proc L336_curve_doload_r { ins } {
	set iic "/$ins/inputs/curve"
	set num [varGetVal $iic/number]
	set ch [varSelGetValLabel $iic/chan]
	varDoSet $iic/do_load -m "Loading curve file [varGetVal $iic/file] as curve $num" 
	if { [catch {L336_curve_do_load $ins} msg] } {
		varDoSet $iic/do_load -v 0 -p off -m $msg
		if { [string match {[ABCD]} $ch] } {
			varDoSet /$ins/inputs/$ch/curve -units FAIL -m "Failed to load curve data: $msg"
		}
		varDoSet $iic/do_load -m "Failed to load curve data: $msg"
		varDoSet $iic/chan -v 0
		return
	}
	if { [string match {[ABCD]} $ch] } {
		set n [L336_select_curve $ins $ch $num]
		if { $n != $num } {
			varDoSet $iic/do_load -v 0 -p off -m "Failed to select curve $num for channel $ch"
			varDoSet /$ins/inputs/$ch/curve -units FAIL -m "Failed to select curve num $num"
			return
		}
		varSet /$ins/inputs/$ch/units -v "K"
	}
	varDoSet $iic/do_load -v 0 -p off -m "Validating curve data"
	if { [catch { L336_read_curve $ch 2 $ins } msg] } {
		varDoSet $iic/do_load -m "Validation failed: $msg"
	} else {
		varDoSet $iic/do_load -m "Loaded $num... validating..."
	}
}

proc L336_curve_doload_w { ins target } {
	global CAMP_VAR_ATTR_POLL
	if { $target == 0 } {
		varDoSet /$ins/inputs/curve/do_load -v $target -p off -m ""
		return
	} else {
		if { ([varGetStatus /$ins/inputs/curve/do_load] & $CAMP_VAR_ATTR_POLL) } {
			return -code error "Busy loading a curve already"
		}
		varDoSet /$ins/inputs/curve/do_load -v $target -p on -p_int 0.1 -m "Start loading curve"
		#L336_curve_do_load $ins
	}
}

CAMP_SELECT /~/inputs/curve/validate -D -S -R -P -T "Validate curve" \
	-H "Validate curve in memory compared with file" \
	-d on -s on -r off -p off -selections  "" "Validate" \
	-readProc L336_validate_more -writeProc L336_curve_validate_w

# Validation starts when this is set (or L336_curve_validate invoked elsewhere),
# and it completes by repeated polls (using L336_validate_more).
proc L336_curve_validate_w { ins target } {
	global CAMP_VAR_ATTR_POLL
	upvar \#0 ${ins}_cache cache
	if { $target == 0 } { 
		varDoSet /$ins/inputs/curve/validate -v $target -p off -m ""
		set cache(lines) {}
		return
	} else {
		if { [varGetStatus /$ins/inputs/curve/do_load] & $CAMP_VAR_ATTR_POLL } {
			return -code error "Busy loading currently"
		}
		set cache(v:chan) ""
		set cache(v:msg) ""
		if { [catch {L336_curve_validate $ins} msg] } {
			varDoSet /$ins/inputs/curve/validate -v 0 -p off -m "Validation failed: $msg"
			set cache(lines) {}
			return -code error  "Validation failed: $msg"
		}
	}
}

CAMP_SELECT /~/inputs/curve/delete -D -S -T "Delete curve" \
	-H "Delete curve from slot {number}" \
	-d on -s on -selections  "" "Delete" "Disuse&Delete" \
	-writeProc L336_curve_delete_w

proc L336_curve_delete_w { ins target } {
	upvar \#0 ${ins}_cache cache
	if { $target > 0 } {
		set num [varGetVal /$ins/inputs/curve/number]
		if { $num < 21 || $num > 59 } {
			return -code error "Only 'user' curves in the range 21 to 59 may be deleted"
		}
		foreach ch {A B C D} {
			if { $cache(${ch}curve) == $num } {
				if { $target == 1 } {
					return -code error "Sorry, curve number $num is being used by channel $ch"
				} else {
					set cache(${ch}curve) 0
				}
			}
		}
		insIfWRT /$ins "CRVDEL $num"
		set cache(crvhdr_$num) ""
	}
}

# Here we load a curve from a file, using the variables on the inputs/curve page.
# Some parameters may be overridden by information in the curve file.
proc L336_curve_do_load { ins } {
	upvar \#0 ${ins}_cache cache
	set iic /$ins/inputs/curve
	set file [varGetVal $iic/file]
	set name [file tail $file]
	if { ! [string compare $name $file] } {set file "./dat/$file"}
	set num [varGetVal $iic/number]
	set fmt [varGetVal $iic/format]
	set coef 0
	set sn [varGetVal $iic/id]
	set npt 0
	set ncol 2
	set lim 999
	varDoSet $iic/do_load -v 1 -p off
	if { [catch {set fid [open $file r]}] } {
		return -code error "could not open $file"
	}
	# read header lines until we get to the curve data
	set nln 0
	while { 1 } {
		incr nln
		if { [gets $fid line] == -1 } {
			close $fid
			return -code error "Failed to read a line ($nln)"
		}
		set line [string trim $line]
		set ns [scan $line "%d %c" v1 xx]
		if { $ns == 1 || ($ns > 1 && $xx == 44) } {
			set npt $v1
		} elseif { [scan $line {%d %f %f %c} v1 v2 v3 xx] == 3 && $v1 == 1 } {
			set ncol 3
			break
		} elseif { [scan $line {%f %f %f %c} v1 v2 v3 xx] == 3 } {
			set ncol 2
			break
		} elseif { [scan $line {%f %f %c} v1 v2 xx] == 2 } {
			set ncol 2
			break
		} elseif { [scan $line "Serial Number: %s %c" v1 xx] == 1 } {
			set sn $v1
		} elseif { [scan $line "Data Format: %d" v1] == 1 } {
			set fmt $v1
			varDoSet $iic/format -v $fmt
		} elseif { [scan $line "Temperature coefficient: %d" v1] == 1 } {
			set coef $v1
		} elseif { [scan $line "Number of Breakpoints: %d" v1] == 1 } {
			set npt $v1
		} elseif { [scan $line "Setpoint Limit: %f" v1] == 1 } {
			set lim $v1
		}
	}
	if { $coef == 0 } { set coef [lindex {0 2 1 2 1} $fmt] }

	# Collect data lines, but we already have the first in $line
	set lines [linsert [split [string trim [read $fid]] "\n"] 0 $line]
	close $fid
	if { $npt > 0 } {
		if { $npt != [llength $lines] } {
			return -code error "bad file $file: expected $npt data points, but found [llength $lines]"
		}
	} else {
		set npt [llength $lines]
	}
	# delete the curve in memory 
	insIfWRT /$ins [format "CRVDEL %2d" $num]
	# send the new header
	insIfWRT /$ins [format "CRVHDR %2d,%s,%s,%d,%.2f,%d" $num $name $sn $fmt $lim $coef]
	# 
	if { $ncol == 3 } {
		set scancmd {[scan $line " %*d %f %f" v t]}
	} else {
		set scancmd {[scan $line " %f %f" t v]}
	}
	# 
	set cmd_line ""
	set i 0
	set maxt -1.0
	foreach line $lines {
		incr i
		if "$scancmd < 2" {# quotes for double substitution in expr; see above
			return -code error "bad format in file $file at line $nln"
		}
		if { $t > $maxt } { set maxt $t }
		if { $i > 1 } {
			if { ( $t != $tp ) && ( $v != $vp ) } {
				set s [expr ($v-$vp)/($t-$tp)]
				if { $coef == 0 } { set coef [expr { ($s>0.0) + 1 }] }
				if { ($s > 0.0) != ($coef - 1) } {
					return -code error "bad file: slope changed sign at point $i, line $nln"
				}
			}
		}
		set tp $t ; set vp $v
		if { $t < 99.9 } {
			set cmd [format "CRVPT %d,%d,%.5f,%.4f" $num $i $v $t]
		} else {
			set cmd [format "CRVPT %d,%d,%.5f,%.3f" $num $i $v $t]
		}
		set cll [string length $cmd_line]
		if { $cll == 0 } {
			set cmd_line $cmd
		} elseif { $cll + 1 + [string length $cmd] > 65 } {
			insIfWRT /$ins $cmd_line
			set cmd_line $cmd
		} else {
			append cmd_line ";" $cmd
		}
		incr nln
	}
	if { [string length $cmd_line] } {
		insIfWRT /$ins $cmd_line
	}
	if { $lim == 999 } { set lim $maxt }
	insIfWRT /$ins [format "CRVHDR %2d,%s,%s,%d,%.3f,%d" $num $name $sn $fmt $lim $coef]
}

####################################################################################

CAMP_STRUCT /~/setup -D -d on -T "Setup parameters"

CAMP_SELECT /~/setup/id -D -R -P -T "ID Query" -d on -r on -p off -p_int 1.0 \
	-selections FALSE TRUE -readProc L336_id_r

proc L336_id_r { ins } {
	global CAMP_VAR_ATTR_POLL
	upvar \#0 ${ins}_cache cache
	set id [varGetVal /$ins/setup/id]
	set p [expr [varGetStatus /$ins/setup/id] & $CAMP_VAR_ATTR_POLL]
	if { ! ($id && $p) } {
		set id 0
		set status [catch {insIfRRT /$ins "*IDN?" 60} buf]
		if { $status } { sleep 0.2; set status [catch {insIfRRT /$ins "*IDN?" 60} buf]}
		if { $status == 0 } {
			set id [scan $buf { LSCI,MODEL336,%*[^,],%f} val]
			if { $id != 1 } { set id 0 }
		}
		varDoSet /$ins/setup/id -v $id
	}
	if { ! ($id && $p) } { return }
	# polling:
	if { [varGetPollInterval /$ins/setup/id] == 0.125 } {
		varRead /$ins/output_1/heater/max_current
		varRead /$ins/output_2/heater/max_current
		varRead /$ins/output_1/control_mode
		varRead /$ins/output_2/control_mode
		foreach c {A B C D} {varRead /$ins/inputs/$c/units}
		varDoSet /$ins/setup/id -p on -p_int 0.25
		return
	}
	# read titles and preload cache of curve numbers
	foreach ch {A B C D} {
		if { $cache(${ch}curve) == 999 } {
			varRead /$ins/inputs/$ch/title
			scan [insIfRRT /$ins "INCRV? $ch" 60] { %d} cache(${ch}curve)
			return
		}
	}
	# preload cache of curve headers slowly
	varDoSet /$ins/setup/id -p on -p_int 1.0
	for {set i 21} {$i<=35} {incr i} {
		if { ! [info exists cache(crvhdr_$i)] } {
			set cache(crvhdr_$i) [insIfRRT /$ins "CRVHDR? $i" 80]
			return
		}
	}
	# Done initializing
	varDoSet /$ins/setup/id -p off
}

CAMP_SELECT /~/setup/names -D -S -T "Name treatment" -d on -s on \
	-selections normal aliases \
	-H "Select how to display some variable names, with the fixed name or the variable title" \
	-writeProc L336_treat_w

proc L336_treat_w { ins val } {
	varDoSet /$ins/setup/names -v $val
	foreach ic {A B C D} {
		varRead /$ins/inputs/$ic/title
	}
}

unset _ich
unset _io
