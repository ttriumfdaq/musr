# -*- mode: tcl; tab-width: 4; -*-
# camp_ins_lake372.tcl: driver for Lakeshore model 372 temperature controller
# with the 3726 scanner option installed giving 16 virtual inputs, but fewer
# are supported in this driver.  Some variables and functions are defined
# specifically for running our DR.
# Inputs: name	   chan	   Outputs:	name	chan
#		  mix_cham  A				heater   0
#		  sample	1				warm-up  1
#		  M2		2				still    2
#		  M3		3
#		  M4		4
#		  M5		5
#		  M6		6
# The instrument does autoranging, but this driver does auto-excitation setting,
# auto-still heater control, and auto-heat-range-boost (in lieu of warmup heater,
# which cannot share a sensor with control).
#
# $Log: camp_ins_lake372.tcl,v $
# Revision 1.3  2018/06/15 05:08:25  asnd
# Changes to auto-still, curve loading & validation; shrink.
#
# Revision 1.2  2017/06/21 04:21:44  asnd
# All the improvements from the first run period.
#
# Revision 1.1  2017/05/17 03:47:31  asnd
# New instrument Lakeshore 372 DR temperature controller. Driver has customizations for our DR.
#

CAMP_INSTRUMENT /~ -D -T "LakeShore 372" \
	-H "LakeShore 372 DR Temperature Controller" -d on \
	-initProc L372_init -deleteProc L372_delete \
	-onlineProc L372_online -offlineProc L372_offline

proc L372_init { ins } {
	global ${ins}_cache
	set ${ins}_cache(can_reload) 0
	set ${ins}_cache(TdiffStill) {}
	foreach c {A 1 2 3 4 5 6} {set ${ins}_cache(${c}curve) 999}
	insSet /$ins -if tcpip 0.15 2 142.90.154.124 7777 CRLF CRLF
}
# insSet /$ins -if gpib 0.1 2 16 CRLF CRLF

proc L372_delete { ins } {
	global ${ins}_cache
	insSet /$ins -line off
	unset ${ins}_cache
}

proc L372_online { ins } {
	global ${ins}_cache
	insIfOn /$ins
	varDoSet /$ins/setup/id -p off
	varRead /$ins/setup/id
	if { [varGetVal /$ins/setup/id] == 0 } {
		insIfOff /$ins
		return -code error "failed ID query, check interface definition and connections"
	}
	foreach c {A 1 2 3 4 5 6} {set ${ins}_cache(${c}curve) 999}
	varDoSet /$ins/setup/id -p on -p_int 0.5
	catch {
		varRead /$ins/outputs/heater/resistance
		varRead /$ins/outputs/warm-up/max_current
		varRead /$ins/inputs/mix_cham/units
		varRead /$ins/inputs/sample/units
	}
	if { [varGetVal /$ins/outputs/heater/resistance] != 500.0 } {
		varDoSet /$ins/outputs/heater/resistance -s on
		return -code error "Warning: heat_range powers assume a heater resistance of 500 ohm"
	}
}

proc L372_offline { ins } {
	insIfOff /$ins
}

# insIfRead and insIfWrite with a simple retry, for problem of flaky network.

proc insIfRRT {path req len} {
	if {[catch {insIfRead $path $req $len} msg]} {
		sleep 0.1
		if { [catch {insIfRead $path $req $len} msg] } {return -code error $msg}
	}
	return $msg
}

proc insIfWRT {path req} {
	if {[catch {insIfWrite $path $req} msg]} {
		sleep 0.1
		if {[catch {insIfWrite $path $req} msg]} {return -code error $msg}
	}
	return $msg
}

CAMP_FLOAT /~/read_mix_cham -D -R -P -L -A -T "Mixing chamber (A) reading" \
	-d on -r on -p_int 8 -l off -tol 0.1 -units K \
	-readProc [list L372_read_r mix_cham A]

CAMP_FLOAT /~/read_sample -D -R -P -L -T "Sample (M1) reading" \
	-d on -r on -p_int 15 \
	-readProc [list L372_read_r sample 1]

foreach _ch {2 3 4 5 6} {
CAMP_FLOAT /~/read_M$_ch -D -R -P -L -T "Channel M$_ch reading" \
	-d off -r off -p_int 15 \
	-readProc [list L372_read_r "M$_ch" $_ch]
}

# Always read resistance (R); maybe read temperature (K)
proc L372_read_r { vch ch ins } {
	set u [varGetVal /$ins/inputs/$vch/units]
	set cs [varGetVal /$ins/control_set]
	foreach v {R K} {
		set $v 0.0
		set buf [insIfRRT /$ins "RDG${v}? $ch" 48]
		scan $buf " %f" $v
		if { $u != 1 } { break }
	}
	set m [varGetTitle /$ins/read_$vch]
	set m [string range $m 0 [string first " reading" $m]]
	if { $u == 1 } {
		set bad [expr { $K == 0.0 ? " OUT OF RANGE" : "" }]
		append m "temperature reading" $bad "; raw resistance is " $R " ohm."
		if { $K == 0.0 } {
			varDoSet /$ins/read_$vch -units K -m $m
			set cs -99999 ; # trigger alarm for A
		} else {
			varDoSet /$ins/read_$vch -v $K -units K -m $m
		}
	} else {
		append m "resistance reading."
		varDoSet /$ins/read_$vch -v $R -units Ohm -m $m
	}
	if { "$ch" == "A" } {
		varTestAlert /$ins/read_$vch $cs
		L372_autostill $ins
	}
}

CAMP_FLOAT /~/control_set -D -R -S -P -L -T "Mixing chamber set point" \
	-d on -r on -p off -s on -units K \
	-readProc L372_ctrlset_r -writeProc L372_ctrlset_w

proc L372_ctrlset_r { ins } {
	set cs [varGetVal /$ins/control_set]
	insIfReadVerify /$ins "SETP? 0" 48 /$ins/control_set " %f" 2
	if { $cs != [varGetVal /$ins/control_set] } {
		L372_autoboost $ins
	}
	L372_autostill $ins
}

# Enforce a 0.1K buffer against hitting temperature limit with setpoint.
# At present, input and output channels are fixed, as above
#	set ch [lindex {A 1 2 3 4 5 6} [varGetVal /$ins/outputs/heater/input_chan]]
proc L372_ctrlset_w { ins val } {
	set prev [varGetVal /$ins/control_set]
	set lim 0.0
	set ch "A"
	if { [varGetVal /$ins/inputs/mix_cham/units] == 1 } {
		varRead /$ins/inputs/mix_cham/temp_limit
		set lim [varGetVal /$ins/inputs/mix_cham/temp_limit]
		if { $lim > 0.0 && $val > $lim-0.09 } {
			return -code error "Setpoint too high for T limit of $lim"
		}
	}
	set err [expr {0.001+abs($val)/500.0}]
	insIfWRT /$ins "SETP 0,$val"
	varDoSet /$ins/control_set -v $val
	L372_autoboost $ins
	L372_autostill $ins
	if { [varGetVal /$ins/outputs/ramp/ramping] } {
		varDoSet /$ins/ramp_status -p on -p_int 8
	}
}

CAMP_SELECT /~/heat_range -D -R -S -L -P -T "Control heater range" \
	-d on -r on -s on -p off \
	-selections "OFF" "500nW, 32uA" "5.0uW, 0.1 mA" "50uW,  0.3mA" "0.5mW, 1mA" \
		"5.0mW, 3mA" "50mW,  10mA" "0.2W,  20mA" \
	-H "Mixing chamber heater output range." \
	-readProc L372_heat_range_r -writeProc L372_heat_range_w

CAMP_FLOAT /~/heat_output -D -R -L -P -T "Heater output" \
	-d on -r on -p off -units mA \
	-H "Mixing chamber heater output (current or power)" \
	-readProc L372_heatout_r

proc L372_heat_range_r { ins } {
	global CAMP_VAR_ATTR_POLL
	set h 0
	scan [insIfRRT /$ins "RANGE? 0" 48] " %d" h
	varDoSet /$ins/heat_range -v $h
	if { ([varGetStatus /$ins/heat_range] & $CAMP_VAR_ATTR_POLL) && [varGetPollInterval /$ins/heat_range] == 15.5 } {
		varDoSet /$ins/heat_range -p on -p_int 25
	}
}

#  Heater-on is not immediate, so check it after 15.5 seconds (but issue no error).
#  Active heat range boost is switched off by heat range setting, so autoboost must set heat 
#  range before setting boost variable.
proc L372_heat_range_w { ins target } {
	global CAMP_VAR_ATTR_POLL
	set hr $target
	insIfWRT /$ins "RANGE 0,$hr"
	varDoSet /$ins/heat_range -v $target
	varRead /$ins/heat_range
	if { !([varGetStatus /$ins/heat_range] & $CAMP_VAR_ATTR_POLL) } {
		varDoSet /$ins/heat_range -p on -p_int 15.5
	}
	if { [varGetVal /$ins/outputs/heater/boost] == 2 } {
		varDoSet /$ins/outputs/heater/boost -p off -v 1
	}
}

# Main Heat ranges 0.0316 0.1 0.316 1 3.16 10 31.6 100 mA. Max_current not configurable.
# Try to avoid reading the display type every time.
proc L372_heatout_r { ins } {
	set mes ""
	set d [varGetVal /$ins/outputs/heater/display]
	set buf [insIfRRT /$ins "HTR?" 48]
	set val 0.0
	scan $buf " %g" val
	if { $d == 2 } {
		set u "mW"
		set ho [expr {$val*1000.0}]
	} else {
		set u "mA"
		varRead /$ins/heat_range
		set hr [varGetVal /$ins/heat_range]
		set max_curr [lindex {0.0 0.031623 0.1 0.31623 1.0 3.1623 10 31.623 100 100} $hr]
		set R [varGetVal /$ins/outputs/heater/resistance]
		set pow [expr {$R*pow(10,($hr-10))*$val*$val/10000.0}]
		foreach um {W mW uW nW pW} {
			if { $pow > 0.505 } { break }
			set pow [expr {$pow*1000.0}]
		}
		set mes "[varGetHelp /$ins/heat_output] Corresponding power is $pow $um."
		set ho [expr {$max_curr*$val/100.0}]
	}
	varDoSet /$ins/heat_output -v $ho -m $mes -units $u
}

CAMP_FLOAT /~/still_output -D -R -P -L -T "Still output" \
	-d on -r on -units "%" \
	-H "Measured still heater output given as a percentage" \
	-readProc L372_stillreadout_r

# We do not implement manual output modes for heaters. Just use the configurable
# max_current for the warm-up and still heaters.

CAMP_SELECT /~/ramp_status -D -R -P -T "Ramp status" \
	-d on -r on -p off -p_int 5 -selections "done" "ramping " \
	-readProc L372_ramp_status_r

#########################################################################################

CAMP_STRUCT /~/alarms -D -d on -T "Alarms and Relays"

foreach _ch {0 1 2 3 4 5 6} {
	set _vch [lindex {mix_cham sample M2 M3 M4 M5 M6} $_ch]
	if { $_ch == 0 } { set _ch "A" }

	CAMP_SELECT /~/alarms/alarm_$_vch -D -R -P -L -A -T "$_vch Alarm state" \
		-d on -r on -p off -p_int 8 -a off \
		-H "Alarm state for $_vch (input $_ch)" \
		-selections OK Low_Trip Hi_Trip Both_Trip \
		-v 0 -readProc [list L372_alarmst_r $_vch $_ch]
}

proc L372_alarmst_r { vch ch ins } {
	set buf [insIfRRT /$ins "ALARMST? $ch" 16]
	if { [scan $buf " %d, %d" hi lo] != 2 } {
		return -code error "invalid status $buf"
	}
	varDoSet /$ins/alarms/alarm_$vch -v [expr {$lo + 2*$hi}]
	varTestAlert /$ins/alarms/alarm_$vch 0
}

CAMP_SELECT /~/alarms/reset -D -S -T "Reset alarms" \
	-d on -s on -selections "" RESET -v 0 \
	-writeProc L372_alarmreset_w

proc L372_alarmreset_w { ins target } {
	if { $target } {
		insIfWRT /$ins "ALMRST"
	}
}

foreach _ch {0 1 2 3 4 5 6} {
	set _vch [lindex {mix_cham sample M2 M3 M4 M5 M6} $_ch]
	if { $_ch == 0 } { set _ch "A" }

	CAMP_STRUCT /~/alarms/cfg_$_vch -D -d on -T "Configure $_vch Alarm"

	CAMP_SELECT /~/alarms/cfg_$_vch/enable -D -S -R -T "Enable $_vch Alarm" \
		-d on -s on -r on -selections Disable Enable \
		-readProc [list L372_almcfg_r $_vch $_ch] -writeProc [list L372_almcfg_w $_vch $_ch enable]
	CAMP_FLOAT /~/alarms/cfg_$_vch/low_limit -D -S -R -T "$_vch low limit" -d on -s on -r on \
		-readProc [list L372_almcfg_r $_vch $_ch] -writeProc [list L372_almcfg_w $_vch $_ch low_limit]
	CAMP_FLOAT /~/alarms/cfg_$_vch/high_limit -D -S -R -T "$_vch high limit" -d on -s on -r on \
		-readProc [list L372_almcfg_r $_vch $_ch] -writeProc [list L372_almcfg_w $_vch $_ch high_limit]
	CAMP_FLOAT /~/alarms/cfg_$_vch/deadband -D -S -R -T "$_vch deadband" -d on -s on -r on \
		-readProc [list L372_almcfg_r $_vch $_ch] -writeProc [list L372_almcfg_w $_vch $_ch deadband]
	CAMP_SELECT /~/alarms/cfg_$_vch/latching -D -S -R -T "$_vch alarm latching" \
		-d on -s on -r on -selections off on \
		-readProc [list L372_almcfg_r $_vch $_ch] -writeProc [list L372_almcfg_w $_vch $_ch latching]

}

proc L372_almcfg_r { vch ch ins } {
	set buf [insIfRRT /$ins "ALARM? $ch" 80]
	if { [scan $buf " %d,%d,%f,%f,%f,%d,%d,%d" enable unit high_limit low_limit deadband latching audible visible] != 8 } {
		return -code error "invalid alarms config $buf"
	}
	set vp "/$ins/alarms/cfg_$vch"
	varDoSet $vp/enable -v $enable
	varDoSet $vp/low_limit -v $low_limit
	varDoSet $vp/high_limit -v $high_limit
	varDoSet $vp/deadband -v $deadband
	varDoSet $vp/latching -v $latching
}

proc L372_almcfg_w { vch ch var ins val } {
	set buf [insIfRRT /$ins "ALARM? $ch" 80]
	if { [scan $buf " %d,%d,%g,%g,%g,%d,%d,%d" enable unit high_limit low_limit deadband latching audible visible] != 8 } {
		return -code error "invalid alarms config $buf"
	}
	set $var $val
	insIfWRT /$ins "ALARM $ch,$enable,$unit,$high_limit,$low_limit,$deadband,$latching,$audible,$visible"
	varRead /$ins/alarms/cfg_$vch/$var
}

CAMP_SELECT /~/alarms/relay_1_set -D -R -S -T "Relay 1 setting" \
	-d on -s on -r on -selections Off On Low_Alarm High_Alarm Any_Alarm MC_zone WU_zone \
	-readProc {L372_relayact_r 1} -writeProc {L372_relayact_w 1}

CAMP_SELECT /~/alarms/relay_2_set -D -R -S -T "Relay 2 setting" \
	-d on -s on -r on -selections Off On Low_Alarm High_Alarm Any_Alarm MC_zone WU_zone \
	-readProc {L372_relayact_r 2} -writeProc {L372_relayact_w 2}

proc L372_relayact_r { id ins } {
	set buf [insIfRRT /$ins "RELAY? $id" 48]
	if { [scan $buf { %d,%[A0-9],%d} mode chan type] != 3 } {
		return -code error "invalid relay config $buf"
	}
	set mode [expr {$mode + ($mode>3? 2 : ($mode==3? $type :0))}]
	varDoSet /$ins/alarms/relay_${id}_set -v $mode
}

proc L372_relayact_w { id ins target } {
	set buf [insIfRRT /$ins "RELAY? $id" 48]
	if { [scan $buf { %d,%[A0-9],%d} mode chan type] != 3 } {
		return -code error "invalid relay config $buf"
	}
	if { $target >= 5 } {# zone
		set mode [expr {$target-2}]
		set type 0
	} elseif { $target >= 2 } {# alarm
		set type [expr {$target-2}]
		set mode 2
	} else {# manual
		set mode $target
		set type 0
	}
	insIfWRT /$ins "RELAY $id,$mode,$chan,$type"
	varDoSet /$ins/alarms/relay_${id}_set -v $target
}

#############################################################################################
# The input channel is not configurable for the main heater: Input A is used.
# Warm-up heater sensor may be set, but if "A" will disable regular heater control.

CAMP_STRUCT /~/outputs -D -d on -T "Configure the outputs"

CAMP_STRUCT /~/outputs/heater -D -d on -T "Configure the heater output"

CAMP_STRUCT /~/outputs/warm-up -D -d on -T "Configure the warm-up heater"

CAMP_STRUCT /~/outputs/still -D -d on -T "Configure the still heater"

CAMP_SELECT /~/outputs/heater/mode -D -R -S -T "Control mode" \
	-d on -r on -s on -selections "Off" "PID" "Zone PID" \
	-readProc [list L372_control_mode_r heater 0] -writeProc [list L372_control_mode_w heater 0]

CAMP_SELECT /~/outputs/warm-up/mode -D -R -S -T "Control mode" \
	-d on -r on -s on -selections "Off" "PID" "Zone PID" "Warm-up" \
	-readProc [list L372_control_mode_r warm-up 1] -writeProc [list L372_control_mode_w warm-up 1]

CAMP_SELECT /~/outputs/still/mode -D -R -S -T "Operation on off" \
	-d on -r on -s on -selections "Off" "On" \
	-H "Still heater operation state on/off. May be controlled automatically when threshold is crossed." \
	-readProc [list L372_control_mode_r still 2] -writeProc [list L372_control_mode_w still 2]

CAMP_FLOAT /~/outputs/still/threshold -D -S -T "Threshold temperature" \
	-d on -s on -v 0.5 -units K \
	-H "Temperature setpoint threshold below which still is turned on. Set to 0 for manual control." \
	-writeProc L372_threshold_w

CAMP_SELECT /~/outputs/heater/input_chan -D -R -S -T "Input channel" \
	-d on -r on -s on -selections none "mix_cham A" "sample M1" M2 M3 M4 M5 M6 \
	-v "mix_cham A" -H "Input channel for controlling mixing chamber heater" \
	-readProc "L372_input_chan_r heater 0" -writeProc "L372_input_chan_w heater 0"

CAMP_SELECT /~/outputs/warm-up/input_chan -D -R -S -T "Input channel" \
	-d on -r on -s on -selections none "mix_cham A" "sample M1" M2 M3 M4 M5 M6 \
	-v "none" -H "Input channel for controlling warm-up heater output" \
	-readProc "L372_input_chan_r warm-up 1" -writeProc "L372_input_chan_w warm-up 1"

CAMP_SELECT /~/outputs/heater/display -D -S -R -T "Heater output display" \
	-d on -s on -r on -H "Heater output is displayed as current or power" \
	-selections "" "Current" "Power" \
	-readProc "L372_heat_r heater 0" -writeProc "L372_heat_w heater 0 display 1 2"

CAMP_SELECT /~/outputs/heater/boost -D -S -P -T "Heat range boost" \
	-d on -s on -H "Boost heat range when far below set-point" \
	-selections "Disabled" "Enabled" "Active" -v 1 \
	-readProc L372_autoboost -writeProc L372_boost_w

CAMP_FLOAT /~/outputs/heater/boost_offset -D -S -T "T difference for boost" \
	-d on -s on -v 0.3 -units "K" \
	-H "Heat range boost happens when temperature is at least this far below set-point" \
	-writeProc L372_boostoffset_w

CAMP_FLOAT /~/outputs/heater/resistance -D -S -R -L -T "Heater resistance" \
	-d on -s off -r on -v 500.0 -units "Ohm" -H "Heater resistance (1-2000 ohm)" \
	-readProc "L372_heat_r heater 0" -writeProc "L372_heat_w heater 0 resistance 1 2000"

CAMP_SELECT /~/outputs/warm-up/resistance -D -S -R -L -T "Warm-up heater resistance" \
	-d on -s on -r on -selections "" "25 Ohm" "50 Ohm" \
	-H "Nominal warm-up heater resistance" \
	-readProc "L372_heat_r warm-up 1" -writeProc "L372_heat_w warm-up 1 resistance 1 2"

CAMP_FLOAT /~/outputs/warm-up/max_current -D -S -R -L -T "Maximum Current" \
	-d on -s on -r on -units mA \
	-H "Maximum output current, in mA." \
	-readProc "L372_heat_r warm-up 1" -writeProc "L372_heat_w warm-up 1 max_current 0 10000"
# ??? What are valid limits to max warm-up current???

CAMP_FLOAT /~/outputs/still/set_output -D -S -R -L -T "Set output" \
	-d on -s on -r on -units "%" \
	-H "Set still heater output given as a percentage" \
	-readProc L372_stillsetout_r -writeProc L372_stillsetout_w

CAMP_FLOAT /~/outputs/still/output -D -R -P -L -T "Percentage output" \
	-d on -r on -units "%" \
	-H "Measured still heater output given as a percentage" \
	-readProc L372_stillreadout_r

CAMP_FLOAT /~/outputs/warm-up/output -D -R -P -L -T "Percentage output" \
	-d on -r on -units "%" \
	-H "Measured warm-up heater output given as a percentage" \
	-readProc L372_warmupout_r

foreach _io {0 1} {
	set _vch [lindex {heater warm-up still} $_io]

	CAMP_FLOAT /~/outputs/$_vch/P -D -R -S -T "Gain setting (P)" \
		-d on -r on -s on \
		-H "Proportional gain parameter; use high values when response is slow" \
		-readProc [list L372_PID_r $_vch $_io] -writeProc [list L372_PID_w $_vch $_io p]

	CAMP_FLOAT /~/outputs/$_vch/I -D -R -S -T "Reset setting (I)" \
		-d on -r on -s on \
		-H "Integral reset parameter, relative to P (I = Ki/P = 1000sec/time_constant); use low values when response is slow" \
		-readProc [list L372_PID_r $_vch $_io] -writeProc [list L372_PID_w $_vch $_io i]

	CAMP_FLOAT /~/outputs/$_vch/D -D -R -S -T "Rate setting (D)" \
		-d on -r on -s on \
		-H "Derivative rate parameter, relative to P (D=Kd/P)" \
		-readProc [list L372_PID_r $_vch $_io] -writeProc [list L372_PID_w $_vch $_io d]

#####################################################################################

	CAMP_STRUCT /~/outputs/$_vch/zone -D -d on -T "PID Zone configuration"

	CAMP_SELECT /~/outputs/$_vch/zone/apply_zones -D -S -T "Apply zones" -d on -s on \
		-H "Check and apply all zone settings" -selections APPLY \
		-writeProc "L372_apply_zones_w $_vch $_io"

	for { set _i 1 } { $_i <= 10 } { incr _i } {
		CAMP_STRING /~/outputs/$_vch/zone/zone$_i -D -S -R -T "Zone $_i info" \
			-d on -s on -r on -v "" \
			-H "List of zone $_i parameters: Top Temp, Heat range, P, I, D, Manual out%, Ramp rate, relay1, relay2" \
			-readProc [list L372_zone_r $_vch $_io $_i] -writeProc [list L372_zone_w $_vch $_io $_i]
	}
	unset _i

}
# end of _io loop over outputs 0 & 1


#####################################################################################
# Model 372 uses restricted control settings.
# Input A controls output 0 ("sample heater") which, for us, heats the Mixing Chamber (not "sample")
# Any input (A,1-16) can control output 1, but not the same input as used for the heater.
# Output 2 can only be controlled manually, and is used for the still heater.

proc L372_input_chan_r { vch io ins } {
	set buf [insIfRRT /$ins "OUTMODE? $io" 48]
	if { [scan $buf { %d,%[A0-9],} mode inch] != 2 || [set i [string first $inch "0A123456"]] < 0 } {
		return -code error $buf
	}
	varDoSet /$ins/outputs/$vch/input_chan -v $i
#	if { $io == 0 && ![string match {[A0]} $inch] } {
#		return -code error "Input channel A should always be selected for control (not '$inch')"
#	}
}

proc L372_input_chan_w { vch io ins target } {
	set buf [insIfRRT /$ins "OUTMODE? $io" 48]
	if { [scan $buf { %d,%[A0-9],%d,%d,%d,%d} mode inch pup pol f d] != 6 } {
		return -code error $buf
	}
	set inch [string range "0A123456" $target $target]
	insIfWRT /$ins "OUTMODE $io,$mode,$inch,$pup,0,$f,$d"
	varDoSet /$ins/outputs/$vch/input_chan -v $target
	if { $target == 0 && [varGetVal /$ins/outputs/$vch/mode] > 0 } {
		varSet /$ins/outputs/$vch/mode -v 0
	}
	if { ($io == 0 && $target > 1) || ($io > 0 && $target == 1) } {
		if { $io > 0 } { varRead /$ins/outputs/heater/input_chan }
		return -code error "WARNING: Bad input channel for control of [lindex {heater warm-up still} $io]. Check other outputs."
	}
}

proc L372_control_mode_r { vch io ins } {
	set buf [insIfRRT /$ins "OUTMODE? $io" 64]
	set mode 1
	set m -1
	scan $buf { %d,%[A0-9],} mode inch
	set m [lindex [lindex {{0 -1 -1 2 -1 1 -1} {0 -1 -1 2 -1 1 3} {0 -1 -1 -1 1 -1 -1}} $io] $mode]
	if { $io > 0 && $m > 0 } {# If range for iotputs 1&2 is "off" -> "disabled"
		set r 0
		scan [insIfRRT /$ins "RANGE? $io" 32] { %d} r
		if { $r == 0 } { set m 0 }
	}
	if { $m < 0 } {
		return -code error "Unknown or unsupported mode index $mode"
	}
	varDoSet /$ins/outputs/$vch/mode -v $m
}

proc L372_control_mode_w { vch io ins target } {
	set buf [insIfRRT /$ins "OUTMODE? $io" 64]
	if { [scan $buf { %d,%[A0-9],%d,%d,%d,%d} mode inch pup pol f d] != 6 } {
		return -code error "Improper control mode $buf"
	}
#	if { $target > 0 && $inch == "0" } { set inch "A" }
	set m [lindex [lindex {{0 5 3} {0 5 3 6} {0 4}} $io] $target]
	insIfWRT /$ins "OUTMODE $io,$m,$inch,$pup,0,$f,$d"
	varDoSet /$ins/outputs/$vch/mode -v $target
	if { $io == 2 } { varDoSet /$ins/still_output -p on -p_int 2 }
	if { $io > 0 && $target > 0 } { insIfWRT /$ins "RANGE $io,1" }
}

proc L372_boost_w { ins val } {
	set B [varGetVal /$ins/outputs/heater/boost]
	if { $val == 0 && $B == 2 } {
		L372_endboost $ins
	}
	if { $val > 0 } {# enabled: retain active/passive state
		set val [expr { $B==2 ? 2 : 1 }]
	}
	varDoSet /$ins/outputs/heater/boost -v $val
	if { $val > 0 } { L372_autoboost $ins }
}

proc L372_boostoffset_w { ins val } {
	varDoSet /$ins/outputs/heater/boost_offset -v [expr {abs($val)}]
}

proc L372_autoboost { ins } {
	set B [varGetVal /$ins/outputs/heater/boost]
	if { $B == 0 || \
		 [varGetVal /$ins/outputs/heater/input_chan] != 1 || \
		 [varGetVal /$ins/inputs/mix_cham/units] != 1} {
		return
	}
	set T  [varGetVal /$ins/read_mix_cham]
	set To [varGetVal /$ins/outputs/heater/boost_offset]
	set Ts [varGetVal /$ins/control_set]
	set HR [varGetVal /$ins/heat_range]
	if { $B == 1 && $T < $Ts-$To && $HR > 0 } {
		if { [varGetVal /$ins/outputs/heater/mode] == 2 } {# Zone PID - don't believe $HR
			for {set z 1} {$z <= 10} {incr z} {
				scan [varGetVal /$ins/outputs/heater/zone/zone$z] {%f %d} Tz HR
				if {$Tz >= $Ts } { break }
			}
		}
		if { $HR < 7 && $HR > 2 } {
			# incr heat range THEN(!) set boost "active" and polling
			varSet /$ins/heat_range -v [expr {$HR+1}]
			varDoSet /$ins/outputs/heater/boost -p on -p_int 10 -v 2 \
				-m "Change HR from $HR to [expr $HR+1] when T=$T and set=$Ts"
		}
	} elseif { $B == 2 && $T > $Ts-$To } {
		L372_endboost $ins
	}
}

proc L372_endboost { ins } {
	set B [varGetVal /$ins/outputs/heater/boost]
	varRead /$ins/heat_range
	set HR [varGetVal /$ins/heat_range]
	if { $B == 2 && $HR > 2 } {
		varSet /$ins/heat_range -v [expr {$HR-1}]
	}
	varDoSet /$ins/outputs/heater/boost -m "" -p off -v [expr {$B==0 ? 0 : 1}]
}

proc L372_threshold_w { ins target } {
	varDoSet /$ins/outputs/still/threshold -v $target
	if { $target > 0.0 } {
		catch { varRead /$ins/control_set ; varRead /$ins/read_mix_cham }
	}
	L372_autostill $ins
}

# control still heater based on maximum of setpoint & readback temperature.
# Only switch still on or off when T crosses the threshold, so manual 
# settings persist awhile.  If setpoint is close to threshold, use a bigger
# hysteresis on the readback.
proc L372_autostill { ins } {
	varDoSet /$ins/outputs/still/threshold -m ""
	if { [varNumGetUnits /$ins/control_set] != "K" } { return }
	upvar \#0 ${ins}_cache cache
	set Tt [varGetVal /$ins/outputs/still/threshold]
	set sm [varGetVal /$ins/outputs/still/mode]
	set Ts [expr { ($sm?0.995:1.005) * [varGetVal /$ins/control_set] }]
  	set Tr [expr { ($sm?0.995:1.005) * [varGetVal /$ins/read_mix_cham] }]
	if { $Tr <= 0.0 || $Ts < 0.0 || $Tt <= 0.0 } {
		set cache(TdiffStill) {}
		return
	}
	if { abs($Ts-$Tt) < 0.01*$Tt } { set Tr [expr { ($sm?0.95:1.05) * $Tr }] }
	set Tdiff [expr { ($Tr>$Ts ? $Tr : $Ts) - $Tt }]
	if { [string length $cache(TdiffStill)] == 0 } {
		set cache(TdiffStill) [expr {-$Tdiff}]
	}
	varDoSet /$ins/outputs/still/threshold -m "AS: m $sm r $Tr s $Ts t $Tt d $Tdiff c $cache(TdiffStill)"
	if { $Tdiff > 0.0 && $cache(TdiffStill) <= 0.0 } {
		varSet /$ins/outputs/still/mode -v 0
	} elseif { $Tdiff < 0.0 && $cache(TdiffStill) >= 0.0 } {
		varSet /$ins/outputs/still/mode -v 1
	}
	set cache(TdiffStill) $Tdiff
}

proc L372_PID_r { vch io ins } {
	set buf [insIfRRT /$ins "PID? $io" 48]
	if { [scan $buf { %f,%f,%f} p i d ] < 3 } {
		return -code error "Bad PID Readback \"$buf\""
	}
	varDoSet /$ins/outputs/$vch/P -v $p
	varDoSet /$ins/outputs/$vch/I -v $i
	varDoSet /$ins/outputs/$vch/D -v $d
}

# $which should be p, i, or d, matching the variable names
proc L372_PID_w { vch io which ins target } {
	if { $target < 0 || $target > 999 } {
		return -code error "invalid value \"$target\""
	}
	set buf [insIfRRT /$ins "PID? $io" 48]
	if { [scan $buf { %f,%f,%f} p i d ] < 3 } {
		return -code error "Bad PID Readback \"$buf\""
	}
	set $which $target
	insIfWRT /$ins [format {PID %s,%.3f,%.1f,%.0f} $io $p $i $d]
	L372_PID_r $vch $io $ins
}

# resistance is float for heater but integer code for warm-up
proc L372_heat_w { vch io which min max ins target } {
	if { $target < $min || $target > $max } {
		return -code error "$which value out of range ($min to $max) for $vch"
	}
	set fmt [lindex {%f %d %d} $io]
	set buf [insIfRRT /$ins "HTRSET? $io" 64]
	if { [scan $buf " $fmt,%d,%f,%d" resistance mc max_current display] != 4 } {
		return -code error "Invalid heater readback: $buf"
	}
	set $which $target
	if { $which == "max_current" } {
		set mc [expr {abs($target-0.45)<0.11? 1 : (abs($target-0.63)<0.11? 2 : 0)}]
	}
	insIfWRT /$ins [format "HTRSET %d,$fmt,%d,%f,%d" $io $resistance $mc $max_current $display]
	varDoSet /$ins/outputs/$vch/$which -v $target
}

proc L372_heat_r { vch io ins } {
	set buf [insIfRRT /$ins "HTRSET? $io" 64]
	if { [scan $buf { %f,%d,%f,%d} resist mc muc disp] != 4 } {
		return -code error "Invalid heater readback: $buf"
	}
	if { $io == 0 } {
		varDoSet /$ins/outputs/$vch/display -v $disp
	} elseif { $io == 1 } {
		set resist [expr {round($resist)}]
		varDoSet /$ins/outputs/$vch/max_current -v [lindex [list $muc 0.45 0.63] $mc]
	}
	varDoSet /$ins/outputs/$vch/resistance -v $resist
}

proc L372_stillsetout_r { ins } {
	set s 0.0
	scan [insIfRRT /$ins "STILL?" 32] " %f" s
	varDoSet /$ins/outputs/still/set_output -v $s
}

proc L372_stillsetout_w { ins pct } {
	if { $pct < 0.0 || $pct > 50.0 } {
		return -code error "Percentage out of range"
	}
	set pct [format "%.2f" $pct]
	insIfWRT /$ins "STILL $pct"
	varDoSet /$ins/outputs/still/set_output -v $pct
	varDoSet /$ins/still_output -p on -p_int 2
}

proc L372_stillreadout_r { ins } {
	set s 0.0
	scan [insIfRRT /$ins "AOUT? 2" 32] " %f" s
	varDoSet /$ins/outputs/still/output -v $s
	varDoSet /$ins/still_output -v $s
	if { ($s>0) != ([varGetVal /$ins/outputs/still/mode]>0) } {
		varRead /$ins/outputs/still/mode
	}
	if { [varGetPollInterval /$ins/still_output] == 2.0 } {
		varDoSet /$ins/still_output -p on -p_int 30
	}
}

proc L372_warmupout_r { ins } {
	set s 0.0
	scan [insIfRRT /$ins "AOUT? 1" 32] " %f" s
	varDoSet /$ins/outputs/warm-up/output -v $s
}

# Top Temp, Heat range, P, I, D, Manual out%, ramp rate, relay1, relay2

proc L372_apply_zones_w { vch io ins target } {
	set max 0.0
	set adj 0
	for { set i 1 } { $i <= 10 } { incr i } {
		set z [varGetVal /$ins/outputs/$vch/zone/zone$i]
		# If this zone is blank, copy from previous.
		if { [llength $z] == 0 && $i > 1 } {
			set z [varGetVal /$ins/outputs/$vch/zone/zone[expr {$i-1}]]
		}
		while { [llength $z] < 9 } { lappend z 0 }
		set T [lindex $z 0]
		if { $T < $max && $T > 0.0 } {
			if { $max < 300.0 } {
				return -code error "Invalid sequence of temperatures: must increase from zone1 to zone10"
			}
			set T [string trim [format %6.3f $max]]
			set z [lreplace $z 0 0 $T]
			varDoSet /$ins/outputs/$vch/zone/zone$i -v $z
		}
		set max $T
		insIfWRT /$ins "ZONE $io,$i,$T,[join [lrange $z 2 5] {,}],[lindex $z 1],[join [lrange $z 6 8] {,}]"
	}
}

proc L372_zone_w { vch io i ins target } {
	set z [join [split $target ,] " "]
	while { [llength $z] < 6 } { lappend z 0 }
	varDoSet /$ins/outputs/$vch/zone/zone$i -v $z
}

proc L372_zone_r { vch io i ins } {
	set r1 0; set r2 0; set R 0.0
	set z [insIfRRT /$ins "ZONE? $io,$i" 90]
	if { [scan $z { %f,%f,%f,%f,%f,%d,%f,%d,%d} T P I D M H R r1 r2] >= 6 } {
		foreach v {T P I D M R} { # reduce round numbers to integers
			set vv [set $v]
			set $v [expr {($vv==round($vv)?round($vv):$vv)}]
		}
		if { $r1 == 0 && $r2 == 0 } { set r1 "" ; set r2 "" }
		varDoSet /$ins/outputs/$vch/zone/zone$i -v "$T $H $P $I $D $M $R $r1 $r2"
	} else {
		return -code error "Invalid zone readback '$z'"
	}
}

#####################################################################################

CAMP_STRUCT /~/outputs/ramp -D -d on -T "Configure ramping"

CAMP_SELECT /~/outputs/ramp/ramping -D -S -R -P -T "Ramp enable" \
	-d on -s on -r on -selections DISABLED ENABLED \
	-H "Enable or disable slow ramping of setpoint" \
	-readProc L372_ramp_r -writeProc L372_ramp_w

CAMP_SELECT /~/outputs/ramp/ramp_status -D -R -T "Ramp status" \
	-d on -r on -selections DONE RAMPING \
	-readProc L372_ramp_status_r

CAMP_FLOAT /~/outputs/ramp/ramp_rate -D -S -R -L -P -T "Ramp rate" \
	-d on -s on -r on -p off -units {K/min} \
	-H "Temperature (setpoint) ramp rate (if ramp is enabled) in Kelvin per minute (from 0.001 to 99.99, or 0 for off)" \
	-readProc L372_ramp_r -writeProc L372_ramp_rate_w

proc L372_ramp_r { ins } {
	set buf [insIfRRT /$ins "RAMP? 0" 48]
	if { [scan $buf " %d, %f" en rate] < 2 } {
		return -code error "Invalid Ramp reading $buf"
	}
	varDoSet /$ins/outputs/ramp/ramping -v $en
	varDoSet /$ins/outputs/ramp/ramp_rate -v $rate
}

proc L372_ramp_w { ins target } {
	set en 0
	set rate 0.0
	scan [insIfRRT /$ins "RAMP? 0" 48] " %d, %f" en rate
	if { $target == 1 && $rate < 0.001 } { set rate 0.1 }
	set rate [format "%.3f" $rate]
	insIfWRT /$ins "RAMP 0,$target,$rate"
	varDoSet /$ins/outputs/ramp/ramping -v $target
	varDoSet /$ins/outputs/ramp/ramp_rate -v $rate
}

proc L372_ramp_status_r { ins } {
	set rs -1
	scan [insIfRRT /$ins "RAMPST? 0" 32] " %d" rs
	if { $rs == -1 } {
		return -code error "Failed reading ramp status"
	} elseif { $rs == 0 } {
		varDoSet /$ins/ramp_status -v $rs -p off
	} else {
		varDoSet /$ins/ramp_status -v $rs -p on -p_int 7
		varRead /$ins/control_set
	}
	varDoSet /$ins/outputs/ramp/ramp_status -v $rs
}

proc L372_ramp_rate_w { ins target } {
	if { $target < 0.0 || $target > 99.99 } {
		return -code error "Invalid ramp rate. Must be 0 to 99.99 K/min"
	}
	set rate [format "%.3f" $target]
	set en [expr {$target > 0.0}]
	insIfWRT /$ins "RAMP 0,$en,$rate"
	varDoSet /$ins/outputs/ramp/ramping -v $en
	varDoSet /$ins/outputs/ramp/ramp_rate -v $rate
}

########################################################################################

CAMP_STRUCT /~/inputs -D -d on -T "Configure inputs"

foreach _ch {0 1 2 3 4 5 6} {
	set _vch [lindex {mix_cham sample M2 M3 M4 M5 M6} $_ch]
	if { $_ch == 0 } { set _ch "A" }

	CAMP_STRUCT /~/inputs/$_vch -D -d on -T "Configure input $_ch"

	CAMP_SELECT /~/inputs/$_vch/enable -D -R -S -T "Input $_ch enable" \
		-d on -r on -s on -v 0 -selections Disable Enable Shunted \
		-readProc "L372_inset_r $_vch $_ch" -writeProc "L372_inen_w $_vch $_ch"

	CAMP_STRING /~/inputs/$_vch/title -D -R -S -T "Channel $_ch title" \
		-d on -r on -s on \
		-readProc "L372_title_r $_vch $_ch" -writeProc "L372_title_w $_vch $_ch"

	CAMP_SELECT /~/inputs/$_vch/units -D -R -S -T "$_vch units" \
		-d on -r on -s on -v 0 -selections "" K Ohm \
		-readProc "L372_intyp_r $_vch $_ch" -writeProc "L372_intyp_w un 0 2 $_vch $_ch"

	CAMP_STRING /~/inputs/$_vch/curve -D -R -P -S -T "Input $_ch curve" \
		-d on -r on -s on -p off -p_int 0.1 \
		-H "Load or read the $_vch (input $_ch) calibration curve (number or name)" \
		-readProc "L372_curve_r $_vch $_ch" -writeProc "L372_set_curve $_vch $_ch"

	CAMP_SELECT /~/inputs/$_vch/autorange -D -R -S -T "$_vch autorange" \
		-d on -r on -s on -selections OFF ON ROX102B \
		-readProc "L372_intyp_r $_vch $_ch" -writeProc "L372_intyp_w ar 0 1 $_vch $_ch"

	if { "$_ch" == "A" } {

		CAMP_SELECT /~/inputs/$_vch/range -D -R -S -T "$_vch input range" \
			-d on -r on -s on \
			-selections "" "316pA 632kOhm" "1nA 200kOhm" "3.16nA 63kOhm" "10nA 20kOhm" "31.6nA 6.32kOhm" "100nA 2kOhm" \
			-readProc "L372_intyp_r $_vch $_ch" -writeProc "L372_intyp_w er 0 6 $_vch $_ch"

		CAMP_FLOAT /~/inputs/$_vch/temp_limit -D -S -R -T "Temperature limit" \
			-d on -s off -r on -units {K} \
			-H "Temperature limit on setpoint and heaters output" \
			-readProc "L372_tlimit_r $_vch $_ch" -writeProc "L372_tlimit_w $_vch $_ch"

	} else {

		CAMP_INT /~/inputs/$_vch/R-range -D -R -S -L -T "$_vch resistance range code" \
			-d on -r on -s on -H "Automatic resistance range control" \
			-readProc "L372_intyp_r $_vch $_ch" -writeProc "L372_intyp_w rr 1 22 $_vch $_ch"

		CAMP_INT /~/inputs/$_vch/excitation -D -R -S -L -T "$_vch excitation code" \
			-d on -r on -s on \
			-readProc "L372_intyp_r $_vch $_ch" -writeProc "L372_intyp_w er 1 22 $_vch $_ch"

		CAMP_SELECT /~/inputs/$_vch/ex_mode -D -R -S -T "$_vch excitation mode" \
			-d on -r on -s on -selections "Voltage" "Current" -v 1 \
			-H "Sensor excitation mode" \
			-readProc "L372_intyp_r $_vch $_ch" -writeProc "L372_intyp_w em 0 1 $_vch $_ch"

		CAMP_SELECT /~/inputs/$_vch/autoexcite -D -R -P -S -T "$_vch auto-excitation" \
			-d on -s on -r on -p on -p_int 80 -selections "Disable" "Enable" -v 1 \
			-H "Automatically adjust sensor excitation according to temperature and resistance" \
			-readProc "L372_autoexcite_r $_vch $_ch" -writeProc "L372_autoexcite_w $_vch $_ch"

		CAMP_FLOAT /~/inputs/$_vch/rdg_power -D -R -P -L -T "$_vch sensor power" \
			-d on -r on -p off -p_int 30 -units "nW" \
			-H "$_vch sensor power dissipation, in nW, caused by excitation." \
			-readProc "L372_rdgpower_r $_vch $_ch"

		CAMP_INT /~/inputs/$_vch/scan_dwell -D -R -S -T "Scan dwell time" \
			-d on -r on -s on -units s \
			-readProc "L372_inset_r $_vch $_ch" -writeProc "L372_inset_w dw 0 200 $_vch $_ch"

		CAMP_INT /~/inputs/$_vch/scan_pause -D -R -S -T "Scan pause time" \
			-d on -r on -s on -units s \
			-readProc "L372_inset_r $_vch $_ch" -writeProc "L372_inset_w pa 3 200 $_vch $_ch"
	}

	CAMP_SELECT /~/inputs/$_vch/filter -D -R -S -T "$_vch filtering" \
		-d on -r on -s on -selections OFF ON \
		-readProc "L372_filter_r $_vch $_ch" -writeProc "L372_filter_w filter 0 1 $_vch $_ch"

	CAMP_INT /~/inputs/$_vch/f_time -D -R -S -T "$_vch filter time" \
		-d on -r on -s on -units "s" \
		-H "Input $_vch filter settle time, 0 to 200s" \
		-readProc "L372_filter_r $_vch $_ch" -writeProc "L372_filter_w time 0 200 $_vch $_ch"

	CAMP_INT /~/inputs/$_vch/f_window -D -R -S -T "$_vch filter window" \
		-d on -r on -s on -units "%" \
		-H "Input $_vch filter window, as percent of full-scale reading (1 to 80%)" \
		-readProc "L372_filter_r $_vch $_ch" -writeProc "L372_filter_w window 1 80 $_vch $_ch"
}
# end _ch loop

CAMP_STRING /~/inputs/scanning -D -R -T "Scanning channels" \
	-d on -r on -readProc "L372_scan_r 0"

proc L372_inset_r { vch ch ins } {
	global CAMP_VAR_ATTR_POLL CAMP_VAR_ATTR_LOG
	set buf [insIfRRT /$ins "INSET? $ch" 64]
	if { [scan $buf { %d,%d,%d,%d,%d} en dw pa cn tc] < 5 } {
		return -code error "Bad input $ch reading '$buf'"
	}
	varDoSet /$ins/inputs/$vch/enable -v $en
	set d [expr { $en ? "on" : "off" }]
	set vst [varGetStatus /$ins/read_$vch]
	if { $en != ( ($vst & $CAMP_VAR_ATTR_POLL) != 0 ) } {
		varDoSet /$ins/read_$vch -p $d
	}
	if { !$en && ($vst & $CAMP_VAR_ATTR_LOG) } {
		varDoSet /$ins/read_$vch -l off
	}
	if { "$ch" != "A" } {
		varDoSet /$ins/inputs/$vch/scan_dwell -v $dw
		varDoSet /$ins/inputs/$vch/scan_pause -v $pa
		varDoSet /$ins/read_$vch -d $d -r $d
	}
	if { $cn < 21 } {varDoSet /$ins/inputs/$vch/curve -v $cn}
	if { $en } { L372_intyp_r $vch $ch $ins }
}

proc L372_inset_w { v min max vch ch ins val } {
	if { $val < $min || $val > $max } {
		return -code error "Setting out of range $min-$max"
	}
	set buf [insIfRRT /$ins "INSET? $ch" 64]
	if { [scan $buf { %d,%d,%d,%d,%d} en dw pa cn tc] < 5 } {
		return -code error "Bad input $ch reading '$buf'"
	}
	set $v $val
	insIfWRT /$ins "INSET $ch,$en,$dw,$pa,$cn,$tc"
	L372_inset_r $vch $ch $ins
}

proc L372_inen_w { vch ch ins val } {
	set val [expr {$val == 1}]
	L372_inset_w en 0 1 $vch $ch $ins $val
	set d [expr { $val ? "on" : "off" }]
	if { "$ch" != "A" } {
		set cs [expr {$val? $ch : 0}]
		insIfWRT /$ins [L372_scan_r $cs $ins]
		set css [expr {[varGetVal /$ins/inputs/scanning] == "off"}]
		varDoSet /$ins/read_$vch -d $d -r $d -p $d
		varSet /$ins/inputs/$vch/autoexcite -v [varGetVal /$ins/inputs/$vch/autoexcite]
	} else {
		set css [expr {!$val}]
		varDoSet /$ins/read_$vch -d on -p $d
	}
	L372_intyp_w css 0 1 $vch $ch $ins $css
}

# Set list of monitored inputs, return scan command string
# If input $ch is non-zero, prefer it as choice scan channel
proc L372_scan_r { ch ins } {
	set vlis {}
	set sc $ch
	foreach c {1 2 3 4 5 6} {
		set v [lindex {mix_cham sample M2 M3 M4 M5 M6} $c]
		if {[varGetVal /$ins/inputs/$v/enable]==1} {
			lappend vlis $v
			if {!$sc} {set sc $c}
		}
	}
	set a [expr { [llength $vlis] > 1 }]
	if { [llength $vlis] == 0 } {
		set m "off"
	} elseif { [llength $vlis] > 4 } {
		set m "[llength $vlis] inputs"
	} else {
		set m [join $vlis { }]
	}
	varDoSet /$ins/inputs/scanning -v $m
	return "SCAN $sc,$a"
}

proc L372_title_r { vch ch ins } {
	set title [string trim [insIfRead /$ins "INNAME? $ch" 64]]
	varDoSet /$ins/inputs/$vch/title -v $title
	if { "$title" == "" || [regexp {(Input|Channel) [A0-9]+} $title] } {
		set title "$vch ($ch)"
	}
	varDoSet /$ins/read_$vch -T "$title reading"
}

proc L372_title_w { vch ch ins target} {
	set title [string trim [join [split $target ",;\"\n"]]]
	if { [string length $title] > 15 } {
		return -code error "Title is too long -- 15 characters max"
	}
	insIfWrite /$ins "INNAME $ch, \"$title\""
	varRead /$ins/inputs/$vch/title
}

proc L372_intyp_r { vch ch ins } {
	set p /$ins/inputs/$vch
	set buf [insIfRRT /$ins "INTYPE? $ch" 64]
	if { [scan $buf { %d,%d,%d,%d,%d,%d} em er ar rr css un] == 6 } {
		set units [lindex {"" K Ohm} $un]
		varDoSet $p/units -v $un
		varDoSet /$ins/read_$vch -units $units
		if { $ch == "A" } {
			varDoSet /$ins/control_set -units $units
			varDoSet $p/range -v $er -m \
				"Excitation current [lindex {0.0 0.316 1.0 3.16 10.0 31.6 100} $er] nA and resistance range [lindex {? 632 200 63.2 20 6.32 2} $er] kOhm"
		} else {
			varDoSet $p/ex_mode -v $em 
			if { $em } {
				set mes "Excitation current [lindex {1.00 3.16 10.0 31.6 100 316} [expr {($er-1)%6}]] [lindex {p n u m} [expr {($er-1)/6}]]A"
			} else {
				set mes "Excitation voltage [lindex {2.00 6.32 20.0 63.2 200 632} [expr {($er-1)%6}]] [lindex {u m ""} [expr {($er-1)/6}]]V"
			}
			varDoSet $p/excitation -v $er -m $mes
			varDoSet $p/R-range -v $rr -m \
				"Resistance range [lindex {2.00 6.32 20.0 63.2 200 632} [expr {($rr-1)%6}]] [lindex {m "" k M} [expr {($rr-1)/6}]]Ohm"
		}
		varDoSet $p/autorange -v $ar
		set en [varGetVal $p/enable]
		if { ($en == 1 && $css == 1) || ($en == 2 && $css == 0) } {
			varDoSet $p/enable -v [expr {1+$css}]
		}
	}
}

proc L372_intyp_w { var min max vch ch ins val } {
	if {[string first " $var " " er rr un "]>=0 && $val == 0} {return}
	if { $val < $min || $val > $max } {
		return -code error "Setting out of range $min-$max"
	}
	set buf [insIfRRT /$ins "INTYPE? $ch" 64]
	if { [scan $buf { %d,%d,%d,%d,%d,%d} em er ar rr css un] < 6 } {
		return -code error "Bad input type readback"
	}
	set $var $val
	insIfWRT /$ins "INTYPE $ch,$em,$er,$ar,$rr,$css,$un"
	L372_intyp_r $vch $ch $ins
}

proc L372_tlimit_r { vch ch ins } {
	set lim 0.0
	scan [insIfRRT /$ins "TLIMIT? $ch" 32] " %f" lim
	varDoSet /$ins/inputs/$vch/temp_limit -v $lim
}

proc L372_tlimit_w { vch ch ins lim } {
	set lim [format "%.2f" [expr {$lim<0.0?0.0:$lim}]]
	insIfWRT /$ins "TLIMIT $ch,$lim"
	varDoSet /$ins/inputs/$vch/temp_limit -v $lim
}

proc L372_filter_r { vch ch ins } {
	set buf [insIfRRT /$ins "FILTER? $ch" 48]
	if {[scan $buf " %d, %d, %d" o t w] != 3} {
		return -code error "Invalid filter readback: $buf"
	}
	varDoSet /$ins/inputs/$vch/filter -v $o
	varDoSet /$ins/inputs/$vch/f_time -v $t
	varDoSet /$ins/inputs/$vch/f_window -v $w
}

proc L372_filter_w { par min max vch ch ins val } {
	if {$val < $min || $val > $max } {
		return -code error "Invalid setting (must be $min-$max)"
	}
	set buf [insIfRRT /$ins "FILTER? $ch" 48]
	if {[scan $buf " %d, %d, %d" filter time window] != 3} {
		return -code error "Invalid filter readback: $buf"
	}
	set $par $val
	insIfWRT /$ins "FILTER $ch,$filter,$time,$window"
	L372_filter_r $vch $ch $ins
}

proc L372_rdgpower_r { vch ch ins } {
	set buf [insIfRRT /$ins "RDGPWR? $ch" 48]
	set val 0.0
	scan $buf " %g" val
	set ho [expr {$val*1.0E9}]
	varDoSet /$ins/inputs/$vch/rdg_power -v $ho
}

proc L372_autoexcite_w { vch ch ins val } {
	if { $val } {
		varSet /$ins/inputs/$vch/ex_mode -v 1
		L372_autoexcite_r $vch $ch $ins
	}
	varDoSet /$ins/inputs/$vch/autoexcite -v $val -p [lindex {off on} $val] -p_int 80
}

proc L372_autoexcite_r { vch ch ins } {
	set p /$ins/inputs/$vch
	if { [varGetVal $p/enable] != 1 || [varGetVal $p/autoexcite] == 0 } return
	if { [varNumGetUnits /$ins/read_mix_cham] == "K" } {
		set T [varGetVal /$ins/read_mix_cham]
	} else return
	set rp [varRead $p/rdg_power; varGetVal $p/rdg_power]
	if { $rp <= 0.0 } return
	set ex [varRead $p/excitation; varGetVal $p/excitation]
	set old $ex
	set lT [expr {log10($T)}]
	if { $T < 8.0 } {
		set ep [expr {pow(10.,1.96284123*$lT-4.37160174-0.337162755*$lT*$lT-0.235438181*$lT*$lT*$lT)}]
	} else {
		set ep [expr {pow(10.,1.09194*$lT-4.1145)}]
	}
	while { $rp < $ep/3.5 } { set rp [expr {10.0*$rp}]; incr ex }
	while { $rp > $ep*3.3 } { set rp [expr {0.1*$rp}]; incr ex -1 }
	if { $ex != $old } {
		varSet $p/excitation -v $ex
	}
}

########################################################################################

proc L372_curve_r { vch ch ins } {
	global CAMP_VAR_ATTR_POLL
	if { [varGetStatus /$ins/inputs/$vch/curve] & $CAMP_VAR_ATTR_POLL } {# Asynchronous
		varDoSet /$ins/inputs/$vch/curve -p off
		L372_read_curve $vch $ch 2 $ins
	} else {
		# Do not validate on normal read because it's too slow when user reads all (space bar)
		L372_read_curve $vch $ch 0 $ins
	}
}

# For setting curve. For built-in curves, $curve must be an integer. For loaded curves,
# any identifying name, but usually a serial number > 59.
# Sets units to K and may initiate polling of inputs/curve/do_load
proc L372_set_curve { vch ch ins curve } {
	upvar \#0 ${ins}_cache cache
	set cc "Channel $vch ($ch) curve."
	varDoSet /$ins/inputs/$vch/curve -m "Select curve $curve"
	if { [catch { expr { $curve|0 }}] || $curve > 59 } {# non-numeric or a high number
		# First see if it is already loaded, in range 21-35 (also locate unused in that range)
		set num 0; set unused 0; set read 0
		for {set i 21} {$i<=35} {incr i} {
			if { ! [info exists cache(crvhdr_$i)] } {
				set cache(crvhdr_$i) [insIfRRT /$ins "CRVHDR? $i" 80]
				set read $i
			}
			if { [scan $cache(crvhdr_$i) {%[^,],%[^,],%d} name sn fmt ] == 3 } {
				set name [string trim $name " \""]
				set sn [string trim $sn " \""]
				if { [string tolower $sn] == [string tolower $curve] && $read != $i } {
					# curve has been loaded, according to old cache, but recheck before believing.
					set cache(crvhdr_$i) [insIfRRT /$ins "CRVHDR? $i" 80]
					scan $cache(crvhdr_$i) {%[^,],%[^,],%d} name sn fmt
					set sn [string trim $sn]
				}
				if { [string tolower $sn] == [string tolower $curve] } {
					set num $i
					break
				}
				if {$sn == "" && $unused == 0} { set unused $i }
			}
		}
		if { $num == 0 } {# User curve not already loaded
			if { $unused > 0 } {# Use unoccupied slot when possible
				set num $unused
			} else {# Pick a curve slot not currently referenced, and not the previous, in range 21-35
				foreach i {A 1 2 3 4 5 6} {
					if { $cache(${i}curve) == 999 } {
						scan [insIfRRT /$ins "INCRV? $i" 48] { %d} cache(${i}curve)
					}
					set ${i}c $cache(${i}curve)
				}
				set prev [varGetVal /$ins/inputs/curve/number]
				set num [expr { ($prev<21 || $prev>33) ? 25 : $prev+1 }]
				while { $num==$Ac || $num==$1c || $num==$2c || $num==$3c || $num==$4c || $num==$5c || $num==$6c } {
					set num [expr { $num>=35 ? 21 : $num+1 }]
				}
			}
			# now have number to load into. Initiate the asynchronous loading.
			varDoSet /$ins/inputs/$vch/curve -m "$cc Loading curve \"$curve\" as number $num"
			L372_setup_load $ins $vch $ch $curve $num
			return
		}
	} elseif { $curve < 0 } {
		varDoSet /$ins/inputs/$vch/curve -m "$cc FAIL \"$curve\""
		return -code error "bad value \"$curve\""
	} elseif { $curve == 0 } {# $curve 0 to deselect
		set sn 0
		set num 0
		set curve 0
	} else {# $curve <= 59 : slot $num
		set sn ""
		# always update cached header
		set cache(crvhdr_$curve) [insIfRRT /$ins "CRVHDR? $curve" 80]
		scan $cache(crvhdr_$curve) {%[^,],%[^,],} name sn
		set sn [string trim $sn " \""]
		if { $sn == "" } {
			varDoSet /$ins/inputs/$vch/curve -m "$cc Curve $curve is corrupt or empty"
			return -code error "Curve $curve is corrupt or empty"
		}
		set num $curve
		set curve $sn
	}
	# Select the curve (which we did not load)
	set n [L372_select_curve $ins $ch $num]
	if { $num == $n } {
		varDoSet /$ins/inputs/$vch/curve -m "Selected curve $num"
	} else {# Failed
		set e "Could not select curve $num for channel $ch"
		varDoSet /$ins/inputs/$vch/curve -m "$cc $e"
		if { $num >= 21 } { # A user curve - try reloading
			varDoSet /$ins/inputs/$vch/curve -m "$cc Reloading corrupt curve \"$curve\" as number $num"
			L372_setup_load $ins $vch $ch $curve $num
			return
		}
		return -code error $e
	}
	# read curve info without validation (sets curve/file to the name in the curve header)
	L372_read_curve $vch $ch 0 $ins
	# Use curve by setting units
	if {$num>0} {varSet /$ins/inputs/$vch/units -v "K"}
}

# Select the curve for the channel. This also verifies the integrity, somewhat.
# There is some delayed response to the selection, so do some retries.
# Return the proper number on success.

proc L372_select_curve { ins ch num } {
	upvar \#0 ${ins}_cache cache
	insIfWRT /$ins "INCRV $ch,$num"
	set i -5
	set n 0
	while { [incr i] < 0 && $n != $num } {
		catch {
			sleep 0.2
			scan [insIfRRT /$ins "INCRV? $ch" 48] { %d} n
			set cache(${ch}curve) $n
		}
	}
	return $n
}

# Set up the asynchronous loading of a curve. Try to take the "best" file
# type, and allow for several types for the same sensor. The silly glob-all
# followed by lsearch is because file name from header is uppercased, and
# some problem on vxworks. $curve is the identity string, but usually a
# serial number.
proc L372_setup_load { ins vch ch curve num } {
	set buf {}
	set matches {}
	catch { set buf [glob "./dat/*.*"] }
	set lcc [string tolower $curve]
	foreach file $buf {
		if { [string compare [string tolower [file rootname [file tail $file]]] $lcc] == 0 } {
			lappend matches $file
		}
	}
	set ei 0
	set fi -1
	foreach ext [list curve 340 dat 330 tr tbl spl] {
		incr ei
		set fi [lsearch -glob $matches "*.$ext"]
		if { $fi >= 0 } { break }
	}
	if { $fi < 0 } {
		varDoSet /$ins/inputs/$vch/curve -m \
			"Channel $vch ($ch) curve. Failed to find a data file for curve \"$curve\"."
		return -code error "curve \"$curve\" not found"
	}
	set file [format "./dat/%s" [file tail [lindex $matches $fi]]]
	# default format may be changed later by information in the file
	set format [lindex {0 2 0 2 1 1 1 3 0} $ei]
	# Put parameters in place to perform the loading asynchronously
	set cvp "/$ins/inputs/curve"
	varDoSet $cvp/number -v $num
	varDoSet $cvp/format -v $format
	varDoSet $cvp/id -v $curve
	varDoSet $cvp/file -v $file
	varDoSet $cvp/chan -v $vch
	varDoSet $cvp/do_load -p on -p_int 0.1
}

# ch is A, 1, 2, 3, 4, 5, 6 or "",
# validate is:
#   0 just read header,
#   1 just read header now, and schedule validation asynchronously
#   2 read and start to validate now -- validation completes asynchronously
# "name" in the curve header may be a sensor type or a curve file name or
# a curve file path/name. If not a filename, we can't validate.
# "sn" in the curve header is usually a serial number, but may be other name.

proc L372_read_curve { vch ch validate ins } {
	global CAMP_VAR_ATTR_POLL
	upvar \#0 ${ins}_cache cache
	set iic "/$ins/inputs/curve"
	varDoSet $iic/validate -m ""
	set num -1
	if { [string match {[A123456]} $ch] } {
		set curvar "/$ins/inputs/$vch/curve"
		set buf [insIfRRT /$ins "INCRV? $ch" 48]
		set num -99
		scan $buf " %d" num
		set cc "Channel $vch ($ch) curve."
	} else {
		set curvar "$iic/id"
		set num [varGetVal $iic/number]
		set cc ""
	}
	if { $num<0 || $num>59 } {
		set e "Invalid curve number $num"
		varDoSet $curvar -m $e
		return -code error $e
	}
	set cache(${ch}curve) $num
	if { $num==0 } {
		varDoSet $curvar -v $num -m [concat $cc "No curve"]
		return
	} 
	set curve $num
	# Fetch curve header
	set buf [insIfRRT /$ins "CRVHDR? $num" 80]
	if { [scan $buf {%[^,],%[^,],%d} name sn fmt ] != 3 } {
		set e "Failed read of curve $num"
		varDoSet $curvar -v $num -m [concat $cc $e]
		return -code error $e
	}
	set cache(crvhdr_$num) $buf
	set units [lindex {{} {mV/K} {V/K} {Ohm/K} {logOhm/K} {logOhm/logK} {?} {Ohm/K}} $fmt]
	set name [string trim $name "\" "]
	set sn [string trim $sn "\" "]
	set curve [string tolower $sn]
	if { $num < 21 } {
		varDoSet $curvar -v $num -m [concat $cc "Standard curve $name"]
		return
	}
	set file [string tolower $name]
	if { [string length $curve] && [string compare $name "User Curve"] } {
		if { [string first "/" $file] < 0 } { set file "./dat/$file" }
		set dir [file dirname $file]
		set tail [file tail $file]
		set root [file rootname $tail]
		set flist [glob ${dir}/*.*]
		set matches {}
		foreach ff $flist {
			set lcf [string tolower [file tail $ff]]
			if { ! [string compare $lcf $tail] } {# case-insensitive match
				lappend matches $ff
			}
		}
		if { [llength $matches] == 0 } {
			set file ""
		} else {
			set file [lindex $matches 0]
			if { [string match "./dat/*.*" [string tolower $file]] } {
				set file [file tail $file]
			}
		}
	} else {
		varDoSet $curvar -v $num -m "Empty User Curve $num"
		return
	}
	set cc [concat $cc "Curve $num containing $curve ($name)"]
	varDoSet $iic/number -v $num
	varDoSet $iic/file -v $file
	varDoSet $iic/id -v $curve
	varDoSet $iic/format -v [lindex {0 0 0 1 2 0 0 3 0} $fmt]
	varDoSet $iic/chan -v $vch
	if { $validate == 0 } {
		varDoSet $curvar -v $curve -m $cc
		return
	}
	if { [string length $file] == 0 || \
		  ([varGetStatus $iic/validate] & $CAMP_VAR_ATTR_POLL) || \
		  ([varGetStatus $iic/do_load] & $CAMP_VAR_ATTR_POLL) } {
		varDoSet $curvar -v $curve -m [concat $cc "(unvalidated)"]
		return
	}
	if { $validate == 1 && [string match {[A123456]} $ch] } {
		varDoSet $curvar -v $curve -p on -p_int 1 -m [concat $cc "(to be validated)"]
		return
	}
	# validate == 2
	varDoSet $curvar -v $curve -m [concat $cc "validating..."]
	set cache(v:chan) $ch
	set cache(v:msg) $cc
	if { [catch { L372_curve_validate $ins } msg] } {
		set e "Validation failed: $msg"
		varDoSet $curvar -m [concat $cc $e]
		varDoSet $iic/validate -m $e
		set cache(crvhdr_$num) ""
		return -code error $e
	}
}

# Start validation of curve data against file. This is invoked by read curve polled after 
# loading, or by setting curve/validate.  Validation of points is carried out in chunks by 
# polling curve/validate
proc L372_curve_validate { ins } {
	upvar \#0 ${ins}_cache cache
	set iic "/$ins/inputs/curve"
	set num [varGetVal $iic/number]
	set file [varGetVal $iic/file]
	varDoSet $iic/validate -m "Open file $file"
	if { [string first "/" $file] < 0 } { set file "./dat/$file" }
	if { [catch {set fid [open $file r]}] } {
		return -code error "Could not open file $file"
	}
	set npt 0
	set ncol 2
	# read header lines until we get to the curve data
	varDoSet $iic/validate -v 1 -m "Read header of file $file"
	set nln 0
	while { 1 } {
		incr nln
		if { [gets $fid line] == -1 } {
			close $fid
			return -code error "Failed to read a line ($nln)"
		}
		set line [string trim $line]
		set ns [scan $line "%d %c" v1 xx]
		if { $ns == 1 || ($ns > 1 && $xx == 44) } {# allow comma after number
			set npt $v1
		} elseif { [scan $line {%d %f %f %c} v1 v2 v3 xx] == 3 && $v1 == 1 } {
			set ncol 3
			break
		} elseif { [scan $line {%f %f %f %c} v1 v2 v3 xx] == 3 && $v1 != 1 } {
			set ncol 4
			break
		} elseif { [scan $line {%f %f %f %c} v1 v2 v3 xx] == 3 } {
			set ncol 2
			break
		} elseif { [scan $line {%f %f %c} v1 v2 xx] == 2 } {
			set ncol 2
			break
		} elseif { [scan $line "Serial Number: %s %c" v1 xx] == 1 } {
			set sn $v1
		} elseif { [scan $line "Data Format: %d" v1] == 1 } {
			if { [varGetVal $iic/format] != [lindex {0 0 0 1 2 0 0 3 0} $v1] } {
				return -code error "mismatched format codes"
			}
		} elseif { [scan $line "Number of Breakpoints: %d" v1] == 1 } {
			set npt $v1
		}
	}
	# Collect data lines, but we already have the first in $line
	set cache(v:lines) [linsert [split [string trim [read $fid]] "\n"] 0 $line]
	close $fid
	set ll [llength $cache(v:lines)]
	if { $npt > 0 } {
		if { $npt != $ll } {
			set cache(v:lines) {}
			return -code error "Bad file: expected $npt data points, but found $ll"
		}
	} else {
		set npt $ll
	}
	# We made it to the data. Do the rest later by polling.
	set cache(v:num) $num
	set cache(v:npt) $npt
	set cache(v:ncol) $ncol
	set cache(v:i) 0
	varDoSet $iic/validate -p on -p_int 0.1 -v 1 -m "Validate against $file with $npt points"
}

# This is the asynchronous part, so put errors in messages
proc L372_validate_more { ins } {
	upvar \#0 ${ins}_cache cache
	set iic "/$ins/inputs/curve"
	set npt $cache(v:npt)
	set ini $cache(v:i)
	set num $cache(v:num)
	set pt [expr {$ini+1}]
	set ch $cache(v:chan)
	set ich [expr {[string first $ch "A123456"]+1}]
	set vch [lindex {"" mix_cham sample M2 M3 M4 M5 M6} $ich]
	set scancmd [lindex { {} {}
		{[scan $line " %f %f" tf vf]}
		{[scan $line " %*d %f %f" vf tf]}
		{[scan $line " %f %f %f" vf tf cf]}
	} $cache(v:ncol)]
	varDoSet $iic/delete -m [concat $npt $ini $num $pt $ch $vch $scancmd [llength $cache(v:lines)]]
	set err ""
	for { set i $ini } { $i < $npt } { incr i; incr pt } {
		set line [lindex $cache(v:lines) $i]
		if "$scancmd < 2" {# quotes for double substitution in expr
			set err "bad data format in $file at point $pt"; break
		}
		if { [catch {insIfRRT /$ins "CRVPT? $num,$pt" 64} buf] || \
			 [scan $buf { %f,%f} vi ti] != 2 } {
			set err "invalid readback of point $pt: $buf"; break
		}
		if { abs($vf-$vi) > 0.00001 || abs($tf-$ti) > 0.001 } {
			set err "discrepancy at $pt: '$line' vs '$buf'"; break
		}
		if { $i % 10 == 9 && $i+1 < $npt } {
			set cache(v:i) [incr i]
			varDoSet $iic/validate -m "Validated $pt of $npt points"
			if { $ich } {
				varDoSet /$ins/inputs/$vch/curve -m "$cache(v:msg). Validated $pt of $npt points"
			}
			return
		}
	}
	if { [string length $err] } {
		set err "Validation failed: $err"
		varDoSet $iic/validate -p off -v 0 -m $err
		varDoSet $iic/do_load -m $err
		if { $ich } {
			varDoSet /$ins/inputs/$vch/curve -m "$cache(v:msg). $err"
		}
		set cache(v:lines) {}
		return -code error $err
	}
	varDoSet $iic/validate -p off -v 0 -m "Validated $npt points"
	varDoSet $iic/do_load -m ""
	if { $ich } {
		varDoSet /$ins/inputs/$vch/curve -m "$cache(v:msg). Validated."
	}
	set cache(v:lines) {}
}

############################################################################
#  Variables for loading curves.
#  Any curve file may be loaded into any curve number using these variables.
#  Curve loading and validation are done asynchronously by polling variables here.

CAMP_STRUCT /~/inputs/curve -D -d on -T "Curve Loading" \
	-H "Variables for loading curves (calibration tables)"

CAMP_INT /~/inputs/curve/number -D -S -T "Curve number" -d on -s on -units " " \
	-H "Identifies the internal curve number. Uploaded curves generally use 21-35, but the maximum is 59." \
	-writeProc L372_curve_number_w

proc L372_curve_number_w { ins target } {
	if { ($target < 21) || ($target > 59) } {
		return -code error "invalid curve number $target (expect 21-59)"
	}
	varDoSet /$ins/inputs/curve/number -v $target
}

CAMP_STRING /~/inputs/curve/file -D -S -R -T "Curve datafile" -d on -s on -r on \
	-readProc L372_curve_header_r -writeProc L372_curve_file_w

proc L372_curve_file_w { ins target } {
	varDoSet /$ins/inputs/curve/file -v $target
}

CAMP_SELECT /~/inputs/curve/format -D -S -R -T "Curve format" -d on -s on -r on \
	-selections "" {K,Ohm} {K,logOhm} {K,Ohm spline} \
	-readProc L372_curve_header_r -writeProc L372_curve_format_w

proc L372_curve_format_w { ins target } {
	varDoSet /$ins/inputs/curve/format -v $target
}

# Here the id is a string (not integer) for more generality.
CAMP_STRING /~/inputs/curve/id -D -S -R -T "Curve ID (serial num)" -d on -s on -r on \
	-H "The sensor curve's serial number or other identifier." \
	-readProc L372_curve_header_r -writeProc L372_curve_id_w

proc L372_curve_id_w { ins target } {
	varDoSet /$ins/inputs/curve/id -v [string trim [string tolower $target]]
}

proc L372_curve_header_r { ins } {
	L372_read_curve "" "" 0 $ins
}

CAMP_SELECT /~/inputs/curve/chan -D -S -T "Channel loading" \
	-H "Sensor channel to load curve for. Choose blank to load curves for future use." \
	-s on -d on -selections "" mix_cham sample M2 M3 M4 M5 M6 \
	-writeProc L372_curve_chan_w

proc L372_curve_chan_w { ins ich } {
	varDoSet /$ins/inputs/curve/chan -v $ich
}

CAMP_SELECT /~/inputs/curve/do_load -D -S -R -P -T "Do curve load" \
	-H "Load the curve now" \
	-d on -s on -r off -selections "" "LOAD" \
	-readProc L372_curve_doload_r -writeProc L372_curve_doload_w

# Since do_load is activated asynchronously by polling, capture
# errors and emit them as messages
proc L372_curve_doload_r { ins } {
	set iic "/$ins/inputs/curve"
	set num [varGetVal $iic/number]
	set ich [varGetVal $iic/chan]
	set ch [lindex {"" A 1 2 3 4 5 6} $ich]
	set vch [varSelGetValLabel $iic/chan]
	set dol "$iic/do_load"
	varDoSet $dol -m "Loading curve file [varGetVal $iic/file] as curve $num"
	if { [catch {L372_curve_do_load $ins} msg] } {
		varDoSet $dol -v 0 -p off -m $msg
		if { $ich } {
			varDoSet /$ins/inputs/$vch/curve -m "Failed to load curve data: $msg"
		}
		varDoSet $dol -m "Failed to load curve data: $msg"
		varDoSet $iic/chan -v 0
		return
	}
	if { $ich } {
		set n [L372_select_curve $ins $ch $num]
		if { $n != $num } {
			varDoSet $dol -v 0 -p off -m "Failed to select curve $num for channel $ch"
			varDoSet /$ins/inputs/$vch/curve -m "Failed to select curve num $num"
			return
		}
		varSet /$ins/inputs/$vch/units -v "K"
	}
	varDoSet $dol -v 0 -p off -m "Validating curve data"
	if { [catch { L372_read_curve $vch $ch 2 $ins } msg] } {
		varDoSet $dol -m "Validation failed: $msg"
	} else {
		varDoSet $dol -m "Loaded $num. Validating..."
	}
}

proc L372_curve_doload_w { ins target } {
	global CAMP_VAR_ATTR_POLL
	if { $target == 0 } {
		varDoSet /$ins/inputs/curve/do_load -v $target -p off -m ""
		return
	} else {
		if { ([varGetStatus /$ins/inputs/curve/do_load] & $CAMP_VAR_ATTR_POLL) } {
			return -code error "Busy loading a curve already"
		}
		varDoSet /$ins/inputs/curve/do_load -v $target -p on -p_int 0.1 -m "Start loading curve"
		#L372_curve_do_load $ins
	}
}

CAMP_SELECT /~/inputs/curve/validate -D -S -R -P -T "Validate curve" \
	-H "Validate curve in memory compared with file" \
	-d on -s on -r off -p off -selections "" "Validate" \
	-readProc L372_validate_more -writeProc L372_curve_validate_w

# Validation starts when this is set (or L372_curve_validate invoked),
# and it completes by repeated polls (using L372_validate_more).
proc L372_curve_validate_w { ins target } {
	global CAMP_VAR_ATTR_POLL
	upvar \#0 ${ins}_cache cache
	if { $target == 0 } { 
		varDoSet /$ins/inputs/curve/validate -v $target -p off -m ""
		set cache(lines) {}
		return
	} else {
		if { [varGetStatus /$ins/inputs/curve/do_load] & $CAMP_VAR_ATTR_POLL } {
			return -code error "Busy loading currently"
		}
		set cache(v:chan) ""
		set cache(v:msg) ""
		if { [catch {L372_curve_validate $ins} msg] } {
			varDoSet /$ins/inputs/curve/validate -v 0 -p off -m "Validation failed: $msg"
			set cache(lines) {}
			return -code error  "Validation failed: $msg"
		}
	}
}

CAMP_SELECT /~/inputs/curve/delete -D -S -T "Delete curve" \
	-H "Delete curve from slot {number}" \
	-d on -s on -selections "" "Delete" "Disuse&Delete" \
	-writeProc L372_curve_delete_w

proc L372_curve_delete_w { ins target } {
	upvar \#0 ${ins}_cache cache
	if { $target > 0 } {
		set num [varGetVal /$ins/inputs/curve/number]
		if { $num < 21 || $num > 59 } {
			return -code error "Only 'user' curves in the range 21 to 59 may be deleted"
		}
		foreach ch {A 1 2 3 4 5 6} {
			if { $cache(${ch}curve) == $num } {
				if { $target == 1 } {
					return -code error "Sorry, curve number $num is being used by channel $ch"
				} else {
					set cache(${ch}curve) 0
					set vch mix_cham
					catch { set vch [lindex {mix_cham sample M2 M3 M4 M5 M6} $ch] }
					varSet /$ins/inputs/$vch/curve -v 0
				}
			}
		}
		insIfWRT /$ins "CRVDEL $num"
		set cache(crvhdr_$num) ""
	}
}

# Here we load a curve from a file, using the variables on the inputs/curve page.
# Some parameters may be overridden by information in the curve file.
proc L372_curve_do_load { ins } {
	upvar \#0 ${ins}_cache cache
	set cache(can_reload) 0
	set iic /$ins/inputs/curve
	set file [varGetVal $iic/file]
	set name [file tail $file]
	if { ! [string compare $name $file] } {set file "./dat/$file"}
	set num [varGetVal $iic/number]
	set fmt [lindex {0 3 4 7} [varGetVal $iic/format]]
	set coef [lindex {0 0 0 2 1 0 0 1} $fmt]
	set sn [varGetVal $iic/id]
	set npt 0
	set ncol 2
	set lim 999
	varDoSet $iic/do_load -v 1 -p off
	if { [catch {set fid [open $file r]}] } {
		return -code error "could not open $file"
	}
	# read header lines until we get to the curve data
	set nln 0
	while { 1 } {
		incr nln
		if { [gets $fid line] == -1 } {
			close $fid
			return -code error "Failed to read a line ($nln)"
		}
		set line [string trim $line]
		set ns [scan $line "%d %c" v1 xx]
		if { $ns == 1 || ($ns > 1 && $xx == 44) } {
			set npt $v1
		} elseif { [scan $line {%d %f %f %c} v1 v2 v3 xx] == 3 && $v1 == 1 } {
			set ncol 3
			break
		} elseif { [scan $line {%f %f %f %c} v1 v2 v3 xx] == 3 && $v1 != 1 } {
			set ncol 4
			break
		} elseif { [scan $line {%f %f %f %c} v1 v2 v3 xx] == 3 } {
			set ncol 2
			break
		} elseif { [scan $line {%f %f %c} v1 v2 xx] == 2 } {
			set ncol 2
			break
		} elseif { [scan $line "Serial Number: %s %c" v1 xx] == 1 } {
			set sn $v1
		} elseif { [scan $line "Data Format: %d" v1] == 1 } {
			set fmt $v1
			varDoSet $iic/format -v [lindex {0 0 0 1 2 0 0 3} $fmt]
		} elseif { [scan $line "Temperature coefficient: %d" v1] == 1 } {
			set coef $v1
		} elseif { [scan $line "Number of Breakpoints: %d" v1] == 1 } {
			set npt $v1
		} elseif { [scan $line "Setpoint Limit: %f" v1] == 1 } {
			set lim $v1
		}
	}
	if { $coef == 0 } { set coef [lindex {0 0 0 2 1 0 0 1} $fmt] }

	# Collect data lines, but we already have the first in $line
	set lines [linsert [split [string trim [read $fid]] "\n"] 0 $line]
	close $fid
	if { $npt > 0 } {
		if { $npt != [llength $lines] } {
			return -code error "bad file $file: expected $npt data points, but found [llength $lines]"
		}
	} else {
		set npt [llength $lines]
	}
	# delete the curve in memory
	insIfWRT /$ins [format "CRVDEL %2d" $num]
	# send the new header
	insIfWRT /$ins [format "CRVHDR %2d,%s,%s,%d,%.2f,%d" $num $name $sn $fmt $lim $coef]
	if { $ncol == 3 } {
		set scancmd {[scan $line " %*d %f %f" v t]}
	} elseif { $ncol == 4 && $fmt == 7 } {
		set scancmd {[scan $line " %f %f %f" v t c]}
	} else {
		if { $fmt == 7 } { set fmt 3 }
		set scancmd {[scan $line " %f %f" t v]}
	}
	set cmd_line ""
	set i 0
	set maxt -1.0
	set sp 0.0
	set pt1 ""
	foreach line $lines {
		incr i
		if "$scancmd < 2" {# quotes for double substitution in expr; see above
			return -code error "bad format in file $file at line $nln"
		}
		if { $t > $maxt } { set maxt $t }
		if { $i > 1 } {
			if { ( $t != $tp ) && ( $v != $vp ) } {
				set s [expr {($v-$vp)/($t-$tp)}]
				if { $sp != 0.0 && ($s > 0.0) != ($sp > 0.0) } {
					return -code error "bad file: slope changed sign at point $i, line $nln"
				}
			}
			if { $v < $vp } {
				return -code error "bad file: raw sensor values should be increasing"
			}
		}
		set tp $t ; set vp $v
		set tfor [expr { $t < 9.99 ? "%.5f" : "%.6g" }]
		set vfor [expr { $v < 9.99 ? "%.5f" : "%.6g" }]
		if { $fmt == 7 } {
			set cmd [format "CRVPT %d,%d,$vfor,$tfor,%.4e" $num $i $v $t $c]
		} else {
			set cmd [format "CRVPT %d,%d,$vfor,$tfor" $num $i $v $t]
		}
		if { $i == 1 } {# keep first point
			set pt1 $cmd
		}
		set cll [string length $cmd_line]
		if { $cll == 0 } {
			set cmd_line $cmd
		} elseif { $cll + 1 + [string length $cmd] > 76 } {
			insIfWRT /$ins $cmd_line
			set cmd_line $cmd
		} else {
			append cmd_line ";" $cmd
		}
		incr nln
	}
	if { [string length $cmd_line] } {
		insIfWRT /$ins $cmd_line
	}
	if { $lim == 999 } { set lim $maxt }
	# repeat first point because it gets lost!! WHY???!!
	insIfWRT /$ins $pt1
	insIfWRT /$ins [format "CRVHDR %2d,%s,%s,%d,%.3f,%d" $num $name $sn $fmt $lim $coef]
}

####################################################################################

CAMP_STRUCT /~/setup -D -d on -T "Setup parameters"

CAMP_SELECT /~/setup/id -D -R -P -T "ID Query" -d on -r on -p off -p_int 1.0 \
	-selections FALSE TRUE -readProc L372_id_r

proc L372_id_r { ins } {
	global CAMP_VAR_ATTR_POLL
	upvar \#0 ${ins}_cache cache
	set pl [expr {[varGetStatus /$ins/setup/id] & $CAMP_VAR_ATTR_POLL}]
	set id [varGetVal /$ins/setup/id]
	if { $id==0 || $pl==0 } {
		set id 0
		set status [catch {insIfRRT /$ins "*IDN?" 60} buf]
		if { $status } {
			sleep 0.3
			set status [catch {insIfRRT /$ins "*IDN?" 60} buf]
		}
		if { $status == 0 } {
			set id [scan $buf { LSCI,MODEL372,%*[^,],%f} val]
			if { $id != 1 } { set id 0 }
		}
		varDoSet /$ins/setup/id -v $id
	}
	#
	if { $id==0 } { return }
	# preload cache of curve numbers, and read vars
	foreach ch {0 1 2 3 4 5 6} {
		set vch [lindex {mix_cham sample M2 M3 M4 M5 M6} $ch]
		if { $ch == 0 } { set ch "A" }
		if { $cache(${ch}curve) == 999 } {
			varRead /$ins/inputs/$vch/enable
			varRead /$ins/inputs/$vch/units
			scan [insIfRRT /$ins "INCRV? $ch" 60] { %d} cache(${ch}curve)
			if {"$vch"=="M6"} { varRead /$ins/inputs/scanning }
			return
		}
	}
	# read zones
	for {set i 1} {$i<=10} {incr i} {
		set v /$ins/outputs/heater/zone/zone$i
		if {[llength [varGetVal $v]] < 4} {
			varRead $v ; return
		}
	}
	# preload cache of curve headers slowly
	for {set i 21} {$i<=35} {incr i} {
		if { ! [info exists cache(crvhdr_$i)] } {
			varDoSet /$ins/setup/id -p on -p_int 2.0
			set cache(crvhdr_$i) [insIfRRT /$ins "CRVHDR? $i" 80]
			return
		}
	}
	# Done initializing
	varDoSet /$ins/setup/id -p off
}

unset _vch
unset _ch
unset _io
