# camp_ins_magnavolt_hcl.tcl
# Magnavolt HCL series high-voltage power supply
#
# Hard-coded for unipolar supply.  Could be extended if upgrade to bipolar: look for
# "unipolar" comment.

CAMP_INSTRUMENT /~ -D -T "Magnavolt HCL PS" \
    -H "Magnavolt HCL series high-voltage power supply" -d on \
    -initProc magv_init \
    -deleteProc magv_delete \
    -onlineProc magv_online \
    -offlineProc magv_offline

proc magv_init { ins } {
   insSet /${ins} -if rs232 0.1 1 /tyCo/1 9600 8 none 1 CR CR
}
proc magv_delete { ins } {
  insSet /${ins} -line off
}
proc magv_online { ins } {
  insIfOn /${ins}
  insIfWrite /$ins "G0"
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == 0) } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections and switch to 'digital'"
  }
  varRead /${ins}/output
  varRead /${ins}/setup/max_current
  varRead /${ins}/setup/max_voltage
}
proc magv_offline { ins } {
  insIfOff /${ins}
}

CAMP_FLOAT /~/set_voltage -D -S -L -T "Set voltage" \
    -H "Set desired output voltage (output will be less when limited by current)" \
    -d on -s on -units kV \
    -writeProc magv_voltage_w
  proc magv_voltage_w { ins target } {
      set m [varGetVal /${ins}/setup/max_voltage]
      if { $target < 0.0 || $target > $m } {# unipolar
          return -code error "Voltage setting outside range 0-$m"
      }
      set v [expr {1000.0*$target}]
      insIfWrite /$ins "U$v"
      magv_error_check $ins
      varDoSet /${ins}/set_voltage -v $target
  }

CAMP_FLOAT /~/read_voltage -D -R -P -L -A -T "Read voltage" \
    -H "Read output voltage" \
    -d on -r on -p on -p_int 10 -units kV \
    -readProc magv_voltage_r
  proc magv_voltage_r { ins } {
      insIfWrite /$ins "N0"
      set buf [insIfRead /$ins "?" 32]
      if { [scan $buf " %gV%c" v c] != 2 } {
          return -code error "Invalid voltage readback: $buf"
      }
      varDoSet /${ins}/read_voltage -v [format %.3f [expr { $v * 0.001 }] ]
      varTestAlert /${ins}/read_voltage [varGetVal /${ins}/set_voltage]
  }

CAMP_FLOAT /~/set_current -D -S -L -T "Set Current" \
    -H "Set desired output current (current will be less when regulating voltage)" \
    -d on -s on -units mA \
    -writeProc magv_current_w
  proc magv_current_w { ins target } {
      set m [varGetVal /${ins}/setup/max_current]
      if { $target < 0.0 || $target > $m } {# unipolar
          return -code error "Current setting outside range 0-$m"
      }
      set i [expr {0.001*$target}] 
      insIfWrite /$ins "I$i"
      magv_error_check $ins
      varDoSet /${ins}/set_current -v $target
  }

CAMP_FLOAT /~/read_current -D -R -P -L -A -T "Read Current" \
    -H "Read output current" \
    -d on -r on -p off -p_int 10 -units mA \
    -readProc magv_current_r
  proc magv_current_r { ins } {
      insIfWrite /$ins "N1"
      set buf [insIfRead /$ins "?" 16]
      if { [scan $buf " %gA%c" i c] != 2 } {
          return -code error "Invalid current readback: $buf"
      }
      varDoSet /${ins}/read_current -v [format %.5f [expr { $i * 1000.0 }] ]
      varTestAlert /${ins}/read_current [varGetVal /${ins}/set_current]
  }

CAMP_SELECT /~/output -D -S -R -P -T "Supply on/off" \
    -H "Switch supply on or off" \
    -selections "OFF" "ON" \
    -d on -s on -r on -p off -p_int 10 \
    -readProc magv_status_r -writeProc magv_output_w
  proc magv_output_w { ins target } {
      insIfWrite /$ins "F$target"
      magv_status_r $ins
  }

CAMP_SELECT /~/regulating -D -R -P -T "Regulating what" \
    -H "Supply is regulating this" \
    -selections "None" "Current" "Voltage" \
    -d on -r on -p off -p_int 10 \
    -readProc magv_status_r

# unipolar...
#CAMP_SELECT /~/polarity -D -R -P -T "Polarity" \
#    -selections "Negative" "Positive" \
#    -d on -r on -p off -p_int 10 \
#    -readProc magv_status_r


proc magv_status_r { ins } {
    insIfWrite /$ins "N2"
    set buf [insIfRead /$ins "?" 16]
    if { [scan $buf "%1d%1d%1d%1d%1d%1d%1d" i u f x p c e] != 7 } {
        return -code error "Invalid status code: $buf"
    }
    varDoSet /${ins}/regulating -v [expr { $u ? 2 : $i }]
    varDoSet /${ins}/output -v $f
    # unipolar... varDoSet /${ins}/polarity -v $p
}

proc magv_error_check { ins } {
    insIfWrite /$ins "N2"
    set buf [insIfRead /$ins "?" 16]
    set e 0
    scan $buf "%6d%1d" x e
    if { $e == 0 } { return }
    if { $e == 2 } { return -code error "Value out of range" }
    return -code error "Unknown command"
}


CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc magv_id_r
  proc magv_id_r { ins } {
      set id 0
      set buf x
      insIfWrite /${ins} "N5"
      catch {insIfRead /${ins} "?" 15} buf
      set id [expr {[scan $buf " IB%d V%f%c" i1 i2 c] == 3}]
      varDoSet /${ins}/setup/id -v $id
  }

# See manual for special procedures before attempting to set new limits.
# The version here does *not* apply new settings to the instrument, but
# only uses them as local variables.
CAMP_FLOAT /~/setup/max_voltage -D -R -S -T "Maximum Voltage" \
    -H "Rated voltage limit" \
    -d on -s off -r on -units kV \
    -readProc magv_maxvoltage_r -writeProc magv_maxvoltage_w
  proc magv_maxvoltage_w { ins target } {
      varRead /${ins}/max_voltage
      set m [varGetVal /${ins}/setup/max_voltage]
      if { $target < 0.0 || $target > $m } { # unipolar
          return -code error "Voltage limit must be in range 0-$m"
      }
      # set v [expr {1000.0*$target}] 
      # insIfWrite /$ins "V$v"
      # magv_error_check $ins
      varDoSet /${ins}/max_voltage -v $target
  }
  proc magv_maxvoltage_r { ins } {
      insIfWrite /$ins "N3"
      set buf [insIfRead /$ins "?" 16]
      if { [scan $buf " %g" v] != 1 } {
          return -code error "Invalid voltage readback: $buf"
      }
      varDoSet /${ins}/setup/max_voltage -v [expr { $v * 0.001 }]
  }

CAMP_FLOAT /~/setup/max_current -D -R -S -T "Maximum Current" \
    -H "Rated current limit" \
    -d on -s off -r on -units mA \
    -readProc magv_maxcurrent_r -writeProc magv_maxcurrent_w
  proc magv_maxcurrent_w { ins target } {
      varRead /${ins}/max_current
      set m [varGetVal /${ins}/setup/max_current]
      if { $target < 0.0 || $target > $m } { # unipolar
          return -code error "Current limit must be in range 0-$m"
      }
      # set i [expr {0.001*$target}] 
      # insIfWrite /$ins "A$i"
      # magv_error_check $ins
      varDoSet /${ins}/max_current -v $target
  }
  proc magv_maxcurrent_r { ins } {
      insIfWrite /$ins "N4"
      set buf [insIfRead /$ins "?" 16]
      if { [scan $buf " %g" i] != 1 } {
          return -code error "Invalid current readback: $buf"
      }
      varDoSet /${ins}/setup/max_current -v [expr { $i * 1000.0 }]
  }

CAMP_SELECT /~/setup/integ_time -D -S -T "Integration time" \
    -H "V and I reading integration time" \
    -selections "2ms" "16.66ms" "20ms" "50ms" "60ms" "100ms" "200ms" "800ms" \
    -d on -s on -v 5  \
    -writeProc magv_integt_w
  proc magv_integt_w { ins target } {
      insIfWrite /$ins "S$target"
      # Do another I/O for extra delay
      insIfWrite /$ins ""
      varDoSet /${ins}/setup/integ_time -v $target
  }
