CAMP_INSTRUMENT /~ -D -T "Metrolab PT3020 NMR Teslameter" -d on \
    -initProc pt3020_init \
    -deleteProc pt3020_delete \
    -onlineProc pt3020_online \
    -offlineProc pt3020_offline
proc pt3020_init { ins } {
    insSet /${ins} -if rs232 0.5 2 tt0 9600 8 none 1 CRLF none
}
proc pt3020_delete { ins } {
    insSet /${ins} -line off
}
proc pt3020_online { ins } {
    insIfOn /${ins}
    varRead /${ins}/op_mode
    set mode [varGetVal /${ins}/op_mode]
    if { $mode == 0 } {
        insIfOff /${ins}
        return -code error "failed query, check interface definition and connections"
    }
    if {[catch {varRead /${ins}/field} err]} {
        return -code error "failed read: $err"
    }
    if { $mode == 1 } {
        return -code error "Warning: meter is in autonomous mode for reading only (switch 7)"
    }
    varSet /${ins}/units -v "T"
}
proc pt3020_offline { ins } {
    insIfOff /${ins}
}

CAMP_FLOAT /~/field -D -R -P -L -T "Field reading" -d on -r on \
    -readProc pt3020_reading_r

proc pt3020_reading_r { ins } {
    for { set i 0 } { $i < 3 } { incr i } {
        set buf [insIfRead /${ins} "\x05" 16]
        if { [scan $buf {%[LN]%f%[FT]} l f u] == 3 } { break }
    }
    if { $i < 3 } {
        if { $u == "F" } { set u "MHz" }
        switch $l {
            L {set lock 2}
            N {set lock 1}
            default {set lock 0}
        }
        varDoSet /${ins}/field -v $f -units $u
	varDoSet /${ins}/units -v $u
        varDoSet /${ins}/lock_status -v $lock
    } else {
        return -code error "Failed read. Got \"$buf\""
    }
}

CAMP_SELECT /~/units -D -R -S -T "Field units" -d on -r on -s on \
    -selections MHz T \
    -readProc pt3020_units_r -writeProc pt3020_units_w

proc pt3020_units_r { ins } {
    if { [varGetVal /${ins}/op_mode] == 2 && \
	     [scan [insIfRead /${ins} "S3" 8] "S%02x%c" val xx] == 1 } {
	varDoSet /${ins}/units -v [expr {$val&0x1}] -m $val
	return
    }
    # fallback:
    varRead /${ins}/field
}

# This and all other settings should use the temporary REMOTE setting.
proc pt3020_units_w { ins target } {
    insIfWrite /${ins} "R" 
    insIfWrite /${ins} "D$target" 
    #insIfWrite /${ins} "L"
    varRead /${ins}/units
}

CAMP_SELECT /~/lock_status -D -R -T "Signal lock status" -d on -r on \
    -selections "" Unlocked Locked \
    -readProc pt3020_reading_r

CAMP_SELECT /~/op_mode -D -R -T "Operation mode" -d on -r on \
    -selections None Autonomous Interactive \
    -readProc pt3020_op_mode_r

proc pt3020_op_mode_r { ins } {
    set mode 0
    for { set i 0 } { $i < 3 } { incr i } {
        if { ! [catch {insIfRead /${ins} "S3" 16 } buf] } {
            if {[scan $buf "S%2x" val]==1} {
                set mode 2 ; break
            } elseif {[scan $buf {%[LN]%f%[FT]} l f u] == 3 } {
                set mode 1 ; break
            }
        }
    }
    varDoSet /${ins}/op_mode -v $mode
}
