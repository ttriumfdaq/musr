# camp_ins_omega_cn606.tcl
#
# Camp instrument driver for Omega CN606 6-channel temperature monitor.
# This device has very obscure front panel controls, so this driver aims
# to fully control the operation.
#
# $Log: camp_ins_omega_cn606.tcl,v $
# Revision 1.3  2015/03/15 00:42:03  suz
# Ted's 201404 interface changes
#
# Revision 1.2  2009/06/18 05:51:14  asnd
# Fix setting of alarm trip points (bogus checksum)
#
# Revision 1.1  2008/10/30 05:22:01  asnd
# New instrument Omega CN606 temperature monitor
#

CAMP_INSTRUMENT /~ -D -T "Omega CN606" -d on \
	-H "Omega CN606 Temperature Monitor" \
	-initProc cn606_init \
	-deleteProc cn606_delete \
	-onlineProc cn606_online \
	-offlineProc cn606_offline

proc cn606_init { ins } {
    insSet /${ins} -if rs232 0.5 2 /tyCo/1 4800 8 none 1 none none
}
proc cn606_delete { ins } { insSet /${ins} -line off }
proc cn606_online { ins } {
    insIfOn /${ins}
    if { [catch {
	varRead /${ins}/setup/model
        varRead /${ins}/setup/enabled_chan
    } msg ] } {
        # Leave interface on in case user needs to set setup/serial_num
	# insIfOff /${ins}
	return -code error "Check connections, interface settings, and setup/serial_num"
    }
    varDoSet /${ins}/temp_1 -p on
}
proc cn606_offline { ins } { insIfOff /${ins} }


for { set chann 1 } { $chann<=6 } { incr chann } {
   
CAMP_FLOAT /~/temp_$chann -D -T "Temperature $chann" -R -P -L -A \
    -d off -r off -p off -p_int 10 -units K \
    -readProc cn606_temp_r

}

proc cn606_temp_r { ins } {
    set buf [cn606_read $ins T 49]
    if { [scan $buf {%4d%4d%4d%4d%4d%4d} t(1) t(2) t(3) t(4) t(5) t(6)] != 6 } {
        return -code error "Failed scan $buf"
    }
    for { set ch 1 } { $ch <= 6 } { incr ch } {
        if { $t($ch) < 5000 } { incr t($ch) 273 }
        varDoSet /${ins}/temp_$ch -v $t($ch)
    }
    cn606_inp_alm_r $ins
}

CAMP_STRUCT /~/alarms -D -T "Alarm settings" -d on


for { set chann 1 } { $chann<=6 } { incr chann } {
   
CAMP_FLOAT /~/alarms/low_$chann -D -T "Low Alarm $chann" -R -S \
    -d on -r on -s on -units K \
    -readProc cn606_almsetp_r -writeProc [list cn606_almsetp_w low $chann] 

CAMP_FLOAT /~/alarms/high_$chann -D -T "High Alarm $chann" -R -S \
    -d on -r on -s on -units K \
    -readProc cn606_almsetp_r -writeProc [list cn606_almsetp_w high $chann] 

}

unset chann

# Read alarm setpoints, all channels
proc cn606_almsetp_r { ins } {
    set buf [cn606_read $ins S 97]
    foreach limit {low high} {
        if { [scan $buf "%4d%4d%4d%4d%4d%4d" t(1) t(2) t(3) t(4) t(5) t(6)] != 6 } {
            return -code error "Failed scan for $limit limits"
        }
        for { set ch 1 } { $ch <= 6 } { incr ch } {
            incr t($ch) 273
            varDoSet /${ins}/alarms/${limit}_$ch -v $t($ch)
        }
        set buf [string range $buf 48 97]
    }
}

proc cn606_almsetp_w { limit ch ins val } {
    set val [expr round($val)]
    if { $val < 273 || $val > 9700 } {
        return -code error "Alarm setpoint out of range"
    }
    set buf [cn606_read $ins S 97]
    set idx [expr { ("$limit"=="high" ? 48 : 0) + 4*($ch-1) - 1 }]
    set buf "[string range $buf 0 $idx][format %4.4d [expr {$val-273}]][string range $buf [incr idx 5] 95]"
    # Requres a checksum of 48 = ascii "0"
    # cn606_write $ins s $buf
    set sn [varGetVal /${ins}/setup/serial_num]
    set csum "0"
    insIfWrite /${ins} "L${sn}s${buf}${csum}"
    varDoSet /${ins}/alarms/${limit}_$ch -v $val 
}


CAMP_STRUCT /~/setup -D -T "Configuration settings" -d on

CAMP_STRING /~/setup/enabled_chan -D -R -S -T "Enabled channels" \
    -d on -s on -r on \
    -H "List of all enabled channels, from 1 to 6" \
    -readProc cn606_inp_alm_r -writeProc cn606_inputs_w

proc cn606_inp_alm_r { ins } {
    set buf [cn606_read $ins A 15]
    cn606_check_sum $buf 14
    if { [scan $buf {%2x%2x%2x%2x%2x%2x%2d} enab e2 hi h2 lo l2 scan] != 7 } {
        return -code error "Failed scan $buf"
    }
    set mask 1
    set enlist ""
    for { set ch 1 } { $ch <= 6 } { incr ch } {
        if { $enab & $mask } {
            append enlist $ch
            if { ($hi|$lo) & $mask } {
                varDoSet /${ins}/temp_$ch -r on -d on -alert on 
            } else {
                varDoSet /${ins}/temp_$ch -r on -d on -alert off
            }
        } else {
            varDoSet /${ins}/temp_$ch -r off -d off -alert off
        } 
        set mask [expr 2*$mask]
    }
    varDoSet /${ins}/setup/enabled_chan -v $enlist
    varDoSet /${ins}/setup/scan_time -v $scan
}

proc cn606_inputs_w { ins inputs } {
    set scan [varGetVal /${ins}/setup/scan_time]
    set mask 0
    foreach c [split $inputs {}] {
        catch { 
            incr c -1
            set mask [expr { $mask | 1<<($c) } ]
        }
    }
    if { $mask != ($mask & 0x3f) } {
        return -code error "Channels must be in range 1 to 6"
    }
    set sn [varGetVal /${ins}/setup/serial_num]
    # Requires "0" for check-sum character, always.
    set buf [format "L%1de%2.2X00%2.2d0" $sn $mask $scan]
    insIfWrite /${ins} $buf
    cn606_inp_alm_r $ins
}

CAMP_SELECT /~/setup/model -D -R -T "model type" \
    -H "Meter model (sensor) type" \
    -selections Thermocouple Resistor \
    -d on -r on -v 0 \
    -readProc cn606_model_r

proc cn606_M_set { var ins val } {
    set buf [cn606_read $ins M 15]
    #cn606_check_sum $buf 14
    if { [scan $buf {%1d%1d%1d%1d%4d%1d%1d%2d} \
              alarm_type relu model sens_type passw skip idn ninp] != 8 } {
        return -code error "Failed scan $buf"
    }
    set relay_latch [expr {$relu / 2}]
    set temp_unit [expr {$relu % 2}]
    set $var $val
    set relu [expr {$relay_latch*2 + $temp_unit}]
    set buf [format {%1d%1d%1d%1d%4d%1d} $alarm_type $relu $model $sens_type $passw $idn]
    cn606_write $ins m $buf
    varDoSet /${ins}/setup/$var -v $val
}   

# This readproc reads all the setup info given by "function 7"
proc cn606_model_r { ins } {
    set buf [cn606_read $ins M 13]
    #cn606_check_sum $buf 12
    if { [scan $buf {%1d%1d%1d%1d%4d%2d%2d} alm relu model sens passw idn ninp] != 7 } {
        return -code error "Failed scan $buf"
    }
    varDoSet /${ins}/setup/alarm_type -v $alm
    varDoSet /${ins}/setup/relay_latch -v [expr {$relu / 2}]
    varDoSet /${ins}/setup/temp_unit -v [expr {$relu % 2}]
    varDoSet /${ins}/setup/model -v $model
    if { $model } {
        varDoSet /${ins}/setup/TC_type -r off -s off -d off
        varDoSet /${ins}/setup/RTD_type -v $sens -r on -s on -d on
    } else {
        varDoSet /${ins}/setup/TC_type -v $sens -r on -s on -d on
        varDoSet /${ins}/setup/RTD_type -r off -s off -d off
    }
    #varDoSet /${ins}/setup/passwd -v $passw
    #varDoSet /${ins}/setup/idnum -v $idn
    #varDoSet /${ins}/setup/num_input -v $ninp
}


CAMP_SELECT /~/setup/TC_type -D -R -S -T "Thermocouple type" \
    -selections B C E J K R S T \
    -d on -r on -s on \
    -readProc cn606_model_r -writeProc {cn606_M_set sens_type}

CAMP_SELECT /~/setup/RTD_type -D -R -S -T "Resistor type" \
    -selections "100 ohm Pt" "120 ohm Ni" "10 ohm Cu" \
    -d off -r off -s off \
    -readProc cn606_model_r -writeProc {cn606_M_set sens_type}

CAMP_SELECT /~/setup/temp_unit -D -R -S -T "Temperature unit" \
    -selections C F \
    -H "Temperature units on instrument panel display" \
    -d on -r on -s on \
    -readProc cn606_model_r -writeProc {cn606_M_set temp_unit}

CAMP_SELECT /~/setup/alarm_type -D -R -S -T "Alarm type" \
    -selections "Overtemp" "Undertemp" "Over/Under" \
    -d on -r on -s on \
    -readProc cn606_model_r -writeProc {cn606_M_set alarm_type}

CAMP_SELECT /~/setup/relay_latch -D -R -S -T "Alarm relay latching" \
    -selections "Latching" "Nonlatching" \
    -d on -r on -s on \
    -readProc cn606_model_r -writeProc {cn606_M_set relay_latch}

CAMP_INT /~/setup/scan_time -D -R -S -T "Scan Time" \
    -H "Time (seconds) that each channel is displayed on instrument front panel" \
    -d on -r on -s on -units s \
    -readProc cn606_inp_alm_r -writeProc cn606_scan_time_w

proc cn606_scan_time_w { ins time } {
    if { $time < 1 || $time > 40 } {
        return -code error "Scan time out of range (1-40 sec)"
    }
    set buf [cn606_read $ins A 15]
    cn606_check_sum $buf 14
    if { [scan $buf {%2x%2x%2x%2x%2x%2x%2d} enab e2 hi h2 lo l2 scan] != 7 } {
        return -code error "Failed scan $buf"
    }
    set sn [varGetVal /${ins}/setup/serial_num]
    # Requires "0" for check-sum character, always.
    set buf [format "L%1de%2.2X00%2.2d0" $sn $enab $time]
    insIfWrite /${ins} $buf
    varDoSet /${ins}/setup/scan_time -v $time
}

CAMP_INT /~/setup/serial_num -D -S -T "Serial num" -d on -s on -v 1 \
	-H "Serial number identifier for multiple devices" \
	-writeProc cn606_serial_w

proc cn606_serial_w { ins val } {
    if { $val < 0 || $val > 9 } {
        return -code error "Serial number out of range"
    }
    varDoSet /${ins}/setup/serial_num -v $val
}


#CAMP_INT /~/setup/num_input -D -T "Number of inputs???" -d on

#CAMP_INT /~/setup/idnum -D -T "ID Num???" -d on

#CAMP_INT /~/setup/passwd -D -T "Password???" -d on

proc cn606_read { ins cmd bytes } {
    set sn [varGetVal /${ins}/setup/serial_num]
    insIfRead /${ins} "L${sn}${cmd}" $bytes
}

proc cn606_write { ins cmd string } {
    set sn [varGetVal /${ins}/setup/serial_num]
    set csum [cn606_check_sum $string 0]
    insIfWrite /${ins} "L${sn}${cmd}${string}${csum}"
}

# Procedure to verify or calculate the checksum byte.
# If $index is given as non-zero, use it to pull out the
# byte at that index and verify the string, returning error 
# maybe.  If $index is zero, calculate the checksum char
# for the string and return it.  Remember that the index of
# the last character is one less than the string length!
# This routine is generally not used because the unit
# does not calculate proper checksums!
proc cn606_check_sum { str index } {
    if { $index } {
        scan [string range $str $index $index] %c csb
        set str [string range $str 0 [expr {$index-1}]]
    }
    set sum 0
    foreach chr [split $str {}] {
	scan $chr "%c" i
	incr sum $i
    }
    set ss $sum
    while { $sum >= 256 } {
	set sum [expr {($sum/256)+($sum%256)}]
    }
    if { $index } {
        if { $csb != $sum } {
            return -code error "Bad checksum"
        }
    }
    return [format %c $sum]
}

