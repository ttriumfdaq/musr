# camp_ins_omega_fma1428.tcl
# 
# This is the tcl script camp device driver for the Omega FMA1400 Mass Flow controller.
# This driver is basically just a combination of a tip850 ADC and DAC, with some specific
# calibration.

CAMP_INSTRUMENT /~ -D -T "Omega FMA1428 Mass Flow" \
    -H "Omega FMA 1428 Mass Flow Controller" -d on \
    -initProc Ofma_init -deleteProc Ofma_delete \
    -onlineProc Ofma_online -offlineProc Ofma_offline

  proc Ofma_init { ins } {
      insSet /${ins} -if none 0.0 0.0
      varDoSet /${ins}/setup/IPPos -v 3
      insIfOn /${ins}
  }
  proc Ofma_delete { ins } {
      insSet /${ins} -line off
  }
  proc Ofma_online { ins } {
      insIfOn /${ins}
      varSet /${ins}/setup/adc_gain -v 0
  }
  proc Ofma_offline { ins } {
      insIfOff /${ins}
  }

  CAMP_FLOAT /~/read_flow -D -R -P -L -A -T "Read Flow" \
	-d on -r on -p off -p_int 18 \
	-readProc  Ofma_read_flow_r

    proc Ofma_read_flow_r { ins } {
	set pos [varGetVal /${ins}/setup/IPPos]
	set chan [varGetVal /${ins}/setup/dac_adc_chan]
	if { $chan < 0 } {
	    return -code error "Select a channel first (setup/dac_adc_chan)"
	}
	set full [varGetVal /${ins}/setup/full_scale]
	set offset [varGetVal /${ins}/setup/offset]
	set maxi [lindex {1024.0 2048.0 4096.0 8192.0} \
		[varGetVal /${ins}/setup/adc_gain] ]
	insIfRead /${ins} "AdcRead $pos $chan adc_read" 10
	set value [format %.3f [ expr 0.0 + $adc_read * ( $full / $maxi ) - $offset ] ]
	varDoSet /${ins}/read_flow -v $value
        varTestAlert /${ins}/read_flow [varGetVal /${ins}/set_flow]
    }

  CAMP_FLOAT /~/set_flow -D -S -L -T "Set Flow" \
	-d on -s on \
        -writeProc Ofma_set_flow_w

    proc Ofma_set_flow_w { ins target } {
	set full [varGetVal /${ins}/setup/full_scale]
	if { $full < 1.0 } { set full 1.0 }
	set dac_set [expr round( 0.0 + $target * ( 1024.0 / $full ) ) ]
	if { ( $dac_set < 0 ) || ( $dac_set > 1024 ) } {
	    return -code error "Setting out of range 0 to $full "
	}
	set pos [varGetVal /${ins}/setup/IPPos]
	set chan [varGetVal /${ins}/setup/dac_adc_chan]
	if { $chan < 0 } {
	    return -code error "Select a channel!"
	}
	insIfWrite /${ins} "DacSet $pos $chan dac_set"
	varDoSet /${ins}/set_flow -v $target
}

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on 

  CAMP_SELECT /~/setup/IPPos -D -S -T "IP Position" \
	-d on -s off -v 3 -selections 0 1 2 3 \
	-H "Industry Pack slot occupied by ADC/DAC board" \
	-writeProc Ofma_IPPos_w

    proc Ofma_IPPos_w { ins target } {
        varDoSet /${ins}/setup/IPPos -v $target
    }

# This is tricky.  Because the interface goes "online" immediately, perhaps
# with the wrong channel number, we do not apply the adc_gain parameter
# at startup, but only when the channel is selected.  The readback checks
# for an invalid channel number before trying to read.  When the server
# is restarted, the channel is "polled" to apply the restored flow setting.
#
  CAMP_INT /~/setup/dac_adc_chan -D -S -R -P -T "DAC \& ADC Channel" \
        -d on -s on -r off -p on -p_int 3 -v "-1" \
	-H "DAC and ADC channel number (use the same for both)" \
        -readProc Ofma_channel_r -writeProc Ofma_channel_w

    proc Ofma_channel_r { ins } {
	set c [varGetVal /${ins}/setup/dac_adc_chan]
	if { $c >= 0 } {
	    varSet /${ins}/setup/dac_adc_chan -p off -v $c
	}
    }
    proc Ofma_channel_w { ins target } {
	if { ( $target < 0 ) || ( $target > 3 ) } {
	    return -code error "Channel number out of range: 0 to 3"
	}
        varDoSet /${ins}/setup/dac_adc_chan -v $target
	varSet /${ins}/setup/adc_gain -v [ varGetVal /${ins}/setup/adc_gain ]
	varSet /${ins}/set_flow -v [ varGetVal /${ins}/set_flow ]
        varDoSet /${ins}/read_flow -p on
    }

  CAMP_FLOAT /~/setup/full_scale -D -S -T "Full Scale" \
	-d on -s on -v 100.0 \
	-H "Calibration: full scale corresponds to this flow rate" \
	-writeProc Ofma_full_scale_w

    proc Ofma_full_scale_w { ins target } {
	varDoSet /${ins}/setup/full_scale -v $target
    }

  CAMP_FLOAT /~/setup/offset -D -S -T "Readback Offset" \
	-d on -s on -v 0.0 \
	-H "Set this if you need to correct for a non-zeroed readback" \
	-writeProc Ofma_offset_w

    proc Ofma_offset_w { ins target } {
	varDoSet /${ins}/setup/offset -v $target
    }

  CAMP_SELECT /~/setup/adc_gain -D -S -T "ADC Gain" \
	-d on -s on -v 0 -selections x1 x2 x4 x8 \
	-writeProc Ofma_adc_gain_w

    proc Ofma_adc_gain_w { ins target } {
	set gain $target
	set pos [varGetVal /${ins}/setup/IPPos]
	set chan [varGetVal /${ins}/setup/dac_adc_chan]
	insIfWrite /${ins} "GainSet $pos $chan gain"
	varDoSet /${ins}/setup/adc_gain -v $gain
    }
