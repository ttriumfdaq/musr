# camp_ins_omega_fvl2600.tcl
# 
# $Log: camp_ins_omega_fvl2600.tcl,v $
# Revision 1.4  2015/03/15 00:40:39  suz
# Ted's 201404 interface changes
#
# Revision 1.3  2004/12/18 03:31:49  asnd
# Merely fix previous log message
#
# Revision 1.2  2004/10/02 06:24:04  asnd
# Retries for bad serial connections (due to rate being so high)
#
# Revision 1.1  2003/06/25 07:30:37  asnd
# Initial version
#
#

CAMP_INSTRUMENT /~ -D -T "Omega FVL2600 Flow" \
    -H "Omega FVL 2600 Series Flow Controller" -d on \
    -initProc Ofvl_init -deleteProc Ofvl_delete \
    -onlineProc Ofvl_online -offlineProc Ofvl_offline


  proc Ofvl_init { ins } {
    insSet /${ins} -if rs232 0.1 2 /tyCo/1 19200 8 none 1 CR CR
  }
  proc Ofvl_delete { ins } { insSet /${ins} -line off }
  proc Ofvl_online { ins } { 
    insIfOn /${ins} 
    insIfWrite /${ins} "*@=[varSelGetValLabel /${ins}/setup/ident]"
    if { [catch {varRead /${ins}/read_flow}] } {
      insIfOff /${ins}
      return -code error "failed test read; check interface definition and connections, and set IO to serial"
    }
    if { [varGetPollInterval /${ins}/read_flow] > 0.0 } {
	varDoSet /${ins}/read_flow -p on
    }
    catch { varRead /${ins}/setup/full_scale }
  }
  proc Ofvl_offline { ins } { insIfOff /${ins} }

  CAMP_FLOAT /~/read_flow -D -R -P -L -A -T "Read Flow" \
	-d on -r on -p off -p_int 12 -tol 0.03 -units "LPM" \
	-readProc  Ofvl_read

    proc Ofvl_read { ins } {
        set id [varSelGetValLabel /${ins}/setup/ident]
	for {set i 0} {$i<8} {incr i} {
	    set buf [insIfRead /$ins "${id}" 50]
	    if { [scan " $buf" " %s %f %f %s" c r s g] == 4 && $c == $id && \
		    [lsearch -exact [list Air Argon Methane CO CO2 Ethane \
		    H2 Helium N2 N2O Neon Oxygen Propane Butane ] $g] >= 0 } {
		break
	    }
	}
	if { $i >= 5 } {
	    return -code error "Failed to get readings: $buf"
	}
        if { $c != $id } {
	    return -code error "Wrong identity ${c} vs ${id}"
	}
        varDoSet /${ins}/read_flow -v $r -m "$i failed tries to read"
        varDoSet /${ins}/set_flow -v $s
        varDoSet /${ins}/setup/gas -v $g
        varTestAlert /${ins}/read_flow $s
    }

  CAMP_FLOAT /~/set_flow -D -S -R -L -T "Set Flow" \
	-d on -s on -r on -units "LPM" \
        -readProc Ofvl_read -writeProc Ofvl_set_flow_w

    proc Ofvl_set_flow_w { ins target } {
	set full [varGetVal /${ins}/setup/full_scale]
        set id [varSelGetValLabel /${ins}/setup/ident]
        if { $target > 1.01*$full } {
            return -code error "Setpoint out of range: 0 to $full (see /${ins}/setup/full_scale)"
        }
        set setpoint [expr { round( $target * 64000 / $full ) }]
        insIfWrite /$ins "${id}${setpoint}"
        Ofvl_read $ins
        if { abs( [varGetVal /${ins}/set_flow] - $target) > $full / 100.0 } {
              return -code error "Failed to set desired flow rate"
        }
    }


CAMP_STRUCT /~/setup -D -T "Setup variables" -d on 

  CAMP_SELECT /~/setup/ident -D -S -T "Identification code" -d on -s on \
          -v 0 -selections A B C D E \
          -writeProc Ofvl_ident_w -readProc Ofvl_ident_r

#     Setting ident does not set the identity in the instrument!  See page 23
#     of the FVL 2600 user guide for the explanation.
      proc Ofvl_ident_w { ins target } {
          varDoSet /${ins}/setup/ident -v $target
      }


  CAMP_SELECT /~/setup/gas -D -R -S -T "Gas type" -d on -r on -s on \
         -selections Air Argon Methane CO CO2 Ethane \
                 H2 Helium N2 N2O Neon Oxygen Propane Butane \
         -readProc Ofvl_read -writeProc Ofvl_gas_w
      proc Ofvl_gas_w { ins target } {
          set id [varSelGetValLabel /${ins}/setup/ident]
          insIfWrite /$ins "${id}\$\$${target}"
          Ofvl_read $ins
          if { [varGetVal /${ins}/setup/gas] != $target } {
              return -code error "Failed to set gas type"
          }
      }

  CAMP_FLOAT /~/setup/full_scale -D -S -R -T "Full scale" \
          -d on -s on -r on -units "LPM" -v 1.00 \
          -writeProc Ofvl_full_scale_w -readProc Ofvl_full_scale_r
      proc Ofvl_full_scale_w { ins target } {
          varDoSet /${ins}/setup/full_scale -v $target
      }
      proc Ofvl_full_scale_r { ins } {
          set id [varSelGetValLabel /${ins}/setup/ident]
	  varRead /${ins}/set_flow
	  set sp [varGetVal /${ins}/set_flow]
	  # Set to 10% of full scale momentarily, and read back setpoint.
	  insIfWrite /$ins "${id}6400"
	  set buf [insIfRead /$ins "${id}" 40]
	  scan " $buf" " %s %f %f %s" c r s g
	  varDoSet /${ins}/setup/full_scale -v [expr $s*10.0]
	  # restore setpoint
	  varSet /${ins}/set_flow -v $sp
      }
