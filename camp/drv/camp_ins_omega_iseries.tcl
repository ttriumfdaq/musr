# camp_ins_omega_iseries.tcl
#
# Camp instrument driver for Omega iSeries units, covering both the
# temperature and the "strain" (or "process" only) type devices.  Which
# device this is is indicated by variable ~/setup/comm/model.  There
# are many different device options, with no way for the driver to
# know which options are installed; it is up to the operator to make
# appropriate settings.  There are two outputs: number 1 may be analog
# for PID or manual control, or a relay for an alarm or on/off control;
# number 2 must be a relay for an alarm or on/off control.  The user
# must choose output functions appropriate to the individual unit.
# An additional output function "none" is provided by the driver for
# simplicity when reading only.  There are separate setpoints for each
# output channel, but this driver unifies them, forcing them to the same
# value, for simplicity and convenience.
#
# Given the diversity of models and functions, there are a great many
# Camp variables, but inappropriate ones are generally hidden in the Camp
# Cui display.
#
# Several instrument parameters exist in both volatile and non-volatile
# memory in the unit. This driver sets both when possible to the same
# values.  Other parameters can only have their non-volatile instance set
# directly, and we must reset the instrument to copy them into volatile
# memory.  Such resets are performed after a short delay so a single reset
# can take care of several parameter settings (see ~/setup/comm/reset).
#
# Tcpip communications have been found to work poorly, so there are
# retries coded in the driver.
#
# $Log: camp_ins_omega_iseries.tcl,v $
# Revision 1.12  2015/04/25 00:10:24  asnd
# More variety detecting COMM ID num (or none)
#
# Revision 1.11  2015/03/15 00:39:55  suz
# Ted's 201404 interface changes
#
# Revision 1.8  2009/10/11 11:13:01  asnd
# Use reset to apply alarm limits (error in communications manual and even in local operation)
#
# Revision 1.7  2009/09/30 07:35:34  asnd
# Bug (from manual) in setpoint encoding fixed;
# Avoid some RESETs, but confirm RESET has been taken
#
# Revision 1.6  2009/08/29 05:37:04  asnd
# Bugfix (high/low limit procs)
#
# Revision 1.5  2008/04/02 00:44:52  asnd
# Rewritten driver for both temperature and process, and allowing for bad tcpip.
#
# Two previous major versions.
#

CAMP_INSTRUMENT /~ -D -T "Omega iSeries" -d on \
	-H "Omega iSeries Monitor/Controller" \
	-initProc ois_init \
	-deleteProc ois_delete \
	-onlineProc ois_online \
	-offlineProc ois_offline

proc ois_init { ins } {
    insSet /${ins} -if tcpip 0.15 1 142.90.154.122 1000 CR CR
    #insSet /${ins} -if rs232 0.25 2 /tyCo/2 9600 7 odd 1 CR CR
}
proc ois_delete { ins } { insSet /${ins} -line off }
proc ois_online { ins } {
    insIfOn /${ins}
    set retr [varGetVal /${ins}/setup/comm/retries]
    # Choose address code based on interface.
    set iftype [insGetIfTypeIdent /$ins]
    switch -glob $iftype {
	rs232* {
	    varDoSet /${ins}/setup/comm/addr -v ""
	}
	tcpip* { # use many retries when attempting to go online.
	    varDoSet /${ins}/setup/comm/retries -v 10
	    if { $retr < 7 } { set retr 10 }
	}
    }
    # set BUSFORMAT = 18 (no echo)  ... this is not needed for every attempt to go online
    # insIfRead /$ins "*01W1F18" 16
    if { [catch {
	varSet /${ins}/setup/comm/initialize -v 1
    } msg ] } {
	varDoSet /${ins}/setup/comm/retries -v $retr
	insIfOff /${ins}
	return -code error "Check configuration and interface settings."
    }
    varDoSet /${ins}/setup/comm/retries -v $retr
    # More initialization done asynchronously
    varDoSet /${ins}/setup/comm/initialize -p on -p_int 3
}
proc ois_offline { ins } { insIfOff /${ins} }

CAMP_FLOAT /~/reading -D -R -P -L -A -T "Reading" \
	-d on -r on -p off -p_int 15 -tol 1.0 -units "K"  \
	-readProc ois_reading_r

proc ois_reading_r { ins } {
    set adr [varGetVal /${ins}/setup/comm/addr]
    set max [varGetVal /${ins}/setup/comm/retries]
    for { set retries 0 } { $retries < $max } { incr retries } {
	set resp ""
	catch { set resp [insIfRead /$ins "*${adr}X01" 32] } msg
	if { [regexp "X01(.*)" $resp mat resp] } { break }
    }
    if { [regexp {\?\d+} $resp code] } {
	return -code error "Read failure: Signal error (code ${code})"
    }
    if { [scan $resp "%f %c" C x] != 1 } {
	return -code error "Failed to read value after $max tries.  Got '$resp'. $msg"
    }
    if { [varGetVal /${ins}/setup/input/input_type] == 2 } {
	set u [varGetVal /${ins}/setup/calibration/units]
	if { [varGetVal /${ins}/setup/calibration/segments] == 0 } {
	    set m "Raw"
	} else {
	    set m "Calibrated"
	}
	varDoSet /${ins}/reading -v $C -units $u -m "$m reading"
    } else {
	varDoSet /${ins}/reading -v [expr { $C + 273.2 }] -units K \
	-m "Temperature reading $C C"
    }
    varTestAlert /${ins}/reading [varGetVal /${ins}/setpoint]
}

CAMP_FLOAT /~/setpoint -D -S -R -P -L -T "setpoint" \
	-d on -s on -r on -p off -p_int 30 -units K \
	-H "Setpoint for both output channels (control and alarms)" \
	-readProc ois_setpoint_r -writeProc ois_setpoint_w

# Read number as string, initially. Decrypt value. If temperature, convert to kelvin.
proc ois_setpoint_r { ins } {
    set resp [ois_get $ins "R01" "R01%s"]
    set C [ois_decrypt_setpoint $resp]
    if { [varGetVal /${ins}/setup/input/input_type] == 2 } {
	set u [varGetVal /${ins}/setup/calibration/units]
	varDoSet /${ins}/setpoint -v $C -units $u
    } else {
	varDoSet /${ins}/setpoint -v [expr { $C + 273.2 }] -units K
    }
}

# Write setpoint, in its convoluted hex form, into both permanent and
# volatile storage for both setpoint1 and setpoint2 (and we don't need
# to apply the setting via reset).
# How does setpoint scale with calibration?
proc ois_setpoint_w { ins setp } {
    if { [varGetVal /${ins}/setup/input/input_type] == 2 } {
	set u [varGetVal /${ins}/setup/calibration/units]
	set dp [varGetVal /${ins}/setup/calibration/decimals]
	set max [lindex {9999 999.9 99.99 9.999} $dp]
	set min -$max
	set setp [format "%.*f" $dp $setp]
	set dat [ois_encrypt_setpoint $setp $dp]
    } else {
	set u "K"
	set max 1270
	set min 0
	set setp [format "%.1f" $setp]
	set dat [ois_encrypt_setpoint [expr {$setp - 273.2}] 1 ]
    }
    if { ($setp < $min || $setp > $max ) } {
	return -code error "Setpoint must be in range $min to $max"
    }
    #ois_set $ins "D03" "D03"
    ois_set $ins "W01$dat" "W01"
    ois_set $ins "W02$dat" "W02"
    varDoSet /${ins}/setpoint -v $setp -units $u
    #ois_set $ins "E03" "E03"
}

# Parameter values for setpoint (and others) are packed strangely into
# a few bytes. They need to be "encrypted" and "decrypted" for communications.
# Note ERROR on page 18 of the communications manual: Decimal format F.FFF
# is indicated by binary 100 (4) not 101 (5).
proc ois_encrypt_setpoint { input decimals } {
    set input [format "%.*f" $decimals $input]
    set val [expr { round(abs($input * pow(10.0,$decimals))) }]
    while { $val > 0xfffff } {
	set val [expr {$val/10}]
	incr decimals -1
    }
    set sps [expr { (($input < 0.0) ? 0x08 : 0x00) | ($decimals+1) }]
    return [format "%1X%05X" $sps $val]
}

proc ois_decrypt_setpoint { input } {
    if { [scan $input "%1x%x %c" i d c] != 2 } {
	return -code error "Invalid hex-encoded number: $input"
    }
    set sign [expr { ($i & 0x08) ? -1.0 : 1.0 }]
    set div [lindex {1. 1. 10. 100. 1000. 1000. 1. 1.} [expr {$i & 0x07}]]
    return [expr { $sign * double( $d ) / $div }]
}


CAMP_SELECT /~/alarm_1 -D -R -P -A -T "Alarm 1 state" \
	-d on -r on -p off -p_int 15 -a off \
	-H "Alarm 1 state, if not analog output 1 and not controlling on 1" \
	-selections ok BAD \
	-v 0 \
	-readProc ois_alarms_r

CAMP_SELECT /~/alarm_2 -D -R -P -A -T "Alarm 2 state" \
	-d on -r on -p off -p_int 25 -a off \
	-H "Alarm 2 state, if not controlling with output 2" \
	-selections ok BAD \
	-v 0 \
	-readProc ois_alarms_r

proc ois_alarms_r { ins } {
    set resp [ois_get $ins "U01" "U01 %s"]
    set bits [string first $resp "@ABCD"]
    varDoSet /${ins}/alarm_1 -v [expr {$bits & 1}]
    varTestAlert /${ins}/alarm_1 0
    varDoSet /${ins}/alarm_2 -v [expr {$bits/2}]
    varTestAlert /${ins}/alarm_2 0
}


CAMP_STRUCT /~/setup -D -T "Configuration settings" -d on

# Select what each output channel is used for.

CAMP_SELECT /~/setup/out1_func -D -S -R -T "Output 1 function" \
	-d on -s on -r on \
	-H "Function of output 1.  Be careful to choose one that is installed!" \
	-selections Control Manual Alarm None \
	-v 0 \
	-readProc ois_out1func_r -writeProc ois_out1func_w

CAMP_SELECT /~/setup/out2_func -D -S -R -T "Output 2 function" \
	-d on -s on -r on \
	-H "Function of output 2." \
	-selections Control Alarm None \
	-v 1 \
	-readProc ois_out2func_r -writeProc ois_out2func_w

# Output function.  A setting of "None" is provided here, although that is not
# available on the device, which is mainly intended to clean up the Camp interface
# for "read-only" use.  We actually leave alarms enabled for "None".
# Perhaps I should set cmd 09 bit 7 (0x80) to 0 *only* when both outputs are "None",
# and set it to 1 otherwise.  (That bit controls both alarm channels, and it
# refers also to the RESET function (ois_resume), not just power-on.)
proc ois_out1func_w { ins val } {
    set alarm_en [expr {$val==2 ? 1 : 0}]
    set alarm_cmd [expr {$alarm_en ? "E" : "D"}]
    set self_en [expr {$val==1 ? 1 : 0}]
    set self_cmd [expr {$self_en ? "E" : "D"}]
    set control_en [expr {$val==0 ? 1 : 0}]
    ois_set $ins "${alarm_cmd}01" "${alarm_cmd}01"
    ois_set $ins "${self_cmd}04" "${self_cmd}04"
    ois_write_bits $ins "09" [expr {$val>=2}] 1 1
    varDoSet /${ins}/setup/out1_func -v $val
    varDoSet /${ins}/setup/control_1 -d [lindex {off on} $control_en]
    ois_aldisp $ins
    if { $val == 3 } {
	varRead /${ins}/setup/alarm_1/norm_color
	varSet /${ins}/setup/alarm_1/trip_color -v [varGetVal /${ins}/setup/alarm_1/norm_color]
    }
}

# When we read the function and see alarms enabled, maybe that means "none".
# I see no way to disambiguate alarms from manual (self).
proc ois_out1func_r { ins } {
    set prev [varGetVal /${ins}/setup/out1_func]
    set alarm_en [ois_read_bits $ins "09" 1 1]
    if { $alarm_en } {
	varDoSet /${ins}/setup/out1_func -v [expr { $prev==3 ? 3 : 2 }]
	varDoSet /${ins}/setup/control_1 -d off
    } else {
	varDoSet /${ins}/setup/out1_func -v 0
	varDoSet /${ins}/setup/control_1 -d on
    }
    ois_aldisp $ins
}

proc ois_out2func_w { ins val } {
    set alarm_en [expr {$val==1 ? 1 : 0}]
    set alarm_cmd [expr {$alarm_en ? "E" : "D"}]
    set control_en [expr {$val==0 ? 1 : 0}]
    ois_set $ins "${alarm_cmd}02" "${alarm_cmd}02"
    ois_write_bits $ins "0A" [expr {$val >= 1}] 1 1
    varDoSet /${ins}/setup/out2_func -v $val
    varDoSet /${ins}/setup/control_2 -d [lindex {off on} $control_en]
    ois_aldisp $ins
    if { $val == 2 } {
	varRead /${ins}/setup/alarm_2/norm_color
	varSet /${ins}/setup/alarm_2/trip_color -v [varGetVal /${ins}/setup/alarm_2/norm_color]
    }
}

proc ois_out2func_r { ins } {
    set prev [varGetVal /${ins}/setup/out2_func]
    set alarm_en [ois_read_bits $ins "0A" 1 1]
    if { $alarm_en } {
	varDoSet /${ins}/setup/out2_func -v [expr { $prev==2 ? 2 : 1 }]
	varDoSet /${ins}/setup/control_2 -d off
    } else {
	varDoSet /${ins}/setup/out2_func -v 0
	varDoSet /${ins}/setup/control_2 -d on
    }
    ois_aldisp $ins
}

# Control hiding of variables based on output function.
# Hide alarm only for "None" (alarm may still function as a warning
# during control).  Hide setpoint if both outputs are "None".
# When setpoint is hidden, then no alarm on reading.
proc ois_aldisp { ins } {
    set f1 [varGetVal /${ins}/setup/out1_func]
    set f2 [varGetVal /${ins}/setup/out2_func]
    set ad [lindex {on on on off} $f1]
    set ap [lindex {off off on off} $f1]
    varDoSet /${ins}/alarm_1 -d $ad -r $ad -p $ap -alert off
    set ad [lindex {on on off} $f2]
    set ap [lindex {off on off} $f2]
    varDoSet /${ins}/alarm_2 -d $ad -r $ad -p $ap -alert off
    if { $f1 == 3 && $f2 == 2 } {
	varDoSet /${ins}/setpoint -d off -r off -p off
	varDoSet /${ins}/reading -a off -alert off
    } else {
	varDoSet /${ins}/setpoint -d on -r on -p on
    }
}

CAMP_STRUCT /~/setup/control_1 -D -T "Out-1 control settings" -d on \
	-H "Settings for process control on output 1"

CAMP_SELECT /~/setup/control_1/autotune -D -S -R -T "Autotuning PID" \
	-d on -s on -r on \
	-selections STOP START \
	-readProc ois_out1_r -writeProc {ois_out1_w 32}

CAMP_SELECT /~/setup/control_1/autoPID -D -S -R -T "Auto PID" \
	-d on -s on -r on \
	-selections DISABLE ENABLE \
	-readProc ois_out1_r -writeProc ois_autopid_w

CAMP_SELECT /~/setup/control_1/antiwindup -D -S -R -T "Anti-Wind-up" \
	-d on -s on -r on \
	-selections DISABLE ENABLE \
	-readProc ois_out1_r -writeProc {ois_out1_w 16}

CAMP_SELECT /~/setup/control_1/action -D -S -R -T "Action Direction" \
	-H "Output action direction: Reverse means heating, Direct means cooling." \
	-d on -s on -r on \
	-selections REVERSE DIRECT \
	-readProc ois_out1_r -writeProc {ois_out1_w 2}

# I would like to combine the next two, or enable only the appropriate one,
# but there is no way to detect whether the analog option is installed!

CAMP_SELECT /~/setup/control_1/analog_range -D -S -R -T "Analog range" \
	-d on -s on -r on \
	-H "Selects analog output range.  Only functional with analog option installed." \
	-selections {0-20mA, 0-10V} {4-20mA, 2-10V} \
	-readProc ois_out1_r -writeProc {ois_out1_w 64}

CAMP_SELECT /~/setup/control_1/relay_out -D -S -R -T "Relay 1 method" \
	-d on -s on -r on \
	-H "Selects type of relay control output: either on/off like a thermostat or time-proportional.  Not functional when analog option installed." \
	-selections {On/Off} {Time-prop} \
	-readProc ois_out1_r -writeProc {ois_out1_w 1}

# read output1 config
# read whole status byte, apply each bit to individual variables
proc ois_out1_r { ins } {
    set flags [ois_read_num $ins "R0C"]
    varDoSet /${ins}/setup/control_1/autotune -v [expr { ($flags & 32) ? 1 : 0 }]
    varDoSet /${ins}/setup/control_1/autoPID  -v [expr { ($flags & 4) ? 1 : 0 }]
    varDoSet /${ins}/setup/control_1/antiwindup -v [expr { ($flags & 16) ? 1 : 0 }]
    varDoSet /${ins}/setup/control_1/action -v [expr { ($flags & 2) ? 1 : 0 }]
    varDoSet /${ins}/setup/control_1/analog_range -v [expr { ($flags & 64) ? 1 : 0 }]
    varDoSet /${ins}/setup/control_1/relay_out -v [expr { ($flags & 1) ? 1 : 0 }]
}

# set output1 config
proc ois_out1_w { bit ins val } {
    ois_write_bits $ins "0C" $val $bit $bit
    ois_out1_r $ins
}

# If turn off auto-pid, then also turn off any autotune in progress
proc ois_autopid_w { ins val } {
    ois_out1_w 4 $ins $val
    if { $val == 0 } { ois_out1_w 32 $ins $val }
}

CAMP_INT /~/setup/control_1/cycle_time -D -S -R -T "Cycle time" \
	-H "Cycle time (seconds) for relay operation in time-proportional mode (1-199)." \
	-units sec -d on -s on -r on \
	-readProc {ois_setup_int_r control_1/cycle_time 1A} \
	-writeProc {ois_setup_int_w control_1/cycle_time 1A 2 1 199}

CAMP_FLOAT /~/setup/control_1/P_band -D -S -R -T "Proportional band" \
	-H "Proportional band (see units) for P term (higher values give slower response); also the dead-band for simple on/off control." \
	-units counts -d on -s on -r on \
	-readProc {ois_P_band_r 1 17} -writeProc {ois_P_band_w 1 17}

CAMP_INT /~/setup/control_1/I_reset -D -S -R -T "Reset seconds" \
	-H "Reset time, in seconds, for I term. Shared with channel 2. 0-3999" \
	-units sec -d on -s on -r on \
	-readProc {ois_I_D_shared_r I_reset 18} \
	-writeProc {ois_I_D_shared_w I_reset 18}

CAMP_INT /~/setup/control_1/D_rate -D -S -R -T "Proportional band" \
	-H "Rate parameter for D term, in seconds. Shared with channel 2. 0-3999" \
	-units sec -d on -s on -r on \
	-readProc {ois_I_D_shared_r D_rate 19} \
	-writeProc {ois_I_D_shared_w D_rate 19}

CAMP_INT /~/setup/control_1/highest_out -D -S -R -T "High output limit" \
	-units % -d on -s on -r on \
	-readProc {ois_setup_int_r control_1/highest_out 28} \
	-writeProc ois_highest_out_w

CAMP_INT /~/setup/control_1/lowest_out -D -S -R -T "Low output limit" \
	-units % -d on -s on -r on \
	-readProc {ois_setup_int_r control_1/lowest_out 27} \
	-writeProc ois_lowest_out_w

CAMP_SELECT /~/setup/control_1/damping -D -S -R -T "Damping factor" \
	-d on -s on -r on \
	-selections 0 1 2 3 4 5 6 7 \
	-readProc ois_damping_r -writeProc ois_damping_w

proc ois_setup_int_r { var idx ins } {
    varDoSet /${ins}/setup/$var -v [ois_read_num $ins "R${idx}"]
}

proc ois_setup_int_w { var idx nib lo hi ins val } {
    if { $val > $hi || $val < $lo } {
	return -code error "Value must be in range ${lo}-${hi}"
    }
    set req "W${idx}"
    ois_write_num $ins $req $nib $val
    varDoSet /${ins}/setup/$var -v $val
}

proc ois_highest_out_w { ins v } {
    varRead /${ins}/setup/control_1/lowest_out
    if { $v > 99 || $v < 0 || $v < [varGetVal /${ins}/setup/control_1/lowest_out] } {
	return -code error "Value must be in range 0-99, and above low limit"
    }
    ois_write_num $ins "W28" 2 $v
    varDoSet /${ins}/setup/control_1/highest_out -v $v
}

proc ois_lowest_out_w { ins v } {
    varRead /${ins}/setup/control_1/highest_out
    if { $v > 98 || $v < 0 || $v > [varGetVal /${ins}/setup/control_1/highest_out] } {
	return -code error "Value must be in range 0-98, and below high limit"
    }
    ois_write_num $ins "W27" 2 $v
    varDoSet /${ins}/setup/control_1/lowest_out -v $v
}

# Proportional band has a decimal-point on the unit, but is handled as
# an integer for remote control.  So convert to float for the user.

proc ois_P_band_r { ch cmd ins } {
    set decim [ois_read_bits $ins "08" 7 1]
    set factor [lindex {1. 1. 10. 100. 1000. 1000. 1000. 1000.} $decim]
    set counts [ois_read_num $ins "R${cmd}"]
    set val [format %.4f [expr {$counts/$factor}]]
    set unit [varNumGetUnits /${ins}/setpoint]
    varDoSet /${ins}/setup/control_${ch}/P_band -v $val -units $unit
}

proc ois_P_band_w { ch cmd ins val } {
    set decim [ois_read_bits $ins "08" 7 1]
    set factor [lindex {1. 1. 10. 100. 1000. 1000. 1000. 1000.} $decim]
    set counts [expr {round($val*$factor)}]
    if { $counts > 9999 || $counts < 0 } {
	return -code error "Value must be in range 0-[expr {9999./$factor}]"
    }
    ois_write_num $ins "W${cmd}" 4 $counts
    set unit [varNumGetUnits /${ins}/setpoint]
    varDoSet /${ins}/setup/control_${ch}/P_band -v $val -units $unit
}

proc ois_I_D_shared_r { var cmd ins } {
    ois_setup_int_r control_1/$var $cmd $ins
    varDoSet /${ins}/setup/control_2/$var -v [varGetVal /${ins}/setup/control_1/$var]
}

proc ois_I_D_shared_w { var cmd ins val } {
    ois_setup_int_w control_1/$var $cmd 4 0 3999 $ins $val
    varDoSet /${ins}/setup/control_2/$var -v [varGetVal /${ins}/setup/control_1/$var]
}

CAMP_STRUCT /~/setup/control_2 -D -T "Out2 control settings" -d off \
	-H "Settings for process control on output 2"

CAMP_SELECT /~/setup/control_2/autoPID -D -S -R -T "Auto PID" \
	-d on -s on -r on \
	-selections DISABLE ENABLE \
	-H "When enabled, use all the control params from output 1; when disabled, use our own P-band, but copy others." \
	-readProc ois_out2_r -writeProc {ois_out2_w 4}

CAMP_SELECT /~/setup/control_2/action -D -S -R -T "Action Direction" \
	-H "Output action direction: Reverse means heating, Direct means cooling." \
	-d on -s on -r on \
	-selections REVERSE DIRECT \
	-readProc ois_out2_r -writeProc {ois_out2_w 2}

CAMP_SELECT /~/setup/control_2/relay_out -D -S -R -T "Relay 2 method" \
	-d on -s on -r on \
	-H "Selects type of relay control output, either on/off like a thermostat, or time-proportional." \
	-selections {On/Off} {Time-prop} \
	-readProc ois_out2_r -writeProc {ois_out2_w 1}

CAMP_INT /~/setup/control_2/cycle_time -D -S -R -T "Cycle time" \
	-H "Cycle time (seconds) for relay operation in time-proportional mode (1-199)." \
	-units sec -d on -s on -r on \
	-readProc {ois_setup_int_r control_2/cycle_time 1D} \
	-writeProc {ois_setup_int_w control_2/cycle_time 1D 2 1 199}

CAMP_FLOAT /~/setup/control_2/P_band -D -S -R -T "Proportional band" \
	-H "Proportional band (see units) for P term (higher values give slower response); also the dead-band for simple on/off control." \
	-units counts -d on -s on -r on \
	-readProc {ois_P_band_r 2 1C} -writeProc {ois_P_band_w 2 1C}

CAMP_INT /~/setup/control_2/I_reset -D -S -R -T "Reset seconds" \
	-H "Reset time, in seconds, for I term. Shared with channel 1. 0-3999" \
	-units sec -d on -s on -r on \
	-readProc {ois_I_D_shared_r I_reset 18} \
	-writeProc {ois_I_D_shared_w I_reset 18}

CAMP_INT /~/setup/control_2/D_rate -D -S -R -T "Proportional band" \
	-H "Rate parameter for D term, in seconds. Shared with channel 1. 0-3999" \
	-units sec -d on -s on -r on \
	-readProc {ois_I_D_shared_r D_rate 19} \
	-writeProc {ois_I_D_shared_w D_rate 19}

CAMP_SELECT /~/setup/control_2/damping -D -S -R -T "Damping factor" \
	-d on -s on -r on \
	-selections 0 1 2 3 4 5 6 7 \
	-readProc ois_damping_r -writeProc ois_damping_w

# read output2 config
# read whole status byte, apply each bit to individual variables
proc ois_out2_r { ins } {
    set flags [ois_read_num $ins "R0D"]
    varDoSet /${ins}/setup/control_2/autoPID  -v [expr { ($flags& 4) ? 1 : 0 }]
    varDoSet /${ins}/setup/control_2/action -v [expr { ($flags&2) ? 1 : 0 }]
    varDoSet /${ins}/setup/control_2/relay_out -v [expr { ($flags&1) ? 1 : 0 }]
}

proc ois_out2_w { bit ins val } {
    ois_write_bits $ins "0D" $val $bit $bit
    ois_out2_r $ins
}

proc ois_damping_r { ins } {
    set d [ois_read_bits $ins "0D" 0xe0 32]
    varDoSet /${ins}/setup/control_1/damping -v $d
    varDoSet /${ins}/setup/control_2/damping -v $d
}

proc ois_damping_w { ins val } {
    ois_write_bits $ins "0D" $val 0xe0 32
    varDoSet /${ins}/setup/control_1/damping -v $val
    varDoSet /${ins}/setup/control_2/damping -v $val
}

# ??? Should have immediate alarm response (power-on) either forced or under control.

CAMP_STRUCT /~/setup/alarm_1 -D -T "Alarm1 settings" -d on \
	-H "Settings for alarms on output 1, if not analog"

# The limits (absolute:deviation) and the trips should interact,
# but it is unclear how to interpret mismatched values.  So set
# them in concert, but read them independently.

CAMP_SELECT /~/setup/alarm_1/limits -D -S -R -T "A1 Limit types" \
	-d on -s on -r on -selections Absolute Deviation \
	-readProc {ois_al_cfg_r 1} -writeProc {ois_limtyp_w 1}

CAMP_SELECT /~/setup/alarm_1/trips_on -D -S -R -T "A1 Trips on" \
	-d on -s on -r on -selections High Low {Hi/Low} Band \
	-H "Define conditions for which alarm 1 will trip" \
	-readProc {ois_al_cfg_r 1} -writeProc {ois_tripson_w 1}

CAMP_SELECT /~/setup/alarm_1/latching -D -S -R -T "A1 Latching" \
	-d on -s on -r on -selections Unlatch Latch \
	-readProc {ois_al_cfg_r 1} -writeProc {ois_al_cfg_w 1 4 4}

CAMP_SELECT /~/setup/alarm_1/contacts -D -S -R -T "A1 Contacts" \
	-d on -s on -r on -selections Norm_open Norm_close \
	-H "Behavior of alarm 1 relay" \
	-readProc {ois_al_cfg_r 1} -writeProc {ois_al_cfg_w 1 8 8}

CAMP_SELECT /~/setup/alarm_1/norm_color -D -S -R -T "Normal Color" \
	-d on -s on -r on -selections Amber Green Red \
	-H "Display color when alarm not tripped" \
	-readProc ois_al_color_r -writeProc {ois_al_color_w 0}

CAMP_SELECT /~/setup/alarm_1/trip_color -D -S -R -T "A1 Trip Color" \
	-d on -s on -r on -selections Amber Green Red \
	-H "Display color when Alarm 1 tripped" \
	-readProc ois_al_color_r -writeProc {ois_al_color_w 1}

proc ois_al_cfg_r { ch ins } {
    set idx [lindex {"" "09" "0A"} $ch]
    set flags [ois_read_num $ins "R${idx}"]
    varDoSet /${ins}/setup/alarm_${ch}/limits -v [expr {($flags & 2)/2}]
    varDoSet /${ins}/setup/alarm_${ch}/latching -v [expr {($flags & 4)/4}]
    varDoSet /${ins}/setup/alarm_${ch}/contacts -v [expr {($flags & 8)/8}]
    varDoSet /${ins}/setup/alarm_${ch}/trips_on -v [expr {($flags & 48)/16}]
    if { ($flags & 48)/16 == 3 } {
	varDoSet /${ins}/setup/alarm_${ch}/band -d on -r on
	varDoSet /${ins}/setup/alarm_${ch}/low_lim -d off -r off
	varDoSet /${ins}/setup/alarm_${ch}/high_lim -d off -r off
    } else {
	varDoSet /${ins}/setup/alarm_${ch}/band -d off -r off
	varDoSet /${ins}/setup/alarm_${ch}/low_lim -d on -r on
	varDoSet /${ins}/setup/alarm_${ch}/high_lim -d on -r on
    }
}

proc ois_al_cfg_w { ch bits mult ins val } {
    set cmd [lindex {"" "09" "0A"} $ch]
    ois_write_bits $ins $cmd $val $bits $mult
    ois_al_cfg_r $ch $ins
}

# deviation vs absolute works with the trips_on selection
proc ois_limtyp_w { ch ins val } {
    ois_tripson_w $ch $ins [expr { ($val ? 3 : 2) }]
}

proc ois_tripson_w { ch ins item } {
    set cmd [lindex {"" "09" "0A"} $ch]
    set val [expr { 16*$item + 2*($item==3) + 1 }]
    ois_write_bits $ins $cmd $val 51 1
    ois_al_cfg_r $ch $ins
}

proc ois_al_color_r { ins } {
    set flags [ois_read_num $ins "R11"]
    varDoSet /${ins}/setup/alarm_1/norm_color -v [expr {($flags) & 3}]
    varDoSet /${ins}/setup/alarm_2/norm_color -v [expr {($flags) & 3}]
    varDoSet /${ins}/setup/alarm_1/trip_color -v [expr {($flags>>2) & 3}]
    varDoSet /${ins}/setup/alarm_2/trip_color -v [expr {($flags>>4) & 3}]
}

proc ois_al_color_w { ch ins val } {
    set mult [expr { 1 << (2*$ch) }]
    ois_write_bits $ins "11" $val [expr {3*$mult}] $mult
    ois_al_color_r $ins
}

CAMP_FLOAT /~/setup/alarm_1/band -D -S -R -L -T "Alarm band" \
	-d off -s on -r off -units K \
	-readProc {ois_al_lim_r 1 1} -writeProc {ois_al_lim_w 1 2}

CAMP_FLOAT /~/setup/alarm_1/high_lim -D -S -R -L -T "High trip point" \
	-d on -s on -r on -units K \
	-readProc {ois_al_lim_r 1 1} -writeProc {ois_al_lim_w 1 1}

CAMP_FLOAT /~/setup/alarm_1/low_lim -D -S -R -L -T "Low trip point" \
	-d on -s on -r on -units K \
	-readProc {ois_al_lim_r 1 0} -writeProc {ois_al_lim_w 1 0}

proc ois_al_lim_r { ch hiflag ins } {
    set cmd [lindex {"12" "15" "13" "16"} [expr {$ch+2*$hiflag-1}]]
    set resp [ois_get $ins "R${cmd}" "R${cmd}%s"]
    set C [ois_decrypt_setpoint $resp]
    if { [varGetVal /${ins}/setup/input/input_type] == 2 } {
	set u "mV"
	set K $C
    } else {
	set u "K"
	set K [expr { $C + 273.2 }]
    }
    if { $hiflag } {
	varDoSet /${ins}/setup/alarm_${ch}/high_lim -v $K -units $u
	varDoSet /${ins}/setup/alarm_${ch}/band -v $C -units $u
    } else {
	varDoSet /${ins}/setup/alarm_${ch}/low_lim -v $K -units $u
    }
}

# hiflag: 0=lowlim  1=hilim  2=band
proc ois_al_lim_w { ch hiflag ins val } {
    if { [varGetVal /${ins}/setup/input/input_type] == 2 } {
	set u [varGetVal /${ins}/setup/calibration/units]
	set dp [varGetVal /${ins}/setup/calibration/decimals]
	set max [lindex {9999 999.9 99.99 9.999} $dp]
	set min [expr { $hiflag==2 ? 0.0 : -$max}]
	set val [format "%.*f" $dp $val]
	set C $val
	set K $val
    } else {
	set u "K"
	set dp 1
	set max [expr {($hiflag==2 ? 999.0 : 1270.0 )}]
	set min 0.0
	set val [format "%.1f" $val]
	set C [expr {$val - ($hiflag==2 ? 0 : 273.2)}]
	set K [expr {$val + ($hiflag==2 ? 273.2 : 0)}]
    }

    if { ($val < $min || $val > $max ) } {
	return -code error "Value must be in range $min to $max"
    }
    set cmd [expr { $ch==1? ($hiflag? "13" : "12") : ($hiflag? "16" : "15") }]
    set dat [ois_encrypt_setpoint $C $dp]
    set stat [catch {ois_set $ins "W${cmd}${dat}" "W${cmd}"} msg]
    if { $stat } {
	return -code $stat $msg
    }
    if { $hiflag } {
	varDoSet /${ins}/setup/alarm_${ch}/high_lim -v $K -units $u
	varDoSet /${ins}/setup/alarm_${ch}/band -v $C -units $u
    } else {
	varDoSet /${ins}/setup/alarm_${ch}/low_lim -v $K -units $u
    }
    varDoSet /${ins}/setup/comm/reset -p on -p_int 0.5
}

CAMP_STRUCT /~/setup/alarm_2 -D -T "Alarm2 settings" -d on \
	-H "Settings for alarms on output 2"

# The limits (absolute:deviation) and the trips should interact,
# but it is unclear how to interpret mismatched values.  So set
# them in concert, but read them independently.

CAMP_SELECT /~/setup/alarm_2/limits -D -S -R -T "A2 Limit types" \
	-d on -s on -r on -selections Absolute Deviation \
	-readProc {ois_al_cfg_r 2} -writeProc {ois_limtyp_w 2}

CAMP_SELECT /~/setup/alarm_2/trips_on -D -S -R -T "A2 Trips on" \
	-d on -s on -r on -selections High Low {Hi/Low} Band \
	-H "Define conditions for which alarm 2 will trip" \
	-readProc {ois_al_cfg_r 2} -writeProc {ois_tripson_w 2}

CAMP_SELECT /~/setup/alarm_2/latching -D -S -R -T "A2 Latching" \
	-d on -s on -r on -selections Unlatch Latch \
	-readProc {ois_al_cfg_r 2} -writeProc {ois_al_cfg_w 2 4 4}

CAMP_SELECT /~/setup/alarm_2/contacts -D -S -R -T "A2 Contacts" \
	-d on -s on -r on -selections Norm_open Norm_close \
	-H "Behavior of alarm 2 relay" \
	-readProc {ois_al_cfg_r 2} -writeProc {ois_al_cfg_w 2 8 8}

CAMP_SELECT /~/setup/alarm_2/norm_color -D -S -R -T "Normal Color" \
	-d on -s on -r on -selections Amber Green Red \
	-H "Display color when alarms not tripped" \
	-readProc ois_al_color_r -writeProc {ois_al_color_w 0}

CAMP_SELECT /~/setup/alarm_2/trip_color -D -S -R -T "A2 Color" \
	-d on -s on -r on -selections Amber Green Red \
	-H "Display color when Alarm 2 tripped" \
	-readProc ois_al_color_r -writeProc {ois_al_color_w 2}

CAMP_FLOAT /~/setup/alarm_2/band -D -S -R -L -T "Alarm band" \
	-d off -s on -r off -units K \
	-readProc {ois_al_lim_r 2 1} -writeProc {ois_al_lim_w 2 2}

CAMP_FLOAT /~/setup/alarm_2/high_lim -D -S -R -L -T "High trip point" \
	-d on -s on -r on -units K \
	-readProc {ois_al_lim_r 2 1} -writeProc {ois_al_lim_w 2 1}

CAMP_FLOAT /~/setup/alarm_2/low_lim -D -S -R -L -T "Low trip point" \
	-d on -s on -r on -units K \
	-readProc {ois_al_lim_r 2 0} -writeProc {ois_al_lim_w 2 0}

CAMP_STRUCT /~/setup/input -D -T "Input Config" -d on \
	-H "Configure sensor input"

CAMP_SELECT /~/setup/input/input_type -D -S -R -T "Input type" \
	-d on -s on -r on -selections TC RTD Process \
	-readProc ois_input_r -writeProc ois_inputtype_w

CAMP_SELECT /~/setup/input/TC_type -D -S -R -T "TC type" \
	-d on -s on -r on -selections J K T E N DIN-J R S B C \
	-readProc ois_input_r -writeProc ois_tctype_w

CAMP_SELECT /~/setup/input/RTD_type -D -S -R -T "RTD type" \
	-d off -s on -r off \
	-H "Selection of RTD types, and number of wires" \
	-selections 392,2w 392,3w 392,4w 385,2w 385,3w 385,4w " "\
	-readProc ois_input_r -writeProc ois_rtdtype_w

CAMP_SELECT /~/setup/input/proc_type -D -S -R -T "Process type" \
	-d off -s on -r off \
	-selections {0-10 mV (HR)} {0-100 mV} {0-1000 mV} {0-10 V} {0-2 mA (HR)} {0-20 mA} \
	-H "The type and range of the input signal" \
	-readProc ois_input_r -writeProc ois_proctype_w

CAMP_SELECT /~/setup/input/RTD_ohms -D -S -R -T "RTD ohms" \
	-d off -s on -r off -selections 100 500 1000 ?? \
	-readProc ois_input_r -writeProc ois_rtdohms_w

CAMP_SELECT /~/setup/input/filter -D -S -R -T "Filter factor" \
	-d on -s on -r on -selections 1 2 4 8 16 32 64 128 \
	-readProc ois_filter_r -writeProc ois_filter_w

CAMP_SELECT /~/setup/input/ratiometric -D -S -R -T "Ratiometric oper" \
	-d off -s off -r off -selections Disable Enable \
	-H "Selection of ratiometric operation (for strain gauges)." \
	-readProc ois_input_r -writeProc ois_ratio_w

proc ois_input_r { ins } {
    set flags [ois_read_num $ins "R07"]
    switch [varGetVal /${ins}/setup/comm/model] {
	0 {# Temperature
	    set it [expr {$flags&3}]
	    set typ [expr {($flags/4)&15}]
	    set ohm [expr {($flags/64)&3}]
	    set rat 0
	    set reso 0
	}
	1 {# Strain -- always Process
	    set it 2
	    set typ [expr {$flags&3}]
	    set ohm 0
	    set rat [expr {($flags/4)&1}]
	    set reso [expr {($flags/8)&1}]
	    varDoSet /${ins}/setup/input/ratiometric -d on -r on -s on -v $rat
	}
    }
    varDoSet /${ins}/setup/input/input_type -v $it
    switch $it {
	0 {# TC
	    varDoSet /${ins}/setup/input/TC_type -d on -r on -v $typ
	    varDoSet /${ins}/setup/input/RTD_type -d off -r off
	    varDoSet /${ins}/setup/input/proc_type -d off -r off
	    varDoSet /${ins}/setup/input/RTD_ohms -d off -r off
	}
	1 {# RTD
	    set rt [lindex {0 1 2 3 6 5 6 6 6 6 4} $typ]
	    # 6 is index for " " for bogus setting
	    varDoSet /${ins}/setup/input/TC_type -d off -r off
	    varDoSet /${ins}/setup/input/RTD_type -d on -r on -v $rt
	    varDoSet /${ins}/setup/input/proc_type -d off -r off
	    varDoSet /${ins}/setup/input/RTD_ohms -d on -r on -v $ohm
	}
	2 {# Process
	    set pt [expr { ($typ&3) + (($typ&3)==3) + ($reso==0) }]
	    varDoSet /${ins}/setup/input/TC_type -d off -r off
	    varDoSet /${ins}/setup/input/RTD_type -d off -r off
	    varDoSet /${ins}/setup/input/RTD_ohms -d off -r off
	    varDoSet /${ins}/setup/input/proc_type -d on -r on -v $pt
	    varDoSet /${ins}/setup/calibration/segments -p on -p_int 1
	}
    }
}

proc ois_inputtype_w { ins val } {
    set previous [varGetVal /${ins}/setup/input/input_type]
    if { [varGetVal /${ins}/setup/comm/model] == 1 } { # strain
	if { $val != 2 } {
	    return -code error "Only Process input type allowed for process/strain models"
	}
    } else { # temperature model
	ois_write_bits $ins "07" $val 3 1
    }
    if { $val == 2 } { # process.
	ois_input_r $ins
	if { $previous != $val } {# ensure consistency with process type, raw units
	    varSet /${ins}/setup/input/proc_type -v [varGetVal /${ins}/setup/input/proc_type]
	}
    } else { # temperature: FFF.F, and units C
	ois_write_bits $ins "08" 2 15 1
	ois_input_r $ins
	if { $previous == 2 } {
	    ois_adjust_setpt_fmt $ins 1
	}
	varRead /${ins}/setpoint
    }
}

proc ois_tctype_w { ins val } {
    ois_req_model $ins 0
    ois_write_bits $ins "07" $val 0x3c 4
    ois_input_r $ins
}

proc ois_rtdtype_w { ins val } {
    ois_req_model $ins 0
    set t [lindex {0 1 2 3 10 4} $val]
    ois_write_bits $ins "07" $t 0x3c 4
    ois_input_r $ins
}

# Set the process type, which is entangled with resolution in this driver.
#
# num  name          range_num  resolution  uncalib units, max, decim
#  0  0-10 mV (HR)     0          1                  mV   9.999  3
#  1  0-100 mV         0          0                  mV   99.99  2
#  2  0-1000 mV        1          0                  mV   999.9  1
#  3  0-10 V           2          0                  V    9.999  3
#  4  0-2 mA (HR)      3          1                  mA   1.999  3
#  5  0-20 mA          3          0                  mA   19.99  2
#
proc ois_proctype_w { ins pt } {
    set proctyp [lindex {0 0 1 2 3 3} $pt]
    set resolut [lindex {1 0 0 0 1 0} $pt]
    if { [varGetVal /${ins}/setup/comm/model] == 1 } { # strain
	set dat [expr {$proctyp + 8*$resolut}]
	ois_write_bits $ins "07" $dat 0x0b 1
    } else { # temperature
	if { $resolut != 0 } {
	    return -code error "Invalid (high-resolution) setting for this model"
	}
	ois_write_bits $ins "07" $proctyp 0x3c 4
    }
    varDoSet /${ins}/setup/input/proc_type -v $pt
    varSet /${ins}/setup/calibration/segments -v 0
    ois_default_units $ins
}

proc ois_rtdohms_w { ins val } {
    ois_req_model $ins 0
    ois_write_bits $ins "07" $val 0xc0 64
    ois_input_r $ins
}

proc ois_ratio_w { ins val } {
    ois_req_model $ins 1
    ois_write_bits $ins "07" $val 0x04 4
    ois_input_r $ins
}

proc ois_filter_r { ins } {
    set val [ois_read_bits $ins "08" 0xe0 32]
    varDoSet /${ins}/setup/input/filter -v $val
}

proc ois_filter_w { ins val } {
    ois_write_bits $ins "08" $val 0xe0 32
    varDoSet /${ins}/setup/input/filter -v $val
}

# Although several settings numbers have a specification for number of
# decimal places, the instrument *really* obeys the decimal-point format
# specified for the readback (RDGCFG 08), ignoring the individual
# specifications.  So whenever the decimal format changes, the setpoint and
# alarm limits are effectively, but surreptitiously, scaled by factors of
# ten.  Therefore, we will look at the various numbers, and adjust their
# specified number of decimals to correspond to what the instrument is
# really using.  This makes visible the rescaling of the values that the
# instrument has already done.
#
# We don't need to "apply" the alarm limit settings with a reset.
#
#
proc ois_adjust_setpt_fmt { ins decimals } {
    ois_resume $ins
    set dpcode [lindex {1 2 3 5} $decimals]
    foreach cmd {01 02 12 13 15 16} {
	set dat [ois_get $ins "R${cmd}" "R${cmd}%s"]
	scan $dat "%1x%x" sdf mant
	if { ($sdf & 7) != $dpcode } {# discrepancy, so adjust
	    set sdf [expr { ($sdf & 8) | $dpcode }]
	    set dat [format "%1.1X%5.5X" $sdf $mant]
	    ois_set $ins "W${cmd}${dat}" "W${cmd}"
	}
    }
}


CAMP_STRUCT /~/setup/calibration -D -T "Calibration" \
	-H "Calibration configuration" \
	-d on

CAMP_STRING /~/setup/calibration/load_calib -D -S -P -T "Load Cal File" \
	-H "Load a calibration file" \
	-d on -s on -p off \
	-readProc ois_loadcal_r -writeProc ois_loadcal_w

CAMP_STRING /~/setup/calibration/units -D -S -T "Calibrated units" \
	-H "Enter the units for the calibrated numbers" \
	-d on -s on -v "" \
	-writeProc ois_calunits_w

CAMP_SELECT /~/setup/calibration/decimals -D -S -R -T "Decimal format" \
	-d on -s on -r on \
	-selections nnnn nnn.n nn.nn n.nnn \
	-H "Decimal format for (calibrated) reading" \
	-readProc ois_decimals_r -writeProc ois_decimals_w

CAMP_SELECT /~/setup/calibration/segments -D -S -R -P -T "Num segments" \
	-d on -s on -r on -p off \
	-selections None 1 2 3 4 5 6 7 8 9 \
	-H "The number of segments in calibration (= points - 1). Use None for raw readings." \
	-readProc ois_segments_r -writeProc ois_segments_w

# For scale and offset, use polling for single-shot read
CAMP_FLOAT /~/setup/calibration/scale -D -S -R -P -T "Input Scale" \
	-H "Current calibration scaling factor or slope: cal_value/raw_value" \
	-d on -s on -r on -p on -p_int 3 \
	-readProc ois_scale_r -writeProc ois_scale_w

CAMP_FLOAT /~/setup/calibration/offset -D -S -R -P -T "Input Offset" \
	-H "Current calibration offset value (in calibrated value units)" \
	-d on -s on -r on -p on -p_int 3 \
	-readProc ois_offset_r -writeProc ois_offset_w

proc ois_loadcal_w { ins val } {
    if { [string length $val] == 0 } {
	# Blank -> no calibration
	varSet /${ins}/setup/calibration/segments -v 0
	varDoSet /${ins}/setup/calibration/load_calib -v $val
	return
    }
    # locate file, and read it
    set val [file rootname [file tail $val]]
    varDoSet /${ins}/setup/calibration/load_calib -v $val -m "Locate file [glob ./dat/${val}.o*]"
    if { [llength [glob "./dat/${val}.o*"]] < 1 } {
	varDoSet /${ins}/setup/calibration/load_calib -m "Failed to locate calibration file for $val.  Try again."
	return -code error "Failed to locate file $val.oic"
    }
    varDoSet /${ins}/setup/calibration/load_calib -p on -p_int 0.1 -m "Read calibration file"
}

# Continue the load procedure.  It takes a long time so run in the background
# by poll.
proc ois_loadcal_r { ins } {
    set curve [varGetVal /${ins}/setup/calibration/load_calib]
    foreach file [glob "./dat/${curve}.o*"] {
	if { [string match $file "./dat/${curve}.oic"] } { break }
    }
    varDoSet /${ins}/setup/calibration/load_calib -p off
    set lf "Load failure"
    if { [catch {open $file r} fh] } {
	varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- could not read file $file."
	return -code error $lf
    }
    # Read file.  There should be 4+num_points lines
    set max 4
    for { set i 0 } { $i < $max } { } {
	if { [gets $fh line] == -1 } {
	    close $fh
	    varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- could not read file $file."
	    return -code error $lf
	}
	if { [string match "\#*" $line] } {
	    incr max
	    continue
	}
	incr i
	if { [scan $line "%s %s" tag val] != 2 } {
	    close $fh
	    varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- bad line $i in $file."
	    return -code error $lf
	}
	set tag [string trim $tag " \n\r\t:"]
	if { [lsearch {number proc_type decimals units} $tag] < 0 } {
	    close $fh
	    varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- Invalid tag ($tag) in file"
	    return -code error $lf
	}
	set $tag $val
    }
    if { [catch {set points [read $fh]}] } {
	close $fh
	varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- could not read calibration data."
	return -code error $lf
    }
    close $fh
    set points [split [string trim $points] "\n"]
    if { [catch {
	set segments [expr {$number-1}]
	set decimals [expr {$decimals + 0}]
	set proc_type [expr {$proc_type + 0}]
    }] || $proc_type < 0 || $proc_type > 5 || $decimals < 0 || $decimals > 3 } {
	varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- invalid calibration header information"
	return -code error $lf
    }
    if { [llength $points] != $number || $number < 2 || $number > 10 } {
	varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- Incorrect number of data points"
	return -code error $lf
    }
    # Apply settings for proc_type, decimals, units, and num_segments
    varDoSet /${ins}/setup/calibration/load_calib -m "Set process type ($proc_type)"
    if { [catch { varSet /${ins}/setup/input/proc_type -v $proc_type }] } {
	varDoSet /${ins}/setup/calibration/load_calib -m "$lf, when set process type ($proc_type)"
	return -code error $lf
    }
    foreach var {decimals segments units } {
	varDoSet /${ins}/setup/calibration/load_calib -v $curve -m "Set $var ([set $var])"
	if { [catch { varSet /${ins}/setup/calibration/$var -v [set $var] }] } {
	    varDoSet /${ins}/setup/calibration/load_calib -v $curve -m "$lf, when set $var ([set $var])"
	    return -code error $lf
	}
    }
    # go through points, filling in segments (just varDoSet)
    # Could insert tests for points in range here
    varDoSet /${ins}/setup/calibration/load_calib -v $curve -m "Applying segments of calibration"
    for {set i 0} {$i < $number} {incr i} {
	if { [scan [lindex $points $i] "%f %f" raw cal] != 2 } {
	    varDoSet /${ins}/setup/calibration/load_calib -m "$lf -- Invalid calibration point $i"
	    return -code error $lf
	}
	varDoSet /${ins}/setup/calibration/load_calib -m "Point $i : $raw  $cal"
	if { $i > 0 } {
	    if { [catch {
		set slope [expr { ($cal-$prevcal)/($raw-$prevraw) }]
		set offs [expr {$cal - $slope*$raw}]
		varSet /${ins}/setup/calibration/segment_$i -v "$raw,$slope,$offs"
	    } m ] } {
		varDoSet /${ins}/setup/calibration/load_calib -m "$lf Point $i : $m"
		return -code error ""
	    }
	}
	set prevraw $raw
	set prevcal $cal
    }
    varDoSet /${ins}/setup/calibration/load_calib -m ""
}

proc ois_calunits_w { ins units } {
    varDoSet /${ins}/setup/calibration/units -v $units
    if { [varGetVal /${ins}/setup/input/input_type] == 2 } { # process
	varDoSet /${ins}/setpoint -units $units
	set pt [varGetVal /${ins}/setup/input/proc_type]
	set raw_uni [lindex {mV mV mV V mA mA} $pt]
	varDoSet /${ins}/setup/calibration/scale -units "$units/$raw_uni"
	varDoSet /${ins}/setup/calibration/offset -units $units
	# others here?
    }
}

# The readback of number of points is offset by 2, and for number of
# segments the offset is 1: a readback of 1 means 3 points and 2 segments,
# which is selection number 2.
# I don't know exactly how the whole-range slope/offset (cmds 14/03) apply.
# "G"etting the value shows the currently-applicaple values from the calibration
# table, and "P"utting a value alters the calibrated readback immediately.
# When I read that there are 1 segment/2 points, I also read the scale & offset
# to determine if that means "none" or actually one section of calibration.
proc ois_segments_r { ins } {
    varDoSet /${ins}/setup/calibration/segments -p off
    if { [catch {ois_get $ins "R29" "R29%x"} num] == 0 } {
	if { [varGetVal /${ins}/setup/comm/model] != 1 } {
	    varDoSet /${ins}/setup/comm/model -v 1
	}
	if { $num < 0 || $num > 8 } { set num 0 }
	# Check for direct mapping, meaning "none"
	incr num
	if { $num == 1 } {
	    varRead /${ins}/setup/calibration/scale
	    varRead /${ins}/setup/calibration/offset
	    if { [varGetVal /${ins}/setup/calibration/offset] == 0.0 && \
		     [varGetVal /${ins}/setup/calibration/scale] == 1.0} {
		set num 0
		ois_default_units $ins
	    }
	}
    } elseif { [string match {*\?43*} $num] } {
	if { [varGetVal /${ins}/setup/comm/model] != 0 } {
	    varDoSet /${ins}/setup/comm/model -v 0
	}
	set num 0
    } else {
	return -code error "Unexpected response $num"
    }
    # Apply value
    varDoSet /${ins}/setup/calibration/segments -v $num
    ois_display_segments $ins $num
}

# Setting the number of points to "none" actually sets the calibration
# to two points (one slope) and sets the slope to 1.
proc ois_segments_w { ins val } {
    varRead /${ins}/setup/calibration/segments
    set prev [varGetVal /${ins}/setup/calibration/segments]
    set num $val
    if { $val == 0 } {# "none"
	ois_default_units $ins
	set num 1
    }
    if { [varGetVal /${ins}/setup/comm/model] == 1 } {
	if { $val == 0 } {
	    varSet /${ins}/setup/calibration/segment_1 -v "999.9,1,0"
	}
	set dat [format "%02X" [expr {$num-1}]]
	ois_set $ins "W29$dat" "W29"
    }
    varDoSet /${ins}/setup/calibration/scale -p on -p_int 5
    varDoSet /${ins}/setup/calibration/offset -p on -p_int 5
    varDoSet /${ins}/setup/calibration/segments -v $val
    ois_display_segments $ins $val
}

proc ois_display_segments { ins num } {
    set s "on"
    for { set i 0 } { $i < 9 } { } {
	if { $i == $num } { set s "off" }
	varDoSet /${ins}/setup/calibration/segment_[incr i] -d $s -s $s -r $s
    }
}

proc ois_default_units { ins } {
    set pt [varGetVal /${ins}/setup/input/proc_type]
    set dp_num [lindex {3 2 1 3 3 2} $pt]
    if { $dp_num != [varGetVal /${ins}/setup/calibration/decimals] } {
	varSet /${ins}/setup/calibration/decimals -v $dp_num
    }
    set uni [lindex {mV mV mV V mA mA} $pt]
    varSet /${ins}/setup/calibration/units -v $uni
    varDoSet /${ins}/setup/calibration/load_calib -v ""
}

proc ois_decimals_r { ins } {
    set dp_num [expr { [ois_read_bits $ins "08" 7 1] - 1 }]
    varDoSet /${ins}/setup/calibration/decimals -v $dp_num
}

proc ois_decimals_w { ins dp_num } {
    set dp_fmt [expr {$dp_num+1}]
    ois_write_bits $ins "08" $dp_fmt 7 1
    varDoSet /${ins}/setup/calibration/decimals -v $dp_num
    ois_adjust_setpt_fmt $ins $dp_num
    varRead /${ins}/setpoint
    varDoSet /${ins}/setup/calibration/scale -p on -p_int 0.2
    varDoSet /${ins}/setup/calibration/offset -p on -p_int 0.2
}

proc ois_scale_r { ins } {
    set resp [ois_get $ins "G14" "G14%s"]
    set pt [varGetVal /${ins}/setup/input/proc_type]
    set dp [varGetVal /${ins}/setup/calibration/decimals]
    set scale [ois_decrypt_scale $resp $pt $dp]
    varDoSet /${ins}/setup/calibration/scale -v $scale -p off
}

# Set the scale factor.  First, set the active calibration, both permanent and volatile;
# then, if there is only a single-region calibration, set that region.
proc ois_scale_w { ins scale } {
    if { [varGetVal /${ins}/setup/input/input_type] != 2 } {
	return -code error "No scaling of temperature allowed"
    }
    set pt [varGetVal /${ins}/setup/input/proc_type]
    set dp [varGetVal /${ins}/setup/calibration/decimals]
    set dat [ois_encrypt_scale $scale $pt $dp]
    varDoSet /${ins}/setup/calibration/scale
    ois_set $ins "P14$dat" "P14"
    ois_set $ins "W14$dat" "W14"
    varDoSet /${ins}/setup/calibration/scale -v $scale
    if { [varGetVal /${ins}/setup/comm/model] == 1 && \
	     [varGetVal /${ins}/setup/calibration/segments] < 2 } {
	ois_set $ins "W34$dat" "W34"
    }
}

proc ois_offset_r { ins } {
    set dp [varGetVal /${ins}/setup/calibration/decimals]
    set resp [ois_get $ins "G03" "G03%s"]
    set offset [ois_decrypt_offset $resp $dp]
    varDoSet /${ins}/setup/calibration/offset -v $offset -p off
}

proc ois_offset_w { ins offset } {
    set dp [varGetVal /${ins}/setup/calibration/decimals]
    set dat [ois_encrypt_offset $offset $dp]
    ois_set $ins "P03$dat" "P03"
    ois_set $ins "W03$dat" "W03"
    varDoSet  /${ins}/setup/calibration/offset -v $offset
    if { [varGetVal /${ins}/setup/comm/model] == 1 && \
	     [varGetVal /${ins}/setup/calibration/segments] < 2 } {
	ois_set $ins "W3D$dat" "W3D"
    }
}

# Generate hex string data for scaling factor (slope).  Inputs are the scale
# factor, the process type, and the (scaled) decimals.  The power-of-ten
# is shifted by the difference between the desired number of decimals and
# the natural format for the raw process-type.  There is a "conversion number"
# in the documentation, but it seems to not work right.
# We calculate the decimal-shifted slope, then multiply by 10 until we
# get an integer or run out of digits.
proc ois_encrypt_scale { sf pt scdp } {
    set rev [expr { $sf < 0.0 ? 1 : 0 }]
    set rawdp [lindex {3 2 1 3 3 2} $pt]
    set slope [expr { abs($sf) * pow(10.0,$scdp-$rawdp-1.0) }]
    set dpp 0
    while { round($slope) != $slope && $dpp < 9 && 10.0*$slope < (1<<19) } {
	set slope [expr {$slope*10.0}]
	incr dpp
    }
    set dat [expr { round($slope) | ($rev<<19) }]
    return [format "%1X%05X" $dpp $dat]
}

# Decryption; the reverse of above.
proc ois_decrypt_scale { input pt scdp } {
    set rawdp [lindex {3 2 1 3 3 2} $pt]
    if { [scan $input "%1x%x %c" dp dat c] != 2 } {
	return -code error "Invalid hex-encoded scale: $input"
    }
    set sign [expr { ($dat & 0x080000) ? -1.0 : 1.0 }]
    set dat  [expr { $dat & 0x07FFFF }]
    return [expr { $sign * double($dat) / pow(10.0,$dp-1+$scdp-$rawdp) }]
}

# For the offset, we will try to use the same power-of-ten as used for
# the result, but if the offset is too large we can adjust the power.
proc ois_encrypt_offset { val dp } {
    set dat [expr { round( abs($val) * pow(10.0,$dp)) }]
    set dp 0
    while { $dat > 0xfffff } {
	set dat [expr {$dat/10}]
	incr dp -1
    }
    if { $dp+2 < 0 } {
	return -code error "Value too large to handle"
    }
    set sps [expr { (($val<0.0) ? 0x08 : 0) | ($dp+2) }]
    return [format "%1X%05X" $sps $dat]
}

proc ois_decrypt_offset { val dp } {
    if { [scan $val "%1x%x %c" i d c] != 2 } {
	return -code error "Invalid hex-encoded scale: $val"
    }
    set sign [expr { ($i & 0x08) ? -1.0 : 1.0 }]
    set div [expr { pow(10.0, ($i&0x07) - 2 + $dp) }]
    return [expr { $sign * double( $d ) / $div }]
}


for { set ipt 1 } { $ipt <= 9 } { incr ipt } {
CAMP_STRING /~/setup/calibration/segment_$ipt -D -S -R -T "Segment $ipt" \
	-H "Segment of calibration curve. Enter three numbers, separated by comma or space: max_raw, scale, offset" \
	-d on -s on -r on -v "" \
	-readProc [list ois_calseg_r $ipt] \
	-writeProc [list ois_calseg_w $ipt]
}
unset ipt

proc ois_calseg_r { i ins } {
    ois_req_model $ins 1 "calibrations"
    set pt [varGetVal /${ins}/setup/input/proc_type]
    set dp [varGetVal /${ins}/setup/calibration/decimals]
    set cmd [format %X [expr { $i + 0x2a }] ]
    set resp [ois_get $ins "R$cmd" "R${cmd}%s"]
    set upto [string trimright [string trimright [ois_decrypt_setpoint $resp] 0] .]
    set cmd [format %X [expr { $i + 0x33 }] ]
    set resp [ois_get $ins "R$cmd" "R${cmd}%s"]
    set scal [string trimright [string trimright [ois_decrypt_scale $resp $pt $dp] 0] .]
    set cmd [format %X [expr { $i + 0x3c }] ]
    set resp [ois_get $ins "R$cmd" "R${cmd}%s"]
    set offs [string trimright [string trimright [ois_decrypt_offset $resp $dp] 0] .]
    varDoSet /${ins}/setup/calibration/segment_$i -v "$upto,$scal,$offs"
}

proc ois_calseg_w { i ins val } {
    ois_req_model $ins 1 "calibrations"
    set val [join [split $val ,] " "]
    if { [scan $val "%f %f %f %c" upto scal offs c] != 3 } {
	return -code error "Invalid segment.  Need three numbers"
    }
    varDoSet /${ins}/setup/comm/reset -p off
    set pt [varGetVal /${ins}/setup/input/proc_type]
#
    set cmd [format %X [expr { $i + 0x2a }] ]
    set dp [lindex {3 2 1 3 3 2} $pt]
    set upto [format "%.${dp}f" $upto]
    set dat [ois_encrypt_setpoint $upto $dp]
    ois_set $ins "W${cmd}${dat}" "W${cmd}"
#
    set dp [varGetVal /${ins}/setup/calibration/decimals]
    set cmd [format %X [expr { $i + 0x33 }] ]
    set scal [format %f [format "%.5e" $scal]]
    set dat [ois_encrypt_scale $scal $pt $dp]
    ois_set $ins "W${cmd}${dat}" "W${cmd}"
#
    set cmd [format %X [expr { $i + 0x3c }] ]
    set offs [format "%.${dp}f" $offs]
    set dat [ois_encrypt_offset $offs $dp]
    ois_set $ins "W${cmd}${dat}" "W${cmd}"
#
#   Remove superfluous characters for brevity
    set upto [string trimright [string trimright $upto 0] .]
    set scal [string trimright [string trimright $scal 0] .]
    set offs [string trimright [string trimright $offs 0] .]
    varDoSet /${ins}/setup/calibration/segment_$i -v "$upto,$scal,$offs"
    varDoSet /${ins}/setup/comm/reset -p on -p_int 0.9
}


CAMP_STRUCT /~/setup/comm -D -T "Communications" -d on

CAMP_SELECT /~/setup/comm/model -D -S -R -T "Model type" \
	-selections Temperature Strain \
	-d on -s on -r on \
	-H "Indicate instrument model type here" \
	-writeProc ois_model_w -readProc ois_model_r

# Look for "?43" error when reading number of linearization points, and
# set model type accordingly.
proc ois_model_r { ins } {
    if { [catch {ois_get $ins "R29" "R29%d"} resp] == 0 } {
	varSet /${ins}/setup/comm/model -v 1
    } elseif { [string match {*\?43*} $resp] } {
	varSet /${ins}/setup/comm/model -v 0
    } else {
	return -code error "Unexpected response $resp"
    }
}

proc ois_model_w { ins val } {
    varDoSet /${ins}/setup/comm/model -v $val
    switch $val {
	0 {# Temperature/Process
	    varDoSet /${ins}/setup/input/ratiometric -r off -d off
	    varDoSet /${ins}/setup/calibration -d off
	    varRead /${ins}/setup/input/input_type
	}
	1 {# Process/Strain
	    varRead /${ins}/setup/input/input_type
	    varDoSet /${ins}/setup/input/ratiometric -r on -d on
	    varDoSet /${ins}/setup/calibration -d on
	}
    }
}

# require a particular model
proc ois_req_model { ins val {what "setting"} } {
    if { [varGetVal /${ins}/setup/comm/model] != $val } {
	return -code error "Wrong model type for $what"
    }
}



CAMP_FLOAT /~/setup/comm/version -D -R -T "Soft version" \
	-d on -r on -units {} \
	-H "Software version (web-server version is different)" \
	-readProc ois_softver_r

proc ois_softver_r { ins } {
    set ret [varGetVal /${ins}/setup/comm/retries]
    set adr [varGetVal /${ins}/setup/comm/addr]
    insIfReadVerify /${ins} "*${adr}U03" 16 /${ins}/setup/comm/version "${adr}U03%f" $ret
}

CAMP_INT /~/setup/comm/retries -D -S -T "Retries" -d on -s on -v 2 \
	-H "Number of retries to attempt when readings fail" \
	-writeProc ois_retries_w

proc ois_retries_w { ins val } {
    varDoSet /${ins}/setup/comm/retries -v $val
}

CAMP_STRING /~/setup/comm/addr -D -S -T "Comm ID" -d on -s on -v "__" \
	-H "Communication address (not tcpip address but identifier string for multi-device chains)" \
	-writeProc ois_addr_w

proc ois_addr_w { ins val } {
    varDoSet /${ins}/setup/comm/addr -v $val
}

CAMP_INT /~/setup/comm/initialize -D -S -R -P -T "Initialize" \
	-d on -s on -r off -p off -p_int 0.5 -units {} \
	-H "This is set and polled when going online." \
	-readProc ois_init_vars_r -writeProc ois_init_comm_w

# Initialize communication (bus) parameters to what we expect.
# We can't rely on command echo until we set it, so do some
# string manipulations. We can't rely on device address either,
# so try some possibilities.  Do some retries, more for tcpip.
# Don't set the comm parameters if they are already correct,
# saving a "reset" operation to apply the settings.
#
# loop over retries
#   read bus format (1F)
#   if address or prefix error, then
#      select another address
#   if not correct, then
#      standby
#      set bus: space-separator, command mode, echo, no linefeed, no modbus
#      reset (to apply)
#      sleep while instrument is deaf doing reset
# read the "readback format"
# if readback format is not "only value", then
#   set readback format to only value (strange problems with others)
#   schedule a reset to apply
# determine model type, by trying to read number of linearization points
proc ois_init_comm_w { ins val } {
    set adr [varGetVal /${ins}/setup/comm/addr]
    if { "__" == "$adr" } {
        set adr ""
        set adrs [list 01 "" 01 02 03 04 ""]
    } else {
        set adrs [list "" 01 $adr "" 01 02 03 04 $adr]
    }
    set serial [string match "rs232*" [insGetIfTypeIdent /$ins] ]
    set j 0
    for { set j 0} { $j < ($serial ? 4 : 12) } {incr j} {
        set resp ""
	if { [catch { set resp [insIfRead /$ins "*${adr}R1F" 16]}] || \
                 [scan "_$resp" {%*[^?]?%2d} err] == 1 } {
            # Try various device addresses
	    set adr [lindex $adrs 0]
	    if { [llength $adrs] > 1 } { set adrs [lrange $adrs 1 99] }
	    continue
	}
	# Don't rely on echoed prefix, so provide one
	set resp "00R1F$resp"
	set l [string length $resp]
	if { [scan [string range $resp [expr {$l-5}] $l] {R1F%2x} old] != 1 } { continue }
	set new [expr { ($old & 0xC8) | 0x14 }]
	set cmd [format "*${adr}W1F%02x" $new]
	set mask [expr { ( $serial ? 16+7 : 7 ) }]
	if { ($old&$mask) == ($new&$mask) || \
		 0 == [catch {
		     insIfWrite /$ins "*${adr}D03"
		     set resp [insIfRead /$ins $cmd 16]
		     # reset to apply
		     ois_set $ins "Z02" "Z02"
		     sleep 0.5
		 }]} {
	    set j 99
	}
    }
    varDoSet /${ins}/setup/comm/addr -v $adr
    # now we can rely on echo, so use higher-level procedures, with retries
    set resp [ois_get $ins "R20" "R20%s"]
    if { [string compare $resp "02"] } {
        ois_set $ins "W2002" "W20"
        varDoSet /${ins}/setup/comm/reset -p on -p_int 0.5
    }
    varRead /${ins}/setup/comm/model
    varDoSet /${ins}/setup/comm/initialize -v $val
}

# Initialize some variables by reading from the instrument.  This
# affects the display of some variables.  Also force some settings.
# We typically poll this once after going online.
proc ois_init_vars_r { ins } {
    varDoSet /${ins}/setup/comm/initialize -p off
    varRead /${ins}/setup/input/input_type
    varRead /${ins}/setup/out1_func
    varRead /${ins}/setup/out2_func
    varRead /${ins}/setup/calibration/decimals
    set inputtype [varGetVal /${ins}/setup/input/input_type]
    if { $inputtype == 2 } {# Process
	varRead /${ins}/setup/input/proc_type
    }
    # Set appropriate readback formats (note: setting the same value skips some settings)
    varSet /${ins}/setup/input/input_type -v $inputtype
    # Know the setpoint
    varRead /${ins}/setpoint
    varRead /${ins}/setup/comm/version
}

# Standby (disable operation).  This is (supposedly) needed during configuration settings.
proc ois_standby { ins } {
    ois_set $ins "D03" "D03"
}

# Resume (disable standby) and then hard reset (to apply settings that were
# made during standby).  There is some dead time after the reset, so wait
# a bit.
proc ois_resume { ins } {
    #ois_set $ins "E03" "E03"
    ois_set $ins "Z02" "Z02"
    varDoSet /${ins}/setup/comm/reset -p off
    sleep 0.55
}


CAMP_INT /~/setup/comm/reset -D -S -P -T "Reset" \
	-d on -s on -p off -p_int 0.3 -units {} \
	-H "This is set (or polled once) to reset and copy settings to active memory." \
	-readProc ois_resume -writeProc ois_reset_w

proc ois_reset_w { ins target } {
    ois_resume $ins
}

# Perform a setting command, with retries, and look for confirmation string.
proc ois_set { ins cmd conf } {
    set max [varGetVal /${ins}/setup/comm/retries]
    set adr [varGetVal /${ins}/setup/comm/addr]
    set cmd "*${adr}${cmd}"
    set conf "${adr}${conf}"
    for { set retries 0 } { $retries < $max } { incr retries } {
	set resp ""
	catch { set resp [insIfRead /$ins $cmd 32] } msg
	if { [string match $conf $resp] } { return }
	if { [string match {\?*} $resp] } {
	    return -code error "Command error (${resp}) "
	}
        sleep 0.2
    }
    return -code error "No response ($resp) after $retries tries "
}

# Read something, with retries.  Extract value from command echo.
# The scan pattern (patt) should contain one scan element, which is returned.
proc ois_get { ins cmd patt } {
    set max [varGetVal /${ins}/setup/comm/retries]
    set adr [varGetVal /${ins}/setup/comm/addr]
    set cmd "*${adr}${cmd}"
    set patt "${adr}${patt}"
    for { set retries 0 } { $retries < $max } { incr retries } {
	set resp ""
	catch { set resp [insIfRead /$ins $cmd 32] } msg
	if { [scan $resp "$patt %c" val extra] == 1 } { return $val }
	if { [string match {*\?[45][306]*} $resp] } {
	    return -code error "Read error (${resp}) cmd: $cmd, patt: $patt"
	}
        sleep 0.2
    }
    return -code error "Read failure (got '${resp}', $msg) "
}

# Write some portion of a register, identified by the bit mask, setting
# those bits to val * mult
proc ois_write_bits { ins cmd val mask mult } {
    set flags [ois_read_num $ins "R${cmd}"]
    set flags [expr { ($flags & ~$mask) | ($val*$mult & $mask) }]
    ois_write_num $ins "W${cmd}" 2 $flags
}

proc ois_read_bits { ins cmd mask mult } {
    set flags [ois_read_num $ins "R${cmd}"]
    return [expr { ($flags & $mask) / $mult }]
}

# Read a simple number (given in hex)
proc ois_read_num { ins req } {
    set patt "$req %x"
    return [ois_get $ins $req $patt]
}

# write a simple number (as hex).
# nibbles correspond to hex characters; two nibbles per byte.
proc ois_write_num { ins cmd nibbles val } {
    set req [format "%s%0${nibbles}X" $cmd $val]
    varDoSet /${ins}/setup/comm/reset -p on -p_int 0.5
    ois_set $ins $req $cmd
}
