# camp_ins_oms_xy_motors.tcl
#
# $Log: camp_ins_oms_xy_motors.tcl,v $
# Revision 1.3  2015/03/15 00:39:29  suz
# Ted's 201404 interface changes
#
# Revision 1.2  2004/12/18 03:17:53  asnd
# Fix STOP, Read state less often
#
# Revision 1.1  2004/11/04 01:49:46  asnd
# Instrument for Hi Time cryostat-position motors (OMS PC68 controller)
#
#

CAMP_INSTRUMENT /~ -D -T "OmsPC68 Cryostat position" \
    -H "OmsPC68 x-y motors for HiTime cryostat position" -d on \
    -initProc Oms_init -deleteProc Oms_delete \
    -onlineProc Oms_online -offlineProc Oms_offline

# This device formats it responses with <LF><CR> both before and
# after the relevant text.  Therefore, we need to set a read
# terminator of "none".  But then we have no way of knowing
# when all the characters are transmitted, which will lead to
# either truncation or timeouts (timeouts can only be integer
# seconds -- a bad implementation).  So here is the plan:
# we will set "none" as the read terminator, and we will
# include an extra "WY" in every query, which gives a 30 char
# response string which can act as padding to fill out the
# read buffer, thus avoiding a wait for timeout.  Characters
# that overflow the buffer are still waiting, but when the next
# read happens, the buffer will be cleared (positive timeout
# value).  Every read must have a carefully set buffer size, so
# that the desired information cannot overflow, but the padded
# response will always overflow.
# (Yes, another termination mode could be implemented in the
# rs232 interface driver, and that would work best.)

proc Oms_init { ins } {
    insSet /${ins} -if rs232 0.1 1 /tyCo/1 9600 8 none 1 none LF
}

proc Oms_delete { ins } { insSet /${ins} -line off }

proc Oms_online { ins } {
    insIfOn /${ins}
    if { [catch {
        varRead /${ins}/setup/ident
        varRead /${ins}/movement
	if { [varGetVal /${ins}/movement] != 1 } {
	    varRead /${ins}/position_x
	    varRead /${ins}/position_y
	    varDoSet /${ins}/destin_x -v [varGetVal /${ins}/position_x]
	    varDoSet /${ins}/destin_y -v [varGetVal /${ins}/position_y]
	}
    }] || [varGetVal /${ins}/setup/ident] == 0 } {
        insIfOff /${ins}
        return -code error "failed test read; check interface definition and connections, and set IO to serial"
    }
    varDoSet /${ins}/movement -p on -p_int 2.0
}

proc Oms_offline { ins } { insIfOff /${ins} }

CAMP_FLOAT /~/position_x -D -R -P -L -A -T "Read x position" \
	-d on -r on -tol 0.05 -units "mm" \
	-readProc {Oms_read_xy_r x}

CAMP_FLOAT /~/position_y -D -R -P -L -A -T "Read y position" \
	-d on -r on -tol 0.05 -units "mm" \
	-readProc {Oms_read_xy_r y}

  proc Oms_read_xy_r { ax ins } {
    set raw [Oms_query /$ins "A$ax RP" 16 {%d} 3]
    set cal [varGetVal /${ins}/setup/calib_$ax]
    varDoSet /${ins}/position_$ax -v [format %.3f [expr {$cal*$raw}]]
    # Maybe change the formula!
    varTestAlert /${ins}/position_$ax [varGetVal /${ins}/destin_$ax]
  }

# Here we implement the device read using the WY to generate buffer-filler

  proc Oms_query { path query size patt retry } {
    for {set j 0} {$j<$retry} {incr j} {
        set buf [insIfRead $path "$query WY" $size]
        if { [scan $buf "$patt PC%d" val n] == 2 } {
            return $val
        }
    }
    return -code error "Failed parse of $buf"
  }

CAMP_FLOAT /~/destin_x -D -S -L -T "Set x position" \
	-d on -s on -units "mm" \
        -H {Set x- and y-axis destinations, then set movement to apply.} \
	-writeProc Oms_destin_x_w

  proc Oms_destin_x_w { ins target } {
    varDoSet /${ins}/destin_x -v $target
  }

CAMP_FLOAT /~/destin_y -D -S -L -T "Set y position" \
	-d on -s on -units "mm" \
        -H {Set x- and y-axis destinations, then set movement to apply.} \
	-writeProc Oms_destin_y_w

  proc Oms_destin_y_w { ins target } {
    varDoSet /${ins}/destin_y -v $target
  }

CAMP_SELECT /~/movement -D -R -P -S -T "Movement status" \
        -d on -r on -s on -p on -p_int 3 \
        -H {Set or read movement status. Set this to start or stop a move.} \
        -selections STOP MOVE RESTORE LIMIT \
        -readProc Oms_movement_r -writeProc Oms_movement_w

  proc Oms_movement_r { ins } {
    varRead /${ins}/position_x
    varRead /${ins}/position_y
    for {set j 0} {$j<3} {incr j} {
        set buf [insIfRead /$ins "AA RV WY" 32]
        if { [scan $buf " %d,%d,%d,%d PC%d" vx vy vz vt n] == 5 } {
            if { $vx != 0 || $vy != 0 } { # moving
                varDoSet /${ins}/movement -v 1 -p_int 0.8
                return
            }
            break
        }
    }
    if { $j>=3 } {
        return -code error "Failed movement status"
    }
    for {set j 0} {$j<3} {incr j} {
        set buf [insIfRead /$ins "AX RA AY RA WY" 32]
        if { [scan $buf " %1s%1s%1s%1s %1s%1s%1s%1s PC%d" xdir xi xl xh ydir yi yl yh n] == 9 } {
            if { $xl == "L" || $yl == "L" } { # limit switch
                varDoSet /${ins}/movement -v 3 -p_int 5
                return
            }
            break
        }
    }
    if { $j>=3 } {
        return -code error "Failed limit status"
    }
    # else, stopped
    varDoSet /${ins}/movement -v 0 -p_int 15
  }

  proc Oms_movement_w { ins target } {
    #varRead /${ins}/movement
    set prev [varGetVal /${ins}/movement]
    switch $target {
        0 -
        3 { # STOP -- abort movement
            insIfWrite /$ins "ST"
            varDoSet /${ins}/movement -v 0 -p off
        }
        1 { # MOVE -- initiate movement
	    if { $prev == 1 } {
		insIfWrite /$ins "ST"
	    }
            set x [varGetVal /${ins}/destin_x]
            set y [varGetVal /${ins}/destin_y]
            set r [varGetVal /${ins}/setup/radius]
            if { $x*$x + $y*$y > $r*$r + 0.01 } {
                return -code error "Position is outside allowed radius"
            }
            set rawx [expr {round($x/[varGetVal /${ins}/setup/calib_x])}]
            set rawy [expr {round($y/[varGetVal /${ins}/setup/calib_y])}]
            set acc [varGetVal /${ins}/setup/acceleration]
            set vel [varGetVal /${ins}/setup/velocity]
            insIfWrite /$ins "AA AC$acc,$acc; VL$vel,$vel; MT$rawx,$rawy; GO"
            varDoSet /${ins}/movement -v 1 -p off
        }
        2 { # RESTORE -- return destin to current position
            varSet /${ins}/destin_x -v [varGetVal /${ins}/position_x]
            varSet /${ins}/destin_y -v [varGetVal /${ins}/position_y]
        }
    }
    varDoSet /${ins}/movement -p on -p_int 0.9
 }


CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

CAMP_SELECT /~/setup/ident -D -R -T "Identification" -d on -r on \
        -selections ? {OMS PC68} \
        -readProc Oms_ident_r

  proc Oms_ident_r { ins } {
    set buf [insIfRead /$ins "WY" 28]
    varDoSet /${ins}/setup/ident -v [expr {[scan $buf { PC%d ver %f} i v] == 2 } ]
  }


CAMP_SELECT /~/setup/define_pos -D -S -T "Define position" \
        -d on -s on -v 0 -selections { } DEFINE \
        -H {Set this to declare the present 'destination' settings to be the position. No movement involved.} \
        -writeProc Oms_def_pos_w

proc Oms_def_pos_w { ins target } {
    set x [varGetVal /${ins}/destin_x]
    set y [varGetVal /${ins}/destin_y]
    set r [varGetVal /${ins}/setup/radius]
    if { $x*$x + $y*$y > $r*$r + 0.01 } {
        return -code error "Position is outside allowed radius"
    }
    set rawx [expr {round($x/[varGetVal /${ins}/setup/calib_x])}]
    set rawy [expr {round($y/[varGetVal /${ins}/setup/calib_y])}]
    insIfWrite /$ins "AA LP$rawx,$rawy;"
    varRead /${ins}/movement
}

CAMP_FLOAT /~/setup/radius -D -S -T "Radius of motion" \
        -d on -s on -v 3.0 -units mm \
        -H {Radius of permitted motion} \
        -writeProc Oms_radius_w

  proc Oms_radius_w { ins r } {
    varDoSet /${ins}/setup/radius -v $r
  }

CAMP_INT /~/setup/velocity -D -S -T "Move velocity" \
        -d on -s on -v 1000 -units {ticks/s} \
        -writeProc Oms_vel_w

  proc Oms_vel_w { ins v } {
    if { $v < 1 || $v > 1000000 } {
        return -code error "Value out of range; must be from 1 to 1000000"
    }
    varDoSet /${ins}/setup/velocity -v $v
    insIfWrite /$ins "AA VL$v,$v;"
  }

CAMP_INT /~/setup/acceleration -D -S -T "Move acceleration" \
        -d on -s on -v 2500 -units {ticks/s^2} \
        -writeProc Oms_accel_w

  proc Oms_accel_w { ins a } {
    if { $a < 1 || $a > 8000000 } {
        return -code error "Value out of range; must be from 1 to 8000000"
    }
    varDoSet /${ins}/setup/acceleration -v $a
    insIfWrite /$ins "AA AC$a,$a;"
  }

CAMP_FLOAT /~/setup/calib_x -D -S -T "X calibration" -d on -s on \
        -H {Distance calibration for horizontal movement (x axis)} \
        -v 0.00049609375 -units {mm/tick} \
        -writeProc {Oms_cal_w x}

CAMP_FLOAT /~/setup/calib_y -D -S -T "Y calibration" -d on -s on \
        -H {Distance calibration for vertical movement (y axis)} \
        -v 0.00049609375 -units {mm/tick} \
        -writeProc {Oms_cal_w y}

  proc Oms_cal_w { axis ins cal } {
    if { $cal < 0.00001 || $cal > 0.01 } {
        return -code error "Calibration out of range"
    }
    varDoSet /${ins}/setup/calib_$axis -v $cal
  }


