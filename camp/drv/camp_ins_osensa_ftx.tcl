# camp_ins_osensa_ftx.tcl
# Camp Tcl instrument driver for Osensa FTX logger, where the most recent 
# logged information is served out by the osensaserv.tcl program.
#
# $Log: camp_ins_osensa_ftx.tcl,v $
# Revision 1.2  2015/04/25 00:07:37  asnd
# Still initial (typo fix)
#
# Revision 1.1  2015/03/15 00:52:52  suz
# Initial version
#



CAMP_INSTRUMENT /~ -D -T "Osensa FTX" \
    -H "Osensa FTX temperature logger" \
    -d on \
    -initProc oftx_init \
    -deleteProc oftx_delete \
    -onlineProc oftx_online \
    -offlineProc oftx_offline

proc oftx_init { ins } {
    insSet /${ins} -if tcpip 0.1 1.0 142.90.0.0 10002 CR CR 
}
proc oftx_delete { ins } {
    insSet /${ins} -line off
}
proc oftx_online { ins } {
    insIfOn /${ins}
    # Todo: nicer test of connection (now done by provoking error)
    set id [catch {insIfRead /$ins TEST 100} msg]
    if { $id == 0 && [string compare $msg "Request should be \"get\""] } {
        set id 1
    }
    if { $id } {
        insIfOff /$ins
        return -code error "Failed to connect to osensaserv service: $msg"
    }
    # Todo: get list of channels being logged (A-D)
}
proc oftx_offline { ins } {
    insIfOff /${ins}
}

CAMP_FLOAT /~/temperature -D -R -P -L -T "Temperature reading" \
    -d on -r on -units C  \
    -readProc oftx_temperature_r 

# Initially, I will discard all other information on the line of data returned by "get",
# and have just one temperature of interest (likely chan A). Don't check timestamp
# to verify that the value is fresh.

proc oftx_temperature_r { ins } {
    set line [insIfRead /$ins "get" 120]
    scan $line { %*d/%*d/%*d,%*d:%*d:%*d,%d,%[ABCD],%f,%f} busid chan temperature what
    varDoSet /${ins}/temperature -v $temperature
}

CAMP_SELECT /~/T_units -D -S -T "Temperature units" \
    -d on -s on -selections "C" "K" "F" \
    -H "Set this to match the units used for logging in OsensaView FTX." \
    -writeProc oftx_tunits_w

proc oftx_tunits_w { ins ui } {
    varDoSet /${ins}/T_units -v $ui
    varDoSet /${ins}/temperature -units [lindex {C K F} $ui]
}




