 # camp_ins_oxford_ips120.tcl or camp_ins_oxford_ips120_doc.tcl
 # Camp Tcl instrument driver for Oxford IPS120-10 superconducting magnet power supply.
 # Donald Arseneau,  TRIUMF
 # Last revised   (Friday) 13-Feb-2009
 #
 # Do not edit camp_ins_oxford_ips120.tcl!  Edit camp_ins_oxford_ips120_doc.tcl
 # instead; it has lots of comments.  If there are no following comments, then
 # this is camp_ins_oxford_ips120.tcl, and you should not edit it.

CAMP_INSTRUMENT /~ -D -T "Oxford IPS120-10" \
    -H "Oxford Instruments IPS120-10 Superconducting Magnet Power Supply" \
    -d on \
    -initProc ox_ips120_init -deleteProc ox_ips120_delete \
    -onlineProc ox_ips120_online -offlineProc ox_ips120_offline

    if { [catch { set MAG(names) } n ] == 1 || $n == ""} {
	source "drv/magnet_procs.tcl"
    }

    proc ox_ips120_init { ins } {
	global MAG
	mag_var_init $ins
	set MAG($ins,PS) ox_ips120
	set MAG($ins,tol) 0.0003
	set MAG($ins,bigtol) 0.03
	insSet /${ins} -if rs232 0.3 2 /tyCo/2 9600 8 none 2 CR CR
    }

    proc ox_ips120_delete { ins } {
	insSet /${ins} -line off
    }

    proc ox_ips120_online { ins } {
	global MAG
	if { [insGetIfDelay /${ins}] < 0.15 } {
	    set c "\[insSet /${ins} -if [insGetIfTypeIdent /${ins}] 0.25 [insGetIfTimeout /${ins}] [insGetIf /${ins}] \]"
	    expr $c
	}
	insIfOn /${ins}
	varRead /${ins}/setup/id
	if { [varGetVal /${ins}/setup/id ] == 0 } {
	    insIfOff /${ins}
	    return -code error "failed ID query, check interface definition and connections"
	}
	insIfWrite /${ins} "\$Q4"
	insIfWrite /${ins} "\$C3"
	varDoSet /${ins}/controls/do_ramp -p off
	varDoSet /${ins}/setup/magnet -p on -p_int 0.1
	if { [catch {varRead /${ins}/controls/activity}] } {
	    insIfOff /${ins}
	    return -code error "Failed to read status"
	}
	if { [varGetVal /${ins}/controls/activity] > 2 } {
	    return -code error "Output is clamped. Check conditions, then set /${ins}/controls/activity to \"hold\" if safe"
	}
    }

    proc ox_ips120_offline { ins } {
	insIfOff /${ins}
    }

    CAMP_FLOAT /~/mag_set -D -S -T "Set magnet current" \
	-H "Set magnet current with intelligent processing." \
	-d on -s on -units A -writeProc mag_set_w

    CAMP_FLOAT /~/mag_read -D -R -P -L -A -T "Output current reading" \
	-H "Read power supply output current" \
	-d on -r on -units A -tol 0.001 -readProc ox_ips120_mag_read_r

      proc ox_ips120_mag_read_r { ins } {
	insIfReadVerify /${ins} "R0" 32 /${ins}/mag_read "R%f" 2
	varTestAlert /${ins}/mag_read [varGetVal /${ins}/mag_set]
      }

    CAMP_FLOAT /~/mag_field -D -S -R -P -L -T "Nominal Magnetic Field" \
	-H "Nominal Magnetic Field, using magnet calibration constant (read and set)" \
	-d on -s on -r on -units T -tol 0.0002 -readProc ox_ips120_field_r -writeProc ox_ips120_field_w

      proc ox_ips120_field_r { ins } {
	varRead /${ins}/heat_status
	if { [varGetVal /${ins}/heat_status] == 1 } {
	    insIfReadVerify /${ins} "R18" 32 /${ins}/mag_field "R%f" 3
	} else {
	    insIfReadVerify /${ins} "R7" 32 /${ins}/mag_field "R%f" 3
	}
	varRead /${ins}/mag_read
      }

      proc ox_ips120_field_w { ins target } {
	varSet /${ins}/mag_set -v [ expr ( $target + 0.0 ) / ([varGetVal /${ins}/setup/calibration]) ]
	varDoSet /${ins}/degauss/set_field -v $target
      }

    CAMP_FLOAT /~/degauss_field -D -S -T "Set with Degauss" \
	-H "Sets nominal magnetic field using Degauss procedures" \
	-d on -s on -units T -writeProc mag_degauss_set_field_w

    CAMP_SELECT /~/ramp_status -D -S -T "Ramp status" -d on -s on \
	-H "Indicator for ramp-control sequencer" \
	-selections Holding Ramping Settling Persistent "Ramp leads +" "Ramp leads -" \
		"Heat switch" "Cool switch" "Turn on Heat" "Turn off Heat" "Degauss" \
	-writeProc ox_ips120_ramp_status_w

      proc ox_ips120_ramp_status_w { ins target } { }

    CAMP_SELECT /~/heat_status -D -S -R -P -T "Heater status" \
	-H "Superconducting switch heater status" \
	-d on -s on -r on -selections none off on force_on \
	-readProc ox_ips120_status_r -writeProc ox_ips120_heat_status_w

      proc ox_ips120_heat_status_w { ins target } {
	varRead /${ins}/heat_status
	set h [varGetVal /${ins}/heat_status]
	if { ($h==0) == ($target==0) } {
	    if { $target>0 } { insIfWriteMatch /${ins} H[ expr $target - 1 ] H 32 5 }
	    varRead /${ins}/heat_status
	} else {
	    return -code error "define magnet to declare presence/absence of switch"
	}
      }

    CAMP_FLOAT /~/fast_set -D -S -R -T "Fast magnet set, no checks" \
	-H "Set magnet current. No smart control; no checks. (Read PS setpoint.)" \
	-d on -s on -r on -units A \
	-readProc ox_ips120_fast_set_r -writeProc ox_ips120_fast_set_w

      proc ox_ips120_fast_set_w { ins target } {
	set val [format "%+.4f" $target]
	if { [catch {
	  insIfWriteVerify /${ins} "\$I${val}" "R5" 32 /${ins}/fast_set "R%f" 5 $val .0002
	  insIfWriteMatch /${ins} "A1" A 32 5
	} msg ] } {
	  if { 0 == [catch {varRead /${ins}/controls/activity}] && [varGetVal /${ins}/controls/activity] > 2 } {
	    return -code error "Output is clamped. Check conditions, then set /${ins}/controls/activity to \"hold\" if safe"
	  }
	  return -code error $msg
	}
      }

      proc ox_ips120_fast_set_r { ins } {
	insIfReadVerify /${ins} "R5" 32 /${ins}/fast_set "R%f" 3
      }


    CAMP_FLOAT /~/settle_set -D -S -T "Set and Settle" \
	-H "Sets magnet and waits until stable" \
	-d on -s on -units A -writeProc mag_settle_w

    CAMP_FLOAT /~/volts -D -R -P -L -T "Output voltage" \
	-d on -r on -units V -readProc ox_ips120_volts_r

      proc ox_ips120_volts_r { ins } {
	    insIfReadVerify /${ins} "R1" 32 /${ins}/volts "R%f" 3
	}

    CAMP_SELECT /~/abort -D -S -T "Abort or Pause Ramp" \
	-H "Abort, pause, or continue a magnet change" \
	-d on -s on -v 0 -selections Abort Pause Resume "Never mind" " " \
	-writeProc mag_abort_w

    CAMP_SELECT /~/refresh -D -S -R -P -T "Refresh Persistent Current" \
	-H "Set or poll this to rejuvenate persistent field in magnet" \
	-d on -s on -r off -v 0 -selections Refresh \
	-readProc mag_refresh_r -writeProc mag_refresh_w



    CAMP_STRUCT /~/degauss -D -T "Degauss magnet" -d on 

	CAMP_FLOAT /~/degauss/amplitude -D -S -T "degauss delta B" \
	    -H "Set magnitude of field oscillations for degaussing magnet (if 0, use 0.25% of the field change, plus 20 G)" \
	    -units T -d on -s on \
	    -writeProc mag_deg_ampl_w

	CAMP_FLOAT /~/degauss/decrement -D -S -T "degauss % decrement" \
	    -H "The percentage decrease in field for each stage of degaussing" \
	    -d on -s on -units "%" -v 10.0 \
	    -writeProc mag_deg_decrement_w

	CAMP_FLOAT /~/degauss/set_field -D -S -T "degauss set B" \
	    -H "Set the final field for after degaussing magnet" \
	    -units T -d on -s on \
	    -writeProc mag_deg_setf_w

	CAMP_SELECT /~/degauss/degauss -D -R -P -S -T "degauss magnet" \
	    -d on -s on -r off -p off -selections "FINISHED" "DEGAUSS NOW" \
	    -readProc mag_degauss_r -writeProc mag_degauss_w


    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on \
	-H "Set magnet and power supply parameters"

	CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections false true -readProc ox_ips120_setup_id_r

	  proc ox_ips120_setup_id_r { ins } {
	    set id 0
	    set buf x
	    catch {insIfRead /${ins} "V" 80} buf
	    set id [expr { [scan $buf " IPS120-10  Version %f  (c) OXFORD %d" d1 d2] == 2 } ]
	      set xx $id
	    if { $id != 1 } {
	      while {[catch {insIfRead /${ins} "\$" 128 buf}] == 0} {}
	      catch {insIfRead /${ins} "V" 80} buf
	      set id [expr { [scan $buf " IPS120-10  Version %f  (c) OXFORD %d" d1 d2] == 2 } ]
	    }
	    varDoSet /${ins}/setup/id -v $id
	  }
	CAMP_SELECT /~/setup/magnet -D -S -R -P -T "Select magnet" \
	    -H "Identify superconducting magnet" \
	    -d on -s on -r on -p off -selections None Helios DR Belle bNMR HiTime \
	    -readProc ox_ips120_magnet_r -writeProc ox_ips120_magnet_w
	  proc ox_ips120_magnet_r { ins } {
	    global MAG
	    varDoSet /${ins}/setup/magnet -p off
	    varRead /${ins}/controls/inductance
	    set induc [varGetVal /${ins}/controls/inductance]
	    set i -1
	    set target 0
	    foreach c $MAG(indlist) {
		incr i
		if { $induc == $c } {
		    set target $i
		}
	    }
	    varDoSet /${ins}/setup/magnet -v $target
	    set mag_S [lindex [string tolower $MAG(namelist)] $target]
	    if { $MAG($ins,induc) != $induc || [string compare $mag_S $MAG($ins,name)] } {
		set MAG($ins,upload) 0
		source "drv/magnet_${mag_S}.ini"
	    }
	    ox_ips120_all_status_r $ins
	  }

	  proc ox_ips120_magnet_w { ins target } {
	    global MAG
	    varDoSet /${ins}/setup/magnet -v $target
	    set mag_S [lindex [string tolower $MAG(namelist)] $target ]
	    source "drv/magnet_${mag_S}.ini"
	    ox_ips120_all_status_r $ins
	    varRead /${ins}/controls/inductance
	    set induc [varGetVal /${ins}/controls/inductance]
	    set i [lindex $target $MAG(indlist)]
	    if { abs($i-$induc) > 0.1 } {
	      return -code error "magnet configuration can only be changed manually, using the Test 07 (SUP) Menu"
	    }
	  }

	CAMP_FLOAT /~/setup/mag_max -D -S -T "Upper current limit" \
	    -H "Set maximum allowed current" \
	    -d on -s on -units A -writeProc ox_ips120_setup_mmax_w

	  proc ox_ips120_setup_mmax_w { ins target } {
		insIfReadVerify /${ins} "R22" 32 /${ins}/setup/mag_max "R%f" 2
		if { [varGetVal /${ins}/setup/mag_max] > $target } {
		    varDoSet /${ins}/setup/mag_max -v $target
		}
	  }

	CAMP_FLOAT /~/setup/mag_min -D -S -T "Lower current limit" \
	    -H "Set minimum allowed current" \
	    -d on -s on -units A -writeProc ox_ips120_setup_mmin_w

	  proc ox_ips120_setup_mmin_w { ins target } {
		insIfReadVerify /${ins} "R21" 32 /${ins}/setup/mag_min "R%f" 2
		if { [varGetVal /${ins}/setup/mag_min] < $target } {
		    varDoSet /${ins}/setup/mag_min -v $target
		}
	  }

	CAMP_FLOAT /~/setup/volt_max -D -R -T "Voltage limit" \
	    -d on -r on -units V -readProc ox_ips120_setup_vmax_r

	  proc ox_ips120_setup_vmax_r { ins } {
		insIfReadVerify /${ins} "R15" 32 /${ins}/setup/volt_max "R%f" 2
	  }

	CAMP_FLOAT /~/setup/ramp_rate -D -S -R -T "Ramp rate (A/s)" \
	    -H "Requested MAXIMUM ramp rate for magnet. Lower rates may be chosen automatically." \
	    -d on -s on -r on -units "A/s" \
	    -readProc ox_ips120_ramp_rate_r -writeProc ox_ips120_ramp_rate_w

	  proc ox_ips120_ramp_rate_w { ins target } {
	    varDoSet /${ins}/setup/ramp_rate -v $target
	    if { $target > 0 } {
		varSet /${ins}/controls/ramp_rate -v $target
	    }
	  }

	  proc ox_ips120_ramp_rate_r { ins } {
	    if { [scan [insIfRead /${ins} "R6" 32] "R%f" val] != 1 } {
	      while {[catch {insIfRead /${ins} "\$" 128 buf}] == 0} {}
	      if { [scan [insIfRead /${ins} "R6" 32] "R%f" val] != 1 } {
		return -code error "error reading IPS120 ramp rate"
	      }
	    }
	    varDoSet /${ins}/setup/ramp_rate -v [expr $val/60.0]
	  }

	CAMP_SELECT /~/setup/ramp_table -D -S -R -T "Ramp Rates Table" \
	    -d on -s on -r on -selections Normal Slow \
	    -readProc ox_ips120_status_r -writeProc ox_ips120_ramp_table_w

	  proc ox_ips120_ramp_table_w { ins target } {
	      set buf [insIfRead /${ins} "X" 32]
	      set stat [scan $buf "X%1d%1dA%1dC%1dH%1dM%1d%1dP%1d%1d" s1 s2 a c h m1 m2 p1 p2]
	      if { $stat != 9 } { return -code error "failed reading IPS120 status" }
	      set m1 [ expr ( $m1 & 3 ) | (4 * $target) ]
	      if { [ string first "?" [ insIfRead /${ins} "M${m1}" 32 ] ] >= 0 } {
		  return -code error "Failed to set ramp table"
	      }
	      varDoSet /${ins}/setup/ramp_table -v $target
	  }

	CAMP_SELECT /~/setup/ramp_mode -D -S -R -T "Ramp mode" \
	    -H "Set this to choose method of magnet operation. Read to choose mode automatically." \
	    -d on -r on -s on -selections non-persistant persistant \
	    -readProc mag_ramp_mode_r -writeProc mag_ramp_mode_w

	CAMP_FLOAT /~/setup/heat_i -D -R -T "Heater current (mA)" \
	    -d on -r on -units mA -readProc ox_ips120_heat_r

	  proc ox_ips120_heat_r { ins } {
	    insIfReadVerify /${ins} "R20" 32 /${ins}/setup/heat_i "R%f" 2
	  }

	CAMP_FLOAT /~/setup/heat_time -D -S -T "Time to heat switch" \
	    -H "Time for SC switch to change state after heater turned on/off." \
	    -d on -s on -units s -v 0.0 -writeProc mag_heat_time_w

	CAMP_FLOAT /~/setup/settle_time -D -S -T "Magnet settling time" \
	    -H "Magnet settling time (for settle_set)" \
	    -d on -s on -units s -v 20.0 -writeProc mag_settle_time_w

	CAMP_FLOAT /~/setup/calibration -D -S -T "Tesla per Amp" \
	    -H "Magnet calibration, Tesla per Amp, used for setting mag_field" \
	    -d on -s on -units "T/A" -writeProc ox_ips120_calib_w

	  proc ox_ips120_calib_w { ins target } {
	    varDoSet /${ins}/setup/calibration -v [ bounded $target 0.00001 10. ]
	  }


    CAMP_STRUCT /~/controls -D -T "Control variables" -d on \
	    -H "Internal control variables: Don't set them manually."

	CAMP_SELECT /~/controls/activity -D -R -S -T "Zero or setpoint" \
	    -H "Sets PS to zero or setpoint" \
	    -d on -r on -s on -selections setpoint zero hold clamped quench \
	    -readProc ox_ips120_status_r -writeProc oxips120_activity_w

	  proc oxips120_activity_w { ins target } {
	    set a [lindex {1 2 0 4 4} $target]
	    if { [catch {insIfWriteMatch /${ins} "A${a}" A 32 1}] } {
	      insIfWrite /${ins} "\$Q4"
	      insIfWrite /${ins} "\$C3"
	      insIfWriteMatch /${ins} "A${a}" A 32 2
	    }
	    varDoSet /${ins}/controls/activity -v $target
	  }

	CAMP_SELECT /~/controls/ramp_status -D -R -P -T "Instrument Ramp status" \
	    -d on -r on -selections holding ramping \
	    -readProc ox_ips120_status_r

	CAMP_SELECT /~/controls/do_ramp -D -P -R -T "Ramping Sequencer (poll)" \
	    -H "Ramping Sequencer (polled when in operation)" \
	    -d on -r off -selections "DO RAMP" -readProc mag_do_ramp_r

	CAMP_SELECT /~/controls/fast_leads -D -R -P -S -T "Fast or slow" \
	    -H "This has no effect" \
	    -d on -r on -s on -selections "Slow Mag" "Fast leads" \
	    -readProc ox_ips120_status_r -writeProc ox_ips120_leads_w

	  proc ox_ips120_leads_w { ins target } {
	    varDoSet /${ins}/controls/fast_leads -v $target
	  }

	CAMP_FLOAT /~/controls/ramp_rate -D -S -T "Set Ramp Rate" \
	    -H "Set magnet ramp rate" \
	    -d on -s on -units "A/s" -writeProc ox_ips120_con_rr_w

	  proc ox_ips120_con_rr_w { ins target } {
	    set r1 [ varGetVal /${ins}/setup/ramp_rate ]
	    if { $r1 > 0.0 } {
		set r [expr ( $r1 < $target ) ? $r1 : $target]
	    } else {
		set r $target
	    }
	    insIfWriteMatch /${ins} [format "S%.3f" [expr 60.*$r]] S 32 3
	    varDoSet /${ins}/controls/ramp_rate -v $r
	  }

	CAMP_FLOAT /~/controls/i_persist -D -R -T "Persistent current" \
	    -d on -r on -units A -tol 0.001 -readProc ox_ips120_controls_ip_r

	  proc ox_ips120_controls_ip_r { ins } {
	    insIfReadVerify /${ins} "R16" 32 /${ins}/controls/i_persist "R%f" 2
	  }

	CAMP_FLOAT /~/controls/inductance -D -R -T "Magnet Inductance" \
	    -H "Magnet Inductance: read to identify magnet" \
	    -d on -r on -units H -readProc ox_ips120_induct_r

	  proc ox_ips120_induct_r { ins } {
	    insIfReadVerify /${ins} "R24" 32 /${ins}/controls/inductance "R%f" 2
	  }


	CAMP_STRING /~/controls/dump_file -D -S -T "Memory dump" \
	    -d on -s on  \
	    -H "Enter the filename to receive a memory dump (dat/dump/ox.dat)" \
	    -writeProc ox_ips120_dump_w

	proc ox_ips120_dump_w { ins val } {
	    insIfWrite /${ins} "\$U1"
	    insIfWrite /${ins} "\$W0"
	    set typ [insGetIfTypeIdent /${ins}]
	    switch -glob $typ {
		rs232* { set n 11 }
		gpib* { set n 7 }
		default { return -code error "Bad interface type: $typ" }
	    }
	    set insdef "\[insSet /${ins} -if $typ [insGetIfDelay /${ins}] [insGetIfTimeout /${ins}] [insGetIf /${ins}] \]"
	    insSet /${ins} -lock on
	    insIfOff /${ins}
	    expr [lreplace $insdef $n $n none ]
	    insIfOn /${ins}
	    set stat [catch { set buf [ insIfDump /${ins} $val Z2 2048 Z ] } ]
	    insIfOff /${ins}
	    expr $insdef
	    insIfOn /${ins}
	    insSet /${ins} -lock off
	    if { $stat } { return -code error "$buf" }
	    varDoSet /${ins}/controls/dump_file -m "Wrote $buf bytes" -v $val
	  }

	CAMP_STRING /~/controls/undump_file -D -S -T "Memory upload" \
	    -d on -s on  \
	    -H "Enter the filename to upload" \
	    -writeProc ox_ips120_undump_w

	proc ox_ips120_undump_w { ins val } {
	    insIfWrite /${ins} "\$W0"
	    varRead /${ins}/controls/activity
	    if { [ varGetVal /${ins}/controls/activity] != 2 } {
		return -code error "Power supply activity must be \"hold\""
	    }
	    set typ [insGetIfTypeIdent /${ins}]
	    switch -glob $typ {
		rs232* { set n 12 }
		gpib* { set n 8 }
		default { return -code error "Bad interface type: $typ" }
	    }
	    set insdef "\[insSet /${ins} -if $typ [insGetIfDelay /${ins}] [insGetIfTimeout /${ins}] [insGetIf /${ins}] \]"
	    insSet /${ins} -lock on
	    insIfOff /${ins}
	    expr [lreplace $insdef $n $n none ]
	    insIfOn /${ins}
	    insIfWrite /${ins} "\$U9999\r"
	    set stat [catch { set buf [ insIfUndump /${ins} $val "\$Y2\r" 2048 ] } ]
	    sleep 2.5
	    insIfOff /${ins}
	    expr $insdef
	    insIfOn /${ins}
	    insIfWrite /${ins} "\$Q4"
	    insIfWrite /${ins} "\$C3"
	    insIfWrite /${ins} "\$A0"
	    insIfWrite /${ins} "\$U1"
	    insSet /${ins} -lock off
	    if { $stat } { return -code error "$buf" }
	    varDoSet /${ins}/controls/undump_file -v $val
	  }

proc ox_ips120_status_r { ins } {
    set buf [insIfRead /${ins} "X" 32]
    set stat [scan $buf "X%1d%1dA%1dC%1dH%1dM%1d%1dP%1d%1d" s1 s2 a c h m1 m2 p1 p2]
    if { $stat != 9 } {
      set buf [insIfRead /${ins} "X" 32]
      set stat [scan $buf "X%1d%1dA%1dC%1dH%1dM%1d%1dP%1d%1d" s1 s2 a c h m1 m2 p1 p2]
      if { $stat != 9 } { return -code error "failed reading IPS120 status" }
    }
    varDoSet /${ins}/controls/activity -v [expr {$s1 == 1 ? 4 : [lindex {2 0 1 3 3} $a]}]
    varDoSet /${ins}/heat_status -v [lindex {1 2 1 0 0 1 0 0 0} $h]
    varDoSet /${ins}/controls/ramp_status -v [expr $m2 > 0]
    varDoSet /${ins}/controls/fast_leads -v [lindex {1 1 0 0 1 1 0 0} $m1]
    varDoSet /${ins}/setup/ramp_table -v [lindex {0 0 0 0 1 1 1 1} $m1]
}

proc ox_ips120_all_status_r { ins } {
    varRead /${ins}/controls/ramp_status
    varRead /${ins}/setup/volt_max
    insIfReadVerify /${ins} "R21" 32 /${ins}/setup/mag_min "R%f" 2
    insIfReadVerify /${ins} "R22" 32 /${ins}/setup/mag_max "R%f" 2
    insIfReadVerify /${ins} "R5" 32 /${ins}/mag_set "R%f" 2
    varRead /${ins}/fast_set
    varRead /${ins}/mag_field
    set rs [varGetVal /${ins}/controls/ramp_status]
    set hs [varGetVal /${ins}/heat_status]
    set act [varGetVal /${ins}/controls/activity]
    set rs [lindex [list $rs [expr {3+$rs*($act==0)}] 1] $hs]
    varDoSet /${ins}/ramp_status -v $rs
    if { [varGetVal /${ins}/mag_set] == 0.0 && [varNumGetNum /${ins}/mag_set] == 0 } {
	if { $hs==1 } {
	    varDoSet /${ins}/mag_set -v [varGetVal /${ins}/controls/i_persist]
	} else {
	    varDoSet /${ins}/mag_set -v [varGetVal /${ins}/fast_set]
	}
    }
    if { $rs==0 } {
	varDoSet /${ins}/mag_set -v [varGetVal /${ins}/mag_read]
    } elseif { $act==1 && $hs != 1 } {
	varDoSet /${ins}/mag_set -v 0.0
    }
    if { $rs == 0 || $rs == 3 } {
	varDoSet /${ins}/controls/do_ramp -p off
    } else {
	varSet /${ins}/mag_set -v [varGetVal /${ins}/mag_set]
    }
}

proc insIfWriteMatch { ins cmd pat buf tries } {
    set i 0
    while { [string match "${pat} " [set r "[insIfRead $ins $cmd $buf ] "]] == 0 } {
	incr i
	if { $i > $tries } { return -code error "Failed insIfWriteMatch (${tries} tries). Expected \"${pat}\" but got \"${r}\"." }
    }
}

