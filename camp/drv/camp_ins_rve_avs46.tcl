# camp_ins_rve_avs46.tcl
# 
# $Log: camp_ins_rve_avs46.tcl,v $
# Revision 1.3  2015/03/15 00:37:13  suz
# Ted's 201404 interface changes
#
# Revision 1.1  2004/01/29 06:55:22  asnd
# Initial - DR resistance bridge
#
#

CAMP_INSTRUMENT /~ -D -T "RVE AVS-46 Bridge" \
    -H "RV Electroniikka AVS-46 Resistance Bridge" -d on \
    -initProc avs_init -deleteProc avs_delete \
    -onlineProc avs_online -offlineProc avs_offline

  proc avs_init { ins } {
    global $ins
    set ${ins}(npt) 0
    set ${ins}(X1) 0
    set ${ins}(X2) 0
    set ${ins}(Y1) 0
    set ${ins}(Y2) 0
    insSet /${ins} -if rs232 0.2 2 /tyCo/1 4800 8 none 1 CR CR
  }
  proc avs_delete { ins } { insSet /${ins} -line off }
  proc avs_online { ins } { 
    insIfOn /${ins}
    catch { insIfRead /${ins} "E0;O0;C0" 32 }
    set buf ""
    catch {insIfRead /${ins} "E0;O0;C0" 32} buf
    if { $buf != ";;" } {
        insIfOff /${ins}
        return -code error "Failed readback"
    }
    set c [varGetVal /${ins}/setup/curve]
    if { [string length $c] } {
        catch { varSet /${ins}/setup/curve -v $c }
    }
    if { [varGetPollInterval /${ins}/resistance] > 0.0 || \
            [varGetPollInterval /${ins}/temperature] > 0.0 } {
        varDoSet /${ins}/resistance -p on
    }
  }
  proc avs_offline { ins } { insIfOff /${ins} }

  CAMP_FLOAT /~/resistance -D -R -P -L -T "Resistance reading" \
          -d on -r on -units Ohm \
          -readProc avs_resistance_r
  proc avs_resistance_r { ins } {
      set buf [insIfRead /${ins} "H?" 32]
      if { [scan $buf " %f %c" r c] != 1 } {
          return -code error "Invalid resistance readback: $buf"
      }
      if { $r == 9999900.0 } {
          varDoSet /${ins}/resistance -m "Overload" -v $r
          varDoSet /${ins}/temperature -m "Overload" -v 0.0
          return
      } 
      varDoSet /${ins}/resistance -m "" -v $r
      global $ins
      if { [set ${ins}(npt)] > 1 } {# then calc temperature
          if { $r < [set ${ins}(minr)] || $r > [set ${ins}(maxr)] } {
              varDoSet /${ins}/temperature -m "Overload: Resistance out of calibrated range." -v 0.0
              return
          }
          set lt [avs_calibrate $ins [expr { log10($r) } ] ]
          set T [expr { pow(10,$lt) }]
          varDoSet /${ins}/temperature -m "" -v [format %f [format %.6g $T] ]
      }
  }

  CAMP_FLOAT /~/temperature -D -R -P -L -T "Temperature reading" \
          -d off -r off -units K \
          -readProc avs_resistance_r

# avs_calibrate: 
# Use the raw resistance to calculate (log) temperature from calibration curve.
# The curve is a list of pairs { log(R) log(T) } which I will interpolate
# linearly.  Parameter x is log(R).  The resistance should have already been 
# checked to be in the calibrated range minr to maxr.  We return the calibrated
# value, which is log10(T/K)

proc avs_calibrate { ins x } {
  global $ins
  # see if this point is within previous interval
  set X1 [set ${ins}(X1)]
  set X2 [set ${ins}(X2)]
  # interpolate using previous pair, if good
  if { ($x >= $X1) && ($x <= $X2) } {
      set Y1 [set ${ins}(Y1)]
      set Y2 [set ${ins}(Y2)]
  } else {
    # Check each interval in curve, and use the proper one.
    set curve [set ${ins}(curve)]
    set X1 [lindex [lindex $curve 0] 0]
    set Y1 [lindex [lindex $curve 0] 1]
    foreach pair [lrange $curve 1 end] {
	set X2 [lindex $pair 0]
	set Y2 [lindex $pair 1]
	if { $x < $X2 } {
	    break
	}
	set X1 $X2
	set Y1 $Y2
    }
    set ${ins}(X1) $X1
    set ${ins}(X2) $X2
    set ${ins}(Y1) $Y1
    set ${ins}(Y2) $Y2
  }
  # Calculate interpolated value:
  return [expr { ( $Y2*($x-$X1) + $Y1*($X2-$x) ) / ($X2-$X1) } ]
}


  CAMP_SELECT /~/range -D -R -P -T "Range" \
          -d on -r on \
          -selections "" "2 Ohm" "20 Ohm" "200 Ohm" "2 kOhm" "20 kOhm" "200 kOhm" "2 MOhm" \
          -readProc avs_range_r
  proc avs_range_r { ins } {
      insIfReadVerify /$ins "R?" 32 /${ins}/range " %d" 2
  }


  CAMP_SELECT /~/excitation -D -R -P -T "Excitation" \
          -d on -r on \
          -selections "" "10 uV" "30 uV" "100 uV" "300 uV" "1 mV" "3 mV" \
          -readProc avs_excitation_r
  proc avs_excitation_r { ins } {
      insIfReadVerify /$ins "X?" 32 /${ins}/excitation " %d" 2
  }


  CAMP_STRUCT /~/setup -D -d on


# Curve table is .dat file which contains a line with the number of points,
# then the data in two columns:#   Temperature/K  log10(resitance/Ohm)
# I will read the calibration data in this form, but save the curve internally
# as a monotonically-increasing list of pairs:  log(R), log(T).  Temperatures
# will be determined by linear interpolation on log(T) vs log(R).

  CAMP_STRING /~/setup/curve -D -R -S -T "Calibration curve" \
          -d on -r on -s on \
          -H "Load a calibration curve for calculating temperature" \
          -writeProc avs_curve_w

  proc avs_curve_w { ins curve_name } {
      global $ins

      set ${ins}(npt) 0
      if { [string tolower $curve_name] == "none" } {
          set curve_name ""
      }
      varDoSet /${ins}/setup/curve -v $curve_name

      if { [string length $curve_name] == 0 } {
          varDoSet /${ins}/temperature -d off -r off -p off
          return
      }
      set fname "./dat/${curve_name}.dat"
      if { [catch {open $fname r} fid] } {
          return -code error "Failed to open $fname; $fid"
      }
      # first line has the number of points
      # all the others contain pairs of curve points:  Temp   log(R)
      set bff "Bad file format `$fname':"
      if {[scan [gets $fid] " %d" npoints] != 1} {
          close $fid
          return -code error "$bff first line should contain the number of points."
      }
      set rmin 99999.
      set rmax -9999.
      set npt 0
      set curve [list]
      while { $npt < $npoints } {
          if {([scan [gets $fid] " %f %f" temp logr] != 2)} {
              close $fid
              return -code error "$bff need two floats of data per line."
          }
          if { $temp <= 0.0 } {
              close $fid
              return -code error "$bff invalid temperature value $temp."
          }
          if { $rmin > $rmax } { set rmin $logr }
          if { $logr > $rmax } {
              lappend curve [list $logr [expr {log10($temp)}]]
              set rmax $logr
          } elseif { $logr < $rmin } {
              set curve [linsert $curve 0 [list $logr [expr {log10($temp)}]] ]
              set rmin $logr
          } else {
              close $fid
              return -code error "$bff curve is not monotonic."
          }
          incr npt
      }
      close $fid
      set ${ins}(curve) $curve
      set ${ins}(npt) $npt
      set ${ins}(X1) 1.0
      set ${ins}(X2) 0.0
      set ${ins}(maxr) [expr {pow(10,$rmax)}]
      set ${ins}(minr) [expr {pow(10,$rmin)}]
      varDoSet /${ins}/temperature -d on -r on
  }


