CAMP_INSTRUMENT /~ -D -T "SEN OR2088 Output Register" \
    -H "SEN OR2088 Output Register" -d on \
    -initProc or2088_init \
    -deleteProc or2088_delete \
    -onlineProc or2088_online \
    -offlineProc or2088_offline 
  proc or2088_init { ins } { insSet /${ins} -if camac 0.0 0.0 0 0 0 } 
  proc or2088_delete { ins } { insSet /${ins} -line off }
  proc or2088_online { ins } {
    insIfOn /${ins}
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }
  proc or2088_offline { ins } { insIfOff /${ins} }
    CAMP_INT /~/output_set -D -S -L -T "Output setting" -d on -s on \
      -writeProc or2088_set_w
      proc or2088_set_w { ins target } {
	set target [expr $target & 0xffff]
	cdreg ext [insGetIfCamacB /${ins}] [insGetIfCamacC /${ins}] [insGetIfCamacN /${ins}] 0
	insIfWrite /${ins} "cfsa 16 $ext target q"
        for { set i 0 } { $i <= 15 } { incr i } {
	  varDoSet /${ins}/output${i} -v [expr ($target>>$i)&1]
        }        
	varDoSet /${ins}/output_set -v $target
      }
    for { set i 0 } { $i <= 15 } { incr i } {
      CAMP_INT /~/output${i} -D -L -T "Output $i" -d on
    }
    CAMP_STRUCT /~/setup -D -T "Setup variables" -d on
      CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
	    -selections FALSE TRUE -readProc or2088_id_r
        proc or2088_id_r { ins } {
          set id 0
          if {[catch {varSet /${ins}/output_set -v 0}] == 0} {set id 1}
          varDoSet /${ins}/setup/id -v $id
        }
