# TO DO:
#
# * A setup command to disable panel display

# ---------
# NOTES:

# *  The `manual' and page numbers refer to the "7061 SYSTEMS VOLTMETER, 
# Operating Manual".  Syd Kreitzman should have it.

# *  If you make any changes to the configuration switches inside the
# physical instrument, please remember to update this driver.

# *  The only `sensor' types for which voltage is measured are `Thermocouple'
# and, of course, `V_Raw'.  For all the others, 4 wire measuring is used, 
# regardless of the name.

# In all the 4 wire measuring modes for real sensors, NEVER set the range 
# to `Auto' because that changes the applied current and we want to keep it
# under control:  A too large current would heat the sensor, which can 
# lower the resistance (FourWireResistors have a negative slope R vs T), 
# which requires a larger current... creating positive feedback and runaway 
# heating.

# Instrument defaults are "track on" and "range auto" (Appendix A in the 
# instrument's manual), which are appropriate for stand-alone operation
# and are used for the V_Raw and R_Raw `sensor' types.
# These are set to "track off" and "range 0.1" in the `online' function.

# For diodes: the measuring current should always be 10 uA (R4 or R5), page 7.3
#             the measured resistance should then be converted to voltage.

# *  The display on the instrument is 20 chars long; chars with index > 20 won't
# be displayed.

# Acronyms & Abbreviations:
# ins = instrument , ch = channel , disp = display , curve = calibration curve
# temp = temperature (and not temporary)

# ** GLOBALS **
# ins_name : string
# Sol_SensorTypes : array indexed on sensor numbers (as returned by CAMP)
# Sol_read_units : indexed on sensor types
#             The units as they must be set on the instrument, like "Trueohm"
# Sol_disp_units : indexed on sensor types
#             fixed for each sensor when reading without a calibration curve,
#             and "K" for all sensors when calibration curves are in use.
#             Sol_disp_units are the same units as those for the curve domain.
# Sol_range : array indexed on the range name: Auto,R1...R6
#             These are the abbreviations used by cryptic commands.
#             (see the Reference Card of the voltmeter - there are two of them in 
#             the manual). Used only by the proc that does the physical reading.
# Sol_channel(ch#,raw_read): Most recent reading, in raw units.
# Sol_channel(ch#,curve) : Name and type of curve; index into Sol_curve.
#             /${ins}/setup/channel_#/curve contains only the name of the curve.
#             (Null means no conversion performed.)
# Sol_channel(ch#,previous) : location in curve where we found the previous
#             reading (remembered for efficiency when readings do not change much).
# Sol_channel(ch#,range) : Range value for each channel
# Sol_curve(name,type): assoc array indexed on curve name an type --
#             Each value is a pair of numbers; the first in raw units (V, KOHM, etc)
#             and the second in Kelvin.  Defined when a calibration curve is selected.
#  _____
# |	   	|
# |	ins	|--> triggered_read --> ch_read --> sol_convert --> map_curve ---> read_process_set_display
# |_____| 
#        

# For each channel:
#   sensor_type must be chosen before doing anything

CAMP_INSTRUMENT /~ -D -T "Solartron 7061 systems voltmeter" \
    -H "Solartron 7061 volt/amp/ohm meter" -d on \
    -initProc solartron7061_init \
    -deleteProc solartron7061_delete \
    -onlineProc solartron7061_online \
    -offlineProc solartron7061_offline
proc solartron7061_init { ins } {
  global ins_name Sol_SensorTypes Sol_read_units Sol_disp_units Sol_range Sol_curve Sol_channel

  set ins_name "Solartron 7061 systems voltmeter"

  set Sol_SensorTypes(0) NONE
  set Sol_SensorTypes(1) Diode
  set Sol_SensorTypes(2) FourWireResistor
  set Sol_SensorTypes(3) PTR
  set Sol_SensorTypes(4) Thermocouple
  set Sol_SensorTypes(5) V_Raw
  set Sol_SensorTypes(6) R_Raw

  set Sol_read_units(NONE) ""
  set Sol_disp_units(NONE) ""
  set Sol_read_units(Diode) KOHM
  set Sol_disp_units(Diode)    V
  set Sol_read_units(FourWireResistor) KOHM
  set Sol_disp_units(FourWireResistor)  OHM
  set Sol_read_units(PTR) Trueohm
  set Sol_disp_units(PTR) OHM
  set Sol_read_units(Thermocouple) VDC
  set Sol_disp_units(Thermocouple)  mV
  set Sol_read_units(V_Raw) VDC
  set Sol_disp_units(V_Raw) V
  set Sol_read_units(R_Raw) Trueohm
  set Sol_disp_units(R_Raw) OHM

  set Sol_range(Auto)  auto
  set Sol_range(R1)     0.1
  set Sol_range(R2)     1
  set Sol_range(R3)    10
  set Sol_range(R4)   100
  set Sol_range(R5)  1000
  set Sol_range(R6) 10000

  set Sol_curve() ""

  for {set i 0} {$i < 10} {incr i} { 
    set Sol_channel($i,raw_read) 0.0
    set Sol_channel($i,curve) ""
  }

  insSet /${ins} -if gpib 0.5 5 16 LF LF
}
proc solartron7061_delete { ins } {
  insSet /${ins} -line off
}
proc solartron7061_online { ins } {
  insIfOn /${ins}
# the following line makes the gpib reads to hang
#  insIfWrite /${ins} "initialize"
#
# The last character output gets put at the start of the next output, so
# put a space at the end (beginning) of all output!
  if { [catch {
      insIfWrite /${ins} "gpib delim 32 eoi"
      varRead /${ins}/setup/id
    } ] || [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
  }
  insIfWrite /${ins} "track off:range 0.1: display on:format dvm:literal off"
  sol7061_init_channels $ins
}
proc solartron7061_offline { ins } {
  insIfOff /${ins}  # mandatory call
}

# Re-initialize all channels after coming online.  Re-load any
# curves that are missing, and turn on polling where it eas wanted.
# This is necessary when recovering from a Camp system reload.
proc sol7061_init_channels { ins } {
  for { set i 0 } { $i < 10 } { incr i } {
    set s [varGetVal /${ins}/setup/channel_${i}/sensor_type]
    catch {
      set c [varGetVal /${ins}/setup/channel_${i}/curve]
      set p [varGetPollInterval /${ins}/channel_${i}]
      varSet /${ins}/setup/channel_${i}/sensor_type -v $s
      if { $s > 0 } {
        if { [string length $c] } {
          varSet /${ins}/setup/channel_${i}/curve -v $c
        }
        if { $p > 0.0 } {
          varDoSet /${ins}/channel_${i} -p on
        }
      }
    }
  }
}

#----------------------------------------------------------------------------
CAMP_STRUCT /~/setup -D -d on
CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc sol7061_id_r
proc sol7061_id_r {ins} {
  set id 0
  if { [catch {insIfRead /${ins} "GPIB?" 80} buf] == 0} {
# C   set id [scan $buf "%*cGPIB %s" ignored]
# C   if {$id != 1} {set id 0}
# S    set status [scan $buf " %s" string1]
# S    if {$status != 1} {set id 0}
     set id [ expr [string first "GPIB" $buf]  != -1 ]
  }
  varDoSet /${ins}/setup/id -v $id
}

# Set up channels 0-9 in a for-loop.
# Channels start out with -r off,  They are turned on when
# a sensor is set up.

for { set i 0 } { $i < 10 } { incr i } {
  CAMP_FLOAT /~/channel_$i -D -d off -R -r off -P -L \
      -T "Channel ${i} reading" \
      -H "Read and display channel ${i}" \
      -readProc [list read_process_set_display $i]
  #
  CAMP_STRUCT /~/setup/channel_${i} -D -d on -T "Configure channel ${i}"
  # `-selections' here should be the same and in same order as Sol_SensorTypes
  # defined in `proc solartron7061_init' above
  
  CAMP_SELECT /~/setup/channel_${i}/sensor_type -D -S \
      -d on -s on \
      -selections NONE Diode FourWireResistor PTR Thermocouple V_Raw R_Raw \
      -writeProc [list set_sensor_type $i]
  
  CAMP_STRING /~/setup/channel_${i}/curve -D -S \
      -d on -s on \
      -H "Select the conversion table to give temperature in K" \
      -writeProc [list set_curve_w $i]
  
  CAMP_SELECT /~/setup/channel_${i}/zeroing_chan -D -S \
      -d on -s on -v 0 \
      -selections NONE 0 1 2 3 4 5 6 7 8 9 \
      -H "Set which channel measures a zeroing offset." \
      -writeProc [list set_zero_chan_w $i]
  
  CAMP_SELECT /~/setup/channel_${i}/panel_display  -D -S \
      -d on -s on \
      -H "Display reading on front panel" \
      -selections No Yes \
      -writeProc [list set_panel_display $i]
}


CAMP_STRING /~/setup/command -D -S -T "Arbitray command" -d on -s on\
    -H "Send an arbitray command to the instrument"\
    -writeProc solartron7061_cmd_w

proc solartron7061_cmd_w { ins str } {
  varDoSet /${ins}/setup/command -v [insIfWrite /${ins} ${str}]
}

# --------- procedures used by some of the camp variables above ---------

#called by /~/channel_$ch
proc read_process_set_display {ch ins} {
  global Sol_channel Sol_disp_units Sol_read_units
  set sensortype [get_sensortype $ch $ins]
  set ru $Sol_disp_units($sensortype)
  if {[varGetVal /${ins}/setup/channel_${ch}/curve] == ""} {
    # there isn't any curve defined for this channel
    set read_val [read_wout_curve $ins $ch]
    set du $ru
    varDoSet /${ins}/channel_$ch -v $read_val -units $ru -m \
            "Channel ${ch} raw reading"
  } else {
    set read_val [read_with_curve $ins $ch]
    set du K
    set C [format {%.2f} [expr {$read_val - 273.15}] ]
    varDoSet /${ins}/channel_$ch -v $read_val -units K -m \
            "Channel ${ch} temperature = $C C."
  }
  if { [varGetVal /${ins}/setup/channel_${ch}/panel_display] } {
    set read_val [expr [format %.7g $read_val] ]
    insIfWrite /${ins} "display \[CH $ch = $read_val $du\]" 
  }
}
# used by: read_process_set_display
proc read_wout_curve {ins ch} {
  return [sol_convert $ch $ins [ch_read $ins $ch]]
}
# used by: read_process_set_display
proc read_with_curve {ins ch} {
  return [ map_curve $ch $ins [read_wout_curve $ins $ch]]
}

# hand-made `autorange' adaptive change to the range should be done here
# dito for averaging by doing multiple reads
# For FourWireResistor :
# breakpoins at 1.0 and 0.1 KOHM hurts precision badly and delay reading of value a lot
# (camp server won't get to answer in time)
# they were initially at 2000 and 20 KOHM and seemed to work better (got more digits) 

# used by: read_wout_curve (indirectly by read_with_curve)
proc ch_read {ins ch} {
  global ins_name Sol_SensorTypes Sol_channel Sol_read_units Sol_range
  # $channel($ch,range) $Sol_read_units($sensortype)
  set sensortype_num [varGetVal /${ins}/setup/channel_${ch}/sensor_type]
  set sensortype $Sol_SensorTypes($sensortype_num)
  set theMode  $Sol_read_units($sensortype)
  set theRange $Sol_range($Sol_channel($ch,range))

  set read_val [triggered_read $ins $ch $theRange $theMode]
  switch $sensortype {
	Diode {
	  return $read_val
	}
	FourWireResistor {
	  if {1.0 < $read_val} {
		set Sol_channel($ch,range) R6
	  } elseif {0.1 < $read_val} {
		set Sol_channel($ch,range) R5
	  } else {
		set Sol_channel($ch,range) R3
	  }
          if { $Sol_range($Sol_channel($ch,range)) != $theRange } {
        	set theRange $Sol_range($Sol_channel($ch,range))
        	set read_val [triggered_read $ins $ch $theRange $theMode]
          }
	  return $read_val
	}
	PTR {
	  return $read_val
	}
	Thermocouple {
	  return $read_val
	}
        V_Raw {
          return $read_val
        }
        R_Raw {
          return $read_val
        }
	default {
	  return -code error "Error in Tcl driver $ins_name. Reached default switch branch in `ch_read'. Please report this to Data Acquisition."
	}
  }
}
# used by: ch_read.  When we get a bad (or zero) readback, try again -- ONCE.
proc triggered_read { ins ch theRange theMode } {
  set prompt "chan $ch:range $theRange:mode $theMode:trigger"
  if {[catch {insIfRead /${ins} $prompt 40} buf] } {
    set buf ""
  }
  if {[scan $buf " %f" result] != 1 } {
    set result 0.0
  }
  if { $result != 0.0 } { return $result }
  # Zero result or failed reading:  Try ONCE more.
  if {[catch {insIfRead /${ins} $prompt 40} buf] } {
    return -code error "Couldn't read from the instrument."
  }
  if {[scan $buf " %f" result] != 1 } {
    return -code error "Couldn't parse the instrument's output ($buf)"
  }
  return $result
}

# sol_convert: convert really-raw readback into displayed "raw" units.
# If there is a zeroing channel and sensor types match, apply the zeroing 
# correction here, on the raw units.
# used by: map_curve read_wout_curve
proc sol_convert {ch ins raw_val} {
  global Sol_SensorTypes Sol_channel
  set is [varGetVal /${ins}/setup/channel_${ch}/sensor_type]
  set sensortype $Sol_SensorTypes($is)
  switch $sensortype {
    NONE {
      return -code error "Error in Tcl driver for $ins_name. No conversion possible for sensor type NONE."
    }
    # KOHM -> V at a 10uA reading current (for range R4)
    Diode { set v [expr $raw_val / 100] }
    # KOHM -> OHM
    FourWireResistor { set v [expr $raw_val * 1000] }
    # KOHM -> OHM
    PTR { set v [expr $raw_val * 1000] }
    # V -> mV
    Thermocouple { set v [expr $raw_val * 1000] }
    # V -> V
    V_Raw { set v $raw_val }
    # KOHM -> OHM
    R_Raw { set v [expr $raw_val * 1000] }
    default {
      return -code error "Error in Tcl driver for $ins_name. No such sensor type: $sensortype."
    }
  }
  set Sol_channel($ch,raw_read) $v
  # Now do zeroing: if sensor types match subtract raw reading
  set z [varGetVal /${ins}/setup/channel_${ch}/zeroing_chan]
  # NOTE: $z is a selection index, which is the channel number plus 1
  if { $z } {
    incr z -1
    if { [raw_sensors_match $ch $z] } {
      set v [expr { $v - $Sol_channel($z,raw_read) }]
    }
  }
  return $v
}
# map_curve: Use result of sol_convert ($x, in raw units) to calculate temperature
# from a calibration curve.  If there is a zeroing channel, and the sensors/curves 
# do NOT match, then apply zero correction here, in Kelvin
# used by: read_with_curve
proc map_curve { ch ins x } {
  # curves are lists of pairs
  # pairs are lists of two elements: read value(KOHM,V,etc) and temperature (K)
  # assumption: curves are monotonic; this is checked when the curve is read in (`set_curve_w')
  # Even if they monotonically decrease in the curve file, they are stored in the `curve'
  # list monotonically increasing.
  global Sol_channel Sol_curve
  set curve   $Sol_curve($Sol_channel($ch,curve))
  set prelind $Sol_channel($ch,previous)
  # try between previous points in curve
  set X1 [lindex [lindex $curve $prelind] 0]
  set X2 [lindex [lindex $curve [expr $prelind+1]] 0]
  # interpolate using previous pair, if good
  if { ($x >= $X1) && ($x <= $X2) } {
      set Y1 [lindex [lindex $curve $prelind] 1]
      set Y2 [lindex [lindex $curve [expr $prelind+1]] 1]
  } else {
    # Not in previous interval.  Check if in entire range. 
    # (vxWorks Tcl does not accept [lindex {...} end]
    set index_last [expr [llength $curve] - 1]
    set X1 [lindex [lindex $curve 0] 0]
    set Y1 [lindex [lindex $curve 0] 1]
    set Xn  [lindex [lindex $curve $index_last] 0]
    # if x not inside [X1,Xn], return obviously wrong values
    set impossibly_small 0.0
    set ridiculously_large 10000.0
    if { $x < $X1 } { return $impossibly_small }
    if { $Xn < $x } { return $ridiculously_large }

    # Check each interval in curve, and interpolate the proper one.
    set prelind 0
    foreach pair [lrange $curve 1 end] {
	set X2 [lindex $pair 0]
	set Y2 [lindex $pair 1]
	if { $x < $X2 } {
	    set Sol_channel($ch,previous) $prelind
	    break
	}
	set X1 $X2
	set Y1 $Y2
	incr prelind
    }
  }
  set v [expr ( $Y2*($x-$X1) + $Y1*($X2-$x) ) / ($X2-$X1) ]
  # Now do zeroing: if did not subtract raw reading, then subtract values
  set z [varGetVal /${ins}/setup/channel_${ch}/zeroing_chan]
  # NOTE: $z is a selection index, which is the channel number plus 1
  if { $z } {
    incr z -1
    if { ! [raw_sensors_match $ch $z] } {
      set v [expr { $v - [varGetVal /${ins}/channel_${z}] }]
    }
  }
  return $v
}

#  This tests: if either channel is raw, we can only compare raw readings;
#  if both channels have identical sensors we prefer to subtract raw readings.
proc raw_sensors_match { ch z } {
    global Sol_channel
    return [ expr { $Sol_channel($ch,curve) == "" || $Sol_channel($z,curve) == "" 
                 || $Sol_channel($ch,curve) == $Sol_channel($z,curve) } ]
}

# called by /~/setup/channel_$ch/sensor_type
proc set_sensor_type {ch ins sensortype_num} {
  global ins_name Sol_SensorTypes Sol_disp_units Sol_channel
  # Set the sensor type and channels parameters regardless of what the channel
  # reads, but give an error message to the experimenter when reality doesn't
  # check.
  varDoSet /${ins}/setup/channel_${ch}/sensor_type -v $sensortype_num
  set sensortype $Sol_SensorTypes($sensortype_num)
  # Enable this channel (it begins disabled)
  varDoSet /${ins}/channel_$ch -d on -r on
  # unset curve name for this channel; if sensor type is modified, the curve has to be re-set
  varDoSet /${ins}/setup/channel_${ch}/curve -v ""
  set Sol_channel($ch,curve) ""
  set Sol_channel($ch,previous) 0
  varDoSet /${ins}/channel_$ch -units $Sol_disp_units($sensortype)

  switch $sensortype {
    NONE {
      varDoSet /${ins}/channel_$ch -d off -r off
      return
    }
    Diode {
      set Sol_channel($ch,range) R4
      insIfWrite /${ins} "track off"
      set read_val [read_wout_curve $ins $ch]
      if {$read_val < 1} {
        return -code error "Warning: voltage on diodes at 10 uA should be over 1 $Sol_disp_units($sensortype), but this one reads $read_val $Sol_disp_units($sensortype)"
      }
    }
    FourWireResistor {
      # the range of FourWireResistors changes during mesurements between {R3,R5,R6}
      # pick R6 to start with (it has the smallest current)
      set Sol_channel($ch,range) R6
      insIfWrite /${ins} "track off"
      set read_val [read_wout_curve $ins $ch]
      if {($read_val < 500) || (50000 < $read_val)} {
        return -code error "Warning: resistance of four wire resistors should be within \[500..50000\] $Sol_disp_units($sensortype), but this one reads $read_val $Sol_disp_units($sensortype)"
      }
    }
    PTR {
      set Sol_channel($ch,range) R3
      insIfWrite /${ins} "track off"
      set read_val [read_wout_curve $ins $ch]
      if {400 < $read_val} {
        return -code error "Warning: resistance of platinum resistors should be under 400 $Sol_disp_units($sensortype), but this one reads $read_val $Sol_disp_units($sensortype)"
      }
    }
    Thermocouple {
      set Sol_channel($ch,range) R1
      insIfWrite /${ins} "track off"
      set read_val [read_wout_curve $ins $ch]
      if {100 < [expr abs($read_val)]} {
        return -code error "Warning: absolute value of voltage read from thermocouples should be under 100 $Sol_disp_units($sensortype), but this one reads $read_val $Sol_disp_units($sensortype)"
      }
    }
    V_Raw {
      set Sol_channel($ch,range) Auto
      insIfWrite /${ins} "track on"
      set read_val [read_wout_curve $ins $ch]
    }
    R_Raw {
      set Sol_channel($ch,range) Auto
      insIfWrite /${ins} "track on"
      set read_val [read_wout_curve $ins $ch]
    }
    default {
      return -code error "Error in Tcl driver for $ins_name. None of the sensor types matched.  Please report this to Data Acquisition."
    }
  }
}

# called by /~/setup/channel_$ch/curve
proc set_curve_w {ch ins curve_name} {
  global Sol_channel Sol_disp_units Sol_curve

  if {$curve_name == "NONE" || $curve_name == "none" } {
    set curve_name ""
  }
  set sensortype [get_sensortype $ch $ins]
  if {$curve_name == "" } {
    varDoSet /${ins}/setup/channel_${ch}/curve -v ""
    varDoSet /${ins}/channel_$ch -units $Sol_disp_units($sensortype)
    set Sol_channel($ch,curve) ""
    return
  }
  set isens [varGetVal /${ins}/setup/channel_${ch}/sensor_type]
  set curve_type [ lindex {"" "spl" "tr" "tr" "tc" "" ""} $isens]

  if { ! [info exists Sol_curve($curve_name,$curve_type)] } {# Must read curve
    set fname [format "./dat/%s.%s" $curve_name $curve_type]
    set fid [open $fname r]

    # first line has the number of points
    # all the others contain pairs of curve points: temp x :where `x' is in Sol_disp_units(sensortype)
    set bff "Bad file format `$fname':"
    if {[scan [gets $fid] " %d" npoints] != 1} {
      close $fid
      return -code error "$bff first line should contain the number of points."
    }
    if {([scan [gets $fid] " %f %f" temp1 x1] != 2)} {
      close $fid
      return -code error "$bff need two floats in the first line of data."
    }
    if {([scan [gets $fid] " %f %f" temp2 x2] != 2)} {
      close $fid
      return -code error "$bff need two floats in the second line of data."
    }
#
    set LeftRight [expr { $x1 < $x2 }]
    set curve [list [list $x1 $temp1] [list $x2 $temp2]]
    set nlines 2
    set prev_x $x2
    while {[scan [gets $fid] " %f %f" temp x] == 2} {
      # check if curve is monotonic
      if {($LeftRight && ($x <= $prev_x)) || (! $LeftRight && ($x >= $prev_x))} {
        close $fid
        return -code error "$bff curve is not monotonic."
      }
      # curve is stored monotonically increasing
      if {$LeftRight} {
        lappend curve [list $x $temp]
      } else {
        set curve [linsert $curve 0 [list $x $temp] ]
      }
      set prev_x $x
      incr nlines
    }
    close $fid
    if {$nlines != $npoints} {
      return -code error "$bff $npoints points expected, but $nlines found."
    }
    set Sol_curve($curve_name,$curve_type) $curve
  }
# Finished loading curve from disk
#
  varDoSet /${ins}/setup/channel_${ch}/curve -v $curve_name
  varDoSet /${ins}/channel_$ch -units K
  set Sol_channel($ch,previous) 0
  set Sol_channel($ch,curve) "$curve_name,$curve_type"

  #check reality
  if {[catch {read_with_curve $ins $ch}] != 0} {
    return -code error "Warning: couldn't do a proper read of the channel.  Try doing a normal read (through the variable) to see what goes wrong."
  }
}

proc set_panel_display {ch ins val} {
  varDoSet /${ins}/setup/channel_${ch}/panel_display -v $val
}

# The zeroing channel is mainly for thermocouples, but may be of other use.
# If the sensor types are the same, and the raw measurements should be corrected,
# then the zeroing channel should have no curve defined.  If a curve is defined,
# then the temperature is (K) is subtracted.
# NOTE that the value is a selection index, and is channel_num + 1.
proc set_zero_chan_w {this_ch ins val} {
  global Sol_disp_units
  if { $val } {
    set zero_ch [expr {$val - 1}]
    set this_sens [get_sensortype $this_ch $ins]
    set zero_sens [get_sensortype $zero_ch $ins]
    if { [string compare $Sol_disp_units($this_sens) $Sol_disp_units($zero_sens)] } {
      return -code error "The raw units for channels $this_ch and $zero_ch do not match."
    }
    if { [string length [varGetVal /${ins}/setup/channel_${zero_ch}/curve]] } {
      return -code error "Channel $zero_ch is using a calibration curve.  Please set its curve to nil and retry."
    }
  }
  varDoSet /${ins}/setup/channel_${this_ch}/zeroing_chan -v $val
}

# used by read_process_set_display, set_curve_w, and set_zero_chan_w
proc get_sensortype {ch ins} {
  global Sol_SensorTypes
  set sensortype $Sol_SensorTypes([varGetVal /${ins}/setup/channel_${ch}/sensor_type])
  if { $sensortype == "NONE"} {
      return -code error "No sensor type defined for channel $ch.  Please define one first."
  }
  return $sensortype
}

# Revision 2.3   2003/05/15  D. Arseneau  Retry on glitchy readback (rename single_trigger 
# to triggered_read).  Resume better after Camp reload.  Save curves in Sol_curve.
# Allow zero subtraction when sensors not same.
#
# Revision 2.2   2003/05/10  D. Arseneau  Add zeroing channel
#
# Revision 2.0   2000/05/03 23:05:00  D. Arseneau 
# Use TCL loop to define channels, rewrite a lot, put space in delimiter string.
# Make undefined channels invisible.
#
# Revision 1.18  1997/12/03 17:41:00  D. Arseneau
# Separate Raw "sensors", remember old interval, optional display, 
# curves stored increasing
#
# Revision 1.17  1997/10/04 19:55:14  D. Arseneau
# Modify TC limit and allow negative mV; fix `map_curve' interpolation (but still linear)
# Fix display of units.
#
# Revision 1.16  1997/04/18 21:09:26  constant
# Modified `map_curve', `ch_read', `sol_convert', maybe others
# Seems to work.
#
# Revision 1.15  1997/04/11 22:07:39  constant
# Added `raw_measurement'
#
# Revision 1.14  1997/04/10 18:26:43  constant
# Stable,working version.  Major changes ahead.
#
# Revision 1.13  1997/03/19 19:27:33  constant
# Added hand-made auto-range for Diodes and FourWireResistors in `ch_read' and
# moved "trigger" in `triggered_read'.
#
# Revision 1.12  1997/03/18 20:04:40  constant
# More corrections and changes.
#
# Revision 1.11  1997/03/18 18:37:00  constant
# More comments.
#
# Revision 1.10  1997/03/18 17:23:06  constant
# Exploring/testing the RCS and Emacs functionality.
#
# Revision 1.9  1997/03/18 17:16:27  constant
# Added version control headers: Id stamps and Log entries.
#
