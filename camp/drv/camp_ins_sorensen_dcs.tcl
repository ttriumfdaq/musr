# camp_ins_sorensen_dcs.tcl 
# Camp Tcl instrument driver for Sorensen DCS series of power supplies.
# Donald Arseneau,  TRIUMF
#
# $Log: camp_ins_sorensen_dcs.tcl,v $
# Revision 1.9  2018/06/13 01:35:41  asnd
# Accumulated changes. Restoration and display of settings mostly.
#
# Revision 1.8  2017/06/21 04:20:44  asnd
# Always ramp the setpoint, even for disable.
#
# Revision 1.7  2016/06/30 02:32:08  asnd
# Automatic enable/disable of output
#
# Revision 1.6  2015/09/11 03:59:14  asnd
# Get rid of spurious errors
#
# Revision 1.5  2015/04/18 04:56:40  asnd
# Restore changes from Rev 1.2, plus two trivial edits.
#
# Revision 1.4  2015/03/15 00:35:51  suz
# Ted's 201404 interface changes
#
# Revision 1.3  2015/03/15 00:20:21  suz
# change made before 201404 Ted interface changes == accidental return to Rev 1.1
#
# Revision 1.2  2013/05/21 04:15:39  asnd
# Going online, maybe restore the previous voltage setting.
#
# Revision 1.1  2007/02/25 03:33:04  asnd
# Initial version
#

CAMP_INSTRUMENT /~ -D -T "Sorensen DCS" \
    -H "Sorensen DCS power supply" \
    -d on \
    -initProc dcs_init \
    -deleteProc dcs_delete \
    -onlineProc dcs_online \
    -offlineProc dcs_offline

# Here are functions to do insIfReadVerify and insIfWriteVerify, but read and report
# instrument's error message on failure. (Currently, ntry is fixed at 2, but could
# become a parameter.)
#
proc dcsWriteVerify { pins setc readc buff var fmt target } {
  set ntry 2
  set m ""
  for { set i 1 } { $i <= $ntry } { incr i } {
      if { $i == $ntry } { insIfWrite $pins "*CLS"; sleep 0.4 }
      if { 0 == [catch { insIfWriteVerify $pins $setc $readc $buff $var $fmt 1 $target 0.002 } m ] } {
          return
      }
  }
  if { 0 == [catch {insIfRead $pins "SYST:ERR?" 80} err] } {
      if { [scan $err "%d, %\[^\r]" n m ] == 2 } {
          if { $n < 0 } { return -code error "($n) $m" }
      }
  }
  if { [string length $m] == 0 || [string match "*No error*" $m] } {
      varRead $var
  } else {
      return -code error $m
  }
}

proc dcsReadVerify { pins readc buff var fmt } {
  set ntry 2
  set m ""
  for { set i 1 } { $i <= $ntry } { incr i } {
      if { $i == $ntry } { insIfWrite $pins "*CLS" ; sleep 0.4 }
      if { 0 == [catch { insIfReadVerify $pins $readc $buff $var $fmt 1 } m ] } {
          return
      }
  }
  if { 0 == [catch {insIfRead $pins "SYST:ERR?" 80} err] } {
      if { [scan $err "%d, %\[^\r]" n m ] == 2 } {
          if { $n < 0 } { return -code error "($n) $m" }
      }
  }
  if { [string length $m] == 0 || [string match "*No error*" $m] } {
      return
  } else {
      return -code error $m
  }
}


proc dcs_init { ins } {
    insSet /$ins -if tcpip 0.15 -1 142.90.154.141 9221 CRLF CRLF
    #insSet /$ins -if rs232 0.15 1 /tyCo/2 19200 8 none 1 LF CRLF
}
proc dcs_delete { ins } {
    insSet /$ins -line off
}

# Read ID and status. Suggest interface parameters on error.
# use ~/actions/initialize to read or re-apply limits and setpoints.

proc dcs_online { ins } {
    insIfOn /$ins
    varDoSet /$ins/actions/pending -v 0 -p off
    varRead /$ins/setup/id
    if { [varGetVal /$ins/setup/id] == "?" || [catch {varRead /$ins/status}] } {
        insIfOff /$ins
	if { [string match "rs232*" [insGetIfTypeIdent /$ins]] && \
		 ! [string match "* 19200 8 none 1 LF CRLF" [insGetIf /$ins]] } {
	    return -code error "failed ID query, check [insGetIfRs232Name /$ins] connection and try parameters 19200 8 none 1 LF CRLF"
	} else {
	    return -code error "failed ID query, check interface definition and connections"
	}
    }
    varDoSet /$ins/actions/initialize -p on -p_int 0.5 -v "Pending"
}
proc dcs_offline { ins } {
    insIfOff /$ins
}

# Polarity is ignored here because our models do not include the
# polarity switching unit (although they do have a remote setting
# for polarity, and we can add a polarity switch externally). 
# To support polarity, we would detect reversal of sign, in which 
# case: disable output, reverse polarity, enable output. Setting
# the isolation relay in parallel to the enable/disable. Current
# setting and current read is always positive for the supply, so 
# we would have to fake the sign. On the other hand, voltage setting
# and reading is signed for the supply control, and the sign must 
# match the polarity setting.
#
# The initial value is impossible, and is a flag to indicate uninitialized 
# state (see actions/initialize).

CAMP_FLOAT /~/curr_set -D -S -R -P -L -T "Set current" \
    -d on -s on -r on -units A -v 1234.5 \
    -readProc dcs_curr_set_r -writeProc dcs_curr_set_w

# Always read, but only report current setting when output is enabled. 
# When disabled, show previous setpoint (not the zero automatically appled 
# when disabling).

proc dcs_curr_set_r { ins } {
    varRead /$ins/actions/curr_set
    if { [varGetVal /$ins/output] && ![varGetVal /$ins/actions/pending]} {
        set var "actions/curr_set"
    } else {
        set var "curr_set"
    }
    varDoSet /$ins/curr_set -v [varGetVal /$ins/$var]
}

# Check target against limit without reading limit, in case the supply
# has lost the proper limit.
# When setting current, perhaps perform autoenable. Always ramp.
# Possibly set starting current based on reading.
proc dcs_curr_set_w { ins target } {
    set target [format %.3f $target]
    set Imx [expr [varGetVal /$ins/setup/maximum_curr]]
    if { $target < 0.0 || $target > $Imx } {
        return -code error "Current setting $target out of range (0.0-$Imx)"
    }
    set Vsp [varGetVal /$ins/volt_set]
    set Isp [varGetVal /$ins/curr_set]
    varDoSet /$ins/curr_set -v $target
    varDoSet /$ins/actions/pending -v 0 -p off
    if { [varGetVal /$ins/setup/autoenable] } {
        set olden [varGetVal /$ins/actions/endisable]
	set newen [expr { $target != 0.0 && $Vsp != 0.0 }]
        if { $olden } {
            set Ir [varRead /$ins/curr_read; varGetVal /$ins/curr_read]
	    if { abs($Ir-$Isp) > 0.02*$Imx && $Ir < $Imx && $Ir >= 0.0 } {
		dcs_do_curr_set $ins [format %.3f $Ir]
	    }
            if { !$newen } {
                varDoSet /$ins/actions/pending -v 1 -p on -p_int 3
            }
            dcs_do_curr_ramp $ins $target
        } elseif { $newen } {
            varSet /$ins/output -v 1
        }
    } else { # not autoenable
        dcs_do_curr_ramp $ins $target
    }
    varDoSet /$ins/status -p on -p_int 2
}

CAMP_FLOAT /~/curr_read -D -R -P -L -A -T "Read current" \
	-H "Read power supply output current" \
	-d on -r on -units A -tol 0.004 -readProc dcs_curr_read_r

proc dcs_curr_read_r { ins } {
    dcsReadVerify /$ins "MEAS:CURR?" 32 /$ins/curr_read " %f"
    if { [varGetVal /$ins/output] } {
        varTestAlert /$ins/curr_read [varGetVal /$ins/curr_set]
    }
}

CAMP_FLOAT /~/volt_set -D -S -R -P -L -T "Set voltage" \
    -d on -s on -r on -units V -v 1234.5 \
    -readProc dcs_volt_set_r -writeProc dcs_volt_set_w

proc dcs_volt_set_r { ins } {
    dcsReadVerify /$ins "SOUR:VOLT?" 32 /$ins/volt_set " %f"
}

proc dcs_volt_set_w { ins target } {
    set target [format %.3f $target]
    if { $target < 0.0 || $target > [varGetVal /$ins/setup/maximum_volt] } {
        return -code error "Voltage setting $target out of range"
    }
    set Vsp [varGetVal /$ins/volt_set]
    set Isp [varGetVal /$ins/curr_set]
    varDoSet /$ins/volt_set -v $target
    if { [varGetVal /$ins/setup/autoenable] } {
        varDoSet /$ins/actions/pending -v 0 -p off
	set newen [expr { $target != 0.0 && $Isp != 0.0 }]
	set olden [varGetVal /$ins/actions/endisable]
        if { $Isp == 0.0 } {
            varSet /$ins/actions/endisable -v 0
            dcs_do_volt_set $ins $target
        } elseif { $olden } {
            dcs_do_volt_ramp $ins $target 5
        } else {
            dcs_do_volt_set $ins $target
        }
	if { $newen && !$olden } { varSet /$ins/output -v 1 }
	if { !$newen && $olden } { varDoSet /$ins/actions/pending -v 1 -p on -p_int 3 }
    } else {
        dcs_do_volt_ramp $ins $target 5
    }
    #varRead /$ins/status
    varDoSet /$ins/status -p on -p_int 2
}

proc dcs_do_volt_set { ins target } {
    dcsWriteVerify /$ins "SOUR:VOLT $target" "SOUR:VOLT?" 32 \
        /$ins/volt_set " %f" $target
}

proc dcs_do_volt_ramp { ins target rt } {
    if { [catch {insIfWrite /$ins "SOUR:VOLT:RAMP $target $rt"}] } {
	sleep 0.1
	insIfWrite /$ins "SOUR:VOLT:RAMP $target $rt"
    }
    varDoSet /$ins/volt_set -v $target
    varDoSet /$ins/status -v "Ramping"
}

CAMP_FLOAT /~/volt_read -D -R -P -L -A -T "Read output voltage" \
	-H "Read power supply output voltage" \
	-d on -r on -units V -tol 0.004 -readProc dcs_volt_read_r

proc dcs_volt_read_r { ins } {
    dcsReadVerify /$ins "MEAS:VOLT?" 32 /$ins/volt_read " %f"
    varTestAlert /$ins/volt_read [varGetVal /$ins/volt_set]
}

# Output enable/disable, with safe ramping. The raw instrument control is actions/endisable
CAMP_SELECT /~/output -D -S -R -P -T "Output" \
    -H "Output - enabled or disabled" \
    -selections Disable Enable \
    -d on -s on -r on \
    -readProc dcs_status_r -writeProc dcs_output_w

proc dcs_output_w { ins enable } {
    varDoSet /$ins/actions/pending -v 0 -p off
    set Vsp [varGetVal /$ins/volt_set]
    set Isp [varGetVal /$ins/curr_set]
    set enabled [varGetVal /$ins/actions/endisable]
    if { $enable } {
        if { !$enabled && $Vsp != 0.0 && $Isp != 0.0 } {
            if { abs([varGetVal /$ins/actions/curr_set]) > 0.2 } {
                dcs_do_curr_set $ins 0.0
            }
            varSet /$ins/actions/endisable -v 1
            dcs_do_curr_ramp $ins $Isp
        } else {# safely get sensible
            varSet /$ins/actions/endisable -v 1
            dcs_do_curr_ramp $ins $Isp
        }
    } else {# disable
        if { $enabled } {
            dcs_do_curr_ramp $ins 0.0
            varDoSet /$ins/actions/pending -v 1 -p on
        } else {# repeat disable setting
            varSet /$ins/actions/endisable -v 0
        }
    }
    varDoSet /$ins/status -p on -p_int 2
}

CAMP_SELECT /~/status -D -R -P -T "Status" \
    -H "Indicator for power supply status" \
    -selections {Hold current} {Hold voltage} {Ramping} {Shutdown} {Overvoltage} {Overtemp} {Isolated} \
    -d on -r on -p on -p_int 10 \
    -readProc dcs_status_r

# Read status.  The status block contains most information in one readback
# (sf = status flags, sr = status register) but the ramp status must be read
# separately.  Poll status rapidly when ramping or when output status disagrees
# with output setting, and poll 9sec otherwise.

proc dcs_status_r { ins } {
    set buf [insIfRead /$ins "SOUR:STAT:BLOCK?" 200]
    if { [scan $buf {%d,%d,%x,%x,%x,%x,%x,%x,%[^,],%f,%f,%f,} cn ol sf sr as fmr fr er sn mv mc mov] != 12 } {
        return -code error "Failed to read status"
    }
    set o [varGetVal /$ins/output]
    set p 9
    set en [expr { ($sf & 0x800) ? 1 : 0 }]
    varDoSet /$ins/setup/maximum_volt -v $mv
    varDoSet /$ins/setup/maximum_curr -v $mc
    varDoSet /$ins/output -v $en
    varDoSet /$ins/actions/endisable -v $en
    if { $sr & 0x08 } {
        set stat "Overvoltage"
    } elseif { $sr & 0x10 } {
        set stat "Overtemp"
    } elseif { ($sr & 0x20) || ! ($sf & 0x800) } {
        set stat "Shutdown"
	if { $o } { set p 1 }
    } elseif { ($sf & 0x080) == 0 } {
        set stat "Isolated"
    } else { 
        # When ramping, *both* SOUR:CURR:RAMP and SOUR:VOLT:RAMP say so, so we
        # only need to read one of them.
        set buf [insIfRead /$ins "SOUR:CURR:RAMP?" 32]
        set r 0
        scan $buf %d r
        if { $r } {
            set stat "Ramping"
	    set p 1
	    varRead /$ins/curr_read
        } elseif { $sr & 0x01 } {
            set stat "Hold voltage"
        } else {
            set stat "Hold current"
        }
	if { !$o } { set p 1 }
    }
    varDoSet /$ins/status -v $stat -p on -p_int $p
}

#
#----------------------  setup  --------------------------------------------------
#
CAMP_STRUCT /~/setup -D -d on -T "Setup variables" \
    -H "Set power supply parameters here"

CAMP_STRING /~/setup/id -D -R -T "ID Query" -d on -r on \
    -v "?" -readProc dcs_id_r

proc dcs_id_r { ins } {
    set id 0
    set status [catch {insIfRead /$ins "*IDN?" 80} buf]
    if { $status == 0 } {
        set id [scan $buf " SORENSEN,DCS%d-%d," V I]
    }
    if { $id != 2 } { # Retry once
	sleep 0.1
        set status [catch {insIfRead /$ins "*IDN?" 80} buf]
        if { $status == 0 } {
            set id [scan $buf " SORENSEN,DCS%d-%d," V I]
        }
    }
    if { $id == 2 } {
        varDoSet /$ins/setup/id -v "DCS$V-$I"
    } else {
        varDoSet /$ins/setup/id -v "?"
    }
}

CAMP_FLOAT /~/setup/curr_limit -D -S -R -T "Soft current limit" \
    -d on -s on -r on -units A -v 1234.5 \
    -readProc dcs_curr_lim_r -writeProc dcs_curr_lim_w

proc dcs_curr_lim_r { ins } {
    dcsReadVerify /$ins "SOUR:CURR:LIMIT?" 80 /$ins/setup/curr_limit " %f"
}

proc dcs_curr_lim_w { ins target } {
    if { $target < 0.0 || $target > [varGetVal /$ins/setup/maximum_curr] } {
        return -code error "Current limit out of range"
    }
    dcsWriteVerify /$ins "SOUR:CURR:LIMIT $target" "SOUR:CURR:LIMIT?" 32 \
        /$ins/setup/curr_limit " %f" $target
}

CAMP_FLOAT /~/setup/volt_limit -D -S -R -T "Soft voltage limit" \
    -d on -s on -r on -units V -v 1234.5 \
    -readProc dcs_volt_lim_r -writeProc dcs_volt_lim_w

proc dcs_volt_lim_r { ins } {
    dcsReadVerify /$ins "SOUR:VOLT:LIMIT?" 80 /$ins/setup/volt_limit " %f"
}

proc dcs_volt_lim_w { ins target } {
    if { $target < 0.0 || $target > [varGetVal /$ins/setup/maximum_volt] } {
        return -code error "Voltage limit out of range"
    }
    dcsWriteVerify /$ins "SOUR:VOLT:LIMIT $target" "SOUR:VOLT:LIMIT?" 32 \
        /$ins/setup/volt_limit " %f" $target
}

CAMP_FLOAT /~/setup/maximum_curr -D -T "Hard current limit" \
    -d on -units A

CAMP_FLOAT /~/setup/maximum_volt -D -T "Hard voltage limit" \
    -d on -units V

CAMP_FLOAT /~/setup/ramp_rate -D -S -L -T "Ramp rate" \
    -H "Ramp rate for current changes, A/s" \
    -d on -s on -v 20.0 -units "A/s" \
    -writeProc dcs_ramp_rate_w

proc dcs_ramp_rate_w { ins target } {
    if { $target < 0.01} {
        return -code error "Ramp rates must be at least 0.01 A/s"
    }
    varDoSet /$ins/setup/ramp_rate -v $target
}

CAMP_SELECT /~/setup/autoenable -D -S -T "Automatic enable & disable" \
    -H "Select whether to automatically disable/enable output when setting is zero/finite" \
    -selections Manual Automatic \
    -d on -s on -v 1 \
    -writeProc dcs_autoen_w

proc dcs_autoen_w { ins target } {
    varDoSet /$ins/setup/autoenable -v $target
}

#
#----------------------  actions  --------------------------------------------------
#
CAMP_STRUCT /~/actions -D -d on -T "Actions" \
    -H "Internal procedures"

CAMP_SELECT /~/actions/initialize -D -R -P -T "Initialize" \
    -selections "" "Pending" "Doing" "Done" "Failed" -d on -r off -p off \
    -readProc dcs_initialize_r


# Going online when there is a pre-existing limits and setpoints, then re-apply 
# settings; when there are just initial flag values, then read setpoints and limits.
# note: online_proc reads status, and therefore the maximum_curr/volt

proc dcs_initialize_r { ins } {
    varDoSet /$ins/actions/initialize -p off -v "Doing"
    if { [catch {
        if { [set cl [varGetVal /$ins/setup/curr_limit]] == 1234.5 } {
            varRead /$ins/setup/curr_limit
        } else {
            varSet /$ins/setup/curr_limit -v $cl
        }
        if { [set vl [varGetVal /$ins/setup/volt_limit]] == 1234.5 } {
            varRead /$ins/setup/volt_limit
        } else {
            varSet /$ins/setup/volt_limit -v $vl
        }
        if { [set vv [varGetVal /$ins/volt_set]] == 1234.5 || $vv > $vl } {
            varRead /$ins/volt_set
        } else {
            dcs_do_volt_set $ins $vv
        }
        if { [set cc [varGetVal /$ins/curr_set]] == 1234.5 || $cc > $cl } {
            varRead /$ins/curr_set
        } elseif { [varGetVal /$ins/output] } {
            varSet /$ins/curr_set -v $cc
        }
    }] } {
	varDoSet /$ins/actions/initialize -p off -v "Failed"
    } else {
	varDoSet /$ins/actions/initialize -p off -v "Done"
    }
}

# Polled to perform an automatic Disable/Shutdown after ramping to zero.
CAMP_SELECT /~/actions/pending -D -R -P -T "Flag pending shutdown" \
    -selections no yes -d on -r off -p off -v 0 \
    -readProc dcs_pending_r

proc dcs_pending_r { ins } {
    varRead /$ins/status
    set stat [varGetVal /$ins/status]
    if { $stat != 2 } { # not ramping
        varRead /$ins/curr_read
        set Ir [varGetVal /$ins/curr_read]
	if { abs($Ir) < 0.2 } {
	    varSet /$ins/actions/endisable -v 0
	    varDoSet /$ins/output -v 0
	}
	varDoSet /$ins/actions/pending -v 0 -p off
    }
}

CAMP_SELECT /~/actions/endisable -D -S -R -P -T "Enable/disable" \
    -H "Output - enabled or disabled" \
    -selections {Disable} {Enable} \
    -d on -s on -r on \
    -readProc dcs_status_r -writeProc dcs_endisable_w

proc dcs_endisable_w { ins target } {
    dcsWriteVerify /$ins "OUTP:STATE $target" "OUTP:STATE?" 32 \
            /$ins/actions/endisable " %d" $target
}

CAMP_FLOAT /~/actions/curr_set -D -S -R -T "Real current setpoint" \
    -d on -s on -r on -units A \
    -readProc dcs_curr_rcsp_r -writeProc dcs_do_curr_ramp

proc dcs_curr_rcsp_r { ins } {
    dcsReadVerify /$ins "SOUR:CURR?" 32 /$ins/actions/curr_set " %f"
}

proc dcs_do_curr_set { ins target } {
    set target [format %.3f $target]
    dcsWriteVerify /$ins "SOUR:CURR $target" "SOUR:CURR?" 32 \
        /${ins}/actions/curr_set " %f" $target
}

proc dcs_do_curr_ramp { ins target } {
    set target [format %.3f $target]
    set rr [varGetVal /$ins/setup/ramp_rate]
    set rt [expr {abs([varGetVal /$ins/actions/curr_set] - $target) / ($rr<0.01?0.01:$rr)}]
    set rt [format %.1f [expr { $rt < 0.1 ? 0.1 : ($rt>99.0 ? 99.0 : $rt) }]]
    if { [catch {insIfWrite /$ins "SOUR:CURR:RAMP $target $rt"}] } {# Retry once
	sleep 0.1
	insIfWrite /$ins "SOUR:CURR:RAMP $target $rt"
    }
    varDoSet /$ins/actions/curr_set -v $target
    varDoSet /$ins/status -v "Ramping"
}
