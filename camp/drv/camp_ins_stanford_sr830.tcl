# CAMP instrument driver for Stanford Research Systems SR830 DSP Lock-In Amplifier,
# Donald Arseneau, Dec 2015
# $Log: camp_ins_stanford_sr830.tcl,v $
# Revision 1.3  2016/06/08 01:08:51  asnd
# Fix bug of wrong proc name (add a Logging property too)
#
# Revision 1.2  2016/06/04 05:35:26  asnd
# Fix bugs, especially problem with old buffered output from instrument.
#
# Revision 1.1  2016/04/28 22:24:11  asnd
# Initial version (old instrument new driver)
#

CAMP_INSTRUMENT /~ -D -T "SRS SR830 Lock-In Amp" \
    -H "Stanford Research Systems SR830 DSP Lock-In Amplifier" -d on \
    -initProc sr830_init \
    -deleteProc sr830_delete \
    -onlineProc sr830_online \
    -offlineProc sr830_offline 

proc sr830_init { ins } { 
    insSet /${ins} -if rs232 0.2 1.0 tty 9600 8 none 1 CR CR
}
proc sr830_delete { ins } { insSet /${ins} -line off }
proc sr830_online { ins } {
    insIfOn /${ins}
    switch -glob [insGetIfTypeIdent /${ins}] {
        rs232* { insIfWrite /$ins "OUTX 0" }
        gpib* { insIfWrite /$ins "OUTX 1" }
    }
    for {set i 0} {$i < 2} {incr i} {
	set status [catch {varRead /${ins}/setup/id}]
	if { ( $status != 0 ) || ( [varGetVal /${ins}/setup/id] == 0 ) } {
	    if { $i } {
		catch {sr830_clearbuffer $ins}
	    } else {
		insIfOff /${ins}
		return -code error "failed ID query, check interface definition and connections"
	    }
	}
    }
}
proc sr830_offline { ins } { insIfOff /${ins} }

# Clear instrument's output buffer
proc sr830_clearbuffer { ins } {
    set icmd [concat insSet /$ins -if [insGetIfTypeIdent /${ins}] [insGetIfDelay /$ins] [insGetIfTimeout /$ins] [insGetIf /${ins}] ]
    insIfOff /$ins
    expr "\[[lreplace $icmd 4 5 0.01 0.01]\]"
    insIfOn /$ins
    while 1 {
	if { [catch {insIfRead /$ins "" 256}] } { break }
    }
    insIfOff /$ins
    expr "\[$icmd\]"
    insIfOn /$ins
}

CAMP_FLOAT /~/X -D -R -P -L -T "X component" \
    -d on -r on -units "V" \
    -readProc sr830_XY_r

CAMP_FLOAT /~/Y -D -R -P -L -T "Y component" \
    -d on -r on -units "V" \
    -readProc sr830_XY_r

proc sr830_XY_r { ins } {
    set buf [insIfRead /${ins} "SNAP? 1,2" 80]
    if { [scan $buf { %f , %f} x y] < 2 } {
        return -code error "Invalid X,Y readback: $buf"
    }
    varDoSet /${ins}/X -v $x
    varDoSet /${ins}/Y -v $y
}

CAMP_FLOAT /~/R -D -R -P -L -T "Magnitude R" \
    -d on -r on -units "V" \
    -readProc sr830_Rtheta_r

CAMP_FLOAT /~/theta -D -R -P -L -T "phase theta" \
    -d on -r on -units "deg" \
    -readProc sr830_Rtheta_r

proc sr830_Rtheta_r { ins } {
    set buf [insIfRead /${ins} "SNAP? 3,4" 80]
    if { [scan $buf { %f , %f} r t] < 2 } {
        return -code error "Invalid R,theta readback: $buf"
    }
    varDoSet /${ins}/R -v $r
    varDoSet /${ins}/theta -v $t
}

CAMP_INT /~/sensitivity -D -S -R -T "Sensitivity range" \
    -H "Sensitivity range: 0 (2 nV or fA) to 26 (1 V or uA)" \
    -d on -s on -r on -units "" \
    -readProc sr830_sens_r -writeProc sr830_sens_w
proc sr830_sens_r { ins } {
    insIfReadVerify /$ins "SENS?" 42 /${ins}/sensitivity " %d" 2
    sr830_sens_msg $ins
}
proc sr830_sens_w { ins sens } {
    if { $sens < 0 || $sens > 26 } {
        return -code error "Sensitivity must be an integer from 0 (2 nV) to 26 (1 V)"
    }
    insIfWriteVerify /$ins "SENS $sens" "SENS?" 42 /${ins}/sensitivity " %d" 2 $sens
    sr830_sens_msg $ins
}
proc sr830_sens_msg { ins } {
    set s [varGetVal /${ins}/sensitivity]
    set ni [expr {($s+1) % 9}]
    set ui [expr {($s+1) / 9}]
    set n [lindex {1 2 5 10 20 50 100 200 500} $ni]
    varRead /${ins}/setup/input/source
    if { [varGetVal /${ins}/setup/input/source] > 1 } {# currents
        set u [lindex {fA pA nA uA} $ui]
    } else {# voltages
        set u [lindex {nV uV mV V} $ui]
    }
    varDoSet /${ins}/sensitivity -m "Sensitivity range $s = $n $u"
}
        
CAMP_INT /~/time_const -D -S -R -L -T "Time constant index" \
    -H "Time constant: 0 (10 us) to 19 (500 min)" \
    -d on -s on -r on -units "" \
    -readProc sr830_tconst_r -writeProc sr830_tconst_w
proc sr830_tconst_r { ins } {
    insIfReadVerify /$ins "OFLT?" 42 /${ins}/time_const " %d" 2
    sr830_tconst_msg $ins
}
proc sr830_tconst_w { ins tc } {
    if { $tc < 0 || $tc > 19 } {
        return -code error "Time constant flag must be an integer from 0 (10 us) to 19 (500 min)"
    }
    insIfWriteVerify /$ins "OFLT $tc" "OFLT?" 42 /${ins}/time_const " %d" 2 $tc
    sr830_tconst_msg ${ins}
}
proc sr830_tconst_msg { ins } {
    set s [varGetVal /${ins}/time_const]
    set ni [expr {($s+2) % 6}]
    set ui [expr {($s+2) / 6}]
    set n [lindex {1 3 10 30 100 300} $ni]
    set u [lindex {us ms s ks} $ui]
    varDoSet /${ins}/time_const -m "Time constant = $n $u for index $s"
}

CAMP_SELECT /~/reserve -D -S -R -T "Reserve mode" \
    -selections "High" "Normal" "Low" \
    -d on -s on -r on \
    -readProc sr830_reserve_r -writeProc sr830_reserve_w
proc sr830_reserve_r { ins } {
    insIfReadVerify /$ins "RMOD?" 42 /${ins}/reserve " %d" 2
}
proc sr830_reserve_w { ins val } {
    insIfWriteVerify /$ins "RMOD $val" "RMOD?" 42 /${ins}/reserve " %d" 2 $val
}

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

CAMP_INT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -readProc sr830_id_r
proc sr830_id_r { ins } {
    set id [insIfRead /${ins} "*IDN?" 88]
    set n [scan $id " Stanford_Research_Systems,SR830,s/n%d,ver%f" sn ver]
    varDoSet /${ins}/setup/id -v [expr {$n == 2 ? 1 : 0}] -m $id
}

CAMP_SELECT /~/setup/auto_set -D -S -T "Automatic settings" \
    -selections "Gain" "Reserve" "Phase" "Offset X" "Offset Y" "Offset R" \
    -H "Set theis to perform various auto-functions." \
    -d on -s on -writeProc sr830_auto_w
proc sr830_auto_w { ins val } {
    set cmd [lindex {"AGAN" "ARSV" "APHS" "AOFF 1" "AOFF 2" "AOFF 3"} $val ]
    insIfWrite /$ins $cmd 
}

CAMP_STRUCT /~/setup/input -D -T "Input and filtering" -d on

CAMP_SELECT /~/setup/input/source -D -S -R -T "Input source" \
    -d on -r on -s on \
    -selections "V single (A)" "V diff (A-B)" "I (1M gain)" "I (100M gain)" \
    -readProc sr830_insrc_r -writeProc sr830_insrc_w
proc sr830_insrc_r { ins } {
    insIfReadVerify /$ins "ISRC?" 42 /${ins}/setup/input/source " %d" 2
}
proc sr830_insrc_w { ins val } {
    insIfWriteVerify /$ins "ISRC $val" "ISRC?" 42 /${ins}/setup/input/source " %d" 2 $val
}

CAMP_SELECT /~/setup/input/ground -D -S -R -T "Input shield ground" \
    -d on -r on -s on \
    -selections "Float" "Ground" \
    -readProc sr830_ingnd_r -writeProc sr830_ingnd_w
proc sr830_ingnd_r { ins } {
    insIfReadVerify /$ins "IGND?" 42 /${ins}/setup/input/ground " %d" 2
}
proc sr830_ingnd_w { ins val } {
    insIfWriteVerify /$ins "IGND $val" "IGND?" 42 /${ins}/setup/input/ground " %d" 2 $val
}

CAMP_SELECT /~/setup/input/coupling -D -S -R -T "Input coupling" \
    -d on -r on -s on \
    -selections "AC" "DC" \
    -readProc sr830_incpl_r -writeProc sr830_incpl_w
proc sr830_incpl_r { ins } {
    insIfReadVerify /$ins "ICPL?" 42 /${ins}/setup/input/coupling " %d" 2
}
proc sr830_incpl_w { ins val } {
    insIfWriteVerify /$ins "ICPL $val" "ICPL?" 42 /${ins}/setup/input/coupling " %d" 2 $val
}

CAMP_SELECT /~/setup/input/notch -D -S -R -T "Input notch filter" \
    -d on -r on -s on \
    -selections "None" "1x Line" "2x Line" "1x2x Line" \
    -readProc sr830_innf_r -writeProc sr830_innf_w
proc sr830_innf_r { ins } {
    insIfReadVerify /$ins "ILIN?" 42 /${ins}/setup/input/notch " %d" 2
}
proc sr830_innf_w { ins val } {
    insIfWriteVerify /$ins "ILIN $val" "ILIN?" 42 /${ins}/setup/input/notch " %d" 2 $val
}

CAMP_STRUCT /~/setup/reference -D -T "Reference setup" -d on

CAMP_SELECT /~/setup/reference/source -D -S -R -T "Reference source" \
    -d on -r on -s on \
    -selections "External" "Internal" \
    -readProc sr830_rfsrc_r -writeProc sr830_rfsrc_w
proc sr830_rfsrc_r { ins } {
    insIfReadVerify /$ins "FMOD?" 42 /${ins}/setup/reference/source " %d" 2
}
proc sr830_rfsrc_w { ins val } {
    insIfWriteVerify /$ins "FMOD $val" "FMOD?" 42 /${ins}/setup/reference/source " %d" 2 $val
}

CAMP_FLOAT /~/setup/reference/freq -D -S -R -T "Internal reference frequency" \
    -d on -r on -s on -units "Hz" \
    -readProc sr830_rffrq_r -writeProc sr830_rffrq_w
proc sr830_rffrq_r { ins } {
    insIfReadVerify /$ins "FREQ?" 42 /${ins}/setup/reference/freq " %f" 2
}
proc sr830_rffrq_w { ins val } {
    set tol [expr { $val > 1 ? $val*0.00015 : 0.00015 }]
    insIfWriteVerify /$ins "FREQ $val" "FREQ?" 42 /${ins}/setup/reference/freq " %f" 2 $val $tol
}

CAMP_FLOAT /~/setup/reference/phase -D -S -R -T "Internal reference phase shift" \
    -d on -r on -s on -units "degrees" \
    -readProc sr830_rfpha_r -writeProc sr830_rfpha_w
proc sr830_rfpha_r { ins } {
    insIfReadVerify /$ins "PHAS?" 42 /${ins}/setup/reference/phase " %f" 2
}
proc sr830_rfpha_w { ins val } {
    insIfWriteVerify /$ins "PHAS $val" "PHAS?" 42 /${ins}/setup/reference/phase " %f" 2 $val 0.05
}

CAMP_INT /~/setup/reference/harm -D -S -R -T "Reference harmonic" \
    -d on -r on -s on -units "" \
    -readProc sr830_rfhar_r -writeProc sr830_rfhar_w
proc sr830_rfhar_r { ins } {
    insIfReadVerify /$ins "HARM?" 42 /${ins}/setup/reference/harm " %d" 2
}
proc sr830_rfhar_w { ins val } {
    insIfWriteVerify /$ins "HARM $val" "HARM?" 42 /${ins}/setup/reference/harm " %d" 2 $val 
}

CAMP_FLOAT /~/setup/reference/ampl -D -S -R -T "Amplitude of sine output" \
    -d on -r on -s on -units "V" \
    -readProc sr830_rfamp_r -writeProc sr830_rfamp_w
proc sr830_rfamp_r { ins } {
    insIfReadVerify /$ins "SLVL?" 42 /${ins}/setup/reference/ampl " %f" 2
}
proc sr830_rfamp_w { ins val } {
    if { $val < 0.004 || $val > 5.0 } {
        return -code error "Amplitude out of range 0.004 - 5.0 V"
    }
    insIfWriteVerify /$ins "SLVL $val" "SLVL?" 42 /${ins}/setup/reference/ampl " %f" 2 $val 0.005
}

