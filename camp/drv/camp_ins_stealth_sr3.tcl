# camp_ins_stealth_sr3.tcl
# Stealth Microwave SR3 microwave amplifier
#
CAMP_INSTRUMENT /~ -D -T "Stealth SR3 Amp" \
    -H "Stealth SR3 Microwave Amplifier" -d on \
    -initProc ssr3_init \
    -deleteProc ssr3_delete \
    -onlineProc ssr3_online \
    -offlineProc ssr3_offline

proc ssr3_init { ins } {
  global $ins
  set ${ins}(listfc) -1.
  set ${ins}(listrc) -1.
  insSet /${ins} -if rs232 0.33 1 /tyCo/1 9600 8 none 1 CR CR
}
proc ssr3_delete { ins } {
  insSet /${ins} -line off
}
proc ssr3_online { ins } {
  insIfOn /${ins}
  insIfWrite /${ins} "ECHO OFF;M0"
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == 0) } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections"
  }
  varRead /${ins}/status
}
proc ssr3_offline { ins } {
  insIfOff /${ins}
}

CAMP_FLOAT /~/fwd_power -D -R -P -L -T "Forward power" \
    -H "Reads forward transmitted power as a percentage" \
    -d on -r on -p off -p_int 10 -units {%} \
    -readProc ssr3_fwd_r
  proc ssr3_fwd_r { ins } {
      set buf [insIfRead /$ins "FWD" 10]
      if { [scan $buf "%d" i] < 1 } {
          return -code error "invalid number: $buf"
      }
      set r [expr {double($i)/1023.0}]
      varDoSet /${ins}/fwd_power -v [format {%.2f} $r]
      # update list of readings
      global $ins
      set num [ expr [varGetVal /${ins}/setup/num_to_max] - 2 ]
      set lis [linsert [lrange [set ${ins}(listfc)] 0 $num] 0 $r]
      set ${ins}(listfc) $lis
      # select maximum
      set val [lindex [lsort -decreasing -real $lis] 0]
      varDoSet /${ins}/fwd_max -v $val
  }

CAMP_FLOAT /~/rev_power -D -R -P -L -T "Reverse power" \
    -H "Reads reverse power as a percentage" \
    -d on -r on -p off -p_int 10 -units {%} \
    -readProc ssr3_rev_r
  proc ssr3_rev_r { ins } {
      set buf [insIfRead /$ins "REV" 10]
      if { [scan $buf " %d" i] < 1 } {
          return -code error "invalid number: $buf"
      }
      set r [expr {double($i)/1023.0}]
      varDoSet /${ins}/rev_power -v [format {%.2f} $r]
      # update list of readings
      global $ins
      set num [ expr [varGetVal /${ins}/setup/num_to_max] - 2 ]
      set lis [linsert [lrange [set ${ins}(listrc)] 0 $num] 0 $r]
      set ${ins}(listrc) $lis
      # select maximum
      set val [lindex [lsort -decreasing -real $lis] 0]
      varDoSet /${ins}/rev_max -v $val
  }

CAMP_FLOAT /~/fwd_max -D -L -T "Max forwd power" \
    -H "Recent maximum forward power, as a percentage." \
    -d on -units {%}

CAMP_FLOAT /~/rev_max -D -L -T "Max rev power" \
    -H "Recent maximum reverse power, as a percentage." \
    -d on -units {%}

CAMP_SELECT /~/amp_on_off -D -S -R -P -T "Amplifier on/off" \
    -H "Set and read Amplifier on or off" \
    -selections "OFF" "ON" \
    -d on -s on -r on -p off \
    -readProc ssr3_amp_r -writeProc ssr3_amp_w
  proc ssr3_amp_r { ins } {
      insIfReadVerify /$ins "AMP?" 10 /${ins}/amp_on_off " %s" 2
  }
  proc ssr3_amp_w { ins val } {
      set target [lindex {OFF ON} $val]
      insIfWriteVerify /$ins "AMP $target" "AMP?" 10 /${ins}/amp_on_off " %s" 2 $target
  }

CAMP_STRING /~/status -D -R -P -A -T "Amplifier status" \
    -d on -r on -p off -v "" \
    -readProc ssr3_status_r
  proc ssr3_status_r { ins } {
      set buf [insIfRead /$ins "STATUS" 10]
      if { [scan $buf " %x" sb] < 1 } {
          return -code error "invalid status byte: $buf"
      }
      if { ($sb & 1) == 0 } {
          varDoSet /${ins}/status -v "OK" -alert off
          return
      }
      set alarms "Alarm:"
      if { $sb & 4 } { append alarms " Over_power" }
      if { $sb & 8 } { append alarms " High_temp" }
      if { $sb & 16 } { append alarms " Temp_trip" }
      if { $sb & 32 } { append alarms " Current" }
      if { $sb & 128 } { append alarms " Fan" }
      varDoSet /${ins}/status -v $alarms -alert on
  }

CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc ssr3_id_r
  proc ssr3_id_r { ins } {
      set id 0
      set buf x
      catch {insIfRead /${ins} "VER" 100} buf
      set id [expr {[scan $buf " Firmware Rev. %d.%d.%d" i1 i2 i3 ] == 3}]
      varDoSet /${ins}/setup/id -v $id
  }

CAMP_INT /~/setup/num_to_max -D -S -T "Number of readings per max" \
    -d on -s on -v 10 -writeProc ssr3_num_to_max_w 
  proc ssr3_num_to_max_w { ins target } { 
      global $ins
      set ${ins}(listfc) -1.
      set ${ins}(listrc) -1.
      varDoSet /${ins}/setup/num_to_max -v $target
  }

