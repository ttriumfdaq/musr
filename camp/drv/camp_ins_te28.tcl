# camp_ins_te28.tcl
# Purpose:  This tcl script defines the instrument interface for a Technology 80 Model 28
#           servo motor controller
# Inputs:   None
# Precond:  A functioning tcl interpreter must be running before the script can
#	    be processed. All the channels on the motor board must be created
#           using the te28Lib function te28CreateAll.
# Outputs:  Returns a result code 
# Postcond: The driver will be created and available for access by the system

CAMP_INSTRUMENT /~ -D -T "Te28 Motor" -d on \
    -initProc te28_init \
    -deleteProc te28_delete \
    -onlineProc te28_online \
    -offlineProc te28_offline

  proc te28_init { ins } { 
    insSet /${ins} -if none 0 0
    varDoSet /${ins}/setup/IPPos -v 2
  }

  proc te28_delete { ins } { insSet /${ins} -line off }

  proc te28_online { ins } { 
    insIfOn /${ins} 
    varRead /${ins}/setup/id
    if { [varGetVal /${ins}/setup/id] == 0 } {
      insIfOff /${ins}
      return -code error "failed ID query, check interface definition and connections"
    }
  }

  proc te28_offline { ins } { insIfOff /${ins} }

# motor move declarations
# Purpose:  Defines the actions to move the motor associated with the specified channel.
#           The position and channel are retrieved from the setup variables and a call
#           to the C library is made.
#           The data is converted from engineering units with the slope and offset.
# Inputs:   ~ is instrument name
# Precond:  Matching C function for write is available in camp_tcl.c
# Outputs:  error code if the call to insIfWrite fails
# Postcond: destination is updated to reflect the new destination

# destination:  new target for motor
# Get each of the motor parameters and write them before each move

CAMP_FLOAT /~/destin -D -S -L -T "Move motor" -d on -s on \
        -writeProc te28_move_w

proc te28_move_w { ins target } {
    set pos [varGetVal /${ins}/setup/IPPos]
    set chan [varGetVal /${ins}/setup/IPChan]

    set filter_kp [varGetVal /${ins}/setup/filter_kp]
    insIfWrite /${ins} "MotorFilterKP $pos $chan filter_kp"

    set filter_ki [varGetVal /${ins}/setup/filter_ki]
    insIfWrite /${ins} "MotorFilterKI $pos $chan filter_ki"

    set filter_kd [varGetVal /${ins}/setup/filter_kd]
    insIfWrite /${ins} "MotorFilterKD $pos $chan filter_kd"

    set filter_il [varGetVal /${ins}/setup/filter_il]
    insIfWrite /${ins} "MotorFilterIL $pos $chan filter_il"

    set filter_si [varGetVal /${ins}/setup/filter_si]
    insIfWrite /${ins} "MotorFilterSI $pos $chan filter_si"

    set slope [varGetVal /${ins}/setup/slope]
    insIfWrite /${ins} "MotorSlope $pos $chan slope"

    set offset [varGetVal /${ins}/setup/offset]
    insIfWrite /${ins} "MotorOffset $pos $chan offset"

    set accel [varGetVal /${ins}/setup/accel]
    insIfWrite /${ins} "MotorAcc $pos $chan accel"

    set vel [varGetVal /${ins}/setup/vel]
    insIfWrite /${ins} "MotorVel $pos $chan vel"

    set destin [expr $target]
    insIfWrite /${ins} "MotorMove $pos $chan destin"
    varDoSet /${ins}/destin -v $target
}

CAMP_FLOAT /~/posit -D -R -P -L -T "Read position" \
	-d on -r on -p on -p_int 1.0 \
        -readProc te28_pos_r

proc te28_pos_r { ins } {
    set pos [varGetVal /${ins}/setup/IPPos]
    set chan [varGetVal /${ins}/setup/IPChan]
    set slope [varGetVal /${ins}/setup/slope]
    set offset [varGetVal /${ins}/setup/offset]

    set status [catch {insIfRead /${ins} "MotorPosition $pos $chan motor_pos" 15}]
    if { $status != 0 } { return -code error "failed reading instrument $ins" }

    varDoSet /${ins}/posit -v $motor_pos
}

CAMP_SELECT /~/stop -D -S -R -P -T "Motor stop" -d on -s on -r on -p off \
	-selections STOP \
        -readProc te28_stop_r \
        -writeProc te28_stop_w

# This "panic button" is readable to allow:
# - single key stop: press "r" in the camp cui.
# - stop after a specified timout period - enabled by setting a poll interval
#   (To facilitate this use, polling is switched off when the stop is performed.)
proc te28_stop_r { ins } {
    varSet /${ins}/stop -p off
    set status [catch {te28_stop_w ${ins} STOP }]
}

proc te28_stop_w { ins target } {
    set pos [varGetVal /${ins}/setup/IPPos]
    set chan [varGetVal /${ins}/setup/IPChan]
    insIfWrite /${ins} "MotorStop $pos $chan 0"
}

# setup:   structure for holding setup parameters which follow

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on


# id:      id entry for this device

CAMP_SELECT /~/setup/id -D -R -T "ID query" -d on -r on \
	-selections FALSE TRUE \
	-readProc te28_id_r

proc te28_id_r { ins } {
    set id 1
    if {[catch {varRead /${ins}/posit}] == 0} {set id 1}
#    if {[catch {varSet /${ins}/destin -v 0}] == 0} {set id 1}
#    set id 1
    varDoSet /${ins}/setup/id -v $id
}


# IPPos:  position of Industry Pack on carrier

CAMP_INT /~/setup/IPPos -D -S -T "IPPos" \
	-d on -s off -v 0 
#	-writeProc te28_IPPos_w

#proc te28_IPPos_w { ins target } {
#    set IPPos $target
#    if { ( $IPPos < 0 ) || ( $IPPos > 3 ) } {
#	return -code error "IPPos out of range 0 to 3"
#    }
#    varDoSet /${ins}/setup/IPPos -v $IPPos
#}


# IPChan:  channel on specific industry pack

CAMP_INT /~/setup/IPChan -D -S -T "IPChan" \
	-d on -s on -v 0 \
	-writeProc te28_IPChan_w

proc te28_IPChan_w { ins IPChan } {
    if { ( $IPChan < 0 ) || ( $IPChan > 1 ) } {
	return -code error "IPChan out of range 0 to 1"
    }
    varDoSet /${ins}/setup/IPChan -v $IPChan
}


# slope:  slope for converting to engineering units - linear only

CAMP_FLOAT /~/setup/slope -D -S -T "Slope" \
	-d on -s on -v 0.000001 \
	-writeProc te28_slope_w

proc te28_slope_w { ins target } {
    set slope $target
    varDoSet /${ins}/setup/slope -v $slope
}


# offset:   offset for converting to engineering units

CAMP_FLOAT /~/setup/offset -D -S -T "Offset" \
	-d on -s on -v 0.0 \
	-writeProc te28_offset_w

proc te28_offset_w { ins target } {
    set offset $target
    varDoSet /${ins}/setup/offset -v $offset
}

# filter_kp:   Proportional parameter for PID loop in controller

CAMP_INT /~/setup/filter_kp -D -S -T "Filter - Proportional" \
	-d on -s on -v 100 \
	-writeProc te28_filter_kp_w

proc te28_filter_kp_w { ins target } {
    set filter_kp $target
    varDoSet /${ins}/setup/filter_kp -v $filter_kp
}

# filter_ki:   Integral parameter for PID loop in controller

CAMP_INT /~/setup/filter_ki -D -S -T "Filter - Integral" \
	-d on -s on -v 0 \
	-writeProc te28_filter_ki_w

proc te28_filter_ki_w { ins target } {
    set filter_ki $target
    varDoSet /${ins}/setup/filter_ki -v $filter_ki
}

# filter_kd:   Differential parameter for PID loop in controller

CAMP_INT /~/setup/filter_kd -D -S -T "Filter - Differential" \
	-d on -s on -v 2000 \
	-writeProc te28_filter_kd_w

proc te28_filter_kd_w { ins target } {
    set filter_kd $target
    varDoSet /${ins}/setup/filter_kd -v $filter_kd
}

# filter_il:    parameter for PID loop in controller

CAMP_INT /~/setup/filter_il -D -S -T "Filter - il" \
	-d on -s on -v 0 \
	-writeProc te28_filter_il_w

proc te28_filter_il_w { ins target } {
    set filter_il $target
    varDoSet /${ins}/setup/filter_il -v $filter_il
}

# filter_si:    parameter for PID loop in controller

CAMP_INT /~/setup/filter_si -D -S -T "Filter - si" \
	-d on -s on -v 0 \
	-writeProc te28_filter_si_w

proc te28_filter_si_w { ins target } {
    set filter_si $target
    varDoSet /${ins}/setup/filter_si -v $filter_si
}

# accel:    parameter for motor acceleration

CAMP_INT /~/setup/accel -D -S -T "Acceleration" \
	-d on -s on -v 50000 \
	-writeProc te28_accel_w

proc te28_accel_w { ins target } {
    set accel $target
    varDoSet /${ins}/setup/accel -v $accel
}

# vel:    parameter for motor velocity

CAMP_INT /~/setup/vel -D -S -T "Velocity" \
	-d on -s on -v 1000000 \
	-writeProc te28_vel_w

proc te28_vel_w { ins target } {
    set vel $target
    varDoSet /${ins}/setup/vel -v $vel
}
