# camp_ins_tek_afg5101.tcl
# Tektronix AFG 5101: Programmable Arbitrary Function Generator
#
#
CAMP_INSTRUMENT /~ -D -T "Tektronix AFG 5101" \
    -H "Tektronix AFG 5101 Arbitrary Function Generator" -d on \
    -initProc Tafg_init \
    -deleteProc Tafg_delete \
    -onlineProc Tafg_online \
    -offlineProc Tafg_offline

global TafgNames
global TafgFunctions
# TafgFunctions($name) is a list: check t0 t1 mag function_string
set TafgNames "GAUSS SECH HERMITE {SINT/T} PARAB" 
set TafgFunctions(GAUSS)   {1 -2.9 2.9 2047.2 {exp(-($t*$t))}}
set TafgFunctions(SECH)    {2 -9. 9. 2047.2 {1.0/cosh($t)}}
set TafgFunctions(HERMITE) {3 -3.2 3.2 2047.2 {(1.0-0.957*$t*$t)*exp(-$t*$t)}}
set TafgFunctions(SINT/T)  {4 -31.4159265 31.4159265 2047.2 {sin($t)/$t}}
set TafgFunctions(PARAB)   {5 -1. 1. 2047.2 {1.0-($t*$t)}}

proc Tafg_init { ins } {
   insSet /${ins} -if gpib 0.1 5 7 LF LF
}
proc Tafg_delete { ins } {
  insSet /${ins} -line off
}
proc Tafg_online { ins } {
  insIfOn /${ins}
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == 0) } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections"
  }
  insIfWrite /${ins} "output on"
  varRead /${ins}/setup/func_stat
  varRead /${ins}/raw/function
  varRead /${ins}/frequency
  insIfWrite /${ins} "DT TRIG"
}
proc Tafg_offline { ins } {
  insIfOff /${ins}  # mandatory call
}


CAMP_FLOAT /~/amplitude -D -S -R -P -L -T "Amplitude" \
    -d on -s on -r on -units V \
    -readProc Tafg_ampl_r -writeProc Tafg_ampl_w
  proc Tafg_ampl_r { ins } {
      insIfReadVerify /${ins} AMPL? 64 /${ins}/amplitude " AMPL %f;" 2
  }
  proc Tafg_ampl_w { ins val } {
      insIfWriteVerify /${ins} "AMPL $val" AMPL? 64 /${ins}/amplitude " AMPL %f;" 2 $val 0.01
  }

#  New version!  Use built-in signals to allow higher frequency for them, and relieve
#  dependance on using 1000 points.

CAMP_SELECT /~/function -D -S -T "Functional Form" -d on -s on \
    -selections DC SINE SQUARE TRIANGLE UPRAMP DNRAMP GAUSS SECH HERMITE {SINT/T} PARAB OTHER_FUNC " " \
    -v " " -writeProc Tafg_func_form_w 
  proc Tafg_func_form_w { ins val } {
      global TafgFunctions
#     (defeat) Temporary hack because loading doesn't affect anything after first!!
#      varSet /${ins}/setup/func_stat -v 0
#
      varDoSet /${ins}/function -v $val
      set fn [ varSelGetValLabel /${ins}/function ]

      varRead /${ins}/frequency
      set freq [varGetVal /${ins}/frequency]
      varRead /${ins}/sweep/rate

      switch [ lindex "0 0 0 0 1 1 2 2 2 2 2 3 4" $val ] {
	  0 {# Intrinsic (not using arbitrary)
	      varSet /${ins}/raw/function -v "$fn"
              varSet /${ins}/frequency -v $freq
	      return
 	    }
	  1 {# built in arbitrary functions (sawtooth up and dn)
              set npt 1000
              varDoSet /${ins}/setup/num_pt -v $npt
	      set a 0
	      insIfWrite /${ins} "FUNC ARB;ARBSEL 2;ARBLOAD $fn"
	    }
	  2 {# pre-defined soft functions
	      set npt [varGetVal /${ins}/setup/num_pt]
	      set i [ expr $val - 6 ]
	      set fs "[ varGetVal /${ins}/setup/func_stat ]"
	      insIfWrite /${ins} "FUNC ARBITRARY;ARBSEL 1"
	      set b [ lindex "$fs" $i ]
	      set fpar $TafgFunctions(${fn})
	      set fp0 [ lindex $fpar 0 ]
	      if { $b == 1 } {# This function has supposedly been downloaded
		  set buf [ insIfRead /${ins} "ARBADRS [expr 5 * $i + 10];ARBDATA? 5:A" 50 ]
		  set check -1
		  scan $buf " ARBDATA %d,%d,%d,%d,%d;" b starth startl len check
		  if { $check != $fp0 } { set b 0 }
		  if { $len != $npt } { set b 0 }
	      }
	      set a [expr 100 + $i*1000]
	      if { $b != 1 } {# This function needs to be downloaded
		  set fp1 [ lindex $fpar 1 ]
		  set fp2 [ lindex $fpar 2 ]
		  set fp3 [ lindex $fpar 3 ]
		  set fp4 [ lindex $fpar 4 ]
		  set s [ Tafg_load_func $ins $npt $a $fp1 $fp2 $fp3 $fp4 ]
		  if { $s == 1 } {# success 
		      varDoSet /${ins}/setup/func_stat -v [ lreplace $fs $i $i 1 ]
		      varDoSet /${ins}/f_first_t -v $fp1
		      varDoSet /${ins}/f_last_t -v $fp2
		      varDoSet /${ins}/other_func -v $fp4
		      set starth [ expr $a / 1000 ]
		      set startl [ expr $a - 1000*$starth ]
		      set check $fp0
		      insIfWrite /${ins} "ARBADRS [expr 5 * $i + 10];ARBDATA 1,$starth,$startl,$npt,$check"
		  } else {# failure 
		      return -code error "Failed to load function $fn"
		  }
	      }
	    }
	  3 {# Arbitrary, user-entered function
	      set npt [varGetVal /${ins}/setup/num_pt]
	      set a 1002
	      insIfWrite /${ins} "FUNC ARBITRARY;ARBSEL 2"
	      set s [ Tafg_load_func $ins $npt $a [varGetVal /${ins}/f_first_t] \
                  [varGetVal /${ins}/f_last_t] 0.0 [varGetVal /${ins}/other_func] ]
	      if { $s != 1 } {
		  return -code error "Failed to load function"
	      }
	    }
	  4 {# none
	      return
	    }
	}
	insIfWrite /${ins} "ARBSTART $a;ARBSTOP [expr $a+$npt-1];"
	# update rate and set auto filter based on rate
        varSet /${ins}/raw/function -v "ARBITRARY"
	varSet /${ins}/frequency -v $freq
        Tafg_auto_filter $ins $freq $npt
    }

# Here we evaluate the function to produce a list of $npt values and load them
# into the Tek afg at address $add.  $fpar is a list of function parameters:
# check t0 t1 mag function($t); we evaluate $mag*function($t) for $t from $t0 to $t1.
# Returns logical 1=success, 0=failure
  proc Tafg_load_func { ins npt add t0 t1 mag fun } {
      insIfWrite /${ins} "ARBADRS $add"
      set vstr ""
#     set c 0.0
      set dt [ expr (0.0+$t1-$t0)/($npt) ]
      if { $mag <= 0.0 } {# determine magnitude automatically
	  for { set i 0 } { $i < $npt } { incr i } {
	      set t [ expr {$t0+$dt*$i} ]
	      set val [ expr abs($fun) ]
	      if { $val > $mag } { set mag $val }
	  }
	  set mag [ expr {2047.0/$mag} ]
      }
      if { $mag <= 0.0 } { 
	  return -code error "Function does not depend on \$t"
      }
      set f_of_t "round([ expr double($mag) ]*($fun))"
      for { set i 0 } { $i < $npt } { incr i } {
	  if { [ string length $vstr ] > 900 } {# write long strings in pieces
	      insIfWrite /${ins} "ARBDATA [ string trimleft $vstr , ]"
	      set vstr ""
	  }
	  set t [ expr {$t0+$dt*$i} ]
	  set val [ expr $f_of_t ]
#	  set c [ expr {$c+abs($val)+$val+$i} ]
	  append vstr ",$val"
      }
      insIfWrite /${ins} "ARBDATA [ string trimleft $vstr , ]"
      set a -1
      scan [ insIfRead /${ins} "ARBADRS?" 64 ] " ARBADRS %d;" a
      return [expr $a == $add + $npt ]
  }

CAMP_STRING /~/other_func -D -S -T "Arbitrary function" \
    -H "Enter any arbitrary function of \$t, then set function to OTHER_FUNC" \
    -d on -s on -writeProc Tafg_other_func_s
  proc Tafg_other_func_s { ins val } {
      if { [string first "\$t" $val] == -1 } {
	  return -code error "Expression is not a function of \"\$t\""
      }
      varDoSet /${ins}/other_func -v $val
  }
      

CAMP_FLOAT /~/f_first_t -D -S -T "Function's first \$t" \
    -H "First value for \$t in arbitrary \"other function\"." \
    -d on -s on -writeProc Tafg_firstt_s
  proc Tafg_firstt_s { ins val } {
      varDoSet /${ins}/f_first_t -v $val
  }

CAMP_FLOAT /~/f_last_t -D -S -T "Function's last \$t value" \
    -H "Last value for \$t in arbitrary \"other function\"." \
    -d on -s on -writeProc Tafg_lastt_s
  proc Tafg_lastt_s { ins val } {
      varDoSet /${ins}/f_last_t -v $val
  }

CAMP_SELECT /~/lp_filter -D -S -R -P -T "Low Pass Filter" \
    -d on -s on -r on \
    -selections OFF {1 MHz} {100 kHz} {11 kHz} {1.3 kHz} AUTO  -v 5 \
    -readProc Tafg_lpfilter_r -writeProc Tafg_lpfilter_w
  proc Tafg_lpfilter_r { ins } {
      scan [ insIfRead /${ins} FILTER? 64 ] { FILTER %[^;]} v
      if { [ varGetVal /${ins}/lp_filter ] == 5 } {# auto filter
          if { $v == "OFF" } { set v 0 }
	  set m [ lindex "OFF {1 MHz} {100 kHz} {11 kHz} {1.3 kHz} AUTO" $v ]
	  varDoSet /${ins}/lp_filter -m "Auto-range low pass filter: now $m"
      } else {
	  varDoSet /${ins}/lp_filter -v $v -m {}
      }
  }
  proc Tafg_lpfilter_w { ins val } {
      if { $val < 5 } {
	  insIfWrite /${ins} "FILTER $val"
      }
      varDoSet /${ins}/lp_filter -v $val
      varRead /${ins}/lp_filter
  }

# Automatic filter setting based on the sweep step frequency
  proc Tafg_auto_filter { ins freq np } {
      if { $np > 100 } { set np 100 }
      set d [ expr int(7.8-log10($freq*$np+1.0)) ]
      if { $d < 0 } { set d 0 }
      if { $d > 4 } { set d 4 }
      insIfWrite /${ins} "FILTER $d"
      set m [ lindex "OFF {1 MHz} {100 kHz} {11 kHz} {1.3 kHz} AUTO" $d ]
      varDoSet /${ins}/lp_filter -m "Auto-range low pass filter: now $m ($freq $np [ expr log10($freq*$np+1.0) ]"
  }
CAMP_FLOAT /~/period  -D -S -T "Period of function" \
    -H "Time, in seconds, to output functional shape (reciprocal of frequency)" \
    -d on -s on -units "s" -v 0.01 \
    -writeProc Tafg_period_w
  proc Tafg_period_w { ins val } {
      varSet /${ins}/frequency -v [ expr {1.0/$val} ]
  }

# Frequency has dual nature, depending on whether function is ARBITRARY
CAMP_FLOAT /~/frequency -D -S -R -P -L -T "Frequency output" \
    -d on -s on -r on -units Hz \
    -readProc Tafg_freq_r -writeProc Tafg_freq_w
  proc Tafg_freq_w { ins val } {
      if { [varSelGetValLabel /${ins}/raw/function] == "ARBITRARY" } {
          set prevfrq  [varGetVal /${ins}/frequency]
          set n [varGetVal /${ins}/setup/num_pt]
          set val [expr {$val*double($n)} ]
          if { $val > 10.0e6 } { 
              return -code error "Maximum frequency for arbitrary functions is 10 MHz / num_pts (num_pts = $n)"
          }
          insIfWrite /${ins} "RATE $val:Hz"
          varDoSet /${ins}/period -v [ expr $n/$val ]
          set val [ expr $val/$n ]
          varDoSet /${ins}/frequency -v $val
          # auto filter.  Apply only when major change
          if { [ varGetVal /${ins}/lp_filter ] == 5 && abs(($val-$prevfrq)/($prevfrq+1.0)) > 0.3 } {
              Tafg_auto_filter $ins $val $n
          }
      } else {# Built-in functions.  Do quick set (no verify) to facilitate sweeping
          insIfWrite /${ins} "FREQUENCY $val:Hz"
          varDoSet /${ins}/frequency -v $val
          varDoSet /${ins}/period -v [ expr {1.0/$val} ]
      }
  }
  proc Tafg_freq_r { ins } {
      if { [varSelGetValLabel /${ins}/raw/function] == "ARBITRARY" } {
          varRead /${ins}/sweep/rate
      } else {
          set buf [insIfRead /${ins} FREQ? 64 ]
          if { [scan $buf " FREQ %f;" val] != 1 } {
              return -code error "Invalid readback: $buf"
          }
          varDoSet /${ins}/frequency -v $val
          varDoSet /${ins}/period -v [ expr {1.0/$val}]
      }
  }
#  proc Tafg_freq_r { ins } {
#      insIfReadVerify /${ins} FREQ? 64 /${ins}/frequency " FREQ %f;" 2
#  }
#  proc Tafg_freq_w { ins val } {
#      insIfWriteVerify /${ins} "FREQ $val:HZ" FREQ? 64 /${ins}/frequency " FREQ %f;" 2 $val [ expr 0.005*$val ]
#  }

CAMP_STRUCT /~/trigger -D -d on -T "Triggering functions"

CAMP_SELECT /~/trigger/mode -D -S -R -P -T "Triggering Mode" \
    -H "Triggering mode: continuous, single trigger, burst, or gated" \
    -d on -s on -r on -selections CONT TRIG BURST GATE -v 0 \
    -readProc Tafg_tmode_r -writeProc Tafg_tmode_w
  proc Tafg_tmode_r { ins } {
      insIfReadVerify /${ins} MODE? 64 /${ins}/trigger/mode { MODE %[^;]} 2
  }
  proc Tafg_tmode_w { ins val } {
      set vs [ lindex {CONT TRIG BURST GATE} $val ]
      insIfWriteVerify /${ins} "MODE $vs" MODE? 64 /${ins}/trigger/mode { MODE %[^;]} 2 $vs
  }

CAMP_INT /~/trigger/nburst -D -S -R -P -L -T "Number in burst" \
    -H "Number of pulses or cycles for each trigger" \
    -d on -s on -r on -readProc Tafg_nburst_r -writeProc Tafg_nburst_w
  proc Tafg_nburst_r { ins } {
      insIfReadVerify /${ins} NBURST? 64 /${ins}/trigger/nburst { NBUR %d;} 2
  }
  proc Tafg_nburst_w { ins val } {
      if { $val > 9999 || $val < 1 } {
	  return -code error "Burst number must be in range 1 to 9999"
      }
      insIfWriteVerify /${ins} "NBURST $val" NBURST? 64 /${ins}/trigger/nburst { NBUR %d;} 2 $val
  }

CAMP_SELECT /~/trigger/source -D -S -R -P -T "Triggering Source" \
    -d on -s on -r on -selections INT EXT MANUAL -v 1 \
    -readProc Tafg_trigs_r -writeProc Tafg_trigs_w
  proc Tafg_trigs_r { ins } {
      insIfReadVerify /${ins} TRIG? 64 /${ins}/trigger/source { TRIG %[^;]} 2
  }
  proc Tafg_trigs_w { ins val } {
      set vs [ lindex {INT EXT MANUAL} $val ]
      insIfWriteVerify /${ins} "TRIG $vs" TRIG? 64 /${ins}/trigger/source { TRIG %[^;]} 2 $vs
  }


CAMP_STRUCT /~/sweep -D -d on -T "Frequency sweep"

CAMP_SELECT /~/sweep/shape -D -S -R -P -T "Sweep Shape" \
    -H "Shape of sweep can be none, linear, log, or arbitrary (using function shape)." \
    -d on -s on -r on -selections OFF LIN LOG ARB \
    -readProc Tafg_swshape_r -writeProc Tafg_swshape_w
  proc Tafg_swshape_r { ins } {
      insIfReadVerify /${ins} SWEEP? 64 /${ins}/sweep/shape { SWEEP %[^;]} 2
  }
  proc Tafg_swshape_w { ins val } {
      set vs [ lindex {OFF LIN LOG ARB} $val ]
      insIfWriteVerify /${ins} "SWEEP $vs" SWEEP? 64 /${ins}/sweep/shape { SWEEP %[^;]} 2 $vs
  }

CAMP_FLOAT /~/sweep/frq_start -D -S -R -P -L -T "Frequency sweep start" \
    -d on -s on -r on -units Hz \
    -readProc Tafg_fstart_r -writeProc Tafg_fstart_w
  proc Tafg_fstart_r { ins } {
      insIfReadVerify /${ins} FRQSTART? 64 /${ins}/sweep/frq_start " FRQSTART %f;" 2
  }
  proc Tafg_fstart_w { ins val } {
      insIfWriteVerify /${ins} "FRQSTART $val:HZ" \
	      FRQSTART? 64 /${ins}/sweep/frq_start " FRQSTART %f;" 2 $val [ expr 0.005*$val ]
      varSet /${ins}/sweep/shape -v [varGetVal /${ins}/sweep/shape]
  }

CAMP_FLOAT /~/sweep/frq_stop -D -S -R -P -L -T "Frequency sweep stop" \
    -d on -s on -r on -units Hz \
    -readProc Tafg_fstop_r -writeProc Tafg_fstop_w
  proc Tafg_fstop_r { ins } {
      insIfReadVerify /${ins} FRQSTOP? 64 /${ins}/sweep/frq_stop " FRQSTOP %f;" 2
  }
  proc Tafg_fstop_w { ins val } {
      insIfWriteVerify /${ins} "FRQSTOP $val:HZ" \
	      FRQSTOP? 64 /${ins}/sweep/frq_stop " FRQSTOP %f;" 2 $val [ expr 0.005*$val ]
      varSet /${ins}/sweep/shape -v [varGetVal /${ins}/sweep/shape]
  }

CAMP_FLOAT /~/sweep/rate -D -S -R -L -T "sweep rate or interval" \
    -H "Instrument sweep/step rate/interval (units may change)." \
    -d on -s on -r on -units S \
    -readProc Tafg_rate_r -writeProc Tafg_rate_w
  proc Tafg_rate_r { ins } {
      set buf [ insIfRead /${ins} RATE? 64 ]
      if { [ scan $buf { RATE %f:%[^;]} r u ] != 2 } {
	  return -code error "Fail to parse \"$buf\""
      }
      varDoSet /${ins}/sweep/rate -v $r -units "$u"
      if { [varSelGetValLabel /${ins}/raw/function] == "ARBITRARY" } {
          set npt [varGetVal /${ins}/setup/num_pt]
          if { [string compare $u S] } {#  units Hz
              varDoSet /${ins}/sweep/rate -m "Units Hz, set freq $r/$npt"
              varDoSet /${ins}/period -v [expr {$npt/$r}]  
              varDoSet /${ins}/frequency -v [expr {$r/$npt}]
          } else {# units sec
              varDoSet /${ins}/sweep/rate -m "Units S, set freq 1/($npt*$r)"
              varDoSet /${ins}/period -v [expr {$npt*$r}]
              varDoSet /${ins}/frequency -v [expr {1.0/($npt*$r)}]
          }
      }
  }
  proc Tafg_rate_w { ins val } {
      set u [ varNumGetUnits /${ins}/sweep/rate ]
      insIfWrite /${ins} "RATE $val:$u"
      varDoSet /${ins}/sweep/rate -v $val
      varSet /${ins}/sweep/shape -v [varGetVal /${ins}/sweep/shape]
  }

CAMP_STRUCT /~/raw -D -d on

CAMP_SELECT /~/raw/am -D -S -R -P -T "Amplitude Modulation" \
    -d on -s on -r on -selections OFF ON -v 0 \
    -readProc Tafg_AM_r -writeProc Tafg_AM_w
  proc Tafg_AM_r { ins } {
      insIfReadVerify /${ins} AM? 64 /${ins}/raw/am { AM %[^;]} 2
  }
  proc Tafg_AM_w { ins val } {
      set vs [ lindex {OFF ON} $val ]
      insIfWriteVerify /${ins} "AM $vs" AM? 64 /${ins}/raw/am { AM %[^;]} 2 $vs
  }

CAMP_SELECT /~/raw/fm -D -S -R -P -T "Frequency Modulation" \
    -d on -s on -r on -selections OFF ON -v 0 \
    -readProc Tafg_FM_r -writeProc Tafg_FM_w
  proc Tafg_FM_r { ins } {
      insIfReadVerify /${ins} FM? 64 /${ins}/raw/fm { FM %[^;]} 2
  }
  proc Tafg_FM_w { ins val } {
      set vs [ lindex {OFF ON} $val ]
      insIfWriteVerify /${ins} "FM $vs" FM? 64 /${ins}/raw/fm { FM %[^;]} 2 $vs
  }

CAMP_SELECT /~/raw/function -D -S -R -T "Function" \
    -d on -s on -r on -selections SINE SQUARE TRIANGLE DC ARBITRARY \
    -readProc Tafg_rawFunc_r -writeProc Tafg_rawFunc_w
  proc Tafg_rawFunc_r { ins } {
      insIfReadVerify /${ins} FUNC? 64 /${ins}/raw/function { FUNC %[^;]} 2
  }
  proc Tafg_rawFunc_w { ins val } {
      set vs [ lindex {SINE SQUARE TRIANGLE DC ARBITRARY} $val ]
      insIfWriteVerify /${ins} "FUNC $vs" FUNC? 64 /${ins}/raw/function { FUNC %[^;]} 2 $vs
  }


CAMP_STRUCT /~/setup -D -d on

CAMP_SELECT /~/setup/id -D -R -T "ID Query" -d on -r on \
    -selections FALSE TRUE -readProc Tafg_id_r
  proc Tafg_id_r { ins } {
      set id 0
      if { [catch {insIfRead /${ins} "ID?" 80} buf] == 0} {
	  set id [scan $buf " ID TEK/AFG5101,V%*f,F%f;" val ]
      }
      varDoSet /${ins}/setup/id -v $id
  }

CAMP_INT /~/setup/num_pt -D -S -T "Number of points in functions" \
    -d on -s on -v 1000 \
    -writeProc Tafg_npt_w
proc Tafg_npt_w { ins val } {
    varDoSet /${ins}/setup/num_pt -v $val
}

# TafgFunctions($name) is a list: check t0 t1 mag function_string
CAMP_STRING /~/setup/func_stat -D -R -S -T "Function memory status" -d on -r on -s on \
      -v "0 0 0 0 0 0 0 0" -readProc Tafg_func_stat_r -writeProc Tafg_func_stat_s
  proc Tafg_func_stat_r { ins } {
      global TafgNames TafgFunctions
      set buf [ insIfRead /${ins} {ARBSEL 1;ARBDATA? 10:A} 100 ]
      set s [ scan $buf " ARBDATA 2000,6,14,-1958,-3,-14,%d," nf ]
      # At present, using 8 fixed-size function arrays of 1000 points:
      #varDoSet /${ins}/setup/func_stat -m "scanned s=$s nf=$nf "
      if { ($s != 1) || ( $nf != 8 ) } {
	  return [ Tafg_func_stat_s $ins 1 ]
      }
      set val ""
      set nf [ llength $TafgNames ]
      for { set i 0 } { $i < $nf } { incr i } {
	  set buf [ insIfRead /${ins} "ARBDATA? 5:A" 50 ]
	  set b 0
	  scan $buf " ARBDATA %d,%d,%d,%d,%d;" b starth startl len check
          #varDoSet /${ins}/setup/func_stat -m "For $i: $b $starth $startl $len $check ([expr $b != ( $b == 1 )])"
	  # At present, using 8 fixed-size function arrays of 1000 points:
	  if { $len != [ varGetVal /${ins}/setup/num_pt ] } { set b 0 }
	  if { $b != ( $b == 1 ) } { return [ Tafg_func_stat_s $ins 1 ] }
	  if { $b } {# do checksum comparison:
	      set fn [ lindex $TafgNames $i ]
	      if { [ lindex $TafgFunctions(${fn}) 0 ] != $check } { return [ Tafg_func_stat_s $ins 1 ] }
	  }
	  lappend val $b
      }
      varDoSet /${ins}/setup/func_stat -v $val
  }

# Initialize the records of loaded arbitrary funtions to "none":
  proc Tafg_func_stat_s { ins val } {
      if { $val != [varGetVal /${ins}/setup/func_stat] } {
	  varDoSet /${ins}/setup/func_stat -v "0 0 0 0 0 0 0 0"
	  set s "0,0,0,0,0,0,0,0,0,0"
	  insIfWrite /${ins} "ARBSEL 1;ARBCLR ALL;ARBDATA 2000,6,14,-1958,-3,-14,8"
      }
  }

