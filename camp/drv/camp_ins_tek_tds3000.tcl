#  camp_ins_tek_tds3000.tcl
#
#  Tektronix TDS 3000: Programmable Arbitrary Function Generator
#
#  20140331  TW  simple initial version reading the four channels
#
#  note: this was written to work with two instruments on site:
#        TDS3034 (ip: 142.90.127.90)
#        TDS3054B (ip: 142.90.126.33) 
#
#  The programming manual implies that external programming is only
#  available via GPIB and not ethernet.  Ethernet provides a web
#  server only.  (unverified as of 20140331).
#

CAMP_INSTRUMENT /~ -D -T "Tektronix TDS3000" \
    -H "Tektronix TDS3000 Series Oscilloscope" -d on \
    -initProc tek_tds3000_init \
    -deleteProc tek_tds3000_delete \
    -onlineProc tek_tds3000_online \
    -offlineProc tek_tds3000_offline

proc tek_tds3000_init { ins } {
  insSet /${ins} -if rs232 0.1 2 /tyCo/1 9600 8 none 1 LF LF
}

proc tek_tds3000_delete { ins } {
  insSet /${ins} -line off
}

proc tek_tds3000_online { ins } {
  insIfOn /${ins}
  insIfWrite /${ins} "HEADer ON"
  if { [catch {varRead /${ins}/setup/id}] || ([varGetVal /${ins}/setup/id] == "?") } {
    insIfOff /${ins}
    return -code error "failed ID query, check interface definition and connections"
  }
}

proc tek_tds3000_offline { ins } {
  insIfOff /${ins}  # mandatory call
}


CAMP_FLOAT /~/Ch1 -D -R -P -L -T "Channel 1 Value" \
    -d on -r on -units "" \
    -readProc tek_tds3000_ch1_r
  proc tek_tds3000_ch1_r { ins } {
      insIfReadVerify /${ins} "MEASUrement:MEAS1:VALue?" 64 /${ins}/Ch1 " :MEASUREMENT:MEAS1:VALUE %f" 2
  }

CAMP_FLOAT /~/Ch2 -D -R -P -L -T "Channel 2 Value" \
    -d on -r on -units "" \
    -readProc tek_tds3000_ch2_r
  proc tek_tds3000_ch2_r { ins } {
      insIfReadVerify /${ins} "MEASUrement:MEAS2:VALue?" 64 /${ins}/Ch2 " :MEASUREMENT:MEAS2:VALUE %f" 2
  }

CAMP_FLOAT /~/Ch3 -D -R -P -L -T "Channel 3 Value" \
    -d on -r on -units "" \
    -readProc tek_tds3000_ch3_r
  proc tek_tds3000_ch3_r { ins } {
      insIfReadVerify /${ins} "MEASUrement:MEAS3:VALue?" 64 /${ins}/Ch3 " :MEASUREMENT:MEAS3:VALUE %f" 2
  }

CAMP_FLOAT /~/Ch4 -D -R -P -L -T "Channel 4 Value" \
    -d on -r on -units "" \
    -readProc tek_tds3000_ch4_r
  proc tek_tds3000_ch4_r { ins } {
      insIfReadVerify /${ins} "MEASUrement:MEAS4:VALue?" 64 /${ins}/Ch4 " :MEASUREMENT:MEAS4:VALUE %f" 2
  }


CAMP_STRUCT /~/setup -D -d on

CAMP_STRING /~/setup/id -D -R -T "ID Query" -d on -r on \
    -readProc tek_tds3000_id_r
  proc tek_tds3000_id_r { ins } {
      set model ""
      set scan_count 0
      set status [catch {insIfRead /${ins} "ID?" 80} buf]
      if { $status == 0 } {
	  set scan_count [scan $buf " ID TEK/TDS %d," model]
      }
      if { $scan_count > 0 } {
	  varDoSet /${ins}/setup/id -v "Textronix TDS $model" -m "ID recognized"
      } else {
	  varDoSet /${ins}/setup/id -v "?" -m "ID not recognized"
      }
  }


