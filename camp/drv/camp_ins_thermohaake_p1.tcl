# camp_ins_thermohaake_p1.tcl
# 
# $Log: camp_ins_thermohaake_p1.tcl,v $
# Revision 1.3  2016/06/02 08:46:14  asnd
# Avoid false-flag on [switch]
#
# Revision 1.2  2015/03/15 00:34:21  suz
# Ted's 201404 interface changes
#
# Revision 1.1  2004/06/04 03:39:43  asnd
# New instrument
#
#
# There are sporadic IO failures, so we definitely need to retry communications,
# using insIfReadVerify and insIfWriteVerify where possible.  But some variables
# involve units conversion, so those commands are not useful, and we use an
# explicit retrial loop in Tcl.

CAMP_INSTRUMENT /~ -D -T "ThermoHaake P1 Circulator" \
    -H "ThermoHaake Phoenix P1 Circulator" -d on \
    -initProc THP1_init -deleteProc THP1_delete \
    -onlineProc THP1_online -offlineProc THP1_offline

  proc THP1_init { ins } {
    insSet /${ins} -if rs232 0.25 2 /tyCo/0 9600 8 none 1 CR CR
  }
  proc THP1_delete { ins } { insSet /${ins} -line off }
  proc THP1_online { ins } { 
    insIfOn /${ins} 
    if { [catch {varRead /${ins}/setup/ident} msg] } {
      insIfOff /${ins}
      return -code error $msg
    }
  }
  proc THP1_offline { ins } { insIfOff /${ins} }

  CAMP_FLOAT /~/internal_read -D -R -P -L -A -T "Read Intern T" \
	-d on -r on -p on -p_int 15 -tol 0.15 -units C \
	-readProc {THP1_read_temp internal_read 1 F1}

  CAMP_FLOAT /~/external_read -D -R -P -L -A -T "Read Extern T" \
	-d on -r on -p off -p_int 15 -tol 0.05 -units C \
	-readProc {THP1_read_temp external_read 2 F2}

    proc THP1_read_temp { var contrID query ins } {
        set stat 0
        set retry 0
        while { $stat < 1 && $retry < 3 } {
            set buf [insIfRead /$ins $query 40]
            set U C
            set stat [scan $buf { %f %[^$]$} T U]
            incr retry
        }
        if { $retry >= 3 } {
            return -code error "Invalid temperature: [THP1_error $buf]"
        }
        if { $T != -222.22 } {
            set T [THP1convertT $T $U [varSelGetValLabel /${ins}/setup/units]]
            varDoSet /${ins}/${var} -v $T
            if { [varGetVal /${ins}/controlling] == $contrID } {
                varTestAlert /${ins}/${var} [varGetVal /${ins}/setpoint]
            } else { # reset any alert
                varTestAlert /${ins}/${var} $T
            }
        }
    }

  CAMP_FLOAT /~/setpoint -D -S -R -L -T "Set Temperature" \
	-d on -s on -r on -units C \
        -H "Set the target temperature (external or internal)" \
        -readProc THP1_setpoint_r -writeProc THP1_setpoint_w

    proc THP1_setpoint_r { ins } {
        set stat 0
        set retry 0
        while { $stat < 1 && $retry < 3 } {
            set buf [insIfRead /$ins "R SW" 40]
            set stat [scan $buf { SW%f$} T]
            incr retry
        }
        if { $retry >= 3 } {
            return -code error "Invalid temperature: [THP1_error $buf]"
        }
        set T [THP1convertT $T C [varSelGetValLabel /${ins}/setup/units]]
        varDoSet /${ins}/setpoint -v $T
    }

    proc THP1_setpoint_w { ins target } {
        set T [THP1convertT $target [varSelGetValLabel /${ins}/setup/units] C]
        set stat 1
        set retry 0
        while { $stat && $retry < 3 } {
            set buf [insIfRead /$ins "W SW [format {%.2f} $T]" 40]
            set stat [string compare $buf \$]
            incr retry
        }
        if { $retry >= 3 } {
            return -code error "Invalid setting: [THP1_error $buf]"
        }
        varRead /${ins}/setpoint
    }

    proc THP1convertT { val fromU toU } {
        # First normalize to Celcius
        switch -- $fromU {
            K { set val [expr { $val - 273.15 }] }
            F { set val [expr { ($val - 32.0) * 5.0/9.0 }] }
        }
        # Then convert to desired units
        switch -- $toU {
            K { set val [expr { $val + 273.15 }] }
            F { set val [expr { $val * 9.0/5.0 + 32.0 }] }
        }
        return $val
    }

  CAMP_SELECT /~/operation -D -S -R -T "Unit ON/OFF" \
          -d on -s on -r on -selections OFF ON\
          -H "Turn ON/OFF heating and pump" \
          -writeProc THP1_oper_w -readProc THP1_oper_r

      proc THP1_oper_w { ins target } {
          set resp [insIfRead /$ins "W TS$target" 40]
          if { [string compare $resp \$] } {
              set resp [insIfRead /$ins "W TS$target" 40]
          }
          if { [string compare $resp \$] } {
              return -code error [THP1_error $resp]
          }
          varDoSet /${ins}/operation -v $target
      }

      # There is no feature to read "operation" status, but we can read the
      # pump speed: if zero (anything less than 5%) then unit is OFF.
      proc THP1_oper_r { ins } {
          set buf [insIfRead /$ins "R PF" 40]
          if { [scan $buf { PF%f$} p] < 1 } {
              set buf [insIfRead /$ins "R PF" 40]
          }
          if { [scan $buf { PF%f$} p] < 1 } {
              return -code error "Failed to read status: $buf"
          }
          varDoSet  /${ins}/operation -v [expr {$p > 3.0}]
      }

  CAMP_SELECT /~/controlling -D -S -R -P -T "Control sel" -d on -s on -r on -p off -p_int 60 \
          -H "Selection of which temperature to control: Internal, External, or Off" \
          -v 0 -selections OFF Internal External \
          -writeProc THP1_controlling_w -readProc THP1_status_r

      proc THP1_status_r { ins } {
          set retry 0
          set stat 0
          while { $stat < 12 && $retry < 3 } {
              set buf [insIfRead /$ins "R BS" 50]
              set stat [scan $buf { BS%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d} \
                           tcon ext mrm aot all aol aex ac  afc zz  aipt aexpt ]
              incr retry
          }
          if { $retry >= 3 } {
              return -code error "Failed to read status: [THP1_error $buf]"
          }
          varDoSet /${ins}/controlling -v [expr { $tcon ? (1+$ext) : 0 }]
          # Set any other status variables here
      }

      proc THP1_controlling_w { ins target } {
          if { $target } {# on
              insIfWrite /$ins "W SR"
              set buf [insIfRead /$ins "W [lindex {ER IN EX} $target]" 16]
              if { [string compare $buf \$] } {
                  set buf [insIfRead /$ins "W [lindex {ER IN EX} $target]" 16]
              }
              if { [string compare $buf \$] } {
                  varRead /${ins}/controlling
                  return -code error "Failure: [THP1_error $buf]"
              }
          } else {# off
              insIfWrite /$ins "W ER"
          }
          varDoSet /${ins}/controlling -v $target
      }


CAMP_STRUCT /~/setup -D -T "Setup variables" -d on 

  CAMP_SELECT /~/setup/ident -D -R -T "Identification" -d on -r on \
          -v 0 -selections {?} {Phoenix P1} \
          -readProc THP1_ident_r

      proc THP1_ident_r { ins } {
          set i 0
          if { ![catch {insIfRead /$ins "R V1" 50} buf] } {
              if { [scan $buf { 1P/H:%f} ver] == 1 } {
                  varDoSet /${ins}/setup/ident -v 1
                  return
              }
          }
          varDoSet /${ins}/setup/ident -v 0
          return -code error "Failed to read identification: $buf"
      }

  CAMP_SELECT /~/setup/units -D -S -T "Temperature units" -d on -s on \
         -v 0 -selections C K F \
         -writeProc THP1_units_w

      proc THP1_units_w { ins target } {
          set us [lindex {C K F} $target]
          insIfWrite /$ins "W TE $us"
          varDoSet /${ins}/setup/units -v $target
          varDoSet /${ins}/setpoint -units $us
          varDoSet /${ins}/internal_read -units $us
          varDoSet /${ins}/external_read -units $us
          varRead /${ins}/setpoint
          varRead /${ins}/internal_read
          varRead /${ins}/external_read
     }

#  CAMP_FLOAT /~/setup/int_correction -D -S -R -L -T "Intern T correction" \
#	-d on -r on -s on -units C \
#	-readProc {THP1_read_corr int_correction CI} \
#        -writeProc {THP1_set_corr int_correction CI}
#
#  CAMP_FLOAT /~/setup/ext_correction -D -S -R -L -T "Extern T correction" \
#	-d on -r on -s on -units C \
#	-readProc {THP1_read_corr ext_correction CE} \
#        -writeProc {THP1_set_corr ext_correction CE}
#
#    proc THP1_read_corr { var query ins } {
#        insIfReadVerify /$ins "R $query" 40 /${ins}/setup/${var} " ${query}%f\$" 3
#    }
#
#    proc THP1_set_corr { var query ins target } {
#        insIfWriteVerify /$ins [format "W %s %.2f" $query $target] "R $query" 40 \
#                /${ins}/setup/${var} " ${query}%f\$" 3 $target
#    }

  CAMP_SELECT /~/setup/cooling -D -S -R -T "Cooling" \
          -d on -s on -r on -selections OFF ON \
          -H "Enable or disable cooling" \
          -writeProc THP1_cooling_w -readProc THP1_cooling_r

      proc THP1_cooling_w { ins target } {
          insIfWriteVerify /$ins "W CC $target" "R CC" 40 /${ins}/setup/cooling { CC%d$} 3 $target
      }

      proc THP1_cooling_r { ins } {
          insIfReadVerify /$ins "R CC" 40 /${ins}/setup/cooling { CC%d$} 3
      }


  CAMP_FLOAT /~/setup/pump_speed -D -S -R -T "Pump Speed" \
          -H "Control pump speed, in percent" \
          -d on -s on -r on -units {%} \
          -writeProc THP1_pump_w -readProc THP1_pump_r

      proc THP1_pump_w { ins target } {
          if { $target < 5 || $target > 100 } {
              return -code error "Invalid value: $target.  Select from 5 to 100%."
          }
          insIfWriteVerify /$ins "W PF $target" "R PF" 40 /${ins}/setup/pump_speed { PF%f$} 3 $target 0.4
      }

      proc THP1_pump_r { ins } {
          insIfReadVerify /$ins "R PF" 40 /${ins}/setup/pump_speed { PF%f$} 3
      }


proc THP1_error { result } {
    set err 666
    scan $result { F%d} err
    switch -- $err {
        1 { set result "Command unknown" }
        93 { set result "No external thermometer" }
        123 { set result "Value out of limits" }
        126 { set result "Ramp error" }
        127 { set result "Pause error" }
    }
    return $result
}
