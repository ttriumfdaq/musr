# camp_ins_tip850_adc.tcl
# Purpose:  This tcl script defines the ADC portion of a TEWS DatenTechnik TIP850
#	    DAC/ADC module. There are eight ADC channels in the module. The core functions
#           online/offline/etc are declared here
# Inputs:   None
# Precond:  A functioning tcl interpreter must be running before the script can
#	    be processed. All the channels on the DAC/ADC board must be created
#           using the tipLib function tip850CreateAll.
# Outputs:  Returns a result code 
# Postcond: The driver will be created and available for access by the system

# This driver uses interface type "none", and goes online immediately so the
# user doesn't have to specify.  The real interface parameter is ~/setup/channel.
# When the instrument is loaded, it starts polling the channel in case a number is 
# assigned by loading an initialization file (camp server restart).  When the channel
# is assigned, the gain parameter is also applied.

CAMP_INSTRUMENT /~ -D -T "Tip850 ADC" -d on \
    -initProc tip850_adc_init -deleteProc tip850_adc_delete \
    -onlineProc tip850_adc_online -offlineProc tip850_adc_offline
  proc tip850_adc_init { ins } {
    insSet /${ins} -if none 0 0
    insIfOn /${ins}
  }
  proc tip850_adc_delete { ins } { insSet /${ins} -line off }
  proc tip850_adc_online { ins } { insIfOn /${ins} }
  proc tip850_adc_offline { ins } { insIfOff /${ins} }

# ADC data declarations
# Purpose:  Defines the actions to read data from an ADC channel. The position and channel
#           are retrieved from the setup variables and a call to the C library is made.
#           The data is converted to engineering units with the slope and offset.
# Inputs:   ~ is instrument name
# Precond:  Matching C function for read is available in camp_tcl.c
# Outputs:  error code if the call to insIfRead fails
# Postcond: adc_read is resulting variable with data stored

CAMP_FLOAT /~/adc_read -D -R -P -L -A -T "Read ADC" \
	-d on -r on -a off -tol 0 \
        -readProc tip850_adc_read_r

proc tip850_adc_read_r { ins } {
    set pos [varGetVal /${ins}/setup/IPPos]
    set chan [varGetVal /${ins}/setup/channel]
    if { $chan < 0 } { return -code error "Please set the channel number /$ins/setup/channel" }

    set status [catch {insIfRead /${ins} "AdcRead $pos $chan x" 10}]

    set func [varGetVal /${ins}/setup/conversion]
    if { [string first {$x} $func] < 0 } {
	set slope [varGetVal /${ins}/setup/slope]
	set offset [varGetVal /${ins}/setup/offset]
	set func {$x * $slope - $offset}
    }
    set value [expr $func]
    varDoSet /${ins}/adc_read -v $value

    if { [varGetVal /${ins}/setup/high_trip] < $value } {
	varTestAlert /${ins}/adc_read [varGetVal /${ins}/setup/high_trip]
    } elseif { [varGetVal /${ins}/setup/low_trip] > $value } {
	varTestAlert /${ins}/adc_read [varGetVal /${ins}/setup/low_trip]
    } else {
	varTestAlert /${ins}/adc_read  $value
    }
}

# Variables declaration
# Purpose:  This defines the local variables used by the ADC for configuration.
# Inputs:   setup - Declares the structure for holding setup variables
#           position - the Industry Pack position on the carrier board
#           channel - the channel on the Industry Pack
#           gain - the gain of the pre-amp for this channel 1, 2, 4 or 8
#           slope - conversion slope for engineering units
#           offset - conversion offset for engineering units
# Precond:  A functioning tcl interpreter must be running before the script can
#	    be processed. The specified channel on the DAC/ADC board must be created.
#           The specified channel in IP must be created using tipLib before it can
#           be used here.
# Outputs:  Returns a result code 
# Postcond: The device will be set up and available for access by the system

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

#Can't do ID query without a channel
#CAMP_SELECT /~/setup/id -D -R -T "ID query" -d on -r on \
#	-selections FALSE TRUE \
#	-readProc tip850_adc_id_r
#proc tip850_adc_id_r { ins } {
#    set id 0
#    if {[catch {varSet /${ins}/adc_read -v 0}] == 0} {set id 1}
#    varDoSet /${ins}/setup/id -v $id
#}

CAMP_INT /~/setup/IPPos -D -T "IP Position" -H "Slot used in industry pack" \
        -d on -v 3
#	-writeProc tip850_adc_IPPos_w
#proc tip850_adc_IPPos_w { ins target } {
#    set IPPos $target
#    if { ( $IPPos < 0 ) || ( $IPPos > 3 ) } {
#	return -code error "IPPos out of range 0 to 3"
#    }
#    varDoSet /${ins}/setup/IPPos -v $IPPos
#}

# There are 4 ordinary adc channels (0-3) and 4 pot channels (4-7), but
# the two highest are reserved as motor channels.  Channel -1 is a flag
# indicating the channel is not set properly.  The channel is polled 
# until a legal value has been set, at which time the gain parameter
# is written to the industry-pack internals.

CAMP_INT /~/setup/channel -D -S -R -P -T "ADC Channel" \
        -d on -s on -r off -p on -p_int 5 -v -1 \
	-readProc tip850_adc_channel_r -writeProc tip850_adc_channel_w
proc tip850_adc_channel_w { ins target } {
    set channel $target
    if { ( $channel < 0 ) || ( $channel > 5 ) } {
	return -code error "channel out of range 0 to 5"
    }
    varDoSet /${ins}/setup/channel -v $channel
    varSet /${ins}/setup/gain -v [varGetVal /${ins}/setup/gain]
}
proc tip850_adc_channel_r { ins } {
    set channel [varGetVal /${ins}/setup/channel]
    if { $channel >= 0 } {
	varSet /${ins}/setup/channel -p off -v $channel
    }
}

CAMP_SELECT /~/setup/gain -D -S -T "Gain Factor" \
        -d on -s on -v 0 -selections x1 x2 x4 x8 \
	-writeProc tip850_adc_gain_w
proc tip850_adc_gain_w { ins target } {
    set gain $target
    set pos [varGetVal /${ins}/setup/IPPos]
    set chan [varGetVal /${ins}/setup/channel]
    insIfWrite /${ins} "GainSet $pos $chan gain"
    varDoSet /${ins}/setup/gain -v $gain
}

CAMP_FLOAT /~/setup/slope -D -S -T "Slope" \
	-d on -s on -v 1.0 \
	-H "Multiplier value for default linear conversion function" \
	-writeProc tip850_adc_slope_w
proc tip850_adc_slope_w { ins target } {
    set slope $target
    varDoSet /${ins}/setup/slope -v $slope
}

CAMP_FLOAT /~/setup/offset -D -S -T "Offset" \
	-d on -s on -v 0.0 \
	-H "Offset value for default linear conversion function" \
	-writeProc tip850_adc_offset_w
proc tip850_adc_offset_w { ins target } {
    set offset $target
    varDoSet /${ins}/setup/offset -v $offset
}

CAMP_STRING /~/setup/conversion -D -S -T "Conversion func" \
	-d on -s on -v "" \
	-H {Conversion function (Tcl expr syntax using $x). If blank, use linear conversion.} \
	-writeProc tip850_adc_conversion_w
proc tip850_adc_conversion_w { ins target } {
    set target [string trim $target]
    if { [string length $target] > 0 } {
	if { [catch {
	    set x 1 
	    set y1 [expr $target]
	    set x 1023
	    set y2 [expr $target]
	} ] } {
	    return -code error "Not a valid expression: $target"
	}
	if { $y1 == $y2 } {
	    return -code error "Conversion function does not depend on \$x"
	}
    }
    varDoSet /${ins}/setup/conversion -v $target
}
    
CAMP_FLOAT /~/setup/high_trip -D -S -T "High Trip" \
	-d on -s on -v 99999999.999 \
	-H "High trip level, over which to trip the alarm on adc_read" \
	-writeProc tip850_adc_high_trip_w
proc tip850_adc_high_trip_w { ins target } {
    varDoSet /${ins}/setup/high_trip -v $target
}

CAMP_FLOAT /~/setup/low_trip -D -S -T "Low Trip" \
	-d on -s on -v "-99999999.999" \
	-H "Low trip level, below which to trip the alarm on adc_read" \
	-writeProc tip850_adc_low_trip_w
proc tip850_adc_low_trip_w { ins target } {
    varDoSet /${ins}/setup/low_trip -v $target
}


