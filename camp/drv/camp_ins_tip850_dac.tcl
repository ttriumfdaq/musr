# camp_ins_tip850_dac.tcl
# Purpose:  This tcl script defines the DAC portion of a TEWS DatenTechnik TIP850
#	    DAC/ADC module. There are four DAC channels in the module.The core functions
#           online/offline/etc are declared here
# Inputs:   None
# Precond:  A functioning tcl interpreter must be running before the script can
#	    be processed. All the channels on the DAC/ADC board must be created
#           using the tipLib function tip850CreateAll.
# Outputs:  Returns a result code 
# Postcond: The driver will be created and available for access by the system

CAMP_INSTRUMENT /~ -D -T "Tip850 DAC" -d on \
    -initProc tip850_dac_init -deleteProc tip850_dac_delete \
    -onlineProc tip850_dac_online -offlineProc tip850_dac_offline

  proc tip850_dac_init { ins } { 
    varDoSet /${ins}/setup/IPPos -v 3
    insSet /${ins} -if none 0 0
    insIfOn /${ins} 
  }
  proc tip850_dac_delete { ins } { insSet /${ins} -line off }
  proc tip850_dac_online { ins } { insIfOn /${ins} }
  proc tip850_dac_offline { ins } { insIfOff /${ins} }

# DAC data declarations
# Purpose:  Defines the actions to write data to an ADC channel. The position and channel
#           are retrieved from the setup variables and a call to the C library is made.
# Inputs:   ~ is instrument name
# Precond:  Matching C function for write is available in camp_tcl.c
# Outputs:  error code if the call to insIfWrite fails
# Postcond: dac_set is resulting variable with data stored

# dac_set:  data for writing to dac

CAMP_INT /~/dac_set -D -S -L -T "Set DAC" -d on -s on \
        -writeProc tip850_dac_set_w
proc tip850_dac_set_w { ins target } {
    set dac_set $target
    if { ( $dac_set < -2047 ) || ( $dac_set > 2047 ) } {
	return -code error "dac_set (${dac_set}) out of range -2047<=dac_set<=2047"
    }
    set pos [varGetVal /${ins}/setup/IPPos]
    set chan [varGetVal /${ins}/setup/channel]
    if { $chan < 0 } { return -code error "DAC channel /${ins}/setup/channel needs to be set" }
    insIfWrite /${ins} "DacSet $pos $chan dac_set"
    varDoSet /${ins}/dac_set -v $dac_set
}
CAMP_INT /~/dac_incr -D -S -L -P -T "Increment DAC" -d on -s on -p off\
	-H "Change DAC setting by this value; \"set\" to set increment; poll to perform increment" \
        -readProc tip850_dac_incr_r -writeProc tip850_dac_incr_w
proc tip850_dac_incr_w { ins target } {
    varDoSet /${ins}/dac_incr -v $target
}
proc tip850_dac_incr_r { ins } {
    set i [varGetVal /${ins}/dac_incr]
    set d [varGetVal /${ins}/dac_set]
    varSet /${ins}/dac_set -v [expr $i+$d]
}

# setup:   structure for holding setup parameters which follow

CAMP_STRUCT /~/setup -D -T "Setup variables" -d on

# id:      id entry for this device
#          There is no way to test connection without mis-setting dac
#CAMP_SELECT /~/setup/id -D -R -T "ID query" -d on -r on \
#	-selections FALSE TRUE \
#	-readProc tip850_dac_id_r
#proc tip850_dac_id_r { ins } {
#    set id 0
#    if {[catch {varSet /${ins}/dac_set -v 0}] == 0} {set id 1}
#    varDoSet /${ins}/setup/id -v $id
#}


# IPPos:  position of Industry Pack on carrier
CAMP_INT /~/setup/IPPos -D -T "IP Position" \
	-H "Position of Industry Pack on carrier" \
        -d on -v 3
#	-writeProc tip850_dac_IPPos_w
#proc tip850_dac_IPPos_w { ins target } {
#    set IPPos $target
#    if { ( $IPPos < 0 ) || ( $IPPos > 3 ) } {
#	return -code error "IPPos out of range 0 to 3"
#    }
#    varDoSet /${ins}/setup/IPPos -v $IPPos
#}


# channel:  output channel number.  The read procedure is invoked
# once 5 seconds after the instrument is loaded.  If there is a valid
# channel already, we are probably rebooting the camp server, so the
# dac set value is applied to the actual dac output.

CAMP_INT /~/setup/channel -D -S -R -P -T "DAC Channel" \
        -d on -s on -r off -p on -p_int 5 -v -1 \
        -readProc tip850_dac_channel_r -writeProc tip850_dac_channel_w
proc tip850_dac_channel_w { ins target } {
    set channel $target
    if { ( $channel < 0 ) || ( $channel > 3 ) } {
        return -code error "Channel number out of range 0 to 3"
    }
    varDoSet /${ins}/setup/channel -v $channel
    varSet /${ins}/dac_set -v 0
}
proc tip850_dac_channel_r { ins } {
    if { [varGetVal /${ins}/setup/channel] >= 0 } {
	varSet /${ins}/dac_set -v [varGetVal /${ins}/dac_set]
    }
    varDoSet /${ins}/setup/channel -p off
}

