#   Varian TPS turbo pumping station

# At present, only reading pressure is implemented; no control.

CAMP_INSTRUMENT /~ -D -T "Varian TPS" \
    -H "Varian Compact Turbo Pump System" -d on \
    -initProc tps_init \
    -deleteProc tps_delete \
    -onlineProc tps_online \
    -offlineProc tps_offline
proc tps_init { ins } {
    insSet /$ins -if rs232 0.2 0.2 /tyCo/1 9600 8 none 1 none none
}
proc tps_delete { ins } {
    insSet /$ins -line off
}
proc tps_online { ins } {
    insIfOn /$ins
    if { [catch { varRead /$ins/unit } mes] } {
        if { [catch { varRead /$ins/unit } mes] } {
            insIfOff /$ins
            return -code error "failed query, $mes. Check interface definition and connections"
        }
    }
}
proc tps_offline { ins } {
    insIfOff /$ins
}

CAMP_FLOAT /~/pressure -D -R -P -L -T "Pressure" \
    -d on -r on -p off -p_int 9 \
    -H "Read Pressure" \
    -readProc tps_pressure_r

CAMP_FLOAT /~/log_p -D -R -L -T "Logarithm P" \
    -d on -r on -units "" -H "Logarithm of pressure reading" \
    -readProc tps_pressure_r

proc tps_pressure_r { ins } {
    set resp [tps_query $ins 224 14]
    if { [string length $resp] != 14 || [scan $resp "2240%f" p] != 1 } {
        return -code error [tps_err "Bad reading" $resp]
    }
    varDoSet /$ins/pressure -v $p
    if { $p > 0.0 } {
        varDoSet /$ins/log_p -v [format %.3f [expr {log10($p)}]]
    }
}

CAMP_SELECT /~/unit -D -R -P -L -T "Pressure units" \
    -d on -r on -p off -p_int 9 \
    -selections mBar Pa Torr \
    -H "Read pressure unit definition" \
    -readProc tps_unit_r

proc tps_unit_r { ins } {
    set resp [tps_query $ins 163 10]
    if { [string length $resp] != 10 || [scan $resp "1630%d" u] != 1 } {
        return -code error [tps_err "Bad reading" $resp]
    }
    varDoSet /$ins/unit -v $u
    varDoSet /$ins/pressure -units [lindex {mBar Pa Torr} $u]
}


proc tps_padstr { str } {
    format "%c%s%2.2X" 2 $str [tps_check $str]
}

proc tps_check { str } {
    set check 0 ; set i 0
    foreach c [split $str {}] {
        scan $c %c i
        set check [expr {$check ^ $i}]
    }
    return [expr {$check % 256}]
}

proc tps_query { ins win nc } {
    set req [format {%c%3.3d%1.1d%c} 0x80 $win 0 3] 
    set buf [insIfRead /$ins [tps_padstr $req] [expr 5+$nc]]
    set beg [expr {[string first \002 $buf]+1}]
    set end [string first \003 $buf]
    if { $beg > 0 && $end > $beg } {
        if { [scan [string range $buf $beg 99] "%c%\[^\003\]%*c%2x" add resp chk] == 3 } {
            if { $chk == [tps_check [string range $buf $beg $end]] } {
                return $resp
            } else {
                return -code error "Checksum error on read"
            }
        }
    }
    return -code error [tps_err "Failed query" $buf]
}

proc tps_err { str buf } {
    set ec 0; set et ""
    set beg [expr {[string first \002 $buf]+1}]
    set end [string first \003 $buf]
    if { $end <= $beg } { set end 42 }
    set buf [string range $buf $beg $end]
    if { [scan $buf "%c%\[^\003\]" ad ec] == 2 && [string length $ec] == 1 } {
        switch -exact -- $ec {
            "\006" { set et "Success" }
            "\025" { set et "Failure" }
            "\062" { set et "Unknown request code" }
            "\063" { set et "Data type mismatch" }
            "\064" { set et "Value out of range" }
            "\065" { set et "Setting read-only value" }
        }
    }
    if { [string length $str] && [string length $et] } { append str ": " }
    return "${str}${et}"
}

