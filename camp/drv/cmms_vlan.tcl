#! /usr/bin/tclsh
array set map {
   142.90.109.122  142.90.154.122
   142.90.101.151  142.90.154.151
   142.90.126.131  142.90.154.131
   142.90.126.132  142.90.154.132
   142.90.126.133  142.90.154.133
   142.90.126.134  142.90.154.134
   142.90.126.124  142.90.154.124
   142.90.126.53   142.90.154.53
   142.90.101.141  142.90.154.141
   142.90.101.142  142.90.154.142
   142.90.101.143  142.90.154.143
   142.90.101.144  142.90.154.144
   142.90.101.145  142.90.154.145
   142.90.126.110  142.90.154.127
}

foreach oldip [array names map] {
    puts "IP $oldip"
    set files {}
    catch {set files [exec fgrep -l $oldip {*}[glob camp_ins_*.*] ]}
    puts "Files that match $oldip: \n$files"
    foreach file $files {
	puts "file $file convert $oldip -> $map($oldip)"
	set mtime [file mtime $file]
	if { [catch { exec perl -pi -e "s/$oldip/$map($oldip)/g" $file } msg] } {
	    puts "Error updating $file: $msg"
	} else {
	    #file mtime $file $mtime
	}
    }
}