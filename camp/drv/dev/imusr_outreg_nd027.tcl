#
# OutReg (Output Register) procs for the ND027
#
# bit0 - Start
# bit1 - Clear
# bit2 and bit3 - Modulation (00=off,01=up,10=down)
# bit4 - Stop
# bit5 - Active
#
proc ImusrInitOutReg { ins } {
  ImusrSetOutReg $ins 0
}
proc ImusrSetOutRegStart { ins flag } {
  global $ins
  ImusrSetOutReg $ins [expr [set ${ins}(outregData)]|(1*$flag)]
}
proc ImusrSetOutRegClear { ins flag } {
  global $ins
  ImusrSetOutReg $ins [expr [set ${ins}(outregData)]|(2*$flag)]
}
proc ImusrSetOutRegModulation { ins flag } {
  global $ins
  # flag: 0=off, 1=up, 2=down
  ImusrSetOutReg $ins [expr [set ${ins}(outregData)]|(4*$flag)]
}
proc ImusrSetOutRegStop { ins flag } {
  global $ins
  ImusrSetOutReg $ins [expr [set ${ins}(outregData)]|(16*$flag)]
}
proc ImusrSetOutRegActive { ins flag } {
  global $ins
  ImusrSetOutReg $ins [expr [set ${ins}(outregData)]|(32*$flag)]
}
proc ImusrSetOutReg { ins data } {
  global $ins
  set slot [set ${ins}(outregSlot)]
  cdreg ext 0 0 $slot 0
  cfsa 16 $ext data q
  set ${ins}(outregData) $data 
}