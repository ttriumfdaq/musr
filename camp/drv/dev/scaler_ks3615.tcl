#
#  procs for Kinetic Systems 3615 scaler module
#
proc ScalerClear { b c n s } {
  cdreg ext $b $c $n $s
  cfsa 9 $ext data q
}
proc ScalerRead { b c n s } {
  cdreg ext $b $c $n $s
  cfsa 0 $ext data q
  return $data
}
proc ScalerReadAndClear { b c n s } {
  cdreg ext $b $c $n $s
  cfsa 2 $ext data q
  return $data
}

