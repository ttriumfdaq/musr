 # This file is magnet_procs_doc.tcl or magnet_procs.tcl.
 # Do not edit magnet_procs.tcl!
 # Instead, edit magnet_procs_doc.tcl, which is full of comments.
 # Donald Arseneau        last revised 29-Jul-2015
 #
proc mag_var_init { ins } {
    global MAG
    set MAG(namelist) [list none Helios  DR    Belle  bNMR   hiTime  nutime]
    set MAG(indlist)  [list 0.0  140.0   52.0  11.0   69.6   12.0    64.2]
    set MAG(callist)  [list -1  .096233 .0617 .0747  .10052 .09068   0.09950]
    set MAG($ins,catch) "-"
    set MAG($ins,induc) -99.99
    set MAG($ins,name) ""
    set MAG($ins,direct) "+"
    set MAG($ins,startI) 0.0
    set MAG($ins,prev_I) 0.0
    set MAG($ins,stop_n) 0
}

proc mag_set_w { ins target } {
    global MAG
    set MAG($ins,stype) 1
    mag_start_set $ins $target
}
proc mag_settle_w { ins target } {
    global MAG
    set MAG($ins,stype) 2
    mag_start_set $ins $target
}
proc mag_start_set { ins target } {
    global MAG
    if { [bounded $target [varGetVal /$ins/setup/mag_min] [varGetVal /$ins/setup/mag_max] ] != $target } {
	return -code error "setpoint out of range"
    }
    varDoSet /$ins/mag_field -p off
    varDoSet /$ins/abort -v 0
    varDoSet /$ins/controls/do_ramp -p off -p_int 0
    varDoSet /$ins/mag_set -v $target
    varDoSet /$ins/settle_set -v $target
    varDoSet /$ins/volts -p on
    mag_restore_Vmax $ins
    set h [varGetVal /$ins/heat_status]
    set rstat [varGetVal /$ins/ramp_status]
    set rmode [varGetVal /$ins/setup/ramp_mode]
    if { $rmode == 1 && $h == 0 } {
	return -code error "ramp_mode (persistent) incompatible with heat_status (none)"
    }
    set MAG($ins,prev_I) 0.0
    set MAG($ins,stop_n) 0
    if { $rstat >= 6 && $rstat <= 9 } { 
	set r 8
    } elseif { $h == 1 } {
	set r 3
    } else {
	if { $rstat == 0 && $rmode == 1 && \
		[varTestTol /$ins/mag_read [varGetVal /$ins/mag_set]] } {
	    set r 9
	} else {
	    set r 6
	}
    }
    varDoSet /$ins/ramp_status -v $r
    set r [expr [varNumGetTol /$ins/mag_read] - 0.0]
    if { [varNumGetTolType /$ins/mag_read] != 0 } {
	set r [expr ($r/100.0)*[varGetVal /$ins/setup/mag_max] ]
    }
    if { $r < $MAG($ins,tol) } { set r $MAG($ins,tol) }
    varDoSet /$ins/mag_read -tolType 0 -tol $r
    varRead /$ins/controls/do_ramp
}
proc mag_do_ramp_r { ins } {
  global MAG
  switch [varGetVal /$ins/ramp_status] {
    0 { # Holding -- should never happen
	varDoSet /$ins/controls/do_ramp -p off -p_int 0
      }
    1 - 
    10 { # Ramping or degaussing
	varRead /$ins/mag_field
	set mag_I [varGetVal /$ins/mag_read]
	set mag_S [varGetVal /$ins/mag_set]
	set mag_G [varGetVal /$ins/fast_set]
	if { [varGetVal /$ins/ramp_status] == 1 } {# Ramping
	  varDoSet /$ins/controls/do_ramp -p on -p_int 2.5
	  set mag_T [expr { abs( $mag_G - $mag_S ) <= $MAG($ins,tol) ? $MAG($ins,tol) : $MAG($ins,bigtol) }] 
	  if { [expr ($mag_I $MAG($ins,direct) $mag_T $MAG($ins,relat)= $mag_G)] } {
	    if { abs( $mag_G - $mag_S ) <= $MAG($ins,tol) } {
	      varDoSet /$ins/controls/do_ramp -p off -p_int 0
	      if { [varGetVal /$ins/degauss/degauss] } {
		varDoSet /$ins/ramp_status -v 10
		mag_degauss_init $ins $mag_S
		mag_degauss_step $ins $mag_S
	      } elseif { [varGetVal /$ins/setup/ramp_mode] == 0 && $MAG($ins,stype) != 2 } {
		varDoSet /$ins/ramp_status -v 0
		varDoSet /$ins/mag_field -p on -p_int 20
		varDoSet /$ins/controls/do_ramp -p off
	      } else {
		set MAG($ins,prev_OK) [expr {abs($mag_I-$mag_G) <= $mag_T}]
		mag_start_settle $ins
	      }
	    } else {
	      mag_set_next_ramp $ins $mag_G $mag_S
	    }
	  }
	} else { # Degaussing
	  varDoSet /$ins/controls/do_ramp -p on -p_int 2.0
	  if { $MAG($ins,degOff)*($mag_G-$mag_I) <= abs($MAG($ins,degOff))*$MAG($ins,tol) } {
	    set MAG($ins,prev_OK) [expr {abs($mag_I-$mag_G) <= $MAG($ins,tol)}]
	    mag_degauss_step $ins $mag_S
	  }
	}
	if { [varGetVal /$ins/abort] != 1 } {
	    set a [varRead /$ins/controls/activity; varGetVal /$ins/controls/activity]
	    if { $a > 2 } {
		varDoSet /$ins/ramp_status -v 11
	    } elseif { $a != 0 } {
		varSet /$ins/fast_set -v $mag_G
	    } elseif { $mag_I == $MAG(prev_I) } {# stopped without showing
		if { [incr MAG(prev_n)] >= 5 } {
		    varSet /$ins/fast_set -v $mag_G
		    set MAG(prev_n) 0
		}
	    } else {
		set MAG(prev_n) 0
	    }
	}
	set MAG(prev_I) $mag_I
      }
    2 { # Settling
	varRead /$ins/mag_field
	if { [varTestTol /$ins/mag_read [varGetVal /$ins/mag_set]] == 1 } {
	    if { $MAG($ins,prev_OK) } {
		mag_end_settle $ins
	    }
	    set MAG($ins,prev_OK) 1
	} else {
	    set MAG($ins,prev_OK) 0
	}
      }	
    3 {	# Persistent (at zero) Used when starting.  Initiate Ramp leads up to the persistent current.
	varDoSet /$ins/controls/do_ramp -p on -p_int 2
	varSet /$ins/controls/fast_leads -v 1
	varRead /$ins/controls/i_persist 
	varSet /$ins/fast_set -v [varGetVal /$ins/controls/i_persist]
	varDoSet /$ins/ramp_status -v 4
      }
    4 { # Ramping leads up
	varDoSet /$ins/controls/do_ramp -p on -p_int 2
	varRead /$ins/mag_read
	varRead /$ins/controls/i_persist
	set i [varGetVal /$ins/controls/i_persist]
	if { [varTestTol /$ins/mag_read $i ] == 1 } {
	    varSet /$ins/controls/fast_leads -v 0
	    varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	    varDoSet /$ins/ramp_status -v 8
	} else {
	    varSet /$ins/fast_set $i
	}
      }
    5 { # Ramping leads down
	varRead /$ins/volts
	set v [varGetVal /$ins/volts]
	set vl [bounded "abs(0.8*$MAG($ins,holdVolt))+0.1" "0.2" "0.95*[varGetVal /$ins/setup/volt_max]"]
	if { abs($v) >= $vl && $v*$MAG($ins,holdVolt) < 0.0 } {
	    if { [incr MAG($ins,badRamp-)] > 2 } {
		varSet /$ins/setup/heat_time -v [bounded "1.5*[varGetVal /$ins/setup/heat_time]" 60.0 180.0]
		varDoSet /$ins/ramp_status -v 3
		catch { varDoSet /$ins/heat_status -alert on -a_act just_beep }
		return
	    }
	}
	varRead /$ins/controls/ramp_status
	if { [varGetVal /$ins/controls/ramp_status] == 0 } {# Hold
	    varRead /$ins/mag_read
	    if { [varTestTol /$ins/mag_read 0.0 ] == 1 } {
		mag_restore_Vmax $ins
		varDoSet /$ins/ramp_status -v 3
		varDoSet /$ins/controls/do_ramp -p off -p_int 0
		varDoSet /$ins/mag_field -p on -p_int 30
		varSet /$ins/controls/fast_leads -v 0
	    } else {
		varSet /$ins/controls/activity -v 1
	    }
	}
      }
    6 { # (done) Heating switch so start ramping;
	mag_restore_Vmax $ins
	varDoSet /$ins/controls/do_ramp -p on -p_int 0.5
	varDoSet /$ins/ramp_status -v 1
	catch { varRead /$ins/mag_read }
	set MAG($ins,startI) [varGetVal /$ins/mag_read]
	if { [catch {mag_set_next_ramp $ins $MAG($ins,startI) [varGetVal /$ins/mag_set]} mess] } {
	    catch {mag_end_settle $ins}
	    return -code error $mess
	}
      }
    7 { # (done) Cooling switch.
	set MAG($ins,badRamp-) 0
	catch {
	    set pl 0
	    varDoSet /$ins/heat_status -alert off
	    set pl [varGetVal /$ins/setup/pers_leads]
	}
	if { $pl } {
	    varDoSet /$ins/ramp_status -v 3
	    varDoSet /$ins/controls/do_ramp -p off -p_int 0
	    varDoSet /$ins/mag_field -p on -p_int 30
	} else {
	    mag_reduce_Vmax $ins
	    varSet /$ins/setup/volt_max -v [bounded "abs($MAG($ins,holdVolt))+0.3" 0.2 $MAG($ins,maxVolt)]
	    varSet /$ins/controls/fast_leads -v 1
	    varDoSet /$ins/controls/do_ramp -p on -p_int 2
	    varSet /$ins/controls/activity -v 1
	    varDoSet /$ins/ramp_status -v 5
	}
      }
    8 { # Turning heater on.  Use safe hold mode. set heater on; verify it is on
	varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	varSet /$ins/controls/activity -v 2
	varSet /$ins/heat_status -v 2
	varRead /$ins/heat_status
	if { [varGetVal /$ins/heat_status] > 1 } {
	    varDoSet /$ins/controls/do_ramp -p on -p_int [expr {0.1+[varGetVal /$ins/setup/heat_time]}]
	    varDoSet /$ins/ramp_status -v 6
	}
      }
    9 { # Turning heater off.  Set heater off and verify.
	varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	varSet /$ins/heat_status -v 1
	varRead /$ins/heat_status
	if { [varGetVal /$ins/heat_status] == 1 } {
	    varDoSet /$ins/controls/do_ramp -p on -p_int [expr {0.1+[varGetVal /$ins/setup/heat_time]}]
	    varDoSet /$ins/ramp_status -v 7
	}
      }
    default {
	varDoSet /$ins/controls/do_ramp -p on -p_int 5
      }
  }
}
proc mag_set_next_ramp { ins start target } {
    global MAG
    set i 0
    if { $start > $target } {
	set MAG($ins,direct) "-"
	set MAG($ins,relat) "<"
	while { [lindex $MAG($ins,currents) $i] < $start } {incr i}
	varSet /$ins/controls/ramp_rate -v [ lindex $MAG($ins,ramps) $i ] 
	varSet /$ins/fast_set -v [bounded \
		[lindex [linsert $MAG($ins,currents) 0 -999.] $i] $target $start]
    } else {
	set MAG($ins,direct) "+"
	set MAG($ins,relat) ">"
	while { [lindex $MAG($ins,currents) $i] <= $start } {incr i}
	varSet /$ins/controls/ramp_rate -v [lindex $MAG($ins,ramps) $i]
	varSet /$ins/fast_set -v [bounded [lindex $MAG($ins,currents) $i] $start $target]
    }
    varRead /$ins/controls/ramp_status
}

proc mag_start_settle { ins } {
    varDoSet /$ins/ramp_status -v 2
    varDoSet /$ins/controls/do_ramp -p off
    varDoSet /$ins/controls/do_ramp -p on -p_int [varGetVal /$ins/setup/settle_time]
    varRead /$ins/controls/ramp_status
}
proc mag_end_settle { ins } {
    global MAG
    varDoSet /$ins/controls/do_ramp -p off -p_int 0
    if { [varGetVal /$ins/setup/ramp_mode] } {
	varRead /$ins/volts
	set MAG($ins,holdVolt) [varGetVal /$ins/volts]
	mag_remember_Vmax $ins
	varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	varDoSet /$ins/ramp_status -v 9
    } else {
	varDoSet /$ins/ramp_status -v 0
	varDoSet /$ins/mag_field -p on -p_int 30
    }
}
proc mag_remember_Vmax { ins } {
    global MAG
    if { ! [info exists MAG($ins,maxVolt)] } {
	varRead /$ins/setup/volt_max
	set MAG($ins,maxVolt) [varGetVal /$ins/setup/volt_max]
    }
}
proc mag_restore_Vmax { ins } {
    global MAG
    if { [info exists MAG($ins,maxVolt)] } {
	varSet /$ins/setup/volt_max -v $MAG($ins,maxVolt)
	unset MAG($ins,maxVolt)
    }
}
proc mag_reduce_Vmax { ins } {
    global MAG
    mag_remember_Vmax $ins
    varSet /$ins/setup/volt_max -v [bounded "abs($MAG($ins,holdVolt))+0.3" 0.2 $MAG($ins,maxVolt)]
}
proc mag_ramp_mode_r { ins } {
    return
}
proc mag_ramp_mode_w { ins target } {
    global MAG
    varRead /$ins/mag_field
    varRead /$ins/heat_status
    if { [varGetVal /$ins/setup/ramp_mode] != $target } {# change requested
	if { [varGetVal /$ins/ramp_status] == 3 } {
	    set curr [expr ([varGetVal /$ins/mag_field] + 0.0) / ([varGetVal /$ins/setup/calibration])]
	} else {
	    set curr [varGetVal /$ins/mag_set]
	}
	if { $target && [varGetVal /$ins/heat_status ] == 0 } { 
	    return -code error "persistent mode ramping is impossible without a switch heater"
	}
	set MAG($ins,stype) 1
	varDoSet /$ins/setup/ramp_mode -v $target
	mag_start_set $ins $curr
    }
}


proc mag_abort_w { ins target } {
    set prev [varGetVal /$ins/abort]
    set rs [varGetVal /$ins/ramp_status]
    set hs [varRead /$ins/heat_status; varGetVal /$ins/heat_status]
    switch $target {
	0 {# ---- Abort ----
	    varDoSet /$ins/degauss/degauss -v 0
	    switch $rs {
		0 -
		3 {
		    varDoSet /$ins/controls/do_ramp -p off
		  }
		10 -
		1 {
		    if { $hs == 1 } {# heater off -- don't turn it on!
			mag_end_settle $ins
		    } else {
			varSet /$ins/controls/activity -v 2
			varRead /$ins/mag_read
			varSet /$ins/mag_set -v [varGetVal /$ins/mag_read]
		    }
		  }
		2 -
		9 {
		    mag_end_settle $ins
		  }
		4 -
		6 -
		8 {
		    set s [varGetVal /$ins/fast_set]
		    varDoSet /$ins/mag_set -v $s
		    varDoSet /$ins/settle_set -v $s
		    if { $hs != 1 } {# heater on or absent -- continue with end-ramp
			mag_end_settle $ins
		    } else {# heater off -- don't turn it on!
			varDoSet /$ins/ramp_status -v 9
			varDoSet /$ins/controls/do_ramp -p on -p_int 2
		    }
		  }
		5 -
		7 {
		    varRead /$ins/mag_read
		    if { abs([varGetVal /$ins/mag_read]) < 0.1 } {
			varDoSet /$ins/ramp_status -v 3
			varDoSet /$ins/controls/do_ramp -p off
		    } else {
			mag_end_settle $ins
		    }
		  }
	    }
	    varDoSet /$ins/abort -v 0
	  }
	1 {# ---- Pause ----
	    varDoSet /$ins/controls/do_ramp -p off
	    if { $rs != 0 && $rs != 3 && $prev != 1 } {
		varRead /$ins/controls/activity
		set a [varGetVal /$ins/controls/activity]
		varSet /$ins/controls/activity -v 2
		varDoSet /$ins/controls/activity -v $a
		varDoSet /$ins/abort -v 1
	    }
	  }
	2 {# ---- Resume ----
	    if { $prev == 1 } {
		varSet /$ins/controls/activity -v [varGetVal /$ins/controls/activity]
		varDoSet /$ins/controls/do_ramp -p on
		varDoSet /$ins/abort -v 0
	    }
	  }
	default {# ---- "Never mind"
	  }
    }
    varDoSet /$ins/mag_field -p on -p_int 30
}



proc mag_degauss_set_field_w { ins target } {
    varSet /$ins/degauss/set_field -v $target
    varSet /$ins/degauss/degauss -v 1
}

proc mag_deg_ampl_w { ins target } {
    varDoSet /$ins/degauss/amplitude -v $target
}

proc mag_deg_decrement_w { ins target } {
    if { $target < 1.0 || $target > 50. } {
	return -code error "Value out of range 1-50"
    }
    varDoSet /$ins/degauss/decrement -v $target
}
 
proc mag_deg_setf_w { ins target } {
    varDoSet /$ins/degauss/set_field -v $target
}

proc mag_degauss_w { ins target } {
    global MAG
    varDoSet /$ins/degauss/degauss -v $target
    if { $target } {
	varDoSet /$ins/degauss_field -v [varGetVal /$ins/degauss/set_field]
	varSet /$ins/mag_field -v [varGetVal /$ins/degauss/set_field]
    } elseif { [varGetVal /$ins/ramp_status] == 10 } {
	set ms [varGetVal /$ins/mag_set]
	varRead /$ins/mag_read
	set mr [varGetVal /$ins/mag_read]
	varRead /$ins/heat_status
	set off [expr {[varGetVal /$ins/degauss/amplitude]/([varGetVal /$ins/setup/calibration]+.001)}]
	if { [varGetVal /$ins/heat_status] == 1 } {
	    varDoSet /$ins/controls/do_ramp -p off
	    varDoSet /$ins/ramp_status -v 3
	} elseif { abs($ms-$mr) <= abs($off) + .001 } {
	    mag_degauss_step $ins [varGetVal /$ins/mag_set]
	} else {
	    varSet /$ins/mag_set -v $ms
	}
    }
}
proc mag_degauss_init { ins target } {
    global MAG
    set cal [varGetVal /$ins/setup/calibration]
    set fo [varGetVal /$ins/degauss/amplitude]
    if { $fo < 0.001 } {
	set fo [expr { abs($target-$MAG($ins,startI)) * 0.0025 * $cal + 0.002 } ]
    }
    set fo [expr "0.0 $MAG($ins,direct) $fo"]
    set MAG($ins,degOin) 0.0
    set MAG($ins,degOff) [expr {$fo/$cal}]
    varDoSet /$ins/ramp_status -v 10
    varDoSet /$ins/controls/do_ramp -p off
    varDoSet /$ins/controls/do_ramp -p on -p_int 4
}
proc mag_degauss_step { ins target } {
    global MAG
    if { [varGetVal /$ins/degauss/degauss] == 0 } {
	set new 0.0
    } elseif { $MAG($ins,degOin) == 0.0 } {
	set MAG($ins,degOin) $MAG($ins,degOff)
    } else {
	set pct [varGetVal /$ins/degauss/decrement]
	set new [expr { ($MAG($ins,degOff) < 0.0 ? 1.0 : -1.0) * ( abs($MAG($ins,degOff)) - ($pct/200.0) * (abs($MAG($ins,degOff)) + abs($MAG($ins,degOin))) ) }]
	if { $new * $MAG($ins,degOff) >= 0. } {
	    set new 0.0
	}
	set MAG($ins,degOff) $new
    }
    set sp [expr {$target + $MAG($ins,degOff)}]
    set sp [bounded $sp [varGetVal /$ins/setup/mag_min] [varGetVal /$ins/setup/mag_max] ] 
    varSet /$ins/fast_set -v $sp
    varDoSet /$ins/degauss_field -m "Go to [format %.4f $target][format %+.4f $MAG($ins,degOff)] = [format %.4f $sp]"
    if { $MAG($ins,degOff) == 0.0 } {
	varDoSet /$ins/degauss/degauss -v 0
	varDoSet /$ins/degauss_field -m ""
	mag_start_settle $ins
    }
}


proc mag_heat_time_w { ins target } {
    varDoSet /$ins/setup/heat_time -v $target
}
proc mag_settle_time_w { ins target } {
    varDoSet /$ins/setup/settle_time -v [bounded $target 1. 999.]
}
proc mag_pers_leads_w { ins target } {
    varDoSet /$ins/setup/pers_leads -v $target
    if { $target == 0 && [varGetVal /$ins/ramp_status] == 3 } {
	varDoSet /$ins/ramp_status -v 5
	varSet /$ins/controls/fast_leads -v 1
	varDoSet /$ins/controls/do_ramp -p on -p_int 2
	varSet /$ins/controls/activity -v 1
    }
}
proc mag_refresh_r { ins } {
    if { [varGetVal /$ins/ramp_status ] == 3 } {
	varRead /$ins/controls/i_persist
	varSet /$ins/mag_set -v [varGetVal /$ins/controls/i_persist ]
    }
}
proc mag_refresh_w { ins target } {
    mag_refresh_r $ins
}


proc bounded { x lo hi } {
    expr "($x)<($lo)? ($lo) : (($x)>($hi)? ($hi) : ($x))"
}

