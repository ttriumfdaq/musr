 # This file is magnet_procs_doc.tcl or magnet_procs.tcl.
 # Do not edit magnet_procs.tcl!
 # Instead, edit magnet_procs_doc.tcl, which is full of comments.
 # Donald Arseneau        last revised 29-Jul-2015
 #
#
# This is magnet_procs_doc.tcl
#
# magnet_procs.tcl is created by stripping the comment lines out of
# magnet_procs_doc.tcl (delete all lines that BEGIN with '#').
# grep -v "^#" magnet_procs_doc.tcl > magnet_procs.tcl
#
# This file contains procedures for controlling supercoducting power supplies,
# and so magnet_procs.tcl should be loaded by the driver for any s.c. magnet 
# power supply.
#
# Of course, there are some assumptions made about the functioning of the main driver.  
# Here are the camp variables that are expected for a functioning magnet (where "~" means 
# the instrument name)
#
# /~/fast_set              Set: ramp mag to setting. Read: present ramp/hold target.
#			   Setting fast_set should, with little or no checking, set the
#			   "setpoint" and cause the P.S. to go to that setpoint.
# /~/heat_status           Set: turn heater on/off.  Read: report status.
#			   selections: none off on force_on
# /~/mag_read              Reads actual P.S. output current (not persistent field)
# /~/mag_set               Ret current with full processing.  Value is nominal ultimate
#			   setpoint
# /~/ramp_status           Status display and index (neither read nor write)
# /~/settle_set            Set: mag_set plus settling to tolerance (of mag_read)
# /~/degauss_field         Set: sets /~/degauss/set_field then activates /~/degauss/degauss
# /~/mag_field             Field in magnet: current*calibration.  set: perform mag_set
#			   value/calibration; read: report remembered persistent current
#			   in magnet (MUST also read the current, even if the power
#	                   supply will provide the field value directly).
# /~/abort		   Settable selection to abort or pause a ramp in progress.
#			   Selections are: Abort, Pause, Resume, Never mind.
# /~/setup/heat_time       (config) time for switch to heat up or cool down
# /~/setup/mag_max         (config) max current
# /~/setup/mag_min         (config) min current
# /~/setup/magnet          User-entered name of magnet.  Used to load other configs.
# /~/setup/ramp_mode       User-requested ramp mode. selections: regular persistent
#			   (check heat_status when setting)
# /~/setup/ramp_rate       User-requested ramp rate (0 means none) The actual ramp rate
#			   will be the lesser of this value and the configured set of
#			   ramp rates. (Not required.)
# /~/setup/settle_time     Interval for checking on settling
# /~/setup/calibration     (config) Magnet calibration T/A
#
# /~/degauss/amplitude     Set magnitude of field oscillations to use for degaussing magnet
# /~/degauss/decrement     Percent decrement for each cycle of degaussing
# /~/degauss/set_field     Set the final field for after degaussing magnet
# /~/degauss/degauss       Start/stop degaussing
#
# There are more required variables, not usually of concern to the user, which control
# operations (when things go wrong it can help to look at them):
#
# /~/controls/activity     Select zero or setpoint.  Should use instrument's "zero" and
#			   "mid" functions, but can use a setpoint. Required selections
#			   are: 0 = setpoint, 1 = zero; 2 = pause, hold, or setpoint;
#			   more states can be added for particular power supplies. 
#			   Note that fast_set targets the setpoint but might not update
#			   this variable.
# /~/controls/do_ramp      This is the engine which performs ramping operations.  It is
#			   a finite-state automaton, where polling defines each iteration
#			   ("generation") and the main state variable is /~/ramp_status.
#			   It must have readProc = mag_do_ramp_r (defined below).
# /~/controls/fast_leads   Set a fast ramp for leads (1=leads, 0=magnet)
# /~/controls/ramp_rate    Actual ramp_rate;  (lesser of config segments and
#			   /~/setup/ramp_rate) OR the fast leads rate.
# /~/controls/ramp_status  Set/read: instrument ramp status HOLDING RAMPING.
# /~/controls/i_persist	   Nominal persistent current in magnet; may differ from P.S.
#			   output.  May be meaningless when heater is on.
# /~/controls/inductance   Magnet inductance
#
# The global array MAG holds more parameters.  Three entries are universal, and are used
# for identifying which magnet the power supply has previously been used with.
#
# MAG(namelist)		list of magnet names
# MAG(indlist) 		list of corresponding magnet inductances
# MAG(callist) 		list of magnet calibration constants
#
# The other entries are specific to each instrument, "$ins", and are identified as
# MAG($ins,<ident>).  Some are magnet parameters:
#
# MAG($ins,name)	name of magnet; set in the magnet .ini file
# MAG($ins,induc)	magnet inductance when last initialized
# MAG($ins,currents)	list of output currents where we should change ramp rates
#			(plus bogus low zeroth entry)
# MAG($ins,ramps)	list of ramp rates.  Each entry is the maximum r.r. below the
#			corresponding entry in "currents"
# MAG($ins,leadsramp)	The (fast) ramp rate for ramping the leads
#
# Some are related to the power supply:
#
# MAG($ins,tol)		default small tolerance on mag_read, used if tolerance was not
#			set.  Should reflect the precision and stability of the PS.
# MAG($ins,bigtol)	default big tolerance on mag_read, used for switching segments
#
# And others are global internal variables:
#
# MAG($ins,PS)		type of power supply for instrument
# MAG($ins,stype)	sweep type for non-persistent (stopping condition)
#			1: regular (mag_set), 2: set and settle (settle_set).
# MAG($ins,direct)	direction of sweep (+ or -)
# MAG($ins,relat)	relational test corresponding to direction (">" or "<")
# MAG($ins,prev_OK)	used when settling to indicate previous test was in tolerance
#			(0: no, 1: yes)
# MAG($ins,prev_I)	previous mag_read (current); used to detect halts
# MAG($ins,stop_n)	counts iterations ramp has been halted
# MAG($ins,degauss)	stage of degaussing procedure
# MAG($ins,degOff)	present offset in amps from target magnet current while degaussing
# MAG($ins,degOin)	initial offset in amps from target magnet current while degaussing
#
# And one is only used by one type of power supply:
#
# MAG($ins,upload)	0 usually, but 1 when we want to load memory to ox_ips120
#
# mag_var_init is a proc to initialize some elements of the MAG array.  It must be called
# by the driver's initProc.
# First, we set the list of known magnet inductances.  These can be used to identify the
# magnet.  The order of these entries must correspond, and they must be the same order as
# the list for the magnet selection (/~/setup/magnet).  Each of the entries in the name
# list must have an initialization file "drv/magnet_<name>.ini". 
# Following are a few other variables used in the ramping proc.
#
proc mag_var_init { ins } {
    global MAG
    set MAG(namelist) [list none Helios  DR    Belle  bNMR   hiTime  nutime]
    set MAG(indlist)  [list 0.0  140.0   52.0  11.0   69.6   12.0    64.2]
    set MAG(callist)  [list -1  .096233 .0617 .0747  .10052 .09068   0.09950]
    set MAG($ins,catch) "-"
    set MAG($ins,induc) -99.99
    set MAG($ins,name) ""
    set MAG($ins,direct) "+"
    set MAG($ins,startI) 0.0
    set MAG($ins,prev_I) 0.0
    set MAG($ins,stop_n) 0
}

#
# Here are the writeProcs for setting magnets.  They start up the ramping engine
# by invoking mag_start_set.
#
proc mag_set_w { ins target } {
    global MAG
    set MAG($ins,stype) 1
    mag_start_set $ins $target
}
proc mag_settle_w { ins target } {
    global MAG
    set MAG($ins,stype) 2
    mag_start_set $ins $target
}
#
# The following routine, mag_start_set, starts up the ramping processor.  It is invoked
# by the different types of magnet-set (mag_set, field_set, settle_set), and by a change
# of ramp mode.  The global variable MAG($ins,stype) indicates which type of "set" is being
# performed; in particular, it indicates the point where the ramping process should end
# for a non-persistent ramp mode 
#
# stype 1: regular (non-persistent) mag_set -- end after ramping magnet
#	2: set and settle (non-persistent) -- end after settling
#
# In persistent mode, we always end after ramping the leads to zero.
#
# The main function of mag_start_set is to jump into mag_do_ramp_r, which would be easy
# if everything is quiescent, but if there is a ramp in progress mag_start_set must
# seamlessly change to the new ramp process.  The stages of the ramp processor are the
# values of ramp_status:
#
#      value  name           Sequence
#	 0    Holding           *
#	 1    Ramping           4
#	 2    Settling          6
#	 3    Persistent        0,10
#	 4    Ramp leads +      1
#	 5    Ramp leads -      9
#	 6    Heat switch       3
#	 7    Cool switch       8
#	 8    Turn on Heat      2
#	 9    Turn off Heat     7
#	10    Degauss           5
#	11    QUENCH            -
#
# The name gives the current and previous status; thus when ramp_status=1 the ramping has
# already begun.  For regular, non-persistent, ramping, mag_do_ramp_r is started with
# ramp_status=6, which is the stage that *starts* ramping (after heating the switch).  
# (If it gets stuck at this point -- from bad communication, say -- then the ramp_status
# display will be a confusing "Heat switch".  Sorry.)  For persistent-mode ramping, the
# sequence is usually started at status 3, but status 8 or 6 may be used when interrupting
# a ramp in progress, and 2 is used for going from "holding" to persistent.  In detail:
#
# when (ramp_status = 6, 7, 8, or 9)  start at 8 (Turn on Heat) which initiates 
#	              6 (Heat switch), which waits for heat_time before ramping.
# else, if heater is off (but present)  start at 3 (Persistent) which initiates
#	              4 (Ramp leads +)
# else (heater on or absent):
#     if (ramp_status = 0 "holding" and ramp_mode = 1 "persistent") 
#	   start at 9 (turn off heat)
#     otherwise, start at 6 (Heat switch) which initiates 1 (Ramping)
#
# Finally, after choosing the start point, mag_start_set verifies a useful tolerance
# value on mag_read, and invokes do_ramp by polling.
#
proc mag_start_set { ins target } {
    global MAG
    if { [bounded $target [varGetVal /$ins/setup/mag_min] [varGetVal /$ins/setup/mag_max] ] != $target } {
	return -code error "setpoint out of range"
    }
    varDoSet /$ins/mag_field -p off
#   # Cancel any "paused" ramp:
    varDoSet /$ins/abort -v 0
#   # Abort any ramping in progress:
    varDoSet /$ins/controls/do_ramp -p off -p_int 0
    varDoSet /$ins/mag_set -v $target
    varDoSet /$ins/settle_set -v $target
    varDoSet /$ins/volts -p on
    mag_restore_Vmax $ins
    set h [varGetVal /$ins/heat_status]
    set rstat [varGetVal /$ins/ramp_status]
    set rmode [varGetVal /$ins/setup/ramp_mode]
#   # 
    if { $rmode == 1 && $h == 0 } {
	return -code error "ramp_mode (persistent) incompatible with heat_status (none)"
    }
    set MAG($ins,prev_I) 0.0
    set MAG($ins,stop_n) 0
#   # Choose starting point in sequencer:
    if { $rstat >= 6 && $rstat <= 9 } { 
#	# Heat / Cool switch / Turn on / off Heat : start with "turn on heat"
	set r 8
    } elseif { $h == 1 } {
#	# Heater is off (but present): start with "persistent" -> "ramp leads +"
	set r 3
    } else {
#	# Heater is on or absent
	if { $rstat == 0 && $rmode == 1 && \
		[varTestTol /$ins/mag_read [varGetVal /$ins/mag_set]] } {
#	    # We are currently "holding" at our setpoint but ramp_mode is "persistent",
#	    # so we are changing ramp mode.  Start with "turn off heat".
	    set r 9
	} else {
#	    # Other situations where heater is on or absent, start with "heat switch" then ramping..
	    set r 6
	}
    }
    varDoSet /$ins/ramp_status -v $r
#   # Now confirm a valid tolerance:
    set r [expr [varNumGetTol /$ins/mag_read] - 0.0]
    if { [varNumGetTolType /$ins/mag_read] != 0 } {
	set r [expr ($r/100.0)*[varGetVal /$ins/setup/mag_max] ]
    }
    if { $r < $MAG($ins,tol) } { set r $MAG($ins,tol) }
    varDoSet /$ins/mag_read -tolType 0 -tol $r
#   # execute ramp sequencer
    varRead /$ins/controls/do_ramp
}
#
#   This routine, mag_do_ramp_r, is the sequencer or engine for the ramping process,
#   and it is the readProc for /~/controls/do_ramp which is polled while ramping. (It
#   is a readProc to allow polling, although it functions more as a writeProc.) There
#   are several stages or states in the ramping process indicated by /~/ramp_status
#   (see above), and this routine is a big "switch" on ramp_status.  Each time
#   mag_do_ramp_r is executed, ramp_status reflects the status during the preceding
#   wait period.  When a particular action completes successfully, ramp_status is
#   changed. 
#
#   If something fails with an error, that poll is silently terminated, but care is
#   taken so that the same step is repeated in those cases. In particular, ramp_status
#   and the poll interval are changed after the various instrument setting commands so
#   if they fail they will be repeated on the next poll of mag_do_ramp_r.
#
#   When starting a ramp, do_ramp is read directly (by mag_start_set) with ramp_status
#   set to the stage *before* the nominal starting point to initiate that first desired
#   stage. This first read can report error messages that later polling cannont.
#
proc mag_do_ramp_r { ins } {
  global MAG
  switch [varGetVal /$ins/ramp_status] {
    0 { # Holding -- should never happen
	varDoSet /$ins/controls/do_ramp -p off -p_int 0
      }
    1 - 
    10 { # Ramping or degaussing
#	# Read field (also reads "mag_read")
	varRead /$ins/mag_field
	set mag_I [varGetVal /$ins/mag_read]
	set mag_S [varGetVal /$ins/mag_set]
	set mag_G [varGetVal /$ins/fast_set]
	if { [varGetVal /$ins/ramp_status] == 1 } {# Ramping
	  varDoSet /$ins/controls/do_ramp -p on -p_int 2.5
#	  # Use big tolerance for waystations, but normal tolerance for final goal.
	  set mag_T [expr { abs( $mag_G - $mag_S ) <= $MAG($ins,tol) ? $MAG($ins,tol) : $MAG($ins,bigtol) }] 
#	  # Check if we are within tolerance of the immediate goal, or have gone past it
	  if { [expr ($mag_I $MAG($ins,direct) $mag_T $MAG($ins,relat)= $mag_G)] } {
#	    # Reached (next or final) goal
	    if { abs( $mag_G - $mag_S ) <= $MAG($ins,tol) } {
#	      # This goal is the final goal
	      varDoSet /$ins/controls/do_ramp -p off -p_int 0
	      if { [varGetVal /$ins/degauss/degauss] } {
#		# When degaussing selected, go on to degauss stage
		varDoSet /$ins/ramp_status -v 10
		mag_degauss_init $ins $mag_S
		mag_degauss_step $ins $mag_S
	      } elseif { [varGetVal /$ins/setup/ramp_mode] == 0 && $MAG($ins,stype) != 2 } {
#		# For non-persistent mag_set (or change-mode-to-non-persistent): finished
		varDoSet /$ins/ramp_status -v 0
		varDoSet /$ins/mag_field -p on -p_int 20
		varDoSet /$ins/controls/do_ramp -p off
	      } else {
#		# persistent mode or settle_set: go on to settling
		set MAG($ins,prev_OK) [expr {abs($mag_I-$mag_G) <= $mag_T}]
		mag_start_settle $ins
	      }
	    } else {
#	      # just reached an intermediate goal. Do next segment (another ramp rate)
	      mag_set_next_ramp $ins $mag_G $mag_S
	    }
	  }
	} else { # Degaussing
	  varDoSet /$ins/controls/do_ramp -p on -p_int 2.0
	  if { $MAG($ins,degOff)*($mag_G-$mag_I) <= abs($MAG($ins,degOff))*$MAG($ins,tol) } {
	    set MAG($ins,prev_OK) [expr {abs($mag_I-$mag_G) <= $MAG($ins,tol)}]
	    mag_degauss_step $ins $mag_S
	  }
	}
#	# whether degaussing or ramping, check for quench or accidental hold state
	if { [varGetVal /$ins/abort] != 1 } {
	    set a [varRead /$ins/controls/activity; varGetVal /$ins/controls/activity]
	    if { $a > 2 } {
		varDoSet /$ins/ramp_status -v 11
	    } elseif { $a != 0 } {
		varSet /$ins/fast_set -v $mag_G
	    } elseif { $mag_I == $MAG(prev_I) } {# stopped without showing
		if { [incr MAG(prev_n)] >= 5 } {
		    varSet /$ins/fast_set -v $mag_G
		    set MAG(prev_n) 0
		}
	    } else {
		set MAG(prev_n) 0
	    }
	}
	set MAG(prev_I) $mag_I
      }
    2 { # Settling
	varRead /$ins/mag_field
	if { [varTestTol /$ins/mag_read [varGetVal /$ins/mag_set]] == 1 } {
	    if { $MAG($ins,prev_OK) } {
		mag_end_settle $ins
	    }
	    set MAG($ins,prev_OK) 1
	} else {
	    set MAG($ins,prev_OK) 0
	}
      }	
    3 {	# Persistent (at zero) Used when starting.  Initiate Ramp leads up to the persistent current.
	varDoSet /$ins/controls/do_ramp -p on -p_int 2
	varSet /$ins/controls/fast_leads -v 1
	varRead /$ins/controls/i_persist 
#	It is important to read i_persist before setting fast_set (AMI magnet mixes them together)
	varSet /$ins/fast_set -v [varGetVal /$ins/controls/i_persist]
	varDoSet /$ins/ramp_status -v 4
      }
    4 { # Ramping leads up
	varDoSet /$ins/controls/do_ramp -p on -p_int 2
	varRead /$ins/mag_read
	varRead /$ins/controls/i_persist
	set i [varGetVal /$ins/controls/i_persist]
	if { [varTestTol /$ins/mag_read $i ] == 1 } {
#	    # Reached setting: cancel fast-ramp, declare attempt-heater-on
	    varSet /$ins/controls/fast_leads -v 0
	    varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	    varDoSet /$ins/ramp_status -v 8
	} else {
#	    # Not there yet.  In case it dropped into hold mode:
	    varSet /$ins/fast_set $i
	}
      }
    5 { # Ramping leads down
	varRead /$ins/volts
	set v [varGetVal /$ins/volts]
	set vl [bounded "abs(0.8*$MAG($ins,holdVolt))+0.1" "0.2" "0.95*[varGetVal /$ins/setup/volt_max]"]
	if { abs($v) >= $vl && $v*$MAG($ins,holdVolt) < 0.0 } {
	    if { [incr MAG($ins,badRamp-)] > 2 } {
#	       # Recovery procedure when SC switch fails to go persistent for leads ramp-to-zero
#	       # Go back to ramp-leads-up with the modest voltage limit already set.
		varSet /$ins/setup/heat_time -v [bounded "1.5*[varGetVal /$ins/setup/heat_time]" 60.0 180.0]
		varDoSet /$ins/ramp_status -v 3
		catch { varDoSet /$ins/heat_status -alert on -a_act just_beep }
		return
	    }
	}
	varRead /$ins/controls/ramp_status
	if { [varGetVal /$ins/controls/ramp_status] == 0 } {# Hold
	    varRead /$ins/mag_read
	    if { [varTestTol /$ins/mag_read 0.0 ] == 1 } {
#		# reached zero.  Finished.  Declare persistent. 
		mag_restore_Vmax $ins
		varDoSet /$ins/ramp_status -v 3
		varDoSet /$ins/controls/do_ramp -p off -p_int 0
		varDoSet /$ins/mag_field -p on -p_int 30
#		# Apply slow_mag ramp rate in case somebody attempts manual control 
		varSet /$ins/controls/fast_leads -v 0
	    } else {
#		# In case it dropped into hold mode:
		varSet /$ins/controls/activity -v 1
	    }
	}
      }
    6 { # (done) Heating switch so start ramping;
	mag_restore_Vmax $ins
#	# set a very short poll interval first, so test for ramping quickly
	varDoSet /$ins/controls/do_ramp -p on -p_int 0.5
#	# fast_set might not be the actual current if we are joining a ramp in progress, 
#	# so read the current.
	varDoSet /$ins/ramp_status -v 1
	catch { varRead /$ins/mag_read }
	set MAG($ins,startI) [varGetVal /$ins/mag_read]
	if { [catch {mag_set_next_ramp $ins $MAG($ins,startI) [varGetVal /$ins/mag_set]} mess] } {
	    catch {mag_end_settle $ins}
	    return -code error $mess
	}
      }
    7 { # (done) Cooling switch.
	set MAG($ins,badRamp-) 0
	catch {
	    set pl 0
	    varDoSet /$ins/heat_status -alert off
	    set pl [varGetVal /$ins/setup/pers_leads]
	}
	if { $pl } {
#	    # Skip zeroing leads.  Finished.  Declare persistent. 
	    varDoSet /$ins/ramp_status -v 3
	    varDoSet /$ins/controls/do_ramp -p off -p_int 0
	    varDoSet /$ins/mag_field -p on -p_int 30
	} else {
#	    # Now ramp leads down, fast, with a reduced voltage limit.
	    mag_reduce_Vmax $ins
	    varSet /$ins/setup/volt_max -v [bounded "abs($MAG($ins,holdVolt))+0.3" 0.2 $MAG($ins,maxVolt)]
	    varSet /$ins/controls/fast_leads -v 1
	    varDoSet /$ins/controls/do_ramp -p on -p_int 2
	    varSet /$ins/controls/activity -v 1
	    varDoSet /$ins/ramp_status -v 5
	}
      }
    8 { # Turning heater on.  Use safe hold mode. set heater on; verify it is on
#	# set a short poll interval first in case one of the varSet/Reads fails.
	varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	varSet /$ins/controls/activity -v 2
	varSet /$ins/heat_status -v 2
	varRead /$ins/heat_status
	if { [varGetVal /$ins/heat_status] > 1 } {
	    varDoSet /$ins/controls/do_ramp -p on -p_int [expr {0.1+[varGetVal /$ins/setup/heat_time]}]
	    varDoSet /$ins/ramp_status -v 6
	}
      }
    9 { # Turning heater off.  Set heater off and verify.
	varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	varSet /$ins/heat_status -v 1
	varRead /$ins/heat_status
	if { [varGetVal /$ins/heat_status] == 1 } {
	    varDoSet /$ins/controls/do_ramp -p on -p_int [expr {0.1+[varGetVal /$ins/setup/heat_time]}]
	    varDoSet /$ins/ramp_status -v 7
	}
      }
    default {
	varDoSet /$ins/controls/do_ramp -p on -p_int 5
      }
  }
}
#
# Procedure mag_set_next_ramp initiates ramping, at an appropriate ramp rate, from the
# present current ("start") aiming for a final destination current ("target").  It may set
# an intermediate destination.  The ramp rates and destinations are taken from the global
# lists MAG($ins,ramps) and MAG($ins,currents).
#
proc mag_set_next_ramp { ins start target } {
    global MAG
#   Don't need every time!    varSet /$ins/controls/fast_leads -v 0
    set i 0
    if { $start > $target } {
	set MAG($ins,direct) "-"
	set MAG($ins,relat) "<"
	while { [lindex $MAG($ins,currents) $i] < $start } {incr i}
	varSet /$ins/controls/ramp_rate -v [ lindex $MAG($ins,ramps) $i ] 
	varSet /$ins/fast_set -v [bounded \
		[lindex [linsert $MAG($ins,currents) 0 -999.] $i] $target $start]
    } else {
	set MAG($ins,direct) "+"
	set MAG($ins,relat) ">"
	while { [lindex $MAG($ins,currents) $i] <= $start } {incr i}
	varSet /$ins/controls/ramp_rate -v [lindex $MAG($ins,ramps) $i]
	varSet /$ins/fast_set -v [bounded [lindex $MAG($ins,currents) $i] $start $target]
    }
    varRead /$ins/controls/ramp_status
}

#
# Procedure mag_start_settle is the code for beginning the settle stage (entered
# at the end of ramping or the end of degaussing).
#
proc mag_start_settle { ins } {
    varDoSet /$ins/ramp_status -v 2
    varDoSet /$ins/controls/do_ramp -p off
    varDoSet /$ins/controls/do_ramp -p on -p_int [varGetVal /$ins/setup/settle_time]
    varRead /$ins/controls/ramp_status
}
#
# mag_end_settle: End of settling; code shared by do_ramp and abort.
proc mag_end_settle { ins } {
    global MAG
    varDoSet /$ins/controls/do_ramp -p off -p_int 0
    if { [varGetVal /$ins/setup/ramp_mode] } {
#	# If persistent mode operation, remember current voltage and go on to turn off switch heater
	varRead /$ins/volts
	set MAG($ins,holdVolt) [varGetVal /$ins/volts]
	mag_remember_Vmax $ins
	varDoSet /$ins/controls/do_ramp -p on -p_int 1.5
	varDoSet /$ins/ramp_status -v 9
    } else {
#	# If regular ramping, we are finished
	varDoSet /$ins/ramp_status -v 0
	varDoSet /$ins/mag_field -p on -p_int 30
    }
}
#
# Remember volt_max, but don't overwrite memory
#
proc mag_remember_Vmax { ins } {
    global MAG
    if { ! [info exists MAG($ins,maxVolt)] } {
	varRead /$ins/setup/volt_max
	set MAG($ins,maxVolt) [varGetVal /$ins/setup/volt_max]
    }
}
#
# Restore the remembered volt_max, if there is one
#
proc mag_restore_Vmax { ins } {
    global MAG
    if { [info exists MAG($ins,maxVolt)] } {
	varSet /$ins/setup/volt_max -v $MAG($ins,maxVolt)
	unset MAG($ins,maxVolt)
    }
}
#
# reduce volt_max temporarily
# (this is not done for the Oxford IPS120 because that doesn't regulate at a voltage limit but stops)
#
proc mag_reduce_Vmax { ins } {
    global MAG
    mag_remember_Vmax $ins
    varSet /$ins/setup/volt_max -v [bounded "abs($MAG($ins,holdVolt))+0.3" 0.2 $MAG($ins,maxVolt)]
}
#
# Procedure mag_ramp_mode_r is the readProc for /~/setup/ramp_mode to assign to ramp_mode
# a value that makes sense for heat_status (persistent if heater is off, non-persistent if
# on or absent).  Procedure mag_ramp_mode_w is the corresponding writeProc.  It actually 
# changes ramp mode, including ramping the leads and turning the heater on/off.
# *** MAKE NO-OP
proc mag_ramp_mode_r { ins } {
    return
#    if { [varGetStatus /$ins/controls/do_ramp] & 8 } {# if do_ramp is polling
#	return 
##NOT:	 -code error "Ramp in progress. ramp_mode remains unchanged"
#    }
#    varRead /$ins/heat_status
#    varDoSet /$ins/setup/ramp_mode -v [expr [varGetVal /$ins/heat_status] == 1 ? 1 : 0 ]
}
proc mag_ramp_mode_w { ins target } {
    global MAG
    varRead /$ins/mag_field
    varRead /$ins/heat_status
    if { [varGetVal /$ins/setup/ramp_mode] != $target } {# change requested
	if { [varGetVal /$ins/ramp_status] == 3 } {
#	    # for persistent, use mag_field for setpoint current
	    set curr [expr ([varGetVal /$ins/mag_field] + 0.0) / ([varGetVal /$ins/setup/calibration])]
	} else {
	    set curr [varGetVal /$ins/mag_set]
	}
	if { $target && [varGetVal /$ins/heat_status ] == 0 } { 
	    return -code error "persistent mode ramping is impossible without a switch heater"
	}
	set MAG($ins,stype) 1
	varDoSet /$ins/setup/ramp_mode -v $target
	mag_start_set $ins $curr
    }
}

# mag_abort_w is the writeproc for the selection variable /~/abort, with selections ...
#
# "Abort" -- Shut down a ramp procedure in an orderly fashion, leaving the mag_set variable at
# 	whatever value we stopped at.  In persistent mode, the heater is switched off and the 
#	leads ramped down.  The exact procedure depends on the current ramp_status:
#
#	0 (holding)	turn off poll of do_ramp
# 	1 (ramping)	pause; set mag_set to mag_read (initiating a new ramp sequence)
# 	2 (settling)	perform same action as do_ramp does after it has finished settling
# 	3 (persistent)	same as 0 (holding)
# 	4 (leads +)	doSet mag_set and settle_set to ramp target; set ramp_status 9 (heat off)
#	5 (leads -)	if mag_read 0, say persistent, else like 2
#	6 (heat sw)	like 4
#	7 (cool)	like 5
#	8 (heat on)	like 4 and 6
#	9 (heat off)	like 2
#      10 (degauss)     like 1
#
# "Pause" -- if a ramp really is in progress, stops polling do_ramp and sets activity to 2 
#	(hold), but saves the previous value with varDoSet.  Beware!  This means that the
#	displayed value of /~/controls/activity does not reflect reality while a ramp is
#	paused.  That's not really so bad because activity can often display the incorrect
#	value (after a fast_set).
#
# "Resume" -- if a ramp really is paused, restores the previous activity and starts
#	polling do_ramp again.
#
# "Never mind" -- do nothing.
#
# Note that starting a new ramp (set_mag) or selecting "abort" will clear the memory of a
# paused ramp.
# 

proc mag_abort_w { ins target } {
    set prev [varGetVal /$ins/abort]
    set rs [varGetVal /$ins/ramp_status]
    set hs [varRead /$ins/heat_status; varGetVal /$ins/heat_status]
    switch $target {
	0 {# ---- Abort ----
	    varDoSet /$ins/degauss/degauss -v 0
	    switch $rs {
		0 -
		3 {
		    varDoSet /$ins/controls/do_ramp -p off
		  }
		10 -
		1 {
		    if { $hs == 1 } {# heater off -- don't turn it on!
			mag_end_settle $ins
		    } else {
			varSet /$ins/controls/activity -v 2
			varRead /$ins/mag_read
			varSet /$ins/mag_set -v [varGetVal /$ins/mag_read]
		    }
		  }
		2 -
		9 {
		    mag_end_settle $ins
		  }
		4 -
		6 -
		8 {
		    set s [varGetVal /$ins/fast_set]
		    varDoSet /$ins/mag_set -v $s
		    varDoSet /$ins/settle_set -v $s
		    if { $hs != 1 } {# heater on or absent -- continue with end-ramp
			mag_end_settle $ins
		    } else {# heater off -- don't turn it on!
			varDoSet /$ins/ramp_status -v 9
			varDoSet /$ins/controls/do_ramp -p on -p_int 2
		    }
		  }
		5 -
		7 {
		    varRead /$ins/mag_read
		    if { abs([varGetVal /$ins/mag_read]) < 0.1 } {
			varDoSet /$ins/ramp_status -v 3
			varDoSet /$ins/controls/do_ramp -p off
		    } else {
			mag_end_settle $ins
		    }
		  }
	    }
	    varDoSet /$ins/abort -v 0
	  }
	1 {# ---- Pause ----
	    varDoSet /$ins/controls/do_ramp -p off
	    if { $rs != 0 && $rs != 3 && $prev != 1 } {
		varRead /$ins/controls/activity
		set a [varGetVal /$ins/controls/activity]
		varSet /$ins/controls/activity -v 2
		varDoSet /$ins/controls/activity -v $a
		varDoSet /$ins/abort -v 1
	    }
	  }
	2 {# ---- Resume ----
	    if { $prev == 1 } {
		varSet /$ins/controls/activity -v [varGetVal /$ins/controls/activity]
		varDoSet /$ins/controls/do_ramp -p on
		varDoSet /$ins/abort -v 0
	    }
	  }
	default {# ---- "Never mind"
	  }
    }
    varDoSet /$ins/mag_field -p on -p_int 30
}


# Procedures for degaussing the magnet by oscillating the field.

# The shortcut degauss_field setting:
#
proc mag_degauss_set_field_w { ins target } {
    varSet /$ins/degauss/set_field -v $target
    varSet /$ins/degauss/degauss -v 1
}

# The degaussing field offset parameter: 
#
proc mag_deg_ampl_w { ins target } {
    varDoSet /$ins/degauss/amplitude -v $target
}

# The degaussing field percentage-decremant parameter: 
#
proc mag_deg_decrement_w { ins target } {
    if { $target < 1.0 || $target > 50. } {
	return -code error "Value out of range 1-50"
    }
    varDoSet /$ins/degauss/decrement -v $target
}
 
# The degaussing field setpoint parameter: 
#
proc mag_deg_setf_w { ins target } {
    varDoSet /$ins/degauss/set_field -v $target
}

# Degaussing writeproc, to start (or stop) degaussing:
#
proc mag_degauss_w { ins target } {
    global MAG
    varDoSet /$ins/degauss/degauss -v $target
    if { $target } {
#	# Start degaussing procedure:
	varDoSet /$ins/degauss_field -v [varGetVal /$ins/degauss/set_field]
	varSet /$ins/mag_field -v [varGetVal /$ins/degauss/set_field]
    } elseif { [varGetVal /$ins/ramp_status] == 10 } {
#	# degaussing now, so don't wait to finish
	set ms [varGetVal /$ins/mag_set]
	varRead /$ins/mag_read
	set mr [varGetVal /$ins/mag_read]
	varRead /$ins/heat_status
	set off [expr {[varGetVal /$ins/degauss/amplitude]/([varGetVal /$ins/setup/calibration]+.001)}]
	if { [varGetVal /$ins/heat_status] == 1 } {
#	    # Heater is off but present => we are messed up => declare finished & persistent
	    varDoSet /$ins/controls/do_ramp -p off
	    varDoSet /$ins/ramp_status -v 3
	} elseif { abs($ms-$mr) <= abs($off) + .001 } {
#	    # within normal degauss range of setpoint
	    mag_degauss_step $ins [varGetVal /$ins/mag_set]
	} else {
#	    # out or range from setpoint => weird => re-initialize set
	    varSet /$ins/mag_set -v $ms
	}
    }
}
#
#   This proc, mag_degauss_init,  starts the degaussing stage of the ramp.  It is in a proc
#   just to unclutter the main do_ramp procedure.
#
proc mag_degauss_init { ins target } {
    global MAG
    set cal [varGetVal /$ins/setup/calibration]
    set fo [varGetVal /$ins/degauss/amplitude]
#   default amplitude (if entered as 0) is 0.25% of the field change, plus 20 G:
    if { $fo < 0.001 } {
	set fo [expr { abs($target-$MAG($ins,startI)) * 0.0025 * $cal + 0.002 } ]
    }
#   direction of initial field offset is chosen to overshoot any change of setpoint.
    set fo [expr "0.0 $MAG($ins,direct) $fo"]
    set MAG($ins,degOin) 0.0
    set MAG($ins,degOff) [expr {$fo/$cal}]
    varDoSet /$ins/ramp_status -v 10
    varDoSet /$ins/controls/do_ramp -p off
    varDoSet /$ins/controls/do_ramp -p on -p_int 4
}
#
#   Perform a degaussing reversal of $MAG($ins,degOff) centered on $target.
#   Reverse and reduce the offset by the decrement: 
#	percent_decrement/100 * abs_average_of(current_offset, initial_offset)
#   $target is the final target setpoint (could use [varGetVal /$ins/mag_set] instead)
proc mag_degauss_step { ins target } {
    global MAG
    if { [varGetVal /$ins/degauss/degauss] == 0 } {
#	# Go straight to setpoint if degaussing cancelled
	set new 0.0
    } elseif { $MAG($ins,degOin) == 0.0 } {
#	# initial field offset
	set MAG($ins,degOin) $MAG($ins,degOff)
    } else {
#	# Reverse and reduce the offset
	set pct [varGetVal /$ins/degauss/decrement]
#	set sig [expr { $MAG($ins,degOff) < 0.0 ? 1.0 : -1.0 }]
#	set dec [expr { ($pct/200.0) * (abs($MAG($ins,degOff)) + abs($MAG($ins,degOin))) }]
#	set new [expr { $sig * ( abs($MAG($ins,degOff)) - $dec ) }]
	set new [expr { ($MAG($ins,degOff) < 0.0 ? 1.0 : -1.0) * ( abs($MAG($ins,degOff)) - ($pct/200.0) * (abs($MAG($ins,degOff)) + abs($MAG($ins,degOin))) ) }]
	if { $new * $MAG($ins,degOff) >= 0. } {
#	    # last step to setpoint
	    set new 0.0
	}
	set MAG($ins,degOff) $new
    }
#   # Get next intermediate setpoint based on degOff
    set sp [expr {$target + $MAG($ins,degOff)}]
    set sp [bounded $sp [varGetVal /$ins/setup/mag_min] [varGetVal /$ins/setup/mag_max] ] 
    varSet /$ins/fast_set -v $sp
    varDoSet /$ins/degauss_field -m "Go to [format %.4f $target][format %+.4f $MAG($ins,degOff)] = [format %.4f $sp]"
#   # Check if we've zeroed in
    if { $MAG($ins,degOff) == 0.0 } {
#	# Yes, go to next stage: settle
	varDoSet /$ins/degauss/degauss -v 0
	varDoSet /$ins/degauss_field -m ""
	mag_start_settle $ins
    }
}

# Some read and write procs that are not magnet-specific:

proc mag_heat_time_w { ins target } {
    varDoSet /$ins/setup/heat_time -v $target
}
proc mag_settle_time_w { ins target } {
    varDoSet /$ins/setup/settle_time -v [bounded $target 1. 999.]
}
proc mag_pers_leads_w { ins target } {
    varDoSet /$ins/setup/pers_leads -v $target
    if { $target == 0 && [varGetVal /$ins/ramp_status] == 3 } {
#	# Now ramp leads down, fast.
	varDoSet /$ins/ramp_status -v 5
	varSet /$ins/controls/fast_leads -v 1
	varDoSet /$ins/controls/do_ramp -p on -p_int 2
	varSet /$ins/controls/activity -v 1
    }
}
proc mag_refresh_r { ins } {
    if { [varGetVal /$ins/ramp_status ] == 3 } {
	varRead /$ins/controls/i_persist
	varSet /$ins/mag_set -v [varGetVal /$ins/controls/i_persist ]
    }
}
proc mag_refresh_w { ins target } {
    mag_refresh_r $ins
}


# Utility function used throughout:
# Change to quotes if need parameters to be sub-expressions like "abs(123)"
proc bounded { x lo hi } {
    expr "($x)<($lo)? ($lo) : (($x)>($hi)? ($hi) : ($x))"
}

