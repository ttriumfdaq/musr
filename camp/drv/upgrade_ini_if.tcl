#! /usr/bin/tclsh

proc modmod { file } {
    set mtime [file mtime ${file}~]
    file mtime $file $mtime
} 

proc upin { file } {
    if { ![file readable $file] } {
	puts "Can't read $file"
	return
    }
    if { [file exists ${file}~] } {
	puts "${file}~ exists -- set modtime only"
	modmod $file
	return
    }
    set fh [open $file r]
    set content [read $fh]
    close $fh

    if { ![regexp {insSet /~ -if (\w+) (.+) -o[nf]+line} $content insset iftype pars] } {
	puts "Found no insSet line"
	return
    }
    switch -- $iftype {
	rs232 {
	    if { [scan $pars {%f %s %d %d %s %d %s %s %f %c} del port speed bits par sb rt wt to xx] != 9 } {
		puts "$file: Mismatched $iftype settings"
		return
	    }
	    set pars "$del $to $port $speed $bits $par $sb $rt $wt"
	}
	tcpip {
	    if { [scan $pars {%f %s %d %s %s %f %c} del addr port rt wt to xx] != 6 } {
		puts "$file: Mismatched $iftype settings"
		return
	    }
	    set pars "$del $to $addr $port $rt $wt"
	}
	gpib {
	    if { [scan $pars {%f %d %s %s %c} del port rt wt xx] != 4 } {
		puts "$file: Mismatched $iftype settings"
		return
	    }
	    set pars "$del 3.00 $port $rt $wt"
	}
	vme {
	    if { [scan $pars {%f %d %c} del addr xx] != 2 } {
		puts "$file: Mismatched $iftype settings"
		return
	    }
	    set pars "$del 1.00 $addr"
	}
	none {
	    if { [scan $pars {%f %c} del xx] != 1 } {
		puts "$file: Mismatched $iftype settings"
		return
	    }
	    set pars "$del 1.00"
	}
	camac {
	    if { [scan $pars {%f %d %d %d %c} del b c i xx] != 4 } {
		puts "$file: Mismatched $iftype settings"
		return
	    }
	    set pars "$del 1.00 $b $c $i"
	}
    }

    if { [regsub {insSet /~ -if \w+ .+ -o[nf]+line} $content "insSet /~ -if $iftype $pars -offline" new] } {
	puts "$file: insSet /~ -if $iftype $pars -offline"
	file rename $file ${file}~
	set fh [open $file w]
	puts -nonewline $fh $new
	close $fh
	modmod $file
    }
}

foreach file $argv {
    upin $file
}
