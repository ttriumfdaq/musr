 

1  User_Manual
   This document describes how to use the online user interface to
   the CAMP system, called "camp_cui". This manual is intended for
   people who will be using CAMP for the control and monitoring 
   of peripheral instrumentation.
 

2  Introduction
   The CAMP (Control And Monitor Peripherals) system may be
   accessed by a user interface run in any standard terminal window.
   It uses RPCs (Remote Procedure Calls) for communication with
   the CAMP server running as a separate process on a remote (or
   the same) computer. More than one user may run the user interface
   simultaneously, and several different types of "client" program can
   access the server. The CAMP character-based user interface (CUI)
   is an interactive, menu-oriented interface that runs in a terminal
   window, and which provides an updating data display suitable for 
   the novice and advanced user. 
   

3  Instruments_and_Variables
   Two key concepts important for working with CAMP are the Instrument
   and the Variable. Each CAMP instrument typically corresponds to a
   physical device used in the experiment, but some may have no real
   device and others have more than one.  Each instrument is a collection
   of variables with a common communication interface. The different 
   types of variables are:

   Instrument:  the top-level variable for every instrument

   Structure:   an aggregate of variables (all types except instrument
                are allowed)

   Numeric:     integers and floating-point numbers

   String:      character string

   Selection:   a selection between an arbitrary number of text labels

   CAMP variables are arranged and accessed in a manner much like
   files in a directory tree. In the root CAMP directory (Path: /)
   are the instrument variables. These must have unique identifiers
   to distinguish from other instrument variables. Each level in the
   tree is delimited by the character /. (Thus, when written, the
   root of the tree is on the left, and the leaves on the right.
   Likewise, in the CAMP Interface, the left arrow key moves towards
   the root of the tree, and the right arrow key moves to the "leaf"
   variables.)

   Below (to the right of) each Instrument variable (i.e., Path: 
   /<instrument name>/) are the variables relating to that instrument.
   The Structure variable type is used to group variables within an 
   instrument. The names of the variables within an instrument are the
   same for all instances of that instrument type. For example, any
   LakeShore 330 temperature controller instruments have the variable 
   "sample_read". Two instances may have paths "/Diffuser/sample_read"
   and "/shield/sample_read", with the only difference being the first
   component - the name of the Instrument.  As this example suggests,
   instrument names should be chosen to be physically meaningful as 
   they are important for identifying all their subsidiary variables.
   

4  Instrument_Variable_Parameters
   Instrument variables are the only ones that appear at the root path.
   They are like a Structure variable plus an interface definition.
   Their parameters specify that communication interface.
   

5  Online/Offline
   An Instrument may be set online or offline in software. When an
   Instrument is offline, no variable setting or reading requests
   are permitted. To set an Instrument online, it must have a defined
   interface (see Interface). 

   When the request is made to set an Instrument online, initial
   communication is attempted and the validity of the interface 
   definition is tested (for the particular instrument type). 

   Once online, setting and reading requests may be fulfilled and the
   polling of polled variables is begun (see Polling).
   

5  Interface
   The interface of each Instrument must be defined in software
   before access to the physical device is possible. The interface
   definition has been made variable for all Instruments to allow for
   flexibility of the physical hardware. 

   Defining an interface entails specifying the interface type, followed
   by the access delay time, followed by a number of other parameters
   specific to that interface type. 

   The access delay time is a floating-point number in seconds that is
   the minimum time between consecutive accesses of the physical 
   instrument. The CAMP Server ensures that at least this time has passed 
   between communications. The parameters of the currently implemented
   interface types are

   Type        Description    Parameters

   rs232       RS-232-C       port name (string)
                              baud (various numbers)
                              data bits (integer)
                              parity ([none|odd|even])
                              stop bits (integer)
                              read terminator ([none|LF|CR|CRLF])
                              write terminator ([none|LF|CR|CRLF])

   gpib        IEEE488        address (integer)
                              read terminator ([none|LF|CR|CRLF])
                              write terminator ([none|LF|CR|CRLF])

   tcpip       TCP/IP         IP address (string nnn.nnn.nnn.nnn)
                              port number (integer)
                              read terminator ([none|LF|CR|CRLF])
                              write terminator ([none|LF|CR|CRLF])

   camac       CAMAC          branch (integer)
                              crate  (integer)
                              niche (slot) (integer)
 
   vme         VME            base address (integer)

   none                       no parameters (for virtual devices, or 
                              those with other communication interfaces)
   

5  Lock
   Instruments may be locked for exclusive control by one process.
   This prevents all other processes from changing any of the
   settings of the instruments. It does not, however, prevent other
   processes from viewing the instrument.

   When a request is made to lock an instrument, the Server checks to
   make sure that it is not already locked. If it is locked, the
   Server checks that the locking process still exists. A non-
   existent process is no longer considered a locker. At present,
   the VMS version of CAMP restricts lockers to be processes on
   the same node as the Server. The VxWorks version does not impose
   this restriction; a best attempt is made to determine whether the
   process on the remote exists.
   

4  Variable_Properties_and_Actions
   The parameters related to ordinary variables are displayed on
   the right-hand panel and also on the full-screen "View Parameters" 
   display. 
   

5  Polling
   Variable polling allows users to specify that the value of a
   Variable is to be read from the physical device at a regular
   interval. The interval is defined by a floating-point number in
   seconds. Variables which can be polled have the poll attribute.
   If a variable is not being polled, Camp will not show changes
   in the relevant parameter until an explicit Read request is given.

                                  NOTE

      It is advised that polling be set conservatively. If too
      many variables are being polled too frequently, some more
      time-critical operations could be compromised.
   
 
5  Tolerance
   For numeric variables only, particularly those that can be read
   from the instrument, and have a corresponding variable used as
   the set-point. Out-of-Tolerance state occurs when the reading
   differs from the setting by more than the requested range.
   There are two parameters to enter:

   o  tolerance method, whether the tolerance value is a plus/minus
      offset or a percentage,

   o  tolerance value, (floating-point).
   

5  Alarm
   Variable alarms allow users to specify that an action should be
   taken when a Variable's read-back value does not equal a corres-
   ponding setpoint. Usually this means a numeric variable going out 
   of tolerance, but alarms can also occur for other types of 
   variables, even those without a comparison setpoint (e.g., a
   string variable that reports error messages). Enabling an
   alarm requires one parameter:

   o  action, one of the available alarm actions. The alarm actions
      that are currently implemented are:

      -  just_beep: no action beyond setting the alarm on

      -  GGL_Alarm: set a hardware logic level used to suppress data
         acquisition

      -  Ramp_Down: set any superconducting magnet to zero
   

5  Logging
   Variable logging allows users to specify that a Variable is to be
   logged in a particular manner. Variable logging has one related
   parameter:

   o  action, one of the available log actions (string). Log
      actions are case-sensitive. The log actions that are currently
      implemented are:

      -  log_mdarc: log in the MUD data file (produced by mdarc) 

      -  log_mdarc_td, log statistics in TD mode MUD files

      -  log_mdarc_ti, log values and statistics in Integral mode

   Variables which can be logged have the log attribute in the "View
   parameters" display.
   

5  Zeroing
   When a Numeric Variable is zeroed, all of the statistics are
   set to zero. If an Variable of aggregate type is zeroed (i.e.
   Instrument or Structure), all of the Numeric Variables in the tree
   below the Variable are zeroed (i.e., recursively).
   

3  Configuration_and_Initialization
   The user may save and load the CAMP configuration or the
   parameters for an individual instrument. A CAMP configuration
   file is a record of instruments and the appropriate initialization
   file to load for each instrument. An initialization file contains
   a record of all of the dynamic parameters for each variable of a
   particular instrument. These files are both editable ASCII text.
   See the document CAMP Programmer's Guide for a description of the
   command format of these files.
   

4  System_Configuration
   System configuration files come in two varieties.

   o  Those saved automatically (named camp.cfg) contain commands
      to create each instrument with their unique names, and then
      to input each individual instrument initialization file.

   o  Those saved deliberately under a different name contain
      all the system and instrument configuration in a single file.
      This ensures that a complete system snapshot can be retained.
   

4  Instrument_Initialization
   Instrument initialization files contain the following information:

   o  The current value of the generic dynamic parameters of all
      variables, including: Polling parameters; Alarm parameters;
      Logging parameters; Titles. (Although messages are dynamic,
      they are NOT saved.)

   o  The current value of the datatype-specific dynamic parameters
      of all Variables, including:

      -  The value for variables of Numeric, String, Selection and 
         Link types that can be Set (have settable attribute).

      -  Units, for Numeric variables that have them.

      -  Tolerance method and value for Numeric types.

      -  Interface definition for Instrument variables
   

4  Other_Initialization
   While most initialization files save the entire state of a 
   particular instrument, other initialization files may be
   hand-crafted to initialize a small group of variables, whatever
   instrument they belong to.
   

2  User_Interface_CUI
   This section describes how to use the online CAMP User Interface.
 

3  Startup
   Start the CAMP User Interface with the command:

    camp [-node <nodename>] [-noansi] [-update <sec>] [-pause <sec>]

   The program is actually named camp_cui, so try that if "camp" does
   not work.  Or click on an icon showing a tent and a camp fire.

   The optional parameters are:

   o  -node <nodename>, giving the internet nodename of the CAMP Server.
      The default is the current computer, but usually the preferred node
      is given by the CAMP_HOST environment variable (or logical name).
      
   o  -noansi, to be used when the terminal (emulator window) does not
      support ANSII line-drawing characters.

   o  -update <sec>, to specify the refresh period (in seconds) of the
      CAMP display. The default is 10. NOTE: Do not set this too low! 

   o  -pause <sec>, to control how long transient messages are displayed
      before returning to the normal display.


3  Main_menu
   You can bring up the main menu at any time with the Tab key. The
   main menu consists of four items:

   o  Configure: add/delete/re-create instruments, open/save 
      configuration files.

   o  Options: Set some user preferences.

   o  Help: get help on the user interface and on CAMP in general

   o  Exit CAMP: exit the user interface

   The current selected menu item is always highlighted in reverse
   text. The arrow keys are used to traverse the menu. Menu items
   with a single character highlighted in boldface may be selected by
   pressing that character.  Thus, TAB-e is a good way to exit from 
   the CAMP Interface.

   The Ctrl-d key or the Tab key are used to cancel any interactive
   menu selection.
 

3  Configure_menu
   The Configure menu, selected from the Main menu, allows for 
   adding and removing instruments, as well as saving and loading 
   CAMP configuration files.
  

4  Add_instrument
   Add a new instrument to the CAMP database.

   You will be prompted to choose an instrument type from a list of all
   available types. Navigate through the names using the cursor arrow 
   keys: up, down, left, right. The selection will "wrap around". Typing
   characters causes the selection to jump to one that starts with that
   character. Press Enter to make the selection.

   Enter a unique name for the instrument that will identify it well.

   Then select its interface parameters.

   CAMP will try to put the instrument Online immediately, and report
   any problems.
     
   
4  Delete_instrument
   Select which instrument to delete, and it will be removed from
   CAMP.  If the instrument is Locked, removal will fail.
 
   
4  Recreate_instrument
   Re-add and restore an instrument that had previously been defined
   in CAMP, but has since been deleted.  It doesn't matter how long
   ago the instrument was previously used, as long as the saved
   instrument initialization file is still present. 
 
   
4  Open_config_file
   This command instructs the CAMP server to remove all of the
   current instruments and restore a previously saved configuration.

   Choose from a list where each entry corresponds to a .cfg file 
   in the CAMP cfg directory.
   
   If any existing instruments are locked by another process, 
   they cannot be deleted and the operation will fail.
 

4  Import_config_file
   This command instructs the CAMP server to load a stored 
   configuration without removing any of the current instruments.

   Choose from a list where each entry corresponds to a .cfg file 
   in the CAMP cfg directory.
 

4  Save_config
   This command instructs the CAMP server to save the current
   configuration of instruments to a file.

   Enter a string that describes the configuration - It will be used
   as part of the file name. DO NOT include a file extension.
   Normally, the entire system state is saved in the file named like
   camp_<yourchoice>.cfg.

   If you enter a null string, the configuration is saved into several
   files:  A separate file for each instrument, plus a small file 
   named "camp.cfg", which declares the instruments and loads them.
   This style of saving is done every few minutes automatically, so
   don't rely on it to retain a particular snapshot of the configuration.
 

3  Options_menu
   The Options menu allows various settings to suit the user's 
   preferences. The available settings are:

   o  Enable/Disable Beeps (audible alerts). Note that sound might
      be suppressed for other reasons, such as a terminal set to 
      use a "visual bell".

   o  Set Update Interval.  That is the number of seconds between 
      display refreshes.  Enter a number from 2 to 60. (Also see
      the "-update" command-line option.)

   o  Set Message Pause Time. This is how long transient information
      messages are displayed on screen, in seconds.  Enter a value
      between 0.0 and 15.0. Note that ERROR messages do not disappear
      until acknowledged with a keystroke. (Also see the "-pause"
      command-line option.)
      

3  Help_menu
   The Help menu, selected from the Main menu, presents options
   for help.


4  About
   Display version information.
 

4  CUI_User_Manual
   Display interactive help on the CAMP CUI program, which also 
   includes some CAMP concepts.  That is where we are now!
 

4  CAMP_Software
   Display the CAMP Software manual, appropriate for programmers
   (or anyone) who want to write new instrument drivers or client
   programs.  THIS MANUAL IS OUT OF DATE AND NEEDS REVISION.
 

3  Variable_menu
   This is NOT a component of the main menu.

   Pressing the Enter (Return) key brings up the menu of actions 
   specific to the highlighted Variable.  Many of these menu actions
   can be executed by pressing a "hot" key, whether or not the menu
   has been activated.  Available hot keys are indicated by bold
   letters.

   The items in the menu (and the available hot keys) depend upon 
   the type of variable selected. 


4  Instrument_Actions
   Action      Description

   Choose      Select that instrument for display. Normally, one
               navigates using the right-arrow key for this, rather
               than the menu.

   Interface   Set the interface definition of the instrument (see
   (i)         Interface). Choose the interface type. You will then
               be prompted for the parameters relating to that type.

   Online      Set the instrument communication status online or 
    /Offline   offline. The available action is whatever is opposite
   (o)         to the current status. When going online, some (inst-
               rument-specific) test of communication is usually 
               performed first. 

   Zero        Zero the statistics of all variables in the instrument
   (z)         (see Zeroing under Instruments_and_Variables).

   Load        Load an instrument initialization from a file (see 
   (l)         Configuration and Initialization Files). Most ini
               files contain the saved state for an instrument, and 
               bear the name of the instrument at the time it was 
               saved. However, some other initialization files may 
               be available to set up just a few of an instrument's
               variables, such as the "zone" table for PID control
               parameters.

   Save        Save the dynamic information for the instrument
   (s)         to an initialization file (see Configuration and
               Initialization Files). This includes the interface
               definition.

               Enter a string that describes the file. DO NOT include
               a file extension as the initialization is saved in a 
               file names <your_string>.ini. If you enter a null string,
               the instrument name is used.

   Lock/Unlock Lock an instrument for exclusive control (see Lock),
   (k)         or unlock an instrument that was locked by the current
               process. If an existing process, other than the
               current process, has already locked the instrument,
               this command will not succeed. 

   View        View all the parameters for the instrument variable.


4  Structure_actions
   Choose      Select that structure for display. Normally, one
               navigates using the right-arrow key for this, rather
               than the menu.

   Zero        Zero the statistics of all variables recursively 
   (z)         within the structure (see the topic Zeroing under 
               Instruments_and_Variables).

   View        View all the parameters for the structure variable.


4  Other_Variable_actions
   Action      Description

   Set         Issue a request to set the target value of a variable,
   (s)         typically involving communication with the physical
               instrument.
               The user will be prompted for a value corresponding to
               the data type of the variable (i.e. integer, floating-
               point, character string, selection).

   Read        Issue a request to read a value back from the physical
   (r)         device and update the corresponding CAMP variable.
               (Pressing the space bar reads all readable variables 
               shown in the current display.)

   Polling     Set the polling status and polling interval of a
   ON/OFF      variable (see Polling under Instruments_and_Variables).
   (p)         If setting the polling status to ON, enter the polling
               interval as a floating-point number in seconds.

   Alarm       Set the alarm status and alarm action of a variable
   ON/OFF      (see Alarm).
   (a)         If setting the alarm status to ON, choose from a list
               of alarm actions.

   Logging     Set the logging of a variable (see Logging in section
   ON/OFF      Instruments_and_Variables). If setting the logging status
   (l)         to ON, choose from a list of logging actions.

   Tolerance   Set the tolerance for a Numeric variable. This value
   (t)         is used to determine when an alarm-enabled variable
               has gone out of tolerance (see Alarm).
               Choose either the Plus/minus (i.e., range) or
               Percent method of tolerance checking. Then enter
               the corresponding value as a positive real number.
               For the Plus/minus method, this number is half of the
               allowable range.

   Zero        Zero the statistics of a variable (see Zeroing).

   New title   The user is prompted to enter a new Title string for
               this variable.

   Units       Units for Numeric variables should normally be left
               as they were set by the instrument definition, but the
               user has the option to change  them.

   Set Link    Set the path of a Link variable.
   (k)         Enter the full associated path as a string.
 
   View        View all the parameters for the variable. The display
   (v)         takes over the entire CAMP CUI window. Press Enter or
               Return to leave the view.


3  Variable_List_Panel
   The CUI Variable List Window is visible at all times except when
   the larger "View Parameters" display is in use. It covers the left
   side of the display. All of the Variables of the current view path 
   are shown, and one of them will be highlighted. The highlighted 
   variable has its details displayed in the right-hand "Variable Info
   Panel". 
   
   Use the Up and Down arrow keys to highlight different variables. 
   (The selection will wrap around from bottom to top and vice versa.)
   Use the right arrow key to move focus into a specific Instrument or
   Structure, selecting a different Current Path with its own Variable
   List. (Instruments and Structures are displayed with "->" after
   their names to indicate this possibility. Use the left arrow key to
   move back towards the root path.

   Sorry, but mouse clicks do nothing.


3  Variable_Info_Panel
   The CUI Variable Info Panel is visible on the right of the display.
   In it, information about the currently-highlighted variable is
   shown. These parameters include:

   o  Path (as the heading), the full CAMP Path for the Variable

   o  Title, a Title for the Variable (need not be unique).

   o  Type, the data type

   o  Value (and Units, if appropriate)

   o  Actions Set and/or Read 

   o  State of Polling, Logging, Tolerance, Alarm.  For Instrument 
      Variables, information on the state of the Interface is shown.

   o  Message from the instrument driver which might be used to alert 
      the user about something, or give the user instructions or other     
      information.  

   o  Help, with a brief explanation of the variable.

   To see all of the parameters of a variable in more detail, bring
   up the "View Parameters" display by pressing "v" or by bringing
   up the variable's specific menu (press Enter) and selecting 
   "View Parameters".
 

3  Other_Panels

   The Status Panel is the top line of the display, and shows the 
   CAMP version number and the server nodename.

   The Key window is a two-line panel that is normally displayed at
   the bottom.  It gives a guide to some important keyboard actions;
   a key to the keys, as it were.

   When alarms are active, the Key window is replaced by an Alarms
   panel, listing the full CAMP paths for all variables that have
   triggered alarms.


