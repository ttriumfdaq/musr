/* 
 *	$Id: camp.h,v 1.20 2018/06/13 00:45:21 asnd Exp $
 *
 *	$Revision: 1.20 $
 *
 *
 *  Purpose:     This file handles all definitions for the system
 *
 *  Modification history:
 *    13-Dec-1999  TW  Casting for xdr_free (gcc warnings)
 *    14-Dec-1999  TW  Prototype of camp_varGetPHead added
 *                     Fixed double define of TRUE,FALSE
 *    16-Dec-1999  TW  Define CAMP_RPC_CLNT_TIMEOUT here
 *                     Use CAMP_DEBUG to conditionally compile all
 *                     camp_debug conditionals
 *    20-Jan-2000  SD  Add nodename to writeable /camp directories to
 *                     allow several servers (MVME) to be accessed by one host
 *                     (VAX or linux presently) without overwriting log,ini files
 *    20140218     TW  notes on status codes
 *    20140220     TW  camp_if_getTerm, my_strncpy
 *
 *  $Log: camp.h,v $
 *  Revision 1.20  2018/06/13 00:45:21  asnd
 *  introduce camp_stoprint_strdup
 *
 *  Revision 1.19  2017/06/21 03:23:13  asnd
 *  Longer RPC timeout when loading instruments
 *
 *  Revision 1.18  2015/12/10 03:50:02  asnd
 *  Improved some standard error messages
 *
 *  Revision 1.17  2015/04/21 04:32:03  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.16  2015/03/17 00:18:43  suz
 *  This file part of Ted's 201404 new interface package (Rev 1.15 comment in error)
 *
 *  Revision 1.15  2015/03/16 22:30:43  suz
 *  This file part of Ted's 201404 new interface package (Rev 1.14 comment in error)
 *
 *  Revision 1.14  2015/03/16 21:03:32  suz
 *  file moved to retired_vms directory
 *
 *  Revision 1.13  2009/04/04 00:00:35  asnd
 *  Remove and adjust priority handling that caused gpib lockups
 *
 *  Revision 1.12  2008/03/27 01:08:51  asnd
 *  Reduce the long "var" timeout, so system does not lock up for so long.
 *
 *  Revision 1.11  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.10  2004/01/28 03:50:32  asnd
 *  Add VME interface type.
 *
 *  Revision 1.9  2002/07/24 00:34:24  suz
 *  Donald adds if CAMP_MULTITHREADED to conditionally skip the thread definition that prevented mdarc from compiling
 *
 *  Revision 1.8  2002/05/08 00:47:43  suz
 *  Add support for ppc (casting & ifdef VXWORKS)
 *
 *  Revision 1.6  2001/02/10 04:16:15  asnd
 *  Add insIfDump and insIfUndump for binary-clean data dump from instrument to
 *  a file and from a file to the instrument.
 *
 *  Revision 1.5  2001/01/15 06:41:28  asnd
 *  Error message for rs232PortInUse.
 *
 *  Revision 1.4  2000/12/23 02:37:50  ted
 *  camp_getIfIndpakChan -> camp_getIfIndpakChannel for consistency (different in some places than others)
 *
 *  Revision 1.3  2000/12/22 22:34:57  David.Morris
 *  Added INDPAK parameter access functions and prototypes. Added CAMP_IF_TYPE_INDPAK
 *  to instrument type list
 *
 *
 */

#ifndef _CAMP_H_
#define _CAMP_H_

#ifndef CAMP_MULTITHREADED
#define CAMP_MULTITHREADED 0
#endif

// #ifndef CAMP_TIRPC 
// #define CAMP_TIRPC 0
// #endif

/*
 *  Need to know this here for the CAMP_INSTRUMENT struct.
 *  Add any other POSIX thread compliant OS here.
 *  _POSIX_THREADS is redefined in pthread.h (in camp_srv.h).
 *  I hope the value doesn't change (or matter) for other
 *  implementations.
 *
 *  20140315  TW  now that all objects compiled with CAMP_MULTITHREADED
 *                can selectively include camp_srv.h (rather than just
 *                camp.h), we don't need to allow for CAMP_MULTITHREADED
 *                workaround in camp.h.
 */
/* #if CAMP_MULTITHREADED */
/* #if defined( VMS ) || defined( OSF1 ) || defined ( ULTRIX ) */
/* #define _POSIX_THREADS 1 */
/* #elif defined( VXWORKS ) */
/* #include "vx_pthread.h" */
/* #endif */
/* #endif /\* CAMP_MULTITHREADED *\/ */


/* 
 *  Support VxWorks
 *  Made conditional with definition VXWORKS
 */
#ifdef VXWORKS
#include <vxWorks.h>
#include <sockLib.h>
#include <selectLib.h>
#include <hostLib.h>
#include <in.h>
#include <ioLib.h>
#include <netinet/tcp.h>
#define NO_FORTRAN 1
#endif /* VXWORKS */

#ifdef linux
#define NO_FORTRAN 1
#endif /* linux */


/*
 *  RPC includes
 */
#if defined( VMS ) && defined( UCX )

#include <ucx$rpcxdr.h>

#else // !(VMS && UCX)

/* UNIX, VxWorks, VMS using MultiNet, etc. */
#undef TRUE
#undef FALSE

// #if ( CAMP_TIRPC != 0 )
// #include <tirpc/rpc/rpc.h>
// #else // !CAMP_TIRPC
#include <rpc/rpc.h>
// #endif // CAMP_TIRPC

#if !defined(TRUE)
#define TRUE 1
#endif // !TRUE

#if !defined(FALSE)
#define FALSE 0
#endif // !FALSE

#ifndef VXWORKS
#include <sys/time.h>
#endif // VXWORKS

#endif // VMS && UCX


#ifdef __linux__
#include <stdint.h> // uintptr_t
#else
#ifdef VXWORKS
typedef unsigned long int uintptr_t;
#endif // VXWORKS
#endif // __linux__

/*
 *  timeval_t and related routines
 */
#include "timeval.h"

/*
 *  Tcl - need it here to define Tcl_Interp used in 
 *        CAMP_INSTRUMENT struct
 *  20140310  TW  forward declaration instead of include
 */
// #include "tcl.h"
struct Tcl_Interp;

#ifdef VMS
#include "vaxc_utils.h"

/*
 * camp_if_rs232_tt_defs.h
 */
#include ttdef
#include tt2def
#include dcdef

#define  LEN_RS232_TT_PORT	63

#define  NO_PARITY		TT$M_ALTRPAR
#define  EVEN_PARITY		TT$M_ALTRPAR | TT$M_PARITY
#define  ODD_PARITY		TT$M_ALTRPAR | TT$M_PARITY | TT$M_ODD

#define  TERM_LF	1
#define  TERM_CR	2
#define  TERM_CRLF	3
#define  TERM_NONE	4

#define  BAUD_110	1
#define  BAUD_300	2
#define  BAUD_600	3
#define  BAUD_1200	4
#define  BAUD_2400	5
#define  BAUD_4800	6
#define  BAUD_9600	7
#define  BAUD_19200	8

#define  PARITY_NONE    1
#define  PARITY_ODD	2
#define  PARITY_EVEN    3

struct device_mode {
    u_char   class;
    u_char   type;
    short  page_width;
    union {
      struct {
	long  basic_chars;
      } s1;
      struct {
	u_char  dummy[3];
	u_char  page_len;
      } s2;
    } u1;
    long ext_chars;
};

#else /* !VMS */

#include "c_utils.h"

#define  SUCCESS_MASK           	0x00000001L
#define  _success( s )                  ( s & SUCCESS_MASK )
#define  _failure( s )                  !( s & SUCCESS_MASK )

#endif /* VMS */


/*
 *  Taken from camp_cui_main.c 16-Dec-1999
 *
 *  Changed method 19-Dec-1999 TW
 *  All timeouts now RPC timeouts
 *  This timeout overrides the RPC default of 25 seconds
 *
 *  14-Dec-2000  TW  Change RPC timeout to 30 seconds for some
 *                   RPC calls (instruments might have very long
 *                   read operations)
 *                   Decrease RPC timeout to 10 s for other calls
 */
#define CAMP_RPC_CLNT_TIMEOUT 10
#define CAMP_RPC_CLNT_VAR_TIMEOUT 15
#define CAMP_RPC_CLNT_LOAD_TIMEOUT 20


/* 
 *  Message definitions
 */
typedef long MSG_TYPE;

/*
 *  Message masks/status bits (VMS Message Utility format)
 *
 *  For historical reasons, CAMP status codes follow the VMS convention:
 *
 *    bits  0- 2: severity
 *    bits  3-27: condition identification
 *          3-15: message number
 *         16-27: facility number
 *    bits 28-31: control bits
 */
#define  MSG_SUCC_MASK		0x00000001L
#define  MSG_SEVER_MASK		0x00000007L

#define  MSG_SUCCESS		0x00000001L
#define  MSG_INFORMATIONAL	0x00000003L
#define  MSG_WARNING		0x00000000L
#define  MSG_ERROR		0x00000002L
#define  MSG_SEVERE		0x00000004L

/* success */
#define CAMP_SUCCESS                   ((1<<3)|MSG_SUCCESS) /* <success> */
/* informational */
#define CAMP_JOB_EXISTS                ((2<<3)|MSG_INFORMATIONAL) /* <the job PID was found to exist> */
#define CAMP_JOB_NONEXISTANT           ((3<<3)|MSG_INFORMATIONAL) /* <the job PID was not found to exist> */
#define CAMP_INS_NOT_LOCKED            ((4<<3)|MSG_INFORMATIONAL) /* <instrument is not locked> */
#define CAMP_INS_LOCK_REQUESTOR        ((5<<3)|MSG_INFORMATIONAL) /* <instrument was locked by the requesting process> */
/* warning */
#define CAMP_CANT_SET_IF               ((6<<3)|MSG_WARNING)  /* <can't set interface while online> */
#define CAMP_NOT_CONN                  ((7<<3)|MSG_WARNING)  /* <instrument not online - cannot perform operation> */
#define CAMP_INS_LOCKED                ((8<<3)|MSG_WARNING)  /* <cannot perform command: instrument is locked> */
#define CAMP_INS_LOCKED_OTHER          ((9<<3)|MSG_WARNING)  /* <instrument is locked by another process> */
//#define CAMP_INVAL_LOCKER              ((10<<3)|MSG_WARNING) /* <cannot lock instrument from remote node> */
//#define CAMP_SERVER_BUSY_TIMEOUT       ((11<<3)|MSG_WARNING) /* <warning: CAMP server busy, will process request ASAP, return status unknown> */
#define CAMP_RS232_PORT_IN_USE         ((12<<3)|MSG_WARNING) /* <rs232 port in use> */
/* failure */
#define CAMP_FAILURE                   ((11<<3)|MSG_ERROR) /* <failure> */
#define CAMP_FAIL_RPC                  ((12<<3)|MSG_ERROR) /* <failure: RPC call> */
#define CAMP_INVAL_INS                 ((13<<3)|MSG_ERROR) /* <invalid: instrument ident> */
#define CAMP_INVAL_INS_TYPE            ((14<<3)|MSG_ERROR) /* <invalid: instrument type> */
#define CAMP_INVAL_LINK                ((15<<3)|MSG_ERROR) /* <invalid: link> */
#define CAMP_INVAL_VAR                 ((16<<3)|MSG_ERROR) /* <invalid: variable item> */
#define CAMP_INVAL_VAR_TYPE            ((17<<3)|MSG_ERROR) /* <invalid: variable type> */
#define CAMP_INVAL_VAR_ATTR            ((18<<3)|MSG_ERROR) /* <invalid: variable attribute not allowed> */
#define CAMP_INVAL_FILE                ((19<<3)|MSG_ERROR) /* <invalid: filename> */
#define CAMP_INVAL_FILESPEC            ((20<<3)|MSG_ERROR) /* <invalid: filespec> */
#define CAMP_INVAL_IF                  ((21<<3)|MSG_ERROR) /* <invalid: interface> */
#define CAMP_INVAL_IF_TYPE             ((22<<3)|MSG_ERROR) /* <invalid: interface type> */
#define CAMP_INVAL_SELECTION           ((23<<3)|MSG_ERROR) /* <invalid: selection variable index> */
#define CAMP_INVAL_PARENT              ((24<<3)|MSG_ERROR) /* <error: couldn't find instrument for var> */
#define CAMP_FAIL_DUMP                 ((25<<3)|MSG_ERROR) /* <failure: couldn't dump to file> */
#define CAMP_FAIL_UNDUMP               ((26<<3)|MSG_ERROR) /* <failure: couldn't undump file to instrument> */

/* #define msg_inval_ins_type msg_getHelpString(CAMP_INVAL_INS_TYPE) */
/* #define msg_inval_if       msg_getHelpString(CAMP_INVAL_IF) */
/* #define msg_inval_if_t     msg_getHelpString(CAMP_INVAL_IF_TYPE) */
/* #define msg_inval_parent   msg_getHelpString(CAMP_INVAL_PARENT) */
/* #define msg_inval_var      msg_getHelpString(CAMP_INVAL_VAR) */
/* #define msg_wrong_type     msg_getHelpString(CAMP_INVAL_VAR_TYPE) */
/* #define msg_ins_locked     msg_getHelpString(CAMP_INS_LOCKED) */
/* #define msg_if_notconn     msg_getHelpString(CAMP_NOT_CONN) */


/*
 *  camp_defs.h
 */

#define	ON 1
#define OFF 0


/*
 *  Recognized operating system types
 *  (for checking the existance of a locker on
 *  a remote node)
 */
#define CAMP_OS_VMS  1
#define CAMP_OS_BSD  2

/*
 *  Directories/Filenames
 *
 *  Define a link or nfs mount point at /camp
 *  For VMS, this is a device called CAMP:
 *  Define also another mount point at /campnode  to be /camp/<node>
 */

enum CAMP_FILEMASK_TYPE {
    CAMP_DIR		        = 0,
    CAMP_DAT_DIR	        ,
    CAMP_LOG_DIR                ,
    CAMP_SRV_LOG_FILE	        ,
    CAMP_CUI_LOG_FILE	        ,
    CAMP_SRV_AUTO_SAVE	        ,
    CAMP_SRV_INI	     	,
    CAMP_INS_INI_GLOB_PATTERN	,
    CAMP_INS_INI_PFMT_BASE	,
    CAMP_INS_INI_PFMT	        ,
    CAMP_INS_INI_SFMT_BASE	,
    CAMP_INS_INI_SFMT	        ,
    CAMP_CFG_GLOB_PATTERN	,
    CAMP_CFG_PFMT_BASE	        ,
    CAMP_CFG_PFMT		,
    CAMP_CFG_SFMT_BASE	        ,
    CAMP_CFG_SFMT	        ,
    CAMP_TCL_GLOB_PATTERN	,
    CAMP_TCL_PFMT_BASE	        ,
    CAMP_TCL_PFMT		,
    CAMP_TCL_SFMT_BASE	        ,
    CAMP_TCL_SFMT		,
    CAMP_NODE_DIR               ,
    CAMP_FILEMASK_TYPE_SIZE
};
typedef enum CAMP_FILEMASK_TYPE CAMP_FILEMASK_TYPE;

/*
 *  Array variable maximums
 */
#define  MAX_ARRAY_DIM          3
#define  MAX_ARRAY_ELEM         1048576


/*
 *  Fixed-length string sizes
 */
#define  LEN_MSG		255
#define  MAX_LEN_MSG		2047
#define  LEN_IDENT		15
#define  LEN_INSSEC_NAME	LEN_IDENT
#define  LEN_PATH		127
#define  LEN_TITLE		63
#define  LEN_HELP		255
#define  LEN_SELECTION_LABEL	31	/* Labels for Selection data type */
#define  LEN_STRING		127	/* String data type */
#define  LEN_DISPLAY_DEFN	127
#define  LEN_STATUS_MSG		127
#define  LEN_IO			8192
#define  LEN_BUF		255
#define  LEN_STR    		255
#define  LEN_FILENAME		127	/* Space for full path */
#define  LEN_NODENAME		127	/* Space for full address */
#define  LEN_PROC		2047
#define  LEN_UNITS		15
#define  LEN_IF_DEFN		127     /* interface parameters, in a space-delimited string */


/*
 *  Alarm status bits
 */
#define	 CAMP_ALARM_ON		(1<<0)
#define	 CAMP_ALARM_WAS_ON	(1<<1)


/*
 *  Data status bits
 *  These must not conflict with instrument status bits
 */
#define  CAMP_VAR_ATTR_MASK	0x00000FFFL
#define  CAMP_INS_ATTR_MASK	0x0000F000L
#define  CAMP_VAR_PEND_MASK	0x00010000L

#define  CAMP_VAR_ATTR_SHOW	0x00000001L
#define  CAMP_VAR_ATTR_SET	0x00000002L
#define  CAMP_VAR_ATTR_READ	0x00000004L
#define  CAMP_VAR_ATTR_POLL	0x00000008L
#define  CAMP_VAR_ATTR_ALARM	0x00000010L
#define  CAMP_VAR_ATTR_LOG	0x00000020L
#define  CAMP_VAR_ATTR_ALERT	0x00000040L
#define  CAMP_VAR_ATTR_IS_SET	0x00000080L

#define  CAMP_INS_ATTR_LOCKED	0x00001000L

#define  CAMP_VAR_PEND_SET	0x00010000L


/*
 *  Interface masks/bits
 */
#define  CAMP_IF_ONLINE		(1<<0)


/*
 *  CAMP data type code definitions
 */
#define  CAMP_MAJOR_VAR_TYPE_MASK	0x0000FF00L


/*
 *  CAMP data type code definitions
 */
#define  CAMP_XDR_ALL		(1<<0)
#define  CAMP_XDR_UPDATE	(1<<1)
#define  CAMP_XDR_NO_NEXT	(1<<2)
#define  CAMP_XDR_NO_CHILD	(1<<3)
#define  CAMP_XDR_CHILD_LEVEL	(1<<4)



/* 
 * camp_types_mod.h
 */

u_long get_current_xdr_flag( void );
void set_current_xdr_flag( u_long flag );

/*
 *  20140315  TW  see note above about CAMP_MULTITHREADED
 *                I think this was a workaround for OSes other than
 *                vxworks and linux (i.e., VMS, ultrix)
 */
/* #if CAMP_MULTITHREADED */
/* #ifdef _POSIX_THREADS */
/* #ifndef PTHREAD */
/* /\*  */
/*  *  From pthread.h and cma.h, so I don't have to include */
/*  *  pthread.h, which forces you to link with pthread */
/*  *  libraries. */
/*  *\/ */
/* typedef void                    *cma_t_address; */
/* typedef struct CMA_T_HANDLE { */
/*     cma_t_address       field1; */
/*     short int           field2; */
/*     short int           field3; */
/*     } cma_t_handle; */
/* typedef cma_t_handle    cma_t_mutex;    /\* Needed for CMA_ONCE_BLOCK *\/ */
/* typedef cma_t_mutex     pthread_mutex_t; */
/* #endif /\* PTHREAD *\/ */
/* #endif /\* _POSIX_THREADS *\/ */
/* #endif /\* CAMP_MULTITHREADED *\/ */


enum INS_CLASS {
	INS_ROOT = 1,
	INS_SUB  = 2
};
typedef enum INS_CLASS INS_CLASS;


enum IO_TYPE {
	IO_READ     = 1,
	/* IO_READ_REQ = 2, */
	IO_WRITE    = 3,
	IO_POLL     = 4
};
typedef enum IO_TYPE IO_TYPE;


enum CAMP_VAR_TYPE {
	CAMP_VAR_TYPE_NONE       = 0,
	CAMP_VAR_TYPE_NUMERIC    = (1<<8),  //   256
	CAMP_VAR_TYPE_INT        = (1<<8)+(1<<4)+(1<<0),  //   256+16+1
	CAMP_VAR_TYPE_FLOAT      = (1<<8)+(1<<5)+(1<<0),  //   256+32+1
	CAMP_VAR_TYPE_SELECTION  = (1<<9),  //   512
	CAMP_VAR_TYPE_STRING     = (1<<10), //  1024
	CAMP_VAR_TYPE_ARRAY      = (1<<11), //  2048
	CAMP_VAR_TYPE_STRUCTURE  = (1<<12), //  4096
	CAMP_VAR_TYPE_INSTRUMENT = (1<<13), //  8192
	CAMP_VAR_TYPE_LINK       = (1<<14)  // 16384
};
typedef enum CAMP_VAR_TYPE CAMP_VAR_TYPE;


enum CAMP_IF_TYPE {
	CAMP_IF_TYPE_NONE      = 0,
	CAMP_IF_TYPE_RS232     = 1,
	CAMP_IF_TYPE_GPIB      = 2,
	CAMP_IF_TYPE_TICS      = 3,
	CAMP_IF_TYPE_CAMAC     = 4,
	CAMP_IF_TYPE_INDPAK    = 5,
	CAMP_IF_TYPE_VME       = 6,
	CAMP_IF_TYPE_TCPIP     = 7,
	CAMP_IF_TYPE_GPIB_MSCB = 8
};
typedef enum CAMP_IF_TYPE CAMP_IF_TYPE;


struct CAMP_NUMERIC {
	double val;
	timeval_t timeStarted;
	u_long num;
	double low;
	double hi;
	double sum;
	double sumSquares;
	double sumCubes;
	double offset;
	u_long tolType;
	float tol;
	char* units;
};
typedef struct CAMP_NUMERIC CAMP_NUMERIC;
bool_t xdr_CAMP_NUMERIC();


struct CAMP_SELECTION_ITEM {
	char* label;
	struct CAMP_SELECTION_ITEM *pNext;
};
typedef struct CAMP_SELECTION_ITEM CAMP_SELECTION_ITEM;
bool_t xdr_CAMP_SELECTION_ITEM();


struct CAMP_SELECTION {
	u_char val;
	u_char num;
	CAMP_SELECTION_ITEM *pItems;
};
typedef struct CAMP_SELECTION CAMP_SELECTION;
bool_t xdr_CAMP_SELECTION();


struct CAMP_STRING {
	char* val;
};
typedef struct CAMP_STRING CAMP_STRING;
bool_t xdr_CAMP_STRING();


struct CAMP_ARRAY {
	CAMP_VAR_TYPE varType;  /* CAMP_INT, CAMP_FLOAT or CAMP_STRING */
        u_int elemSize; // bytes per element, including strings
	u_int dim;
	u_int totElem;
	u_int dimSize[MAX_ARRAY_DIM];
	caddr_t pVal;
};
typedef struct CAMP_ARRAY CAMP_ARRAY;
bool_t xdr_CAMP_ARRAY();


struct CAMP_STRUCTURE {
	u_int dataItems_len;
};
typedef struct CAMP_STRUCTURE CAMP_STRUCTURE;
bool_t xdr_CAMP_STRUCTURE();


struct CAMP_IF {
	u_long status;
	char* typeIdent;
	timeval_t	lastAccess;
	float		accessDelay;
	float		timeout;
        u_long numReads;
        u_long numWrites;
        u_long numReadErrors;
        u_long numWriteErrors;
        u_long numConsecReadErrors;
        u_long numConsecWriteErrors;
	char* defn;  /* Tcl format definition */
        void* priv; /* For use by driver */ // 20140228  TW  long -> void*
	CAMP_IF_TYPE typeID;
/*	CAMP_IF_SPEC spec; */
};
typedef struct CAMP_IF CAMP_IF;
bool_t xdr_CAMP_IF();


struct REQ ;


typedef struct {
        int (*shutdownProc)( void ); 
        int (*onlineProc)( struct CAMP_IF* );
        int (*offlineProc)( struct CAMP_IF* );
        int (*readProc)( struct REQ* );
        int (*writeProc)( struct REQ* );
        int (*gpibClearProc) ( int gpib_address );
        int (*gpibTimeoutProc) ( float timeout );
} IF_t_PROCS;


struct CAMP_IF_t {
	struct CAMP_IF_t *pNext;
	char* ident;
	IF_t_PROCS procs;
	char* defaultDefn;  /* Tcl format default defn */
	char* conf;
	void* priv; /* For use by driver */ // 20140228  TW  long -> void*
	CAMP_IF_TYPE typeID;
/*	CAMP_IF_t_SPEC spec; */
#if CAMP_MULTITHREADED
	pthread_mutex_t mutex;
#endif /* CAMP_MULTITHREADED */
};
typedef struct CAMP_IF_t CAMP_IF_t;
bool_t xdr_CAMP_IF_t();

struct CAMP_INSTRUMENT {
	char* lockHost;
	u_long lockPID;
        char* lockOs;
	char* defFile;
	char* iniFile;
	u_int dataItems_len;
	CAMP_IF *pIF;
	char* typeIdent;
	u_long typeInstance;
	INS_CLASS class;
	char* initProc;
	char* deleteProc;
	char* onlineProc;
	char* offlineProc;
        struct Tcl_Interp* interp;
#if CAMP_MULTITHREADED
	pthread_mutex_t mutex;
#endif /* CAMP_MULTITHREADED */
};
typedef struct CAMP_INSTRUMENT CAMP_INSTRUMENT;
bool_t xdr_CAMP_INSTRUMENT();


struct CAMP_LINK {
	CAMP_VAR_TYPE varType;
	char* path;
};
typedef struct CAMP_LINK CAMP_LINK;
bool_t xdr_CAMP_LINK();


struct CAMP_VAR_CORE {
	char* ident;
	char* path;
	CAMP_VAR_TYPE varType;
	u_long attributes;
	char* title;
	char* help;
	char* readProc;
	char* writeProc;
	u_long status;
	char* statusMsg;
        timeval_t timeLastSet; // set by varSetSpec (varDoSet)
        timeval_t timeLastSetAttempt; // 20140407  TW  set by varSetReq
        timeval_t timeLastReadAttempt; // 20140407  TW  set by varRead
	float pollInterval;
	float logInterval;
	char* logAction;
	char* alarmAction;
};
typedef struct CAMP_VAR_CORE CAMP_VAR_CORE;
bool_t xdr_CAMP_VAR_CORE();


struct CAMP_VAR_SPEC {
	CAMP_VAR_TYPE varType;
	union {
		CAMP_NUMERIC *pNum;
		CAMP_SELECTION *pSel;
		CAMP_STRING *pStr;
		CAMP_ARRAY *pArr;
		CAMP_STRUCTURE *pStc;
		CAMP_INSTRUMENT *pIns;
		CAMP_LINK *pLnk;
	} CAMP_VAR_SPEC_u;
};
typedef struct CAMP_VAR_SPEC CAMP_VAR_SPEC;
bool_t xdr_CAMP_VAR_SPEC();


struct CAMP_VAR {
	CAMP_VAR_CORE core;
	CAMP_VAR_SPEC spec;
	struct CAMP_VAR *pNext;
	struct CAMP_VAR *pChild;
};
typedef struct CAMP_VAR CAMP_VAR;
bool_t xdr_CAMP_VAR();


struct SYS_DYNAMIC {
	timeval_t timeLastSysChange;
	timeval_t timeLastInsChange;
	char* cfgFile;
};
typedef struct SYS_DYNAMIC SYS_DYNAMIC;
bool_t xdr_SYS_DYNAMIC();


struct DIRENT {
	struct DIRENT *pNext;
	char* filename;
};
typedef struct DIRENT DIRENT;
bool_t xdr_DIRENT();


typedef struct {
        int (*initProc)( struct CAMP_VAR* );
        int (*deleteProc)( struct CAMP_VAR* );
        int (*onlineProc)( struct CAMP_VAR* );
        int (*offlineProc)( struct CAMP_VAR* );
        int (*setProc)( struct CAMP_VAR*, struct CAMP_VAR*, struct CAMP_VAR_SPEC* );
        int (*readProc)( struct CAMP_VAR*, struct CAMP_VAR* );
} INS_TYPE_PROCS;


struct INS_TYPE {
	struct INS_TYPE *pNext;
	char* ident;
	char* driverType;
	INS_TYPE_PROCS procs;
};
typedef struct INS_TYPE INS_TYPE;
bool_t xdr_INS_TYPE();


struct INS_AVAIL {
	struct INS_AVAIL *pNext;
	char* ident;
	char* typeIdent;
	char* specInitProc;
};
typedef struct INS_AVAIL INS_AVAIL;
bool_t xdr_INS_AVAIL();


typedef struct {
        int (*alertProc)( bool_t );
} ALARM_ACT_PROCS;


struct ALARM_ACT {
	struct ALARM_ACT *pNext;
	char* ident;
	u_long status;
	timeval_t timeLastSet;
	char* proc;
/*
	ALARM_ACT_PROCS procs;
*/
};
typedef struct ALARM_ACT ALARM_ACT;
bool_t xdr_ALARM_ACT();


struct LOG_ACT {
	struct LOG_ACT *pNext;
	char* ident;
	char* proc;
};
typedef struct LOG_ACT LOG_ACT;
bool_t xdr_LOG_ACT();

/*
 * All the structures with REQ or req are requests from RPC client to the server
 * for various bits of info
 */

/* struct REQ_READ_REQ { */
/* 	CAMP_IF *pIF; */
/* 	u_long efn; */
/* }; */
/* typedef struct REQ_READ_REQ REQ_READ_REQ; */
/* bool_t xdr_REQ_READ_REQ(); */


struct REQ_READ {
	CAMP_IF *pIF;
	char* cmd;
	int cmd_len;
	char* buf;
	int buf_len;
	int read_len;
};
typedef struct REQ_READ REQ_READ;
bool_t xdr_REQ_READ();


struct REQ_WRITE {
	CAMP_IF *pIF;
	char* cmd;
	int cmd_len;
};
typedef struct REQ_WRITE REQ_WRITE;
bool_t xdr_REQ_WRITE();


struct REQ_SPEC {
	IO_TYPE type;
	union {
		/* REQ_READ_REQ readReq; */
		REQ_READ read;
		REQ_WRITE write;
	} REQ_SPEC_u;
};
typedef struct REQ_SPEC REQ_SPEC;
bool_t xdr_REQ_SPEC();


typedef struct {
        int (*retProc)( struct REQ* );
} REQ_PROCS;


struct REQ {
        // struct REQ *pNext;
        // int pending;       /* is this one already processing */
	bool_t cancel;     /* flag to cancel next time around */
	timeval_t time;    /* time when it should next process */
	CAMP_VAR *pVar;
	REQ_PROCS procs;
	REQ_SPEC spec;
};
typedef struct REQ REQ;
bool_t xdr_REQ();


struct CAMP_SYS {
	char* hostname;
	char* prgName;
	SYS_DYNAMIC *pDyna;
        DIRENT *pDir; // 20140304  TW  only used by client now
	ALARM_ACT *pAlarmActs;
	CAMP_IF_t *pIFTypes;
	INS_AVAIL *pInsAvail;
	INS_TYPE *pInsTypes;
	LOG_ACT *pLogActs;
        // REQ *pReqs;
};
typedef struct CAMP_SYS CAMP_SYS;
bool_t xdr_CAMP_SYS();


struct CMD_req {
	char* cmd;
};
typedef struct CMD_req CMD_req;
bool_t xdr_CMD_req();


struct FILE_req {
	int flag;
	const char* filename;
};
typedef struct FILE_req FILE_req;
bool_t xdr_FILE_req();


struct INS_ADD_req {
	char* ident;
	char* typeIdent;
};
typedef struct INS_ADD_req INS_ADD_req;
bool_t xdr_INS_ADD_req();


struct DATA_req {
	char* path;
};
typedef struct DATA_req DATA_req;
bool_t xdr_DATA_req();


struct INS_LOCK_req {
	DATA_req dreq;
	bool_t flag;
};
typedef struct INS_LOCK_req INS_LOCK_req;
bool_t xdr_INS_LOCK_req();


struct INS_LINE_req {
	DATA_req dreq;
	bool_t flag;
};
typedef struct INS_LINE_req INS_LINE_req;
bool_t xdr_INS_LINE_req();


struct INS_FILE_req {
	DATA_req dreq;
	int flag;
	char* datFile;
};
typedef struct INS_FILE_req INS_FILE_req;
bool_t xdr_INS_FILE_req();


struct INS_LINK_req {
	DATA_req dreq;
	char* localIdent;
	char* ident;
};
typedef struct INS_LINK_req INS_LINK_req;
bool_t xdr_INS_LINK_req();


struct INS_IF_req {
	DATA_req dreq;
	CAMP_IF IF;
/*
	char* typeIdent;
	float		accessDelay;
	CAMP_IF_SPEC spec;
*/
};
typedef struct INS_IF_req INS_IF_req;
bool_t xdr_INS_IF_req();


struct INS_READ_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char* cmd_val;
	} cmd;
	u_int buf_len;
};
typedef struct INS_READ_req INS_READ_req;
bool_t xdr_INS_READ_req();


struct INS_WRITE_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char* cmd_val;
	} cmd;
};
typedef struct INS_WRITE_req INS_WRITE_req;
bool_t xdr_INS_WRITE_req();


struct INS_DUMP_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char* cmd_val;
	} cmd;
	struct {
		u_int fname_len;
		char* fname_val;
	} fname;
	struct {
		u_int skip_len;
		char* skip_val;
	} skip;
	u_int buf_len;
};
typedef struct INS_DUMP_req INS_DUMP_req;
bool_t xdr_INS_DUMP_req();


struct INS_UNDUMP_req {
	DATA_req dreq;
	struct {
		u_int cmd_len;
		char* cmd_val;
	} cmd;
	struct {
		u_int fname_len;
		char* fname_val;
	} fname;
	u_int buf_len;
};
typedef struct INS_UNDUMP_req INS_UNDUMP_req;
bool_t xdr_INS_UNDUMP_req();


struct DATA_SET_req {
	DATA_req dreq;
	CAMP_VAR_SPEC spec;
};
typedef struct DATA_SET_req DATA_SET_req;
bool_t xdr_DATA_SET_req();


struct DATA_POLL_req {
	DATA_req dreq;
	bool_t flag;
	float pollInterval;
};
typedef struct DATA_POLL_req DATA_POLL_req;
bool_t xdr_DATA_POLL_req();


struct DATA_ALARM_req {
	DATA_req dreq;
	bool_t flag;
	float tol;
	char* alarmAction;
};
typedef struct DATA_ALARM_req DATA_ALARM_req;
bool_t xdr_DATA_ALARM_req();


struct DATA_LOG_req {
	DATA_req dreq;
	bool_t flag;
	float logInterval;
	char* logAction;
};
typedef struct DATA_LOG_req DATA_LOG_req;
bool_t xdr_DATA_LOG_req();


struct DATA_GET_req {
	DATA_req dreq;
	u_long flag;
};
typedef struct DATA_GET_req DATA_GET_req;
bool_t xdr_DATA_GET_req();


/*
 * All structures with RES and res are result sets from server after an RPC 
 * request
 */

struct RES {
	int status;
	char* msg;
};
typedef struct RES RES;
bool_t xdr_RES();


struct CAMP_VAR_res {
	int status;
	char* msg;
	CAMP_VAR *pVar;
};
typedef struct CAMP_VAR_res CAMP_VAR_res;
bool_t xdr_CAMP_VAR_res();


struct CAMP_SYS_res {
	int status;
	char* msg;
	CAMP_SYS *pSys;
};
typedef struct CAMP_SYS_res CAMP_SYS_res;
bool_t xdr_CAMP_SYS_res();


struct SYS_DYNAMIC_res {
	int status;
	char* msg;
	SYS_DYNAMIC *pDyna;
};
typedef struct SYS_DYNAMIC_res SYS_DYNAMIC_res;
bool_t xdr_SYS_DYNAMIC_res();


struct DIR_res {
	int status;
	char* msg;
	DIRENT *pDir;
};
typedef struct DIR_res DIR_res;
bool_t xdr_DIR_res();


typedef bool_t (*PFB)( void );

#define _xdr_free(proc,objp)	if(objp!=NULL){xdr_free((xdrproc_t)proc,(char*)objp);free((void*)objp);objp=NULL;}


/*
 *  RPC definitions
 */
/*
 *  versioning:
 *
 *    vA.BB.CC.DD
 *
 *     A = server major architectural change
 *    BB = server minor architectural change
 *    CC = server minor change that affects RPC calls (e.g., dynamic titles, dump/undump)
 *    DD = server minor change that doesn't affect RPC calls (e.g., a new interface type)
 *       = client minor change
 *
 *  20140317  TW  v1.04 of the server succeeds v1.3, after minor architectural changes
 *                CAMP_SRV_VERSNUM now follows A.BB.CC (previously was 13 for version 1.3)
 */
#define CAMP_PROGNUM ((u_long)0x20000001) // RPC program number
#define CAMP_VERSNUM ((u_long)10400) // RPC program version number 
//#define CAMP_VERSNUM ((u_long)13) // RPC program version number 
#define CAMP_VERSION_STRING "v1.04.00" 
#define CAMP_SRV_VERSION_STRING CAMP_VERSION_STRING".00" // revisions to server that don't affect RPC calls
//#define CAMP_CUI_VERSION_STRING CAMP_VERSION_STRING".00" // minor revisions to CUI
#define CAMPSRV_SYSSHUTDOWN ((u_long)1)
extern RES *campsrv_sysshutdown_10400();
#define CAMPSRV_SYSUPDATE ((u_long)2)
extern RES *campsrv_sysupdate_10400();
#define CAMPSRV_SYSLOAD ((u_long)3)
extern RES *campsrv_sysload_10400();
#define CAMPSRV_SYSSAVE ((u_long)4)
extern RES *campsrv_syssave_10400();
#define CAMPSRV_SYSREBOOT ((u_long)5)
extern RES *campsrv_sysreboot_10400();
#define CAMPSRV_SYSGET ((u_long)51)
extern CAMP_SYS_res *campsrv_sysget_10400();
#define CAMPSRV_SYSGETDYNA ((u_long)52)
extern SYS_DYNAMIC_res *campsrv_sysgetdyna_10400();
#define CAMPSRV_SYSDIR ((u_long)53)
extern DIR_res *campsrv_sysdir_10400();
#define CAMPSRV_INSADD ((u_long)101)
extern RES *campsrv_insadd_10400();
#define CAMPSRV_INSDEL ((u_long)103)
extern RES *campsrv_insdel_10400();
#define CAMPSRV_INSLOCK ((u_long)104)
extern RES *campsrv_inslock_10400();
#define CAMPSRV_INSLINE ((u_long)105)
extern RES *campsrv_insline_10400();
#define CAMPSRV_INSLOAD ((u_long)106)
extern RES *campsrv_insload_10400();
#define CAMPSRV_INSSAVE ((u_long)107)
extern RES *campsrv_inssave_10400();
#define CAMPSRV_INSIFSET ((u_long)109)
extern RES *campsrv_insifset_10400();
#define CAMPSRV_INSIFREAD ((u_long)110)
extern RES *campsrv_insifread_10400();
#define CAMPSRV_INSIFWRITE ((u_long)111)
extern RES *campsrv_insifwrite_10400();
#define CAMPSRV_INSIFON ((u_long)112)
extern RES *campsrv_insifon_10400();
#define CAMPSRV_INSIFOFF ((u_long)113)
extern RES *campsrv_insifoff_10400();
#define CAMPSRV_INSIFDUMP ((u_long)114)
extern RES *campsrv_insifdump_10400();
#define CAMPSRV_INSIFUNDUMP ((u_long)115)
extern RES *campsrv_insifundump_10400();
#define CAMPSRV_VARSET ((u_long)201)
extern RES *campsrv_varset_10400();
#define CAMPSRV_VARPOLL ((u_long)202)
extern RES *campsrv_varpoll_10400();
#define CAMPSRV_VARALARM ((u_long)203)
extern RES *campsrv_varalarm_10400();
#define CAMPSRV_VARLOG ((u_long)204)
extern RES *campsrv_varlog_10400();
#define CAMPSRV_VARZERO ((u_long)205)
extern RES *campsrv_varzero_10400();
#define CAMPSRV_VARDOSET ((u_long)206)
extern RES *campsrv_vardoset_10400();
#define CAMPSRV_VARREAD ((u_long)207)
extern RES *campsrv_varread_10400();
#define CAMPSRV_VARLNKSET ((u_long)208)
extern RES *campsrv_varlnkset_10400();
#define CAMPSRV_VARGET ((u_long)251)
extern CAMP_VAR_res *campsrv_varget_10400();
#define CAMPSRV_CMD ((u_long)301)
extern RES *campsrv_cmd_10400();


/* 
 * camp_var_utils.c 
 */
char* get_current_ident( void );
void set_current_ident( const char* ident );
void 	camp_varDoProc_recursive( void (*proc )(CAMP_VAR *), CAMP_VAR *pVar_start );
CAMP_VAR* camp_varGetPHead( const char* path/* , bool_t _mutex_lock_varlist_check */ );
CAMP_VAR **camp_varGetpp( const char* path/* , bool_t _mutex_lock_varlist_check */ );
CAMP_VAR *camp_varGetp( const char* path/* , bool_t _mutex_lock_varlist_check */ );
CAMP_VAR **camp_varGetTruePP( const char* path/* , bool_t _mutex_lock_varlist_check */ );
CAMP_VAR *camp_varGetTrueP( const char* path/* , bool_t _mutex_lock_varlist_check */ );
// CAMP_VAR_CORE *camp_varGetpCore( const char* path, bool_t _mutex_lock_varlist_check );
// caddr_t camp_varGetpSpec( const char* path, bool_t _mutex_lock_varlist_check );
CAMP_VAR *camp_varGetpIns( const char* path/* , bool_t _mutex_lock_varlist_check */ );
CAMP_VAR* camp_varGetPHead_clnt( const char* path );
CAMP_VAR **camp_varGetpp_clnt( const char* path );
CAMP_VAR *camp_varGetp_clnt( const char* path );
CAMP_VAR **camp_varGetTruePP_clnt( const char* path );
CAMP_VAR *camp_varGetTrueP_clnt( const char* path );
// CAMP_VAR_CORE *camp_varGetpCore_clnt( const char* path );
// caddr_t camp_varGetpSpec_clnt( const char* path );
CAMP_VAR *camp_varGetpIns_clnt( const char* path );
CAMP_VAR *varGetpIns( CAMP_VAR *pVar );
bool_t 	camp_varExists( const char* path );
int camp_varGetIdent( const char* path, char* ident, size_t ident_size );
int camp_varGetType( const char* path, CAMP_VAR_TYPE* pVarType );
int camp_varGetAttributes( const char* path, u_long* pAttributes );
int camp_varGetTitle( const char* path, char* title, size_t title_size );
int camp_varGetHelp( const char* path, char* help, size_t help_size );
int 	camp_varGetStatus( const char* path , u_long *pStatus );
int camp_varGetStatusMsg( const char* path, char* msg, size_t msg_size );
int 	camp_varGetPoll( const char* path , bool_t *pFlag , float *pInterval );
int 	camp_varGetAlarm( const char* path , bool_t *pFlag , char* action, size_t action_size );
int 	camp_varGetLog( const char* path , bool_t *pFlag , char* action, size_t action_size );
int 	camp_varGetValStr( const char* path , char* str, size_t str_size );
int	camp_varGetTypeStr( const char* path, char* str, size_t str_size );
int 	camp_varNumGetVal( const char* path , double *pVal );
int          varNumGetValStr( CAMP_VAR *pVar , char* str, size_t str_size );
bool_t          numGetValStr( CAMP_VAR_TYPE varType , double val , char* str, size_t str_size );
int 	camp_varNumGetUnits( const char* path , char* units, size_t units_size );
int 	camp_varNumGetStats( const char* path , u_long *pN , double *pLow , double *pHi , double *pOffset , double *pSum , double *pSumSquares , double *pSumCubes );
int 	camp_varNumCalcStats( const char* path , double *pMean , double *pStdDev , double *pSkew );
bool_t 	camp_varNumTestTol( const char* path , double val );
bool_t  camp_varNumTestAlert( const char* path, double val );
int 	camp_varSelGetID( const char* path , u_char* pVal );
int 	camp_varSelGetLabel( const char* path , char* label, size_t label_size );
int 	camp_varSelGetIDLabel( const char* path , u_char val , char* label, size_t label_size );
int          varSelGetIDLabel( CAMP_VAR *pVar , u_char val , char* label, size_t label_size );
int 	camp_varSelGetLabelID( const char* path , const char* label , u_char* pVal );
int          varSelGetLabelID( CAMP_VAR *pVar , const char* label , u_char* pVal );
int 	camp_varStrGetVal( const char* path , char* val, size_t val_size );
int 	camp_varArrGetVal( const char* path , caddr_t pVal );
int 	camp_varLnkGetVal( const char* path , char* val, size_t val_size );


/* 
 * camp_sys_utils.c 
 */
INS_TYPE* camp_sysGetpInsType( const char* ident/* , bool_t _mutex_lock_sys_check */ );
ALARM_ACT* camp_sysGetpAlarmAct( const char* ident/* , bool_t _mutex_lock_sys_check */ );
LOG_ACT* camp_sysGetpLogAct( const char* ident/* , bool_t _mutex_lock_sys_check */ );
CAMP_IF_t* camp_sysGetpIFType( const char* ident/* , bool_t _mutex_lock_sys_check */ );
INS_TYPE* camp_sysGetpInsType_clnt( const char* ident );
ALARM_ACT* camp_sysGetpAlarmAct_clnt( const char* ident );
LOG_ACT* camp_sysGetpLogAct_clnt( const char* ident );
CAMP_IF_t* camp_sysGetpIFType_clnt( const char* ident );


/* 
 * camp_ins_utils.c 
 */
int camp_insDel( const char* path );
void camp_insDelAll( void );
int camp_insGetLock( const char* path , bool_t *pFlag );
int camp_insGetLine( const char* path , bool_t *pFlag );
int camp_insGetFile( const char* path , char* filename, size_t filename_size );
int camp_insGetTypeIdent( const char* path , char* typeIdent, size_t typeIdent_size );
int camp_insGetIfTypeIdent( const char* path , char* typeIdent, size_t typeIdent_size );


/* 
 * camp_if_utils.c 
 */
CAMP_IF_t *camp_ifGetpIF_t( const char* ident/* , bool_t _mutex_lock_sys_check */ );
CAMP_IF_t *camp_ifGetpIF_t_clnt( const char* ident );
char* camp_getIfRs232Port( const char* defn, char* str, size_t str_size );
int camp_getIfRs232Baud( const char* defn );
int camp_getIfRs232Data( const char* defn );
char* camp_getIfRs232Parity( const char* defn, char* str, size_t str_size );
int camp_getIfRs232Stop( const char* defn );
char* camp_getIfRs232ReadTerm( const char* defn, char* str, size_t str_size ) ;
char* camp_getIfRs232WriteTerm( const char* defn, char* str, size_t str_size );
/* int camp_getIfRs232Timeout( const char* defn ); */
int camp_getIfGpibAddr( const char* defn );
char* camp_getIfGpibReadTerm( const char* defn, char* str, size_t str_size );
char* camp_getIfGpibWriteTerm( const char* defn, char* str, size_t str_size );
int camp_getIfGpibMscbAddr( const char* defn );
int camp_getIfCamacB( const char* defn );
int camp_getIfCamacC( const char* defn );
int camp_getIfCamacN( const char* defn );
int camp_getIfIndpakSlot( const char* defn );
char* camp_getIfIndpakType( const char* defn, char* str, size_t str_size );
int camp_getIfIndpakChannel( const char* defn );
uintptr_t camp_getIfVmeBase( const char* defn ); /* 20140221  TW  pedantic: return uintptr_t: integer guaranteed to be size of pointer */
char* camp_getIfTcpipAddr( const char* defn, char* str, size_t str_size );
int camp_getIfTcpipPort( const char* defn );
char* camp_getIfTcpipReadTerm( const char* defn, char* str, size_t str_size );
char* camp_getIfTcpipWriteTerm( const char* defn, char* str, size_t str_size );
/* float camp_getIfTcpipTimeout( const char* defn ); */
void camp_if_termTokenToChars( const char* term_in, char* term_out, size_t term_out_size, int* term_len );

/* 
 * camp_path_utils.c 
 */
bool_t camp_pathAtTop( const char* path );
void camp_pathInit( char* path, size_t path_size );
char* camp_pathExpand( const char* path, char* expansion, size_t expansion_size ); /* expansion_size with terminator */
bool_t camp_pathCompare( const char* path1, const char* path2 );
char* camp_pathGetFirst( const char* path, char* ident, size_t ident_size );
char* camp_pathGetLast( const char* path, char* ident, size_t ident_size );
char* camp_pathGetNext( const char* path, const char* path_curr , char* ident_next, size_t ident_next_size );
char* camp_pathDownNext( const char* path, char* path_curr, size_t path_curr_size );
char* camp_pathDown( char* path, size_t path_size, const char* ident );
char* camp_pathUp( char* path );


/*
 *  camp_log.c 
 */
int camp_startLog ( const char* prog_name, const char* filename, bool_t keepClosedFlag );
int camp_stopLog ( void );
void camp_log( const char* fmt, ... );
void camp_logPrefix( const char* prefix, const char* fmt, ... );
void camp_setLogPrefixOnce( const char* prefix );


/* 
 * camp_msg_utils.c 
 */
char* msg_getHelpString( MSG_TYPE type );
char* camp_getMsg( void );
bool_t camp_isMsg( void );
void camp_setMsg( const char* fmt, ... );
void camp_setMsgStat( int status, ... );
void camp_appendMsg( const char* fmt, ... );
void camp_appendMsgPrefix( const char* format, ... );
void camp_appendMsgNoSep( const char* format, ... );
void camp_appendMsgStat( int status, ... );


//  bad
/* #define _camp_log                              if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_setLogPrefixOnce( __FUNCTION__ ); } camp_log */
/* #define _camp_setMsg        camp_setMsg( "" ); if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); } camp_appendMsg */
/* #define _camp_appendMsg                        if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); } camp_appendMsg */
//  C99 variadic macro __VA_ARGS__
/* #define _camp_log( fmt, ... )                                 if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_setLogPrefixOnce( __FUNCTION__ ); } camp_log      ( fmt, ##__VA_ARGS__ ) */
/* #define _camp_setMsg( fmt, ... )           camp_setMsg( "" ); if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); }  camp_appendMsg( fmt, ##__VA_ARGS__ ) */
/* #define _camp_appendMsg( fmt, ... )                           if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); }  camp_appendMsg( fmt, ##__VA_ARGS__ ) */
//  variadic macro args compatible with older versions of gcc (i.e., for vxworks cross-compiling)
#define _camp_log( fmt, args... )                              if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_setLogPrefixOnce( __FUNCTION__ ); } camp_log      ( fmt , ##args ) // note space before comma

#define _camp_setMsg( fmt, args... )        camp_setMsg( "" ); if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); }  camp_appendMsg( fmt , ##args )
#define _camp_appendMsg( fmt, args... )                        if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); }  camp_appendMsg( fmt , ##args )

#define _camp_setMsgStat( status, args... ) camp_setMsg( "" ); if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); }  camp_appendMsgStat( status , ##args )
#define _camp_appendMsgStat( status, args... )                 if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( __FUNCTION__ ); }  camp_appendMsgStat( status , ##args )


/* 
 * camp_token_defs.h
 */

/*
 * kinds of tokens 
 */
enum tok_kind {
	TOK_SYS_UPDATE,
	TOK_SYS_SHUTDOWN,
	TOK_SYS_REBOOT,
	TOK_SYS_LOAD, 
	TOK_SYS_SAVE, 
	TOK_SYS_GETINSTYPES, 
	TOK_SYS_GETALARMACTS,
	TOK_SYS_GETLOGACTS,
	TOK_SYS_GETIFTYPES,
	TOK_SYS_GETIFCONF,
	TOK_SYS_ADDALARMACT,
	TOK_SYS_ADDLOGACT,
	TOK_SYS_ADDIFTYPE,
	TOK_SYS_ADDINSTYPE,
	TOK_SYS_ADDINSAVAIL,
	TOK_MSG, 
	TOK_INS_ADD, 
	TOK_INS_DEL, 
	TOK_INS_SET, 
	TOK_INS_LOAD, 
	TOK_INS_SAVE, 
	TOK_INS_IF_ON, 
	TOK_INS_IF_OFF, 
	TOK_INS_IF_READ, 
	TOK_INS_IF_WRITE, 
	TOK_INS_IF_READ_VERIFY, 
	TOK_INS_IF_WRITE_VERIFY, 
	TOK_INS_IF_DUMP, 
	TOK_INS_IF_UNDUMP, 
	TOK_VAR_SET, 
	TOK_VAR_DOSET, 
	TOK_VAR_READ, 
	TOK_VAR_GET, 
	TOK_LNK_SET,
	TOK_INT,
	TOK_FLOAT,
	TOK_SELECTION,
	TOK_STRING,
	TOK_ARRAY,
	TOK_STRUCTURE,
	TOK_INSTRUMENT,
	TOK_LINK,
	TOK_SELECTIONS,
        TOK_ARRAYDEF,
	TOK_VARTYPE,
	TOK_INSTYPE,
	TOK_TITLE,
	TOK_HELP,
	TOK_SHOW_ATTR,
	TOK_SET_ATTR,
	TOK_READ_ATTR,
	TOK_POLL_ATTR,
	TOK_LOG_ATTR,
	TOK_ALARM_ATTR,
	TOK_SHOW_FLAG,
	TOK_SET_FLAG,
	TOK_READ_FLAG,
	TOK_POLL_FLAG,
	TOK_POLL_INTERVAL,
	TOK_LOG_FLAG,
	TOK_LOG_ACTION,
	TOK_ALARM_FLAG,
	TOK_ALARM_TOL,
	TOK_ALARM_TOLTYPE,
	TOK_ALARM_ACTION,
	TOK_UNITS,
	TOK_VALUE_FLAG,
	TOK_ZERO_FLAG,
	TOK_ALERT_FLAG,
	TOK_MSG_FLAG,
	TOK_ON, 
	TOK_OFF, 
	TOK_END, 
	TOK_OPT_DEF, 
	TOK_OPT_IF_SET, 
	TOK_OPT_IF_MOD, 
	TOK_OPT_IF, 
	TOK_OPT_LOCK, 
	TOK_OPT_LINE, 
	TOK_OPT_ONLINE, 
	TOK_OPT_OFFLINE, 
	TOK_NONE, 
	TOK_ODD, 
	TOK_EVEN, 
	TOK_CR, 
	TOK_LF, 
	TOK_CRLF, 
	TOK_LFCR, 
	TOK_OPT_READPROC,
	TOK_OPT_WRITEPROC,
	TOK_OPT_SETPROC,
	TOK_OPT_DRIVERTYPE,
	TOK_OPT_INITPROC,
	TOK_OPT_ONLINEPROC,
	TOK_OPT_OFFLINEPROC,
	TOK_OPT_DELETEPROC,
	TOK_EOF
};
typedef enum tok_kind TOK_KIND;

/*
 * a token 
 */
struct token {
	TOK_KIND kind;
	char* str;
};
typedef struct token TOKEN;

typedef struct token_item {
    TOKEN* pTok;
    struct token_item* pNext;
} TOKEN_ITEM;

#include <stddef.h> // for size_t 
#include <stdarg.h> // for va_list 

/* 
 * camp_token.c
 */
char* toktostr( TOK_KIND kind );
bool_t findToken( const char* str, TOKEN* ptok );

/*
 *  camp_string.c
 */
size_t camp_strnlen( const char* str, size_t str_size );
//  free if necessary, then strdup
void camp_strdup( char** pdest, const char* src );
//  always terminates, size includes terminator, returns ideal bytes copied (w/o terminator)
int camp_strncpy( char* dest, size_t dest_size, const char* src ); 
//  copy at most src_bytes_max bytes
int camp_strncpy_max( char* dest, size_t dest_size, const char* src, size_t src_bytes_max ); 
//  copy exactly src_bytes_to_copy bytes
int camp_strncpy_num( char* dest, size_t dest_size, const char* src, size_t src_bytes_to_copy ); 
//  always terminates, size includes terminator, returns ideal bytes copied (w/o terminator)
int camp_strncat( char* dest, size_t dest_size, const char* src ); 
//  copy at most src_bytes_max bytes
int camp_strncat_max( char* dest, size_t dest_size, const char* src, size_t src_bytes_max ); 
//  copy exactly src_bytes_to_copy bytes
int camp_strncat_num( char* dest, size_t dest_size, const char* src, size_t src_bytes_to_copy ); 
//  always terminates, size includes terminator, returns ideal bytes copied (w/o terminator)
int camp_snprintf( char* dest, size_t dest_size, const char* format, ... ); 
//  always terminates, size includes terminator, returns ideal bytes copied (w/o terminator)
int camp_vsnprintf(char* dest, size_t dest_size, const char* format, va_list ap ); 
//  version that checks buffer length, always terminates, returns ideal bytes copied (w/o terminator)
int camp_stoprint_expand( const char* str_in, char* str_out, size_t str_out_size );
//  combo of strdup and stoprint_expand; returns pointer to allocated string.
char * camp_stoprint_strdup( const char* str_in );
//  similar to Tcl split
int camp_stringSplit( const char* str, char delim, char** tokens, size_t tokens_dim, size_t token_size );
//  similar to Tcl join
int camp_stringJoin( char* str, size_t str_size, char delim, char** tokens, size_t tokens_dim );
int camp_stringGetToken( const char* str_in, int delim, int token_index, char* str_out, size_t str_out_size );

/*
 *  camp_file.c
 */
void camp_init_filemasks();
void camp_shutdown_filemasks();
const char* camp_getFilemask( CAMP_FILEMASK_TYPE type, bool_t specific );
const char* camp_getFilemaskGeneric( CAMP_FILEMASK_TYPE type );
const char* camp_getFilemaskSpecific( CAMP_FILEMASK_TYPE type );

/*
 *  translate an old-style generic filemask (beginning with /camp/ or /campnode/)
 *  to a specific filemask, by substituting with the environment variables CAMP_DIR and CAMP_NODE_DIR
 */
void camp_translate_generic_filename( const char* mask_in, char* mask_out, int mask_out_size );

/*
 *  camp_utils.c
 */
extern double timeval_to_double( timeval_t* ptv );
extern timeval_t double_to_timeval( double d );
extern timeval_t addtimeval_double( timeval_t* ptv, double d );
extern timeval_t addtimeval( timeval_t* ptv1, timeval_t* ptv2 );
extern caddr_t camp_zalloc( size_t size );
extern caddr_t camp_rezalloc( caddr_t ptr, size_t size );
extern void* camp_malloc( size_t size );
extern void* camp_calloc( size_t nmemb, size_t size );
extern void* camp_realloc( void* ptr, size_t size );

/* 
 *  globals
 *
 *    declared in camp_api_proc.c for clients
 *    declared in camp_srv_main.c for server
 */
extern CAMP_SYS* pSys;
extern CAMP_VAR* pVarList;
extern unsigned int camp_debug;

/*
 *  16-Dec-1999  TW  Define that overrides all camp_debug conditionals
 *                   (makes code smaller/faster)
 */
#if !defined(CAMP_DEBUG)
#define CAMP_DEBUG 0
#endif

enum CAMP_DEBUG_BITMASK {
    CAMP_DEBUG_ALL           = 0xFFFFFFFF,
    CAMP_DEBUG_ANY           = 0xFFFFFFFF,

    CAMP_DEBUG_SRV           = 0x00FFFFFF,

    CAMP_DEBUG_IF            =       0xFF,
    CAMP_DEBUG_IF_RS232      =        0x1,
    CAMP_DEBUG_IF_GPIB       =        0x2,
    CAMP_DEBUG_IF_VME        =        0x4,
    CAMP_DEBUG_IF_TCPIP      =        0x8,
    CAMP_DEBUG_IF_CAMAC      =       0x10,

    CAMP_DEBUG_SRV_INIT      =      0x100, // server init and shutdown
    CAMP_DEBUG_SRV_LOOP      =      0x200, // server loop stages
    CAMP_DEBUG_SVC           =      0x400, // server svc thread logic (rpc call handling)
    CAMP_DEBUG_POLL          =      0x800, // server poll thread logic
    CAMP_DEBUG_ALARM         =     0x1000, // server alarms
    CAMP_DEBUG_RPC           =     0x2000, // local versions of rpc internals
    CAMP_DEBUG_MUTEX         =     0x4000, // server mutex management
    CAMP_DEBUG_THREAD        =     0x8000, // server thread management
    CAMP_DEBUG_TRACE         =    0x10000, // error status messages include entire traceback
    CAMP_DEBUG_CMD           =    0x20000, // 

    CAMP_DEBUG_CLNT          = 0xFF000000,
};
typedef enum CAMP_DEBUG_BITMASK CAMP_DEBUG_BITMASK;

#define _camp_debug( bitmask ) \
  ( CAMP_DEBUG && ( camp_debug & ( bitmask ) ) )

#endif // !_CAMP_H_
