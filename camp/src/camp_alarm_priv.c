/*
 *  Name:       camp_alarm_priv.c 
 *
 *  Purpose:    CAMP alarm related routines called from the CAMP server
 *
 *
 *  $Log: camp_alarm_priv.c,v $
 *  Revision 1.8  2015/04/21 05:50:30  asnd
 *  Fix erroneous edit in previous rev.
 *
 *  Revision 1.7  2015/04/21 05:27:43  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.6  2015/03/17 00:07:28  suz
 *  This file part of Ted's 201404 new interface package (Rev 1.5 comment in error)
 *
 *  Revision 1.5  2015/03/16 22:31:48  suz
 *  This file part of Ted's 201404 new interface package (Rev 1.4 comment in error)
 *
 *  Revision 1.4  2015/03/16 21:03:32  suz
 *  file moved to retired_vms directory
 *
 *  Revision 1.3  2001/12/19 18:08:55  asnd
 *  Record the time at which alarms change their states
 *
 *  Revision 1.2  2001/07/19 02:22:35  asnd
 *  Make sure alarmAction gets switched off when an alarm is turned
 *  off or changed.
 *
 *  Revision history:
 *
 *    20140214  TW  tidying CAMP_DEBUG
 *    20140217  TW  rename set_global_mutex_noChange to mutex_lock_mainInterp
 *
 */

#include <stdio.h>
#include "camp_srv.h"

static void alarm_update ( CAMP_VAR *pVar );


/*
 *  Name:       alarm_update
 *
 *  Purpose:    Check a CAMP variable, if it has an alarm active then set
 *              the status of the corresponding alarm to ON.
 *
 *  Called by:  camp_alarmCheck (recursively using camp_varDoProc_recursive)
 *              (camp_varDoProc_recursive traverses the linked list calling
 *              a function for every list member).
 * 
 *  Inputs:     A pointer to a CAMP variable structure
 *
 *  Preconditions:
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */
static void
alarm_update( CAMP_VAR* pVar )
{
    // _mutex_lock_begin();

    if( pVar->core.status & CAMP_VAR_ATTR_ALERT )
    {
	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    ALARM_ACT* pAlarmAct;
	    
	    pAlarmAct = camp_sysGetpAlarmAct( pVar->core.alarmAction /* , mutex_lock_sys_check() */ );
	    if( pAlarmAct == NULL )
	    {
		goto return_;
	    }

	    pAlarmAct->status |= CAMP_ALARM_ON;
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    }

return_:
    // _mutex_lock_end();
    return;
}


/*
 *  (before 201403) varSetAlert called camp_alarmCheck to check
 *  the change in status of an alarm when any corresponding variable changed
 *  it's alert status.
 *
 *  (201403) main thread calls srv_do_alarms to check alarm status 
 *  on each main thread processing loop
 *    - this change was made to ensure use of the mainInterp Tcl interpreter
 *      is only at a high level - never from within use of an instrument
 *      Tcl interpreter (which could cause deadlocks since both types
 *      of interpreters have mutexes)
 */
void
srv_do_alarms()
{
    _mutex_lock_begin();
    
    /*
     *  turn all alarm status OFF
     */
    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {

	ALARM_ACT* pAlarmAct;

	for( pAlarmAct = pSys->pAlarmActs; 
	     pAlarmAct != NULL; 
	     pAlarmAct = pAlarmAct->pNext ) 
	{
	    /*
	     *  turn alarm status OFF
	     */
	    pAlarmAct->status &= ~CAMP_ALARM_ON;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     *  traverse variables, alerted variables turn corresponding alarm off
     */
    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	camp_varDoProc_recursive( alarm_update, pVarList );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    /*
     *  for each alarm, act on new status
     */
    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {

	ALARM_ACT* pAlarmAct;

	for( pAlarmAct = pSys->pAlarmActs; 
	     pAlarmAct != NULL; 
	     pAlarmAct = pAlarmAct->pNext ) 
	{
	    if( pAlarmAct->status & CAMP_ALARM_ON )
	    {
		/* 
		 *  Set alarm ON if it is not already
		 */
		if( !( pAlarmAct->status & CAMP_ALARM_WAS_ON ) )
		{
		    int tcl_status = TCL_OK;

		    if( _camp_debug(CAMP_DEBUG_ALARM) ) { _camp_log( "setting alarm '%s' on (proc: '%s')", pAlarmAct->ident, pAlarmAct->proc ); }

		    //  20-Dec-1999  TW  Always use main Tcl interpreter
		    {
			Tcl_Interp* interp;
			_mutex_lock_mainInterp_on();
			interp = camp_tclInterp(); // get_thread_interp();
			tcl_status = Tcl_VarEval( interp, pAlarmAct->proc, " {1}", NULL );
			_mutex_lock_mainInterp_off();
		    }

		    if( tcl_status == TCL_ERROR ) 
		    {
			_camp_appendMsg( "failed alertProc (alarm: '%s')", pAlarmAct->ident );
		    }
		    else
		    {
			pAlarmAct->status |= CAMP_ALARM_WAS_ON;
			gettimeval( &pAlarmAct->timeLastSet );
			// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
			{
			    copytimeval( &pAlarmAct->timeLastSet, &pSys->pDyna->timeLastSysChange );
			}
			// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
		    }
		}
	    }
	    else
	    {
		/* 
		 *  Set alarm OFF if it is not already
		 */
		if( pAlarmAct->status & CAMP_ALARM_WAS_ON )
		{
		    int tcl_status = TCL_OK;

		    if( _camp_debug(CAMP_DEBUG_ALARM) ) { _camp_log( "setting alarm '%s' off (proc: '%s')", pAlarmAct->ident, pAlarmAct->proc ); }

		    //  20140225  TW  don't use interp outside of lock
		    {
			Tcl_Interp* interp = camp_tclInterp(); // get_thread_interp();
			_mutex_lock_mainInterp_on();
			tcl_status = Tcl_VarEval( interp, pAlarmAct->proc, " {0}", NULL );
			_mutex_lock_mainInterp_off();
		    }

		    if( tcl_status == TCL_ERROR ) 
		    {
			_camp_appendMsg( "failed alertProc (alarm: '%s')", pAlarmAct->ident );
		    }
		    else
		    {
			pAlarmAct->status &= ~CAMP_ALARM_WAS_ON;
			gettimeval( &pAlarmAct->timeLastSet );
			// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
			{
			    copytimeval( &pAlarmAct->timeLastSet, &pSys->pDyna->timeLastSysChange );
			}
			// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
		    }
		}
	    }
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

//return_:
    _mutex_lock_end();
}
