/*
 *  Name:       camp_api_proc.c
 *
 *  Purpose:    Layer for all CAMP applications
 *              See the CAMP Programmer's Guide for individual descriptions.
 *              See programs like camp_cmd.c, camp_cui.c and the MuSR
 *              data archivers for examples of usage (camp_cmd.c is the
 *              simplest to start).
 *
 *  Includes:   campSrv_* -  client routines that make RPC calls to the CAMP
 *                           server
 *              camp_clnt* - client routines that manage the particular state
 *                           of the CAMP client program
 *              All FORTRAN wrappers for the above, using cfortran.h
 *              FORTRAN wrappers have the preface "f_"
 *              
 *  Revision history:
 *    09-Feb-2001  DJA Dump/Undump
 *    14-Dec-1999  TW  Casting for Linux (gcc warnings)
 *
 *  $Log: camp_api_proc.c,v $
 *  Revision 1.13  2018/06/15 04:13:07  asnd
 *  Improve error message
 *
 *  Revision 1.12  2015/04/21 05:24:08  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.8  2013/04/16 08:03:55  asnd
 *  Allow return messages to contain "%" (fixed two ways!)
 *
 *  Revision 1.7  2004/01/28 03:42:36  asnd
 *  Includes Suzannah's PPC revisions, plus minor editing.
 *
 *  Revision 1.6  2002/09/27 00:59:07  asnd
 *  camp_clntUpdate: check status of campSrv_varGet properly
 *
 *  Revision 1.5  2002/05/08 00:47:43  suz
 *  Add support for ppc (casting & ifdef VXWORKS)
 *
 *  Revision 1.4  2001/04/20 02:41:30  asnd
 *  Fixes for VMS build
 *
 *  Revision 1.3  2001/02/10 07:14:49  asnd
 *  DJA: insIfDump / insIfUndump API routines
 *
 */

#include <stdlib.h>
#include <string.h>
#include "camp_clnt.h"

#ifdef linux
#include <unistd.h> // gethostname
#include <netdb.h> // gethostbyname
#endif

/*
 *  Need this for struct hostent
 */
#ifdef MULTINET
#include "multinet_common_root:[multinet.include]netdb.h"
#else
#ifndef VXWORKS
#include <netdb.h>
#endif // VXWORKS
#endif /* MULTINET */

/*
 *  globals
 */
CAMP_SYS*        pSys = NULL;
CAMP_VAR*        pVarList = NULL;
unsigned int     camp_debug = 0; // 20140214  TW  bool_t -> int
DIRENT*          pSys_pDir = NULL; // 20140326  TW  replacement for pSys->pDir

// char* gpcPrgName = "CAMP client"; // for mtrpc
char                camp_serverHostname[LEN_NODENAME+1] = "";
enum clnt_stat        camp_clnt_stat;

struct hostent* camp_pServerHostent = NULL;

static CLIENT*        camp_client = NULL;
static timeval_t localTimeLastSysChange;
static timeval_t localTimeLastInsChange;


#include <sys/socket.h>
#ifdef     PPCxxx
#include <errno.h>
#else  // !PPCxxx
#include <sys/errno.h>
#endif //  PPCxxx
// Already included:  #include <netdb.h>

// int mutex_lock_sys( int on ) { return 0; }
// int mutex_lock_varlist( int on ) { return 0; }
// bool_t mutex_lock_sys_check() { return TRUE; }
// bool_t mutex_lock_varlist_check() { return TRUE; }


int
camp_clntInit( char* serverName, long clientTimeout )
{
    timeval_t tv;
    int uid;
    gid_t gid;
    gid_t gids[3];
    // char os[8];
    char hostname[LEN_NODENAME];
#ifdef VXWORKS
    int  host_addr=0;
#endif // VXWORKS

/*
 *  printf("\n");
 *  printf("camp_clntInit: now starting with serverName=%s, timeout=%d\n",
 *     serverName,clientTimeout);
 *  fflush( stdout );
 */
    cleartimeval( &localTimeLastSysChange );
    cleartimeval( &localTimeLastInsChange );

#ifdef VXWORKS

    host_addr = hostGetByName(serverName);
#ifdef PPCxxx
    if (host_addr == -1)
#else // !PPCxxx
    if (host_addr == 0)
#endif // PPCxxx
    {
        _camp_setMsg( "error: can't find server '%s'", serverName );
        return( CAMP_FAILURE );
    }
    camp_strncpy( camp_serverHostname, sizeof( camp_serverHostname ), serverName ); 

#else // !VXWORKS

    camp_pServerHostent = (struct hostent *)gethostbyname(serverName);
    if( camp_pServerHostent == NULL )
    {
        _camp_setMsg( "error: can't find server '%s'", serverName );
        return( CAMP_FAILURE );
    }

    /*
     *  initialize global and static variables
     */
    camp_strncpy( camp_serverHostname, sizeof( camp_serverHostname ), camp_pServerHostent->h_name ); 

#endif // VXWORKS

    /*
     *  build the camp_client structure
     */
    /*  printf( "calling clnt_create with camp_serverHostname=%s...\n",camp_serverHostname ); */

#ifdef MULTINET

    camp_client = clnt_create( camp_serverHostname, CAMP_PROGNUM, CAMP_VERSNUM, "tcp" );

#else // !MULTINET

    /*
     *  19-Dec-2000  TW  Tried using own version of clnt_create in order
     *                   to have control of the timeout at the first
     *                   attempted connection.
     *                   But, using my_clnt_create on Linux
     *                   caused the CUI to intermittently act strangely
     *                   (false RPC timeouts). 
     */
 // camp_client = my_clnt_create( camp_serverHostname, CAMP_PROGNUM, CAMP_VERSNUM, "tcp" );
    camp_client = clnt_create( camp_serverHostname, CAMP_PROGNUM, CAMP_VERSNUM, "tcp" );

#endif // MULTINET

    /* printf( "clnt_create is done\n" );*/ 

    if( camp_client == NULL ) 
    {
        _camp_setMsg( "%s", clnt_spcreateerror( camp_serverHostname ) );
        return( CAMP_FAILURE );
    }

    /*
     *  Possibly change the client timeout
     *
     
     *  16-Dec-1999  TW  Use this again, but set the 'hard' RPC timeout
     *                   to a value somewhat larger than the 'soft' timeout 
     */
    /*
     * printf( "clientTimeout=%d \n",clientTimeout );
     * fflush( stdout );
     */
    if( clientTimeout > 0 )
    {
        tv.tv_sec = clientTimeout;
        tv.tv_usec = 0;
        clnt_control( camp_client, CLSET_TIMEOUT, (char*)&tv );
    }
    
    /*
     *  build the camp_client authorization
     */
    gethostname( hostname, LEN_NODENAME );
    /*
     * printf( "called gethostname with hostname=%s,
     *     LEN_NODENAME=%d\n",hostname,LEN_NODENAME);
     * fflush( stdout );
     */
    
#ifdef VXWORKS
    uid = 0;
    gid = 0;
    gids[0] = 0; // 20140310 TW
    // gids[1] = CAMP_OS_BSD; // 20140310 TW
#else
    uid = getuid();
    gid = getgid();
    gids[0] = getpid();
#endif
    /*
     *  Define client operating system
     */
#if defined( VMS )
    gids[1] = CAMP_OS_VMS;
#else /* Works for Ultrix, Digital Unix, SunOS */
    gids[1] = CAMP_OS_BSD;
#endif

    /*
     *  TW  04-Dec-1996  Server controls timeout
     */
    gids[2] = (int)clientTimeout;

    camp_client->cl_auth = authunix_create( hostname, uid, gid, 3, gids );
    if( camp_client->cl_auth == NULL ) 
    {
	/* printf( "camp_clntInit: no authorization for camp host \"%s\"", camp_serverHostname ); */
        _camp_setMsg( "%s", clnt_spcreateerror( camp_serverHostname ) );
        return( CAMP_FAILURE );
    }

    /*
     *  initialize global variables
     */
    camp_setMsg( "" );

    /*
     *  initialize system section
     */
/*
    camp_status = campSrv_sysGet();
    if( _failure( camp_status ) ) return( camp_status );
*/
    return( CAMP_SUCCESS );
}


int
camp_clntEnd( void )
{
    camp_setMsg( "" );

    _xdr_free( xdr_CAMP_VAR, pVarList );
    _xdr_free( xdr_CAMP_SYS, pSys );
    cleartimeval( &localTimeLastSysChange );
    cleartimeval( &localTimeLastInsChange );

    if( camp_client != NULL ) 
    {
        if( camp_client->cl_auth != NULL ) 
        {
            auth_destroy( camp_client->cl_auth );
            camp_client->cl_auth = NULL;
        }

        /*
         *  Close the socket and free the memory
         *  from the clnt_create() call
         */
        clnt_destroy( camp_client );
        camp_client = NULL;
    }

    return( CAMP_SUCCESS );
}

int
camp_clntUpdate( void )
{
    int camp_status;

    if( pSys == NULL )
    {
        camp_status = campSrv_sysGet();
        if( _failure( camp_status ) ) 
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campSrv_sysGet" ); }
            return( camp_status );
        }
    }
    else
    {
        camp_status = campSrv_sysGetDyna();
        if( _failure( camp_status ) ) 
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campSrv_sysGetDyna" ); }
            return( camp_status );
        }

        if( difftimeval( &localTimeLastSysChange, 
                         &pSys->pDyna->timeLastSysChange ) < 0.0 )
        {
            copytimeval( &pSys->pDyna->timeLastSysChange, 
                         &localTimeLastSysChange );

            camp_status = campSrv_sysGet();
            if( _failure( camp_status ) ) 
            {
                if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campSrv_sysGet" ); }
                return( camp_status );
            }
        }
    }

    if( difftimeval( &localTimeLastInsChange, 
                     &pSys->pDyna->timeLastInsChange ) < 0.0 )
    {
        copytimeval( &pSys->pDyna->timeLastInsChange, &localTimeLastInsChange );

        camp_status = campSrv_varGet( "/", CAMP_XDR_ALL );
        if( _failure( camp_status ) ) 
        {
            if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campSrv_varGet(\"/\")" ); }
            return( camp_status );
	}
    }

    return( CAMP_SUCCESS );
}


          /*----------------------------------------*/
         /*  00X System actions :  to server       */
        /*----------------------------------------*/

int 
campSrv_sysShutdown( void )
{
    RES* pRes;

    pRes = campsrv_sysshutdown_10400( NULL, camp_client );
    return( pRes->status );
}


int 
campSrv_sysUpdate( void )
{
    RES* pRes;

    pRes = campsrv_sysupdate_10400( NULL, camp_client );
    return( pRes->status );
}


int
campSrv_sysLoad( char* filename, int flag )
{
    RES* pRes;
    FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.flag = flag;
    req.filename = filename;

    pRes = campsrv_sysload_10400( &req, camp_client );
    if( _success( pRes->status ) ) 
    {
        camp_clntUpdate();
    }
    return( pRes->status );
}


int
campSrv_sysSave( char* filename, int flag )
{
    RES* pRes;
    FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.flag = flag;
    req.filename = filename;

    pRes = campsrv_syssave_10400( &req, camp_client );
    return( pRes->status );
}


          /*----------------------------------------*/
         /*  05X System actions :  from server     */
        /*----------------------------------------*/

int 
campSrv_sysGet( void )
{
    CAMP_SYS_res* pRes; 

/*    _xdr_free( xdr_CAMP_SYS, pSys ); */
    pRes = campsrv_sysget_10400( NULL, camp_client );
/*    pSys = pRes->pSys; */

    return( pRes->status );
}


int
campSrv_sysGetDyna( void )
{
    SYS_DYNAMIC_res* pRes;

/*    _xdr_free( xdr_SYS_DYNAMIC, pSys->pDyna );*/
    pRes = campsrv_sysgetdyna_10400( NULL, camp_client );
/*    pSys->pDyna = pRes->pDyna;*/

    return( pRes->status );
}


int
campSrv_sysDir( const char* filespec )
{
    FILE_req req;
    DIR_res* pRes;

    bzero((void *)&req, sizeof( req ) );
    req.filename = filespec;

/*    _xdr_free( xdr_SETUP, pSys->pAvailSetups );*/
    pRes = campsrv_sysdir_10400( &req, camp_client );
/*    pSys->pAvailSetups = pRes->pSetup;*/

    return( pRes->status );
}


          /*----------------------------------------*/
         /*  10X instrument actions :  to server     */
        /*----------------------------------------*/

int
campSrv_insAdd( char* typeIdent, char* ident )
{
    RES* pRes;
    INS_ADD_req req;

    if( ident == NULL ) return( CAMP_INVAL_INS );
    if( typeIdent == NULL ) return( CAMP_INVAL_INS_TYPE );

    bzero((void *)&req, sizeof( req ) );
    req.ident = ident;
    req.typeIdent = typeIdent;

    pRes = campsrv_insadd_10400( &req, camp_client );
    if( _success( pRes->status ) ) 
    {
        camp_clntUpdate();
    }

    return( pRes->status );
}


/*
 *  campSrv_insDel,  send path because could be a reference to a instrument
 *                    included under another instrument
 */
int 
campSrv_insDel( char* path )
{
    RES* pRes;
    DATA_req req;

    bzero((void *)&req, sizeof( req ) );
    req.path = path;

    pRes = campsrv_insdel_10400( &req, camp_client );
    if( _success( pRes->status ) ) 
    {
        camp_clntUpdate();
    }

    return( pRes->status );
}


int 
campSrv_insLock( char* path, bool_t flag )
{
    RES* pRes;
    INS_LOCK_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;

    pRes = campsrv_inslock_10400( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_insLine( char* path, bool_t flag )
{
    RES* pRes;
    INS_LINE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;

    pRes = campsrv_insline_10400( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_insLoad( char* path, char* filename, int flag )
{
    RES* pRes;
    INS_FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.datFile = filename;
    req.flag = flag;

    pRes = campsrv_insload_10400( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_insSave( char* path, char* filename, int flag )
{
    RES* pRes;
    INS_FILE_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.datFile = filename;
    req.flag = flag;

    pRes = campsrv_inssave_10400( &req, camp_client );
    return( pRes->status );
}


int
campSrv_insIf( char* path, char* typeIdent, float accessDelay, float timeout, char* defn )
{
    RES* pRes;
    INS_IF_req req;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.IF.typeIdent = typeIdent;
    req.IF.accessDelay = accessDelay;
    req.IF.timeout = timeout;
    req.IF.defn = defn;

    pRes = campsrv_insifset_10400( &req, camp_client );
    return( pRes->status );
}


RES*
campSrv_insIfRead( char* path, char* cmd, int cmd_len, int buf_len )
{
    // TOKEN tok;
    RES* pRes;
    INS_READ_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;
    req.buf_len = buf_len;

    pRes = campsrv_insifread_10400( &req, camp_client );
    return( pRes );
}


int
campSrv_insIfWrite( char* path, char* cmd, int cmd_len )
{
    // TOKEN tok;
    RES* pRes;
    INS_WRITE_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;

    pRes = campsrv_insifwrite_10400( &req, camp_client );
    return( pRes->status );
}

RES*
campSrv_insIfDump( char* path, char* fname, int fname_len, char* cmd, int cmd_len, char* skip, int skip_len, int buf_len )
{
    // TOKEN tok;
    RES* pRes;
    INS_DUMP_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.fname.fname_val = fname;
    req.fname.fname_len = fname_len;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;
    req.skip.skip_val = skip;
    req.skip.skip_len = skip_len;
    req.buf_len = buf_len;

    pRes = campsrv_insifdump_10400( &req, camp_client );
    return( pRes );
}


int
campSrv_insIfUndump( char* path, char* fname, int fname_len, char* cmd, int cmd_len, int buf_len )
{
    // TOKEN tok;
    RES* pRes;
    INS_UNDUMP_req req;

    bzero((void *)&req, sizeof( req ) );

    req.dreq.path = path;
    req.fname.fname_val = fname;
    req.fname.fname_len = fname_len;
    req.cmd.cmd_val = cmd;
    req.cmd.cmd_len = cmd_len;
    req.buf_len = buf_len;

    pRes = campsrv_insifwrite_10400( &req, camp_client );
    return( pRes->status );
}


          /*------------------------------------*/
         /*  20X Data actions :  to server     */
        /*------------------------------------*/

int  
campSrv_varSet( char* path, caddr_t pSpec )
{
    RES* pRes;
    DATA_SET_req req;
    CAMP_VAR* pVar;

    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL )
    {
        return( CAMP_INVAL_VAR );
    }

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.spec.varType = pVar->core.varType;
    req.spec.CAMP_VAR_SPEC_u.pNum = (CAMP_NUMERIC*)pSpec;

    pRes = campsrv_varset_10400( &req, camp_client );
    return( pRes->status );
}


int
campSrv_varDoSet( char* path, caddr_t pSpec )
{
    RES* pRes;
    DATA_SET_req req;
    CAMP_VAR* pVar;

    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL )
    {
        return( CAMP_INVAL_VAR );
    }

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.spec.varType = pVar->core.varType;
    req.spec.CAMP_VAR_SPEC_u.pNum = (CAMP_NUMERIC*)pSpec;

    pRes = campsrv_vardoset_10400( &req, camp_client );
    return( pRes->status );
}


int
campSrv_varNumSetVal( char* path, double val )
{
    CAMP_NUMERIC num;
    CAMP_VAR* pVar;

    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) 
    {
	return( CAMP_INVAL_VAR );
    }

    bcopy((void *)pVar->spec.CAMP_VAR_SPEC_u.pNum, (void *)&num, sizeof( CAMP_NUMERIC ) );
    num.val = val;

    return( campSrv_varSet( path, (caddr_t)&num ) );
}


int
campSrv_varNumSetTol( char* path, u_long tolType, float tol )
{
    CAMP_NUMERIC num;
    CAMP_VAR* pVar;

    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) 
    {
	return( CAMP_INVAL_VAR );
    }

    bcopy( (void *)pVar->spec.CAMP_VAR_SPEC_u.pNum, (void *)&num, sizeof( CAMP_NUMERIC ) );
    num.tol = tol;
    num.tolType = tolType;

    return( campSrv_varSet( path, (caddr_t)&num ) );
}


int
campSrv_varSelSetVal( char* path, u_char val )
{
    CAMP_SELECTION spec;

    bzero((void *)&spec, sizeof( spec ) );
    spec.val = val;

    return( campSrv_varSet( path, (caddr_t)&spec ) );
}


int
campSrv_varStrSetVal( char* path, char* val )
{
    CAMP_STRING spec;

    bzero((void *)&spec, sizeof( spec ) );
    spec.val = val;

    return( campSrv_varSet( path, (caddr_t)&spec ) );
}


int
campSrv_varArrSetVal( char* path, caddr_t pVal )
{
    CAMP_ARRAY spec;

    bzero((void *)&spec, sizeof( spec ) );
    spec.pVal = pVal;

    return( campSrv_varSet( path, (caddr_t)&spec ) );
}


int
campSrv_varLnkSetVal( char* path, char* val )
{
    RES* pRes;
    DATA_SET_req req;
    CAMP_LINK spec;
    CAMP_VAR* pVar;

    if( ( pVar = camp_varGetTrueP_clnt( path ) ) == NULL )
    {
        return( CAMP_INVAL_VAR );
    }

    if( pVar->core.varType != CAMP_VAR_TYPE_LINK ) 
    {
        return( CAMP_INVAL_VAR );
    }

    bzero((void *)&spec, sizeof( spec ) );
    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.spec.varType = pVar->core.varType;
    req.spec.CAMP_VAR_SPEC_u.pLnk = &spec;
    req.spec.CAMP_VAR_SPEC_u.pLnk->path = val;

    pRes = campsrv_varlnkset_10400( &req, camp_client );

    return( pRes->status );
}


int  
campSrv_varRead( char* path )
{
    RES* pRes;
    DATA_req req;
    CAMP_VAR* pVar;

    if( ( pVar = camp_varGetp_clnt( path ) ) == NULL )
    {
        return( CAMP_INVAL_VAR );
    }

    bzero((void *)&req, sizeof( req ) );
    req.path = path;

    pRes = campsrv_varread_10400( &req, camp_client );

    return( pRes->status );
}


int 
campSrv_varPoll( char* path, bool_t flag, float pollInterval )
{
    RES* pRes;
    DATA_POLL_req req;    

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;
    req.pollInterval = pollInterval;

    pRes = campsrv_varpoll_10400( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_varAlarm( char* path, bool_t flag, char* alarmAction )
{
    RES* pRes;
    DATA_ALARM_req req;    

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;
    req.alarmAction = alarmAction;
    
    pRes = campsrv_varalarm_10400( &req, camp_client );
    return( pRes->status );
}


int 
campSrv_varLog( char* path, bool_t flag, char* logAction )
{
    RES* pRes;
    DATA_LOG_req req;    

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;
    req.logAction = logAction;

    pRes = campsrv_varlog_10400( &req, camp_client );
    return( pRes->status );
}


/*
 *  campSrv_varZero,  zero the statistics of a data item and 
 *                           all data items below
 */
int 
campSrv_varZero( char* path )
{
    RES* pRes;
    DATA_req req;

    bzero((void *)&req, sizeof( req ) );
    req.path = path;

    pRes = campsrv_varzero_10400( &req, camp_client );
    return( pRes->status );
}


          /*------------------------------------*/
         /*  25X Data actions :  from server   */
        /*------------------------------------*/

/*
 *  campSrv_varGet() - Get a data item from the server and store
 *                          it locally
 */
int 
campSrv_varGet( char* path, int flag )
{
    DATA_GET_req req;
    CAMP_VAR_res* pVar_res;

    bzero((void *)&req, sizeof( req ) );
    req.dreq.path = path;
    req.flag = flag;

    pVar_res = campsrv_varget_10400( &req, camp_client );
    return( pVar_res->status );
}


          /*----------------------------------------*/
         /*  30X misc                              */
        /*----------------------------------------*/

int
campSrv_cmd( char* cmd )
{
    CMD_req req;
    RES* pRes;

    bzero((void *)&req, sizeof(req) );
    req.cmd = cmd;

    pRes = campsrv_cmd_10400( &req, camp_client );
    return( pRes->status );
}


