/*
 *  Name:       camp_cfg_write.c
 *
 *  Purpose:    Write the CAMP configuration to a file from the CAMP server
 *
 *  Revision history:
 *    18-Dec-2000  TW  In varLink:  added curly brackets, and only write
 *                     lnkSet if path is something other than an empty string
 *
 */

#include <stdlib.h>
#include <string.h>
#include "camp_srv.h"


/*
 *  Name:       campCfg_write
 *
 *  Purpose:    Write the CAMP configuration to a file, and each instrument
 *              initialization to individual files.
 *
 *  Called by:  srv_loop - CAMP server main thread loop, for periodic autosave
 *                      of camp.cfg
 *              srv_shutdown - autosave of camp.cfg at exit
 *              campsrv_syssave - manual save-configuration (for blank file
 *                      name or "camp.cfg")
 * 
 *  Inputs:     
 *
 *  Preconditions:
 *
 *  Outputs:
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */
int
campCfg_write( const char* filename )
{
    int camp_status;

    /*  Save all instrument initialization files  */
    save_all_ini();

    /*  Open the configuration file (see camp_write.c)  */

    camp_status = openOutput( filename );

    if( _failure( camp_status ) ) 
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed openOutput" ); }
	return( camp_status );
    }

    /* 
     *  Initialize file status (sets up initial tab indents properly)
     */
    reinitialize( filename, NULL, NULL );

    /*  Write Tcl commands necessary to add the current list of instruments */
    cfgWrite_insadd();

    /*  Write particular instrument settings as Tcl commands */
    /*  This actually just writes the commands necessary to load instrument
        initialization files to the configuration file */
    cfgWrite_insset();

    /*  Make sure configuration file always exits with success status when
        interpreted by Tcl */
    print( "return -code ok\n" );

    /*  Close configuration file */
    closeOutput();

    return( CAMP_SUCCESS );
}

/*
 *  Name:       save_all_ini
 *
 *  Purpose:    Save all instrument initialization files according to the
 *              current state of the instrument.
 *              File names default to camp_ins_<name>.ini unless set otherwise
 *
 *  Called by:  campCfg_write
 * 
 *  Inputs:     None
 *
 *  Outputs:    None
 *
 *  Revision history:
 *
 */
void
save_all_ini( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    RES* pRes;
    INS_FILE_req insFile_req;
    char filename[LEN_FILENAME];

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;
	    /*
	     *  Make sure an INI file name exists for the Instrument
	     */
	    if( ( pIns->iniFile == NULL ) || ( strlen( pIns->iniFile ) == 0 ) )
	    {
		camp_snprintf( filename, sizeof( filename ), camp_getFilemaskGeneric(CAMP_INS_INI_PFMT), pVar->core.ident );
	    }
	    else
	    {
		camp_strncpy( filename, sizeof( filename ), pIns->iniFile ); // strcpy( filename, pIns->iniFile );
	    }
	    insFile_req.dreq.path = pVar->core.path;
	    insFile_req.flag = 1;
	    insFile_req.datFile = filename;
	    pRes = campsrv_inssave_10400( &insFile_req, get_current_rqstp() );
	    _free( pRes );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock
}


/*
 *  Name:       cfgWrite_insadd
 *
 *  Purpose:    Write Tcl command necessary to add all current instruments
 *
 *  Called by:  campCfg_write
 * 
 *  Inputs:     None
 *
 *  Outputs:    None
 *
 *  Revision history:
 *
 */
void 
cfgWrite_insadd( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;
	    print_tab( "%s %s %s\n", toktostr( TOK_INS_ADD ), 
		       pIns->typeIdent, pVar->core.ident );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock
}


/*
 *  Name:       cfgWrite_insset
 *
 *  Purpose:    Write Tcl commands necessary to load the instrument
 *              initialization files for all current instruments.
 *
 *  Called by:  campCfg_write
 * 
 *  Inputs:     None
 *
 *  Outputs:    None
 *
 *  Revision history:
 *
 */
void 
cfgWrite_insset( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
	{
	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	    if( ( pIns->iniFile != NULL ) && ( strlen( pIns->iniFile ) > 0 ) )
	    {
		print_tab( "catch {%s %s {%s}}\n", toktostr( TOK_INS_LOAD ), 
			   pVar->core.path, pIns->iniFile );
	    }
	    add_tab();
	    camp_varDoProc_recursive( cfgWrite_varLink, pVar->pChild );
	    del_tab();
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock
}


/*
 *  Name:       campState_write
 *
 *  Purpose:    Write the entire CAMP state to a file -- the single-file
 *              counterpart of campCfg_write
 *
 *  Called by:  campsrv_syssave - save configuration to any file name
 * 
 *  Inputs:     
 *
 *  Preconditions:
 *
 *  Outputs:
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */

int
campState_write( const char* filename )
{
    int camp_status;

    /*  Open the configuration file (see camp_write.c)  */

    camp_status = openOutput( filename );

    if( _failure( camp_status ) ) 
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed openOutput" ); }
	return( camp_status );
    }

    /* 
     *  Initialize file status (sets up initial tab indents properly)
     */
    reinitialize( filename, NULL, NULL );

    /*
     * It might make sense to prepare a clean slate for restoration,
     * by writing
     *    foreach ins [sysGetInsNames] { insDel /$ins }
     * but that is too risky. 
     */

    /* 
     * Write commands to create the current list of instruments
     */
    cfgWrite_insadd();

    /*  Write particular instrument settings as Tcl commands */
    cfgWrite_all_inst();

    /*  Make sure configuration file always exits with success status when
        interpreted by Tcl */
    print( "return -code ok\n" );

    /*  Close configuration file */
    closeOutput();

    return( CAMP_SUCCESS );
}

/*
 *  Name:         cfgWrite_if
 *
 *  Purpose:      Write interface settings
 *
 *  Called by:    iniWrite_Instrument, cfgWrite_all_inst (camp_ini_write.c)
 * 
 *  Inputs:       Instrument interface structure pointer
 *
 *  Outputs:      Status
 *
 *  Revision history:
 *
 */
int
cfgWrite_if( CAMP_IF* pIF )
{
    print( "%s %s %f %f %s ", 
                toktostr( TOK_OPT_IF_SET ),
		pIF->typeIdent,
                pIF->accessDelay,
                pIF->timeout,
                pIF->defn );

    return( CAMP_SUCCESS );
}


/*
 *  Name:       cfgWrite_varLink
 *
 *  Purpose:    Write Tcl command necessary to set the value of a
 *              Link variable.
 *
 *  Called by:  cfgWrite_insset - recursively for each instrument
 * 
 *  Inputs:     Pointer to CAMP variable structure
 *
 *  Outputs:    None
 *
 *  Revision history:
 *    18-Dec-2000  TW  Added curly brackets, and only write lnkSet if path
 *                     is something other than an empty string
 *
 */
void 
cfgWrite_varLink( CAMP_VAR* pVar )
{
    if( pVar->spec.varType == CAMP_VAR_TYPE_LINK )
    {
        CAMP_LINK* pLnk = pVar->spec.CAMP_VAR_SPEC_u.pLnk;

        if( !streq( pLnk->path, "" ) )
        {
          print( "%s %s {%s}\n", toktostr( TOK_LNK_SET ), 
                 pVar->core.path, pLnk->path );
        }
    }
}


