/*
 *  Name:       camp_clnt.h 
 *
 *  Purpose:    Declarations for API calls; both C and Fortran.
 *              See the CAMP Programmer's Guide for individual descriptions,
 *              and camp_api_proc.c for the sources.
 *
 *  $Log: camp_clnt.h,v $
 *  Revision 1.5  2015/03/17 00:18:17  suz
 *  This file part of Ted's 201404 new interface package (Rev 1.4 comment in error)
 *
 *  Revision 1.4  2015/03/16 21:03:32  suz
 *  file moved to retired_vms directory
 *
 *  Revision 1.3  2001/07/13 05:28:45  asnd
 *  DA:  Add a comment block.
 *
 */

#ifndef _CAMP_CLNT_H_
#define _CAMP_CLNT_H_

#include "camp.h"

/* camp_api_proc.c */
// int mutex_lock_sys( int on );
// int mutex_lock_varlist( int on );
// bool_t mutex_lock_sys_check();
// bool_t mutex_lock_varlist_check();
int camp_clntInit ( char* serverName , long clientTimeout );
int camp_clntEnd ( void );
int camp_clntUpdate ( void );
int campSrv_sysShutdown ( void );
int campSrv_sysUpdate ( void );
int campSrv_sysLoad ( char* filename , int flag );
int campSrv_sysSave ( char* filename , int flag );
int campSrv_sysGet ( void );
int campSrv_sysGetDyna ( void );
int campSrv_sysDir ( const char* filespec );
int campSrv_insAdd ( char* typeIdent, char* ident );
int campSrv_insDel ( char* path );
int campSrv_insLock ( char* path , bool_t flag );
int campSrv_insLine ( char* path , bool_t flag );
int campSrv_insLoad ( char* path , char* filename , int flag );
int campSrv_insSave ( char* path , char* filename , int flag );
int campSrv_insIf ( char* path, char* typeIdent, float accessDelay, float timeout, char* defn );
RES *campSrv_insIfRead ( char* path , char* cmd , int cmd_len , int buf_len );
int campSrv_insIfWrite ( char* path , char* cmd , int cmd_len );
RES *campSrv_insIfDump ( char* path , char* fname, int fname_len, char* cmd, int cmd_len, char* skip, int skip_len, int buf_len );
int campSrv_insIfUndump( char* path , char* fname, int fname_len, char* cmd, int cmd_len, int buf_len );
int campSrv_varSet ( char* path , caddr_t pSpec );
int campSrv_varDoSet ( char* path , caddr_t pSpec );
int campSrv_varNumSetVal ( char* path , double val );
int campSrv_varNumSetTol ( char* path , u_long tolType, float tol );
int campSrv_varSelSetVal ( char* path , u_char val );
int campSrv_varStrSetVal ( char* path , char* val );
int campSrv_varArrSetVal ( char* path , caddr_t pVal );
int campSrv_varLnkSetVal ( char* path , char* val );
int campSrv_varRead ( char* path );
int campSrv_varPoll ( char* path , bool_t flag , float pollInterval );
int campSrv_varAlarm ( char* path , bool_t flag , char* alarmAction );
int campSrv_varLog ( char* path , bool_t flag , char* logAction );
int campSrv_varZero ( char* path );
int campSrv_varGet ( char* path , int flag );
int campSrv_cmd ( char* cmd );

#define mutex_lock_sys( on )             {}
// #define mutex_lock_sys_check()           TRUE
#define mutex_lock_varlist( on )         {}
// #define mutex_lock_varlist_check()       TRUE

#endif /* _CAMP_CLNT_H_ */
