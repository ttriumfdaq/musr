/*
 *  Name:       camp_cmd.c
 *
 *  Purpose:    Main program for camp_cmd executable
 *              Takes command line input and sends via an RPC call to the
 *              CAMP server.  The commands are interpreted in the CAMP
 *              server's Tcl interpreter.
 *
 *              Tcl commands may operate on all CAMP instruments and
 *              variables, but you will not have access to global Tcl variables
 *              that are within a CAMP instrument driver from this command.
 *
 *              Note though that the driver's interface and variable
 *              Tcl proc's do have access to internal Tcl global variables
 *              when initiated by this client program, as they would when
 *              initiated by the camp_cui or otherwise.
 *
 *              Is that confusing?
 *
 *              Because of this limitation, it is advised that all important
 *              variables be made CAMP variables within the CAMP instrument
 *              driver.  That way, they are always accessible from all
 *              CAMP client programs.
 *
 *  Revision history:
 *    20140218     TW  tidy status handling
 *    16-Dec-1999  TW  Use CAMP_RPC_CLNT_TIMEOUT with clntInit 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef linux
#include <unistd.h> // gethostname
#endif /* Linux */
#include "camp_clnt.h"

#ifdef VMS
#define VMS_CONTROL_BIT_28 (1<<28) // inhibit printing message at image exit
#define _VMS_STATUS( camp_status | VMS_CONTROL_BIT_28 )
#define _CAMP_FAILURE() _VMS_STATUS( CAMP_FAILURE )
#else /* !VMS */
#define _CAMP_FAILURE() CAMP_FAILURE
#endif /* VMS */

#define usage( argv0 ) \
        fprintf( stderr, "usage: %s [-node <server>] \"<command>\"\n", argv0 );


#ifdef VXWORKS
int camp_cmd( char* aServerName, char* cmd )
#else
int main( int argc, char* argv[] )
#endif
{
    int camp_status;
    char serverName[LEN_NODENAME+1];
    char* msg;
    int i;

    serverName[0] = '\0';

#ifdef VXWORKS
    if( aServerName )
    {
	camp_strncpy( serverName, sizeof( serverName ), aServerName );
    }
#else
    char* cmd;

    if( argc < 2 )
    {
	usage( argv[0] );
	exit( _CAMP_FAILURE() );
    }

    for( i = 1; i < argc; i++ )
    {
        if( streq( argv[i], "-node" ) )
        {
            if( ++i >= argc )
            {
                usage( argv[0] );
                exit( CAMP_FAILURE );
            }
            camp_strncpy( serverName, sizeof( serverName ), argv[i] );
        }
        else if( streq( argv[i], "-d" ) )
        {
	    int count;

            if( ++i >= argc )
            {
                usage( argv[0] );
                exit( CAMP_FAILURE );
            }

	    count = sscanf( argv[i], "%x", &camp_debug ); // camp_debug = atoi( argv[i] ); // 20140418 TW bitmask

	    if( count < 1 )
	    {
		usage( argv[0] );
		exit( CAMP_FAILURE );
	    }
        }
    }

/*     if( argc > 2 ) */
/*     { */
/* 	if( streq( argv[1], "-node" ) ) */
/* 	{ */
/* 	    if( argc < 4 ) */
/* 	    { */
/* 		usage( argv[0] ); */
/* 		exit( _CAMP_FAILURE() ); */
/* 	    } */
      
/* 	    camp_strncpy( serverName, sizeof( serverName ), argv[2] );  */
/*          cmd = argv[3]; */
/* 	} */
/* 	else */
/* 	{ */
/* 	    usage( argv[0] ); */
/* 	    exit( _CAMP_FAILURE() ); */
/* 	} */
/*     } */
    
    cmd = argv[argc-1];
#endif

    if( streq( serverName, "" ) )
    {
	char* host;
    
	/*
	 *  Host is CAMP_HOST environment variable
	 *  or else local host if undefined
	 */
	host = getenv( "CAMP_HOST" );
	if( host == NULL )
	{
	    gethostname( serverName, LEN_NODENAME );
	    // printf( "%s: gethostname:'%s'\n", argv[0], serverName );
	}
	else
	{
	    camp_strncpy( serverName, sizeof( serverName ), host ); 
	    // printf( "%s: getenv:'%s'\n", argv[0], serverName );
	}
    }

    if( camp_debug == 0 )
    {
	char* env = getenv( "CAMP_DEBUG" );

	if( env != NULL )
	{
	    unsigned int debug_mask = 0;
	    int count = sscanf( env, "%x", &debug_mask ); // camp_debug = atoi( env ); // 20140418 TW bitmask
	    if( count == 1 ) camp_debug = debug_mask;
	}
    }

    // printf( "%s: server:'%s' cmd:'%s' camp_debug:%d\n", argv[0], serverName, cmd, camp_debug );

    camp_status = camp_clntInit( serverName, CAMP_RPC_CLNT_TIMEOUT );

    if( _success( camp_status ) )
    {
	camp_status = campSrv_cmd( cmd );
    }
  
    msg = camp_getMsg();

#ifdef VMS

    /*
     *  Set local (1) and global (2) symbols
     */
    lib_set_symbol( "CAMP_RESULT", msg, 1 );
    lib_set_symbol( "CAMP_RESULT", msg, 2 );

    if( isatty( fileno( stdin ) ) ) 
    {
	if( *msg != '\0' ) puts( msg );
    }
  
    exit( _VMS_STATUS( camp_status ) );

#else  /* not VMS */

    /*
     *  Consider a corresponding setenv() here for Unix if desired, but
     *  it is generally useless because env propagates downward only.
     */

    if( *msg != '\0' ) puts( msg );

#ifdef VXWORKS
    return 0;
#else
    exit( _success( camp_status ) ? 0 : camp_status );
#endif

#endif /* VMS */
} 


