/*
 *  $Id: camp_cui.h,v 1.17 2015/04/21 01:07:15 asnd Exp $
 *
 *  $Revision: 1.17 $
 *
 *  Purpose: This file handles all definitions for the the Character based User Interface
 *
 *  $Log: camp_cui.h,v $
 *  Revision 1.17  2015/04/21 01:07:15  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API
 *
 *  Revision 1.13  2013/04/16 08:07:09  asnd
 *  Make var titles changeable. Let camp_cui change units.
 *
 *  Revision 1.12  2009/04/08 01:57:01  asnd
 *  Symbolic names for clarity (enum didn't compile)
 *
 *  Revision 1.11  2006/08/03 08:43:16  asnd
 *  Sort directory listings
 *
 *  Revision 1.10  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.9  2005/07/13 04:13:30  asnd
 *  Command-line options to adjust behaviour
 *
 *  Revision 1.8  2005/07/06 10:59:48  asnd
 *  More save/restore instrument modifications
 *
 *  Revision 1.7  2005/07/06 03:53:32  asnd
 *  Change menus (and hotkeys) for loading instrument configurations.
 *
 *  Revision 1.6  2004/01/28 03:13:26  asnd
 *  Add VME interface type. Make alert's BEEP more periodic.
 *
 *  Revision 1.5  2001/12/19 18:05:09  asnd
 *  Make "alerts" window (covering key window) to display alarm conditions.
 *
 *  Revision 1.4  2001/04/20 02:41:30  asnd
 *  Fixes for VMS build
 *
 *  Revision 1.3  2000/12/22 23:59:31  David.Morris
 *  Added INDPAK support for adding instruments in the CUI
 *
 *
 */

#ifndef _CAMP_CUI_H_
#define _CAMP_CUI_H_

#include "camp_clnt.h"

/*
 *  camp_cui maintains a log file (intended for offline debugging only)
 */
#ifndef CAMP_CUI_LOG 
#define CAMP_CUI_LOG 0
#endif

#include <netdb.h>
/* SD add ifdef VMS for smgdef.h */
#ifdef VMS
#include <smgdef.h>
#endif

#include <curses.h>

#ifdef VMS
/*
 *  Define some standard ncurses constants and functions
 *  in terms of VMS's non-standard equivalents, so we can
 *  use standard style in VMS.
 */
#define    A_BLINK         _BLINK
#define    A_BOLD          _BOLD
#define    A_REVERSE       _REVERSE
#define    A_UNDERLINE     _UNDERLINE
#define    attroff(a)      clrattr(a)
#define    wattroff(w,a)   wclrattr(w,a)
#define    attron(a)       setattr(a)
#define    wattron(w,a)    wsetattr(w,a)
#define    cbreak          crmode
#define    nocbreak        nocrmode
#define    wtimeout(w,i)  
#define    wgetnstr(w,r,l) wgetstr(w,r)  
#define    keypad(x,v)      
#define    immedok(x,v)      

#endif /* VMS */

/*
 *  see camp.h for revisioning
 */
#define CAMP_CUI_VERSION_STRING CAMP_VERSION_STRING".00" // revisions to CUI that don't break compatibility with server

#define  SELECTION_CANCEL	0
#define  SELECTION_OK		1

#define  MAX_NUM_SELECTIONS    200
#define  LEN_SELECTION		32

#define  POLL_INTERVAL		5
#define  PAUSE_US           700000

/*  Attributes are BOLD, UNDERLINE, REVERSE, BLINK */
#define SELECTED_ATTR	    A_REVERSE
#define DEV_SELECTED_ATTR   A_UNDERLINE
#define ALARM_ATTR	    A_BLINK
#define CURSOR_ATTR	    A_REVERSE
#define HOTKEY_ATTR	    A_BOLD

#define FLOAT_FORMAT	    "%#8.2E"
#define FLOAT_UNITS_FORMAT  "%#8.2E%2.2s"
#define INTEGER_FORMAT	    "%8d"
#define STRING_FORMAT	    "%8.8s"

#define SCREEN_H	    screenHeight   /* curses macro is LINES */
#define SCREEN_W	    screenWidth    /* curses macro is COLS  */

extern int screenHeight, screenWidth;

#define FIRST_LINE	    0
#define LAST_LINE	    (SCREEN_H-1)

/*
 *  Not really used
 */
#define MESSAGEWIN_H	    0
#define MESSAGEWIN_W	    SCREEN_W
#define MESSAGEWIN_Y	    (LAST_LINE-MESSAGEWIN_H+1)
#define MESSAGEWIN_X	    0

/*
 *  Status line (top line)
 */
#define STATUSWIN_Y	    FIRST_LINE

/*
 *  Menu not used any more
 */
#define MENU_LINE	    FIRST_LINE
#define MENUWIN_H	    (SCREEN_H-MENU_LINE-MESSAGEWIN_H-1)
#define MENU_ITEM_X_START   1

/*
 *  Not used
 */
#define DATATITLEWIN_Y	    FIRST_LINE
#define DATATITLEWIN_H	    1

/*
 *  Display variable names
 */
#define DATAWIN_Y	    (STATUSWIN_Y+1)
#define DATAWIN_X	    (0)
#define DATAWIN_H	    (SCREEN_H-DATAWIN_Y-MESSAGEWIN_H-KEYWIN_H)
#define DATAWIN_W	    (36)

/*
 *  Display details of 'cursored' variable
 */
#define VARINFOWIN_Y	    DATAWIN_Y
#define VARINFOWIN_X	    (DATAWIN_X+DATAWIN_W)
#define VARINFOWIN_H	    (SCREEN_H-VARINFOWIN_Y-MESSAGEWIN_H-KEYWIN_H)
#define VARINFOWIN_W	    (SCREEN_W-VARINFOWIN_X)

/*
 *  Key window (brief help) in bottom
 */
#define KEYWIN_Y	    (SCREEN_H-KEYWIN_H) /* (VARINFOWIN_Y+VARINFOWIN_H) */
#define KEYWIN_X	    (DATAWIN_X)  /* (VARINFOWIN_X) */
#define KEYWIN_H	    (4)
#define KEYWIN_W	    (SCREEN_W) /* (VARINFOWIN_W) */

#define ALERTWIN_Y KEYWIN_Y
#define ALERTWIN_X KEYWIN_X
#define ALERTWIN_H KEYWIN_H
#define ALERTWIN_W KEYWIN_W

/*
 *  Popup help window
 */
#define HELPWIN_Y	    (FIRST_LINE+1)
#define HELPWIN_X	    0
#define HELPWIN_H	    (SCREEN_H-HELPWIN_Y-MESSAGEWIN_H)
#define HELPWIN_W	    (SCREEN_W-HELPWIN_X)

/*
 *  Popup text input window
 */
#define TEXTINPUTWIN_H      3
#define TEXTINPUTWIN_W	    30

#ifdef VMS

#define KEY_CTRL_A	SMG$K_TRM_CTRLA
#define KEY_CTRL_B	SMG$K_TRM_CTRLB
#define KEY_CTRL_D	SMG$K_TRM_CTRLD
#define KEY_CTRL_E	SMG$K_TRM_CTRLE
#define KEY_CTRL_F	SMG$K_TRM_CTRLF
#define KEY_CTRL_G	SMG$K_TRM_CTRLG
#define KEY_CTRL_H	SMG$K_TRM_CTRLH
#define KEY_CTRL_I	SMG$K_TRM_CTRLI
#define KEY_CTRL_J	SMG$K_TRM_CTRLJ
#define KEY_CTRL_K	SMG$K_TRM_CTRLK
#define KEY_CTRL_L	SMG$K_TRM_CTRLL
#define KEY_CTRL_M	SMG$K_TRM_CTRLM
#define KEY_CTRL_N	SMG$K_TRM_CTRLN
#define KEY_CTRL_P	SMG$K_TRM_CTRLP
#define KEY_CTRL_R	SMG$K_TRM_CTRLR
#define KEY_CTRL_U	SMG$K_TRM_CTRLU
#define KEY_CTRL_V	SMG$K_TRM_CTRLV
#define KEY_CTRL_W	SMG$K_TRM_CTRLW
#define KEY_CTRL_Z	SMG$K_TRM_CTRLZ
#define KEY_TAB		SMG$K_TRM_HT
#define KEY_RETURN	SMG$K_TRM_CR
#define KEY_ENTER	SMG$K_TRM_ENTER
#define KEY_DEL		SMG$K_TRM_DELETE
#define KEY_SPACE	SMG$K_TRM_SPACE
#define KEY_PERIOD	SMG$K_TRM_DOT
#define KEY_UP		SMG$K_TRM_UP
#define KEY_DOWN	SMG$K_TRM_DOWN
#define KEY_LEFT	SMG$K_TRM_LEFT
#define KEY_RIGHT	SMG$K_TRM_RIGHT
#define KEY_UP_2	SMG$K_TRM_KP8
#define KEY_DOWN_2	SMG$K_TRM_KP2
#define KEY_LEFT_2	SMG$K_TRM_KP4
#define KEY_RIGHT_2	SMG$K_TRM_KP6
#define KEY_UP_3	SMG$K_TRM_PF3
#define KEY_DOWN_3	SMG$K_TRM_PF4
#define KEY_LEFT_3	SMG$K_TRM_PF1
#define KEY_RIGHT_3	SMG$K_TRM_PF2
#define KEY_NEXT	SMG$K_TRM_NEXT_SCREEN
#define KEY_PREV	SMG$K_TRM_PREV_SCREEN
#define KEY_CANCEL_1	SMG$K_TRM_CTRLZ
#define KEY_CANCEL_2	SMG$K_TRM_HT
#define KEY_BACKSPACE   SMG$K_TRM_BS
#define KEY_DC          SMG$K_TRM_E3

#else /* !VMS */

#define KEY_CTRL_A	1
#define KEY_CTRL_B	2
#define KEY_CTRL_D	4
#define KEY_CTRL_E	5
#define KEY_CTRL_F	6
#define KEY_CTRL_G	7
#define KEY_CTRL_H	8
#define KEY_CTRL_I	9
#define KEY_CTRL_J	10
#define KEY_CTRL_K	11
#define KEY_CTRL_L	12
#define KEY_CTRL_M	13
#define KEY_CTRL_N	14
#define KEY_CTRL_P	16
#define KEY_CTRL_R	18
#define KEY_CTRL_U	21
#define KEY_CTRL_V	22
#define KEY_CTRL_W	23
#define KEY_CTRL_Z	26

#ifdef linux

/* KEY_ENTER is in curses.h linux */
#define KEY_TAB  	KEY_CTRL_I
#define KEY_RETURN	13
#define KEY_SPACE	32
#define KEY_PERIOD      46
#define KEY_DEL		127
#define KEY_UP_2	KEY_UP
#define KEY_DOWN_2	KEY_DOWN
#define KEY_LEFT_2	KEY_LEFT
#define KEY_RIGHT_2	KEY_RIGHT
#define KEY_UP_3	KEY_UP
#define KEY_DOWN_3	KEY_DOWN
#define KEY_LEFT_3	KEY_LEFT
#define KEY_RIGHT_3	KEY_RIGHT
/* KEY_NEXT in curses.h linux */
#define KEY_PREV	KEY_PREVIOUS
#define KEY_CANCEL_1	KEY_CTRL_D
#define KEY_CANCEL_2	KEY_TAB

#else

#define KEY_ENTER	KEY_KP_ENTER    /* KEY_ENTER is in curses.h linux */
#define KEY_DEL		KEY_DELETE
#define KEY_UP_2	KEY_KP_8
#define KEY_DOWN_2	KEY_KP_2
#define KEY_LEFT_2	KEY_KP_4
#define KEY_RIGHT_2	KEY_KP_6
#define KEY_UP_3	KEY_KP_PF3
#define KEY_DOWN_3	KEY_KP_PF4
#define KEY_LEFT_3	KEY_KP_PF1
#define KEY_RIGHT_3	KEY_KP_PF2
#define KEY_NEXT	KEY_NEXT_SCREEN  /* KEY_NEXT in curses.h linux */
#define KEY_PREV	KEY_PREV_SCREEN
#define KEY_CANCEL_1	KEY_CTRL_D
#define KEY_CANCEL_2	KEY_TAB

#endif /* linux */

// #define BEEP()          beep()

void BEEP();

#endif /* VMS */

typedef struct {
    u_char  width;
    u_char  xPos, yPos;
    char    item[LEN_STRING];
    char    hotKey;
    void    (*callback)( void );
} MENU_ITEM;

typedef struct menuEntry {
    MENU_ITEM*		pMenuItem;
    struct menuEntry*	next;
    struct menuEntry*	last;
    struct menuEntry*	subMenu;
    struct menuEntry*	parentMenu;
} MENU_ENTRY;

typedef enum {
    HK_NONE,
    HK_WARM,
    HK_HOT
} HK_FLAG;

typedef struct {
    WINDOW* win;
    WINDOW* subwin;
    char* choices[MAX_NUM_SELECTIONS];
    u_int height;
    u_int width;
    u_int numrows;
    u_int numcols;
    u_int colwidth;
    HK_FLAG hotkeyflag;
    char  hotkeys[MAX_NUM_SELECTIONS];
} SELECTION_WIN;

typedef struct {
    WINDOW* win;
    WINDOW* subwin;
} TEXT_INPUT_WIN;

extern char	currViewPath[LEN_PATH+1];
extern int	currDisplay;

#define _DispTree 0
#define _DispVar  1

/*
 * null-terminated length-limited sprintf
 */
/* #ifdef linux */
/* #define sntprintf(buf,len,...) snprintf(buf,len,__VA_ARGS__),buf[len]='\0' */
/* /\* or: #define sntprintf(buf,len,...) (snprintf(buf,len,__VA_ARGS__)+(buf[len]='\0')) *\/ */
/* #else */
/* /\* Don't know what works on other systems. Perhaps a function with real varargs *\/  */
/* #define sntprintf(buf,len,...) snprintf(buf,len,__VA_ARGS__),buf[len-1]='\0' */
/* #endif */

/* camp_cui_alert.c */
bool_t displayAlertWin(void);
void refreshAlertWin(void);
#if !defined(VMS)
void uncoverAlertWin(void);
#endif
void touchAlertWin(void);
void clearAlertWin(void);
void deleteAlertWin(void);
int initAlertWin(void);
bool_t anyAlerts(void);

/* camp_cui_data.c */
void varPicker( void );
void displayVarLoop(void);
void displayVar(void);
#if !defined(VMS)
void uncoverVarWin(void);
#endif
void refreshVarWin(void);
void touchVarWin(void);
void deleteVarWin(void);
int initVarWin(void);
void displayData(void);
void varGetValStr(CAMP_VAR *pVar,char *str,size_t str_size);
char *getTypeStr(CAMP_VAR_TYPE varType);
#if !defined(VMS)
void uncoverDataDisplay(void);
#endif
void refreshDataDisplay(void);
void touchDataDisplay(void);
void deleteDataDisplay(void);
int initDataDisplay(void);

/*
 * camp_cui_help.c
 */
int createHelpWin( void );
int deleteHelpWin( void );
/* SD add ifdef VMS for s_dsc (defined in libc_tw/src/vaxc_utils.h */
#ifdef VMS
long helpGetInput( s_dsc* pdsc_result, s_dsc* pdsc_prompt, u_short* pResult_len );
long helpPutOutput( s_dsc* pdsc_message );
#else
char* find_help_lib( const char* const library_name, char* library_path, const int len_path );
#endif

#if !defined(VMS)
void vmshelp_reader(char subj[],char library[]);
#endif
#if defined(VMS)
long helpPutOutput(s_dsc *pdsc_message);
long helpGetInput(s_dsc *pdsc_result,s_dsc *pdsc_prompt,u_short *pResult_len);
#endif
int deleteHelpWin(void);
int createHelpWin(void);
#if !defined(VMS)
char *find_help_lib(const char *const library_name,char *library_path,const int library_path_size);
#endif

/* camp_cui_input.c */
void boxWin ( WINDOW *win , u_int height , u_int width );
int createSelectWin( SELECTION_WIN *sw, const char* title, int numRows, int numCols, int num_choices, char* choices[], HK_FLAG hotKeyFlag, u_short hotKeys[] );
void deleteSelectWin ( SELECTION_WIN *sw );
void writeOneSelection( SELECTION_WIN *sw, u_int i );
void hilightCurrSelection ( SELECTION_WIN *sw , u_int index );
void unhilightCurrSelection ( SELECTION_WIN *sw , u_int index );
bool_t inputSelectWin ( const char* title , u_int numChoices , char* choices [], u_int defaultIndex , u_int *selectedIndex );
bool_t inputHotSelectWin ( const char* title, u_int numChoices, char* choices [], u_int defaultIndex, u_int *selectedIndex, HK_FLAG hotKeyFlag, u_short hotKeys[] );
int createTextInputWin ( TEXT_INPUT_WIN *ti , const char* title , int textLen );
void deleteTextInputWin ( TEXT_INPUT_WIN *ti );
bool_t inputFloat ( const char* title , double defaultValue , double *pInput );
bool_t inputInteger ( const char* title , long defaultValue , long *pInput );
bool_t inputString ( const char* title , u_int len , const char* defaultValue , char* input, size_t input_size, bool_t (*charAllowed )(char ));

/* camp_cui_if.c */
bool_t inputIf ( CAMP_VAR *pVar, char* typeIdent, size_t typeIdent_size, double* pAccessDelay, double* pTimeout, char* defn, size_t defn_size );
bool_t inputIfType ( const char* defaultTypeIdent , char* typeIdent, size_t typeIdent_size );
bool_t inputIf_rs232( const char* defaultDefn, const char* conf, char* defn, size_t defn_size );
bool_t inputIf_gpib( const char* defaultDefn, char* defn, size_t defn_size, bool_t gpib_mscb );
bool_t inputIf_camac( const char* defaultDefn, char* defn, size_t defn_size );
bool_t inputIf_indpak( const char* defaultDefn, char* defn, size_t defn_size );
bool_t inputIf_vme( const char* defaultDefn, char* defn, size_t defn_size );
bool_t inputIf_tcpip( const char* defaultDefn, char* defn, size_t defn_size );

/* camp_cui_input_macros.c */
void nestBraces(char *str);
bool_t inputVarLog(const char *path,bool_t *pFlag,char *logAction,size_t logAction_size);
bool_t inputVarAlarm(const char *path,bool_t *pFlag,char *alarmAction,size_t alarmAction_size);
bool_t inputVarPoll(const char *path,bool_t *pFlag,double *pInterval);
bool_t inputVarUnits(const char *path,char *strUnits,size_t strUnits_size);
bool_t inputVarTitle(const char *path,char *strTitle,size_t strTitle_size);
bool_t inputVarString(CAMP_VAR *pVar,char *strVal,size_t strVal_size);
bool_t inputVarSelection(CAMP_VAR *pVar,int *piVal);
bool_t inputVarFloat(CAMP_VAR *pVar,double *pdVal);
bool_t inputVarInteger(CAMP_VAR *pVar,double *pdVal);
bool_t inputVarTol(CAMP_VAR *pVar,u_long *pTolType,double *pTol);
bool_t inputVar(char *path,size_t path_size,const char *title,u_long typeMask,u_long attrMask);
bool_t inputInsLine(const char *path,bool_t *pFlag);
bool_t inputInsLock(const char *path,bool_t *pFlag);
bool_t inputInsAvail(char *ident,size_t ident_size,char *typeIdent,size_t typeIdent_size);
bool_t inputInsType(char *typeIdent,size_t typeIdent_size);
void sortNames(char *names[],int num);
bool_t inputSetupFile(const char *title,const char *defaultFilename,char *filename,size_t filename_size,const char *mask);
bool_t isPrintChar(char c);
bool_t isPathChar(char c);
bool_t isIdentChar(char c);

/* camp_cui_keyboard.c */
int getKey ();
int getKeyUpdate ( WINDOW* win );

/*
 * camp_cui_keywin.c
 */
void displayKeyWin(void);
void refreshKeyWin(void);
#if !defined(VMS)
void uncoverKeyWin(void);
#endif
void touchKeyWin(void);
void clearKeyWin(void);
void deleteKeyWin(void);
int initKeyWin(void);

/* camp_cui_main.c */
int main ( int argc , char* argv []);
int initScreen ( void );
void refreshScreen ( void );
void camp_cui_shutdown ( void );
int updateAndDisplayData ( void );
int checkRPCstatus( void );
void waitForServer( int msgFlag );
int updateData ( void );
int updateServerData ( void );
void updateDisplay ( void );
void uncoverWindows ( void );

/* camp_cui_menu.c */
void boxMenuWin ( WINDOW *win , u_int height , u_int width );
int initMenuDisplay ( void );
void initMenu ( void );
void deleteMenuDisplay ( void );
void touchMenuDisplay ( void );
void refreshMenuDisplay ( void );
void showMainMenu ( void );
int showSubMenu ( MENU_ENTRY *pCurrMenuEntry );
void removeSubMenu ( void );
void showMenuItem ( WINDOW *win , MENU_ITEM *pMenuItem );
void hilightMenuItem ( WINDOW *win , MENU_ITEM *pMenuItem );
void unhilightMenuItem ( WINDOW *win , MENU_ITEM *pMenuItem );
void getMenuSelection ( void );

/* camp_cui_menucb.c */
int menuFileQuit ( void );
int menuFileLoadNew ( void );
int menuFileLoad ( void );
int menuFileSave ( void );
int menuEdit ( u_short key );
int menuViewNavigate ( void );
int menuViewRedraw ( void );
int menuViewRate ( void );
int menuInsChoose ( void );
int menuInsAdd ( void );
int menuInsDel ( void );
int menuInsRecreate ( void );
int menuInsLock ( void );
int menuInsLine ( void );
int menuInsBecomeIni ( void );
int menuInsLoadIni ( void );
int insLoad ( int flag, const char* title );
int menuInsSave ( void );
int menuInsRedirect ( void );
int insSave ( int flag, const char* title );
int menuInsZero ( void );
int menuInsIf ( void );
int menuVarShow ( void );
int menuVarSet ( void );
int menuVarTitle ( void );
int menuVarUnits ( void );
int menuLnkVarSet ( void );
int menuVarRead ( void );
int menuVarPoll ( void );
int menuVarAlarm ( void );
int menuVarLog ( void );
int menuVarZero ( void );
int menuVarTol ( void );
int cbMainMenuHelpAbout( void );
int cbMainMenuHelpManual( void );
int cbMainMenuHelpCamp( void );
int doMainMenu( void );
int cbMainMenuConfig( void );
int cbMainMenuBell( void );
int cbMainMenuOpt( void );
int cbMainMenuHelp( void );
int cbMainMenuQuit( void );

/* camp_cui_menulist.c */
MENU_ENTRY *addMenuEntry ( MENU_ENTRY *pMenuEntry_curr , MENU_ENTRY *pLastEntry , MENU_ITEM *pMenuItem );
MENU_ENTRY *removeMenuEntry ( MENU_ENTRY *pMenuList , const char* menuItem );
void freeMenuEntry ( MENU_ENTRY *pMenuEntry );
void freeMenuList ( MENU_ENTRY *pMenuList );
MENU_ENTRY *getMenuEntry ( MENU_ENTRY *pMenuList , char hotKey );

/* camp_cui_msg.c */
int initMessageDisplay ( void );
void deleteMessageDisplay ( void );
void touchMessageDisplay ( void );
void refreshMessageDisplay ( void );
void clearMessage ( void );
void printMessage ( const char* message );

/* camp_cui_msgwin.c */
void refreshMessage(void);
void touchMessage(void);
void confirmMessage(void);
void addMessage(const char *msg);
void deleteMessage(void);
void startMessage(const char *title,const char *msg);

/* camp_cui_status.c */
void printStatus(void);
void clearStatus(void);
#if !defined(VMS)
void uncoverStatusDisplay(void);
#endif
void refreshStatusDisplay(void);
void touchStatusDisplay(void);
void deleteStatusDisplay(void);
int initStatusDisplay(void);

/* camp_cui_varinfo.c */
void displayVarInfoWin(void);
#if !defined(VMS)
void uncoverVarInfoWin(void);
#endif
void refreshVarInfoWin(void);
void touchVarInfoWin(void);
void clearVarInfoWin(void);
void deleteVarInfoWin(void);
int initVarInfoWin(void);

#endif /* _CAMP_CUI_H_ */

