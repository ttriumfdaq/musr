/*
 *  Name:       camp_cui_alert.c
 *
 *  Purpose:    Manage the "alert" window at the bottom of the CAMP CUI screen.
 *              This window shows up when alerts have been triggered; it covers
 *              the keywin window and displays the camp variables that are in 
 *              an alert condition.
 *
 *  Called by:  camp_cui_main.c
 *
 *  $Revision: 1.5 $
 *
 *  Revision history:
 *  $Log: camp_cui_alert.c,v $
 *  Revision 1.5  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.2  2004/01/28 03:09:30  asnd
 *  Make alert's BEEP more periodic.
 *
 *              Dec 2001       Donald Arseneau       Created
 */

#include <stdio.h>
#include <curses.h>
#include <time.h>
#include <ctype.h> // isspace
#include "timeval.h"
#include "camp_cui.h"

static WINDOW* alertWin = NULL;

//extern long poll_interval;


bool_t 
anyAlerts( void )
{
  ALARM_ACT *pAlarmAct;

  if( pSys ) 
  {
      for( pAlarmAct = pSys->pAlarmActs; 
           pAlarmAct != NULL; 
           pAlarmAct = pAlarmAct->pNext )
        {
          if( pAlarmAct->status & CAMP_ALARM_ON ) return( TRUE );
        }
  }
  return( FALSE );
}


int
initAlertWin( void )
{
    alertWin = newwin( ALERTWIN_H, ALERTWIN_W, 
                         ALERTWIN_Y, ALERTWIN_X ); 
    if( alertWin == NULL )
    {
        return( CAMP_FAILURE );
    }

    leaveok( alertWin, FALSE );
    scrollok( alertWin, FALSE );
    keypad( alertWin, TRUE );
    immedok( alertWin, FALSE );
    clearok( alertWin, FALSE );
    wtimeout( alertWin, -1);

    return( CAMP_SUCCESS );
}


void 
deleteAlertWin( void )
{
    if( alertWin != (WINDOW*)NULL ) 
    {
        delwin( alertWin );
        alertWin = (WINDOW*)NULL;
    }
}


void 
clearAlertWin( void )
{
    if( alertWin != (WINDOW*)NULL ) werase( alertWin );
}


void 
touchAlertWin( void )
{
    if( alertWin != (WINDOW*)NULL ) touchwin( alertWin );
}


#ifndef VMS
void 
uncoverAlertWin( void )
{
    if( alertWin != (WINDOW*)NULL ) 
    {
      touchwin( alertWin );
      wnoutrefresh( alertWin );
    }
}
#endif

void 
refreshAlertWin( void )
{
    if( alertWin != (WINDOW*)NULL ) 
    {
      /* clearok( alertWin, FALSE ); */
      wrefresh( alertWin );
    }
}


bool_t
displayAlertWin( void )
{
    int y, x, w, width, i;
    char *title = " Alerts ";
    char *alertVarPaths = NULL;
    char *p1, *p2;
    int camp_status;
    extern bool_t timedUpdate;

    /*
     *  We could get the list of variables with alerts going (alert)
     *  from our local copy of the variables, but then we would have
     *  to update our whole copy.  It will probably be more efficient
     *  to retrieve the list of alerted variables from the server.
     */

    if( !anyAlerts() ) goto none;
    camp_status = campSrv_cmd( "sysGetAlertVars" ); 
    if( _failure( camp_status ) ) goto none;
    alertVarPaths = camp_getMsg();
    if( alertVarPaths == NULL ) goto none;
    if( *alertVarPaths == '\0' ) goto none;

    if( alertWin == (WINDOW*)NULL ) 
    {
        initAlertWin();
    }
    clearAlertWin();
    wattron( alertWin, A_REVERSE );

    boxWin( alertWin, ALERTWIN_H, ALERTWIN_W );

    y = 0;
    x = _max( ( ( ALERTWIN_W - strlen( title ) )/2 ), 1 );
    mvwaddstr( alertWin, y, x, title );

    /*
     *  Avoid splitting paths between lines
     */
    width = ALERTWIN_W - 2;
    p1 = alertVarPaths;
    for( i = 1; i < ALERTWIN_H-1; i++ )
    {
        y++;
        while( isspace( *p1 ) ) p1++;
        w = strlen( p1 );
        if( w > width )
        {
            for( p2=p1+width-1; (p2>p1) && !isspace(*p2); p2-- ) ;
            w = (p2==p1) ? width : p2-p1;
        }
        wmove( alertWin, y, 1 );
	wprintw( alertWin, "%.*s", w, p1 );
        p1 += w;
        for( ; w<width ; w++)
        {
            wprintw( alertWin, " " );
        }
    }

    refreshAlertWin();

    /*
     * The timedUpdate parameter indicates whether this display was triggered
     * by a periodic update (or if it is an initial display of an alert).  We
     * use it to suppress the alarm beeps while navigating among instruments.
     */
    if( timedUpdate )
    {
        BEEP();
        timedUpdate = FALSE; 
    }

    return( TRUE );

 none:
    deleteAlertWin();
    return( FALSE );
}

/*
 */
