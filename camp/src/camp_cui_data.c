/*
 *  $Id: camp_cui_data.c,v 1.13 2018/06/15 04:14:54 asnd Exp $
 *
 *  $Revision: 1.13 $
 *
 *  Purpose:    Routines to display and manage the CAMP CUI data window
 *              (the window on the left with the "Path" title) (dataWin),
 *              also routines to show the "View parameters" window (varWin),
 *              and the routine varPicker which dispatches all user key input.
 *
 *
 *  $Log: camp_cui_data.c,v $
 *  Revision 1.13  2018/06/15 04:14:54  asnd
 *  Hide first var if "-d off", unless none can be displayed
 *
 *  Revision 1.12  2015/09/26 01:56:33  asnd
 *  Improve alias name/message display
 *
 *  Revision 1.11  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.8  2009/08/19 02:25:52  asnd
 *  Ensure all windows flagged with NULL when deleted
 *
 *  Revision 1.7  2009/04/08 01:57:01  asnd
 *  Symbolic names for clarity (enum didn't compile)
 *
 *  Revision 1.6  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.5  2004/05/01 01:21:01  asnd
 *  Better display of wide file names
 *
 *  Revision 1.4  2004/01/28 03:13:26  asnd
 *  Add VME interface type. Make alert's BEEP more periodic.
 *
 *  Revision 1.3  2001/01/15 05:52:18  asnd
 *  Add case: KEY_RESIZE to update window (This change got lost in December)
 *
 *  Revision 1.2  2000/12/22 23:44:51  David.Morris
 *  Added support for INDPAK type in parameters display
 *  Fixed probable bug in CAMAC parameters display section
 *
 *
 *  Revision history:
 *  00-Oct-1999   DA    Fix (non-) display of invisible variables (_ATTR_SHOW)
 *  08-Feb-2000   DA    Use any "alias: " message instead of variable name;
 *                      Use several GetTrueP()
 */

#include <stdio.h>
#include <curses.h>
#include "timeval.h"
#include "camp_cui.h"

#define DATAWIN_Y_OFFSET     1

#define VARWIN_Y             (FIRST_LINE+1)
#define VARWIN_X             0
#define VARWIN_H             (SCREEN_H-VARWIN_Y-MESSAGEWIN_H)
#define VARWIN_W             (SCREEN_W-VARWIN_X)

#define DATA_TITLE_X         (DATAWIN_X+1)
#define DATA_TITLE_TEXT      "Variable name  "

#define DATA_TYPE_X          (DATA_TITLE_X+16)
#define DATA_TYPE_TEXT       "Type      "

#define DATA_FLAG_X          (DATA_TYPE_X+11)
#define DATA_FLAG_TEXT       "Status"

#define FAC_LINE_FLAG_X      DATA_FLAG_X
#define FAC_LINE_FLAG_TEXT   "O"
#define FAC_LOCK_FLAG_X      (DATA_FLAG_X+1)
#define FAC_LOCK_FLAG_TEXT   "L"

#define DATA_SET_FLAG_X      DATA_FLAG_X
#define DATA_SET_FLAG_TEXT   "S"
#define DATA_READ_FLAG_X     (DATA_FLAG_X+1)
#define DATA_READ_FLAG_TEXT  "R"
#define DATA_POLL_FLAG_X     (DATA_FLAG_X+2)
#define DATA_POLL_FLAG_TEXT  "P"
#define DATA_ALARM_FLAG_X    (DATA_FLAG_X+3)
#define DATA_ALARM_FLAG_TEXT "A"
#define DATA_LOG_FLAG_X      (DATA_FLAG_X+4)
#define DATA_LOG_FLAG_TEXT   "L"

#define DATA_VALUE_X         (DATA_TITLE_X+16)  /* (DATA_FLAG_X+7)*/
#define DATA_VALUE_TEXT      "Value           "

#define DATA_MSG_X           (DATA_VALUE_X+17)
#define DATA_MSG_TEXT        "Message                    "


static WINDOW* dataWin = NULL;
static WINDOW* varWin = NULL;
static char varPath[LEN_PATH+1] = "";
//int curr_line = 0;

extern char currVarPath[LEN_PATH+1];

static void varPicker_show( const char* path );


int
initDataDisplay( void )
{
    dataWin = newwin( DATAWIN_H, DATAWIN_W, 
                      DATAWIN_Y, DATAWIN_X ); 
    if( dataWin == NULL )
    {
        return( CAMP_FAILURE );
    }

    leaveok( dataWin, FALSE );
    scrollok( dataWin, FALSE );
    keypad( dataWin, TRUE );
    immedok( dataWin, FALSE );
    clearok( dataWin, FALSE );
    wtimeout( dataWin, -1);

    return( CAMP_SUCCESS );
}


void 
deleteDataDisplay( void )
{
    if( dataWin != (WINDOW*)NULL ) {
      delwin( dataWin );
      dataWin = (WINDOW*)NULL;
    }
}


static void 
clearDataDisplay( void )
{
    if( dataWin != (WINDOW*)NULL ) werase( dataWin );
}


void 
touchDataDisplay( void )
{
    if( dataWin != (WINDOW*)NULL ) touchwin( dataWin );
}


void 
refreshDataDisplay( void )
{
    if( dataWin != (WINDOW*)NULL ) 
    {
        wrefresh( dataWin );
    }
}

#ifndef VMS
void 
uncoverDataDisplay( void )
{
    if( dataWin != (WINDOW*)NULL ) 
    {
      touchwin( dataWin );
      wnoutrefresh( dataWin );
    }
}
#endif

char*
getTypeStr( CAMP_VAR_TYPE varType )
{
        switch( varType )
        {
          case CAMP_VAR_TYPE_INT:
            return( "integer" );
          case CAMP_VAR_TYPE_FLOAT:
            return( "float" );
          case CAMP_VAR_TYPE_SELECTION:
            return( "selection" );
          case CAMP_VAR_TYPE_STRING:
            return( "string" );
          case CAMP_VAR_TYPE_ARRAY:
            return( "array" );
          case CAMP_VAR_TYPE_STRUCTURE:
            return( "structure" );
          case CAMP_VAR_TYPE_INSTRUMENT:
            return( "instrument" );
          case CAMP_VAR_TYPE_LINK:
            return( "link" );
	default:
	    break;
        }

	return( "" );
}


void
varGetValStr( CAMP_VAR* pVar, char* str, size_t str_size )
{
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_SELECTION:
        camp_varSelGetLabel( pVar->core.path, str, str_size );
        break;
      case CAMP_VAR_TYPE_STRING:
	  camp_strncpy( str, str_size, pVar->spec.CAMP_VAR_SPEC_u.pStr->val ); // strcpy( str, pVar->spec.CAMP_VAR_SPEC_u.pStr->val );
        break;
      case CAMP_VAR_TYPE_ARRAY:
        break;
      case CAMP_VAR_TYPE_NUMERIC:
        varNumGetValStr( pVar, str, str_size );
        break;
      default:
        break;
    }
}

/*
 *  Display the list of camp variables and their values on the left 
 *  half of the screen.
 */
void 
displayData( void )
{
    u_int i;
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_head;
    CAMP_VAR* pLVar;
    char* p;
    u_int x, y;
    char str[LEN_STR+1];
    char buf[LEN_BUF+1];

    clearDataDisplay();

    boxWin( dataWin, DATAWIN_H, DATAWIN_W );
    wattron( dataWin, A_BOLD );
    camp_snprintf( buf, sizeof( buf ), "Path: %s", currViewPath );
    x = _max( ((DATAWIN_W-strlen( buf ))/2), 1 );
    mvwaddstr( dataWin, 0, x, buf );
    wattroff( dataWin, A_BOLD );
    y = DATAWIN_Y_OFFSET;

    pVar_head = camp_varGetPHead_clnt( currVarPath );
    if( pVar_head == NULL )
    {
        refreshDataDisplay();
        return;
    }

    for( pVar = pVar_head; pVar != NULL; pVar = pVar->pNext )
    {
        pLVar = pVar;
        if( !( pVar->core.status & CAMP_VAR_ATTR_SHOW ) ) continue;

        /*
         *  Display the ident
         */
        varPicker_show( pVar->core.path );

        /*
         *  Display the data type
         */
        switch( pVar->core.varType )
        {
          case CAMP_VAR_TYPE_INSTRUMENT:
            /*
             *  Display status
             */
            if( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL )
            {
                camp_strncpy( buf, sizeof( buf ), "offline" ); // strcpy( buf, "offline" );
            }
            else if( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & CAMP_IF_ONLINE )
            {
                camp_strncpy( buf, sizeof( buf ), "online" ); // strcpy( buf, "online" );
            }
            else
            {
                camp_strncpy( buf, sizeof( buf ), "offline" ); // strcpy( buf, "offline" );
            }
/*
            if( pVar->core.status & CAMP_INS_ATTR_LOCKED )
            {
                camp_strncat( buf, LEN_BUF+1, ", locked" ); // strcat( buf, ", locked" );
            }
            mvwaddstr( dataWin, y, DATA_FLAG_X, buf );
            mvwaddstr( dataWin, y, DATA_VALUE_X, buf );
*/
            wmove( dataWin, y, DATA_VALUE_X );
            wprintw( dataWin, "%*s", 17, buf );

            break;

          case CAMP_VAR_TYPE_STRUCTURE:
            break;

          case CAMP_VAR_TYPE_LINK:
            if( pLVar->spec.CAMP_VAR_SPEC_u.pLnk == NULL ) break;
	    if( pLVar->spec.CAMP_VAR_SPEC_u.pLnk->path[0] == '\0' ) break;
	    pLVar = camp_varGetp_clnt( pLVar->spec.CAMP_VAR_SPEC_u.pLnk->path );
	    if( pLVar == NULL ) break;

          default:

            /*
             *  Display flags ... Actually, don't!
             */
/*
            if( pLVar->core.status & CAMP_VAR_ATTR_SET )
            {
                mvwaddstr( dataWin, y, DATA_SET_FLAG_X, 
                           DATA_SET_FLAG_TEXT );
            }

            if( pLVar->core.status & CAMP_VAR_ATTR_READ )
            {
                mvwaddstr( dataWin, y, DATA_READ_FLAG_X, 
                           DATA_READ_FLAG_TEXT );
            }

            if( pLVar->core.status & CAMP_VAR_ATTR_POLL )
            {
                mvwaddstr( dataWin, y, DATA_POLL_FLAG_X, 
                    DATA_POLL_FLAG_TEXT );
            }

            if( pLVar->core.status & CAMP_VAR_ATTR_ALARM )
            {
                mvwaddstr( dataWin, y, DATA_ALARM_FLAG_X, 
                    DATA_ALARM_FLAG_TEXT );
            }

            if( pLVar->core.status & CAMP_VAR_ATTR_LOG )
            {
                mvwaddstr( dataWin, y, DATA_LOG_FLAG_X, 
                    DATA_LOG_FLAG_TEXT );
            }
*/
            /*
             *  Display value
             */
            if( pLVar->core.status & CAMP_VAR_ATTR_IS_SET )
            {
                varGetValStr( pLVar, str, LEN_STR+1 );
/*
                str[DATA_MSG_X-DATA_VALUE_X-1] = '\0';
*/
                if( pLVar->core.status & CAMP_VAR_ATTR_ALERT )
                {
                    wattron( dataWin, A_BLINK );
                }

                wmove( dataWin, y, DATA_VALUE_X );
                wprintw( dataWin, "%*s", 17, str );

                if( pLVar->core.status & CAMP_VAR_ATTR_ALERT )
                {
                    wattroff( dataWin, A_BLINK );
                    /*   BEEP();   Done by alert window now */ 
                }
            }

            break;
        }        
/*
        mvwaddstr( dataWin, y, DATA_MSG_X, pVar->core.statusMsg );
*/
        y++;
    }
    refreshDataDisplay();
}


int
initVarWin( void )
{
    varWin = newwin( VARWIN_H, VARWIN_W, VARWIN_Y, VARWIN_X ); 
    if( varWin == (WINDOW*)NULL )
    {
        return( CAMP_FAILURE );
    }

    leaveok( varWin, FALSE );
    scrollok( varWin, FALSE );
    keypad( varWin, TRUE );
    immedok( varWin, FALSE );
    clearok( varWin, FALSE );
    wtimeout( varWin, -1);

    return( CAMP_SUCCESS );
}

void
deleteVarWin( void )
{
    if( varWin != (WINDOW*)NULL )
    {
        wclear( varWin );
        delwin( varWin );
        varWin = (WINDOW*)NULL;
    }
    return;
}

void 
touchVarWin( void )
{
    if( varWin != (WINDOW*)NULL ) touchwin( varWin );
}


void 
refreshVarWin( void )
{
    if( varWin != (WINDOW*)NULL )
    {
        wrefresh( varWin );
    }
}


#ifndef VMS
void 
uncoverVarWin( void )
{
    if( varWin != (WINDOW*)NULL ) 
    {
      touchwin( varWin );
      wnoutrefresh( varWin );
    }
}
#endif


/*
 *  Display the single-variable information page.
 */
void
displayVar( void )
{
    static u_long efn = 0;
    int ypos, xpos, ypos_last;
    CAMP_VAR* pVar;
    CAMP_VAR_CORE* pCore;
    CAMP_VAR_SPEC* pSpec;
    char str1[LEN_STR+1];
    CAMP_NUMERIC* pNum;
    CAMP_SELECTION* pSel;
    CAMP_SELECTION_ITEM* pItem;
    CAMP_STRING* pStr;
    CAMP_ARRAY* pArr;
    CAMP_STRUCTURE* pStc;
    CAMP_INSTRUMENT* pIns;
    CAMP_LINK* pLnk;
    char str[LEN_STR+1];
    char* p;
    int i;

    if( varWin == (WINDOW*)NULL ) return;

    /* Was:    wclear( varWin ); */
    werase( varWin );

#define  QUIT_TEXT  "Press <Return> to continue"
    wmove( varWin, VARWIN_H-1, (VARWIN_W-strlen(QUIT_TEXT))/2 );
    wattron( varWin, A_REVERSE );
    wprintw( varWin, QUIT_TEXT );
    wattroff( varWin, A_REVERSE );
    pVar = camp_varGetp_clnt( varPath );
    if( pVar == NULL )
    {
        refreshVarWin();
        return;
    }

    pCore = &pVar->core;
    pSpec = &pVar->spec;

#define NONE_TEXT      "<undefined>"
#define _nonull(s)     ((s==NULL||strlen(s)==0)?NONE_TEXT:s)
#define COL_OFFSET     2
#define COL_GAP        4
#define COL1           0
#define COL2           ((VARWIN_W+COL_GAP)/2)
#define COL1_W         ((VARWIN_W-COL_GAP)/2)
#define COL2_W         ((VARWIN_W-COL_GAP)/2)
#define SEL_XPOS       (15)

    ypos = 0;
    xpos = 0;

    wattron( varWin, A_BOLD );

    mvwaddstr( varWin, ypos++, COL1, "Common parameters:" );

    wattroff( varWin, A_BOLD );
    wmove( varWin, ypos, COL1 );
    wprintw( varWin, "%*s", COL1_W, _nonull( pCore->ident ) );
    mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Ident: " );

    wmove( varWin, ypos, COL2 );

    camp_varGetTypeStr( pCore->path, str, sizeof( str ) );
    wprintw( varWin, "%*s", COL2_W, str );
    mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Type: " );

    wmove( varWin, ypos, COL1 );
    wprintw( varWin, "%*s", COL1_W, _nonull( pCore->path ) );
    mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Path: " );

    wmove( varWin, ypos, COL2 );
    wprintw( varWin, "%*s", COL2_W, _nonull( pCore->title ) );
    mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Title: " );

    wmove( varWin, ypos, COL1 );
    if( pCore->status & CAMP_VAR_ATTR_IS_SET )
         wprintw( varWin, "%*s", COL1_W, asctimeval( &pCore->timeLastSet ) );
    else wprintw( varWin, "%*s", COL1_W, NONE_TEXT );
    mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Last set: " );

    wmove( varWin, ypos, COL2 );
    wprintw( varWin, "%*s", COL2_W, _nonull( pCore->statusMsg ) );
    mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Message: " );

    camp_snprintf( str, sizeof( str ), "%.1fs", pCore->pollInterval );
    wmove( varWin, ypos, COL1 );
    wprintw( varWin, "%*s", COL1_W, str );
    mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Poll interval: " );

    wmove( varWin, ypos, COL1 );
    wprintw( varWin, "%*s", COL1_W, _nonull( pCore->logAction ) );
    mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Log action: " );

    wmove( varWin, ypos, COL2 );
    wprintw( varWin, "%*s", COL2_W, _nonull( pCore->alarmAction ) );
    mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Alarm action: " );

    wmove( varWin, ypos, COL1 );
    mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Help:      \t" );
    waddstr( varWin, _nonull( pCore->help ) );
    getyx( varWin, ypos, xpos );
    ypos++;

#define fmt        "%s "

    mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Attributes:\t" );
    if( pCore->attributes & CAMP_VAR_ATTR_SHOW ) wprintw( varWin, fmt, "  show" );
    if( pCore->attributes & CAMP_VAR_ATTR_SET ) wprintw( varWin, fmt, "  set" );
    if( pCore->attributes & CAMP_VAR_ATTR_READ ) wprintw( varWin, fmt, "  read" );
    if( pCore->attributes & CAMP_VAR_ATTR_POLL ) wprintw( varWin, fmt, "  poll" );
    if( pCore->attributes & CAMP_VAR_ATTR_ALARM ) wprintw( varWin, fmt, "  alarm" );
    if( pCore->attributes & CAMP_VAR_ATTR_LOG ) wprintw( varWin, fmt, "  log" );

    mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Status:    \t" );
    if( pCore->status & CAMP_VAR_ATTR_SHOW ) wprintw( varWin, fmt, "  show" );
    else if( pCore->attributes & CAMP_VAR_ATTR_SHOW ) wprintw( varWin, fmt, "noshow" );
    if( pCore->status & CAMP_VAR_ATTR_SET ) wprintw( varWin, fmt, "  set" );
    else if( pCore->attributes & CAMP_VAR_ATTR_SET ) wprintw( varWin, fmt, "noset" );
    if( pCore->status & CAMP_VAR_ATTR_READ ) wprintw( varWin, fmt, "  read" );
    else if( pCore->attributes & CAMP_VAR_ATTR_READ ) wprintw( varWin, fmt, "noread" );
    if( pCore->status & CAMP_VAR_ATTR_POLL ) wprintw( varWin, fmt, "  poll" );
    else if( pCore->attributes & CAMP_VAR_ATTR_POLL ) wprintw( varWin, fmt, "nopoll" );
    if( pCore->status & CAMP_VAR_ATTR_ALARM ) wprintw( varWin, fmt, "  alarm" );
    else if( pCore->attributes & CAMP_VAR_ATTR_ALARM ) wprintw( varWin, fmt, "noalarm" );
    if( pCore->status & CAMP_VAR_ATTR_LOG ) wprintw( varWin, fmt, "  log" );
    else if( pCore->attributes & CAMP_VAR_ATTR_LOG ) wprintw( varWin, fmt, "nolog" );
    if( pCore->status & CAMP_VAR_ATTR_ALERT ) 
    {
        wattron( varWin, ALARM_ATTR );
        wprintw( varWin, fmt, "alert" );
        wattroff( varWin, ALARM_ATTR );
    }
    if( pCore->status & CAMP_INS_ATTR_LOCKED ) wprintw( varWin, fmt, "lock" );

    ypos++;

    switch( pCore->varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        ypos_last = ypos;

        wattron( varWin, A_BOLD );
        mvwaddstr( varWin, ypos++, COL1, "Numeric parameters:" );
        wattroff( varWin, A_BOLD );

        pNum = pSpec->CAMP_VAR_SPEC_u.pNum;

        varNumGetValStr( pVar, str, sizeof( str ) );
        wmove( varWin, ypos, COL1 );
        if( pCore->status & CAMP_VAR_ATTR_IS_SET ) 
             wprintw( varWin, "%*s", COL1_W, str );
        else wprintw( varWin, "%*s", COL1_W, NONE_TEXT );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Value: " );

        wmove( varWin, ypos, COL1 );

        switch( pNum->tolType )
        {
            case 0:
                if( pNum->tol >= 0.0 ) camp_snprintf( str, sizeof( str ), "+-%f", pNum->tol );
                else camp_strncpy( str, sizeof( str ), NONE_TEXT ); // strcpy( str, NONE_TEXT );
                break;
            case 1:
                if( pNum->tol >= 0.0 ) camp_snprintf( str, sizeof( str ), "%.0f%%", pNum->tol );
                else camp_strncpy( str, sizeof( str ), NONE_TEXT ); // strcpy( str, NONE_TEXT );
                break;
            default:
                camp_strncpy( str, sizeof( str ), NONE_TEXT ); // strcpy( str, NONE_TEXT );
                break;
        }
        wprintw( varWin, "%*s", COL1_W, str );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Tolerance: " );

        wmove( varWin, ypos, COL1 );
        if( !streq( pNum->units, "" ) ) wprintw( varWin, "%*s", COL1_W, pNum->units );
        else wprintw( varWin, "%*s", COL1_W, NONE_TEXT );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Units: " );

        if( pNum->timeStarted.tv_sec > 0 )
        {
            double mean, stddev, skew;

            ypos = ypos_last;

            wattron( varWin, A_BOLD );
            mvwaddstr( varWin, ypos++, COL2, "Statistics:" );
            wattroff( varWin, A_BOLD );

            wmove( varWin, ypos, COL2 );
            wprintw( varWin, "%*s", COL2_W, asctimeval( &pNum->timeStarted ) );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Started: " );

            wmove( varWin, ypos, COL2 );
            wprintw( varWin, "%*ld", COL2_W, pNum->num );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Num: " );

            wmove( varWin, ypos, COL2 );
            numGetValStr( pCore->varType, pNum->offset, str, sizeof( str ) );
            wprintw( varWin, "%*s", COL2_W, str );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Offset: " );

            wmove( varWin, ypos, COL2 );
            numGetValStr( pCore->varType, pNum->low, str, sizeof( str ) );
            wprintw( varWin, "%*s", COL2_W, str );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Low: " );

            wmove( varWin, ypos, COL2 );
            numGetValStr( pCore->varType, pNum->hi, str, sizeof( str ) );
            wprintw( varWin, "%*s", COL2_W, str );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "High: " );

            camp_varNumCalcStats( varPath, &mean, &stddev, &skew );

            wmove( varWin, ypos, COL2 );
            numGetValStr( pCore->varType, mean, str, sizeof( str ) );
            wprintw( varWin, "%*s", COL2_W, str );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Mean: " );

            wmove( varWin, ypos, COL2 );
            numGetValStr( pCore->varType, stddev, str, sizeof( str ) );
            wprintw( varWin, "%*s", COL2_W, str );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "StdDev: " );

            wmove( varWin, ypos, COL2 );
            numGetValStr( pCore->varType, skew, str, sizeof( str ) );
            wprintw( varWin, "%*s", COL2_W, str );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Skew: " );
        }
        break;

      case CAMP_VAR_TYPE_SELECTION:
        wattron( varWin, A_BOLD );
        mvwaddstr( varWin, ypos++, COL1, "Selection parameters:" );
        wattroff( varWin, A_BOLD );

        pSel = pSpec->CAMP_VAR_SPEC_u.pSel;

        wmove( varWin, ypos, COL1 );
        camp_varSelGetLabel( pVar->core.path, str, sizeof( str ) );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Value:         " );
        waddstr( varWin, str );

        mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Selections:   " );
        str[0] = '\0';
        for( pItem = pSel->pItems; pItem != NULL; pItem = pItem->pNext )
        {
	    if( ( strlen(str) + 2 + strlen(pItem->label) > VARWIN_W - 3 - SEL_XPOS )
		&& ( str[0] != '\0' ) )
	    {
	        mvwaddstr( varWin, ypos++, SEL_XPOS, str );
		str[0] = '\0';
	    }
            camp_strncat( str, sizeof( str ), " " ); // strcat( str, "  " );
            camp_strncat( str, sizeof( str ), pItem->label ); // strcat( str, pItem->label );
        }
        mvwaddstr( varWin, ypos++, SEL_XPOS, str );

        break;

      case CAMP_VAR_TYPE_STRING:
        wattron( varWin, A_BOLD );
        mvwaddstr( varWin, ypos++, COL1, "String parameters:" );
        wattroff( varWin, A_BOLD );

        pStr = pSpec->CAMP_VAR_SPEC_u.pStr;

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*s", COL1_W, pStr->val );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Value: " );

        break;

      case CAMP_VAR_TYPE_ARRAY:
        wattron( varWin, A_BOLD );
        mvwaddstr( varWin, ypos++, COL1, "Array parameters:" );
        wattroff( varWin, A_BOLD );

        pArr = pSpec->CAMP_VAR_SPEC_u.pArr;

        switch( pArr->varType )
        {
	  case CAMP_VAR_TYPE_INT: 
	      camp_strncpy( str, sizeof( str ), "integer" ); // strcpy( str, "integer" ); 
	      break;
	  case CAMP_VAR_TYPE_FLOAT: 
	      camp_strncpy( str, sizeof( str ), "float" ); // strcpy( str, "float" ); 
	      break;
	  case CAMP_VAR_TYPE_STRING: 
	      camp_strncpy( str, sizeof( str ), "string" ); // strcpy( str, "string" ); 
	      break;
	default:
	    break;
	}
        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*s", COL1_W, str );
        mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Type: " );

        wmove( varWin, ypos, COL2 );
        wprintw( varWin, "%*d", COL2_W, pArr->elemSize );
        mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Size (bytes): " );

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*d", COL1_W, pArr->dim );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Dim: " );

        for( i = 0; i < pArr->dim; i++ )
        {
            wmove( varWin, ypos, COL1 );
            wprintw( varWin, "%*d", COL1_W, pArr->dimSize[i] );
            wmove( varWin, ypos++, COL1+COL_OFFSET );
            wprintw( varWin, "DimSize[%d]: ", pArr->dimSize[i] );
        }
        break;

      case CAMP_VAR_TYPE_STRUCTURE:
        wattron( varWin, A_BOLD );
        mvwaddstr( varWin, ypos++, COL1, "Structure parameters:" );
        wattroff( varWin, A_BOLD );

        pStc = pSpec->CAMP_VAR_SPEC_u.pStc;

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*d", COL1_W, pStc->dataItems_len );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "No. of members: " );

        break;

      case CAMP_VAR_TYPE_INSTRUMENT:
        ypos_last = ypos;

        wattron( varWin, A_BOLD );
        mvwaddstr( varWin, ypos++, COL1, "Instrument parameters:" );
        wattroff( varWin, A_BOLD );

        pIns = pSpec->CAMP_VAR_SPEC_u.pIns;

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*s", COL1_W, _nonull( pIns->typeIdent ) );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Type: " );

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*ld", COL1_W, pIns->typeInstance );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Instance: " );

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*s", COL1_W, _nonull( pIns->lockHost ) );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Lock host: " );

        wmove( varWin, ypos, COL1 );
        if( pIns->lockPID > 0 ) camp_snprintf( str, sizeof( str ), "%08X", pIns->lockPID );
        else camp_strncpy( str, sizeof( str ), NONE_TEXT ); // strcpy( str, NONE_TEXT );
        wprintw( varWin, "%*s", COL1_W, str );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Lock PID: " );

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*s", COL1_W, _nonull( pIns->lockOs ) );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "Lock OS: " );

        if( camp_strnlen( _nonull( pIns->defFile ), COL1_W ) > COL1_W - 10 - COL_OFFSET)
          {
            mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "DEF file: " );
            wmove( varWin, ypos++, COL1 );
            wprintw( varWin, "%*s", COL1_W, _nonull( pIns->defFile ) );
          }
        else
          {
            wmove( varWin, ypos, COL1 );
            wprintw( varWin, "%*s", COL1_W, _nonull( pIns->defFile ) );
            mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "DEF file: " );
          }

        if( camp_strnlen( _nonull( pIns->iniFile ), COL1_W ) > COL1_W - 10 - COL_OFFSET)
          {
            mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "INI file: " );
            wmove( varWin, ypos++, COL1 );
            wprintw( varWin, "%*s", COL1_W, _nonull( pIns->iniFile ) );
          }
        else
          {
            wmove( varWin, ypos, COL1 );
            wprintw( varWin, "%*s", COL1_W, _nonull( pIns->iniFile ) );
            mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "INI file: " );
          }

        wmove( varWin, ypos, COL1 );
        wprintw( varWin, "%*d", COL1_W, pIns->dataItems_len );
        mvwaddstr( varWin, ypos++, COL1+COL_OFFSET, "No. of members: " );

        ypos = ypos_last;

        if( pIns->pIF != NULL )
        {
            wmove( varWin, ypos, COL2 );
            wprintw( varWin, "%*s", COL2_W, _nonull( pIns->pIF->typeIdent ) );
            wattron( varWin, A_BOLD );
            mvwaddstr( varWin, ypos++, COL2, "Interface:" );
            wattroff( varWin, A_BOLD );

            wmove( varWin, ypos, COL2 );
            if( pIns->pIF->status & CAMP_IF_ONLINE ) camp_strncpy( str, sizeof( str ), "online" ); // strcpy( str, "online" );
            else camp_strncpy( str, sizeof( str ), "offline" ); // strcpy( str, "offline" );
            wprintw( varWin, "%*s", COL2_W, str );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Status: " );

	    // 20140408  TW  accessDelay and timeout
            wmove( varWin, ypos, COL2 );
            wprintw( varWin, "%*.2f", COL2_W, pIns->pIF->accessDelay );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Access delay:" );

            wmove( varWin, ypos, COL2 );
            wprintw( varWin, "%*.2f", COL2_W, pIns->pIF->timeout );
            mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Timeout:" );

            switch( pIns->pIF->typeID )
            {
              case CAMP_IF_TYPE_NONE:
                break;

              case CAMP_IF_TYPE_RS232:
                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfRs232Port( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Name: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			 camp_getIfRs232Baud( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Baud: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			 camp_getIfRs232Data( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Data bits: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfRs232Parity( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Parity: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			 camp_getIfRs232Stop( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Stop bits: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfRs232ReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Read term: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfRs232WriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Write term: " );

                /* wmove( varWin, ypos, COL2 ); */
                /* wprintw( varWin, "%*d", COL2_W,  */
		/* 	 camp_getIfRs232Timeout( pIns->pIF->defn ) ); */
                /* mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Timeout: " ); */

                break;

              case CAMP_IF_TYPE_GPIB:

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			 camp_getIfGpibAddr( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Addr: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfGpibReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Read term: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfGpibWriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Write term: " );

                break;

              case CAMP_IF_TYPE_GPIB_MSCB:

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			 camp_getIfGpibAddr( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Addr: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfGpibReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Read term: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfGpibWriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Write term: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			 camp_getIfGpibMscbAddr( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "MSCB addr: " );

                break;

              case CAMP_IF_TYPE_TICS:
                break;

              case CAMP_IF_TYPE_CAMAC:

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W,
			        camp_getIfCamacB( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "B: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			        camp_getIfCamacC( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "C: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			        camp_getIfCamacN( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "N: " );

                break;

              case CAMP_IF_TYPE_INDPAK:

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W,
                    camp_getIfIndpakSlot( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Slot: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
    			    camp_getIfIndpakType( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Type: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			        camp_getIfIndpakChannel( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Channel: " );

                break;

              case CAMP_IF_TYPE_VME:

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*ld", COL2_W,
			        camp_getIfVmeBase( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Base: " );

                break;

              case CAMP_IF_TYPE_TCPIP:

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfTcpipAddr( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Address: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*d", COL2_W, 
			 camp_getIfTcpipPort( pIns->pIF->defn ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Port: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfTcpipReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Read term: " );

                wmove( varWin, ypos, COL2 );
                wprintw( varWin, "%*s", COL2_W, 
			 camp_getIfTcpipWriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
                mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Write term: " );

                /* wmove( varWin, ypos, COL2 ); */
                /* wprintw( varWin, "%*.1f", COL2_W,  */
		/* 	 camp_getIfTcpipTimeout( pIns->pIF->defn ) ); */
                /* mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Timeout: " ); */

                break;

            }
        }
        else
        {
            wattron( varWin, A_BOLD );
            mvwaddstr( varWin, ypos++, COL2, "Interface:" );
            wattroff( varWin, A_BOLD );
        }
        break;

      case CAMP_VAR_TYPE_LINK:
        wattron( varWin, A_BOLD );
        mvwaddstr( varWin, ypos++, COL1, "Link parameters:" );
        wattroff( varWin, A_BOLD );

        pLnk = pSpec->CAMP_VAR_SPEC_u.pLnk;

        wmove( varWin, ypos, COL1 );
        camp_varGetTypeStr( pCore->path, str, sizeof( str ) );
        wprintw( varWin, "%*s", COL1_W, str );
        mvwaddstr( varWin, ypos, COL1+COL_OFFSET, "Type: " );

        wmove( varWin, ypos, COL2 );
        wprintw( varWin, "%*s", COL2_W, _nonull( pLnk->path ) );
        mvwaddstr( varWin, ypos++, COL2+COL_OFFSET, "Path: " );

        break;
      default:
        break;
    }
/*
    boxWin( varWin, VARWIN_H, VARWIN_W );
*/
    refreshVarWin();
}

/*
 *  Display the single-variable information page, with updates, waiting
 *  for a keypress to return.
 */
void
displayVarLoop( void )
{
    short inKey;
    int camp_status;

/*
    if( !inputVar( varPath, "Show variable", CAMP_MAJOR_VAR_TYPE_MASK, 
                        CAMP_VAR_ATTR_SHOW ) )
        return;
*/
    camp_strncpy( varPath, sizeof( varPath ), currVarPath ); // strcpy( varPath, currVarPath );

    camp_status = initVarWin();
    if( _failure( camp_status ) ) return;

    currDisplay = _DispVar;
    updateDisplay();

    while( ( ( inKey = getKeyUpdate( varWin ) ) != KEY_ENTER ) && 
           ( inKey != KEY_RETURN ) )
    { 

        switch( inKey )
        {
          case KEY_SPACE:
            updateServerData();
            updateAndDisplayData();
            break;
          case KEY_CTRL_W:
          case KEY_CTRL_L:
#ifdef KEY_RESIZE
          case KEY_RESIZE:
#endif
            refreshScreen();
            break;
        }
    }

    deleteVarWin();
    currDisplay = _DispTree;
    /*
     *   DA, Redundant:
     *    updateDisplay();
     *    uncoverWindows();
     */
}


static CAMP_VAR* 
getVar_by_index( CAMP_VAR* pVar_start, int index )
{
    int i;
    CAMP_VAR* pVar;

    for( i = 0, pVar = pVar_start; 
         ( i < index ) && ( pVar != NULL );
         i++, pVar = pVar->pNext ) ;

    return( pVar );
}


static int
getVar_index( CAMP_VAR* pVar_start, CAMP_VAR* pVar_seek )
{
    int i;
    CAMP_VAR* pVar;

    for( i = 0, pVar = pVar_start; pVar != NULL; i++, pVar = pVar->pNext )
    {
        if( pVar == pVar_seek ) return( i );
    }
    return( -1 );
}


/* static CAMP_VAR* */
/* getVar_next_wrap( CAMP_VAR* pVar_begin, CAMP_VAR* pVar_curr ) */
/* { */
/*     return ( pVar_curr != NULL && pVar_curr->pNext != NULL ) ? pVar_curr->pNext : pVar_begin; */
/* } */


static int getVarListSize( CAMP_VAR* pVar_begin )
{
    CAMP_VAR* pVar;
    int size;

    for( pVar = pVar_begin, size = 0; pVar != NULL; pVar = pVar->pNext, ++size ) {}

    return size;
}


static CAMP_VAR*
getVar_next_wrap_show( CAMP_VAR* pVar_begin, CAMP_VAR* pVar_curr )
{
    CAMP_VAR* pVar;

    if( pVar_curr == NULL || pVar_begin == NULL ) return NULL;

    for( pVar = pVar_curr->pNext; ; pVar = pVar->pNext )
    {
	if( pVar == NULL ) pVar = pVar_begin; // wrap to the front

	if( pVar == pVar_curr ) return pVar_curr; // we've gone through the list - no other showable var

	if( pVar->core.status & CAMP_VAR_ATTR_SHOW ) return pVar;
    }

    return( NULL );
}


static CAMP_VAR*
getVar_prev_wrap_show( CAMP_VAR* pVar_begin, CAMP_VAR* pVar_curr )
{
    int index, index_curr, index_back, list_size;

    if( pVar_curr == NULL || pVar_begin == NULL ) return NULL;

    index_curr = getVar_index( pVar_begin, pVar_curr );

    list_size = getVarListSize( pVar_begin );

    index_back = list_size-1; // index of last item in list

    for( index = index_curr-1; ; --index )
    {
	CAMP_VAR* pVar;

	if( index < 0 ) index = index_back; // wrap to the back

	if( index == index_curr ) return pVar_curr; // we've gone through the list - no other showable var

	pVar = getVar_by_index( pVar_begin, index );

	if( pVar->core.status & CAMP_VAR_ATTR_SHOW ) return pVar;
    }

    return( NULL );
}

static CAMP_VAR* 
getVar_first_show( CAMP_VAR* pVar_begin )
{
    CAMP_VAR* pVar;

    if( pVar_begin == NULL ) return NULL;

    for( pVar = pVar_begin; ; pVar = pVar->pNext )
    {
	if( pVar == NULL ) return pVar_begin; // we've gone through the list - no showable var

	if( pVar->core.status & CAMP_VAR_ATTR_SHOW ) return pVar;
    }

    return( NULL );
}

/* Show a variable name on the left side of the left "data" window */
static void
varPicker_show( const char* path )
{
    char buf[LEN_BUF+1];
    char msg[LEN_BUF+1];
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_t;
    CAMP_VAR* pVar_head;
    bool_t flag;
    int line;

    pVar = camp_varGetTrueP_clnt( path );
    if( pVar == NULL ) return;

    pVar_head = camp_varGetPHead_clnt( path );
    if( pVar_head == NULL ) return;

    line = DATAWIN_Y_OFFSET;
    for( pVar_t = pVar_head; pVar_t != NULL; pVar_t = pVar_t->pNext )
    {
        if( pVar_t == pVar )
        {
            break;
        }

        if( pVar_t->core.status & CAMP_VAR_ATTR_SHOW )
	{
	    line++;
	}
    }

    flag = camp_pathCompare( path, currVarPath ) ? TRUE : FALSE ;

    if( flag ) 
    {
        wattron( dataWin, CURSOR_ATTR );
    }

    if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) ||
        ( pVar->core.varType == CAMP_VAR_TYPE_STRUCTURE ) )
    {   /* DA: use snprintf to do append and truncate at once */
        camp_snprintf( buf, 15, "%s/  ->", pVar->core.ident );
    }
    else
    {
        if( sscanf( pVar->core.statusMsg, "alias: %15s", buf ) == 1 )
        { /* Using alias name; must still pad with spaces */
            int la = strlen( buf );
            if ( la > 0 ) 
            {
                camp_snprintf( &buf[la], sizeof( buf )-la, "%*.*s", 15-la, 15-la, "               " );
            }
        }
        else
        {
            camp_snprintf( buf, sizeof( buf ), "%-15s", pVar->core.ident );
        }
    }

#if CAMP_CUI_LOG
//    camp_snprintf( msg, sizeof( msg ), "varPicker_show: path: '%s', currVarPath: '%s', pVar: '%s', pVar_head: '%s', display: '%s'", 
//		   path, currVarPath, pVar->core.ident, pVar_head->core.ident, buf );
//    camp_log( "%s", msg );
#endif // CAMP_CUI_LOG

    mvwaddstr( dataWin, line, DATA_TITLE_X, buf );

    if( flag ) 
    {
        wattroff( dataWin, CURSOR_ATTR );
    }
}


void
varPicker( void )
{
    u_short inKey;
    int i;
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_head;
    char oldVarPath[LEN_PATH+1];

    /*
     *  Initialize currVarPath
     */
    if( camp_pathCompare( currViewPath, currVarPath ) )
    {
        pVar = camp_varGetPHead_clnt( currViewPath );
        if( pVar != NULL )
        {
            camp_pathDown( currVarPath, LEN_PATH+1, pVar->core.ident );
        }
    }

    for( ;; ) 
    {
        varPicker_show( currVarPath ); // highlight the current variable
        wrefresh( dataWin );
        displayVarInfoWin();
/*
        printStatus();
*/
        inKey = getKeyUpdate( dataWin );

        pVar = camp_varGetTrueP_clnt( currVarPath );
        pVar_head = camp_varGetPHead_clnt( currVarPath );
        camp_strncpy( oldVarPath, sizeof( oldVarPath ), currVarPath ); // strcpy( oldVarPath, currVarPath );

        switch( inKey )
        {
	case KEY_UP:
#if KEY_UP_2 != KEY_UP
	case KEY_UP_2:
#endif
#if KEY_UP_3 != KEY_UP
	case KEY_UP_3:
#endif
	
	    if( _camp_debug(CAMP_DEBUG_CLNT) )
	    {
		_camp_log( "pVar:%p pVar_head:%p", pVar, pVar_head );
	    }

/* 	    do */
/* 	    {   /\* skip over entries that are not displayed *\/ */
/* 		int var_index; */

/* 		if( pVar == NULL ) */
/* 		{ */
/* 		    BEEP(); */
/* 		    break; */
/* 		} */

/* 		var_index = getVar_index( pVar_head, pVar ); */

/* 		if( var_index == 0 ) */
/* 		{ */
/* 		    // we're at the top, go to the bottom */
/* 		    for( pVar = pVar_head;  */
/* 			 pVar->pNext != NULL;  */
/* 			 pVar = pVar->pNext ) ; */
/* 		} */
/* 		else */
/* 		{ */
/* 		    // one up */
/* 		    pVar = getVar_by_index( pVar_head, var_index-1 ); */
/* 		} */

/* 	    } while ( !( pVar->core.status & CAMP_VAR_ATTR_SHOW ) ); */
            
	    pVar = getVar_prev_wrap_show( pVar_head, pVar );

            if( pVar == NULL )
            {
                BEEP();
                break;
            }

	    camp_strncpy( currVarPath, sizeof( currVarPath ), pVar->core.path ); 
            varPicker_show( oldVarPath ); // unhighlight the previous variable
            break;

          case KEY_DOWN:
#if KEY_DOWN_2 != KEY_DOWN
          case KEY_DOWN_2:
#endif
#if KEY_DOWN_3 != KEY_DOWN
          case KEY_DOWN_3:
#endif

/* 	    do */
/* 	    {  /\* skip over entries that are not displayed *\/ */
/* 		int var_index; */
/* 	        var_index = getVar_index( pVar_head, pVar ); */
/* 		pVar = getVar_by_index( pVar_head, var_index+1 ); */
/* 		if( pVar == NULL ) */
/* 		{ */
/* 		    pVar = pVar_head; */
/* 		} */

/* 		pVar = getVar_next_wrap( pVar_head ); */

/* 	    } while ( !( pVar->core.status & CAMP_VAR_ATTR_SHOW ) ); */

	    pVar = getVar_next_wrap_show( pVar_head, pVar );

            if( pVar == NULL )
            {
                BEEP();
                break;
            }

	    camp_strncpy( currVarPath, sizeof( currVarPath ), pVar->core.path ); 
	    varPicker_show( oldVarPath ); // unhighlight the previous variable
            break;

          case KEY_RIGHT:
#if KEY_RIGHT_2 != KEY_RIGHT
          case KEY_RIGHT_2:
#endif
#if KEY_RIGHT_3 != KEY_RIGHT
          case KEY_RIGHT_3:
#endif
          case KEY_NEXT:

            if( pVar == NULL )
            {
                BEEP();
                break;
            }

            if( ( pVar->core.varType == CAMP_VAR_TYPE_STRUCTURE ) ||
                ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) )
            {
                camp_pathDown( currViewPath, LEN_PATH+1, pVar->core.ident );

                pVar = getVar_first_show( pVar->pChild ); // 20180613 DA - add getVar_first_show
                camp_strncpy( currVarPath, sizeof( currVarPath ), currViewPath ); // strcpy( currVarPath, currViewPath );
                if( pVar != NULL ) 
                {
                    camp_pathDown( currVarPath, sizeof( currVarPath ), pVar->core.ident );
                }
/*                displayData();
*/
                updateAndDisplayData();
            }
            else
            {
                BEEP();
            }
            break;

          case KEY_LEFT:
#if KEY_LEFT_2 != KEY_LEFT
          case KEY_LEFT_2:
#endif
#if KEY_LEFT_3 != KEY_LEFT
          case KEY_LEFT_3:
#endif
          case KEY_PREV:

            if( camp_pathAtTop( currViewPath ) )
            {
                BEEP();
                break;
            }

            camp_pathUp( currViewPath );
            camp_pathUp( currVarPath );
/*            displayData();
*/
            updateAndDisplayData();
            break;

          case KEY_CTRL_W:
          case KEY_CTRL_L:
#ifdef KEY_RESIZE
          case KEY_RESIZE:
#endif
            refreshScreen();
            break;

          case KEY_CTRL_E:
            cbMainMenuQuit();
            break;

          case KEY_CTRL_H:
            cbMainMenuHelp();
            break;

          case KEY_SPACE:
            updateServerData();
            updateAndDisplayData();
            break;

          case KEY_RETURN:
#if KEY_ENTER != KEY_RETURN
          case KEY_ENTER:
#endif
            if( pVar == NULL ) break;
            menuEdit( 0 );
            break;

          case KEY_TAB:
            doMainMenu();
            break;

          default:
            if( pVar == NULL ) break;
            menuEdit( inKey );
            break;
        }
    }
}
