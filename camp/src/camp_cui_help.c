/*
 *  Name:       camp_cui_help.c 
 *
 *  Purpose:    Routines to manage help windows and help access within
 *              the CAMP CUI
 *
 *  Revision history:
 *    ??-???-199?  TW  Initial VMS only
 *    ??-???-1999  DA  Implemented reading of VMS help files from Linux
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#ifndef VMS
#include <unistd.h>
#endif // VMS
#include "camp_cui.h"

#define CH_MAXTOPICS  1024
#define CH_MAXDEPTH   10 
#define CH_KWLENGTH   36
#define CH_FLLEN      128
#define CH_COLWIDTH   20

WINDOW* helpWin = NULL;

#ifndef VMS
/* 
 *  Not VMS, so we implement our own help help-file reader
 *
 *  Several static variables that are used on successive calls 
 */
static FILE *llib = NULL ;  /* file handle of help library */
static long int start[CH_MAXTOPICS], end[CH_MAXTOPICS], topic[CH_MAXDEPTH], helplevel[CH_MAXTOPICS];
static long int ntopic, itopic, nline, level, lastlevel, depth, lpointer;
static int  swid; // screen width
static long int screenline;
static int  help_on; /* boolean */
static char keyword[CH_MAXTOPICS][CH_KWLENGTH+1], keyw[CH_KWLENGTH+1];
static char filename[CH_FLLEN+1], headstr[CH_FLLEN+1];
static char last_library[CH_FLLEN+1] = { "" };
static char iline[CH_FLLEN];

extern char* argv0; /* argv[0] pointer saved by main() */

/*
 *  Some utility routines; these should probably go in a library
 */
/* 
 *  compare two strings case-insensitively.  Return true (1) when
 *  the entire string [input] matches the corresponding characters
 *  in string [proto].
 */
static int
ch_match (char* proto, char* input, int lmax)
{
  char   *imax;
  imax = input + lmax ;
  for ( ; (*input != '\0') && (input < imax) ; ++proto, ++input ) {
    if ( tolower(*proto) != tolower(*input) ) 
      return( 0 );
  }
  return( 1 );
}

/* 
 *  Truncate a "log" string to where it matches another "string".
 *  If log is null, then initialize with whole string.
 */
static void
logmatch( char* log, size_t log_size, const char* string )
{
    char* plog;
    int log_count;

    if( *log == '\0' )
    {
	camp_strncpy( log, log_size, string ); // strcpy( log, string );
    }
    else
    {
	for( plog = log, log_count = 0; 
	     ( log_count+1 < log_size ) && (*plog != '\0') && ( tolower(*plog) == tolower(*string) ) ;
	     ++string, ++plog ) 
	{
	}
	if( log_size > 0 ) *plog = '\0';
    }
    return;
}

static void
strsanitize( char* str, int len )
{
  char* loc;

  str[len-1] = '\0' ;
  loc = strchr( str, '\n' );
  if ( loc != 0 ) *loc = '\0';
  return;
}

/*
 *  copy string s2 to s1[tab], filling any gap with spaces
 */
static void
strcattab( char* s1, const char* s2, int tab )
{
  int m; /* boolean */
  char* k;

  m = 0;
  k = s1 + tab;

  for ( ; s1 < k ; ++s1 ) {
    if ( *s1 == '\0' ) m = 1;
    if ( m ) *s1 = ' ';
  }

  for ( ; *s2 != '\0' ; ++s1, ++s2 ) {
    *s1 = *s2;
  }
  *s1 = *s2;
}


char* 
find_help_lib( const char* const library_name, char* library_path, const int library_path_size )
{
    char* library_dir;
    // int library_name_len;

    // library_name_len = strlen( library_name );

    library_path[0] = '\0';

    /*
     *  Look in $CAMP_HELP
     */
    library_dir = getenv( "CAMP_HELP" );
    if ( library_dir != NULL )
    {
	camp_strncpy( library_path, library_path_size, library_dir ); // strncpy( library_path, library_dir, library_path_size-library_name_len-2 ); library_path[library_path_size-1]='\0';
	int len = strlen(library_path);
	if( len > 0 && library_path[len-1] != '/' ) // don't assume len>0
	{
	    camp_strncat( library_path, library_path_size, "/" ); // strcat( library_path, "/" );
	}
	camp_strncat( library_path, library_path_size, library_name ); // strcat( library_path, library_name );
	if ( access(library_path, R_OK) != 0 )
	{
	    library_path[0] = '\0';
	}
    }
    /*
     *  Look in $CAMP_DIR/help
     */
    if ( library_path[0] == '\0' )
    {
	library_dir = getenv( "CAMP_DIR" );
	if ( library_dir != NULL )
	{
	    camp_strncpy( library_path, library_path_size, library_dir ); // strncpy( library_path, library_dir, library_path_size-library_name_len-7 ); library_path[library_path_size-1]='\0';
	    int len = strlen(library_path);
	    if( len > 0 && library_path[len-1] != '/' ) // don't assume len>0
	    {
		camp_strncat( library_path, library_path_size, "/" ); // strcat( library_path, "/" );
	    }
	    camp_strncat( library_path, library_path_size, "help/" ); // strcat( library_path, "help/" );
	    camp_strncat( library_path, library_path_size, library_name ); // strcat( library_path, library_name );
	    if ( access(library_path, R_OK) != 0 )
	    {
		library_path[0] = '\0';
	    }
	}
    }
    /*
     *  Look in $MUSR_DIR/camp/help
     */
    if ( library_path[0] == '\0' )
    {
	library_dir = getenv( "MUSR_DIR" );
	if ( library_dir != NULL )
	{
	    camp_strncpy( library_path, library_path_size, library_dir ); // strncpy( library_path, library_dir, library_path_size-library_name_len-12 ); library_path[library_path_size-1]='\0';
	    int len = strlen(library_path);
	    if( len > 0 && library_path[len-1] != '/' ) // don't assume len>0
	    {
		camp_strncat( library_path, library_path_size, "/" ); // strcat( library_path, "/" );
	    }
	    camp_strncat( library_path, library_path_size, "camp/help/" ); // strcat( library_path, "camp/help/" );
	    camp_strncat( library_path, library_path_size, library_name ); // strcat( library_path, library_name );
	    if ( access(library_path, R_OK) != 0 )
	    {
		library_path[0] = '\0';
	    }
	}
    }

    /*
     * Now try locations based on the path to the camp_cui program,
     * in case no environment settings were made. For . being the
     * path of the program, look in:  . ; ./help ; ../ ; ../help
     */
    if ( library_path[0] == '\0' )
    {
	char * p;
	library_dir = argv0;
	if ( library_dir != NULL )
	{
	    camp_strncpy( library_path, library_path_size, library_dir ); // strncpy( library_path, library_dir, library_path_size-library_name_len-10+8 ); library_path[library_path_size-1]='\0';
	    p = strrchr( library_path, '/');
	    if ( p != NULL ) 
	    {
		p[1] = '\0';
		camp_strncat( library_path, library_path_size, library_name ); // strcat( library_path, library_name );
		if ( access(library_path, R_OK) != 0 )
		{
		    p[1] = '\0';
		    camp_strncat( library_path, library_path_size, "help/" ); // strcat( library_path, "help/" );
		    camp_strncat( library_path, library_path_size, library_name ); // strcat( library_path, library_name );
		    if ( access(library_path, R_OK) != 0 )
		    {
			p[1] = '\0'; 
			camp_strncat( library_path, library_path_size, "../" ); // strcat( library_path, "../" );
			camp_strncat( library_path, library_path_size, library_name ); // strcat( library_path, library_name );
			if ( access(library_path, R_OK) != 0 )
			{
			    p[1] = '\0';
			    camp_strncat( library_path, library_path_size, "../help/" ); // strcat( library_path, "../help/" );
			    camp_strncat( library_path, library_path_size, library_name ); // strcat( library_path, library_name );
			    if ( access(library_path, R_OK) != 0 )
			    {
				/* not found in any of 4 locations */
				library_path[0] = '\0';
			    }
			}
		    }
		}
	    }
	}
    }

    /*
     *  If not found anywhere, use the bare name as the path
     */
    if ( library_path[0] == '\0' )
    {
        camp_strncpy( library_path, library_path_size, library_name ); // strcpy( library_path, library_name );
    }

    return ( library_path );
}

#endif /* VMS */

/*
 *  Two routines to open and close the help window (which leaves a line
 *  at the top of the screen for the camp status line to show through.
 */

int
createHelpWin( void )
{
    helpWin = newwin( HELPWIN_H, HELPWIN_W, HELPWIN_Y, HELPWIN_X ); 
    if( helpWin == NULL )
    {
        return( CAMP_FAILURE );
    }

    leaveok( helpWin, FALSE );
    scrollok( helpWin, TRUE );
    keypad( helpWin, TRUE );

    return( CAMP_SUCCESS );
}

int
deleteHelpWin( void )
{
    if( helpWin != NULL ) 
    {
      delwin( helpWin );
      helpWin = (WINDOW*)NULL;
    }
    uncoverWindows();

    return( CAMP_SUCCESS );
}

/*
 *  Now routines used by the help-file reader.  First the VMS version,
 *  which only needs two for input and output.  (Linux follows)
 */

#ifdef VMS

long
helpGetInput( s_dsc* pdsc_result, s_dsc* pdsc_prompt, u_short* pResult_len )
{
    char* result;
    u_short inKey = 0;
    int x, y;

    *pResult_len = 0;
    setstrtodsc( &result, pdsc_result );

    helpPutOutput( pdsc_prompt );

    while( ( inKey != KEY_CANCEL_1 ) && ( inKey != KEY_RETURN )
           && ( inKey != KEY_ENTER ) && ( inKey != '?' ) )
    {
        inKey = getKey();
        if( inKey == KEY_DEL )
        {
            if( *pResult_len > 0 )
            {
                getyx( helpWin, y, x );
                mvwdelch( helpWin, y, x-1 );
                result[--(*pResult_len)] = '\0';
            }
        }
        else if( inKey == KEY_CTRL_U )
        {
            while( *pResult_len > 0 )
            {
                getyx( helpWin, y, x );
                mvwdelch( helpWin, y, x-1 );
                result[--(*pResult_len)] = '\0';
            }
        }
        else if( !isprint( inKey ) )
        {
            continue;
        }
        else
        {
            result[(*pResult_len)++] = inKey;
            result[*pResult_len] = '\0';
            waddch( helpWin, inKey );
        }
        wrefresh( helpWin );
    }
}


long
helpPutOutput( s_dsc* pdsc_message )
{
    char* message;

    setstrtodsc( &message, pdsc_message );

    scroll( helpWin );
    mvwaddstr( helpWin, HELPWIN_H-1, 0, message );
    wrefresh( helpWin );
}

#else   /*  lots of routines for Linux and others  */

static long
quitHelpWin( void )
{
    // int   inKey = 0;
    char  ich;

    waddstr ( helpWin, "  < Press any key to quit help > \n" );
    wrefresh( helpWin );

    cbreak ();  noecho(); 
    /*inKey =*/ wgetch( helpWin );

    return( CAMP_SUCCESS );
}

static long int
helpGetInput( char* result, size_t result_size, const char* prompt, const char* preload, int max_len )
     /* 
      *  Returns the number of characters entered, or a negative flag:
      *   0: zero-length; i.e., RETURN.  *or* start with a space
      *  -1: error
      *  -2: ? was entered (first)
      *  -3: terminated by CANCEL
      */
{
    int   inKey = 0; /* ??? change to u_short */
    int   len, cpos;
    int   x, y, i;

    getyx( helpWin, y, x );
    wprintw ( helpWin, "%s%s", prompt, preload );
    wrefresh( helpWin );
    getyx( helpWin, y, x );
    len = strlen( preload );
    cpos = len;
    x = x - cpos;
    camp_strncpy( result, result_size, preload ); // strcpy( result, preload );

    cbreak ();  noecho();  
    wtimeout( helpWin, -1 );
    inKey = 0;

    while ( len < max_len )
    {
       inKey = mvwgetch( helpWin, y, x+cpos );

       if ( ( inKey == KEY_RETURN ) || ( inKey == KEY_ENTER ) 
	    || ( inKey == KEY_TAB ) || ( (len == 0) && (inKey == KEY_SPACE) ) 
	  )
       {
         result[len] = '\0';
	 wprintw ( helpWin, "\n" );
         return( len );
       }
       else if ( inKey == KEY_CANCEL_1 )
       {
         result[len] = '\0';
	 wprintw ( helpWin, "\n" );
	 return( -3 );
       }
       else if ( ( inKey == (int) '?' ) && ( len == 0 ) )
       {
         result[0] = '\0';
	 wprintw ( helpWin, "\n" );
         return( -2 );
       }
       else if ( ( inKey < 256 ) && isprint ( (char) inKey ) )
       { 
	 for ( i = len ; i > cpos ; --i )
	 {
	   result[i+1] = result[i];
	 }
	 result[cpos] = (char) inKey;
	 ++cpos;  ++len;
	 result[len] = '\0';
         winsch( helpWin, inKey );
       }
       else if ( len > 0 )
       {
         if ( ( inKey == KEY_LEFT ) || ( inKey == KEY_LEFT_2 ) 
             || ( inKey == KEY_LEFT_3 ) || ( inKey == KEY_CTRL_B ) )
	 {
	   if ( cpos > 0 )
	     --cpos;
	 }
         else if ( ( inKey == KEY_RIGHT ) || ( inKey == KEY_RIGHT_2 ) 
             || ( inKey == KEY_RIGHT_3 ) || ( inKey == KEY_CTRL_F ) )
	 {
	   if ( cpos < len )
	     ++cpos;
	 }
	 else if ( ( inKey == KEY_DEL ) || ( inKey == KEY_CTRL_H ) )
	 {
	   if ( cpos > 0 )
	   {
	     --cpos;
	     --len;
	     camp_strncpy( result+cpos, result_size-cpos, result+cpos+1 ); // strcpy( result+cpos, result+cpos+1 );
	     mvwdelch( helpWin, y, x+cpos );
	   }
	 }
	 else if ( ( inKey == KEY_CTRL_U ) || ( inKey == KEY_CTRL_W ) )
	 {
	   if ( cpos > 0 )
	   {
	       camp_strncpy( result, result_size, result+cpos ); // strcpy( result, result+cpos );
	     len = len - cpos;
	     for( ; cpos > 0 ; --cpos )
	     {
	       mvwdelch( helpWin, y, x );
	     }
	   }
	 }
	 else if ( ( inKey == KEY_CTRL_E ) )
	 {
	   cpos = len;
	 }
	 else if ( ( inKey == KEY_CTRL_A ) )
	 {
	   cpos = 0;
	 }
       }
       result[len] = '\0';
       wrefresh( helpWin );
    }
    wprintw ( helpWin, "\n" );
    return( -1 );
}


static void
makeHelpHeader()
 /*
  *      Make topic header string.  Combine topics and subtopics on one
  *      line, and drop characters from the _beginning_ if it is too long
  *      (each keyword is known to be smaller than the len, but I'll test 
  *      anyway)
  */
{
    int  excess, sl, hl, i;

#ifdef DEBUGW
    wprintw ( helpWin, 
	      "Begin makeHelpHeader at depth %d for T %d [%s]\n",
	      depth, topic[depth], keyword[topic[depth]] ); 
    if ( depth > 0 ) {
	wprintw ( helpWin, 
		  "Topic above is %d [%s]\n", 
		  topic[depth-1], keyword[topic[depth-1]] );
    }
    wrefresh ( helpWin );
#endif

    if ( depth < 1 ) 
    {
	camp_strncpy( headstr, sizeof( headstr ), "Help" ); // strcpy( headstr, "Help" );
    }
    else
    {
	camp_strncpy_max( headstr, sizeof( headstr ), keyword[topic[1]], swid-5 ); // strncpy( headstr, keyword[topic[1]], swid-5 );
	for ( i = 2; i <= depth; ++i )
	{
	    camp_strncat( headstr, sizeof( headstr ), " / " ); // strcat ( headstr, " / " );
	    sl = strlen( headstr );
	    hl = strlen( keyword[topic[i]] );
	    excess = sl + hl  - ( swid - 5 ) ;
	    if ( excess > 0 )
	    {   /* must shorten */
		camp_strncpy( headstr, sizeof( headstr ), "..." ); // strcpy( headstr, "..." );
		if( excess+3 < sl )
		{   /* shorten the preceding string */
		    camp_strncpy( headstr+3, sizeof( headstr )-3, headstr+excess+3 ); // strcpy( headstr+3, headstr+excess+3 ); // subtracting from size_t is bad, 
		}
	    }
	    // strncat( headstr, keyword[ topic[i] ], swid - strlen(headstr) - 4 );
	    camp_strncat( headstr, swid-4+1, keyword[ topic[i] ] ); // terminates, size includes terminator
	}
    }
}

static void
ch_putHeader( const char* hdr )
/* 
 *  Display the header string at the top of the help window
 */
{
  int y, x;

  if ( *hdr != '\0' )
  {
     getyx( helpWin, y, x );
     mvwaddstr ( helpWin, 0, 0, hdr ); 
     wprintw ( helpWin, "\n\n" );
     wmove ( helpWin, y, x );
  }
}

/*
 *   This is a "more" filter that prints the line "str" on the curses window
 *   "helpWin".  It keeps count of how many lines have been printed in the 
 *   static "screenline".  If screenline is 0, then first print a blank line 
 *   and the header "hdr" (skip the hdr line if it is null).  If the given
 *   line fits on the screen with at least "keep" (or 0) lines below it, then
 *   print it and increment screenline.  If it doesn't fit, then pause for 
 *   input on the last screen line, prompting with string "prompt".  If the 
 *   user enters null or a space, then start over. 
 *   If there is significant input, then return the input and the flag in 
 *   "input" and "pinflag".
 */
static void
ch_linedisplay( const char* str, const char* hdr, int keep, const char* prompt, char* input, size_t input_size, long int* pinflag )
{
  int i, k, l, h;
  // int x, y;

  h = HELPWIN_H - 1 ; /* last line number */
  l = h - ( ( hdr[0] != '\0' ) ? 2 : 0 ); /* number of usable display lines */
  k = ( ( keep > 0 ) ? keep : 0 ); /* number to keep together */

  if ( screenline == 0 )
  {
    /*  Only do the following if the window isn't pre-cleared! 
    wprintw ( helpWin, "\n" ); */  /* and don't count this line -- it can disappear! */
    if ( hdr[0] != '\0' )
    {
	// getyx( helpWin, y, x );   /* remember cursor position */
	wprintw( helpWin, "%s\n\n", hdr );
    }
  }

  if ( (screenline + k)/l > (screenline-1)/l )
  {  /* pause screen */
/*  
 *  make lines for "more" prompt
 */
    for ( i = 0 ; (screenline)/l == (screenline-1)/l ; ++i )
    {
      wprintw( helpWin, "\n" );
      ++screenline;
    }

    scrollok( helpWin, FALSE );
/*
 *  Display the header at the top of the page, unless this is the
 *  the first page, in which case the header is already there.
 */
    if ( (screenline + 1) / l > 1 )
    {
      ch_putHeader( hdr );
    }
    wrefresh(helpWin);
/*
 *  Now go to bottom of window and prompt for RETURN, SPACE, or some
 *  command.  Then erase the input line and restore the cursor position.
 */
    wmove ( helpWin, h, 0 ); 
    *pinflag = helpGetInput( input, input_size, prompt, "", CH_KWLENGTH );
    mvwaddstr ( helpWin, h, 0,
		  "                                                " );
    wmove ( helpWin, h-i, 0 );
    ++screenline;
    scrollok( helpWin, TRUE );

    if ( *pinflag != 0 )
    {
      return;
    }
  }
  wprintw ( helpWin, "%s\n", str );
  ++screenline;
  return;
}

static void
ch_listtopics( char* mat, char* hdr, char* new, size_t new_size, long int* pcf )
/*
 *       List subtopics that match [mat]
 */
{
   long int stopic, sl, hl;
   int lpos, lposp;

   stopic = topic[depth-1] + 1;
   hl = helplevel[topic[depth-1]] + 1;
   lpos = swid + 1;
   iline[0] = '\0';

#ifdef DEBUGW
	     wprintw ( helpWin, 
                "Begin listtopics at depth %d for T %d [%s] matching [%s];\n",
		depth, topic[depth-1], keyword[topic[depth-1]], mat ); 
	     wprintw ( helpWin, 
                 "look for level %d starting at %d (L %d).\n",
		 hl, stopic, helplevel[stopic] );
	     wprintw ( helpWin, 
                 "Try: [%s] T %d L %d;  HL: %d  MA: %d \n",
		  keyword[stopic], stopic, helplevel[stopic], 
		  helplevel[stopic] == hl,
                  ch_match( keyword[stopic], mat, CH_KWLENGTH )     
	          );
	     wrefresh ( helpWin );
#endif

   while ( (stopic <= ntopic) && (helplevel[stopic] >= hl) )
   {
      if (  ( helplevel[stopic] == hl ) 
	   && ( ( mat[0] == '\0' ) || 
	        ( ch_match( keyword[stopic], mat, CH_KWLENGTH ) ) ) )
      {
	 lposp = ( ( lpos + CH_COLWIDTH + 1) / CH_COLWIDTH ) * CH_COLWIDTH ;
	 sl = strlen ( keyword[stopic] );

	 if ( lposp + sl < swid - 1 )
	 {
	    strcattab( iline, keyword[stopic], lposp );
	    lpos = lposp + sl;
	 }
	 else
	 {
	     ch_linedisplay ( iline, hdr, 0, "More: ", new, new_size, pcf );
	     camp_strncpy( iline, sizeof( iline ), keyword[stopic] ); // strcpy( iline, keyword[stopic] );
	     lpos = sl;
	 }
      }
      ++stopic;
      if (*pcf != 0)
      {
	return;
      }
   }
   if (iline[0] != '\0')
   {
       ch_linedisplay ( iline, hdr, 0, "More: ", new, new_size, pcf );
   }
   return;
}


/*
 * vmshelp   : Help facility using VMS help libraries
 * Author    : Donald Arseneau
 * Date      : 1999/Oct/18
 *
 * Parameters: subj --    a 1st level help subject
 *             library -- filename of text help library
 *
 *  On first call, the help library is scanned for lines with numbers
 *  in column 0 and a keyword starting in column 2.  These are used to
 *  indenx the help file and allow for rapid searching on subsequent calls.
 *
 *  The user interface is similar, but not identical to VMS help.
 *  RETURN goes back to the previous help level, or exits at help level 1.
 *  EOF exits from any level.  "?" redisplays the current topic.  Ambiguous
 *  input does not display all matching topics but instead lists the possible
 *  matches, and looks for more input to distinguish;  like a TAB in shell.
 *  In fact, TAB can be used instead of RETURN.
 */

void 
vmshelp_reader ( char subj[], char library[] )
//  char   subj[];      /* subj -- a 1st level help subject */
//  char   library[];
{
    int  searchkw; /* boolean */
    long int  cflag, nmatched, hl, i;
    char buffer[CH_FLLEN+10], matchstr[CH_KWLENGTH+1], newkw[CH_KWLENGTH+1];
    char sub[2][4] = { "", "sub" };

    swid = ( CH_FLLEN < SCREEN_W ) ? CH_FLLEN : SCREEN_W;
/*
 * If the library name is not the same as last time, then open the
 * new library, and scan its structure into static arrays.  The library
 * file is left open between calls for speed. The calling program can
 * call with library = "" to close the file without opening a new one.
 */
    if( library[0] == '\0' )
    {
       last_library[0] = '\0';
       if( llib != NULL ) 
	 fclose ( llib );
       return;
    }

    if ( strncmp( library, last_library, CH_FLLEN ) != 0 )
    {
       if( llib != NULL ) 
       {
	   fclose ( llib );
       }
       camp_strncpy( last_library, CH_FLLEN+1, library ); // strncpy( last_library, library, CH_FLLEN-1 );
       ntopic = 0;
       nline = -1;
       llib = fopen( last_library, "r" );
       if (llib == NULL)
       {
          wprintw ( helpWin, 
		    "\nFailed to open help library %s.\n\n", last_library);
	  quitHelpWin ();
          return;
       }

       help_on = FALSE;
       camp_strncpy( keyword[0], CH_KWLENGTH+1, "Help" ); // strncpy( keyword[0], "Help", CH_KWLENGTH );
       start[0] = 0;
       end[0] = 0;
       helplevel[0] = 0;

       while ( ( ntopic+1 < CH_MAXTOPICS ) && (fgets( iline, CH_FLLEN, llib) != 0 ) )
       {
          ++nline;
	  strsanitize( iline, CH_FLLEN );
          if ( isdigit(iline[0]) )
          {
             ++ntopic;
             level = iline[0] - '0';
             if ( level > lastlevel+1 )
             {
                wprintw ( helpWin, "\nHELP SYSTEM ERROR:\n");
                wprintw ( helpWin, 
                        "Topics out of order at line %ld in help file %s.\n",
			 nline, last_library );
		quitHelpWin ();
                fclose(llib);
                return;
             }
             helplevel[ntopic] = level;
             lastlevel = level;
             camp_strncpy( keyword[ntopic], CH_KWLENGTH+1, iline+3 ); // strncpy( keyword[ntopic], iline+3, CH_KWLENGTH ); 
             start[ntopic] = nline;

	     /*
#ifdef DEBUGW
	     wprintw ( helpWin, "T%3d @%4d L%2d |%s|.\n", 
		       ntopic, nline, level, keyword[ntopic] );
	     wrefresh ( helpWin );
#endif
	     */
             if (ntopic>0) 
	        end[ntopic-1] = nline - 1;
          }
       }
       end[ntopic] = nline;
       help_on = TRUE;
    }
/*  
 *  Abort if there was an error reading the library.  help_on is static
 *  so the following test is needed when a user tries again after a 
 *  failure to read the library.
 */
    if ( ! help_on )
    {
       wprintw ( helpWin, "\nHELP SYSTEM ERROR: help not available.\n");
       quitHelpWin ();
       return;
    }

/*  initialize help loop */
      
    screenline = 0;
    camp_strncpy( keyw, sizeof( keyw ), subj ); // strncpy( keyw, subj, CH_KWLENGTH ); keyw[CH_KWLENGTH] = '\0';
    searchkw = TRUE;
    depth = 0;
    topic[0] = 0;
    itopic = 0;
    rewind( llib );
    lpointer = 0; 

 /***************************************************************************\
 *                                                                           *
 *                            Main help loop                                 *
 *                                                                           *
 \***************************************************************************/

    while (depth >= 0)
    {

       cflag = 0;
#ifdef DEBUGW
       wprintw ( helpWin, 
       "Top of main help loop.  depth %d, itopic %d, keyw [%s]\n",
		 depth, itopic, keyw );
       wrefresh( helpWin );
#endif

       /* Perhaps search for topic among current subtopics */

       if (keyw[0] == '\0')
       {
	  searchkw = FALSE;
       }

       nmatched = ( searchkw ? 0 : 1 );

       matchstr[0] = '\0';

       hl = helplevel[ itopic ] + 1;
       while ( searchkw )
       {
/*        Search for all keys that match [keyw] at depth [depth+1] starting 
 *        from topic [itopic+1].  Quit search when see a topic at <= depth
 *        or at the final topic.  Count the number of matching keywords and
 *        record the portion of the string where all matches match each
 *        other [matchstr]. At end, [itopic] gives the last match, or the
 *        root topic if no subtopic matched.  If a complete exact match is
 *        found at any point, though, the search is terminated and nummatches
 *        set = 1.  This allows "insIfRead" to be selected when there is also
 *        the topic "insIfReadVerify".
 */
	  i = itopic + 1;
#ifdef DEBUGW
	  wprintw ( helpWin,
		    "Search for [%s] at hl %d, start @topic %d; depth = %d.\n", 
		    keyw, hl, i, depth); 
	  wrefresh( helpWin );
#endif
          while ( 
                ! ( ch_match ( keyword[i], keyw, CH_KWLENGTH ) && 
                    (helplevel[i] == hl) )
                && ( helplevel[i] >= hl)
                && ( i <= ntopic)
                )
          {
	    ++i;
          }
	  if ( (helplevel[i] == hl) && (i <= ntopic) )
	  { /* keyword found */
	    ++nmatched;
            itopic = i;
#ifdef DEBUGW
	    wprintw ( helpWin,
		    "Found %d T %d [%s] old ms: [%s].\n", 
		    nmatched, itopic, keyword[itopic], matchstr); 
	    wrefresh( helpWin );
#endif
	    logmatch ( matchstr, sizeof( matchstr ), keyword[itopic] );
#ifdef DEBUGW
	    wprintw ( helpWin,
		    "      %d T %d [%s] new ms: [%s].\n", 
		    nmatched, itopic, keyword[itopic], matchstr); 
	    wrefresh( helpWin );
#endif
	    if ( ch_match ( keyw, keyword[i], CH_KWLENGTH ) )
	    { /* keyword is fully identical, so abort further search */
		nmatched = 1;
		break;
	    }
	  }
	  else
	  {
	    searchkw = FALSE;
	  }
       }
/*
 *     Proceed depending on how many matches were found (will be 1 when
 *     no search was performed.
 */

#ifdef DEBUGW
       wprintw ( helpWin, "Got topic %d [%s] at level %d.\n", 
              itopic, keyword[itopic], helplevel[itopic]);
       wrefresh( helpWin );
#endif
       switch ( nmatched )
       {
       case 0:          /************* no matches *****************/

	 wprintw ( helpWin, "\nTopic \"%s\" not found.\n", keyw );
	 /*
	 What else here?????;
	 */

	 break;

       case 1:          /************ single match ****************/

	 topic[depth] = itopic;
#ifndef DEBUGW
         wclear ( helpWin );
#endif
	 makeHelpHeader();
/*
 *       Display Description lines
 */

         if ( lpointer >= start[itopic] ) 
	 {  /*  rewind if topic has been passed by */
            rewind( llib );
            lpointer = 0;
         }
/*
 *       lpointer points to next record in llib.
 *       Seek to end of topic, printing out topic lines.
 *       Output is paginated by ch_linedisplay(), and the user can enter
 *       a new keyword at the `more' prompt, as signalled by cflag (as 
 *       in helpGetInput).
 */
	 cflag = 0;
         while ( (lpointer < end[itopic]) && (cflag==0) )
	 {
	    ++lpointer;
	    if ( fgets( iline, CH_FLLEN, llib) == 0 )
	    {      /* write error message... something like... */
               wprintw ( helpWin, "\nHELP SYSTEM ERROR:\n");
	       wprintw ( helpWin, 
			"Can't read line %ld in help file %s.\n",
                        lpointer, last_library );
               quitHelpWin ();
               fclose ( llib );
	       help_on = FALSE;
               return;
	    }
	    if ( lpointer > start[itopic]+1 )
	    {
	       strsanitize( iline, CH_FLLEN );
	       ch_linedisplay ( iline, headstr, 0, "More: ", newkw, sizeof( newkw ), &cflag );
	    }
	 }
/*
 *       Postpone handling user input, if any, until below; skip ahead now.
 */
	 if ( cflag == 0 )
	 {
/*
 *          If this topic has subtopics, increase depth and then display
 *          the subtopics.  (This order implies that the user can enter
 *          a new *sub* topic at the "more" prompt.)
 */
            if (helplevel[itopic + 1] > helplevel[itopic]) 
	    {
#ifdef DEBUGW
	       wprintw ( helpWin, 
               "Topic %d [%s] (Lev %d) has subtopics.\n",
	       itopic, keyword[itopic], helplevel[itopic] );
 	       wrefresh ( helpWin );
#endif
	       ch_linedisplay ( " ", headstr, 3, "More: ", newkw, sizeof( newkw ), &cflag );
	       ch_linedisplay ( "Help is available on:", 
			        headstr, 0, "More: ", newkw, sizeof( newkw ), &cflag );
	       if ( cflag == 0 )
               {
		 ++depth;
		 ch_listtopics( "", headstr, newkw, sizeof( newkw ), &cflag );
	       }
	    }
	 }
	 break;

       default:         /************ many matches ****************/
/*
 *	 Redisplay matching subtopics, (tab or cr)
 */
	 ch_linedisplay ( "Matching topics are:", "", 2, "More: ", newkw, sizeof( newkw ), &cflag );
	 if ( cflag == 0 )
	 {
	    ch_listtopics( matchstr, "", newkw, sizeof( newkw ), &cflag );
	 }
	 break;

       }  /*  End switch(nmatched) */
/*
 *     Get a new topic from user (or use topic that the user interjected).
 *     If the user just presses return, then pop a level and repeat.
 */
       do { /* ... while */

 	  itopic = topic[ ( depth < 1 ? 0 : depth-1 ) ];

 	  if ( cflag == 0 )
          {
	    wprintw ( helpWin, "\n");
	    if ( screenline > HELPWIN_H + 2 )
	    {
	       ch_putHeader( headstr );
	    }
#ifdef DEBUGW
	    wprintw ( helpWin, 
		      "Topics: [%d] [%d] [%d] [%d]\n",
		      topic[0], topic[1], topic[2], topic[3] );
	    wprintw ( helpWin, 
		      "Prompt for depth %d topic.  We are at T %d [%s]\n",
		      depth, itopic, keyword[itopic] );
	    wrefresh ( helpWin );
#endif

	    camp_snprintf( buffer, sizeof( buffer ), "%s %stopic? ", 
			   keyword[itopic], sub[ ( depth > 1 ? 1 : 0 ) ] );
	    if ( nmatched < 2 )
	    {
	       matchstr[0] = '\0';
	    }
	    cflag = helpGetInput ( keyw, sizeof( keyw ), buffer, matchstr, CH_KWLENGTH );
	  }
	  else
          {
	      camp_strncpy( keyw, CH_KWLENGTH+1, newkw ); // strncpy( keyw, newkw, CH_KWLENGTH );
	  }

	  strsanitize( keyw, CH_KWLENGTH );

	  searchkw = FALSE;

	  if ( (cflag == -1) || (cflag == -3) )
	  {  /* Quit */
	    return;
	  }
	  else if ( cflag == -2 )
	  {  /* "?" -> Redisplay current topic */
	      camp_strncpy( keyw, CH_KWLENGTH+1, keyword[topic[depth]] ); // strncpy ( keyw, keyword[topic[depth]], CH_KWLENGTH );
	      if ( depth > 0 ) --depth;
	      itopic = topic[depth];
	  }
	  else if ( cflag == 0 )
	  {  /* ( null ) Pop up to previous topic */
	    --depth;
	    if ( depth <= 0 ) 
	      return;
	    nmatched = 1;
	  }
	  else
	  {
	    searchkw = TRUE;
	  }

       screenline = 0;

       } while ( cflag == 0 );

    } /* end of main help loop */
}

#endif

#ifdef HELP_MAIN

void main (int argc, char* argv[] )
  /*
   *  stand-alone vms-help reader.
   *  1999-Oct-19          Donald Arseneau
   *  from Morgan Burke (TRIUMF) 1993-Oct-19
   */
{
  char  libname[80] = { "" };
  char  subj[80];
  char* plibname;

  if (argc <= 1)
    {  
      plibname = getenv( "VMSHELP" );
      if ( plibname == NULL )
	{
	  printf ("Syntax: %s helplib [subject]\n", argv[0] );
	  return;
	}
      else
	{
	    camp_strncpy( libname, sizeof( libname ), plibname ); // strncpy ( libname, plibname, 79 );
	}
    }
  else
    {
	camp_strncpy( libname, sizeof( libname ), argv[1] ); // strncpy ( libname, argv[1], 79 );
    }

  if( argc > 2 )
  {
      camp_strncpy( subj, sizeof( subj ), argv[2] ); // strncpy( subj, argv[2], 79 );
  }
  else
  {
      subj[0] = '\0';
  }

  initscr(); cbreak(); echo(); nonl();
  intrflush( stdscr, FALSE );
  keypad( stdscr, TRUE );
  scrollok( stdscr, TRUE );

  createHelpWin();
  vmshelp_reader (subj, libname);
  deleteHelpWin();
  endwin();

}

#endif
