/*
 *  $Id: camp_cui_if.c,v 1.9 2015/04/21 06:52:35 asnd Exp $
 *
 *  $Revision: 1.9 $
 *
 *  Purpose:    Routines to prompt for input of interface specifications
 *
 *  Called by:  camp_cui_menucb.c
 * 
 *  $Log: camp_cui_if.c,v $
 *  Revision 1.9  2015/04/21 06:52:35  asnd
 *  Eliminate some compiler warnings.
 *
 *  Revision 1.8  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.5  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.4  2004/01/28 03:14:37  asnd
 *  Add VME interface type.
 *
 *  Revision 1.3  2000/12/23 02:42:57  ted
 *  Minor change inputIf_indpak, camp Type[] -> char* Type[]
 *
 *  Revision 1.2  2000/12/22 23:59:31  David.Morris
 *  Added INDPAK support for adding instruments in the CUI
 *
 *
 *  Revision history:
 *   v1.1  20-Apr-1994  [T. Whidden] drivers in server, Ifs generalized
 *         14-Dec-1999  TW  Additional include for gcc (Linux)
 *                          Some integer type changes for gcc
 *
 */

#ifdef linux
#include <string.h>
#endif /* linux */
#include <math.h>
#include "camp_cui.h"


bool_t
inputIf( CAMP_VAR* pVar, char* typeIdent, size_t typeIdent_size, double* pAccessDelay, double* pTimeout, char* defn, size_t defn_size )
{
    CAMP_IF_t* pIF_t;
    char defaultTypeIdent[LEN_IDENT+1];
    CAMP_INSTRUMENT* pIns;
    double accessDelay;
    double timeout;
    char defaultDefn[LEN_IF_DEFN+1];

    defaultTypeIdent[0] = '\0';
    accessDelay = 0.0;
    timeout = 0.0;

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    if( pIns->pIF != NULL ) 
    {
        camp_strncpy( defaultTypeIdent, sizeof( defaultTypeIdent ), pIns->pIF->typeIdent ); // strcpy( defaultTypeIdent, pIns->pIF->typeIdent );
        accessDelay = (double)pIns->pIF->accessDelay;
        timeout = (double)pIns->pIF->timeout;
    }

    if( !inputIfType( defaultTypeIdent, typeIdent, typeIdent_size ) ) 
    {
        return( FALSE );
    }

    pIF_t = camp_ifGetpIF_t_clnt( typeIdent );
    if( pIF_t == NULL ) 
    {
        return( FALSE );
    }

    if( !inputFloat( "Access delay", accessDelay, &accessDelay ) )
    {
        return( FALSE );
    }

    *pAccessDelay = accessDelay;

    if( !inputFloat( "Timeout", timeout, &timeout ) )
    {
        return( FALSE );
    }

    *pTimeout = timeout;

    if( ( pIns->pIF != NULL ) && ( pIns->pIF->typeID == pIF_t->typeID ) )
    {
	camp_strncpy( defaultDefn, sizeof( defaultDefn ), pIns->pIF->defn ); // strcpy( defaultDefn, pIns->pIF->defn );
    }
    else
    {
	camp_strncpy( defaultDefn, sizeof( defaultDefn ), pIF_t->defaultDefn ); // strcpy( defaultDefn, pIF_t->defaultDefn );
    }

    switch( pIF_t->typeID )
    {
    case CAMP_IF_TYPE_NONE:
        break;

    case CAMP_IF_TYPE_RS232:
	if( !inputIf_rs232( defaultDefn, pIF_t->conf, defn, defn_size ) )
        {
            return( FALSE );
        }
        break;

    case CAMP_IF_TYPE_GPIB:
	if( !inputIf_gpib( defaultDefn, defn, defn_size, FALSE ) )
        {
            return( FALSE );
        }

        break;

    case CAMP_IF_TYPE_GPIB_MSCB:
	if( !inputIf_gpib( defaultDefn, defn, defn_size, TRUE ) )
        {
            return( FALSE );
        }

        break;

      case CAMP_IF_TYPE_CAMAC:
        if( !inputIf_camac( defaultDefn, defn, defn_size ) )
        {
            return( FALSE );
        }

        break;

      case CAMP_IF_TYPE_VME:
        if( !inputIf_vme( defaultDefn, defn, defn_size ) )
        {
            return( FALSE );
        }

        break;

      case CAMP_IF_TYPE_TCPIP:
        if( !inputIf_tcpip( defaultDefn, defn, defn_size ) )
        {
            return( FALSE );
        }
        break;

      case CAMP_IF_TYPE_INDPAK:
        if( !inputIf_indpak( defaultDefn, defn, defn_size ) )
        {
            return( FALSE );
        }

        break;

      default:
        return( FALSE );
    }

    return( TRUE );
}


bool_t
inputIfType( const char* defaultTypeIdent, char* typeIdent, size_t typeIdent_size )
{
    char* names[MAX_NUM_SELECTIONS];
    CAMP_IF_t* pIF_t;
    u_int index;
    int num;
    int defIndex;

    num = 0;
    defIndex = 0;
    for( pIF_t = pSys->pIFTypes; 
         pIF_t != NULL; 
         pIF_t = pIF_t->pNext )
    {
        if( streq( defaultTypeIdent, pIF_t->ident ) )
        {
            defIndex = num;
        }
        names[num++] = pIF_t->ident;
    }

    if( inputSelectWin( "Interface Type", num, names, defIndex, &index ) )
    {
        camp_strncpy( typeIdent, typeIdent_size, names[index] ); // strcpy( typeIdent, names[index] );
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
inputIf_rs232( const char* defaultDefn, const char* conf, char* defn, size_t defn_size )
{
    long i;
    char buf[128];
    char* names[MAX_NUM_SELECTIONS];
    u_int index;
    int num;
    int defIndex;
    bool_t flag = TRUE;
    char defPort[LEN_IDENT+1];
    int defBaud;
    int defData;
    int defStop;
    char defParity[LEN_IDENT+1];
    char defReadTerm[LEN_IDENT+1];
    char defWriteTerm[LEN_IDENT+1];
    /* int defTimeout; */

    camp_getIfRs232Port( defaultDefn, defPort, sizeof( defPort ) );
    camp_getIfRs232Parity( defaultDefn, defParity, sizeof( defParity ) );
    camp_getIfRs232ReadTerm( defaultDefn, defReadTerm, sizeof( defReadTerm ) );
    camp_getIfRs232WriteTerm( defaultDefn, defWriteTerm, sizeof( defWriteTerm ) );
    defBaud = camp_getIfRs232Baud( defaultDefn );
    defData = camp_getIfRs232Data( defaultDefn );
    defStop = camp_getIfRs232Stop( defaultDefn );
    /* defTimeout = camp_getIfRs232Timeout( defaultDefn ); */

    camp_strncpy( buf, sizeof( buf ), conf ); // strcpy( buf, conf );
    
    defIndex = 0;
    for( num = 0, names[0] = strtok( buf, " " ); 
         names[num] != NULL; 
         names[++num] = strtok( NULL, " " ) )
    {
        if( streq( defPort, names[num] ) ) defIndex = num;
    }

    if( !inputSelectWin( "Port", num, names, defIndex, &index ) )
        return( FALSE );

    camp_strncpy( defn, defn_size, names[index] ); // strcpy( defn, names[index] );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " ); // terminates, size includes terminator

    for( i = 0; i < MAX_NUM_SELECTIONS; i++ ) names[i] = 0;

    num = 0;
    names[num++] = strdup( "110" ); // 
    names[num++] = strdup( "300" ); // 
    names[num++] = strdup( "600" ); // 
    names[num++] = strdup( "1200" ); // 
    names[num++] = strdup( "2400" ); // 
    names[num++] = strdup( "4800" ); // 
    names[num++] = strdup( "9600" ); // 
    names[num++] = strdup( "19200" ); // 
    names[num++] = strdup( "38400" ); // 
    names[num++] = strdup( "57600" ); // 
    names[num++] = strdup( "115200" ); // 

    switch( defBaud )
    {
      case 110:
        defIndex = 0;
        break;
      case 300:
        defIndex = 1;
        break;
      case 600:
        defIndex = 2;
        break;
      case 1200:
        defIndex = 3;
        break;
      case 2400:
        defIndex = 4;
        break;
      case 4800:
        defIndex = 5;
        break;
      case 9600:
        defIndex = 6;
        break;
      case 19200:
        defIndex = 7;
        break;
      case 38400:
        defIndex = 8;
        break;
      case 57600:
        defIndex = 9;
        break;
      case 115200:
        defIndex = 10;
        break;
      default:
        defIndex = 6;
        break;
    }

    if( !inputSelectWin( "Baud rate", num, names, defIndex, &index ) )
        flag = FALSE;

    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );
    for( i = 0; i < num; i++ ) free( names[i] );
    if( !flag ) return( FALSE );

    if( !inputInteger( "Data bits", defData, &i ) )
    {
        return( FALSE );
    }
    camp_snprintf( buf, sizeof( buf ), "%d ", i );
    camp_strncat( defn, defn_size, buf ); // strcat( defn, buf );

    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_ODD );
    names[num++] = toktostr( TOK_EVEN );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defParity, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Parity", num, names, defIndex, &index ) )
        flag = FALSE;

    if( !flag ) return( FALSE );

    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );

    if( !inputInteger( "Stop bits", defStop, &i ) )
    {
        return( FALSE );
    }
    camp_snprintf( buf, sizeof( buf ), "%d ", i );
    camp_strncat( defn, defn_size, buf ); // strcat( defn, buf );

    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_LF );
    names[num++] = toktostr( TOK_CR );
    names[num++] = toktostr( TOK_CRLF );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defReadTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Read terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defWriteTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Write terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );

    /* if( !inputInteger( "Read timeout", defTimeout, &i ) ) */
    /* { */
    /*     return( FALSE ); */
    /* } */
    /* camp_snprintf( buf, sizeof( buf ), "%d ", i ); */
    /* camp_strncat( defn, defn_size, buf ); // strcat( defn, buf ); */

    return( TRUE );
}


bool_t 
inputIf_gpib( const char* defaultDefn, char* defn, size_t defn_size, bool_t gpib_mscb )
{
    long i;
    char* names[MAX_NUM_SELECTIONS];
    u_int index;
    int num;
    int defIndex;
    int defAddr;
    char defReadTerm[LEN_IDENT+1];
    char defWriteTerm[LEN_IDENT+1];
    int defMscbAddr;
    char buf[LEN_BUF+1];

    defAddr = camp_getIfGpibAddr( defaultDefn );
    camp_getIfGpibReadTerm( defaultDefn, defReadTerm, sizeof( defReadTerm ) );
    camp_getIfGpibWriteTerm( defaultDefn, defWriteTerm, sizeof( defWriteTerm ) );
    defMscbAddr = camp_getIfGpibMscbAddr( defaultDefn );

    if( !inputInteger( "GPIB address", defAddr, &i ) )
    {
        return( FALSE );
    }

    camp_snprintf( defn, defn_size, "%d", i );
    
    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_LF );
    names[num++] = toktostr( TOK_CR );
    names[num++] = toktostr( TOK_CRLF );

    defIndex = 0;
    for( i = 0; i < num; i++ )
    {
        if( streq( defReadTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Read terminator", num, names, defIndex, &index ) )
    {
        return( FALSE );
    }

    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );
    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );

    defIndex = 0;
    for( i = 0; i < num; i++ )
    {
        if( streq( defWriteTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Write terminator", num, names, defIndex, &index ) )
    {
        return( FALSE );
    }

    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );
    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );

    if( gpib_mscb )
    {
	if( !inputInteger( "MSCB address", defMscbAddr, &i ) )
	{
	    return( FALSE );
	}

	camp_snprintf( buf, sizeof( buf ), " %d", i );
	camp_strncat( defn, defn_size, buf ); 
    }

    return( TRUE );
}


bool_t 
inputIf_camac( const char* defaultDefn, char* defn, size_t defn_size )
{
    long i;
    long def;
    char buf[LEN_BUF+1];

    def = camp_getIfCamacB( defaultDefn );
    if( !inputInteger( "Branch", def, &i ) )
    {
        return( FALSE );
    }

    camp_snprintf( defn, defn_size, "%d ", i );
    
    def = camp_getIfCamacC( defaultDefn );
    if( !inputInteger( "Crate", def, &i ) )
    {
        return( FALSE );
    }

    camp_snprintf( buf, sizeof( buf ), "%d ", i );
    camp_strncat( defn, defn_size, buf ); // strcat( defn, buf );

    def = camp_getIfCamacN( defaultDefn );
    if( !inputInteger( "Slot", def, &i ) )
    {
        return( FALSE );
    }

    camp_snprintf( buf, sizeof( buf ), "%d ", i );
    camp_strncat( defn, defn_size, buf ); // strcat( defn, buf );

    return( TRUE );
}


bool_t 
inputIf_vme( const char* defaultDefn, char* defn, size_t defn_size )
{
    long i;
    long def;
    char buf[LEN_BUF+1];

    def = camp_getIfVmeBase( defaultDefn );
    if( !inputInteger( "Base address", def, &i ) )
        return( FALSE );

    camp_snprintf( defn, defn_size, "%d ", i );

    return( TRUE );
}


bool_t 
inputIf_tcpip( const char* defaultDefn, char* defn, size_t defn_size )
{
    long i;
    double f;
    char buf[LEN_BUF+1];
    char* names[MAX_NUM_SELECTIONS];
    u_int index;
    int num;
    int defIndex;
    bool_t flag = TRUE;
    char defAddr[32];
    int defPort;
    char defReadTerm[LEN_IDENT+1];
    char defWriteTerm[LEN_IDENT+1];
    /* double defTimeout; */

    camp_getIfTcpipAddr( defaultDefn, defAddr, sizeof( defAddr ) );
    defPort = camp_getIfTcpipPort( defaultDefn );
    camp_getIfTcpipReadTerm( defaultDefn, defReadTerm, sizeof( defReadTerm ) );
    camp_getIfTcpipWriteTerm( defaultDefn, defWriteTerm, sizeof( defWriteTerm ) );
    /* defTimeout = camp_getIfTcpipTimeout( defaultDefn ); */
 
    if( !inputString( "TCP/IP Address", 31, defAddr, buf, sizeof( buf ), isPrintChar ) )
    {
        return( FALSE );
    }
    camp_strncat( defn, defn_size, buf ); // strcat( defn, buf );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );

    if( !inputInteger( "Port number", defPort, &i ) )
    {
        return( FALSE );
    }
    camp_snprintf( buf, sizeof( buf ), "%d ", i );
    camp_strncat( defn, defn_size, buf ); // strcat( defn, buf );

    num = 0;
    names[num++] = toktostr( TOK_NONE );
    names[num++] = toktostr( TOK_LF );
    names[num++] = toktostr( TOK_CR );
    names[num++] = toktostr( TOK_CRLF );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defReadTerm, names[i] ) ) defIndex = i;
    }
    if( !inputSelectWin( "Read terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );

    defIndex = 0;
    for( i=0; i<num; i++ )
    {
        if( streq( defWriteTerm, names[i] ) ) defIndex = i;
    }

    if( !inputSelectWin( "Write terminator", num, names, defIndex, &index ) )
    {
        flag = FALSE;
    }

    if( !flag ) return( FALSE );

    camp_strncat( defn, defn_size, names[index] ); // strcat( defn, names[index] );
    camp_strncat( defn, defn_size, " " ); // strcat( defn, " " );

    /* if( !inputFloat( "Timeout", defTimeout, &f ) ) */
    /* { */
    /*     return( FALSE ); */
    /* } */
    /* camp_snprintf( buf, sizeof( buf ), "%.1lf ", (fabs(f)+0.02) ); */
    /* camp_strncat( defn, defn_size, buf ); // strcat( defn, buf ); */

    return( TRUE );
}

bool_t 
inputIf_indpak( const char* defaultDefn, char* defn, size_t defn_size )
{
    long i;
    long def;
    char buf[LEN_BUF+1];
    char* Type[] = {"adc ", "dac ", "motor "};

    def = camp_getIfIndpakSlot( defaultDefn );
    if( !inputInteger( "Slot", def, &i ) )
        return( FALSE );

    camp_snprintf( defn, defn_size, "%d ", i );
    
    /* Change this to a select box with default selected etc like GPIB */

    camp_getIfIndpakType( defaultDefn, buf, sizeof( buf ) );
    if( !inputInteger( "Type", def, &i ) )
    {
        return( FALSE );
    }

    /* Check for bounds here! */

    camp_strncat( defn, defn_size, Type[i] ); // strcat( defn, Type[i] );

    def = camp_getIfIndpakChannel( defaultDefn );
    if( !inputInteger( "Channel", def, &i ) )
    {
        return( FALSE );
    }

    camp_snprintf( buf, sizeof( buf ), "%d ", i );
    camp_strncat( defn, defn_size, buf ); // strcat( defn, buf );

    return( TRUE );
}



