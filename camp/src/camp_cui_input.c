/*
 *  Name:       camp_cui_input.c
 *
 *  Purpose:    Routines to manage a generic text selection window and
 *              a generic text input window.
 *              Higher level routines inputSelectWin, inputFloat and
 *              inputInteger can be called to set up the window, get
 *              input and return a result.
 *
 *  Called by:  Many
 * 
 *  $Log: camp_cui_input.c,v $
 *  Revision 1.10  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.7  2007/08/01 02:07:07  asnd
 *  Minor changes
 *
 *  Revision 1.6  2006/08/05 05:31:12  asnd
 *  Enable -noansi in Linux version.  Better upper/lowercase hot-key handling.
 *
 *  Revision 1.5  2006/04/28 07:49:12  asnd
 *  Watch for invalid selection-variable indices
 *
 *  Revision 1.4  2004/01/28 03:10:34  asnd
 *  Support input of hexadecimal integers.
 *
 *  Revision 1.3  2001/01/15 05:58:16  asnd
 *  Allow wider variety of delete keys.
 *
 *
 *  Revision history:
 *       Oct-1999  DA  Convert for ncurses; different taking of defaults.
 *    14-Dec-1999  TW  Additional include for Linux
 *    21-Jan-2000  DA  multi-column hot-keyed select win
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef linux
#include <string.h>
#endif /* linux */
#include <ctype.h>
#include <curses.h>
#include <math.h>
#include "camp_cui.h"

extern bool_t isANSI;

void
boxWin( WINDOW* win, u_int height, u_int width )
{
    int i;

#ifndef VMS
    if( isANSI )
    {
        box( win, 0, 0 );
        return;
    }
#endif

    if( isANSI )
    {
        wprintw( win, "\033(0" );
    }

    /*
     *  Top
     */
    if( isANSI )
    {
        mvwaddch( win, 0, 0, '\154' );
        for( i = 1; i < width-1; i++ ) waddch( win, '\161' );
        waddch( win, '\153' );
    }
    else
    {
        mvwaddch( win, 0, 0, '+' );
        for( i = 1; i < width-1; i++ ) waddch( win, '-' );
        waddch( win, '+' );
    }

    /*
     *  Left
     */
    if( isANSI )
    {
        for( i = 1; i < height-1; i++ ) mvwaddch( win, i, 0, '\170' );
    }
    else
    {
        for( i = 1; i < height-1; i++ ) mvwaddch( win, i, 0, '|' );
    }

    /*
     *  Bottom
     */
    if( isANSI )
    {
        mvwaddch( win, height-1, 0, '\155' );
        for( i = 1; i < width-1; i++ ) waddch( win, '\161' );
        waddch( win, '\152' );
    }
    else
    {
        mvwaddch( win, height-1, 0, '+' );
        for( i = 1; i < width-1; i++ ) waddch( win, '-' );
        waddch( win, '+' );
    }

    /*
     *  Right
     */
    if( isANSI )
    {
        for( i = 1; i < height-1; i++ ) mvwaddch( win, i, width-1, '\170' );
    }
    else
    { 
        for( i = 1; i < height-1; i++ ) mvwaddch( win, i, width-1, '|' );
    }

    if( isANSI )
    {
        wprintw( win, "\033(B" );
    }
}


int
createSelectWin( SELECTION_WIN* sw, const char* title,
                 int numRows, int numCols,
                 int num_choices, char* choices[],
                 HK_FLAG hotKeyFlag, u_short hotKeys[] )
{
    u_int i;
    u_int height, width;
    int colWidth;
    u_int xPos, yPos;
    char hk;
    u_int lenChoices;

    /*
     *  Find column width based on max length of choices
     */
    lenChoices = 0;
    for( i = 0; i < num_choices; i++ )
    {
        lenChoices = _max( lenChoices, strlen( choices[i] ) );
    }
    /*  Add space at beginning and end */
    colWidth = lenChoices + 2;

    /*
     *  Add space for box
     */
    height = numRows + 2;
    width = colWidth*numCols + 2;
    width = _max( width, strlen( title ) + 4 );

    /*
     *  Limit width to screen width, and back-propagate
     */
    if( width > SCREEN_W )
    {
        width = SCREEN_W;
        colWidth = (width-2)/numCols ;
        lenChoices = colWidth - 1;
    }

    /*
     *  Find start point
     */
    xPos = (SCREEN_W-width)/2;
    yPos = (SCREEN_H-height)/2;

    sw->win = newwin( height, width, yPos, xPos );
    if( sw->win == NULL ) 
    {
        return( CAMP_FAILURE );
    }
    leaveok( sw->win, FALSE );
    scrollok( sw->win, FALSE );
    keypad( sw->win, TRUE );
    immedok( sw->win, FALSE );
    clearok( sw->win, FALSE );
    wtimeout( sw->win, -1);

    sw->height = height;
    sw->width = width;
    sw->numrows = numRows;
    sw->numcols = numCols;
    sw->colwidth = colWidth;
    sw->hotkeyflag = hotKeyFlag;

    sw->subwin = subwin( sw->win, height-2, width-3, yPos+1, xPos+2 );
    if( sw->subwin == NULL ) 
    {
        return( CAMP_FAILURE );
    }
    leaveok( sw->subwin, FALSE );
    scrollok( sw->subwin, FALSE );
    keypad( sw->subwin, TRUE );
    immedok( sw->subwin, FALSE );
    clearok( sw->subwin, FALSE );
    wtimeout( sw->subwin, -1);

    werase( sw->win );
    boxWin( sw->win, sw->height, sw->width );
    wattron( sw->win, A_BOLD );
    mvwaddstr( sw->win, 0, (sw->width-strlen(title))/2, title );
    wattroff( sw->win, A_BOLD );

    /*
     *  Display all the choices, with hotkeys highlighted (if enabled hot)
     */
    for( i = 0; i < num_choices; i++ )
    {
        sw->choices[i] = choices[i];
        if( hotKeyFlag == HK_HOT )  hk = hotKeys[i];
        else  hk = '\0';
        sw->hotkeys[i] = hk;
        writeOneSelection( sw, i );
    }

    wrefresh( sw->win );

    return( CAMP_SUCCESS );
}


void 
deleteSelectWin( SELECTION_WIN* sw )
{
    if( sw->subwin != NULL ) {
        delwin( sw->subwin );
        sw->subwin = NULL;
    }
    if( sw->win != NULL ) {
        delwin( sw->win );
        sw->win = NULL;
    }
    uncoverWindows();
}

void 
writeOneSelection( SELECTION_WIN* sw, u_int i )
{
    char c, hk;
    u_int numRows;
    int j;

    if( sw->hotkeyflag == HK_HOT )  hk = sw->hotkeys[i];
    else  hk = '\0';
    numRows = sw->numrows;

    wmove( sw->subwin, i%numRows, (i/numRows)*(sw->colwidth) );
    for( j = 0; j < sw->colwidth; j++)
    {
        c = sw->choices[i][j];
        if( c == '\0' ) break;
        if( tolower(c) == tolower(hk) )
        {
            wattron( sw->subwin, A_BOLD );
            waddch( sw->subwin, c );
            wattroff( sw->subwin, A_BOLD );
            hk = '\0';  /* only highlight first instance of hot key */
        }
        else
        {
            waddch( sw->subwin, c );
        }
    }
}

void 
hilightCurrSelection( SELECTION_WIN* sw, u_int index )
{
    wattron( sw->subwin, CURSOR_ATTR );
    writeOneSelection( sw, index );
    wattroff( sw->subwin, CURSOR_ATTR );
    wrefresh( sw->subwin );
}

void
unhilightCurrSelection( SELECTION_WIN* sw, u_int index )
{
    writeOneSelection( sw, index );
    wrefresh( sw->subwin );
}

/*
 * inputSelectWin is for selecting an item from a list of choices
 * Hotkeys are assigned automatically if possible.
 */

bool_t
inputSelectWin( const char* title, u_int numChoices, char* choices[],
                u_int defaultIndex, u_int* selectedIndex )
{
    int offset;
    HK_FLAG hotKeyFlag;
    u_short hotKeys[MAX_NUM_SELECTIONS];
    u_int i, j;
    char c, c1;

/*
 *  Use heuristics to determine hotkeys from the set of selections.
 *
 *  Say the selections are all identical in the first n characters
 *  (likely n == 0) and let o = n+1.
 *  If any of the selections are only n characters long, then use no
 *  hot keys (HK_NONE).
 *  If the o'th characters are all different, when compared "any case",
 *  then use the o'th characters as hot keys (HK_HOT).
 *  If there are some repetitions in the set of o'th characters, so
 *  hot keys would be ambiguous, then use the o'th characters as warm
 *  keys (move the focus, but require Enter for confirmation).  
 *  When a hot/warm key is pressed, the first attempt at a match is
 *  make case-sensitive.  If that fails, then repeat in a case-insensitve
 *  search.
 *
 *  For example, selections "muon spin rotation" give hot keys "m s r",
 *  so pressing "s" will select "spin".
 *  The list "/tyCo/1 /tyCo/2 /tyCo/3 /tyCo/4 /tyCo/5" gives hot keys
 *  "1 2 3 4 5".
 *  The list "muon spin rotation relaxation resonance" gives *warm* keys
 *  "m s r r r"; pressing "r" moves the focus to "rotation", but the user
 *  must press Enter to select any choice.
 *  The list "rotation relaxation resonance" generates warm keys "o e e".
 */

    if( numChoices < 1 ) return( FALSE );

    if( numChoices == 1 )
    {
        hotKeyFlag = HK_HOT;
	offset = 0;
    }
    else
    {
        hotKeyFlag = HK_WARM;
        for( offset = 0; hotKeyFlag == HK_WARM ; offset++ )
        {
            /*  check for any different (possibly HOT) or any null (NONE) */

            c1 = choices[0][offset];
            if( c1 == '\0' )
            {
                hotKeyFlag = HK_NONE;
                break;
            }
            for( i = 1; i < numChoices; i++ )
            {   /*  loop over choices, comparing characters */
                c = choices[i][offset];
                if( c == '\0' )
                {
                    /* an entry ran out of length -- no hot keys */
                    hotKeyFlag = HK_NONE;
                    break;
                }
                if( tolower(c) != tolower(c1) )
                {
                    /* found a difference, so flag that fact, so we will use this offset */
                    hotKeyFlag = HK_HOT; /* tentative */
                }
                c1 = c;
            }
        }
	offset--;

        /* Check validity of hot keys (hot vs warm) by looking for any duplications */

        for( i = 1; i < numChoices; i++ )
        {
            if( hotKeyFlag != HK_HOT )  break;

            for( j = 0; j < i; j++ )
            {
                if( tolower(choices[i][offset]) == tolower(choices[j][offset]) )
                {
                    hotKeyFlag = HK_WARM;
                    break;
                }
            }
        }
    } /* end if numChoices */

    if( hotKeyFlag != HK_NONE )
    {
        for( i = 0; i < numChoices; i++ )
        {
            hotKeys[i] = choices[i][offset];
        }
    }

    return( inputHotSelectWin( title, numChoices, choices,
            defaultIndex, selectedIndex, hotKeyFlag, hotKeys ) );

}

bool_t
inputHotSelectWin( const char* title, u_int numChoices, char* choices[],
                u_int defaultIndex, u_int* selectedIndex,
                HK_FLAG hotKeyFlag, u_short hotKeys[] )
{
    bool_t selectionFlag = FALSE;
    bool_t doneFlag = FALSE;
    u_int i, j;
    u_short inKey;
    u_int currChoice;
    int numRows, numCols;
    SELECTION_WIN selectWin;

    if( numChoices < 1 ) return( FALSE );

    if( defaultIndex >= numChoices ) 
    {
        defaultIndex = 0;
    }

    /*
     *  Find how many columns are needed
     */
    numCols = 1 + (numChoices - 1) / (SCREEN_H - 3) ;
    numRows = 1 + (numChoices - 1) / numCols ;

    createSelectWin( &selectWin, title, numRows, numCols,
                        numChoices, choices, hotKeyFlag, hotKeys );
    currChoice = defaultIndex;
    hilightCurrSelection( &selectWin, currChoice );

    while( !doneFlag ) 
    {
        inKey = getKey();
	
        switch( inKey ) 
        {
          case KEY_UP:
#if KEY_UP_2 != KEY_UP
          case KEY_UP_2:
#endif
#if KEY_UP_3 != KEY_UP
          case KEY_UP_3:
#endif
            unhilightCurrSelection( &selectWin, currChoice );
            if( currChoice == 0 ) currChoice = numChoices-1;
            else currChoice--;
            hilightCurrSelection( &selectWin, currChoice );
            break;

          case KEY_DOWN:
#if KEY_DOWN_2 != KEY_DOWN
          case KEY_DOWN_2:
#endif
#if KEY_DOWN_3 != KEY_DOWN
          case KEY_DOWN_3:
#endif
            unhilightCurrSelection( &selectWin, currChoice );
            if( currChoice == numChoices-1 ) currChoice = 0;
            else currChoice++;
            hilightCurrSelection( &selectWin, currChoice );
            break;

          case KEY_RIGHT:
            unhilightCurrSelection( &selectWin, currChoice );
            currChoice = ( (int)currChoice + numRows) % (numRows * numCols);
            currChoice = _min( currChoice, numChoices-1 );
            hilightCurrSelection( &selectWin, currChoice );
            break;

          case KEY_LEFT:
            unhilightCurrSelection( &selectWin, currChoice );
            currChoice = ( currChoice + (numCols-1)*numRows ) % (numRows * numCols);
            currChoice = _min( currChoice, numChoices-1 );
            hilightCurrSelection( &selectWin, currChoice );
            break;

          case KEY_RETURN:
#if KEY_ENTER != KEY_RETURN
          case KEY_ENTER:
#endif
            *selectedIndex = currChoice;
            selectionFlag = TRUE;
            doneFlag = TRUE;
            break;

          case KEY_CANCEL_1:
#if KEY_CANCEL_1 != KEY_CANCEL_2
          case KEY_CANCEL_2:
#endif
#ifdef KEY_RESIZE
          case KEY_RESIZE:
#endif
            selectionFlag = FALSE;
            doneFlag = TRUE;
            break;

          default:
            if( hotKeyFlag != HK_NONE )
            {
                /* Apparent bug in gcc causes second try to be skipped when written as 
                 * for( i = 0; i < numChoices; i++ )
                 * {
                 *      if( inKey == hotKeys[i] )  break;
                 * }
                 * if( i < numChoices ) ...
                 */

                j = numChoices;  /* indicates no selection found (yet) */

                /* First try for match: case sensitive */
                for( i = 0; i < numChoices; i++ )
                {
		    if( inKey == hotKeys[i] )
                    {
                        j = i;
                        break;
                    }
                }
                /* Second try for match: case insensitive */
                if( j == numChoices ) 
                {
                    for( i = 0; i < numChoices; i++ )
                    {
                        if( tolower(inKey) == tolower(hotKeys[i]) )
                        {
                            j = i;
                            break;
                        }
                    }
                }
                /* 
                 * Found a match (on either try).  Highlight it.  If "hot" then select it.
                 */
                if( j < numChoices ) 
                {
                    unhilightCurrSelection( &selectWin, currChoice );
                    currChoice = j;
                    hilightCurrSelection( &selectWin, currChoice );
                    if( hotKeyFlag == HK_HOT )
                    {
                        *selectedIndex = currChoice;
                        selectionFlag = TRUE;
                        doneFlag = TRUE;
                    }
                }
            }
            break;
        }
    }
    deleteSelectWin( &selectWin );
#ifdef KEY_RESIZE
    if( inKey == KEY_RESIZE )
      {
        refreshScreen();
      }
#endif
    return( selectionFlag );
}


int 
createTextInputWin( TEXT_INPUT_WIN *ti, const char *title, int textLen )
{
    int width;
    int xPos, yPos;

    width = _max( textLen + 2, strlen( title ) + 4 );
    width = _min( width, SCREEN_W );

    yPos = (SCREEN_H-TEXTINPUTWIN_H)/2;
    xPos = (SCREEN_W-width)/2;

    ti->win = newwin( TEXTINPUTWIN_H, width, yPos, xPos );
    if( ti->win == (WINDOW*)NULL ) 
    {
        return( CAMP_FAILURE );
    }
    leaveok( ti->win, FALSE );
    scrollok( ti->win, FALSE );
    keypad( ti->win, TRUE );
    immedok( ti->win, FALSE );
    clearok( ti->win, FALSE );
    wtimeout( ti->win, -1);

    ti->subwin = subwin( ti->win, TEXTINPUTWIN_H-2, width-4, yPos+1, xPos+2 );
    if( ti->subwin == (WINDOW*)NULL ) 
    {
        return( CAMP_FAILURE );
    }
    werase( ti->win );
    boxWin( ti->win, TEXTINPUTWIN_H, width );
    wattron( ti->win, A_BOLD );
    mvwaddstr( ti->win, 0, ( width - strlen( title ) )/2, title );
    wattroff( ti->win, A_BOLD );

    wattron( ti->subwin, A_REVERSE );
    werase( ti->subwin );
#ifdef linux
    /* Paint the input line bright: */
    whline( ti->subwin, ' ', width-4 );
#endif
    wrefresh( ti->win );
    wrefresh( ti->subwin );

    return( CAMP_SUCCESS );
}


void 
deleteTextInputWin( TEXT_INPUT_WIN* ti )
{
    if( ti->subwin != (WINDOW*)NULL ) {
        delwin( ti->subwin );
        ti->subwin = (WINDOW*)NULL;
    }
    if( ti->win != (WINDOW*)NULL ) {
        delwin( ti->win );
        ti->win = (WINDOW*)NULL;
    }
    uncoverWindows();
}


bool_t 
inputFloat( const char* title, double defaultValue, double* pInput )
{
    int camp_status;
    u_int i;
    TEXT_INPUT_WIN textInputWin;
    int startPosition;
    int position;
    int decimalPosition = -1;
    int expPosition = -1;
    char* pe;
    u_long inKey = 0;
    int y, x;
    int width;
    char bufd[LEN_BUF+1], buf[LEN_BUF+1];
    bool_t initial;

    width = _max( strlen( title ), 12 );

    if( ( (fabs(defaultValue) > 100000. ) || (fabs(defaultValue) < 0.0001 ) )
	   &&  ( defaultValue != 0.0 ) )
        camp_snprintf( bufd, sizeof( bufd ), "%e", defaultValue );
    else
        camp_snprintf( bufd, sizeof( bufd ), "%f", defaultValue );

    decimalPosition = (int) ((long)strchr( bufd, '.' ) - (long)bufd);

    if( ( pe = strchr( bufd, 'e' ) ) == NULL )
    {
      expPosition = -1;
        /* remove trailing zeros (when there is no exponent part) */
      while( ( ( i = strlen(bufd) - 1 ) > decimalPosition + 1 ) && 
	       ( bufd[i] == '0' ) )
        {
  	    bufd[i] = '\0';
        }
    }
    else
    {
        expPosition = (int) (pe - bufd);
    }

    camp_status = createTextInputWin( &textInputWin, title, width );
    if( _failure( camp_status ) ) return( FALSE );

    getyx( textInputWin.subwin, y, startPosition );
    initial = TRUE;
    camp_strncpy( buf, sizeof( buf ), bufd ); // strcpy( buf, bufd );
    mvwaddstr( textInputWin.subwin, y, startPosition, buf );
    position = startPosition + strlen( buf );
    wrefresh( textInputWin.subwin );

    while( ( inKey != KEY_CANCEL_1 )  
        && ( inKey != KEY_CANCEL_2 ) 
#ifdef KEY_RESIZE
        && ( inKey != KEY_RESIZE ) 
#endif
        )
    {
        inKey = getKey();

        if( initial )
        {
            if( inKey != KEY_DEL && inKey != KEY_DC && inKey != KEY_BACKSPACE && inKey != KEY_CTRL_H
                && inKey != KEY_LEFT && inKey != KEY_RIGHT )
            {
                if( position > startPosition )
                {   /* erase default value (like CTRL_U) */
                    wmove( textInputWin.subwin, y, startPosition );
                    for( ; position > startPosition; --position)
                    {
                        waddch( textInputWin.subwin, ' ');
                    }
                    wmove( textInputWin.subwin, y, startPosition );
                    decimalPosition = -1;
                    expPosition = -1;
                    buf[0] = '\0';
                }
            }
            initial = FALSE;
        }

        if( isdigit( inKey ) )
        {
            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        } 
        else if( (inKey == '-') || (inKey == '+') )
        {
            if( ( position - startPosition ) != expPosition + 1  &&
		( position - startPosition ) != 0 ) continue;

            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        }
        else if( ( inKey == KEY_PERIOD ) && 
		 ( decimalPosition < 0 ) && ( expPosition < 0 ) ) 
        {
            decimalPosition = position;
            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        } 
        else if( ( ( inKey == 'e' ) || ( inKey == 'E' ) ) && 
		 ( expPosition < 0 ) ) 
        {
            expPosition = position;
            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        } 
        else if( ( inKey == KEY_DEL || inKey == KEY_DC || inKey == KEY_BACKSPACE || inKey == KEY_CTRL_H )
                 && ( position > startPosition ) )
        {
            position--;
            if( position == decimalPosition ) decimalPosition = -1;
            if( position == expPosition ) expPosition = -1;
            mvwaddch( textInputWin.subwin, y, position, ' ' );
            wmove( textInputWin.subwin, y, position );
            buf[position-startPosition] = '\0';
        } 
        else if( inKey == KEY_CTRL_U || inKey == KEY_UP || inKey == KEY_UP_2 )
        {
            if ( position > startPosition )
            {
                wmove( textInputWin.subwin, y, startPosition );
                for( ; position > startPosition; --position)
                {
                    waddch( textInputWin.subwin, ' ');
                }
                wmove( textInputWin.subwin, y, startPosition );
                decimalPosition = -1;
                expPosition = -1;
                buf[0] = '\0';
            }
            if( inKey == KEY_UP || inKey == KEY_UP_2 )
            {
                camp_strncpy( buf, sizeof( buf ), bufd ); // strcpy( buf, bufd );
                expPosition = ( pe == NULL )? -1 : (int) (pe - bufd);
                decimalPosition = (int) ((long)strchr( bufd, '.' ) - (long)bufd);
                mvwaddstr( textInputWin.subwin, y, startPosition, buf );
                position = startPosition + strlen( buf );
            }
        }
        else if( ( inKey == KEY_RETURN ) || ( inKey == KEY_ENTER ) )
        {
	    if( position == startPosition )
	    {
		*pInput = defaultValue;
	    }
	    else
	    {
		buf[position-startPosition] = '\0';
		*pInput = strtod( buf, NULL );
	    }
            deleteTextInputWin( &textInputWin );
            return( TRUE );
        }
        else
        {
            continue;
        }
        wrefresh( textInputWin.subwin );
    }

    deleteTextInputWin( &textInputWin );
#ifdef KEY_RESIZE
    if( inKey == KEY_RESIZE )
      {
        refreshScreen();
      }
#endif
    return( FALSE );
}


bool_t 
inputInteger( const char* title, long defaultValue, long* pInput )
{
    int camp_status;
    u_int i;
    TEXT_INPUT_WIN textInputWin;
    int startPosition;
    int position;
    u_long inKey = 0;
    int y, x;
    int width;
    char buf[LEN_BUF+1], bufd[LEN_BUF+1];
    bool_t initial;

    width = _max( strlen( title ), 10 );
    camp_snprintf( bufd, sizeof( bufd ), "%d", defaultValue );

    camp_status = createTextInputWin( &textInputWin, title, width );
    if( _failure( camp_status ) ) return ( FALSE );

    getyx( textInputWin.subwin, y, startPosition );
    initial = TRUE;
    camp_strncpy( buf, sizeof( buf ), bufd ); // strcpy( buf, bufd );
    mvwaddstr( textInputWin.subwin, y, startPosition, buf );
    position = startPosition + strlen( buf );
    wrefresh( textInputWin.subwin );

    while( ( inKey != KEY_CANCEL_1 )  
        && ( inKey != KEY_CANCEL_2 ) 
#ifdef KEY_RESIZE
        && ( inKey != KEY_RESIZE ) 
#endif
        )
    {
        inKey = getKey();

        if( initial )
        {
            if( inKey != KEY_DEL && inKey != KEY_DC && inKey != KEY_BACKSPACE && inKey != KEY_CTRL_H
                && inKey != KEY_LEFT && inKey != KEY_RIGHT )
            {
                if( position > startPosition )
                {   /* erase default value (like CTRL_U) */
                    wmove( textInputWin.subwin, y, startPosition );
                    for( ; position > startPosition; --position)
                    {
                        waddch( textInputWin.subwin, ' ');
                    }
                    wmove( textInputWin.subwin, y, startPosition );
                    buf[0] = '\0';
                }
            }
            initial = FALSE;
        }

        if( isdigit( inKey ) ) 
        {
            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        } 
        else if( inKey == '-' )
        {
            if( (position-startPosition) != 0 ) continue;

            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        }
        else if( inKey == 'x' )
        {
            if( (position-startPosition) != 1 || buf[0] != '0' ) continue;

            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        }
        else if( (inKey == KEY_DEL || inKey == KEY_DC || inKey == KEY_BACKSPACE || inKey == KEY_CTRL_H)
                 && (position > startPosition) ) 
        {
            getyx( textInputWin.subwin, y, x );
            mvwaddch( textInputWin.subwin, y, x-1, ' ' );
            wmove( textInputWin.subwin, y, x-1 );
            position--;
            buf[position-startPosition] = '\0';
        } 
        else if( inKey == KEY_CTRL_U || inKey == KEY_UP || inKey == KEY_UP_2 )
        {
            if( position > startPosition )
            {
                wmove( textInputWin.subwin, y, startPosition );
                for( ; position > startPosition; --position )
                {
                    waddch( textInputWin.subwin, ' ' );
                }
                wmove( textInputWin.subwin, y, startPosition );
                buf[0] = '\0';
            }
            if( inKey == KEY_UP || inKey == KEY_UP_2 )
            {
                camp_strncpy( buf, sizeof( buf ), bufd ); // strcpy( buf, bufd );
                mvwaddstr( textInputWin.subwin, y, startPosition, buf );
                position = startPosition + strlen( buf );
            }
        }
        else if( inKey == KEY_RETURN || inKey == KEY_ENTER )
        {
	    if( position == startPosition )
            {
                *pInput = defaultValue;
            }
            else
            {
                buf[position-startPosition] = '\0';
                if( strchr( buf, 'x') )
                    *pInput = strtol( buf, (char**)NULL, 0 );
                else
                    *pInput = atol( buf );
            }
            deleteTextInputWin( &textInputWin );
            return( TRUE );
        }
        else
        {
            continue;
        }
        wrefresh( textInputWin.subwin );
    }

    deleteTextInputWin( &textInputWin );
#ifdef KEY_RESIZE
    if( inKey == KEY_RESIZE )
        refreshScreen();
#endif
    return( FALSE );
}


bool_t 
inputString( const char* title, u_int len, const char* defaultValue, char* input, size_t input_size,
             bool_t (*charAllowed)( char ) )
{
    int camp_status;
    u_int i;
    TEXT_INPUT_WIN textInputWin;
    int startPosition;
    int position;
    u_short inKey = 0;
    int y, x;
    char buf[LEN_BUF+1];
    bool_t initial;

    camp_status = createTextInputWin( &textInputWin, title, len );
    if( _failure( camp_status ) ) return( FALSE );

    camp_snprintf( buf, sizeof( buf ), "%s", defaultValue );
    getyx( textInputWin.subwin, y, startPosition );
    waddstr( textInputWin.subwin, buf );
    wrefresh( textInputWin.subwin );
    getyx( textInputWin.subwin, y, position );
    initial = TRUE;

    while( ( inKey != KEY_CANCEL_1 )  
        && ( inKey != KEY_CANCEL_2 ) 
#ifdef KEY_RESIZE
        && ( inKey != KEY_RESIZE ) 
#endif
        )
    {
        inKey = getKey();

        if( initial )
        {
            if( inKey != KEY_DEL && inKey != KEY_DC && inKey != KEY_BACKSPACE && inKey != KEY_CTRL_H
                         && inKey != KEY_LEFT && inKey != KEY_RIGHT 
	                 && inKey != KEY_RETURN && inKey != KEY_ENTER  )
            {
                if( position > startPosition )
                {   /* erase default value (like CTRL_U) */
                    wmove( textInputWin.subwin, y, startPosition );
                    for( ; position > startPosition; --position)
                    {
                        waddch( textInputWin.subwin, ' ');
                    }
                    wmove( textInputWin.subwin, y, startPosition );
                    buf[0] = '\0';
                }
            }
            initial = FALSE;
        }

        if( charAllowed( inKey ) && ( position < len ) )
        {
            waddch( textInputWin.subwin, (char)inKey );
            buf[position-startPosition] = inKey;
            position++;
        } 
        else if( ( inKey == KEY_DEL || inKey == KEY_DC || inKey == KEY_BACKSPACE || inKey == KEY_CTRL_H )
                 && ( position > startPosition ) ) 
        {
            position--;
            mvwaddch( textInputWin.subwin, y, position, ' ' );
            wmove( textInputWin.subwin, y, position );
            buf[position-startPosition] = '\0';
        } 
        else if( inKey == KEY_CTRL_U || inKey == KEY_UP || inKey == KEY_UP_2 )
        {
            if( position > startPosition )
            {
                wmove( textInputWin.subwin, y, startPosition );
                for( ; position > startPosition; --position )
                {
                    waddch( textInputWin.subwin, ' ' );
                }
                wmove( textInputWin.subwin, y, startPosition );
                buf[0] = '\0';
            }
            if( inKey == KEY_UP || inKey == KEY_UP_2 )
            {
                camp_snprintf( buf, sizeof( buf ), "%s", defaultValue );
                mvwaddstr( textInputWin.subwin, y, startPosition, buf );
                getyx( textInputWin.subwin, y, position );
            }
        }
        else if( ( inKey == KEY_RETURN ) || ( inKey == KEY_ENTER ) )
        {
            buf[position-startPosition] = '\0';
            camp_strncpy( input, input_size, buf ); // strcpy( input, buf );
            deleteTextInputWin( &textInputWin );
            return( TRUE );
        }
        wrefresh( textInputWin.subwin );
    }
    deleteTextInputWin( &textInputWin );
#ifdef KEY_RESIZE
    if( inKey == KEY_RESIZE )
        refreshScreen();
#endif
    return( FALSE );
}
