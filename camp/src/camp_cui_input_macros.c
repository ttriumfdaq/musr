/*
 *  Name:       camp_cui_input_macros.c
 *
 *  Purpose:    Routines to prompt for and return information relating to
 *              CAMP files, instruments and variables.
 *
 *  Revision history:
 *    14-Dec-1999  TW  Get u_int/u_long types distinct for gcc
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <curses.h>
#include "camp_cui.h"

//static char* toggleNames[] = { "off", "on" };

extern DIRENT*          pSys_pDir; // 20140326  TW  replacement for pSys->pDir


bool_t
isIdentChar( char c )
{
    if( isalnum( c ) || ( c == '_' ) || ( c == '-' ) ) return( TRUE );
    return( FALSE );
}


bool_t
isPathChar( char c )
{
    if( isalnum( c ) || ( c == '_' ) || ( c == '-' ) || ( c == '/' ) ) return( TRUE );
    return( FALSE );
}


bool_t
isPrintChar( char c )
{
    return( isprint( c ) );
}


bool_t 
inputSetupFile( const char* title, const char* defaultFilename, char* filename, size_t filename_size, const char* mask )
{
    DIRENT* pDirEnt;
    char* names[MAX_NUM_SELECTIONS];
    u_int num;
    u_int index = 0;
    u_int defaultIndex;
    int i;
    char str[LEN_STR+1];

    if( pSys_pDir == NULL ) 
    {
#if CAMP_CUI_LOG
	// _camp_log( "pSys_pDir == NULL" );
#endif // CAMP_CUI_LOG
	return( FALSE );
    }

    /* 
     *  get the filename -  note: first entry in pDir
     *                            is the filespec
     */

    for( i = 0; i < MAX_NUM_SELECTIONS; i++ ) names[i] = 0;

#if CAMP_CUI_LOG
    // _camp_log( "pSys_pDir first entry: '%s'", pSys_pDir->filename );
#endif // CAMP_CUI_LOG

    num = 0;
    defaultIndex = 0;
    for( pDirEnt = pSys_pDir->pNext; 
         pDirEnt != NULL; 
         pDirEnt = pDirEnt->pNext )
    {
        if( ( pDirEnt->filename != NULL ) && ( defaultFilename != NULL ) )
        {
            if( streq( defaultFilename, pDirEnt->filename ) ) 
                defaultIndex = num;
        }

	str[0] = '\0' ;
        if( mask != NULL )
        {
            sscanf( pDirEnt->filename, mask, str );
        }
        else
        {
            camp_strncpy( str, sizeof( str ), pDirEnt->filename ); // strcpy( str, pDirEnt->filename );
        }

#if CAMP_CUI_LOG
	// _camp_log( "name %d:'%s'", num, str );
#endif // CAMP_CUI_LOG

        names[num++] = strdup( str ); // 

        if( num >= MAX_NUM_SELECTIONS )  
	{
            break;
	}
    }

    sortNames( names, num );
    
    if( !inputSelectWin( title, num, names, defaultIndex, &index ) )
    {
        for( i = 0; i < num; i++ ) _free( names[i] );
        return( FALSE );
    }

    camp_strncpy( filename, filename_size, names[index] ); // strcpy( filename, names[index] );

    for( i = 0; i < num; i++ ) _free( names[i] );
    return( TRUE );
}


bool_t 
inputInsType( char* typeIdent, size_t typeIdent_size )
{
    INS_TYPE* pInsType;
    char* insTypeNames[MAX_NUM_SELECTIONS];
    u_int insTypeIndex;
    u_int numTypes = 0;

    for( pInsType = pSys->pInsTypes; 
         pInsType != NULL; 
         pInsType = pInsType->pNext )
    {
        insTypeNames[numTypes++] = pInsType->ident;
        if( numTypes == MAX_NUM_SELECTIONS )
            break;
    }
    if( !inputSelectWin( "Instrument type", numTypes, insTypeNames,
                            0, &insTypeIndex ) )
        return( FALSE );

    camp_strncpy( typeIdent, typeIdent_size, insTypeNames[insTypeIndex] ); // strcpy( typeIdent, insTypeNames[insTypeIndex] );

    return( TRUE );
}


bool_t 
inputInsAvail( char* ident, size_t ident_size, char* typeIdent, size_t typeIdent_size )
{
    INS_AVAIL* pInsAvail;
    char* names[MAX_NUM_SELECTIONS];
    u_int index;
    int num;
    int last_num;
    char* otherText = "< others >";

    num = 0;
    for( pInsAvail = pSys->pInsAvail; 
         pInsAvail != NULL; 
         pInsAvail = pInsAvail->pNext )
    {
        if( num == MAX_NUM_SELECTIONS ) 
            break;
        names[num++] = pInsAvail->ident;
    }

    if( num == 0 )
    {
        camp_strncpy( ident, ident_size, otherText ); // strcpy( ident, otherText );
        camp_strncpy( typeIdent, typeIdent_size, "" ); // strcpy( typeIdent, "" );
        return( TRUE );
    }

    last_num = num;
    names[num++] = otherText;

    if( !inputSelectWin( "Add instrument", num, names, 0, &index ) )
        return( FALSE );

    if( index == last_num )
    {
        camp_strncpy( ident, ident_size, otherText ); // strcpy( ident, otherText );
        camp_strncpy( typeIdent, typeIdent_size, "" ); // strcpy( typeIdent, "" );
        return( TRUE );
    }

    for( pInsAvail = pSys->pInsAvail; 
         pInsAvail != NULL; 
         pInsAvail = pInsAvail->pNext )
    {
        if( streq( pInsAvail->ident, names[index] ) )
        {
            camp_strncpy( ident, ident_size, pInsAvail->ident ); // strcpy( ident, pInsAvail->ident );
            camp_strncpy( typeIdent, typeIdent_size, pInsAvail->typeIdent ); // strcpy( typeIdent, pInsAvail->typeIdent );
            return( TRUE );
        }
    }

    return( FALSE );
}


bool_t 
inputInsLock( const char* path, bool_t* pFlag )
{
    CAMP_VAR* pVar;
    bool_t defaultIndex;
    u_int index;
/*
    if( !inputVar( path, "Instrument", CAMP_VAR_TYPE_INSTRUMENT,
                        CAMP_VAR_ATTR_SHOW ) )
    {
        return( FALSE );
    }
*/
    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) return( FALSE );
/*
    defaultIndex = !( pVar->core.status & CAMP_INS_ATTR_LOCKED );
    if( !inputSelectWin( "Toggle", 2, toggleNames, defaultIndex, &index ) )
        return( FALSE );

    *pFlag = ( index != 0 );
*/
    *pFlag = !( pVar->core.status & CAMP_INS_ATTR_LOCKED );

    return( TRUE );
}


bool_t 
inputInsLine( const char* path, bool_t* pFlag )
{
    CAMP_VAR* pVar;
    u_int index;
    bool_t defaultIndex;
    CAMP_INSTRUMENT* pIns;
/*
    if( !inputVar( path, "Instrument", CAMP_VAR_TYPE_INSTRUMENT,
                        CAMP_VAR_ATTR_SHOW ) )
    {
        return( FALSE );
    }
*/
    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) return( FALSE );

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;
/*
    if( pIns->pIF == NULL ) defaultIndex = 1;
    else defaultIndex = !( pIns->pIF->status & CAMP_IF_ONLINE );

    if( !inputSelectWin( "Toggle", 2, toggleNames, defaultIndex, &index ) )
        return( FALSE );

    *pFlag = ( index != 0 );
*/
    if( pIns->pIF == NULL ) return( FALSE );

    *pFlag = !( pIns->pIF->status & CAMP_IF_ONLINE );

    return( TRUE );
}


bool_t 
inputVar( char* path, size_t path_size, const char* title, u_long typeMask, u_long attrMask )
{
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_start;
    char* dataNames[MAX_NUM_SELECTIONS];
    int numData;
    u_int dataIndex;

    pVar_start = camp_varGetp_clnt( currViewPath );
    if( pVar_start == NULL )
    {
        pVar_start = pVarList;
    }
    else
    {
        pVar_start = pVar_start->pChild;
    }

    numData = 0;
    for( pVar = pVar_start; pVar != NULL; pVar = pVar->pNext )
    {
        if( !( pVar->core.varType & typeMask ) ) continue;
        if( !( pVar->core.attributes & attrMask ) ) continue;
        if( !( pVar->core.status & CAMP_VAR_ATTR_SHOW ) ) continue;
        dataNames[numData++] = pVar->core.ident;

        if( numData == MAX_NUM_SELECTIONS )
	{
            break;
	}
    }

    if( !inputSelectWin( title, numData, dataNames, 0, &dataIndex ) )
    {
        return( FALSE );
    }

    camp_strncpy( path, path_size, currViewPath ); // strcpy( path, currViewPath );
    camp_pathDown( path, path_size, dataNames[dataIndex] );

    return( TRUE );
}


bool_t 
inputVarTol( CAMP_VAR* pVar, u_long* pTolType, double* pTol )
{
    char* types[2];
    u_int uiTolType; /* Get types right 14-Dec-1999 TW */

    types[0] = "Plus/minus";
    types[1] = "Percent";

    if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != 
        CAMP_VAR_TYPE_NUMERIC ) 
    {
        return( FALSE );
    }

    if( !inputSelectWin( "Tolerance method", 2, types, 0, &uiTolType ) )
    {
        return( FALSE );
    }

    *pTolType = uiTolType; /* Get types right 14-Dec-1999 TW */

    if( !inputFloat( "Tolerance value", pVar->spec.CAMP_VAR_SPEC_u.pNum->tol, pTol ) )
    {
        return( FALSE );
    }

    return( TRUE );
}


bool_t 
inputVarInteger( CAMP_VAR* pVar, double* pdVal )
{
    long iVal;

    if( !inputInteger( "Input value (integer)", 
                        (long)pVar->spec.CAMP_VAR_SPEC_u.pNum->val, &iVal ) )
    {
        return( FALSE );
    }

    *pdVal = (double)iVal;

    return( TRUE );
}


bool_t 
inputVarFloat( CAMP_VAR* pVar, double* pdVal )
{
    return( inputFloat( "Input value (floating point)", 
            pVar->spec.CAMP_VAR_SPEC_u.pNum->val, pdVal ) );
}


bool_t 
inputVarSelection( CAMP_VAR* pVar, int* piVal )
{
    CAMP_SELECTION* pSel;
    CAMP_SELECTION_ITEM* pItem;
    char* names[MAX_NUM_SELECTIONS];
    u_int num = 0;
    u_int index = 0;

    pSel = pVar->spec.CAMP_VAR_SPEC_u.pSel;

    for( pItem = pSel->pItems; pItem != NULL; pItem = pItem->pNext )
    {
        names[num++] = pItem->label;
        if( num == MAX_NUM_SELECTIONS ) 
            break;
    }
    /*
     *  get the data item
     */
    if( !inputSelectWin( "Label", pSel->num, names, pSel->val, &index ) )
    {
        return( FALSE );
    }
    *piVal = index;
    return( TRUE );
}


bool_t 
inputVarString( CAMP_VAR* pVar, char* strVal, size_t strVal_size )
{
    return( inputString( "Input string", LEN_STRING, 
			 pVar->spec.CAMP_VAR_SPEC_u.pStr->val, strVal, strVal_size, isPrintChar ) );
}

bool_t 
inputVarTitle( const char* path, char* strTitle, size_t strTitle_size )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) return( FALSE );

    return( inputString( "Title", LEN_TITLE, 
            pVar->core.title, strTitle, strTitle_size, isPrintChar ) );
}

bool_t 
inputVarUnits( const char* path, char* strUnits, size_t strUnits_size )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) return( FALSE );

    return( inputString( "Units", LEN_UNITS, 
			 pVar->spec.CAMP_VAR_SPEC_u.pNum->units, strUnits, strUnits_size, isPrintChar ) );
}

bool_t 
inputVarPoll( const char* path, bool_t* pFlag, double* pInterval )
{
    CAMP_VAR* pVar;
    u_int dataIndex;
    u_int defaultIndex;
/*
    if( !inputVar( path, "Variable", CAMP_MAJOR_VAR_TYPE_MASK, 
                        CAMP_VAR_ATTR_POLL ) )
        return( FALSE );
*/
    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the linked data
     */
    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) return( FALSE );

/*
    defaultIndex = ( pVar->core.status & CAMP_VAR_ATTR_POLL ) ? 0 : 1;
    if( !inputSelectWin( "Toggle", 2, toggleNames, defaultIndex, &dataIndex ) )
        return( FALSE );

    if( *pFlag = ( dataIndex != 0 ) )
*/
    *pFlag = !( pVar->core.status & CAMP_VAR_ATTR_POLL );
    if( *pFlag )
    {
        if( !inputFloat( "Poll interval", pVar->core.pollInterval, pInterval ) )
	{
            return( FALSE );
	}
    }
    else *pInterval = 0.0;

    return( TRUE );
}


bool_t 
inputVarAlarm( const char* path, bool_t* pFlag, char* alarmAction, size_t alarmAction_size )
{
    CAMP_VAR* pVar;
    char* dataNames[MAX_NUM_SELECTIONS];
    u_int numData;
    u_int dataIndex;
    ALARM_ACT* pAlarmAct;
    u_int defaultIndex;
/*
    if( !inputVar( path, "Variable", CAMP_MAJOR_VAR_TYPE_MASK, 
                        CAMP_VAR_ATTR_ALARM ) )
        return( FALSE );
*/
    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the linked data
     */
    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) return( FALSE );
/*
    defaultIndex = ( pVar->core.status & CAMP_VAR_ATTR_ALARM ) ? 0 : 1;
    if( !inputSelectWin( "Toggle", 2, toggleNames, defaultIndex, &dataIndex ) )
        return( FALSE );

    if( *pFlag = ( dataIndex != 0 ) )
*/
    *pFlag = !( pVar->core.status & CAMP_VAR_ATTR_ALARM );
    if( *pFlag )
    {
        numData = 0;
        defaultIndex = 0;
        for( pAlarmAct = pSys->pAlarmActs; 
             pAlarmAct != NULL; 
             pAlarmAct = pAlarmAct->pNext )
        {
            if( ( pVar->core.alarmAction != NULL ) &&
                ( streq( pAlarmAct->ident, pVar->core.alarmAction ) ) )
                defaultIndex = numData;

            dataNames[numData++] = pAlarmAct->ident;
            if( numData == MAX_NUM_SELECTIONS )
	    {
                break;
	    }
        }

        if( !inputSelectWin( "Alarm action", numData, dataNames,
                            defaultIndex, &dataIndex ) )
	{
            return( FALSE );
	}

        camp_strncpy( alarmAction, alarmAction_size, dataNames[dataIndex] ); // strcpy( alarmAction, dataNames[dataIndex] );
    }
    else 
    {
	camp_strncpy( alarmAction, alarmAction_size, "" ); // strcpy( alarmAction, "" );
    }

    return( TRUE );
}


bool_t 
inputVarLog( const char* path, bool_t* pFlag, char* logAction, size_t logAction_size )
{
    CAMP_VAR* pVar;
    u_int defaultIndex;
    LOG_ACT* pLogAct;
    char* dataNames[MAX_NUM_SELECTIONS];
    u_int numData;
    u_int dataIndex;
/*
    if( !inputVar( path, "Variable", CAMP_MAJOR_VAR_TYPE_MASK, 
                        CAMP_VAR_ATTR_LOG ) )
        return( FALSE );
*/
    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the linked data
     */
    pVar = camp_varGetp_clnt( path );
    if( pVar == NULL ) return( FALSE );
/*
    defaultIndex = ( pVar->core.status & CAMP_VAR_ATTR_LOG ) ? 0 : 1;
    if( !inputSelectWin( "Toggle", 2, toggleNames, defaultIndex, &dataIndex ) )
        return( FALSE );

    if( *pFlag = ( dataIndex != 0 ) )
*/
    *pFlag = !( pVar->core.status & CAMP_VAR_ATTR_LOG );
    if( *pFlag )
    {
        numData = 0;
        defaultIndex = 0;
        for( pLogAct = pSys->pLogActs; 
             pLogAct != NULL; 
             pLogAct = pLogAct->pNext )
        {
            if( ( pVar->core.logAction != NULL ) &&
                ( streq( pVar->core.logAction, pLogAct->ident ) ) )
                defaultIndex = numData;

            dataNames[numData++] = pLogAct->ident;
            if( numData == MAX_NUM_SELECTIONS )
	    {
                break;
	    }
        }

        if( !inputSelectWin( "Log action", numData, dataNames,
                            defaultIndex, &dataIndex ) )
	{
            return( FALSE );
	}

        camp_strncpy( logAction, logAction_size, dataNames[dataIndex] ); // strcpy( logAction, dataNames[dataIndex] );
    }
    else 
    {
	camp_strncpy( logAction, logAction_size, "" ); // strcpy( logAction, "" );
    }

    return( TRUE );
}

/*
 * Sort a list of names, given as an array of pointers.  Uses strcmp and
 * multi-passes (diminishing increment).
 */
void
sortNames( char* names[], int num )
{
  int i,j;
  int incr;
  char* name;

  for ( incr = 1093; incr >= 1 ; incr /= 3 )
    {
      for ( i = incr; i < num; i++ ) 
        {
          name = names[i];
          for ( j = i; j >= incr; j -= incr )
            {
              if ( strcmp( names[j-incr], name ) < 0 ) break;
              names[j] = names[j-incr];
            }
          names[j] = name;
        }
    } 

  return;
}

void
nestBraces( char* str )
{
    char* c;
    int n = 0;

    /* 
     *  Check if braces (if any) nest properly
     */
    for( c = str; *c != '\0'; c++ )
    {
        if ( *c == '{' ) n++ ;
        if ( *c == '}' ) n-- ;
        if( n < 0 ) break;
    }
    if( n == 0 ) return;
    /* 
     *  Non-nesting braces: change all of them to parentheses
     */
    for( c = str; *c != '\0'; c++ )
    {
        if ( *c == '{' ) *c = '(' ;
        if ( *c == '}' ) *c = ')' ;
    }
}
