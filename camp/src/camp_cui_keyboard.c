/*
 *  Name:       camp_cui_keyboard.c
 *
 *  Purpose:    Get keystrokes with or without a timeout of poll_interval.
 *              Update screen whenever we get a timeout.
 *
 *              Provides routines getKey and getKeyUpdate
 *
 *  Revision history:
 *   19-Dec-2000  TW  waitForServer now has parameter
 */

#ifdef VMS
#include ssdef
#include smgdef
#include smgmsg
#endif /* VMS */

#include <curses.h>
#include "camp_cui.h"

/*
 * This window is used for wgetch( statusWin ) because the status window
 * is always visible and never covers any other window.  It is defined 
 * in camp_cui_status.c.
 */

extern WINDOW *statusWin;

static void update();

/*
 *  Not needed, use hook to the keyboard curses defines
 *
u_long keyboardID;

u_long 
initKeyBoard( void )
{
    return( smg$create_virtual_keyboard( &keyboardID, 0, 0, 0, 0 ) );
}
*/


int
getKey()
{

#ifdef VMS

    u_short inKey;

    fflush( stdin );    /* Get spurious input w/o this */

    /* 
     *  May as well use the keyboard ID defined by curses
     */
    smg$read_keystroke(          &(stdkb->_id),        /* keyboardID */
                                     &inKey,        /* inKey */
                                     0,                /* prompt */
                                     0,                /* timeout */
                                     0,                /* display */
                                     0,                /* attribute */
                                     0                /* attributeComplement */
                               );
    return( (int)inKey );

#else

    /* 
     * Get key "from" the status window because it is always visible
     * and covers nothing.  Using getch() brings a blank stdscr to the front.
     */

    int inKey = ERR;
    int iter = 0;
    /* 
     * In order for the window-resize signal handler to work, all
     * (Linux n-)curses getch calls need a timeout, or else they keep
     * waiting for a character after a window resize.  Therefore we
     * use a 100 sec timeout, and repeat until we see a character.
     * Actually, wait only 100 iterations, or 10000 sec, in case 
     * we get an error flag for something other than a timeout.
     * (Even with a long timeout, wgetch() will return KEY_RESIZE 
     * immediately.)
     */
    wtimeout( statusWin, 100000 );
    while( (inKey == ERR) && (++iter<100 ) )
        inKey = wgetch( statusWin );

    return( inKey );

#endif /* VMS */

}


int
getKeyUpdate( WINDOW* win )
{
    extern long poll_interval;

#ifdef VMS
    u_short inKey;
    int vms_status;

    for( ;; )
    {
        fflush( stdin );    /* Get spurious input w/o this */

        /* 
         *  May as well use the keyboard ID defined by curses
         */
        vms_status = smg$read_keystroke( &(stdkb->_id),        /* keyboardID */
                                     &inKey,        /* inKey */
                                     0,                /* prompt */
                                     &poll_interval,/* timeout */
                                     0,                /* display */
                                     0,                /* attribute */
                                     0                /* attributeComplement */
                                   );
        if( _success( vms_status ) || ( vms_status == SMG$_EOF ) )
        {
            return( (int)inKey );
        }
        else
        {
	    update();
        }
    }

#else  /* ! VMS */

    int key;

    wtimeout( win, (poll_interval*1000) );

    for( ;; )
    {
	key = wgetch( win );
        if( key == ERR )
        {
            update();
        }
        else
        {
            wtimeout( win, -1 );
	    return( key );
        }
    }
#endif /* VMS */
}


static void
update()
{
    int camp_status;
    extern enum clnt_stat camp_clnt_stat;
    extern bool_t timedUpdate;
    /* 
     *  Update the data display. 
     *  (See camp_cui_alert.c for info on timedUpdate.)
     */
    timedUpdate = TRUE;
    while( ( ( camp_status = updateAndDisplayData() ) == CAMP_FAIL_RPC ) 
             && ( camp_clnt_stat != RPC_TIMEDOUT ) )
    {
        waitForServer( 1 );
    }
}
