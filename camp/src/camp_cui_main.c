/*
 *  Name:       camp_cui_main.c
 *
 *  Purpose:    Main program for the CAMP CUI
 *
 *  Inputs:     Optional command line parameters:
 *                   -node <host>      - Camp server host name
 *                   -update <intsec>  - Cycle time for refreshes
 *                   -pause <floatsec> - Pause time for transient messages     
 *                   -d <debugLevel>
 *                   -noansi           - should try to use non-graphical
 *                                       characters for screen line drawing
 *
 *  $Log: camp_cui_main.c,v $
 *  Revision 1.16  2018/06/13 00:44:03  asnd
 *  Fix bug on refresh-all. Clarify 'server-down' message.
 *
 *  Revision 1.15  2015/04/21 04:21:34  asnd
 *  Fix static usage() declaration.
 *
 *  Revision 1.14  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.10  2013/04/16 08:10:47  asnd
 *  Change version num. CHANGES MADE NOW WITH DYNAMIC TITLE STRING REQUIRE MATCHING
 *  CUI AND SERVER!  I did not increment server number to "14" (1.4); perhaps should have.
 *
 *  Revision 1.9  2009/04/08 01:57:01  asnd
 *  Symbolic names for clarity (enum didn't compile)
 *
 *  Revision 1.8  2008/04/08 06:53:26  asnd
 *  Avoid reading variables twice with page-updates (space-bar)
 *
 *  Revision 1.7  2005/07/13 04:13:30  asnd
 *  Command-line options to adjust behaviour
 *
 *  Revision 1.6  2004/01/28 03:13:26  asnd
 *  Add VME interface type. Make alert's BEEP more periodic.
 *
 *  Revision 1.5  2001/12/19 18:05:09  asnd
 *  Make "alerts" window (covering key window) to display alarm conditions.
 *
 *  Revision 1.4  2001/04/20 02:36:51  asnd
 *  Fix bug with stuck "server-down" message window.
 *
 *  Revision 1.3  2001/01/15 06:37:11  asnd
 *  Set X-window title; message for failure to initialize screen.
 *
 *
 *  Revision history:
 *    16-Dec-1999  TW  Use CAMP_RPC_CLNT_TIMEOUT with clntInit 
 *    15-Feb-2000  DA  Make updateData() follow links
 *    06-Oct-2000  DA  Window resizing handler
 *    14-Dec-2000  TW  waitForServer doesn't show popup window for first try
 *                     at reconnect (new parameter msgFlag)
 *    05-Jan-2001  DA  Set window title (X)
 *    19-Apr-2001  DA  Fix server-down message flag (cv TW 14-Dec-2000)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include "camp_cui.h"
#ifdef VMS
#include ssdef
#include iodef
#include ttdef
#include tt2def
#include dcdef
#include dvidef
#endif /* VMS */

#ifdef linux
#include <ctype.h>
#include <signal.h>
#include <unistd.h> // gethostname
static void         winAdjSize(int sig);
#endif /* Linux */

#ifdef VMS
struct qiosb {
  short status;
  short d1,d2,d3;
};
#endif /* VMS */

/*
 *  globals
 */
char* prog_name = "CAMP CUI "CAMP_CUI_VERSION_STRING;
char   currViewPath[LEN_PATH+1];
bool_t isCursesInit = FALSE;
int    currDisplay = _DispTree;
long   poll_interval = POLL_INTERVAL;
unsigned long  msgPause_us = PAUSE_US;
bool_t isANSI = TRUE;

static char serverName[LEN_NODENAME+1];

extern enum clnt_stat camp_clnt_stat;
extern char currVarPath[LEN_PATH+1];
char* argv0;

int  screenHeight, screenWidth; /* extern in camp_cui.h */
int  newScreenH, newScreenW;

bool_t noisy = TRUE;
bool_t timedUpdate = TRUE;

#define _handleServerReturn(stat)   \
        if(_success(stat)){addMessage("done");deleteMessage();}\
        else{addMessage("\n  Error: ");addMessage(camp_getMsg());confirmMessage();\
             deleteMessage();}

static void 
usage( char* argv0 )
{
    fprintf( stderr, "usage: %s [-node <server>] [-noansi] [-update <sec>] [-pause <sec>]\n", argv0 );
}


int 
main( int argc, char* argv[] )
{
    int camp_status;
    int i, j;
#ifdef VMS
    char term[L_ctermid];
    s_dsc dsc_term;
    ITEM_LIST item_list;
    u_long result;
    struct qiosb port_qiosb;
    struct device_mode deviceMode;
    short ioChannel;
#endif /* VMS */

    camp_strncpy( serverName, sizeof( serverName ), "" ); // strcpy( serverName, "" );
    isANSI = TRUE;
    camp_debug = 0; // @tw 20140214  bool_t -> int

#ifdef VMS
    ctermid( term );
    setdsctostr( &dsc_term, term );
    item_list.buf_len = sizeof( u_long );
    item_list.item_code = DVI$_TT_ANSICRT;
    item_list.buf_addr = &result;
    result = 1;
    sys$getdviw( 0, 0, &dsc_term, &item_list, 0, 0, 0, 0 );
    if( !result )
    {
        isANSI = FALSE;
    }

    /*
     *  assign the port
     */
    camp_status = sys$assign( &dsc_term, &ioChannel, 0, 0, 0 );
    if( _success( camp_status ) )
      {
        bzero( &deviceMode, sizeof( struct device_mode ) );
	
	/*
	 *  Get terminal characteristics
	 */
        camp_status = sys$qiow( 0,                   /* efn */
			  ioChannel,           /* chan */
			  IO$_SENSEMODE,         /* func */
			  &port_qiosb,         /* iosb */
			  0, 0,                /* astadr, astprm */
			  &deviceMode,         /* P1 - charac. buffer */
			  8,                   /* P2 - len charac. buffer */
			  0,                   /* P3 - baud */
			  0,                   /* P4 - crlf_fill */
			  0,                   /* P5 - parity */
			  0 );                 /* P6 */
	if( _failure( camp_status ) )
	  {
	    fprintf( stderr, "Failed to get terminal settings, status %08x\n", camp_status );
	  }
	else 
	  {
	    /*
	     *  Set terminal characteristics
	     *  Do not receive broadcasts
	     */
	    deviceMode.u1.s1.basic_chars |= TT$M_NOBRDCST;
	    /*
	     *  set port mode
	     *  note:  P2 (size of set mode buffer ) is 8 because this
	     *         is the size without extended characteristics which
	     *         are not setable without LOG_IO or PHY_IO
	     *         (don't need any anyway)
	     */
	    camp_status = sys$qiow( 0,                   /* efn */
			      ioChannel,           /* chan */
			      IO$_SETMODE,         /* func */
			      &port_qiosb,         /* iosb */
			      0, 0,                /* astadr, astprm */
			      &deviceMode,         /* P1 - char. buffer */
			      8,                   /* P2 - len char. buffer */
			      TT$C_BAUD_9600,      /* P3 - baud */
			      0,                   /* P4 - crlf_fill */
			      TT$M_ALTRPAR,        /* P5 - parity */
			      0 );                 /* P6 */
	    if( _failure( camp_status ) )
	      {
		fprintf( stderr, "Failed to set terminal settings, status %08x\n", 
		       camp_status );
	      }
	  }
      }
    else
      {
	fprintf( stderr, "Could not assign terminal, status %08x\n", camp_status );
      }
#endif /* VMS */

#ifdef linux
    (void) signal(SIGWINCH, winAdjSize); 
#endif /* Linux */

    argv0 = argv[0];

    for( i = 1; i < argc; i++ )
    {
	if( streq( argv[i], "-node" ) )
	{
	    if( ++i >= argc )
	    {
		usage( argv[0] );
		exit( CAMP_FAILURE );
	    }

	    camp_strncpy( serverName, sizeof( serverName ), argv[i] ); // strcpy( serverName, argv[i] );
	}
	else if( streq( argv[i], "-d" ) )
	{
	    int count;

	    if( ++i >= argc )
	    {
		usage( argv[0] );
		exit( CAMP_FAILURE );
	    }

	    count = sscanf( argv[i], "%x", &camp_debug ); // camp_debug = atoi( argv[i] ); // 20140418 TW bitmask

	    if( count < 1 )
	    {
		usage( argv[0] );
		exit( CAMP_FAILURE );
	    }
	}
	else if( streq( argv[i], "-pause" ) )
	{
	    if( ++i >= argc )
	    {
		usage( argv[0] );
		exit( CAMP_FAILURE );
	    }

	    j = (int) (1000000.0*atof( argv[i] ) + 10);

	    if ( j >= 0 && j <= 10000000)
	    {
		msgPause_us = j;
	    }
	}
	else if( streq( argv[i], "-update" ) )
	{
	    if( ++i >= argc )
	    {
		usage( argv[0] );
		exit( CAMP_FAILURE );
	    }

	    j = atoi( argv[i] );

	    if ( j > 1 && j < 60 )
	    {
		poll_interval = j;
	    }
	}
	else if( streq( argv[i], "-noansi" ) )
	{
	    isANSI = FALSE;
	}
	else
	{
	    usage( argv[0] );
	    exit( CAMP_FAILURE );
	}
    }

    if( camp_debug == 0 )
    {
	char* env = getenv( "CAMP_DEBUG" );

	if( env != NULL )
	{
	    unsigned int debug_mask = 0;
	    int count = sscanf( env, "%x", &debug_mask ); // camp_debug = atoi( env ); // 20140418 TW bitmask
	    if( count == 1 ) camp_debug = debug_mask;
	}
    }

    camp_init_filemasks(); // before use of camp_getFilemask

    if( streq( serverName, "" ) )
    {
	char* host;

	/*
	 *  Host is CAMP_HOST environment variable
	 *  or else local host (VMS only) if undefined
	 */
	host = getenv( "CAMP_HOST" );
	if( host == NULL )
	{
	    // 20140314  TW  linux may have a server now
// #ifdef linux
//          /* linux has client only - no server available */
// 	    fprintf( stderr, "Linux host has no server available;\n");
// 	    fprintf( stderr, "Specify Camp server node on command line or setenv CAMP_HOST node \n");
// 	    usage( argv[0] );
// 	    exit( CAMP_FAILURE );
// #else
	    /* attempt to connect to a camp server on this computer */
	    gethostname( serverName, LEN_NODENAME );
// #endif
	}
	else
	{
	    camp_strncpy( serverName, sizeof( serverName ), host ); // strcpy( serverName, host );
	}
    }

    /* 
     *  Set terminal and icon names, presuming X window, by printing
     *  escape sequences.  Erase the line just in case it is an ordinary
     *  terminal.
     */
    if( isANSI )
    { 
	printf( "\033]2;Camp CUI for %s\07\033\\\033]21;Camp CUI for %s\033\\\033[2K\r",
		serverName, serverName ); 
    }

    /*
     *  Initialize everything (the order does matter)
     */
    camp_setMsg( "" );

#if CAMP_CUI_LOG
    camp_status = camp_startLog( "camp_cui", camp_getFilemaskGeneric(CAMP_CUI_LOG_FILE), TRUE );
    if( _failure( camp_status ) )
    {
        fprintf( stderr, "camp_cui: failed to initialize log file '%s'\n", camp_getFilemaskGeneric(CAMP_CUI_LOG_FILE) );
    }
    _camp_log( "log started" );
#endif // CAMP_CUI_LOG

    atexit( camp_cui_shutdown );

    camp_status = initScreen();
    if( _failure( camp_status ) )
    {
      endwin();
      fprintf( stderr, "Failed to initialize screen!\n" );
      exit( camp_status );
    }

    camp_pathInit( currViewPath, LEN_PATH+1 );
    camp_pathInit( currVarPath, LEN_PATH+1 );
    camp_setMsg( "" );

    camp_status = camp_clntInit( serverName, CAMP_RPC_CLNT_TIMEOUT );
    if( _failure( camp_status ) )
    {
        waitForServer( 1 );
    }
    camp_setMsg( "" );

    /*
     *  Initialize data and data display
     */
    printStatus();
    updateAndDisplayData();

    /*
     *  Enter user interface loop
     */
/*
    getMenuSelection();
*/
    varPicker();

    return( 0 );
}


int
initScreen( void )
{
    int camp_status;

    if( ! isCursesInit ) 
    {
        isCursesInit = TRUE;
        initscr();
        /* remember screen geometry to detect later changes */
        screenHeight = LINES;
        screenWidth  = COLS;
        newScreenH = screenHeight;
        newScreenW = screenWidth;
    }
    
    noecho();
    cbreak();  /* Meaningful for UNIX only */
    nonl();

    scrollok( stdscr, FALSE );
    attroff( A_BLINK | A_BOLD | A_REVERSE | A_UNDERLINE );
/*
    camp_status = initMessageDisplay();
    if( _failure( camp_status ) ) return( camp_status );
*/
    camp_status = initStatusDisplay();
    if( _failure( camp_status ) ) return( camp_status );
/*
    camp_status = initMenuDisplay();
    if( _failure( camp_status ) ) return( camp_status );
*/
    camp_status = initDataDisplay();
    if( _failure( camp_status ) ) return( camp_status );

    camp_status = initVarInfoWin();
    if( _failure( camp_status ) ) return( camp_status );

    camp_status = initKeyWin();
    if( _failure( camp_status ) ) return( camp_status );

    if( currDisplay == _DispVar )
    {
	camp_status = initVarWin();
	if( _failure( camp_status ) ) return( camp_status );
    }

    return( CAMP_SUCCESS );
}



void
refreshScreen( void )
{
    int camp_status;

#ifdef linux

    if( (newScreenH != screenHeight) || (newScreenW != screenWidth) ) 
    {
	deleteDataDisplay();
	deleteHelpWin();
	deleteAlertWin();
	deleteKeyWin();
	deleteStatusDisplay();
	deleteVarInfoWin();
	deleteVarWin();

        screenHeight = newScreenH;
        screenWidth = newScreenW;
        resizeterm( screenHeight, screenWidth );

	camp_status = initScreen();
	if( _failure( camp_status ) ) 
        {
          endwin();
          fprintf( stderr, "\n\nFailed to \ninitialize \nnew screen!\n" );
          exit( camp_status );
        }

	printStatus();
	updateDisplay();
    }

#endif /* Linux */

/*
    wclear( stdscr );
*/
/*
    touchwin( stdscr );
    wrefresh( stdscr );
*/
    touchStatusDisplay(); 
    refreshStatusDisplay();

    touchDataDisplay();
    refreshDataDisplay();

    touchVarInfoWin();
    refreshVarInfoWin();

    touchKeyWin();
    refreshKeyWin();

    touchAlertWin();
    refreshAlertWin();
/*
    touchMenuDisplay();
    refreshMenuDisplay();
*/
    touchVarWin();
    refreshVarWin();
/*
    touchMessageDisplay();
    refreshMessageDisplay();
*/

    /*  Any temporary message window, 18x60 center of screen */
    touchMessage();
    refreshMessage();

    /*  I found the pasteboard ID in curses.h */
/*  don't need this now, figured out how to do it portably above
        vms_status = smg$repaint_screen( &stdpb->_id );
*/

    /*#else
        wrefresh( curscr );
      #endif
    */
}


/* 
 *  Procedure to redisplay windows that had been covered by another window.
 *  This is written so only one screen update is performed for the several
 *  windows.  VAXcurses does this automatically.
 */

void 
uncoverWindows( void )
{
#ifndef VMS
    uncoverStatusDisplay(); 
    if( currDisplay == _DispTree )
    {
      uncoverDataDisplay();
      uncoverVarInfoWin();
      uncoverKeyWin();
      uncoverAlertWin();
    }
    else if( currDisplay == _DispVar )
    {
      uncoverVarWin();
    }
    doupdate();
#endif
}


void 
camp_cui_shutdown( void )
{
      /*----------------------------*/
     /*   Clean up                 */
    /*----------------------------*/
    if( isCursesInit ) endwin();
    camp_clntEnd();
#if CAMP_CUI_LOG
    _camp_log( "log stopping" );
    camp_stopLog();
#endif // CAMP_CUI_LOG
    camp_shutdown_filemasks();
}


int
updateAndDisplayData( void )
{
    int camp_status;

    camp_status = updateData();
    if( camp_status == CAMP_FAIL_RPC ) return( camp_status );

    updateDisplay();

/* SD VMS system call */
#ifdef VMS
    if( _camp_debug(CAMP_DEBUG_ANY) )
    {
        int code = 0;
        lib$show_vm( &code, NULL, 0 );
    }
#endif // VMS

    return( CAMP_SUCCESS );
}


int
checkRPCstatus( void )
{
    if( camp_clnt_stat == RPC_SUCCESS )
    {
        return( CAMP_SUCCESS );
    }
/*
    else if( camp_clnt_stat == RPC_TIMEDOUT )
    {
        return;
    }
*/
    else
    {
      waitForServer( 1 );
      return( CAMP_FAIL_RPC );
    }
}


/*
 *  waitForServer()
 *
 *  Called by:
 *     update  (camp_cui_keyboard.c)
 *     _doRPC inline (camp_cui_menucb.c)
 *     main, checkRPCstatus   (camp_cui_main.c)
 *
 *  19-Dec-2000  TW  New msgFlag parameter:
 *                       msgFlag = 1  ->  normal operation
 *                       msgFlag = 0  ->  don't show popup window on first
 *                                        try at reconnecting
 */
void
waitForServer( int msgFlag )
{
    int tries = 0;
    int camp_status;
    char str[64];

    camp_clntEnd();

    camp_pathInit( currViewPath, LEN_PATH+1 );
    camp_pathInit( currVarPath, LEN_PATH+1 );
    updateDisplay();

    camp_snprintf( str, sizeof( str ), "CAMP Server error (clnt_stat=%d)", camp_clnt_stat );

    if( msgFlag )
    {
      startMessage( str, "CAMP Server is not responding, attempt to connect..." );
    }
    
    for( ;; )
    {
        tries++;
      
        /*  TW  16-Dec-1999  Use CAMP_RPC_CLNT_TIMEOUT with clntInit */
        camp_status = camp_clntInit( serverName, CAMP_RPC_CLNT_TIMEOUT );
        if( _success( camp_status ) ) break;

        if( msgFlag || (tries > 1) ) {
          addMessage( "\nCAMP Server is not responding, attempt to connect..." );
        } else {
          startMessage( str, "CAMP Server is not responding, attempt to connect..." );
        }
          
        camp_clntEnd();

        sleep( 3 );
    }

    if( msgFlag || ( tries > 1 ) )
    {
        addMessage( "\nConnected to CAMP Server" );
        sleep( 1 );
        deleteMessage();
    }
     
    updateData();
    updateDisplay();
}

#if CAMP_CUI_LOG
static void log_var( CAMP_VAR* pVar )
{
    char msg[LEN_BUF+1];
    
    camp_snprintf( msg, sizeof( msg ), "var: ident: '%s', path: '%s'", pVar->core.ident, pVar->core.path );

    camp_log( "%s", msg );
}
#endif // CAMP_CUI_LOG

int
updateData( void )
{
    /*
     *  request updated data for all variables at current level.
     *  Feb 2000, DA:  request updates on linked variables too
     */
    int camp_status;
    CAMP_VAR* pVar;
    char msg[LEN_BUF+1];

    do 
    {
	camp_status = camp_clntUpdate();
    } while( _failure( checkRPCstatus() ) || _failure( camp_status ) );

#if CAMP_CUI_LOG
    // camp_varDoProc_recursive( log_var, pVarList );
    
    // camp_snprintf( msg, sizeof( msg ), "updateData: currViewPath: '%s', currVarPath: '%s'", currViewPath, currVarPath );
    // camp_log( "%s", msg );
#endif // CAMP_CUI_LOG

    /*
     *  If current path no longer exists, go to top
     */
    if( !camp_pathAtTop( currViewPath ) && 
        ( camp_varGetp_clnt( currViewPath ) == NULL ) )
    {
        camp_pathInit( currViewPath, LEN_PATH+1 );
        camp_pathInit( currVarPath, LEN_PATH+1 );
        if( pVarList != NULL ) 
        {
            camp_pathDown( currVarPath, LEN_PATH+1, pVarList->core.ident );
        }
    }
    else if( camp_varGetp_clnt( currVarPath ) == NULL )
    {
        camp_strncpy( currVarPath, sizeof( currVarPath ), currViewPath ); // strcpy( currVarPath, currViewPath );

        pVar = camp_varGetPHead_clnt( currViewPath );
        if( pVar != NULL )
        {
            camp_pathDown( currVarPath, LEN_PATH+1, pVar->core.ident );
        }
    }

    do 
    {
      camp_status = campSrv_varGet( currViewPath, 
                             CAMP_XDR_UPDATE | CAMP_XDR_CHILD_LEVEL );
    } while( _failure( checkRPCstatus() ) );
    
    if( _failure( camp_status ) ) return( camp_status );

    if( ( pVar = camp_varGetp_clnt( currVarPath ) ) != camp_varGetTrueP_clnt( currVarPath ) )
    {
	do 
	{
	  camp_status = campSrv_varGet( pVar->core.path, 
				   CAMP_XDR_UPDATE | CAMP_XDR_NO_CHILD );
	} while( _failure( checkRPCstatus() ) );
	
	if( _failure( camp_status ) ) return( camp_status );
    }
    
    return( CAMP_SUCCESS );
}



/* 
 * Donald tries to insert optimization code to avoid (re)reading each variable
 * when multiple variables share the same readProc.  Comparing readProc didn't work!  
 * It seems the core.readProc (and writeProc) pointers are zero.
 * Next attempt is to check the last-set time for each variable, and skip it if it
 * was just updated (whether by our actions or by regular polling).  This requires
 * retrieving fresh core data for each variable before deciding to read it.
 */
int
updateServerData( void )
{
    int camp_status;
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_head;
    CAMP_VAR* pVar_ins;
    char buf[LEN_BUF+1];
    bool_t message_started;
    unsigned int update_time, var_upt;

    message_started = FALSE;
    update_time = 0;

    if( currDisplay == _DispTree )
    {
        pVar_head = camp_varGetPHead_clnt( currVarPath );
        if( pVar_head == NULL ) return( CAMP_INVAL_VAR );

        for( pVar = pVar_head; pVar != NULL; pVar = pVar->pNext )
        {
            if( pVar->core.status & CAMP_VAR_ATTR_READ )
            {
                if( !message_started )
                {
                    startMessage( "Variable update", "" );
                    message_started = TRUE;
                }

                if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
                {
                    pVar_ins = varGetpIns( pVar );
                    if( pVar_ins == NULL ) break;

                    if( ( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL ) ||
                        !( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & 
                           CAMP_IF_ONLINE ) )
                    {
                        camp_snprintf( buf, sizeof( buf ), "Instrument \"%s\" is offline, can't read variables", 
                                      pVar_ins->core.path );
                        addMessage( buf );
			confirmMessage();
			break;
                        /* continue; */
                    }
                }

                /*
                 *  Get updated data to know when var was last changed
                 */
                do {
                  camp_status = campSrv_varGet( pVar->core.path,
                             CAMP_XDR_UPDATE | CAMP_XDR_NO_CHILD | CAMP_XDR_NO_NEXT );
                } while( _failure( checkRPCstatus() ) );
                if( _failure( camp_status ) ) continue;

                /*
                 * only read vars that have not changed since we read our first variable
                 */
                var_upt = pVar->core.timeLastSet.tv_sec;
                if ( update_time == 0 || update_time > var_upt )
                {
                    camp_snprintf( buf, sizeof( buf ), "Reading variable \"%s\"...", pVar->core.path );
                    addMessage( buf );

                    do {
                      camp_status = campSrv_varRead( pVar->core.path );
                    } while( _failure( checkRPCstatus() ) );

                    if( _success( camp_status ) )
                    {
                        addMessage( "done\n" );
                    }
                    else
                    {
                        addMessage( "failed\n  " );
                        addMessage( camp_getMsg() );
                        addMessage( "\n" );
                        confirmMessage();
                    }
                    /*
                     * Use the update-time of the first variable to define the start of updates
                     * Sometimes reads do not update the time (no varDoSet), so ignore the update time
                     * in such cases.
                     */
                    if ( update_time == 0 )
                    {
                        do {
                          camp_status = campSrv_varGet( pVar->core.path,
                                                   CAMP_XDR_UPDATE | CAMP_XDR_NO_CHILD | CAMP_XDR_NO_NEXT );
                        } while( _failure( checkRPCstatus() ) );
                        if( _success( camp_status ) )
                        {
                          if ( pVar->core.timeLastSet.tv_sec != var_upt )
                          {
                            update_time = pVar->core.timeLastSet.tv_sec;
                          }
                        }
                    }
                }
                else
                {
                    camp_snprintf( buf, sizeof( buf ), "Retain variable \"%s\". \n", pVar->core.path );
                    addMessage( buf );
                }
            }
        }

        if( message_started ) 
        {
            deleteMessage();
        }
    }
    else if( currDisplay == _DispVar )
    {
        pVar = camp_varGetp_clnt( currVarPath );
        if( pVar == NULL ) return( CAMP_INVAL_VAR );

        if( !( pVar->core.status & CAMP_VAR_ATTR_READ  ) )
        {
            return( CAMP_SUCCESS );
        }

        if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
        {
            pVar_ins = varGetpIns( pVar );
            if( pVar_ins == NULL ) 
            {
                addMessage( "\n  Error: invalid variable" );
                confirmMessage();
                deleteMessage();
                return( CAMP_INVAL_VAR );
            }

            if( ( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL ) ||
                !( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & 
                   CAMP_IF_ONLINE ) )
            {
                camp_snprintf( buf, sizeof( buf ), "Instrument \"%s\" is offline, can't read variables", 
                              pVar_ins->core.path );
                addMessage( buf );
                confirmMessage();
                deleteMessage();
                return( CAMP_NOT_CONN );
            }
        }

        camp_snprintf( buf, sizeof( buf ), "Reading variable \"%s\"...", currVarPath );
        startMessage( "Variable update", buf );

        do {
          camp_status = campSrv_varRead( currVarPath );
        } while( _failure( checkRPCstatus() ) );

	if( _success( camp_status ) )
	  {
	    addMessage( "done\n" );
	  }
	else
	  {
	    addMessage( "failed\n  " );
	    addMessage( camp_getMsg() );
	    addMessage( "\n" );
	    confirmMessage();
	  }
	deleteMessage();
    }    

    return( CAMP_SUCCESS );
}


void 
updateDisplay( void )
{
    if( currDisplay == _DispTree )
    {
        displayData();
        displayVarInfoWin();
        if( anyAlerts() )
        {
            displayAlertWin();
        }
        else
        {
            displayKeyWin();
        }
    }
    else if( currDisplay == _DispVar )
    {
        displayVar();
    }
}

/* 
 *  Even though comments in the curses "resizeterm" routine state it
 *  is unsafe to use it in a signal handler, that is exactly what the
 *  default configuration of ncurses does.  This leads to core dumps
 *  on window resize.  Here we define our own signal handler for
 *  resizes (SIGWINCH) that records the new size, but redisplays the
 *  screen only later.  The resize is sensed either by the changed
 *  global newScreenW/newScreenH, or by the apparent input character
 *  KEY_RESIZE.
 */
#ifdef linux
#include <termios.h>
#include <sys/ioctl.h>

int numSigWinch = 0;

static void  winAdjSize(int sig)
{
  struct winsize size;
  
  ++numSigWinch;
  if (ioctl(fileno(stdout), TIOCGWINSZ, &size) == 0) 
  {
      newScreenW = size.ws_col;
      newScreenH = size.ws_row;
      ungetch( KEY_RESIZE );
  }
  
  (void) signal(SIGWINCH, winAdjSize);	/* need this according to man (?) */
}

#endif /* linux */

void BEEP()
{
  if ( noisy ) 
  {
#ifdef VMS
    addch('\07');
#else
    beep();
#endif
  }
  return;
}

