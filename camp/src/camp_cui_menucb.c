/*
 *  Name:        camp_cui_menucb.c
 *
 *  Purpose:     Callback routines for "menu" selections
 *               Including, entry points to most popup selection windows
 *               and the routines to invoke on selection.
 *
 *  Called by:   varPicker (in camp_cui_data.c) calls many entry points
 *               based on user keystrokes and context.
 * 
 *  Revision history:
 *   12-Dec-1996  TW  Don't retry RPC requests, just try once.
 *      Oct-1999  DA  Add "choose" menu entry for paths
 *   15-Dec-1999  TW  Conditional compilation of basename -> basename_tw 
 *                    for Linux (who made the change originally?)
 *                    This could also be a permanent change in libc_tw
 *   22-Jan-2000  DA  Make hotkeys work in menu.
 *   19-Dec-2000  TW  waitForServer now has parameter
 *   19-Dec-2001  DA  Remove conditional basename: always use basename_tw.
 *                    (Where did VAX basename come from before?)
 *   17-Sep-2004  DA  Shorten transient message display
 *   05-Jul-2005  DA  Alter save/restore functions (add Recreate)
 *   20140218     TW  exit(1) -> exit(0) for consistency
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <curses.h>
#ifdef linux
#include <unistd.h> // usleep
#endif // linux
#include "camp_cui.h"

extern long poll_interval;
extern unsigned long msgPause_us;
extern bool_t noisy;

char currVarPath[LEN_PATH+1];

extern enum clnt_stat camp_clnt_stat;

/*
 *  Display transient status messages for time selected by user; default
 *  0.7 sec.   Some systems may only handle integer seconds, so round.
 */

#ifdef linux
#define _msgSleep usleep( msgPause_us )
#else
#define _msgSleep sleep( (unsigned int)((msgPause_us+800000)/1000000) )
#endif

#define _doRPC( proc, title, msg ) \
        /* do { */ \
	  if( strcmp( msg, "" ) != 0 ) startMessage( title, msg ); \
          camp_status = proc; \
          if( _failure( camp_status ) ) \
	    { \
	      if( strcmp( msg, "" ) == 0 ) startMessage( title, "" ); \
              addMessage( "\n  " ); \
              addMessage( camp_getMsg() ); \
	      confirmMessage(); \
	      deleteMessage(); \
	    } \
	  else if( strcmp( msg, "" ) != 0 ) \
	    { \
	      addMessage( "done" ); \
              _msgSleep ; \
	      deleteMessage(); \
	    } \
        /* } while( _failure( checkRPCstatus() ) ); */ \
          if( camp_clnt_stat != RPC_SUCCESS ) {waitForServer( 0 );}\
          if( _failure( camp_status ) ) return( camp_status );

int
menuFileQuit( void )
{
    camp_cui_shutdown();
    exit( 0 );

    return( CAMP_SUCCESS );
}


int
menuFileLoadNew( void )
{
    int camp_status;
    char filename[LEN_FILENAME+1];
    char str[LEN_STR+1];
    char buf[LEN_BUF+1];

    _doRPC( campSrv_sysDir( camp_getFilemaskGeneric(CAMP_CFG_GLOB_PATTERN) ), "Dir config files", "" )

    if( !inputSetupFile( "Open config file", 
                         pSys->pDyna->cfgFile, str, sizeof( str ), camp_getFilemaskGeneric(CAMP_CFG_SFMT_BASE) ) )
    {
        return( CAMP_SUCCESS );
    }

    camp_snprintf( filename, LEN_FILENAME+1, camp_getFilemaskGeneric(CAMP_CFG_PFMT), str );

    camp_snprintf( buf, LEN_BUF+1, "Opening config file \"%s\" (\"%s\")...", str, filename );

    _doRPC( campSrv_sysLoad( filename, 1 ), "Open config", buf );

    updateData();
    updateDisplay();

    return( CAMP_SUCCESS );
}


int
menuFileLoad( void )
{
    int camp_status;
    char filename[LEN_FILENAME+1];
    char str[LEN_STR+1];
    char buf[LEN_BUF+1];

    _doRPC( campSrv_sysDir( camp_getFilemaskGeneric(CAMP_CFG_GLOB_PATTERN) ), "Dir config files", "" );

    if( !inputSetupFile( "Import config file", 
                         pSys->pDyna->cfgFile, str, sizeof( str ), camp_getFilemaskGeneric(CAMP_CFG_SFMT_BASE) ) )
    {
        return( CAMP_SUCCESS );
    }

    camp_snprintf( filename, LEN_FILENAME+1, camp_getFilemaskGeneric(CAMP_CFG_PFMT), str );

    camp_snprintf( buf, LEN_BUF+1, "Importing config file \"%s\" (\"%s\")...", str, filename );

    _doRPC( campSrv_sysLoad( filename, 0 ), "Import config", buf );

    updateData();
    updateDisplay();

    return( CAMP_SUCCESS );
}


int 
menuFileSave( void )
{
    int camp_status;
    char filename[LEN_FILENAME+1];
    char str[LEN_STR+1] = { "" };
    char buf[LEN_BUF+1];

    buf[0] = '\0';
    if( pSys->pDyna->cfgFile != NULL )
    {
        /* basename --> basename_tw for linux */
        basename_tw( pSys->pDyna->cfgFile, filename );
        sscanf( filename, camp_getFilemaskGeneric(CAMP_CFG_SFMT_BASE), str );
    }

    if( !inputString( "Save config file", 32, str, str, sizeof( str ), isIdentChar ) )
    {
        return( CAMP_SUCCESS );
    }

    if( str[0] == '\0' )
    { /* no file name, so use the same as for auto-save of config */
        camp_strncpy( filename, sizeof( filename ), camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) ); 
    }
    else
    { /* get full path and file name */
        camp_snprintf( filename, LEN_FILENAME+1, camp_getFilemaskGeneric(CAMP_CFG_PFMT), str );
    }

    camp_snprintf( buf, LEN_BUF+1, "Saving config file %s (%s)...", str, filename );

    _doRPC( campSrv_sysSave( filename, 0 ), "Save config file", buf );

    return( CAMP_SUCCESS );
}

/*
 * menuEdit: 
 *
 * Handle hot-keys pressed while navigating a variable's pop-up menu (key==0)
 * or pressed on the  main display screen (key==<code>)
 */

int
menuEdit( u_short key )
{
    CAMP_VAR* pVar;
    CAMP_VAR* pLinkVar = NULL;
    int n;
    int i;
    u_int select = 0;
    int isel;
    u_short hotKey[MAX_NUM_SELECTIONS];
    HK_FLAG hotKeyFlag = HK_HOT;
    char buf[LEN_BUF+1];
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    pVar = camp_varGetTrueP_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( pVar->core.varType == CAMP_VAR_TYPE_LINK )
    {
        pLinkVar = pVar;
        pVar = camp_varGetp_clnt( currVarPath );
        if( pVar == NULL ) return( CAMP_SUCCESS );
    }

    n = 0;

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
      case CAMP_VAR_TYPE_STRING:
      case CAMP_VAR_TYPE_SELECTION:
      case CAMP_VAR_TYPE_LINK:
      case CAMP_VAR_TYPE_ARRAY:
        if( pVar->core.status & CAMP_VAR_ATTR_SET )
        {
            hotKey[n] = 's';
            choices[n] = "Set"; callback[n] = menuVarSet; n++;
        }
        if( pVar->core.status & CAMP_VAR_ATTR_READ )
        {
            hotKey[n] = 'r';
            choices[n] = "Read"; callback[n] = menuVarRead; n++;
        }
        if( pVar->core.attributes & CAMP_VAR_ATTR_POLL )
        {
            hotKey[n] = 'p';
            if( pVar->core.status & CAMP_VAR_ATTR_POLL )
            {
                choices[n] = "Polling OFF"; callback[n] = menuVarPoll; n++;
            }
            else
            {
                choices[n] = "Polling ON"; callback[n] = menuVarPoll; n++;
            }
        }
        if( pVar->core.attributes & CAMP_VAR_ATTR_ALARM )
        {
            hotKey[n] = 'a';
            if( pVar->core.status & CAMP_VAR_ATTR_ALARM )
            {
                choices[n] = "Alarm OFF"; callback[n] = menuVarAlarm; n++;
            }
            else
            {
                choices[n] = "Alarm ON"; callback[n] = menuVarAlarm; n++;
            }
        }
        if( pVar->core.attributes & CAMP_VAR_ATTR_LOG )
        {
            hotKey[n] = 'l';
            if( pVar->core.status & CAMP_VAR_ATTR_LOG )
            {
                choices[n] = "Logging OFF"; callback[n] = menuVarLog; n++;
            }
            else
            {
                choices[n] = "Logging ON"; callback[n] = menuVarLog; n++;
            }
        }
        break;

      case CAMP_VAR_TYPE_STRUCTURE:
	hotKey[n] = 'c';
        choices[n] = "Choose ->"; callback[n] = menuInsChoose; n++;

        hotKey[n] = 'z';
        choices[n] = "Zero statistics"; callback[n] = menuVarZero; n++;
        break;

      case CAMP_VAR_TYPE_INSTRUMENT:
	hotKey[n] = 'c';
        choices[n] = "Choose ->"; callback[n] = menuInsChoose; n++;

        if( ( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL )
            || !( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & CAMP_IF_ONLINE ) )
        {
            hotKey[n] = 'i';
            choices[n] = "Interface"; callback[n] = menuInsIf; n++;
        }

        if( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF != NULL )
        {
            hotKey[n] = 'o';
            if( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & CAMP_IF_ONLINE )
            {
                choices[n] = "Offline"; callback[n] = menuInsLine; n++;
            }
            else
            {
                choices[n] = "Online"; callback[n] = menuInsLine; n++;
            }
        }

        hotKey[n] = 'z';
        choices[n] = "Zero statistics"; callback[n] = menuVarZero; n++;
        hotKey[n] = 'l';
        choices[n] = "Load ini file"; callback[n] = menuInsLoadIni; n++;
        hotKey[n] = 's';
        choices[n] = "Save ini file"; callback[n] = menuInsSave; n++;
	/*
	 * These are only confusing to the users, and have little practical use.
	 * They allow an instrument to have an ini file with a different name
	 * from the instrument.
	 *
	 * hotKey[n] = 'b';
	 * choices[n] = "Become (saved ini)"; callback[n] = menuInsBecomeIni; n++;
	 * hotKey[n] = 'r';
	 * choices[n] = "Redirect ini"; callback[n] = menuInsRedirect; n++;
	 */
        hotKey[n] = 'k';
        if( pVar->core.status & CAMP_INS_ATTR_LOCKED )
        {
            choices[n] = "Lock OFF"; callback[n] = menuInsLock; n++;
        }
        else
        {
            choices[n] = "Lock ON"; callback[n] = menuInsLock; n++;
        }
        break;
    }

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        if( pVar->core.attributes & CAMP_VAR_ATTR_ALARM )
        {
            hotKey[n] = 't';
            choices[n] = "Tolerance"; callback[n] = menuVarTol; n++;
        }
        hotKey[n] = 'z';
        choices[n] = "Zero statistics"; callback[n] = menuVarZero; n++;
        /* Hotkey for units change only operates in menu */
        hotKey[n] = (key==0? 'u': 0);
        choices[n] = "Units"; callback[n] = menuVarUnits; n++;
        break;

      case CAMP_VAR_TYPE_LINK:
        if( pLinkVar->core.status & CAMP_VAR_ATTR_SET )
        {
            hotKey[n] = 'k';
            choices[n] = "Set Link"; callback[n] = menuLnkVarSet; n++;
        }
        break;
    }

    hotKey[n] = 'v';
    choices[n] = "View parameters"; callback[n] = menuVarShow; n++;

    /* Hotkey for title change only operates in menu */
    hotKey[n] = (key==0? 'n': 0);
    choices[n] = "New Title"; callback[n] = menuVarTitle; n++;

    camp_snprintf( buf, LEN_BUF+1, "Variable %s", currVarPath );

    isel = -1;

    if( key != 0 )
    {
        key = tolower( key );
        for( i = 0; i < n; i++ )
        {
            if( key == hotKey[i] )
            {
                isel = i;
                break;
            }
        }
        if( isel == -1 )
        {
            /*  
             *  DA: beep now provided by alert window
             *  BEEP(); 
             */
            return( CAMP_SUCCESS );
        }
        select = isel;
    }
    else
    {
        if( !inputHotSelectWin( buf, n, choices, 0, &select, hotKeyFlag, hotKey ) )
        {
            return( CAMP_SUCCESS );
        }
    }

    (*callback[select])();

    updateData();
    updateDisplay();

    return( CAMP_SUCCESS );
}

/*

* Do not navigate by menu! (except "choose" is provided)

int
menuViewNavigate( void )
{
    CAMP_VAR* pVar_curr;
    CAMP_VAR* pVar;
    int n;
    int select;
    char buf[LEN_BUF+1];
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    camp_strncpy( currVarPath, sizeof( currVarPath ), currViewPath ); // strcpy( currVarPath, currViewPath );

    for( ;; )
    {
        if( camp_pathAtTop( currVarPath ) )
        {
            pVar_curr = pVarList;
        }
        else
        {
            pVar_curr = camp_varGetp_clnt( currVarPath );
            if( pVar_curr == NULL ) 
            {
                return( CAMP_SUCCESS );
            }

            pVar_curr = pVar_curr->pChild;
        }

        n = 0;

        choices[n++] = "< OK >";

        if( !camp_pathAtTop( currVarPath ) )
        {
            choices[n++] = "< Up >";
        }

        for( pVar = pVar_curr; pVar != NULL; pVar = pVar->pNext )
        {
            if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) ||
                ( pVar->core.varType == CAMP_VAR_TYPE_STRUCTURE ) )
            {
                choices[n++] = pVar->core.ident;
            }
        }

        camp_snprintf( buf, LEN_BUF+1, "Set view path [%s]", currVarPath );

        if( !inputSelectWin( buf, n, choices, 0, &select ) )
        {
            return( CAMP_SUCCESS );
        }

        if( select == 0 ) 
        {
            break;
        }
        else if( streq( choices[select], "< Up >" ) )
        {
            camp_pathUp( currVarPath );
        }
        else
        {
            camp_pathDown( currVarPath, choices[select] );
        }
    }

    camp_strncpy( currViewPath, sizeof( currViewPath ), currVarPath ); // strcpy( currViewPath, currVarPath );
    updateData();
    updateDisplay();
}
*/

/*
int 
menuViewRoot( void )
{
    camp_pathInit( currViewPath );
    updateData();
    updateDisplay();
}
*/
/*
int 
menuViewUp( void )
{
    camp_pathUp( currViewPath );
    updateData();
    updateDisplay();
}
*/
/*
int 
menuViewDown( void )
{
    if( !inputVar( currVarPath, "Variable", 
                        CAMP_VAR_TYPE_STRUCTURE | CAMP_VAR_TYPE_INSTRUMENT,
                        CAMP_VAR_ATTR_SHOW ) )
    {
        return( CAMP_SUCCESS );
    }

    camp_strncpy( currViewPath, sizeof( currViewPath ), currVarPath ); // strcpy( currViewPath, currVarPath );
    updateData();
    updateDisplay();
}
*/
/*
int 
menuViewRedraw( void )
{
    refreshScreen();
}
*/
/*
int
menuViewRate( void )
{
    inputInteger( "Set screen update rate (seconds)", 
                  poll_interval, &poll_interval );
}
*/

int 
menuInsAdd( void )
{
    int camp_status;
    char typeIdent[LEN_IDENT+1];
    char ident[LEN_IDENT+1];
    CAMP_VAR* pVar;
    char buf[LEN_BUF+1];

    if( !inputInsAvail( ident, sizeof( ident ), typeIdent, sizeof( typeIdent ) ) ) 
    {
        return( CAMP_SUCCESS );
    }

    if( streq( typeIdent, "" ) )
    {
        if( !inputInsType( typeIdent, sizeof( typeIdent ) ) ) 
        {
            return( CAMP_SUCCESS );
        }

        if( !inputString( "Unique identifier", LEN_IDENT, "", 
                           ident, sizeof( ident ), isIdentChar ) ) 
        {
            return( CAMP_SUCCESS );
        }
    }

    camp_snprintf( buf, LEN_BUF+1, "Adding instrument \"%s\"...", ident );

    _doRPC( campSrv_insAdd( typeIdent, ident ), "Add instrument", buf );

    updateData();

    camp_pathInit( currViewPath, LEN_PATH+1 );
    camp_pathInit( currVarPath, LEN_PATH+1 );
    camp_pathDown( currVarPath, LEN_PATH+1, ident );

    pVar = camp_varGetp_clnt( currVarPath );
    if( ( pVar != NULL ) && 
        ( ( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL ) ||
          !( pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & CAMP_IF_ONLINE ) ) ) 
    {
        menuInsIf();
        updateData();

        menuInsLine();
        updateData();
    }

    updateDisplay();

    return( CAMP_SUCCESS );
}


int
menuInsChoose( void )
{
    CAMP_VAR* pVar;
    pVar = camp_varGetTrueP_clnt( currVarPath );

    camp_pathDown( currViewPath, LEN_PATH+1, pVar->core.ident );

    pVar = pVar->pChild;
    camp_strncpy( currVarPath, sizeof( currVarPath ), currViewPath ); // strcpy( currVarPath, currViewPath );
    if( pVar != NULL )
    {
	camp_pathDown( currVarPath, LEN_PATH+1, pVar->core.ident );
    }

    return( CAMP_SUCCESS );
}

/*
 * menuInsRecreate -- Add a new instrument and restore its config from
 *                   from the saved ini file.
 */
int 
menuInsRecreate( void )
{
    int camp_status;
    char typeIdent[LEN_IDENT+1];
    char ident[LEN_IDENT+1];
    CAMP_VAR* pVar;
    char buf[LEN_BUF+1];
    char filename[LEN_FILENAME+1];
    char* description;
    char ident_descrip[LEN_IDENT+1];

    _doRPC( campSrv_sysDir( camp_getFilemaskGeneric(CAMP_INS_INI_GLOB_PATTERN) ), "Dir init files", "" );

    if( !inputSetupFile( "Recreate old instrument",
                         NULL, ident, sizeof( ident ), camp_getFilemaskGeneric(CAMP_INS_INI_SFMT_BASE) ) )
    {
        return( CAMP_SUCCESS );
    }

    /* 
     * Attempt to determine the type of the instrument from the header-comment
     * in the .ini file.
     */
    typeIdent[0] = '\0';
    camp_snprintf( filename, LEN_FILENAME+1, camp_getFilemaskGeneric(CAMP_INS_INI_PFMT), ident );

    /* Get first line (comment) in cfg file. use proc to avoid global variables */
    camp_snprintf( buf, LEN_BUF+1, "proc _cui_get_cmt_ fn {set fd [open $fn]; return [gets $fd][close $fd]}; _cui_get_cmt_ {%s}", filename );
    camp_status = campSrv_cmd( buf );
    description = camp_getMsg();
    if( _success( camp_status ) )
    {
        sscanf( description, "# instrument %[^,], of type %s", ident_descrip, typeIdent );
        if( !streq( ident_descrip, ident ) )
        {
            typeIdent[0] = '\0';
        }
    }

    /*
     * If no type was automatically determined, ask user.
     */
    if( typeIdent[0] == '\0' )
    {
        if( !inputInsType( typeIdent, sizeof( typeIdent ) ) ) 
        {
            return( CAMP_SUCCESS );
        }
    }

    camp_snprintf( buf, LEN_BUF+1, "Adding instrument \"%s\"...", ident );

    _doRPC( campSrv_insAdd( typeIdent, ident ), "Add instrument", buf );

    updateData();

    camp_pathInit( currViewPath, LEN_PATH+1 );
    camp_pathInit( currVarPath, LEN_PATH+1 );
    camp_pathDown( currVarPath, LEN_PATH+1, ident );

    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar != NULL )
    {

	camp_snprintf( filename, LEN_FILENAME+1, camp_getFilemaskGeneric(CAMP_INS_INI_PFMT), ident );
	camp_snprintf( buf, LEN_BUF+1, "Loading instrument \"%s\" ini file...", 
		       currVarPath );

	_doRPC( campSrv_insLoad( currVarPath, filename, 0 ), "Load initialization", buf );
	
	updateData();
    }

    updateDisplay();

    return( CAMP_SUCCESS );
}


int 
menuInsDel( void )
{
    int camp_status;
    char* confirmLabels[] = { "No", "Yes" };
    u_int selection;
    char buf[LEN_BUF+1];
    char path[LEN_PATH+1];

    if( !inputVar( path, sizeof( path ), "Instrument", 
                        CAMP_VAR_TYPE_INSTRUMENT,
                        CAMP_VAR_ATTR_SHOW ) )
    {
        return( CAMP_SUCCESS );
    }

    if( !inputSelectWin( "Confirm delete", 2, confirmLabels, 0, &selection ) )
    {
        return( CAMP_SUCCESS );
    }
    if( selection == 0 ) return( CAMP_SUCCESS );

    camp_snprintf( buf, LEN_BUF+1, "Deleting instrument \"%s\"...", path );

    _doRPC( campSrv_insDel( path ), "Delete instrument", buf );

    updateData();
    updateDisplay();

    return( CAMP_SUCCESS );
}


int 
menuInsLock( void )
{
    int camp_status;
    bool_t flag;
    char buf[LEN_BUF+1];

    if( !inputInsLock( currVarPath, &flag ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        camp_snprintf( buf, LEN_BUF+1, "Locking instrument \"%s\"...", currVarPath );
    }
    else
    {
        camp_snprintf( buf, LEN_BUF+1, "Unlocking instrument \"%s\"...", currVarPath );
    }

    _doRPC( campSrv_insLock( currVarPath, flag ), "Lock instrument", buf );

    return( CAMP_SUCCESS );
}

int 
menuInsLine( void )
{
    int camp_status;
    bool_t flag;
    char buf[LEN_BUF+1];

    if( !inputInsLine( currVarPath, &flag ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting instrument \"%s\" online...", currVarPath );
    }
    else
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting instrument \"%s\" offline...", currVarPath );
    }

    _doRPC( campSrv_insLine( currVarPath, flag ), "Instrument connection", buf );

    return( CAMP_SUCCESS );
}


int 
menuInsBecomeIni( void )
{
    insLoad( 1, "Load and update ini file" );

    return( CAMP_SUCCESS );
}


int
menuInsLoadIni( void )
{
    insLoad( 0, "Load inst ini file" );

    return( CAMP_SUCCESS );
}


int
insLoad( int flag, const char* title )
{
    int camp_status;
    CAMP_VAR* pVar;
    char filename[LEN_FILENAME+1];
    char buf[LEN_BUF+1];
    char str[LEN_STR+1];

    _doRPC( campSrv_sysDir( camp_getFilemaskGeneric(CAMP_INS_INI_GLOB_PATTERN) ), "Dir init files", "" );

    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    /* 
     *  get the filename
     */
    if( !inputSetupFile( title, 
                         pVar->spec.CAMP_VAR_SPEC_u.pIns->iniFile, 
                         str, sizeof( str ), camp_getFilemaskGeneric(CAMP_INS_INI_SFMT_BASE) ) )
    {
        return( CAMP_SUCCESS );
    }

    camp_snprintf( filename, LEN_FILENAME+1, camp_getFilemaskGeneric(CAMP_INS_INI_PFMT), str );

    if( flag )
    {
        camp_snprintf( buf, LEN_BUF+1, "Load and write instrument \"%s\" ini file \"%s\"...", 
                        currVarPath, str );
    }
    else
    {
        camp_snprintf( buf, LEN_BUF+1, "Load instrument \"%s\" ini file \"%s\"...", 
                        currVarPath, str );
    }

    _doRPC( campSrv_insLoad( currVarPath, filename, flag ), title, buf );

    return( CAMP_SUCCESS );
}


int 
menuInsSave( void )
{
    insSave( 0, "Save inst ini file" );

    return( CAMP_SUCCESS );
}


int
menuInsRedirect( void )
{
    insSave( 1, "Write and update ini file" );

    return( CAMP_SUCCESS );
}

int 
insSave( int flag, const char* title )
{
    int camp_status;
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    char filename[LEN_FILENAME+1];
    char buf[LEN_BUF+1];
    char str[LEN_STR+1];

    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    str[0] = '\0';
    if( pIns->iniFile != NULL )
    {
        /* basename --> basename_tw for linux */
        basename_tw( pIns->iniFile, filename );
        sscanf( filename, camp_getFilemaskGeneric(CAMP_INS_INI_SFMT_BASE), str );
    }

    if( !inputString( title, 32, str, str, sizeof( str ), isIdentChar ) ) 
        return( CAMP_SUCCESS );

    /* If user enters null, then revert to the instrument name */
    if( str[0] == '\0' )
    {
        camp_strncpy( str, sizeof( str ), pVar->core.ident ); // strcpy( str, pVar->core.ident );
    }

    camp_snprintf( filename, LEN_FILENAME+1, camp_getFilemaskGeneric(CAMP_INS_INI_PFMT), str );

    camp_snprintf( buf, LEN_BUF+1, "Saving instrument \"%s\" ini file \"%s\"...", 
                    currVarPath, str );

    _doRPC( campSrv_insSave( currVarPath, filename, flag ), "Save file", buf );

    return( CAMP_SUCCESS );
}


int
menuInsIf( void )
{
    int camp_status;
    CAMP_VAR* pVar;
    char typeIdent[LEN_IDENT+1];
    double accessDelay;
    double timeout;
    char defn[LEN_IF_DEFN+1];
    char buf[LEN_BUF+1];

    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    *defn = '\0';

    if( !inputIf( pVar, typeIdent, LEN_IDENT+1, &accessDelay, &timeout, defn, LEN_IF_DEFN+1 ) ) return( CAMP_SUCCESS );

    camp_snprintf( buf, LEN_BUF+1, 
             "Setting instrument \"%s\"\nto interface type %s (%f %f %s)...", 
             currVarPath, typeIdent, accessDelay, timeout, defn );

    _doRPC( campSrv_insIf( currVarPath, typeIdent, (float)accessDelay, (float)timeout, defn ),
            "Instrument interface", buf );

    return( CAMP_SUCCESS );
}

int
menuVarShow( void )
{
    displayVarLoop();

    return( CAMP_SUCCESS );
}


int 
menuVarTitle( void )
{
    int camp_status;
    char tit[LEN_TITLE+1];
    char buf[LEN_BUF+1];
    char cmd[LEN_STR+1];
    char* c;

    if( !inputVarTitle( currVarPath, tit, sizeof( tit ) ) ) return( CAMP_SUCCESS );

    nestBraces( tit );

    camp_snprintf( cmd, LEN_STR+1, "varDoSet %s -T {%s}", currVarPath, tit );

    camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" title to \"%s\"...", 
              currVarPath, tit ); 

    _doRPC( campSrv_cmd( cmd ), "Set title", buf);

    return( CAMP_SUCCESS );
}

int 
menuVarUnits( void )
{
    int camp_status;
    char un[LEN_UNITS+1];
    char buf[LEN_BUF+1];
    char cmd[LEN_STR+1];
    char* c;

    if( !inputVarUnits( currVarPath, un, sizeof( un ) ) ) return( CAMP_SUCCESS );

    nestBraces( un );

    camp_snprintf( cmd, LEN_STR+1, "varDoSet %s -units {%s}", currVarPath, un );

    camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" units to \"%s\"...", 
              currVarPath, un ); 

    _doRPC( campSrv_cmd( cmd ), "Set units", buf);

    return( CAMP_SUCCESS );
}

int 
menuVarSet( void )
{
    int camp_status;
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_ins;
    double dVal;
    int iVal;
    char str[LEN_STR+1];
    char buf[LEN_BUF+1];

    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the linked data
     *  ARRAYs not implemented
     */
    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( !( pVar->core.status & CAMP_VAR_ATTR_SET  ) ) 
      return( CAMP_INVAL_VAR );

    /*
     *  Check that instrument is online
     */
    pVar_ins = varGetpIns( pVar );
    if( pVar_ins == NULL ) return( CAMP_INVAL_VAR );

    if( ( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL ) ||
       !( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & 
	  CAMP_IF_ONLINE ) )
      {
	camp_snprintf( buf, LEN_BUF+1, "Instrument \"%s\" is offline, can't set variable", 
		pVar_ins->core.path );
	startMessage( "Set variable", "" );
	addMessage( buf );
	confirmMessage();
        deleteMessage();
	return( CAMP_FAILURE );
      }
  
    /* 
     *  data is SETable, get the new setting
     */
    switch( pVar->core.varType )
    {
      case CAMP_VAR_TYPE_INT:
        if( !inputVarInteger( pVar, &dVal ) ) return( CAMP_SUCCESS );

        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" to \"%d\"...", 
                        currVarPath, (int)dVal );

        _doRPC( campSrv_varNumSetVal( currVarPath, dVal ), "Set variable", buf );
        break;

      case CAMP_VAR_TYPE_FLOAT:
        if( !inputVarFloat( pVar, &dVal ) ) return( CAMP_SUCCESS );

        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" to \"%f\"...", 
                        currVarPath, dVal );

        _doRPC( campSrv_varNumSetVal( currVarPath, dVal ), "Set variable", buf );
        break;

      case CAMP_VAR_TYPE_SELECTION:
        if( !inputVarSelection( pVar, &iVal ) ) return( CAMP_SUCCESS );

        varSelGetIDLabel( pVar, iVal, str, sizeof( str ) );
        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" to \"%s\"...", 
                        currVarPath, str );

        _doRPC( campSrv_varSelSetVal( currVarPath, iVal ), "Set variable", buf );
        break;

      case CAMP_VAR_TYPE_STRING:
        if( !inputVarString( pVar, str, sizeof( str ) ) ) return( CAMP_SUCCESS );

        nestBraces( str );

        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" to \"%s\"...", 
                        currVarPath, str );

        _doRPC( campSrv_varStrSetVal( currVarPath, str ), "Set variable", buf);
        break;

      case CAMP_VAR_TYPE_ARRAY:
        /*
         *  Only done in server
         */
        break;

      default:
        return( CAMP_SUCCESS );
    }

    return( CAMP_SUCCESS );
}


int 
menuLnkVarSet( void )
{
    int camp_status;
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_ins;
    char str[LEN_STRING+1];
    char buf[LEN_BUF+1];

    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the source of
     *  the link, not the destination
     */
    pVar = camp_varGetTrueP_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( !( pVar->core.status & CAMP_VAR_ATTR_SET  ) ) 
    {
	return( CAMP_INVAL_VAR );
    }

    /*
     *  Check that instrument is online
     */
    pVar_ins = varGetpIns( pVar );
    if( pVar_ins == NULL ) return( CAMP_INVAL_VAR );

    if( ( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL ) ||
       !( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & 
	  CAMP_IF_ONLINE ) )
    {
	camp_snprintf( buf, LEN_BUF+1, "Instrument \"%s\" is offline, can't set variable", 
		pVar_ins->core.path );
	startMessage( "Set variable", "" );
	addMessage( buf );
	confirmMessage();
        deleteMessage();

	return( CAMP_FAILURE );
    }
  
    switch( pVar->core.varType )
    {
      case CAMP_VAR_TYPE_LINK:
        if( !inputString( "Full path", LEN_STRING,
            pVar->spec.CAMP_VAR_SPEC_u.pLnk->path, str, sizeof( str ), isPathChar ) ) 
        {
            return( CAMP_SUCCESS );
        }

        camp_snprintf( buf, LEN_BUF+1, "Setting link \"%s\" to \"%s\"...", 
                        currVarPath, str );

        _doRPC( campSrv_varLnkSetVal( currVarPath, str ), "Set link", buf );
        return( CAMP_SUCCESS );

      default:
        return( CAMP_SUCCESS );
    }

    return( CAMP_SUCCESS );
}


int 
menuVarRead( void )
{
    int camp_status;
    char buf[LEN_BUF+1];
    CAMP_VAR* pVar;
    CAMP_VAR* pVar_ins;

    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_INVAL_VAR );
    
    if( !( pVar->core.status & CAMP_VAR_ATTR_READ  ) ) 
    {
	return( CAMP_INVAL_VAR );
    }

    /*
     *  Check that instrument is online
     */
    pVar_ins = varGetpIns( pVar );
    if( pVar_ins == NULL ) return( CAMP_INVAL_VAR );

    if( ( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL ) ||
       !( pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF->status & 
	  CAMP_IF_ONLINE ) )
    {
	camp_snprintf( buf, LEN_BUF+1, "Instrument \"%s\" is offline, can't read variable", 
		pVar_ins->core.path );
	startMessage( "Read variable", "" );
	addMessage( buf );
	confirmMessage();
        deleteMessage();
	return( CAMP_FAILURE );
    }
  
    camp_snprintf( buf, LEN_BUF+1, "Reading variable \"%s\"...", currVarPath );
    
    _doRPC( campSrv_varRead( currVarPath ), "Read variable", buf );
    return( CAMP_SUCCESS );
}



int 
menuVarPoll( void )
{
    int camp_status;
    bool_t flag;
    double interval;
    char buf[LEN_BUF+1];

    if( !inputVarPoll( currVarPath, &flag, &interval ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" polling to %.1f seconds...", 
                      currVarPath, interval );
    }
    else
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" polling off...", currVarPath );
    }

    _doRPC( campSrv_varPoll( currVarPath, flag, (float)interval ), "Poll variable", buf );

    return( CAMP_SUCCESS );
}


int 
menuVarAlarm( void )
{
    int camp_status;
    bool_t flag;
    char ident[LEN_IDENT+1];
    char buf[LEN_BUF+1];
    
    if( !inputVarAlarm( currVarPath, &flag, ident, sizeof( ident ) ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" alarm to \"%s\"...", 
                      currVarPath, ident );
    }
    else
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" alarm off...", currVarPath );
    }

    _doRPC( campSrv_varAlarm( currVarPath, flag, ident ), "Alarm variable", buf );

    return( CAMP_SUCCESS );
}


int 
menuVarLog( void )
{
    int camp_status;
    bool_t flag;
    char ident[LEN_IDENT+1];
    char buf[LEN_BUF+1];
    
    if( !inputVarLog( currVarPath, &flag, ident, sizeof( ident ) ) ) return( CAMP_SUCCESS );

    if( flag )
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" logging to \"%s\"...", 
                      currVarPath, ident );
    }
    else
    {
        camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" logging off...", currVarPath );
    }

    _doRPC( campSrv_varLog( currVarPath, flag, ident ), "Log variable", buf );

    return( CAMP_SUCCESS );
}


int 
menuVarZero( void )
{
    int camp_status;
    char buf[LEN_BUF+1];

    camp_snprintf( buf, LEN_BUF+1, "Zeroing variable \"%s\" statistics...", currVarPath );

    _doRPC( campSrv_varZero( currVarPath ), "Zero variable", buf );

    return( CAMP_SUCCESS );
}


int 
menuVarTol( void )
{
    int camp_status;
    u_long tolType;
    double tol;
    CAMP_VAR* pVar;
    char buf[LEN_BUF+1];
    char str[LEN_STR+1];

#define NONE_TEXT "<undefined>"
    
    /*
     *  Only NUMERIC variables with attribute ALARM
     *  (SELECTION variables not yet implemented)
     */

    /* 
     *  get pointer to chosen data
     *  for LINKs, this gets the pointer to the linked data
     */
    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar == NULL ) return( CAMP_SUCCESS );

    if( !inputVarTol( pVar, &tolType, &tol ) )
    {
        return( CAMP_SUCCESS );
    }

    switch( tolType )
    {
    case 0:
        if( tol >= 0.0 ) 
	{
	    camp_snprintf( str, LEN_STR+1, "+-%f", tol );
        }
        else 
	{
	    camp_strncpy( str, sizeof( str ), NONE_TEXT ); // strcpy( str, NONE_TEXT );
        }
        break;

    case 1:
        if( tol >= 0.0 ) 
	{
	    camp_snprintf( str, LEN_STR+1, "%.0f%%", tol );
        }
        else 
	{
	    camp_strncpy( str, sizeof( str ), NONE_TEXT ); // strcpy( str, NONE_TEXT );
        }
        break;

    default:
	camp_strncpy( str, sizeof( str ), NONE_TEXT ); // strcpy( str, NONE_TEXT );
        break;
    }

    camp_snprintf( buf, LEN_BUF+1, "Setting variable \"%s\" tolerance to %s...", 
                    currVarPath, str );

    _doRPC( campSrv_varNumSetTol( currVarPath, tolType, (float)tol ), "Variable tolerance", buf );

    return( CAMP_SUCCESS );
}


int
cbMainMenuHelpAbout( void )
{
    int y;
    extern WINDOW* helpWin;
    extern char* prog_name;

    createHelpWin();

    y = 0;
    mvwaddstr( helpWin, y++, 0, 
"About the CAMP Character-cell User Interface" );
    y++;
    wmove( helpWin, y++, 0 );
    wprintw( helpWin,
"  Version:          %s", prog_name );
    wmove( helpWin, y++, 0 );
    wprintw( helpWin,
"  Server version:   %s", pSys->prgName );
    y++;
    mvwaddstr( helpWin, y++, 0, 
"  To get help:" );
    mvwaddstr( helpWin, y++, 0, 
"    from CAMP CUI:  choose 'CUI User Manual' from the 'Help' menu" );
#ifdef VMS
    mvwaddstr( helpWin, y++, 0, 
"    from DCL:       $ help @camp user_manual" );
#endif
    mvwaddstr( helpWin, y++, 0, 
"    printed:        see contact" );
    y++;
    mvwaddstr( helpWin, y++, 0, 
"  Contact:          Suzannah Daviel (suz@triumf.ca)" );
    mvwaddstr( helpWin, y++, 0, 
"                    Donald Arseneau (asnd@triumf.ca)" );

#define  QUIT_TEXT  "Press any key to continue"
    wattron( helpWin, A_REVERSE );
    wmove( helpWin, HELPWIN_H-1, (HELPWIN_W-strlen(QUIT_TEXT))/2 );
    wprintw( helpWin, QUIT_TEXT );
    wattroff( helpWin, A_REVERSE );

    wrefresh( helpWin );

    getKey();

    deleteHelpWin();

    return( CAMP_SUCCESS );
}


int
cbMainMenuHelpManual( void )
{
    extern WINDOW* helpWin;
#ifdef VMS
#include hlpdef
    s_dsc line_desc;
    s_dsc library_name;
    long output_width;
    long flags;

    createHelpWin();

    setdsctostr( &line_desc, "user_manual cui_manual" );
    setdsctostr( &library_name, "camp:[lib]camp.hlb" );
    output_width = HELPWIN_W;
    flags = HLP$M_PROMPT | HLP$M_HELP;
    lbr$output_help( helpPutOutput, &output_width, 
                     &line_desc, &library_name,
                     &flags, helpGetInput );

    deleteHelpWin();

#else /* Non-VMS, but still use VMS help files. */
    char library_path[90] = { "" };
    char subject[64] = { "User_Manual" };
    const char library_name[] = { "camp_user.hlp" };

    find_help_lib( library_name, library_path, 90 );

    createHelpWin();

    vmshelp_reader ( subject, library_path );

    deleteHelpWin();

#endif /* VMS */

    return( CAMP_SUCCESS );
}


int
cbMenuToggleBeeps( void )
{
    noisy = !noisy;

    return( CAMP_SUCCESS );
}

int 
cbMenuUpdateInterval( void )
{
  long isec, dflt;

  dflt = poll_interval;

  if ( inputInteger( "Input Update Interval (sec)", dflt, &isec ) )
  {
      if ( isec > 1 && isec <= 60.0 ) 
      {
          poll_interval = isec;
      }
  }

    return( CAMP_SUCCESS );
}

int
cbMenuPauseTime( void )
{
  double dsec, dflt;

  dflt = 1.0e-6 * msgPause_us;

  if ( inputFloat( "Input Message Pause Time (sec)", dflt, &dsec ) )
  {
      if ( dsec > 0.0 && dsec <= 15.0 ) 
      {
          msgPause_us = (int) (1000000.0*dsec);
      }
  }

    return( CAMP_SUCCESS );
}

int
cbMainMenuHelpCamp( void )
{
    extern WINDOW* helpWin;
#ifdef VMS

#include hlpdef
    s_dsc line_desc;
    s_dsc library_name;
    long output_width;
    long flags;

    createHelpWin();

    setdsctostr( &line_desc, "" );
    setdsctostr( &library_name, "camp:[lib]camp.hlb" );
    output_width = HELPWIN_W;
    flags = HLP$M_PROMPT | HLP$M_HELP;
    lbr$output_help( helpPutOutput, &output_width, 
                     &line_desc, &library_name,
                     &flags, helpGetInput );

    deleteHelpWin();

#else 

    char library_path[90] = { "" };
    char subject[64] = { "" };
    const char library_name[] = { "camp_soft.hlp" };

    find_help_lib( library_name, library_path, 90 );

    createHelpWin();

    vmshelp_reader ( subject, library_path );

    deleteHelpWin();

#endif /* VMS */

    return( CAMP_SUCCESS );
}


int
doMainMenu( void )
{
    int n;
    u_int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    choices[n] = "Configure";
    callback[n] = cbMainMenuConfig;
    n++;

    choices[n] = "Options";
    callback[n] = cbMainMenuOpt;
    n++;

    choices[n] = "Help";
    callback[n] = cbMainMenuHelp;
    n++;
    choices[n] = "Exit CAMP";
    callback[n] = cbMainMenuQuit;
    n++;

    for( ;; )
    {
        if( !inputSelectWin( "Main menu", n, choices, 0, &select ) )
        {
            break;
        }

        (*callback[select])();
    }

    return( CAMP_SUCCESS );
}


int
cbMainMenuConfig( void )
{
    int n;
    u_int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;
    choices[n] = "Add instrument";
    callback[n] = menuInsAdd;
    n++;
    choices[n] = "Delete instrument"; 
    callback[n] = menuInsDel;
    n++;
    choices[n] = "Recreate instrument"; 
    callback[n] = menuInsRecreate;
    n++;
    choices[n] = "Open config file";
    callback[n] = menuFileLoadNew;
    n++;
    choices[n] = "Import (add) config file";
    callback[n] = menuFileLoad;
    n++;
    choices[n] = "Save config";
    callback[n] = menuFileSave;
    n++;

    if( !inputSelectWin( "Configure", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    (*callback[select])();

    return( CAMP_SUCCESS );
}

int
cbMainMenuOpt( void )
{
    int n;
    u_int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    if (noisy)
    {
      choices[n] = "Disable beeps";
    }
    else
    {
      choices[n] = "Enable beeps";
    }
    callback[n] = cbMenuToggleBeeps;
    n++;

    choices[n] = "Set update interval";
    callback[n] = cbMenuUpdateInterval;
    n++;

    choices[n] = "Set message pause time";
    callback[n] = cbMenuPauseTime;
    n++;

    if( !inputSelectWin( "Options", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    (*callback[select])();

    return( CAMP_SUCCESS );
}


int
cbMainMenuHelp( void )
{
    int n;
    u_int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    choices[n] = "About CAMP CUI";
    callback[n] = cbMainMenuHelpAbout;
    n++;
    choices[n] = "CUI User Manual";
    callback[n] = cbMainMenuHelpManual;
    n++;
    choices[n] = "CAMP Software";
    callback[n] = cbMainMenuHelpCamp;
    n++;

    if( !inputSelectWin( "Help", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    (*callback[select])();

    return( CAMP_SUCCESS );
}


int
cbMainMenuQuit( void )
{
/*
    int n;
    u_int select;
    char* choices[MAX_NUM_SELECTIONS];
    int (*callback[MAX_NUM_SELECTIONS])( void );

    n = 0;

    choices[n] = "No";
    n++;
    choices[n] = "Yes";
    n++;

    if( !inputSelectWin( "Confirm exit", n, choices, 0, &select ) )
    {
        return( CAMP_SUCCESS );
    }

    if( select == 0 ) return( CAMP_SUCCESS );
*/
    camp_cui_shutdown();
    exit( 0 );

    return( CAMP_SUCCESS );
}
