
/*
 *  Name:       camp_cui_status.c
 *
 *  Purpose:    Manage the Status window (the top bar) which is always
 *              visible.
 *
 *  Revision history:
 *
 */

#include <curses.h>
#include "camp_cui.h"


/* 
 *  This window is used 'extern'ally by getKey (in camp_cui_keyboard)
 *  because wgetch( statusWin ) will never cause the wrong window to 
 *  cover anything; in particular, getch() would bring up a blank stdscr.
 */

WINDOW *statusWin = (WINDOW*)NULL;


int
initStatusDisplay( void )
{
    statusWin = newwin(  1, 
                         SCREEN_W, 
                         STATUSWIN_Y, 
                         0 );

    if( statusWin == (WINDOW*)NULL ) 
    {
        return( CAMP_FAILURE );
    }

    leaveok( statusWin, TRUE );
    scrollok( statusWin, FALSE );
    wattron( statusWin, A_REVERSE );
    keypad( statusWin, TRUE );
    immedok( statusWin, FALSE );
    clearok( statusWin, FALSE );
    wtimeout( statusWin, -1);

    return( CAMP_SUCCESS );
}


void 
deleteStatusDisplay( void )
{
    if( statusWin != NULL ) 
    {
        delwin( statusWin );
        statusWin = NULL;
    }
}


void 
touchStatusDisplay( void )
{
    if( statusWin != NULL ) touchwin( statusWin );
}


void 
refreshStatusDisplay( void )
{
    if( statusWin != NULL ) wrefresh( statusWin );
}

#ifndef VMS
void 
uncoverStatusDisplay( void )
{
    if( statusWin != NULL ) 
    {
       touchwin( statusWin );
       wnoutrefresh( statusWin );
    }
}
#endif


void 
clearStatus( void )
{
    werase( statusWin );
    wrefresh( statusWin );
}


void 
printStatus( void )
{
    extern char camp_serverHostname[LEN_NODENAME+1];
    extern char* prog_name;

    if( statusWin == NULL ) return;

    werase( statusWin );
/*  That should have painted the whole line bright, but it doesn't
 *  work in Linux ncurses.
 */
/*
    wprintw( statusWin, " CAMP CUI | Server: %s | View path: %s", 
        camp_serverHostname, currViewPath );
*/
    wmove( statusWin, 0, 0 );
#ifdef linux
    /* Paint the status bar bright: */
    whline( statusWin, ' ', SCREEN_W );
#endif
    wprintw( statusWin, " %s | Server: %s", prog_name, camp_serverHostname );
    wrefresh( statusWin );
}
