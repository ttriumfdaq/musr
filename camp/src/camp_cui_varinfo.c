/*
 *  $Id: camp_cui_varinfo.c,v 1.12 2015/09/26 01:56:33 asnd Exp $
 *
 *  $Revision: 1.12 $
 *
 *  Purpose:    Manage and display the variable info window (the window
 *              on the right of the screen with the "Variable" title).
 *
 *
 *  $Log: camp_cui_varinfo.c,v $
 *  Revision 1.12  2015/09/26 01:56:33  asnd
 *  Improve alias name/message display
 *
 *  Revision 1.11  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.8  2013/04/16 08:16:41  asnd
 *  Make var titles changeable. Let camp_cui change units. Remove some useless varinfo code.
 *
 *  Revision 1.7  2009/08/19 02:25:52  asnd
 *  Ensure all windows flagged with NULL when deleted
 *
 *  Revision 1.6  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.5  2005/07/06 03:53:32  asnd
 *  Change menus (and hotkeys) for loading instrument configurations.
 *
 *  Revision 1.4  2004/01/28 03:14:37  asnd
 *  Add VME interface type.
 *
 *  Revision 1.3  2000/12/22 23:48:11  David.Morris
 *  Added INDPAK display of parameters
 *
 *
 *  Revision history:
 *  00-Oct-1999  DA     Clean up representation of floating point numbers,
 *                      Make some other things fit in their fields,
 *                      For Linux, uncoverVarInfoWin (automatic in VMS),
 *  24-Jan-2000  DA     Display the first non-null of: message, help, title
 *  20140407     TW     MscbAddr, Access delay, Timeout
 *  20140410     TW     RS232 Data,Parity,Stop on one line (so that displaying 
 *                      AccessDelay/Timeout doesn't require an extra line)
 */

#include <stdio.h>
#include <curses.h>
#include <ctype.h> // isspace
#include "timeval.h"
#include "camp_cui.h"

static WINDOW* varInfoWin = NULL;

extern char currVarPath[LEN_PATH+1];

/* 
 *  Given a character buffer containing a floating point number, return 
 *  a representation of that number that fits a specified width.
 *  PACK_PRECISION is the maximum precision, to ensure that packDoubleF
 *  does not fill a wide field width with garbage digits.
 */

#define PACK_PRECISION 11

static char*
packDoubleF( int width, char* buf, size_t buf_size )
{
  double var, avar;
  int i,w;

    i = sscanf( buf, "%lf", &var );
    if( i==1 )
    {
      avar = var;
      if( var < 0.0 ) 
      {
	++i;
	avar = -var;
      }
      w = _min( width-i, PACK_PRECISION);
      if( ((avar >= 100000.) || (avar < 0.001)) && (avar > 1.0e-15) )
	  camp_snprintf( buf, buf_size, "%*.*le", width, _max(0, _min(width-i-5, PACK_PRECISION-1)), var);
      else if( avar >= 10000. )
	  camp_snprintf( buf, buf_size, "%*.*lf", width, w-5, var);
      else if( avar >= 1000. )
	  camp_snprintf( buf, buf_size, "%*.*lf", width, w-4, var);
      else if( avar >= 100. )
	  camp_snprintf( buf, buf_size, "%*.*lf", width, w-3, var);
      else if( avar >= 10. )
	  camp_snprintf( buf, buf_size, "%*.*lf", width, w-2, var);
      else
	  camp_snprintf( buf, buf_size, "%*.*lf", width, w-1, var);
    }
    return( buf );
}

/*
 * Routines for the display of variable info on the right side of the screen.
 */
int
initVarInfoWin( void )
{
    varInfoWin = newwin( VARINFOWIN_H, VARINFOWIN_W, 
                         VARINFOWIN_Y, VARINFOWIN_X ); 
    if( varInfoWin == NULL )
    {
        return( CAMP_FAILURE );
    }

    leaveok( varInfoWin, FALSE );
    scrollok( varInfoWin, FALSE );
    keypad( varInfoWin, TRUE );
    immedok( varInfoWin, FALSE );
    clearok( varInfoWin, FALSE );
    wtimeout( varInfoWin, -1);

    return( CAMP_SUCCESS );
}


void 
deleteVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) {
      delwin( varInfoWin );
      varInfoWin = (WINDOW*)NULL;
    }
}


void 
clearVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) werase( varInfoWin );
}


void 
touchVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) touchwin( varInfoWin );
}


void 
refreshVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) 
    {
      wrefresh( varInfoWin );
    }
}

#ifndef VMS
void 
uncoverVarInfoWin( void )
{
    if( varInfoWin != (WINDOW*)NULL ) 
    {
      touchwin( varInfoWin );
      wnoutrefresh( varInfoWin );
    }
}
#endif

#define OFF_TEXT    "off"
#define NONE_TEXT   "<undefined>"
#define COL_OFFSET  2
#define COL1        2
#define COL2        (COL1+17)
#define COLTOT_W    (VARINFOWIN_W-4)
#define COL2_W      (VARINFOWIN_W-COL2-2)
#define COL1A       (COL1+COL_OFFSET)
#define COL1A_W     ((VARINFOWIN_W-COL1A-COL_OFFSET-2)/2)
#define COL2A       (COL1A+COL1A_W+COL_OFFSET)
#define COL2A_W     (VARINFOWIN_W-COL2A-2)
#define COLTOTA_W   (COL1A_W+COL_OFFSET+COL2A_W)

#define COL1B       (COL1+COL_OFFSET)
#define COL1B_W     ((VARINFOWIN_W-COL1B-2*COL_OFFSET-2)/3)
#define COL2B       (COL1B+COL1B_W+COL_OFFSET)
#define COL2B_W     COL1B_W
#define COL3B       (COL2B+COL2B_W+COL_OFFSET)
#define COL3B_W     (VARINFOWIN_W-COL3B-2)
#define COLTOTB_W   (COL1B_W+COL_OFFSET+COL2B_W+COL_OFFSET+COL3B_W)

#define _nonull(s)  (((s)==NULL||strlen(s)==0)?NONE_TEXT:(s))
/*
 *  Lines limiting HELP_H (instrument with rs232)
 *    1. top border
 *    2. Title
 *    3. Type
 *    4. Instrument type
 *    5. Locker
 *    6. Com
 *    7. Interface
 *    8. Reads
 *    9. R.Errors
 *   10. R.E.Consec
 *   11. Access delay
 *   12. Port
 *   13. Baud
 *   14. Read term
 *   15. bottom border
 */
#define HELP_H      (VARINFOWIN_H-15)
#define HELP_Y      (VARINFOWIN_H-HELP_H-1)

void
displayVarInfoWin( void )
{
    CAMP_VAR* pVar;
    int y, x, i, w, width;
    char buf[128];
    CAMP_INSTRUMENT* pIns;
    char str[128];
    char *p1, *p2;

    clearVarInfoWin();

    boxWin( varInfoWin, VARINFOWIN_H, VARINFOWIN_W );

    pVar = camp_varGetp_clnt( currVarPath );
    if( pVar == NULL )
    {
        refreshVarInfoWin();
        return;
    }

    y = 0;

    wattron( varInfoWin, A_BOLD );
    camp_snprintf( buf, sizeof( buf ), "Variable: %s", pVar->core.path );
    if ( strlen(buf) > VARINFOWIN_W-2 ) 
	camp_snprintf( buf, sizeof( buf ), "%s", pVar->core.path );
    x = _max( ((VARINFOWIN_W-strlen( buf ))/2), 1 );
    mvwaddstr( varInfoWin, y, x, buf );
    wattroff( varInfoWin, A_BOLD );
    y++;

   /* Title has no hot-key to set. Never skip it, even if blank. */
    mvwaddstr( varInfoWin, y, COL1, "Title: " );
    wmove( varInfoWin, y, COL1+7 );
    if ( pVar->core.title && strlen(pVar->core.title) > 0 ) 
    {
	wprintw( varInfoWin, "%*s", COLTOT_W-7, pVar->core.title );
    }
    y++;

    mvwaddstr( varInfoWin, y, COL1, "Type:" );
    wmove( varInfoWin, y, COL2 );
    wprintw( varInfoWin, "%*s", COL2_W, getTypeStr( pVar->core.varType ) );
    y++;

    if( HELP_H > 1 )
    {
        if ( strlen( pVar->core.statusMsg ) > 0 )
        {
            mvwaddstr( varInfoWin, HELP_Y, COL1, "Message:" );
            /*  funny hack for alias name given in message */
            if( sscanf( pVar->core.statusMsg, "alias: %15s", buf ) == 1 )
            {
                int la = strlen(buf);
                if ( strlen( pVar->core.statusMsg ) > 7+la )
                { /* alias plus a message */
                    camp_snprintf( buf, sizeof( buf ), "(Alias for %s) %s", 
                                   pVar->core.path, &((pVar->core.statusMsg)[7+la]) );
                }
                else
                {
                    camp_snprintf( buf, sizeof( buf ), "(Alias for %s)", pVar->core.path );
                }
                p1 = buf;
            }
            else
            {
                p1 = pVar->core.statusMsg;
            }
        }
        else if ( strlen( pVar->core.help ) > 0 )
        {
            mvwaddstr( varInfoWin, HELP_Y, COL1, "Help:" );
            p1 = pVar->core.help;
        }
        else 
        {
            p1 = NULL;
        }

	if( p1 != NULL )
	{
	    width = VARINFOWIN_W-COL1A-2;
	    /*
	     *  Turn all whitespaces into spaces
	     */
	    for( p2=p1; *p2 != '\0'; p2++ ) if( isspace(*p2) ) *p2 = ' ';
	    /*
	     *  Display the help or long message nicely, without breaking up words.
	     */
	    for( i = 1; i < HELP_H; i++ )
	    {
		while( isspace( *p1 ) ) p1++;
		w = strlen( p1 );
		if( w == 0 ) break;
		else if( w > width )
		{
		    for( p2=p1+width-1; (p2>p1) && !isspace(*p2); p2-- ) ;
		    w = (p2==p1) ? width : p2-p1;
		}
		wmove( varInfoWin, HELP_Y+i, COL1A );
		wprintw( varInfoWin, "%.*s", w, p1 );
		p1 += w;
	    }
	}
    }
 
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_STRUCTURE:
        break;

      case CAMP_VAR_TYPE_INSTRUMENT:

        pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

        mvwaddstr( varInfoWin, y, COL1, "Instrument type:" );
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, pIns->typeIdent );
        y++;

        wmove( varInfoWin, y, COL1 );
        waddstr( varInfoWin, "Loc" );
        wattron( varInfoWin, A_BOLD );
        waddch( varInfoWin, 'k' );
        wattroff( varInfoWin, A_BOLD );
        waddstr( varInfoWin, "er:" );
        if( pVar->core.status & CAMP_INS_ATTR_LOCKED )
        {
            camp_snprintf( buf, sizeof( buf ), "%08x", pIns->lockPID );
        }
        else
        {
            camp_strncpy( buf, sizeof( buf ), "<none>" ); // strcpy( buf, "<none>" );
        }
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, buf );
        y++;

        wmove( varInfoWin, y, COL1 );
        waddch( varInfoWin, 'C' );
        wattron( varInfoWin, A_BOLD );
        waddch( varInfoWin, 'o' );
        wattroff( varInfoWin, A_BOLD );
        waddstr( varInfoWin, "m:" );
        if( ( pIns->pIF == NULL ) || 
            !( pIns->pIF->status & CAMP_IF_ONLINE ) )
        {
            camp_strncpy( buf, sizeof( buf ), "offline" ); // strcpy( buf, "offline" );
        }
        else
        {
            camp_strncpy( buf, sizeof( buf ), "online" ); // strcpy( buf, "online" );
        }
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, buf );
        y++;

        wmove( varInfoWin, y, COL1 );
        wattron( varInfoWin, A_BOLD );
        waddch( varInfoWin, 'I' );
        wattroff( varInfoWin, A_BOLD );
        waddstr( varInfoWin, "nterface:" );
        if( pIns->pIF == NULL )
        {
            camp_strncpy( buf, sizeof( buf ), NONE_TEXT ); // strcpy( buf, NONE_TEXT );
        }
        else
        {
            camp_strncpy( buf, sizeof( buf ), pIns->pIF->typeIdent ); // strcpy( buf, pIns->pIF->typeIdent );
        }
        wmove( varInfoWin, y, COL2 );
        wprintw( varInfoWin, "%*s", COL2_W, buf );
        y++;

        if( pIns->pIF == NULL ) break;

        wmove( varInfoWin, y, COL1A );
        wprintw( varInfoWin, "%*ld", COL1A_W, pIns->pIF->numReads );
        mvwaddstr( varInfoWin, y, COL1A, "Reads:" );

        wmove( varInfoWin, y, COL2A );
        wprintw( varInfoWin, "%*ld", COL2A_W, pIns->pIF->numWrites );
        mvwaddstr( varInfoWin, y, COL2A, "Writes:" );
        y++;

        wmove( varInfoWin, y, COL1A );
        wprintw( varInfoWin, "%*ld", COL1A_W, pIns->pIF->numReadErrors );
        mvwaddstr( varInfoWin, y, COL1A, "R.Errors:" );

        wmove( varInfoWin, y, COL2A );
        wprintw( varInfoWin, "%*ld", COL2A_W, pIns->pIF->numWriteErrors );
        mvwaddstr( varInfoWin, y, COL2A, "W.Errors:" );
        y++;

        wmove( varInfoWin, y, COL1A );
        wprintw( varInfoWin, "%*ld", COL1A_W, pIns->pIF->numConsecReadErrors );
        mvwaddstr( varInfoWin, y, COL1A, "R.E.Consec:" );

        wmove( varInfoWin, y, COL2A );
        wprintw( varInfoWin, "%*ld", COL2A_W, pIns->pIF->numConsecWriteErrors );
        mvwaddstr( varInfoWin, y, COL2A, "W.E.Consec:" );
        y++;

        wmove( varInfoWin, y, COL1A );
        wprintw( varInfoWin, "%*.2f", COL1A_W, pIns->pIF->accessDelay );
        mvwaddstr( varInfoWin, y, COL1A, "Access delay:" );

        wmove( varInfoWin, y, COL2A );
        wprintw( varInfoWin, "%*.2f", COL2A_W, pIns->pIF->timeout );
        mvwaddstr( varInfoWin, y, COL2A, "Timeout:" );
        y++;

        switch( pIns->pIF->typeID )
        {
          case CAMP_IF_TYPE_NONE:
            break;

          case CAMP_IF_TYPE_RS232:
            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W,
		     camp_getIfRs232Port( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL1A, "Port:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W,
			 camp_getIfRs232Baud( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Baud:" );

/*             wmove( varInfoWin, y, COL1B ); */
/*             wprintw( varInfoWin, "%*d", COL1B_W, camp_getIfRs232Data( pIns->pIF->defn ) ); */
/*             mvwaddstr( varInfoWin, y, COL1B, "Data:" ); */

/*             wmove( varInfoWin, y, COL2B ); */
/*             wprintw( varInfoWin, "%*s", COL2B_W, camp_getIfRs232Parity( pIns->pIF->defn, str, sizeof( str ) ) ); */
/*             mvwaddstr( varInfoWin, y, COL2B, "Parity:" ); */

/*             wmove( varInfoWin, y, COL3B ); */
/*             wprintw( varInfoWin, "%*d", COL3B_W, camp_getIfRs232Stop( pIns->pIF->defn ) ); */
/*             mvwaddstr( varInfoWin, y, COL3B, "Stop:" ); */

            wmove( varInfoWin, y, COL2A );
	    camp_snprintf( buf, sizeof( buf ), "%d %s %d"
		     , camp_getIfRs232Data( pIns->pIF->defn ) 
		     , camp_getIfRs232Parity( pIns->pIF->defn, str, sizeof( str ) )
		     , camp_getIfRs232Stop( pIns->pIF->defn ) 
		);
	    wprintw( varInfoWin, "%*s", COL2A_W, buf );
            mvwaddstr( varInfoWin, y, COL2A, "Encoding:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfRs232ReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL1A, "Read term:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfRs232WriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL2A, "Write term:" );
            y++;

            /* wmove( varInfoWin, y, COL1A ); */
            /* wprintw( varInfoWin, "%*d", COL1A_W,  */
	    /* 		 camp_getIfRs232Timeout( pIns->pIF->defn ) ); */
            /* mvwaddstr( varInfoWin, y, COL1A, "Timeout:" ); */

            break;

          case CAMP_IF_TYPE_GPIB:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfGpibAddr( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "GPIB addr:" );
            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfGpibReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL1A, "Read term:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfGpibWriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL2A, "Write term:" );
            y++;

            break;

          case CAMP_IF_TYPE_GPIB_MSCB:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfGpibAddr( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "GPIB addr:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*d", COL2A_W, 
			 camp_getIfGpibMscbAddr( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL2A, "MSCB addr:" );
            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfGpibReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL1A, "Read term:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfGpibWriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL2A, "Write term:" );
            y++;

            break;

          case CAMP_IF_TYPE_CAMAC:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfCamacB( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Branch:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfCamacC( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Crate:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfCamacN( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Slot:" );

            y++;

            break;

          case CAMP_IF_TYPE_TCPIP:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W,
			 camp_getIfTcpipAddr( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL1A, "Address:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*d", COL2A_W,
			 camp_getIfTcpipPort( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL2A, "Port:" );
            y++;

            /* wmove( varInfoWin, y, COL2A ); */
            /* wprintw( varInfoWin, "%*.1f", COL2A_W,  */
	    /* 		 camp_getIfTcpipTimeout( pIns->pIF->defn ) ); */
            /* mvwaddstr( varInfoWin, y, COL2A, "Timeout:" ); */
            /* y++; */

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfTcpipReadTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL1A, "Read term:" );

            wmove( varInfoWin, y, COL2A );
            wprintw( varInfoWin, "%*s", COL2A_W, 
			 camp_getIfTcpipWriteTerm( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL2A, "Write term:" );
            y++;

            break;

          case CAMP_IF_TYPE_INDPAK:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfIndpakSlot( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Slot:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*s", COL1A_W, 
			 camp_getIfIndpakType( pIns->pIF->defn, str, sizeof( str ) ) );
            mvwaddstr( varInfoWin, y, COL1A, "Type:" );

            y++;

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*d", COL1A_W, 
			 camp_getIfIndpakChannel( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Channel:" );

            y++;

            break;

          case CAMP_IF_TYPE_VME:

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "%*ld", COL1A_W, 
		     camp_getIfVmeBase( pIns->pIF->defn ) );
            mvwaddstr( varInfoWin, y, COL1A, "Base:" );

            y++;
            break;

          case CAMP_IF_TYPE_TICS:
            break;

          default:
            break;
        }

        break;

      default:
        if( ( pVar->core.status & CAMP_VAR_ATTR_READ ) ||
            ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
        {
            wmove( varInfoWin, y, COL1 );
            waddstr( varInfoWin, "Value (" );

            if( ( pVar->core.status & CAMP_VAR_ATTR_READ ) &&
                ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
            {
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 's' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "et/" );
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 'r' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "ead" );
            }
            else if( pVar->core.status & CAMP_VAR_ATTR_READ )
            {
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 'r' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "ead" );
            }
            else if( pVar->core.status & CAMP_VAR_ATTR_SET )
            {
                wattron( varInfoWin, A_BOLD );
                waddch( varInfoWin, 's' );
                wattroff( varInfoWin, A_BOLD );
                waddstr( varInfoWin, "et" );
            }

            waddstr( varInfoWin, "):" );
        
            if( pVar->core.status & CAMP_VAR_ATTR_IS_SET )
            {
                varGetValStr( pVar, buf, sizeof( buf ) );
            }
            else
            {
                camp_strncpy( buf, sizeof( buf ), NONE_TEXT ); // strcpy( buf, NONE_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( pVar->core.attributes & CAMP_VAR_ATTR_POLL )
        {
            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'P' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "olling:" );
            if( pVar->core.status & CAMP_VAR_ATTR_POLL )
            {
                camp_snprintf( buf, sizeof( buf ), "%.1f sec", pVar->core.pollInterval );
            }
            else
            {
                camp_strncpy( buf, sizeof( buf ), OFF_TEXT ); // strcpy( buf, OFF_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( pVar->core.attributes & CAMP_VAR_ATTR_LOG )
        {
            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'L' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "ogging:" );
            if( pVar->core.status & CAMP_VAR_ATTR_LOG )
            {
                camp_strncpy( buf, sizeof( buf ), pVar->core.logAction ); // strcpy( buf, pVar->core.logAction );
            }
            else
            {
                camp_strncpy( buf, sizeof( buf ), OFF_TEXT ); // strcpy( buf, OFF_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( pVar->core.attributes & CAMP_VAR_ATTR_ALARM )
        {
            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'A' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "larm:" );
            if( pVar->core.status & CAMP_VAR_ATTR_ALARM )
            {
                camp_strncpy( buf, sizeof( buf ), pVar->core.alarmAction ); // strcpy( buf, pVar->core.alarmAction );
            }
            else
            {
                camp_strncpy( buf, sizeof( buf ), OFF_TEXT ); // strcpy( buf, OFF_TEXT );
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( ( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) ==
              CAMP_VAR_TYPE_NUMERIC ) &&
            ( pVar->core.attributes & CAMP_VAR_ATTR_ALARM ) )
        {
            CAMP_NUMERIC* pNum;

            pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

            wmove( varInfoWin, y, COL1 );
            wattron( varInfoWin, A_BOLD );
            waddch( varInfoWin, 'T' );
            wattroff( varInfoWin, A_BOLD );
            waddstr( varInfoWin, "olerance:" );
            switch( pNum->tolType )
            {
	    case 0:
		if( pNum->tol >= 0.0 ) camp_snprintf( buf, sizeof( buf ), "+-%f", pNum->tol );
		else camp_strncpy( buf, sizeof( buf ), NONE_TEXT ); // strcpy( buf, NONE_TEXT );
		break;
	    case 1:
                if( pNum->tol >= 0.0 ) camp_snprintf( buf, sizeof( buf ), "%.0f%%", pNum->tol );
                else camp_strncpy( buf, sizeof( buf ), NONE_TEXT ); // strcpy( buf, NONE_TEXT );
                break;
	    default:
                camp_strncpy( buf, sizeof( buf ), NONE_TEXT ); // strcpy( buf, NONE_TEXT );
                break;
            }
            wmove( varInfoWin, y, COL2 );
            wprintw( varInfoWin, "%*s", COL2_W, buf );
        }
        y++;

        if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) ==
              CAMP_VAR_TYPE_NUMERIC )
        {
            CAMP_NUMERIC* pNum;

            pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

            wmove( varInfoWin, y, COL2 );
            if( !streq( pNum->units, "" ) ) 
            {
                wprintw( varInfoWin, "%*s", COL2_W, pNum->units );
            }
            else
            {
                wprintw( varInfoWin, "%*s", COL2_W, NONE_TEXT );
            }
            wmove( varInfoWin, y, COL1 );
            /* No hot-key for units, because setting should be rare.
             *wattron( varInfoWin, A_BOLD );
             *waddch( varInfoWin, 'U' );
             *wattroff( varInfoWin, A_BOLD );
             */
            waddstr( varInfoWin, "Units:" );
        }
        y++;

        if( ( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) ==
              CAMP_VAR_TYPE_NUMERIC ) && 
            ( pVar->spec.CAMP_VAR_SPEC_u.pNum->timeStarted.tv_sec > 0 ) )
        {
            double mean, stddev, skew;
	    int bl;
            CAMP_NUMERIC* pNum;

            pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

            camp_strncpy( buf, sizeof( buf ), asctimeval( &pNum->timeStarted ) ); // strcpy( buf, asctimeval( &pNum->timeStarted ) );
	    bl = strlen(buf) - 6;
            buf[bl] = '\0';
            wmove( varInfoWin, y, COL1 );
	    if( COLTOT_W - bl < 6 )
	    {
	      wprintw( varInfoWin, "%s", buf );
	    }
	    else if( COLTOT_W - bl < 12 )
	    {
	      wprintw( varInfoWin, "%-*s%s", COLTOT_W - bl, "Start:", buf );
	    }
	    else if( COLTOT_W - bl < 21 )
	    {
	      wprintw( varInfoWin, "%-*s%s", COLTOT_W - bl, "Stats since:", buf );
	    }
	    else
	    {
	      wprintw( varInfoWin, "%-*s%s", COLTOT_W - bl, "Statistics starting:", buf );
	    }

            y++;

            wmove( varInfoWin, y, COL1A );
            numGetValStr( pVar->core.varType, pNum->low, buf, sizeof( buf ) );
            wprintw( varInfoWin, "Low:  %s", packDoubleF( COL1A_W-6, buf, sizeof( buf ) ) );

            wmove( varInfoWin, y, COL2A );
            numGetValStr( pVar->core.varType, pNum->hi, buf, sizeof( buf ) );
            wprintw( varInfoWin, "High: %s", packDoubleF( COL2A_W-6, buf, sizeof( buf ) ) );

            y++;

            camp_varNumCalcStats( pVar->core.path, &mean, &stddev, &skew );

            wmove( varInfoWin, y, COL1A );
            wprintw( varInfoWin, "Num: %*ld", COL1A_W-5, pNum->num );

            wmove( varInfoWin, y, COL2A );
            numGetValStr( pVar->core.varType, mean, buf, sizeof( buf ) );
            wprintw( varInfoWin, "Mean: %s", packDoubleF( COL2A_W-6, buf, sizeof( buf ) ) );

            y++;

            wmove( varInfoWin, y, COL1A );
            numGetValStr( pVar->core.varType, stddev, buf, sizeof( buf ) );
            wprintw( varInfoWin, "StDv: %s", packDoubleF( COL1A_W-6, buf, sizeof( buf ) ) );

            wmove( varInfoWin, y, COL2A );
            numGetValStr( pVar->core.varType, skew, buf, sizeof( buf ) );
            wprintw( varInfoWin, "Skew: %s", packDoubleF( COL2A_W-6, buf, sizeof( buf ) ) );

            y++;
        }

        break;
    }

    refreshVarInfoWin();
}

