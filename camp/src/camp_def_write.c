/*
 *  Name:       camp_def_write.c
 *
 *  Purpose:    Write an instrument definition file based on the current
 *              instrument state.
 *              This file should be equivalent to an instrument driver file.
 *
 *              campDef_write is only invoked from campsrv_inssave when
 *              a flag is set with bit 2 set on (flag&2).  There is currently 
 *              no way to invoke this using the Tcl command insSave.
 *
 *  Called by:  campsrv_inssave (in camp_srv_proc.c) calls campDef_write
 *              Other routines are called by camp_ini_write.c
 * 
 *  Revision history:
 *    22-Dec-1999  TW  Extra check that instrument proc's aren't NULL
 *
 */

#include <stdio.h>
#include "camp_srv.h"

static char curr_path[LEN_PATH+1];


/*
 *  campDef_write(),        
 */
int
campDef_write( const char* filename, CAMP_VAR* pVar )
{
    int camp_status;

    camp_status = openOutput( filename );

    if( _failure( camp_status ) ) 
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed openOutput" ); }
	return( camp_status );
    }

    /* 
     *  Init things
     */
    reinitialize( filename, NULL, NULL );

    camp_pathInit( curr_path, LEN_PATH+1 );
    camp_pathDown( curr_path, LEN_PATH+1, pVar->core.ident );

    defWrite_var( pVar );

    print( "return -code ok\n" );

    closeOutput();

    return( CAMP_SUCCESS );
}


void 
defWrite_var( CAMP_VAR* pVar )
{
    defWrite_stat( &pVar->core );
    defWrite_dyna( &pVar->core );
    defWrite_spec( pVar );
}


void 
defWrite_stat( CAMP_VAR_CORE* pCore )
{
    char str[LEN_STR+1];

    camp_varGetTypeStr( pCore->path, str, sizeof( str ) );
    print_tab( "%s ", str );
    print( "%s ", curr_path );

    if( pCore->attributes & CAMP_VAR_ATTR_SHOW ) 
        print( "%s ", toktostr( TOK_SHOW_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_SET ) 
        print( "%s ", toktostr( TOK_SET_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_READ ) 
        print( "%s ", toktostr( TOK_READ_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_POLL ) 
        print( "%s ", toktostr( TOK_POLL_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_LOG ) 
        print( "%s ", toktostr( TOK_LOG_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_ALARM ) 
        print( "%s ", toktostr( TOK_ALARM_ATTR ) );

    if( pCore->title[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_TITLE ), pCore->title );
    }
    if( pCore->help[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_HELP ), pCore->help );
    }
    if( pCore->readProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_READPROC ), pCore->readProc );
    }
    if( pCore->writeProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_WRITEPROC ), pCore->writeProc );
    }
}


void 
defWrite_dyna( CAMP_VAR_CORE* pCore )
{
    if( pCore->status & CAMP_VAR_ATTR_SHOW ) 
    {
        print( "%s %s ", toktostr( TOK_SHOW_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->status & CAMP_VAR_ATTR_SET ) 
    {
        print( "%s %s ", toktostr( TOK_SET_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->status & CAMP_VAR_ATTR_READ ) 
    {
        print( "%s %s ", toktostr( TOK_READ_FLAG ), toktostr( TOK_ON ) );
    }

/*
 *  Comment here was: 
 *	This can cause major problems
 *	Sorry, it'll have to be done individually
 *  and the writing of "-p on" was commented-out. 
 *
 *  But what problems did it cause?  DJA does not see a problem on preliminary
 *  tests.  I guess that there may have been a difficulty when the Camp server
 *  reboots and poll actions may have happened before an instrument was properly
 *  initialized.  If so, the instrument mutex should now prevent those problems.
 *  DJA restores the writing of "-p on", but only in the case where the poll
 *  interval is positive (Dec 2004).
 */
    if( (pCore->status & CAMP_VAR_ATTR_POLL) && (pCore->pollInterval > 0.0) ) 
    {
        print( "%s %s ", toktostr( TOK_POLL_FLAG ), toktostr( TOK_ON ) );
    }
/* */

    if( pCore->pollInterval > 0.0 ) 
    {
        char str[LEN_STR+1];
        camp_snprintf( str, sizeof( str ), "%f", pCore->pollInterval );
        numStrTrimz( str );
        print( "%s %s ", toktostr( TOK_POLL_INTERVAL ), str );
    }

    if( pCore->status & CAMP_VAR_ATTR_LOG ) 
    {
        print( "%s %s ", toktostr( TOK_LOG_FLAG ), toktostr( TOK_ON ) );
    }
    if( !streq( pCore->logAction, "" ) )
    {
        print( "%s %s ", toktostr( TOK_LOG_ACTION ), pCore->logAction );
    }

    if( pCore->status & CAMP_VAR_ATTR_ALARM ) 
    {
        print( "%s %s ", toktostr( TOK_ALARM_FLAG ), toktostr( TOK_ON ) );
    }
    if( !streq( pCore->alarmAction, "" ) )
    {
        print( "%s %s ", toktostr( TOK_ALARM_ACTION ), pCore->alarmAction );
    }
}


void 
defWrite_spec( CAMP_VAR* pVar )
{
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
      {
      case CAMP_VAR_TYPE_NUMERIC:
        defWrite_Numeric( pVar );
        break;
      case CAMP_VAR_TYPE_SELECTION:
        defWrite_Selection( pVar );
        break;
      case CAMP_VAR_TYPE_STRING:
        defWrite_String( pVar );
        break;
      case CAMP_VAR_TYPE_ARRAY:
        defWrite_Array( pVar );
        break;
      case CAMP_VAR_TYPE_STRUCTURE:
        defWrite_Structure( pVar );
        break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        defWrite_Facility( pVar );
        break;
      case CAMP_VAR_TYPE_LINK:
        defWrite_Link( pVar );
        break;
    }
    return;
}


void 
defWrite_Numeric( CAMP_VAR* pVar )
{
  CAMP_NUMERIC* pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;
  char str[LEN_STR+1];
  
  if( pNum->tol >= 0.0 ) 
    {
      print( "%s %d ", toktostr( TOK_ALARM_TOLTYPE ), pNum->tolType );
      camp_snprintf( str, sizeof( str ), "%f", pNum->tol);
      numStrTrimz( str );
      print( "%s %s ", toktostr( TOK_ALARM_TOL ), str );
    }
  
  if( !streq( pNum->units, "" ) )
    {
      print( "%s {%s} ", toktostr( TOK_UNITS ), pNum->units );
    }
  
  if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
     ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
	numGetValStr( pVar->core.varType, pNum->val, str, sizeof( str ) );
      numStrTrimz( str );
      print( "%s %s ", toktostr( TOK_VALUE_FLAG ), str );
    }
  print( "\n" );
}


void 
defWrite_Selection( CAMP_VAR* pVar )
{
    CAMP_SELECTION* pSel = pVar->spec.CAMP_VAR_SPEC_u.pSel;
    CAMP_SELECTION_ITEM* pSelItem;

    print( "%s ", toktostr( TOK_SELECTIONS ) );
    for( pSelItem = pSel->pItems; pSelItem != NULL; pSelItem = pSelItem->pNext )
    {
        print( "%s ", pSelItem->label );
    }

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s %d ", toktostr( TOK_VALUE_FLAG ), pSel->val );
    }
    print( "\n" );
}


void 
defWrite_String( CAMP_VAR* pVar )
{
    CAMP_STRING* pStr = pVar->spec.CAMP_VAR_SPEC_u.pStr;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s {%s} ", toktostr( TOK_VALUE_FLAG ), pStr->val );
    }
    print( "\n" );
}


void 
defWrite_Array( CAMP_VAR* pVar )
{
    CAMP_ARRAY* pArr = pVar->spec.CAMP_VAR_SPEC_u.pArr;
    int tokKind;
    int i;

    switch( pArr->varType )
    {
    case CAMP_VAR_TYPE_INT: tokKind = TOK_INT; break;
    case CAMP_VAR_TYPE_FLOAT: tokKind = TOK_FLOAT; break;
    case CAMP_VAR_TYPE_STRING: tokKind = TOK_STRING; break;
    default: return;
    }

    print( "%s %s %d %d ", toktostr( TOK_ARRAYDEF ), toktostr( tokKind ),
                           pArr->elemSize, pArr->dim );

    for( i = 0; i < pArr->dim; i++ )
    {
        print( "%d ", pArr->dimSize[i] );
    }

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        /* Not done */
    }
    print( "\n" );
}


void 
defWrite_Structure( CAMP_VAR* pStcVar )
{
    CAMP_VAR* pVar;

    print( "\n" );

    add_tab();

    for( pVar = pStcVar->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        camp_pathDown( curr_path, LEN_PATH+1, pVar->core.ident );
        defWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();
}


void 
defWrite_Facility( CAMP_VAR* pVar_ins )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns;

    print( "%s %s ", toktostr( TOK_INSTYPE ), pIns->typeIdent );

    if( ( pIns->initProc != NULL ) && ( pIns->initProc[0] != '\0' ) )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_INITPROC ), pIns->initProc );
    }

    if( ( pIns->deleteProc != NULL ) && ( pIns->deleteProc[0] != '\0' ) )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_DELETEPROC ), 
                pIns->deleteProc );
    }

    if( ( pIns->onlineProc != NULL ) && ( pIns->onlineProc[0] != '\0' ) )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_ONLINEPROC ), 
                pIns->onlineProc );
    }

    if( ( pIns->offlineProc != NULL ) && ( pIns->offlineProc[0] != '\0' ) )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_OFFLINEPROC ), 
                pIns->offlineProc );
    }
    print( "\n" );

    add_tab();

    for( pVar = pVar_ins->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        camp_pathDown( curr_path, LEN_PATH+1, pVar->core.ident );
        defWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();
}


void 
defWrite_Link( CAMP_VAR* pVar )
{
    CAMP_LINK* pLnk = pVar->spec.CAMP_VAR_SPEC_u.pLnk;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s %s ", toktostr( TOK_VALUE_FLAG ), pLnk->path );
    }
    print( "\n" );
}


