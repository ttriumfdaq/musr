/*
 *  Name:       camp_filenames.c
 *
 *  Purpose:    
 *
 *  Called by:  
 *
 *  Revision history:
 *    20140325  TW  creation
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */

/*
 *  Directories/Filenames
 *
 *  Define a link or nfs mount point at /camp
 *  For VMS, this is a device called CAMP:
 *  Define also another mount point at /campnode  to be /camp/<node>
 */

struct CAMP_FILEMASK
{
    CAMP_FILEMASK_TYPE type;
    char* mask;
};
typedef struct CAMP_FILEMASK CAMP_FILEMASK;

//static CAMP_FILEMASK* camp_filemasks = NULL;
static CAMP_FILEMASK* camp_filemasks_translated = NULL;
static char* camp_dir = NULL;
static char* camp_node_dir = NULL;

#define  CAMP_INS_INI_PFMT_BASE	"camp_ins_%s.ini"
#define  CAMP_INS_INI_SFMT_BASE	"camp_ins_%[^.]"
#define  CAMP_CFG_PFMT_BASE	"camp_%s.cfg"
#define  CAMP_CFG_SFMT_BASE	"camp_%[^.]"
#define  CAMP_TCL_PFMT_BASE	"camp_ins_%s.tcl"
#define  CAMP_TCL_SFMT_BASE	"camp_ins_%[^.]"

static const char* camp_filemasks[CAMP_FILEMASK_TYPE_SIZE] = {
    "/camp/"                                 // CAMP_DIR
    ,"/camp/dat/"                            // CAMP_DAT_DIR used in camp_sys_priv as a default
    ,"/camp/log/"                            // CAMP_LOG_DIR not used
    ,"/campnode/log/camp_srv.log"            // CAMP_SRV_LOG_FILE write
    ,"/campnode/log/camp_cui.log"            // CAMP_CUI_LOG_FILE write
    ,"/campnode/cfg/camp.cfg"                // CAMP_SRV_AUTO_SAVE write
    ,"/camp/dat/camp.ini"                    // CAMP_SRV_INI read
    ,"/campnode/cfg/camp_ins_*.ini"          // CAMP_INS_INI_GLOB_PATTERN write
    ,CAMP_INS_INI_PFMT_BASE                  // CAMP_INS_INI_PFMT_BASE
    ,"/campnode/cfg/"CAMP_INS_INI_PFMT_BASE  // CAMP_INS_INI_PFMT write
    ,CAMP_INS_INI_SFMT_BASE                  // CAMP_INS_INI_SFMT_BASE
    ,"/camp/dat/"CAMP_INS_INI_SFMT_BASE      // CAMP_INS_INI_SFMT not used ?
    ,"/campnode/cfg/camp_*.cfg"              // CAMP_CFG_GLOB_PATTERN format for finding config
    ,CAMP_CFG_PFMT_BASE                      // CAMP_CFG_PFMT_BASE
    ,"/campnode/cfg/"CAMP_CFG_PFMT_BASE      // CAMP_CFG_PFMT format for writing config
    ,CAMP_CFG_SFMT_BASE                      // CAMP_CFG_SFMT_BASE
    ,"/camp/dat/"CAMP_CFG_SFMT_BASE          // CAMP_CFG_SFMT not used ?
    ,"/camp/drv/camp_ins_*.tcl"              // CAMP_TCL_GLOB_PATTERN not used ?
    ,CAMP_TCL_PFMT_BASE                      // CAMP_TCL_PFMT_BASE
    ,"/camp/drv/"CAMP_TCL_PFMT_BASE          // CAMP_TCL_PFMT
    ,CAMP_TCL_SFMT_BASE                      // CAMP_TCL_SFMT_BASE
    ,"/camp/drv/"CAMP_TCL_SFMT_BASE          // CAMP_TCL_SFMT not used ?
    ,"/campnode/"                            // CAMP_NODE_DIR
};

static bool_t
ends_in_slash( const char* str )
{
    size_t len = strlen( str );

    return ( len > 0 ) && ( str[len-1] == '/' );
}

void
camp_translate_generic_filename( const char* mask_in, char* mask_out, int mask_out_size )
{
    char* camp_dir_gen = "/camp/";
    char* camp_node_dir_gen = "/campnode/";
    char* p_camp_node_dir_gen;

    if( camp_dir == NULL )
    {
#ifdef VXWORKS
	/*
	 *  on the vxworks nodes, these are set to nfs mount points
	 */
	camp_dir = "/camp/";
	camp_node_dir = "/campnode/";
#else
	camp_dir = getenv( "CAMP_DIR" ); if( camp_dir == NULL ) camp_dir = "";
	camp_node_dir = getenv( "CAMP_NODE_DIR" ); if( camp_node_dir == NULL ) camp_node_dir = "";
#endif
    }

    p_camp_node_dir_gen = strstr( mask_in, camp_node_dir_gen );

    if( p_camp_node_dir_gen == mask_in ) // only if at the beginning of mask_in
    {
	//  substitute '/campnode/' with camp_node_dir in mask_in

	camp_strncpy( mask_out, mask_out_size, camp_node_dir );
	if( !ends_in_slash( camp_node_dir ) )
	{
	    camp_strncat( mask_out, mask_out_size, "/" );
	}
	camp_strncat( mask_out, mask_out_size, mask_in+strlen( camp_node_dir_gen ) );
    }
    else
    {
	char* p_camp_dir_gen = strstr( mask_in, camp_dir_gen );

	if( p_camp_dir_gen == mask_in ) // only if at the beginning of mask_in
	{
	    //  substitute '/camp/' with camp_dir in mask_in

	    camp_strncpy( mask_out, mask_out_size, camp_dir );
	    if( !ends_in_slash( camp_dir ) )
	    {
		camp_strncat( mask_out, mask_out_size, "/" );
	    }
	    camp_strncat( mask_out, mask_out_size, mask_in+strlen( camp_dir_gen ) );
	}
	else
	{
	    // mask_out = mask_in

	    camp_strncpy( mask_out, mask_out_size, mask_in );
	}
    }
}

static void
camp_init_filemask( CAMP_FILEMASK_TYPE type, const char* mask, CAMP_FILEMASK* pfilemask, int translate )
{
    /*
     *  don't translate generic name here:
     *    - filenames opened by the server should be managed by the server
     *      (client doesn't need to know the server's environment variables)
     *    - only CAMP_CUI_LOG_FILE is opened by the client, which can be done before fopen()
     */

    if( translate )
    {
	char buf[LEN_BUF+1];
	camp_translate_generic_filename( mask, buf, sizeof( buf ) );
	pfilemask->mask = strdup( buf );
    }
    else
    {
	pfilemask->mask = strdup( mask );
    }

    pfilemask->type = type;
}

void
camp_init_filemasks()
{
    CAMP_FILEMASK_TYPE type;

    camp_filemasks_translated = (CAMP_FILEMASK*)camp_zalloc( CAMP_FILEMASK_TYPE_SIZE*sizeof( CAMP_FILEMASK ) );

    for( type = 0; type < CAMP_FILEMASK_TYPE_SIZE; ++type )
    {
	camp_init_filemask( type, camp_filemasks[type], &camp_filemasks_translated[type], 1 );

	// _camp_log( "type:%d mask:'%s' -> '%s'", type, camp_filemasks[type], camp_filemasks_translated[type].mask );
    }
}

void
camp_shutdown_filemasks()
{
    CAMP_FILEMASK_TYPE type;
    
    if( camp_filemasks_translated != NULL )
    {
	for( type = 0; type < CAMP_FILEMASK_TYPE_SIZE; ++type )
	{
	    _free( camp_filemasks_translated[type].mask );
	}
    }
}

const char* 
camp_getFilemask( CAMP_FILEMASK_TYPE type, bool_t translate )
{
    /*
     *  this could init on demand, but we don't because we're 
     *  dealing with multithreaded applications, so we want
     *  the application to do this before spawning threads
     */

    if( camp_filemasks_translated == NULL )
    {
	fprintf( stderr, "%s: called before camp_init_filemasks\n", __FUNCTION__ );
	exit( CAMP_FAILURE );
	return "";
    }

    if( type < 0 || type >= CAMP_FILEMASK_TYPE_SIZE )
    {
	fprintf( stderr, "%s: invalid CAMP_FILEMASK_TYPE %d\n", __FUNCTION__, type );
	exit( CAMP_FAILURE );
	return "";
    }

    return translate ? camp_filemasks_translated[type].mask : camp_filemasks[type];
}

const char* camp_getFilemaskGeneric( CAMP_FILEMASK_TYPE type )
{
    return camp_getFilemask( type, FALSE );
}

const char* camp_getFilemaskSpecific( CAMP_FILEMASK_TYPE type )
{
    return camp_getFilemask( type, TRUE );
}
