/********************************** gpibLib.h *************************/
/*
 *  Modification history:
 *
 *  16-Feb-1996  TW  gpibStatus is global.
 *
 */

typedef struct tag_GPIB_PHYS_DEV
{
  int iPriBusId;

    // index 0=read 1=write
    unsigned long transfers[2]; // 
    unsigned long transfer_bytes[2]; // bytes
    unsigned long transfer_checks_tot[2]; // tot spinning 
    unsigned long transfer_checks_max[2]; // max spinning per transfer
    unsigned long transfer_checks_byte_max[2]; // max spinning per byte
    unsigned long transfer_delays_tot[2]; // tot delays 
    unsigned long transfer_delays_max[2]; // max delays per transfer
    unsigned long transfer_delays_byte_max[2]; // max delays per byte
    float         transfer_time_avg[2]; // in ticks
    unsigned long transfer_time_max[2]; // in ticks
    float         transfer_time_byte_avg[2]; // in ticks
    unsigned long transfer_time_byte_max[2]; // in ticks

    long time_last_dump[2]; // in ticks
} GPIB_PHYS_DEV;

#ifdef GPIBLIB_H

  #define IP300_BAD_ADDRESS   (1)
  #define IPIC_ADDRESS        (0xfffbc000)     /* IPIC chip base address */
  #define IP_BASE_ADRS        (0xfff58000)     /* Industry Packs base address */
  #define IP_MEM_ADDR         (0xc0000000)     /* IP 8M memory address block */

  SEM_ID semGPIB;

  GPIB_PHYS_DEV * gpibDevices[32];
  int gpibError;
  int gpibStatus;
  char gpibAddress;  /* Set with switches on MVIP300 */

unsigned long mvip300_transfers = 0; // total bytes
unsigned long mvip300_transfer_checks_tot = 0; // total checks
unsigned long mvip300_transfer_checks_byte_max = 0; // max check per byte
unsigned long mvip300_transfer_delays_tot = 0; // total delays
unsigned long mvip300_transfer_delays_byte_max = 0; // max check per byte
unsigned long mvip300_transfer_time = 0; // in ticks
unsigned long mvip300_transfer_time_byte_max = 0; // in ticks

#else

  extern GPIB_PHYS_DEV * gpibDevices[];
  extern int gpibError;
  extern int gpibStatus;

extern unsigned long mvip300_transfers; // total bytes
extern unsigned long mvip300_transfer_checks_tot; // total checks
extern unsigned long mvip300_transfer_checks_byte_max; // max checks per byte
extern unsigned long mvip300_transfer_delays_tot; // total delays
extern unsigned long mvip300_transfer_delays_byte_max; // max delays per byte
extern unsigned long mvip300_transfer_time; // in ticks
extern unsigned long mvip300_transfer_time_byte_max; // in ticks

#endif

STATUS gpibInit(char cPosition, int iTimeout);
GPIB_PHYS_DEV * gpibPhysDevCreate(int iPriBusId);
STATUS gpibPhysDevDelete(GPIB_PHYS_DEV * gpibItem);
STATUS gpibShow(void);
STATUS gpibTrigger(GPIB_PHYS_DEV * gpibItem);
STATUS gpibRead(GPIB_PHYS_DEV * gpibItem, char * pszString, int iLength);
STATUS gpibWrite(GPIB_PHYS_DEV * gpibItem, char * pszString);
STATUS gpibIoctl(GPIB_PHYS_DEV * gpibItem, int function, long arg);
char gpibReadSerialPoll(GPIB_PHYS_DEV * gpibItem);
