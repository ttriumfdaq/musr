/*
 *  Name:       camp_if_gpib_mscb.c
 *
 *  Purpose:    Provides a GPIB interface type.
 *
 *              A CAMP GPIB interface definition must provide the following
 *              routines:
 *                int if_gpib_init ( void );
 *                int if_gpib_online ( CAMP_IF *pIF );
 *                int if_gpib_offline ( CAMP_IF *pIF );
 *                int if_gpib_read( REQ* pReq );
 *                int if_gpib_write( REQ* pReq );
 *              and optionally:
 *                int if_gpib_clear ( int gpib_address );
 *                int if_gpib_timeout ( float timeoutSeconds );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *  Notes:
 *
 *      pIF_t->priv == <pointer to local MSCB_FD structure> (gpib available)
 *                  == 0 (gpib unavailable)
 *
 *  Revision history:
 *
 *    20140403     TW  initial
 *    20140410     TW  use CTL variable as a flag for expected reply
 *
 */

#define USE_CTL_FLAG
#define CTL_FLAG_DATA 3 // don't conflict with other uses of CTL in gpib410.c (firmware: 20140414,20140425)
// #define CTL_FLAG_DATA 1 // (firmware: 20140410)

//#define DELAY_READ_AFTER_WRITE pIF->accessDelay
#define DELAY_READ_AFTER_WRITE 0.015

#include <stdio.h>
#include "camp_srv.h"
#include "mscb.h"

#define if_gpib_ok( pIF_t ) ( pIF_t->priv != 0 ) 

#define GPIB410_VAR_OUTPUT   1
#define GPIB410_VAR_INPUT    2
#define GPIB410_VAR_CTL      5
#define GPIB410_VAR_GPIB_ADR 6


/*
 *  gpib_mscb interface type configuration string:
 *
 *     <mscb_node_name> <password> <debug> <retries> <eth_retries>
 *
 *     e.g., mscb506.triumf.ca "" 1 10 10
 *
 *     retries: high-level mscb retries
 *     eth_retries: low-level mscb retries
 */
int
if_gpib_mscb_init( void )
{
    int camp_status = CAMP_SUCCESS;
    int mscb_fd = -1;
    char mscb_device[64];
    char mscb_password[64];
    int mscb_debug = 0;
    int mscb_retries = 10;
    int mscb_eth_retries = 10;

    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
    {
	mscb_debug = 1;
    }

    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( "gpib_mscb" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib_mscb" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	pIF_t->priv = 0; // invalidate by default

	/*
	 *  Check configuration data 
	 */

	int sscanf_count = sscanf( pIF_t->conf, " %s %s %d %d", mscb_device, mscb_password, &mscb_retries, &mscb_eth_retries );

	if( sscanf_count < 4 )
	{
	    _camp_setMsg( "gpib_mscb configuration string invalid '%s' (usage: <node> <password> <retries> <eth_retries>)", pIF_t->conf );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	mscb_set_max_retry( mscb_retries );

	/*
	 *  init connection to mscb submaster
	 */

	mscb_fd = mscb_init( mscb_device, strlen( mscb_device ), mscb_password, mscb_debug );
	if( mscb_fd < 0 ) 
	{
	    _camp_setMsg( "cannot access MSCB submaster '%s'", mscb_device );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	mscb_set_eth_max_retry( mscb_fd, mscb_eth_retries );

	/*
	 *  allocate a local MSCB_FD structure to store information about the connection
	 *
	 *  (the mscb library maintains a private list of these structures for each connection,
	 *  but note that this is not a pointer into that list - we don't have access to those
	 *  - it's just a local structure)
	 */

	MSCB_FD* mscb_fd_local = (MSCB_FD*)camp_zalloc( sizeof( MSCB_FD ) );

	camp_strncpy( mscb_fd_local->device, sizeof( mscb_fd_local->device ), mscb_device );
	mscb_fd_local->fd = mscb_fd;

	/*
	 *  save file descriptor 
	 */

	pIF_t->priv = mscb_fd_local;

	_camp_log( "initialized interface type 'gpib_mscb' (device:'%s' password:'%s' debug:%d retries:%d eth_retries:%d fd:%d)", 
		   mscb_device, mscb_password, mscb_debug, mscb_retries, mscb_eth_retries, mscb_fd );
    }

return_:
    return( camp_status );
}


int
if_gpib_mscb_shutdown( void )
{
    int camp_status = CAMP_SUCCESS;
    int mscb_fd = -1;

    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( "gpib_mscb" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib_mscb" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	{
	    MSCB_FD* mscb_fd_local = (MSCB_FD*)pIF_t->priv;

	    mscb_fd = mscb_fd_local->fd;

	    if( mscb_fd > 0 )
	    {
		_camp_log( "shutdown interface type 'gpib_mscb' (device:'%s' fd:%d)", mscb_fd_local->device, mscb_fd );

		mscb_exit( mscb_fd );
	    }

	    _free( mscb_fd_local );
	}
    }

return_:
    return( camp_status );
}


int
if_gpib_mscb_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    int mscb_status = MSCB_SUCCESS;
    unsigned char gpib_address;
    int mscb_fd;
    unsigned short mscb_gpib_base_address;
    char mscb_device[256];
    unsigned char mscb_variable_index;
    /*
     *  The RS485 standard says maximum 32 "unit loads" on the bus.
     *  Vendors are making devices with 1/8th of a unit load, for a
     *  maximum of 256 devices on the bus. 
     *  todo: do mscb addresses start at zero or one?
     *  todo: does the mscb implementation of rs485 have a particular maximum?
     */
    unsigned short mscb_base_address_maximum = 256;
    // MSCB_INFO node_info;

    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	{
	    MSCB_FD* mscb_fd_local = (MSCB_FD*)pIF_t->priv;
	    mscb_fd = mscb_fd_local->fd;
	    camp_strncpy( mscb_device, sizeof( mscb_device ), mscb_fd_local->device );
	}

	gpib_address = camp_getIfGpibAddr( pIF->defn );
	if( gpib_address > 31 )
	{
	    _camp_setMsg( "invalid gpib address %d", gpib_address );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	mscb_gpib_base_address = camp_getIfGpibMscbAddr( pIF->defn );
	if( mscb_gpib_base_address > mscb_base_address_maximum )
	{
	    _camp_setMsg( "invalid mscb address %d", mscb_gpib_base_address );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	/*
	 *  turn online here
	 */
    
#ifdef CONSIDER_TRYING_THIS
	/*
	 *  check if devices is alive 
	 */
	mscb_status = mscb_ping( mscb_fd, mscb_gpib_base_address, 1 );
	if( mscb_status != MSCB_SUCCESS ) 
	{
	    _camp_setMsg( "cannot ping MSCB GPIB node, check power and connection (mscb dev:'%s' addr:%d)", mscb_device, mscb_gpib_base_address );
	    camp_status = CAMP_FAILURE; goto return_;
	}
#endif
    
        /*
	 *  note: to do this diagnostic test we would need the node_name of the
	 *  mscb module (e.g., GPIB410).  requiring this item in the interface
	 *  definition, when it's only for diagnostic, seemed excessive
	 *
	 *  the integral address on the mscb bus is the only thing actually
	 *  needed to find the module on the bus.
	 *
	 *  TODO: ask Pierre if every gpib410 module has node_name "GPIB410"
	 *  then we could do this check here with a hardcoded node_name.
	 *  I've also seen "MSCB_410_GPIB" in the firmware file.
	 */
#ifdef WE_DONT_HAVE_THE_NODE_NAME 
	/*
	 *  Check for right device
	 */
	mscb_status = mscb_info( mscb_fd, mscb_gpib_base_address, &node_info );
	if( strcmp( node_info.node_name, mscb_gpib_node_name ) != 0 ) 
	{
	    _camp_setMsg( "did not find expected MSCB GPIB node '%s' (mscb dev:'%s' addr:%d)"
		     , node_info.node_name, mscb_device, mscb_gpib_base_address );
	    camp_status = CAMP_FAILURE; goto return_;
	}
	// printf( "mscb_test: found expected GPIB node '%s' (mscb dev:'%s' addr:%d)\n"
	//	   , node_info.node_name, mscb_device, mscb_gpib_base_address );
#endif

	/*
	 *  set the gpib address
	 */

	mscb_variable_index = GPIB410_VAR_GPIB_ADR; // write to the module's "GPIB Adr" variable
	mscb_status = mscb_write( mscb_fd, mscb_gpib_base_address, mscb_variable_index, &gpib_address, sizeof( gpib_address ) );
	if( mscb_status != MSCB_SUCCESS ) 
	{
	    _camp_setMsg( "failed to set gpib address of MSCB GPIB node (mscb dev:'%s' addr:%d var:%d gpib_addr:%d status:%d)", 
			    mscb_device, mscb_gpib_base_address, mscb_variable_index, gpib_address, mscb_status );
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }

    camp_status = CAMP_SUCCESS;

return_:
    return( camp_status );
}


int
if_gpib_mscb_offline( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;

    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	/*
	 *  Turn offline here
	 *
	 *  nothing to do
	 */
    }

return_:
    return( camp_status );
}


/*
 *  if_gpib_mscb_read,  write command, read response
 */
int 
if_gpib_mscb_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd;
    int cmd_len;
    char* buf;
    int buf_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    timeval_t	timeLastAccess;
    timeval_t	timeWrite;
    int mscb_fd;
    char mscb_device[256];
    unsigned short mscb_gpib_base_address;
    unsigned char mscb_variable_index;
    int mscb_status;
    float timeout;
    int* pRead_len;
    unsigned char gpib410_ctl;
    int read_attempts;

    {
	CAMP_IF_t* pIF_t;
	CAMP_IF* pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked by ins mutex

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	{
	    MSCB_FD* mscb_fd_local = (MSCB_FD*)pIF_t->priv;
	    mscb_fd = mscb_fd_local->fd;
	    camp_strncpy( mscb_device, sizeof( mscb_device ), mscb_fd_local->device );
	}

	mscb_gpib_base_address = camp_getIfGpibMscbAddr( pIF->defn );

	cmd = pReq->spec.REQ_SPEC_u.read.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
	buf = pReq->spec.REQ_SPEC_u.read.buf;
	buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;
	pRead_len = &pReq->spec.REQ_SPEC_u.read.read_len;
	*pRead_len = 0;

	camp_if_termTokenToChars( camp_getIfGpibWriteTerm( pIF->defn, str, sizeof( str ) ), term, sizeof( term ), &term_len );

	timeout = pIF->timeout;

	/*
	 *  Write 
	 */
	{
	    char cmd_term[cmd_len + term_len + 1];
	    int cmd_term_size = cmd_len + term_len + 1;

	    camp_strncpy_num( cmd_term, cmd_term_size, cmd, cmd_len );
	    camp_strncpy_num( &cmd_term[cmd_len], cmd_term_size-cmd_len, term, term_len );

	    if( cmd_term_size > 64 ) // yes, null terminator is included 
	    {
		_camp_setMsg( "gpib_mscb command '%s' is more than 64 bytes", cmd );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    {
		int globalMutexLockCount = mutex_unlock_global_all(); mutex_lock_if_t( pIF_t, 1 );

#ifdef USE_CTL_FLAG
		/*
		 *  20140410  TW  use CTL as a flag for expected reply
		 */
		mscb_variable_index = GPIB410_VAR_CTL;
		gpib410_ctl = CTL_FLAG_DATA;
		mscb_status = mscb_write( mscb_fd, mscb_gpib_base_address, mscb_variable_index, &gpib410_ctl, sizeof( gpib410_ctl ) );

		if( mscb_status == MSCB_SUCCESS ) 
#endif // USE_CTL_FLAG
		{
		    mscb_variable_index = GPIB410_VAR_OUTPUT;
		    mscb_status = mscb_write( mscb_fd, mscb_gpib_base_address, mscb_variable_index, cmd_term, cmd_term_size ); // note: must send null terminator
		    gettimeval( &timeLastAccess );
		    timeWrite = timeLastAccess;
		}

		mutex_lock_if_t( pIF_t, 0 ); mutex_lock_global_all( globalMutexLockCount );
	    }

	    if( mscb_status != MSCB_SUCCESS ) 
	    {
		_camp_setMsg( "failed mscb_write (mscb dev:'%s' addr:%d var:%d status:%d cmd:'%s')", mscb_device, mscb_gpib_base_address, mscb_variable_index, mscb_status, cmd );
		camp_status = CAMP_FAILURE; goto return_;
	    }
	}

	camp_if_termTokenToChars( camp_getIfGpibReadTerm( pIF->defn, str, sizeof( str ) ), term, sizeof( term ), &term_len );

	for( read_attempts = 0;; ++read_attempts )
	{
	    // add an accessDelay between write and consecutive reads
	    {
		double delay, diff;
		timeval_t timeCurrent;
		CAMP_IF* pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked

		gettimeval( &timeCurrent );

		diff = difftimeval( &timeCurrent, &timeLastAccess ); // timeCurrent - timeLastAccess

		delay = DELAY_READ_AFTER_WRITE - diff;

		if( delay > 0.0 )
		{
		    int globalMutexLockCount = mutex_unlock_global_all();
		    camp_fsleep( delay );
		    mutex_lock_global_all( globalMutexLockCount );
		}
	    }

	    /*
	     *  Read
	     */
	    {
		int read_size;
		//  we need at least 64 bytes for the mscb gpib module's 64 byte string
		//  but not more than 256 bytes (imposed by mscb_read)
		char read_buf[256];

		//  read_size passes in the read buffer size, and returns bytes read
		//  but note that the return value is not useful (it's always 64, 
		//  not the number of bytes the instrument sent)

		read_size = sizeof( read_buf ); // buf_len

		{
		    int globalMutexLockCount = mutex_unlock_global_all(); mutex_lock_if_t( pIF_t, 1 );
		    
		    {
			mscb_variable_index = GPIB410_VAR_INPUT;
			mscb_status = mscb_read( mscb_fd, mscb_gpib_base_address, mscb_variable_index, read_buf, &read_size );
			gettimeval( &timeLastAccess );
		    }

		    mutex_lock_if_t( pIF_t, 0 ); mutex_lock_global_all( globalMutexLockCount );
		}

		if( mscb_status != MSCB_SUCCESS ) 
		{
		    _camp_setMsg( "failed mscb_read, got '%s' (mscb dev:'%s' addr:%d var:%d status:%d cmd:'%s')", read_buf, mscb_device, mscb_gpib_base_address, mscb_variable_index, mscb_status, cmd );
		    camp_status = CAMP_FAILURE; goto return_;
		}

		// todo: check if strlen(read_buf)+1 > buf_len

		camp_strncpy( buf, buf_len, read_buf );
	    }

	    /*
	     *  check for terminator
	     */

	    if( term_len > 0 )
	    {
		char* pterm = strstr( buf, term );
		if( pterm != NULL ) 
		{
		    *pterm = '\0';
		    *pRead_len = (int)(pterm - buf);
		    break; // read complete
		}
	    }
	    else
	    { 
		//  no terminator means no way to check if finished
		//  we have to count on the accessDelay being long enough
		// 
		//  note: returned read_size is not the number of bytes sent from instrument
                //  but mscb_read terminates string reliably
		*pRead_len = strlen( buf );
		break;
	    }
	    
	    /*
	     *  local software timeout
	     *
	     *  (keep reading the mscb gpib until we detect the terminator, or timeout has passed)
	     */
	    {
		double diff;
		timeval_t timeCurrent;

		gettimeval( &timeCurrent );

		diff = difftimeval( &timeCurrent, &timeWrite ); // timeCurrent - timeWrite

		if( diff > timeout )
		{
		    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
		    {
			_camp_log( "mscb_read timeout, read:'%s' attempts:%d totaltime:%.1fs (mscb dev:'%s' addr:%d cmd:'%s')", buf, read_attempts, diff, mscb_device, mscb_gpib_base_address, cmd );
		    }
		    _camp_setMsg( "mscb_read timeout, got '%s' (mscb dev:'%s' addr:%d cmd:'%s')", buf, mscb_device, mscb_gpib_base_address, cmd );
		    camp_status = CAMP_FAILURE; goto return_;
		}
	    }
	}
    }

return_:
    return( camp_status );
}


/*
 *  if_gpib_mscb_write,  
 */
int 
if_gpib_mscb_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    int mscb_fd;
    char mscb_device[256];
    unsigned short mscb_gpib_base_address;
    unsigned char mscb_variable_index;
    int mscb_status;
    unsigned char gpib410_ctl;

    {
	CAMP_IF_t* pIF_t;
	CAMP_IF* pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) ) 
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	{
	    MSCB_FD* mscb_fd_local = (MSCB_FD*)pIF_t->priv;
	    mscb_fd = mscb_fd_local->fd;
	    camp_strncpy( mscb_device, sizeof( mscb_device ), mscb_fd_local->device );
	}

	mscb_gpib_base_address = camp_getIfGpibMscbAddr( pIF->defn );

	cmd = pReq->spec.REQ_SPEC_u.write.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

	camp_if_termTokenToChars( camp_getIfGpibWriteTerm( pIF->defn, str, sizeof( str ) ), term, sizeof( term ), &term_len );

	/*
	 *  Write 
	 */
	{
	    char cmd_term[cmd_len + term_len + 1]; // cmd with terminator
	    int cmd_term_size = cmd_len + term_len + 1;

	    camp_strncpy_num( cmd_term, cmd_term_size, cmd, cmd_len );
	    camp_strncpy_num( &cmd_term[cmd_len], cmd_term_size-cmd_len, term, term_len );

	    if( cmd_term_size > 64 ) // null terminator should be included in the size
	    {
		_camp_setMsg( "gpib_mscb command '%s' is more than 64 bytes", cmd );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    {
		int globalMutexLockCount = mutex_unlock_global_all(); mutex_lock_if_t( pIF_t, 1 );

#ifdef USE_CTL_FLAG
		/*
		 *  20140410  TW  use CTL as a flag for expected reply
		 */
		mscb_variable_index = GPIB410_VAR_CTL;
		gpib410_ctl = 0;
		mscb_status = mscb_write( mscb_fd, mscb_gpib_base_address, mscb_variable_index, &gpib410_ctl, sizeof( gpib410_ctl ) );

		if( mscb_status == MSCB_SUCCESS ) 
#endif // USE_CTL_FLAG
		{
		    mscb_variable_index = GPIB410_VAR_OUTPUT;
		    mscb_status = mscb_write( mscb_fd, mscb_gpib_base_address, mscb_variable_index, cmd_term, cmd_term_size ); // note: must send null terminator
		}

		mutex_lock_if_t( pIF_t, 0 ); mutex_lock_global_all( globalMutexLockCount );
	    }

	    if( mscb_status != MSCB_SUCCESS ) 
	    {
		_camp_setMsg( "failed mscb_write (mscb dev:'%s' addr:%d var:%d status:%d cmd:'%s')", mscb_device, mscb_gpib_base_address, mscb_variable_index, mscb_status, cmd );
		camp_status = CAMP_FAILURE; goto return_;
	    }
	}
    }

return_:
    return( camp_status );
}


/*
 *  if_gpib_mscb_clear
 *
 *  Sets up GPIB bus and sends an SDC (selected device clear)
 *  Does not use any CAMP interface definition, just sends the
 *  command straight to the GPIB bus.  
 *
 *  21-Dec-1999  TW  Implemented for VxWorks MVIP300.  Found
 *                   to increase reliability of LakeShore GPIB
 *                   devices.
 *  20140403     TW  no apparent gpib clear using mscb gpib
 */
int 
if_gpib_mscb_clear( int gpib_addr )
{
    int camp_status = CAMP_SUCCESS;

    /*
     *  implement when available
     */

    return( camp_status );
}


/*
 *  if_gpib_mscb_timeout
 *
 *  Set timeout of GPIB interface bus
 *
 *  13-Dec-2000  TW  Implemented for VxWorks MVIP300 only.
 *  20140403     TW  changed argument from int to float
 *  20140403     TW  no apparent control of gpib timeout using mscb gpib
 */
int 
if_gpib_mscb_timeout( float timeoutSeconds )
{
    int camp_status = CAMP_SUCCESS;

    /*
     *  implement when available
     */

    return( camp_status );
}
