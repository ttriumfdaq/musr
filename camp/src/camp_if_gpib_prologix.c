/*
 *  Name:       camp_if_gpib_prologix.c
 *
 *  Purpose:    Provides an IEEE-488 aka GPIB communications interface type
 *              using the Prologix GPIB-USB controller.
 *
 *              The following routines are provided:
 *                int if_gpib_init ( void );
 *                int if_gpib_online ( CAMP_IF *pIF );
 *                int if_gpib_offline ( CAMP_IF *pIF );
 *                int if_gpib_read( REQ* pReq );
 *                int if_gpib_write( REQ* pReq );
 *              and optionally:
 *                int if_gpib_clear ( int gpib_address );
 *                int if_gpib_timeout ( float timeoutSeconds );
 *
 *              Communication with the prologix closely resembles serial
 *              port communication, sending and receiving from a virtual
 *              tty port, so this interface resembles rs232_tty. Three big
 *              differences are: The virtual tty is shared so there is a 
 *              mutex to limit access; The virtual tty has no settings for
 *              baud, parity, and such; The prologix has GPIB control
 *              commands embedded in the text stream flagged by "+++".
 *              See PrologixGpibUsbManual-6.0.pdf
 *
 *              In this first iteration, each access to a particular gpib 
 *              instrument holds control of the entire gpib bus until finished.
 *              In the future, we can try to multiplex writes and reads
 *              going to different gpib instruments.
 * 
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode. It will be unlocked
 *              during write(), select(), and read() calls.
 *
 *  Notes:
 *
 *     Ini file configuration string:
 *        {<port>}
 *     which which will be stored as  pIF_t->conf
 *
 *     Meaning of private (driver-specific) variables:
 *        pIF_t->priv - not used OR virtual tty file descriptor
 *        pIF->priv   - virtual tty file descriptor OR not used
 *
 *  Revision history:
 *
 *    2014-03-11   DJA   Initial version
 */

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/select.h>
#ifdef linux
#include <unistd.h> // read,write,close
#endif // linux

#include "camp_srv.h" /* for thread-related defs and prototypes beyond camp.h */

#define give_global_take_gpib() int globalMutexLockCount = mutex_unlock_global_all(); mutex_lock_if_t( pIF_t, 1 ); 
#define give_gpib_take_global() mutex_lock_if_t( pIF_t, 0 ); mutex_lock_global_all( globalMutexLockCount );

#define if_gpib_ok( pIF_t )     ( pointerToInteger( pIF_t->priv ) == 1 )
#define _close( fd )            if( fd != -1 ) { close( fd ); fd = -1; }

/* 
 * escape_termin_strlen: 
 * count length that an escaped string will have after appending terminator 
 * characters and escaping special characters.
 */
static int escape_termin_strlen( char* src, char* termin )
{
    const char LF=10;
    const char CR=13;
    const char ESC=27;

    int n = 2; /* count bookend-LFs but not zero termination */
    char* s = src;

    while ( *s != '\0' ) 
    {
	n += ( ( *s == CR || *s == LF || *s == ESC || *s == '+' ) ? 2 : 1 );
	s++;
    }

    s = termin;
    while ( *s != '\0' ) 
    {
	n += ( ( *s == CR || *s == LF || *s == ESC || *s == '+' ) ? 2 : 1 );
	s++;
    }
    return ( n );
}

/* 
 * escape_termin_str: 
 * copy src string to dest buffer, escaping characters as needed, and appending
 * the (escaped) terminator characters. Put an unescaped LF at beginning and end
 * as delimiters for the prologix; the one at the beginning is in case there are
 * junk characters pending on the prologix already.
 * Return the length of dest string (max size dlen), or -1 for failure.
 */
static int escape_termin_str( char* dest, int dlen, char* src, char* termin )
{
    const char LF=10;
    const char CR=13;
    const char ESC=27;

    int fi = dlen - 3; /* "final index" */
    int i = 0;
    char* s = src;

    dest[i++] = LF;
    while ( *s != '\0' ) 
    {
	if ( *s == CR || *s == LF || *s == ESC || *s == '+' ) 
	{
	    if ( i > fi-1 ) { goto toolong; }
	    dest[i++] = ESC;
	    dest[i++] = *s;
	}
	else
	{
	    if ( i > fi ) { goto toolong; }
	    dest[i++] = *s;
	}
	s++;
    }

    s = termin;
    while ( *s != '\0' ) 
    {
	if ( i > fi-1 ) { goto toolong; }
	dest[i++] = ESC;
	dest[i++] = *s;
	s++;
    }
 
    dest[i++] = LF;
    dest[i] = '\0';

    return ( i );

toolong:

    dest[i] = '\0';
    return ( -1 );

}

/*  
 *  The init routine (if_gpib_init) is called when the interface type is added 
 *  (sysAddIfType).
 *
 *  We want to check that the interface's port exists and corresponds
 *  to the prologix FTDI USB Serial Device converter.
 *
 *  Question:   Do we want to open the tty port here and and leave it open?
 *              If so, the file descriptor can be retained as  pIF_t->priv
 *  Question:   When we set up the prologix initially with 
 *              ++mode 1  (controller mode)
 *              ++auto 0  (no auto-read-after-write)
 *              ++eos 3   (do not append extra terminators)
 *              ++ifc     (take control of gpib bus)
 *              should we also read back ++ver?
 */

int if_gpib_init( void )
{
    int camp_status = CAMP_FAILURE;
    int fd = -1;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock // lock for pIF_t
    {
	CAMP_IF_t* pIF_t;
	char* port;
	const char inicmd[] = { "\n++mode 1\n++auto 0\n++eos 3\n++eoi 1\n++ifc\n" };

	pIF_t = camp_ifGetpIF_t( "gpib" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
  
	pIF_t->priv = integerToPointer( 0 ); 

	/*
	 *  Check FTDI port, perform setup and configuration here
	 *  (As of now, the global mutex is held, so we don't need a separate copy 
	 *  of the port string.)
	 */
	port = pIF_t->conf;
	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf( "Gpib: Try to open port %s\n", port );
	if( ( fd = open( port, (O_RDWR|O_NOCTTY)) ) < 0 )
	{
	    _camp_setMsg( "failure opening gpib FDTI port %s", port );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf( "Gpib: Initialize with commands:%s", inicmd );
	if( write( fd, inicmd, strlen(inicmd) ) < 0 )
	{
	    _camp_setMsg( "failure initializing gpib on %s", port );
	    camp_status = CAMP_FAILURE; goto return_;
	}
        /*
         *  ++ifc command takes 150 microseconds; wait twice as long
         */
        camp_fsleep(0.0003);

	pIF_t->priv = integerToPointer( 1 ); /* or fd */
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    camp_status = CAMP_SUCCESS;

return_:

    /* 
     * Implementation is using non-persistent fd; 
     * read/write operations will open and close port each time. 
     */
    _close( fd );

    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  Function when bringing a particular instrument online.
 *  Can't think of anything useful to put here. Just check address is in range 1-30
 */
int if_gpib_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    int addr;
    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: if_gpib_online begins\n");

    // _mutex_lock_begin();

    // sys_mutex unnecessary because it must already be taken
    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock // lock for pIF_t
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if ( pIF_t == NULL )
	{
            if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: ERROR invalid interface type\n");
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if ( !if_gpib_ok( pIF_t ) )
	{
            if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("gpib unavailable\n");
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    addr = camp_getIfGpibAddr( pIF->defn );
    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: requested address is %d\n", addr);
    if ( addr < 0 || addr > 30 )
    {
	_camp_setMsg( "invalid gpib address" );
	camp_status = CAMP_FAILURE; goto return_;
    }
    camp_status = CAMP_SUCCESS;

return_:
    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: if_gpib_online ends\n");
    // _mutex_lock_end();
    return( camp_status );
}


int if_gpib_offline( CAMP_IF* pIF )
{
    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_read,  synchronous write, then read with timeout
 */
int if_gpib_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_IF* pIF;
    int fd = -1;
    int nfds;
    char* cmd;
    int cmd_len;
    char term[8], rterm[8], wterm[8];
    int rterm_len, wterm_len;
    fd_set readfds;
    char* port = NULL;
    char* buf;
    int buf_len;
    char* bufBegin;
    char* eol;
    int* pRead_len;
    int nread;
    double timeout;
    timeval_t tvTimeout;
    timeval_t tvFlush = { 0, 0 }; /* zero wait for typeahead to clear */
    char prelimstr[32];
    int prelimlen;
    int addr;
    int to_ms;
    char* comstr = NULL;
    int comsize = 0;
    int comlen = 0;
    char junkstr[64];
    const int junksize = 64;
    int nj, ij;
#if  CAMP_DEBUG
    char dbuf[64];
    int ncexp;
#endif
    // _mutex_lock_begin();

    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: if_gpib_read begins\n");

    pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked by ins mutex
    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    bufBegin = buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;
    camp_if_termTokenToChars( camp_getIfGpibWriteTerm( pIF->defn, term, sizeof( term ) ), 
			      wterm, sizeof( wterm ), &wterm_len );
    camp_if_termTokenToChars( camp_getIfGpibReadTerm( pIF->defn, term, sizeof( term ) ), 
			      rterm, sizeof( rterm ), &rterm_len );
    pRead_len = &pReq->spec.REQ_SPEC_u.read.read_len;
    *pRead_len = 0;

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock // lock for pIF_t
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL )
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

        // use local copy of port string because we will release mutex 
	port = strdup( pIF_t->conf );

	addr = camp_getIfGpibAddr( pIF->defn );

        timeout = pIF->timeout;
        tvTimeout = double_to_timeval( timeout );
	to_ms = (int) (abs( 1000 * timeout) + 10 );
	if ( to_ms > 3000) to_ms = 3000;
	prelimlen = camp_snprintf( prelimstr, sizeof(prelimstr), "\n++addr %d\n++read_tmo_ms %d\n", addr, to_ms );

	if( cmd_len > 0 )
	{
	    comsize = escape_termin_strlen( cmd, wterm ) + 2;
	    comstr = (char*)camp_malloc( comsize );
	    comlen = escape_termin_str( comstr, comsize, cmd, wterm );
	    if ( comlen < 0 ) 
	    {
		_camp_setMsg( "Weird error escaping string" );
		camp_status = CAMP_FAILURE; goto return_;
	    }
	}

	// give_global_take_gpib
	{
	    give_global_take_gpib();

	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: open port %s\n", port);
	    if( ( fd = open( port, (O_RDWR|O_NOCTTY) ) ) < 0 )
	    {
		give_gpib_take_global();
		_camp_setMsg( "failure opening gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }
	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: opened port %s on fd %d\n", port, fd);

	    /*
	     *  Write the gpib commands (select address)
	     */

	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: write commands:%s", prelimstr);
	    if( write( fd, prelimstr, prelimlen ) < 0 )
	    {
		_close( fd );
		give_gpib_take_global();
		_camp_setMsg( "write failure on gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    /*
	     *  Write the (escaped) instrument query command (if any)
	     */
	    if( cmd_len > 0 )
	    {
		if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: write the query: %s\n", cmd);
		if( write( fd, comstr, comlen ) < 0 )
		{
		    _close( fd );
		    give_gpib_take_global();
		    _camp_setMsg( "write failure on gpib FDTI port %s", port );
		    camp_status = CAMP_FAILURE; goto return_;
		}
	    }

	    /*
	     * At this point we could perhaps close fd, release the gpib mutex and wait a bit.
	     * If we do, then we have to re-open the fd and re-select the address before reading.
	     */

	    camp_fsleep(0.01);

	    /*
	     * Flush buffer before requesting the actual read (maybe switch to tcflush)
	     */

	    FD_ZERO( &readfds );
	    FD_SET( fd, &readfds );	
	    cleartimeval( &tvFlush );
	    while ( select( fd+1, &readfds, NULL, NULL, &tvFlush ) > 0 )
	    {
		if( FD_ISSET( fd, &readfds ) )
		{
		    nj = read( fd, junkstr, junksize );
                    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
                    {
                        junkstr[_min(junksize-1,nj)] = '\0';
                        ncexp = camp_stoprint_expand( junkstr, dbuf, sizeof( dbuf ) );
                        if ( ncexp > sizeof(dbuf) - 4 )
                        {
                            dbuf[sizeof(dbuf)-5] = '\0';
                            camp_strncat( dbuf, sizeof(dbuf), "..." );
                        }
                        printf( "Gpib: flushed %d chars: '%s' from port '%s'\n", nj, dbuf, port );
                    }
		}
		FD_ZERO( &readfds );
		FD_SET( fd, &readfds );
                cleartimeval( &tvFlush );
	    }

	    /*
	     *  Request read from instrument (address to talk)
	     */
	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) { printf("Gpib: request read with ++read eoi ... "); fflush(stdout); }

	    if( write( fd, "\n++read eoi\n", 12 ) < 0 )
	    {
		_close( fd );
		give_gpib_take_global();
		_camp_setMsg( "write failure on gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }
            if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("done\n");
	    /*
	     *  Read with timeout.
	     *
	     *  Probably should change to multiple read attempts with short timeouts, with pauses
             *  releasing the gpib mutex between them.
	     */

	    buf[0] = '\0';

	    for( ;; )
	    {
		FD_ZERO( &readfds );
		FD_SET( fd, &readfds );

		nfds = select( fd+1, &readfds, NULL, NULL, &tvTimeout );
		if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: select returns nfds %d\n", nfds);

		switch( nfds )
		{
		case -1:
		    /* 
		     *  Error
		     */
		    if( errno == EINTR ) continue;
		    _close( fd );
		    give_gpib_take_global();
		    _camp_setMsg( "select failure on port '%s'", port );
		    camp_status = CAMP_FAILURE; goto return_;

		case 0:
		    /* 
		     *  Timeout from select()
		     *  timeout shoud only happen when there are no characters.
		     *  There is also the instrument (character-by-character) timeout for the
		     *  communication across gpib.  Unfortuantely, the Prologix gives no way 
		     *  to tell if this sort of timeout occurred, versus normal end of transmission.
		     *  Call it success for now until I do testing.
		     */
		    _close( fd );
		    give_gpib_take_global();
		    if( (timeout <= 0 || *pRead_len > 0) ) /* rterm_len == 0 &&  */
		    {
		        camp_status = CAMP_SUCCESS; goto return_;
		    }
		    else
		    {
                        if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: read timeout from select().\n");
			_camp_setMsg( "timeout reading port '%s', got '%s'", port, bufBegin );
			camp_status = CAMP_FAILURE; goto return_;
		    }

		default:
		    if( !FD_ISSET( fd, &readfds ) ) continue;
		    /* 
		     *  Something to read
		     */ 
		    if( buf_len == 0 )
		    {
			break; /* from switch */
		    }
          
		    nread = read( fd, buf, buf_len );
                    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) 
                    {
                        if ( buf_len > nread )
                        {
                            buf[nread] = '\0';
                            ncexp = camp_stoprint_expand( buf, dbuf, sizeof(dbuf) );
                            if ( ncexp > sizeof(dbuf) - 2 ) 
                            {
                                dbuf[sizeof(dbuf)-5] = '\0';
                                camp_strncat( dbuf, sizeof(dbuf), "..." );
                            }
                            printf( "Gpib: read %d chars: '%s'\n", nread, dbuf );
                        }
                        else
                        {
                            printf( "Gpib: read %d chars, filling buffer\n", nread );
                        }   
                    }
		    buf += nread;
		    buf_len -= nread;
		    *pRead_len += nread;
		    if( buf_len > 0 ) buf[0] = '\0';
		    break; /* from switch */
		}

                if( _camp_debug(CAMP_DEBUG_IF_GPIB) && buf_len == 0 ) printf("Gpib: filled the buffer.\n");
                /* should we set  buf[-1] = '\0' ? */
		if( buf_len == 0 ) break; /* from for loop */ 
		/*
		 *  Check if we have terminator yet
		 */
		if( rterm_len > 0 )
		{
		    eol = strstr( bufBegin, rterm );
		    if( eol != NULL ) 
		    {
                        if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: found terminator string.\n");
			*eol = '\0';
			*pRead_len = (int)(eol - bufBegin);
			break; /* from for loop */
		    }
		}
	    } /* end for(;;) loop */
  
	    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: success... close port.\n");

	    _close( fd );
	    give_gpib_take_global();
	} // give_global_take_gpib
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    _free( comstr );
    _free( port );
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_gpib_write,  synchronous write 
 */
int if_gpib_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_IF* pIF;
    int fd = -1;
    char* cmd;
    /* int cmd_len; */
    char term[8];  /* descriptive "CRLF" */
    char wterm[8]; /* the actual termination characters */
    int wterm_len;
    char* comstr = NULL;
    int comsize = 0;
    int comlen = 0;
    char* port = NULL;
    char setadd[16];
    int addr, adlen;
    // _mutex_lock_begin();

    pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked by ins mutex
    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    /* cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len; */
    camp_if_termTokenToChars( camp_getIfGpibWriteTerm( pIF->defn, term, sizeof( term ) ), 
			      wterm, sizeof( wterm ), &wterm_len );

    addr = camp_getIfGpibAddr( pIF->defn );
    adlen = camp_snprintf( setadd, sizeof(setadd), "\n++addr %2d\n", addr);
    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: if_gpib_write %s to addr %d\n", cmd, addr);

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock // lock for pIF_t
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL )
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
  
	port = strdup( pIF_t->conf );

	comsize = escape_termin_strlen( cmd, wterm ) + 1;
	comstr = (char*)camp_malloc( comsize );
	comlen = escape_termin_str( comstr, comsize, cmd, wterm );
	if ( comlen < 0 ) 
	{
	    _camp_setMsg( "Weird error escaping string" );
	    camp_status = CAMP_FAILURE; goto return_;
	}

	// give_global_take_gpib
	{
	    give_global_take_gpib();

	    if( ( fd = open( port, (O_WRONLY|O_NOCTTY)) ) < 0 )
	    {
		give_gpib_take_global();
		_camp_setMsg( "failure opening gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    if( write( fd, setadd, adlen ) < 0 )
	    {
		_close( fd );
		give_gpib_take_global();
		_camp_setMsg( "write failure on gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    if( write( fd, comstr, comlen ) < 0 )
	    {
		_close( fd );
		give_gpib_take_global();
		_camp_setMsg( "write failure on gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    _close( fd );

	    give_gpib_take_global();
	} // give_global_take_gpib
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    _free( comstr );
    _free( port );
    // _mutex_lock_end();
    return( camp_status );
}

/*
 *  if_gpib_clear
 *
 *  Sends an SDC (selected device clear) to the specified address.
 *  Does not use any CAMP instrument interface definition, but does
 *  require previous setup by if_gpib_init. Nevertheless, the gpib
 *  setup commands performed by if_gpib_init are repeated.
 */
int if_gpib_clear( int gpib_addr )
{
    int camp_status = CAMP_SUCCESS;
    int fd = -1;
    const char inicmd[] = { "\n++mode 1\n++auto 0\n++eos 3\n++eoi 1\n++ifc\n" };
    char* port = NULL;
    char command[20];
    int  commlen;
    long timeout;
    fd_set writefds;
    int nfds;
    // _mutex_lock_begin();

    if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: if_gpib_clear begins\n");

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock // lock for pIF_t
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( "gpib" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if ( !if_gpib_ok( pIF_t ) )
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}
  
	port = strdup( pIF_t->conf );

	if ( gpib_addr<1 || gpib_addr>30 ) 
	{
	    _camp_setMsg( "gpib address out of range 1-30" );
	    camp_status = CAMP_FAILURE; goto return_;
	}
        if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: Clear address %d on port %s.\n", gpib_addr, port);

	commlen = camp_snprintf( command, sizeof(command), "\n++addr %d\n++clr\n", gpib_addr);

	// give_global_take_gpib
	{
	    give_global_take_gpib();
  
	    if( ( fd = open( port, (O_WRONLY|O_NOCTTY)) ) < 0 )
	    {
		give_gpib_take_global();
                if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf( "failure opening gpib FDTI port %s", port );
		_camp_setMsg( "failure opening gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }
            if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: opened port %s as fd %d\n", port, fd);

            if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: write ini string:%s\n", inicmd);
	    if( write( fd, inicmd, strlen(inicmd) ) < 0 )
	    {
                if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: write failed\n");
		_close( fd );
		give_gpib_take_global();
                if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf ( "initialization failure on gpib FDTI port %s", port );
		_camp_setMsg( "initialization failure on gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }

            if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf("Gpib: write command string:%s\n", command);
	    if( write( fd, command, commlen ) < 0 )
	    {
                if( _camp_debug(CAMP_DEBUG_IF_GPIB) ) printf ( "write failure on gpib FDTI port %s", port );
		_close( fd );
		give_gpib_take_global();
		_camp_setMsg( "write failure on gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    _close( fd );
	    give_gpib_take_global();
	} // give_global_take_gpib
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     * Status could be checked here with ++status (?)  
     */
  
return_:
    _free( port );
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_gpib_timeout
 *
 *  Sets the Prologix timeout parameter read_tmo_ms, but that is
 *  probably not what is wanted.
 *
 *  Does not use any CAMP instrument interface definition, but does
 *  require previous setup by if_gpib_init. Why is the timeout an
 *  integer number of seconds? That is not fine-grained.
 *  It was an integer because both of the two preceding gpib
 *  drivers took an integral timeout.
 *
 *  20140403     TW  changed argument from int to float
 */
int if_gpib_timeout( float timeoutSeconds )
{
    int camp_status = CAMP_SUCCESS;
    int fd = -1;
    char* port = NULL;
    char command[20];
    int  commlen;
    int  to_ms;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock // lock for pIF_t
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( "gpib" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "gpib" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}

	if( !if_gpib_ok( pIF_t ) )
	{
	    _camp_setMsg( "gpib unavailable" );
	    camp_status = CAMP_FAILURE; goto return_;
	}
  
	port = strdup( pIF_t->conf );

	to_ms = (int)1000.0f*timeoutSeconds;
	if ( to_ms < 10 ) to_ms = 10;
	if ( to_ms > 3000 ) to_ms = 3000;
	commlen = camp_snprintf( command, sizeof(command), "++read_tmo_ms %d", to_ms );

	// give_global_take_gpib
	{
	    give_global_take_gpib();
  
	    if( ( fd = open( port, (O_WRONLY|O_NOCTTY)) ) < 0 )
	    {
		give_gpib_take_global();
		_camp_setMsg( "failure opening gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }

	    if( write( fd, command, commlen ) < 0 )
	    {
		_close( fd );
		give_gpib_take_global();
		_camp_setMsg( "write failure on gpib FDTI port %s", port );
		camp_status = CAMP_FAILURE; goto return_;
	    }
  
	    _close( fd );
	    give_gpib_take_global();
	} // give_global_take_gpib
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    
return_:
    _free( port );
    // _mutex_lock_end();
    return( camp_status );
}
