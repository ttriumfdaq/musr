/*
 *  $Id: camp_if_indpak_template.c,v 1.2 2015/04/21 03:47:14 asnd Exp $
 *
 *  $Revision: 1.2 $
 *
 *  Purpose:    Provides an Industry Pack (INDPAK) interface type.
 *
 *              THIS FILE IS A TEMPLATE for interface implementation.
 *
 *              20140214 TW  The notes below were copied straight 
 *              from camp_if_camac_gen.c and may not apply to this 
 *              interface type.  To my knowledge, no indpak interface
 *              has ever been implemented.
 *
 *              Interface routines implementing a generic library.
 *
 *              Note that indpak I/O implementation in CAMP is by way of
 *              ASCII command strings which are interpreted in a Tcl
 *              interpreter.  The routines if_indpak_write and if_indpak_read
 *              supplied here simply pass valid ASCII command strings to
 *              the interpreter.  Consequently, there doesn't need to be
 *              anything specific to a particular library in this file.
 *
 *              A CAMP INDPAK interface definition must provide the following
 *              routines:
 *                int if_indpak_init ( void );
 *                int if_indpak_online ( CAMP_IF *pIF );
 *                int if_indpak_offline ( CAMP_IF *pIF );
 *                int if_indpak_read( REQ* pReq );
 *                int if_indpak_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines are called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *  Revision history:
 *
 *    20140217  TW  rename set_global_mutex_noChange to mutex_lock_mainInterp
 *
 *  $Log: camp_if_indpak_template.c,v $
 *  Revision 1.2  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.2  2004/01/28 03:25:50  asnd
 *  Fix comments.
 *
 *  Revision 1.1  2000/12/22 22:52:01  David.Morris
 *  Copied from camp_if_camac_gen.c. Initial version has no action for
 *  read and write methods. Always return success. These need to be fleshed out.
 *
 *
 */

#include <stdio.h>
#include "camp_srv.h"


int
if_indpak_init( void )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	pIF_t = camp_ifGetpIF_t( "indpak" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     *  Interperate interface configuration info here
     *  (none for generic).
     */

 return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_indpak_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

 return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_indpak_offline( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

 return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_indpak_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    return( camp_status );
}


int
if_indpak_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    return( camp_status );
}
