/*
 *  Name:       camp_if_none_gen.c
 *
 *  $Id: camp_if_none_gen.c,v 1.4 2015/04/21 03:47:14 asnd Exp $
 *
 *  $Revision: 1.4 $
 *
 *  Purpose:    Provides the "none" interface type, for Camp "instrument" 
 *              types that have no real instrument; for example the generic
 *              PID controller.
 *
 *              Provide the following:
 *                int if_none_init ( void );
 *                int if_none_online ( CAMP_IF *pIF );
 *                int if_none_offline ( CAMP_IF *pIF );
 *                int if_none_read( REQ* pReq );
 *                int if_none_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In the implementation of CAMP "none" interface, the read and
 *              write functions are defined to execute some Tcl script in
 *              its own interp.  This might be useful for multi-threading
 *              slow operations.
 *
 *  Revision history:
 *
 *    20140217  TW  rename set_global_mutex_noChange to mutex_lock_mainInterp
 *
 *  $Log: camp_if_none_gen.c,v $
 *  Revision 1.4  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.1  2003/11/10 22:41:25  asnd
 *  Get interface type "none" working
 *
 */

#include <stdio.h>
#include "camp_srv.h"

int
if_none_init( void )
{
    int camp_status = CAMP_SUCCESS;

    {
	// pedantic check
	CAMP_IF_t* pIF_t = camp_ifGetpIF_t( "none" );
	if( pIF_t == NULL)
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, "none" );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }

return_:
    return( camp_status );
}


int
if_none_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;

    {
	// pedantic check
	CAMP_IF_t* pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
	if( pIF_t == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }

return_:
    return( camp_status );
}


int
if_none_offline( CAMP_IF* pIF )
{
    return( CAMP_SUCCESS );
}

int
if_none_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd; 
    _mutex_lock_begin();

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e., the
     *  interpreter in the main thread of execution) then
     *  lock this (the main) interpreter while using it.
     *
     *  20140217  TW  This has been copied from the camp_if_camac_gen
     *  interface which uses Tcl (as a convenience) to parse/dispatch
     *  camac commands (like cfsa) as text strings.  Does this make
     *  sense for a 'none' interface?
     */
    {
	int tcl_status = TCL_OK;
	int useMainInterp = FALSE;
	Tcl_Interp* interp = get_thread_interp();
	if( interp == camp_tclInterp() ) useMainInterp = TRUE;

	//  20140225  TW  don't use interp outside of lock
	if( useMainInterp ) _mutex_lock_mainInterp_on();
	{
	    tcl_status = Tcl_Eval( interp, cmd );

	    if( tcl_status == TCL_ERROR )
	    {
		char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

		assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

		camp_setMsg( "%s", tcl_message );		
		_camp_appendMsg( "failed read" );

		free( tcl_message );
	    }
	}
	if( useMainInterp ) _mutex_lock_mainInterp_off();

	if( tcl_status == TCL_ERROR )
	{
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }

    camp_status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( camp_status );
}

int
if_none_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    char* cmd; 
    _mutex_lock_begin();

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
    {
	int tcl_status = TCL_OK;
	int useMainInterp = FALSE;
	Tcl_Interp* interp = get_thread_interp();
	if( interp == camp_tclInterp() ) useMainInterp = TRUE;

	//  20140225  TW  don't use interp outside of lock
	if( useMainInterp ) _mutex_lock_mainInterp_on();
	{
	    tcl_status = Tcl_Eval( interp, cmd );

	    if( tcl_status == TCL_ERROR )
	    {
		char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

		assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

		camp_setMsg( "%s", tcl_message );	    
		_camp_appendMsg( "failed write" );

		free( tcl_message );
	    }
	}
	if( useMainInterp ) _mutex_lock_mainInterp_off();

	if( tcl_status == TCL_ERROR )
	{
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }

    camp_status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( camp_status );
}

