/*
 *  Name:       camp_if_priv.c
 *
 *  Purpose:    These routines provide access to an instrument's interface.
 *
 *              An instrument has an associated interface, identified
 *              by an ASCII "typeIdent" (pIF->typeIdent).  These routines
 *              make the association between this typeIdent and an
 *              interface type which is general to all instruments
 *              (i.e., rs232, gpib, camac,...).  The interface types are
 *              maintained by the CAMP server separate from the instruments.
 *              These routines then call the appropriate routine for
 *              the particular interface type.
 *
 *              The interface type procs (e.g., onlineProc) are set in
 *              camp_tcl.c using the Tcl command sysAddIfType normally
 *              upon server initialization.
 *
 *              Note that these routines should initiate hardware
 *              level
 *
 *  Provides:   camp_ifSet
 *              camp_ifOnline
 *              camp_ifOffline
 *              camp_ifRead
 *              camp_ifWrite
 *
 *  Called by:  camp_srv_proc.c
 * 
 *  Revision history:
 *
 *    20140219     TW  mutex_un/lock_global -> mutex_un/lock_global_all, change scheme
 *    20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "camp_srv.h"


int
camp_ifSet( CAMP_IF** ppIF, CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
    if( pIF_t == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
        camp_status = CAMP_INVAL_IF_TYPE; goto return_;
    }

    if( *ppIF == NULL )
    {
        *ppIF = (CAMP_IF*)camp_zalloc( sizeof( CAMP_IF ) );
    }
    else
    {
        if( (*ppIF)->status & CAMP_IF_ONLINE )
        {
            _camp_setMsg( "can't reset online interface (type='%s')", pIF->typeIdent );
            camp_status = CAMP_CANT_SET_IF; goto return_;
        }

        xdr_free( (xdrproc_t)xdr_CAMP_IF, (char*)*ppIF );
        bzero( (void*)*ppIF, sizeof( CAMP_IF ) );
    }

    camp_strdup( &(*ppIF)->typeIdent, pIF->typeIdent ); // free and strdup
    (*ppIF)->accessDelay = pIF->accessDelay;
    (*ppIF)->timeout = pIF->timeout;
    camp_strdup( &(*ppIF)->defn, pIF->defn ); // free and strdup
    (*ppIF)->typeID = camp_getIfTypeID( pIF->typeIdent );

return_:
    return( camp_status );
}


int
camp_ifOnline( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_IF_t* pIF_t;

    if( pIF == NULL ) 
    {
	 // get_current_ident may not be meaningful here, but it's all we have
        _camp_setMsgStat( CAMP_INVAL_IF, get_current_ident() );
        camp_status = CAMP_INVAL_IF; goto return_;
    }

    if( pIF->status & CAMP_IF_ONLINE ) 
    {
        camp_status = CAMP_SUCCESS; goto return_;
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
    if( pIF_t == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
        camp_status = CAMP_INVAL_IF_TYPE; goto return_;
    }

    if( pIF_t->procs.onlineProc != NULL )
    {
        camp_status = (*pIF_t->procs.onlineProc)( pIF );
        if( _failure( camp_status ) )
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed onlineProc '%s'", get_current_ident() ); }
            goto return_;
        }
    }

    pIF->status |= CAMP_IF_ONLINE;
    pIF->numConsecReadErrors = 0;
    pIF->numConsecWriteErrors = 0;

return_:
    return( camp_status );
}


int
camp_ifOffline( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_IF_t* pIF_t;

    if( pIF == NULL ) 
    {
        // _camp_setMsgStat( CAMP_INVAL_IF, get_current_ident() );
        // camp_status = CAMP_INVAL_IF; goto return_;

	//  Make this a successful request, since it is harmless. 
        camp_status = CAMP_SUCCESS; goto return_;
    }

    if( !( pIF->status & CAMP_IF_ONLINE ) )
    {
        camp_status = CAMP_SUCCESS; goto return_;
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
    if( pIF_t == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
        camp_status = CAMP_INVAL_IF_TYPE; goto return_;
    }

    if( pIF_t->procs.offlineProc != NULL )
    {
        camp_status = (*pIF_t->procs.offlineProc)( pIF );
        if( _failure( camp_status ) )
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed offlineProc '%s'", get_current_ident() ); }
            goto return_;
        }
    }

    pIF->status &= ~CAMP_IF_ONLINE;

return_:
    return( camp_status );
}


/*
 *  Called by:
 *     campsrv_insifread / insIfRead
 *     campsrv_insifdump / insIfDump
 */
int
camp_ifRead( CAMP_IF* pIF, CAMP_VAR* pVar, char* cmd, int cmd_len, 
             char* buf, int buf_len, int* pRead_len )
{
    int camp_status = CAMP_SUCCESS;
    REQ* pReq = NULL;
    CAMP_IF_t* pIF_t = NULL;
    timeval_t tv;

    if( pIF == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF, pVar->core.ident ); // get_current_ident()
        camp_status = CAMP_INVAL_IF; goto return_;
    }

    if( pVar == NULL )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR, "?" );
        camp_status = CAMP_INVAL_VAR; goto return_;
    }

    if( !( pIF->status & CAMP_IF_ONLINE ) )
    {
        _camp_setMsgStat( CAMP_NOT_CONN, pVar->core.ident ); // get_current_ident()
        camp_status = CAMP_NOT_CONN; goto return_;
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
    if( pIF_t == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
        camp_status = CAMP_INVAL_IF_TYPE; goto return_;
    }

    if( pIF_t->procs.readProc != NULL )
    {
        pReq = (REQ*)camp_zalloc( sizeof( REQ ) );

        pReq->pVar = pVar; // note: mutex locking handled in camp_req
        pReq->spec.type = IO_READ;
        pReq->spec.REQ_SPEC_u.read.pIF = pIF;
        pReq->spec.REQ_SPEC_u.read.cmd = cmd;
        pReq->spec.REQ_SPEC_u.read.cmd_len = cmd_len;
        pReq->spec.REQ_SPEC_u.read.buf = buf;
        pReq->spec.REQ_SPEC_u.read.buf_len = buf_len;
	// REQ_add( pReq ); // no: use REQ locally

        if( pIF->accessDelay < 0 ) 
        {
            int globalMutexLockCount = mutex_unlock_global_all();
            camp_fsleep( fabs( (double)(pIF->accessDelay) ) );
            mutex_lock_global_all( globalMutexLockCount );
        }
        else if( pIF->accessDelay > 0 )
        {
	    double delay;

            gettimeval( &tv );
            delay = pIF->accessDelay - difftimeval( &tv, &pIF->lastAccess );
            if( delay > 0.0 )
            {
                int globalMutexLockCount = mutex_unlock_global_all();
                camp_fsleep( delay );
                mutex_lock_global_all( globalMutexLockCount );
            }
        }

        camp_status = (*pIF_t->procs.readProc)( pReq ); // call if_*_read
        pIF->numReads++;
        gettimeval( &pIF->lastAccess );
        if( _failure( camp_status ) )
        {
            _free( pReq );
            *pRead_len = 0;
            pIF->numReadErrors++;
            pIF->numConsecReadErrors++;

	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed readProc '%s'", pVar->core.ident ); }

            if( pIF->numConsecReadErrors >= MAX_CONSECUTIVE_IF_ERRORS )
            {
                _camp_appendMsg( "Too many read errors, setting instrument '%s' offline", pVar->core.ident );
                camp_ifOffline( pIF );
            }

            goto return_;
        }
        else
        {
            pIF->numConsecReadErrors = 0;
        }

        *pRead_len = pReq->spec.REQ_SPEC_u.read.read_len;
        _free( pReq );
    }

return_:
    return( camp_status );
}


/*
 *  camp_ifWrite
 *  
 *  Call the interfaces writeProc with a variable pointer and 
 *  an ASCII string command.  The corresponding writeProc's can
 *  be found in camp_if_rs232*.c camp_if_gpib*.c and camp_if_camac*.c .
 *  For RS232 and GPIB, the string will be sent to the instrument.
 *  For CAMAC, things are a little different, the string command is
 *  actually a CAMAC command with parameters (like cfsa ...), which 
 *  initiates a call to the camac function defined in camp_tcl.c
 *  (camp_tcl_camac_cdreg, camp_tcl_camac_single, camp_tcl_camac_block).
 *
 *  Called from:  campsrv_insifwrite (called by direct RPC call, or
 *                   by a Tcl command insIfWrite)
 */
int
camp_ifWrite( CAMP_IF* pIF, CAMP_VAR* pVar, char* cmd, int cmd_len )
{
    int camp_status = CAMP_SUCCESS;
    REQ* pReq = NULL;
    CAMP_IF_t* pIF_t = NULL;
    timeval_t tv;

    if( pIF == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF, pVar->core.ident ); // get_current_ident() );
        camp_status = CAMP_INVAL_IF; goto return_;
    }

    if( pVar == NULL )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR, "?" );
        camp_status = CAMP_INVAL_VAR; goto return_;
    }

    if( !( pIF->status & CAMP_IF_ONLINE ) )
    {
        _camp_setMsgStat( CAMP_NOT_CONN, pVar->core.ident ); // get_current_ident() );
        camp_status = CAMP_NOT_CONN; goto return_;
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
    if( pIF_t == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF_TYPE, pIF->typeIdent );
        camp_status = CAMP_INVAL_IF_TYPE; goto return_;
    }

    if( pIF_t->procs.writeProc != NULL )
    {
        pReq = (REQ*)camp_zalloc( sizeof( REQ ) );

        pReq->pVar = pVar; // note: mutex locking handled in camp_req
        pReq->spec.type = IO_WRITE;
        pReq->spec.REQ_SPEC_u.write.pIF = pIF;
        pReq->spec.REQ_SPEC_u.write.cmd = cmd;
        pReq->spec.REQ_SPEC_u.write.cmd_len = cmd_len;
	// REQ_add( pReq ); // no: use REQ locally

        if( pIF->accessDelay < 0 ) 
        {
            int globalMutexLockCount = mutex_unlock_global_all();
            camp_fsleep( fabs( (double)(pIF->accessDelay) ) );
            mutex_lock_global_all( globalMutexLockCount );
        }
        else if( pIF->accessDelay > 0 )
        {
	    double delay;

            gettimeval( &tv );
            delay = pIF->accessDelay - difftimeval( &tv, &pIF->lastAccess );
            if( delay > 0.0 )
            {
                int globalMutexLockCount = mutex_unlock_global_all();
                camp_fsleep( delay );
                mutex_lock_global_all( globalMutexLockCount );
            }
        }

        camp_status = (*pIF_t->procs.writeProc)( pReq );
        pIF->numWrites++;
        gettimeval( &pIF->lastAccess );
        if( _failure( camp_status ) )
        {
            _free( pReq );
            pIF->numWriteErrors++;
            pIF->numConsecWriteErrors++;

	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed writeProc '%s'", pVar->core.ident ); }

            if( pIF->numConsecWriteErrors >= MAX_CONSECUTIVE_IF_ERRORS )
            {
                _camp_appendMsg( "Too many write errors, setting instrument '%s' offline", pVar->core.ident );
                camp_ifOffline( pIF );
            }

            goto return_;
        }
        else
        {
            pIF->numConsecWriteErrors = 0;
        }

        _free( pReq );
    }

return_:
    return( camp_status );
}


/*
 *  Name:        camp_getIfTypeID
 *
 *  Purpose:     Return interface type id (integer) based on
 *               interface type identifier (string)
 *
 *               Note that interface types are maintained in the CAMP
 *               server, independent of what instruments are present.
 *
 *  Inputs:      Interface type identifier (string) (e.g., rs232, gpib).
 *
 *  Outputs:     Interface type id (integer).
 *
 *  Revision history:
 *    20140226  TW  moved from camp_if_utils to camp_if_priv
 */
CAMP_IF_TYPE
camp_getIfTypeID( const char* typeIdent )
{
    CAMP_IF_TYPE typeID = CAMP_IF_TYPE_NONE;

    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( typeIdent /* , mutex_lock_sys_check() */ );

	if( pIF_t != NULL ) typeID = pIF_t->typeID;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    // _mutex_lock_end();
    return( typeID );
}


/*
 *  Name:        camp_getIfTypeIdent
 *
 *  Purpose:     Return interface type identifier (string) based on
 *               interface type id (integer)
 *
 *               Note that interface types are maintained in the CAMP
 *               server, independent of what instruments are present.
 *
 *  Inputs:      Interface type id (integer).
 *
 *  Outputs:     Interface type identifier (string) (e.g., rs232, gpib).
 *               The returned string pointer points to a memory location
 *               in the CAMP database and should not be overwritten or
 *               deallocated.
 *
 *  Revision history:
 *    20140226  TW  moved from camp_if_utils to camp_if_priv
 */
const char* 
camp_getIfTypeIdent( CAMP_IF_TYPE typeId )
{
    const char* typeIdent = NULL;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;

	for( pIF_t = pSys->pIFTypes; 
	     pIF_t != NULL; 
	     pIF_t = pIF_t->pNext ) 
	{
	    if( typeId == pIF_t->typeID ) 
	    {
		typeIdent = pIF_t->ident;
		break;
	    }
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    // _mutex_lock_end();
    return( typeIdent );
}


/*
 *  Name:        camp_rs232PortInUse
 *
 *  Purpose:     Test if the rs232 port used by instrument *pIns
 *               is in use by another (online) instrument.  If yes,
 *               return pointer to that instrument; else NULL.
 *
 *  Inputs:      Pointer to instrument attempting to go online
 *               (also external pVarList).
 *
 *  Outputs:     Pointer to conflicting instrument (CAMP_VAR*) or NULL
 *
 *  Called by:   campsrv_insifon in camp_srv_proc.c (but the test 
 *               may get moved to another place).
 * 
 *  Revision history:
 *    29-Sep-2000   DJA   Initial version
 *    20140225  TW  moved from camp_if_utils to camp_if_priv - server-only function
 */
CAMP_VAR*
camp_rs232PortInUse( CAMP_INSTRUMENT* pIns_in /* , bool_t _mutex_lock_varlist_check */ )
{
    CAMP_VAR* pVar_found = NULL;
    char port[LEN_IDENT+1];
    char portOther[LEN_IDENT+1];
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF = pIns_in->pIF;
	camp_getIfRs232Port( pIF->defn, port, sizeof( port ) );
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar = NULL;

	for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
	{
	    CAMP_INSTRUMENT* pIns;

	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;
	    if( pIns != pIns_in )
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF != NULL )
		{
		    if( pIF->typeID == CAMP_IF_TYPE_RS232 )
		    {
			if( pIF->status & CAMP_IF_ONLINE )
			{
			    camp_getIfRs232Port( pIF->defn, portOther, sizeof( portOther ) );
			    if( strncmp( port, portOther, LEN_IDENT ) == 0)
			    {
				pVar_found = pVar;
				break;
			    }
			}
		    }
		}
	    }
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // _mutex_lock_end();
    return( pVar_found );
}

  
