/*
 *  Name:       camp_if_rs232_tty.c
 *
 *  Purpose:    Provides an RS232 serial communications interface type.
 *              Implementation using standard Unix TTY ports.
 *
 *              A CAMP RS232 interface definition must provide the following
 *              routines:
 *                int if_rs232_init ( void );
 *                int if_rs232_online ( CAMP_IF *pIF );
 *                int if_rs232_offline ( CAMP_IF *pIF );
 *                int if_rs232_read( REQ* pReq );
 *                int if_rs232_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In this implementation, the global mutex is unlocked
 *              during calls to the Unix library functions write(),
 *              select(), and read().
 *
 *  Notes:
 *
 *     Ini file configuration string:
 *        {<port1> <port2> ...}
 *
 *     Meaning of private (driver-specific) variables:
 *        pIF_t->priv - not used
 *        pIF->priv - not used (was: terminal port file descriptor)
 *
 *  Revision history:
 *
 *    20140220     TW  camp_if_termTokenToChars
 *    20140219     TW  mutex_un/lock_global -> mutex_un/lock_global_all, change scheme
 *    20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *    1999-10-19   dbm Added stop bit call into if-rs232-settings
 *    15-Dec-1999  TW/DA In if_rs232_read: don't check terminator if there
 *                     isn't one
 *                     Also, continue normally if FIOSTOPBITS ioctl fails
 *                     (some TTY drivers are not implemented with a stop bits
 *                     function even though probably all RS232 chips will
 *                     do it).
 *    21-Dec-1999  TW  Took out calls to srv_check_timeout() - not used anymore
 *                     (this was previously allowing the possibility of 
 *                     a timeout response to a CAMP client before the client
 *                     does an RPC timeout, but found to be better without)
 *    24-Jan-1999  DA  When there is a terminator, terminate the string!
 *
 */
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#ifdef VXWORKS
#define CAMP_IF_RS232_VXWORKS
#elif defined( linux )
#define CAMP_IF_RS232_TERMIOS  // termios-style serial I/O
#elif defined( VMS )
#define CAMP_IF_RS232_BSD  // BSD-style serial I/O
#endif // 

#ifdef CAMP_IF_RS232_VXWORKS
#include "ioLibx.h"
#endif /* CAMP_IF_RS232_VXWORKS */

#ifdef CAMP_IF_RS232_TERMIOS
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#ifndef CAMP_IF_RS232_PERSISTENT
#define CAMP_IF_RS232_PERSISTENT 1
#endif
#endif /* CAMP_IF_RS232_TERMIOS */

#ifdef CAMP_IF_RS232_BSD
#include <sys/termio.h>
#include <sys/sgtty.h>
#include <sys/ioctl.h>
#endif /* CAMP_IF_RS232_BSD */

#ifdef linux
#include <unistd.h> // read,write,close
#endif // linux

/*
 *  CAMP_IF_RS232_PERSISTENT flags if the serial port is held open between accesses.
 *  Found problems with flushing buffers when closing and reopening usb-serial ports.
 */
#ifndef CAMP_IF_RS232_PERSISTENT
#define CAMP_IF_RS232_PERSISTENT 0
#endif

#include "camp_srv.h" // "camp.h" // 20140226  TW

static int if_rs232_settings( CAMP_IF* pIF, int fd );
#if defined( CAMP_IF_RS232_TERMIOS ) || defined( CAMP_IF_RS232_BSD )
#if defined( CAMP_IF_RS232_TERMIOS )
static speed_t
#elif defined( CAMP_IF_RS232_BSD )
static int
#endif
getBaud( int baud );
static int getCharSize( int size );
#endif // CAMP_IF_RS232_TERMIOS || CAMP_IF_RS232_BSD

int
if_rs232_init( void )
{
    /*
     *  Could check that the ports exist here
     */
    return( CAMP_SUCCESS );
}


static int
if_rs232_settings( CAMP_IF* pIF, int fd )
{
    char port[LEN_STR+1];
    int baud;
    int databits;
    char sParity[8];
    int cParity;
    int stopbits;
#ifdef CAMP_IF_RS232_TERMIOS
    struct termios ttySettings;
#endif // CAMP_IF_RS232_TERMIOS
#ifdef CAMP_IF_RS232_BSD
    struct sgttyb ttySettings;
#endif // CAMP_IF_RS232_BSD

    camp_getIfRs232Port( pIF->defn, port, sizeof( port ) );
    baud = camp_getIfRs232Baud( pIF->defn );
    databits = camp_getIfRs232Data( pIF->defn );
    camp_getIfRs232Parity( pIF->defn, sParity, sizeof( sParity ) );
    cParity = toupper( sParity[0] );
    stopbits = camp_getIfRs232Stop( pIF->defn );

    /*
     *  Set the port settings
     */

#ifdef CAMP_IF_RS232_VXWORKS

    if( ioctl( fd, FIOSETOPTIONS, OPT_RAW ) == ERROR )
    {
	_camp_setMsg( "failure setting port %s parameters", port );
	goto failure;
    }

    if( ioctl( fd, FIOBAUDRATE, baud ) == ERROR )
    {
	_camp_setMsg( "failure setting port %s baud rate", port );
	goto failure;
    }

    /*
     *  ioctl functions FIOPARITY and FIODATABITS were
     *  added to drivers for onboard serial, IP-QuadSerial, 
     *  and IP-OctalSerial, but not others.  Ignore failure
     *  on any of parity "none", databits 8, stopbits 1.
     */
    if( ioctl( fd, FIOPARITY, cParity ) == ERROR && cParity != 'N' )
    {
	_camp_setMsg( "failure setting port '%s' parity", port );
	goto failure;
    }

    if( ioctl( fd, FIODATABITS, databits ) == ERROR && databits != 8 )
    {
	_camp_setMsg( "failure setting port '%s' databits", port );
	goto failure;
    }

    if( ioctl( fd, FIOSTOPBITS, stopbits ) == ERROR && stopbits != 1 )
    {
	_camp_setMsg( "failure setting port '%s' stopbits", port );
	goto failure;
    }

#elif defined( CAMP_IF_RS232_TERMIOS )

    if( tcgetattr( fd, &ttySettings ) == -1 ) 
    {
	_camp_setMsg( "failure getting port %s parameters", port );
	goto failure;
    }

    /*
     * c_iflag: input modes: INPCK,IXON
     * c_oflag: output modes:
     * c_cflag: control modes: (BAUD,CSIZE,CSTOPB,PARENB,PARODD,
     * c_lflag: local modes: ECHO,
     */

    /* tcflush( fd, TCIOFLUSH );  Do not flush when timeout <= 0 */

    cfmakeraw( &ttySettings );

    cfsetispeed ( &ttySettings, getBaud( baud ) );
    cfsetospeed ( &ttySettings, getBaud( baud ) );

    ttySettings.c_cflag = (ttySettings.c_cflag & ~CSIZE) | (getCharSize(databits) & CSIZE) ;

    if ( cParity == 'O' || cParity == 'E' ) 
	ttySettings.c_cflag |= PARENB;
    else
	ttySettings.c_cflag &= ~PARENB;

    if ( cParity == 'O' ) ttySettings.c_cflag |= PARODD;
    if ( cParity == 'E' ) ttySettings.c_cflag &= ~PARODD;

    if ( stopbits == 2 ) 
	ttySettings.c_cflag |= CSTOPB;
    else
	ttySettings.c_cflag &= ~CSTOPB;


    if( tcsetattr( fd, TCSANOW, &ttySettings ) == -1 )
    {
	_camp_setMsg( "failure setting port %s parameters", port );
	goto failure;
    }

#elif defined( CAMP_IF_RS232_BSD )

    if( ioctl( fd, TIOCGETP, &ttySettings ) == -1 ) 
    {
	_camp_setMsg( "failure getting port %s parameters", port );
	goto failure;
    }

    ttySettings.sg_flags |= RAW;
    ttySettings.sg_flags &= ~(ECHO|CRMOD);

    ttySettings.sg_ispeed = getBaud( baud );
    ttySettings.sg_ospeed = ttySettings.sg_ispeed;

    /*
     *  Must set parity/databits here
     *  Default is 8 data, no parity
     */

    if( ioctl( fd, TIOCSETP, &ttySettings ) == -1 )
    {
	_camp_setMsg( "failure setting port %s parameters", port );
	goto failure;
    }

#endif // CAMP_IF_RS232_*

    // pIF->priv = integerToPointer( fd );

    return( CAMP_SUCCESS );

failure:
    return( CAMP_FAILURE );
}


int
if_rs232_online( CAMP_IF* pIF )
{
    int fd;
    char port[LEN_STR+1];

    camp_getIfRs232Port( pIF->defn, port, sizeof( port ) );

    /*
     *  Open the port
     *  (Make sure open call does not block. We don't use blocking reads.)
     */ 
    if( ( fd = open( port, (O_RDWR|O_NONBLOCK), 0 ) ) < 0 )
    {
	_camp_setMsg( "failure opening port %s", port );
	goto failure;
    }

    if( if_rs232_settings( pIF, fd ) == CAMP_FAILURE ) 
    {
	goto failure;
    }

#if CAMP_IF_RS232_PERSISTENT
    pIF->priv = integerToPointer( fd );
#else
    pIF->priv = integerToPointer( 1 );
    close( fd );
#endif

    return( CAMP_SUCCESS );

failure:

    if ( fd >= 0 ) close( fd );
    pIF->priv = integerToPointer( 0 );
    return( CAMP_FAILURE );
}


int
if_rs232_offline( CAMP_IF* pIF )
{
#if CAMP_IF_RS232_PERSISTENT
     int fd = pointerToInteger( pIF->priv );
     if ( fd > 0 ) 
     {
         close( fd );
         pIF->priv = integerToPointer( 0 );
     }
#endif
    return( CAMP_SUCCESS );
}


/*
 *  if_rs232_read,  synchronous write, then read with timeout
 */
int
if_rs232_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    int fd = -1, nfds;
    char* cmd;
    int cmd_len;
    char term[8];
    int term_len;
    fd_set readfds;
    char port[LEN_IDENT+1];
    char* buf;
    int buf_len;
    char* bufBegin;
    char* eol;
    int* pRead_len;
    int nread;
    double timeout;
    timeval_t tvTimeout;
    timeval_t tvFlush = { 0, 0 }; /* zero wait for typeahead to clear */
//#if CAMP_DEBUG
    char dbuf[80];
//#endif /* CAMP_DEBUG */
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;

	pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked

        fd = pointerToInteger( pIF->priv );
	cmd = pReq->spec.REQ_SPEC_u.read.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
	bufBegin = buf = pReq->spec.REQ_SPEC_u.read.buf;
	buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;
	camp_if_termTokenToChars( camp_getIfRs232WriteTerm( pIF->defn, term, sizeof( term ) ), term, sizeof( term ), &term_len );
	timeout = pIF->timeout; // timeout = camp_getIfRs232Timeout( pIF->defn );
	tvTimeout = double_to_timeval( fabs(timeout) ); // tv.tv_sec = labs( timeout ); tv.tv_usec = 0;
	camp_getIfRs232Port( pIF->defn, port, sizeof( port ) );
	pRead_len = &pReq->spec.REQ_SPEC_u.read.read_len;
	*pRead_len = 0;

	/*
	 *  Open the port
	 */ 
        if (CAMP_IF_RS232_PERSISTENT)
        {
            fd = pointerToInteger( pIF->priv );
        }
        else
        {
            fd = open( port, O_RDWR, 0 );
        }
	if( fd <= 0 )
	{
	    _camp_setMsg( "failure opening port %s", port );
	    goto failure;
	}

	if( if_rs232_settings( pIF, fd ) == CAMP_FAILURE ) goto failure;

	/*
	 *  Flush typeahead if read timeout is greater than zero
	 */
	if( timeout > 0.0 )
	{
#ifdef FOR_TESTING
          /*
           * For testing, three ways of flushing buffers. (It turns out that none
	   * works immediately after opening a ttyUSB port)
           */
          if ( 0 ) {/* Flush with read; requires opening with O_NONBLOCK */
            for( nread = 1; nread > 0; )
            {
                nread = read( fd, buf, buf_len );
                if ( nread > 0 ) {
                    printf( "Flushed %d chars: [%.*s]\n", nread, nread, buf);
                }
                if ( ( nread == -1 ) && ( _camp_debug(CAMP_DEBUG_IF_RS232) ) )
                {
                  printf("RS232 flush gives error %d: ",errno);
                  switch (errno) {
                    case EAGAIN: printf("EAGAIN\n"); break;
                    case EBADF: printf("EBADF\n"); break;
                    case EFAULT: printf("EFAULT\n"); break;
                    case EINTR: printf("EINTR\n"); break;
                    case EINVAL: printf("EINVAL\n"); break;
                    case EIO: printf("EIO\n"); break;
                  }
                }
            }
          }
          if ( 0 ) {/* Flush with tcflush -- can use this if CAMP_IF_RS232_TERMIOS */
              tcflush(fd, TCIFLUSH);
          }
#endif // FOR_TESTING
            for(;;) 
            {
                FD_ZERO( &readfds );
                FD_SET( fd, &readfds );
		cleartimeval( &tvFlush );
                nfds = select( fd+1, &readfds, NULL, NULL, &tvFlush );
                if ( nfds <= 0 ) break;
		if( FD_ISSET( fd, &readfds ) )
		{
		    /*
		     *  This data is ignored, just using buf for space
		     */
		    nread = read( fd, buf, buf_len );
                    if( _camp_debug(CAMP_DEBUG_IF_RS232) ) 
                    {
                        if ( nread > 0 ) {
                          printf( "Flushed %d chars: [%.*s]\n", nread, nread, buf);
                        } else {
                          //printf( "Nothing read.\n");
                        }
                    }
                }
            }
	}
    
	/*
	 *  Write the command
	 */
	if( cmd_len > 0 )
	{
            int globalMutexLockCount = mutex_unlock_global_all();

            if( _camp_debug(CAMP_DEBUG_IF_RS232) ) 
            {
                camp_strncpy_num( dbuf, sizeof(dbuf), cmd, cmd_len );
	        //printf( "term_len = %d\n", term_len );
                if( term_len > 0 ) camp_snprintf( &dbuf[cmd_len], sizeof( dbuf )-cmd_len, "\\%03o", term[0] );
                if( term_len > 1 ) camp_snprintf( &dbuf[cmd_len+4], sizeof( dbuf )-(cmd_len+4), "\\%03o", term[1] );
                printf( "writing '%s' to port '%s'\n", dbuf, port );
            }

	    if( write( fd, cmd, cmd_len ) > -1 )
	    {
	      if( term_len > 0 ) (void)!write( fd, term, term_len ); // no warning
	    }

            mutex_lock_global_all( globalMutexLockCount );
	}

	buf[0] = '\0';

	camp_if_termTokenToChars( camp_getIfRs232ReadTerm( pIF->defn, term, sizeof( term ) ), term, sizeof( term ), &term_len );
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     *  Read with timeout
     */
    if( _camp_debug(CAMP_DEBUG_IF_RS232) ) printf("if_rs232_read: Get the response(s).\n");
    for( ;; )
    {
        FD_ZERO( &readfds );
        FD_SET( fd, &readfds );

	{
	  int globalMutexLockCount = mutex_unlock_global_all();

	  nfds = select( fd+1, &readfds, NULL, NULL, &tvTimeout );

	  /*
	   *  17-Dec-1996  TW  Check CAMP's own RPC timeout
	   *  16-Dec-1999  TW  reverse order - call srv_check_timeout within global lock
	   *                   This is important because there is non-reentrant code
	   *                   within srv_check_timeout (RPC library, etc.) 
	   *  21-Dec-1999  TW  srv_check_timeout no longer used
	   */
	  mutex_lock_global_all( globalMutexLockCount );
	}

	/* srv_check_timeout(); */
        if( _camp_debug(CAMP_DEBUG_IF_RS232) ) { printf("if_rs232_read: select returns %d... ",nfds); fflush(stdout); }

        switch( nfds )
	{
	case -1:
            /* 
             *  Error
             */
            if( errno == EINTR ) continue;
            _camp_setMsg( "select failure on port '%s'", port );
            goto failure;

	case 0:
            /* 
             *  Timeout. 
             *  Success if terminator "none" and buffer unflushed.(timeout <= 0)
             *  Success if terminator "none" and we got some characters.
             *  Timeout error otherwise.
             */
            if( _camp_debug(CAMP_DEBUG_IF_RS232) ) printf("\nTimeout!\n");
            if( term_len == 0 && (timeout <= 0.0 || *pRead_len > 0) ) goto success;
            _camp_setMsg( "timeout reading port '%s', got '%s'", port, bufBegin );
            goto failure;

	default:
            if( !FD_ISSET( fd, &readfds ) ) continue;
            /* 
             *  Something to read
             */
            
            if( buf_len <= 0 )
	    {
		if( _camp_debug(CAMP_DEBUG_IF_RS232) ) 
		{
		    printf( "something to read from port '%s' but buffer is full\n", port);
		}
                break;
	    }

	    {
	      int globalMutexLockCount = mutex_unlock_global_all();

              if( _camp_debug(CAMP_DEBUG_IF_RS232) ) { printf( "doing read... " ); fflush(stdout); }

	      nread = read( fd, buf, buf_len );

	      if( _camp_debug(CAMP_DEBUG_IF_RS232) ) printf( "done\n" );

	      mutex_lock_global_all( globalMutexLockCount );
	    }

	    if( _camp_debug(CAMP_DEBUG_IF_RS232) ) 
	    {
		camp_stoprint_expand( buf, dbuf, 64 );
                camp_strncpy_num( dbuf+60, sizeof(dbuf)-60, "...", 4 );
		printf( "read %d chars '%s' from port '%s'\n", nread, dbuf, port );
	    }

            buf += nread;
	    buf_len -= nread;
            *pRead_len += nread;
            if( buf_len > 0 ) buf[0] = '\0'; // 20140219  TW  check buf_len
            break; /* from switch */
	}

	if( buf_len <= 0 ) break; /* from for loop */

        /*
         *  Check if we have terminator yet
         *  15-Dec-1999  TW  If terminator == NONE, don't look for one!
	 *  24-Jan-1999  DA  When there is a terminator, terminate the string!
         */
        if( term_len > 0 )
	{
	    eol = strstr( bufBegin, term );
	    if( eol != NULL ) 
	    {
		*eol = '\0';
		*pRead_len = (int)(eol - bufBegin);
		/* ... or even ...
		 *pRead_len -= term_len;
		 */
		break; /* from for loop */
	    }
	}
    } /* end for(;;) loop */

 success:
    camp_status = CAMP_SUCCESS;
    goto finish;

 failure:
    camp_status = CAMP_FAILURE;
    goto finish;

 finish:
    if( (! CAMP_IF_RS232_PERSISTENT) && (fd > 0) ) close( fd );
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  if_rs232_write,  synchronous write 
 */
int 
if_rs232_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    int fd = -1;
    char* cmd;
    int cmd_len;
    char term[8]; /* Used for both descriptive "CRLF" and the actual characters */
    int term_len;
    char port[LEN_IDENT+1];
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;

	pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked

        // fd = pointerToInteger( pIF->priv );
	cmd = pReq->spec.REQ_SPEC_u.write.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;
	camp_if_termTokenToChars( camp_getIfRs232WriteTerm( pIF->defn, term, sizeof( term ) ), term, sizeof( term ), &term_len );
	camp_getIfRs232Port( pIF->defn, port, sizeof( port ) );

	/*
	 *  Open the port
	 */ 
        if ( CAMP_IF_RS232_PERSISTENT )
        {
            fd = pointerToInteger( pIF->priv );
        }
        else
        {
            fd = open( port, O_WRONLY, 0 );
        }
	if( fd <= 0 )
	{
	    _camp_setMsg( "failure opening port %s", port );
	    goto failure;
	}

	if( if_rs232_settings( pIF, fd ) == CAMP_FAILURE ) goto failure;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    {
      int globalMutexLockCount = mutex_unlock_global_all();

      if( write( fd, cmd, cmd_len ) > -1 )
	{
	  if( term_len > 0 ) (void)!write( fd, term, term_len ); // no warning
	}

      mutex_lock_global_all( globalMutexLockCount );
    }

    camp_status = CAMP_SUCCESS;
    goto finish;

 failure:
    camp_status = CAMP_FAILURE;
    goto finish;

 finish:
    if( (! CAMP_IF_RS232_PERSISTENT) && (fd > 0) ) close( fd );
    // _mutex_lock_end();
    return( camp_status );
}


#if defined( CAMP_IF_RS232_TERMIOS ) || defined( CAMP_IF_RS232_BSD )

#if defined( CAMP_IF_RS232_TERMIOS )
static speed_t
#elif defined( CAMP_IF_RS232_BSD )
static int
#endif
getBaud( int baud )
{
    switch( baud )
    {
    case 110: return( B110 );
    case 300: return( B300 );
    case 600: return( B600 );
    case 1200: return( B1200 );
    case 2400: return( B2400 );
    case 4800: return( B4800 );
    case 9600: return( B9600 );
    case 19200: return( B19200 );
    case 38400: return( B38400 );
    case 57600: return( B57600 );
    case 115200: return( B115200 );
    case 230400: return( B230400 );
    default: return( B9600 );
    }
}

static int
getCharSize( int size )
{
    switch( size )
    {
    case 5: return( CS5 );
    case 6: return( CS6 );
    case 7: return( CS7 );
    case 8: return( CS8 );
    default: return( CS8 );
    }
}
#endif // CAMP_IF_RS232_TERMIOS || CAMP_IF_RS232_BSD
