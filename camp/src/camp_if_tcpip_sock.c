/*
 *  Name:       camp_if_tcpip_sock.c
 *
 *  Purpose:    Provides TCP/IP socket communications interface type.
 *              Implementation using standard tcp sockets on vxworks, which
 *              will be much the same on other systems (not tried yet).
 *
 *              A CAMP tcpip interface definition must provide the following
 *              routines:
 *                int if_tcpip_init ( void );
 *                int if_tcpip_online ( CAMP_IF *pIF );
 *                int if_tcpip_offline ( CAMP_IF *pIF );
 *                int if_tcpip_read( REQ* pReq );
 *                int if_tcpip_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini.
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In this implementation, the global mutex is unlocked
 *              during calls to connectwithtimeout(), send(), select(),
 *              and recv().
 *
 *
 *  Notes:
 *
 *     Ini file configuration string:
 *        {}
 *
 *     Meaning of private (driver-specific) variables:
 *        pIF_t->priv - not used
 *        pIF->priv - socket descriptor
 *
 *  Revision history:
 *
 *    20140220     TW  camp_if_termTokenToChars
 *    20140219     TW  mutex_un/lock_global -> mutex_un/lock_global_all, change scheme
 *    20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *
 *   $Log: camp_if_tcpip_sock.c,v $
 *   Revision 1.9  2018/06/13 00:38:29  asnd
 *   Add detail to error report
 *
 *   Revision 1.8  2017/06/29 02:44:06  asnd
 *   Plug small memory leak
 *
 *   Revision 1.7  2016/10/02 00:06:58  asnd
 *   Optional persistent connection, flagged by negative timeout parameter.
 *
 *   Revision 1.6  2016/08/05 02:08:52  asnd
 *   Make tcpip interface flush characters when timeout parameter is negative.
 *
 *   Revision 1.5  2015/12/10 03:52:41  asnd
 *   Adjust comments
 *
 *   Revision 1.4  2015/04/21 03:47:14  asnd
 *   Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *   Revision 1.1  2006/04/27 04:08:23  asnd
 *   Create a tcpip socket interface type
 *
 *
 */

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#ifdef VXWORKS

#include "ioLibx.h"
#include "types.h"
#include "socket.h" 
#include "sockLib.h"
#include "inetLib.h" // inet_addr
#define  _IF_INET AF_INET

#else // !VXWORKS

#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#ifdef linux
#include <unistd.h> // close
//#include <sys/socket.h>
//#include <netinet/in.h>
#include <arpa/inet.h> // inet_addr
#endif // linux
#define  _IF_INET PF_INET
#define  ERROR  (-1)
#define  OK (0)

#endif // VXWORKS

#include "camp_srv.h" // "camp.h" // 20140226  TW

static int if_tcpip_connect( CAMP_IF* pIF );

/* 
 *  Number of chunks that a read may receive.  (According to the rules, this
 *  only needs to be 1, because the socket collects the full text before 
 *  triggering select().)
 */
#define MAX_RECV_TRY 3

/* 
 *  This structure defines a socket and associated address.  It is linked in 
 *  to pIf->priv.  Note that sockaddr_in is interchangeable with sockaddr 
 *  structure type, but has more detailed subdivisions.
 */

typedef struct socketDrv_str {
    int	 Fd;
    struct sockaddr_in SockAddr;
    struct timeval tv_timeout;
    bool_t keepCon;
} socketDrv_t;

typedef struct socketDrv_str *SOCKET_ID;


int
if_tcpip_init( void )
{
    /*
     *  Don't check anything yet.
     */
    return( CAMP_SUCCESS );
}

static int
if_tcpip_connect( CAMP_IF* pIF )
{
    int stat;
    // int yes = 1;
    char addr[LEN_NODENAME+1];
    int port;
    float timeout;
    socketDrv_t* pSock;
    int sockAddrSize = sizeof (struct sockaddr_in);

    if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "tcpip connect to %s", pIF->defn ); }
    pSock = (socketDrv_t*)pIF->priv;

    if( pSock->Fd > 0 ) 
    {
	if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "Socket was left open -- close" ); }
	close( pSock->Fd );
	pSock->Fd = 0;
    }

    camp_getIfTcpipAddr( pIF->defn, addr, sizeof( addr ) );
    port = camp_getIfTcpipPort( pIF->defn );
    if (pIF->timeout < 0) 
    {
        pSock->keepCon = TRUE;
    }
    else 
    {
        pSock->keepCon = FALSE;
    }
    /*
     *  Fill in the timeout parameter, rounded to 1/100 second
     */
    timeout = fabs(pIF->timeout);
    pSock->tv_timeout.tv_sec = (long int)timeout;
    pSock->tv_timeout.tv_usec = (long int)( 100.0 * (timeout - pSock->tv_timeout.tv_sec) ) * 10000;

    /*
     *  Fill in some connection address parameters
     */
    bzero ((char*)&pSock->SockAddr, sockAddrSize);
    pSock->SockAddr.sin_family = _IF_INET;
    pSock->SockAddr.sin_port = htons(port);
    pSock->SockAddr.sin_addr.s_addr = inet_addr(addr);

    /*
     *  Open the socket connection.
     */ 

    pSock->Fd = socket( _IF_INET, SOCK_STREAM, 0 );

    if (pSock->Fd == ERROR) 
    {
        if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "tcpip failed to create socket: %s", strerror(errno) ); }
	_camp_setMsg( "tcpip failed to create socket: %s", strerror(errno) );
	goto failure;
    }

    if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "Connect to sock %d with timeout %.1f", pSock->Fd, timeout ); }

#if CAMP_MULTITHREADED
    {
    int globalMutexLockCount = mutex_unlock_global_all();
#endif /* CAMP_MULTITHREADED */

#ifdef VXWORKS
    stat = connectWithTimeout(pSock->Fd, 
			      (struct sockaddr *)&pSock->SockAddr,
			      sockAddrSize,
			      &pSock->tv_timeout
	);
    /* Or maybe use a different timeout value for the connection */
#else
    /*
     * The socket connect() ignores timeouts in older Linux (SL6 etc)
     */
    setsockopt (pSock->Fd, SOL_SOCKET, SO_RCVTIMEO, 
		(void *)&(pSock->tv_timeout), sizeof(pSock->tv_timeout));
    setsockopt (pSock->Fd, SOL_SOCKET, SO_SNDTIMEO, 
		(void *)&(pSock->tv_timeout), sizeof(pSock->tv_timeout));

    stat = connect(pSock->Fd, 
		   (struct sockaddr *)&pSock->SockAddr,
		   sockAddrSize
	);
#endif

#if CAMP_MULTITHREADED
    mutex_lock_global_all( globalMutexLockCount );
    }
#endif /* CAMP_MULTITHREADED */

    if( stat != OK ) 
    {
	if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "Failed connection to sock %d: %d", pSock->Fd, stat ); }
	_camp_setMsg( "failure opening tcpip connection" );
	goto failure;
    }
    if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "Connected to sock %d with status %d", pSock->Fd, stat ); }

    /*
     *  If necessary...
     *  Set real-time operation -- no delays.
     *  Set blocking IO.
     */
    /*
     *  setsockopt (pSock->Fd, SOL_SOCKET, TCP_NODELAY, (char*)(&yes), sizeof(yes));
     *  stat = ioctl ( pSock->Fd, FIONBIO, (int)&yes );
     */

    return( CAMP_SUCCESS );

failure:
    if( pSock->Fd > 0 ) { close( pSock->Fd ); pSock->Fd = 0; }
    return( CAMP_FAILURE );

}


int
if_tcpip_online( CAMP_IF* pIF )
{
    socketDrv_t* pSock;

    /*
     *  set private data to socket descriptor
     */
    //printf("Connect tcpip interface; debug is %d (%d&& %d&%d)\n", _camp_debug(CAMP_DEBUG_IF_TCPIP), CAMP_DEBUG, camp_debug, CAMP_DEBUG_IF_TCPIP);
    pIF->priv = camp_malloc( sizeof( socketDrv_t ) );
    pSock = (socketDrv_t*)pIF->priv;
    pSock->Fd = 0;
    if( if_tcpip_connect( pIF ) == CAMP_FAILURE )
    {
        if( pSock->Fd > 0 ) { close( pSock->Fd ); pSock->Fd = 0; }
        return( CAMP_FAILURE );
    }
    if ( !pSock->keepCon ) { close( pSock->Fd ); pSock->Fd = 0; }
    return( CAMP_SUCCESS );
}


int
if_tcpip_offline( CAMP_IF* pIF )
{
    socketDrv_t* pSock;

    /*
     *  free socket descriptor in private data
     */

    pSock = (socketDrv_t*)pIF->priv;
  
    if( pSock->Fd > 0 ) 
    {
	close( pSock->Fd );
    }
    free( (void*)pSock );
    pIF->priv = NULL;

    return( CAMP_SUCCESS );
}


/*
 *  if_tcpip_read.  synchronous write, then read with timeout
 */
int
if_tcpip_read( REQ* pReq )
{
    char* command = NULL;
    char* cmd;
    int cmd_len;
    char term[8];
    int term_len;
    int stat;
    int j;
    int nfds;
    char* buf;
    int buf_len;
    char* bufBegin;
    char* eol;
    int* pRead_len;
    int nread;
    int avail;
    socketDrv_t* pSock;
    fd_set readfds;
    timeval_t tv;
    timeval_t tvFlush = { 0, 0 }; 

    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;

	pIF = pReq->spec.REQ_SPEC_u.read.pIF; // locked

	cmd = pReq->spec.REQ_SPEC_u.read.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
	buf = pReq->spec.REQ_SPEC_u.read.buf;
	bufBegin = buf;
	buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;
	pRead_len = &pReq->spec.REQ_SPEC_u.read.read_len;
	*pRead_len = 0;

	/*
	 *  Optionally open the connection and make settings
	 */ 
	pSock = (socketDrv_t*)pIF->priv;
        if( pSock->Fd <= 0 || !(pSock->keepCon) )
        {
            if( if_tcpip_connect( pIF ) == CAMP_FAILURE ) 
            {
                goto failure;
            }
        }
	/*
	 *  Flush incoming text.  If select triggers, but no text was read, this should
         *  indicate that the remote device has dropped the connection, so re-connect.
	 */
        for(;;)
        {
            FD_ZERO( &readfds );
            FD_SET( pSock->Fd, &readfds );
            nfds = select( pSock->Fd+1, &readfds, NULL, NULL, &tvFlush );
            if ( nfds < 0 ) {/* error */
              if( errno == EINTR ) continue;
              //printf ("Flushing select failure.\n");
              break;
            }
            else if (nfds == 0) {/* timeout */
              //printf ("Flushing select timed out.\n");
              break;
            }
            else {
              if( !FD_ISSET( pSock->Fd, &readfds ) ) {
                //printf ("Some other fd is ready.\n");
                continue;
              }
              nread = read( pSock->Fd, buf, buf_len );
              if ( nread == 0 ) { /* This may mean the connection was dropped! */
                //printf("Flushing select triggered, but no characters read.\n");
                if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "Flushing select triggered, but no characters read." ); }
                if(pSock->keepCon) {
                  if( if_tcpip_connect( pIF ) == CAMP_FAILURE ) {
                    goto failure;
                  }
                  continue;
                }
                break;
              }
              //printf( "Flushed %d chars: [%.*s]\n", nread, nread, buf);
              tvFlush = double_to_timeval( 0.0 );
            }
        }

	/*
	 *  Write the command
	 */
	camp_if_termTokenToChars( camp_getIfTcpipWriteTerm( pIF->defn, term, sizeof( term ) ), term, sizeof( term ), &term_len );
	if( _camp_debug(CAMP_DEBUG_IF_TCPIP) )
	{
	    char dbuf[512];
	    camp_strncpy_num( dbuf, sizeof( dbuf ), cmd, cmd_len );
	    /*_camp_log( "term_len = %d", term_len );*/
	    if( term_len > 0 ) camp_snprintf( &dbuf[cmd_len], sizeof( dbuf )-cmd_len, "\\%03o", term[0] );
	    if( term_len > 1 ) camp_snprintf( &dbuf[cmd_len+4], sizeof( dbuf )-(cmd_len+4), "\\%03o", term[1] );
	    _camp_log( "writing '%s'", dbuf );
	}

	if( cmd_len > 0 )
	{
	    size_t command_size = cmd_len + term_len + 1;
	    command = (char*)camp_malloc( command_size );
	    camp_strncpy_num( command, command_size, cmd, cmd_len );
	    camp_strncpy_num( &command[cmd_len], command_size-cmd_len, term, term_len );

#if CAMP_MULTITHREADED
            {
	    int globalMutexLockCount = mutex_unlock_global_all();
#endif /* CAMP_MULTITHREADED */

	    stat = send(pSock->Fd, command, cmd_len+term_len, 0);

#if CAMP_MULTITHREADED
	    mutex_lock_global_all( globalMutexLockCount );
	    }
#endif /* CAMP_MULTITHREADED */

	    if( stat == ERROR ) 
	    {
		_camp_setMsg( "failed to send request" );
		goto failure;
	    }

	    if( stat < cmd_len+term_len ) 
	    {
		_camp_setMsg( "sent request incomplete (%d of %d)", stat, cmd_len+term_len );
		goto failure;
	    }
	}

	/*
	 *  Read response with timeout
	 */
	buf[0] = '\0';

	camp_if_termTokenToChars( camp_getIfTcpipReadTerm( pIF->defn, term, sizeof( term ) ), term, sizeof( term ), &term_len );
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     *  select() for sockets of type SOCK_STREAM, supposedly checks that the
     *  virtual socket corresponding to the socket has been closed, so we
     *  shouldn't have to loop over fragments of the response--it all comes
     *  at once.  But there has been flakiness, particularly with recv()
     *  hanging after select() returns!  Therefore, we will double-check that
     *  there is some text to read.  The for() loop is limited to MAX_RECV_TRY
     *  iterations because the select() always indicates a readable condition after
     *  the (virtual) socket gets closed. 
     */

    nread = 0;

    for( j = 0 ; j < MAX_RECV_TRY ; j++ )
    {

        tv.tv_sec = pSock->tv_timeout.tv_sec;
        tv.tv_usec = pSock->tv_timeout.tv_usec;

        FD_ZERO( &readfds );
        FD_SET( pSock->Fd, &readfds );

#if CAMP_MULTITHREADED
        {
        int globalMutexLockCount = mutex_unlock_global_all();
#endif /* CAMP_MULTITHREADED */

        nfds = select( (pSock->Fd)+1, &readfds, NULL, NULL, &tv );

#if CAMP_MULTITHREADED
        mutex_lock_global_all( globalMutexLockCount );
	}
#endif /* CAMP_MULTITHREADED */

        if( nfds == -1 )
	{ /* Error */
            if( errno == EINTR ) continue;
            _camp_setMsg( "select failure" );
            goto failure;
	}
        if( nfds == 0 )
	{ /* Timeout */
	    if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "Timeout from select" ); }
            _camp_setMsg( "timeout during read; got '%s'", bufBegin );
            goto failure;
	}
        
        if( !FD_ISSET( pSock->Fd, &readfds ) ) continue;
        
        /*
         *  The socket's fd has been stimulated...
         */
        stat = ioctl( pSock->Fd, FIONREAD, &avail ); // (int)&avail ); // 20140221  TW  remove (int) cast, causing warning on vxworks
	if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "%d Socket has %d bytes available", j, avail ); }
        if( avail < 1 ) 
	{
            /*
             * There were actually no characters readable. 
             * If this is the last try and we have no characters at all, abort.
             */
            if ( j+1 == MAX_RECV_TRY && *pRead_len == 0 ) goto failure;
            /* 
             * else, wait a bit and retry. In practice, the retries don't seem to get 
             * any characters either.
             */
            camp_fsleep( 0.05 );
            continue;
	}
        
        if( buf_len == 0 )
	{
	    if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "something to read but buffer is full" ); }
            break;
	}
        
#if CAMP_MULTITHREADED
	{
        int globalMutexLockCount = mutex_unlock_global_all();
#endif /* CAMP_MULTITHREADED */

	if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "doing receive  %d...", j ); }
        
        nread = recv( pSock->Fd, buf, buf_len, 0 );
        
#if CAMP_MULTITHREADED
        mutex_lock_global_all( globalMutexLockCount );
	}
#endif /* CAMP_MULTITHREADED */
        
	if( _camp_debug(CAMP_DEBUG_IF_TCPIP) )
	{
	    char* dbuf;
	    dbuf = camp_stoprint_strdup( buf );
	    _camp_log( "received %d '%s'", nread, dbuf );
            _free( dbuf )
	}

        buf += nread;
        buf_len -= nread;
        *pRead_len += nread;
        buf[0] = '\0';
        
        if( buf_len == 0 ) break; /* buffer is full, exit from loop */

        /*
         *  Check if we have terminator yet.  If so, truncate string there.
         */
        if( term_len > 0 )
	{
            eol = strstr( bufBegin, term );
            if( eol != NULL )
	    {
                *eol = '\0';
                *pRead_len = (int)(eol - bufBegin);
	    }
            break; /* from for loop */
	}
        else 
	{
            /*
             * For the case of no terminator, accept the first chunk of text
             * and return.  Do *not* wait for a timeout as we do with rs232.
             */
            break; /* from for loop */
	}
        
    } /* end for(;;) loop */

    if( pSock->Fd > 0 && !(pSock->keepCon) ) { close( pSock->Fd ); pSock->Fd = 0; }
    _free( command );

    // _mutex_lock_end();
    return( CAMP_SUCCESS );

failure:
    /* 
     * On failure, always close socket, even if keepCon is TRUE.
     */
    if( pSock->Fd > 0 ) { close( pSock->Fd ); pSock->Fd = 0; }
    _free( command );
    // _mutex_lock_end();
    return( CAMP_FAILURE );
}


/*
 *  if_tcpip_write,  synchronous write 
 */
int 
if_tcpip_write( REQ* pReq )
{
    int i;
    char* cmd;
    int cmd_len;
    char term[8];
    int term_len;
    char* command = NULL;
    int stat;
    socketDrv_t* pSock;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;

	pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked

	cmd = pReq->spec.REQ_SPEC_u.write.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

	pSock = (socketDrv_t*)pIF->priv;

        if( pSock->Fd <= 0 || !(pSock->keepCon) )
        {
            if( if_tcpip_connect( pIF ) == CAMP_FAILURE ) 
            {
                // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
                goto failure;
            }
        }

	camp_if_termTokenToChars( camp_getIfTcpipWriteTerm( pIF->defn, term, sizeof( term ) ), term, sizeof( term ), &term_len );
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    if( _camp_debug(CAMP_DEBUG_IF_TCPIP) )
    {
	char dbuf[512];
	camp_strncpy_num( dbuf, sizeof( dbuf ), cmd, cmd_len );
	_camp_log( "term_len = %d", term_len );
	if( term_len > 0 ) camp_snprintf( &dbuf[cmd_len], sizeof( dbuf )-cmd_len, "\\%03o", term[0] );
	if( term_len > 1 ) camp_snprintf( &dbuf[cmd_len+4], sizeof( dbuf )-(cmd_len+4), "\\%03o", term[1] );
	_camp_log( "writing '%s'", dbuf );
    }
    
    if( cmd_len > 0 )
    {
	size_t command_size = cmd_len + term_len + 1;
        command = (char*)camp_malloc( command_size );
        camp_strncpy_num( command, command_size, cmd, cmd_len ); 
        camp_strncpy_num( &command[cmd_len], command_size-cmd_len, term, term_len ); 

        for ( i=0; i<2; i++) /* Try twice to facilitate a re-opening */
        {
            if( pSock->Fd <= 0 )
            {
                CAMP_IF* pIF;
                pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked
                if( if_tcpip_connect( pIF ) == CAMP_FAILURE ) 
                {
                    goto failure;
                }
            }

#if CAMP_MULTITHREADED
            {
            int globalMutexLockCount = mutex_unlock_global_all();
#endif /* CAMP_MULTITHREADED */

            stat = send(pSock->Fd, command, cmd_len+term_len, 0);

#if CAMP_MULTITHREADED
            mutex_lock_global_all( globalMutexLockCount );
            }
#endif /* CAMP_MULTITHREADED */

            if( pSock->keepCon && (stat == ERROR || stat < cmd_len+term_len) ) 
            { /* after failure, close socket, so as to re-connect on second try */
              if( _camp_debug(CAMP_DEBUG_IF_TCPIP) ) { _camp_log( "Re-open connection for write" ); }
              close( pSock->Fd );
              pSock->Fd = 0; 
            }
            else
            {
              break;
            }
        }

        if( stat == ERROR ) 
	{
	    _camp_setMsg( "failed write" );
	    goto failure;
        }

        if( stat < cmd_len+term_len ) 
	{
	    _camp_setMsg( "incomplete write (%d of %d)", stat, cmd_len+term_len );
	    goto failure;
        }

    }

    if( pSock->Fd > 0 && !(pSock->keepCon) ) { close( pSock->Fd ); pSock->Fd = 0; }
    _free( command );
    // _mutex_lock_end();
    return( CAMP_SUCCESS );

failure:

    if( pSock->Fd > 0 ) { close( pSock->Fd ); pSock->Fd = 0; }
    _free( command );
    // _mutex_lock_end();
    return( CAMP_FAILURE );
}
