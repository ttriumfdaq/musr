/*
 *	$Id: camp_if_utils.c,v 1.10 2015/04/21 03:47:14 asnd Exp $
 *
 *	$Revision: 1.10 $
 *
 *
 *  Purpose:     Routines for accessing interface type information in the
 *               CAMP database, either in general or specific to an
 *               interface type.
 *
 *  Called by:   camp_cui_*.c, camp_if_*.c, camp_tcl.c
 *
 *  Revision history:
 *
 *    20140220  TW  camp_if_termTokenToChars
 * 
 *  $Log: camp_if_utils.c,v $
 *  Revision 1.10  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.7  2006/04/27 04:08:23  asnd
 *  Create a tcpip socket interface type
 *
 *  Revision 1.6  2004/01/28 03:18:28  asnd
 *  Add VME interface type.
 *
 *  Revision 1.5  2001/01/17 04:16:10  asnd
 *  Debug rs232PortInUse error message
 *
 *  Revision 1.4  2001/01/15 06:41:28  asnd
 *  Error message for rs232PortInUse.
 *
 *  Revision 1.3  2000/12/23 02:37:50  ted
 *  camp_getIfIndpakChan -> camp_getIfIndpakChannel for consistency (different in some places than others)
 *
 *  Revision 1.2  2000/12/22 22:34:57  David.Morris
 *  Added INDPAK parameter access functions and prototypes. Added CAMP_IF_TYPE_INDPAK
 *  to instrument type list
 *
 *
 */

#include <stdlib.h>
#include <string.h>
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */


/*
 *  Name:        camp_ifGetpIF_t
 *
 *  Purpose:     Return pointer to interface type structure based on
 *               interface type identifier (string)
 *
 *               Note that interface types are maintained in the CAMP
 *               server, independent of what instruments are present.
 *
 *  Inputs:      Interface type identifier (string) (e.g., rs232, gpib).
 *
 *  Outputs:     Pointer to interface type structure.
 *
 */
CAMP_IF_t* 
camp_ifGetpIF_t( const char* ident /* , bool_t _mutex_lock_sys_check */ )
{
    CAMP_IF_t* pIF_t = NULL;
    CAMP_IF_t* pIF_t_found = NULL;

    for( pIF_t = pSys->pIFTypes; 
	 pIF_t != NULL; 
	 pIF_t = pIF_t->pNext ) 
    {
	if( streq( ident, pIF_t->ident ) )
        {
	    pIF_t_found = pIF_t;
	    break;
	}
    }
    
    return( pIF_t_found );
}

CAMP_IF_t* camp_ifGetpIF_t_clnt( const char* ident ) { return camp_ifGetpIF_t( ident /* , TRUE */ ); }


/*
 *  Name:           camp_getIf*
 *
 *  Purpose:        The following routines are specific to an interface
 *                  type (RS232, GPIB,...).  They retrieve individual
 *                  strings from the interface definition, which is also
 *                  a string.
 *
 *                  Each instance of a CAMP instrument has an associated
 *                  interface (RS232, GPIB,...) and an interface definition.
 *                  The interface definition is a string that includes all
 *                  the parameters necessary to define an instance of the
 *                  interface, separated by spaces.  Interface definition
 *                  strings for current defined interface types are as follows:
 *
 *                  RS232:    "<port> <baud> <databits> <parity> <stopbits>
 *                             <readTerminator> <writeTerminator>"
 *
 *                  GPIB:     "<address> <readTerminator> <writeTerminator>"
 *
 *                  CAMAC:    "<Branch> <Crate> <Num>"
 *
 *                  INDPAK:   "<slot> <type> <channel>"
 *
 *                  VME:      "<base>"
 *
 *                  TCPIP:    "<ipAddress> <portNumber> <readTerminator> 
 *                             <writeTerminator>"
 *
 *                  The following routines are used in many parts of CAMP
 *                  to extract each of the parameters from the definition
 *                  string.
 *
 *  Called by:      camp_cui*.c, camp_if_*.c, camp_tcl.c
 * 
 *  Inputs:         Instrument interface definition string.
 *
 *  Preconditions:  An instrument's interface has been defined.
 *
 *  Outputs:        The parameter requested in either string or numeric
 *                  format as appropriate.
 *
 *  Postconditions:
 *
 *  Revision history:
 *
 */


/*
 *                  RS232:    "<port> <baud> <databits> <parity> <stopbits>
 *                             <readTerminator> <writeTerminator>"
 */
char*
camp_getIfRs232Port( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 0, str, str_size );
    return str;
}


int
camp_getIfRs232Baud( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 1, str, sizeof( str ) );
    return atol( str );
}


int
camp_getIfRs232Data( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 2, str, sizeof( str ) );
    return atol( str );
}


char*
camp_getIfRs232Parity( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 3, str, str_size );
    return str;
}


int
camp_getIfRs232Stop( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 4, str, sizeof( str ) );
    return atol( str );
}


char*
camp_getIfRs232ReadTerm( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 5, str, str_size );
    return str;
}


char*
camp_getIfRs232WriteTerm( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 6, str, str_size );
    return str;
}


/* int */
/* camp_getIfRs232Timeout( const char* defn ) */
/* { */
/*     char str[LEN_IDENT+1]; */
/*     camp_stringGetToken( defn, ' ', 7, str, sizeof( str ) ); */
/*     return atol( str ); */
/* } */


/*
 *                  GPIB:     "<address> <readTerminator> <writeTerminator>"
 */

int
camp_getIfGpibAddr( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 0, str, sizeof( str ) );
    return atol( str );
}


char*
camp_getIfGpibReadTerm( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 1, str, str_size );
    return str;
}


char*
camp_getIfGpibWriteTerm( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 2, str, str_size );
    return str;
}


int
camp_getIfGpibMscbAddr( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 3, str, sizeof( str ) );
    return atol( str );
}


/*
 *                  CAMAC:    "<Branch> <Crate> <Num>"
 */

int
camp_getIfCamacB( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 0, str, sizeof( str ) );
    return atol( str );
}


int
camp_getIfCamacC( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 1, str, sizeof( str ) );
    return atol( str );
}


int
camp_getIfCamacN( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 2, str, sizeof( str ) );
    return atol( str );
}


/*
 *                  INDPAK:   "<slot> <type> <channel>"
 */

int
camp_getIfIndpakSlot( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 0, str, sizeof( str ) );
    return atol( str );
}


char*
camp_getIfIndpakType( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 1, str, str_size );
    return str;
}


int
camp_getIfIndpakChannel( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 2, str, sizeof( str ) );
    return atol( str );
}


/*
 *                  VME:      "<base>"
 */

/*
 *  20140221  TW  pedantic: return uintptr_t: integer guaranteed to be size of pointer
 */
uintptr_t
camp_getIfVmeBase( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 0, str, sizeof( str ) );
    return atol( str );
}


/*
 *                  TCPIP:    "<ipAddress> <portNumber> <readTerminator> 
 *                             <writeTerminator>"
 */

char*
camp_getIfTcpipAddr( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 0, str, str_size );
    return str;
}


int
camp_getIfTcpipPort( const char* defn )
{
    char str[LEN_IDENT+1];
    camp_stringGetToken( defn, ' ', 1, str, sizeof( str ) );
    return atol( str );
}


char*
camp_getIfTcpipReadTerm( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 2, str, str_size );
    return str;
}


char*
camp_getIfTcpipWriteTerm( const char* defn, char* str, size_t str_size )
{
    camp_stringGetToken( defn, ' ', 3, str, str_size );
    return str;
}


/* float */
/* camp_getIfTcpipTimeout( const char* defn ) */
/* { */
/*     char str[LEN_IDENT+1]; */
/*     camp_stringGetToken( defn, ' ', 4, str, sizeof( str ) ); */
/*     return (float)atof( str ); */
/* } */


void
camp_if_termTokenToChars( const char* term_in, char* term_out, size_t term_out_size, int* term_len )
{
    TOKEN tok;

    findToken( term_in, &tok );

    switch( tok.kind )
    {
        case TOK_LF:   camp_strncpy( term_out, term_out_size, "\012" );     *term_len = 1; return;
        case TOK_CR:   camp_strncpy( term_out, term_out_size, "\015" );     *term_len = 1; return;
        case TOK_CRLF: camp_strncpy( term_out, term_out_size, "\015\012" ); *term_len = 2; return;
        case TOK_LFCR: camp_strncpy( term_out, term_out_size, "\012\015" ); *term_len = 2; return;
        case TOK_NONE: camp_strncpy( term_out, term_out_size, "" );         *term_len = 0; return;
        default:       camp_strncpy( term_out, term_out_size, "" );         *term_len = 0; return;
    }
}

