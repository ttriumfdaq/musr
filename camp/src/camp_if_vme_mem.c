/*
 *  Name:       camp_if_vme_mem.c
 *
 *  Purpose:    Provides a VME interface type.
 *
 *  Applicability:
 *              These routines could be generic, in that they use VME access 
 *              functions (vmeRead, vmeWrite) that should be portable, but all 
 *              that is guaranteed is that they work for a vxworks system in 
 *              a vme crate.  It is intended to work on any systems where the
 *              VME bus addresses are mapped to the computer address space
 *              (may need defining A16).  A system with a remote vme crate is
 *              likely to require more/different interface parameters, a real
 *              "online" initialization, etc. 
 *
 *  Description:
 *              A CAMP "vme" interface definition must provide the following
 *              routines:
 *                int if_vme_init ( void );
 *                int if_vme_online ( CAMP_IF *pIF );
 *                int if_vme_offline ( CAMP_IF *pIF );
 *                int if_vme_read( REQ* pReq );
 *                int if_vme_write( REQ* pReq );
 * 
 *              The access model of sending and receiving character strings
 *              which is good for RS232 devices is no good for VME devices,
 *              where access is to specific register addresses.  Nevertheless,
 *              the routines if_vme_write and if_vme_read supplied here take
 *              parameters encoded in the "prompt" (or "command") string used
 *              by other interfaces.
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              Read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *              In this implementation of a CAMP VME interface, the global
 *              locks are not removed because the VME IO is a quick memory
 *              access.
 *
 *  
 *  $Log: camp_if_vme_mem.c,v $
 *  Revision 1.4  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.1  2004/01/28 03:21:43  asnd
 *  Add VME interface type.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> // isspace
#include "camp_srv.h"
#include "camp_vme_mem.h"

int
if_vme_init( void )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( "vme" /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    camp_status = CAMP_FAILURE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     *  Interpret interface configuration info here
     *  (none for generic).
     */

return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_vme_online( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_vme_offline( CAMP_IF* pIF )
{
    int camp_status = CAMP_SUCCESS;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;

	pIF_t = camp_ifGetpIF_t( pIF->typeIdent /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL ) 
	{
	    camp_status = CAMP_INVAL_IF_TYPE; goto return_;
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

return_:
    // _mutex_lock_end();
    return( camp_status );
}


/* 
 * convert a read request [insIfRead /ins "$addr int" 10] to act like
 * the call [vmeRead $baseAddr $addr int 10]
 */

int
if_vme_read( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    VME_IO_CODE stat;
    char* buf;
    int   buf_len;
    int*  pRead_len;
    char  dattype[16];
    void* base_addr;
    char* cmd;
    //int   cmd_len;
    char* endptr;
    int   offset;
    char  synt[] = {"; require <addr> <type>" };
    int i;
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;

	pIF =       pReq->spec.REQ_SPEC_u.read.pIF; // locked
	cmd =       pReq->spec.REQ_SPEC_u.read.cmd;
	//cmd_len =   pReq->spec.REQ_SPEC_u.read.cmd_len;
	buf =       pReq->spec.REQ_SPEC_u.read.buf;
	buf_len =   pReq->spec.REQ_SPEC_u.read.buf_len; // 20140224  TW  note: buf_len includes terminator
	pRead_len = &pReq->spec.REQ_SPEC_u.read.read_len;
	base_addr = integerToPointer( camp_getIfVmeBase( pIF->defn ) ); // 20140221  TW  pedantic
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    *pRead_len = 0;

    offset = (int) strtol( cmd, &endptr, 0 );
    if( endptr == cmd ) 
    {
	_camp_setMsg( "Failed VME read: invalid address in \"%s\"%s", cmd, synt );
	camp_status = CAMP_FAILURE; goto return_;
    }
    i = sscanf( endptr, "%15s", dattype);
    if( sscanf( endptr, "%15s", dattype) != 1 ) 
    {
	_camp_setMsg( "Failed VME read: %d invalid data type \"%s\" in \"%s\"%s", i, endptr, cmd, synt );
	camp_status = CAMP_FAILURE; goto return_;
    }

    /* printf("About to try vmeRead with %x + %x\n", base_addr, offset); */
    stat = vmeRead( base_addr, offset, (int*)dattype, buf, buf_len ); // 20140224  TW  note: buf_len includes terminator

    switch (stat) 
    {
    case VME_IO_UNKNOWN:
	_camp_setMsg( "Failed VME read: unknown data type \"%s\"", dattype );
	camp_status = CAMP_FAILURE; goto return_;
    case VME_IO_OVERFLOW:
	_camp_setMsg( "Failed VME read: data overflows buffer (%d)", buf_len );
	camp_status = CAMP_FAILURE; goto return_;
    case VME_IO_ACCESS_ERR:
	_camp_setMsg( "Failed VME read: access violation (bad address)" );
	camp_status = CAMP_FAILURE; goto return_;
    default:
	buf[buf_len-1] = '\0';
	*pRead_len = strlen( buf );
	break;
    }

return_:
    // _mutex_lock_end();
    return( camp_status );
}


int
if_vme_write( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    VME_IO_CODE stat;
    char* cmd;
    int   cmd_len, val_len;
    char  dattype[16];
    char  prevalstr[4]; /* preliminary scan of value string */
    void* base_addr;
    char* endptr;
    int   offset;
    char  synt[33] = {"; require <addr> <type> <value>" };
    // _mutex_lock_begin();

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF* pIF;

	pIF = pReq->spec.REQ_SPEC_u.write.pIF; // locked
	cmd = pReq->spec.REQ_SPEC_u.write.cmd;
	cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;
	base_addr = integerToPointer( camp_getIfVmeBase( pIF->defn ) ); // 20140221  TW  pedantic
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /* printf("insIfWrite with vme write data /%s/\n",cmd); */

    offset = (int) strtol( cmd, &endptr, 0 );
    if( endptr == cmd ) 
    {
	_camp_setMsg( "Failed VME write: invalid address in \"%s\"%s", cmd, synt );
	camp_status = CAMP_FAILURE; goto return_;
    }
  
    switch ( sscanf( endptr, "%15s%2s", dattype, prevalstr) ) 
    {
    case 0:
	/* printf("Failed to scan /%s/: 0 results\n",endptr); */
	_camp_setMsg( "Failed VME write: invalid data type in \"%s\"%s", cmd, synt );
	camp_status = CAMP_FAILURE; goto return_;
    case 1:
	/* printf("Failed to scan /%s/: 1 result: %s\n",endptr,dattype); */
	_camp_setMsg( "Failed VME write: missing value in \"%s\"%s", cmd, synt );
	camp_status = CAMP_FAILURE; goto return_;
    default:
	break;
	/* printf("Scanned /%s/: 2 results: /%s/ and /%.2s.../\n",endptr,dattype,prevalstr);*/ 
    }
    /*
     * Now locate beginning of value string, and decide its length.
     */
    while( isspace(*endptr) ) endptr++ ;
    endptr += strlen(dattype);
    while( isspace(*endptr) ) endptr++ ;
    val_len = cmd_len - (int)(endptr-cmd);

    /*
     * Perform the write
     */
    stat = vmeWrite( base_addr, offset, (int*)dattype, endptr, val_len );

    switch (stat) 
    {
    case VME_IO_UNKNOWN:
	_camp_setMsg( "Failed VME write: unknown data type \"%s\"", dattype );
	camp_status = CAMP_FAILURE; goto return_;
    case VME_IO_OVERFLOW:
	_camp_setMsg( "Failed VME write: numeric overflow (%s)", dattype );
	camp_status = CAMP_FAILURE; goto return_;
    case VME_IO_INVAL_NUM:
	_camp_setMsg( "Failed VME write: invalid numeric value" );
	camp_status = CAMP_FAILURE; goto return_;
    case VME_IO_ACCESS_ERR:
	_camp_setMsg( "Failed VME write: access violation (bad address)" );
	camp_status = CAMP_FAILURE; goto return_;
    default:
	break;
    }

return_:
    // _mutex_lock_end();
    return( camp_status );
}

