/*
 *  Name:       camp_ins_tcl.c
 *
 *  Purpose:    Routines to implement a Tcl instrument driver
 *
 *              Note that Tcl instrument drivers are the only instrument
 *              drivers implemented in CAMP.  An alternative could be
 *              compiled instrument drivers.  See also the note in the file
 *              camp_ins_priv.c.
 *
 *  Called by:  camp_ins_priv.c
 *
 *  Inputs:     Pointer to a CAMP instrument's variable structure
 *              (plus possibly more parameters)
 *
 *  Preconditions:
 *              Instrument types are added in camp_tcl_sys using the
 *              command sysAddInsType at server startup.  Instruments are
 *              then added as one of those types.  Tcl instrument drivers
 *              have parameters to the CAMP_INSTRUMENT definition that
 *              define various proc's to be executed for various states of
 *              the instrument.  The proc's are called as follows:
 *
 *                 ins_tcl_init      calls instrument's  -initProc 
 *                 ins_tcl_delete    calls instrument's  -deleteProc 
 *                 ins_tcl_online    calls instrument's  -onlineProc 
 *                 ins_tcl_offline   calls instrument's  -offlineProc 
 *
 *              Tcl instrument drivers also have variable definitions.
 *              Each variable has an associated "-readProc" and "-writeProc".
 *              The proc's are called as follows:
 *
 *                 ins_tcl_set       calls variable's  -writeProc
 *                 ins_tcl_read      calls variable's  -readProc
 *
 *              These procedures are evaluated in the instrument's own Tcl
 *              interpreter.
 *
 *  Outputs:    CAMP status
 *
 *  Revision history:
 *    20140217   TW     rename get/release_ins_thread_lock -> mutex_un/lock_ins
 *    Dec 2004   DJA    Get instrument mutex lock when using
 *                      instrument's Tcl interp.
 *                      - 20140219 TW I think these locks were already on at a
 *                        higher level (camp_srv_proc) but no problem being
 *                        safe, since mutexes are recursive
 *
 *  $Log: camp_ins_tcl.c,v $
 *  Revision 1.5  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.2  2004/12/18 01:04:37  asnd
 *  Request mutex when switching Tcl interpreter -- fix server crashes.
 *
 */

#include <stdio.h>
#include <string.h>
#include "camp_srv.h"


int
ins_tcl_init( CAMP_VAR* pVar )
{
    int tcl_status = TCL_OK;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    if( ( pIns->initProc == NULL ) || ( pIns->initProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );

    tcl_status = Tcl_VarEval( pIns->interp, 
			 pIns->initProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );

    if( tcl_status == TCL_ERROR )
    {
	char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

	assemble_tcl_message( pIns->interp, tcl_message, MAX_LEN_MSG+1 );

	camp_setMsg( "%s", tcl_message );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed initProc (ins='%s')", pVar->core.ident ); }

	free( tcl_message );
    }

    set_thread_interp( oldInterp );

    if( tcl_status == TCL_ERROR )
    {
	return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
ins_tcl_delete( CAMP_VAR* pVar )
{
    int tcl_status = TCL_OK;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    if( ( pIns->deleteProc == NULL ) || ( pIns->deleteProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
    mutex_lock_ins( pVar, 1 );

    tcl_status = Tcl_VarEval( pIns->interp, 
			 pIns->deleteProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );

    if( tcl_status == TCL_ERROR )
    {
	char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

	assemble_tcl_message( pIns->interp, tcl_message, MAX_LEN_MSG+1 );

	camp_setMsg( "%s", tcl_message );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed deleteProc (ins='%s')", pVar->core.ident ); }

	free( tcl_message );
    }

    mutex_lock_ins( pVar, 0 );
    set_thread_interp( oldInterp );

    if( tcl_status == TCL_ERROR )
    {
	return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
ins_tcl_online( CAMP_VAR* pVar )
{
    int tcl_status = TCL_OK;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    if( ( pIns->onlineProc == NULL ) || ( pIns->onlineProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
    mutex_lock_ins( pVar, 1 );

    tcl_status = Tcl_VarEval( pIns->interp, 
			 pIns->onlineProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );

    if( tcl_status == TCL_ERROR )
    {
	char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

	assemble_tcl_message( pIns->interp, tcl_message, MAX_LEN_MSG+1 );

	camp_setMsg( "%s", tcl_message );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed onlineProc (ins='%s')", pVar->core.ident ); }

	free( tcl_message );
    }

    mutex_lock_ins( pVar, 0 );
    set_thread_interp( oldInterp );

    if( tcl_status == TCL_ERROR )
    {
	return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
ins_tcl_offline( CAMP_VAR* pVar )
{
    int tcl_status = TCL_OK;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

    if( ( pIns->offlineProc == NULL ) || ( pIns->offlineProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
    mutex_lock_ins( pVar, 1 );

    tcl_status = Tcl_VarEval( pIns->interp, 
			 pIns->offlineProc,
			 " {",
			 pVar->core.ident,
                         "}",
			 NULL );

    if( tcl_status == TCL_ERROR )
    {
	char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

	assemble_tcl_message( pIns->interp, tcl_message, MAX_LEN_MSG+1 );

	camp_setMsg( "%s", tcl_message );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed offlineProc (ins='%s')", pVar->core.ident ); }

	free( tcl_message );
    }

    mutex_lock_ins( pVar, 0 );
    set_thread_interp( oldInterp );

    if( tcl_status == TCL_ERROR )
    {
	return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  Name:       ins_tcl_set
 *
 *  Purpose:    Call a variables -writeProc
 *
 *  Input:      pVar_ins - the variable's instrument variable structure
 *              pVar    - the structure of the variable to set
 *              pSpec   - the CAMP_VAR_SPEC structure identifying the new value
 *                        of the variable.  CAMP_VAR_SPEC will be interpreted
 *                        differently according to the variable type.
 *
 */
int
ins_tcl_set( CAMP_VAR* pVar_ins, CAMP_VAR* pVar, CAMP_VAR_SPEC* pSpec )
{
    int tcl_status = TCL_OK;
    CAMP_VAR setVar;
    char val[LEN_STRING+1];
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    if( ( pVar->core.writeProc == NULL ) || 
        ( pVar->core.writeProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    /*
     *  This section gets the value as a string from the pSpec
     *  structure.  Selection variable values can be specified as
     *  either a label or an index.
     */
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
    case CAMP_VAR_TYPE_NUMERIC:
	bcopy( (void*)pVar, (void*)&setVar, sizeof( CAMP_VAR ) );
	bcopy( (void*)pSpec, (void*)&setVar.spec, sizeof( CAMP_VAR_SPEC ) );
	varNumGetValStr( &setVar, val, sizeof( val ) );
	break;
    case CAMP_VAR_TYPE_STRING:
	camp_strncpy( val, sizeof( val ), pSpec->CAMP_VAR_SPEC_u.pStr->val ); // strcpy( val, pSpec->CAMP_VAR_SPEC_u.pStr->val );
	break;
    case CAMP_VAR_TYPE_SELECTION:
	if( pSpec->CAMP_VAR_SPEC_u.pSel->pItems != NULL )
	{
	    if( pSpec->CAMP_VAR_SPEC_u.pSel->pItems->label == NULL )
	    {
		_camp_setMsg( "error: missing selection label '%s'", pVar->core.path );
		return( CAMP_FAILURE );
	    }
	    camp_strncpy( val, sizeof( val ), pSpec->CAMP_VAR_SPEC_u.pSel->pItems->label ); // strcpy( val, pSpec->CAMP_VAR_SPEC_u.pSel->pItems->label );
	}
	else
	{
	    camp_snprintf( val, sizeof( val ), "%d", pSpec->CAMP_VAR_SPEC_u.pSel->val );
	}
	break;
    }
    
    pIns = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns;

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
    mutex_lock_ins( pVar_ins, 1 );

    tcl_status = Tcl_VarEval( pIns->interp, 
                          pVar->core.writeProc, 
			  " {",
                          pVar_ins->core.ident, 
			  "} {",
                          val, 
                          "}",
                          NULL );

    if( tcl_status == TCL_ERROR )
    {
	char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

	assemble_tcl_message( pIns->interp, tcl_message, MAX_LEN_MSG+1 );

	camp_setMsg( "%s", tcl_message );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed writeProc (var='%s')", pVar->core.path ); }

	free( tcl_message );
    }

    mutex_lock_ins( pVar_ins, 0 );
    set_thread_interp( oldInterp );

    if( tcl_status == TCL_ERROR )
    {
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  Name:       ins_tcl_read
 *
 *  Purpose:    Call a variables -readProc
 *
 *  Input:      pVar_ins - the variable's instrument variable structure
 *              pVar    - the structure of the variable to read
 *
 */
int
ins_tcl_read( CAMP_VAR* pVar_ins, CAMP_VAR* pVar )
{
    int tcl_status = TCL_OK;
    CAMP_INSTRUMENT* pIns;
    Tcl_Interp* oldInterp;

    if( ( pVar->core.readProc == NULL ) || 
        ( pVar->core.readProc[0] == '\0' ) )
    {
        return( CAMP_SUCCESS );
    }

    pIns = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns;

    oldInterp = get_thread_interp();
    set_thread_interp( pIns->interp );
    mutex_lock_ins( pVar_ins, 1 );

    tcl_status = Tcl_VarEval( pIns->interp, 
                          pVar->core.readProc, 
                          " {",
                          pVar_ins->core.ident, 
                          "}",
                          NULL );

    if( tcl_status == TCL_ERROR )
    {
	char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

	assemble_tcl_message( pIns->interp, tcl_message, MAX_LEN_MSG+1 );

	camp_setMsg( "%s", tcl_message );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed readProc (var='%s')", pVar->core.path ); }

	free( tcl_message );
    }

    mutex_lock_ins( pVar_ins, 0 );
    set_thread_interp( oldInterp );

    if( tcl_status == TCL_ERROR )
    {
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}

