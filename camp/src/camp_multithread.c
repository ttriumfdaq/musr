/*
 *  Tcl interpreters and multithreading: 
 *
 *   o  I believe Tcl to be reentrant, but must keep Tcl
 *      interpreters independant between threads (no two
 *      threads using the same interpreter at the same time).
 *
 *   o  Tcl interpreters are used by the following:
 *      1. sysLoad at startup (camp_srv_main.c)
 *      2. RPC requests sysUpdate (camp_sys_priv.c), sysLoad, insAdd, 
 *         insLoad and command strings (camp_srv_proc.c)
 *      3. instrument initProc, deleteProc, onlineProc, offlineProc
 *         and variable readProc, writeProc
 *      4. when alarms go on/off (camp_alarm_priv.c)
 *      5. CAMAC read/write (camp_if_camac_*.c)
 *  
 *   1.  sysLoad at startup (camp_srv_main.c)
 *
 *      Safe.  Only one thread at this point, use main interp.
 *
 *      USE main interp.
 *
 *   2.  RPC requests sysUpdate (camp_sys_priv.c), sysLoad, insAdd, 
 *       insLoad and Tcl command strings (camp_srv_proc.c)
 *     
 *      Trouble.  All but insLoad unrelated to a single existing 
 *      instrument.  So, use main interp with global lock on.  Have
 *      a new flag "global_mutex_noChange" to make sure that the 
 *      global mutex is not turned off inside any of these.  If the
 *      global mutex was not held on in this way, another thread
 *      might use it before the current has finished its Tcl_Eval.
 *      This would confuse the context of the interpreter.
 *
 *      20140219  TW  This was changed in 1999 or 2000,
 *      global_mutex_noChange was replaced with a mutex for the 
 *      main interpreter.
 *
 *      Side effects:  Tcl commands sent to CAMP will block the
 *      server.
 *
 *      USE main interp for sysUpdate, sysLoad, Tcl command,
 *        with mainInterp mutex on
 *      USE main interp for insAdd for the instrument definition
 *        line (first line) only.  Interpret the rest using the
 *        newly created instrument interp.  It is necessary to
 *        have all instrument-specific procedures defined in the
 *        interp for that instrument.
 *      USE instrument interp for insLoad.
 *
 *   3.  Instrument initProc, deleteProc, onlineProc, offlineProc
 *       and variable readProc, writeProc
 *
 *      Changed  16-Dec-2004  DJA
 *      Have separate interp for each instrument.  Use instrument 
 *      mutexes (get_ins_thread_lock) to ensure only one thread can
 *      access an instrument (and its interp) at the same time.
 *
 *      USE instrument interp, blocked with instrument mutex.
 *
 *   4.  When alarms go on/off (camp_alarm_priv.c)
 *
 *      Changed  20-Dec-1999  TW
 *      Alarm procs only defined in the main Tcl interpreter (when
 *      server starts up and reads camp.ini file).  So must use
 *      main Tcl interpreter.  
 *      
 *      USE main interpreter with global lock on.
 *
 *      OLD EXPLANATION:
 *          Trouble.  Alarms aren't necessarily related to a particular
 *          instrument.  The originating call is from a variable proc, 
 *          so could use the interp for that instrument.  What if the
 *          the alarm proc tries to access a different instrument.  That's
 *          safe because the alarm thread will wait for access to that
 *          instrument like any other thread.
 *
 *          MUST SET thread interp to instrument interp in variable
 *          read/write proc.  
 *
 *          TRY TO USE instrument interp.  If that is NULL, use
 *          main interp with global_mutex_noChange set TRUE.
 *
 *   5.  CAMAC read/write (camp_if_camac_*.c)
 *
 *      CAMAC calls aren't related to an instrument, but these
 *      calls are normally made from within an instrument driver.
 *      Also, only instrument interpreter has access to that instrument's
 *      Tcl global variables.
 *
 *      USE thread interpreter (which will be the instrument interpreter 
 *      if the CAMAC call is made from within the instrument driver).
 */
