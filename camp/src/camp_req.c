/*
 *  Name:       camp_req.c
 *
 *  Purpose:    Maintain requests in the CAMP server (variable polling)
 *
 *              In the current implementation, requests are requests to
 *              perform a polling operation on one instrument variable.
 *
 *              Requests (variable polling reads) are started at regular
 *              intervals, and are executed when the instrument mutex is available
 *              (see camp_srv_poll.c).
 *
 *  Provides:
 *              REQ_createPoll - allocate and init a poll request
 *
 *  Called by:
 *              REQ_addPoll is called by REQ_createPoll (here)
 *              REQ_doPoll is the callback in the request (performs the poll)
 *              REQ_doPending is called by srv_loop (in camp_srv_main.c)
 *              REQ_cancelAllIns is called by campsrv_insdel (in
 *                 camp_srv_proc.c)
 * 
 *  Revision history:
 *
 *    20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *
 */

#include <math.h>
#include "camp_srv.h"


REQ* 
REQ_createPoll( CAMP_VAR *pVar )
{
    REQ* pReq;

    pReq = (REQ*)camp_zalloc( sizeof( REQ ) );

    // pReq->pending = 0;
    pReq->cancel = FALSE;
    pReq->pVar = pVar;
    pReq->procs.retProc = REQ_doPoll;
    pReq->spec.type = IO_POLL;

    return( pReq );
}


/*
 *  called as a callback from poll thread
 */
int
REQ_doPoll( REQ* pReq )
{
    int camp_status = CAMP_SUCCESS;
    timeval_t time_current;
    // _mutex_lock_begin();

    if( pReq->cancel )
    {
	// pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
	// return( REQ_remove( pReq ) ); // 20140219  TW  don't remove here, let main thread clean up in REQ_doPending
	camp_status = CAMP_SUCCESS;
	if( _camp_debug(CAMP_DEBUG_POLL) ) 
	{
	    _camp_log( "poll cancelled pReq:%x var:'%s'", pReq, pReq->pVar->core.path );
	}
	goto return_;
    }

    // _mutex_lock_varlist_on(); // for pReq->pVar // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar_ins;
	CAMP_VAR* pVar = pReq->pVar;

	pVar_ins = varGetpIns( pVar );
	if( pVar_ins == NULL )
	{
	    // pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
	    _camp_setMsgStat( CAMP_INVAL_PARENT, pVar->core.ident );
	    camp_status = CAMP_INVAL_VAR;
	    goto return_;
	}

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns->pIF;

	    if( pIF == NULL )
	    {
		// pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
		camp_status = CAMP_INVAL_IF;
		goto return_;
	    }

	    if( !( pIF->status & CAMP_IF_ONLINE ) )
	    {
		// pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter
		camp_status = CAMP_NOT_CONN;

		if( _camp_debug(CAMP_DEBUG_POLL) ) 
		{
		    _camp_log( "not online pReq:%x var:'%s'", pReq, pReq->pVar->core.path );
		}
		goto return_;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	camp_status = varRead( pVar );

	if( camp_status != CAMP_SUCCESS )
	if( _camp_debug(CAMP_DEBUG_POLL) ) 
	{
	    _camp_log( "varRead status:%x pReq:%x var:'%s'", camp_status, pReq, pReq->pVar->core.path );
	}

	/* 
         *  20191213  DA Since 2014 poll requests are one-shot, so do not update for next poll
         * 
	 *  Next poll will occur in pollInterval seconds
	 *  after the current time.
	 *  This means that polling isn't really 
	 *  happening every pollInterval seconds, since 
	 *  the time for the varRead is significant, but
	 *  reduces the chance of backlogs.
	 */
	//   gettimeval( &time_current );  // 20191213 DA 
 	// rem = modf( pVar->core.pollInterval, &integ ); */
 	// pReq->time.tv_sec = time_current.tv_sec + (long)integ; */
 	// pReq->time.tv_usec = time_current.tv_usec + (long)(1e6*rem); */
	// pReq->time = addtimeval_double( &time_current, pVar->core.pollInterval ); // 20191213 DA 
    }
    // _mutex_lock_varlist_off(); // for pReq->pVar // warning: don't return inside mutex lock

    // pReq->pending = 0; // pReq->pending--; // 20140225  TW  flag not counter

return_:
    // _mutex_lock_end();
    return( camp_status );
}



