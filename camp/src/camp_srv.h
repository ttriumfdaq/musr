/*
 *      $Id: camp_srv.h,v 1.11 2018/06/13 00:35:10 asnd Exp $
 *
 *      $Revision: 1.11 $
 *
 *  $Log: camp_srv.h,v $
 *  Revision 1.11  2018/06/13 00:35:10  asnd
 *  Just comments
 *
 *  Revision 1.10  2015/04/21 05:51:56  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 */

#ifndef _CAMP_SRV_H_
#define _CAMP_SRV_H_

#ifndef CAMP_MULTITHREADED
#define CAMP_MULTITHREADED 0
#endif

#ifndef CAMP_TIRPC 
#define CAMP_TIRPC 0
#endif

#ifndef CAMP_MTRPC 
#define CAMP_MTRPC 0
#endif

/* #ifndef CAMP_MUTEX_SYS  */
/* #define CAMP_MUTEX_SYS 0 */
/* #endif */

/* #ifndef CAMP_MUTEX_VARLIST  */
/* #define CAMP_MUTEX_VARLIST 0 */
/* #endif */

#ifndef CAMP_MUTEX_SYSTEM_MODIFICATIONS 
#define CAMP_MUTEX_SYSTEM_MODIFICATIONS 0
#endif

// #define CAMP_RPC_THREAD_MODE_SERIAL 0 // rpc handled in serial (e.g., tsrpc/tirpc on linux)
#define CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET 1 // spawn threads from svc_getreqset (e.g., tsrpc on vxworks)
#define CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN 2 // spawn threads from svc_run (e.g. mtrpc)
#define CAMP_RPC_THREAD_MODE_SERIAL_IN_SINGLE 3 // serial rpc in single-thread server
#define CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI 4 // serial rpc in multi-thread server

/* #ifndef CAMP_RPC_THREAD_MODE */
/* #define CAMP_RPC_THREAD_MODE CAMP_RPC_THREAD_MODE_SERIAL */
/* #endif */

/* #if ( CAMP_RPC_THREAD_MODE == CAMP_RPC_THREAD_MODE_SERIAL ) */
/* #undef CAMP_RPC_THREAD_MODE */
/* #if CAMP_MULTITHREADED */
/* #define CAMP_RPC_THREAD_MODE CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI */
/* #else */
/* #define CAMP_RPC_THREAD_MODE CAMP_RPC_THREAD_MODE_SERIAL_IN_SINGLE */
/* #endif */
/* #endif */

/*
 *  Includes for Multithreading (see also camp.h).
 *  Add any other POSIX thread compliant OS here.
 *  No includes needed for VxWorks
 */
#if CAMP_MULTITHREADED
#if defined( VMS ) || defined( OSF1 ) || defined( ULTRIX ) || defined( linux )
#include <pthread.h>
#ifdef linux
#define PTHREAD_MUTEX_RECURSIVE PTHREAD_MUTEX_RECURSIVE_NP
#define pthread_addr_t void*
#define pthread_mutexattr_default NULL
#endif
#elif defined( VXWORKS )
#include <vxWorks.h>
/*
 *  Mimic pthreads
 */
#include "vx_pthread.h"
#endif /* operating system */
#endif /* CAMP_MULTITHREADED */

#include "tcl.h"
#include "camp.h" // before CAMP_MULTITHREADED use

/*
 *  Automatically turn an instrument offline
 *  after this many consecutive read or write
 *  errors
 */
#define MAX_CONSECUTIVE_IF_ERRORS 20

/* camp_alarm_priv.c */
void srv_do_alarms();

/* camp_cfg_write.c */
int campCfg_write ( const char* filename );
void save_all_ini ( void );
void cfgWrite_insadd ( void );
void cfgWrite_insset ( void );
int campState_write( const char* filename );
int cfgWrite_if ( CAMP_IF *pIF );
void cfgWrite_varLink ( CAMP_VAR *pVar );

/* camp_def_write.c */
int campDef_write ( const char* filename , CAMP_VAR *pVar );
void defWrite_var ( CAMP_VAR *pVar );
void defWrite_stat ( CAMP_VAR_CORE *pCore );
void defWrite_dyna ( CAMP_VAR_CORE *pCore );
void defWrite_spec ( CAMP_VAR *pVar );
void defWrite_Numeric ( CAMP_VAR *pVar );
void defWrite_Selection ( CAMP_VAR *pVar );
void defWrite_String ( CAMP_VAR *pVar );
void defWrite_Array ( CAMP_VAR *pVar );
void defWrite_Structure ( CAMP_VAR *pStcVar );
void defWrite_Facility ( CAMP_VAR *pInsVar );
void defWrite_Link ( CAMP_VAR *pVar );

/* camp_if_priv.c */
int camp_ifSet ( CAMP_IF **ppIF, CAMP_IF* pIF );
int camp_ifOnline ( CAMP_IF *pIF );
int camp_ifOffline ( CAMP_IF *pIF );
int camp_ifRead ( CAMP_IF *pIF , CAMP_VAR *pVar , char* cmd , int cmd_len , char* buf , int buf_len , int *pRead_len );
int camp_ifWrite ( CAMP_IF *pIF , CAMP_VAR *pVar , char* cmd , int cmd_len );
CAMP_IF_TYPE camp_getIfTypeID( const char* typeIdent );
const char* camp_getIfTypeIdent( CAMP_IF_TYPE typeId );
CAMP_VAR* camp_rs232PortInUse( CAMP_INSTRUMENT* pIns/* , bool_t _mutex_lock_varlist_check */ );

/* camp_ini_write.c */
int campIni_write ( const char* filename , CAMP_VAR *pVar );
void iniWrite_stat ( CAMP_VAR_CORE *pCore );
void iniWrite_dyna ( CAMP_VAR_CORE *pCore );
void iniWrite_var ( CAMP_VAR *pVar );
void iniWrite_Selection ( CAMP_VAR *pVar );
void iniWrite_Array ( CAMP_VAR *pVar );
void iniWrite_Structure ( CAMP_VAR *pStcVar );
void iniWrite_Instrument ( CAMP_VAR *pInsVar );
void cfgWrite_all_inst( void );

/* camp_ins_priv.c */
int camp_insInit ( CAMP_VAR *pVar );
int camp_insDelete ( CAMP_VAR *pVar );
int camp_insOnline ( CAMP_VAR *pVar );
int camp_insOffline ( CAMP_VAR *pVar );

/* camp_req.c */
// int REQ_add ( REQ *pReq );
// int REQ_remove ( REQ *pReq );
// void REQ_cancel ( CAMP_VAR *pVar , IO_TYPE type );
// void REQ_cancelAllIns ( CAMP_VAR *pVar );
REQ* REQ_createPoll( CAMP_VAR *pVar );
// void REQ_addPoll ( CAMP_VAR *pVar );
int REQ_doPoll ( REQ* pReq );
// void REQ_doPending( void );

/* camp_srv_main.c */
void srv_loop_main2_thread( void );

/* camp_srv_poll.c */
void srv_do_polling();
void cancel_all_polling_for_instrument( CAMP_VAR* pVar_ins );

/* camp_srv_rpc.c */
int srv_init_rpc();
void srv_shutdown_rpc();
void svc_run_once( void );

/* camp_srv_svc.c */
void camp_srv_10400_serial_in_single( struct svc_req *rqstp, SVCXPRT *transp ); // mode 0 singlethreaded
void camp_srv_10400_serial_in_multi( struct svc_req *rqstp, SVCXPRT *transp ); // mode 0 multithreaded
void camp_srv_10400_threaded_svc_getreqset( struct svc_req *rqstp, SVCXPRT *transp ); // mode 1
void camp_srv_10400_threaded_svc_run( struct svc_req *rqstp, SVCXPRT *transp ); // mode 2
void srv_handle_served_svc_mode1_threads( void );

/* camp_srv_thread.c */
struct _THREAD_DATA;
// typedef _THREAD_DATA THREAD_DATA;
int mutex_lock_global_once( void ); // was thread_lock_global_np
// int mutex_lock_global_once_no_count( void );
int mutex_unlock_global_once( void ); // was thread_unlock_global_np
// int mutex_unlock_global_once_no_count( void );
int mutex_lock_global_all( int count ); // was thread_lock_global_np
int mutex_unlock_global_all( void ); // was thread_unlock_global_np
int mutex_lock_global_count( int on, int count );
int mutex_lock_mainInterp( int on ); // was set_global_mutex_noChange
int mutex_lock_ins( CAMP_VAR* pVar, int on ); // was get_ins_thread_lock
// int mutex_lock_sys( int on );
// int mutex_lock_varlist( int on );
// int mutex_lock_gpib( int on );
int mutex_lock_if_t( CAMP_IF_t* pIF_t, int on );
bool_t mutex_lock_global_check();
bool_t mutex_lock_mainInterp_check();
// bool_t mutex_lock_sys_check();
// bool_t mutex_lock_varlist_check();
// bool_t mutex_lock_gpib_check();
// todo: bool_t mutex_lock_if_t_check( CAMP_IF_t* pIF_t )
// todo: bool_t mutex_lock_ins_check( CAMP_VAR* pVar )
int get_mutex_lock_count_global();
int get_mutex_lock_count_mainInterp();
// int get_mutex_lock_count_sys();
// int get_mutex_lock_count_varlist();
// int get_mutex_lock_count_gpib();
// todo: int get_mutex_lock_count_if_t( CAMP_IF_t* pIF_t )
// todo: int get_mutex_lock_count_ins( CAMP_VAR* pVar );
const char* getStringThreadType( int val );
const char* getStringThreadState( int val );
void set_thread_executing( int flag );
int wait_no_thread_executing( int flag );
struct svc_req* get_current_rqstp( void );
void set_current_rqstp( struct svc_req* rqstp );
SVCXPRT* get_current_transp( void );
void set_current_transp( SVCXPRT* transp );
Tcl_Interp* get_thread_interp( void );
void set_thread_interp( Tcl_Interp* interp );
u_long get_thread_xdr_flag( void );
void set_thread_xdr_flag( u_long flag );
char* get_thread_ident( void );
void set_thread_ident( const char* ident );
char* get_thread_msg( void );
void set_thread_msg( const char* msg );
int check_shutdown( void );
void set_shutdown( void );
void init_thread_data( struct _THREAD_DATA* pThread_data, int type, int state );
void reset_thread_data( struct _THREAD_DATA* pThread_data, int state );
void do_thread_finished( int index );
void do_thread_finished2( struct _THREAD_DATA* pThread_data );
int find_free_thread_data( int type, int globalMutexLockCount );
int try_find_free_thread_data( int type );
void kill_all_threads( void );
int srv_init_main_thread( void );
void srv_init_thread_once( void );
void srv_end_thread( void );
int check_rpc_transport_in_use( SVCXPRT* transp );
struct _THREAD_DATA* get_thread_data( int thread_data_index );
struct _THREAD_DATA* get_thread_data_this();
int get_thread_data_this_index();
int get_thread_data_size();
// int set_thread_key( void* thread_key_data );
int set_thread_key_to_thread_data( int thread_data_index );
int srv_start_main2_thread( void );

/* camp_srv_utils.c */
void srv_delAllInss( void );
/* some of these need fixing for non-multithreaded compilation !!!! */
int camp_checkInsLock( CAMP_VAR* pVar, struct svc_req* rq );
int camp_checkInsLockAll( struct svc_req* rq );
void camp_sleep( timeval_t *ptv );
void camp_fsleep( double delay );
uintptr_t pointerToInteger( void* p );
void* integerToPointer( uintptr_t );
int camp_glob( DIRENT** ppDirEnt, const char* filespec_in );

/* camp_sys_priv.c */
void sys_free ( void );
int sys_init ( void );
int sys_update ( void );
int sys_initDyna ( void );
u_long sys_getTypeInstance ( char* typeIdent );

/* camp_tcl.c */
Tcl_Interp* camp_tclInterp( void );
int camp_tclInit ( void );
void camp_tclEnd( void );
Tcl_Interp* campTcl_CreateInterp( void );
void campTcl_DeleteInterp( Tcl_Interp* interp );
void assemble_tcl_message( Tcl_Interp* interp, char* message, int message_size );

/* camp_var_priv.c */
// void varInitPoll ( CAMP_VAR *pVar );
int varRead ( CAMP_VAR *pVar );
int varSetReq ( CAMP_VAR *pVar , CAMP_VAR_SPEC *pSpec );
int varSetSpec ( CAMP_VAR *pVar , CAMP_VAR_SPEC *pSpec );
// int var_set ( CAMP_VAR *pVar );
// int varNumSet ( CAMP_VAR *pVar , double val );
// int varNumSetTol ( CAMP_VAR *pVar , u_long tolType , float tol );
// int varSelSet ( CAMP_VAR *pVar , u_char val );
// int varSelSetLabel ( CAMP_VAR *pVar , char* label );
// int varStrSet ( CAMP_VAR *pVar , char* val );
// int varArrSet ( CAMP_VAR *pVar , caddr_t pVal );
// int varLnkSet ( CAMP_VAR *pVar , char* path );
int varSetShow ( CAMP_VAR *pVar , bool_t flag );
int varSetSet ( CAMP_VAR *pVar , bool_t flag );
int varSetRead ( CAMP_VAR *pVar , bool_t flag );
int varSetPoll ( CAMP_VAR *pVar , bool_t flag , float interval );
int varSetAlarm ( CAMP_VAR *pVar , bool_t flag , char* alarmAction );
int varSetLog ( CAMP_VAR *pVar , bool_t flag , char* logAction );
bool_t varNumTestAlert( CAMP_VAR* pVar, double val );
int varSetAlert ( CAMP_VAR *pVar , bool_t flag );
// int varSetIsSet ( CAMP_VAR *pVar , bool_t flag );
// int varSetPendSet ( CAMP_VAR *pVar , bool_t flag );
int varSetMsg ( CAMP_VAR *pVar , char* msg );
int varZeroStats ( CAMP_VAR *pVar );
// void varNumZeroStats ( CAMP_NUMERIC *pNum );
// int  varDoStats ( CAMP_VAR *pVar );
// void varNumDoStats ( CAMP_NUMERIC *pNum );
// void varFree ( CAMP_VAR *pVar );
int  varAdd ( char* path , CAMP_VAR *pVar );

/* camp_write.c */
int openOutput ( const char* filename );
int closeOutput ( void );
void add_tab ( void );
void del_tab ( void );
void print ( const char* fmt , ...);
void print_tab ( const char* fmt , ...);
void reinitialize ( const char* name, PFB parse_proc, const char* line );
int camp_writeDumpFile( const char* filename, unsigned int fnlen,
                        const char* dump, unsigned int dump_len );
void numStrTrimz( char* str );


/* if_none */
int if_none_init( void );
int if_none_online( CAMP_IF* pIF );
int if_none_offline( CAMP_IF* pIF );
int if_none_read( REQ* pReq );
int if_none_write( REQ* pReq );

/* if_rs232 */
int if_rs232_init( void );
int if_rs232_online ( CAMP_IF *pIF );
int if_rs232_offline ( CAMP_IF *pIF );
int if_rs232_read ( REQ *pReq );
int if_rs232_write ( REQ *pReq );

/* if_gpib */
int if_gpib_init( void );
int if_gpib_online ( CAMP_IF *pIF );
int if_gpib_offline ( CAMP_IF *pIF );
int if_gpib_read ( REQ *pReq );
int if_gpib_write ( REQ *pReq );
int if_gpib_clear ( int gpib_address );
int if_gpib_timeout ( float timeoutSeconds );

#ifndef CAMP_HAVE_GPIB_MSCB
#define CAMP_HAVE_GPIB_MSCB 0
#endif // CAMP_HAVE_GPIB_MSCB

#if CAMP_HAVE_GPIB_MSCB
int if_gpib_mscb_init( void );
int if_gpib_mscb_shutdown( void );
int if_gpib_mscb_online ( CAMP_IF *pIF );
int if_gpib_mscb_offline ( CAMP_IF *pIF );
int if_gpib_mscb_read ( REQ *pReq );
int if_gpib_mscb_write ( REQ *pReq );
int if_gpib_mscb_clear ( int gpib_address );
int if_gpib_mscb_timeout ( float timeoutSeconds );
#endif // CAMP_HAVE_GPIB_MSCB

/* if_camac */
int if_camac_init ( void );
int if_camac_online ( CAMP_IF *pIF );
int if_camac_offline ( CAMP_IF *pIF );
int if_camac_read( REQ* pReq );
int if_camac_write( REQ* pReq );

/* if_indpak */
int if_indpak_init ( void );
int if_indpak_online ( CAMP_IF *pIF );
int if_indpak_offline ( CAMP_IF *pIF );
int if_indpak_read( REQ* pReq );
int if_indpak_write( REQ* pReq );

/* if_vme */
int if_vme_init ( void );
int if_vme_online ( CAMP_IF *pIF );
int if_vme_offline ( CAMP_IF *pIF );
int if_vme_read( REQ* pReq );
int if_vme_write( REQ* pReq );

/* if_tcpip */
int if_tcpip_init( void );
int if_tcpip_online ( CAMP_IF *pIF );
int if_tcpip_offline ( CAMP_IF *pIF );
int if_tcpip_read ( REQ *pReq );
int if_tcpip_write ( REQ *pReq );

/* ins_tcl */
int ins_tcl_init ( CAMP_VAR *pVar );
int ins_tcl_delete ( CAMP_VAR *pVar );
int ins_tcl_online ( CAMP_VAR *pVar );
int ins_tcl_offline ( CAMP_VAR *pVar );
int ins_tcl_set ( CAMP_VAR *pInsVar , CAMP_VAR *pVar , CAMP_VAR_SPEC *pSpec );
int ins_tcl_read ( CAMP_VAR *pInsVar , CAMP_VAR *pVar );


typedef union {
                FILE_req campsrv_sysload_10400_arg;
                FILE_req campsrv_syssave_10400_arg;
                FILE_req campsrv_sysdir_10400_arg;
                INS_ADD_req campsrv_insadd_10400_arg;
                DATA_req campsrv_insdel_10400_arg;
                INS_LOCK_req campsrv_inslock_10400_arg;
                INS_LINE_req campsrv_insline_10400_arg;
                INS_FILE_req campsrv_insload_10400_arg;
                INS_FILE_req campsrv_inssave_10400_arg;
                INS_IF_req campsrv_insifset_10400_arg;
                INS_READ_req campsrv_insifread_10400_arg;
                INS_WRITE_req campsrv_insifwrite_10400_arg;
                INS_DUMP_req campsrv_insifdump_10400_arg;
                INS_UNDUMP_req campsrv_insifundump_10400_arg;
                DATA_req campsrv_insifon_10400_arg;
                DATA_req campsrv_insifoff_10400_arg;
                DATA_SET_req campsrv_varset_10400_arg;
                DATA_POLL_req campsrv_varpoll_10400_arg;
                DATA_ALARM_req campsrv_varalarm_10400_arg;
                DATA_LOG_req campsrv_varlog_10400_arg;
                DATA_req campsrv_varzero_10400_arg;
                DATA_SET_req campsrv_vardoset_10400_arg;
                DATA_req campsrv_varread_10400_arg;
                DATA_SET_req campsrv_varlnkset_10400_arg;
                DATA_GET_req campsrv_varget_10400_arg;
                CMD_req campsrv_cmd_10400_arg;
} CampRpcArgument;


#if CAMP_MULTITHREADED

/*
 *  thread types:  
 *      main (main server thread)
 *      svc (handle RPC requests) 
 *      poll (variable polling)
 */
#define CAMP_THREAD_TYPE_NONE 0 
#define CAMP_THREAD_TYPE_MAIN 1
#define CAMP_THREAD_TYPE_SVC 2
#define CAMP_THREAD_TYPE_POLL 3 

/*
 *  thread states
 */
#define CAMP_THREAD_STATE_INIT 0
#define CAMP_THREAD_STATE_RUNNING 1
#define CAMP_THREAD_STATE_FINISHED 2

/*
 *  was considering a hash table to keep track of instrument mutex with pVar as key
 */
// #include "uthash.h"
// typedef struct {
//     CAMP_VAR* pVar;
//     int count;
//     UT_hash_handle hh;
// } uthash_entry_ins_mutex_count;

/*
 *  Thread private data structure
 */
typedef struct _THREAD_DATA {
    struct _THREAD_DATA* pNext;
    pthread_t thread_handle;
    // pthread_mutex_t mutex_handle; // 20140225  TW  thread mutex unnecessary
    struct svc_req* rqstp;     // svc thread: RPC svc_req (deep copy)
                               // argument for camp_srv_proc's called from camp_tcl (many) and camp_cfg_write:save_all_ini
                               // allocated by svc thread, freed by main thread
    SVCXPRT* transp;           // svc thread: RPC SVCXPRT (not copied)
    /* SVCXPRT orig_trans; */  /* 16-Dec-1999  TW  Copy of original transport */
    CampRpcArgument argument;  // svc thread: RPC arguments
    char* pResult;             // svc thread: RPC result
                               // pointer to RES structure returned by camp_srv_proc routines to svc thread, 
			       // used by the main thread to send RPC reply
                               // allocated by svc thread, freed by main thread
    u_long xdr_flag;           /* how to xdr for this thread */
    char ident[LEN_IDENT+1];   /* instrument ident */
    char* msg;                 /* allocate once (previously allocated dynamically, before 201402) */
    REQ* pReq;                 /* poll thread: REQ */
    timeval_t tv_start;        /* svc thread */
    Tcl_Interp* interp;        /* separate Tcl interpreter for thread */
    // fd_set readfds;         /* 17-Dec-1999  TW  Thread does all RPC work */ // 20140303  TW  unused
    unsigned char type;                /* MAIN, SVC or POLL */
    unsigned char state;               /* INIT, RUNNING or FINISHED */
    unsigned char served;              /* svc thread */
    unsigned char executing;           /* 20140304  TW  thread is in its processing loop (similar to where
				  it takes the global lock, but doesn't release until loop is finished) */
    unsigned char mutex_count_global;  /* 19-Dec-1999  TW  Count the number of times
                                  this thread has claimed the global mutex
                                  In implementation, this is used to restrict
                                  the number of times to a maximum of 1 */
    // unsigned char mutex_count_sys;
    // unsigned char mutex_count_varlist;
    unsigned char mutex_count_mainInterp;
    // unsigned char mutex_count_gpib;
    // uthash_entry_ins_mutex_count* uthashtable_mutex_count_ins;
} THREAD_DATA;

#endif // CAMP_MULTITHREADED


/*
 *  to save a little space, don't do lock counting with VxWorks
 *
 *  (sys and varlist mutexes not used, ins mutex counting is only
 *  a sanity check.  Only mainInterp mutex locking must be checked
 *  for correctness.)
 *
 *  20140421  TW  now that vxworks stack use is optimized, turn this back on
 */
// #if !defined( VXWORKS )
#define CAMP_MUTEX_MACRO_LOCK_COUNTING
// #endif // VXWORKS

#if CAMP_MULTITHREADED && defined( CAMP_MUTEX_MACRO_LOCK_COUNTING )

#define _mutex_lock_with_count_begin( type )				\
    int mutex_lock_count_ ## type ## _begin = get_mutex_lock_count_ ## type ()

#define _mutex_lock_with_count_on( type )				\
    {									\
	mutex_lock_ ## type ( 1 );					\
    }

#define _mutex_lock_with_count_off( type )				\
    {									\
	mutex_lock_ ## type ( 0 );					\
    }

#define _mutex_lock_with_count_end( type )	\
    {									\
	int count_begin = mutex_lock_count_ ## type ## _begin;		\
	int count_end = get_mutex_lock_count_ ## type ();		\
	int i;								\
	for( i = 0; i < (count_end-count_begin); ++i ) { mutex_lock_ ## type ( 0 ); } \
    }

// if( count_end > count_begin )					
// {									
//     camp_log( "unexpected lock of " #type " mutex" );		
//     exit( CAMP_FAILURE );						
// }									

#define _mutex_lock_mainInterp_begin()	_mutex_lock_with_count_begin( mainInterp )
#define _mutex_lock_mainInterp_on()	_mutex_lock_with_count_on( mainInterp )
#define _mutex_lock_mainInterp_off()	_mutex_lock_with_count_off( mainInterp )
#define _mutex_lock_mainInterp_end()    _mutex_lock_with_count_end( mainInterp )

#define _mutex_lock_ins_begin()						\
    int mutex_lock_count_ins = 0
#define _mutex_lock_ins_on( pVar )					\
    {									\
	mutex_lock_ins( pVar, 1 ); ++mutex_lock_count_ins;		\
    }
#define _mutex_lock_ins_off( pVar )					\
    {									\
	mutex_lock_ins( pVar, 0 ); --mutex_lock_count_ins;		\
    }
#define _mutex_lock_ins_end()						\
    {									\
	int count_begin = 0;						\
	int count_end = mutex_lock_count_ins;				\
	if( count_end > count_begin )					\
	{								\
	    camp_log( "unexpected lock of ins mutex" );		\
	    exit( CAMP_FAILURE );					\
	}								\
    }

#else // !CAMP_MULTITHREADED (or not CAMP_MUTEX_MACRO_LOCK_COUNTING)

#if CAMP_MULTITHREADED

#define _mutex_lock_ins_begin()		 {}
#define _mutex_lock_ins_on( pVar )       mutex_lock_ins( pVar, 1 )
#define _mutex_lock_ins_off( pVar )      mutex_lock_ins( pVar, 0 )
#define _mutex_lock_ins_end()		 {}
#define _mutex_lock_mainInterp_begin()	 {}
#define _mutex_lock_mainInterp_on()      mutex_lock_mainInterp( 1 )
#define _mutex_lock_mainInterp_off()     mutex_lock_mainInterp( 0 )
#define _mutex_lock_mainInterp_end()     {}

#else

#define _mutex_lock_ins_begin()		 {}
#define _mutex_lock_ins_on( pVar )	 {}
#define _mutex_lock_ins_off( pVar )	 {}
#define _mutex_lock_ins_end(  )	         {}
#define _mutex_lock_mainInterp_begin()	 {}
#define _mutex_lock_mainInterp_on()	 {}
#define _mutex_lock_mainInterp_off()	 {}
#define _mutex_lock_mainInterp_end()     {}

#endif // CAMP_MULTITHREADED

#endif // CAMP_MULTITHREADED and CAMP_MUTEX_MACRO_LOCK_COUNTING

/*
 *  retired mutexes
 */
// #define _mutex_lock_sys_begin()          {}
// #define _mutex_lock_sys_on()		 {}
// #define _mutex_lock_sys_off()		 {}
// #define _mutex_lock_sys_end()	         {}
// #define _mutex_lock_varlist_begin()	 {}
// #define _mutex_lock_varlist_on()	 {}
// #define _mutex_lock_varlist_off()	 {}
// #define _mutex_lock_varlist_end()        {}
// #define mutex_lock_sys( on )             {}
// #define mutex_lock_sys_check()           TRUE
// #define mutex_lock_varlist( on )         {}
// #define mutex_lock_varlist_check()       TRUE

#define _mutex_lock_begin()                                             \
    _mutex_lock_mainInterp_begin();                                     \
    _mutex_lock_ins_begin();						

//     _mutex_lock_sys_begin();						
//     _mutex_lock_varlist_begin();					

#define _mutex_lock_end()					        \
    _mutex_lock_mainInterp_end();				        \
    _mutex_lock_ins_end();					        

//     _mutex_lock_sys_end();					        
//     _mutex_lock_varlist_end();					

#endif /* _CAMP_SRV_H_ */
