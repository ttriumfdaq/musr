/*
 *  Name:       camp_srv_clnt_mod.c
 *
 *  Purpose:    CAMP client side low-level routines to make all RPC calls
 *              to the CAMP server.
 *
 *              Parallel to the server-side calls in camp_srv_proc.c
 *
 *              Based initially on the file camp_srv_clnt.c generated by
 *              rpcgen from a *.x file
 *
 *              Note:  it is not recommended to use these routines in a
 *              client application.  It is recommended instead to use the
 *              entry points in camp_api_proc.c as described in the CAMP
 *              programmer's guide.  These higher level routines manage
 *              all RPC structures without knowledge of their content or
 *              context.  The only situation in which these routines may
 *              be needed directly would be in the case of a complicated
 *              client, such as one that was multithreaded, etc.
 *
 *  Called by:  camp_api_proc.c
 * 
 *  Revision history:
 *    04-Dec-1996  TW  Timeout now controlled from server.
 *    13-Dec-1999  TW  Casting for clnt_call (gcc warnings)
 *    14-Dec-2000  TW  Increase RPC timeout (to 60 seconds) for some
 *                     RPC calls (several instruments have very long
 *                     read operations).
 *    19-Jun-2017  DA  Separate longer timeouts for loading and var read
 *                     or write (see camp.h for values)
 *
 *
 */

#include <stdlib.h>
#include <string.h>
#include "camp_clnt.h" // "camp.h" // 20140226  TW

/*
 *  12-Dec-1996  TW  This TIMEOUT used in clnt_call() doesn't seem to matter.
 *                   Requests still timeout at the default of 25 seconds.
 *  19-Dec-2000  TW  TIMEOUT used to be 600 seconds, now make it
 *                   CAMP_RPC_CLNT_TIMEOUT which is consistent with the
 *                   timeout set by clnt_control (not that TIMEOUT has
 *                   any effect - only seems to take effect if it is zero).
 *  19-Jun-2017  DA  Note that timeout setting by clnt_control supersedes
 *                   any in clnt_call (man 3 rpc)
 */
static struct timeval TIMEOUT = { CAMP_RPC_CLNT_TIMEOUT, 0 };

extern char camp_serverHostname[LEN_NODENAME+1];
extern enum clnt_stat camp_clnt_stat;

extern DIRENT*          pSys_pDir; // 20140326  TW  replacement for pSys->pDir

// DIRENT* pDirEnt_global = NULL;


/*
 *  14-Dec-2000   TW  Increase the RPC timeout for some var operations.
 *                    Specifically, campsrv_varset and
 *                    campsrv_varread call var_timeout to increase
 *                    the timeout during the call, and set it back
 *                    with default_timeout after the call.  This
 *                    functionality is what one would assume
 *                    clnt_call() with the TIMEOUT parameter is supposed
 *                    to do (DA: Not after any clnt_control).
 */
static void
load_timeout( CLIENT* clnt )
{
  timeval_t tv = { CAMP_RPC_CLNT_LOAD_TIMEOUT, 0 };
  clnt_control( clnt, CLSET_TIMEOUT, (char*)&tv );
}
static void
var_timeout( CLIENT* clnt )
{
  timeval_t tv = { CAMP_RPC_CLNT_VAR_TIMEOUT, 0 };
  clnt_control( clnt, CLSET_TIMEOUT, (char*)&tv );
}
static void
default_timeout( CLIENT* clnt )
{
  timeval_t tv = { CAMP_RPC_CLNT_TIMEOUT, 0 };
  clnt_control( clnt, CLSET_TIMEOUT, (char*)&tv );
}

RES*
campsrv_sysshutdown_10400(argp, clnt)
        void *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_SYSSHUTDOWN, (xdrproc_t)xdr_void, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
          _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_sysupdate_10400(argp, clnt)
        void *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_SYSUPDATE, (xdrproc_t)xdr_void, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_sysload_10400(argp, clnt)
        FILE_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        load_timeout( clnt );
        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_SYSLOAD, (xdrproc_t)xdr_FILE_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            default_timeout( clnt );
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        default_timeout( clnt );
        return (&res);
}


RES*
campsrv_syssave_10400(argp, clnt)
        FILE_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_SYSSAVE, (xdrproc_t)xdr_FILE_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


#if CAMP_CUI_LOG
static void
print_sysdir( const char* msg, DIRENT* pDir )
{
    if( pDir == NULL ) 
    {
	camp_log( "%s: pDir:%p", msg, pDir );
	return;
    }
    camp_log( "%s: pDir:%p pNext:%p filename:%p '%s'", msg, pDir, pDir->pNext, pDir->filename, pDir->filename );
    if( pDir->pNext != NULL ) print_sysdir( msg, pDir->pNext );
}
#endif // CAMP_CUI_LOG


CAMP_SYS_res *
campsrv_sysget_10400(argp, clnt)
        void *argp;
        CLIENT *clnt;
{
        static CAMP_SYS_res res = { 0, NULL, NULL };

#if CAMP_CUI_LOG
	/* if( pSys != NULL ) */
	/* print_sysdir( "campsrv_sysget before clnt_call", pSys->pDir ); */
#endif // CAMP_CUI_LOG

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        res.pSys = pSys;
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_SYSGET, (xdrproc_t)xdr_void, (caddr_t)argp, (xdrproc_t)xdr_CAMP_SYS_res, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }

        pSys = res.pSys;

#if CAMP_CUI_LOG
	/* if( pSys != NULL ) */
	/* print_sysdir( "campsrv_sysget after  clnt_call", pSys->pDir ); */
#endif // CAMP_CUI_LOG

        if( _failure( res.status ) ) { _camp_setMsg( "%s", res.msg ); }
        return (&res);
}


SYS_DYNAMIC_res *
campsrv_sysgetdyna_10400(argp, clnt)
        void *argp;
        CLIENT *clnt;
{
        static SYS_DYNAMIC_res res = { 0, NULL, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        res.pDyna = pSys->pDyna;
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_SYSGETDYNA, (xdrproc_t)xdr_void, (caddr_t)argp, (xdrproc_t)xdr_SYS_DYNAMIC_res, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        pSys->pDyna = res.pDyna;
        if( _failure( res.status ) ) { _camp_setMsg( "%s", res.msg ); }
        return (&res);
}


DIR_res *
campsrv_sysdir_10400(argp, clnt)
        FILE_req *argp;
        CLIENT *clnt;
{
        static DIR_res res = { 0, NULL, NULL };

#if CAMP_CUI_LOG
	// print_sysdir( "campsrv_sysdir before clnt_call", res.pDir );
#endif // CAMP_CUI_LOG

        _free( res.msg );
	_xdr_free( xdr_DIRENT, res.pDir ); // 20140325  TW  free previous linked-list
        bzero((void *)&res, sizeof(res));
        // res.pDir = pSys_pDir; // 20140304  TW  unnecessary attempt to reuse storage. 

        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_SYSDIR, (xdrproc_t)xdr_FILE_req, (caddr_t)argp, (xdrproc_t)xdr_DIR_res, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
	    pSys_pDir = res.pDir;
            return (&res);
        }

#if CAMP_CUI_LOG
	// print_sysdir( "campsrv_sysdir after  clnt_call", res.pDir );
#endif // CAMP_CUI_LOG

	//_xdr_free( xdr_DIRENT, pSys_pDir ); // 20140304  TW  
        pSys_pDir = res.pDir; // save the directory to a global for external access

        if( _failure( res.status ) ) { _camp_setMsg( "%s", res.msg ); }
        return (&res);
}


RES*
campsrv_insadd_10400(argp, clnt)
        INS_ADD_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        load_timeout( clnt );
        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSADD, (xdrproc_t)xdr_INS_ADD_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            default_timeout( clnt );
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
	default_timeout( clnt );
        return (&res);
}


RES*
campsrv_insdel_10400(argp, clnt)
        DATA_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSDEL, (xdrproc_t)xdr_DATA_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_inslock_10400(argp, clnt)
        INS_LOCK_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSLOCK, (xdrproc_t)xdr_INS_LOCK_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insline_10400(argp, clnt)
        INS_LINE_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        var_timeout( clnt );
        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSLINE, (xdrproc_t)xdr_INS_LINE_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            default_timeout( clnt );
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
	default_timeout( clnt );
        return (&res);
}


RES*
campsrv_insload_10400(argp, clnt)
        INS_FILE_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        load_timeout( clnt );
        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSLOAD, (xdrproc_t)xdr_INS_FILE_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            default_timeout( clnt );
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        default_timeout( clnt );
        return (&res);
}


RES*
campsrv_inssave_10400(argp, clnt)
        INS_FILE_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSSAVE, (xdrproc_t)xdr_INS_FILE_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insifset_10400(argp, clnt)
        INS_IF_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSIFSET, (xdrproc_t)xdr_INS_IF_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insifread_10400(argp, clnt)
        INS_READ_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSIFREAD, (xdrproc_t)xdr_INS_READ_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insifwrite_10400(argp, clnt)
        INS_WRITE_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSIFWRITE, (xdrproc_t)xdr_INS_WRITE_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insifon_10400(argp, clnt)
        DATA_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSIFON, (xdrproc_t)xdr_DATA_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insifoff_10400(argp, clnt)
        DATA_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSIFOFF, (xdrproc_t)xdr_DATA_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insifdump_10400(argp, clnt)
        INS_DUMP_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSIFDUMP, (xdrproc_t)xdr_INS_DUMP_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_insifundump_10400(argp, clnt)
        INS_UNDUMP_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_INSIFUNDUMP, (xdrproc_t)xdr_INS_UNDUMP_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_varset_10400(argp, clnt)
        DATA_SET_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        var_timeout( clnt );
        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARSET, (xdrproc_t)xdr_DATA_SET_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            default_timeout( clnt );
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        default_timeout( clnt );
        return (&res);
}


RES*
campsrv_varpoll_10400(argp, clnt)
        DATA_POLL_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARPOLL, (xdrproc_t)xdr_DATA_POLL_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_varalarm_10400(argp, clnt)
        DATA_ALARM_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARALARM, (xdrproc_t)xdr_DATA_ALARM_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_varlog_10400(argp, clnt)
        DATA_LOG_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARLOG, (xdrproc_t)xdr_DATA_LOG_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_varzero_10400(argp, clnt)
        DATA_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARZERO, (xdrproc_t)xdr_DATA_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_vardoset_10400(argp, clnt)
        DATA_SET_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARDOSET, (xdrproc_t)xdr_DATA_SET_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


RES*
campsrv_varread_10400(argp, clnt)
        DATA_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        var_timeout( clnt );
        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARREAD, (xdrproc_t)xdr_DATA_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            default_timeout( clnt );
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        default_timeout( clnt );
        return (&res);
}


RES*
campsrv_varlnkset_10400(argp, clnt)
        DATA_SET_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));
        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARLNKSET, (xdrproc_t)xdr_DATA_SET_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        _camp_setMsg( "%s", res.msg );
        return (&res);
}


CAMP_VAR_res *
campsrv_varget_10400(argp, clnt)
        DATA_GET_req *argp;
        CLIENT *clnt;
{
        static CAMP_VAR_res res = { 0, NULL, NULL };
        CAMP_VAR** ppVar;

        _free( res.msg );
        bzero((void *)&res, sizeof(res));

        if( camp_pathAtTop( argp->dreq.path ) )
        {
            ppVar = &pVarList;
        }
        else 
        {
            ppVar = camp_varGetpp( argp->dreq.path/* , mutex_lock_varlist_check() */ );
            if( ppVar == NULL ) 
            {
                res.status = CAMP_INVAL_VAR;
		_camp_setMsg( msg_getHelpString( CAMP_INVAL_VAR ), argp->dreq.path );
                return( &res );
            }
    
            if( argp->flag & CAMP_XDR_CHILD_LEVEL )
            {
                ppVar = &(*ppVar)->pChild;
                if( *ppVar == NULL ) 
                {
                    res.status = CAMP_INVAL_VAR;
		    _camp_setMsg( "Invalid child level for %s", argp->dreq.path );
                    return( &res );
                }
            }
        }
    
        if( !( argp->flag & CAMP_XDR_UPDATE ) )
        {
            _xdr_free( xdr_CAMP_VAR, (*ppVar) );
        }
    
        if( argp->flag & CAMP_XDR_CHILD_LEVEL )
        {
            argp->flag |= CAMP_XDR_NO_CHILD;
            argp->flag &= ~CAMP_XDR_NO_NEXT;
        }

        set_current_xdr_flag( argp->flag );

        res.pVar = *ppVar;

        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_VARGET, (xdrproc_t)xdr_DATA_GET_req, (caddr_t)argp, (xdrproc_t)xdr_CAMP_VAR_res, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }

        *ppVar = res.pVar;

        set_current_xdr_flag( CAMP_XDR_ALL );

        if( _failure( res.status ) ) { _camp_setMsg( "%s", res.msg ); }
        return (&res);
}


RES*
campsrv_cmd_10400(argp, clnt)
        CMD_req *argp;
        CLIENT *clnt;
{
        static RES res = { 0, NULL };

        _free( res.msg );
        bzero((void *)&res, sizeof(res));

        if( ( camp_clnt_stat = clnt_call(clnt, CAMPSRV_CMD, (xdrproc_t)xdr_CMD_req, (caddr_t)argp, (xdrproc_t)xdr_RES, (caddr_t)&res, TIMEOUT) ) != RPC_SUCCESS ) {
            _camp_setMsg( "%s", clnt_sperror( clnt, camp_serverHostname ) );
            res.status = CAMP_FAIL_RPC;
            return (&res);
        }
        camp_setMsg( "%s", res.msg );
        return (&res);
}



