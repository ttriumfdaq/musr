/*
 *  Name:       camp_srv_main.c
 *
 *  Purpose:    Main loop routines for the CAMP server
 *
 *  Called by:  None
 * 
 *  Revision history:
 *          20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *          20140214     TW  tidying CAMP_DEBUG
 *    v1.3  23-Apr-1995  TW  Use _POSIX_THREADS to make server multithreaded
 *    v1.1  20-Apr-1994  TW  drivers in server, IFs generalized
 *
 */

//#ifdef linux
/* try to force "fds_bits" to be the item in fd_set */
//#define _XOPEN_SOURCE 1
//#endif

#include <string.h>
#include <stdio.h>
#include <time.h>
#include "camp_srv.h"

#ifdef VXWORKS
#include "timeval.h"
#include <rpc/rpcGbl.h>    /* for VxWorks RPC globals */
#include "rpcLib.h" // rpcTaskInit
#endif /* VXWORKS */

#ifdef linux
#include <pthread.h>
#endif /* linux */

/*
 *  Server polling definitions
 */
#define SRV_POLL_INTERVAL 1

/*
 *  Auto save definitions
 */
#define AUTO_SAVE_INTERVAL 600
#define do_auto_save TRUE

/*
 *  globals
 */
CAMP_SYS* pSys = NULL;
CAMP_VAR* pVarList = NULL;
unsigned int camp_debug = 0;
int camp_rpc_thread_mode = -1;
unsigned int camp_rpc_serial = 0;
int camp_max_threads = -1;
bool_t camp_ever_had_inst = FALSE;  /* Flags if server has ever had an instrument */

// char* prog_name = "CAMP server " CAMP_SRV_VERSION_STRING;
// char* gpcPrgName = "CAMP server " CAMP_SRV_VERSION_STRING; // for mtrpc

static int srv_init( void );
static void srv_shutdown( void );
static void srv_loop_main_thread( void );
static void srv_loop_main1_thread( void );
static void srv_loop_local_tasks( void );
static void init_rpc_thread_mode( void );
static void init_max_threads( void );

#ifdef VXWORKS
/*
 * usage is
 *    camp_srv camp_debug rpc_serial max_threads 
 * Arguments are integers, and are 0 (forcing defaults) if omitted.
 */
#else /* !VXWORKS */
void
usage( char* argv0 )
{
    fprintf( stderr, "usage: %s [-d <camp_debug bitmask>] [--rpc_serial] [--max_threads <int>]\n", argv0 );
}
#endif

/*
 *  Name:       main (campSrv for VxWorks)
 *
 *  Purpose:    CAMP server main program
 *
 *              Initialize the CAMP server, then enter a loop waiting for
 *              RPC calls, and fulfilling requests (etc.).
 *
 *  Called by:  None
 * 
 *  Inputs:     Optional debug bitmask
 *              For VxWorks: integer parameter
 *              For other  : -d [<integer>]
 *
 *  Revision history:
 *
 */
#ifdef VXWORKS
int 
campSrv( int arg1, int arg2, int arg3 )
#else /* !VXWORKS */
int 
main( int argc, char* argv[] )
#endif /* VXWORKS */
{
    int camp_status;
#if CAMP_MULTITHREADED
    pthread_once_t t_init_once_block = PTHREAD_ONCE_INIT;
#endif /* CAMP_MULTITHREADED */

#ifdef VXWORKS

    camp_debug = arg1;
    camp_rpc_serial = arg2;
    camp_max_threads = arg3;

#else // !VXWORKS
    int i; 

    for( i = 1; i < argc; i++ )
    {
	if( streq( argv[i], "-d" ) )
	{
	    if( i+1 < argc )
	    {
		int count;

		i++;

		count = sscanf( argv[i], "%x", &camp_debug ); // camp_debug = atoi( argv[i] ); // 20140331 TW bitmask

		//printf( "argv:'%s' camp_debug=0x%08x\n", argv[i], camp_debug );

		if( count < 1 )
		{
		    usage( argv[0] );
		    exit( CAMP_FAILURE );
		}
	    }
	    else
	    {
		camp_debug = 0xFFFFFFFF; // 1;
	    }
	}
	else if( streq( argv[i], "--rpc_serial" ) )
	{
	    camp_rpc_serial = 1;
	}
	else if( streq( argv[i], "--max_threads" ) )
	{
	    if( ++i >= argc )
	    {
		usage( argv[0] );
		exit( CAMP_FAILURE );
	    }

	    camp_max_threads = atoi( argv[i] );
	}
    }

    if( camp_debug == 0 )
    {
	char* env = getenv( "CAMP_DEBUG" );

	if( env != NULL )
	{
	    sscanf( env, "%x", &camp_debug ); // camp_debug = atoi( env ); // 20140418 TW bitmask
	}
    }

#endif // VXWORKS

    camp_init_filemasks(); // before use of camp_getFilemask

#ifdef VXWORKS

    if( _camp_debug(CAMP_DEBUG_SRV_INIT) )
    {
	printf( "%s: before xdr_free: pSys:%p pVarList:%p\n", __FUNCTION__, pSys, pVarList );
    }

    /*
     *  Make sure global linked lists are freed in case
     *  CAMP Server was stopped uncleanly
     */
    _xdr_free( xdr_CAMP_VAR, pVarList );
    _xdr_free( xdr_CAMP_SYS, pSys );

    /*
     *  Initialize VxWorks libraries
     *  Find that only rpcTaskInit is really necessary
     */
    // stdioInit(); // called from root task
    // selectInit(); // 20140418 unnecessary: called from root task
    // taskVarInit(); // 
    rpcTaskInit();

#endif // VXWORKS

#if !CAMP_MULTITHREADED

    /* 
     *  Initialize exit handler
     */
    atexit( srv_shutdown );

#endif /* CAMP_MULTITHREADED */

#ifdef VMS

    /*
     *  Global symbol to indicate exit status to shell
     */
    lib_set_symbol( "CAMP_STATUS", "FAILURE", 1 );

#endif /* VMS */

    /*
     *  Direct messages to log file
     */
    camp_status = camp_startLog( "camp_srv", camp_getFilemaskGeneric(CAMP_SRV_LOG_FILE), TRUE );
    if( _failure( camp_status ) )
    {
        fprintf( stderr, "camp_srv: failed to initialize log file '%s'\n", camp_getFilemaskGeneric(CAMP_SRV_LOG_FILE) );
    }

    _camp_log( "command-line arguments: camp_debug=0x%08x, camp_rpc_serial=%d, camp_max_threads=%d", camp_debug, camp_rpc_serial, camp_max_threads );

    init_rpc_thread_mode();
    init_max_threads();

#if CAMP_MULTITHREADED

    /* 
     *  initialize multithreading
     */
    pthread_once( &t_init_once_block, srv_init_thread_once );

    /*
     *  initialize the main thread
     *
     *  In Multithreaded mode the main thread 
     *  dispatches RPC request to SVC threads, and starts Poll threads
     *  to do variable polling.
     */
    camp_status = srv_init_main_thread();
    if( _failure( camp_status ) )
    {
        camp_log( "%s", camp_getMsg() );
        exit( camp_status );
    }

#endif /* CAMP_MULTITHREADED */

    /*
     *  initialize rpc program
     */
    camp_status = srv_init_rpc();
    if( _failure( camp_status ) )
    {
        camp_log( "%s", camp_getMsg() );
        exit( camp_status );
    }

    /*
     *  Initialize server
     */
    camp_status = srv_init();
    if( _failure( camp_status ) )
    {
        camp_log( "%s", camp_getMsg() );
        exit( camp_status );
    }

    if( camp_isMsg() ) { camp_log( "%s", camp_getMsg() ); camp_setMsg( "" ); }

    /*
     *  Go into server loop
     */
    if( ( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_SERIAL_IN_SINGLE ) || /* serial rpc in single-threaded server */
	( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET ) ) /* we spawn svc threads locally */
    {
	srv_loop_main_thread(); // main thread manages rpc calls and polling/alarms (no main2 thread)
    }
    else if( 
	( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI ) || /* serial rpc in multi-threaded server */
	( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN ) ) /* svc_run spawns svc threads */
    {
	camp_status = srv_start_main2_thread(); // main2 thread manages polling/alarms
	if( _failure( camp_status ) )
	{
	    camp_log( "%s", camp_getMsg() );
	    exit( camp_status );
	}

	srv_loop_main1_thread(); // main1 thread manages rpc calls
    }

    /*
     *  Clean up before exiting
     */
    srv_shutdown();

    // exit( 0 );
    return( 0 );

    // fprintf( stderr, "srv_loop returned\n" );
    // exit( CAMP_FAILURE );
    // return( CAMP_FAILURE );
}


//    CAMP_RPC_THREAD_MODE: 
//        0: (SERIAL) rpc calls served in serial (synchronously) (e.g., TSRPC/TIRPC on linux)
//          - use this with CAMP_MULTITHREADED=(0 or 1) and CAMP_RPC_LIBRARY=(TSRPC or TIRPC)
//        1: (THREADED_SVC_GETREQSET) rpc calls served from threads started from svc_getreqset/my_svc_getreqset (e.g., TSRPC on vxworks)
//          - use this with CAMP_MULTITHREADED=1 and CAMP_RPC_LIBRARY=TSRPC
//          - a thread is spawned for each rpc call
//          - svc_getargs and svc_sendreply done in the main thread
//        2: (THREADED_SVC_RUN) rpc calls served from threads spawned automatically by svc_run (e.g., MTRPC)
//          - use this with CAMP_MULTITHREADED=1 and CAMP_RPC_LIBRARY=MTRPC
//          - a thread is spawned for each client connection
//          - svc_getargs and svc_sendreply done in the spawned thread
void init_rpc_thread_mode( void )
{
/*
 *  CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET is only sensible with CAMP_MULTITHREADED
 */
// #if ( CAMP_RPC_THREAD_MODE == CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET ) && !CAMP_MULTITHREADED
// #error "don't use CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET without CAMP_MULTITHREADED" 
// #endif

/*
 *  CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN is only sensible with CAMP_MULTITHREADED
 */
// #if ( CAMP_RPC_THREAD_MODE == CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN ) && !CAMP_MULTITHREADED
// #error "don't use CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN without CAMP_MULTITHREADED" 
// #endif

/*
 *  CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI is only sensible with CAMP_MULTITHREADED
 */
// #if ( CAMP_RPC_THREAD_MODE == CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI ) && !CAMP_MULTITHREADED
// #error "don't use CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI without CAMP_MULTITHREADED"
// #endif

// #if ( CAMP_RPC_THREAD_MODE == CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN ) && defined( VXWORKS )
// #error "don't use CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN with VXWORKS"
// #endif

    camp_rpc_thread_mode = -1;

    if( CAMP_MULTITHREADED && !camp_rpc_serial )
    {
	// service rpc calls in parallel

#if CAMP_MTRPC
	camp_rpc_thread_mode = CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN;
#endif
#ifdef VXWORKS
	camp_rpc_thread_mode = CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET;
#endif

	if( camp_rpc_thread_mode == -1 )
	{
	    _camp_log( "warning: parallel rpc handling not supported for this build" );
	}
    }

    if( camp_rpc_thread_mode == -1 )
    {
	// service rpc calls in serial

	// all rpc libraries should be able to handle this option

	camp_rpc_thread_mode = CAMP_MULTITHREADED ? CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI : CAMP_RPC_THREAD_MODE_SERIAL_IN_SINGLE;
    }
}

void init_max_threads( void )
{
    if( camp_max_threads <= 0 )
    {
#if defined( VXWORKS )
	camp_max_threads = 10; // 4mb mvme162's, each thread has a stack of ~30kb
#else // !VXWORKS
	camp_max_threads = 42;
#endif // VXWORKS
    }

    //  sanity check

    if( ( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI ) || /* serial rpc in multi-threaded server */
	( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN ) ) /* svc_run spawns svc threads */
    {
	/*
	 *  require a main2 thread
	 */
	if( camp_max_threads < 3 )
	{
	    _camp_log( "warning: rpc thread mode requires at least 3 threads" );
	    camp_max_threads = 3;
	}
    }
    else if( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET )
    {
	if( camp_max_threads < 2 )
	{
	    _camp_log( "warning: rpc thread mode requires at least 2 threads" );
	    camp_max_threads = 2;
	}
    }
}

/*
 *  Name:       srv_init
 *
 *  Purpose:    Initialize CAMP server database, Tcl interpreter
 *
 *  Called by:  main
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              CAMP server should be initialized as an RPC server and
 *              should have thread properties initialized (for multithreaded
 *              implementation).
 *
 *  Outputs:    CAMP status
 *
 *  Postconditions:
 *              Server now ready to enter (infinite) processing loop
 *
 *  Revision history:
 *
 */
int
srv_init( void )
{
    int camp_status;
    RES* pRes;
    FILE_req req;

    /* 
     *  Tcl interpreter initialization
     */
    if( _camp_debug(CAMP_DEBUG_SRV_INIT) ) { _camp_log( "begin initializing Tcl" ); }

    camp_status = camp_tclInit();
    if( _failure( camp_status ) )
    {
        if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_tclInit" ); }
        return( camp_status );
    }

    if( _camp_debug(CAMP_DEBUG_SRV_INIT) ) { _camp_log( "done initializing Tcl" ); }

    /* 
     *  System structure initialization
     */
    camp_status = sys_init();
    if( _failure( camp_status ) ) 
    {
        if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed sys_init" ); }
        return( camp_status );
    }

    if( do_auto_save )
    {
        bzero( (void*)&req, sizeof( req ) );
        req.flag = 0;
        req.filename = camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE);

        /* 
         *  Load the CAMP configuration file
         *
         *  Careful - some routines (reading/writing instruments)
         *            expect the global thread lock to be ON
	 *
	 *  20140219  TW  global mutex scheme change, shouldn't be necessary here
	 *                (lower-level functions will release only if taken)
         */
        // mutex_lock_global_once(); // lock global when doing sysLoad

	if( _camp_debug(CAMP_DEBUG_SRV_INIT) ) { _camp_log( "begin restoring saved configuration" ); }

        pRes = campsrv_sysload_10400( &req, NULL );

	if( _camp_debug(CAMP_DEBUG_SRV_INIT) ) { _camp_log( "done restoring saved configuration" ); }

        // mutex_unlock_global_once();

        _free( pRes );
    }

    return( CAMP_SUCCESS );
}


/*
 *  Name:       srv_shutdown
 *
 *  Purpose:    Clean up before exiting
 *
 *  Called by:  main
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              Server received shutdown request and ready to exit
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              Server should exit after returning from this routine
 *
 *  Revision history:
 *
 */
void 
srv_shutdown( void )
{
    int camp_status;

#if CAMP_MULTITHREADED
   /*
    *  Wait for other threads to stop
    */
    set_thread_executing( 1 ); // before global lock
    mutex_lock_global_once(); 
    _camp_log( "waiting for threads to stop executing" );
    wait_no_thread_executing( 1 ); // wait no thread executing, and stop any new rpc calls (rpc thread mode 2)
    _camp_log( "waiting for threads to stop running, and finishing rpc replies" );
    kill_all_threads(); // wait no thread running, and finish replies to rpc calls
    mutex_unlock_global_once(); 
    _camp_log( "secondary threads stopped" );
#endif /* CAMP_MULTITHREADED */

    /*
     *  write camp.cfg
     */
    if( camp_ever_had_inst )
    {
        _camp_log( "writing config" );
        camp_status = campCfg_write( camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) );
        if( _failure( camp_status ) ) 
        {
            camp_log( "%s", camp_getMsg() );
            _camp_log( "failed writing config '%s'", camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) );
        }
    }
    else
    {
        _camp_log( "skip writing blank config" );
    }
    /*
     *  Clean up
     */
    srv_shutdown_rpc();
    srv_delAllInss();
    sys_free();
    camp_tclEnd();
#if CAMP_MULTITHREADED
    srv_end_thread(); // clean up srv_init_thread_once
#endif /* CAMP_MULTITHREADED */
    _camp_log( "shutdown complete" );
    camp_stopLog();
    camp_shutdown_filemasks();
}


/*
 *  Name:       srv_loop
 *
 *  Purpose:    CAMP server processing loop
 *
 *              Loop continuously doing regular server tasks, processing RPC
 *              requests and checking for a shutdown condition.  The loop
 *              has a delay to be nice to system resources.
 *
 *  Called by:  main
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              CAMP server initialized completely
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              CAMP server should exit on returning from this routine
 *
 *  Revision history:
 *
 */
void
srv_loop_main_thread( void )
{
    for( ;; )
    {
	/*
	 *  lock global while:
	 *    - receiving RPC calls, and starting svc threads
	 *
	 *  20140303  TW  moved this out around both srv_loop_local_tasks and svc_run_once
	 *                (previously had multiple lock/unlock within srv_loop_local_tasks, at various depths)
	 */

	set_thread_executing( 1 ); // before global lock
	mutex_lock_global_once(); 

	/*
	 *  Do regular server tasks
	 */
	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "srv_loop calling srv_loop_local_tasks" ); }

	srv_loop_local_tasks();

	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "done srv_loop_local_tasks" ); }

	/*
	 *  Dispatch pending RPC calls (i.e., from the CAMP CUI, camp_cmd
	 *  or archiver).  Note that svc_run_once is called with the global
	 *  thread locked.  This is necessary because the RPC library cannot
	 *  necessarily be trusted for re-entrancy.  The condition of the global
	 *  lock on within this call is assumed for routines dispatched by
	 *  svc_run_once which may unlock the global lock at certain times during
	 *  long waiting periods.  This is a very important point to understand
	 *  the operation of the CAMP server.
	 *
	 *  22-Dec-1999  TW  Now only dispatch RPCs if there is a free thread
	 *                   available to service the RPC.  Also, svc_run_once
	 *                   now only dispatches at most one RPC per call.  So,
	 *                   if two RPCs are pending, the second will not be
	 *                   serviced until the next call to svc_run_once.  The
	 *                   try_find_free_thread_data mechanism will thus work when
	 *                   there is more than one pending RPC.
	 */
	if( !CAMP_MULTITHREADED
#if CAMP_MULTITHREADED
	    || ( try_find_free_thread_data( CAMP_THREAD_TYPE_SVC ) > -1 ) 
#endif
	    )
	{
	    svc_run_once(); // check for incoming rpc calls
	}
	else
	{
	    if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "No free threads, skipping svc_run_once" ); }
	}
	
	mutex_unlock_global_once();
	set_thread_executing( 0 );

	/*
	 *  See if we should shutdown
	 */
	if( check_shutdown() ) break;

	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "srv_loop calling camp_fsleep..." ); }

	/*
	 *  Delay 10 ms to take the load off
	 *  16.67 ms when we're on an MVME with VxWorks (1 tick = 1/60 seconds)
	 */
	camp_fsleep( 0.01 ); // for VxWorks this calls taskDelay 

	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "done sleep, end of srv_loop" ); }
    }
}


/*
 *  main thread loop, when there are two main threads
 */
void
srv_loop_main1_thread( void )
{
    svc_run();
}


/*
 *  main2 thread loop, when main1 thread is in svc_run
 */
void
srv_loop_main2_thread( void )
{
    for( ;; )
    {
	set_thread_executing( 1 ); // before global lock
	mutex_lock_global_once(); 

	/*
	 *  Do regular server tasks
	 */
	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "srv_loop calling srv_loop_local_tasks" ); }

	srv_loop_local_tasks();

	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "done srv_loop_local_tasks" ); }
	
	mutex_unlock_global_once();
	set_thread_executing( 0 );

	/*
	 *  See if we should shutdown
	 */
	if( check_shutdown() ) break;

	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "srv_loop calling camp_fsleep..." ); }

	/*
	 *  Delay 10 ms to take the load off
	 *  16.67 ms when we're on an MVME with VxWorks (1 tick = 1/60 seconds)
	 */
	camp_fsleep( 0.01 ); // for VxWorks this calls taskDelay 

	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "done sleep, end of srv_loop" ); }
    }

    /*
     *  Clean up before exiting
     */
    srv_shutdown();

    exit( 0 );
}


/*
 *  Name:       srv_loop_local_tasks
 *
 *  Purpose:    Process regular server tasks which are checked continuously
 *              throughout the life of the server.
 *
 *  Called by:  srv_loop
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              Server initialized as RPC server.
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *
 *  Revision history:
 *    20140303  TW  move global lock out to caller
 */
void
srv_loop_local_tasks( void )
{
    static time_t time_last_auto_save = 0; // safe: only the main thread uses this
    time_t time_now;
    int camp_status;

    if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "calling srv_do_alarms..." ); }

    /*
     *  check status of alarms
     *
     *  20140305  TW  check alarms here, instead of from varSetAlert
     *                - this eliminates possible deadlocks between the mainInterp
     *                  and instrument mutexes
     */

    srv_do_alarms(); 

    if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "calling srv_do_polling..." ); }

    /*
     *  process all pending polling requests 
     */

    srv_do_polling(); // REQ_doPending(); // 20140303  TW

    if( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET )
    {
	if( _camp_debug(CAMP_DEBUG_SRV_LOOP) ) { _camp_log( "calling srv_handle_served_svc_mode1_threads..." ); }

	/*
	 *  17-Dec-1999  TW  Main thread makes RPC replies for svc threads
	 *                   and then cleans up after them.  This means
	 *                   that the main thread is the only thread that 
	 *                   makes RPC calls which is appropriate for the 
	 *                   VxWorks implementation of ONC RPC.
	 */
	srv_handle_served_svc_mode1_threads();
    }

    /*
     *  Autosave the CAMP configuration file at regular time intervals, but only
     *  if the server has ever had an instrument (don't erase old configuration
     *  when there are troubles).
     */
    if( do_auto_save )
    {
        /*
         *  Set initial time_last_auto_save so that an auto save
         *  is not done at startup
         */
        if( time_last_auto_save == 0 )
        {
            time( &time_last_auto_save );
        }
    
        if( camp_ever_had_inst && ( time( &time_now ) - time_last_auto_save ) > AUTO_SAVE_INTERVAL )
        {
            time_last_auto_save = time_now;
    
            /*
             *  Write the CAMP configuration file
             *
             *  The global lock should be on to be safe while doing
             *  file I/O and accessing the entire CAMP database.
             */
            camp_status = campCfg_write( camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) );
	    if( _failure( camp_status ) ) 
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) 
		{
		    _camp_appendMsg( "failed campCfg_write '%s'", camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) );
		}
		camp_log( "%s", camp_getMsg() );
	    }
        }
    }
}
