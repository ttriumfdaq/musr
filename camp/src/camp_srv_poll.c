/*
 *  Name:       camp_srv_poll.c
 *
 *  Purpose:    manage poll threads
 *
 *  Called by:  
 * 
 *  Revision history:
 *
 *    20140306  TW  extracted from camp_srv_svc_mod.c (camp_srv_svc.c)
 *    20151120  DA  check_do_poll clean up on failure
 *    20180604  DA  Start fewer poll threads, to reduce server overload
 *    20191212  DA  re-check poll scheduling just before read
 */

#include <math.h>
#include "camp_srv.h"

static void choose_next_poll( CAMP_VAR* pVar );
static double poll_time_diff( CAMP_VAR* pVar );

#if CAMP_MULTITHREADED

static int count_poll_threads( CAMP_VAR* pVar );
static void start_poll_thread( CAMP_VAR* pVar );
static pthread_addr_t poll_thread( pthread_addr_t arg );

#ifdef VXWORKS
#define MAX_POLL_THREAD_PER_INS 2
#else
#define MAX_POLL_THREAD_PER_INS 3
#endif /* VXWORKS */

#else // not CAMP_MULTITHREADED

#define MAX_POLL_THREAD_PER_INS 1

#endif // CAMP_MULTITHREADED


/*
 *  srv_do_polling
 *
 *  this replaces REQ_doPending as of 201403 (start poll for any due variable)
 *  DA changed 20180604 to reduce server bog-down:
 *  Start at most one poll thread per instrument per cycle, and the var is the 
 *  one most overdue to be polled.
 *  Only allow MAX_POLL_THREAD_PER_INS poll threads per instrument.
 */
// static vars that we only trust as long as we hold the global lock
static CAMP_VAR* pnextPollVar = NULL;
static double pollingOverdue = 0.0;

void
srv_do_polling()
{
    CAMP_VAR* pIVar;

    // _mutex_lock_begin();

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock

    /*
     *  For each instrument, pick (at most) one var to poll (in this cycle)
    */
    {
        for ( pIVar = pVarList; pIVar != NULL; pIVar = pIVar->pNext )
        {
            /*
             *  Check that the interface is online
             */
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIVar->spec.CAMP_VAR_SPEC_u.pIns->pIF;

		if( ( pIF == NULL ) || !( pIF->status & CAMP_IF_ONLINE ) ) continue;
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

#if CAMP_MULTITHREADED

            if( count_poll_threads( pIVar ) >= MAX_POLL_THREAD_PER_INS ) continue;

#endif // CAMP_MULTITHREADED

            pnextPollVar = NULL;
            pollingOverdue = -1.0;

            if( pIVar->pChild != NULL )
            {
                camp_varDoProc_recursive( choose_next_poll, pIVar->pChild );
            }

            /*
             *  start poll thread (or do the polling in serial)
             */
            if( pnextPollVar != NULL )
            {
                CAMP_VAR* pVar = pnextPollVar;

#if CAMP_MULTITHREADED

                if( _camp_debug(CAMP_DEBUG_POLL) ) 
                {
                    _camp_log( "var:'%s' overdue %.2fs - calling start_poll_thread", pVar->core.path, pollingOverdue); 
                }

                start_poll_thread( pVar );

#else // !CAMP_MULTITHREADED

                int camp_status = CAMP_SUCCESS;
                REQ* pReq = REQ_createPoll( pVar );
                camp_status = (*pReq->procs.retProc)( pReq );

#endif // CAMP_MULTITHREADED
            }
        } // for pIVar loop
    }

//return_:
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // _mutex_lock_end();
}

/*
 * Invoked for each var. Choose the var that is most overdue for polling,
 * recording the var pointer in (static) pnextPollVar and track the time
 * overdue in pollingOverdue.
 */

static void
choose_next_poll( CAMP_VAR* pVar )
{
    double diff;
    // _mutex_lock_begin();
    if( ( (pVar->core.status & CAMP_VAR_ATTR_POLL) == 0 ) || ( pVar->core.pollInterval <= 0.0 ) ) 
    {// pVar is not polled
        goto return_; 
    }

    diff = poll_time_diff( pVar ); // time_current - time_next_poll

    if( diff < 0.0 || diff < pollingOverdue ) goto return_;

#if CAMP_MULTITHREADED

    /*
     *  check that Var doesn't already have a poll thread 
     */
    if( count_poll_threads( pVar ) > 0 ) 
    {
        //if( _camp_debug(CAMP_DEBUG_POLL) ) 
        //{ 
        //    _camp_log( "var:'%s' overdue %.2fs - poll thread exists", pVar->core.path, diff ); 
        //}

        goto return_; 
    }

#endif // CAMP_MULTITHREADED

    pollingOverdue = diff;
    pnextPollVar = pVar;

return_:
    // _mutex_lock_end();
    return;
}


/* 
 * poll_time_diff  -  Calculate time that polling this var is overdue; in (double) seconds.
 *                    Result is positive if polling is required before now, and negative if 
 *                    still pending.
 */
static double poll_time_diff( CAMP_VAR* pVar )
{
    timeval_t time_current;
    timeval_t time_next_poll;
    double diff;

    gettimeval( &time_current );

    /*
     * Base target poll time based on the later of the last poll try or the last
     * setting. Note that vars can be set by reading, polling, setting, or even reading
     * a different var; also, vars might not be set when polled, due to error or just
     * normal function for a polling "iterative engine".
     */
    if( timeval_to_double( &pVar->core.timeLastSet ) > timeval_to_double( &pVar->core.timeLastReadAttempt ) )
    {
        time_next_poll = addtimeval_double( &pVar->core.timeLastSet, pVar->core.pollInterval );
    }
    else
    {
        time_next_poll = addtimeval_double( &pVar->core.timeLastReadAttempt, pVar->core.pollInterval );
    }

    diff = difftimeval( &time_current, &time_next_poll ); // time_current - time_next_poll

    if( diff != diff ) // observed NaN, but should never happen (did see)!
    {
        _camp_log( "Time diff NaN for %ld.%06ld - %ld.%06ld" , (long)time_current.tv_sec, (long)time_current.tv_usec
                 , (long)time_next_poll.tv_sec, (long)time_next_poll.tv_usec );
        diff = 0.0;
    }
    //if( diff >= 0.0 && _camp_debug(CAMP_DEBUG_POLL) ) 
    //{
    //    _camp_log( "var:'%s' time for poll (%.2f-%.2f=%.2f)", pVar->core.path, 
    //               timeval_to_double(&time_current), timeval_to_double(&time_next_poll), diff );
    //}

    return ( diff );
}

#if CAMP_MULTITHREADED
/*
 *  Count existing poll threads.
 *  For pVar NULL, count all threads. For pVar of type instrument, count the 
 *  number of poll threads on that instrument. Else count poll threads for that var.
 */
static int
count_poll_threads( CAMP_VAR* pVar )
{
    int thread_data_index;
    int num_found;

    if( pVar == NULL )
    {
	//  any thread
        //  20191213  DA  Count all running threads, because we don't allocate threads by type anymore
        for( thread_data_index = 0, num_found = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
	{
	    THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	    if( /* ( pThread_data->type == CAMP_THREAD_TYPE_POLL ) && */
		( pThread_data->state == CAMP_THREAD_STATE_RUNNING ) )
	    {// active thread
		++num_found; 
	    }
	}
    }
    else
    {

	for( thread_data_index = 0, num_found = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
	{
	    REQ* pReq;
	    CAMP_VAR* pVar_req;
	    THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	    if( pThread_data->type != CAMP_THREAD_TYPE_POLL ) continue; // not poll thread

	    if( pThread_data->state != CAMP_THREAD_STATE_RUNNING ) 
	    {
                //if( _camp_debug(CAMP_DEBUG_POLL) ) 
                //{	
                //    _camp_log( "count %s poll threads: inactive thread: index:%d handle:0x%lx state:%s", 
                //               pVar->core.path, thread_data_index, pThread_data->thread_handle, 
                //               getStringThreadState(pThread_data->state) );
                //}
		continue; // thread not active 
	    }

	    pReq = pThread_data->pReq;

	    //if( _camp_debug(CAMP_DEBUG_POLL) ) 
	    //{	
                //_camp_log( "count %s poll threads: poll thread: index:%d handle:0x%lx state:%s req: var:'%s' cancel:%d", 
                //           pVar->core.path, thread_data_index, pThread_data->thread_handle, 
                //           getStringThreadState(pThread_data->state), pReq->pVar->core.path, pReq->cancel/* , pReq->pending */ );
	    //}

	    if( pReq == NULL ) continue; // should not be NULL until finished

	    if( pReq->cancel ) continue; // request cancelled (e.g., if instrument deleted before poll thread executes)

	    // if( !pReq->pending ) continue; // request not pending (should not occur with new poll management scheme)

	    pVar_req = pReq->pVar;

	    if( pVar_req == pVar )
            {
                ++num_found;
            }
            else if( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT )
            {   // look for matching ins name at start of var's path /ins/path/elem...
                CAMP_VAR* pVar_ins = varGetpIns( pVar_req );
                if( pVar_ins == pVar )
                {
                    ++num_found;
                }
            }
	}
    }

    return( num_found );
}
#endif // CAMP_MULTITHREADED


/*
 *  pVar_ins = instrument CAMP_VAR
 */
void 
cancel_all_polling_for_instrument( CAMP_VAR* pVar_ins )
{
#if CAMP_MULTITHREADED

    int thread_data_index;

    /*
     *  find poll threads for instrument==pVar_ins, and cancel them
     */

    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
	REQ* pReq;
	CAMP_VAR* pVar_req;
	CAMP_VAR* pVar_req_ins;
	THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	if( pThread_data->type != CAMP_THREAD_TYPE_POLL ) continue; // not poll thread

	if( pThread_data->state != CAMP_THREAD_STATE_RUNNING ) continue; // thread not active 

	pReq = pThread_data->pReq;

	if( pReq == NULL ) continue; // should not occur

	pVar_req = pReq->pVar;

	pVar_req_ins = varGetpIns( pVar_req ); // instrument of pVar_req
	if( pVar_req_ins == NULL ) continue; // should not occur

	if( pVar_req_ins != pVar_ins ) continue; // not the instrument to be cancelled

	if( !pReq->cancel ) pReq->cancel = TRUE;
    }

#endif // CAMP_MULTITHREADED
}


#if CAMP_MULTITHREADED

/*
 *  start_poll_thread  -  start a poll thread, called by srv_do_polling from the main thread
 */
static void
start_poll_thread( CAMP_VAR* pVar )
{
    int thread_data_index;
    pthread_attr_t thread_attr;
    THREAD_DATA* pThread_data;
    REQ* pReq;
    int ern;

    /* 
     *  Allocate thread index for thread.
     *  Note that the global lock is on here, but find_free_thread_data will unlock it
     *  temporarily while acquiring a thread (if there are none available immediately)
     *  TODO: use try_find_free_thread_data instead, but beware of blocking later instruments.
     */
    thread_data_index = find_free_thread_data( CAMP_THREAD_TYPE_POLL , get_mutex_lock_count_global() );

    if ( thread_data_index < 0 )
    { /* this only needed if we use try_find_free_thread_data above */
        if( _camp_debug(CAMP_DEBUG_THREAD|CAMP_DEBUG_POLL) ) 
        {
            _camp_log( "no thread available to poll %s. Skip", pVar->core.path );
        }
        return;
    }

    pReq = REQ_createPoll( pVar ); // allocates pReq; freed by reset_thread_data

    pThread_data = get_thread_data( thread_data_index );

    // 20140218  TW  unnecessary lock: this is the main thread which has found an unused thread
    // if( mutex_lock_thread( &(pThread_data->mutex_handle) ) != 0 )
    // {
    //     _camp_log( "error locking poll thread mutex" );
    //     return;
    // }

    init_thread_data( pThread_data, CAMP_THREAD_TYPE_POLL, CAMP_THREAD_STATE_RUNNING );

    pThread_data->pReq = pReq;

    // if( mutex_unlock_thread( &(pThread_data->mutex_handle) ) != 0 )
    // {
    //     _camp_log( "error unlocking poll thread mutex" );
    //     return;
    // }

    ern = pthread_attr_init( &thread_attr );
    if( ern != 0 )
    {
        _camp_log( "error creating poll thread attributes (var=%s, err=%d)", pReq->pVar->core.path, ern );
        reset_thread_data( pThread_data, CAMP_THREAD_STATE_INIT );
        return;
    }

    /*
     *  Explicitely set to inherit scheduling
     *  policy and priority from creating thread
     */
    ern = pthread_attr_setinheritsched( &thread_attr, PTHREAD_INHERIT_SCHED );
    if( ern != 0 )
    {
        _camp_log( "error setting poll thread attributes (var=%s, err=%d)", pReq->pVar->core.path, ern );
        pthread_attr_destroy( &thread_attr );
        reset_thread_data( pThread_data, CAMP_THREAD_STATE_INIT );
        return;
    }

    // FYI: pthread_attr_setstacksize( &thread_attr, 1024 * 1024 );

    ern = pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED );
    if( ern != 0 )
    {
        _camp_log( "error setting poll thread state (var=%s, err=%d)", pReq->pVar->core.path, ern );
        pthread_attr_destroy( &thread_attr );
        reset_thread_data( pThread_data, CAMP_THREAD_STATE_INIT );
        return;
    }

    ern = pthread_create( &pThread_data->thread_handle, // thread id
                          &thread_attr, // pthread_attr_default // thread attributes
                          poll_thread, // start_routine
                          integerToPointer( thread_data_index ) 
                          );
    if ( ern != 0 )
    {
        _camp_log( "error creating poll thread (var=%s, err=%d)", pReq->pVar->core.path, ern );
        pthread_attr_destroy( &thread_attr );
        reset_thread_data( pThread_data, CAMP_THREAD_STATE_INIT );
        return;
    }

    ern = pthread_attr_destroy( &thread_attr );
    if( ern != 0 )
    {
        _camp_log( "error deleting poll thread attributes (var=%s, err=%d)", pReq->pVar->core.path, ern );
        /* Don't return on this error; just accept possible memory leak */
    }

    /*
     *  Reclaim internal storage for thread
     *  immediately upon thread exit
     *  20140306  TW  use pthread_attr_setdetachstate instead
     */
    //if( pthread_detach( pThread_data->thread_handle ) != 0 )
    //{
    //    _camp_log( "error detaching poll thread" );
    //    return;
    //}

    if( _camp_debug(CAMP_DEBUG_THREAD | CAMP_DEBUG_POLL) ) 
    {
	_camp_log( "created poll thread: index:%d handle:0x%lx pReq:%x var:'%s'"
		      , thread_data_index, pThread_data->thread_handle, pThread_data->pReq, pReq->pVar->core.path );
    }

    return;

}


/*
 *  poll_thread  -  poll thread start point (where a poll thread begins
 *                  execution)
 */
static pthread_addr_t
poll_thread( pthread_addr_t arg )
{
    int thread_data_index;
    THREAD_DATA* pThread_data;
    _mutex_lock_begin();

    thread_data_index = pointerToInteger( arg ); // (int)arg; // 20140221  TW

    if( set_thread_key_to_thread_data( thread_data_index ) != 0 )
    {
    	_camp_log( "error setting poll thread key" );
	exit( CAMP_FAILURE );
    }

    pThread_data = get_thread_data( thread_data_index );

    /*
     *  Tried using this to stop killing threads
     *  that you want to complete before exiting.
     *  Now do it another way.
     */
    // int prevcs;
    // pthread_setcancelstate( PTHREAD_CANCEL_DISABLE, &prevcs);

    set_thread_executing( 1 ); // before global lock
    mutex_lock_global_once(); // lock global when doing poll thread

    // _mutex_lock_varlist_on(); // for pReq->pVar // warning: don't return inside mutex lock
    {
	REQ* pReq;
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	pReq = pThread_data->pReq;
	pVar = pReq->pVar;
	pVar_ins = varGetpIns( pVar );

	_mutex_lock_ins_on( pVar_ins ); // lock instrument

	/*
	 *  Check that pVar is still being polled and that polling is (still) due
	 *  (in case polling was turned off or a reading happened between poll thread 
         *  creation and this execution). (funny !(<) is in case of NaN)
	 */
	if((  pVar->core.status & CAMP_VAR_ATTR_POLL ) && !( poll_time_diff(pVar) < 0.0 ) )
        {
            int camp_status = CAMP_SUCCESS;
            if( _camp_debug(CAMP_DEBUG_POLL) ) 
            {
                _camp_log( "poll_thread: var:'%s' begin read" , pVar->core.path );
            }
            /*
             *  Call the poll request procedure
             */
            camp_status = (*pReq->procs.retProc)( pReq );
            
            //if( camp_status != CAMP_SUCCESS ) // &&
            if( _camp_debug(CAMP_DEBUG_POLL) ) 
            {
                _camp_log( "poll_thread: pollstatus:%x thread: index:%d handle:0x%lx pReq:%x var:'%s'" ,
                           camp_status, thread_data_index, pThread_data->thread_handle, pReq, pVar->core.path );
            }
        }
        else 
        {
            if( _camp_debug(CAMP_DEBUG_POLL) ) 
            {
                _camp_log( "poll_thread: var:'%s' skip read" , pVar->core.path );
            }
        }

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // for pReq->pVar // warning: don't return inside mutex lock

    mutex_unlock_global_once();
    set_thread_executing( 0 );

    _mutex_lock_end();

    /*
     *  Clean up thread-specific data structure
     *
     *  thread_data can be reused by main thread after this point
     */
    reset_thread_data( pThread_data, CAMP_THREAD_STATE_FINISHED ); // do_thread_finished( thread_data_index );

    // pthread_setcancelstate( PTHREAD_CANCEL_ENABLE, &prevcs );
    // pthread_testcancel();

    return( arg ); // Return an address; this program doesn't check it.
}

#endif /* CAMP_MULTITHREADED */
