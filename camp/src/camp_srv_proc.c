/*
 *  Name:       camp_srv_proc.c
 *
 *  Purpose:    Server-side dispatch routines for all possible RPC calls
 *
 *              Parallel to the client-side calls in camp_srv_clnt_mod.c
 *
 *              The routines correspond to routines in camp_api_proc.c
 *              and Tcl commands as documented in the CAMP programmer's guide.
 *
 *  Called by:  In camp_srv_svc_mod.c the RPC call is dispatched by
 *              do_service for the multithreaded version and by camp_srv
 *              for the single-threaded version.
 *
 *              camp_tcl.c (various) calls these routines as entry points
 *              to satisfy various requests that were initiated by Tcl
 *              commands (either from Tcl instrument drivers, or directly
 *              from the command line interface).
 * 
 *  Inputs:     As per all RPC dispatch routines:  a pointer to a data
 *              structure specific to the RPC call type, and an svc_req
 *              structure general to all RPC calls.
 *
 *  Preconditions:
 *              Multithreaded implementation:
 *              The CAMP global lock is assumed to be ON for the processing
 *              of all these routines.  Internal routines called by these
 *              routines may unlock the global lock during periods of long
 *              waiting or to synchronize with the instrument locks.
 *
 *  Outputs:    A pointer to a structure that contains the resultant information
 *              from the RCP call.
 *
 *  Postconditions:
 *              Upon returning from these routines, the request has been
 *              fulfilled (either successfully or unsuccessfully).  SVC threads
 *              that call these routines to satisfy RPC calls may exit
 *              after the call.  There is no further processing to wait for.
 *
 *  $Log: camp_srv_proc.c,v $
 *  Revision 1.14  2018/06/13 00:31:13  asnd
 *  Remove ins locking from inssave.
 *
 *  Revision 1.13  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.10  2013/04/16 08:26:26  asnd
 *  Rationalize configuration saving. Allow return messages to contain "%".
 *
 *  Revision 1.9  2009/04/07 02:05:16  asnd
 *  Fix bug where a var defined with polling on, if followed by some unrelated
 *  error in the instrument defintion, would cause a camp crash (thread lock).
 *  Now clean up pending poll requests.
 *
 *  Revision 1.8  2006/04/27 04:10:58  asnd
 *  Generate more and better error messages.
 *
 *  Revision 1.7  2004/12/18 01:02:28  asnd
 *  Alter debugging messages
 *
 *  Revision 1.6  2001/02/10 04:16:15  asnd
 *  Add insIfDump and insIfUndump for binary-clean data dump from instrument to
 *  a file and from a file to the instrument.
 *
 *  Revision 1.5  2001/01/17 04:16:10  asnd
 *  Debug rs232PortInUse error message
 *  Revision 1.4  2001/01/15 06:48:27  asnd
 *  Remove the bogus message from rs232PortInUse
 *
 *
 *  Revision history:
 *
 *    20140218     TW    moved mutex_un/lock_ins to camp_srv_svc_mod.c
 *    20140217     TW    rename set_global_mutex_noChange to mutex_lock_mainInterp
 *                       rename camp_thread_mutex_lock to mutex_lock
 *                       rename pthread_mutex_unlock to mutex_unlock
 *                       rename thread_un/lock_global_np to mutex_un/lock_global
 *                       rename get/release_ins_thread_lock -> mutex_un/lock_ins
 *    20140214     TW    tidying CAMP_DEBUG
 *    02-FEB-2001  DJA   campsrv_insifdump, campsrv_insifundump
 *    18-Dec-2000  TW    In insadd, delete the instrument cleanly 
 *                       if the Tcl script has an error.
 *    14-Dec-2000  DJA   error message for PortInUse
 *    20-SEP-2000  DJA   use camp_rs232PortInUse
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <signal.h>
#include <ctype.h>

#include "timeval.h"
#include "camp_srv.h"


RES* 
campsrv_sysshutdown_10400( void* dummy, struct svc_req* rq )
{
    RES* pRes;

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    pRes->status = camp_checkInsLockAll( rq );
    if( _failure( pRes->status ) ) 
    {
        _camp_setMsg( "can't shutdown when an instrument is locked" );
        goto return_;
    }

    /* pRes->status = campCfg_write( camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) ); */
    /* if( _failure( pRes->status ) ) */
    /* { */
    /* 	_camp_log( "failed auto-save" ); */
    /* } */
    /* else */
    /* { */
    /* 	_camp_log( "done auto-save" ); */
    /* } */

    _camp_setMsg( "CAMP Server shutting down" );
    pRes->status = CAMP_SUCCESS;

#ifdef VMS
    lib_set_symbol( "CAMP_STATUS", "SHUTDOWN", 1 );
#endif /* VMS */

    /*
     *  Flag main thread to stop all threads and exit
     */
    set_shutdown();

 return_:
    return( pRes );
}


RES* 
campsrv_sysreboot_10400( void* dummy, struct svc_req* rq )
{
    RES* pRes;

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    pRes->status = camp_checkInsLockAll( rq );
    if( _failure( pRes->status ) ) 
    {
        _camp_setMsg( "can't shutdown when an instrument is locked" );
        goto return_;
    }

    /* pRes->status = campCfg_write( camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) ); */
    /* if( _failure( pRes->status ) ) */
    /* { */
    /* 	_camp_log( "failed auto-save" ); */
    /* } */
    /* else */
    /* { */
    /* 	_camp_log( "done auto-save" ); */
    /* } */

    _camp_setMsg( "CAMP Server restarting" );
    pRes->status = CAMP_SUCCESS;

#ifdef VMS
    lib_set_symbol( "CAMP_STATUS", "RESTART", 1 );
#endif /* VMS */

    /*
     *  Flag main thread to stop all threads and exit
     */
    set_shutdown();

 return_:
    return( pRes );
}


/*
 *  load camp.ini (system setup)
 */
RES* 
campsrv_sysupdate_10400( void* dummy, struct svc_req* rq )
{
    RES* pRes;

    wait_no_thread_executing( 1 );

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    pRes->status = sys_update();

    // if( camp_isMsg() ) camp_log( "%s", camp_getMsg() );

//return_:
    wait_no_thread_executing( 0 );
    return( pRes );
}


/*
 *  load camp.cfg (instruments)
 *
 *  load a new configuration file, optionally deleting all instruments beforehand
 */
RES* 
campsrv_sysload_10400( FILE_req* pFile_req, struct svc_req* rq )
{
    RES* pRes;
    FILE* fin = NULL;
    char filename[LEN_FILENAME+1];
    _mutex_lock_begin();

    wait_no_thread_executing( 1 );

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    /*
     *  Make sure the file exists
     */
    camp_translate_generic_filename( pFile_req->filename, filename, sizeof( filename ) ); // 20140325
    fin = fopen( filename, "r" );
    if( fin == NULL )
    {
        _camp_setMsg( "failed to open file '%s'", pFile_req->filename );
        pRes->status = CAMP_INVAL_FILE;
	goto return_;
    }
    fclose( fin );

    if( pFile_req->flag == 1 )
    {
        pRes->status = camp_checkInsLockAll( rq );
        if( _failure( pRes->status ) ) 
        {
            _camp_setMsg( "can't load configuration when an instrument is locked" );
            goto return_;
        }
    }

    /*
     *  Delete all current instruments
     */
    if( pFile_req->flag == 1 )
    {
        srv_delAllInss();
    }

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	camp_strdup( &pSys->pDyna->cfgFile, pFile_req->filename ); // free and strdup
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    /*
     *  Interpret the file using Tcl
     *  Lock the main interpreter mutex
     */
    _mutex_lock_mainInterp_on(); 
    {
	int tcl_status = TCL_OK;
	// char filename[LEN_FILENAME+1];
	Tcl_Interp* interp = camp_tclInterp();

	camp_translate_generic_filename( pFile_req->filename, filename, sizeof( filename ) ); // 20140325

	tcl_status = Tcl_EvalFile( interp, filename );

	if( tcl_status == TCL_ERROR )
	{
	    {
		char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

		assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

		camp_setMsg( "%s", tcl_message );

		free( tcl_message );
	    }

	    if( _camp_debug(CAMP_DEBUG_TRACE) ) 
	    {
		_camp_appendMsg( "failed TclEvalFile '%s'", filename );
	    }
	    else
	    {
		_camp_appendMsg( "failed to load config file '%s'", filename );
	    }

	    _mutex_lock_mainInterp_off(); 
	    pRes->status = CAMP_FAILURE; goto return_;
	}
    }
    _mutex_lock_mainInterp_off(); 

    pRes->status = CAMP_SUCCESS;

 return_:
    wait_no_thread_executing( 0 );
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_syssave_10400( FILE_req* pFile_req, struct svc_req* rq )
{
    RES* pRes;
    const char* filename;
    // _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	filename = pFile_req->filename;
	if ( filename == NULL ) filename = pSys->pDyna->cfgFile;
	if ( filename == NULL ) filename = camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE);
	if ( filename[0] == '\0' ) filename = pSys->pDyna->cfgFile;
	if ( filename[0] == '\0' ) filename = camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE);

	if ( streq( filename, camp_getFilemaskGeneric(CAMP_SRV_AUTO_SAVE) ) ) 
	{   
            /* 
	     *  Save in multiple files, matching the style of auto-save 
	     */
	    pRes->status = campCfg_write( filename );
	    if( _failure( pRes->status ) )
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campCfg_write '%s'", filename ); }
		goto return_;
	    }
	}
	else
	{   
            /* 
	     *  Save in one big file 
	     */
	    pRes->status = campState_write( filename );
	    if( _failure( pRes->status ) )
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campState_write '%s'", filename ); }
		goto return_;
	    }

	    /*
	     *  Record this file name as the cfg file
	     */
	    camp_strdup( &pSys->pDyna->cfgFile, filename ); // free and strdup
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

  return_:
    // _mutex_lock_end();
    return( pRes );
}


CAMP_SYS_res* 
campsrv_sysget_10400( void* dummy, struct svc_req* rq )
{
    CAMP_SYS_res* pRes;

    pRes = (CAMP_SYS_res*)camp_zalloc( sizeof( CAMP_SYS_res ) );

    pRes->pSys = pSys; // note: xdr routine handles lock (xdr_CAMP_SYS_res)

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


SYS_DYNAMIC_res* 
campsrv_sysgetdyna_10400( void* dummy, struct svc_req* rq )
{
    SYS_DYNAMIC_res* pRes;

    pRes = (SYS_DYNAMIC_res*)camp_zalloc( sizeof( SYS_DYNAMIC_res ) );

    pRes->pDyna = pSys->pDyna; // note: xdr routine handles lock (xdr_SYS_DYNAMIC_res)

    pRes->status = CAMP_SUCCESS;
    return( pRes );
}


/*
 *  note: unlike all other RES/_res structures, DIR_res allocates
 *  a linked-list in DIR_res->pDir (i.e., deeper than just the DIR_res
 *  structure itself).
 *
 *  20140304  TW  don't save in pSys->pDir
 */
DIR_res* 
campsrv_sysdir_10400( FILE_req* pFile_req, struct svc_req* rq )
{
    DIR_res* pRes;

    pRes = (DIR_res*)camp_zalloc( sizeof( DIR_res ) );

    /*
     *  Expand the filespec
     */
    // pRes->status = sys_initDir( pFile_req->filename );
    pRes->status = camp_glob( &pRes->pDir, pFile_req->filename );

    if( _failure( pRes->status ) )
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_glob '%s'", pFile_req->filename ); }
        goto return_;
    }

    // pRes->pDir = pSys->pDir; // note: xdr routine handles lock (xdr_DIR_res)

    pRes->status = CAMP_SUCCESS;

 return_:
    return( pRes );
}


RES* 
campsrv_insadd_10400( INS_ADD_req* pInsAdd_req, struct svc_req* rq )
{
    RES* pRes;
    char defFile[LEN_FILENAME+1];
    char path[LEN_PATH+1];
    char* p;
    // int camp_status = CAMP_SUCCESS;
    Tcl_Interp* interp;
    Tcl_Interp* oldInterp;
    FILE* fin = NULL;
    char buf[512];
    char* cmdBuf = NULL;
    int cmdLen;
    int cmdBufLen;
    int i;
    char filename[LEN_FILENAME+1];
    // bool_t done;
    _mutex_lock_begin();
    
    wait_no_thread_executing( 1 );

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	INS_TYPE* pInsType;
	INS_AVAIL* pInsAvail;
	bool_t isAvail;

	/*
	 *  Make sure the instrument type exists
	 */
	pInsType = camp_sysGetpInsType( pInsAdd_req->typeIdent /* , mutex_lock_sys_check() */ );
	if( pInsType == NULL )
	{
	    _camp_setMsg( "invalid instrument type '%s'", pInsAdd_req->typeIdent );
	    pRes->status = CAMP_INVAL_INS_TYPE;
	    goto return_;
	}

	/*
	 *  determine instrument definition file name
	 *
	 *  Only Tcl drivers
	 */
	if( streq( pInsType->driverType, "Tcl" ) )
	{
	    camp_snprintf( defFile, sizeof( defFile ), camp_getFilemaskGeneric(CAMP_TCL_PFMT), pInsAdd_req->typeIdent ); // -> camp_ins_<typeIdent>.tcl
	}
	else
	{
	    _camp_setMsg( "invalid instrument driver type '%s'", pInsType->driverType );
	    pRes->status = CAMP_FAILURE;
	    goto return_;
	}

	/*
	 *  Make sure the instrument DOESN'T exist
	 */
	camp_pathInit( path, LEN_PATH+1 );
	camp_pathDown( path, LEN_PATH+1, pInsAdd_req->ident );
	if( camp_varExists( path ) )
	{
	    _camp_setMsg( "instrument '%s' already exists", path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	/*
	 *  check validity of ident
	 */
	if( ( strlen( pInsAdd_req->ident ) < 1 ) || 
	    ( strlen( pInsAdd_req->ident ) > LEN_IDENT ) )
	{
	    _camp_setMsg( "invalid instrument identifier '%s'", pInsAdd_req->ident );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	for( p = pInsAdd_req->ident; *p != '\0'; p++ )
	{
	    if( !isalnum( *p ) && ( *p != '_' ) && ( *p != '-' ) )
	    {
		_camp_setMsg( "invalid instrument identifier '%s'", pInsAdd_req->ident );
		pRes->status = CAMP_INVAL_INS;
		goto return_;
	    }
	}

	/* 
	 *  Check if this is an "available" instrument
	 */
	isAvail = FALSE;
	for( pInsAvail = pSys->pInsAvail; 
	     pInsAvail != NULL; 
	     pInsAvail = pInsAvail->pNext )
	{
	    if( ( pInsAvail->ident[0] == pInsAdd_req->ident[0] ) && 
		( streq( pInsAvail->ident, pInsAdd_req->ident ) ) )
	    {
		if( !streq( pInsAvail->typeIdent, pInsAdd_req->typeIdent ) )
		{
		    _camp_setMsg( "wrong type '%s' for known ident '%s'", pInsAdd_req->typeIdent, pInsAdd_req->ident );
		    pRes->status = CAMP_FAILURE;
		    goto return_;
		}
		else
		{
		    isAvail = TRUE;
		    break;
		}
	    }
	}

	/*
	 *  set global variable "current_ident" to unique ident of instrument.
	 *  this ident replaces the "~" in the definition file.
	 */
	set_current_ident( pInsAdd_req->ident );

	camp_translate_generic_filename( defFile, filename, sizeof( filename ) ); // 20140325 
	fin = fopen( filename, "r" );
	if( fin == NULL )
	{
	    _camp_setMsg( "failed to open def'n file '%s'", defFile );
	    pRes->status = CAMP_FAILURE;
	    goto return_;
	}

	cmdBufLen = 512;
	cmdBuf = (char*)camp_zalloc( cmdBufLen );

	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    CAMP_VAR* pVar_ins; // pointer to instrument CAMP_VAR
	    CAMP_INSTRUMENT* pIns;

	    /*
	     *  Interpret lines in the definition file until
	     *  the instrument has been defined, then stop.
	     *  This should be just the first noncomment line
	     *  (including continuation lines).
	     */
	    for( pVar_ins = NULL; pVar_ins == NULL; pVar_ins = camp_varGetp( path /* , mutex_lock_varlist_check() */ ) )
	    {
		cmdLen = 0;
		cmdBuf[0] = '\0';
		/*
		 *  Read a line (including continuation lines)
		 */
		for(;;)
		{
		    if( fgets( buf, 512, fin ) == NULL )
		    {
			_camp_setMsg( "premature end-of-file in def'n file '%s'", defFile );
			pRes->status = CAMP_FAILURE;
			goto return_;
		    }
		    cmdLen += strlen( buf );
		    /*
		     *  Make buffer bigger if necessary
		     */
		    if( cmdLen + 1 > cmdBufLen )
		    {
			cmdBufLen += 512;
			cmdBuf = camp_rezalloc( cmdBuf, cmdBufLen );
		    }
		    camp_strncat( cmdBuf, cmdBufLen, buf ); // strcat( cmdBuf, buf ); // terminates, size includes terminator
		    /*
		     *  Check if the line is continued
		     *  look for last non-whitespace character
		     */
		    for( i = cmdLen-1; ( i > 0 ) && ( !isgraph( cmdBuf[i] ) ); i-- ) ;
		    if( cmdBuf[i] != '\\' ) break;
		}

		/*
		 *  Interpret the line using the main interpreter
		 *  Lock the main interpreter mutex
		 */
		if( cmdLen > 0 )
		{
		    int tcl_status = TCL_OK;

		    /*
		     *  warning: don't lock_mainInterp within lock_ins
		     *  (could cause deadlock)
		     */
		    _mutex_lock_mainInterp_on();
		    {
			Tcl_Interp* interp = camp_tclInterp();

			tcl_status = Tcl_Eval( interp, cmdBuf );

			if( tcl_status == TCL_ERROR )
			{
			    char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

			    assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

			    camp_setMsg( "%s", tcl_message );
			    _camp_appendMsg( "failed reading instrument %s driver", pInsAdd_req->ident );
			    
			    free( tcl_message );
			}
		    }
		    _mutex_lock_mainInterp_off();

		    if( tcl_status == TCL_ERROR )
		    {
			/*
			 *  18-Dec-2000  TW  Try to delete the instrument cleanly
			 */
			camp_insDel( path );
                
			pRes->status = CAMP_FAILURE;
			goto return_;
		    }
		}
	    }

	    /*
	     *  Instrument not fully defined, but get the lock
	     *  to make sure nobody tries anything before it is
	     *  defined.
	     */
	    _mutex_lock_ins_on( pVar_ins ); 

	    pIns = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns;

	    /*
	     *  Interpret the rest of the file
	     *  (very similar to Tcl_EvalFile)
	     */
	    cmdLen = 0;
	    cmdBuf[0] = '\0';

	    /*
	     *  Read the whole file
	     *  Could improve this using read() with larger blocks
	     */
	    while( fgets( buf, 512, fin ) != NULL )
	    {
		cmdLen += strlen( buf );
		/*
		 *  Make buffer bigger if necessary
		 */
		if( cmdLen + 1 > cmdBufLen )
		{
		    cmdBufLen += 10240;
		    cmdBuf = camp_rezalloc( cmdBuf, cmdBufLen );
		}
	
		camp_strncat( cmdBuf, cmdBufLen, buf );
	    }

	    /*
	     *  Interpret the file using the instrument interpreter
	     */
	    if( cmdLen > 0 )
	    {
		int tcl_status = TCL_OK;

		oldInterp = get_thread_interp();
		set_thread_interp( pIns->interp );
		interp = pIns->interp;
	
		tcl_status = Tcl_Eval( interp, cmdBuf );

		set_thread_interp( oldInterp );

		if( tcl_status == TCL_ERROR )
		{
		    {
			char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks
		
			assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

			camp_setMsg( "%s", tcl_message );
			_camp_appendMsg( "failed reading instrument %s driver", pInsAdd_req->ident );

			free( tcl_message );
		    }

		    /*
		     *  18-Dec-2000  TW  Try to delete the instrument cleanly
		     *  06-Apr-2009  DA  Cancel all pending requests as part of deleting cleanly.
		     *  20140303     TW  no need to cancel REQs here with new method, but doesn't hurt
		     *                   (because no other thread can interrupt instrument addition to 
		     *                   create a poll thread)
		     */
		    cancel_all_polling_for_instrument( pVar_ins ); 

		    _mutex_lock_ins_off( pVar_ins ); 

		    camp_insDel( pVar_ins->core.path );

		    pRes->status = CAMP_FAILURE;
		    goto return_;
		}
	    }

	    fclose( fin );  fin = NULL;
	    _free( cmdBuf );

	    /*
	     *  initialize things that aren't defined by def file
	     */
	    pIns->class = INS_ROOT;
	    camp_strdup( &pIns->defFile, defFile ); // free and strdup
	    camp_strdup( &pIns->typeIdent, pInsAdd_req->typeIdent ); // free and strdup
	    pIns->typeInstance = sys_getTypeInstance( pInsAdd_req->typeIdent );

	    gettimeval( &pSys->pDyna->timeLastInsChange );

	    {
		int camp_status = camp_insInit( pVar_ins );
		if( _failure( camp_status ) ) 
		{
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "warning: failed camp_insInit '%s'", pVar_ins->core.path ); }
		}
	    }

	    if( isAvail )
	    {
		int tcl_status = TCL_OK;

		/*
		 *  Interpret the initProc defined in camp.ini
		 *  using the instrument interpreter
		 */
		oldInterp = get_thread_interp();
		set_thread_interp( pIns->interp );
		interp = pIns->interp;
	
		tcl_status = Tcl_Eval( interp, pInsAvail->specInitProc );

		set_thread_interp( oldInterp );

		/*
		 *  not fatal 
		 */

		if( tcl_status == TCL_ERROR )
		{
		    char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

		    assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

		    // append: not fatal
		    camp_appendMsg( "%s", tcl_message );
		    _camp_appendMsg( "warning: failed initProc for instrument %s", pInsAdd_req->ident );

		    free( tcl_message );
		}
	    }
    
	    _mutex_lock_ins_off( pVar_ins ); 
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

  return_:
    if( fin != NULL ) { fclose( fin ); fin = NULL; }
    _free( cmdBuf );
    wait_no_thread_executing( 0 );
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insdel_10400( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    wait_no_thread_executing( 1 );

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVar_req->path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS; goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS; goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	/*
	 *  Cancel all pending polls
	 */
	cancel_all_polling_for_instrument( pVar );

	/*
	 *  Tell instrument driver to shut down
	 */
	pRes->status = camp_insDelete( pVar );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_insDelete '%s'", pVar->core.path ); }
	    // else { _camp_appendMsg( "failed to delete instrument '%s'", pVar->core.path ); }

	    // pRes->status = CAMP_FAILURE;
	    // _mutex_lock_ins_off( pVar );
	    // goto return_;
	}

	_mutex_lock_ins_off( pVar );

	/*
	 *  Free local memory and var list entry
	 */
	pRes->status = camp_insDel( pVar->core.path );
	if( _failure( pRes->status ) )
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_insDel '%s'", pVar->core.path ); }
	    // else { _camp_appendMsg( "failed to delete instrument '%s'", pVar->core.path ); }

	    // pRes->status = CAMP_FAILURE;
	    // goto return_;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	gettimeval( &pSys->pDyna->timeLastInsChange );
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    wait_no_thread_executing( 0 );
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_inslock_10400( INS_LOCK_req* pInsLock_req, struct svc_req* rq )
{
    RES* pRes;
    struct authunix_parms* aup;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;
	char* path;

	path = pInsLock_req->dreq.path;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	aup = (struct authunix_parms*)rq->rq_clntcred;
	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	/*
	 *  Lock instrument for calling PID and host (or unlock)
	 */
	if( pInsLock_req->flag )
	{
#ifdef VMS
	    char host[LEN_NODENAME+1];
	    char camp_server_host[LEN_NODENAME+1];

	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		camp_strncpy( camp_server_host, sizeof(camp_server_host), pSys->hostname );
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	    /*
	     *  Allow only local processes to lock (no remote)
	     */
	    camp_strncpy( host, sizeof( host ), aup->aup_machname );
	    stolower( host );
	    if( !streq( camp_server_host, host ) )
	    {
		_camp_setMsg( "cannot lock instrument from remote node" );
		pRes->status = CAMP_FAILURE;
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }
#endif /* VMS */

	    switch( aup->aup_gids[1] )
	    {
	    case CAMP_OS_VMS:
		camp_strdup( &pIns->lockOs, "vms" ); // free and strdup
		break;
	    case CAMP_OS_BSD:
		camp_strdup( &pIns->lockOs, "bsd" ); // free and strdup
		break;
	    default:
		_camp_setMsg( "unknown or invalid operating system on remote node" );
		pRes->status = CAMP_FAILURE;
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }
	    pIns->lockPID = aup->aup_gids[0];
	    camp_strdup( &pIns->lockHost, stolower( aup->aup_machname ) ); // free and strdup

	    pVar->core.status |= CAMP_INS_ATTR_LOCKED;
	}
	else
	{
	    pIns->lockPID = 0;
	    camp_strdup( &pIns->lockHost, "" ); // free and strdup
	    camp_strdup( &pIns->lockOs, "" ); // free and strdup

	    pVar->core.status &= ~CAMP_INS_ATTR_LOCKED;
	}

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insline_10400( INS_LINE_req* pInsLine_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsLine_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsg( "variable is not an instrument ('%s', '%s', '%s')", pVar->core.ident, path, pVar->core.path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	if( pInsLine_req->flag )
	{
	    pRes->status = camp_insOnline( pVar );
	    if( _failure( pRes->status ) ) 
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_insOnline '%s'", pVar->core.path ); }
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }
	}
	else
	{
	    pRes->status = camp_insOffline( pVar );
	    if( _failure( pRes->status ) ) 
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_insOffline '%s'", pVar->core.path ); }
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }
	}

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


/*
 *  load instrument ini file
 */
RES* 
campsrv_insload_10400( INS_FILE_req* pInsFile_req, struct svc_req* rq )
{
    RES* pRes;
    FILE* fin = NULL;
    char* path;
    Tcl_Interp* interp;
    Tcl_Interp* oldInterp;
    char filename[LEN_FILENAME+1];
    int tcl_status = TCL_OK;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsFile_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( pInsFile_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	if( ( pInsFile_req->datFile == NULL ) || 
	    ( strlen( pInsFile_req->datFile ) == 0 ) )
	{
	    _camp_setMsg( "invalid filename '%s'", pInsFile_req->datFile );
	    pRes->status = CAMP_INVAL_FILE;
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	/*
	 *  Make sure the file exists
	 */
	camp_translate_generic_filename( pInsFile_req->datFile, filename, sizeof( filename ) ); // 20140325
	fin = fopen( filename, "r" );
	if( fin == NULL )
	{
	    _camp_setMsg( "failed opening file '%s'", pInsFile_req->datFile );
	    pRes->status = CAMP_INVAL_FILE;
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}
	fclose( fin );

	/*
	 *  set global variable "current_ident" to unique ident of instrument.
	 *  this ident replaces the "~" in the ini file.
	 *  20140303  TW  is this necessary? don't ini files have hardcoded instrument ident?
	 *  NO they do not!  That is a feature!
	 */
	set_current_ident( pVar->core.ident );

	/*
	 *  Use the instrument's Tcl interpreter
	 */
	oldInterp = get_thread_interp();
	set_thread_interp( pIns->interp );
	interp = pIns->interp;

	/*
	 *  Interpret the file using Tcl
	 */
	// camp_translate_generic_filename( pInsFile_req->datFile, filename, sizeof( filename ) ); // 20140325 done above
	tcl_status = Tcl_EvalFile( interp, filename );

	set_thread_interp( oldInterp );

	if( tcl_status == TCL_ERROR )
	{
	    {
		char* tcl_message = (char*)camp_malloc( MAX_LEN_MSG+1 ); tcl_message[0] = '\0'; // on heap for stack-limited vxworks

		assemble_tcl_message( interp, tcl_message, MAX_LEN_MSG+1 );

		camp_setMsg( "%s", tcl_message );
		_camp_appendMsg( "failed loading ini file '%s' for instrument '%s'", pInsFile_req->datFile, pVar->core.ident );
	    
		free( tcl_message );
	    }

	    pRes->status = CAMP_FAILURE;
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	/*
	 *  Set the var file name
	 */
	if( pInsFile_req->flag & (1<<0) )
	{
	    camp_strdup( &pIns->iniFile, pInsFile_req->datFile ); // free and strdup
	}

	/*
	 *  Call init routine, not
	 */
	/* camp_insInit( pVar ); */

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}

/*
 * 20180604 DA remove locking of instrument mutex (at "//>>") -- it did nothing useful, 
 * but created a bottleneck when periodically saving the config.
 */

RES* 
campsrv_inssave_10400( INS_FILE_req* pInsFile_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsFile_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( pInsFile_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	//>> _mutex_lock_ins_on( pVar );

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	/*
	 *  Check if instrument locked
	 */
	// pRes->status = camp_checkInsLock( pVar, rq );
	// if( _failure( pRes->status ) ) 
	// {
	//     _camp_setMsgStat( CAMP_INS_LOCKED, path );
	//     //>> _mutex_lock_ins_off( pVar );
	//     goto return_;
	// }

	if( ( pInsFile_req->datFile == NULL ) || 
	    ( pInsFile_req->datFile[0] == '\0' ) )
	{
	    _camp_setMsg( "invalid filename '%s'", pInsFile_req->datFile );
	    pRes->status = CAMP_INVAL_FILE;
	    //>> _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	if( pInsFile_req->flag & (1<<1) )
	{
	    pRes->status = campDef_write( pInsFile_req->datFile, pVar );
	    if( _failure( pRes->status ) )
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campDef_write '%s'", pInsFile_req->datFile ); }
		pRes->status = CAMP_FAILURE;
		//>> _mutex_lock_ins_off( pVar );
		goto return_;
	    }
	}
	else
	{
	    pRes->status = campIni_write( pInsFile_req->datFile, pVar );
	    if( _failure( pRes->status ) )
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campIni_write '%s'", pInsFile_req->datFile ); }
		pRes->status = CAMP_FAILURE;
		//>> _mutex_lock_ins_off( pVar );
		goto return_;
	    }
	}

	/*
	 *  Set the ins file name
	 */
	if( pInsFile_req->flag & (1<<0) )
	{
	    camp_strdup( &pIns->iniFile, pInsFile_req->datFile ); // free and strdup
	}

	//>> _mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insifset_10400( INS_IF_req* pInsIF_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    bool_t wasOnline;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsIF_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( pInsIF_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	/*
	 *  If instrument is online, turn it offline
	 *  while setting the interface
	 */
	wasOnline = FALSE;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF** ppIF = &pIns->pIF;

	    if( *ppIF != NULL )
	    {
		wasOnline = ( (*ppIF)->status & CAMP_IF_ONLINE );
		if( wasOnline )
		{
		    pRes->status = camp_insOffline( pVar );
		    if( _failure( pRes->status ) )
		    {
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_insOffline '%s'", pVar->core.path ); }
			_mutex_lock_ins_off( pVar );
			goto return_;
		    }
		}
	    }

	    /*
	     *  Set the interface
	     */
	    pRes->status = camp_ifSet( ppIF, &pInsIF_req->IF );
	    if( _failure( pRes->status ) ) 
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_ifSet '%s' '%s'", pVar->core.path, pInsIF_req->IF.typeIdent ); }
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	/*
	 *  If instrument had been online, turn it back online
	 */
	if( wasOnline )
	{
	    pRes->status = camp_insOnline( pVar );
	    if( _failure( pRes->status ) )
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_insOnline '%s'", pVar->core.path ); }
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }
	}

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}

RES* 
campsrv_insifread_10400( INS_READ_req* pInsRead_req, struct svc_req* rq )
{
    RES* pRes;
    char* buf = NULL;
    int read_len = 0;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsRead_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( pInsRead_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// if( ( pInsType = camp_sysGetpInsType( pIns->typeIdent ) ) == NULL )
	// {
	//     _camp_setMsg( "invalid instrument type '%s'", pIns->typeIdent );
	//     pRes->status = CAMP_INVAL_INS_TYPE;
	//     _mutex_lock_ins_off( pVar );
	//     goto return_;
	// }

	buf = (char*)camp_zalloc( pInsRead_req->buf_len + 1 );

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    pRes->status = camp_ifRead( pIF, pVar, 
					pInsRead_req->cmd.cmd_val, 
					pInsRead_req->cmd.cmd_len, 
					buf, pInsRead_req->buf_len, &read_len );
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_ifRead '%s'", path ); }
	    _free( buf );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

    /*
     *  Set the CAMP msg string to the reading
     */
    camp_setMsg( "%s", buf );

return_:
    _free( buf );
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insifwrite_10400( INS_WRITE_req* pInsWrite_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsWrite_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pInsWrite_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    pRes->status = camp_ifWrite( pIF, 
					 pVar, 
					 pInsWrite_req->cmd.cmd_val, 
					 pInsWrite_req->cmd.cmd_len );
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_ifWrite '%s'", path ); }
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insifon_10400( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    char port[LEN_IDENT+1];
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVar_req->path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVar_req->path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	/*
	 *  Sep 2000, DJA added test to prevent instruments sharing the
	 *  same rs232 port (allowed for gpib and camac).
	 *  Dec 14, 2000,  DJA: better error message.
	 */
	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;

	    if( pIF == NULL )
	    {
		_camp_setMsgStat( CAMP_INVAL_IF, path );
		pRes->status = CAMP_INVAL_IF;
		goto return_;
	    }

	    if( pIF->typeID == CAMP_IF_TYPE_RS232 )
	    {
		CAMP_VAR* pOther;
		// _mutex_lock_varlist_on(); // already have it
		pOther = camp_rs232PortInUse( pIns /* , mutex_lock_varlist_check() */ );
		if( pOther )
		{
		    camp_getIfRs232Port( pIF->defn, port, sizeof( port ) );
		    /* 
		     * Port is in use by another (online) instrument, so this
		     * online attempt fails.  The following should work, but
		     * currently does not; message is "online error: Port "
		     */
		    _camp_setMsg( "port '%s' is in use by instrument '%s'", 
				    port, pOther->core.ident ); 
		    pRes->status = CAMP_RS232_PORT_IN_USE;
		    goto return_;
		}
		// _mutex_lock_varlist_off();
	    }

	    _mutex_lock_ins_on( pVar );

	    /*
	     *  Check if instrument locked
	     */
	    pRes->status = camp_checkInsLock( pVar, rq );
	    if( _failure( pRes->status ) ) 
	    {
		_camp_setMsgStat( CAMP_INS_LOCKED, path );
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }

	    pRes->status = camp_ifOnline( pIF );
	    if( _failure( pRes->status ) ) 
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_ifOnline '%s'", path ); }
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }

	    _mutex_lock_ins_off( pVar );
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insifoff_10400( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVar_req->path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVar_req->path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    pRes->status = camp_ifOffline( pIF );
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_ifOffline '%s'", path ); }
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insifdump_10400( INS_DUMP_req* pInsDump_req, struct svc_req* rq )
{
    RES* pRes;
    char* buf = NULL;
    int dump_len = 0;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsDump_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to var
	 */
	pVar = camp_varGetp( pInsDump_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	_mutex_lock_ins_on( pVar );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// if( ( pInsType = camp_sysGetpInsType( pIns->typeIdent ) ) == NULL )
	// {
	//     _camp_setMsg( "invalid instrument type '%s'", pIns->typeIdent );
	//     pRes->status = CAMP_INVAL_INS_TYPE;
	//     _mutex_lock_ins_off( pVar );
	//     goto return_;
	// }

	buf = (char*)camp_zalloc( pInsDump_req->buf_len + 1 );

	/* 
	 *  Read the data from the instrument into the buffer
	 */
	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    pRes->status = camp_ifRead( pIF, pVar, 
					pInsDump_req->cmd.cmd_val, 
					pInsDump_req->cmd.cmd_len, 
					buf, pInsDump_req->buf_len, &dump_len );
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_ifDump '%s'", path ); }
	    _free( buf );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    /* 
     * Match and skip any specified prefix
     */
    if( pInsDump_req->skip.skip_len > 0 )
    {
        if( strncmp( pInsDump_req->skip.skip_val, buf, pInsDump_req->skip.skip_len ) )
        {
            _camp_setMsg( "Dump prefix fails match" );
            pRes->status = CAMP_FAIL_DUMP;
            _free( buf );
            goto return_;
        }
    }

    /*
     *  Write the output file
     */
    dump_len -= pInsDump_req->skip.skip_len;

    pRes->status = camp_writeDumpFile( 
                   pInsDump_req->fname.fname_val,
                   pInsDump_req->fname.fname_len, 
                   buf + pInsDump_req->skip.skip_len, 
                   dump_len );

    _free( buf );

    if( _failure( pRes->status ) ) 
    {
        _camp_setMsg( "Failure to write dump file" );
        pRes->status = CAMP_FAIL_DUMP;
    }
    else
    {
	/*
	 *  Set the CAMP msg string to the number of dumped characters
	 */
	camp_setMsg( "%d", dump_len );
	pRes->status = CAMP_SUCCESS;
    }

return_:
    _free( buf );
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_insifundump_10400( INS_UNDUMP_req* pInsUndump_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    char* buf = NULL;
    FILE* fin = NULL;
    int n;
    char filename[LEN_FILENAME+1];
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pInsUndump_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pInsUndump_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _camp_setMsgStat( CAMP_INVAL_VAR_TYPE, path );
	    pRes->status = CAMP_INVAL_INS;
	    goto return_;
	}

	/*
	 *  Read the dump file
	 */
	camp_translate_generic_filename( pInsUndump_req->fname.fname_val, filename, sizeof( filename ) ); // 20140325
	fin = fopen( filename, "r" );
	if( fin == NULL )
	{
	    _camp_setMsg( "error opening file '%s'", pInsUndump_req->fname.fname_val );
	    pRes->status = CAMP_INVAL_FILE;
	    goto return_;
	}

	buf = (char*)camp_malloc( pInsUndump_req->buf_len );

	if( !buf )
	{
	    _camp_setMsg( "error allocating buffer of size %d", pInsUndump_req->buf_len );
	    pRes->status = CAMP_INVAL_FILE;
	    goto return_;
	}

	n = fread( buf, 1, pInsUndump_req->buf_len, fin );

	fclose( fin );

	if( n < pInsUndump_req->buf_len )
	{
	    _camp_setMsg( "error: file too short (%d < %d)", n, pInsUndump_req->buf_len );
	    pRes->status = CAMP_FAIL_UNDUMP;
	    _free( buf );
	    goto return_;
	}

	/*
	 *  Check if instrument locked
	 */
	_mutex_lock_ins_on( pVar );

	pRes->status = camp_checkInsLock( pVar, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _free( buf );
	    _mutex_lock_ins_off( pVar );
	    goto return_;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    /*
	     *  Write the command
	     */
	    pRes->status = camp_ifWrite( pIF, 
					 pVar, 
					 pInsUndump_req->cmd.cmd_val, 
					 pInsUndump_req->cmd.cmd_len );
	    if( _failure( pRes->status ) ) 
	    {
		_free( buf );
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }

	    /*
	     *  Write the binary dump buffer
	     */
	    pRes->status = camp_ifWrite( pIF, pVar, buf, n );
	    if( _failure( pRes->status ) ) 
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_ifWrite '%s'", path ); }
		_free( buf );
		_mutex_lock_ins_off( pVar );
		goto return_;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	_mutex_lock_ins_off( pVar );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _free( buf );
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_varset_10400( DATA_SET_req* pVarSet_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVarSet_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVarSet_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Request setting of data
	 */
	pRes->status = varSetReq( pVar, &pVarSet_req->spec );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetReq '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_varread_10400( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVar_req->path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVar_req->path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Request reading of data
	 */
	pRes->status = varRead( pVar );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varRead '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_vardoset_10400( DATA_SET_req* pVarSet_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVarSet_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVarSet_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Set the data
	 */
	pRes->status = varSetSpec( pVar, &pVarSet_req->spec );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetSpec '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_varlnkset_10400( DATA_SET_req* pVarSet_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVarSet_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetTrueP( pVarSet_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	if( pVar->core.varType != CAMP_VAR_TYPE_LINK )
	{
	    pRes->status = CAMP_INVAL_VAR_TYPE;
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Set the data
	 */
	pRes->status = varSetSpec( pVar, &pVarSet_req->spec );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetSpec '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_varpoll_10400( DATA_POLL_req* pVarPoll_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVarPoll_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVarPoll_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Set the POLL parameters
	 */
	pRes->status = varSetPoll( pVar, 
				   pVarPoll_req->flag, 
				   pVarPoll_req->pollInterval );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetPoll '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_varalarm_10400( DATA_ALARM_req* pVarAlarm_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVarAlarm_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVarAlarm_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Set the ALARM parameters
	 */
	pRes->status = varSetAlarm( pVar, 
				    pVarAlarm_req->flag, 
				    pVarAlarm_req->alarmAction );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetAlarm '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_varlog_10400( DATA_LOG_req* pVarLog_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVarLog_req->dreq.path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVarLog_req->dreq.path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	/*
	 *  Set the LOG parameters
	 */
	pRes->status = varSetLog(  pVar, 
				   pVarLog_req->flag, 
				   pVarLog_req->logAction );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetLog '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_varzero_10400( DATA_req* pVar_req, struct svc_req* rq )
{
    RES* pRes;
    char* path;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    path = pVar_req->path;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_VAR* pVar_ins;

	/*
	 *  Get pointer to data
	 */
	pVar = camp_varGetp( pVar_req->path /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL ) 
	{ 
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	    pRes->status = CAMP_INVAL_VAR;
	    goto return_;
	}

	pVar_ins = varGetpIns( pVar );
	_mutex_lock_ins_on( pVar_ins );

	/*
	 *  Check if instrument locked
	 */
	pRes->status = camp_checkInsLock( pVar_ins, rq );
	if( _failure( pRes->status ) ) 
	{
	    _camp_setMsgStat( CAMP_INS_LOCKED, path );
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	pRes->status = varZeroStats( pVar );
	if( _failure( pRes->status ) ) 
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varZeroStats '%s'", path ); }
	    _mutex_lock_ins_off( pVar_ins );
	    goto return_;
	}

	_mutex_lock_ins_off( pVar_ins );
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    pRes->status = CAMP_SUCCESS;

return_:
    _mutex_lock_end();
    return( pRes );
}


CAMP_VAR_res* 
campsrv_varget_10400( DATA_GET_req* pVarGet_req, struct svc_req* rq )
{
    CAMP_VAR_res* pRes;
    char* path;
    // _mutex_lock_begin();

    pRes = (CAMP_VAR_res*)camp_zalloc( sizeof( CAMP_VAR_res ) );

    path = pVarGet_req->dreq.path;

    set_current_xdr_flag( pVarGet_req->flag );

    pRes->status = CAMP_SUCCESS;

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;

	if( camp_pathAtTop( pVarGet_req->dreq.path ) )
	{
	    pVar = pVarList; // pVarList==NULL is ok
	}
	else
	{
	    pVar = camp_varGetp( pVarGet_req->dreq.path /* , mutex_lock_varlist_check() */ );

	    if( pVar == NULL )
	    {
		_camp_setMsgStat( CAMP_INVAL_VAR, path );
		pRes->status = CAMP_INVAL_VAR;
	    }
	    else if( pVarGet_req->flag & CAMP_XDR_CHILD_LEVEL )
	    {
		pVar = pVar->pChild;
		if( pVar == NULL )
		{
		    _camp_setMsg( "child variable of '%s' doesn't exist", path );
		    pRes->status = CAMP_INVAL_VAR;
		}
	    }
	}

	pRes->pVar = pVar; // note: lock on returned value handled by xdr_CAMP_VAR_res
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

//return_:
    // _mutex_lock_end();
    return( pRes );
}


RES* 
campsrv_cmd_10400( CMD_req* pCMD_req, struct svc_req* rq )
{
    RES* pRes;
    _mutex_lock_begin();

    pRes = (RES*)camp_zalloc( sizeof( RES ) );

    /*
     *  Parse the command line using Tcl
     */
    _mutex_lock_mainInterp_on(); 
    {
	int tcl_status = TCL_OK;
	Tcl_Interp* interp = camp_tclInterp();

	tcl_status = Tcl_Eval( interp, pCMD_req->cmd );

	pRes->status = ( tcl_status == TCL_ERROR ) ? CAMP_FAILURE : CAMP_SUCCESS;

	if( _camp_debug(CAMP_DEBUG_CMD) ) 
	{
	    _camp_log( "tcl result: '%s' (len:%d)", interp->result, strlen( interp->result ) ); 
	}

	/*
	 *  In the case of insIfRead and insIfReadVerify and insIfDump
	 *  campMsg should be empty to properly return the reading here
	 *
	 *  20140424  TW  this could be camp_setMsg now, the entire message
	 *                should be in Tcl result when returning from Tcl_Eval
	 *                (camp msg should be empty)
	 */
	camp_appendMsg( "%s", interp->result );
    }
    _mutex_lock_mainInterp_off(); 

    if( _camp_debug(CAMP_DEBUG_CMD) ) 
    {
	_camp_log( "camp msg: '%s' (len:%d)", camp_getMsg(), strlen( camp_getMsg() ) );
    }

    _mutex_lock_end();
    return( pRes );
}

