/*
 *  Name:       camp_srv_rpc.c
 *
 *  Purpose:    register RPC server
 *              modified versions of RPC calls (srv_run_once, my_svc_getreqset)
 *
 *  Called by:  
 * 
 *  Revision history:
 *
 *    20140306  TW  extracted from camp_srv_main.c and camp_srv_svc_mod.c
 */

#include <stdio.h>
#include <errno.h>
#include "camp_srv.h"

#ifdef VXWORKS
#include "timeval.h"
#include <rpc/rpcGbl.h>    /* for VxWorks RPC globals */
#include <rpc/pmap_clnt.h>    
#endif /* VXWORKS */

#ifdef linux
#include <sys/select.h>
#include <unistd.h> // getdtablesize

// #if CAMP_TIRPC
// #include <tirpc/rpc/svc.h>
// #include <tirpc/rpc/pmap_clnt.h>
// #else // !CAMP_TIRPC
#include <rpc/svc.h>
// #ifdef PORTMAP
#include <rpc/pmap_clnt.h>
// #endif // PORTMAP
// #endif // CAMP_TIRPC

#endif /* linux */

#ifdef VXWORKS
static u_long my_ffs( u_long i );
static void my_svc_getreqset();
#endif /* VXWORKS */

#ifdef VXWORKS
#define getdtablesize()  FD_SETSIZE   /* check the maximum */
#define svc_fdset        taskRpcStatics->svc.svc_fdset
#define xports           taskRpcStatics->svc.xports
#define svc_head         taskRpcStatics->svc.svc_head
#endif /* VXWORKS */

extern int camp_rpc_thread_mode;


typedef void (*rpc_dispatch_proc_t)( struct svc_req*, SVCXPRT* );

int srv_init_rpc()
{
    rpc_dispatch_proc_t dispatch;
#if !CAMP_TIRPC
    SVCXPRT* svc_transport;
    u_int send_buf_size = 0; // use default
    u_int recv_buf_size = 0; // use default
#endif // !CAMP_TIRPC

    switch( camp_rpc_thread_mode )
    {
	/*
	 *  mode 0
	 */
    case CAMP_RPC_THREAD_MODE_SERIAL_IN_SINGLE:
	dispatch = camp_srv_10400_serial_in_single; // rpc handled in serial within svc_run (e.g., tirpc, tsrpc on linux)
	break;
    case CAMP_RPC_THREAD_MODE_SERIAL_IN_MULTI:
	dispatch = camp_srv_10400_serial_in_multi; // rpc handled in serial within svc_run_once (e.g., tirpc, tsrpc on linux)
	break;
	/*
	 *  mode 1
	 */
    case CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET: // we start threads from my_svc_getreqset (e.g., tsrpc on vxworks)
	dispatch = camp_srv_10400_threaded_svc_getreqset;
	break;
	/*
	 *  mode 2
	 */
    case CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN: // svc_run automatically spawns threads (e.g. mtrpc)
	dispatch = camp_srv_10400_threaded_svc_run;
	break;
    default:
        fprintf( stderr, "unrecognized camp_rpc_thread_mode %d\n", camp_rpc_thread_mode );
        return( CAMP_FAILURE );
    }

#if CAMP_TIRPC // TI-RPC

    /*
     *  Unregister any other CAMP server with the RPC portmapper
     *
     *  even though ti-rpc doesn't use the portmapper, this call
     *  is implemented with what appears to be something useful.
     *
     *  however, rpcgen generated code does not call this, so leave it out
     */
    //pmap_unset( CAMP_PROGNUM, CAMP_VERSNUM ); //TEMP

    int num_server_handles = svc_create( dispatch, CAMP_PROGNUM, CAMP_VERSNUM, "tcp" );

    // switch( rpc_createerr.cf_stat )
    if( rpc_createerr.cf_stat == RPC_UNKNOWNPROTO )
    {
        _camp_appendMsg( "svc_create: RPC_UNKNOWNPROTO" );
    }
    else if( rpc_createerr.cf_stat == RPC_UNKNOWNADDR )
    {
        _camp_appendMsg( "svc_create: RPC_UNKNOWNADDR" );
    }
    else if( rpc_createerr.cf_stat == RPC_UNKNOWNHOST )
    {
        _camp_appendMsg( "svc_create: RPC_UNKNOWNHOST" );
    }
    else if( rpc_createerr.cf_stat == RPC_SYSTEMERROR )
    {
        _camp_appendMsg( "svc_create: RPC_SYSTEMERROR" );
    }
    else if( rpc_createerr.cf_stat == RPC_N2AXLATEFAILURE )
    {
        _camp_appendMsg( "svc_create: RPC_N2AXLATEFAILURE" );
    }
    else
    {
        _camp_appendMsg( "svc_create: rpc_createerr.cf_stat: %d", rpc_createerr.cf_stat );
    }

    if( num_server_handles == 0 ) 
    {
        fprintf( stderr, "%s: unable to create program (%lu, %lu) for tcp\n", __FUNCTION__, CAMP_PROGNUM, CAMP_VERSNUM );
        return( CAMP_FAILURE );
    }
    else
    {
        _camp_appendMsg( "created %d server handles for program (%d, %d)", num_server_handles, CAMP_PROGNUM, CAMP_VERSNUM );
    }

#else // original Sun RPC aka TS-RPC

    /*
     *  Initialize an RPC TCP transport
     */
    svc_transport = svctcp_create( RPC_ANYSOCK, send_buf_size, recv_buf_size );
    if( svc_transport == NULL )
    {
        fprintf( stderr, "%s: failed svctcp_create\n", __FUNCTION__ );
        return( CAMP_FAILURE );
    }

    /*
     *  Unregister any other CAMP server with the RPC portmapper
     */
    pmap_unset( CAMP_PROGNUM, CAMP_VERSNUM );

    /*
     *  Register the program as the CAMP server with the RPC
     *  portmapper.
     */

    if( !svc_register( svc_transport, CAMP_PROGNUM, CAMP_VERSNUM, dispatch, IPPROTO_TCP ) )
    {
        fprintf( stderr, "%s: failed svc_register\n", __FUNCTION__ );
        return( CAMP_FAILURE );
    }

#endif // CAMP_TIRPC

    return( CAMP_SUCCESS );
}


void 
srv_shutdown_rpc()
{
    /*
     *  Unregister RPC service
     */
#if CAMP_TIRPC // TI-RPC

    svc_unreg( CAMP_PROGNUM, CAMP_VERSNUM );

#else // original Sun RPC aka TS-RPC

    svc_unregister( CAMP_PROGNUM, CAMP_VERSNUM );

#endif // CAMP_TIRPC
}


/*
 *  Name:       svc_run_once
 *
 *  Purpose:    Check for any pending calls to the RPC sockets with a
 *              zero timeout.  If there is a call, dispatch it.
 *
 *              22-Dec-1999  TW  Multinet implementation note
 *              Note that in the Multinet (VAX/VMS) implementation, there
 *              is already a routine called svc_run_once which is used
 *              instead (found by analyzing Multinet rpc.olb (svc.obj)).
 *              This routine will call the svc_getreqset in the RPC
 *              library.  Since this is not the recommended procedure for
 *              implementing a multithreaded RPC server, the Multinet
 *              implementation of the CAMP server is no longer recommended
 *              for multithreading.  Replacement of svc_run_once for Multinet
 *              (i.e., a port of svc_run_once to Multinet, which would
 *              involve determining the correct procedure for the "select"
 *              call) would be necessary before re-implementing a multithreaded
 *              server with Multinet.
 *
 *  Called by:  srv_loop
 * 
 *  Inputs:     None
 *
 *  Preconditions:
 *              CAMP server initialized as RPC server.
 *              CAMP global lock is ON.
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              CAMP global lock will still be ON and should be turned OFF
 *              (when appropriate).
 *
 *              In the multithreaded implementation, there may still be
 *              valid RPC requests pending when this routine returns.  It
 *              will dispatch at most one RPC per call.  Furthermore, since
 *              threads are dispatched to satisfy the RPC call, the RPC will
 *              effectively still be active after returning from this call.
 *              Private variables in the svc thread's structure indicate when
 *              the svc thread is finished processing.  It is not until after
 *              this condition that the main thread will reply to the RPC
 *              client (which is done in srv_loop).  So, it is important to
 *              note that RPCs are still active after a return from this
 *              routine, and that associated RPC tranports must be considered
 *              active as well until the reply to the client.
 *
 *              In the single-threaded implementation, all pending RPCs
 *              should be satisfied when this routine returns.
 *
 *  Revision history:
 *    22-Dec-1999  TW  Call to my_svc_getreqset for multithreaded implementation
 *                     now processes at most one pending RPC.  Also, have made
 *                     sure that RPC transports are not re-used between calls
 *                     unless the RPC has really been satisfied (by the svc
 *                     thread).
 */
void
svc_run_once( void )
{
    timeval_t tv = { 0, 0 };  /* poll */
    fd_set readfds;
    int selectStat;

    readfds = svc_fdset;

    selectStat = select( getdtablesize(), &readfds, (fd_set*)0, (fd_set*)0, &tv );

    switch( selectStat )
    {
    case -1:
	if( errno == EINTR ) break;
	_camp_log( "failed select" );
	break;
    case 0:
	break;
    default:
#if defined( VXWORKS )
        my_svc_getreqset( &readfds ); // modified version for multithreading
#else 
	svc_getreqset( &readfds );
#endif
	break;
    }
}

#if defined( VXWORKS )

/*
 *  A replacement of the ffs C library function which does not exist
 *  in all C libraries.
 *
 *  ffs finds the first set bit in a four-byte integer
 */
static u_long
my_ffs( u_long i )
{
    register u_long j;

    if( i == 0 ) return( 0 );

    for( j = 1; j <= ( sizeof( u_long )*NBBY ); j++ )
    {
	if( (i>>(j-1))&1 ) return( j );
    }

    return( 0 );
}


//  this only compiles on VxWorks, because of the version of RPC it was 
//  written for.  there's nothing VxWorks-specific about this.
/*
 *  Name:       my_svc_getreqset
 *
 *  Purpose:    Replace the RPC library routine svc_getreqset with a version
 *              compatible with a multithreaded RPC server.
 *
 *              This routine is only needed to implement a multithreaded server.
 *
 *              The version of svc_getreqset that this was based on is
 *              contemporary with the CAMP server creation.  It may now (1999)
 *              have been superseded by later versions, but is still sufficient
 *              for our purposes.
 *
 *              Differences from svc_getreqset in the RPC library are:
 *
 *              1.  If input is found waiting on an RPC transport socket
 *                  that is associated with an active RPC call (one that an
 *                  svc thread has not yet completed) do not try any operations
 *                  on this transport.
 *
 *              2.  Within the RPC dispatch "do" loop, only destroy (free)
 *                  RPC transports that are really inactive.  This is similar
 *                  to point "1" above, but effectively does not allow
 *                  a transport to be destroyed immediately after the RPC call
 *                  has been dispatched.
 *
 *              3.  If an RPC call is dispatched, return from the routine
 *                  without testing any more sockets.  This means that at most
 *                  one RPC call is dispatched per call.
 *
 *  Called by:  svc_run_once
 * 
 *  Inputs:     The pointer to the file descriptor mask that is associated
 *              with the RPC server sockets.
 *
 *  Preconditions:
 *              The RPC server sockets have been checked for input using
 *              a select() call.  A socket has input waiting.
 *
 *  Outputs:    None
 *
 *  Postconditions:
 *              In multithreaded implementation a dispatched RPC call will
 *              not have been satisfied when returning from this routine.  As
 *              such, associated RPC information (in particular the transport
 *              structure) must not be used until the RPC call is really
 *              completed and the client has been sent a response (unless
 *              of course the client has exited prematurely).
 *
 *  Revision history:
 *    22-Dec-1999  TW  Creation
 *
 */
/* @(#)svc.c	2.4 88/08/11 4.0 RPCSRC; from 1.44 88/02/08 SMI */

#define NULL_SVC ((struct svc_callout *)0)
#define	RQCRED_SIZE	400		/* this size is excessive */

static void
my_svc_getreqset(readfds)
#ifdef FD_SETSIZE
	fd_set *readfds;
{
#else
	int *readfds;
{
    int readfds_local = *readfds;
#endif /* def FD_SETSIZE */
	enum xprt_stat stat;
	struct rpc_msg msg;
	int prog_found;
	u_long low_vers;
	u_long high_vers;
	struct svc_req r;
	register SVCXPRT *xprt;
	register u_long mask;
	register int bit;
	register u_long *maskp;
	register int setsize;
	register int sock;
	char cred_area[2*MAX_AUTH_BYTES + RQCRED_SIZE];
        //struct authunix_parms* aupp;
        //struct authunix_parms* aupp_orig;

        int num_dispatched = 0;
        int i;
        extern int check_rpc_transport_in_use( SVCXPRT* transp );

	msg.rm_call.cb_cred.oa_base = cred_area;
	msg.rm_call.cb_verf.oa_base = &(cred_area[MAX_AUTH_BYTES]);
	r.rq_clntcred = &(cred_area[2*MAX_AUTH_BYTES]);

#ifdef FD_SETSIZE
	setsize = getdtablesize();
#ifdef linux
#define NFDBITS	32
	maskp = (u_long *)readfds;
#else
	maskp = (u_long *)readfds->fds_bits;
#endif
	for (sock = 0; sock < setsize; sock += NFDBITS) {
	    for (mask = *maskp++; (bit = my_ffs(mask)); mask ^= (1 << (bit - 1))) {
		/* sock has input waiting */
		xprt = xports[sock + bit - 1];
#else
	for (sock = 0; readfds_local != 0; sock++, readfds_local >>= 1) {
	    if ((readfds_local & 1) != 0) {
		/* sock has input waiting */
		xprt = xports[sock];
#endif /* def FD_SETSIZE */

		/*
		 *  18-Dec-1999  TW  transports now active between calls to
                 *                   svc_getreqset.  A symptom is that active
                 *                   tranports may still report input waiting
                 *                   between calls, and sometimes SVC_RECV will
                 *                   be successful multiple times on the same
                 *                   transport, causing unpredictible results.
		 *                   FIX:  check that a transport is already
                 *                   active on a running svc thread.  If so,
                 *                   don't try any operations on it here.
		 */
		if( ( i = check_rpc_transport_in_use( xprt ) ) > -1 ) 
		{
		    if( _camp_debug(CAMP_DEBUG_RPC) ) 
		    {
			_camp_log( "found transport in use thread %d sock %d bit %d", i, sock, bit );
		    }
		    continue;
		}

		if( _camp_debug(CAMP_DEBUG_RPC) ) 
		{
		    _camp_log( "before dispatch SVC_STAT( xprt ) %d sock %d bit %d", SVC_STAT( xprt ), sock, bit );
		}

		/* now receive msgs from xprtprt (support batch calls) */
		do {
			if (SVC_RECV(xprt, &msg)) {

				/* now find the exported program and call it */
				register struct svc_callout *s;
				enum auth_stat why;

				r.rq_xprt = xprt;
				r.rq_prog = msg.rm_call.cb_prog;
				r.rq_vers = msg.rm_call.cb_vers;
				r.rq_proc = msg.rm_call.cb_proc;
				r.rq_cred = msg.rm_call.cb_cred;

				/* first authenticate the message */
				if ((why= _authenticate(&r, &msg)) != AUTH_OK) {
					svcerr_auth(xprt, why);
					goto call_done;
				}

				/* now match message with a registered service*/
				prog_found = FALSE;
				low_vers = 0 - 1;
				high_vers = 0;
				for (s = svc_head; s != NULL_SVC; s = s->sc_next) {
					if (s->sc_prog == r.rq_prog) {
						if (s->sc_vers == r.rq_vers) {
							(*s->sc_dispatch)(&r, xprt);
							num_dispatched++;
							goto call_done;
						}  /* found correct version */
						prog_found = TRUE;
						if (s->sc_vers < low_vers)
							low_vers = s->sc_vers;
						if (s->sc_vers > high_vers)
							high_vers = s->sc_vers;
					}   /* found correct program */
				}
				/*
				 * if we got here, the program or version
				 * is not served ...
				 */
				if (prog_found)
					svcerr_progvers(xprt,
					low_vers, high_vers);
				else
					 svcerr_noprog(xprt);
				/* Fall through to ... */
			}
		call_done:
			if ((stat = SVC_STAT(xprt)) == XPRT_DIED){
                          /*
                           *  Only destroy RPC transports (that is, free them
                           *  up for re-use) if the transport is really inactive
                           *  There can be a condition here in which the client
                           *  has exited prematurely (thus leaving the port
                           *  in the XPRT_DIED state) but the SVC thread has
                           *  not finished the RPC request.  If a new RPC
                           *  request is made and same transport was available,
                           *  two active RPC calls would now be associated with
                           *  one transport (that appeared in non-dead state
                           *  to both).  When the first SVC thread then
                           *  finished it's call, it could potentially reply
                           *  to the wrong client request.  INSTEAD, we must
                           *  not try to use an RPC transport until the
                           *  corresponding RPC request is really finished.
                           */
			  if( check_rpc_transport_in_use( xprt ) == -1 ) 
			  {
			      if( _camp_debug(CAMP_DEBUG_RPC) ) 
			      {
				  _camp_log( "server destroying stale xprt sock %d bit %d", sock, bit );
			      }
			      SVC_DESTROY( xprt );
			  }
                          /*
                           * Don't break here, want to check num_dispatched
                           * first
                           */
			  /* break; */
			}
			/*
			 *  Return if there has been an RPC dispatch
                         *  This means that a maximum on one RPC will be
                         *  dispatched.  This is necessary to allow the
                         *  try_find_free_thread_data methodology in the calling
                         *  routine to work properly.
			 */
			if( num_dispatched > 0 )
			{
			    if( _camp_debug(CAMP_DEBUG_RPC) ) 
			    {
				_camp_log( "num_dispatched %d stat %d sock %d bit %d", 
				   num_dispatched, stat, sock, bit );
			    }
			    return;
			}

			if( stat == XPRT_MOREREQS )
			{
			    if( _camp_debug(CAMP_DEBUG_RPC) ) 
			    {
				_camp_log( "more requests on sock %d bit %d", sock, bit );
			    }
			}

		} while (stat == XPRT_MOREREQS);
	    }
	}
}

#endif // VXWORKS

#ifdef LINUX_TEST

static void
my_svc_getreqset(readfds)
//#ifdef FD_SETSIZE
	fd_set *readfds;
{
//#else
//	int *readfds;
//{
//        int readfds_local = *readfds;
//#endif /* def FD_SETSIZE */
        /* declarations in glibc's svc_getreqset */
        register fd_mask mask;
        register fd_mask *maskp;
        register int setsize;
        register int sock;
        register int bit;
        /* my declaration for parameter passed to svc_getreq_common */
        int fd;
        /* declarations from svc_getreq_common */
        enum xprt_stat stat;
        struct rpc_msg msg;
        register SVCXPRT *xprt;
        char cred_area[2 * MAX_AUTH_BYTES + RQCRED_SIZE];
        // struct authunix_parms* aupp;
        // struct authunix_parms* aupp_orig;

        int num_dispatched = 0;
        int i,isr;

        msg.rm_call.cb_cred.oa_base = cred_area;
        msg.rm_call.cb_verf.oa_base = &(cred_area[MAX_AUTH_BYTES]);

//#ifdef FD_SETSIZE
	setsize = getdtablesize();
        if (setsize > FD_SETSIZE)
          setsize = FD_SETSIZE;
	maskp = readfds->__fds_bits;
	for (sock = 0; sock < setsize; sock += NFDBITS) 
          {
            for (mask = *maskp++; (bit = my_ffs(mask)); mask ^= (1L << (bit - 1))) 
              {
                /* sock has input waiting */
                /* Here, inside the douple loop, glibc uses svc_getreq_common(sock + bit - 1) */
                fd = sock + bit - 1;
                /* Begin inlined variation of svc_getreq_common */
                xprt = xports[fd];

//#else

//	for (sock = 0; readfds_local != 0; sock++, readfds_local >>= 1) {
//	    if ((readfds_local & 1) != 0) {
//		/* sock has input waiting */
//		xprt = xports[sock];

//#endif /* def FD_SETSIZE */

		/*
		 *  18-Dec-1999  TW  transports now active between calls to
                 *                   svc_getreqset.  A symptom is that active
                 *                   transports may still report input waiting
                 *                   between calls, and sometimes SVC_RECV will
                 *                   be successful multiple times on the same
                 *                   transport, causing unpredictible results.
		 *                   FIX:  check that a transport is already
                 *                   active on a running svc thread.  If so,
                 *                   don't try any operations on it here.
		 */
		if( ( i = check_rpc_transport_in_use( xprt ) ) > -1 ) 
		{
		    if( _camp_debug(CAMP_DEBUG_RPC) ) 
		    {
			_camp_log( "found transport in use thread %d sock %d bit %d", i, sock, bit );
		    }
		    continue;
		}

		if( _camp_debug(CAMP_DEBUG_RPC) ) 
		{
		    _camp_log( "before dispatch SVC_STAT( xprt ) %d sock %d bit %d", SVC_STAT( xprt ), sock, bit );
		}

		/* now receive msgs from xprtprt (support batch calls) */
		do 
                  {
                    if (isr=SVC_RECV(xprt, &msg)) 
                      {
                        /* now find the exported program and call it */
                        register struct svc_callout *s;
                        struct svc_req r;
                        enum auth_stat why;
                        rpcvers_t low_vers;
                        rpcvers_t high_vers;
                        int prog_found;

                        r.rq_clntcred = &(cred_area[2 * MAX_AUTH_BYTES]);
                        r.rq_xprt = xprt;
                        r.rq_prog = msg.rm_call.cb_prog;
                        r.rq_vers = msg.rm_call.cb_vers;
                        r.rq_proc = msg.rm_call.cb_proc;
                        r.rq_cred = msg.rm_call.cb_cred;

                        /* first authenticate the message */
                        /* Check for null flavor and bypass these calls if possible */

                        if (msg.rm_call.cb_cred.oa_flavor == AUTH_NULL)
                          {
                            r.rq_xprt->xp_verf.oa_flavor = _null_auth.oa_flavor;
                            r.rq_xprt->xp_verf.oa_length = 0;
                          }
                        else if ((why = _authenticate(&r, &msg)) != AUTH_OK)
                          {
                            svcerr_auth(xprt, why);
                            goto call_done;
                          }

                        /* now match message with a registered service*/
                        prog_found = FALSE;
                        low_vers = 0 - 1;
                        high_vers = 0;
                        for (s = svc_head; s != NULL_SVC; s = s->sc_next)
                          {
                            if (s->sc_prog == r.rq_prog)
                              {
                                if (s->sc_vers == r.rq_vers)
                                  {
                                    (*s->sc_dispatch)(&r, xprt);
                                    num_dispatched++;
                                    goto call_done;
                                  }  /* found correct version */
                                prog_found = TRUE;
                                if (s->sc_vers < low_vers)
                                  low_vers = s->sc_vers;
                                if (s->sc_vers > high_vers)
                                  high_vers = s->sc_vers;
                              }
                            /* found correct program */
                          }
                        /*
                         * if we got here, the program or version
                         * is not served ...
                         */
                        if (prog_found)
                          svcerr_progvers(xprt, low_vers, high_vers);
                        else
                          svcerr_noprog(xprt);
                        /* Fall through to call_done  */
                      }
                    else /* temporary only */
                      {
                        if( _camp_debug(CAMP_DEBUG_RPC) ) 
			{
			    _camp_log( "SVC_RECV = %d", isr );
			}
                      }
                  call_done:
                    if ((stat = SVC_STAT(xprt)) == XPRT_DIED)
		    {
                          /*
                           *  Only destroy RPC transports (that is, free them
                           *  up for re-use) if the transport is really inactive
                           *  There can be a condition here in which the client
                           *  has exited prematurely (thus leaving the port
                           *  in the XPRT_DIED state) but the SVC thread has
                           *  not finished the RPC request.  If a new RPC
                           *  request is made and same transport was available,
                           *  two active RPC calls would now be associated with
                           *  one transport (that appeared in non-dead state
                           *  to both).  When the first SVC thread then
                           *  finished it's call, it could potentially reply
                           *  to the wrong client request.  INSTEAD, we must
                           *  not try to use an RPC transport until the
                           *  corresponding RPC request is really finished.
                           */
                        if( check_rpc_transport_in_use( xprt ) == -1 ) 
			{
                            if( _camp_debug(CAMP_DEBUG_RPC) ) 
			    {
				_camp_log( "server destroying stale xprt sock %d bit %d", sock, bit );
			    }
                            SVC_DESTROY( xprt );
			}
			/*
			 * Don't break here, want to check num_dispatched
			 * first
			 */
			/* break; */
		    }
                    /*
                     *  Return if there has been an RPC dispatch
                     *  This means that a maximum of one RPC will be
                     *  dispatched.  This is necessary to allow the
                     *  try_find_free_thread_data methodology in the calling
                     *  routine to work properly.
                     */
                    if( num_dispatched > 0 )
		    {
                        if( _camp_debug(CAMP_DEBUG_RPC) ) 
			{
			    _camp_log( "num_dispatched %d stat %d sock %d bit %d", 
                                        num_dispatched, stat, sock, bit );
			}
                        return;
		    }

                    if( stat == XPRT_MOREREQS )
		    {
                        if( _camp_debug(CAMP_DEBUG_RPC) ) 
			{
			    _camp_log( "more requests on sock %d bit %d", sock, bit );
			}
		    }

                  } while (stat == XPRT_MOREREQS);
                /* End of inlined svc_getreq_common */
              } /* End loop for (mask =... */
          } /* End loop for (sock =... */
} /* End my_svc_getreqset */

#endif // LINUX_TEST
