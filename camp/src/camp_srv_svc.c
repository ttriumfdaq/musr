/*
 *  Name:       camp_srv_svc.c
 *
 *  Purpose:    Dispatch RPC calls to the appropriate entry point.
 *
 *  Called by:  CAMP server routines only (various).
 * 
 *  Revision history:
 *
 *    20140409     TW  removed old commented-out free_thread mutex/condition
 *    20140306     TW  renamed from camp_srv_svc_mod.c
 *    20140218     TW  exit(0) -> exit(CAMP_FAILURE) for failures
 *    20140217     TW  tidying CAMP_DEBUG
 *                     rename camp_thread_mutex_lock to mutex_lock
 *                     rename pthread_mutex_unlock to mutex_unlock
 *                     rename set_global_mutex_noChange to mutex_lock_mainInterp
 *                     rename thread_un/lock_global_np to mutex_un/lock_global
 *                     rename get/release_ins_thread_lock -> mutex_un/lock_ins
 *    09-Feb-2001  DJA Dump/Undump
 *    21-Dec-1999  TW  orig_trans (copy of transport structure) no longer
 *                     used.  All use of transports now in main thread
 *                     _exclusively_.  Only main thread handles (its own)
 *                     RPC transport structures.
 *    21-Dec-1999  TW  free_thread mutex and condition no longer used
 *                     find_free_thread_data (and try_find_free_thread_data) now 
 *                     check explicitly for a free thread.
 *
 *  $Log: camp_srv_svc.c,v $
 *  Revision 1.2  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.4  2004/12/18 01:02:28  asnd
 *  Alter debugging messages
 *
 *  Revision 1.3  2001/02/10 07:14:49  asnd
 *  DJA: insIfDump / insIfUndump API routines
 */

#include <string.h>
#include <stdlib.h>
#include "camp_srv.h"

typedef char* (*srvproc_t)();

static void get_rq_proc( u_long rq_proc, xdrproc_t* pxdr_argument, xdrproc_t* pxdr_result, srvproc_t* psrv_proc );
// static void camp_srv_10400_multithreaded( struct svc_req *rqstp, SVCXPRT *transp );
#if CAMP_MULTITHREADED
static void start_svc_mode1_thread( struct svc_req* rqstp, SVCXPRT* transp );
static pthread_addr_t svc_mode1_thread( pthread_addr_t arg );
static void svc_mode1_thread_inner( struct svc_req* rqstp, SVCXPRT* transp );
static void end_svc_mode1_thread( int arg );
static bool_t test_lost_client( SVCXPRT* transp );
#endif // CAMP_MULTITHREADED
static void svc_mode02_inner( struct svc_req *rqstp, SVCXPRT *transp );


/*
 *  Get the RPC parameters for the specific RPC call.  These are used
 *  to manage the arguments and the result of the call through XDR and
 *  to get the pointer to the right dispatch entry point.
 *
 *  Used for both multithreaded and single-threaded
 */
void
get_rq_proc( u_long rq_proc, xdrproc_t* pxdr_argument, xdrproc_t* pxdr_result, srvproc_t* psrv_proc )
{
    switch( rq_proc ) 
    {
        case CAMPSRV_SYSSHUTDOWN:
                *pxdr_argument = (xdrproc_t)xdr_void;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_sysshutdown_10400;
                break;

        case CAMPSRV_SYSUPDATE:
                *pxdr_argument = (xdrproc_t)xdr_void;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_sysupdate_10400;
                break;

        case CAMPSRV_SYSLOAD:
                *pxdr_argument = (xdrproc_t)xdr_FILE_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_sysload_10400;
                break;

        case CAMPSRV_SYSSAVE:
                *pxdr_argument = (xdrproc_t)xdr_FILE_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_syssave_10400;
                break;

        case CAMPSRV_SYSGET:
                *pxdr_argument = (xdrproc_t)xdr_void;
                *pxdr_result   = (xdrproc_t)xdr_CAMP_SYS_res;
                *psrv_proc     = (srvproc_t)campsrv_sysget_10400;
                break;

        case CAMPSRV_SYSGETDYNA:
                *pxdr_argument = (xdrproc_t)xdr_void;
                *pxdr_result   = (xdrproc_t)xdr_SYS_DYNAMIC_res;
                *psrv_proc     = (srvproc_t)campsrv_sysgetdyna_10400;
                break;

        case CAMPSRV_SYSDIR:
                *pxdr_argument = (xdrproc_t)xdr_FILE_req;
                *pxdr_result   = (xdrproc_t)xdr_DIR_res;
                *psrv_proc     = (srvproc_t)campsrv_sysdir_10400;
                break;

        case CAMPSRV_INSADD:
                *pxdr_argument = (xdrproc_t)xdr_INS_ADD_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insadd_10400;
                break;

        case CAMPSRV_INSDEL:
                *pxdr_argument = (xdrproc_t)xdr_DATA_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insdel_10400;
                break;

        case CAMPSRV_INSLOCK:
                *pxdr_argument = (xdrproc_t)xdr_INS_LOCK_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_inslock_10400;
                break;

        case CAMPSRV_INSLINE:
                *pxdr_argument = (xdrproc_t)xdr_INS_LINE_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insline_10400;
                break;

        case CAMPSRV_INSLOAD:
                *pxdr_argument = (xdrproc_t)xdr_INS_FILE_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insload_10400;
                break;

        case CAMPSRV_INSSAVE:
                *pxdr_argument = (xdrproc_t)xdr_INS_FILE_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_inssave_10400;
                break;

        case CAMPSRV_INSIFSET:
                *pxdr_argument = (xdrproc_t)xdr_INS_IF_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insifset_10400;
                break;

        case CAMPSRV_INSIFREAD:
                *pxdr_argument = (xdrproc_t)xdr_INS_READ_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insifread_10400;
                break;

        case CAMPSRV_INSIFWRITE:
                *pxdr_argument = (xdrproc_t)xdr_INS_WRITE_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insifwrite_10400;
                break;

        case CAMPSRV_INSIFDUMP:
                *pxdr_argument = (xdrproc_t)xdr_INS_DUMP_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insifdump_10400;
                break;

        case CAMPSRV_INSIFUNDUMP:
                *pxdr_argument = (xdrproc_t)xdr_INS_UNDUMP_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insifundump_10400;
                break;

        case CAMPSRV_INSIFON:
                *pxdr_argument = (xdrproc_t)xdr_DATA_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insifon_10400;
                break;

        case CAMPSRV_INSIFOFF:
                *pxdr_argument = (xdrproc_t)xdr_DATA_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_insifoff_10400;
                break;

        case CAMPSRV_VARSET:
                *pxdr_argument = (xdrproc_t)xdr_DATA_SET_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_varset_10400;
                break;

        case CAMPSRV_VARPOLL:
                *pxdr_argument = (xdrproc_t)xdr_DATA_POLL_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_varpoll_10400;
                break;

        case CAMPSRV_VARALARM:
                *pxdr_argument = (xdrproc_t)xdr_DATA_ALARM_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_varalarm_10400;
                break;

        case CAMPSRV_VARLOG:
                *pxdr_argument = (xdrproc_t)xdr_DATA_LOG_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_varlog_10400;
                break;

        case CAMPSRV_VARZERO:
                *pxdr_argument = (xdrproc_t)xdr_DATA_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_varzero_10400;
                break;

        case CAMPSRV_VARDOSET:
                *pxdr_argument = (xdrproc_t)xdr_DATA_SET_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_vardoset_10400;
                break;

        case CAMPSRV_VARREAD:
                *pxdr_argument = (xdrproc_t)xdr_DATA_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_varread_10400;
                break;

        case CAMPSRV_VARLNKSET:
                *pxdr_argument = (xdrproc_t)xdr_DATA_SET_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_varlnkset_10400;
                break;

        case CAMPSRV_VARGET:
                *pxdr_argument = (xdrproc_t)xdr_DATA_GET_req;
                *pxdr_result   = (xdrproc_t)xdr_CAMP_VAR_res;
                *psrv_proc     = (srvproc_t)campsrv_varget_10400;
                break;

        case CAMPSRV_CMD:
                *pxdr_argument = (xdrproc_t)xdr_CMD_req;
                *pxdr_result   = (xdrproc_t)xdr_RES;
                *psrv_proc     = (srvproc_t)campsrv_cmd_10400;
                break;

        default:
                *pxdr_argument = (xdrproc_t)xdr_void;
                *pxdr_result   = (xdrproc_t)xdr_void;
                *psrv_proc     = (srvproc_t)NULL;
                return;
    }
}


/*
 *  routine registered with the RPC server
 *
 *  called by svc_run_once->svc_getreqset.
 *
 *  This routine processes the request synchronously and sends the result
 *  to the CAMP client.
 *
 *  Preconditions:
 *    - global lock on
 */
void
camp_srv_10400_serial_in_single( struct svc_req *rqstp, SVCXPRT *transp )
{
    svc_mode02_inner( rqstp, transp );
}


/*
 *  routine registered with the RPC server
 *
 *  called by svc_run
 *
 *  This routine processes the request synchronously and sends the result
 *  to the CAMP client.
 *
 *  Preconditions:
 *    - global lock off
 */
void
camp_srv_10400_serial_in_multi( struct svc_req *rqstp, SVCXPRT *transp )
{
    set_thread_executing( 1 ); // before global lock
    mutex_lock_global_once(); // lock global when doing svc thread
    {
	svc_mode02_inner( rqstp, transp );
    }
    mutex_unlock_global_once();
    set_thread_executing( 0 );
}


/*
 *  routine registered with the RPC server
 *
 *  called by: svc_run
 *
 *  This function, running within a thread spawned from svc_run, processes 
 *  and replies to the rpc call.
 *
 *  Preconditions:
 *    - global lock off
 */
void
camp_srv_10400_threaded_svc_run( struct svc_req *rqstp, SVCXPRT *transp )
{
#if CAMP_MULTITHREADED

    int thread_data_index;
    THREAD_DATA* pThread_data;
    int globalMutexLockCount = 1;

    //  acquire and init thread_data before set_thread_executing

    mutex_lock_global_count( 1, globalMutexLockCount ); // lock global while acquiring thread_data 
    {
	thread_data_index = find_free_thread_data( CAMP_THREAD_TYPE_SVC, globalMutexLockCount );

	// _camp_log( "got thread_data_index %d", thread_data_index );

	if( set_thread_key_to_thread_data( thread_data_index ) != 0 )
	{
	    _camp_log( "error setting thread key" );
	    exit( CAMP_FAILURE );
	}

	pThread_data = get_thread_data( thread_data_index );

	init_thread_data( pThread_data, CAMP_THREAD_TYPE_SVC, CAMP_THREAD_STATE_RUNNING );

	if( _camp_debug(CAMP_DEBUG_THREAD) ) 
	{
	    _camp_log( "thread: index:%d handle:0x%lx"
			  , thread_data_index, pThread_data->thread_handle );
	}
    }
    mutex_lock_global_count( 0, globalMutexLockCount );

    //  handle rpc call with 'executing' and global mutex on 

    set_thread_executing( 1 ); // before global lock
    mutex_lock_global_once(); // lock global when doing svc thread
    {
	svc_mode02_inner( rqstp, transp );

	pThread_data->served = TRUE; // not used in this rpc thread mode
    }
    mutex_unlock_global_once();
    set_thread_executing( 0 );

    reset_thread_data( pThread_data, CAMP_THREAD_STATE_FINISHED ); // thread_data can be reused after this point

#else // !CAMP_MULTITHREADED 

    fprintf( stderr, "CAMP_RPC_THREAD_MODE_THREADED_SVC_RUN only sensible with CAMP_MULTITHREADED\n" );
    exit( CAMP_FAILURE );

#endif // CAMP_MULTITHREADED
}


/*
 *  svc_mode02_inner - inner handling of rpc call for camp_rpc_thread_mode 0 and 2 (0 includes 3 and 4)
 *
 *  Called by:
 *    camp_srv_10400_serial_in_single (mode0 singlethreaded) (sychronous rpc calls in single-threaded server)
 *    camp_srv_10400_serial_in_multi (mode0 multithreaded) (sychronous rpc calls in single-threaded server)
 *    camp_srv_10400_threaded_svc_run (mode2) (svc_run spawns threads)
 */
void
svc_mode02_inner( struct svc_req *rqstp, SVCXPRT *transp )
{
    CampRpcArgument argument;
    char* result;
    xdrproc_t xdr_argument, xdr_result;
    srvproc_t srv_proc;
    // timeval_t tv;

    /*
     *  Set these 'global' variables so that 
     *  other routines may access it.
     *  These variables are global for non-multithreading server
     *  and part of the thread_data structure of a multithreaded server.
     */
    // gettimeval( &tv );
    // set_tv_start( &tv );
    set_current_rqstp( rqstp );
    set_current_transp( transp );
    // set_orig_transp( transp );
    camp_setMsg( "" ); // incoming msg may not be blank in mode0 case

    /*
     *  get the argument, result and dispatch routines for the RPC call
     */
    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &srv_proc );

    bzero( (void*)&argument, sizeof( CampRpcArgument ) );

    set_current_xdr_flag( CAMP_XDR_ALL );

    /* 
     *  Get argument
     *  We are already in a global lock to ensure that
     *  this is the only thread calling this routine.
     */
    if( !svc_getargs( transp, xdr_argument, (caddr_t)&argument ) ) 
    {
	svcerr_decode( transp );
	return;
    }

    /*
     *  Process the request
     */
    result = (*srv_proc)( &argument, rqstp );

    if( result != NULL ) 
    {
	RES* result_res = (RES*)result; // danger

	result_res->msg = camp_getMsg();
	if( _failure( result_res->status ) )
	{
	    camp_log( "%s", result_res->msg );
	}
    }

    /* 
     *  Send reply
     */
    if( ( result != NULL ) && !svc_sendreply( transp, xdr_result, result ) )
    {
	svcerr_systemerr(transp);
    }

    /*
     *  note that there is nothing nested within any result structure that requires free'ing
     *  (result->msg points to a global or thread-specific message string, and other 
     *  pointers point to the system database)
     */
    _free( result );

    /* 
     *  Free argument
     */
    if( !svc_freeargs( transp, xdr_argument, (caddr_t)&argument ) ) 
    {
	_camp_log( "unable to free arguments" );
    }

    set_current_xdr_flag( CAMP_XDR_ALL );

    camp_setMsg( "" );
}


/*
 *  routine registered with the RPC server
 *
 *  called by: svc_run_once->my_svc_getreqset
 *
 *  This routine starts an SVC thread that asynchronously processes the RPC
 *  request.
 *
 *  Preconditions:
 *    - global lock on
 */
void
camp_srv_10400_threaded_svc_getreqset( struct svc_req *rqstp, SVCXPRT *transp )
{
#if CAMP_MULTITHREADED

    start_svc_mode1_thread( rqstp, transp );

#else // !CAMP_MULTITHREADED 

    fprintf( stderr, "CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET only sensible with CAMP_MULTITHREADED\n" );
    exit( CAMP_FAILURE );

#endif // CAMP_MULTITHREADED
}


#if CAMP_MULTITHREADED


/*
 *  start_svc_mode1_thread  -  start an svc (service) thread from the main thread
 *
 *  Preconditions:
 *              The global lock must be ON.
 *
 *  Postconditions:
 *              The global lock is ON again before returning to the calling
 *              routine.
 */
void
start_svc_mode1_thread( struct svc_req *rqstp, SVCXPRT *transp )
{
    int thread_data_index;
    pthread_attr_t thread_attr;
    struct authunix_parms* aupp;
    xdrproc_t xdr_argument, xdr_result;
    srvproc_t srv_proc;
    THREAD_DATA* pThread_data;

    /*
     *  get the argument, result and dispatch routines for the RPC call
     */
    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &srv_proc );

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "thread 0x%x, serving request type %u", pthread_self(), rqstp->rq_proc ); // format generic for vxworks
    }

    /* 
     *  Allocate thread index for thread
     *  This routine is called within global lock, so unlock
     *  global lock while calling find_free_thread_data.
     */
    thread_data_index = find_free_thread_data( CAMP_THREAD_TYPE_SVC, get_mutex_lock_count_global() );

    pThread_data = get_thread_data( thread_data_index );

    //  20140218  TW  unnecessary lock: this is the main thread which has found an unused thread
    // if( mutex_lock_thread( &(pThread_data->mutex_handle) ) != 0 )
    // {
    //     _camp_log( "error locking thread mutex" );
    //     return;
    // }

    /*  
     *  16-Dec-1999  TW  initialize thread parameters before creating thread 
     */
    init_thread_data( pThread_data, CAMP_THREAD_TYPE_SVC, CAMP_THREAD_STATE_RUNNING );

    pThread_data->transp = transp;
    /* bcopy( transp, &(pThread_data->orig_trans), sizeof( SVCXPRT ) ); */ /*  16-Dec-1999  TW  */

    /*
     *  Make a copy of rqstp
     *
     *  This is necessary even when managing our own svc_getreqset
     *  because some of the authunix structure is managed inside the
     *  internal RPC dispatch (between svc_getreqset and camp_srv).
     */
    pThread_data->rqstp = (struct svc_req*)camp_zalloc( sizeof( struct svc_req ) );
    bcopy( (void*)rqstp, (void*)pThread_data->rqstp, sizeof( struct svc_req ) );

    pThread_data->rqstp->rq_clntcred = (caddr_t)camp_zalloc( sizeof( struct authunix_parms ) );
    bcopy( (void*)rqstp->rq_clntcred, (void*)pThread_data->rqstp->rq_clntcred, sizeof( struct authunix_parms ) );

    aupp = (struct authunix_parms*)pThread_data->rqstp->rq_clntcred;
    aupp->aup_machname = strdup( ((struct authunix_parms*)rqstp->rq_clntcred)->aup_machname );

    aupp->aup_gids = (gid_t*)camp_zalloc( (aupp->aup_len)*sizeof( int ) );
    bcopy( (void*)((struct authunix_parms*)rqstp->rq_clntcred)->aup_gids, (void*)aupp->aup_gids, (aupp->aup_len)*sizeof( int ) );

    /* 
     *  Get argument
     *
     *  RPC library is non-reentrant, must be inside global lock
     *  (start_svc_mode1_thread routine called from within global lock)
     *  
     *  Call svc_getargs() from the main thread (svc listener) only.
     *  Under VxWorks, calling svc_getargs() from a separate
     *  task doesn't work (doesn't inherit RPC server capabilities).
     *  svc_sendreply does work from other threads or tasks, but
     *  in CAMP the main thread handles that as well.
     */
    bzero( (void*)&(pThread_data->argument), sizeof( CampRpcArgument ) );
    if( !svc_getargs( transp, (xdrproc_t)xdr_argument, (char*)&(pThread_data->argument) ) ) 
    {
        svcerr_decode(transp);
        return;
    }

    // if( mutex_unlock_thread( &(pThread_data->mutex_handle) ) != 0 )
    // {
    //     _camp_log( "error unlocking thread mutex" );
    //     return;
    // }

    if( pthread_attr_init( &thread_attr ) != 0 )
    {
        _camp_log( "error creating thread attributes" );
        return;
    }

    /*
     *  Explicitly set to inherit scheduling
     *  policy and priority from creating thread
     */
    if( pthread_attr_setinheritsched( &thread_attr, PTHREAD_INHERIT_SCHED ) != 0 )
    {
        _camp_log( "error setting thread attributes" );
        return;
    }

    if( pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED ) != 0 )
    {
        _camp_log( "error setting thread attributes" );
        return;
    }

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "creating thread %d:", thread_data_index );
    }

    /* pass integer value i as fake pointer */
    if( pthread_create( &(pThread_data->thread_handle), 
                        &thread_attr, /* pthread_attr_default, */ 
                        svc_mode1_thread, 
                        integerToPointer( thread_data_index ) ) != 0 ) // (void*)thread_data_index ) != 0 ) // 20140221 TW
    {
        _camp_log( "error creating thread" );
        return;
    }

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "created thread %d with handle 0x%x.", thread_data_index, pThread_data->thread_handle );
    }
    
    if( pthread_attr_destroy( &thread_attr ) != 0 )
    {
        _camp_log( "error deleting thread attributes" );
        return;
    }
    
    /*
     *  Reclaim internal storage for thread
     *  immediately upon thread exit
     *  20140306  TW  use pthread_attr_setdetachstate instead
     */
    //if( pthread_detach( (pThread_data->thread_handle) ) != 0 )
    //{
    //    _camp_log( "error detaching thread" );
    //    return;
    //}

    if( _camp_debug(CAMP_DEBUG_THREAD) ) 
    {
	_camp_log( "thread: index:%d handle:0x%lx"
		      , thread_data_index, pThread_data->thread_handle );
    }
}


/*
 *  svc_mode1_thread  -  starting point of a service thread
 */
pthread_addr_t
svc_mode1_thread( pthread_addr_t arg )
{
    int thread_data_index;
    THREAD_DATA* pThread_data;

    thread_data_index = pointerToInteger( arg ); // (int)arg; // 20140221  TW

    /*
     *  Set the thread_key to the pointer to the thread data structure
     *  This allows access to the structure from within the thread
     */
    if( set_thread_key_to_thread_data( thread_data_index ) != 0 )
    {
    	_camp_log( "error setting thread key" );
	exit( CAMP_FAILURE );
        // return( (pthread_addr_t)(-1) );
    }

    pThread_data = get_thread_data( thread_data_index );

#ifdef VXWORKS
     /*  17-Dec-1999  TW  No longer needed.  Only main thread calls RPCs */
     /* rpcTaskInit(); */   /* must be done for every task that uses RPCs */
#endif /* VXWORKS */

    /*
     *  svc_mode1_thread_inner (i.e., service processing) must be called from 
     *  within global lock.  This is required because we cannot be
     *  sure that the Tcl and RPC libraries (etc.) are thread safe.
     *  The global lock is released within the service during periods
     *  of lengthy waiting (RS232 and GPIB reads/writes).
     */
    set_thread_executing( 1 ); // before global lock
    mutex_lock_global_once(); // lock global when doing svc thread
    {
	if( _camp_debug(CAMP_DEBUG_SVC) ) 
	{
	    _camp_log( "thread %d (0x%x) calling svc_mode1_thread_inner for request type %u",
		       thread_data_index, pthread_self(), pThread_data->rqstp->rq_proc );
	}

	svc_mode1_thread_inner( pThread_data->rqstp, pThread_data->transp );

	if( _camp_debug(CAMP_DEBUG_SVC) ) 
	{
	    _camp_log( "thread %d (0x%x) finished==================================", thread_data_index, pthread_self() );
	}
    }
    // camp_fsleep( 2.1 ); /* debug DJA */
    mutex_unlock_global_once();
    set_thread_executing( 0 );

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "thread %d (0x%x) post-finished", thread_data_index, pthread_self() );
    }

    /*
     *  Return a value, this program doesn't check it.  Returning causes the thread to be ended.
     */

    return( (pthread_addr_t)arg );
}


/*
 *  svc_mode1_thread_inner
 *
 *  Provides the actual processing of the RPC request within an SVC thread.
 *  Calls the appropriate routine corresponding to the particular request.
 *
 *  Reports that the processing is completed in the thread private data
 *  structure before returning.
 */
void
svc_mode1_thread_inner( struct svc_req* rqstp, SVCXPRT* transp )
{
    char *result;
    xdrproc_t xdr_argument, xdr_result;
    srvproc_t srv_proc;
    THREAD_DATA* pThread_data;
    CampRpcArgument* pArgument;

    /*
     *  get the argument, result and dispatch routines for the RPC call
     */
    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &srv_proc );

    pThread_data = get_thread_data_this();
    pArgument = &(pThread_data->argument); // RPC argument, already processed

    if( _camp_debug(CAMP_DEBUG_SVC) )
    {
        _camp_log( "thread 0x%x", pthread_self() );
#if ( CAMP_TIRPC == 0 ) // these structures not quite the same with TI-RPC
	_camp_log( "before processing request (%lu)", rqstp->rq_proc );
	if( rqstp->rq_proc==CAMPSRV_CMD ) 
	{
  	    // camp_log( "   command :  %s", pThread_data->argument ); // 20140310  TW  argument is a union of structs
	    camp_log( "   interp    = 0x%p", pThread_data->interp ); 
	}
	camp_log( "   served             = %d", pThread_data->served );
        camp_log( "   transp             = 0x%p", transp );
        camp_log( "   transp->xp_sock    = %d", transp->xp_sock );
        camp_log( "   transp->xp_port    = %d", transp->xp_port );
        camp_log( "   transp->xp_ops     = %p", transp->xp_ops );
        camp_log( "   transp->xp_addrlen = %d", transp->xp_addrlen );
	camp_log( "   transp->xp_raddr.sin_family = %d", transp->xp_raddr.sin_family );
	camp_log( "   transp->xp_raddr.sin_port   = %d", transp->xp_raddr.sin_port );
	camp_log( "   transp->xp_raddr.sin_addr.s_addr = %u", transp->xp_raddr.sin_addr.s_addr );
        camp_log( "   transp->xp_p1      = 0x%p", transp->xp_p1 );
#endif // !TIRPC
	camp_log( "   SVC_STAT( transp ) = %d", SVC_STAT( transp ) );
    }

    /*
     *  Process the request
     */
    result = (*srv_proc)( pArgument, rqstp );

    if( result != NULL ) 
    {
        RES* result_res = (RES*)result; // danger

        result_res->msg = camp_getMsg();
        if( _failure( result_res->status ) )
        {
            camp_log( "%s", result_res->msg );
        }
    }

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "thread 0x%x got result", pthread_self() );
    }

    // 20140225  TW  thread mutex unnecessary
    // mutex_lock_thread( &(pThread_data->mutex_handle) );
    pThread_data->pResult = result;

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "thread 0x%x stored result", pthread_self() );
    }

    /*
     *  Notify main thread that this thread has been served
     *  Main thread will then make the RPC reply and clean up
     */
    pThread_data->served = TRUE; // warning: main thread will take over and process the RPC reply after this setting
    // mutex_unlock_thread( &(pThread_data->mutex_handle) );

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "thread 0x%x finished", pthread_self() );
    }

    // end_svc_mode1_thread( -1 ); // Moved to srv_handle_served_svc_mode1_threads
}


/*
 *  end_svc_mode1_thread() - finish processing service thread
 *                     called from main thread
 *
 *  Purpose:  Send RPC reply, then clean up after an svc thread
 *            Done from the main thread because it is safer to 
 *            call all RPC from the main thread on VxWorks. Note 
 *            that the svc thread is likely already ended before 
 *            end_svc_mode1_thread is invoked.
 *
 *  Preconditions:
 *              The global lock must be ON.
 *
 *  Postconditions:
 *              The global lock is ON again before returning to the calling
 *              routine.
 */
void 
end_svc_mode1_thread( int arg )
{
    int thread_data_index;
    char *result;
    xdrproc_t xdr_argument, xdr_result;
    srvproc_t srv_proc;
    THREAD_DATA* pThread_data;
    CampRpcArgument* pArgument;
    bool_t lost_client;
    struct svc_req *rqstp;
    SVCXPRT *transp;

    thread_data_index = arg;

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "thread %d (0x%x)", thread_data_index, pthread_self() );
    }

    if( thread_data_index == -1 )
    {
        pThread_data = get_thread_data_this(); // called from svc thread (obsolete)
    }
    else
    {  
        pThread_data = get_thread_data( thread_data_index ); // called from main thread
    }
    
    pArgument = &(pThread_data->argument);
    transp = pThread_data->transp;
    rqstp = pThread_data->rqstp;
    result = pThread_data->pResult;

    /*
     *  get the argument, result and dispatch routines for the RPC call
     */
    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &srv_proc );

    if( !pThread_data->served )
    {
	_camp_log( "error ending thread that hasn't been served" );
	return;
    }

    /*
     *  Test whether client still exists
     *  Sending reply to absent client can cause unwanted behaviour
     */
    /* lost_client = test_lost_client( transp, &(pThread_data->orig_trans) ); */
    lost_client = test_lost_client( transp );

    if( _camp_debug(CAMP_DEBUG_SVC) )
    {
	_camp_log( "lost_client = %d", lost_client );

	_camp_log( "before svc_sendreply:" );
	camp_log( "   served             = %d", pThread_data->served );
	camp_log( "   transp             = %p", transp );
#if ( CAMP_TIRPC == 0 ) // these structures not quite the same with TI-RPC
	camp_log( "   transp->xp_sock    = %d", transp->xp_sock );
	camp_log( "   transp->xp_port    = %d", transp->xp_port );
	camp_log( "   transp->xp_ops     = %p", transp->xp_ops );
	camp_log( "   transp->xp_addrlen = %d", transp->xp_addrlen );
	camp_log( "   transp->xp_raddr.sin_family = %d", transp->xp_raddr.sin_family );
	camp_log( "   transp->xp_raddr.sin_port   = %d", transp->xp_raddr.sin_port );
	camp_log( "   transp->xp_raddr.sin_addr.s_addr = %x", transp->xp_raddr.sin_addr.s_addr );
	camp_log( "   transp->xp_p1      = %p", transp->xp_p1 );
#endif // !TIRPC
	camp_log( "   SVC_STAT( transp ) = %d", SVC_STAT( transp ) );
    }

    if( lost_client )
    {
	if( _camp_debug(CAMP_DEBUG_SVC) )
	{
	    _camp_log( "lost client connection, not sending reply" );
	}
    }
    else
    {
	/*
	 *  CAUTION.  We are in the main thread.  But, we want
	 *  the xdr_flag to be equal to the thread's xdr_flag when
	 *  sending the RPC reply.  So, temporarilly set the main
	 *  thread's xdr_flag to the svc thread's for the svc_sendreply.
	 *  Afterwards, set back to CAMP_XDR_ALL
	 */
	set_current_xdr_flag( pThread_data->xdr_flag );

	/* 
	 *  Send reply
	 *  No need for global lock around RPC library calls here
	 *  svc_mode1_thread_inner is called inside global lock
	 */
	if( ( result != NULL ) && !svc_sendreply( transp, xdr_result, result ) )
	{
	    svcerr_systemerr(transp);
	}

	set_current_xdr_flag( CAMP_XDR_ALL );
    }

    if( _camp_debug(CAMP_DEBUG_SVC) )
    {
	_camp_log( "did sendreply, now free result/args/req" );
    }

    /*
     *  free the RES structure returned from camp_srv_proc routines
     */
    _free( result );

    /* 
     *  Free RPC arguments
     */
    if( !svc_freeargs( transp, xdr_argument, (char*)pArgument ) ) 
    {
	_camp_log( "unable to free arguments" );
    }

    /*
     *  Free up RPC request storage allocated in start_svc_mode1_thread
     */
    {
	struct authunix_parms* aupp;
	aupp = (struct authunix_parms*)(pThread_data->rqstp->rq_clntcred);
	free( aupp->aup_machname );
	free( aupp->aup_gids );
	free( aupp );
	free( pThread_data->rqstp );
    }

    /*
     *  Clean up thread-specific data structure
     *
     *  thread_data can be reused by main thread after this point
     */
    reset_thread_data( pThread_data, CAMP_THREAD_STATE_FINISHED ); // do_thread_finished2( pThread_data );
 
    camp_setMsg( "" ); // note: this is the main thread's msg, not the svc thread's

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "finished" );
    }
}


/*
 *  srv_handle_served_svc_mode1_threads  -  CAMP server main thread calls this
 *         routine to check if there are any SVC threads that have finished
 *         processing the RPC request.  If so, the main thread sends the
 *         reply to the RPC client program and cleans up the thread data
 *         structure.
 *
 *  Preconditions:
 *              global lock ON
 *              (take global lock before calling dangerous RPC calls in
 *               end_svc_mode1_thread and SVC_STAT/SVC_DESTROY)
 *
 *  Postconditions:
 *              global lock ON 
 *
 *  20140303  TW  moved global locking to calling routine
 *                (who cares if making RPC replies takes a little time)
 */
void
srv_handle_served_svc_mode1_threads( void )
{
    int thread_data_index;

    if( !mutex_lock_global_check() )
    {
	// todo: error
    }

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "begin" );
    }

    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
        THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	// 20140225  TW  thread mutex unnecessary
	/*
	 *  Take the thread lock before accessing the thread private data
	 */
	// if( pthread_mutex_trylock( &(pThread_data->mutex_handle) ) == 0 ) 
	{
	    /*
	     *  Check for SVC threads that are active (CAMP_THREAD_STATE_RUNNING) but also
	     *  served (finished processing)
	     */
	    if( ( pThread_data->type == CAMP_THREAD_TYPE_SVC ) &&
		( pThread_data->state == CAMP_THREAD_STATE_RUNNING ) &&
		( pThread_data->served == TRUE ) )
	    {
  	        SVCXPRT* transp = pThread_data->transp; // copy before end_svc_mode1_thread

		if( _camp_debug(CAMP_DEBUG_SVC) ) 
		{
		    _camp_log( "ending thread %d", thread_data_index );
		}

		/*
		 *  Unlock a thread lock before taking the global lock
		 *  This relieves possible deadlocks
		 *
		 *  20140218  TW  changed mutex strategy: give up global mutex while
		 *                waiting for thread mutex (i.e., if another thread is
		 *                waiting for this thread mutex - which is unlikely anyway -
		 *                it won't have the global mutex)
		 */
		// mutex_unlock_thread( &(pThread_data->mutex_handle) );

		/*
		 *  Take global lock before calling dangerous RPC calls in
		 *  end_svc_mode1_thread and SVC_STAT/SVC_DESTROY
		 */
		// mutex_lock_global_once(); // lock global when replying to RPC calls
	      
		/*
		 *  Reply to the RPC client program with the result of the
		 *  request and clean up the SVC thread data structure and state.
		 */
		end_svc_mode1_thread( thread_data_index );

		/*
		 *  This moved from svc_getreqset
		 *  checks whether transport has died
		 *  If so, destroy it.
		 */

		if( _camp_debug(CAMP_DEBUG_SVC) ) 
		{
		    _camp_log( "checking thread transport" );
		}

		/*
		 *  Might not need this here.  SVC_DESTROY in my_svc_getreqset
		 *  may be sufficient.  But, it does not hurt.
		 */
		if( SVC_STAT( transp ) == XPRT_DIED )
		{
		    if( _camp_debug(CAMP_DEBUG_SVC) ) 
		    {
			_camp_log( "destroying thread transport" );
		    }

		    SVC_DESTROY( transp );
		}

		// mutex_unlock_global_once();
	    }
	    else
	    {
		//  20140218  TW  changed mutex strategy
		// mutex_unlock_thread( &(pThread_data->mutex_handle) );
	    }

	    //  20140225  TW  thread mutex unnecessary
	    /*
	     *  20140218  TW  changed mutex strategy - prevent deadlocks between global
	     *                mutex and thread mutex by giving up the global when taking
	     *                the thread
	     */
	    // mutex_unlock_thread( &(pThread_data->mutex_handle) );
	}
    }

    if( _camp_debug(CAMP_DEBUG_SVC) ) 
    {
	_camp_log( "end" );
    }
}

#endif /* CAMP_MULTITHREADED */

/*
 *  17-Dec-1999  TW  Made robust.  Main thread makes all RPC calls
 *                   now.  As a result the SVC_STAT check works properly.
 *
 *  OLD COMMENT:
 *    Test whether client
 *    has timed out.  This is not a good enough
 *    check, but sometimes works.  I think that
 *    it works unless another client comes in and
 *    starts using the same transp structure that
 *    the timed out client had been using.  Then,
 *    the server goes to reply to the wrong client
 *    request.
 *    How do I know if the client connection is lost?
 *    (i.e., if client timed out)
 *    Comparing the transport xp_sock seems to work every 
 *    time for VxWorks, but others might be OK too (like sin_port).
 *
 *    If the client is lost, then will not calling svc_sendreply
 *    and svc_freeargs cause some storage to not be freed
 *    properly.
 */
bool_t
test_lost_client( SVCXPRT* transp )
{
    if( SVC_STAT( transp ) == XPRT_DIED ) return( TRUE );

    // This was old way
    // if( ( bcmp( transp, orig_transp, sizeof( SVCXPRT) ) != 0 ) )
    // {
    //     return( TRUE );
    // }
    // else
    // {
    //     if( SVC_STAT( transp ) != XPRT_IDLE )
    // {
    //     return( TRUE );
    // }
    // }

    return( FALSE );
}
