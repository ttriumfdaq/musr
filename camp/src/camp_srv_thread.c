/*
 *  Name:       camp_srv_thread.c
 *
 *  Purpose:    Handle all thread specific data structures and most of the
 *              issues of multithreading.
 *
 *  Called by:  CAMP server routines only (various).
 * 
 *  Revision history:
 *
 *    20140420     TW  execution_mutex renamed sysmod_mutex
 *    20140418     TW  retire sys/varlist_mutexes
 *    20140409     TW  removed old commented-out code related to free_thread 
 *                     mutex/condition and gpib_mutex
 *    20140306     TW  extracted from camp_srv_svc_mod.c
 *    20140218     TW  exit(0) -> exit(CAMP_FAILURE) for failures
 *    20140217     TW  tidying CAMP_DEBUG
 *                     rename camp_thread_mutex_lock to mutex_lock
 *                     rename pthread_mutex_unlock to mutex_unlock
 *                     rename set_global_mutex_noChange to mutex_lock_mainInterp
 *                     rename thread_un/lock_global_np to mutex_un/lock_global
 *                     rename get/release_ins_thread_lock -> mutex_un/lock_ins
 *    09-Feb-2001  DJA Dump/Undump
 *    21-Dec-1999  TW  orig_trans (copy of transport structure) no longer
 *                     used.  All use of transports now in main thread
 *                     _exclusively_.  Only main thread handles (its own)
 *                     RPC transport structures.
 *    21-Dec-1999  TW  free_thread mutex and condition no longer used
 *                     find_free_thread_data (and try_find_free_thread_data) now 
 *                     check explicitly for a free thread.
 *
 *  $Log: camp_srv_thread.c,v $
 *  Revision 1.3  2018/06/13 00:32:16  asnd
 *  Commented debugging lines
 *
 *  Revision 1.2  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.4  2004/12/18 01:02:28  asnd
 *  Alter debugging messages
 *
 *  Revision 1.3  2001/02/10 07:14:49  asnd
 *  DJA: insIfDump / insIfUndump API routines
 *
 */

#define __USE_UNIX98 // for pthread_mutexattr_settype

#include <string.h>
#include <stdlib.h>
#include <errno.h>
// #include <signal.h>
#include "camp_srv.h"
#ifdef VXWORKS
#include "sysLib.h" // sysClkRateGet
#include "kernelLib.h" // kernelTimeSlice
#endif // VXWORKS

/*
 *  20140324  TW  thread_data as linked-list is dodgy:
 *                the linked-list only increases in size, so if the list is
 *                added-to in one thread it should not harm another thread
 *                looping over the list, but it's nasty nonetheless
 *                so just allocate one maximum thread_data, and use as an array
 */
//#define THREAD_DATA_IS_LINKED_LIST

#if CAMP_MULTITHREADED

/*
 *  Declare the thread private data
 */
/* #if defined( VXWORKS ) */
/* #define THREAD_DATA_SIZE_MAX 10  // historic maximum */
/* #else // !VXWORKS */
/* #define THREAD_DATA_SIZE_MAX 42 */
/* #endif // VXWORKS */

static int thread_data_size = 0;
static THREAD_DATA* thread_data = NULL;

static pthread_key_t thread_key;

/*
 *  The CAMP global mutex.  This is very important.  It allows restriction
 *  of processing to one thread at a time during operations which are not
 *  assured of being safe under multithreading conditions and also to block
 *  the use of resources to one thread.
 */
static pthread_mutex_t global_mutex; // = PTHREAD_MUTEX_INITIALIZER;

/*
 *  Mutex that governs access to the shutdown_flag that signals the CAMP
 *  server to shutdown.
 *
 *  20140225  TW  unnecessary: only ever set TRUE, no competition
 */
// static pthread_mutex_t shutdown_mutex = PTHREAD_MUTEX_INITIALIZER;

/*
 *  19-Dec-1999  TW  mainInterp_mutex for exclusive use of the main Tcl
 *                   interpreter.
 */
static pthread_mutex_t mainInterp_mutex; // = PTHREAD_MUTEX_INITIALIZER;

/*
 *  20140225  TW  sys_mutex. control access to pSys and pVarList.
 */
// static pthread_mutex_t sys_mutex; // = PTHREAD_MUTEX_INITIALIZER;
// static pthread_mutex_t varlist_mutex; // = PTHREAD_MUTEX_INITIALIZER;
/*
 *  20140304  TW  
 */
static pthread_mutex_t sysmod_mutex; // = PTHREAD_MUTEX_INITIALIZER;

static int mutex_lock( pthread_mutex_t * mutex );
static int mutex_unlock( pthread_mutex_t * mutex );
// static int mutex_lock_thread( pthread_mutex_t * mutex );
// static int mutex_unlock_thread( pthread_mutex_t * mutex );
static void thread_key_destructor( void* pThread_data );
static pthread_addr_t main2_thread( pthread_addr_t arg );

#else /* !CAMP_MULTITHREADED */

static struct svc_req* current_rqstp = NULL;
static SVCXPRT* current_transp = NULL;
// static SVCXPRT orig_trans;  /* 16-Dec-1999  TW  Added for proper timeout check - multithreading only */
#ifdef UNUSED
static timeval_t tv_start;
#endif // UNUSED
// static char* pResult;       /* 17-Dec-1999  TW  For compatibility with single threading, not used */
// static short svc_served;    /* 17-Dec-1999  TW  For compatibility with single threading, not used */

#endif /* CAMP_MULTITHREADED */

/*
 *  Used for multi- and single-threaded versions
 */
static int shutdown_flag = FALSE;
extern int camp_rpc_thread_mode;
extern int camp_max_threads;

#if (CAMP_MULTITHREADED && CAMP_MUTEX_SYSTEM_MODIFICATIONS) || defined( THREAD_DATA_IS_LINKED_LIST )

static int fatal_error_1( const char* fmt, const char* msg1 )
{
    char msg[LEN_MSG+1];
    camp_snprintf( msg, sizeof( msg ), fmt, msg1 );

    fprintf( stderr, fmt, msg1 );
    // camp_setMsg( fmt, msg1 ); camp_log( "%s", camp_getMsg() ); // avoid using thread_data->msg here
    camp_log( "%s", msg );
    exit( CAMP_FAILURE );
    return CAMP_FAILURE; // _errno;
}

#endif // (CAMP_MULTITHREADED && CAMP_MUTEX_SYSTEM_MODIFICATIONS) || defined( THREAD_DATA_IS_LINKED_LIST )

static int fatal_error_2( const char* fmt, const char* msg1, const char* msg2 )
{
    char msg[LEN_MSG+1];
    camp_snprintf( msg, sizeof( msg ), fmt, msg1, msg2 );

    fprintf( stderr, fmt, msg1, msg2 );
    // camp_setMsg( fmt, msg2, msg1 ); camp_log( "%s", camp_getMsg() ); // avoid using thread_data->msg here
    camp_log( "%s", msg );
    exit( CAMP_FAILURE );
    return CAMP_FAILURE; // _errno;
}

static int error_using_mutex_with_no_thread_data( const char* function_name, const char* mutex_name )
{
    return fatal_error_2( "%s: attempt to use %s mutex before initializing thread_data", function_name, mutex_name );
}

#if CAMP_MULTITHREADED && CAMP_MUTEX_SYSTEM_MODIFICATIONS
static int error_no_thread_data( const char* function_name )
{
    return fatal_error_1( "%s: thread_data not initialized", function_name );
}
#endif

#if CAMP_MULTITHREADED


static void thread_key_destructor( void* pThread_data )
{
}

/* int set_thread_key( void* thread_key_data ) */
/* { */
/*     return pthread_setspecific( thread_key, thread_key_data ); */
/* } */

int set_thread_key_to_thread_data( int thread_data_index )
{
    return pthread_setspecific( thread_key, (void*)integerToPointer( thread_data_index ) );
}

THREAD_DATA* get_thread_data( int thread_data_index_in )
{
#if defined( THREAD_DATA_IS_LINKED_LIST )

    int thread_data_index;
    THREAD_DATA* pThread_data;

    for( thread_data_index = 0, pThread_data = thread_data;
	 thread_data_index < get_thread_data_size();
	 thread_data_index++, pThread_data = pThread_data->pNext )
    {
	if( pThread_data == NULL )
	{
	    fatal_error_1( "%s: unexpected NULL THREAD_DATA", __FUNCTION__ );
	}

	if( thread_data_index == thread_data_index_in ) 
	{
	    break;
	}
    }

    if( pThread_data == NULL )
    {
	char msg[LEN_MSG+1];
	camp_snprintf( msg, sizeof( msg ), "%s: thread_data for thread_data_index (%d) not found", __FUNCTION__, thread_data_index_in );
	fatal_error_1( "%s", msg );
    }
    
    return( pThread_data );

#else

    return &thread_data[thread_data_index_in];

#endif
}

THREAD_DATA* get_thread_data_this()
{
    // return pthread_getspecific( thread_key );
    // return &thread_data[ get_thread_data_this_index() ];
    return get_thread_data( get_thread_data_this_index() );
}

int get_thread_data_this_index()
{
    return pointerToInteger( pthread_getspecific( thread_key ) );
}

int get_thread_data_size()
{
    return thread_data_size; // NUM_THREADS;
}


/*
 *  replace pthread_mutex_lock with a non-blocking version
 *
 *  why was this necessary?  (pthread_mutex_lock shouldn't block)
 *  I think it was a workaround for a broken VxWorks semTake( WAIT_FOREVER )
 *  In which case, this workaround could be put in the pthread_mutex_lock
 *  in vx_pthread.c.  It shouldn't be necessary for Linux.
 *
 *  20140217  tw  camp_thread_mutex_lock -> mutex_lock
 */
static int
mutex_lock( pthread_mutex_t* mutex )
{
#ifdef MOVED_THIS_WORKAROUND_TO_VXWORKS_pthread_mutex_lock
    /*
     *  20140220  TW  moved this workaround to pthread_mutex_lock for VxWorks (in vx_pthread.c)
     *                as it is assumed it was only needed for VxWorks
     */
    int mutex_status; 

    while( ( mutex_status = pthread_mutex_trylock( mutex ) ) != 0 ) 
    {
	// caution: for vxworks, pthread_mutex_trylock returns -1 on error

	if( (mutex_status == EINVAL) || (mutex_status == EAGAIN) ) 
	{
	    char* tok = ( (mutex_status==EINVAL) ? "EINVAL" : (mutex_status==EAGAIN) ? "EAGAIN" : "" );
            _camp_log( "pthread_mutex_trylock returned %s", tok );
            exit( CAMP_FAILURE );
	}
	
	camp_fsleep( 0.01 ); /* Wait for 10 ms */
    }

    return( 0 );
#endif // MOVED_THIS_WORKAROUND_TO_VXWORKS_pthread_mutex_lock

    int mutex_status = pthread_mutex_lock( mutex );

    // caution: for vxworks, pthread_mutex_lock returns -1 on error

    if( (mutex_status == EINVAL) || (mutex_status == EAGAIN) || (mutex_status == EDEADLK) ) 
    {
	char* tok = ( (mutex_status==EINVAL) ? "EINVAL" : (mutex_status==EAGAIN) ? "EAGAIN" : "EDEADLK" );
	_camp_log( "pthread_mutex_lock returned %s", tok );
	exit( CAMP_FAILURE );
    }

    return mutex_status;
}


static int
mutex_unlock( pthread_mutex_t* mutex )
{
    return pthread_mutex_unlock( mutex );
}


static int 
mutex_lock_with_global_release( pthread_mutex_t* mutex )
{
    int mutex_status = pthread_mutex_trylock( mutex );

    // caution: for vxworks, pthread_mutex_trylock returns -1 on error
    // if( mutex_status == EBUSY ) // if fail to get, then wait
    if( mutex_status != 0 ) // if fail to get, then wait
    {
        int globalMutexLockCount = mutex_unlock_global_all();
	mutex_status = mutex_lock( mutex );
        mutex_lock_global_all( globalMutexLockCount );
    }

    return( mutex_status );
}
#endif /* CAMP_MULTITHREADED */


/*
 *  replace pthread_lock_global_np with something that gives us more control
 *  (i.e., of recursion)
 *
 *  20140218  TW  renamed mutex_lock_global_once (as opposed to mutex_lock_global_all)
 *                fixed so that counter increments when called on subsequent calls
 *  20140217  TW  renamed mutex_lock_global (was thread_lock_global_np)
 */
int
mutex_lock_global_once( void )
{
#if CAMP_MULTITHREADED
    
    int mutex_status = 0;
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	error_using_mutex_with_no_thread_data( "mutex_lock_global_once", "global" );
	return EINVAL;
    }

/*     if( _camp_debug(CAMP_DEBUG_MUTEX) ) */
/*     { */
/* 	if( pThread_data->mutex_count_global > 0 )  */
/* 	{ */
/* 	    if( pThread_data->type == CAMP_THREAD_TYPE_MAIN ) printf( "main thread already has global lock\n" ); */
/* 	    else if( pThread_data->type == CAMP_THREAD_TYPE_SVC ) printf( "svc thread already has global lock\n" ); */
/* 	} */
/* 	else */
/* 	{ */
/* 	    if( pThread_data->type == CAMP_THREAD_TYPE_MAIN ) printf( "main thread getting global lock\n" ); */
/* 	    else if( pThread_data->type == CAMP_THREAD_TYPE_SVC ) printf( "svc thread getting global lock\n" ); */
/* 	} */
/*     } */

    /*
     *        this implements global_mutex as non-recursive, so
     *        it should be an error to take it twice (otherwise
     *        we'll give it early)
     */
/*     if( pThread_data->mutex_count_global > 0 )  */
/*     { */
/* 	return( 0 ); */
/*     } */
/*     else */
/*     { */
/* 	int mutex_status = mutex_lock( &global_mutex ); */
/* 	pThread_data->mutex_count_global++; */
/* 	return mutex_status; */
/*     } */

    if( pThread_data->mutex_count_global == 0 )
    {
	mutex_status = mutex_lock( &global_mutex );
    }

    if( mutex_status == 0 ) pThread_data->mutex_count_global++;

    return( mutex_status );

#endif /* CAMP_MULTITHREADED */

  return( 0 );
}


/*
 *  replace pthread_unlock_global_np
 *
 *  20140218  TW  renamed mutex_unlock_global_once (as opposed to mutex_unlock_global_all)
 *                fixed so that counter decrements when called on subsequent calls
 *  20140217  TW  renamed mutex_unlock_global (was thread_unlock_global_np)
 */
int
mutex_unlock_global_once( void )
{
#if CAMP_MULTITHREADED

    int mutex_status = 0;
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	error_using_mutex_with_no_thread_data( "mutex_unlock_global_once", "global" );
	return EINVAL;
    }

/*     if( _camp_debug(CAMP_DEBUG_MUTEX) ) */
/*     { */
/* 	if( pThread_data->mutex_count_global == 0 ) */
/* 	{ */
/* 	    if( pThread_data->type == CAMP_THREAD_TYPE_MAIN ) printf( "main thread has no global lock to give\n" ); */
/* 	    else if( pThread_data->type == CAMP_THREAD_TYPE_SVC ) printf( "svc thread has no global lock to give\n" ); */
/* 	} */
/* 	else */
/* 	{ */
/* 	    if( pThread_data->type == CAMP_THREAD_TYPE_MAIN ) printf( "main thread giving global lock\n" ); */
/* 	    else if( pThread_data->type == CAMP_THREAD_TYPE_SVC ) printf( "svc thread giving global lock\n" ); */
/* 	} */
/*     } */

    /*
     *  should be an error if try to unlock when we don't have it
     */
/*     if( pThread_data->mutex_count_global == 0 )  */
/*     { */
/* 	return( 0 ); */
/*     } */
/*     else */
/*     { */
/* 	int mutex_status = mutex_unlock( &global_mutex ); */
/* 	pThread_data->mutex_count_global--; */

/* 	return mutex_status; */
/*     } */

    if( pThread_data->mutex_count_global == 1 )
    {
	mutex_status = mutex_unlock( &global_mutex ); 
    }
    else if( pThread_data->mutex_count_global <= 0 )
    {
	// TODO: error
    }

    if( mutex_status == 0 ) pThread_data->mutex_count_global--; 

    return mutex_status; 

#endif /* CAMP_MULTITHREADED */

  return( 0 );
}


int
mutex_lock_global_all( int count )
{
#if CAMP_MULTITHREADED

    int i;
    for( i = 0; i < count; i++ ) 
    {
	int mutex_status = mutex_lock_global_once();
	if( mutex_status != 0 ) return mutex_status;
    }

    return( 0 );

#endif /* CAMP_MULTITHREADED */

    return( 0 );
}


/*
 *  returns number of times global locked was unlocked
 */
int
mutex_unlock_global_all( void )
{
#if CAMP_MULTITHREADED

    int i;
    int count;
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	/*
	 *  this is harmless when called from main thread init
	 */
	return 0;
    }

    count = pThread_data->mutex_count_global;

    for( i = 0; i < count; i++ ) 
    {
	int mutex_status = mutex_unlock_global_once();
	if( mutex_status != 0 ) return -1;
    }

    return count;

#endif /* CAMP_MULTITHREADED */

    return( 0 );
}


/* int */
/* mutex_lock_global_once_no_count( void ) */
/* { */
/* #if CAMP_MULTITHREADED */

/*     return( mutex_lock( &global_mutex ) ); */

/* #endif /\* CAMP_MULTITHREADED *\/ */

/*   return( 0 ); */
/* } */


/* int */
/* mutex_unlock_global_once_no_count( void ) */
/* { */
/* #if CAMP_MULTITHREADED */

/*     return mutex_unlock( &global_mutex ); */

/* #endif /\* CAMP_MULTITHREADED *\/ */

/*   return( 0 ); */
/* } */


int
mutex_lock_global_count( int on, int count )
{
#if CAMP_MULTITHREADED

    //  lock/unlock at most once (global_mutex is non-recursive)

    if( on )
    {
	if( count > 0 )
	{
	    return( mutex_lock( &global_mutex ) );
	}
	else
	{
	    return( 0 );
	}
    }
    else
    {
	if( count > 0 )
	{
	    return( mutex_unlock( &global_mutex ) ); 
	}
	else
	{
	    return( 0 );
	}
    }

#endif /* CAMP_MULTITHREADED */

  return( 0 );
}


/*
 *  DA does not understand this, and it does not work on Linux. 
 *   - TW: this locks/unlocks mainInterp_mutex (mutex for main Tcl interpreter)
 *         and gives/takes the global_mutex when locking
 *         (the 'count' implements global_mutex as a recursive mutex)
 *
 *  Parameters:
 *    val = TRUE:  lock mainInterp_mutex
 *          FALSE: unlock mainInterp_mutex
 *
 *  19991219  TW  Changed method from a global flag to a mutex
 *  20140217  TW  rename set_global_mutex_noChange to mutex_lock_mainInterp
 */
int
mutex_lock_mainInterp( int on )
{
#if CAMP_MULTITHREADED
    
    int mutex_status;
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	error_using_mutex_with_no_thread_data( "mutex_lock_mainInterp", "mainInterp" );
	return EINVAL;
    }

    if( on )
    {
	mutex_status = mutex_lock_with_global_release( &mainInterp_mutex );

	if( mutex_status == 0 ) pThread_data->mutex_count_mainInterp++;
    }
    else 
    {
	mutex_status = mutex_unlock( &mainInterp_mutex );

	if( mutex_status == 0 ) pThread_data->mutex_count_mainInterp--;
    }

    return( mutex_status );
#else
    return( 0 );
#endif /* CAMP_MULTITHREADED */
}


/*
 *  get_ins_thread_lock  -  get instrument-specific mutex
 *
 *  There are two locks (mutexes) used by the CAMP server for
 *  multithreading purposes:  global and instrument.  The
 *  global lock is (mainly) used to guard the threads against
 *  non-reentrant code.  The instrument mutex is used to
 *  make sure that only one thread is using an instrument's
 *  Tcl interp or performing I/O to the instrument at one time.
 *
 *  "lock" and "mutex" are used interchangeably.
 *
 *  Strategy:  The global lock will already be taken upon 
 *             entry.  Try getting the instrument lock.  If
 *             free, it's taken right away.  If not free,
 *             must unlock the global lock to allow the thread
 *             that has the instrument lock to continue (as 
 *             well as any others needing the global lock), 
 *             then wait indefinitely for the instrument lock.
 *             Once the instrument lock is available, then
 *             take it and wait again for the global mutex.
 */
int
mutex_lock_ins( CAMP_VAR* pVar, int on )
{
#if CAMP_MULTITHREADED

    int mutex_status;
    pthread_mutex_t* mutex = &(pVar->spec.CAMP_VAR_SPEC_u.pIns->mutex);
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	error_using_mutex_with_no_thread_data( "mutex_lock_ins", "instrument" );
	return EINVAL;
    }

    if( on )
    {
	mutex_status = mutex_lock_with_global_release( mutex );

	// todo: save count in thread_data hash table
    }
    else
    {
	mutex_status = mutex_unlock( mutex );

	// todo: save count in thread_data hash table
    }

    return( mutex_status );
#else
    return( 0 );
#endif /* CAMP_MULTITHREADED */
}


#if CAMP_MULTITHREADED
#ifdef UNUSED
/*
 *  manage thread_data state/served
 */
int
mutex_lock_thread( pthread_mutex_t* mutex )
{
    return mutex_lock_with_global_release( mutex );
}


int
mutex_unlock_thread( pthread_mutex_t* mutex )
{
    return mutex_unlock( mutex );
}
#endif // UNUSED
#endif /* CAMP_MULTITHREADED */


#ifdef RETIRED
int
mutex_lock_sys( int on )
{
#if CAMP_MULTITHREADED && CAMP_MUTEX_SYS

    int mutex_status;
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	error_using_mutex_with_no_thread_data( "mutex_lock_sys", "sys" );
	return EINVAL;
    }

    if( on )
    {
	mutex_status = mutex_lock_with_global_release( &sys_mutex );

	if( mutex_status == 0 ) pThread_data->mutex_count_sys++;
    }
    else
    {
	mutex_status = mutex_unlock( &sys_mutex );

	if( mutex_status == 0 ) pThread_data->mutex_count_sys--;
    }

    return( mutex_status );
#else
    return( 0 );
#endif /* CAMP_MULTITHREADED */
}
#endif // RETIRED


#ifdef RETIRED
int
mutex_lock_varlist( int on )
{
#if CAMP_MULTITHREADED && CAMP_MUTEX_VARLIST

    int mutex_status;
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	error_using_mutex_with_no_thread_data( "mutex_lock_varlist", "varlist" );
	return EINVAL;
    }

    if( on )
    {
	mutex_status = mutex_lock_with_global_release( &varlist_mutex );

	if( mutex_status == 0 ) pThread_data->mutex_count_varlist++;
    }
    else
    {
	mutex_status = mutex_unlock( &varlist_mutex );

	if( mutex_status == 0 ) pThread_data->mutex_count_varlist--;
    }

    return( mutex_status );
#else
    return( 0 );
#endif /* CAMP_MULTITHREADED */
}
#endif // RETIRED


int
mutex_lock_if_t( CAMP_IF_t* pIF_t, int on )
{
#if CAMP_MULTITHREADED
    int mutex_status;
    pthread_mutex_t* mutex = &(pIF_t->mutex);
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( pThread_data == NULL ) 
    {
	error_using_mutex_with_no_thread_data( "mutex_lock_if_t", "interface type" );
	return EINVAL;
    }

    if( on )
    {
	mutex_status = mutex_lock_with_global_release( mutex );

	// todo: save count in thread_data hash table
    }
    else
    {
	mutex_status = mutex_unlock( mutex );

	// todo: save count in thread_data hash table
    }

    return( mutex_status );
#else
    return( 0 );
#endif /* CAMP_MULTITHREADED */
}


bool_t mutex_lock_global_check()
{
    return( get_mutex_lock_count_global() > 0 );
}


#ifdef RETIRED
bool_t mutex_lock_sys_check()
{
    // int mutex_status = pthread_mutex_trylock( &sys_mutex );
    // if( mutex_status == 0 )
    // {
    //     pthread_mutex_unlock( &sys_mutex );
    // }
    // return ( mutex_status == 0 );

    return( get_mutex_lock_count_sys() > 0 );
}
#endif // RETIRED


#ifdef RETIRED
bool_t mutex_lock_varlist_check()
{
    return( get_mutex_lock_count_varlist() > 0 );
}
#endif // RETIRED


bool_t mutex_lock_mainInterp_check()
{
    return( get_mutex_lock_count_mainInterp() > 0 );
}


#ifdef TODO // requires map(pIF_t,count) in thread_data
bool_t mutex_lock_if_t_check( CAMP_IF_t* pIF_t )
{
    return( get_mutex_lock_count_if_t( pIF_t ) > 0 );
}
#endif // TODO


#ifdef TODO // requires map(pIF_t,count) in thread_data
bool_t mutex_lock_ins_check( CAMP_VAR* pVar )
{
    return( get_mutex_lock_count_ins( pVar ) > 0 );
}
#endif // TODO


int get_mutex_lock_count_global()
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( (pThread_data!=NULL) ? pThread_data->mutex_count_global : 0 );
#else
    return 1;
#endif // CAMP_MULTITHREADED
}


#ifdef RETIRED
int get_mutex_lock_count_sys()
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( (pThread_data!=NULL) ? pThread_data->mutex_count_sys : 0 );
#else
    return 1;
#endif // CAMP_MULTITHREADED
}
#endif // RETIRED


#ifdef RETIRED
int get_mutex_lock_count_varlist()
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( (pThread_data!=NULL) ? pThread_data->mutex_count_varlist : 0 );
#else
    return 1;
#endif // CAMP_MULTITHREADED
}
#endif // RETIRED


int get_mutex_lock_count_mainInterp()
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( (pThread_data!=NULL) ? pThread_data->mutex_count_mainInterp : 0 );
#else
    return 1;
#endif // CAMP_MULTITHREADED
}


#ifdef TODO // requires map(pIF_t,count) in thread_data
int get_mutex_lock_count_if_t( CAMP_IF_t* pIF_t )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    //return( (pThread_data!=NULL) ? pThread_data->mutex_count_if_t : 0 );
#else
    return 1;
#endif // CAMP_MULTITHREADED
}
#endif // TODO


#ifdef TODO // requires map(pVar,count) in thread_data
int get_mutex_lock_count_ins( CAMP_VAR* pVar )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    //return( (pThread_data!=NULL) ? pThread_data->mutex_count_ins : 0 );
#else
    return( 1 );
#endif // CAMP_MULTITHREADED
}
#endif // TODO

#if CAMP_MULTITHREADED
const char* getStringThreadType( int val )
{
    switch( val )
    {
    case CAMP_THREAD_TYPE_NONE: return "NONE";
    case CAMP_THREAD_TYPE_MAIN: return "MAIN";
    case CAMP_THREAD_TYPE_SVC: return "SVC";
    case CAMP_THREAD_TYPE_POLL: return "POLL";
    default: return "";
    }
}
#endif // CAMP_MULTITHREADED

#if CAMP_MULTITHREADED
const char* getStringThreadState( int val )
{
    switch( val )
    {
    case CAMP_THREAD_STATE_INIT: return "INIT";
    case CAMP_THREAD_STATE_RUNNING: return "RUNNING";
    case CAMP_THREAD_STATE_FINISHED: return "FINISHED";
    default: return "";
    }
}
#endif // CAMP_MULTITHREADED

/*
 *  Preconditions:
 *    global lock OFF
 */
void set_thread_executing( int flag )
{
#if CAMP_MULTITHREADED && CAMP_MUTEX_SYSTEM_MODIFICATIONS
    THREAD_DATA* pThread_data = get_thread_data_this();

    if( flag == 1 )
    {
	if( mutex_lock_global_check() )
	{
	    // TODO: fatal error: global lock must be off in this scheme
	}

	//  Wait for the sysmod_mutex to be released
	//  The sysmod_mutex is used as a global flag
	//  to block other threads from beginning execution
	//  Threads do not take it while executing (instead,
	//  they set the 'executing' flag in thread_data).
	
	/* _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, waiting to execute" */
	/*     ,get_thread_data_this_index() */
	/*     ,getStringThreadType(pThread_data->type) */
	/*     ,getStringThreadState(pThread_data->state) */
	/*     ,pThread_data->executing */
	/*     ,pThread_data->served */
	/*     ); */

	mutex_lock( &sysmod_mutex ); 
	
	pThread_data->executing = 1;

	mutex_unlock( &sysmod_mutex ); // give it back after setting 'executing'

	/* _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, executing" */
	/*     ,get_thread_data_this_index() */
	/*     ,getStringThreadType(pThread_data->type) */
	/*     ,getStringThreadState(pThread_data->state) */
	/*     ,pThread_data->executing */
	/*     ,pThread_data->served */
	/*     ); */
    }
    else
    {
	pThread_data->executing = 0;

	/* _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, not executing" */
	/*     ,get_thread_data_this_index() */
	/*     ,getStringThreadType(pThread_data->type) */
	/*     ,getStringThreadState(pThread_data->state) */
	/*     ,pThread_data->executing */
	/*     ,pThread_data->served */
	/*     ); */
    }
#endif // CAMP_MULTITHREADED
}


/*
 *  wait until no other thread is 'executing'
 *
 *  Preconditions:
 *    global lock ON
 *
 *  ISSUE: 
 *    deadlock: two threads are executing, both call wait_no_thread_executing 
 *      1. thread1,thread2 set 'executing'
 *      2. thread1 takes global_mutex, thread2 waits for global_mutex
 *      3. thread1 goes into wait_no_thread_executing, takes sysmod_mutex, releases global_mutex
 *      4. thread2 takes global_mutex, thread1 waits for global_mutex
 *      5. thread2 goes into wait_no_thread_executing, waits for sysmod_mutex
 *
 *    SOLUTION:
 *      - when waiting for the sysmod_mutex, set 'executing' and global_mutex off
 */
int wait_no_thread_executing( int flag )
{
#if CAMP_MULTITHREADED && CAMP_MUTEX_SYSTEM_MODIFICATIONS
    if( flag == 1 )
    {
	int thread_data_index;
	THREAD_DATA* pThread_data_this = get_thread_data_this();

	if( pThread_data_this == NULL )
	{
	    error_no_thread_data( "wait_no_thread_executing" );
	    return CAMP_FAILURE;
	}

	if( !mutex_lock_global_check() )
	{
	    // TODO: fatal error: global lock must be on in this scheme
	}

	// sysmod_mutex: block any threads from beginning their execution
	// turn off 'executing' and global_mutex while waiting for sysmod_mutex, to prevent
	// deadlock between two executing threads that require wait_no_thread_executing
	{
	    int globalMutexLockCount;
	    int executingCount;

	    /* _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, giving global_mutex" */
	    /* 		  ,get_thread_data_this_index() */
	    /* 		  ,getStringThreadType(pThread_data_this->type) */
	    /* 		  ,getStringThreadState(pThread_data_this->state) */
	    /* 		  ,pThread_data_this->executing */
	    /* 		  ,pThread_data_this->served */
	    /* 	); */

	    globalMutexLockCount = mutex_unlock_global_all();
	    executingCount = pThread_data_this->executing;
	    pThread_data_this->executing = 0;

	    /* _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, taking sysmod_mutex" */
	    /* 		  ,get_thread_data_this_index() */
	    /* 		  ,getStringThreadType(pThread_data_this->type) */
	    /* 		  ,getStringThreadState(pThread_data_this->state) */
	    /* 		  ,pThread_data_this->executing */
	    /* 		  ,pThread_data_this->served */
	    /* 	); */

	    mutex_lock( &sysmod_mutex ); // used as a global flag
	    pThread_data_this->executing = executingCount;

	    /* _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, taking global_mutex" */
	    /* 		  ,get_thread_data_this_index() */
	    /* 		  ,getStringThreadType(pThread_data_this->type) */
	    /* 		  ,getStringThreadState(pThread_data_this->state) */
	    /* 		  ,pThread_data_this->executing */
	    /* 		  ,pThread_data_this->served */
	    /* 	); */

	    mutex_lock_global_all( globalMutexLockCount );
	}

	for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
	{
	    THREAD_DATA* pThread_data = get_thread_data( thread_data_index );
	    bool_t waiting = pThread_data->executing;

	    if( pThread_data == pThread_data_this ) continue;

	    /* if( waiting ) */
	    /* { _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, waiting for thread:%d type:%s state:%s executing:%d served:%d begin" */
	    /* 		  ,get_thread_data_this_index() */
	    /* 		  ,getStringThreadType(pThread_data_this->type) */
	    /* 		  ,getStringThreadState(pThread_data_this->state) */
	    /* 		  ,pThread_data_this->executing */
	    /* 		  ,pThread_data_this->served */
	    /* 		  ,thread_data_index */
	    /* 		  ,getStringThreadType(pThread_data->type) */
	    /* 		  ,getStringThreadState(pThread_data->state) */
	    /* 		  ,pThread_data->executing */
	    /* 		  ,pThread_data->served */
	    /* 	); } */

	    while( pThread_data->executing )
	    {
		int globalMutexLockCount = mutex_unlock_global_all();
		camp_fsleep( 0.01 );    /* 10 ms */
		mutex_lock_global_all( globalMutexLockCount );
	    }

	    /* if( waiting ) */
	    /* { _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, waiting for thread:%d end" */
	    /* 		  ,get_thread_data_this_index() */
	    /* 		  ,getStringThreadType(pThread_data_this->type) */
	    /* 		  ,getStringThreadState(pThread_data_this->state) */
	    /* 		  ,pThread_data_this->executing */
	    /* 		  ,pThread_data_this->served */
	    /* 		  ,thread_data_index */
	    /* 	); } */
	}
    }
    else
    {
	// this tells the other threads that they can go ahead
	THREAD_DATA* pThread_data_this = get_thread_data_this();

	/* _camp_log( "thread:%d type:%s state:%s executing:%d served:%d, giving sysmod_mutex" */
	/* 	      ,get_thread_data_this_index() */
	/* 	      ,getStringThreadType(pThread_data_this->type) */
	/* 	      ,getStringThreadState(pThread_data_this->state) */
	/* 	      ,pThread_data_this->executing */
	/* 	      ,pThread_data_this->served */
	/*     ); */

	mutex_unlock( &sysmod_mutex ); // used as a global flag
    }
#endif // CAMP_MULTITHREADED

    return( 0 );
}


struct svc_req*
get_current_rqstp( void )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( pThread_data->rqstp );
#else
    return( current_rqstp );
#endif /* CAMP_MULTITHREADED */
}


void
set_current_rqstp( struct svc_req* rqstp )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    pThread_data->rqstp = rqstp;
#else
    current_rqstp = rqstp;
#endif /* CAMP_MULTITHREADED */
}


#ifdef UNUSED
timeval_t*
get_tv_start( void )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( &pThread_data->tv_start );
#else
    return( &tv_start );
#endif /* CAMP_MULTITHREADED */
}
#endif // UNUSED


#ifdef UNUSED
void
set_tv_start( timeval_t* ptv )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    copytimeval( ptv, &pThread_data->tv_start );
#else
    copytimeval( ptv, &tv_start );
#endif /* CAMP_MULTITHREADED */
}
#endif // UNUSED


SVCXPRT*
get_current_transp( void )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( pThread_data->transp );
#else
    return( current_transp );
#endif /* CAMP_MULTITHREADED */
}


void
set_current_transp( SVCXPRT* transp )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    pThread_data->transp = transp;
#else
    current_transp = transp;
#endif /* CAMP_MULTITHREADED */
}


Tcl_Interp*
get_thread_interp( void )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( pThread_data->interp );
#else
    return( camp_tclInterp() );
#endif /* CAMP_MULTITHREADED */
}


void
set_thread_interp( Tcl_Interp* interp )
{
#if CAMP_MULTITHREADED
    THREAD_DATA* pThread_data = get_thread_data_this();
    pThread_data->interp = interp;
#endif /* CAMP_MULTITHREADED */
}


#if CAMP_MULTITHREADED


u_long
get_thread_xdr_flag( void )
{
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( pThread_data->xdr_flag );
}


void
set_thread_xdr_flag( u_long flag )
{
    THREAD_DATA* pThread_data = get_thread_data_this();
    pThread_data->xdr_flag = flag;
}


char*
get_thread_ident( void )
{
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( ( pThread_data!=NULL ) ? pThread_data->ident : "" );
}


void
set_thread_ident( const char* ident )
{
    THREAD_DATA* pThread_data = get_thread_data_this();
    camp_strncpy( pThread_data->ident, LEN_IDENT+1, ident ); 
}


char*
get_thread_msg( void )
{
    THREAD_DATA* pThread_data = get_thread_data_this();
    return( ( pThread_data!=NULL ) ? pThread_data->msg : "" );
}


void
set_thread_msg( const char* msg )
{
    THREAD_DATA* pThread_data = get_thread_data_this();
    camp_strncpy( pThread_data->msg, MAX_LEN_MSG+1, msg ); 
}


#endif /* CAMP_MULTITHREADED */


int
check_shutdown( void )
{
    int shutdown;

/* #if CAMP_MULTITHREADED */
/*     mutex_lock( &shutdown_mutex ); */
/* #endif /\* CAMP_MULTITHREADED *\/ */

    shutdown = shutdown_flag;

/* #if CAMP_MULTITHREADED */
/*     mutex_unlock( &shutdown_mutex ); */
/* #endif /\* CAMP_MULTITHREADED *\/ */

    return( shutdown );
}


void
set_shutdown( void )
{
    _camp_log( "shutdown requested" );

/* #if CAMP_MULTITHREADED */
/*     mutex_lock( &shutdown_mutex ); */
/* #endif /\* CAMP_MULTITHREADED *\/ */

    shutdown_flag = TRUE;

/* #if CAMP_MULTITHREADED */
/*     mutex_unlock( &shutdown_mutex ); */
/* #endif /\* CAMP_MULTITHREADED *\/ */
}


#if CAMP_MULTITHREADED


void 
init_thread_data( THREAD_DATA* pThread_data, int type, int state )
{
    timeval_t tv;

    reset_thread_data( pThread_data, state );
    pThread_data->thread_handle = 0;
    pThread_data->type = type;
    if( pThread_data->msg == NULL ) pThread_data->msg = (char*)camp_malloc( MAX_LEN_MSG+1 );
    pThread_data->msg[0] = '\0'; 
    pThread_data->interp = camp_tclInterp(); // use main Tcl interpreter by default
    gettimeval( &tv );
    copytimeval( &tv, &pThread_data->tv_start ); /* 16-Dec-1999 TW */
}


void
reset_thread_data( THREAD_DATA* pThread_data, int state )
{
    // mutex_lock_thread( &pThread_data->mutex_handle ); //  20140225  TW  thread mutex unnecessary
    // pThread_data->thread_handle = 0; // don't reset, for info
    pThread_data->rqstp = NULL;  // RPC svc_req (deep copy)
    pThread_data->transp = NULL; // RPC SVCXPRT
    bzero( (void*)&pThread_data->argument, sizeof( CampRpcArgument ) ); // RPC call arguments
    pThread_data->pResult = NULL; // RPC result (RES structure)
    pThread_data->xdr_flag = CAMP_XDR_ALL; // xdr option
    pThread_data->ident[0] = '\0';
    if( pThread_data->msg != NULL ) pThread_data->msg[0] = '\0'; // _free( pThread_data->msg ); // no longer reallocated
    _free( pThread_data->pReq ); // poll threads now responsible for pReq storage (not saved in pSys)
    cleartimeval( &pThread_data->tv_start );
    pThread_data->interp = NULL;
    // pThread_data->type = CAMP_THREAD_TYPE_NONE; // don't reset, for info
    pThread_data->served = FALSE;
    pThread_data->executing = 0;
    pThread_data->mutex_count_global = 0;
    // pThread_data->mutex_count_sys = 0;
    // pThread_data->mutex_count_varlist = 0;
    pThread_data->mutex_count_mainInterp = 0;
    // pThread_data->mutex_count_gpib = 0;
    // pThread_data->uthashtable_mutex_count_ins = NULL;

    pThread_data->state = state; // warning: this statement last: main thread can reuse thread_data after this setting
    // mutex_unlock_thread( &pThread_data->mutex_handle );
}


/*
 *  Name:       find_free_thread_data
 *
 *  Purpose:    
 * 
 *    Wait indefinitely until a free thread is found.  The return is the
 *    integer ID of the free thread which is also the index to the array
 *    of private thread data structures.
 *
 *  Called by:  main thread only
 *
 *  Preconditions:
 *    global lock ON
 *
 *  Postconditions:
 *    global lock ON
 *
 *  Revision history:
 *    20140219  TW  change global mutex strategy, move unlocking in here
 *
 */
int
find_free_thread_data( int type, int globalMutexLockCount )
{
    int thread_data_index = -1;
    int found = FALSE;
    int retry = 0;

    if( globalMutexLockCount < 1 )
    {
	// TODO: fatal error: must lock acquisition of thread_data
	//       (except when initializing main thread)
    }

    do 
    {
        /*
	 *  17-Dec-1999  TW  Fix possible deadlock here.
	 *                   Main thread can get into infinite loop
	 *                   inside find_free_thread_data.  And, since the
	 *                   main thread now cleans up after svc threads
	 *                   could be a problem where no served threads
	 *                   are recovered while main thread waits in here.
	 *                   So, just check for handling served threads here.
	 *  21-Dec-1999  TW  Keep this here, but should no longer need it.
	 *                   In srv_loop_thread, RPC calls are no longer
	 *                   dispatched unless there is a free thread
	 *                   (tested with try_find_free_thread_data).
	 */
        if( retry > 0 )
	{
	    if( _camp_debug(CAMP_DEBUG_THREAD|CAMP_DEBUG_POLL) && retry == 1 ) 
	    { 
		_camp_log( "waiting for free thread" ); 
	    }

	    /*
	     *  unlock global briefly to allow other threads to finish
	     *
	     *  use mutex_lock_global_count which doesn't rely on thread_key/thread_data
	     *  (which may not yet be initialized for this thread)
	     */
	    mutex_lock_global_count( 0, globalMutexLockCount ); // int globalMutexLockCount = mutex_unlock_global_all();
	    camp_fsleep( 0.01 );    /* 10 ms */
	    mutex_lock_global_count( 1, globalMutexLockCount ); // mutex_lock_global_all( globalMutexLockCount );

	    if( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET )
	    {
		srv_handle_served_svc_mode1_threads();
	    }
	}

	++retry ;
	thread_data_index = try_find_free_thread_data( type );
	if( thread_data_index > -1 ) found = TRUE;

    } while( !found );

    if( found )
    {
	if( _camp_debug(CAMP_DEBUG_THREAD) ) 
	{ 
	    _camp_log( "found free thread %d", thread_data_index ); 
	}

        //  20140218  TW  unnecessary lock: this is the main thread which has found an unused thread
        // mutex_lock_thread( &pThread_data->mutex_handle ); // unnecessary lock
        // pThread_data->type = type;
        // mutex_unlock_thread( &pThread_data->mutex_handle ); // unnecessary lock
    }

    return( thread_data_index );
}


/*
 *  Name:       try_find_free_thread_data
 *
 *  Purpose:    
 *  
 *    Check if there are any free threads.  Return the integer
 *    corresponding to the first free thread index, or -1 if no
 *    free threads found.
 *
 *    This routine does not wait indefinitely like find_free_thread_data
 *
 *  Called by:  main thread only
 */
int
try_find_free_thread_data( int type )
{
    int thread_data_index;

    /*
     *  find an existing, unused thread_data
     */

    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
	THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	int found = ( pThread_data->state != CAMP_THREAD_STATE_RUNNING );

	if( found ) 
	{
	    if( _camp_debug(CAMP_DEBUG_THREAD) ) 
	    {
		_camp_log( "found existing thread_data: index:%d type:%d->%d", thread_data_index, pThread_data->type, type );
	    }
	    pThread_data->type = type; // we don't allocate by type
	    return( thread_data_index );
	}
    }

    /*
     *  create a new thread_data
     */

    if( thread_data_size < camp_max_threads )
    {
	THREAD_DATA* pThread_data = NULL;

#if defined( THREAD_DATA_IS_LINKED_LIST )

	thread_data_index = thread_data_size++;

	{
	    THREAD_DATA** ppThread_data;
	    int thread_data_index_check;
	    for( ppThread_data = &thread_data, thread_data_index_check = 0; 
		 *ppThread_data != NULL; 
		 ppThread_data = &((*ppThread_data)->pNext), thread_data_index_check++ ) ;

	    if( thread_data_index != thread_data_index_check )
	    {
		char msg[LEN_MSG+1];
		camp_snprintf( msg, sizeof( msg ), "%s: thread_data_index (%d) != thread_data_index_check (%d)", __FUNCTION__, thread_data_index, thread_data_index_check );
		fatal_error_1( "%s", msg );
		return -1;
	    }

	    *ppThread_data = (THREAD_DATA*)camp_zalloc( sizeof( THREAD_DATA ) );

	    pThread_data = *ppThread_data;
	}

#else // !THREAD_DATA_IS_LINKED_LIST
	
	//  note: can't realloc: threads may have pointers to their data 
	//  (realloc would invalidate), so just camp_malloc once.

	// _camp_log( "sizeof(THREAD_DATA)=%d", sizeof(THREAD_DATA) );

	if( thread_data == NULL )
	{
	    thread_data = (THREAD_DATA*)camp_zalloc( camp_max_threads*sizeof(THREAD_DATA) );
	}

	thread_data_index = thread_data_size++; // enable one more THREAD_DATA entry

	pThread_data = get_thread_data( thread_data_index );

#endif // THREAD_DATA_IS_LINKED_LIST

	reset_thread_data( pThread_data, CAMP_THREAD_STATE_INIT ); // unnecessary: init_thread_data is called immediately after acquiring thread_data

	if( _camp_debug(CAMP_DEBUG_THREAD) ) 
	{
	    _camp_log( "creating new thread_data: index:%d type:%d->%d", thread_data_index, pThread_data->type, type );
	}

	pThread_data->type = type; // we don't allocate by type

	return( thread_data_index );
    }
    else
    {
	return( -1 );
    }
}


/*
 *  kill_all_threads
 *
 *  In implementation, doesn't actually kill the threads, but waits until
 *  they have all finished normally.  Could consider a delay in the loop
 *  to relieve the processor a little.

 *  Preconditions:
 *    global lock ON
 *
 *  Postconditions:
 *    global lock ON
 */
void
kill_all_threads( void )
{
    int thread_data_index;

    /*
     *  Wait for threads to complete
     *  (doesn't actually cancel or join. pthread_join wasn't working, I don't know why).
     */
    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
	bool_t running;
	THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	if( pThread_data->type == CAMP_THREAD_TYPE_MAIN ) continue; 

	for( running = TRUE; running; )
	{
	    //  20140225  TW  thread mutex unnecessary
	    // mutex_lock_thread( &pThread_data->mutex_handle );
	    if( pThread_data->state != CAMP_THREAD_STATE_RUNNING ) 
	    {
		running = FALSE;
	    }
	    // mutex_unlock_thread( &pThread_data->mutex_handle );

	    //  20140225  TW  wait and handle svc threads that are served but still running
	    if( running )
	    {
		int globalMutexLockCount = mutex_unlock_global_all();
		camp_fsleep( 0.01 );    /* 10 ms */
		mutex_lock_global_all( globalMutexLockCount );

		if( camp_rpc_thread_mode == CAMP_RPC_THREAD_MODE_THREADED_SVC_GETREQSET )
		{
		    srv_handle_served_svc_mode1_threads();
		}
	    }
	}
    }

    /*
     *  verify
     */
    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
	THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	if( pThread_data->type == CAMP_THREAD_TYPE_MAIN ) continue; 

	// 20140225  TW  thread mutex unnecessary
	// mutex_lock_thread( &pThread_data->mutex_handle );

	if( pThread_data->state == CAMP_THREAD_STATE_RUNNING ) 
	{
	    _camp_log( "error: thread %d is still running", thread_data_index );
	}

	// mutex_unlock_thread( &pThread_data->mutex_handle );
    }
}


/*
 *  Initialize the thread aspects of the main server thread (the first
 *  thread of execution)
 */
int
srv_init_main_thread( void )
{
    int thread_data_index;
    THREAD_DATA* pThread_data;
    int globalMutexLockCount = 0; // unnecessary during init

    thread_data_index = find_free_thread_data( CAMP_THREAD_TYPE_MAIN, globalMutexLockCount ); // should always return thread_data_index==0

    if( set_thread_key_to_thread_data( thread_data_index ) != 0 )
    {
        _camp_log( "error setting main thread key" );
        return( CAMP_FAILURE );
    }

    pThread_data = get_thread_data( thread_data_index );

    //  20140218  TW  unnecessary lock: this is the main thread which has found an unused thread
    // if( mutex_lock_thread( &pThread_data->mutex_handle ) != 0 )
    // {
    //     _camp_log( "error locking main thread mutex" );
    //     return( CAMP_FAILURE );
    // }

    init_thread_data( pThread_data, CAMP_THREAD_TYPE_MAIN, CAMP_THREAD_STATE_RUNNING );

    // if( mutex_unlock_thread( &pThread_data->mutex_handle ) != 0 )
    // {
    //     _camp_log( "error unlocking main thread mutex" );
    //     return( CAMP_FAILURE );
    // }

    /*
     *  The main thread is now effectively just another thread
     *  Set the scheduling policy and priority 
     *  explicitely to the defaults.
     */
    /*
     * ERROR using this - I guess pthread_self() isn't working for the 
     * main thread.  Oh well, I just wanted default behaviour anyway.
     */
    // sched_param sched_param = { 0 };
    // sched_param.sched_priority = (PRI_FG_MAX_NP+PRI_FG_MIN_NP)/2;
    // if( pthread_setschedparam( pthread_self(), SCHED_FG_NP, *sched_param ) != 0 )
    // {
    //     _camp_log( "error setting scheduling" );
    //     return( CAMP_FAILURE );
    // }

    if( _camp_debug(CAMP_DEBUG_THREAD) ) 
    {	
	_camp_log( "thread: index:%d type:%s handle:0x%lx state:%s", 
		      thread_data_index, getStringThreadType(pThread_data->type), pThread_data->thread_handle, 
		      getStringThreadState(pThread_data->state) );
    }

    return( CAMP_SUCCESS );
}


int
srv_start_main2_thread( void )
{
    int thread_data_index;
    pthread_attr_t thread_attr;
    THREAD_DATA* pThread_data;

    thread_data_index = find_free_thread_data( CAMP_THREAD_TYPE_MAIN, get_mutex_lock_count_global() ); 

    pThread_data = get_thread_data( thread_data_index );

    init_thread_data( pThread_data, CAMP_THREAD_TYPE_MAIN, CAMP_THREAD_STATE_RUNNING );

    if( pthread_attr_init( &thread_attr ) != 0 )
    {
        _camp_log( "error creating main2 thread attributes" );
        return( CAMP_FAILURE );
    }

    /*
     *  Explicitely set to inherit scheduling
     *  policy and priority from creating thread
     */
    if( pthread_attr_setinheritsched( &thread_attr, PTHREAD_INHERIT_SCHED ) != 0 )
    {
        _camp_log( "error setting main2 thread attributes" );
        return( CAMP_FAILURE );
    }

    // FYI: pthread_attr_setstacksize( &thread_attr, 1024 * 1024 );

    if( pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED ) != 0 )
    {
        _camp_log( "error setting main2 thread attributes" );
        return( CAMP_FAILURE );
    }

    if( pthread_create( &pThread_data->thread_handle, // thread id
                        &thread_attr, // pthread_attr_default // thread attributes
                        main2_thread, // start_routine
                        integerToPointer( thread_data_index ) // 
	    ) != 0 ) 
    {
        _camp_log( "error creating main2 thread" );
        return( CAMP_FAILURE );
    }

    if( pthread_attr_destroy( &thread_attr ) != 0 )
    {
        _camp_log( "error deleting main2 thread attributes" );
        return( CAMP_FAILURE );
    }

    if( _camp_debug(CAMP_DEBUG_THREAD) ) 
    {	
	_camp_log( "thread: index:%d type:%s handle:0x%lx state:%s", 
		   thread_data_index, getStringThreadType(pThread_data->type), pThread_data->thread_handle, 
		   getStringThreadState(pThread_data->state) );
    }

    return( CAMP_SUCCESS );
}


pthread_addr_t
main2_thread( pthread_addr_t arg )
{
    int thread_data_index;
    //THREAD_DATA* pThread_data;

    thread_data_index = pointerToInteger( arg ); // (int)arg; // 20140221  TW

    if( set_thread_key_to_thread_data( thread_data_index ) != 0 )
    {
    	_camp_log( "error setting main2 thread key" );
        return( (pthread_addr_t)integerToPointer( -1 ) );
    }

    srv_loop_main2_thread();

    return( arg ); // Return an address; this program doesn't check it.
}


/*
 *  A routine that is called only once for all threads
 *
 *  This routine initializes the general multithreading state of the program.
 */
void
srv_init_thread_once( void )
{
    int thread_data_index;
    pthread_mutexattr_t mutexattr_recursive;

#ifdef VMS
    /*
     *  Set reentrancy behaviour properly for VMS
     */
#if (defined(__DECC)||defined(__DECCXX)) && defined(CAMP_MULTITHREADED)
#include <reentrancy.h>
    DECC$SET_REENTRANCY( C$C_MULTITHREAD );
#endif /* __DECC && CAMP_MULTITHREADED */
#endif /* VMS */

    /* 
     *  Initialize mutex attributes for recursive mutexes
     */
    if( pthread_mutexattr_init( &mutexattr_recursive ) != 0 )
    {
        _camp_log( "error creating mutex attribute" );
        exit( CAMP_FAILURE );
    }

    if( pthread_mutexattr_settype( &mutexattr_recursive, PTHREAD_MUTEX_RECURSIVE ) != 0 )
    {
	_camp_log( "error setting mutex attribute as recursive" );
        exit( CAMP_FAILURE );
    }

    // FYI: pthread_mutexattr_setpshared( &mutexattr, PTHREAD_PROCESS_PRIVATE ); // default: intra-process mutex

    /*
     *  Initialize thread data
     *  Thread data must be static so that
     *  find_free_thread_data methodology works
     *  (and also the shutdown methodology).
     */
    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
	THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	// 20140225  TW  thread mutex unnecessary
	// 20140218  TW  recursive
        // if( pthread_mutex_init( &pThread_data->mutex_handle, &mutexattr_recursive ) != 0 )
        // {
	//     char buf[LEN_BUF+1];
	//     camp_snprintf( buf, LEN_BUF+1, "error initializing thread %d mutex", i ); // terminates, size includes terminator
        //     _camp_log( "%s", buf );
        //     exit( CAMP_FAILURE );
        // }

#if !defined( THREAD_DATA_IS_LINKED_LIST )
	bzero( (void*)pThread_data, sizeof( THREAD_DATA ) );
#endif // !THREAD_DATA_IS_LINKED_LIST

	reset_thread_data( pThread_data, CAMP_THREAD_STATE_INIT );
    }

    if( pthread_key_create( &thread_key, thread_key_destructor ) != 0 )
    {
        _camp_log( "error creating thread key" );
        exit( CAMP_FAILURE );
    }

    /*
     *  Initialize global mutex
     */
    if( pthread_mutex_init( &global_mutex, pthread_mutexattr_default ) != 0 ) // not recursive
    {
        _camp_log( "error initializing global_mutex" );
        exit( CAMP_FAILURE );
    }
    //printf("Created global mutex at %p\n", global_mutex);

    /*
     *  Shutdown mutex & flag
     */
    // if( pthread_mutex_init( &shutdown_mutex, pthread_mutexattr_default ) != 0 ) // not recursive
    // {
    //     _camp_log( "error initializing shutdown_mutex" );
    //     exit( CAMP_FAILURE );
    // }
    shutdown_flag = FALSE;

    /*
     *  mainInterp_mutex for exclusive use of the main Tcl interpreter.
     *  As with all interp mutexes it must be recursive.
     */
    if( pthread_mutex_init( &mainInterp_mutex, &mutexattr_recursive ) != 0 ) // recursive
    {
        _camp_log( "error initializing mainInterp_mutex" );
        exit( CAMP_FAILURE );
    }
    //printf("Created mainInterp mutex at %p\n",mainInterp_mutex );

    // if( pthread_mutex_init( &sys_mutex, &mutexattr_recursive ) != 0 ) // recursive
    // {
    //     _camp_log( "error initializing sys_mutex" );
    //     exit( CAMP_FAILURE );
    // }

    // if( pthread_mutex_init( &varlist_mutex, &mutexattr_recursive ) != 0 ) // recursive
    // {
    //     _camp_log( "error initializing varlist_mutex" );
    //     exit( CAMP_FAILURE );
    // }

    if( pthread_mutex_init( &sysmod_mutex, &mutexattr_recursive ) != 0 ) // recursive
    {
        _camp_log( "error initializing sysmod_mutex" );
        exit( CAMP_FAILURE );
    }
    //printf("Created sysmod mutex at %p\n",sysmod_mutex );

    if( pthread_mutexattr_destroy( &mutexattr_recursive ) != 0 ) // recursive
    {
	_camp_log( "error deleting mutex attribute" );
	exit( CAMP_FAILURE );
    }

    /*
     *  In VxWorks, set scheduling policy once 
     *  for all tasks.
     *  I'm just guessing on a value here.
     */
#ifdef VXWORKS
    kernelTimeSlice( sysClkRateGet()*0.01 );  /* 0.01 seconds */
#endif /* VXWORKS */
}


/*
 *  srv_end_thread  -  clean up all thread related storage (acquired by srv_init_thread_once)
 *  
 *  Called from main thread before program exits.  
 */
void
srv_end_thread( void )
{
    int thread_data_index;

    if( pthread_mutex_destroy( &sysmod_mutex ) != 0 )
    {
        _camp_log( "failed deleting sysmod_mutex" );
    }

    // if( pthread_mutex_destroy( &varlist_mutex ) != 0 )
    // {
    //     _camp_log( "failed deleting varlist_mutex" );
    // }

    // if( pthread_mutex_destroy( &sys_mutex ) != 0 )
    // {
    //     _camp_log( "failed deleting sys_mutex" );
    // }

    if( pthread_mutex_destroy( &mainInterp_mutex ) != 0 )
    {
        _camp_log( "failed deleting mainInterp_mutex" );
    }

    // if( pthread_mutex_destroy( &shutdown_mutex ) != 0 )
    // {
    //     _camp_log( "failed deleting shutdown_mutex" );
    // }

    if( pthread_mutex_destroy( &global_mutex ) != 0 )
    {
        _camp_log( "failed deleting global_mutex" );
    }

    if( pthread_key_delete( thread_key ) != 0 )
    {
        _camp_log( "failed deleting thread_key" );
    }

    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
	THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	// 20140225  TW  thread mutex unnecessary
        // if( pthread_mutex_destroy( &pThread_data->mutex_handle ) != 0 )
        // {
	//     char buf[LEN_BUF+1];
	//     camp_snprintf( buf, LEN_BUF+1, "failed deleting thread %d mutex", i ); // terminates, size includes terminator
        //     _camp_log( "%s", buf );
	// }

        _free( pThread_data->msg );
    }
}

#endif // CAMP_MULTITHREADED


/*
 *  Check to see if an RPC transport is associated with any active
 *  SVC thread. Returns the thread number if there is one associated, or 
 *  -1 if there is no associated SVC thread. 
 */
int
check_rpc_transport_in_use( SVCXPRT* transp )
{
#if CAMP_MULTITHREADED

    int thread_data_index;
    bool_t transport_in_use = FALSE;

    for( thread_data_index = 0; thread_data_index < get_thread_data_size(); thread_data_index++ )
    {
	THREAD_DATA* pThread_data = get_thread_data( thread_data_index );

	// 20140225  TW  thread mutex unnecessary
	// mutex_lock_thread( &(pThread_data->mutex_handle) );
	if( ( pThread_data->type == CAMP_THREAD_TYPE_SVC ) &&
	    ( pThread_data->state == CAMP_THREAD_STATE_RUNNING ) &&
	    ( pThread_data->transp == transp ) )
	{
	    transport_in_use = TRUE;
	}
	// mutex_unlock_thread( &(pThread_data->mutex_handle) );
	if( transport_in_use ) break;
    }

    return ( transport_in_use ) ? thread_data_index : -1;

#else // !CAMP_MULTITHREADED

    return -1;

#endif // CAMP_MULTITHREADED
}
