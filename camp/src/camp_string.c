/*
 *  Name:       camp_string.c
 *
 *  Purpose:    
 *
 *  Called by:  
 *
 *  Revision history:
 *    20140315  TW  creation
 */

//#include <stdlib.h>
//#define _GNU_SOURCE // causes problems on certain linuxes
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h> // strnlen
#include <ctype.h>
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h"
#endif /* RPC_SERVER */
/* #ifdef VXWORKS */
/* #include "snprintf.h" */
/* #endif // VXWORKS */

/* /\* */
/*  *  strncpy that always terminates */
/*  *\/ */
/* char*  */
/* my_strncpy( char* dest, const char* src, size_t n ) */
/* {  */
/*     // at most n characters written from src */
/*     // does not terminate if src is longer than n */
/*     char* d = strncpy( dest, src, n ); */
    
/*     if( n > 0 ) dest[n-1] = '\0'; */

/*     return d; */
/* } */

size_t
camp_strnlen( const char* str, size_t str_size )
{
#ifdef linux

    return strnlen( str, str_size );

#else // linux

    // todo: smarter version (e.g., compare 4 bytes at a time, as gnu implementation does)

    const char* pstr;
    size_t count;

    for( pstr = str, count = 0; ( count < str_size ) && ( *pstr != '\0' ); ++pstr ) { ++count; }

    return count;

#endif // linux
}

/*
 *  camp_strdup
 */
void
camp_strdup( char** pdest, const char* src )
{
    _free( *pdest );
    *pdest = strdup( src );

    // return strlen( src );
}

/*
 *  camp_strncpy
 *
 *  note: replace strncpy with a termination scheme and return value
 *        consistent with snprintf
 *
 *  returns: size of dest (w/o terminator) if dest had had enough room for src and terminator
 */
int
camp_strncpy( char* dest, size_t dest_size, const char* src )
{
    if( dest_size > 0 ) dest[0] = '\0';

    return camp_strncat_num( dest, dest_size, src, strlen( src ) ); 
}

/*
 *  copy at most src_bytes_max from src (src does not need to be terminated, as long as src_bytes_max is <= sizeof(src))
 */
int
camp_strncpy_max( char* dest, size_t dest_size, const char* src, size_t src_bytes_max )
{
    if( dest_size > 0 ) dest[0] = '\0';

    return camp_strncat_num( dest, dest_size, src, camp_strnlen( src, src_bytes_max ) ); 
}

/*
 *  copy exactly src_bytes_to_copy from src (src does not need to be terminated)
 */
int
camp_strncpy_num( char* dest, size_t dest_size, const char* src, size_t src_bytes_to_copy )
{
    if( dest_size > 0 ) dest[0] = '\0';

    return camp_strncat_num( dest, dest_size, src, src_bytes_to_copy );
}

#ifndef breakeven_point
#  define breakeven_point   6	/* some reasonable one-size-fits-all value */
#endif

#define fast_memcpy(d,s,n)						\
    {									\
        register size_t nn = (size_t)( n );				\
        if( nn >= breakeven_point ) memcpy( (d), (s), nn );		\
	else if( nn > 0 )						\
	{   /* proc call overhead is worth only for large strings*/	\
	    register char *dd; register const char *ss;			\
	    for( ss=(s), dd=(d); nn>0; nn-- ) *dd++ = *ss++;		\
	}								\
    }

/*
 *  camp_strncat
 *
 *  note: replace strncat with a termination scheme and return value
 *        consistent with snprintf
 *
 *  returns: size of dest (w/o terminator) if dest had had enough room for src and terminator
 */
int
camp_strncat( char* dest, size_t dest_size, const char* src )
{ 
    return camp_strncat_num( dest, dest_size, src, strlen( src ) ); 
}

/*
 *  copy at most src_bytes_max from src (src does not need to be terminated, as long as src_bytes_max is <= sizeof(src))
 */
int
camp_strncat_max( char* dest, size_t dest_size, const char* src, size_t src_bytes_max )
{
    return camp_strncat_num( dest, dest_size, src, camp_strnlen( src, src_bytes_max ) ); 
}

/*
 *  copy exactly src_bytes_to_copy from src (src does not need to be terminated)
 */
int
camp_strncat_num( char* dest, size_t dest_size, const char* src, size_t src_bytes_to_copy )
{ 
    int dest_len_max = (dest_size > 0) ? dest_size-1 : -1;
    int dest_len = strlen( dest );    
    int dest_len_appended_ideal = dest_len + src_bytes_to_copy;
    int dest_len_remaining = dest_len_max - dest_len;
    int num_to_copy = _min( src_bytes_to_copy, dest_len_remaining );

    if( dest_size == 0 )
    {
	return dest_len_appended_ideal;
    }

    if( dest_len_max < dest_len ) // shouldn't happen
    {
	if( dest_len_max >= 0 ) dest[dest_len_max] = '\0';
	return dest_len_appended_ideal;
    }

    fast_memcpy( &dest[dest_len], src, num_to_copy );
    
    dest_len += num_to_copy;

    dest[dest_len] = '\0';

    return dest_len_appended_ideal; // return ideal size to allow caller to check if there was truncation
}

/*
 *  snprintf that always terminates
 */
int
camp_snprintf( char *dest, size_t dest_size, const char *format, ... ) 
{
    va_list args;
    int num_copied;
    
    va_start( args, format );
    num_copied = camp_vsnprintf( dest, dest_size, format, args ); 
    va_end( args );

    return num_copied;
}

/*
 *  vsnprintf that always terminates
 *
 *  return: number that would be printed (without dest_size limit), excluding terminator
 */
int 
camp_vsnprintf( char *dest, size_t dest_size, const char *format, va_list args ) 
{
    int print_len; // number (that would be) printed, excluding terminator
    size_t copy_size; // size of string returned in dest, including terminator

/* #define CAMP_VSNPRINTF_TEST */
/* #ifdef CAMP_VSNPRINTF_TEST */

/*     // 20140418  TW  this requires 8k of stack, consider getting from heap, at least for */
/*     //               size-limited vxworks */

/* #ifdef VXWORKS */

/*     char* buf; size_t buf_size = LEN_IO; */

/*     buf = (char*)camp_malloc( buf_size ); */
/*     print_len = vsprintf( buf, format, args );  */

/* #else // !VXWORKS */

/*     char buf[LEN_IO]; size_t buf_size = LEN_IO; */

/*     print_len = vsnprintf( buf, buf_size, format, args );  */

/* #endif // VXWORKS */

/*     dest_size = _min( dest_size, buf_size ); */
/*     copy_size = _min( dest_size, print_len+1 ); */

/*     if( copy_size > 0 ) strncpy( dest, buf, copy_size-1 ); // fast_memcpy( dest, buf, copy_size-1 ); */

/* #ifdef VXWORKS */
/*     _free( buf ); */
/* #endif // VXWORKS */

/* #else // !CAMP_VSNPRINTF_TEST */

#ifdef VXWORKS

    /*
     *  since we don't have a working vsnprintf on vxworks,
     *  vsprintf to a large 8k buffer, then safely strncpy to 
     *  the dest string
     */

    char* buf; size_t buf_size = _max( dest_size, LEN_IO );

    buf = (char*)camp_malloc( buf_size ); // on heap for stack-limited VxWorks

    print_len = vsprintf( buf, format, args ); // live dangerously on VxWorks

    dest_size = _min( dest_size, buf_size );
    copy_size = _min( dest_size, print_len+1 );
    
    if( copy_size > 0 ) strncpy( dest, buf, copy_size-1 ); // fast_memcpy( dest, buf, copy_size-1 );
    
    free( buf );

#else // !VXWORKS

    print_len = vsnprintf( dest, dest_size, format, args ); 

    copy_size = _min( dest_size, print_len+1 );

#endif // VXWORKS

/* #endif // CAMP_VSNPRINTF_TEST */

    if( copy_size > 0 ) dest[copy_size-1] = '\0'; // terminate: 

    if( print_len+1 > dest_size )
    {
	if( _camp_debug(CAMP_DEBUG_ANY) )
	{
	    _camp_log( "output truncated while printing '%s' (%d > %d)", 
		       format, print_len+1, (int)dest_size );
	    printf( "%s: output truncated while printing '%s' (%d > %d)", 
	  	          __FUNCTION__, format, print_len+1, (int)dest_size );
	}
    }

    return print_len;
}

/*  camp_stoprint_expand
 *  like stoprint_expand from libc_tw, that accounts for buffer size. Use two
 *  passes, forward and backward, to allow in-place substitution.
 */
int
camp_stoprint_expand( const char* str_in, char* str_out, size_t str_out_size )
{
    const char* p_in;     
    char* p_out;
    int str_out_count = 0; // _ideal_ number of characters copied to str_out
    int item_size;
    char c;

    if( str_out_size <= 0 ) 
    {
        goto done;
    }

    // First pass to measure the output length
    for( p_in = str_in; *p_in != '\0'; ++p_in ) 
    {
	if( !isascii( *p_in ) || !isprint( *p_in ) )
	{
            item_size = 4;
	}
	else if( *p_in == '\\' )
	{
            item_size = 2;
	}
	else
	{
	    item_size = 1;
	}

        if( str_out_count + item_size < str_out_size )
        {
            str_out_count += item_size;
        }
        else  // not enough room in output
        {
            break;
        }
    }
    // At the end of this loop, p_in points to the terminating NULL, or the the input
    // character that would overflow the output. str_out_count does not count the
    // null or the overflowing character(s).

    // Second pass to do copying, in reverse order, starting with the null terminator.

    p_out = str_out + str_out_count;
    *p_out = '\0';
    --p_in;

    for( ; p_in >= str_in ; --p_in ) 
    {
	if( !isascii( *p_in ) || !isprint( *p_in ) )
	{
          // Unfortunately, sprintf always null-terminates, so fix that (ugly)
	    item_size = 4;
            c = *p_out;
            p_out -= item_size;
	    sprintf( p_out, "\\%03o", (unsigned char)*p_in );
            p_out[item_size] = c;
	}
	else if( *p_in == '\\' )
	{
          // double backslashes (without sprintf)
	    item_size = 2;
	    p_out -= item_size;
            p_out[0] = '\\';
            p_out[1] = '\\';
	}
	else
	{
	    item_size = 1;
            p_out -= item_size;
            *p_out = *p_in;
	}
    }

 done:
    return str_out_count;
}

/*  camp_stoprint_strdup
 *  like camp_stoprint_expand above, but it allocates the output string based
 *  on the measured length (returns pointer to the string, or NULL for error).
 *  The calling program should free the output string when finished.
 */

char *
camp_stoprint_strdup( const char* str_in )
{
    const char* p_in;     
    char* str_out = { 0 };
    char* p_out;
    int str_out_count = 0; // _ideal_ number of characters copied to str_out
    int item_size;

    // First pass to measure the output length
    for( p_in = str_in; *p_in != '\0'; ++p_in ) 
    {
	if( !isascii( *p_in ) || !isprint( *p_in ) )
	{
            item_size = 4;
	}
	else if( *p_in == '\\' )
	{
            item_size = 2;
	}
	else
	{
	    item_size = 1;
	}
        str_out_count += item_size;
    }
    // At the end of this loop, p_in points to the terminating NULL, but str_out_count 
    // does not count the null.

    str_out = (char*) camp_malloc( str_out_count + 1 );

    if ( str_out == NULL )
    {
        goto done;
    }

    // Second pass to do copying.

    for( p_in = str_in, p_out = str_out; *p_in != '\0'; ++p_in ) 
    {
	if( !isascii( *p_in ) || !isprint( *p_in ) )
	{
	    sprintf( p_out, "\\%03o", (unsigned char)*p_in );
            p_out += 4;
	}
	else if( *p_in == '\\' )
	{
            sprintf( p_out, "\\\\" ); 
            p_out += 2;
	}
	else
	{
            *p_out = *p_in;
            p_out += 1;
	}
    }
    *p_out = '\0';

 done:
    return str_out;
}

/*
 *  strchrnul not available on all platforms
 */
static const char* camp_strchrnul( const char* s, int c )
{
    const char* p = strchr( s, c );

    return ( p != NULL ) ? p : s+strlen( s );
}

int
camp_stringSplit( const char* str, char delim, char** tokens, size_t tokens_dim, size_t token_size )
{
    const char* p_str;

    int num_tokens_found = 0;

    if( str == NULL ) return 0;

    for( p_str = str;
         *p_str != '\0';
	)
    {
	const char* p_token_start = p_str;
    	const char* p_token_end = camp_strchrnul( p_token_start, delim ); // never NULL

	size_t token_len = p_token_end - p_token_start;

	if( num_tokens_found < tokens_dim )
	{
	    camp_strncpy_num( tokens[num_tokens_found], token_size, p_token_start, token_len );
	}

	num_tokens_found++;

	p_str += token_len; // move p_str past token

        if( *p_str != '\0' )
	{
	    p_str++; // move p_str past deliminator
	}
    }

    return num_tokens_found;
}

int
camp_stringJoin( char* str, size_t str_size, char delim, char** tokens, size_t tokens_dim )
{
    int itoken;
    int str_len_ideal = 0;
    char delim_str[1] = { delim };

    if( str_size > 0 ) str[0] = '\0';

    for( itoken = 0; itoken < tokens_dim; itoken++ )
    {
	str_len_ideal += camp_strncat( str, str_size, tokens[itoken] );

	if( itoken+1 < tokens_dim )
	{
	    str_len_ideal += camp_strncat_num( str, str_size, delim_str, 1 );
	}
    }

    return str_len_ideal; // number of bytes that would be in str, if str_size was large enough
}

/*
 *  Find and return (in str_out) the token_index'th substring of str_in 
 *  delimited by the character (int) delim
 *
 *  This routine is used by the camp_getIf* routines in camp_if_utils.c.
 */
int
camp_stringGetToken( const char* str_in, int delim, int token_index, char* str_out, size_t str_out_size )
{
    int i;
    char* p_str_out;
    const char* p_str_in;
    int str_out_len_ideal = 0;

    if( str_in == NULL ) return 0;

    /*
     *  find the token within str_in
     */
    for( p_str_in = str_in, i = 0;
         (*p_str_in != '\0') && (i < token_index);
         i++ )
    {
    	p_str_in = camp_strchrnul( p_str_in, delim );

        if( *p_str_in != '\0') // move past delim
	{
	    p_str_in++;
	}
    }

    /*
     *  copy the token from str_in to str_out
     */
    for( p_str_out = str_out;
         (*p_str_in != delim) && (*p_str_in != '\0');
	  ) 
    {
	if( str_out_len_ideal+1 < str_out_size )
	{
	    *p_str_out = *p_str_in;
	    p_str_out++;
	}

	p_str_in++;
	str_out_len_ideal++;
    }

    if( str_out_size > 0 ) *p_str_out = '\0';

    return str_out_len_ideal;
}
