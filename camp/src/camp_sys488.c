/************************************************************************
 *
 * Enhanced Engineering Software Package for GPIB-1014 Interface
 * System Support Functions for VxWorks
 * Version 1.0
 * Copyright (c) 1990,1991 National Instruments Corporation
 * All rights reserved.
 *
 * NOTE: This file is included and compiled within esp488.c.
 *       It is NOT compiled separately.
 *
 ************************************************************************/

#include "vxWorks.h"
/*
#include "68k/iv.h"
*/
#include "iv.h"
#include "wdLib.h"
#include "semLib.h"
#include "sysLib.h"
#include "intLib.h"
#include "taskLib.h"

int mvip300TimeoutSecondsToFlag( double seconds );
double mvip300TimeoutFlagToSeconds( int flag );

static unsigned long mvip300_timer_ticks_begin = 0; // for info
static unsigned long mvip300_timer_ticks_end = 0;
static double mvip300_timer_delay = 0.0;

IBLCL void startTimer(v)			/* Starts the timeout task  */
int v;					/* v = index into timeTable */
{
	DBGin(STARTTIMER);
	noTimo = INITTIMO;
	if (v > 0) {
		DBGprint(DBG_DATA, ("timo=%d  ", timeTable[v]));
		noTimo = timeTable[v];
		pgmstat |= PS_TIMINST;
		// 20140421  TW
		{
		    int ticks_per_second;
		    double timeout_seconds;
		    unsigned long timeout_ticks;

		    ticks_per_second = sysClkRateGet();
		    timeout_seconds = ( mvip300_timer_delay > 0.0 ) ? mvip300_timer_delay : mvip300TimeoutFlagToSeconds( v );
		    timeout_ticks = _max( (int)( timeout_seconds * (double)ticks_per_second + 0.5 ), 1 );

		    mvip300_timer_ticks_begin = tickGet();
		    mvip300_timer_ticks_end = mvip300_timer_ticks_begin + timeout_ticks;
		}
	}
	DBGout();
}


IBLCL void removeTimer()			/* Removes the timeout task */
{
	DBGin(REMOVETIMER);
	if (pgmstat & PS_TIMINST) {
		if (noTimo > 0)
			noTimo = INITTIMO;
		pgmstat &= ~PS_TIMINST;
		// 20140421  TW
		{
		    // mvip300_timer_delay = 0.0; // require application to call mvip300SetTimerDelay each time
		}
	}
	DBGout();
}


/*
 * VxWorks initialization functions
 */
IBLCL void osInit()
{
	DBGin(OSINIT);
	pgmstat |= PS_SYSRDY;
	DBGout();
}


IBLCL void osReset()
{
	DBGin(OSRESET);
/*
 *	Disable GPIB interrupt request line...
 *
 *	*LCSR_INT_MASK = *LCSR_INT_MASK & ~((char) (1 << ibirq));
 */
	sysIntDisable(ibirq);	/* performs function of above line */
	pgmstat &= ~PS_SYSRDY;
	DBGout();
}


/*
 *  20140421  TW  
 */
void mvip300SetTimerDelay( double delay )
{
    mvip300_timer_delay = delay;
}


/*
 *  20140421  TW  
 */
int isTimerExpired()
{
    /*
     *  greater-than or greater-than-or-equal-to ?
     *    greater-than: because of system clock resolution, we need to
     *    wait two ticks to ensure we've waited at least one
     */

    int expired = ( tickGet() > mvip300_timer_ticks_end ) ? 1 : 0;

    if( expired ) noTimo = 0; // flag checked by esp488 functions

    return( expired );
}


/*
 *  20140421  TW  
 */
int mvip300TimeoutSecondsToFlag( double seconds )
{
    int flag;

    if     ( seconds > 3e+2 ) flag = 17;
    else if( seconds > 1e+2 ) flag = 16;
    else if( seconds > 3e+1 ) flag = 15;
    else if( seconds > 1e+1 ) flag = 14;
    else if( seconds > 3e+0 ) flag = 13;
    else if( seconds > 1e+0 ) flag = 12;
    else if( seconds > 3e-1 ) flag = 11;
    else if( seconds > 1e-1 ) flag = 10;
    else if( seconds > 3e-2 ) flag =  9;
    else if( seconds > 1e-2 ) flag =  8;
    else if( seconds > 3e-3 ) flag =  7;
    else if( seconds > 1e-3 ) flag =  6;
    else if( seconds > 3e-4 ) flag =  5;
    else if( seconds > 1e-4 ) flag =  4;
    else if( seconds > 3e-5 ) flag =  3;
    else if( seconds > 1e-5 ) flag =  2;
    else flag =  1;

    return flag;
}


/*
 *  20140421  TW  
 */
double mvip300TimeoutFlagToSeconds( int flag )
{
    switch( flag )
    {
    case  1: return( 1e-5 );
    case  2: return( 3e-5 );
    case  3: return( 1e-4 );
    case  4: return( 3e-4 );
    case  5: return( 1e-3 );
    case  6: return( 3e-3 );
    case  7: return( 1e-2 );
    case  8: return( 3e-2 );
    case  9: return( 1e-1 );
    case 10: return( 3e-1 );
    case 11: return( 1e+0 );
    case 12: return( 3e+0 );
    case 13: return( 1e+1 );
    case 14: return( 3e+1 );
    case 15: return( 1e+2 );
    case 16: return( 3e+2 );
    default: return( 1e+3 );
    }
}
