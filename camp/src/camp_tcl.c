/*
 *  $Id: camp_tcl.c,v 1.30 2018/06/13 00:27:54 asnd Exp $
 *
 *  $Revision: 1.30 $
 *
 *  Purpose:    Create and manage the CAMP Tcl interpreters
 *              Routines to interpret all CAMP Tcl commands
 *
 *  Called by:
 * 
 *  Inputs:
 *
 *  Preconditions:
 *
 *  Outputs:
 *
 *  Postconditions:
 *
 *  Notes:
 *              Tcl and VxWorks:
 *              The port of Tcl 7.3 that I'm using allows only one interpreter
 *              per task.  A single taskVar is used for the Tcl context (called
 *              Tcl_ctxt).  But, this seems to be only true when using the
 *              tclTCP extensions.  So, I removed all references to Tcl_ctxt
 *              in tclBasic.c and removed tclTCP.c and simpleEvent.c from the
 *              Makefile.
 *
 *  Revision history:
 *
 *  v1.0   ??-???-1994  TW  Initial version
 *  v1.0a  01-Nov-1994  TW  Added insIfReadVerify, insIfWriteVerify and 
 *                            proc-specific usage error messages
 *         06-Oct-1995  TW  No more rs232 retries
 *  V1.1   28-Apr-1998  DBM Added drivers for TIP850 and Tech80 Motor
 *                          Controller Industry Packs. Added some documentation
 *         20-Dec-1999  TW  Added gpibClear
 *  V1.1a  29-Sep-2000  DJA Fix bug where "insSet -if_mod" changed access delay 
 *                          to 0.0s
 *         13-Dec-2000  TW  Added gpibTimeout
 *         14-Dec-2000  TW/DJA  Added sysGetInsNames
 *         18-Dec-2000  TW  Make invalid tokens in varSet an error.  This was
 *                          too relaxed before.  Now properly returns error
 *                          on instrument loading, etc.
 *         22-Dec-2000  DM  Indpack interface type
 *         23-Dec-2000  TW  Prototypes for indpak; revise dirs of adcdac and motor
 *         10-Feb-2001  DJA Added insIfDump and insIfUndump
 *         19-Dec-2001  DJA Added sysGetLoggedVars, sysGetAlertVars, sysGetVarsOnPath
 *         20140217     TW  rename thread_un/lock_global_np to mutex_un/lock_global
 *         20140219     TW  mutex_un/lock_global -> mutex_un/lock_global_all, change scheme
 *         20140220     TW  replace sprintf with snprintf
 *         20140224     TW  improved token handling in TOK_OPT_IF_MOD
 *         20140408     TW  Added insGetIfTimeout
 *         20140423     TW  Error message scheme: on return from tcl functions, accumulated
 *                          message is entirely in Tcl result, nothing in camp msg.  
 *                          Traceback messages/prefixes restricted to debug
 *                          (rely on lower level to supply error message)
 *         20140424     TW  Added sysSetDebug
 *         20140502     TW  Added sysGetDebug
 *         20140505     TW  usage messages for insSet -if and -if_mod, and varSet
 *
 *  $Log: camp_tcl.c,v $
 *  Revision 1.30  2018/06/13 00:27:54  asnd
 *  Expand, fix, and refactor error messages.
 *
 *  Revision 1.29  2017/06/21 03:30:52  asnd
 *  Keep track if ever had instruments; more detailed error messages; fix broken "-alert on"
 *
 *  Revision 1.28  2015/04/22 04:43:17  asnd
 *  Eliminate some warnings.
 *
 *  Revision 1.27  2015/04/21 04:19:19  asnd
 *  Out-of-order application of alert settings.
 *
 *  Revision 1.26  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.23  2013/04/16 08:28:10  asnd
 *  Add commands to retrieve calculated stats. Make var titles changeable.
 *
 *  Revision 1.22  2009/04/08 01:58:54  asnd
 *  Change motor API to pass values instead of variable names
 *
 *  Revision 1.21  2008/03/27 01:29:40  asnd
 *  Remove old unused omegaLib.
 *
 *  Revision 1.20  2007/09/15 01:48:40  asnd
 *  Make sysGetLoggedVars match logging methods
 *
 *  Revision 1.19  2007/02/16 05:45:49  asnd
 *  Make previous change use floats for int variables (sums are big)
 *
 *  Revision 1.18  2007/02/16 05:12:28  asnd
 *  Avoid bad round-off when retrieving statistics
 *
 *  Revision 1.17  2006/05/06 06:18:36  asnd
 *  Allow values to be specified before selections on CAMP_SELECT (apply value setting last)
 *
 *  Revision 1.16  2006/05/02 07:46:33  asnd
 *  Enable alarms on selection variables
 *
 *  Revision 1.15  2006/04/28 07:49:13  asnd
 *  Watch for invalid selection-variable indices
 *
 *  Revision 1.14  2006/04/27 04:10:03  asnd
 *  Create a tcpip socket interface type.
 *  Generate more and better error messages.
 *
 *  Revision 1.13  2005/11/16 23:23:59  asnd
 *  Commit updates for new motor boards
 *
 *  Revision 1.12  2004/01/28 03:18:28  asnd
 *  Add VME interface type.
 *
 *  Revision 1.11  2003/11/10 22:41:25  asnd
 *  Get interface type "none" working
 *
 *  Revision 1.10  2003/07/25 10:16:06  asnd
 *  Bug fixes on the initial minimal omega-controller low-level driver
 *
 *  Revision 1.9  2003/07/23 00:01:17  midas
 *  Added Omega instrument to camp_tcl.c
 *  Added Omega iSeries Ethernet Heater Controller driver files for CAMP
 *  Modified Makefile to build into vxWorks version of CAMP
 *
 *  Revision 1.8  2001/12/19 18:07:35  asnd
 *  Added sysGetLoggedVars, sysGetAlertVars, sysGetVarsOnPath
 *
 *  Revision 1.7  2001/02/10 04:16:15  asnd
 *  Add insIfDump and insIfUndump for binary-clean data dump from instrument to
 *  a file and from a file to the instrument.
 *
 *  Revision 1.6  2000/12/23 00:44:27  ted
 *  Prototypes for indpak
 *
 *  Revision 1.5  2000/12/23 00:30:46  ted
 *  No absolute directories for adcdac and motor
 *
 *  Revision 1.4  2000/12/22 23:36:20  David.Morris
 *  Added INDPAK support:
 *  In campTcl_CreateInterp added Tcl_CreateCommand def'ns.
 *  Added camp_tcl_varGetIfIndpak routine to process commands
 *  Added case for selecting the right processing command
 *  Added case for processing INDPAK parameters
 *  Added case for modifying channel parameter on the fly
 *
 *  Revision 1.3  2000/12/22 22:43:45  David.Morris
 *  Added function to make Tcl aware of the INDPAK interface in camp_tcl_sys
 *
 *
 */

#define __USE_UNIX98 // for pthread_mutexattr_settype

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
//#include "tclInt.h" // for Tcl_ScanCmd // include causes unresolved conflict
#ifdef linux
#include <unistd.h> // gethostname
#endif /* linux */
#include "camp_srv.h"
extern int Tcl_ScanCmd( ClientData clientData, Tcl_Interp *interp, int argc, char **argv );

#ifdef VXWORKS
#include "tip850Lib.h"
#include "te28Lib.h"
#endif // VXWORKS

/* This next may apply to more than VXWORKS */
#include "camp_vme_mem.h"
#if CAMP_HAVE_CAMAC
#include "camacLib.h" // for CAMAC_CB (not vxworks-specific)
#endif // CAMP_HAVE_CAMAC

/*
 *  Main Tcl interpreter
 *  For multithreaded, this is used for sysUpdate, sysLoad, insAdd
 *  and RPC-sent Tcl commands and alarm action procs
 */
Tcl_Interp* tclInterp = NULL;

// #define usage( str )  Tcl_SetResult( interp, str, TCL_VOLATILE );
#define usage( str )  camp_setMsg( "usage: %s", str );

// #define if_failure_status_goto_error( status ) if( _failure( status ) ) { camp_tcl_statmsg( interp, status ); goto error; }

static void campTcl_list_each( Tcl_Interp* interp, u_long attr, CAMP_VAR* pVar_start );
static void campTcl_list_each_log( Tcl_Interp* interp, CAMP_VAR* pVar_start, int num_act, char* actions[] );

// static char* itoa( int i );
// static char* ltoa( long i ); 
// static char* ftoa( double d ); 
// int camp_tcl_insAvail ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_sys ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_ins ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varDef ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varSet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varRead ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varTest ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_lnkSet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varNumGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varArr ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varSelGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varInsGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfRs232 ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfGpib ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfCamac ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfVme ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfTcpip ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_varGetIfIndpak ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_lnkGet ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_msg ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_sleep ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_gettime ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
#if CAMP_HAVE_CAMAC
int camp_tcl_camac_cdreg ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_camac_single ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_camac_block ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
#endif // CAMP_HAVE_CAMAC
int camp_tcl_vme_io ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_gpib_clear ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
int camp_tcl_gpib_timeout ( ClientData clientData , Tcl_Interp *interp , int argc , char *argv []);
#ifdef VXWORKS
int camp_tcl_tip850(ClientData clientData, Tcl_Interp * interp, int argc, char * argv[]);
int camp_tcl_te28(ClientData clientData, Tcl_Interp * interp, int argc, char * argv[]);
#endif // VXWORKS
int camp_tcl_alarmTdMusr( ClientData clientData, Tcl_Interp* interp, int argc, char* argv[] );
int camp_tcl_alarmIMusr( ClientData clientData, Tcl_Interp* interp, int argc, char* argv[] );
// static void  camp_tcl_statmsg( Tcl_Interp *interp, int stat );

extern int if_gpib_clear( int gpib_addr );
extern int if_gpib_timeout( float timeoutSeconds );

typedef enum
{
    SYS_UPDATE=1,
    SYS_SHUTDOWN,
    SYS_REBOOT,
    SYS_LOAD,
    SYS_SAVE,
    SYS_DIR,
    SYS_GETDIR,
    SYS_GETINSTYPES,
    SYS_GETALARMACTS,
    SYS_GETLOGACTS,
    SYS_GETIFTYPES,
    SYS_GETIFCONF,
    SYS_ADDALARMACT,
    SYS_ADDLOGACT,
    SYS_ADDIFTYPE,
    SYS_ADDINSTYPE,
    SYS_ADDINSAVAIL,
    SYS_GETINSNAMES,
    SYS_GETVARSONPATH,
    SYS_GETLOGGEDVARS,
    SYS_GETALERTVARS,
    SYS_SETDEBUG,
    SYS_GETDEBUG,
} SYS_PROCS;

typedef enum 
{
    INS_ADD=1,
    INS_SET,
    INS_DEL,
    INS_LOAD,
    INS_SAVE,
    INS_IF_ON,
    INS_IF_OFF,
    INS_IF_READ,
    INS_IF_WRITE,
    INS_IF_READ_VERIFY,
    INS_IF_WRITE_VERIFY,
    INS_IF_DUMP,
    INS_IF_UNDUMP,
} INS_PROCS;

typedef enum 
{
    VAR_INT=1,
    VAR_FLOAT,
    VAR_STRING,
    VAR_SELECTION,
    VAR_ARRAY,
    VAR_STRUCTURE,
    VAR_INSTRUMENT,
    VAR_LINK,
    VAR_DEF,
    VAR_SET,
    VAR_DOSET,
    VAR_READ,
    LNK_SET,
} VAR_PROCS;

typedef enum 
{
    VAR_TESTTOL,
    VAR_TESTALERT,
} VAR_UTILPROCS;

typedef enum 
{
    VAR_IDENT=1,
    VAR_PATH,
    VAR_VARTYPE,
    VAR_ATTRIBUTES,
    VAR_TITLE,
    VAR_HELP,
    VAR_STATUS,
    VAR_STATUSMSG,
    VAR_TIMELASTSET,
    VAR_POLLINTERVAL,
    VAR_LOGINTERVAL,
    VAR_LOGACTION,
    VAR_ALARMACTION,
    VAR_VAL,
} VAR_GETPROCS;

typedef enum 
{
    VAR_TIMESTARTED=1,
    VAR_NUM,
    VAR_LOW,
    VAR_HI,
    VAR_SUM,
    VAR_SUMSQUARES,
    VAR_SUMCUBES,
    VAR_OFFSET,
    VAR_TOL,
    VAR_TOLTYPE,
    VAR_UNITS,
    VAR_MEAN,
    VAR_STDDEV,
    VAR_SKEW,
} VAR_NUMGETPROCS;

typedef enum 
{
    SEL_VALLABEL=1,
} VAR_SELGETPROCS;

typedef enum
{
    ARR_SETVAL=1,
    ARR_RESIZE,
    ARR_GETVAL,
    ARR_GETVARTYPE,
    ARR_GETELEMSIZE,
    ARR_GETDIM,
    ARR_GETDIMSIZE,
    ARR_GETTOTELEM,
} VAR_ARR_PROCS;

typedef enum 
{
    INS_LOCKHOST=1,
    INS_LOCKPID,
    INS_DEFFILE,
    INS_INIFILE,
    INS_DATAITEMS_LEN,
    INS_TYPEIDENT,
    INS_TYPEINSTANCE,
    INS_LEVELCLASS,
    INS_IF_STATUS,
    INS_IF_TYPEIDENT,
    INS_IF_ACCESSDELAY,
    INS_IF_TIMEOUT, // 20140407  TW  added
    INS_IF,
    INS_IF_NUMREAD, // 20191207  DJA  added these 6 for connection bookkeeping
    INS_IF_NUMWRITE,
    INS_IF_READERR,
    INS_IF_WRITEERR,
    INS_IF_CONSECREADERR,
    INS_IF_CONSECWRITEERR,
} VAR_INSGETPROCS;

typedef enum 
{
    INS_CAMP_IF_RS232_NAME=1,
    INS_CAMP_IF_RS232_BAUD,
    INS_CAMP_IF_RS232_DATABITS,
    INS_CAMP_IF_RS232_PARITY,
    INS_CAMP_IF_RS232_STOPBITS,
    INS_CAMP_IF_RS232_READTERM,
    INS_CAMP_IF_RS232_WRITETERM,
    INS_CAMP_IF_RS232_READTIMEOUT,
    INS_CAMP_IF_GPIB_ADDR,
    INS_CAMP_IF_GPIB_READTERM,
    INS_CAMP_IF_GPIB_WRITETERM,
    INS_CAMP_IF_CAMAC_B,
    INS_CAMP_IF_CAMAC_C,
    INS_CAMP_IF_CAMAC_N,
    INS_CAMP_IF_INDPAK_SLOT,
    INS_CAMP_IF_INDPAK_TYPE,
    INS_CAMP_IF_INDPAK_CHANNEL,
    INS_CAMP_IF_VME_BASE,
    INS_CAMP_IF_TCPIP_ADDRESS,
    INS_CAMP_IF_TCPIP_PORT,
    INS_CAMP_IF_TCPIP_READTERM,
    INS_CAMP_IF_TCPIP_WRITETERM,
    INS_CAMP_IF_TCPIP_TIMEOUT,
    INS_CAMP_IF_GPIB_MSCBADDR,
} VAR_GETIFPROCS;

typedef enum 
{
    LNK_VARTYPE=1,
    LNK_PATH,
} VAR_GETLNKPROCS;

typedef enum
{
    CAMAC_CDREG=1,
    CAMAC_CFSA,
    CAMAC_CSSA,
    CAMAC_CFUBC,
    CAMAC_CFUBR,
    CAMAC_CSUBC,
    CAMAC_CSUBR
} CAMAC_PROCS;

typedef enum
{
    VME_READ=1,
    VME_WRITE
} VME_PROCS;

/*
 *  GPIB procs - direct GPIB bus commands
 *  21-Dec-1999  TW  gpib_clear does a GPIB SDC (selected device clear)
 */
typedef enum
{
    GPIB_CLEAR=1,
    GPIB_TIMEOUT
} GPIB_PROCS;


/* DAC/ADC functions for tip850 Industry Pack channels */

typedef enum
{
    DAC_SET=1,
    DAC_READ,
    ADC_READ,
    GAIN_SET
} DACADC_PROCS;

/* Motor functions for te28 Industry Pack channels */

typedef enum
{
    MOTOR_RESET = 1,
    MOTOR_VEL,
    MOTOR_ACC,
    MOTOR_FILTER_KP,
    MOTOR_FILTER_KI,
    MOTOR_FILTER_KD,
    MOTOR_FILTER_IL,
    MOTOR_FILTER_SI,
    MOTOR_STOP,
    MOTOR_ABORT,
    MOTOR_SLOPE,
    MOTOR_OFFSET,
    MOTOR_ENCODER,
    MOTOR_LIMIT,
    MOTOR_MOVE,
    MOTOR_POSITION
} MOTOR_PROCS;


/*
 *  camp_itoa - integer to ASCII
 */
static char*
camp_itoa( int i, char* str, size_t str_size )
{
    camp_snprintf( str, str_size, "%d", i );
    return( str );
}


/*
 *  Return pointer to main Tcl interp
 */
Tcl_Interp*
camp_tclInterp( void )
{
    return( tclInterp );
}


/*
 *  camp_tclInit()  -  create main Tcl interpreter
 */
int
camp_tclInit( void )
{
    tclInterp = campTcl_CreateInterp();
    if( tclInterp == NULL )
    {
        _camp_setMsg( "failed to create Tcl interpreter" );
        return( CAMP_FAILURE );
    }
    return( CAMP_SUCCESS );
}


/*
 *  camp_tclEnd
 *
 *  Delete the main Tcl interpreter
 */
void
camp_tclEnd( void )
{
    if( tclInterp != NULL )
    {
	campTcl_DeleteInterp( tclInterp );
	tclInterp = NULL;
    }
}


/*
 *  campTcl_CreateInterp()  -  create a Tcl interpreter with all
 *                             of the CAMP variables and built-in
 *                             procedures defined.
 *
 *  In multithreaded implementation this routine is called to create
 *  a Tcl interpreter for the main server thread, and each instance
 *  of a CAMP instrument.  This allows use of Tcl interpreters for
 *  instrument drivers without conflict between the context of interpreters
 *  for different instruments.  Since only one action (read/write) is
 *  allowed on an instrument at one time, one interpreter is sufficient
 *  per instrument.
 */
Tcl_Interp* 
campTcl_CreateInterp( void )
{
    char hostname[LEN_NODENAME+1];
    char* s;
    Tcl_Interp* interp;
    char buf[LEN_BUF+1];

    interp = Tcl_CreateInterp();
    if( interp == NULL )
    {
        _camp_setMsg( "failed to create Tcl interpreter" );
        return( NULL );
    }

    /*
     *  Hostname variables
     */
    gethostname( hostname, LEN_NODENAME );
    stolower( hostname );
    Tcl_SetVar( interp, "camp_hostname", hostname, TCL_GLOBAL_ONLY );
    if( ( s = strchr( hostname, '.' ) ) != NULL ) *s = '\0';
    Tcl_SetVar( interp, "camp_host", hostname, TCL_GLOBAL_ONLY );

    Tcl_SetVar( interp, "CAMP_DIR", (char*)camp_getFilemaskSpecific( CAMP_DIR ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_NODE_DIR", (char*)camp_getFilemaskSpecific( CAMP_NODE_DIR ), TCL_GLOBAL_ONLY );

    /*
     *  Set the precision to something more than the 
     *  default of 6
     */
    Tcl_SetVar( interp, "tcl_precision", "12", TCL_GLOBAL_ONLY );

    /*
     *  Var attribute constants
     */
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_SHOW", 
                camp_itoa( CAMP_VAR_ATTR_SHOW, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_SET", 
                camp_itoa( CAMP_VAR_ATTR_SET, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_READ", 
                camp_itoa( CAMP_VAR_ATTR_READ, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_POLL", 
                camp_itoa( CAMP_VAR_ATTR_POLL, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_ALARM", 
                camp_itoa( CAMP_VAR_ATTR_ALARM, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_LOG", 
                camp_itoa( CAMP_VAR_ATTR_LOG, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_ALERT", 
                camp_itoa( CAMP_VAR_ATTR_ALERT, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_ATTR_IS_SET", 
                camp_itoa( CAMP_VAR_ATTR_IS_SET, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );

    /*
     *  Instrument constants
     */
    Tcl_SetVar( interp, "CAMP_INS_ATTR_LOCKED", 
                camp_itoa( CAMP_INS_ATTR_LOCKED, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );

    /*
     *  Interface constants
     */
    Tcl_SetVar( interp, "CAMP_IF_ONLINE", 
                camp_itoa( CAMP_IF_ONLINE, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );

    Tcl_SetVar( interp, "CAMP_MAJOR_VAR_TYPE_MASK", 
                camp_itoa( CAMP_MAJOR_VAR_TYPE_MASK, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );

    /*
     *  Var type constants
     */
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_NUMERIC", 
                camp_itoa( CAMP_VAR_TYPE_NUMERIC, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_INT", 
                camp_itoa( CAMP_VAR_TYPE_INT, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_FLOAT", 
                camp_itoa( CAMP_VAR_TYPE_FLOAT, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_SELECTION", 
                camp_itoa( CAMP_VAR_TYPE_SELECTION, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_STRING", 
                camp_itoa( CAMP_VAR_TYPE_STRING, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_STRUCTURE", 
                camp_itoa( CAMP_VAR_TYPE_STRUCTURE, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_ARRAY", 
                camp_itoa( CAMP_VAR_TYPE_ARRAY, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_INSTRUMENT", 
                camp_itoa( CAMP_VAR_TYPE_INSTRUMENT, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_VAR_TYPE_LINK", 
                camp_itoa( CAMP_VAR_TYPE_LINK, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );

    /*
     *  Interface type constants
     */
/*  Don't need these, interfaces are identified by
    the TypeIdent

    Tcl_SetVar( interp, "CAMP_IF_TYPE_NONE", 
                camp_itoa( CAMP_IF_TYPE_NONE, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_RS232", 
                camp_itoa( CAMP_IF_TYPE_RS232, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_GPIB", 
                camp_itoa( CAMP_IF_TYPE_GPIB, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_TICS_RPC", 
                camp_itoa( CAMP_IF_TYPE_TICS, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
    Tcl_SetVar( interp, "CAMP_IF_TYPE_CAMAC", 
                camp_itoa( CAMP_IF_TYPE_CAMAC, buf, sizeof( buf ) ), TCL_GLOBAL_ONLY );
*/
    /*
     *  Alarm commands
     */
    Tcl_CreateCommand( interp, "alarmTdMusr", (Tcl_CmdProc *)camp_tcl_alarmTdMusr,
                        (ClientData)0, NULL );
    Tcl_CreateCommand( interp, "alarmIMusr", (Tcl_CmdProc *)camp_tcl_alarmIMusr,
                        (ClientData)0, NULL );

    /*
     *  sys commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_UPDATE ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_UPDATE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_SHUTDOWN ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_SHUTDOWN, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_REBOOT ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_REBOOT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_LOAD ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_LOAD, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_SAVE ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_SAVE, NULL );
    Tcl_CreateCommand( interp, "sysDir", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_DIR, NULL );
#ifdef RETIRED
    Tcl_CreateCommand( interp, "sysGetDir", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETDIR, NULL );
#endif // RETIRED
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETALARMACTS ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETALARMACTS, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETIFCONF ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETIFCONF, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETIFTYPES ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETIFTYPES, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETINSTYPES ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETINSTYPES, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_GETLOGACTS ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETLOGACTS, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDALARMACT ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_ADDALARMACT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDIFTYPE ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_ADDIFTYPE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDINSAVAIL ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_ADDINSAVAIL, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDINSTYPE ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_ADDINSTYPE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SYS_ADDLOGACT ), (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_ADDLOGACT, NULL );
    Tcl_CreateCommand( interp, "sysGetInsNames", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETINSNAMES, NULL );
    Tcl_CreateCommand( interp, "sysGetVarsOnPath", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETVARSONPATH, NULL );
    Tcl_CreateCommand( interp, "sysGetLoggedVars", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETLOGGEDVARS, NULL );
    Tcl_CreateCommand( interp, "sysGetAlertVars", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETALERTVARS, NULL );
    Tcl_CreateCommand( interp, "sysSetDebug", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_SETDEBUG, NULL );
    Tcl_CreateCommand( interp, "sysGetDebug", (Tcl_CmdProc *)camp_tcl_sys,
                        (ClientData)SYS_GETDEBUG, NULL );

    /*
     *  ins commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_INS_ADD ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_ADD, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_SET ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_SET, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_DEL ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_DEL, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_LOAD ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_LOAD, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_SAVE ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_SAVE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_ON ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_IF_ON, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_OFF ),(Tcl_CmdProc *) camp_tcl_ins, 
                        (ClientData)INS_IF_OFF, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_READ ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_IF_READ, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_WRITE ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_IF_WRITE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_READ_VERIFY ), 
                        (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_IF_READ_VERIFY, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_WRITE_VERIFY ), 
                        (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_IF_WRITE_VERIFY, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_DUMP ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_IF_DUMP, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INS_IF_UNDUMP ), (Tcl_CmdProc *)camp_tcl_ins, 
                        (ClientData)INS_IF_UNDUMP, NULL );

    /*
     *  varSet commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_INT ), (Tcl_CmdProc *)camp_tcl_varDef, 
                        (ClientData)VAR_INT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_FLOAT ), (Tcl_CmdProc *)camp_tcl_varDef, 
                        (ClientData)VAR_FLOAT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_STRING ), (Tcl_CmdProc *)camp_tcl_varDef, 
                        (ClientData)VAR_STRING, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_SELECTION ), (Tcl_CmdProc *)camp_tcl_varDef, 
                        (ClientData)VAR_SELECTION, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_STRUCTURE ), (Tcl_CmdProc *)camp_tcl_varDef, 
                        (ClientData)VAR_STRUCTURE, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_INSTRUMENT ), (Tcl_CmdProc *)camp_tcl_varDef, 
                        (ClientData)VAR_INSTRUMENT, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_LINK ), (Tcl_CmdProc *)camp_tcl_varDef, 
                        (ClientData)VAR_LINK, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_VAR_SET ), (Tcl_CmdProc *)camp_tcl_varSet, 
                        (ClientData)VAR_SET, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_VAR_DOSET ), (Tcl_CmdProc *)camp_tcl_varSet,
                        (ClientData)VAR_DOSET, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_VAR_READ ), (Tcl_CmdProc *)camp_tcl_varRead, 
                        (ClientData)VAR_READ, NULL );
    Tcl_CreateCommand( interp, toktostr( TOK_LNK_SET ), (Tcl_CmdProc *)camp_tcl_lnkSet, 
                        (ClientData)LNK_SET, NULL );

    /*
     *  var utilities
     */
    Tcl_CreateCommand( interp, "varTestTol", (Tcl_CmdProc *)camp_tcl_varTest,
                        (ClientData)VAR_TESTTOL, NULL );
    Tcl_CreateCommand( interp, "varTestAlert", (Tcl_CmdProc *)camp_tcl_varTest,
                        (ClientData)VAR_TESTALERT, NULL );

    /*
     *  varGet commands
     */
    Tcl_CreateCommand( interp, "varGetIdent", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_IDENT, NULL );
    Tcl_CreateCommand( interp, "varGetPath", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_PATH, NULL );
    Tcl_CreateCommand( interp, "varGetVarType", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_VARTYPE, NULL );
    Tcl_CreateCommand( interp, "varGetAttributes", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_ATTRIBUTES, NULL );
    Tcl_CreateCommand( interp, "varGetTitle", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_TITLE, NULL );
    Tcl_CreateCommand( interp, "varGetHelp", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_HELP, NULL );
    Tcl_CreateCommand( interp, "varGetStatus", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_STATUS, NULL );
    Tcl_CreateCommand( interp, "varGetStatusMsg", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_STATUSMSG, NULL );
    Tcl_CreateCommand( interp, "varGetTimeLastSet", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_TIMELASTSET, NULL );
    Tcl_CreateCommand( interp, "varGetPollInterval", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_POLLINTERVAL, NULL );
    Tcl_CreateCommand( interp, "varGetLogInterval", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_LOGINTERVAL, NULL );
    Tcl_CreateCommand( interp, "varGetLogAction",(Tcl_CmdProc *) camp_tcl_varGet, 
                        (ClientData)VAR_LOGACTION, NULL );
    Tcl_CreateCommand( interp, "varGetAlarmAction", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_ALARMACTION, NULL );
    Tcl_CreateCommand( interp, "varGetVal", (Tcl_CmdProc *)camp_tcl_varGet, 
                        (ClientData)VAR_VAL, NULL );

    /*
     *  varNumGet commands
     */
    Tcl_CreateCommand( interp, "varNumGetTimeStarted", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_TIMESTARTED, NULL );
    Tcl_CreateCommand( interp, "varNumGetNum", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_NUM, NULL );
    Tcl_CreateCommand( interp, "varNumGetLow", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_LOW, NULL );
    Tcl_CreateCommand( interp, "varNumGetHi", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_HI, NULL );
    Tcl_CreateCommand( interp, "varNumGetSum", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_SUM, NULL );
    Tcl_CreateCommand( interp, "varNumGetSumSquares", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_SUMSQUARES, NULL );
    Tcl_CreateCommand( interp, "varNumGetSumCubes", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_SUMCUBES, NULL );
    Tcl_CreateCommand( interp, "varNumGetSumOffset", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_OFFSET, NULL );
    Tcl_CreateCommand( interp, "varNumGetTol", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_TOL, NULL );
    Tcl_CreateCommand( interp, "varNumGetTolType", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_TOLTYPE, NULL );
    Tcl_CreateCommand( interp, "varNumGetUnits", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_UNITS, NULL );
    Tcl_CreateCommand( interp, "varNumGetMean", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_MEAN, NULL );
    Tcl_CreateCommand( interp, "varNumGetStdDev", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_STDDEV, NULL );
    Tcl_CreateCommand( interp, "varNumGetSkew", (Tcl_CmdProc *)camp_tcl_varNumGet, 
                        (ClientData)VAR_SKEW, NULL );

    /*
     *  varSelGet commands
     */
    Tcl_CreateCommand( interp, "varSelGetValLabel",(Tcl_CmdProc *) camp_tcl_varSelGet, 
                        (ClientData)SEL_VALLABEL, NULL );

    /*
     *  varArr commands
     */
    Tcl_CreateCommand( interp, "varArrSetVal", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_SETVAL, NULL );
    Tcl_CreateCommand( interp, "varArrResize", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_RESIZE, NULL );
    Tcl_CreateCommand( interp, "varArrGetVal", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_GETVAL, NULL );
    Tcl_CreateCommand( interp, "varArrGetVarType", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_GETVARTYPE, NULL );
    Tcl_CreateCommand( interp, "varArrGetElemSize", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_GETELEMSIZE, NULL );
    Tcl_CreateCommand( interp, "varArrGetDim", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_GETDIM, NULL );
    Tcl_CreateCommand( interp, "varArrGetDimSize", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_GETDIMSIZE, NULL );
    Tcl_CreateCommand( interp, "varArrGetTotElem", (Tcl_CmdProc *)camp_tcl_varArr,
                       (ClientData)ARR_GETTOTELEM, NULL );

    /*
     *  insGet commands
     */
    Tcl_CreateCommand( interp, "insGetLockHost", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_LOCKHOST, NULL );
    Tcl_CreateCommand( interp, "insGetLockPid", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_LOCKPID, NULL );
    Tcl_CreateCommand( interp, "insGetDefFile", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_DEFFILE, NULL );
    Tcl_CreateCommand( interp, "insGetIniFile", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_INIFILE, NULL );
    Tcl_CreateCommand( interp, "insGetDataItemsLen", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_DATAITEMS_LEN, NULL );
    Tcl_CreateCommand( interp, "insGetTypeIdent", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_TYPEIDENT, NULL );
    Tcl_CreateCommand( interp, "insGetInstance", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_TYPEINSTANCE, NULL );
    Tcl_CreateCommand( interp, "insGetClass", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_LEVELCLASS, NULL );
    Tcl_CreateCommand( interp, "insGetIfStatus", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_STATUS, NULL );
    Tcl_CreateCommand( interp, "insGetIfTypeIdent", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_TYPEIDENT, NULL );
    Tcl_CreateCommand( interp, "insGetIfDelay", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_ACCESSDELAY, NULL );
    Tcl_CreateCommand( interp, "insGetIfTimeout", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_TIMEOUT, NULL );
    Tcl_CreateCommand( interp, "insGetIf", (Tcl_CmdProc *)camp_tcl_varInsGet,
                        (ClientData)INS_IF, NULL );
    Tcl_CreateCommand( interp, "insGetIfNumReads", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_NUMREAD, NULL );
    Tcl_CreateCommand( interp, "insGetIfNumWrites", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_NUMWRITE, NULL );
    Tcl_CreateCommand( interp, "insGetIfNumReadErr", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_READERR, NULL );
    Tcl_CreateCommand( interp, "insGetIfNumWriteErr", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_WRITEERR, NULL );
    Tcl_CreateCommand( interp, "insGetIfConsecReadErr", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_CONSECREADERR, NULL );
    Tcl_CreateCommand( interp, "insGetIfConsecWriteErr", (Tcl_CmdProc *)camp_tcl_varInsGet, 
                        (ClientData)INS_IF_CONSECWRITEERR, NULL );

    /*
     *  varGetIfRs232 commands
     */
    Tcl_CreateCommand( interp, "insGetIfRs232Name", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_NAME, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Baud", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_BAUD, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Data", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_DATABITS, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Parity", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_PARITY, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Stop", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_STOPBITS, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232ReadTerm", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_READTERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232WriteTerm", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_WRITETERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfRs232Timeout", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfRs232, 
                        (ClientData)INS_CAMP_IF_RS232_READTIMEOUT, NULL );

    /*
     *  varGetIfGpib commands
     */
    Tcl_CreateCommand( interp, "insGetIfGpibAddr", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfGpib, 
                        (ClientData)INS_CAMP_IF_GPIB_ADDR, NULL );
    Tcl_CreateCommand( interp, "insGetIfGpibReadTerm", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfGpib, 
                        (ClientData)INS_CAMP_IF_GPIB_READTERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfGpibWriteTerm", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfGpib, 
                        (ClientData)INS_CAMP_IF_GPIB_WRITETERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfGpibMscbAddr", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfGpib, 
                        (ClientData)INS_CAMP_IF_GPIB_MSCBADDR, NULL );

    /*
     *  varGetIfCamac commands
     */
    Tcl_CreateCommand( interp, "insGetIfCamacB", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfCamac, 
                        (ClientData)INS_CAMP_IF_CAMAC_B, NULL );
    Tcl_CreateCommand( interp, "insGetIfCamacC", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfCamac, 
                        (ClientData)INS_CAMP_IF_CAMAC_C, NULL );
    Tcl_CreateCommand( interp, "insGetIfCamacN", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfCamac, 
                        (ClientData)INS_CAMP_IF_CAMAC_N, NULL );

    /*
     *  varGetIfVme commands
     */
    Tcl_CreateCommand( interp, "insGetIfVmeBase", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfVme, 
                        (ClientData)INS_CAMP_IF_VME_BASE, NULL );

    /*
     *  varGetIfTcpip commands
     */
    Tcl_CreateCommand( interp, "insGetIfTcpipAddress", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_ADDRESS, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipPort", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_PORT, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipReadTerm", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_READTERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipWriteTerm", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_WRITETERM, NULL );
    Tcl_CreateCommand( interp, "insGetIfTcpipTimeout", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfTcpip, 
                        (ClientData)INS_CAMP_IF_TCPIP_TIMEOUT, NULL );

#ifdef VXWORKS
    /*
     *  varGetIfIndpak commands
     */
    Tcl_CreateCommand( interp, "insGetIfIndpakSlot", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfIndpak, 
                        (ClientData)INS_CAMP_IF_INDPAK_SLOT, NULL );
    Tcl_CreateCommand( interp, "insGetIfIndpakType", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfIndpak, 
                        (ClientData)INS_CAMP_IF_INDPAK_TYPE, NULL );
    Tcl_CreateCommand( interp, "insGetIfIndpakChannel", 
                        (Tcl_CmdProc *)camp_tcl_varGetIfIndpak, 
                        (ClientData)INS_CAMP_IF_INDPAK_CHANNEL, NULL );
#endif // VXWORKS

    /*
     *  varLnkGet commands
     */
    Tcl_CreateCommand( interp, "lnkGetVarType", (Tcl_CmdProc *)camp_tcl_lnkGet, 
                        (ClientData)LNK_VARTYPE, NULL );
    Tcl_CreateCommand( interp, "lnkGetPath", (Tcl_CmdProc *)camp_tcl_lnkGet, 
                        (ClientData)LNK_PATH, NULL );

#if CAMP_HAVE_CAMAC
    /*
     *  CAMAC commands
     */
    Tcl_CreateCommand( interp, "cdreg", (Tcl_CmdProc *)camp_tcl_camac_cdreg, 
                        (ClientData)CAMAC_CDREG, NULL );
    Tcl_CreateCommand( interp, "cfsa", (Tcl_CmdProc *)camp_tcl_camac_single, 
                        (ClientData)CAMAC_CFSA, NULL );
    Tcl_CreateCommand( interp, "cssa", (Tcl_CmdProc *)camp_tcl_camac_single, 
                        (ClientData)CAMAC_CSSA, NULL );
    Tcl_CreateCommand( interp, "cfubc", (Tcl_CmdProc *)camp_tcl_camac_block, 
                        (ClientData)CAMAC_CFUBC, NULL );
    Tcl_CreateCommand( interp, "cfubr", (Tcl_CmdProc *)camp_tcl_camac_block, 
                        (ClientData)CAMAC_CFUBR, NULL );
    Tcl_CreateCommand( interp, "csubc", (Tcl_CmdProc *)camp_tcl_camac_block, 
                        (ClientData)CAMAC_CSUBC, NULL );
    Tcl_CreateCommand( interp, "csubr", (Tcl_CmdProc *)camp_tcl_camac_block, 
                        (ClientData)CAMAC_CSUBR, NULL );
#endif // CAMP_HAVE_CAMAC

    /*
     *  GPIB commands
     */
    Tcl_CreateCommand( interp, "gpibClear", (Tcl_CmdProc *)camp_tcl_gpib_clear, 
                        (ClientData)GPIB_CLEAR, NULL );
    Tcl_CreateCommand( interp, "gpibTimeout", (Tcl_CmdProc *)camp_tcl_gpib_timeout, 
                        (ClientData)GPIB_TIMEOUT, NULL );

    /*
     *  VME commands (May apply to more than VXWORKS)
     */
    Tcl_CreateCommand( interp, "vmeRead", (Tcl_CmdProc *)camp_tcl_vme_io, 
                        (ClientData)VME_READ, NULL );
    Tcl_CreateCommand( interp, "vmeWrite", (Tcl_CmdProc *)camp_tcl_vme_io, 
                        (ClientData)VME_WRITE, NULL );


#ifdef VXWORKS

    /* Tip850 DAC/ADC commands - used only in VxWorks image with
       Industry Packs on carrier board     */

    Tcl_CreateCommand(interp, "DacSet", (Tcl_CmdProc *)camp_tcl_tip850, 
		      (ClientData) DAC_SET, NULL);
    Tcl_CreateCommand(interp, "DacRead", (Tcl_CmdProc *)camp_tcl_tip850, 
		      (ClientData) DAC_READ, NULL);
    Tcl_CreateCommand(interp, "AdcRead", (Tcl_CmdProc *)camp_tcl_tip850, 
		      (ClientData) ADC_READ, NULL);
    Tcl_CreateCommand(interp, "GainSet", (Tcl_CmdProc *)camp_tcl_tip850, 
		      (ClientData) GAIN_SET, NULL);


    /*
     * Te28 Motor Controller commands
     */

    Tcl_CreateCommand(interp, "MotorReset", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_RESET, NULL);
    Tcl_CreateCommand(interp, "MotorVel", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_VEL, NULL);
    Tcl_CreateCommand(interp, "MotorAccel", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_ACC, NULL);
    Tcl_CreateCommand(interp, "MotorFilterKP", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_KP, NULL);
    Tcl_CreateCommand(interp, "MotorFilterKI", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_KI, NULL);
    Tcl_CreateCommand(interp, "MotorFilterKD", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_KD, NULL);
    Tcl_CreateCommand(interp, "MotorFilterIL", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_IL, NULL);
    Tcl_CreateCommand(interp, "MotorFilterSI", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_FILTER_SI, NULL);
    Tcl_CreateCommand(interp, "MotorStop", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_STOP, NULL);
    Tcl_CreateCommand(interp, "MotorAbort", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_ABORT, NULL);
    Tcl_CreateCommand(interp, "MotorSlope", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_SLOPE, NULL);
    Tcl_CreateCommand(interp, "MotorOffset", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_OFFSET, NULL);
    Tcl_CreateCommand(interp, "MotorEncoder", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_ENCODER, NULL);
    Tcl_CreateCommand(interp, "MotorLimit", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_LIMIT, NULL);
    Tcl_CreateCommand(interp, "MotorMove", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_MOVE, NULL);
    Tcl_CreateCommand(interp, "MotorPosition", (Tcl_CmdProc *)camp_tcl_te28, 
		      (ClientData) MOTOR_POSITION, NULL);

#endif // VXWORKS

    /*
     *  misc commands
     */
    Tcl_CreateCommand( interp, toktostr( TOK_MSG ), (Tcl_CmdProc *)camp_tcl_msg, 
                        (ClientData)0, NULL );
    Tcl_CreateCommand( interp, "sleep", (Tcl_CmdProc *)camp_tcl_sleep, 
                        (ClientData)0, NULL );
    Tcl_CreateCommand( interp, "gettime", (Tcl_CmdProc *)camp_tcl_gettime, 
                        (ClientData)0, NULL );

    return( interp );
}


/*
 *  campTcl_DeleteInterp
 *
 *  Delete a CAMP Tcl interpreter
 */
void
campTcl_DeleteInterp( Tcl_Interp* interp )
{
    Tcl_DeleteInterp( interp );
}


/*
 *  Name:       campTcl_list_each
 *
 *  Purpose:    Generate a Tcl result listing (the path of) each variable
 *              currently holding (any of) the specified attribute(s)
 */

void
campTcl_list_each( Tcl_Interp* interp, u_long attr, CAMP_VAR* pVar_start )
{
    CAMP_VAR* pVar;

    for( pVar = pVar_start; pVar != NULL; pVar = pVar->pNext )
    {
        if( pVar->core.status & attr )
        {
            // Tcl_AppendResult( interp, pVar->core.path, " ", (char*)NULL );
	    camp_appendMsgNoSep( "%s ", pVar->core.path );
        }            

        if( pVar->pChild != NULL ) 
        {
	    campTcl_list_each( interp, attr, pVar->pChild );
        }
    }

    return;
}

void
campTcl_list_each_log( Tcl_Interp* interp, CAMP_VAR* pVar_start, int num_act, char* actions[] )
{
    CAMP_VAR* pVar;

    for( pVar = pVar_start; pVar != NULL; pVar = pVar->pNext )
    {
        if( pVar->core.status & CAMP_VAR_ATTR_LOG )
        {
            if( num_act <= 0 ) /* List all logged vars, whatever log action */
            {
                // Tcl_AppendResult( interp, pVar->core.path, " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pVar->core.path );
            }
            else /* Must match specified log action */
            {
		int n;
                for( n = 0; n < num_act; n++ )
                {
                    if( !strncmp( pVar->core.logAction, actions[n], 32 ) )
                    {
                        // Tcl_AppendResult( interp, pVar->core.path, " ", (char*)NULL );
			camp_appendMsgNoSep( "%s ", pVar->core.path );
                        break;
                    }
                }
            }
        }

        if( pVar->pChild != NULL ) 
        {
            campTcl_list_each_log( interp, pVar->pChild, num_act, actions );
        }
    }

    return;
}

/*
 * //  variadic macro args compatible with older versions of gcc (i.e., for vxworks cross-compiling)
 *#undef  _camp_log
 *#define _camp_log( fmt, args... )                             if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_setLogPrefixOnce( argv[0] ); } camp_log      ( fmt , ##args ) // note space before comma
 *#undef  _camp_setMsg
 *#define _camp_setMsg( fmt, args... )       camp_setMsg( "" ); if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( argv[0] ); }  camp_appendMsg( fmt , ##args )
 *#undef  _camp_appendMsg
 *#define _camp_appendMsg( fmt, args... )                       if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( argv[0] ); }  camp_appendMsg( fmt , ##args )
 *#undef  _camp_setMsgStat
 *#define _camp_setMsgStat( status, arg )    camp_setMsg( "" ); if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( argv[0] ); }  camp_appendMsgStat( status, arg )
 *#undef  _camp_appendMsgStat
 *#define _camp_appendMsgStat( status, arg )                    if( _camp_debug(CAMP_DEBUG_TRACE) ) { camp_appendMsgPrefix( argv[0] ); }  camp_appendMsgStat( status, arg )
 */

/* 
 * could unify the following 3 macros with:
 * #define _campTcl_setErrMsg( arg1, args... ) _camp_setMsg( "%s in %s %s %s %s", arg1, argv[0], argv[1] , ##args )
 * at the cost of some extra trailing spaces. (Note space before final comma.)
 */
#define _campTcl_setErrMsg1( arg1 ) _camp_setMsg( "%s in %s %s", arg1, argv[0], argv[1] )
#define _campTcl_setErrMsg2( arg1, arg2 ) _camp_setMsg( "%s in %s %s %s", arg1, argv[0], argv[1], arg2 )
#define _campTcl_setErrMsg3( arg1, arg2, arg3 ) _camp_setMsg( "%s in %s %s %s %s", arg1, argv[0], argv[1], arg2, arg3 )

/*
 *  Name:       camp_tcl_sys
 *
 *  Purpose:    Process "sys*" Tcl commands
 *
 *  Provides:
 *              sysUpdate
 *              sysShutdown
 *              sysReboot
 *              sysLoad
 *              sysSave
 *              sysDir
 *              sysGetAlarmActs
 *              sysGetDir (retired)
 *              sysGetIfConf
 *              sysGetIfTypes
 *              sysGetInsTypes
 *              sysGetLogActs
 *              sysAddAlarmAct
 *              sysAddIfType
 *              sysAddInsAvail
 *              sysAddInsType
 *              sysAddLogAct
 *              sysSetDebug
 *              sysGetDebug
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_sys( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int camp_status = CAMP_SUCCESS;
    RES* pRes = NULL;
    FILE_req fileReq;
    char* endptr;
    char message[LEN_MSG+1];
    DIR_res* pDirRes = NULL;
    // _mutex_lock_begin();

    switch( (SYS_PROCS)clientData )
    {
      case SYS_UPDATE:

        pRes = campsrv_sysupdate_10400( NULL, get_current_rqstp() );
        if( _failure( pRes->status ) ) goto fail;
        _free( pRes );
        break;
    
      case SYS_SHUTDOWN:

        pRes = campsrv_sysshutdown_10400( NULL, get_current_rqstp() );
        if( _failure( pRes->status ) ) goto fail;
        _free( pRes );
        break;

      case SYS_REBOOT:

        pRes = campsrv_sysreboot_10400( NULL, get_current_rqstp() );
        if( _failure( pRes->status ) ) goto fail;
        _free( pRes );
        break;

      case SYS_LOAD:

        camp_snprintf( message, sizeof( message ), "%s <filename> [<flag>]", argv[0] );

        if( argc < 2 ) goto usage;

        fileReq.filename = argv[1];
        if( argc > 2 )
        {
            fileReq.flag = strtol( argv[2], &endptr, 0 );
            if( isgraph( *endptr ) ) goto usage;
        }
        else
        {
            fileReq.flag = 0;
        }

        pRes = campsrv_sysload_10400( &fileReq, get_current_rqstp() );
        if( _failure( pRes->status ) ) goto fail;
        _free( pRes );
        break;

      case SYS_SAVE:

        camp_snprintf( message, sizeof( message ), "%s <filename> [<flag>]", argv[0] );

        if( argc < 2 ) goto usage;

        fileReq.filename = argv[1];
        if( argc > 2 )
        {
            fileReq.flag = strtol( argv[2], &endptr, 0 );
            if( isgraph( *endptr ) ) goto usage;
        }
        else
        {
            fileReq.flag = 0;
        }

        pRes = campsrv_syssave_10400( &fileReq, get_current_rqstp() );
        if( _failure( pRes->status ) ) goto fail;
        _free( pRes );
        break;
    
      case SYS_DIR:

        camp_snprintf( message, sizeof( message ), "%s <filespec>", argv[0] );

        if( argc < 2 ) goto usage;

        fileReq.filename = argv[1];

        pDirRes = campsrv_sysdir_10400( &fileReq, get_current_rqstp() );
        if( _failure( pDirRes->status ) ) goto fail;

        // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    DIRENT* pDirEnt;

	    for( pDirEnt = pDirRes->pDir; 
		 pDirEnt != NULL; 
		 pDirEnt = pDirEnt->pNext )
	    {
		// Tcl_AppendResult( interp, pDirEnt->filename, " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pDirEnt->filename );
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;
    
      case SYS_GETDIR:

	  /*
	   *  previously retrieved the contents of pSys->pDir (which were
	   *  the results of the last sysDir)
	   */

	  // Tcl_SetResult( interp, "sysGetDir command retired", TCL_STATIC );
	  _camp_setMsg( "sysGetDir command retired - use glob" );
	  break;

      case SYS_GETINSTYPES:

	  // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    INS_TYPE* pInsType;
	    for( pInsType = pSys->pInsTypes;  
		 pInsType != NULL; 
		 pInsType = pInsType->pNext )
	    {
		// Tcl_AppendResult( interp, pInsType->ident, " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pInsType->ident );
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETALARMACTS:

        // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    ALARM_ACT* pAlarmAct;
	    for( pAlarmAct = pSys->pAlarmActs;  
		 pAlarmAct != NULL; 
		 pAlarmAct = pAlarmAct->pNext )
	    {
		// Tcl_AppendResult( interp, pAlarmAct->ident , " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pAlarmAct->ident );
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETLOGACTS:

	  // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    LOG_ACT* pLogAct;
	    for( pLogAct = pSys->pLogActs;  
		 pLogAct != NULL; 
		 pLogAct = pLogAct->pNext )
	    {
		// Tcl_AppendResult( interp, pLogAct->ident , " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pLogAct->ident );
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETIFTYPES:

	  // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF_t* pIF_t;
	    for( pIF_t = pSys->pIFTypes;  
		 pIF_t != NULL; 
		 pIF_t = pIF_t->pNext )
	    {
		// Tcl_AppendResult( interp, pIF_t->ident , " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pIF_t->ident );
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETIFCONF:

        camp_snprintf( message, sizeof( message ), "%s <typeIdent>", argv[0] );

        if( argc < 2 ) goto usage;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF_t* pIF_t;
	    pIF_t = camp_ifGetpIF_t( argv[1] /* , mutex_lock_sys_check() */ );
	    if( pIF_t == NULL )
	    {
		// Tcl_SetResult( interp, "invalid interface type", TCL_VOLATILE );
                _campTcl_setErrMsg1( "invalid interface type" );
		goto error;
	    }

	    // Tcl_SetResult( interp, pIF_t->conf, TCL_VOLATILE );
	    camp_setMsg( "%s", pIF_t->conf );
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETINSNAMES:

	  // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    CAMP_VAR* pVar;
	    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
	    {
		// Tcl_AppendResult( interp, pVar->core.ident, " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pVar->core.ident );
	    }
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETVARSONPATH:

        camp_snprintf( message, sizeof( message ), "%s <path>", argv[0] );

        if( argc < 2 ) goto usage;

        // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    CAMP_VAR* pVar;
	    pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	    if( pVar == NULL )
	    {
		/* camp_snprintf( message, sizeof( message ), "invalid path: '%s'", argv[1] ); */
		/* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
		_campTcl_setErrMsg1( "invalid path" );
		goto error;
	    }

	    for( pVar = pVar->pChild; pVar != NULL; pVar = pVar->pNext )
	    {
		// Tcl_AppendResult( interp, pVar->core.ident, " ", (char*)NULL );
		camp_appendMsgNoSep( "%s ", pVar->core.ident );
	    }
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETALERTVARS:

	  // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    campTcl_list_each( interp, CAMP_VAR_ATTR_ALERT, pVarList );
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

        break;

      case SYS_GETLOGGEDVARS:

	  // Tcl_SetResult( interp, "", TCL_VOLATILE );

	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    campTcl_list_each_log( interp, pVarList, argc-1, argv+1 );
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

        break;

      case SYS_ADDALARMACT:

        camp_snprintf( message, sizeof( message ), "%s <ident> [<proc>]", argv[0] );

        if( argc < 2 ) goto usage;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    ALARM_ACT** ppAlarmAct;
	    for( ppAlarmAct = &pSys->pAlarmActs; 
		 *ppAlarmAct != NULL; 
		 ppAlarmAct = &((*ppAlarmAct)->pNext) ) 
	    {
		if( streq( (*ppAlarmAct)->ident, argv[1] ) )
		{
		    _camp_appendMsg( "replacing alarm action '%s'", (*ppAlarmAct)->ident ); // non-fatal
		    break;
		}
	    }
	    
	    if( *ppAlarmAct == NULL ) *ppAlarmAct = (ALARM_ACT*)camp_zalloc( sizeof( ALARM_ACT ) );
	    camp_strdup( &(*ppAlarmAct)->ident, argv[1] ); // free and strdup
	    camp_strdup( &(*ppAlarmAct)->proc, ( ( argc > 2 ) ? argv[2] : "" ) ); // free and strdup
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_ADDLOGACT:

        camp_snprintf( message, sizeof( message ), "%s <ident> [<proc>]", argv[0] );

        if( argc < 2 ) goto usage;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    LOG_ACT** ppLogAct;
	    for( ppLogAct = &pSys->pLogActs; 
		 *ppLogAct != NULL; 
		 ppLogAct = &((*ppLogAct)->pNext) ) 
	    {
		if( streq( (*ppLogAct)->ident, argv[1] ) )
		{
		    _camp_appendMsg( "replacing log action '%s'", (*ppLogAct)->ident ); // non-fatal
		    break;
		}
	    }

	    if( *ppLogAct == NULL ) *ppLogAct = (LOG_ACT*)camp_zalloc( sizeof( LOG_ACT ) );
	    camp_strdup( &(*ppLogAct)->ident, argv[1] ); // free and strdup
	    camp_strdup( &(*ppLogAct)->proc, ( ( argc > 2 ) ? argv[2] : "" ) ); // free and strdup
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_ADDIFTYPE:

        camp_snprintf( message, sizeof( message ), "%s <ident> <conf> <defaultDefn>", argv[0] );

        if( argc < 4 ) goto usage;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF_t** ppIF_t;
	    bool_t is_replacement = FALSE;

	    for( ppIF_t = &pSys->pIFTypes; 
		 *ppIF_t != NULL; 
		 ppIF_t = &((*ppIF_t)->pNext) )
	    {
		if( streq( (*ppIF_t)->ident, argv[1] ) )
		{
		    is_replacement = TRUE;
		    _camp_appendMsg( "replacing interface type '%s'", (*ppIF_t)->ident ); // non-fatal
		    break;
		}
	    }

	    if( *ppIF_t == NULL ) *ppIF_t = (CAMP_IF_t*)camp_zalloc( sizeof( CAMP_IF_t ) );
	    camp_strdup( &(*ppIF_t)->ident, argv[1] ); // free and strdup
	    camp_strdup( &(*ppIF_t)->conf, argv[2] ); // free and strdup
	    camp_strdup( &(*ppIF_t)->defaultDefn, argv[3] ); // free and strdup

#if CAMP_MULTITHREADED
	    /* 
	     *  Initialize the mutex 
	     */
	    if( !is_replacement )
	    {
		pthread_mutexattr_t mutex_attr;

		if( pthread_mutexattr_init( &mutex_attr ) != 0 )
		{
		    _camp_log( "error creating interface type mutex attribute" );
		    exit( CAMP_FAILURE );
		}

		if( pthread_mutexattr_settype( &mutex_attr, PTHREAD_MUTEX_RECURSIVE ) != 0 )
		{
		    _camp_log( "error setting interface type mutex attribute" );
		    exit( CAMP_FAILURE );
		}

		if( pthread_mutex_init( &(*ppIF_t)->mutex, &mutex_attr ) != 0 )
		{
		    _camp_log( "error initializing interface type mutex" );
		    exit( CAMP_FAILURE );
		}
                //printf("Created interface type %s mutex at %p\n",argv[1],(*ppIF_t)->mutex );

		if( pthread_mutexattr_destroy( &mutex_attr ) != 0 )
		{
		    _camp_log( "error deleting interface type mutex attribute" );
		    exit( CAMP_FAILURE );
		}
	    }
#endif /* CAMP_MULTITHREADED */

	    if( streq( argv[1], "none" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_NONE;
		(*ppIF_t)->procs.onlineProc = if_none_online;
		(*ppIF_t)->procs.offlineProc = if_none_offline;
		(*ppIF_t)->procs.readProc = if_none_read;
		(*ppIF_t)->procs.writeProc = if_none_write;
		camp_status = CAMP_SUCCESS;
	    }
	    else if( streq( argv[1], "rs232" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_RS232;
		(*ppIF_t)->procs.onlineProc = if_rs232_online;
		(*ppIF_t)->procs.offlineProc = if_rs232_offline;
		(*ppIF_t)->procs.readProc = if_rs232_read;
		(*ppIF_t)->procs.writeProc = if_rs232_write;
		camp_status = if_rs232_init();
	    }
	    else if( streq( argv[1], "gpib" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_GPIB;
		(*ppIF_t)->procs.onlineProc = if_gpib_online;
		(*ppIF_t)->procs.offlineProc = if_gpib_offline;
		(*ppIF_t)->procs.readProc = if_gpib_read;
		(*ppIF_t)->procs.writeProc = if_gpib_write;
		(*ppIF_t)->procs.gpibClearProc = if_gpib_clear;
		(*ppIF_t)->procs.gpibTimeoutProc = if_gpib_timeout;
		camp_status = if_gpib_init();
	    }
#if CAMP_HAVE_GPIB_MSCB
	    else if( streq( argv[1], "gpib_mscb" ) )
	    {
		_camp_log( "adding interface type 'gpib_mscb'" );
		(*ppIF_t)->typeID = CAMP_IF_TYPE_GPIB_MSCB;
		(*ppIF_t)->procs.shutdownProc = if_gpib_mscb_shutdown;
		(*ppIF_t)->procs.onlineProc = if_gpib_mscb_online;
		(*ppIF_t)->procs.offlineProc = if_gpib_mscb_offline;
		(*ppIF_t)->procs.readProc = if_gpib_mscb_read;
		(*ppIF_t)->procs.writeProc = if_gpib_mscb_write;
		(*ppIF_t)->procs.gpibClearProc = if_gpib_mscb_clear;
		(*ppIF_t)->procs.gpibTimeoutProc = if_gpib_mscb_timeout;
		if( is_replacement ) if_gpib_mscb_shutdown();
		camp_status = if_gpib_mscb_init();
	    }
#endif // CAMP_HAVE_GPIB_MSCB
#if CAMP_HAVE_CAMAC
	    else if( streq( argv[1], "camac" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_CAMAC;
		(*ppIF_t)->procs.onlineProc = if_camac_online;
		(*ppIF_t)->procs.offlineProc = if_camac_offline;
		(*ppIF_t)->procs.readProc = if_camac_read;
		(*ppIF_t)->procs.writeProc = if_camac_write;
		camp_status = if_camac_init();
	    }
#endif // CAMP_HAVE_CAMAC
	    else if( streq( argv[1], "vme" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_VME;
		(*ppIF_t)->procs.onlineProc = if_vme_online;
		(*ppIF_t)->procs.offlineProc = if_vme_offline;
		(*ppIF_t)->procs.readProc = if_vme_read;
		(*ppIF_t)->procs.writeProc = if_vme_write;
		camp_status = if_vme_init();
	    }
	    else if( streq( argv[1], "tcpip" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_TCPIP;
		(*ppIF_t)->procs.onlineProc = if_tcpip_online;
		(*ppIF_t)->procs.offlineProc = if_tcpip_offline;
		(*ppIF_t)->procs.readProc = if_tcpip_read;
		(*ppIF_t)->procs.writeProc = if_tcpip_write;
		camp_status = if_tcpip_init();
	    }
	    else if( streq( argv[1], "tics" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_TICS;
		camp_status = CAMP_FAILURE;
	    }
#ifdef VXWORKS
	    else if( streq( argv[1], "indpak" ) )
	    {
		(*ppIF_t)->typeID = CAMP_IF_TYPE_INDPAK;
		(*ppIF_t)->procs.onlineProc = if_indpak_online;
		(*ppIF_t)->procs.offlineProc = if_indpak_offline;
		(*ppIF_t)->procs.readProc = if_indpak_read;
		(*ppIF_t)->procs.writeProc = if_indpak_write;
		camp_status = if_indpak_init();
	    }
#endif // VXWORKS
	    else
	    {
		camp_status = CAMP_INVAL_IF_TYPE;
	    }

	    if( _failure( camp_status ) )
	    {
		_xdr_free( xdr_CAMP_IF_t, *ppIF_t );
		// Tcl_AppendResult( interp, "failed to add iftype ", argv[1], (char*)NULL );
		_camp_appendMsg( "failed to add iftype '%s'", argv[1] ); // non-fatal
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

	if( _failure( camp_status ) )
	{
	    goto error;
	}

        break;

      case SYS_ADDINSTYPE:

        camp_snprintf( message, sizeof( message ), "%s <ident>", argv[0] );

        if( argc < 2 ) goto usage;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    INS_TYPE** ppInsType;
	    for( ppInsType = &pSys->pInsTypes; 
		 *ppInsType != NULL; 
		 ppInsType = &((*ppInsType)->pNext) )
	    {
		if( streq( (*ppInsType)->ident, argv[1] ) )
		{
		    _camp_appendMsg( "replacing instrument type '%s'", (*ppInsType)->ident ); // non-fatal
		    break;
		}
	    }

	    /*
	     *  Only Tcl drivers
	     */
	    if( *ppInsType == NULL ) *ppInsType = (INS_TYPE*)camp_zalloc( sizeof( INS_TYPE ) );
	    camp_strdup( &(*ppInsType)->ident, argv[1] ); // free and strdup
	    /*
	     *  Important to set driverType - checked in campsrv_insadd
	     */
	    camp_strdup( &(*ppInsType)->driverType, "Tcl" ); // free and strdup
	    (*ppInsType)->procs.initProc = ins_tcl_init;
	    (*ppInsType)->procs.deleteProc = ins_tcl_delete;
	    (*ppInsType)->procs.onlineProc = ins_tcl_online;
	    (*ppInsType)->procs.offlineProc = ins_tcl_offline;
	    (*ppInsType)->procs.setProc = ins_tcl_set;
	    (*ppInsType)->procs.readProc = ins_tcl_read;
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_ADDINSAVAIL:

        camp_snprintf( message, sizeof( message ), "%s <type> <ident> [<initProc>]", argv[0] );

        if( argc < 3 ) goto usage;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    INS_AVAIL** ppInsAvail;
	    for( ppInsAvail = &pSys->pInsAvail; 
		 *ppInsAvail != NULL;
		 ppInsAvail = &(*ppInsAvail)->pNext )
	    {
		if( streq( (*ppInsAvail)->ident, argv[2] ) )
		{
		    _camp_appendMsg( "replacing InsAvail '%s'", (*ppInsAvail)->ident ); // non-fatal
		    break;
		}
	    }

	    if( *ppInsAvail == NULL ) *ppInsAvail = (INS_AVAIL*)camp_zalloc( sizeof( INS_AVAIL ) );
	    camp_strdup( &(*ppInsAvail)->typeIdent, argv[1] ); // free and strdup
	    camp_strdup( &(*ppInsAvail)->ident, argv[2] ); // free and strdup
	    camp_strdup( &(*ppInsAvail)->specInitProc, ( ( argc > 3 ) ? argv[3] : "" ) ); 
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock

        break;

      case SYS_SETDEBUG:

        camp_snprintf( message, sizeof( message ), "%s <debug mask>", argv[0] );

        if( argc < 2 ) goto usage;

	{
            unsigned int camp_debug_old = camp_debug;
            unsigned int debug_mask;
	    int num_scanned;

	    num_scanned = sscanf( argv[1], "%x", &debug_mask ); // debug_mask = strtol( argv[1], &endptr, 0 );
            if( num_scanned < 1 )  goto usage; // isgraph( *endptr ) )

	    camp_debug = debug_mask;

	    _camp_log( "camp_debug set to 0x%08X (previous value 0x%08X)", camp_debug, camp_debug_old );
	}

        break;

      case SYS_GETDEBUG:
	  
	camp_setMsg( "0x%08x", camp_debug );

        break;
    }

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    _free( pRes );
    _xdr_free( xdr_DIR_res, pDirRes ); // _free( pDirRes );
    // _mutex_lock_end();
    return( TCL_OK );

usage:
    usage( message );
    goto error;

fail:
    _camp_appendMsg( "failed %s", argv[0] );
    _free( pRes );
    goto error;

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    _free( pRes );
    _xdr_free( xdr_DIR_res, pDirRes ); // _free( pDirRes );
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_ins
 *
 *  Purpose:    Process "ins*" Tcl commands
 *
 *  Provides:   
 *              insAdd
 *              insDel
 *              insSet
 *              insLoad
 *              insSave
 *              insIfOn
 *              insIfOff
 *              insIfRead
 *              insIfWrite
 *              insIfReadVerify
 *              insIfWriteVerify
 *              insIfDump
 *              insIfUndump
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *    19-Jan-2001   DA   insIfDump & insIfUndump
 *    20140505      TW   better usage messages for all ( i >= argc ) cases
 */
int
camp_tcl_ins( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    RES* pRes;
    INS_ADD_req insAddReq;
    INS_LOCK_req insLockReq;
    INS_LINE_req insLineReq;
    INS_IF_req insIfReq;
    DATA_req dataReq;
    INS_FILE_req insFileReq;
    INS_READ_req insReadReq;
    INS_WRITE_req insWriteReq;
    INS_DUMP_req insDumpReq;
    INS_UNDUMP_req insUndumpReq;
    TOKEN tok;
    char* endptr;
    int camp_status = CAMP_SUCCESS;
    int tcl_status = TCL_OK;
    int tries = 0;
    int max_tries = 1;
    char* fmt;
    char* argv2[64];
    bool_t done;
    int i, j, n;
    double f;
    double tol;
    char* val;
    u_char uc;
    char buf[LEN_BUF+1];
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    switch( (INS_PROCS)clientData )
    {
      case INS_ADD:

        if( argc < 3 )
        {
	    camp_snprintf( message, sizeof( message ), "%s <type> <ident>", argv[0] );
            goto usage;
        }

        insAddReq.typeIdent = argv[1];
        insAddReq.ident = argv[2];

        pRes = campsrv_insadd_10400( &insAddReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            // Tcl_SetResult( interp, "failed insAdd", TCL_STATIC );
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insadd" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
        break;

      case INS_SET:

        if( argc < 2 ) 
        {
	    camp_snprintf( message, sizeof( message ), "%s <ins>", argv[0] );
            goto usage;
        }
        
	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    CAMP_VAR* pVar;
	    CAMP_INSTRUMENT* pIns;

	    pVar = camp_varGetp( argv[1] );
	    if( pVar == NULL ) 
	    {
		_campTcl_setErrMsg1( "invalid path" );
		goto error;
	    }

	    pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	    for( i = 2; i < argc; i++ )
	    {
		if( !findToken( argv[i], &tok ) )
		{
		    _campTcl_setErrMsg2( "invalid token", argv[i] );
		    goto error;
		}

		switch( tok.kind )
		{
		case TOK_OPT_LOCK:

		    if( ++i >= argc )
		    {
			camp_snprintf( message, sizeof( message ), "%s <ins> -lock <on|off>", argv[0] );
			goto usage;
		    }

		    if( !findToken( argv[i], &tok ) || 
			( ( tok.kind != TOK_ON ) && ( tok.kind != TOK_OFF ) ) )
		    {
		        _campTcl_setErrMsg3( "invalid on/off", argv[i-1], argv[i] );
			goto error;
		    }

		    insLockReq.dreq.path = argv[1];
		    insLockReq.flag = ( tok.kind == TOK_ON );
		    pRes = campsrv_inslock_10400( &insLockReq, get_current_rqstp() );
		    if( _failure( pRes->status ) )
		    {
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_inslock" ); }
			_free( pRes );
			goto error;
		    }
		    _free( pRes );
		    break;

		case TOK_OPT_LINE:

		    if( ++i >= argc )
		    {
			camp_snprintf( message, sizeof( message ), "%s <ins> -line <on|off>", argv[0] );
			goto usage;
		    }

		    if( !findToken( argv[i], &tok ) || 
			( ( tok.kind != TOK_ON ) && ( tok.kind != TOK_OFF ) ) )
		    {
		        _campTcl_setErrMsg3( "invalid on/off", argv[i-1], argv[i] );
			goto error;
		    }

		    insLineReq.dreq.path = argv[1];
		    insLineReq.flag = ( tok.kind == TOK_ON );
		    pRes = campsrv_insline_10400( &insLineReq, get_current_rqstp() );
		    if( _failure( pRes->status ) )
		    {
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insline" ); }
			_free( pRes );
			goto error;
		    }
		    _free( pRes );
		    break;

		case TOK_OPT_ONLINE:

		    insLineReq.dreq.path = argv[1];
		    insLineReq.flag = 1;
		    pRes = campsrv_insline_10400( &insLineReq, get_current_rqstp() );
		    if( _failure( pRes->status ) )
		    {
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insline( online )" ); }
			_free( pRes );
			goto error;
		    }
		    _free( pRes );
		    break;

		case TOK_OPT_OFFLINE:

		    insLineReq.dreq.path = argv[1];
		    insLineReq.flag = 0;
		    pRes = campsrv_insline_10400( &insLineReq, get_current_rqstp() );
		    if( _failure( pRes->status ) )
		    {
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insline( offline )" ); }
			_free( pRes );
			goto error;
		    }
		    _free( pRes );
		    break;

		case TOK_OPT_IF_SET:

		    camp_snprintf( message, sizeof( message ), "%s <ins> %s <ifType> <accessDelay> <timeout>", argv[0], argv[i] );

		    if( i+3 >= argc ) goto usage;

		    bzero( (char*)&insIfReq, sizeof( INS_IF_req ) );

		    insIfReq.dreq.path = argv[1]; // note: used locally, no need to camp_malloc string

		    ++i;

		    insIfReq.IF.typeIdent = argv[i]; // note: used locally, no need to camp_malloc string

		    { // localize if_typeID
		    CAMP_IF_TYPE if_typeID = CAMP_IF_TYPE_NONE;
		    
		    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
		    {
			CAMP_IF_t* pIF_t;
			pIF_t = camp_ifGetpIF_t( insIfReq.IF.typeIdent );
			if( pIF_t == NULL ) 
			{
			    _campTcl_setErrMsg3( "invalid interface type", argv[i-1], argv[i] );
			    goto error;
			}
			if_typeID = pIF_t->typeID;
		    }
		    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

		    ++i;

		    insIfReq.IF.accessDelay = (float)strtod( argv[i], &endptr );
		    if( isgraph( *endptr ) ) goto usage;

		    ++i;

		    insIfReq.IF.timeout = (float)strtod( argv[i], &endptr );
		    if( isgraph( *endptr ) ) goto usage;

		    n = 0;
		    switch( if_typeID )
		    {
		    case CAMP_IF_TYPE_NONE:
			n = 0;
			break;

		    case CAMP_IF_TYPE_RS232:
			camp_strncat( message, sizeof( message ), " <port> <baud> <databits> <parity> <stopbits> <readTerminator> <writeTerminator>" );
			n = 7;
			break;

		    case CAMP_IF_TYPE_GPIB:
			camp_strncat( message, sizeof( message ), " <address> <readTerminator> <writeTerminator>" );
			n = 3;
			break;

		    case CAMP_IF_TYPE_GPIB_MSCB:
			camp_strncat( message, sizeof( message ), " <address> <readTerminator> <writeTerminator> <mscbAddr>" );
			n = 4;
			break;

		    case CAMP_IF_TYPE_CAMAC:
			camp_strncat( message, sizeof( message ), " <Branch> <Crate> <Num>" );
			n = 3;
			break;

		    case CAMP_IF_TYPE_VME:
			camp_strncat( message, sizeof( message ), " <base>" );
			n = 1;
			break;

		    case CAMP_IF_TYPE_TCPIP:
			camp_strncat( message, sizeof( message ), " <ipAddress> <portNumber> <readTerminator> <writeTerminator>" );
			n = 4;
			break;

		    case CAMP_IF_TYPE_INDPAK:
			camp_strncat( message, sizeof( message ), " <slot> <type> <channel>" );
			n = 3;
			break;

		    default:
			break;
		    }
		    } // localize if_typeID

		    if( i+n >= argc ) // 20140224  TW 
		    {
			goto usage;
		    }

		    buf[0] = '\0';

		    for( j = 0; ( j < n ) && ( ++i < argc ); j++ )
		    {
			camp_strncat( buf, LEN_BUF+1, argv[i] ); 
			camp_strncat( buf, LEN_BUF+1, " " ); 
		    }

		    {
			int len = strlen(buf);
			if( ( len > 0 ) && ( buf[len-1] == ' ' ) ) 
			{
			    buf[len-1] = '\0'; // trim trailing space
			}
		    }

		    insIfReq.IF.defn = buf; // note: used locally, no need to camp_malloc string

		    pRes = campsrv_insifset_10400( &insIfReq, get_current_rqstp() );
		    if( _failure( pRes->status ) )
		    {
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifset" ); }
			_free( pRes );
			goto error;
		    }
		    _free( pRes );

		    break;

		case TOK_OPT_IF_MOD:
		{
		    // enough room for all interface tokens (including tcpip address)
		    // (note that an ipv6 address has up to 39 characters)
		    int if_token_size = 40; // sizeof( if_tokens[0] ); // size of each string
		    int if_tokens_dim = 8; // sizeof( if_tokens ) / if_token_size; // if_tokens array dimension
		    char** if_tokens;
		    int if_tokens_num = 0;

		    camp_snprintf( message, sizeof( message ), "%s <ins> %s", argv[0], argv[i] );

		    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
		    {
			CAMP_IF* pIF = pIns->pIF;

			if( pIF == NULL ) 
			{
			    _camp_setMsg( "interface undefined for instrument '%s'", argv[1] );
			    ++i; // skip over -if_mod, but not over subsequent parameters
			    break;
			}

			bzero( (char*)&insIfReq, sizeof( INS_IF_req ) );

			insIfReq.dreq.path = argv[1]; // note: used locally, no need to camp_malloc string

			insIfReq.IF.typeIdent = pIF->typeIdent; // note: used locally, no need to camp_malloc string

			insIfReq.IF.accessDelay = pIF->accessDelay; /* Sept 29, 2000;  DJA adds: */

			insIfReq.IF.timeout = pIF->timeout; // 20140407  TW 

			if_tokens = (char**)camp_malloc( if_tokens_dim );
			for( j = 0; j < if_tokens_dim; ++j ) { if_tokens[j] = (char*)camp_zalloc( if_token_size ); }

			if_tokens_num = camp_stringSplit( pIF->defn, ' ', if_tokens, if_tokens_dim, if_token_size );

			// todo: test whether if_tokens_num is correct for each IF type (RS232=8, etc.)

			switch( pIF->typeID )
			{
			case CAMP_IF_TYPE_RS232:
			    // RS232:  set: <port> <baud> <databits> <parity> <stopbits> <readTerminator> <writeTerminator> 
			    // RS232:  mod: <port> 
			    if( ++i >= argc )
			    {
				camp_strncat( message, sizeof( message ), " <port>" );
				goto usage;
			    }
		    
			    camp_strncpy( if_tokens[0], if_token_size, argv[i] ); // port 

			    break;

			case CAMP_IF_TYPE_GPIB:
			    // GPIB:  set: <address> <readTerminator> <writeTerminator>
			    // GPIB:  mod: <address>
			    if( ++i >= argc )
			    {
				camp_strncat( message, sizeof( message ), " <address>" );
				goto usage;
			    }

			    camp_strncpy( if_tokens[0], if_token_size, argv[i] ); // address

			    break;

			case CAMP_IF_TYPE_GPIB_MSCB:
			    // GPIB:  set: <address> <readTerminator> <writeTerminator> <mscbAddr>
			    // GPIB:  mod: <address>
			    if( ++i >= argc )
			    {
				camp_strncat( message, sizeof( message ), " <address>" );
				goto usage;
			    }

			    camp_strncpy( if_tokens[0], if_token_size, argv[i] ); // address 

			    break;

			case CAMP_IF_TYPE_CAMAC:
			    // CAMAC:  <Branch> <Crate> <Num>
			    if( i+3 >= argc ) // 20140224  TW 
			    {
				camp_strncat( message, sizeof( message ), " <Branch> <Crate> <Num>" );
				goto usage;
			    }

                            ++i;
			    camp_strncpy( if_tokens[0], if_token_size, argv[i] ); // Branch 

			    ++i;
			    camp_strncpy( if_tokens[1], if_token_size, argv[i] ); // Crate 

			    ++i;
			    camp_strncpy( if_tokens[2], if_token_size, argv[i] ); // Num 

			    break;

			case CAMP_IF_TYPE_VME:
			    // VME:    <base>
			    if( ++i >= argc )
			    {
				camp_strncat( message, sizeof( message ), " <base>" );
				goto usage;
			    }

			    camp_strncpy( if_tokens[0], if_token_size, argv[i] ); // Base 

			    break;

			case CAMP_IF_TYPE_TCPIP:
			    // TCPIP: set: <ipAddress> <portNumber> <readTerminator> <writeTerminator>
			    // TCPIP: mod: <ipAddress> <portNumber>
			    if( i+2 >= argc ) // 20140224  TW 
			    {
				camp_strncat( message, sizeof( message ), " <ipAddress> <portNumber>" );
				goto usage;
			    }

			    ++i;
			    camp_strncpy( if_tokens[0], if_token_size, argv[i] ); // ipAddress 

			    ++i;
			    camp_strncpy( if_tokens[1], if_token_size, argv[i] ); // portNumber 

			    break;

			case CAMP_IF_TYPE_INDPAK: /* Only last param of INDPAK defn can be changed on the fly */
			    // INDPAK: set: <slot> <type> <channel>
			    // INDPAK: mod: <channel>

			    if( ++i >= argc )
			    {
				camp_strncat( message, sizeof( message ), " <channel>" );
				goto usage;
			    }

			    camp_strncpy( if_tokens[2], if_token_size, argv[i] ); // channel

			    break;

			default:
			    break;
			}
		    }
		    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

		    camp_stringJoin( buf, sizeof( buf ), ' ', if_tokens, if_tokens_num ); // 20140317  TW  if_tokens_num

		    for( j = 0; j < if_tokens_dim; ++j ) free( if_tokens[j] );
		    free( if_tokens );

		    insIfReq.IF.defn = buf; // note: used locally, no need to camp_malloc string

		    pRes = campsrv_insifset_10400( &insIfReq, get_current_rqstp() );
		    // _free( insIfReq.IF.typeIdent ); // 20140224  TW  no longer malloc'd
		    // _free( insIfReq.IF.defn ); // 20140224  TW  no longer malloc'd
		    if( _failure( pRes->status ) )
		    {
			// Tcl_SetResult( interp, "insSet -if: failed campsrv_insifset", TCL_STATIC );
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifset" ); }
			_free( pRes );
			goto error;
		    }
		    _free( pRes );
		    break;
		} // case TOK_OPT_IF_MOD

		default:
		    break;
		}
	    }
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock
	break;

      case INS_DEL:

        if( argc < 2 )
        {
	    camp_snprintf( message, sizeof( message ), "%s <ins>", argv[0] );
            goto usage;
        }

        dataReq.path = argv[1]; // note: used locally, no need to camp_malloc string

        pRes = campsrv_insdel_10400( &dataReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insdel" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
        break;

      case INS_LOAD:

        camp_snprintf( message, sizeof( message ), "%s <ins> <filename> [<flag>]", argv[0] );

        if( argc < 3 )
        {
            goto usage;
        }

        insFileReq.dreq.path = argv[1];
        insFileReq.datFile = argv[2];
        if( argc > 3 )
        {
            insFileReq.flag = strtol( argv[3], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                goto usage;
            }
        }
        else
        {
            insFileReq.flag = 0;
        }

        pRes = campsrv_insload_10400( &insFileReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insload" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
        break;

      case INS_SAVE:

        camp_snprintf( message, sizeof( message ), "%s <ins> <filename> [<flag>]", argv[0] );

        if( argc < 3 )
        {
            goto usage;
        }

        insFileReq.dreq.path = argv[1];
        insFileReq.datFile = argv[2];
        if( argc > 3 )
        {
            insFileReq.flag = strtol( argv[3], &endptr, 0 );
            if( isgraph( *endptr ) )
            {
                goto usage;
            }
        }
        else
        {
            insFileReq.flag = 0;
        }

        pRes = campsrv_inssave_10400( &insFileReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_inssave" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
        break;

      case INS_IF_ON:

        camp_snprintf( message, sizeof( message ), "%s <ins>", argv[0] );

        if( argc < 2 )
        {
            goto usage;
        }

        dataReq.path = argv[1];

        pRes = campsrv_insifon_10400( &dataReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifon" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
        break;

      case INS_IF_OFF:

        camp_snprintf( message, sizeof( message ), "%s <ins>", argv[0] );

        if( argc < 2 )
        {
            goto usage;
        }

        dataReq.path = argv[1];

        pRes = campsrv_insifoff_10400( &dataReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifoff" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
        break;

      case INS_IF_READ:

        camp_snprintf( message, sizeof( message ), "%s <ins_path> <cmd> <buf_len>", argv[0] );

        if( argc < 4 )
        {
            goto usage;
        }

        insReadReq.dreq.path = argv[1];
        insReadReq.cmd.cmd_val = argv[2];
        insReadReq.cmd.cmd_len = strlen( insReadReq.cmd.cmd_val );
        insReadReq.buf_len = strtol( argv[3], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            goto usage;
        }

        pRes = campsrv_insifread_10400( &insReadReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {   // Leave the message simple, without context
	    //camp_appendMsgNoSep( " in %s %s", argv[0], argv[1] );
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifread" ); }
            _free( pRes );
            goto error;
        }

        _free( pRes );

        // Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" );

	goto success;

      case INS_IF_WRITE:

        camp_snprintf( message, sizeof( message ), "%s <ins> <cmd>", argv[0] );

        if( argc < 3 )
        {
            goto usage;
        }

        insWriteReq.dreq.path = argv[1];
        insWriteReq.cmd.cmd_val = argv[2];
        insWriteReq.cmd.cmd_len = strlen( insWriteReq.cmd.cmd_val );

        pRes = campsrv_insifwrite_10400( &insWriteReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
	{   // Leave the message simple, without context
	    //camp_appendMsgNoSep( " in %s %s", argv[0], argv[1] );
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifwrite" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
	goto success;

      case INS_IF_DUMP:

        camp_snprintf( message, sizeof( message ), "%s <ins> <filename> <cmd> <buf_len> <echo> ", argv[0] );

        if( argc < 6 )
        {
            goto usage;
        }

        insDumpReq.dreq.path = argv[1];
        insDumpReq.fname.fname_val = argv[2];
        insDumpReq.fname.fname_len = strlen( insDumpReq.fname.fname_val );
        insDumpReq.cmd.cmd_val = argv[3];
        insDumpReq.cmd.cmd_len = strlen( insDumpReq.cmd.cmd_val );
        insDumpReq.skip.skip_val = argv[5];
        insDumpReq.skip.skip_len = strlen( insDumpReq.skip.skip_val );
        insDumpReq.buf_len = strtol( argv[4], &endptr, 0 ) + insDumpReq.skip.skip_len;

        if( isgraph( *endptr ) )
        {
            goto usage;
        }

        pRes = campsrv_insifdump_10400( &insDumpReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            // Tcl_SetResult( interp, "insIfDump: failed campsrv_insifdump", TCL_STATIC );
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifdump" ); }
            free( pRes );
            goto error;
        }

        free( (void*)pRes );

        // Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" );

	goto success;

      case INS_IF_UNDUMP:

        camp_snprintf( message, sizeof( message ), "%s <ins> <filename> <cmd> <buf_len>", argv[0] );

        if( argc < 5 )
        {
            goto usage;
        }

        insUndumpReq.dreq.path = argv[1];
        insUndumpReq.fname.fname_val = argv[2];
        insUndumpReq.fname.fname_len = strlen( insUndumpReq.fname.fname_val );
        insUndumpReq.cmd.cmd_val = argv[3];
        insUndumpReq.cmd.cmd_len = strlen( insUndumpReq.cmd.cmd_val );
        insUndumpReq.buf_len = strtol( argv[4], &endptr, 0 );

        pRes = campsrv_insifundump_10400( &insUndumpReq, get_current_rqstp() );
        if( _failure( pRes->status ) )
        {
            // Tcl_SetResult( interp, "insIfUndump: failed campsrv_insifundump", TCL_STATIC );
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_insifundump" ); }
            _free( pRes );
            goto error;
        }
        _free( pRes );
	goto success;

      case INS_IF_READ_VERIFY:

        camp_snprintf( message, sizeof( message ), 
                 "%s <ins_path> <cmd> <buf_len> <var_path> <fmt> [<max_tries>]", 
                 argv[0] );

        if( argc < 6 || argc > 7 )
        {
            goto usage;
        }
        else if( argc == 6 )
        {
            max_tries = 1;
        }
        else
        {
	    max_tries = strtol( argv[6], &endptr, 0 );
            if( max_tries < 1 || isgraph( *endptr ) )
            {
	        _campTcl_setErrMsg3( "invalid <max_tries> value", "...", argv[6] );
                goto error;
            }
        }

	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    CAMP_VAR* pVar;

	    pVar = camp_varGetp( argv[4] /* , mutex_lock_varlist_check() */ );
	    if( pVar == NULL ) 
	    {
	        _campTcl_setErrMsg3( "invalid var path", "...", argv[4] );
		goto error;
	    }

	    fmt = argv[5];

	    {
		char* bigbuf = (char*)camp_malloc( MAX_LEN_MSG+1 ); // on heap for stack-limited vxworks

		for( tries = 0; tries < max_tries; tries++ )
		{
		    /*
		     *  Issue the read 
		     *
		     *  insIfRead <ins_path> <cmd> <buf_len>
		     */
		    tcl_status = camp_tcl_ins( (ClientData)INS_IF_READ, interp, 4, argv );
		    if( tcl_status == TCL_OK )
		    {
			camp_strncpy( bigbuf, MAX_LEN_MSG+1, interp->result );

			/*
			 *  Parse the result string
			 *
			 *  scan <result of insIfRead> <fmt> <internal_variable>
			 */
			argv2[0] = "scan";
			argv2[1] = bigbuf;
			argv2[2] = fmt;
			argv2[3] = "_camp_tcl_tmp_var_";

			tcl_status = Tcl_ScanCmd( 0, interp, 4, argv2 );
			if( ( tcl_status == TCL_OK ) && streq( interp->result, "1" ) )
			{
			    /*
			     *  Set the value of the variable accordingly
			     *
			     *  varDoSet <var_path> -v <value_internal_variable>
			     */
			    argv2[0] = toktostr( TOK_VAR_DOSET );
			    argv2[1] = pVar->core.path;
			    argv2[2] = toktostr( TOK_VALUE_FLAG );
			    argv2[3] = (char*)Tcl_GetVar( interp, "_camp_tcl_tmp_var_", 0 );

			    tcl_status = camp_tcl_varSet( (ClientData)VAR_DOSET, interp, 4, argv2 );
			    if( tcl_status == TCL_OK )
			    {
				/*
				 *  Set result to the reading
				 */
				// Tcl_SetResult( interp, bigbuf, TCL_VOLATILE ); camp_setMsg( "" );
				
				break;
			    }
			    else
			    {
				_campTcl_setErrMsg3( "failed setting variable", "...", argv[4] );
				if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varDoSet" ); }
			    }
			}
			else
			{
			    //  camp_setMsg so as not to accumulate redundant messages
			    //  buffer sanitized and maybe shortened for error message (note in-place)
                            camp_stoprint_expand( bigbuf, bigbuf, 60 );
			    _camp_setMsg( "failed parsing reading '%s' in %s %s", bigbuf, argv[0], argv[1] );
			}
		    }
		    else // read failed
		    {
		        _campTcl_setErrMsg1( interp->result ); // error message from camp_tcl_ins INS_IF_READ
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed insIfRead" ); }
		    }
		}

		_free( bigbuf );
	    }
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

        if( tries == max_tries && tries > 1 )
        {
	    _camp_appendMsg( "failed %d attempts", tries ); // append to keep the lower-level error
            goto error;
        }

	goto success;

      case INS_IF_WRITE_VERIFY:

        camp_snprintf( message, sizeof( message ), 
                 "%s <ins_path> <w_cmd> <r_cmd> <buf_len> <var_path> <fmt> <max_tries> <val> [<tol>]", 
                 argv[0] );

        if( argc < 9 || argc > 10 )
        {
            goto usage;
        }

	// _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
	{
	    CAMP_VAR* pVar;

	    pVar = camp_varGetp( argv[5] /* , mutex_lock_varlist_check() */ );
	    if( pVar == NULL ) 
	    {
		_campTcl_setErrMsg3( "invalid var path", "...", argv[5] );
		goto error;
	    }

	    max_tries = strtol( argv[7], &endptr, 0 );
            if( max_tries < 1 || isgraph( *endptr ) )
            {
		_campTcl_setErrMsg3( "invalid <max_tries> value", "...", argv[7] );
                goto error;
            }

	    for( tries = 0; tries < max_tries; tries++ )
	    {
		if( !streq( argv[2], "" ) )
		{
		    /*
		     *  Issue the Write command
		     *
		     *  insIfWrite <ins_path> <w_cmd>
		     */
		    //argv2[0] = argv[0]; // toktostr( TOK_INS_IF_WRITE );
		    //argv2[1] = argv[1];
		    //argv2[2] = argv[2];

		    tcl_status = camp_tcl_ins( (ClientData)INS_IF_WRITE, interp, 3, argv ); // argv2

		    if( tcl_status != TCL_OK )
		    {
		        _campTcl_setErrMsg1( interp->result ); // use simple error message from camp_tcl_ins
			if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed insIfWrite" ); }
			continue;
		    }
		}

		/*
		 *  Readback the result
		 *
		 *  insIfRead <ins_path> <r_cmd> <buf_len>
		 */
		argv2[0] = toktostr( TOK_INS_IF_READ );
		argv2[1] = argv[1];
		argv2[2] = argv[3];
		argv2[3] = argv[4];

		tcl_status = camp_tcl_ins( (ClientData)INS_IF_READ, interp, 4, argv2 );

		if( tcl_status != TCL_OK )
		{
		    _campTcl_setErrMsg1( interp->result ); // use simple error message from camp_tcl_ins
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed insIfRead" ); }
		    continue;
		}

		/*
		 *  Parse the result string
		 *
		 *  scan <result of insIfRead> <fmt> <internal_variable>
		 */
		argv2[0] = "scan";
		argv2[1] = strdup( interp->result ); 
		argv2[2] = argv[6];
		argv2[3] = "_camp_tcl_tmp_var_";
		tcl_status = Tcl_ScanCmd( 0, interp, 4, argv2 );

		if( !( ( tcl_status == TCL_OK ) && streq( interp->result, "1" ) ) )
		{
                    // sanitize and possibly shorten result for error message
                    j = strlen( argv2[1] );
                    if (j > 60) { j = 60; }
                    camp_stoprint_expand( argv2[1], argv2[1], j );
		    //  camp_setMsg so as not to accumulate redundant messages
                    _camp_setMsg( "failed parsing reading '%s' in %s %s", argv2[1], argv[0], argv[1] );
                    _free( argv2[1] );
		    continue;
		}
		_free( argv2[1] );

		/*
		 *  Set the value of the variable accordingly
		 *
		 *  varDoSet <var_path> -v <value_internal_variable>
		 */
		argv2[0] = toktostr( TOK_VAR_DOSET );
		argv2[1] = argv[5];
		argv2[2] = toktostr( TOK_VALUE_FLAG );
		argv2[3] = (char*)Tcl_GetVar( interp, "_camp_tcl_tmp_var_", 0 );

		tcl_status = camp_tcl_varSet( (ClientData)VAR_DOSET, interp, 4, argv2 );

		if( tcl_status != TCL_OK )
		{
		    _campTcl_setErrMsg3( "failed setting variable", "...", argv[5] );
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varDoSet" ); }
		    continue;
		}

		/*
		 *  Compare the read value with the target value
		 */
		val = argv[8];

		done = FALSE;
		switch( pVar->core.varType )
		{
		case CAMP_VAR_TYPE_INT:
		    i = strtol( val, &endptr, 0 );
		    if( isgraph( *endptr ) )
		    {
			goto usage;
		    }

		    if( (int)pVar->spec.CAMP_VAR_SPEC_u.pNum->val == i )
		    {
			done = TRUE;
		    }
		    break;

		case CAMP_VAR_TYPE_FLOAT:
		    f = strtod( val, &endptr );
		    if( isgraph( *endptr ) )
		    {
			goto usage;
		    }

		    if( argc > 9 )
		    {
			tol = strtod( argv[9], &endptr );
			if( isgraph( *endptr ) )
			{
			    goto usage;
			}
                        if( tol < 0.0 )
                        {
                            tol = 0.0;
                        }
		    }
		    else
		    {
			tol = 0.0;
		    }

		    if( ( pVar->spec.CAMP_VAR_SPEC_u.pNum->val >= (f-tol) ) &&
			( pVar->spec.CAMP_VAR_SPEC_u.pNum->val <= (f+tol) ) )
		    {
			done = TRUE;
		    }
		    break;

		case CAMP_VAR_TYPE_STRING:
		    if( streq( pVar->spec.CAMP_VAR_SPEC_u.pStr->val, val ) )
		    {
			done = TRUE;
		    }
		    break;

		case CAMP_VAR_TYPE_SELECTION:
		    i = strtol( val, &endptr, 0 );
		    if( isgraph( *endptr ) )
		    {
			/*
			 *  Assume the value was given by its label
			 */
			camp_status = varSelGetLabelID( pVar, val, &uc );
			if( _failure( camp_status ) )
			{
			    goto usage;
			}

			i = (int)uc;
		    }

		    if( pVar->spec.CAMP_VAR_SPEC_u.pSel->val == i )
		    {
			done = TRUE;
		    }
		    break;

		default:
		    break;
		}

		if( done ) goto success;
	    }

            if( tries == max_tries && tries > 1 )
	    {
	        _camp_appendMsg( "failed %d attempts", tries ); // append to keep the lower-level error
	    }
	    goto error;
	}
	// _mutex_lock_varlist_off(); // warning: don't return inside mutex lock
    }

success:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    // _mutex_lock_end();
    return( TCL_OK );

usage:
    usage( message );
    goto error;

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varDef
 *
 *  Purpose:    Process variable definition Tcl commands
 *
 *              Interpret the variable definition command and add the newly
 *              created variable to the CAMP instrument database.
 *              Normally called from a CAMP Tcl instrument driver only.
 *
 *  Provides:   
 *              CAMP_INT
 *              CAMP_FLOAT
 *              CAMP_SELECT
 *              CAMP_STRING
 *              CAMP_ARRAY
 *              CAMP_STRUCT
 *              CAMP_INSTRUMENT
 *              CAMP_LINK
 *        
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_varDef( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    CAMP_VAR* pVar;
    int camp_status = CAMP_SUCCESS;
    int tcl_status = TCL_OK;
    char ident[LEN_IDENT+1];
    // char str[LEN_STR+1];
    char buf[LEN_BUF+1];
    char message[LEN_MSG+1];
    // _mutex_lock_begin();
    extern bool_t camp_ever_had_inst;

    camp_snprintf( message, sizeof( message ), "%s <var> [...]", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    pVar = (CAMP_VAR*)camp_zalloc( sizeof( CAMP_VAR ) );

    switch( (VAR_PROCS)clientData )
    {
      case VAR_INT:

        pVar->core.varType = CAMP_VAR_TYPE_INT;
        pVar->spec.CAMP_VAR_SPEC_u.pNum = 
            (CAMP_NUMERIC*)camp_zalloc( sizeof( CAMP_NUMERIC ) );
        break;

      case VAR_FLOAT:

        pVar->core.varType = CAMP_VAR_TYPE_FLOAT;
        pVar->spec.CAMP_VAR_SPEC_u.pNum = 
            (CAMP_NUMERIC*)camp_zalloc( sizeof( CAMP_NUMERIC ) );
        break;

      case VAR_STRING:

        pVar->core.varType = CAMP_VAR_TYPE_STRING;
        pVar->spec.CAMP_VAR_SPEC_u.pStr = 
            (CAMP_STRING*)camp_zalloc( sizeof( CAMP_STRING ) );
        break;

      case VAR_SELECTION:

        pVar->core.varType = CAMP_VAR_TYPE_SELECTION;
        pVar->spec.CAMP_VAR_SPEC_u.pSel = 
            (CAMP_SELECTION*)camp_zalloc( sizeof( CAMP_SELECTION ) );
        break;

      case VAR_ARRAY:

        pVar->core.varType = CAMP_VAR_TYPE_ARRAY;
        pVar->spec.CAMP_VAR_SPEC_u.pArr = 
            (CAMP_ARRAY*)camp_zalloc( sizeof( CAMP_ARRAY ) );
        break;

      case VAR_STRUCTURE:

        pVar->core.varType = CAMP_VAR_TYPE_STRUCTURE;
        pVar->spec.CAMP_VAR_SPEC_u.pStc = 
            (CAMP_STRUCTURE*)camp_zalloc( sizeof( CAMP_STRUCTURE ) );
        break;

      case VAR_INSTRUMENT:

        pVar->core.varType = CAMP_VAR_TYPE_INSTRUMENT;
        pVar->spec.CAMP_VAR_SPEC_u.pIns = 
            (CAMP_INSTRUMENT*)camp_zalloc( sizeof( CAMP_INSTRUMENT ) );
        break;

      case VAR_LINK:

        pVar->core.varType = CAMP_VAR_TYPE_LINK;
        pVar->spec.CAMP_VAR_SPEC_u.pLnk = 
            (CAMP_LINK*)camp_zalloc( sizeof( CAMP_LINK ) );
        break;

    default:
	break;
    }

    /*
     *  Initializations
     */
    /* Static */
    camp_pathExpand( argv[1], buf, LEN_BUF+1 );
    camp_strdup( &pVar->core.path, buf ); // free and strdup
    camp_pathGetLast( buf, ident, LEN_IDENT+1 );
    camp_strdup( &pVar->core.ident, ident ); // free and strdup
    camp_strdup( &pVar->core.help, "" ); // free and strdup
    camp_strdup( &pVar->core.readProc, "" ); // free and strdup
    camp_strdup( &pVar->core.writeProc, "" ); // free and strdup
    /* Dynamic */
    camp_strdup( &pVar->core.title, "" ); // free and strdup
    camp_strdup( &pVar->core.logAction, "" ); // free and strdup
    camp_strdup( &pVar->core.alarmAction, "" ); // free and strdup
    camp_strdup( &pVar->core.statusMsg, "" ); // free and strdup

    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
    case CAMP_VAR_TYPE_NUMERIC:

        pVar->spec.CAMP_VAR_SPEC_u.pNum->tol = -1.0;
        camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pNum->units, "" ); // free and strdup
        break;

    case CAMP_VAR_TYPE_STRING:

	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pStr->val, "" ); // free and strdup
        break;

    case CAMP_VAR_TYPE_SELECTION:

        break;

    case CAMP_VAR_TYPE_ARRAY:

        break;

    case CAMP_VAR_TYPE_STRUCTURE:

        break;

    case CAMP_VAR_TYPE_INSTRUMENT:

	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->lockHost, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->lockOs, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->defFile, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->iniFile, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->typeIdent, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->initProc, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->deleteProc, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->onlineProc, "" ); // free and strdup
	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->offlineProc, "" ); // free and strdup
        /*
	 *  Create Tcl interpreter private to instrument
	 *  20140313  TW  use an instrument interpreter for single- and multi-threaded
	 *                servers - the Tcl drivers may expect to be in their 
	 *                own interpreter, and may be creating procs and global variables
	 *                that are not unique across all drivers
	 */
        pVar->spec.CAMP_VAR_SPEC_u.pIns->interp = campTcl_CreateInterp();
	// pVar->spec.CAMP_VAR_SPEC_u.pIns->interp = camp_tclInterp(); // use the main interpreter

#if CAMP_MULTITHREADED
	/* 
	 *  Initialize the mutex 
	 */
	{
	    pthread_mutexattr_t mutex_attr;

	    if( pthread_mutexattr_init( &mutex_attr ) != 0 )
	    {
		_camp_log( "error creating instrument mutex attribute" );
		exit( CAMP_FAILURE );
	    }

	    if( pthread_mutexattr_settype( &mutex_attr, PTHREAD_MUTEX_RECURSIVE ) != 0 )
	    {
		_camp_log( "error setting instrument mutex attribute" );
		exit( CAMP_FAILURE );
	    }

	    if( pthread_mutex_init( &(pVar->spec.CAMP_VAR_SPEC_u.pIns->mutex), &mutex_attr ) != 0 )
	    {
		_camp_log( "error initializing instrument mutex" );
		exit( CAMP_FAILURE );
	    }
            //printf("Created instrument %s mutex at %p\n",ident,(pVar->spec.CAMP_VAR_SPEC_u.pIns->mutex) );

	    if( pthread_mutexattr_destroy( &mutex_attr ) != 0 )
	    {
		_camp_log( "error deleting instrument mutex attribute" );
		exit( CAMP_FAILURE );
	    }
	}
#endif /* CAMP_MULTITHREADED */

        camp_ever_had_inst = TRUE;
        break;

      case CAMP_VAR_TYPE_LINK:

	camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pLnk->path, "" ); // free and strdup
	break;
    }

    pVar->spec.varType = pVar->core.varType;

    camp_status = varAdd( argv[1], pVar );

    if( _failure( camp_status ) )
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varAdd" ); }
	goto error;
    }

    tcl_status = camp_tcl_varSet( (ClientData)VAR_DEF, interp, argc, argv );

    camp_setMsg( "%s", interp->result ); // message from camp_tcl_varSet

    if( tcl_status == TCL_ERROR )
    {
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed camp_tcl_varSet( VAR_DEF )" ); }
    }
	    
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // done by camp_tcl_varSet
    // _mutex_lock_end();
    return( tcl_status );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varSet
 *
 *  Purpose:    Process variable setting Tcl commands
 *
 *              Can be called in different contexts.  When called by
 *              camp_tcl_varDef, parameters relating only to variable
 *              creation are in effect.  When called by varDoSet
 *              (or camp_tcl_varDef) parameters normally set only by the
 *              Tcl instrument driver are in effect.  Additionally,
 *              a value set command from "varDoSet" or camp_tcl_varDef
 *              initiates an immediate change of the value in the CAMP
 *              database.  When called with "varSet", however, a value
 *              set command (parameter "-v") initiates the varSetReq
 *              (variable set request) routine which eventually calls the
 *              variable's "-writeProc" in the Tcl driver.  This is also
 *              true when setting certain alarm related parameters.  Thus,
 *              a "varDoSet" is an immediate hard set of the CAMP database,
 *              while a "varSet" is a higher level command requesting
 *              instrument communication.  "varSet -v" is parallel to the
 *              "varRead" command.
 *
 *  Provides:   
 *              varSet
 *              varDoSet
 *              also processes setting parameters passed to camp_tcl_varDef
 *
 *              These commands have many parameters which set many aspects
 *              of CAMP variables, as described in the CAMP programmer's
 *              guide.
 *
 *  Called by:  Tcl interpreter and camp_tcl_varDef
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 *    20140505  TW  usage messages on all ( ++i >= argc ) as in camp_tcl_ins
 *    20161004  DA  Fix missing break on alert case
 */
int
camp_tcl_varSet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int camp_status = CAMP_SUCCESS;
    int tcl_status = TCL_OK;
    CAMP_VAR_SPEC spec;
    TOKEN tok;
    TOKEN tok2;
    int i, j;
    bool_t dopoll;
    bool_t poll_flag;
    float poll_interval;
    bool_t dolog;
    bool_t log_flag;
    char* log_action;
    bool_t doalarm;
    bool_t alarm_flag;
    char* alarm_action;
    bool_t doalert;
    bool_t alert_flag;
    int valArg;
    char* endptr;
    int argc2;
    char** argv2;
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    if( argc < 2 )
    {
        if ( (VAR_PROCS)clientData == VAR_DEF ) 
        {
            camp_snprintf( message, sizeof( message ), "%s <var> <flag> [<value>] ...", argv[0] );
        } 
        else
        {
            camp_snprintf( message, sizeof( message ), "%s <var> <flag> <value> ...", argv[0] );
        }
        goto usage;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_ARRAY* pArr;

	if( ( pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ ) ) == NULL )
	{
	    _campTcl_setErrMsg1( "invalid var" );
	    goto error;
	}

	camp_status = camp_checkInsLock( pVar, get_current_rqstp() );
	if( _failure( camp_status ) ) 
	{
	    _campTcl_setErrMsg1( "instrument locked, cannot set" );
	    goto error;
	}

	/*
	 * Some things (poll, alarm, log, value) depend on multiple switches (-p on -p_int 10;
         * or -v x -selections x y z; or -a on -a_act foo -alert on) so delay processing them 
         * until after the whole command is parsed.  The following variables are used to hold 
         * on to the parameters/values to be done at the end. (NULL pointer for strings means 
         * no setting.)
	 */
	valArg = 0;

	dopoll = FALSE;
	poll_flag = pVar->core.status & CAMP_VAR_ATTR_POLL;
	poll_interval = pVar->core.pollInterval;

	dolog = FALSE;
	log_flag = pVar->core.status & CAMP_VAR_ATTR_LOG;
	log_action = NULL;

	doalarm = FALSE;
	alarm_flag = pVar->core.status & CAMP_VAR_ATTR_ALARM;
	alarm_action = NULL;

	doalert = FALSE;
	alert_flag = pVar->core.status & CAMP_VAR_ATTR_ALERT;

	for( i = 2; i < argc; i++ )
	{
	    bzero( (char*)&tok, sizeof( TOKEN ) );
	    if( !findToken( argv[i], &tok ) )
	    {
		goto invalidtok;
	    }

	    switch( tok.kind )
	    {
	    case TOK_SHOW_ATTR:

		if( (VAR_PROCS)clientData == VAR_DEF )
		{
		    pVar->core.attributes |= CAMP_VAR_ATTR_SHOW;
		}
		break;

	    case TOK_SET_ATTR:

		if( (VAR_PROCS)clientData == VAR_DEF )
		{
		    pVar->core.attributes |= CAMP_VAR_ATTR_SET;
		}
		break;

	    case TOK_READ_ATTR:

		if( (VAR_PROCS)clientData == VAR_DEF )
		{
		    pVar->core.attributes |= CAMP_VAR_ATTR_READ;
		}
		break;

	    case TOK_POLL_ATTR:

		if( (VAR_PROCS)clientData == VAR_DEF )
		{
		    pVar->core.attributes |= CAMP_VAR_ATTR_POLL;
		}
		break;

	    case TOK_LOG_ATTR:

		if( (VAR_PROCS)clientData == VAR_DEF )
		{
		    pVar->core.attributes |= CAMP_VAR_ATTR_LOG;
		}
		break;

	    case TOK_ALARM_ATTR:

		if( (VAR_PROCS)clientData == VAR_DEF )
		{
		    pVar->core.attributes |= CAMP_VAR_ATTR_ALARM;
		}
		break;

	    case TOK_TITLE:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <title>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		camp_strdup( &pVar->core.title, argv[i] ); 
		break;

	    case TOK_HELP:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <help-text>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( (VAR_PROCS)clientData == VAR_DEF ) || ( (VAR_PROCS)clientData == VAR_DOSET ) )
		{
		    camp_strdup( &pVar->core.help, argv[i] ); 
		}
		break;

	    case TOK_INSTYPE:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <insType>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
		    ( (VAR_PROCS)clientData == VAR_DEF ) )
		{
		    camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->typeIdent, argv[i] ); 
		}
		break;

	    case TOK_OPT_INITPROC:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <proc>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
		    ( ( (VAR_PROCS)clientData == VAR_DEF ) || ( (VAR_PROCS)clientData == VAR_DOSET ) ) )
		{
		    camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->initProc, argv[i] ); 
		}
		break;

	    case TOK_OPT_DELETEPROC:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <proc>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
		    ( ( (VAR_PROCS)clientData == VAR_DEF ) || ( (VAR_PROCS)clientData == VAR_DOSET ) ) )
		{
		    camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->deleteProc, argv[i] ); 
		}
		break;

	    case TOK_OPT_ONLINEPROC:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <proc>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
		    ( ( (VAR_PROCS)clientData == VAR_DEF ) || ( (VAR_PROCS)clientData == VAR_DOSET ) ) )
		{
		    camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->onlineProc, argv[i] ); 
		}
		break;

	    case TOK_OPT_OFFLINEPROC:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <proc>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT ) &&
		    ( ( (VAR_PROCS)clientData == VAR_DEF ) || ( (VAR_PROCS)clientData == VAR_DOSET ) ) )
		{
		    camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pIns->offlineProc, argv[i] ); 
		}
		break;

	    case TOK_OPT_READPROC:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <proc>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT ) &&
		    ( ( (VAR_PROCS)clientData == VAR_DEF ) || ( (VAR_PROCS)clientData == VAR_DOSET ) ) )
		{
		    camp_strdup( &pVar->core.readProc, argv[i] ); 
		}
		break;

	    case TOK_OPT_WRITEPROC:
	    case TOK_OPT_SETPROC:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <proc>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT ) &&
		    ( ( (VAR_PROCS)clientData == VAR_DEF ) || ( (VAR_PROCS)clientData == VAR_DOSET ) ) )
		{
		    camp_strdup( &pVar->core.writeProc, argv[i] ); 
		}
		break;

	    case TOK_SELECTIONS:

		if( ( pVar->core.varType == CAMP_VAR_TYPE_SELECTION ) &&
		    ( (VAR_PROCS)clientData == VAR_DEF ) )
		{
		    CAMP_SELECTION_ITEM** ppItem;
		    ppItem = &pVar->spec.CAMP_VAR_SPEC_u.pSel->pItems;
		    while( ( (i+1) < argc ) && ( argv[i+1][0] != '-' ) )
		    {
			i++;
			*ppItem = (CAMP_SELECTION_ITEM*)camp_zalloc( sizeof( CAMP_SELECTION_ITEM ) );
			camp_strdup( &(*ppItem)->label, argv[i] ); 
			pVar->spec.CAMP_VAR_SPEC_u.pSel->num++;
			ppItem = &(*ppItem)->pNext;
		    }
		}
		break;

	    case TOK_ARRAYDEF:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <type> <size> <dim> <list>", argv[0], argv[i] );

		if( ( pVar->core.varType == CAMP_VAR_TYPE_ARRAY ) &&
		    ( (VAR_PROCS)clientData == VAR_DEF ) )
		{
		    pArr = pVar->spec.CAMP_VAR_SPEC_u.pArr;

		    if( ++i >= argc ) 
		    {
			goto usage;
		    }

		    if( !findToken( argv[i], &tok2 ) )
		    {
			// Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
			goto usage;
		    }

		    switch( tok2.kind )
		    {
		    case TOK_INT:
			pArr->varType = CAMP_VAR_TYPE_INT;
			break;
		    case TOK_FLOAT:
			pArr->varType = CAMP_VAR_TYPE_FLOAT;
			break;
		    case TOK_STRING:
			pArr->varType = CAMP_VAR_TYPE_STRING;
			break;
		    default:
			// Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
			goto usage;
		    }

		    if( ++i >= argc ) 
		    {
			goto usage;
		    }

		    pArr->elemSize = strtoul( argv[i], &endptr, 0 );
		    if( isgraph( *endptr ) )
		    {
			// Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
			goto usage;
		    }

		    if( ++i >= argc ) 
		    {
			goto usage;
		    }

		    pArr->dim = strtoul( argv[i], &endptr, 0 );
		    if( isgraph( *endptr ) )
		    {
			// Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
			goto usage;
		    }

		    if( pArr->dim > MAX_ARRAY_DIM )
		    {
			// Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
			goto usage;
		    }

		    if( ++i >= argc ) 
		    {
			goto usage;
		    }

		    tcl_status = Tcl_SplitList( interp, argv[i], &argc2, &argv2 );
		    if( tcl_status == TCL_ERROR )
		    {
			// Tcl_SetResult( interp, "invalid arrayDef list", TCL_STATIC );
			goto usage;
		    }

		    if( pArr->dim != argc2 )
		    {
			free( argv2 );
			// Tcl_SetResult( interp, "conflicting array dimension parameters", TCL_STATIC );
			_camp_setMsg( "in %s %s: conflicting array dimension parameters (%d %d)", argv[0], argv[1], pArr->dim, argc2 );
			goto error;
		    }

		    pArr->totElem = 1;
		    for( j = 0; j < pArr->dim; j++ )
		    {
		        if( Tcl_GetInt( interp, argv2[j], (int*)&(pArr->dimSize[j]) ) == TCL_ERROR )
			{
			    free( argv2 );
			    goto usage;
			}

			pArr->totElem *= pArr->dimSize[j];
		    }

		    free( argv2 );

		    pArr->pVal = (caddr_t)camp_zalloc( pArr->elemSize*pArr->totElem );
		}
		else
		{
		    // Tcl_SetResult( interp, "invalid arrayDef format", TCL_STATIC );
		    goto usage;
		}
		break;

	    case TOK_VALUE_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <value>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		/*
		 *  Postpone setting value, to handle cases like "CAMP_SELECT -v 2 -selections foo bar"
		 */
		valArg = i;
		break;

	    case TOK_ALARM_TOL:

		if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != CAMP_VAR_TYPE_NUMERIC ) 
		{
		    goto notnum;
		}

		camp_snprintf( message, sizeof( message ), "%s <var> %s <tolerance>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		bzero( (char*)&spec, sizeof( CAMP_VAR_SPEC ) );
		spec.varType = pVar->core.varType;
		spec.CAMP_VAR_SPEC_u.pNum = (CAMP_NUMERIC*)camp_zalloc( sizeof( CAMP_NUMERIC ) );
		bcopy( (char*)pVar->spec.CAMP_VAR_SPEC_u.pNum, (char*)spec.CAMP_VAR_SPEC_u.pNum, sizeof( CAMP_NUMERIC ) );
		spec.CAMP_VAR_SPEC_u.pNum->tol = strtod( argv[i], &endptr );
		if( isgraph( *endptr ) )
		{
		    free( spec.CAMP_VAR_SPEC_u.pNum );
		    _campTcl_setErrMsg3( "invalid alarm tolerance", argv[i-1], argv[i] );
		    goto error;
		}

		if( ( (VAR_PROCS)clientData == VAR_DOSET ) || ( (VAR_PROCS)clientData == VAR_DEF ) )
		{
		    camp_status = varSetSpec( pVar, &spec );
		}
		else
		{
		    camp_status = varSetReq( pVar, &spec );
		}

		_free( spec.CAMP_VAR_SPEC_u.pNum ); // not: xdr_free( xdr_CAMP_VAR_SPEC, (char*)&spec );

		// if_failure_status_goto_error( camp_status );

		if( _failure( camp_status ) ) 
		{ 
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetSpec/Req" ); }
		    goto error; 
		}

		break;

	    case TOK_ALARM_TOLTYPE:

		if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != CAMP_VAR_TYPE_NUMERIC ) 
		{
		    goto notnum;
		}

		camp_snprintf( message, sizeof( message ), "%s <var> %s <tolType>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		bzero( (char*)&spec, sizeof( CAMP_VAR_SPEC ) );
		spec.varType = pVar->core.varType;
		spec.CAMP_VAR_SPEC_u.pNum = (CAMP_NUMERIC*)camp_zalloc( sizeof( CAMP_NUMERIC ) );
		bcopy( (char*)pVar->spec.CAMP_VAR_SPEC_u.pNum, (char*)spec.CAMP_VAR_SPEC_u.pNum, 
		       sizeof( CAMP_NUMERIC ) );

		spec.CAMP_VAR_SPEC_u.pNum->tolType = strtol( argv[i], &endptr, 0 );
		if( isgraph( *endptr ) )
		{
		    _free( spec.CAMP_VAR_SPEC_u.pNum );
		    // Tcl_SetResult( interp, "invalid alarm tolerance type", TCL_STATIC );		    
		    _campTcl_setErrMsg3( "invalid alarm tolerance type", argv[i-1], argv[i] );
		    goto error;
		}

		if( ( (VAR_PROCS)clientData == VAR_DOSET ) || ( (VAR_PROCS)clientData == VAR_DEF ) )
		{
		    camp_status = varSetSpec( pVar, &spec );
		}
		else
		{
		    camp_status = varSetReq( pVar, &spec );
		}

		_free( spec.CAMP_VAR_SPEC_u.pNum ); // not: xdr_free( xdr_CAMP_VAR_SPEC, (char*)&spec );

		// if_failure_status_goto_error( camp_status );

		if( _failure( camp_status ) ) 
		{ 
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetSpec/Req" ); }
		    goto error; 
		}

		break;

	    case TOK_UNITS:

		if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != CAMP_VAR_TYPE_NUMERIC ) 
		{
		    goto notnum;
		}

		camp_snprintf( message, sizeof( message ), "%s <var> %s <units>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		camp_strdup( &pVar->spec.CAMP_VAR_SPEC_u.pNum->units, argv[i] ); 

		break;

	    case TOK_SHOW_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <on|off>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( (VAR_PROCS)clientData != VAR_DEF ) && ( (VAR_PROCS)clientData != VAR_DOSET ) )
		{
		    break;
		}

		if( !findToken( argv[i], &tok ) )
		{
		    goto invalidtok;
		}

		camp_status = varSetShow( pVar, ( ( tok.kind == TOK_ON ) ? TRUE : FALSE ) );

		// if_failure_status_goto_error( camp_status );

		if( _failure( camp_status ) ) 
		{ 
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetShow" ); }
		    goto error; 
		}

		break;

	    case TOK_SET_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <on|off>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( (VAR_PROCS)clientData != VAR_DEF ) && ( (VAR_PROCS)clientData != VAR_DOSET ) )
		{
		    break;
		}

		if( !findToken( argv[i], &tok ) )
		{
		    goto invalidtok;
		}

		camp_status = varSetSet( pVar, ( ( tok.kind == TOK_ON ) ? TRUE : FALSE ) );

		// if_failure_status_goto_error( camp_status );

		if( _failure( camp_status ) ) 
		{ 
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetSet" ); }
		    goto error; 
		}

		break;

	    case TOK_READ_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <on|off>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( (VAR_PROCS)clientData != VAR_DEF ) && ( (VAR_PROCS)clientData != VAR_DOSET ) )
		{
		    break;
		}

		if( !findToken( argv[i], &tok ) )
		{
		    goto invalidtok;
		}

		camp_status = varSetRead( pVar, ( ( tok.kind == TOK_ON ) ? TRUE : FALSE ) );

		// if_failure_status_goto_error( camp_status );

		if( _failure( camp_status ) ) 
		{ 
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetRead" ); }
		    goto error; 
		}

		break;

	    case TOK_LOG_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <on|off>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( !findToken( argv[i], &tok ) )
		{
		    goto invalidtok;
		}

		log_flag = ( tok.kind == TOK_ON ) ? TRUE : FALSE;
		dolog = TRUE;
		break;

	    case TOK_LOG_ACTION:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <logAction>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		log_action = argv[i];
		dolog = TRUE;
		break;

	    case TOK_POLL_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <on|off>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( !findToken( argv[i], &tok ) )
		{
		    goto invalidtok;
		}

		poll_flag = ( tok.kind == TOK_ON ) ? TRUE : FALSE;
		dopoll = TRUE;
		break;

	    case TOK_POLL_INTERVAL:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <interval>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		poll_interval = (float)strtod( argv[i], &endptr );
		if( isgraph( *endptr ) )
		{
		    _campTcl_setErrMsg3( "invalid poll interval", argv[i-1], argv[i] );
		    goto error;
		}

		dopoll = TRUE;
		break;

	    case TOK_ALARM_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <on|off>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( !findToken( argv[i], &tok ) )
		{
		    goto invalidtok;
		}

		alarm_flag = ( tok.kind == TOK_ON ) ? TRUE : FALSE;
		doalarm = TRUE;
		break;

	    case TOK_ALARM_ACTION:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <alarmAction>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		alarm_action = argv[i];
		doalarm = TRUE;
		break;

	    case TOK_ZERO_FLAG:

		camp_status = varZeroStats( pVar );

		// if_failure_status_goto_error( camp_status );

		if( _failure( camp_status ) ) 
		{ 
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varZeroStats" ); }
		    goto error; 
		}

		break;

	    case TOK_ALERT_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <on|off>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( (VAR_PROCS)clientData != VAR_DEF ) && ( (VAR_PROCS)clientData != VAR_DOSET ) )
		{
		    break;
		}

		if( !findToken( argv[i], &tok ) )
		{
		    goto invalidtok;
		}

		alert_flag = ( tok.kind == TOK_ON ) ? TRUE : FALSE;
		doalert = TRUE;

                break;

	    case TOK_MSG_FLAG:

		camp_snprintf( message, sizeof( message ), "%s <var> %s <message>", argv[0], argv[i] );

		if( ++i >= argc ) 
		{
		    goto usage;
		}

		if( ( (VAR_PROCS)clientData != VAR_DEF ) && ( (VAR_PROCS)clientData != VAR_DOSET ) )
		{
		    break;
		}

		camp_status = varSetMsg( pVar, argv[i] );

		// if_failure_status_goto_error( camp_status );

		if( _failure( camp_status ) ) 
		{ 
		    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetMsg" ); }
		    goto error; 
		}

		break;

	    default:
		break;
	    }
	}

	/* 
	 * Now the whole command is parsed and we perform the settings that we delayed
	 */
	/*
	 * Perform value setting at last (value index saved in valArg)
	 */
	if( valArg > 0 )
	{
	    i = valArg;
	    bzero( (char*)&spec, sizeof( CAMP_VAR_SPEC ) );
	    spec.varType = pVar->core.varType;

	    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
	    {
	    case CAMP_VAR_TYPE_NUMERIC:
		spec.CAMP_VAR_SPEC_u.pNum = 
		    (CAMP_NUMERIC*)camp_zalloc( sizeof( CAMP_NUMERIC ) );
		bcopy( (char*)pVar->spec.CAMP_VAR_SPEC_u.pNum, (char*)spec.CAMP_VAR_SPEC_u.pNum, 
		       sizeof( CAMP_NUMERIC ) );

		spec.CAMP_VAR_SPEC_u.pNum->val = strtod( argv[i], &endptr );
		if( isgraph( *endptr ) )
		{
		    free( spec.CAMP_VAR_SPEC_u.pNum );
		    _campTcl_setErrMsg3( "invalid numeric value", argv[i-1], argv[i] );
		    goto error;
		}
		break;

	    case CAMP_VAR_TYPE_SELECTION:
		spec.CAMP_VAR_SPEC_u.pSel = 
		    (CAMP_SELECTION*)camp_zalloc( sizeof( CAMP_SELECTION ) );
		spec.CAMP_VAR_SPEC_u.pSel->val = 
		    (u_char)strtol( argv[i], &endptr, 0 );
		if( isgraph( *endptr ) )
		{
		    /*
		     *  Assume the value was given by its label
		     */
		    camp_status = varSelGetLabelID( pVar, argv[i], &spec.CAMP_VAR_SPEC_u.pSel->val );
		    if( _failure( camp_status ) )
		    {
			_campTcl_setErrMsg3( "invalid selection value", argv[i-1], argv[i] );
			goto error;
		    }
		}
		break;

	    case CAMP_VAR_TYPE_STRING:
		spec.CAMP_VAR_SPEC_u.pStr = 
		    (CAMP_STRING*)camp_zalloc( sizeof( CAMP_STRING ) );
		spec.CAMP_VAR_SPEC_u.pStr->val = argv[i];
		break;

	    default:
		break;
	    }

	    if( ( (VAR_PROCS)clientData == VAR_DOSET ) || ( (VAR_PROCS)clientData == VAR_DEF ) )
	    {
		camp_status = varSetSpec( pVar, &spec );
	    }
	    else
	    {
		camp_status = varSetReq( pVar, &spec );
	    }

	    _free( spec.CAMP_VAR_SPEC_u.pNum );

	    // if_failure_status_goto_error( camp_status );

	    if( _failure( camp_status ) ) 
	    { 
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed varSetSpec/Req" ); }
		goto error; 
	    }
	}

	/* 
	 * perform poll setting
	 */
	if( dopoll )
	{
	    camp_status = varSetPoll( pVar, poll_flag, poll_interval );

	    if( _failure( camp_status ) ) 
	    {
		_campTcl_setErrMsg1( "invalid poll setting" );
		goto error;
	    }
	}

	/*
	 * perform alarm setting
	 */
	if( doalarm )
	{
	    camp_status = varSetAlarm( pVar, alarm_flag, alarm_action );

	    if( _failure( camp_status ) ) 
	    {
		_campTcl_setErrMsg1( "invalid alarm setting" );
		goto error;
	    }
	}

	/*
	 * perform alert setting after any alarm settings
	 */
	if( doalert )
	{
	    camp_status = varSetAlert( pVar, alert_flag );

	    if( _failure( camp_status ) ) 
	    {
		_campTcl_setErrMsg1( "invalid alert setting" );
		goto error;
	    }
	}

	/*
	 * perform log setting
	 */
	if( dolog )
	{
	    camp_status = varSetLog( pVar, log_flag, log_action );

	    if( _failure( camp_status ) ) 
	    {
		_campTcl_setErrMsg1( "invalid log setting" );
		goto error;
	    }
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    // _mutex_lock_end();
    return( TCL_OK );

    /*
     *  Common processing for several instances of "invalid token" error
     */
 notnum: 
    _campTcl_setErrMsg2( "not a numeric variable", argv[i] );
    goto error;

 invalidtok:
    _campTcl_setErrMsg2( "invalid token", argv[i] );
    goto error;

 usage:
    usage ( message );
    goto error;

 error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varRead
 *
 *  Purpose:    Process variable read Tcl command
 *
 *              This routine calls the RPC entry point campsrv_varread,
 *              thereby making a high level request to read the variable
 *              value from an instrument.  This eventually calls the
 *              variable's "-readProc" in the Tcl driver.
 *
 *  Provides:   
 *              varRead
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_varRead( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    DATA_req dataReq;
    RES* pRes = NULL;
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    if( argc < 2 )
    {
	camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );
        usage( message );
        goto error;
    }

    dataReq.path = argv[1];
    pRes = campsrv_varread_10400( &dataReq, get_current_rqstp() );
    if( _failure( pRes->status ) )
    {
	camp_appendMsgNoSep( " in %s %s", argv[0], argv[1] );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_varread" ); }
        _free( pRes );
        goto error;
    }
    _free( pRes );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); // tcl takes over camp message
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varTest
 *
 *  Purpose:    Process variable test Tcl command
 *
 *              For Numeric variable types only this routine tests whether
 *              a variable is within some tolerance or whether it is in
 *              alert state.  These functions relate to CAMP alarms.
 *
 *  Provides:   
 *              varTestTol
 *              varTestAlert
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 * 
 *              The Tcl result string is a boolean integer indicating status.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varTest( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    double val;
    bool_t status = TRUE;
    long int n;
    char message[LEN_MSG+1];
    char* endptr;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var> <val>", argv[0] );

    if( argc < 3 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;

	pVar = camp_varGetp( argv[1] );
	if( pVar == NULL )
	{
	    _campTcl_setErrMsg1( "invalid path" );
	    goto error;
	}

	switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
	{
	case CAMP_VAR_TYPE_NUMERIC: 
	    val = strtod( argv[2], &endptr );
	    if( isgraph( *endptr ) )
	    {
		usage( message );
		goto error;
	    }

	    switch( (VAR_UTILPROCS)clientData )
	    {
	    case VAR_TESTTOL:
		status = camp_varNumTestTol( argv[1], val );
		break;
	    case VAR_TESTALERT:
		status = varNumTestAlert( pVar, val );
		break;
	    default:
		status = TRUE;
		break;
	    }
	    break;

	case CAMP_VAR_TYPE_SELECTION:

	    n = strtol( argv[2], &endptr, 10 );
	    if( isgraph( *endptr ) )
	    {
		usage( message );
		goto error;
	    }
	    val = (double)n;
        
	    switch( (VAR_UTILPROCS)clientData )
	    {
	    case VAR_TESTTOL:
		status = camp_varNumTestTol( argv[1], val );
		break;
	    case VAR_TESTALERT:
		status = varNumTestAlert( pVar, val );
		break;
	    default:
		status = TRUE;
		break;
	    }
	    break;

	default:
	    status = TRUE;
	    break;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    /* if( status )  */
    /* { */
    /*     Tcl_SetResult( interp, camp_itoa( 1, message, sizeof( message ) ), TCL_VOLATILE ); */
    /* } */
    /* else */
    /* { */
    /*     Tcl_SetResult( interp, camp_itoa( 0, message, sizeof( message ) ), TCL_VOLATILE ); */
    /* } */
    camp_setMsg( "%d", status );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_lnkSet
 *
 *  Purpose:    Process Link variable set Tcl command
 *
 *              For Link variable types only this routine sets the path
 *              of a link variable.  The command "varSet -v" for a link
 *              variable will in contrast initiate a setting of the variable
 *              that the link "points to".
 *
 *  Provides:   
 *              lnkSet
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_lnkSet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    DATA_SET_req dataSetReq;
    RES* pRes = NULL;
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var> <val>", argv[0] );

    if( argc < 3 )
    {
        usage( message );
        goto error;
    }

    dataSetReq.dreq.path = argv[1];
    dataSetReq.spec.varType = CAMP_VAR_TYPE_LINK;
    dataSetReq.spec.CAMP_VAR_SPEC_u.pLnk = (CAMP_LINK*)camp_zalloc( sizeof( CAMP_LINK ) );
    dataSetReq.spec.CAMP_VAR_SPEC_u.pLnk->path = argv[2];

    pRes = campsrv_varlnkset_10400( &dataSetReq, get_current_rqstp() );
    if( _failure( pRes->status ) )
    {
	// Tcl_SetResult( interp, "failed campsrv_varlnkset", TCL_STATIC );
	if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed campsrv_varlnkset" ); }
        _free( pRes );
        _free( dataSetReq.spec.CAMP_VAR_SPEC_u.pLnk );
        goto error;
    }
    _free( pRes );

    _free( dataSetReq.spec.CAMP_VAR_SPEC_u.pLnk );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varGet
 *
 *  Purpose:    Process "varGet*" Tcl commands
 *
 *              These commands are available to all variable types, although
 *              not meaningful for some.
 *
 *  Provides:   
 *              varGetIdent
 *              varGetPath
 *              varGetVarType
 *              varGetAttributes
 *              varGetTitle
 *              varGetHelp
 *              varGetStatus
 *              varGetStatusMsg
 *              varGetTimeLastSet
 *              varGetPollInterval
 *              varGetLogInterval
 *              varGetLogAction
 *              varGetAlarmAction
 *              varGetVal
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid variable '%s'", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "invalid variable" );
	    goto error;
	}

	switch( (VAR_GETPROCS)clientData )
	{
	case VAR_IDENT:
	    // Tcl_SetResult( interp, pVar->core.ident, TCL_VOLATILE );
	    camp_setMsg( "%s", pVar->core.ident );
	    break;
	case VAR_PATH:
	    // Tcl_SetResult( interp, pVar->core.path, TCL_VOLATILE );
	    camp_setMsg( "%s", pVar->core.path );
	    break;
	case VAR_VARTYPE:
	    // camp_snprintf( message, sizeof( message ), "%d", pVar->core.varType );
	    // Tcl_SetResult( interp, message, TCL_VOLATILE );
	    camp_setMsg( "%d", pVar->core.varType );
	    break;
	case VAR_ATTRIBUTES:
	    // camp_snprintf( message, sizeof( message ), "%d", pVar->core.attributes );
	    // Tcl_SetResult( interp, message, TCL_VOLATILE );
	    camp_setMsg( "%d", pVar->core.attributes );
	    break;
	case VAR_TITLE:
	    // Tcl_SetResult( interp, pVar->core.title, TCL_VOLATILE );
	    camp_setMsg( "%s", pVar->core.title );
	    break;
	case VAR_HELP:
	    // Tcl_SetResult( interp, pVar->core.help, TCL_VOLATILE );
	    camp_setMsg( "%s", pVar->core.help );
	    break;
	case VAR_STATUS:
	    // camp_snprintf( message, sizeof( message ), "%d", pVar->core.status );
	    // Tcl_SetResult( interp, message, TCL_VOLATILE );
	    camp_setMsg( "%d", pVar->core.status );
	    break;
	case VAR_STATUSMSG:
	    // Tcl_SetResult( interp, pVar->core.statusMsg, TCL_VOLATILE );
	    camp_setMsg( "%s", pVar->core.statusMsg );
	    break;
	case VAR_TIMELASTSET:
	    // camp_snprintf( message, sizeof( message ), "%d", pVar->core.timeLastSet.tv_sec );
	    // Tcl_SetResult( interp, message, TCL_VOLATILE );
	    camp_setMsg( "%d", pVar->core.timeLastSet.tv_sec );
	    break;
	case VAR_POLLINTERVAL:
	    // camp_snprintf( message, sizeof( message ), "%g", pVar->core.pollInterval );
	    // Tcl_SetResult( interp, message, TCL_VOLATILE );
	    camp_setMsg( "%g", pVar->core.pollInterval );
	    break;
	case VAR_LOGINTERVAL:
	    // camp_snprintf( message, sizeof( message ), "%g", pVar->core.logInterval );
	    // Tcl_SetResult( interp, message, TCL_VOLATILE );
	    camp_setMsg( "%g", pVar->core.logInterval );
	    break;
	case VAR_LOGACTION:
	    // Tcl_SetResult( interp, pVar->core.logAction, TCL_VOLATILE );
	    camp_setMsg( "%s", pVar->core.logAction );
	    break;
	case VAR_ALARMACTION:
	    // Tcl_SetResult( interp, pVar->core.alarmAction, TCL_VOLATILE );
	    camp_setMsg( "%s", pVar->core.alarmAction );
	    break;
	case VAR_VAL:
	    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
	    {
	    case CAMP_VAR_TYPE_NUMERIC:
		varNumGetValStr( pVar, message, sizeof( message ) );
		// // camp_snprintf( message, sizeof( message ), "%g", pVar->spec.CAMP_VAR_SPEC_u.pNum->val );
		// Tcl_SetResult( interp, message, TCL_VOLATILE );
		camp_setMsg( "%s", message );
		break;

	    case CAMP_VAR_TYPE_STRING:
		// Tcl_SetResult( interp, pVar->spec.CAMP_VAR_SPEC_u.pStr->val, TCL_VOLATILE );
		camp_setMsg( "%s", pVar->spec.CAMP_VAR_SPEC_u.pStr->val );
		break;

	    case CAMP_VAR_TYPE_SELECTION:

		// camp_status = varSelGetIDLabel( pVar, pVar->spec.CAMP_VAR_SPEC_u.pSel->val, message );
		// if( _failure( camp_status ) )
		// {
		//     Tcl_SetResult( interp, "bad selection value", TCL_STATIC );
		//     goto error;
		// }
	      
		// camp_snprintf( message, sizeof( message ), "%d", pVar->spec.CAMP_VAR_SPEC_u.pSel->val );
		// Tcl_SetResult( interp, message, TCL_VOLATILE );
		camp_setMsg( "%d", pVar->spec.CAMP_VAR_SPEC_u.pSel->val );
		break;

	    default:
		// Tcl_SetResult( interp, "var has no value", TCL_STATIC );
                _campTcl_setErrMsg1( "variable has no value property" );
		goto error;
		break;
	    }
	    break;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varNumGet
 *
 *  Purpose:    Process "varNumGet*" Tcl commands
 *
 *              These commands are only available for Numeric variable types.
 *              They return information specific to Numeric variables.
 *
 *  Provides:   
 *              varNumGetTimeStarted
 *              varNumGetNum
 *              varNumGetLow
 *              varNumGetHi
 *              varNumGetSum
 *              varNumGetSumSquares
 *              varNumGetSumCubes
 *              varNumGetSumOffset
 *              varNumGetTol
 *              varNumGetTolType
 *              varNumGetUnits
 *              varNumGetMean
 *              varNumGetStdDev
 *              varNumGetSkew
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history: 
 *      DJA     Apr 2013   Added varNumGetMean, varNumGetStdDev, varNumGetSkew.
 *
 */
int
camp_tcl_varNumGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    double mean = 0.0;
    double stddev = 0.0;
    double skew = 0.0;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_NUMERIC* pNum;
	pVar = camp_varGetp( argv[1] );
	if( pVar == NULL )
	{
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) != CAMP_VAR_TYPE_NUMERIC )
	{
	    _campTcl_setErrMsg1( "not a numeric variable" );
	    goto error;
	}

	pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

	switch( (VAR_NUMGETPROCS)clientData )
	{
	case VAR_TIMESTARTED:
	    camp_snprintf( message, sizeof( message ), "%d", pNum->timeStarted.tv_sec );
	    break;
	case VAR_NUM:
	    camp_snprintf( message, sizeof( message ), "%d", pNum->num );
	    break;
	case VAR_LOW:
	    numGetValStr( pVar->core.varType, pNum->low, message, sizeof( message ) );
	    break;
	case VAR_HI:
	    numGetValStr( pVar->core.varType, pNum->hi, message, sizeof( message ) );
	    break;
	case VAR_SUM:
	    numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->sum, message, sizeof( message ) );
	    break;
	case VAR_SUMSQUARES:
	    numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->sumSquares, message, sizeof( message ) );
	    break;
	case VAR_SUMCUBES:
	    numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->sumCubes, message, sizeof( message ) );
	    break;
	case VAR_OFFSET:
	    numGetValStr( CAMP_VAR_TYPE_FLOAT, pNum->offset, message, sizeof( message ) );
	    break;
	case VAR_TOL:
	    camp_snprintf( message, sizeof( message ), "%g", pNum->tol );
	    break;
	case VAR_TOLTYPE:
	    camp_snprintf( message, sizeof( message ), "%d", pNum->tolType );
	    break;
	case VAR_UNITS:
	    camp_snprintf( message, sizeof( message ), "%s", pNum->units );
	    break;
        /*
	 *  The following statistics calculations already have their own function,
	 *  so use that at the expense of repeating the path decoding.
	 */
	case VAR_MEAN:
	    camp_varNumCalcStats( argv[1], &mean, &stddev, &skew );
	    numGetValStr( CAMP_VAR_TYPE_FLOAT, mean, message, sizeof( message ) );
	    break;
	case VAR_STDDEV:
	    camp_varNumCalcStats( argv[1], &mean, &stddev, &skew );
	    numGetValStr( CAMP_VAR_TYPE_FLOAT, stddev, message, sizeof( message ) );
	    break;
	case VAR_SKEW:
	    camp_varNumCalcStats( argv[1], &mean, &stddev, &skew );
	    numGetValStr( CAMP_VAR_TYPE_FLOAT, skew, message, sizeof( message ) );
	    break;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varSelGet
 *
 *  Purpose:    Process "varSelGet*" Tcl commands
 *
 *              These commands are only available for Selection variable types.
 *              They return information specific to Selection variables.
 *
 *  Provides:   
 *              varSelGetValLabel
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varSelGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int camp_status = CAMP_SUCCESS;
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_SELECTION* pSel;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_SELECTION )
	{
	    _campTcl_setErrMsg1( "not a selection variable" );
	    goto error;
	}

	pSel = pVar->spec.CAMP_VAR_SPEC_u.pSel;

	switch( (VAR_SELGETPROCS)clientData )
	{
	case SEL_VALLABEL:
	    camp_status = varSelGetIDLabel( pVar, pSel->val, message, sizeof( message ) );
	    if( _failure( camp_status ) )
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_setMsg( "failed varSelGetIDLabel" ); }
		goto error;
	    }
	    break;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varArr
 *
 *  Purpose:    Process "varArr*" Tcl commands
 *
 *              These commands are only available for Array variable types.
 *              They return information specific to Array variables.
 *
 *  Provides:   
 *              varArrSetVal
 *              varArrResize
 *              varArrGetVal
 *              varArrGetVarType
 *              varArrGetElemSize
 *              varArrGetDim
 *              varArrGetDimSize
 *              varArrGetTotElem
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varArr( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int tcl_status = TCL_OK;
    char message[LEN_MSG+1];
    int argc2;
    char** argv2;
    int j;
    int dimInd[MAX_ARRAY_DIM];
    int elem;
    int ival;
    double fval;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_ARRAY* pArr;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid variable '%s'", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_ARRAY )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting array", TCL_STATIC );
	    _campTcl_setErrMsg1( "not an array variable" );
	    goto error;
	}

	pArr = pVar->spec.CAMP_VAR_SPEC_u.pArr;

	switch( (VAR_ARR_PROCS)clientData )
	{
	case ARR_SETVAL:
	    camp_snprintf( message, sizeof( message ), "%s <var> <indexList> <val>", argv[0] );
	    if( argc < 4 )
	    {
		usage( message );
		goto error;
	    }

	    tcl_status = Tcl_SplitList( interp, argv[2], &argc2, &argv2 );
	    if( tcl_status == TCL_ERROR )
	    {
	        _campTcl_setErrMsg1( "invalid <indexList>" );
		goto error;
	    }

	    if( pArr->dim != argc2 )
	    {
		free( argv2 );
		// Tcl_SetResult( interp, "invalid <indexList>", TCL_STATIC );
	        _campTcl_setErrMsg1( "invalid <indexList>" );
		goto error;
	    }

	    elem = 0;
	    for( j = 0; j < pArr->dim; j++ )
	    {
		if( Tcl_GetInt( interp, argv2[j], &dimInd[j] ) == TCL_ERROR )
		{
		    free( argv2 );
		    goto error;
		}
		if( j != 0 ) elem *= pArr->dimSize[j];
		elem += dimInd[j];
	    }

	    free( argv2 );

	    switch( pArr->varType )
	    {
	    case CAMP_VAR_TYPE_INT:
		if( Tcl_GetInt( interp, argv[3], &ival ) == TCL_ERROR )
		{
		    goto error;
		}
		switch( pArr->elemSize )
		{
		case 1:
		    ((char*)pArr->pVal)[elem] = (char)ival;
		    break;
		case 2:
		    ((short*)pArr->pVal)[elem] = (short)ival;
		    break;
		case 4:
		    ((long*)pArr->pVal)[elem] = (long)ival;
		    break;
		}
		break;

	    case CAMP_VAR_TYPE_FLOAT:
		if( Tcl_GetDouble( interp, argv[3], &fval ) == TCL_ERROR )
		{
		    goto error;
		}
		switch( pArr->elemSize )
		{
		case 4:
		    ((float*)pArr->pVal)[elem] = (float)fval;
		    break;
		case 8:
		    ((double*)pArr->pVal)[elem] = (double)fval;
		    break;
		}
		break;

	    case CAMP_VAR_TYPE_STRING:
		// note: elemSize is max size including terminator
		camp_strncpy( &((pArr->pVal)[elem]), pArr->elemSize, argv[3] ); // terminates, size includes terminator
		break;

	    default:
		break;
	    }

	    goto success;

	case ARR_RESIZE:

	    camp_snprintf( message, sizeof( message ), "%s <var> <dimSizeList>", argv[0] );

	    if( argc < 3 )
	    {
		usage( message );
		goto error;
	    }

	    tcl_status = Tcl_SplitList( interp, argv[2], &argc2, &argv2 );
	    if( tcl_status == TCL_ERROR )
	    {
	        _campTcl_setErrMsg1( "invalid <dimSizeList>" );
		goto error;
	    }

	    if( pArr->dim != argc2 )
	    {
		free( argv2 );
		// Tcl_SetResult( interp, "invalid <indexList>", TCL_STATIC );
	        _campTcl_setErrMsg1( "invalid <dimSizeList>" );
		goto error;
	    }

	    for( j = 0; j < pArr->dim; j++ )
	    {
		if( Tcl_GetInt( interp, argv2[j], &dimInd[j] ) == TCL_ERROR )
		{
		    free( argv2 );
		    goto error;
		}
	    }

	    free( argv2 );

	    pArr->totElem = pArr->dimSize[0] = dimInd[0];
	    for( j = 1; j < pArr->dim; j++ )
	    {
		pArr->totElem *= dimInd[j];
		pArr->dimSize[j] = dimInd[j];
	    }

	    pArr->pVal = camp_realloc( pArr->pVal, pArr->totElem*pArr->elemSize );

	    goto success;

	case ARR_GETVAL:

	    camp_snprintf( message, sizeof( message ), "%s <var> <indexList>", argv[0] );
	    if( argc < 3 )
	    {
		usage( message );
		goto error;
	    }

	    tcl_status = Tcl_SplitList( interp, argv[2], &argc2, &argv2 );
	    if( tcl_status == TCL_ERROR )
	    {
	        _campTcl_setErrMsg1( "invalid <indexList>" );
		goto error;
	    }

	    if( pArr->dim != argc2 )
	    {
		free( argv2 );
		// Tcl_SetResult( interp, "invalid <indexList>", TCL_STATIC );
	        _campTcl_setErrMsg1( "invalid <indexList>" );
		goto error;
	    }

	    elem = 0;
	    for( j = 0; j < pArr->dim; j++ )
	    {
		if( Tcl_GetInt( interp, argv2[j], &dimInd[j] ) == TCL_ERROR )
		{
		    free( argv2 );
		    goto error;
		}
		if( j != 0 ) elem *= pArr->dimSize[j];
		elem += dimInd[j];
	    }

	    free( argv2 );

	    switch( pArr->varType )
	    {
	    case CAMP_VAR_TYPE_INT:
		switch( pArr->elemSize )
		{
		case 1:
		    camp_snprintf( message, sizeof( message ), "%d", ((char*)pArr->pVal)[elem] );
		    break;
		case 2:
		    camp_snprintf( message, sizeof( message ), "%d", ((short*)pArr->pVal)[elem] );
		    break;
		case 4:
		    camp_snprintf( message, sizeof( message ), "%d", ((long*)pArr->pVal)[elem] );
		    break;
		}
		break;
	    case CAMP_VAR_TYPE_FLOAT:
		switch( pArr->elemSize )
		{
		case 4:
		    Tcl_PrintDouble( interp, ((float*)pArr->pVal)[elem], message );
		    break;
		case 8:
		    Tcl_PrintDouble( interp, ((double*)pArr->pVal)[elem], message );
		    break;
		}
		break;
	    case CAMP_VAR_TYPE_STRING:
		// note: this copy allows pArr->pVal[elem] to be pArr->elemSize without a terminating character,
		//       but note that everywhere else pArr->pVal[elem] is terminated
		camp_strncpy_max( message, LEN_MSG+1, &((pArr->pVal)[elem]), pArr->elemSize ); 
		break;
	    default:
		break;
	    }

	    break;

	case ARR_GETVARTYPE:
	    camp_snprintf( message, sizeof( message ), "%d", pArr->varType );
	    break;

	case ARR_GETELEMSIZE:
	    camp_snprintf( message, sizeof( message ), "%d", pArr->elemSize );
	    break;

	case ARR_GETDIM:
	    camp_snprintf( message, sizeof( message ), "%d", pArr->dim );
	    break;

	case ARR_GETDIMSIZE:
	    camp_snprintf( message, sizeof( message ), "%d", pArr->dimSize[0] );
	    for( j = 1; j < pArr->dim; j++ )
	    {
		camp_snprintf( message, sizeof( message ), "%s %d", message, pArr->dimSize[j] );
	    }
	    break;

	case ARR_GETTOTELEM:
	    camp_snprintf( message, sizeof( message ), "%d", pArr->totElem );
	    break;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );
		
success:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varInsGet
 *
 *  Purpose:    Process "insGet*" Tcl commands
 *
 *              These commands are only available for Instrument variable
 *              types.  They return information specific to Instrument
 *              variables.
 *
 *  Provides:   
 *              insGetLockHost
 *              insGetLockPid
 *              insGetDefFile
 *              insGetIniFile
 *              insGetDataItemsLen
 *              insGetTypeIdent
 *              insGetInstance
 *              insGetClass
 *              insGetIfStatus
 *              insGetIfTypeIdent
 *              insGetIfDelay
 *              insGetIfTimeout
 *              insGetIf
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varInsGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    _campTcl_setErrMsg1( "not an instrument variable" );
	    goto error;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	switch( (VAR_INSGETPROCS)clientData )
	{
	case INS_LOCKHOST:
	    camp_strncpy( message, LEN_MSG+1, pIns->lockHost ); 
	    break;
	case INS_LOCKPID:
	    camp_snprintf( message, sizeof( message ), "%d", pIns->lockPID );
	    break;
	case INS_DEFFILE:
	    camp_strncpy( message, LEN_MSG+1, pIns->defFile ); 
	    break;
	case INS_INIFILE:
	    camp_strncpy( message, LEN_MSG+1, pIns->iniFile ); 
	    break;
	case INS_DATAITEMS_LEN:
	    camp_snprintf( message, sizeof( message ), "%d", pIns->dataItems_len );
	    break;
	case INS_TYPEIDENT:
	    camp_strncpy( message, LEN_MSG+1, pIns->typeIdent ); 
	    break;
	case INS_TYPEINSTANCE:
	    camp_snprintf( message, sizeof( message ), "%d", pIns->typeInstance );
	    break;
	case INS_LEVELCLASS:
	    camp_snprintf( message, sizeof( message ), "%d", pIns->class );
	    break;
	case INS_IF_STATUS:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%d", pIF->status );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_TYPEIDENT:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_strncpy( message, LEN_MSG+1, pIF->typeIdent ); 
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_ACCESSDELAY:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%f", pIF->accessDelay );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_TIMEOUT:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%f", pIF->timeout );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_strncpy( message, LEN_MSG+1, pIF->defn ); 
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_NUMREAD:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%lu", pIF->numReads );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_NUMWRITE:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%lu", pIF->numWrites );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_READERR:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%lu", pIF->numReadErrors );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_WRITEERR:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%lu", pIF->numWriteErrors );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_CONSECREADERR:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%lu", pIF->numConsecReadErrors );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	case INS_IF_CONSECWRITEERR:
	    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	    {
		CAMP_IF* pIF = pIns->pIF;
		if( pIF == NULL )
		{
		    goto intundef;
		}
		else
		{
		    camp_snprintf( message, sizeof( message ), "%lu", pIF->numConsecWriteErrors );
		}
	    }
	    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock
	    break;

	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

intundef:
    _campTcl_setErrMsg1( "interface undefined" );
    goto error;

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varGetIfRs232
 *
 *  Purpose:    Process "insGetIfRs232*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              an RS232 interface defined.  They return information
 *              specific to an RS232 interface definition.
 *
 *  Provides:   
 *              insGetIfRs232Name
 *              insGetIfRs232Baud
 *              insGetIfRs232Data
 *              insGetIfRs232Parity
 *              insGetIfRs232Stop
 *              insGetIfRs232ReadTerm
 *              insGetIfRs232WriteTerm
 *              insGetIfRs232Timeout
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGetIfRs232( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;
	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid: variable (%s)", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
	    _campTcl_setErrMsg1( "not an instrument variable" );
	    goto error;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;
    
	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    if( pIF == NULL )
	    {
		// Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
		_campTcl_setErrMsg1( "interface undefined" );
		goto error;
	    }

	    if( pIF->typeID != CAMP_IF_TYPE_RS232 )
	    {
		// Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
		_camp_setMsg( "wrong interface type '%s' in %s %s", pIF->typeIdent, argv[0], argv[1] );
		goto error;
	    }

	    switch( (VAR_GETIFPROCS)clientData )
	    {
	    case INS_CAMP_IF_RS232_NAME:
		camp_getIfRs232Port( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_RS232_BAUD:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfRs232Baud( pIF->defn ) );
		break;
	    case INS_CAMP_IF_RS232_DATABITS:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfRs232Data( pIF->defn ) );
		break;
	    case INS_CAMP_IF_RS232_PARITY:
		camp_getIfRs232Parity( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_RS232_STOPBITS:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfRs232Stop( pIF->defn ) );
		break;
	    case INS_CAMP_IF_RS232_READTERM:
		camp_getIfRs232ReadTerm( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_RS232_WRITETERM:
		camp_getIfRs232WriteTerm( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_RS232_READTIMEOUT:
		// camp_snprintf( message, sizeof( message ), "%d", camp_getIfRs232Timeout( pIF->defn ) );
		camp_snprintf( message, sizeof( message ), "%.1f", pIF->timeout ); // maintain backwards compatibility
		break;
	    default:
		break;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varGetIfGpib
 *
 *  Purpose:    Process "insGetIfGpib*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              a GPIB interface defined.  They return information
 *              specific to a GPIB interface definition.
 *
 *  Provides:   
 *              insGetIfGpibAddr
 *              insGetIfGpibReadTerm
 *              insGetIfGpibWriteTerm
 *              insGetIfGpibMscbAddr
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGetIfGpib( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;
	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid: variable (%s)", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
	    _campTcl_setErrMsg1( "not an instrument variable" );
	    goto error;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    if( pIF == NULL )
	    {
		// Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
		_campTcl_setErrMsg1( "interface undefined" );
		goto error;
	    }

	    if( ( pIF->typeID != CAMP_IF_TYPE_GPIB ) && ( pIF->typeID != CAMP_IF_TYPE_GPIB_MSCB ) )
	    {
		// Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
		_camp_setMsg( "wrong interface type '%s' in %s %s", pIF->typeIdent, argv[0], argv[1] );
		goto error;
	    }

	    switch( (VAR_GETIFPROCS)clientData )
	    {
	    case INS_CAMP_IF_GPIB_ADDR:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfGpibAddr( pIF->defn ) );
		break;
	    case INS_CAMP_IF_GPIB_READTERM:
		camp_getIfGpibReadTerm( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_GPIB_WRITETERM:
		camp_getIfGpibWriteTerm( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_GPIB_MSCBADDR:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfGpibMscbAddr( pIF->defn ) );
		break;
	    default:
		break;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varGetIfCamac
 *
 *  Purpose:    Process "insGetIfCamac*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              a CAMAC interface defined.  They return information
 *              specific to a CAMAC interface definition.
 *
 *  Provides:   
 *              insGetIfCamacB
 *              insGetIfCamacC
 *              insGetIfCamacN
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_varGetIfCamac( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid: variable (%s)", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
	    _campTcl_setErrMsg1( "not an instrument variable" );
	    goto error;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    if( pIF == NULL )
	    {
		// Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
		_campTcl_setErrMsg1( "interface undefined" );
		goto error;
	    }

	    if( pIF->typeID != CAMP_IF_TYPE_CAMAC )
	    {
		// Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
		_camp_setMsg( "wrong interface type '%s' in %s %s", pIF->typeIdent, argv[0], argv[1] );
		goto error;
	    }

	    switch( (VAR_GETIFPROCS)clientData )
	    {
	    case INS_CAMP_IF_CAMAC_B:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfCamacB( pIF->defn ) );
		break;
	    case INS_CAMP_IF_CAMAC_C:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfCamacC( pIF->defn ) );
		break;
	    case INS_CAMP_IF_CAMAC_N:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfCamacN( pIF->defn ) );
		break;
	    default:
		break;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}

/*
 *  Name:       camp_tcl_varGetIfVme
 *
 *  Purpose:    Process "insGetIfVme*" Tcl command(s)
 *
 *              Return information specific to a VME interface definition.
 *              (Only one command yet.)
 *
 *  Provides:   
 *              insGetIfVmeBase
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history: 
 *              10-Nov-2003       DA     Created
 *
 */
int
camp_tcl_varGetIfVme( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid: variable (%s)", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
	    _campTcl_setErrMsg1( "not an instrument variable" );
	    goto error;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    if( pIF == NULL )
	    {
		// Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
		_campTcl_setErrMsg1( "interface undefined" );
		goto error;
	    }

	    if( pIF->typeID != CAMP_IF_TYPE_VME )
	    {
		// Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
		_camp_setMsg( "wrong interface type '%s' in %s %s", pIF->typeIdent, argv[0], argv[1] );
		goto error;
	    }

	    switch( (VAR_GETIFPROCS)clientData )
	    {
	    case INS_CAMP_IF_VME_BASE:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfVmeBase( pIF->defn ) );
		break;
	    default:
		break;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_varGetIfTcpip
 *
 *  Purpose:    Process "insGetIfTcpip*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              an TCPIP interface defined.  They return information
 *              specific to an TCPIP interface definition.
 *
 *  Provides:   
 *              insGetIfTcpipAddress
 *              insGetIfTcpipPort
 *              insGetIfTcpipReadTerm
 *              insGetIfTcpipWriteTerm
 *              insGetIfTcpipTimeout
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *              17-Mar-2006   DA   Created
 *
 */
int
camp_tcl_varGetIfTcpip( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid: variable (%s)", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
	    _campTcl_setErrMsg1( "not an instrument variable" );
	    goto error;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;
    
	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    if( pIF == NULL )
	    {
		// Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
		_campTcl_setErrMsg1( "interface undefined" );
		goto error;
	    }

	    if( pIF->typeID != CAMP_IF_TYPE_TCPIP )
	    {
		// Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
		_camp_setMsg( "wrong interface type '%s' in %s %s", pIF->typeIdent, argv[0], argv[1] );
		goto error;
	    }

	    switch( (VAR_GETIFPROCS)clientData )
	    {
	    case INS_CAMP_IF_TCPIP_ADDRESS:
		camp_getIfTcpipAddr( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_TCPIP_PORT:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfTcpipPort( pIF->defn ) );
		break;
	    case INS_CAMP_IF_TCPIP_READTERM:
		camp_getIfTcpipReadTerm( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_TCPIP_WRITETERM:
		camp_getIfTcpipWriteTerm( pIF->defn, message, sizeof( message ) );
		break;
	    case INS_CAMP_IF_TCPIP_TIMEOUT:
		// camp_snprintf( message, sizeof( message ), "%.1f", camp_getIfTcpipTimeout( pIF->defn ) );
		camp_snprintf( message, sizeof( message ), "%.1f", pIF->timeout ); // maintain backwards compatibility
		break;
	    default:
		break;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}

/*
 *  Name:       camp_tcl_varGetIfIndpak
 *
 *  Purpose:    Process "insGetIfIndpak*" Tcl commands
 *
 *              These commands are only available for Instruments with
 *              a Industry Pack (Indpak) interface defined.  They return information
 *              specific to a Indpak interface definition.
 *
 *  Provides:   
 *              insGetIfIndpakSlot
 *              insGetIfIndpakType
 *              insGetIfIndpakChannel
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */

int
camp_tcl_varGetIfIndpak( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{

#ifdef VXWORKS
    char message[LEN_MSG+1];
    char buf[LEN_BUF+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_INSTRUMENT* pIns;

	pVar = camp_varGetp( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid: variable (%s)", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _campTcl_setErrMsg1( "no such variable" );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_INSTRUMENT )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting instrument", TCL_STATIC );
	    _campTcl_setErrMsg1( "not an instrument variable" );
	    goto error;
	}

	pIns = pVar->spec.CAMP_VAR_SPEC_u.pIns;

	// _mutex_lock_sys_on(); // warning: don't return inside mutex lock
	{
	    CAMP_IF* pIF = pIns->pIF;
	    if( pIF == NULL )
	    {
		// Tcl_SetResult( interp, "instrument interface is undefined", TCL_STATIC );
		_campTcl_setErrMsg1( "interface undefined" );
		goto error;
	    }

	    if( pIF->typeID != CAMP_IF_TYPE_INDPAK )
	    {
		// Tcl_SetResult( interp, "wrong interface type", TCL_STATIC );
		_camp_setMsg( "wrong interface type '%s' in %s %s", pIF->typeIdent, argv[0], argv[1] );
		goto error;
	    }

	    switch( (VAR_GETIFPROCS)clientData )
	    {
	    case INS_CAMP_IF_INDPAK_SLOT:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfIndpakSlot( pIF->defn ) );
		break;
	    case INS_CAMP_IF_INDPAK_TYPE:
		camp_getIfIndpakType( pIF->defn, buf, sizeof( buf ) );
		break;
	    case INS_CAMP_IF_INDPAK_CHANNEL:
		camp_snprintf( message, sizeof( message ), "%d", camp_getIfIndpakChannel( pIF->defn ) );
		break;
	    default:
	      break;
	    }
	}
	// _mutex_lock_sys_off(); // warning: don't return inside mutex lock
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );

#else /* !VXWORKS */

    Tcl_SetResult( interp, "interface unimplemented", TCL_STATIC ); camp_setMsg( "" ); 
    return( TCL_ERROR );

#endif // VXWORKS
}


/*
 *  Name:       camp_tcl_lnkGet
 *
 *  Purpose:    Process "lnkGet*" Tcl commands
 *
 *              These routines are available for Link variable types only.
 *              They return information specific to Link variables.
 *
 *  Provides:   
 *              lnkGetVarType
 *              lnkGetPath
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_lnkGet( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <var>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_LINK* pLnk;

	pVar = camp_varGetTrueP( argv[1] /* , mutex_lock_varlist_check() */ );
	if( pVar == NULL )
	{
	    /* camp_snprintf( message, sizeof( message ), "invalid: variable (%s)", argv[1] ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _camp_setMsg( "invalid variable '%s'", argv[1] );
	    goto error;
	}

	if( pVar->core.varType != CAMP_VAR_TYPE_LINK )
	{
	    // Tcl_SetResult( interp, "bad vartype, expecting link", TCL_STATIC );
	    _camp_setMsg( "not a link variable '%s'", argv[1] );
	    goto error;
	}

	pLnk = pVar->spec.CAMP_VAR_SPEC_u.pLnk;

	switch( (VAR_GETLNKPROCS)clientData )
	{
	case LNK_VARTYPE:
	    camp_snprintf( message, sizeof( message ), "%d", pLnk->varType );
	    break;
	case LNK_PATH:
	    camp_strncpy( message, LEN_MSG+1, pLnk->path ); 
	    break;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    // Tcl_SetResult( interp, message, TCL_VOLATILE );
    camp_setMsg( "%s", message );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_msg
 *
 *  Purpose:    Process "msg" Tcl command
 *
 *              This command causes the CAMP server to log a string message.
 *
 *  Provides:   
 *              msg
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_msg( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <msg>", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    camp_log( "%s", argv[1] );
 
    // // camp_snprintf( message, sizeof( message ), "Message '%s' logged", argv[1] );
    // // Tcl_SetResult( interp, message, TCL_VOLATILE );
    // _camp_setMsg( "Message '%s' logged", argv[1] );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_sleep
 *
 *  Purpose:    Process "sleep" Tcl command
 *
 *              This command causes the executing process (or thread)
 *              to delay execution for a specified time (floating point
 *              parameter).
 *
 *              NOTE:  this command sends the current thread to sleep
 *              for the specified (floating-point) time.  By default,
 *              the other threads (if multithreaded) are allowed to
 *              proceed.  Using the "lock" flag, however, turns the
 *              global lock on, so that no other threads may execute
 *              while the current thread is sleeping.
 *
 *              CAUTION:  turning the global lock on for long periods
 *              of time (i.e., seconds) causes unpredictable problems
 *              in both the VAX/VMS and VxWorks versions.
 *
 *  Provides:   
 *              sleep
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *
 */
int
camp_tcl_sleep( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    double delay;
    char* endptr = NULL;
    char message[LEN_MSG+1];
    bool_t lockOff = TRUE;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <float> [lock]", argv[0] );

    if( argc < 2 )
    {
        usage( message );
        goto error;
    }

    if( ( argc > 2 ) && ( streq( argv[2], "lock" ) ) )
    {
        lockOff = FALSE;
    }

    delay = strtod( argv[1], &endptr );
    if( isgraph( *endptr ) )
    {
        usage( message );
        goto error;
    }

    {
	int globalMutexLockCount = 0;
	if( lockOff ) globalMutexLockCount = mutex_unlock_global_all();

	camp_fsleep( delay );

	if( lockOff ) mutex_lock_global_all( globalMutexLockCount );
    }

    // // camp_snprintf( message, sizeof( message ), "Server slept for %f seconds", delay );
    // // Tcl_SetResult( interp, message, TCL_VOLATILE );
    // _camp_setMsg( "Server slept for %f seconds", delay );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_gettime
 *
 *  Purpose:    Process "gettime" Tcl command
 *
 *              This command causes the server to query the computer
 *              system time and report the time back as a floating point
 *              number of seconds since Jan. 1, 1970 (the default time
 *              zero for C libraries).
 *
 *  Provides:
 *              gettime
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The Tcl string result is the string that was requested,
 *              or an error message if the command failed.
 *
 *  Revision history:
 *
 */
int
camp_tcl_gettime( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    char message[LEN_MSG+1];
    timeval_t tv;

    gettimeval( &tv );
    camp_snprintf( message, sizeof( message ), "%.6f", timeval_to_double( &tv ) );

    Tcl_SetResult( interp, message, TCL_VOLATILE ); camp_setMsg( "" ); 
    return( TCL_OK );
}


#if CAMP_HAVE_CAMAC
/*
 *  Name:       camp_tcl_camac_cdreg
 *
 *  Purpose:    Process "cdreg" Tcl command
 *
 *              Calls a CAMAC cdreg command.
 *
 *  Provides:
 *              cdreg
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The "ext" result of the command is set in the variable
 *              passed in the ext_var parameter.
 *
 *  note: this code will work for any architecture as long as cdreg,cfsa,etc are implemented
 *        and linked to the server
 *
 *  Revision history:
 *
 */
int
camp_tcl_camac_cdreg( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int tcl_status = TCL_OK;
    long ext;
    long b, c, n, a;
    char message[LEN_MSG+1];
    char* endptr;
    char buf[LEN_BUF+1];
    // _mutex_lock_begin();

    switch( (CAMAC_PROCS)clientData )
    {
      case CAMAC_CDREG:
        camp_snprintf( message, sizeof( message ), "%s <ext_var> <b> <c> <n> <a>", argv[0] );
        if( argc < 6 )
        {
            usage( message );
            goto error;
        }

        b = strtol( argv[2], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            goto error;
        }

        c = strtol( argv[3], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            goto error;
        }

        n = strtol( argv[4], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            goto error;
        }

        a = strtol( argv[5], &endptr, 0 );
        if( isgraph( *endptr ) )
        {
            usage( message );
            goto error;
        }

	if( _camp_debug(CAMP_DEBUG_IF_CAMAC) ) printf( "calling cdreg..." );

	{
	    int globalMutexLockCount = mutex_unlock_global_all();

	    tcl_status = TCL_OK;

// #ifdef VXWORKS
	    if( !cdreg( &ext, &b, &c, &n, &a ) ) tcl_status = TCL_ERROR;
// #endif // VXWORKS

	    mutex_lock_global_all( globalMutexLockCount );
	}

	if( _camp_debug(CAMP_DEBUG_IF_CAMAC) ) printf( "done\n" );

	if( tcl_status == TCL_ERROR ) 
	{
	    // Tcl_SetResult( interp, "Failed cdreg, check connections to the CAMAC crate", TCL_STATIC );
	    _camp_setMsg( "failed cdreg, check connections to the CAMAC crate" );
	    goto error;
	}
	
        Tcl_SetVar( interp, argv[1], camp_itoa( ext, buf, sizeof( buf ) ), 0 );

        break;

    default: 
	break;
    }

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}
#endif // CAMP_HAVE_CAMAC


#if CAMP_HAVE_CAMAC
/*
 *  Name:       camp_tcl_camac_single
 *
 *  Purpose:    Process CAMAC single data Tcl commands
 *
 *              Calls a CAMAC cfsa or cssa command.
 *
 *  Provides:
 *              cfsa
 *              cssa
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The resulting "q" is set in the Tcl variable passed as the
 *              q_var parameter.  For read operations, the resulting data
 *              is set in the Tcl variable passed as the data_var parameter.
 *
 *  note: this code will work for any architecture as long as cdreg,cfsa,etc are implemented
 *        and linked to the server
 *
 *  Revision history:
 *
 */
int
camp_tcl_camac_single( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int tcl_status = TCL_OK;
    long ext;
    long f;
    u_long data = 0;
    u_long data32 = 0;
    u_short data16 = 0;
    char q = 0;
    char* p;
    char message[LEN_MSG+1];
    char* endptr;
    char buf[LEN_BUF+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <f> <ext> <data_var> <q_var>", argv[0] );
    if( argc < 5 )
    {
	usage( message );
	goto error;
    }
    
    f = strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	usage( message );
	goto error;
    }
    
    ext = strtol( argv[2], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	usage( message );
	goto error;
    }
    
    if( ( f >= 16 ) && ( f <= 23 ) )
    {
	if( ( p = Tcl_GetVar( interp, argv[3], 0 ) ) == NULL )
	{
	    // Tcl_AppendResult( interp, "no such variable '", argv[3], "'", NULL );
	    _camp_setMsg( "no such variable '%s'", argv[3] );
	    goto error;
	}
	
	data = strtol( p, &endptr, 0 );
	if( isgraph( *endptr ) )
	{
	    usage( message );
	    goto error;
	}
    }
    
    if( _camp_debug(CAMP_DEBUG_IF_CAMAC) ) printf( "calling cfsa/cssa..." );

    {
	int globalMutexLockCount = mutex_unlock_global_all();

	tcl_status = TCL_OK;
    
// #ifdef VXWORKS
	switch( (CAMAC_PROCS)clientData )
	{
	case CAMAC_CFSA: 
	    data32 = data; /* if it's a write */
	    if( !cfsa( &f, &ext, &data32, &q ) ) tcl_status = TCL_ERROR;
	    data = data32; /* if it's a read */
	    break;
	case CAMAC_CSSA: 
	    data16 = data;
	    if( !cssa( &f, &ext, &data16, &q ) ) tcl_status = TCL_ERROR;
	    data = data16;
	    break;
	default:
	    break;
	}
// #endif // VXWORKS

	mutex_lock_global_all( globalMutexLockCount );
    }

    if( _camp_debug(CAMP_DEBUG_IF_CAMAC) ) printf( "done\n" );

    if( tcl_status == TCL_ERROR ) 
    {
	// Tcl_SetResult( interp, "Failed camac data function", TCL_STATIC );
	_camp_setMsg( "failed camac data function" );
	goto error;
    }

    Tcl_SetVar( interp, argv[4], camp_itoa( (int)q, buf, sizeof( buf ) ), 0 );
    
    if( ( f >= 0 ) && ( f <= 7 ) )
    {
	Tcl_SetVar( interp, argv[3], camp_itoa( data, buf, sizeof( buf ) ), 0 );
    }

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}
#endif // CAMP_HAVE_CAMAC


#if CAMP_HAVE_CAMAC
/*
 *  Name:       camp_tcl_camac_block
 *
 *  Purpose:    Process CAMAC block data Tcl commands
 *
 *              Calls a CAMAC command.
 *
 *  Provides:
 *              cfubc
 *              cfubr
 *              csubc
 *              csubr
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *              The var_path parameter should refer to a CAMP array variable
 *              which will be used to maintain the data of block transfers
 *              (reading or writing).
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *              The tally_var parameter will be set to the number of
 *              transfers tallied by the call.  The result of a block read
 *              will be set in the var_path variable which must refer to
 *              a valid CAMP array variable.
 *
 *  note: this code will work for any architecture as long as cdreg,cfsa,etc are implemented
 *        and linked to the server
 *
 *  Revision history:
 *
 */
int
camp_tcl_camac_block( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int tcl_status = TCL_OK;
    long ext;
    long f;
    char message[LEN_MSG+1];
    char* endptr;
    CAMAC_CB cb;
    char buf[LEN_BUF+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <f> <ext> <var_path> <count> <tally_var> <lam_ident> <channel>", argv[0] );
    if( argc < 8 )
    {
        goto usage;
    }

    f = strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	goto usage;
    }

    ext = strtol( argv[2], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	goto usage;
    }

    /*
     *  Check this size (are array variables dynamic?)
     */
    cb.count = (short)strtol( argv[4], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	goto usage;
    }

    cb.lam_ident = (short)strtol( argv[6], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	goto usage;
    }

    cb.channel = (short)strtol( argv[7], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	goto usage;
    }

    // _mutex_lock_varlist_on(); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	char* pData;

	if( ( ( f >= 16 ) && ( f <= 23 ) ) ||
	    ( ( f >= 0 ) && ( f <= 7 ) ) )
	{
	    /*
	     *  Set pData to CAMP array variable
	     */
	    pVar = camp_varGetp( argv[3] /* , mutex_lock_varlist_check() */ );
	    if( pVar == NULL )
	    {
		// Tcl_AppendResult( interp, "no such CAMP variable '", argv[3], "'", NULL );
		_camp_setMsg( "no such variable '%s'", argv[3] );
		goto error;
	    }

	    pData = pVar->spec.CAMP_VAR_SPEC_u.pArr->pVal;
	}
	else
	{
	    /*
	     *  Not read or write
	     */
	    pData = NULL;
	}

	if( _camp_debug(CAMP_DEBUG_IF_CAMAC) ) printf( "calling camac block data routine..." );

	{
	    int globalMutexLockCount = mutex_unlock_global_all();

	    tcl_status = TCL_OK;
	
	    // todo: copy the data to/from pVar outside of the global unlock/lock
	    //       (i.e., take/release varlist mutex outside of release/take global mutex)

	    // todo: check that pData (i.e., array variable value) has the appropriate size for the operation
	    //       - array dim == cb.count
	    //       - varType == CAMP_VAR_TYPE_INT
	    //       - elemSize == 2 (CSUBC/CSUBR) or 4 (CFUBC/CFUBR)

//#ifdef VXWORKS
	    switch( (CAMAC_PROCS)clientData )
	    {
	    case CAMAC_CFUBC: 
		if( !cfubc( &f, &ext, (long*)buf, &cb ) ) tcl_status = TCL_ERROR;
		break;
	    case CAMAC_CFUBR: 
		if( !cfubr( &f, &ext, (long*)buf, &cb ) ) tcl_status = TCL_ERROR;
		break;
	    case CAMAC_CSUBC: 
		if( !csubc( &f, &ext, (short*)buf, &cb ) ) tcl_status = TCL_ERROR;
		break;
	    case CAMAC_CSUBR: 
		if( !csubr( &f, &ext, (short*)buf, &cb ) ) tcl_status = TCL_ERROR;
		break;
	    default:
		break;
	    }
//#endif // VXWORKS

	    mutex_lock_global_all( globalMutexLockCount );  //@tw 20140213 bug: was unlock
	}

	if( _camp_debug(CAMP_DEBUG_IF_CAMAC) ) printf( "done\n" );

	if( tcl_status == TCL_ERROR ) 
	{
	    // Tcl_SetResult( interp, "Failed camac block data function", TCL_STATIC );
	    _camp_setMsg( "failed camac block data function" );
	    goto error;
	}
    }
    // _mutex_lock_varlist_off(); // warning: don't return inside mutex lock

    Tcl_SetVar( interp, argv[5], camp_itoa( (int)cb.tally, buf, sizeof( buf ) ), 0 );

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );
usage:
    usage( message );
    goto error;
error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}
#endif // CAMP_HAVE_CAMAC


/*
 *  Name:       camp_tcl_vme_io
 *
 *  Purpose:    Process vme reads and writes
 *
 *  Provides:
 *              vmeRead
 *              vmeWrite
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters. Tcl args:
 *              vmeWrite: base_address offset_address data_type value
 *              vmeread:  base_address offset_address data_type ?buffer_size?
 *              (An omitted buffer size defaults to 16, which is more than
 *              enough for any numerics.)  Note that there are no floating-
 *              point number data types.
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status.  Message returns value string or error message.
 *
 *  Revision history:
 *              10-Nov-2003     DA     Created
 *
 */
int
camp_tcl_vme_io( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    // VME_IO_CODE status;
    void* base;
    int   offset;
    int   nreq;
    char  dtype[16] = { 0 };
    // int*  typetok;
    char  message[LEN_MSG+1];
    char* endptr;
    char* buff = 0;
    long  buffsize = 0;
    VME_IO_CODE stat;
    char* failread = "Failed VME read:";
    char* failwrite = "Failed VME write:";
    // _mutex_lock_begin();

    if ( VME_READ == (VME_PROCS)clientData )
    {
        camp_snprintf( message, sizeof( message ), "%s <base_addr> <addr_offset> <data_type> ?<buffer_size>?", argv[0] );
        nreq = 4;
    }
    else
    {
        camp_snprintf( message, sizeof( message ), "%s <base_addr> <addr_offset> <data_type> <value>", argv[0] );
        nreq = 5;
    }

    if( argc < nreq )
    {
	goto vmeioerr;
    }

    base = (void*)strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) || base == 0 )
    {
	goto vmeioerr;
    }

    offset = strtol( argv[2], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
	goto vmeioerr;
    }

    camp_strncpy( dtype, sizeof( dtype ), argv[3] ); 

    switch( (VME_PROCS)clientData ) 
    {
      case VME_READ:
	  if( ( argc < 5 ) && ( strncmp( argv[3],"string", 4 ) != 0 ) ) 
          { /* read: not a string, no buffer specified  --  use 16 */
            buffsize = 16;
          }
        else
          {
            buffsize = 0;
            if( argc < 5 ) 
	    {
		goto vmeioerr;
	    }
            else
	    {
                buffsize = strtol( argv[4], &endptr, 0 );
                if( isgraph( *endptr ) ) 
		{
		    goto vmeioerr;
		}
	    }
          }
        buff = (char*) camp_zalloc( buffsize+1 );

        stat = vmeRead( base, offset, (int*)dtype, buff, buffsize+1 ); // 20140224  TW  buffsize -> buffsize+1 (and accounted for within vmeRead)

        switch (stat) 
	{
        case VME_IO_UNKNOWN:
	    camp_snprintf( message, sizeof( message ), "%s unknown data type '%s'", failread, dtype );
	    goto vmeioerr;
        case VME_IO_OVERFLOW:
	    camp_snprintf( message, sizeof( message ), "%s data overflows buffer (%d)", failread, buffsize );
	    goto vmeioerr;
        case VME_IO_ACCESS_ERR:
	    camp_snprintf( message, sizeof( message ), "%s access violation (bad address)", failread );
	    goto vmeioerr;
        default:
	    buff[buffsize] = '\0';
	    // Tcl_SetResult( interp, buff, TCL_VOLATILE );
	    camp_setMsg( "%s", buff );
	    free( buff );
	    goto success;
        }
        break;

      case VME_WRITE:
        buffsize = strlen( argv[4] );

        stat = vmeWrite( base, offset, (int*)dtype, argv[4], buffsize );

        switch (stat) 
	{
        case VME_IO_UNKNOWN:
	    camp_snprintf( message, sizeof( message ), "%s unknown data type '%s'", failwrite, dtype );
	    goto vmeioerr;
        case VME_IO_OVERFLOW:
	    camp_snprintf( message, sizeof( message ), "%s numeric overflow for type '%s'", failwrite, dtype );
	    goto vmeioerr;
        case VME_IO_INVAL_NUM:
	    camp_snprintf( message, sizeof( message ), "%s invalid numeric value %.16s", failwrite, dtype );
	    goto vmeioerr;
        case VME_IO_ACCESS_ERR:
	    camp_snprintf( message, sizeof( message ), "%s access violation (bad address)", failwrite );
	    goto vmeioerr;
        default:
	    buff[buffsize] = '\0';
	    // Tcl_SetResult( interp, buff, TCL_VOLATILE );
	    camp_setMsg( "%s", buff );
	    free( buff );
	    goto success;
        }

        break;
    }

success:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

vmeioerr:
    _free( buff );
    usage( message );

//error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
 *  Name:       camp_tcl_gpib_clear
 *
 *  Purpose:    Process general GPIB Tcl command
 *
 *              Called using command "gpibClear <if_type> <GPIB address>"
 *
 *              Calls the function gpib_clear() which should be implemented for
 *              each GPIB interface as a high level GPIB SDC.  That is, it
 *              should set up the talker and listener properly, and then send
 *              an SDC to the listener.  See the MVIP300 implementation called
 *              dvclr() for an example.
 *
 *  Provides:
 *              gpibClear
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *              The GPIB address is any valid GPIB bus address.  It does
 *              not have to refer to the address of any valid CAMP instrument.
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *    21-Dec-1999  TW  Creation
 *    20140403     TW  take if_type name, call gpibClearProc
 *    20140407     TW  not implemented for mscb_gpib, because the mscb gpib
 *                     firmware doesn't provide a clear function at this time
 *                     (implementing this would require an extra parameter - the mscb address)
 *
 */
int
camp_tcl_gpib_clear( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int tcl_status = TCL_OK;
    int addr;
    char message[LEN_MSG+1];
    char* endptr;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <if_type> <GPIB addr>", argv[0] );
    if( argc < 3 )
    {
	usage( message );
	tcl_status = TCL_ERROR; goto return_;
    }

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	char* if_type_ident = argv[1];

	pIF_t = camp_ifGetpIF_t( if_type_ident /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    /* camp_snprintf( message, sizeof( message ), "%s: invalid interface type '%s'", argv[0], if_type_ident ); */
	    /* Tcl_SetResult( interp, message, TCL_VOLATILE ); */
	    _camp_setMsg( "invalid interface type '%s'", argv[1] );
	    tcl_status = TCL_ERROR; goto return_;
	}

	addr = strtol( argv[2], &endptr, 0 );
	if( isgraph( *endptr ) )
	{
	    usage( message );
	    tcl_status = TCL_ERROR; goto return_;
	}

	//if( if_gpib_clear( addr ) == CAMP_FAILURE ) 
	//{
	//    tcl_status = TCL_ERROR; goto return_;
	//}

	if( pIF_t->procs.gpibClearProc != NULL )
	{
	    int camp_status = (*pIF_t->procs.gpibClearProc)( addr );

	    if( _failure( camp_status ) ) 
	    {
		// camp_snprintf( message, sizeof( message ), "%s: failed gpibClearProc", argv[0] );
		// Tcl_SetResult( interp, message, TCL_VOLATILE );
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed gpibClearProc" ); }
		tcl_status = TCL_ERROR; goto return_;
	    }
	}
	else
	{
	    // gpibClearProc not implemented for this interface type
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    tcl_status = TCL_OK;

return_:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" );
    // _mutex_lock_end();
    return( tcl_status );
}


/*
 *  Name:       camp_tcl_gpib_timeout
 *
 *  Purpose:    Set the gpib interface timeout
 *
 *              Called using command "gpibTimeout <if_type> <time>"
 *
 *              Calls the function if_gpib_timeout which should be implemented for
 *              each GPIB interface.
 *
 *  Provides:
 *              gpibTimeout
 *
 *  Called by:  Tcl interpreter
 * 
 *  Inputs:     Standard Tcl command processing parameters
 *
 *              timeout value in seconds
 *
 *  Preconditions:
 *              Initialization of a CAMP Tcl interpreter
 *
 *  Outputs:    Tcl status
 *
 *  Revision history:
 *    13-Dec-2000  TW  Creation
 *    20140403     TW  take if_type name, call gpibTimeoutProc
 *                     changed timeoutVal from int to float
 *    20140422     TW  this is redundant now that gpib interfaces have a timeout parameter
 */
int
camp_tcl_gpib_timeout( ClientData clientData, Tcl_Interp* interp,
                       int argc, char* argv[] )
{
    int tcl_status = TCL_OK;
    float timeoutVal;
    char message[LEN_MSG+1];
    char* endptr;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <if_type> <seconds>", argv[0] );
    if( argc < 3 )
    {
	usage( message );
	tcl_status = TCL_ERROR; goto return_;
    }

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	char* if_type_ident = argv[1];

	pIF_t = camp_ifGetpIF_t( if_type_ident /* , mutex_lock_sys_check() */ );
	if( pIF_t == NULL)
	{
	    _camp_setMsg( "invalid interface type '%s'", argv[1] );
	    tcl_status = TCL_ERROR; goto return_;
	}

	timeoutVal = strtod( argv[2], &endptr );
	if( isgraph( *endptr ) )
	{
	    usage( message );
	    tcl_status = TCL_ERROR; goto return_;
	}

	//if( if_gpib_timeout( timeoutVal ) == CAMP_FAILURE ) 
	//{
	//    tcl_status = TCL_ERROR; goto return_;
	//}

	if( pIF_t->procs.gpibTimeoutProc != NULL )
	{
	    int camp_status = (*pIF_t->procs.gpibTimeoutProc)( timeoutVal );

	    if( _failure( camp_status ) ) 
	    {
		if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed gpibTimeoutProc" ); }
		tcl_status = TCL_ERROR; goto return_;
	    }
	}
	else
	{
	    // gpibTimeoutProc not implemented for this interface type
	}
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    tcl_status = TCL_OK;

return_:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" );
    // _mutex_lock_end();
    return( tcl_status );
}


#ifdef VXWORKS

/*
camp_tcl_tip850
Purpose:  Interfaces tcl calls to the TIP850 DAC/ADC module
Inputs:   clientData - indentifies type of call DAC or ADC
          interp - pointer to tcl interpreter
	  argc - number of arguments passed to function
	  argv - array or pointers to char arguments
	  argv[0] - function name
	  argv[1] - position of industry pack
	  argv[2] - channel number
	  argv[3] - data passed to or returned from function
Precond:  The tcl interpreter must be successfully made
Outputs:  int - returns success or failure of function TCL_OK or TCL_ERROR
          Other outputs are returned in argv[3]
Postcond: The DAC value may be changed
Note:     It is unclear what some of the tests and error returns are for.
          These functions were copied from other undocumented code.
          They do not cause problems, and may be useful.
*/

int camp_tcl_tip850(ClientData clientData, Tcl_Interp * interp,
		    int argc, char * argv[])
{
    TIP850_DEV * tip850Dev;
    int iPosition;
    int iChannel;
    char message[LEN_MSG+1];
    char* endptr;
    char* pcData;
    int iData = 0;
    double dData;
    char buf[LEN_BUF+1];
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <position> <channel> <data>", argv[0] );
    if( argc < 4 )
    {
	usage( message );
	goto error;
    }

    /* Extract the position type and channel from the char array */

    iPosition = strtol( argv[1], &endptr, 0 );
    iChannel = strtol( argv[2], &endptr, 0 );

    /* select the function requested by caller
       Get the device pointer from the position and channel data. Determine the type from
       the clientData passed to routine. Write argv[3] or read and place in argv[3] */

    switch( (DACADC_PROCS)clientData )
    {
    case DAC_SET: 
	tip850Dev = tip850GetChannel( iPosition, TIP850_DAC, iChannel );

	if( ( pcData = Tcl_GetVar( interp, argv[3], 0 ) ) == NULL )
	{
	    // Tcl_AppendResult( interp, "no such variable '", argv[3], "'", NULL );
	    _camp_setMsg( "no such variable '%s'", argv[3] );
	    goto error;
	}
	
	iData = (int)strtol( pcData, &endptr, 0 );
	if( isgraph( *endptr) )
	{
	    usage( message );
	    goto error;
	}

	dData = (double)iData;
	tip850Ioctl( tip850Dev, TIP850_WRITE, &dData );
	break;

    case DAC_READ:
	tip850Dev = tip850GetChannel( iPosition, TIP850_DAC, iChannel );
	if( tip850Dev == NULL )
	{
	    goto error;
	}

	tip850Ioctl( tip850Dev, TIP850_READ, &dData );
	iData = (int)round( dData ); // 20140225  TW  round()
	Tcl_SetVar( interp, argv[3], camp_itoa( iData, buf, sizeof( buf ) ), 0 );
	break;

    case ADC_READ:
	tip850Dev = tip850GetChannel( iPosition, TIP850_ADC, iChannel );
	if( tip850Dev == NULL )
	{
	    goto error;
	}

	tip850Ioctl( tip850Dev, TIP850_READ, &dData );
	iData = (int)round( dData ); // 20140225  TW  round()
	Tcl_SetVar( interp, argv[3], camp_itoa( iData, buf, sizeof( buf ) ), 0 );
	break;

    case GAIN_SET: 
	tip850Dev = tip850GetChannel(iPosition, TIP850_ADC, iChannel);

	if( ( pcData = Tcl_GetVar( interp, argv[3], 0 ) ) == NULL )
	{
	    // Tcl_AppendResult( interp, "no such variable '", argv[3], "'", NULL );
	    _camp_setMsg( "no such variable '%s'", argv[3] );
	    goto error;
	}
	
	iData = (int)strtol( pcData, &endptr, 0 );
	if( isgraph( *endptr ) )
	{
	    usage( message );
	    goto error;
	}

	dData = (double)iData;
	tip850Ioctl( tip850Dev, TIP850_GAIN, &dData );
	break;
    }

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


/*
camp_tcl_te28
Purpose:  Interfaces tcl calls to the TE28 Servo Motor module
Inputs:   clientData - indentifies type of call 
          interp - pointer to tcl interpreter
	  argc - number of arguments passed to function
	  argv - array or pointers to char arguments
	  argv[0] - function name
	  argv[1] - position of industry pack
	  argv[2] - channel number
	  argv[3] - data passed to or returned from function
Precond:  The tcl interpreter must be successfully made
Outputs:  int - returns success or failure of function TCL_OK or TCL_ERROR
          Other outputs are returned in argv[3]
Postcond: The DAC value may be changed
Note:     It is unclear what some of the tests and error returns are for.
          These functions were copied from other undocumented code.
          They do not cause problems, and may be useful.
*/


int camp_tcl_te28(ClientData clientData, Tcl_Interp * interp,
		  int argc, char * argv[])
{
    TE28_DEV * te28Dev;
    int iPosition;
    int iChannel;
    char buf[LEN_BUF+1];
    char message[LEN_MSG+1];
    char * endptr;
    long lValue = 0;
    long llim = 0;
    int iTemp = 0;
    double dValue;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <position> <channel> <data>", argv[0] );
    if (argc < 4)
    {
	usage(message);
	goto error;
    }

    /* Extract the position and channel from the char array and get pointer*/

    iPosition = strtol(argv[1], &endptr, 0);
    iChannel = strtol(argv[2], &endptr, 0);
    te28Dev = te28GetChannel(iPosition, iChannel);
    if (te28Dev == NULL)
    {
	goto error;
    }

    /* select the function requested by caller
       Write value argv[3] or read and place result in argv[3] */

    switch((MOTOR_PROCS) clientData)
    {
    case MOTOR_RESET:
	te28Ioctl(te28Dev, TE28_RESET_AXIS, lValue);
	te28Set(te28Dev, TE28_ENABLE, 1);	 /* make sure the motor is enabled */
	te28Set(te28Dev, TE28_LIMITS, 0, 0);	/* Ignore default limit state with no connection */
	te28Set(te28Dev, TE28_LIMITS_ENABLE, 1);  /* Enable them but above makes them always not hit */
	break;

    case MOTOR_ENCODER:
	lValue = strtol(argv[3], &endptr, 0);
	if (isgraph(*endptr))
	{
	    usage(message);
	    goto error;
	}
	iTemp = TE28_ENCODER;
	te28Set(te28Dev, iTemp, lValue);
	break;

    case MOTOR_LIMIT:
	/*  0 to disable, neg for active low, pos for active high */
	lValue = strtol(argv[3], &endptr, 0);
	if (isgraph(*endptr))
	{
	    usage(message);
	    goto error;
	}
	llim = (lValue == 0 ? 0 : 1 );
	iTemp = TE28_LIMITS_ENABLE;
	te28Set(te28Dev, iTemp, llim);

	if ( llim == 0 ) break;

	llim = (lValue > 0 ? 1 : 0 );
	iTemp = TE28_LIMITS;
	te28Set(te28Dev, iTemp, llim);
	break;

    case MOTOR_FILTER_KP:
    case MOTOR_FILTER_KI:
    case MOTOR_FILTER_KD:
    case MOTOR_FILTER_IL:
    case MOTOR_FILTER_SI:
	lValue = strtol(argv[3], &endptr, 0);
	if (isgraph(*endptr))
	{
	    usage(message);
	    goto error;
	}

	switch((MOTOR_PROCS) clientData)
	{
	case MOTOR_FILTER_KP:
	    iTemp = TE28_FILTER_KP;
	    break;
	case MOTOR_FILTER_KI:
	    iTemp = TE28_FILTER_KI;
	    break;
	case MOTOR_FILTER_KD:
	    iTemp = TE28_FILTER_KD;
	    break;
	case MOTOR_FILTER_IL:
	    iTemp = TE28_FILTER_IL;
	    break;
	case MOTOR_FILTER_SI:
	    iTemp = TE28_FILTER_SI;
	    break;
	default:
	  break;
	}

	te28Ioctl(te28Dev, iTemp, lValue);
	break;

    case MOTOR_STOP:
	te28Ioctl(te28Dev, TE28_STOP_MOVE, lValue);
	break;
      
    case MOTOR_ABORT:
	te28Ioctl(te28Dev, TE28_ABORT_MOVE, lValue);
	break;

    case MOTOR_VEL:
    case MOTOR_ACC:
    case MOTOR_SLOPE:
    case MOTOR_OFFSET:
    case MOTOR_MOVE:
	dValue = strtod(argv[3], &endptr);
	if (isgraph(*endptr))
	{
	    usage(message);
	    goto error;
	}
	switch((MOTOR_PROCS) clientData)
	{
        case MOTOR_VEL:
	    iTemp = TE28_VELOCITY;
	    break;
        case MOTOR_ACC:
	    iTemp = TE28_ACCELERATION;
	    break;
        case MOTOR_SLOPE:
	    iTemp = TE28_SLOPE;
	    break;
        case MOTOR_OFFSET:
	    iTemp = TE28_OFFSET;
	    break;
	case MOTOR_MOVE:
	    iTemp = TE28_MOVE;
	    break;
	default:
	  break;
	}
	te28Set(te28Dev, iTemp, dValue);
	break;

    case MOTOR_POSITION:
	te28Get(te28Dev, TE28_POSITION, &dValue);

	camp_snprintf( buf, LEN_BUF+1, "%f", dValue );
	Tcl_SetVar( interp, argv[3], buf, 0 );
	break;
    }

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );
}


#endif /* VXWORKS */


int
camp_tcl_alarmTdMusr( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
#if defined( VMS )
    int flag;
    int b;
    int c;
    int cbsw_m_mon;
    long have_camac;
    char message[LEN_MSG+1];
    char* endptr;
    // _mutex_lock_begin();

    camp_snprintf( message, sizeof( message ), "%s <flag> <b> <c> <cbsw>", argv[0] );
    if( argc < 5 )
    {
        usage( message );
        goto error;
    }

    /*
     *  Test whether CAMAC is online
     */
    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	CAMP_IF_t* pIF_t;
	pIF_t = camp_ifGetpIF_t( "camac" /* , mutex_lock_sys_check() */ );
	have_camac = ( pIF_t != NULL ) ? pIF_t->priv : 0;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    flag = strtol( argv[1], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        goto error;
    }

    b = strtol( argv[2], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        goto error;
    }

    c = strtol( argv[3], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        goto error;
    }

    cbsw_m_mon = strtol( argv[4], &endptr, 0 );
    if( isgraph( *endptr ) )
    {
        usage( message );
        goto error;
    }

    if( _failure( cb_log_utils( &have_camac, &b, &c ) ) )
    {
        _camp_setMsg( "TD-MuSR not halted (failed init cb)" );
        goto error;
    }

    if( flag )
    {
        if( _failure( cb_log_set( &cbsw_m_mon ) ) )
	{
            _camp_setMsg( "TD-MuSR not halted (failed set cb)");
            goto error;
	}
    }
    else
    {
        if( _failure( cb_log_clr( &cbsw_m_mon ) ) )
	{
            _camp_setMsg( "TD-MuSR not resumed (failed clear cb)");
            goto error;
	}
    }

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_OK );

error:
    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE ); camp_setMsg( "" ); 
    // _mutex_lock_end();
    return( TCL_ERROR );

#else 

    /*
     *  Not implemented
     */
    Tcl_SetResult( interp, "TD-MuSR control by alarms not implemented on this platform", TCL_STATIC ); camp_setMsg( "" ); 
    return( TCL_ERROR );

#endif /* VMS */
}


int
camp_tcl_alarmIMusr( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    /*
     *  Not implemented
     */
    Tcl_SetResult( interp, "TI-MuSR control by alarms not implemented on this platform", TCL_STATIC ); camp_setMsg( "" ); 
    return( TCL_ERROR );
}


#ifdef RETIRED
static void  
camp_tcl_statmsg( Tcl_Interp *interp, int stat )
{
    char* msg;
    char cstat[32];

    switch( stat )
    {
    case CAMP_FAILURE:
        msg = "operation failed";
        break;
    case CAMP_INVAL_VAR:
        msg = "no such variable";
        break;
    case CAMP_INVAL_VAR_TYPE:
        msg = "invalid variable type";
        break;
    case CAMP_INVAL_VAR_ATTR:
        msg = "invalid variable attribute";
        break;
    case CAMP_INVAL_INS_TYPE:
        msg = "invalid instrument type";
        break;
    case CAMP_INVAL_IF:
        msg = "undefined interface";
        break;
    case CAMP_INVAL_SELECTION:
        msg = "invalid selection index";
        break;
    default:
	camp_snprintf( cstat, sizeof( cstat ), "failure (status: %u)", stat );
        msg = cstat;
        break;
    }

    Tcl_AppendResult( interp, msg, (char*)NULL );
}
#endif // RETIRED


void assemble_tcl_message( Tcl_Interp* interp, char* message, int message_size )
{
    char* perr;
    bool_t separator_needed = FALSE;

    if( message_size > 0 ) message[0] = '\0';
    
    if( _camp_debug( CAMP_DEBUG_TRACE ) )
    {
	perr = (char*)Tcl_GetVar( interp, "errorInfo", TCL_GLOBAL_ONLY );

	if( perr )
	{
	    camp_strncat( message, message_size, perr );
	    separator_needed = TRUE;
	}
    }

    if( interp->result[0] != '\0' )
    {
	if( separator_needed ) camp_strncat( message, message_size, " | " ); 

	camp_strncat( message, message_size, interp->result );
    }
}
