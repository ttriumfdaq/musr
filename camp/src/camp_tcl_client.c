/*
 *  Name:       camp_tcl_client.c
 *
 *  Purpose:    Provides an initialization procedure for Tcl so that
 *              a tclsh or wish executable can be built with access to
 *              the CAMP server by way of the command "camp_cmd".
 *
 *              This interface is functionally identical to the camp_cmd
 *              executable
 *
 *              The date command was added for convenience in the
 *              implementation of a CAMP client program.  It is not
 *              necessary to access the CAMP server.
 *
 *  Provides:
 *              camp_cmd
 *              camp_host
 *              date
 *
 *  Called by:  Tcl_AppInit or Tk_AppInit (tclsh or wish initialization code)
 * 
 *  Inputs:     Pointer to the Tcl interpreter to add the commands to.
 *
 *  Preconditions:
 *              Valid Tcl interpreter has been created and is ready for
 *              initialization.
 *
 *  Outputs:    Tcl status
 *
 *  Postconditions:
 *
 *  Revision history:
 *    14-Dec-1999  TW  Different ctime_r for Linux
 *    16-Dec-1999  TW  Use CAMP_RPC_CLNT_TIMEOUT with camp_clntInit
 *    19-May-2002  DA  Create camp_host command
 *
 *  $Log: camp_tcl_client.c,v $
 *  Revision 1.9  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.6  2009/04/08 01:53:33  asnd
 *  Cast away the warnings
 *
 *  Revision 1.5  2004/12/18 03:44:25  asnd
 *  Another test of failed connection
 *
 *  Revision 1.4  2002/11/17 08:15:13  asnd
 *  Re-connect to Camp server automatically.
 *
 *  Revision 1.3  2002/09/26 23:59:41  asnd
 *  Create camp_host command
 *
 */

#include <time.h>
#ifdef linux
#include <unistd.h> // gethostname
#endif /* Linux */

#include "tcl.h" 
#include "camp_clnt.h" // "camp.h"
// #include "timeval.h" // included by camp.h

static int
camp_tcl_cmd( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] );
static int
camp_tcl_host( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] );
static int
camp_tcl_date( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] );
static int call_camp_clntInit( Tcl_Interp* interp );

static int campTclServerConnected = 0 ;
static char campTclServerName[LEN_NODENAME+1] = { 0 };

extern enum clnt_stat camp_clnt_stat;


int
campTclClientInit( Tcl_Interp* interp )
{
    char* host;

    /*
     *  Host is CAMP_HOST environment variable
     *  or else local host if undefined
     */

    host = getenv( "CAMP_HOST" );
    if( host == NULL )
    {
	gethostname( campTclServerName, LEN_NODENAME );
    }
    else
    {
	camp_strncpy( campTclServerName, sizeof( campTclServerName ), host ); 
    }

    Tcl_CreateCommand( interp, "camp_host", (Tcl_CmdProc *)camp_tcl_host,
                        (ClientData)0, NULL );

    Tcl_CreateCommand( interp, "camp_cmd", (Tcl_CmdProc *)camp_tcl_cmd,
                        (ClientData)0, NULL );

    Tcl_CreateCommand( interp, "date", (Tcl_CmdProc *)camp_tcl_date,
                        (ClientData)0, NULL );

    if( strlen( campTclServerName ) == 0 )
    {
	campTclServerConnected = 0 ;
        return( TCL_OK );
    }

    return call_camp_clntInit( interp );
}


static int call_camp_clntInit( Tcl_Interp* interp )
{
    int camp_status;

    camp_setMsg( "" );

    camp_status = camp_clntInit( campTclServerName, CAMP_RPC_CLNT_TIMEOUT );

    if( _failure( camp_status ) )
    {
        Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE );
	camp_clntEnd();
	campTclServerConnected = 0;
        return( TCL_ERROR );
    }

    campTclServerConnected = 1;
    return( TCL_OK );
}


static int
camp_tcl_host( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int camp_status;

    if( argc < 2 )
    {
        Tcl_SetResult( interp, "usage: camp_host <server>", TCL_VOLATILE );
        return( TCL_ERROR );
    }

    camp_strncpy( campTclServerName, LEN_NODENAME+1, argv[1] ); 

    if( campTclServerConnected )
    {
	camp_clntEnd();
	campTclServerConnected = 0;
    }

    if( call_camp_clntInit( interp ) == TCL_ERROR )
    {
        return( TCL_ERROR );
    }

    Tcl_SetResult( interp, campTclServerName, TCL_VOLATILE );

    return( TCL_OK );
}

static int
camp_tcl_cmd( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    int camp_status;

    if( argc < 2 )
    {
        Tcl_SetResult( interp, "usage: camp_cmd <command>", TCL_VOLATILE );
        return( TCL_ERROR );
    }

    if( campTclServerConnected == 0 )
    {
        if( campTclServerName[0] == '\0' )
	{
	    Tcl_SetResult( interp, "invalid CAMP hostname", TCL_VOLATILE );
	    return( TCL_ERROR );
	}
    
	if( call_camp_clntInit( interp ) == TCL_ERROR )
	{
	    return( TCL_ERROR );
	}
    }
	
    camp_status = campSrv_cmd( argv[1] );

    if( camp_clnt_stat != RPC_SUCCESS && camp_clnt_stat != RPC_TIMEDOUT )
    {   /* RPC failure (worse than a timeout) -- Camp host has disconnected */
        campTclServerConnected = 0;
	camp_clntEnd();
	Tcl_SetResult( interp, "RPC failed. CAMP host does not respond", TCL_VOLATILE );
	return( TCL_ERROR );
    }
	
    if( _failure( camp_status ) )
    {
        Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE );
        return( TCL_ERROR );
    }

    Tcl_SetResult( interp, camp_getMsg(), TCL_VOLATILE );

    return( TCL_OK );
}


static int
camp_tcl_date( ClientData clientData, Tcl_Interp* interp,
                      int argc, char* argv[] )
{
    time_t currTime;
    char buf[32];
    size_t buf_size = 32;
    size_t buf_len;

    time( &currTime );
#ifdef linux
    ctime_r( &currTime, buf );
#else
    ctime_r( &currTime, buf, &buf_size ); 
#endif
     /*
     *  Strip newline character
     */
    buf_len = strlen(buf);
    if( buf_len > 0 ) buf[buf_len-1] = '\0';

    Tcl_SetResult( interp, buf, TCL_VOLATILE );

    return( TCL_OK );
}


