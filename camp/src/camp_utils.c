/*
 *  Name:       camp_utils.c
 *
 *  Purpose:    supplement libc_tw/timeval.c without changing it
 *
 *  Called by:  Various
 *
 *  Revision history:
 *    20140407  TW  
 *
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#if defined( linux )
#include <sys/time.h> // this include is temporary for builders whose timeval.h doesn't have this
#endif // linux
#include "timeval.h"

#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp.h" // for local prototypes
#endif /* RPC_SERVER */

double timeval_to_double( timeval_t* ptv )
{
  double t = ((double)ptv->tv_sec) + 1.0e-6*((double)ptv->tv_usec);
  if ( t != t ) {
    _camp_log( "timeval NaN for sec %ld usec %ld", (long)ptv->tv_sec, (long)ptv->tv_usec );
  }
    return ((double)ptv->tv_sec) + 1.0e-6*((double)ptv->tv_usec);
}

timeval_t double_to_timeval( double d )
{
    double integ, rem;
    timeval_t tv_out;

    rem = modf( d, &integ );

    tv_out.tv_sec  = (long)integ;
    tv_out.tv_usec = (long)(1.0e6*rem);

    return tv_out;
}

timeval_t addtimeval_double( timeval_t* ptv, double d )
{
    timeval_t tv_d;

    tv_d = double_to_timeval( d );

    return addtimeval( ptv, &tv_d );
}

timeval_t addtimeval( timeval_t* ptv1, timeval_t* ptv2 )
{
    timeval_t tv_out;

    tv_out.tv_sec  = ptv1->tv_sec  + ptv2->tv_sec;
    tv_out.tv_usec = ptv1->tv_usec + ptv2->tv_usec;

    return tv_out;
}

static void alloc_error( const char* function, size_t size )
{
    fprintf( stderr, "%s: memory allocation failed (size=%d)\n", function, (int)size );
    camp_log( "%s: memory allocation failed (size=%d)", function, (int)size );
    exit( CAMP_FAILURE );
}

caddr_t camp_zalloc( size_t size )
{
    caddr_t p = zalloc( size );

    if( p == NULL ) alloc_error( __FUNCTION__, size );

    return( p );
}

caddr_t camp_rezalloc( caddr_t ptr, size_t size )
{
    caddr_t p = rezalloc( ptr, size );

    if( p == NULL ) alloc_error( __FUNCTION__, size );

    return( p );
}

void* camp_malloc( size_t size )
{
    caddr_t p = malloc( size );

    if( p == NULL ) alloc_error( __FUNCTION__, size );

    return( p );
}

void* camp_calloc( size_t nmemb, size_t size )
{
    caddr_t p = calloc( nmemb, size );

    if( p == NULL ) alloc_error( __FUNCTION__, nmemb*size );

    return( p );
}

void* camp_realloc( void* ptr, size_t size )
{
    caddr_t p = realloc( ptr, size );

    if( p == NULL ) alloc_error( __FUNCTION__, size );

    return( p );
}
