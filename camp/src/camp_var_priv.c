/*
 *  Name:       camp_var_priv.c
 *
 *  Purpose:    Utilities to manage the CAMP variables
 *
 *              These routines are used only by the CAMP server.
 *
 *  Called by:  CAMP server routines only.
 * 
 *  Revision history:
 *  $Log: camp_var_priv.c,v $
 *  Revision 1.9  2015/12/10 03:50:02  asnd
 *  Improved some standard error messages
 *
 *  Revision 1.8  2015/04/21 04:20:17  asnd
 *  Make first polling-read wait for the poll interval (as before).
 *
 *  Revision 1.7  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.4  2006/05/06 06:17:28  asnd
 *  Fix selection-in-range test
 *
 *  Revision 1.3  2006/04/28 07:49:13  asnd
 *  Watch for invalid selection-variable indices
 *
 *  Revision 1.2  2001/07/19 02:22:35  asnd
 *  Make sure alarmAction gets switched off when an alarm is turned
 *  off or changed.
 *
 *
 */

#include <stddef.h>
#include <math.h>
//#define __USE_GNU // need this to get linux strndup ? but causes other clashes
#include <string.h>
#include "timeval.h"
#include "camp_srv.h"

static int var_set ( CAMP_VAR *pVar );
static int varNumSet ( CAMP_VAR *pVar , double val );
static int varNumSetTol ( CAMP_VAR *pVar , u_long tolType , float tol );
static int varSelSet ( CAMP_VAR *pVar , u_char val );
static int varSelSetLabel ( CAMP_VAR *pVar , char* label );
static int varStrSet ( CAMP_VAR *pVar , char* val );
static int varArrSet ( CAMP_VAR *pVar , caddr_t pVal );
static int varLnkSet ( CAMP_VAR *pVar , char* path );
static int varSetIsSet ( CAMP_VAR *pVar , bool_t flag );
static void varNumZeroStats ( CAMP_NUMERIC *pNum );
static int  varDoStats ( CAMP_VAR *pVar );
static void varNumDoStats ( CAMP_NUMERIC *pNum );


      /*----------------------------------------*/
     /*   Data setting routines                */
    /*----------------------------------------*/

/*
 *  varRead
 *
 *  Calls the variable's instrument readProc.  This eventually calls the
 *  variable's Tcl "readProc" in the instrument driver (which calls
 *  insIfRead, which calls camp_ifRead, which calls if_*_read)
 */
int
varRead( CAMP_VAR* pVar )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_INSTRUMENT* pIns;
    CAMP_VAR* pVar_ins;
    int (*insType_readProc)( struct CAMP_VAR*, struct CAMP_VAR* ) = NULL;
    // _mutex_lock_begin();

    pVar_ins = varGetpIns( pVar );
    if( pVar_ins == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_PARENT, pVar->core.path );
        camp_status = CAMP_INVAL_VAR; goto return_;
    }

    pIns = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns;
    if( pIns->pIF == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF, pVar_ins->core.path );
        camp_status = CAMP_INVAL_IF;	goto return_;
    }

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	INS_TYPE* pInsType;
	pInsType = camp_sysGetpInsType( pIns->typeIdent /* , mutex_lock_sys_check() */ );
	if( pInsType == NULL ) 
	{
	    _camp_setMsgStat( CAMP_INVAL_INS_TYPE, pVar_ins->core.path, pIns->typeIdent );
	    camp_status = CAMP_INVAL_INS_TYPE; goto return_;
	}

	insType_readProc = pInsType->procs.readProc;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    set_current_ident( pVar_ins->core.ident );

    if( insType_readProc != NULL )
    {
	camp_status = (*insType_readProc)( pVar_ins, pVar ); // always ins_tcl_read
	if( _failure( camp_status ) )
	{
 	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed ins_tcl_read (var='%s')", pVar->core.path ); }
	    goto return_;
	}
    }

    camp_status = CAMP_SUCCESS;

return_:
    gettimeval( &pVar->core.timeLastReadAttempt );
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  varSetReq
 *
 *  Request to set a variable to the contents of pSpec.  Calls the
 *  variable's instrument setProc, which eventually call the variable's
 *  "-writeProc" in the Tcl instrument driver.
 */
int
varSetReq( CAMP_VAR* pVar, CAMP_VAR_SPEC* pSpec )
{
    int camp_status = CAMP_SUCCESS;
    CAMP_INSTRUMENT* pIns;
    CAMP_VAR* pVar_ins;
    int (*insType_setProc)( struct CAMP_VAR*, struct CAMP_VAR*, struct CAMP_VAR_SPEC* ) = NULL;
    // _mutex_lock_begin();

    if( pSpec == NULL ) 
    {
        camp_status = CAMP_FAILURE; goto return_;
    }

    switch( pSpec->varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        varNumSetTol( pVar, pSpec->CAMP_VAR_SPEC_u.pNum->tolType, pSpec->CAMP_VAR_SPEC_u.pNum->tol );
        break;
      default:
        break;
    }

    pVar_ins = varGetpIns( pVar );
    if( pVar_ins == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_PARENT, pVar->core.path );
        camp_status = CAMP_INVAL_VAR; goto return_;
    }

    pIns = pVar_ins->spec.CAMP_VAR_SPEC_u.pIns;
    if( pIns->pIF == NULL ) 
    {
        _camp_setMsgStat( CAMP_INVAL_IF, pVar_ins->core.path );
        camp_status = CAMP_INVAL_IF; goto return_;
    }

    // _mutex_lock_sys_on(); // warning: don't return inside mutex lock
    {
	INS_TYPE* pInsType;
	pInsType = camp_sysGetpInsType( pIns->typeIdent /* , mutex_lock_sys_check() */ );
	if( pInsType == NULL ) 
	{
            _camp_setMsgStat( CAMP_INVAL_INS_TYPE, pVar_ins->core.path, pIns->typeIdent );
	    camp_status = CAMP_INVAL_INS_TYPE; goto return_;
	}

	insType_setProc = pInsType->procs.setProc;
    }
    // _mutex_lock_sys_off(); // warning: don't return inside mutex lock

    set_current_ident( pVar_ins->core.ident );

    if( insType_setProc != NULL )
    {
	camp_status = (*insType_setProc)( pVar_ins, pVar, pSpec ); // always ins_tcl_set
	if( _failure( camp_status ) )
	{
	    if( _camp_debug(CAMP_DEBUG_TRACE) ) { _camp_appendMsg( "failed ins_tcl_set (var='%s')", pVar->core.path ); }
	    goto return_;
	}
    }
    camp_status = CAMP_SUCCESS;

return_:
    gettimeval( &pVar->core.timeLastSetAttempt );
    // _mutex_lock_end();
    return( camp_status );
}


/*
 *  varSetSpec
 *
 *  Actually set the variable to the applicable contents of pSpec.
 *  This is what is called by a varDoSet command.  It does a hard set of
 *  the value in the CAMP database without any instrument communication.
 */
int 
varSetSpec( CAMP_VAR* pVar, CAMP_VAR_SPEC* pSpec )
{
    int camp_status = CAMP_SUCCESS;

    if( pSpec == NULL ) 
    {
	return( CAMP_FAILURE );
    }

    switch( pSpec->varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        varNumSetTol( pVar, pSpec->CAMP_VAR_SPEC_u.pNum->tolType, pSpec->CAMP_VAR_SPEC_u.pNum->tol );
        camp_status = varNumSet( pVar, pSpec->CAMP_VAR_SPEC_u.pNum->val );
        break;
      case CAMP_VAR_TYPE_SELECTION:
        if( pSpec->CAMP_VAR_SPEC_u.pSel->pItems != NULL )
        {
            camp_status = varSelSetLabel( pVar, pSpec->CAMP_VAR_SPEC_u.pSel->pItems->label );
        }
        else
        {
            camp_status = varSelSet( pVar, pSpec->CAMP_VAR_SPEC_u.pSel->val );
        }
        break;
      case CAMP_VAR_TYPE_STRING:
        camp_status = varStrSet( pVar, pSpec->CAMP_VAR_SPEC_u.pStr->val );
        break;
      case CAMP_VAR_TYPE_ARRAY:
        camp_status = varArrSet( pVar, pSpec->CAMP_VAR_SPEC_u.pArr->pVal );
        break;
      case CAMP_VAR_TYPE_STRUCTURE:
        break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        break;
      case CAMP_VAR_TYPE_LINK:
        camp_status = varLnkSet( pVar, pSpec->CAMP_VAR_SPEC_u.pLnk->path );
        break;
      default:
        camp_status = CAMP_INVAL_VAR_TYPE;
	break;
    }

    return( camp_status );
}


/*
 *  var_set
 *
 *  Update general "set" status of a variable
 */
static int
var_set( CAMP_VAR* pVar )
{
    varSetIsSet( pVar, TRUE );
    gettimeval( &pVar->core.timeLastSet );

    return( CAMP_SUCCESS );
}


int
varNumSet( CAMP_VAR* pVar, double val )
{
    CAMP_NUMERIC* pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

    pNum->val = val;

    var_set( pVar );
    varDoStats( pVar );

    return( CAMP_SUCCESS );
}


int
varNumSetTol( CAMP_VAR* pVar, u_long tolType, float tol )
{
    CAMP_NUMERIC* pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

    pNum->tol = tol;
    pNum->tolType = tolType;

    return( CAMP_SUCCESS );
}


int
varSelSet( CAMP_VAR* pVar, u_char val )
{
    CAMP_SELECTION* pSpec = pVar->spec.CAMP_VAR_SPEC_u.pSel;

    if( val >= pSpec->num ) 
    {
        _camp_setMsgStat( CAMP_INVAL_SELECTION, pVar->core.path );
        return( CAMP_INVAL_SELECTION );
    }

    pSpec->val = val;

    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varSelSetLabel( CAMP_VAR* pVar, char* label )
{
    int camp_status;
    u_char val;

    camp_status = varSelGetLabelID( pVar, label, &val );
    if( _failure( camp_status ) ) 
    {
	return( camp_status );
    }

    camp_status = varSelSet( pVar, val );

    return( camp_status );
}


int
varStrSet( CAMP_VAR* pVar, char* val )
{
    CAMP_STRING* pSpec = pVar->spec.CAMP_VAR_SPEC_u.pStr;

    if( val != NULL && pSpec->val != val )
    {
        camp_strdup( &pSpec->val, val ); // strndup( val, LEN_STRING ); // free and strdup
    }
    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varArrSet( CAMP_VAR* pVar, caddr_t pVal )
{
    CAMP_ARRAY* pSpec = pVar->spec.CAMP_VAR_SPEC_u.pArr;

    bcopy( pVal, pSpec->pVal, (pSpec->totElem)*(pSpec->elemSize) );

    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varLnkSet( CAMP_VAR* pVar, char* path )
{
    CAMP_LINK* pSpec = pVar->spec.CAMP_VAR_SPEC_u.pLnk;

    if( path != NULL && pSpec->path != path )
    {
        camp_strdup( &pSpec->path, path ); // strndup( path, LEN_PATH ); // free and strdup
    }
    var_set( pVar );

    return( CAMP_SUCCESS );
}


int
varSetShow( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_SHOW ) )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR_ATTR, "-d", pVar->core.path );
        return( CAMP_INVAL_VAR_ATTR );
    }

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_SHOW;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_SHOW;

    return( CAMP_SUCCESS );
}


int
varSetSet( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_SET ) )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR_ATTR, "-s", pVar->core.path );
        return( CAMP_INVAL_VAR_ATTR );
    }

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_SET;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_SET;

    return( CAMP_SUCCESS );
}


int
varSetRead( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_READ ) )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR_ATTR, "-r", pVar->core.path );
        return( CAMP_INVAL_VAR_ATTR );
    }

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_READ;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_READ;

    return( CAMP_SUCCESS );
}


int
varSetPoll( CAMP_VAR* pVar, bool_t flag, float interval )
{
    int camp_status = CAMP_SUCCESS;
    bool_t wasPolled;

    if( !( pVar->core.attributes & CAMP_VAR_ATTR_POLL ) )
    {
      _camp_setMsgStat( CAMP_INVAL_VAR_ATTR, "-p", pVar->core.path );
        camp_status = CAMP_INVAL_VAR_ATTR;
	goto return_;
    }

    wasPolled = ( pVar->core.status & CAMP_VAR_ATTR_POLL );

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_POLL;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_POLL;

    /*
     *  For a long time, interval<=0 has been a flag to retain old value.  Who uses
     *  that hack/feature? DA wants to make negative intervals indicate that the
     *  first poll is to be delayed; otherwise the first reading is done asap.
     *  Until 2014, the first poll was scheduled to occur after the interval, but 
     *  in 2014, the first poll began to be done immediately.  Unfortunately, some 
     *  uses of "polling" are to perform an action after a specified delay, and that 
     *  use got broken.
     *  As a (probably temporary) fix, set the timeLastReadAttempt whenever 
     *  polling switches on.  For the future, DA would like to make negative
     *  intervals imply poll-after-delay and positive intervals mean first-poll-now,
     *  but that conflicts with previous meaning of interval<=0. 
     */

    if( interval > 0.0 ) pVar->core.pollInterval = interval; 

    if( !wasPolled && flag )
    { /* Polling just started. Record last-read as "now" so as to delay first poll-reading */
        gettimeval( &pVar->core.timeLastReadAttempt );
    }

    // 20140303  TW  was this test really useful here? perhaps pVar_ins was being used in the past
    // (No, TW added the test recently)
    // CAMP_VAR* pVar_ins;
    // pVar_ins = varGetpIns( pVar );
    // if( pVar_ins == NULL )
    // {
    //   camp_status = CAMP_INVAL_VAR;
    //   goto return_;
    // }

    /* old polling management
     * if( !wasPolled && flag )
     * {
     *     REQ_addPoll( pVar );
     * }
     * else if( wasPolled && !flag )
     * {
     *     REQ_cancel( pVar, IO_POLL );
     * }
     */

return_:
    return( camp_status );
}


int
varSetAlarm( CAMP_VAR* pVar, bool_t flag, char* alarmAction )
{
    bool_t aaflag = FALSE;

    if( !( pVar->core.attributes & CAMP_VAR_ATTR_ALARM ) )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR_ATTR, "-a", pVar->core.path );
        return( CAMP_INVAL_VAR_ATTR );
    }

    /*
     *  First, turn off any existing alert if flag == FALSE  OR  if the
     *  alarmProc is changed from previous. (Otherwise, we could leave
     *  the previous alarmAction in the ON state.)
     */
    if( pVar->core.alarmAction != NULL )
    {
	if( alarmAction == NULL )
	{
	    aaflag = TRUE;
	}
	else
	{
	    aaflag = !streq( pVar->core.alarmAction, alarmAction) ;
	}
    }

    if( aaflag || !flag )
    {
	varSetAlert( pVar, FALSE );
    }

    /*
     *  Replace variable's alarm action with the one given.
     */
    if( alarmAction != NULL && alarmAction != pVar->core.alarmAction )
    {
        camp_strdup( &pVar->core.alarmAction, alarmAction ); // strndup( alarmAction, LEN_PROC ); // free and strdup
    }

    /*
     *  Finally, set the variable's alarm status bit (on/off)
     */
    if( flag ) 
    {
        pVar->core.status |= CAMP_VAR_ATTR_ALARM;
    }
    else
    { 
        pVar->core.status &= ~CAMP_VAR_ATTR_ALARM;
    }

    return( CAMP_SUCCESS );
}


int
varSetLog( CAMP_VAR* pVar, bool_t flag, char* logAction )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_LOG ) )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR_ATTR, "-l", pVar->core.path );
        return( CAMP_INVAL_VAR_ATTR );
    }

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_LOG;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_LOG;

    if( logAction != NULL && logAction != pVar->core.logAction )
    {
        camp_strdup( &pVar->core.logAction, logAction ); // strndup( logAction, LEN_PROC ); // free and strdup
    }

    return( CAMP_SUCCESS );
}


/*
 *  TRUE = within tolerance, alert off
 *  FALSE = out of tolerance, alert on
 */
bool_t
varNumTestAlert( CAMP_VAR* pVar, double val )
{
    if( !( pVar->core.status & CAMP_VAR_ATTR_ALARM ) )
    {
        return( TRUE );
    }

    if( camp_varNumTestTol( pVar->core.path, val ) )
    {
        varSetAlert( pVar, FALSE );
        return( TRUE );
    }
    else
    {
        varSetAlert( pVar, TRUE );
        return( FALSE );
    }
}

/*
 *  varSetAlert
 *
 *  Sets a variable's alert status on/off, then calls camp_alarmCheck
 *  to test whether the alarm associated with that variable should 
 *  also change status.
 *
 *  Called from: varSetAlarm - set off when changing alarm flag to off
 *               varNumTestAlert - test alert using varTestAlert command
 *               camp_tcl_varSet - set alert explicitly using varDoSet command
 */
int
varSetAlert( CAMP_VAR* pVar, bool_t flag )
{
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_ALARM ) )
    {
        _camp_setMsgStat( CAMP_INVAL_VAR_ATTR, "alert", pVar->core.path );
        return( CAMP_INVAL_VAR_ATTR );
    }

    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_ALERT;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_ALERT;

    // camp_alarmCheck( pVar->core.alarmAction ); // 20140305  TW  main thread checks alarm status, each processing loop

    return( CAMP_SUCCESS );
}


int
varSetIsSet( CAMP_VAR* pVar, bool_t flag )
{
/*
    if( !( pVar->core.attributes & CAMP_VAR_ATTR_SET ) &&
        !( pVar->core.attributes & CAMP_VAR_ATTR_READ ) )
        return( CAMP_INVAL_VAR_ATTR );
*/
    if( flag ) pVar->core.status |= CAMP_VAR_ATTR_IS_SET;
    else       pVar->core.status &= ~CAMP_VAR_ATTR_IS_SET;

    return( CAMP_SUCCESS );
}


int
varSetMsg( CAMP_VAR* pVar, char* msg )
{
    if( msg != NULL && pVar->core.statusMsg != msg ) 
    {
        camp_strdup( &pVar->core.statusMsg, msg ); // strndup( msg, LEN_STATUS_MSG ); // free and strdup
    }

    return( CAMP_SUCCESS );
}


int
varZeroStats( CAMP_VAR* pVar )
{
    if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) == CAMP_VAR_TYPE_NUMERIC )
    {
        varNumZeroStats( pVar->spec.CAMP_VAR_SPEC_u.pNum );
    }

    if( pVar->pChild != NULL ) 
    {
        for( pVar = pVar->pChild; pVar != NULL; pVar = pVar->pNext )
        {
            varZeroStats( pVar );
        }
    }

    return( CAMP_SUCCESS );
}


void 
varNumZeroStats( CAMP_NUMERIC* pNum )
{
    cleartimeval( &pNum->timeStarted );
    pNum->num = 0;
    pNum->low = 0.0;
    pNum->hi = 0.0;
    pNum->sum = 0.0;
    pNum->sumSquares = 0.0;
    pNum->sumCubes = 0.0;
    pNum->offset = 0.0;
}


int
varDoStats( CAMP_VAR* pVar )
{
    if( ( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) == CAMP_VAR_TYPE_NUMERIC )
    {
        varNumDoStats( pVar->spec.CAMP_VAR_SPEC_u.pNum );
    }

    return( CAMP_SUCCESS );
}


void 
varNumDoStats( CAMP_NUMERIC* pNum )
{
    double delta;
    timeval_t tv_zero = { 0, 0 };

    if( difftimeval( &pNum->timeStarted, &tv_zero ) == 0.0 )
    {
        /*
	 *  Stats were zeroed
	 */
        gettimeval( &pNum->timeStarted );
        pNum->offset = pNum->hi = pNum->low = pNum->val;
    }
    (pNum->num)++;
    pNum->low = _min( pNum->low, pNum->val );
    pNum->hi = _max( pNum->hi, pNum->val );
    delta = pNum->val - pNum->offset;
    pNum->sum += delta;
    pNum->sumSquares += pow( delta, (double)2.0 );
    pNum->sumCubes += pow( delta, (double)3.0 );
}


      /*----------------------------------------*/
     /*  Data deallocation                     */
    /*----------------------------------------*/


int
varAdd( char* path, CAMP_VAR* pVar )
{
    CAMP_VAR** ppVar_start;
    char parentPath[LEN_PATH+1];
    char* p;
    int camp_status = CAMP_SUCCESS;

    camp_strncpy( parentPath, sizeof( parentPath ), path ); // strcpy( parentPath, path );
    p = strrchr( parentPath, '/' );
    *p = '\0';

    // printf( "varAdd: ident: '%s', path: '%s', add path: '%s', parentPath: '%s'\n", pVar->core.ident, pVar->core.path, path, parentPath );

    /*
     *  find the front of the list we're appending to
     */

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock,
    {
	if( parentPath[0] == '\0' )
	{
	    ppVar_start = &pVarList;
	}
	else
	{
	    ppVar_start = camp_varGetpp( parentPath /* , mutex_lock_varlist_check() */ );
	    if( ppVar_start != NULL ) 
	    {
		ppVar_start = &(*ppVar_start)->pChild;
	    }
	}

	if( ppVar_start == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, parentPath );
	}
	else
	{
	    CAMP_VAR** ppVar;

	    // if( *ppVar_start != NULL )
	    // printf( "    varAdd: var start: ident: '%s', path: '%s'\n", (*ppVar_start)->core.ident, (*ppVar_start)->core.path );

	    /*
	     *  find the end of the list we're appending to
	     */

	    for( ppVar = ppVar_start; *ppVar != NULL; ppVar = &(*ppVar)->pNext ) ;

	    /*
	     *  append to the end of the list
	     */

	    *ppVar = pVar;
	    (*ppVar)->pNext = NULL;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}
