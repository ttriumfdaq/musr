/*
 *  Name:       camp_var_utils.c
 *
 *  Purpose:    Utilities to access variable information in the CAMP
 *              database.
 *
 *              FORTRAN wrappers are provided for essential routines using
 *              the cfortran interface.
 *
 *  Called by:  CAMP server and client routines.
 * 
 *  Revision history:
 *
 *  DA   31-Jan-2000   allow camp_varGetpp to get an unlinked link variable.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include "timeval.h"
#ifdef RPC_SERVER
#include "camp_srv.h"
#else
#include "camp_clnt.h" // "camp.h" // 20140226  TW 
#endif /* RPC_SERVER */

#if !defined( RPC_SERVER ) || !CAMP_MULTITHREADED
static char current_ident[LEN_IDENT+1] = "";
#define get_thread_ident() current_ident
#define set_thread_ident( str ) camp_strncpy( current_ident, LEN_IDENT+1, str );
#endif /* !RPC_SERVER || !CAMP_MULTITHREADED */


char*
get_current_ident( void )
{
    return( get_thread_ident() );
}


void
set_current_ident( const char* ident )
{
    set_thread_ident( ident );
}


      /*---------------------------*/
     /*   Data access routines    */
    /*---------------------------*/

void
camp_varDoProc_recursive( void (*proc)( CAMP_VAR* ), CAMP_VAR* pVar_start )
{
    CAMP_VAR* pVar;

    for( pVar = pVar_start; pVar != NULL; pVar = pVar->pNext )
    {
        proc( pVar );
        if( pVar->pChild != NULL ) 
	{
	    camp_varDoProc_recursive( proc, pVar->pChild );
	}
    }
}


CAMP_VAR*
camp_varGetPHead( const char* path /* , bool_t _mutex_lock_varlist_check */ )
{
    char path_up[LEN_PATH+1];
    CAMP_VAR* pVar_up;

    camp_strncpy( path_up, sizeof( path_up ), path ); // strcpy( path_up, path );
    camp_pathUp( path_up );

    if( camp_pathAtTop( path_up ) )
    {
	return( pVarList );
    }
    else
    {
	pVar_up = camp_varGetp( path_up /* , _mutex_lock_varlist_check */ );
	if( pVar_up == NULL ) return( NULL );

	return( pVar_up->pChild );
    }
}


CAMP_VAR** 
camp_varGetpp( const char* path_req /* , bool_t _mutex_lock_varlist_check */ )
{
    char path_curr[LEN_PATH+1];
    char ident_next[LEN_IDENT+1];
    char ident_search[LEN_IDENT+1];
    CAMP_VAR** ppVar;
    CAMP_VAR** ppVarL;

    camp_pathInit( path_curr, LEN_PATH+1 );

    for( ppVar = &pVarList; *ppVar != NULL; ppVar = &(*ppVar)->pChild )
    {
	if( camp_pathGetNext( path_req, path_curr, ident_next, LEN_IDENT+1 ) == NULL ) 
        {
	    return( NULL );
        }

	if( strlen( ident_next ) == 0 ) 
        {
            return( NULL );
        }
	else if( streq( ident_next, "~" ) ) 
        {
	    camp_strncpy( ident_search, sizeof( ident_search ), get_current_ident() );
        }
	else 
        {
	    camp_strncpy( ident_search, sizeof( ident_search ), ident_next );
        }

	/*
	 *  Traverse current level of the tree
	 */
	for( ; *ppVar != NULL; ppVar = &(*ppVar)->pNext )
        {
	    if( streq( ident_search, (*ppVar)->core.ident ) ) 
            {
		break;
            }
        }

	if( *ppVar == NULL ) 
        {
            return( NULL );
        }

	if( (*ppVar)->core.varType == CAMP_VAR_TYPE_LINK )
	{
 	    /* DA: only follow defined links */
	    if( strcmp( (*ppVar)->spec.CAMP_VAR_SPEC_u.pLnk->path, "") != 0 )
	    {
	        ppVarL = camp_varGetpp( (*ppVar)->spec.CAMP_VAR_SPEC_u.pLnk->path /* , _mutex_lock_varlist_check */ );
		if( ppVarL != NULL ) 
		{
		    if( *ppVarL != NULL ) 
		    {
		        ppVar = ppVarL;
		    }
		}
	    }
	}

	if( camp_pathDown( path_curr, LEN_PATH+1, ident_next ) == NULL ) 
        {
            return( NULL );
        }

	if( streq( path_req, path_curr ) ) 
        {
            return( ppVar );
        }
    }

    return( NULL );
}


CAMP_VAR* 
camp_varGetp( const char* path /* , bool_t _mutex_lock_varlist_check */ )
{
    CAMP_VAR** ppVar;

    ppVar = camp_varGetpp( path /* , _mutex_lock_varlist_check */ );

    return( ( ppVar == NULL ) ? NULL : *ppVar );
}


CAMP_VAR** 
camp_varGetTruePP( const char* path_req /* , bool_t _mutex_lock_varlist_check */ )
{
    char path_curr[LEN_PATH+1];
    char ident_next[LEN_IDENT+1];
    char ident_search[LEN_IDENT+1];
    CAMP_VAR** ppVar;

    camp_pathInit( path_curr, LEN_PATH+1 );

    for( ppVar = &pVarList; *ppVar != NULL; ppVar = &(*ppVar)->pChild )
    {
	if( camp_pathGetNext( path_req, path_curr, ident_next, LEN_IDENT+1 ) == NULL ) 
        {
	    return( NULL );
        }

	if( strlen( ident_next ) == 0 ) 
        {
            return( NULL );
        }
	else if( streq( ident_next, "~" ) ) 
        {
	    camp_strncpy( ident_search, sizeof( ident_search ), get_current_ident() );
        }
	else 
        {
	    camp_strncpy( ident_search, sizeof( ident_search ), ident_next );
        }

	/*
	 *  Traverse current level of the tree
	 */
	for( ; *ppVar != NULL; ppVar = &(*ppVar)->pNext )
        {
	    if( streq( ident_search, (*ppVar)->core.ident ) ) 
            {
		break;
            }
        }

	if( *ppVar == NULL ) 
        {
            return( NULL );
        }

	camp_pathDown( path_curr, LEN_PATH+1, ident_next );

	/*
	 *  Return if this is the one we're looking for
	 */
	if( streq( path_req, path_curr ) ) 
        {
            return( ppVar );
        }

	/*
	 *  The current var is not the one we're looking for
	 *  but it may be a Link, so check and expand
	 */
	if( (*ppVar)->core.varType == CAMP_VAR_TYPE_LINK )
	{
	    ppVar = camp_varGetpp( (*ppVar)->spec.CAMP_VAR_SPEC_u.pLnk->path /* , _mutex_lock_varlist_check */ );
	    if( *ppVar == NULL ) 
            {
                return( NULL );
            }
	}
    }

    return( NULL );
}


CAMP_VAR* 
camp_varGetTrueP( const char* path /* , bool_t _mutex_lock_varlist_check */ )
{
    CAMP_VAR** ppVar;

    ppVar = camp_varGetTruePP( path /* , _mutex_lock_varlist_check */ );

    return( ( ppVar == NULL ) ? NULL : *ppVar );
}


/*
 *  camp_varGetpIns  -  get pointer to a variable's parent
 *                      instrument variable
 */
CAMP_VAR*
camp_varGetpIns( const char* path /* , bool_t _mutex_lock_varlist_check */ )
{
    CAMP_VAR* pVar;

    pVar = camp_varGetp( path /* , _mutex_lock_varlist_check */ );
    if( pVar == NULL ) return( NULL );

    return( varGetpIns( pVar ) );
}


/*
 *  varGetpIns  -  get pointer to a variable's parent
 *                 instrument variable
 *
 *  don't need _mutex_lock_varlist_check: already have mutex to have pVar_start
 */
CAMP_VAR*
varGetpIns( CAMP_VAR* pVar_start )
{
    CAMP_VAR* pVar;
    char path[LEN_PATH+1];

    if( pVar_start->core.varType == CAMP_VAR_TYPE_INSTRUMENT )
    {
	return( pVar_start );
    }

    camp_strncpy( path, sizeof( path ), pVar_start->core.path ); // strcpy( path, pVar_start->core.path );
    camp_pathUp( path );

    /*
     *  Traverse up the tree,
     *  return the first instrument found
     */
    for( ; !camp_pathAtTop( path ); camp_pathUp( path ) )
    {
        pVar = camp_varGetp( path /* , TRUE */ ); // already locked

        if( pVar->core.varType == CAMP_VAR_TYPE_INSTRUMENT )
        {
            return( pVar );
        }
    }

    return( NULL );
}


bool_t
camp_varExists( const char* path )
{
    bool_t status;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );
	status = ( pVar != NULL ) ? TRUE : FALSE;
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( status );
}


int
camp_varGetIdent( const char* path, char* ident, size_t ident_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    camp_strncpy( ident, ident_size, pVar->core.ident );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetType( const char* path, CAMP_VAR_TYPE* pVarType )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    *pVarType = pVar->core.varType;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetAttributes( const char* path, u_long* pAttributes )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    *pAttributes = pVar->core.attributes;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetTitle( const char* path, char* title, size_t title_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    camp_strncpy( title, title_size, pVar->core.title ); 
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetHelp( const char* path, char* help, size_t help_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    camp_strncpy( help, help_size, pVar->core.help ); 
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetStatus( const char* path, u_long* pStatus )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    *pStatus = pVar->core.status;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetStatusMsg( const char* path, char* msg, size_t msg_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    camp_strncpy( msg, msg_size, pVar->core.statusMsg ); 
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetPoll( const char* path, bool_t* pFlag, float* pInterval )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    *pFlag = ( pVar->core.status & CAMP_VAR_ATTR_POLL ) ? TRUE : FALSE;
	    *pInterval = pVar->core.pollInterval;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetAlarm( const char* path, bool_t* pFlag, char* action, size_t action_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    *pFlag = ( pVar->core.status & CAMP_VAR_ATTR_ALARM ) ? TRUE : FALSE;
	    camp_strncpy( action, action_size, pVar->core.alarmAction ); 
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetLog( const char* path, bool_t* pFlag, char* action, size_t action_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    *pFlag = ( pVar->core.status & CAMP_VAR_ATTR_LOG ) ? TRUE : FALSE;
	    camp_strncpy( action, action_size, pVar->core.logAction ); 
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetValStr( const char* path, char* str, size_t str_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
	    {
	    case CAMP_VAR_TYPE_NUMERIC:
		camp_status = varNumGetValStr( pVar, str, str_size );
		break;
	    case CAMP_VAR_TYPE_STRING:
		camp_strncpy( str, str_size, pVar->spec.CAMP_VAR_SPEC_u.pStr->val ); 
		break;
	    case CAMP_VAR_TYPE_SELECTION:
		camp_status = varSelGetIDLabel( pVar, pVar->spec.CAMP_VAR_SPEC_u.pSel->val, str, str_size );
		break;
	    default:
		camp_status = CAMP_INVAL_VAR;
		_camp_setMsgStat( CAMP_INVAL_VAR, path );
		break;
	    }
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varGetTypeStr( const char* path, char* str, size_t str_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    switch( pVar->core.varType )
	    {
	    case CAMP_VAR_TYPE_INT:
		camp_strncpy( str, str_size, toktostr( TOK_INT ) ); 
		break;
	    case CAMP_VAR_TYPE_FLOAT:
		camp_strncpy( str, str_size, toktostr( TOK_FLOAT ) ); 
		break;
	    case CAMP_VAR_TYPE_SELECTION:
		camp_strncpy( str, str_size, toktostr( TOK_SELECTION ) ); 
		break;
	    case CAMP_VAR_TYPE_STRING:
		camp_strncpy( str, str_size, toktostr( TOK_STRING ) ); 
		break;
	    case CAMP_VAR_TYPE_ARRAY:
		camp_strncpy( str, str_size, toktostr( TOK_ARRAY ) ); 
		break;
	    case CAMP_VAR_TYPE_STRUCTURE:
		camp_strncpy( str, str_size, toktostr( TOK_STRUCTURE ) ); 
		break;
	    case CAMP_VAR_TYPE_INSTRUMENT:
		camp_strncpy( str, str_size, toktostr( TOK_INSTRUMENT ) ); 
		break;
	    case CAMP_VAR_TYPE_LINK:
		camp_strncpy( str, str_size, toktostr( TOK_LINK ) ); 
		break;
	    default:
		camp_status = CAMP_INVAL_VAR;
		_camp_setMsgStat( CAMP_INVAL_VAR, path );
		break;
	    }
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varNumGetVal( const char* path, double* pVal )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_NUMERIC* pNum;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;
	    *pVal = pNum->val;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
varNumGetValStr( CAMP_VAR* pVar, char* str, size_t str_size )
{
    return( ( numGetValStr( pVar->core.varType, 
	    pVar->spec.CAMP_VAR_SPEC_u.pNum->val, str, str_size ) ) ? 
	    CAMP_SUCCESS : CAMP_FAILURE );
}

bool_t
numGetValStr( CAMP_VAR_TYPE varType, double val, char* str, size_t str_size )
{
    int exp;
    int prec;
#define STRING_PRECISION 10

    switch( varType )
    {
      case CAMP_VAR_TYPE_INT:
	  camp_snprintf( str, str_size, "%d", (int)val );
	break;

      case CAMP_VAR_TYPE_FLOAT:
	if( val == 0.0 ) 
	{
	    camp_snprintf( str, str_size, "%.*f", STRING_PRECISION, val );
	}
	else
	{
	    exp = (int)floor( log10( fabs( val ) ) );

	    if( ( exp > 8 ) || ( exp < -4 ) )
	    {
                /*
                 * > 1e9 || < 0.0001
                 */
		camp_snprintf( str, str_size, "%.*e", STRING_PRECISION, val );
	    }
	    else
	    {
                /*
                 *  0.0001 - 1e9
                 */
		if( exp > 0 ) prec = STRING_PRECISION-exp; /* 1e9 - 10 */
		else          prec = STRING_PRECISION;     /* 10 - 0.0001 */
		camp_snprintf( str, str_size, "%.*f", prec, val );
	    }
	}
        break;	 

      default:
        return( FALSE );
    }
    return( TRUE );
}


int
camp_varNumGetUnits( const char* path, char* units, size_t units_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_NUMERIC* pNum;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    if( (pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK) == CAMP_VAR_TYPE_NUMERIC )
	    {
		pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;
		camp_strncpy( units, units_size, pNum->units ); 
	    }
	    else
	    {
		units[0] = '\0';
	    }
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


/*
 *  TRUE = within tolerance
 *  FALSE = out of tolerance
 */
bool_t
camp_varNumTestTol( const char* path, double val )
{
    bool_t status = FALSE;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_NUMERIC* pNum;
	CAMP_SELECTION* pSel;
	double min, max;

	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    status = TRUE;
	}
	else
	{
	    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK ) 
	    {
	    case CAMP_VAR_TYPE_NUMERIC:
		pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

		if( pNum->tolType == 0 )
		{
		    /* Plus/minus tolerance */
		    min = pNum->val - pNum->tol;
		    max = pNum->val + pNum->tol;

		    status = ( ( pNum->tol < 0.0 ) || ( ( val >= min ) && ( val <= max ) ) );
		}
		else if( pNum->tolType == 1 )
		{
		    /* Percent tolerance */
		    min = pNum->val - pNum->val*pNum->tol/100.0;
		    max = pNum->val + pNum->val*pNum->tol/100.0;

		    status = ( ( pNum->tol < 0.0 ) || ( ( val >= min ) && ( val <= max ) ) );
		}
		else
		{
		    status = TRUE;
		}

		break;

	    case CAMP_VAR_TYPE_SELECTION:
		pSel = pVar->spec.CAMP_VAR_SPEC_u.pSel;

		status = ( ( val == pSel->val ) ? TRUE : FALSE );
		break;

	    default:
		status = TRUE;
		break;
	    }
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( status );
}


int
camp_varNumGetStats( const char* path, u_long* pN, 
		  double* pLow, double* pHi, double* pOffset,
		  double* pSum, double* pSumSquares, double* pSumCubes )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_NUMERIC* pNum;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

	    if( pN ) *pN = pNum->num;
	    if( pLow ) *pLow = pNum->low;
	    if( pHi ) *pHi = pNum->hi;
	    if( pSum ) *pSum = pNum->sum;
	    if( pSumSquares ) *pSumSquares = pNum->sumSquares;
	    if( pSumCubes ) *pSumCubes = pNum->sumCubes;
	    if( pOffset ) *pOffset = pNum->offset;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varNumCalcStats( const char* path, double* pMean, double* pStdDev,
		      double* pSkew )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_NUMERIC* pNum;
	u_long n;
	double sum;
	double sumSquares;
	double sumCubes;
	double mean;
	double stddev;
	double skew;
	double var;

	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    pNum = pVar->spec.CAMP_VAR_SPEC_u.pNum;

	    n = pNum->num;
	    sum = pNum->sum;
	    sumSquares = pNum->sumSquares;
	    sumCubes = pNum->sumCubes;

	    if( n == 0 )
	    {
		mean = stddev = skew = 0.0;
	    }
	    else
	    {
		mean = ( sum == 0.0 ) ? 0.0 : sum/n;

		/*
		 *  Calculate standard deviation
		 */
		if( n == 1 ) 
		{
		    stddev = 0.0;
		}
		else
		{
		    var = ( sumSquares - ( (sum==0.0)?0.0:(pow(sum,2)/n) ) )/
			( n-1 );
		    stddev = sqrt( fabs( var ) );
		}

		/*
		 *  Calculate skewness
		 */
		if( stddev == 0.0 ) 
		{
		    skew = 0.0;
		}
		else
		{
		    skew = ( sumCubes - 3*mean*sumSquares + 
			     2*( (sum==0.0)?0.0:pow(sum,3) )/( pow(n,2) ) )/
			( n*pow(stddev,3) );
		}
	    }

	    *pMean = mean + pNum->offset;
	    *pStdDev = stddev;
	    *pSkew = skew;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varSelGetID( const char* path, u_char* pVal )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    *pVal = pVar->spec.CAMP_VAR_SPEC_u.pSel->val;
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varSelGetLabel( const char* path, char* label, size_t str_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_SELECTION* pSel;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    pSel = pVar->spec.CAMP_VAR_SPEC_u.pSel;
	    camp_status = varSelGetIDLabel( pVar, pSel->val, label, str_size );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varSelGetIDLabel( const char* path, u_char val, char* label, size_t str_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    camp_status = varSelGetIDLabel( pVar, val, label, str_size );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
varSelGetIDLabel( CAMP_VAR* pVar, u_char val, char* label, size_t label_size )
{
    CAMP_SELECTION* pSel;
    CAMP_SELECTION_ITEM* pItem;
    int i;

    pSel = pVar->spec.CAMP_VAR_SPEC_u.pSel;

    for( i = 0,	    pItem = pSel->pItems; 
	 ( i < val ) && ( pItem != NULL ); 
	 i++,	    pItem = pItem->pNext ) ;

    if( pItem == NULL )
    {
	label[0] = '\0';
	_camp_setMsg( "invalid selection index %d (var='%s')", val, pVar->core.path );
	return( CAMP_INVAL_SELECTION );
    }

    camp_strncpy( label, label_size, pItem->label ); 

    return( CAMP_SUCCESS );
}


int
camp_varSelGetLabelID( const char* path, const char* label, u_char* pVal )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    camp_status = varSelGetLabelID( pVar, label, pVal );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
varSelGetLabelID( CAMP_VAR* pVar, const char* label, u_char* pVal )
{
    CAMP_SELECTION* pSel;
    CAMP_SELECTION_ITEM* pItem;
    int i;

    pSel = pVar->spec.CAMP_VAR_SPEC_u.pSel;

    for( i = 0, pItem = pSel->pItems; 
		pItem != NULL; 
	 i++,	pItem = pItem->pNext )
    {
	if( streq( label, pItem->label ) )
	{
	    *pVal = (u_char)i;
	    return( CAMP_SUCCESS );
	}
    }

    _camp_setMsg( "invalid selection '%s' (var='%s')", label, pVar->core.path );

    return( CAMP_INVAL_SELECTION );
}


int
camp_varStrGetVal( const char* path, char* val, size_t val_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_STRING* pSpec;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    pSpec = pVar->spec.CAMP_VAR_SPEC_u.pStr;
	    camp_strncpy( val, val_size, pSpec->val ); 
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varArrGetVal( const char* path, caddr_t pVal )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_ARRAY* pSpec;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    pSpec = pVar->spec.CAMP_VAR_SPEC_u.pArr;
	    bcopy( pSpec->pVal, pVal, pSpec->elemSize*pSpec->totElem );
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}


int
camp_varLnkGetVal( const char* path, char* val, size_t val_size )
{
    int camp_status = CAMP_SUCCESS;

    // mutex_lock_varlist( 1 ); // warning: don't return inside mutex lock
    {
	CAMP_VAR* pVar;
	CAMP_LINK* pSpec;
	pVar = camp_varGetp( path /* , mutex_lock_varlist_check() */ );

	if( pVar == NULL )
	{
	    camp_status = CAMP_INVAL_VAR;
	    _camp_setMsgStat( CAMP_INVAL_VAR, path );
	}
	else
	{
	    pSpec = pVar->spec.CAMP_VAR_SPEC_u.pLnk;
	    camp_strncpy( val, val_size, pSpec->path ); 
	}
    }
    // mutex_lock_varlist( 0 ); // warning: don't return inside mutex lock

    return( camp_status );
}

CAMP_VAR* camp_varGetPHead_clnt( const char* path ) { return camp_varGetPHead( path /* , TRUE */ ); }
CAMP_VAR** camp_varGetpp_clnt( const char* path ) { return camp_varGetpp( path /* , TRUE */ ); }
CAMP_VAR* camp_varGetp_clnt( const char* path ) { return camp_varGetp( path /* , TRUE */ ); }
CAMP_VAR** camp_varGetTruePP_clnt( const char* path ) { return camp_varGetTruePP( path /* , TRUE */ ); }
CAMP_VAR* camp_varGetTrueP_clnt( const char* path ) { return camp_varGetTrueP( path /* , TRUE */ ); }
// CAMP_VAR_CORE* camp_varGetpCore_clnt( const char* path ) { return camp_varGetpCore( path /* , TRUE */ ); }
// caddr_t camp_varGetpSpec_clnt( const char* path ) { return camp_varGetpSpec( path /* , TRUE */ ); }
CAMP_VAR* camp_varGetpIns_clnt( const char* path ) { return camp_varGetpIns( path /* , TRUE */ ); }

