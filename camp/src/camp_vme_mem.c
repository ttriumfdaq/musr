/*
 *  Name:       camp_vme_mem.c
 *
 *  Purpose:    Provides VME access routines.
 *
 *  Applicability:
 *              These routines (vmeRead, vmeWrite) might be portable, but all 
 *              that is guaranteed is that they work for a vxworks system in 
 *              a vme crate.  They are likely to work on other systems where 
 *              the VME bus addresses are mapped to the computer address space
 *              (may need defining A16 in camp_vme_mem.h).  A system with a remote
 *              vme crate is likely to require different interface parameters, 
 *              a real "online" initialization, etc. 
 *
 *  Description:
 *              vmeRead (base_addr, offset, data_type, buffer, buf_len)
 *              Get data of type <data_type> at address <A16>+<base_addr>+<offset>
 *              and write it as a string into <buffer> of length <buf_len>
 *
 *              vmeWrite (base_addr, offset, data_type, val_str, str_len)
 *              Write data to VME address <A16>+<base_addr>+<offset>, using
 *              value string <val_string> of length <str_len>, converted to 
 *              the requested <data_type>
 *
 *              The token for the data type is just the integer value of the
 *              first four ascii characters in the name of the data type. 
 *              Known types are:
 *                   int,  int4    signed 4 byte integer
 *                   uint, dword   unsigned 4 byte integer
 *                   short, int2   signed 2 byte integer
 *                   ushort, word  unsigned 2 byte integer
 *                   int1          signed 1 byte integer
 *                   byte          unsigned 1 byte integer
 *                   char          single 1 byte character
 *                   string        null-terminated character string
 *
 *
 *  Called by:  camp_if_vme_mem.c, camp_tcl.c
 * 
 *  $Log: camp_vme_mem.c,v $
 *  Revision 1.4  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.1  2004/01/28 03:18:28  asnd
 *  Add VME interface type.
 *
 */


/*
 *  The token for the data type is just the integer value of the
 *  first four ascii characters in the name of the data type. 
 *  Such integer values depend on the system byte order, so the
 *  values are taken from explicit string constants (even though 
 *  the byte order of the mvme is known).  (Such values aren't
 *  recognized as constants by the C compiler, so can't be used in
 *  a switch; therefore we use if...elseif....)
 */

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <setjmp.h>
#include <stdlib.h> // strtol
#include <ctype.h> // isspace
#include "camp_srv.h" // camp_snprintf,etc
#include "camp_vme_mem.h"

static jmp_buf segvbuf;

static volatile int accVioCount;

static void vmeAccessError( int sig )
{
  accVioCount++;
  /* printf( "Access Fault!!!\n"); */
  longjmp(segvbuf, 1);
  /* NO: return; */
}

static int vmeRegTrans( int nbytes, char * source, char * destin)
{
  short   isle = 1;
  char   *isLittleEnded;
  int     i;
  void  (*prevHandler) ();
  // int     errcode = 0;
  char    c;

  isLittleEnded = (char*)&isle;

  /*  printf( "isLittleEnded: %d, VME_BIG_END: %d\n", *isLittleEnded, VME_BIG_END);
   *  printf( "Transfer %d bytes from %x to %x:", nbytes, source, destin);
   */

  prevHandler = signal(SIGSEGV, vmeAccessError);

  accVioCount = 0;

  if( setjmp(segvbuf)) 
  {
      accVioCount = 1;
  }
  else 
  {
      if( *isLittleEnded == (char) VME_BIG_END )
      {  /* reverse byte order */
          for( i=0 ; i<nbytes ; i++ )
          {
              c = *((volatile char*)(source+i));
              if( accVioCount ) break;
              destin[nbytes-i-1] = c & 0xff ;
          }
      }
      else
      { /* preserve byte order */
          for( i=0 ; i<nbytes ; i++ )
          {
              c = *((volatile char*)(source+i));
              if( accVioCount ) break;
              destin[i] = c & 0xff ;
          }
      }
  }
  (void) signal(SIGSEGV, prevHandler);

  return( accVioCount>0 );
}

/*
 *  vmeRead:    Read data from a vme address.
 *
 *  Parameters: see just below
 *
 *  Returns:    status code (of type VME_IO_CODE)
 *              data, encoded as a string, in buffer
 *
 *  20140224  TW  Changed buf_len -> buffer_size to be the size of buffer including terminating character, 
 *                for all cases (this was the case for numeric types, but not "string").
 *
 *  20140224  TW  You're casting data_type characters strings to (int*) to compare the first
 *                4 characters of the string.  This is unnatural and looks very odd.
 *                Please don't use strange/cryptic syntax or terseness for the sake of speed.
 *                It's more natural to use strncmp/streq than '==' when comparing strings.
 *                If you're doing it for speed, then you can assign each string token
 *                a corresponding integer (i.e., "int"=1, "int4"=2, ...) and pass the integer
 *                to vmeRead/vmeWrite.  Use an enum to standardize the values:
 *                  enum VME_DATA_TYPE {
 *                    VME_DATA_TYPE_INT = 1,
 *                    VME_DATA_TYPE_INT4 = 2,
 *                    ...
 *                  };
 *                And note that switch statements are faster than 'if' statements.
 */

VME_IO_CODE vmeRead ( 
       void*  base_addr,   /* Base address for instrument */
       int    offset,      /* address within instrument's address range */
       int*   data_type,   /* data type token (see top) */
       char*  buffer,      /* data is written here, as a string */
       int    buffer_size )    /* buffer length = maximum data length */
{
    char binbuff[16] = { 0 };     /* binary bytes buffer holding register contents */
    char strbuff[16];     /* string buffer for conversion of numeric types */
    char * address;       /* source address for data */
    int  j;

    address = (char*) A16 + (long) base_addr + offset;
    /* printf("Start vmeRead for %4s at addr %x + %x + %x = %x\n", 
       (char*)data_type, A16, base_addr, offset, address); */

    if( *data_type == *((int*)"int") || *data_type == *((int*)"int4") ) 
    {
	/* Data type is int  */
	vmeRegTrans( 4, address, binbuff);
	camp_snprintf( strbuff, sizeof( strbuff ), "%d", *((int*)binbuff) );
    }
    else if( *data_type == *((int*)"uint") || *data_type == *((int*)"dword") ) 
    {
	/* Data type is unsigned int  */
	vmeRegTrans( 4, address, binbuff);
	camp_snprintf( strbuff, sizeof( strbuff ), "%u", *((unsigned int*)binbuff) );
    }
    else if( *data_type == *((int*)"short") || *data_type == *((int*)"int2") ) 
    {
	/* Data type is short */
	vmeRegTrans( 2, address, binbuff);
	camp_snprintf( strbuff, sizeof( strbuff ), "%d", *((short*)binbuff) );
    }
    else if( *data_type == *((int*)"ushort") || *data_type == *((int*)"word") ) 
    {
	/* Data type is unsigned short */
	vmeRegTrans( 2, address, binbuff);
	camp_snprintf( strbuff, sizeof( strbuff ), "%u", *((unsigned short*)binbuff) );
    }
    else if( *data_type == *((int*)"int1") ) 
    { /* Data type is signed byte */
	vmeRegTrans( 1, address, binbuff);
	camp_snprintf( strbuff, sizeof( strbuff ), "%d", *binbuff );
    }
    else if( *data_type == *((int*)"byte") ) 
    { /* Data type is unsigned byte */
	vmeRegTrans( 1, address, binbuff);
	camp_snprintf( strbuff, sizeof( strbuff ), "%u", *binbuff );
    }
    else if( *data_type == *((int*)"char") ) 
    { /* Data type is single character */
	vmeRegTrans( 1, address, binbuff);
	camp_snprintf( strbuff, sizeof( strbuff ), "%c", *binbuff );
    }
    else if( *data_type == *((int*)"string") ) 
    { /* Data type is string */
	for( j = 0; j < buffer_size-1; j++ ) // 20140224  TW  changed buffer_size to buffer_size-1 and accounted for by callers
	{   /* Copy it now and return */ 
	    vmeRegTrans( 1, address+j, strbuff );
	    if( accVioCount ) return( VME_IO_ACCESS_ERR );
	    buffer[j] = *strbuff;
	    if( *strbuff == '\0' ) break; // 20140224  TW  '==' not '='
	}
	buffer[buffer_size-1] = '\0'; // 20140224  TW  ensure termination
	return VME_IO_SUCCESS;
    }
    else 
    {
	return VME_IO_UNKNOWN;
    }

    if( accVioCount ) return( VME_IO_ACCESS_ERR );

    /* printf("Got data: %s\n", strbuff); */

    /* 
     * Give error on buffer overflow for numeric data types.
     * For strings, simply truncate at the buffer size (best?)
     */
    if( sizeof( strbuff ) > buffer_size ) 
    {
	if( strlen( strbuff ) >= buffer_size ) 
	{
	    return VME_IO_OVERFLOW;
	}
    }

    camp_strncpy( buffer, buffer_size, strbuff ); // strncpy( buffer, strbuff, buffer_size );  buffer[buffer_size-1] = '\0';

    return VME_IO_SUCCESS;
}

/*
 *  vmeWrite:   Write data to a vme address.  
 *
 *  Parameters: see just below
 *
 *  Returns:    status code (of type VME_IO_CODE)
 *
 */
VME_IO_CODE vmeWrite (
       void*  base_addr,   /* Base address for instrument */
       int    offset,      /* address within instrument's address range */
       int*   data_type,   /* data type token (see top) */
       char*  val_str,     /* data to write, given as a string */
       int    str_len )    /* length of data string */
{
  char * address;       /* destination VME address for data */
  char * endptr;        /* for diagnosing conversion errors */

  int   bytes;          /* number of bytes to transfer, for numerics */
  int   numbase;        /* numeric base for conversion */
  long  longi;          /* numeric value from val_str */
  long  longj;          /* for diagnosing conversion errors */
  char  lbuf[16];       /* space for numeric value of whatever type */
  int   j;

  address = (char*) A16 + (long) base_addr + offset;

  /* printf("vmeWrite %4s value %s\n", (char*)data_type, val_str);*/

  if( *data_type == *((int*)"char") ) 
  { /* Data type is single character */
      vmeRegTrans( 1, val_str, address);
  }
  else if( *data_type == *((int*)"string") ) 
  { /* Data type is string */
      for( j=0; j<str_len; j++) 
      {
	  if( val_str[j]=='\0' ) break;
	  vmeRegTrans( 1, val_str+j, address+j);
      }
  }
  else 
  {
    /* 
     * Remaining types are all numeric.  Allow 0x... notation for HEX, but
     * disallow the awful 0... notation for Octal (read as decimal).
     */
    if( strchr( val_str, 'x' ) ) 
    {
       numbase = 0;
    }
    else 
    {
       numbase = 10;
    }
    longi = strtol( val_str, &endptr, numbase );

    /* printf("Numeric argument has value %d = 0x%x\n", longi, longi);*/
    /*
     * We copy the data over two passes, just to get the error checking in
     * the most sensible order: just copy the data to a temporary location
     * the first time, to check all possible failure conditions.  Check for
     * conversion error at end of the first pass, after detecting invalid
     * data-type tokens.
     */

    if( *data_type == *((int*)"int") || *data_type == *((int*)"int4") ) {
        /* Data type is int  */
        longj = (int) (longi & 0xffffffff);
        *((int*) lbuf) = (int) (longi & 0xffffffff);
        bytes = 4;
    }
    else if( *data_type == *((int*)"uint") || *data_type == *((int*)"dword") ) {
        /* Data type is unsigned int  */
        longj = (unsigned int) (longi & 0xffffffff);
        *((unsigned int*) lbuf) = (unsigned int) (longi & 0xffffffff);
        bytes = 4;
    }
    else if( *data_type == *((int*)"short") || *data_type == *((int*)"int2") ) {
        /* Data type is short */
        longj = (short) (longi & 0xffff);
        *((short*) lbuf) = (short) (longi & 0xffff);
        bytes = 2;
    }
    else if( *data_type == *((int*)"ushort") || *data_type == *((int*)"word") ) {
        /* Data type is unsigned short */
        longj = (unsigned short) (longi & 0xffff);
        *((unsigned short*) lbuf) = (unsigned short) (longi & 0xffff);
        bytes = 2;
    }
    else if( *data_type == *((int*)"int1") ) { /* Data type is signed byte */
        longj = (char) (longi & 0xff);
        *((char*) lbuf) = (char) (longi & 0xff);
        bytes = 1;
    }
    else if( *data_type == *((int*)"byte") ) { /* Data type is unsigned byte */
        longj = (unsigned char) (longi & 0xff);
        *((unsigned char*) lbuf) = (unsigned char) (longi & 0xff);
        bytes = 1;
    }
    else {
        return VME_IO_UNKNOWN;
    }

    /* printf( "Buffered %d bytes %ld -> %ld\n", bytes, longi, longj);*/
    /*
     * Give error on incomplete numeric conversion
     */
    if( isgraph( *endptr ) ) {
        return VME_IO_INVAL_NUM;
    }
      
    /* 
     * Give error on numeric overflow for numeric data types.
     */
    if( longj != longi ) {
        /* printf("Error: numeric overflow\n"); */
        return VME_IO_OVERFLOW;
    }

    /*
     * Copy the requisite number of bytes to VME
     */
    vmeRegTrans( bytes, lbuf, address);

  } /* end else (numeric types) */

  if( accVioCount ) return( VME_IO_ACCESS_ERR );

  return VME_IO_SUCCESS;
}

