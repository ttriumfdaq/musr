/* camp_vme_mem.h  --   prototypes and defines for camp_vme_mem.c
 *                      (For VME access when VME bus addresses are mapped
 *                      onto system memory.)
 *
 *  $Log: camp_vme_mem.h,v $
 *  Revision 1.4  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.1  2004/01/28 03:18:28  asnd
 *  Add VME interface type.
 *
 *
 */

/*
 * A16 is the starting address where the VME bus is mapped into the system's
 * memory space.  VME_BIG_END is a boolean indicating if multi-byte numbers
 * are stored with the most-significant byte first.  (This may depend on the
 * individual VME modules, so it may become an interface parameter later.)
 */
#ifdef VXWORKS
#ifdef PPCxxx
#define A16            0xfbff0000
#define VME_BIG_END    1
#else
#define A16            0xffff0000
#define VME_BIG_END    1
#endif
#endif

/* Don't know the parameters for vmic linux yet */
#ifdef linux
#ifdef PPCxxx
#define A16            0xfbff0000
#define VME_BIG_END    1
#else
#define A16            0xffff0000
#define VME_BIG_END    1
#endif
#endif

enum VME_IO_CODE {
  VME_IO_SUCCESS,
  VME_IO_UNKNOWN,
  VME_IO_INVAL_NUM,
  VME_IO_OVERFLOW,
  VME_IO_ACCESS_ERR
};
typedef enum VME_IO_CODE VME_IO_CODE;

VME_IO_CODE vmeRead ( void* base_addr, int offset, int* data_type, char* buffer, int buf_len ) ;
VME_IO_CODE vmeWrite ( void* base_addr, int offset, int* data_type, char* val_str, int str_len ) ;

