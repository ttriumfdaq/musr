/*
 *  Name:       camp_if_gpib_deltat.c
 *
 *  Purpose:    Provides a GPIB interface type.
 *              Communicate with GPIB instruments by way of a
 *              CAMAC Kinetic Systems 3388 Module using Dave Maden's
 *              gpib_package in the DELTAT library for VAX/VMS.
 *
 *              A CAMP GPIB interface definition must provide the following
 *              routines:
 *                int if_gpib_init ( void );
 *                int if_gpib_online ( CAMP_IF *pIF );
 *                int if_gpib_offline ( CAMP_IF *pIF );
 *                int if_gpib_read( REQ* pReq );
 *                int if_gpib_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communication).
 *
 *              Note that the global lock is not removed in this interface
 *              implementation because of the uncertainty of the underlying
 *              library calls gpib_send and gpib_enter.
 *
 *  Notes:
 *
 *    INI file configuration string:
 *     {<b> <c> <n> <a> <timeout> <delay>}
 *      timeout - integer in seconds
 *      delay - float in seconds (delay between each byte transfered)
 *
 *    Meaning of private (driver-specific) variables:
 *       pIF_t->priv - GPIB semaphore address
 *                   == 1 (gpib available)
 *                   == 0 (gpib unavailable)
 *       pIF->priv   - not used
 *
 *  Revision history:
 *
 */

#include <stdio.h>
#include "deltat_defs.h"
#include "camp_srv.h"

static u_char getTerm( char* term );

#define gpib_cam_ok  (pIF_t->priv!=0)
#define gpib_sem_addr (pIF_t->priv)


int
if_gpib_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    int prim_addr = 0, tmo = 0;
    float delay = 0;
    char str_tmo[16];
    s_dsc dsc_tmo;
    u_long sem_addr[2];
    long l;
    int b = 0, c = 0, n = 0;

    /*
     *  Need CAMAC to use this GPIB driver
     */
    pIF_t = camp_ifGetpIF_t( "camac" );
    if( pIF_t == NULL)
    {
        camp_appendMsg( "gpib needs camac, gpib disabled" );
        return( CAMP_FAILURE );
    }

    pIF_t = camp_ifGetpIF_t( "gpib" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /* 
     *  priv holds the semaphore address
     *  The value is zero if GPIB is unavailable
     */
    pIF_t->priv = 0; 

    sscanf( pIF_t->conf, " %d %d %d %d %d %f", 
                           &b, &c, &n, &prim_addr, &tmo, &delay );

    sprintf( str_tmo, "0 0:0:%d", tmo );
    setdsctostr( &dsc_tmo, str_tmo );

    /*
     *  "delay" is the delay between each data read 
     *  from the 3388 in seconds
     */
    status = GPIB_INIT( &b, &c, &n, &prim_addr, &dsc_tmo, &delay );
    if( _failure( status ) )
    {
        camp_appendMsg( "gpib initialization failed, gpib disabled" );
        return( CAMP_FAILURE );
    }

    l = MAP__CAMAC_SEMAPHORE;
    status = MAP_GLOBAL( &l, sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "mapping gpib semaphore failed, gpib disabled" );
        return( CAMP_FAILURE );
    }

    /*
     *  Compute address of GPIB semaphore
     */
    pIF_t->priv = sem_addr[0] + SEM__CAMAC_GPIB;

    return( CAMP_SUCCESS );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;
    int addr;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        status = if_gpib_init();
        if( _failure( status ) ) 
        {
            camp_appendMsg( "gpib unavailable" );
            return( CAMP_FAILURE );
        }
    }

    if( ( addr = camp_getIfGpibAddr( pIF->defn ) < 0 ) ||
        ( addr > 31 ) )
    {
        camp_appendMsg( "invalid gpib address" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int status;
    int status2;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    s_dsc dsc_cmd;
    s_dsc dsc_term;
    s_dsc dsc_buf;
    long zero = 0;
    u_char term;
    char str[LEN_STR+1];
    u_long addr;
    char buf[512];

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    setdsctobuf( &dsc_cmd, pReq->spec.REQ_SPEC_u.read.cmd,
                 pReq->spec.REQ_SPEC_u.read.cmd_len );

    term = getTerm( camp_getIfGpibReadTerm( pIF->defn, str ) );
    setdsctobuf( &dsc_term, &term, 1 );

    addr = camp_getIfGpibAddr( pIF->defn );

    status = SEM_CLAIM( gpib_sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to claim gpib semaphore" );
        camp_appendVMSMsg( status );
        return( status );
    }

    status = gpib_send( &addr, &dsc_cmd, &dsc_term );
    if( _failure( status ) )
    {
    	sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
        camp_appendMsg( buf );
        camp_appendVMSMsg( status );
    }

    status2 = SEM_RELEASE( gpib_sem_addr );
    if( _failure( status2 ) )
    {
        camp_appendMsg( "failed releasing gpib semaphore" );
        camp_appendVMSMsg( status2 );
    }

    if( _failure( status ) )
    {
        /*
         *  BIG FAT CLUGE
         *  Sometimes gpib_cam gets stuck when CAMAC changes state.
         *  If the error is not just a wrong GPIB address,
         *  reinitializing CAMAC and then GPIB fixes this.
         */
        status = if_camac_init();
        if( _failure( status ) ) return( status );

        status = if_gpib_init();
        if( _failure( status ) ) return( status );
	
	status = SEM_CLAIM( gpib_sem_addr );
	if( _failure( status ) )
	  {
	    camp_appendMsg( "failed to claim gpib semaphore" );
	    camp_appendVMSMsg( status );
	    return( status );
	  }
	
        status = gpib_send( &addr, &dsc_cmd, &dsc_term );
	
	status2 = SEM_RELEASE( gpib_sem_addr );
	if( _failure( status2 ) )
	  {
	    camp_appendMsg( "failed releasing gpib semaphore" );
	    camp_appendVMSMsg( status2 );
	  }
	
        if( _failure( status ) )
        {
    	    sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
            camp_appendMsg( buf );
            camp_appendVMSMsg( status );
            return( status );
        }
    }

    setdsctobuf( &dsc_buf, pReq->spec.REQ_SPEC_u.read.buf, 
                 pReq->spec.REQ_SPEC_u.read.buf_len );

    status = SEM_CLAIM( gpib_sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to claim gpib semaphore" );
        camp_appendVMSMsg( status );
        return( status );
    }

    status = gpib_enter( &dsc_buf, 
                         &pReq->spec.REQ_SPEC_u.read.read_len,
                         &addr, &dsc_term );

    status2 = SEM_RELEASE( gpib_sem_addr );
    if( _failure( status2 ) )
    {
        camp_appendMsg( "failed releasing gpib semaphore" );
        camp_appendVMSMsg( status2 );
    }

    if( _failure( status ) )
    {
        camp_appendMsg( "failed receiving gpib data" );
        camp_appendVMSMsg( status );
        return( status );
    }

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int status, status2;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    s_dsc dsc_cmd;
    s_dsc dsc_term;
    long zero = 0;
    u_long addr;
    u_char term;
    char str[LEN_STR+1];
    char buf[512];

    pIF = pReq->spec.REQ_SPEC_u.write.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !gpib_cam_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    setdsctobuf( &dsc_cmd, pReq->spec.REQ_SPEC_u.write.cmd,
                 pReq->spec.REQ_SPEC_u.write.cmd_len );
    term = getTerm( camp_getIfGpibReadTerm( pIF->defn, str ) );
    setdsctobuf( &dsc_term, &term, 1 );

    addr = camp_getIfGpibAddr( pIF->defn );

    status = SEM_CLAIM( gpib_sem_addr );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to claim gpib semaphore" );
        camp_appendVMSMsg( status );
        return( status );
    }
    
    status = gpib_send( &addr, &dsc_cmd, &dsc_term );
    if( _failure( status ) )
    {
        sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
        camp_appendMsg( buf );
        camp_appendVMSMsg( status );
    }

    status2 = SEM_RELEASE( gpib_sem_addr );
    if( _failure( status2 ) )
      {
        camp_appendMsg( "failed releasing gpib semaphore" );
        camp_appendVMSMsg( status2 );
      }
    
    if( _failure( status ) )
    {
        /*
         *  BIG FAT CLUGE
         *  Sometimes gpib_cam gets stuck when CAMAC changes state.
         *  If the error is not is not just a wrong GPIB address,
         *  reinitializing CAMAC and then GPIB fixes this.
         */
        status = if_camac_init();
        if( _failure( status ) ) return( status );

        status = if_gpib_init();
        if( _failure( status ) ) return( status );
	
	status = SEM_CLAIM( gpib_sem_addr );
	if( _failure( status ) )
	  {
	    camp_appendMsg( "failed to claim gpib semaphore" );
	    camp_appendVMSMsg( status );
	    return( status );
	  }
	
        status = gpib_send( &addr, &dsc_cmd, &dsc_term );

	status2 = SEM_RELEASE( gpib_sem_addr );
	if( _failure( status2 ) )
	  {
	    camp_appendMsg( "failed releasing gpib semaphore" );
	    camp_appendVMSMsg( status2 );
	  }
	
        if( _failure( status ) )
        {
    	    sprintf( buf, "failed sending gpib data (VMS status: %d)", status );
            camp_appendMsg( buf );
            camp_appendVMSMsg( status );
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_gpib_clear
 *
 *  Sets up GPIB bus and sends an SDC (selected device clear)
 *  Does not use any CAMP interface definition, just sends the
 *  command straight to the GPIB bus.  
 *
 *  21-Dec-1999  TW  This routine is UNTESTED
 */
int 
camp_gpib_clear( int gpib_addr )
{
  /*
   *  For implementation:  call GPIB_TRANSMIT with the proper
   *  string, i.e., 'unl unt ren listen <gpib_addr> mta sdc'
   *  See gpib_package_1 for examples gpib_send and gpib_enter.
   */
  int status, status2;
  s_dsc dsc_cmd;
  char buf[512];
  long deltat_gpib_sem_addr;
  u_long sem_addr[2];
  long l;

  l = MAP__CAMAC_SEMAPHORE;
  status = MAP_GLOBAL( &l, sem_addr );
  if( _failure( status ) )
    {
      camp_appendMsg( "mapping gpib semaphore failed" );
      return( CAMP_FAILURE );
    }

  /*
   *  Compute address of GPIB semaphore
   */
  deltat_gpib_sem_addr = sem_addr[0] + SEM__CAMAC_GPIB;

  /*
   *  Build the gpib_transmit string.  gpib_transmit is from
   *  Dave Maden's gpib_package.
   */
  sprintf( buf, "unl unt ren listen %d mta sdc", gpib_addr );
  setdsctobuf( &dsc_cmd, buf, strlen( buf ) );

  status = SEM_CLAIM( deltat_gpib_sem_addr );
  if( _failure( status ) )
    {
      camp_appendMsg( "failed to claim gpib semaphore" );
      camp_appendVMSMsg( status );
      return( status );
    }
    
  status = GPIB_TRANSMIT( &dsc_cmd );
  if( _failure( status ) )
    {
      sprintf( buf, "failed sending gpib transmit (VMS status: %d)", status );
      camp_appendMsg( buf );
      camp_appendVMSMsg( status );
    }

  status2 = SEM_RELEASE( deltat_gpib_sem_addr );
  if( _failure( status2 ) )
    {
      camp_appendMsg( "failed releasing gpib semaphore" );
      camp_appendVMSMsg( status2 );
    }
    
  if( _failure( status ) ) return( status );

  return( CAMP_SUCCESS );
}


/*
 *  camp_gpib_timeout
 *
 *  Set timeout of GPIB interface bus
 *
 *  13-Dec-2000  TW  Implemented for VxWorks MVIP300 only.
 *                   For DELTAT implementation, GPIB_INIT would need
 *                   to be called again.
 */
int 
camp_gpib_timeout( int timeoutVal )
{
  return( CAMP_SUCCESS );
}


static u_char
getTerm( char* term )
{
    TOKEN tok;

    findToken( term, &tok );

    switch( tok.kind )
    {
        case TOK_LF:
            return( '\012' );
        case TOK_CR:
            return( '\015' );
        case TOK_CRLF:
            return( '\012' );
        case TOK_LFCR:
            return( '\015' );
        case TOK_NONE:
            return( 0 );
        default:
            return( 0 );
    }
}
