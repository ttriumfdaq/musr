/*
 *  Name:       camp_if_rs232_vms_term.c
 *
 *  Purpose:    Provides an RS232 serial communications interface type.
 *              Implementation using VAX TT and LAT ports.
 *
 *              A CAMP RS232 interface definition must provide the following
 *              routines:
 *                int if_rs232_init ( void );
 *                int if_rs232_online ( CAMP_IF *pIF );
 *                int if_rs232_offline ( CAMP_IF *pIF );
 *                int if_rs232_read( REQ* pReq );
 *                int if_rs232_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In this implementation, the global mutex is unlocked
 *              during calls to sys$synch system calls which wait for
 *              read or write functions to complete.
 *
 *  Notes:
 *
 *    Ini file configuration string:
 *       {<port1> <port2> ...}
 *
 *    Meaning of private (driver-specific) variables:
 *       pIF_t->priv - not used
 *       pIF->priv - low-order 2 bytes contain VMS IO channel
 *                   high-order 2 bytes contain LAT flag and LAT service flag
 *
 *  Revision history:
 *
 *  v1.0               TW  Initial
 *  v1.1  18-Nov-1994  TW  Added rs232_tt_emptyBuffer, use in rs232_tt_read
 *                         (tries to get rid of unsolicited data before read)
 *
 */

#include <iodef.h>
#include <stdio.h>
#include <ssdef.h>
#include <descrip.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include "camp_srv.h"

struct qiosb {
  short status;
  union {
    struct {
      short d1,d2,d3;
    } s1;
    struct {
      u_char  trans_speed, recv_speed;
      u_char  CR_count, LF_count;
      u_char  parity, dummy;
    } s2;
  } u1;
};

typedef struct _REQ_QIO {
    struct _REQ_QIO*	pNext;
    u_long		efn;
    struct qiosb	iosb;
    REQ*		pReq;
} REQ_QIO;

/* this is to mask the page_len byte to get basic_chars  */
#define basic_chars_mask  0x00FFFFFF

#define postfix_LF 0x8A000000    /* postfix LF */
#define postfix_CR 0x8D000000    /* postfix CR */
#define postfix_CRLF 0x01000000  /* postfix CRLF */
#define postfix_LFCR 0x8D000000  /* can't do it this way - just send CR */
#define postfix_NONE 0x00000000  /* postfix NONE */
u_long term_mask_LF[2]   = {0,(1<<10)};
u_long term_mask_CR[2]   = {0,(1<<13)};
u_long term_mask_CRLF[2] = {0,(1<<10)};
u_long term_mask_LFCR[2] = {0,(1<<13)};
u_long term_mask_NONE[2] = {0,0};

#define ioChannelMask   0x0000FFFF
#define isLatMask       0x00010000
#define isServiceMask   0x00020000
#define isLat(i)        ((i)&isLatMask)
#define isService(i)    ((i)&isServiceMask)
#define ioChan(i)       ((i)&ioChannelMask)
#define setIoChan(a,b)  a=(a&~ioChannelMask)|b

int rs232_tt_get ( CAMP_IF* pIf );
int rs232_tt_connect ( CAMP_IF* pIf );
int rs232_tt_set ( CAMP_IF* pIf );
int rs232_tt_disconnect ( CAMP_IF* pIf );
int rs232_tt_free ( CAMP_IF* pIf );
int rs232_tt_wwrite ( CAMP_IF* pIf , char *cmd , int cmd_len );
int rs232_tt_qwrite ( CAMP_IF* pIf , char *cmd , int cmd_len , u_long efn , struct qiosb *pIosb , void (**pASTAddr )(), u_long ASTParam );
 int rs232_tt_wread ( CAMP_IF* pIf , char *buf , int buf_len , int *pRead_len );
int rs232_tt_qread ( CAMP_IF* pIf , char *buf , int buf_len , u_long efn , struct qiosb *pIosb , void (**pASTAddr )(), u_long ASTParam );
int rs232_tt_wread_prompt( CAMP_IF* pIf,
                       char* buf, int buf_len, int* pRead_len, 
                       char* prompt, int prompt_len );
int rs232_tt_qread_prompt( CAMP_IF* pIf,
                       char* buf, int buf_len, 
                       char* prompt, int prompt_len,
                       u_long efn, struct qiosb* pIosb,
                       void (**pASTAddr)(), u_long ASTParam );
static u_long getBaud ( u_long baud );
static void getReadTerm ( char* term , u_long termVMS [2 ]);
static u_long getWriteTerm ( char* term );
static u_long getParity ( char* parity );


int
if_rs232_init( void )
{
    /*
     *  Could check that the ports exist
     *  and are online etc. here.
     */
    return( CAMP_SUCCESS );
}


int
if_rs232_online( CAMP_IF* pIF )
{
    int status, status2;
    char port[LEN_STR+1];

    camp_getIfRs232Port( pIF->defn, port );

    stolower( port );

    if( ( strncmp( port, "_tt", 3 ) == 0 ) ||
             ( strncmp( port, "tt", 2 ) == 0 )  ||
             ( strncmp( port, "_tx", 3 ) == 0 ) ||
             ( strncmp( port, "tx", 2 ) == 0 )  ||
             ( strncmp( port, "_cs", 3 ) == 0 ) ||
             ( strncmp( port, "cs", 2 ) == 0 )  ||
             ( strncmp( port, "_op", 3 ) == 0 ) ||
             ( strncmp( port, "op", 2 ) == 0 ) )
    {
    }
    else if( ( strncmp( port, "_lt", 3 ) == 0 ) ||
        ( strncmp( port, "lt", 2 ) == 0 ) )
    {
	pIF->priv |= isLatMask;
    }
    else /* Assume it's a LAT service */
    {
	pIF->priv |= isLatMask;
	pIF->priv |= isServiceMask;
    }

    status = rs232_tt_get( pIF );
    if( _failure( status ) ) goto error;

    status = rs232_tt_set( pIF );
    if( _failure( status ) ) 
    {
        /* not fatal */
    }

    if( isLat( pIF->priv ) )
    {
        status = rs232_tt_connect( pIF );
        if( _failure( status ) ) goto error;
    }

    return( CAMP_SUCCESS );

error:
    status2 = rs232_tt_free( pIF );
    return( status );
}


int
if_rs232_offline( CAMP_IF* pIF )
{
    int status;

    if( isLat( pIF->priv ) )
    {
        status = rs232_tt_disconnect( pIF );
        if( _failure( status ) ) return( status );
    }

    status = rs232_tt_free( pIF );
    if( _failure( status ) ) return( status );

    return( CAMP_SUCCESS );
}


/*
 *  rs232_tt_read,  queue asynchronous read, write command, wait for read
 *                  or use read with prompt operation
 */
int 
if_rs232_read( REQ* pReq )
{
    int status;
    REQ_QIO reqQio;
    CAMP_IF* pIF;
    bool_t done;
    int tries;
    char* write_buf;
    int write_buf_len;
    char port[LEN_STR+1];
    char writeTerm[LEN_STR+1];
    char readTerm[LEN_STR+1];
    TOKEN tok;

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    camp_getIfRs232Port( pIF->defn, port );
    camp_getIfRs232WriteTerm( pIF->defn, writeTerm );
    
    reqQio.pReq = pReq;

    status = lib_get_ef( &reqQio.efn );
    if( _failure( status ) ) 
    {
        camp_appendMsg( "rs232_tt_read: failed lib_get_ef %s", port );
        return( status );
    }
    
    /*
     *  Flush typeahead if read timeout is greater than zero
     */
    if( camp_getIfRs232Timeout( pIF->defn ) > 0 )
      {
	/*
	 *  Make best attempt at making sure there
	 *  is no unsolicited data being sent before
	 *  starting our request
	 */
	rs232_tt_emptyBuffer( pIF );
      }

    /*
     *  Don't use until I'm sure what happens to
     *  the terminator characters sent in the prompt.
     *  (i.e., the CR that is needed to clear the prompt buffer).
     */
#define USE_READ_PROMPT

#ifdef USE_READ_PROMPT

    write_buf = (char*)malloc( pReq->spec.REQ_SPEC_u.read.cmd_len + 2 );
    bcopy( pReq->spec.REQ_SPEC_u.read.cmd, 
	  write_buf, 
	  pReq->spec.REQ_SPEC_u.read.cmd_len );
    write_buf_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    
    /*
     *  IGNORE THE FOLLOWING COMMENT WHEN PORTS IN "PASTHRU" MODE
     *  Must always send a CR or prompt buffer 
     *  (~80 chars) will fill up.
     *  I don't know what happens when the terminator
     *  contains a CR (i.e., that is intended to be sent)
     */
    findToken( writeTerm, &tok );
    switch( tok.kind )
      {
      case TOK_LF:
	write_buf[write_buf_len++] = '\012';
	break;
      case TOK_CR:
	write_buf[write_buf_len++] = '\015';
	break;
      case TOK_CRLF:
	write_buf[write_buf_len++] = '\015';
	write_buf[write_buf_len++] = '\012';
	break;
      case TOK_NONE:
	break;
      default:
	break;
      }
    
    /*
     *  Causes problems for CRYO PS120?
     *  No, I don't think so (18-Nov-1994)
     */
    status = rs232_tt_qread_prompt( pIF, 
				   pReq->spec.REQ_SPEC_u.read.buf, 
				   pReq->spec.REQ_SPEC_u.read.buf_len,
				   write_buf,
				   write_buf_len,
				   reqQio.efn, 
				   &reqQio.iosb,
				   NULL,
				   0 );
    if( _failure( status ) ) 
      {
	return( status );
      }
    
#else /* !USE_READ_PROMPT */
    
    status = rs232_tt_qread( pIF, 
                            pReq->spec.REQ_SPEC_u.read.buf, 
                            pReq->spec.REQ_SPEC_u.read.buf_len,
                            reqQio.efn, 
                            &reqQio.iosb,
                            NULL,
                            0 );
    if( _failure( status ) ) 
      {
	/* 
	 *  Fatal
	 */
	return( status );
      }
    
    status = rs232_tt_wwrite( pIF, 
                             pReq->spec.REQ_SPEC_u.read.cmd, 
                             pReq->spec.REQ_SPEC_u.read.cmd_len 
			     );
    if( _failure( status ) ) 
      {
	/* 
	 *  Fatal
	 */
	return( status );
      }
    
#endif /* USE_READ_PROMPT */
    
    /*
     *  Wait for the read to complete
     */
    thread_unlock_global_np();
    
    sys$synch( reqQio.efn, &reqQio.iosb );
    
    thread_lock_global_np();
    
    if( _failure( reqQio.iosb.status ) ) 
      {
	/* 
	 *  Not fatal
	 */
	if( reqQio.iosb.status == SS$_TIMEOUT )
	  {
	    camp_appendMsg( "timeout reading port %s", port );
	  }
	else
	  {
	    camp_appendMsg( "failed reading port %s", port );
	    camp_appendVMSMsg( reqQio.iosb.status );
	  }
      }
    else
      {
	done = TRUE;
      }

#ifdef USE_READ_PROMPT
    free( write_buf );
#endif /* USE_READ_PROMPT */

    lib_free_ef( reqQio.efn );

    pReq->spec.REQ_SPEC_u.read.read_len = reqQio.iosb.u1.s1.d1;

/*
    camp_appendMsg( "chars = %d, term = %x, term size = %d", 
        reqQio.iosb.u1.s1.d1, reqQio.iosb.u1.s1.d2, reqQio.iosb.u1.s1.d3 );
*/

    return( reqQio.iosb.status );
}


/*
 *  rs232_tt_write,  synchronous write 
 */
int 
if_rs232_write( REQ* pReq )
{
    int status;
    REQ_QIO reqQio;
    CAMP_IF* pIF;
    char port[LEN_STR+1];

    pIF = pReq->spec.REQ_SPEC_u.write.pIF;

    camp_getIfRs232Port( pIF->defn, port );

    reqQio.pReq = pReq;

    status = lib_get_ef( &reqQio.efn );
    if( _failure( status ) ) 
    {
        camp_appendMsg( "rs232_tt_write: failed lib_get_ef %s", port );
        return( status );
    }

    status = rs232_tt_qwrite( pIF, 
                              pReq->spec.REQ_SPEC_u.write.cmd, 
                              pReq->spec.REQ_SPEC_u.write.cmd_len,
                              reqQio.efn, 
                              &reqQio.iosb,
                              NULL,
                              0 );
    if( _failure( status ) ) 
    {
        /* 
         *  Fatal
         */
        lib_free_ef( reqQio.efn );
        return( status );
    }

    /*
     *  Wait for the write to complete
     */
    thread_unlock_global_np();

    sys$synch( reqQio.efn, &reqQio.iosb );

    thread_lock_global_np();

    if( _failure( reqQio.iosb.status ) ) 
    {
        /* 
         *  Not fatal
         */
        camp_appendMsg( "failed write %s", port );
        camp_appendVMSMsg( reqQio.iosb.status );
    }

    lib_free_ef( reqQio.efn );

    return( reqQio.iosb.status );
}


/*
 *  rs232_tt_get,  allocate, assign LAT port
 */
int 
rs232_tt_get( CAMP_IF* pIF )
{
    int status;
    char port[LEN_STR+1];
    u_short ioChannel;

    camp_getIfRs232Port( pIF->defn, port );

    /*
     *  allocate the port
     */
    status = sys_alloc( port );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to allocate port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    /*
     *  assign the port
     */
    status = sys_assign( port, &ioChannel );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed to assign port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    /*
     *  Store iochannel in first two bytes of pIF->priv
     */
    setIoChan( pIF->priv, ioChannel );

    return( CAMP_SUCCESS );
}


/*
 *  rs232_tt_connect,  connect LAT port
 */
int 
rs232_tt_connect( CAMP_IF* pIF )
{
    int status;
    struct qiosb port_qiosb;
    char port[LEN_STR+1];
    u_short ioChannel;

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );

    /*
     *  connect the port
     */
    status = sys$qiow(  0,                    /* efn */
                        ioChannel,    /* chan */
                        IO$_TTY_PORT |        /* func */
                        IO$M_LT_CONNECT,
                        &port_qiosb,          /* iosb */
                        0, 0,                 /* astadr, astprm */
                        0,                    /* P1 */
                        0,                    /* P2 */
                        0,                    /* P3 */
                        0,                    /* P4 */
                        0,                    /* P5 */
                        0 );                  /* P6 */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio connecting port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }
    if( _failure( port_qiosb.status ) )
    {
        camp_appendMsg( "failed to connect port %s", port );
        camp_appendVMSMsg( port_qiosb.status );
        return( port_qiosb.status );
    }

    return( CAMP_SUCCESS );
}


int 
rs232_tt_set( CAMP_IF* pIF )
{
    int  status;
    struct qiosb port_qiosb;
    struct device_mode device_sensemode, device_setmode;
    u_long baud;
    u_long parity;
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );

    bzero( &device_setmode, sizeof( device_setmode ) );

      /*----------------------------*/
     /*     Set Mode settings      */
    /*----------------------------*/
    device_setmode.class = DC$_TERM;
    device_setmode.type = TT$_UNKNOWN;
    device_setmode.u1.s1.basic_chars = 
                            TT$M_NOBRDCST      /* Do not receive broadcasts */
                            | TT$M_NOECHO           /* Do not display input */
                        /*| TT$M_HALFDUP */ /* Half-duplex mode (VMS def'n) */
                            | TT$M_HOSTSYNC         /* I think this is good */
                        /*    | TT$M_TTSYNC */        /* NOT applic. to LAT */
                        /*    | TT$M_READSYNC */      /* NOT applic. to LAT */
			      | TT$M_MODEM       /* use modem signals - DTR */
                            | TT$M_NOTYPEAHD /* Typeahead off between reads */
                            | TT$M_LOWER;               /* Allow lower case */

    /* 
     *  I'm not sure about this.  I'm probably confusing
     *  character size and data size here.
     *  But, I know that LakeShore devices (7 data bits)
     *  must have the NOEIGHTBIT characteristic.
     */
    if( camp_getIfRs232Data( pIF->defn ) == 8 )
    {
        device_setmode.u1.s1.basic_chars |= TT$M_EIGHTBIT;
    }
    device_setmode.ext_chars =  TT2$M_PASTHRU
                                | TT2$M_XON;   /* use XON/XOFF flow control */
    device_setmode.page_width = 80;
    /* must set page_len AFTER basic_chars (they're in the same union) */
    device_setmode.u1.s2.page_len = 24;

    baud = getBaud( camp_getIfRs232Baud( pIF->defn ) );
    parity = getParity( camp_getIfRs232Parity( pIF->defn, str ) );

    /*
     *  set port mode
     *  note:  P2 (size of set mode buffer ) is 8 because this
     *               is the size without extended characteristics which
     *               are not setable without LOG_IO or PHY_IO
     *               (don't need any anyway)
     */
    status = sys$qiow( 0,                   /* efn */
                       ioChannel,   /* chan */
                       IO$_SETMODE,         /* func */
                       &port_qiosb,         /* iosb */
                       0, 0,                /* astadr, astprm */
                       &device_setmode,     /* P1 */
                       12,                  /* P2 */
                       baud,                /* P3 */
                       0,                   /* P4 - crlf_fill */
                       parity,              /* P5 */
                       0 );                 /* P6 */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio setting port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }
    if( _failure( port_qiosb.status ) )
    {
        camp_appendMsg( "failed setting port %s", port );
        camp_appendVMSMsg( port_qiosb.status );
        return( port_qiosb.status );
    }

    return( CAMP_SUCCESS );
}


/*
 *  rs232_tt_disconnect,  disconnect LAT port
 */
int 
rs232_tt_disconnect( CAMP_IF* pIF )
{
    int status;
    struct qiosb port_qiosb;
    char port[LEN_STR+1];
    u_short ioChannel;

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );

    /*
     *  disconnect the port
     */
    status = sys$qiow(  0,
                        ioChannel,
                        IO$_TTY_PORT | IO$M_LT_DISCON,
                        &port_qiosb,
                        0,0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0 );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio disconnecting port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    if( _failure( port_qiosb.status ) )
    {
        camp_appendMsg( "failed qio disconnecting port %s", port );
        camp_appendVMSMsg( port_qiosb.status );
        return( port_qiosb.status );
    }

    return( CAMP_SUCCESS );
}


/*
 *  rs232_tt_free,  deassign and deallocate LAT port
 */
int 
rs232_tt_free( CAMP_IF* pIF )
{
    int status;
    s_dsc dscname;
    char port[LEN_STR+1];
    u_short ioChannel;

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );

    setdsctostr( &dscname, port );

    /*
     *  deassign the port
     */
    status = sys$dassgn( ioChannel );        /* chan */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed deassigning port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    /*
     *  deallocate the port
     */
    status = sys$dalloc( &dscname, 0 );
    if( _failure( status ) )
    {
        camp_appendMsg( "failed deallocating port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    return( CAMP_SUCCESS );
}


/*
 *  rs232_tt_wwrite,  write a string to a LAT port using sys$qiow WRITEVBLK
 */
int
rs232_tt_wwrite( CAMP_IF* pIF, char* cmd, int cmd_len )
{
    int status;
    struct qiosb port_qiosb;
    u_long term;
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );

    term = getWriteTerm( camp_getIfRs232WriteTerm( pIF->defn, str ) );

    /*
     *  do the qiow WRITEVBLK
     */
    status = sys$qiow( 0,                    /* efn */
                       ioChannel,    /* chan */
                       IO$_WRITEVBLK         /* func */
                       | IO$M_NOFORMAT   
                       | IO$M_CANCTRLO,
                       &port_qiosb,          /* iosb */
                       0,                    /* AST addr */
                       0,                    /* AST param */
                       cmd,                  /* P1 */
                       cmd_len,              /* P2 */
                       0,                    /* P3 */
                       term,                 /* P4 */
                       0,                    /* P5 */
                       0 );                  /* P6 */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio writing port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    if( _failure( port_qiosb.status ) )
    {
        camp_appendMsg( "failed writing port %s", port );
        camp_appendVMSMsg( port_qiosb.status );
        return( port_qiosb.status );
    }

    return( port_qiosb.status );
}


/*
 *  rs232_tt_qwrite,  write a string to a LAT port using sys$qiow WRITEVBLK
 */
int
rs232_tt_qwrite( CAMP_IF* pIF, char* cmd, int cmd_len,
                 u_long efn, struct qiosb* pIosb,
                 void (**pASTAddr)(), u_long ASTParam )
{
    int status;
    u_long term;
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );

    term = getWriteTerm( camp_getIfRs232WriteTerm( pIF->defn, str ) );

    /*
     *  do the qiow WRITEVBLK
     */
    status = sys$qio(  efn,                 /* efn */
                       ioChannel,   /* chan */
                       IO$_WRITEVBLK        /* func */
                       | IO$M_NOFORMAT   
                       | IO$M_CANCTRLO,
                       pIosb,               /* iosb */
                       pASTAddr,            /* AST addr */
                       ASTParam,            /* AST param */
                       cmd,                 /* P1 */
                       cmd_len,             /* P2 */
                       0,                   /* P3 */
                       term,                /* P4 */
                       0,                   /* P5 */
                       0 );                 /* P6 */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio writing port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    return( status );
}


/*
 *  rs232_tt_wread,  read a string from a LAT port using sys$qiow READVBLK
 */
int 
rs232_tt_wread( CAMP_IF* pIF, char* buf, int buf_len, 
                int* pRead_len )
{
    int status;
    struct qiosb port_qiosb;
    u_long term[2];
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];
    long timeout;

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );
    getReadTerm( camp_getIfRs232ReadTerm( pIF->defn, str ), term );
    timeout = camp_getIfRs232Timeout( pIF->defn );

    /*
     *  do the qiow READVBLK
     */
    status = sys$qiow( 0,                        /* efn */
                       ioChannel,        /* chan */
                       IO$_READVBLK              /* func */
                       | IO$M_NOECHO
                       | IO$M_NOFILTR
                       | IO$M_TIMED,
                       &port_qiosb,              /* iosb */
                       0,                        /* AST */
                       0,                        /* AST arg */
                       buf,                      /* buf_addr */
                       buf_len,                  /* buf_len */
                       labs( timeout ),          /* t/o */
                       term,                     /* term */
                       0,                        /* prompt_addr */
                       0 );                      /* prompt_len */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio reading port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    if( _failure( port_qiosb.status ) )
    {
        if( port_qiosb.status == SS$_TIMEOUT )
        {
            camp_appendMsg( "timeout reading port %s", port );
        }
        else
        {
            camp_appendMsg( "failed reading port %s", port );
            camp_appendVMSMsg( port_qiosb.status );
            return( port_qiosb.status );
        }
    }

    *pRead_len = port_qiosb.u1.s1.d1;

    return( port_qiosb.status );
}


/*
 *  rs232_tt_qread,  read a string from a LAT port using sys$qio READVBLK
 */
int 
rs232_tt_qread( CAMP_IF* pIF, char* buf, int buf_len,
                u_long efn, struct qiosb* pIosb,
                void (**pASTAddr)(), u_long ASTParam )
{
    int status;
    u_long term[2];
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];
    long timeout;

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );
    getReadTerm( camp_getIfRs232ReadTerm( pIF->defn, str ), term );
    timeout = camp_getIfRs232Timeout( pIF->defn );

    /*
     *  do the qiow READVBLK
     */
    status = sys$qio(  efn,                     /* efn */
                       ioChannel,       /* chan */
                       IO$_READVBLK             /* func */
                       | IO$M_NOECHO
                       | IO$M_NOFILTR
                       | IO$M_TIMED,
                       pIosb,                   /* iosb */
                       pASTAddr, 
                       ASTParam,                /* AST */
                       buf,                     /* buf_addr */
                       buf_len,                 /* buf_len */
                       labs( timeout ),     /* t/o */
                       term,                    /* term */
                       0,                       /* prompt_addr */
                       0 );                     /* prompt_len */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio reading port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    return( status );
}


/*
 *  rs232_tt_wread_prompt,  
 */
int 
rs232_tt_wread_prompt( CAMP_IF* pIF,
                       char* buf, int buf_len, int* pRead_len, 
                       char* prompt, int prompt_len )
{
    int status;
    struct qiosb port_qiosb;
    u_long term[2];
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];
    long timeout;

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );
    getReadTerm( camp_getIfRs232ReadTerm( pIF->defn, str ), term );
    timeout = camp_getIfRs232Timeout( pIF->defn );

    /*
     *  do the qiow READPROMPT
     */
    status = sys$qiow( 0,                       /* efn */
                       ioChannel,       /* chan */
                       IO$_READPROMPT           /* func */
                       | IO$M_NOECHO
                       | IO$M_NOFILTR
                       | IO$M_PURGE
                       | IO$M_TIMED,
                       &port_qiosb,             /* iosb */
                       0,                       /* AST */
                       0,                       /* AST arg */
                       buf,                     /* buf_addr */
                       buf_len,                 /* buf_len */
                       labs( timeout ),     /* t/o */
                       term,                    /* term */
                       prompt,                  /* prompt_addr */
                       prompt_len );            /* prompt_len */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio reading port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    if( _failure( port_qiosb.status ) )
    {
        if( port_qiosb.status == SS$_TIMEOUT )
        {
            camp_appendMsg( "timeout reading port %s", port );
        }
        else
        {
            camp_appendMsg( "failed reading port %s", port );
            camp_appendVMSMsg( port_qiosb.status );
            return( port_qiosb.status );
        }
    }

    *pRead_len = port_qiosb.u1.s1.d1;

    return( port_qiosb.status );
}


/*
 *  rs232_tt_qread_prompt,  
 */
int 
rs232_tt_qread_prompt( CAMP_IF* pIF,
                       char* buf, int buf_len, 
                       char* prompt, int prompt_len,
                       u_long efn, struct qiosb* pIosb,
                       void (**pASTAddr)(), u_long ASTParam )
{
    int status;
    u_long term[2];
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];
    long timeout;

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );
    getReadTerm( camp_getIfRs232ReadTerm( pIF->defn, str ), term );
    timeout = camp_getIfRs232Timeout( pIF->defn );

    /*
     *  IGNORE THE FOLLOWING COMMENT WHEN PORTS IN "PASTHRU" MODE
     *  Must always send a CR or prompt buffer 
     *  (~80 chars) will fill up.
     *  I don't know what happens when the terminator
     *  contains a CR (i.e., that is intended to be sent)
     */

    /*
     *  do the qiow READPROMPT
     */
    status = sys$qio(  efn,                     /* efn */
                       ioChannel,       /* chan */
                       IO$_READPROMPT           /* func */
                       | IO$M_NOECHO
                       | IO$M_NOFILTR
                       | IO$M_PURGE
                       | IO$M_TIMED,
                       pIosb,                     /* iosb */
                       pASTAddr,                /* AST */
                       ASTParam,                /* AST arg */
                       buf,                     /* buf_addr */
                       buf_len,                 /* buf_len */
                       labs( timeout ),     /* t/o */
                       term,                    /* term */
                       prompt,                   /* prompt_addr */
                       prompt_len );             /* prompt_len */
    if( _failure( status ) )
    {
        camp_appendMsg( "failed qio reading port %s", port );
        camp_appendVMSMsg( status );
        return( status );
    }

    return( status );
}


/*
 *  rs232_tt_emptyBuffer,  keep reading with timeout 0 until nothing read
 */
int 
rs232_tt_emptyBuffer( CAMP_IF* pIF )
{
    int status;
    struct qiosb port_qiosb;
    u_long term[2];
#define BUF_LEN 256
    char buf[BUF_LEN];
    int buf_len = BUF_LEN;
    int read_len;
    char port[LEN_STR+1];
    u_short ioChannel;
    char str[LEN_STR+1];

    camp_getIfRs232Port( pIF->defn, port );
    ioChannel = ioChan( pIF->priv );

    getReadTerm( camp_getIfRs232ReadTerm( pIF->defn, str ), term );

    do {
        /*
         *  do READVBLK with timeout zero
         */
        status = sys$qiow( 0,                      /* efn */
                           ioChannel,      /* chan */
                           IO$_READVBLK            /* func */
                           | IO$M_NOECHO
                           | IO$M_NOFILTR
                           | IO$M_TIMED,
                           &port_qiosb,            /* iosb */
                           0,                      /* AST */
                           0,                      /* AST arg */
                           buf,                    /* buf_addr */
                           buf_len,                /* buf_len */
                           0,                      /* t/o */
                           term,                   /* term */
                           0,                      /* prompt_addr */
                           0 );                    /* prompt_len */
        if( _failure( status ) )
        {
            return( status );
        }

        if( _failure( port_qiosb.status ) && 
            ( port_qiosb.status != SS$_TIMEOUT ) )
        {
            return( port_qiosb.status );
        }

        read_len = port_qiosb.u1.s1.d1;

    } while( !( ( port_qiosb.status == SS$_TIMEOUT ) && ( read_len == 0 ) ) );

    return( SS$_NORMAL );
}


static u_long
getBaud( u_long baud )
{
    switch( baud )
    {
        case  110:
            return( TT$C_BAUD_110 );
        case  300:
            return( TT$C_BAUD_300 );
        case  600:
            return( TT$C_BAUD_600 );
        case  1200:
            return( TT$C_BAUD_1200 );
        case  2400:
            return( TT$C_BAUD_2400 );
        case  4800:
            return( TT$C_BAUD_4800 );
        case  9600:
            return( TT$C_BAUD_9600 );
        case  19200:
            return( TT$C_BAUD_19200 );
        default:
            return( 0 );
    }
}


static void
getReadTerm( char* term, u_long termVMS[2] )
{
    TOKEN tok;

    findToken( term, &tok );

    switch( tok.kind )
    {
        case TOK_LF:
            bcopy( term_mask_LF, termVMS, 2*sizeof(u_long) );
            return;
        case TOK_CR:
            bcopy( term_mask_CR, termVMS, 2*sizeof(u_long) );
            return;
        case TOK_CRLF:
            bcopy( term_mask_CRLF, termVMS, 2*sizeof(u_long) );
            return;
        case TOK_LFCR:
            bcopy( term_mask_LFCR, termVMS, 2*sizeof(u_long) );
            return;
        case TOK_NONE:
            bcopy( term_mask_NONE, termVMS, 2*sizeof(u_long) );
            return;
        default:
            return;
    }
}


static u_long
getWriteTerm( char* term )
{
    TOKEN tok;

    findToken( term, &tok );

    switch( tok.kind )
    {
        case TOK_LF:
            return( postfix_LF );
        case TOK_CR:
            return( postfix_CR );
        case TOK_CRLF:
            return( postfix_CRLF );
        case TOK_LFCR:
            return( postfix_LFCR );
        case TOK_NONE:
            return( postfix_NONE );
        default:
            return( 0 );
    }
}


static u_long 
getParity( char* parity )
{
    TOKEN tok;

    findToken( parity, &tok );

    switch( tok.kind )
    {
        case TOK_NONE:
            return( NO_PARITY );
        case TOK_EVEN:
            return( EVEN_PARITY );
        case TOK_ODD:
            return( ODD_PARITY );
        default:
            return( 0 );
    }
}




