/*
 *  Name:       camp_srv_svc_mod.c
 *
 *  Purpose:    Dispatch RPC calls to the appropriate entry point.
 *              Handle all thread specific data structures and most of the
 *              issues of multithreading.
 *
 *              This is the most important file in understanding the
 *              multithreading of CAMP.
 *
 *  Called by:  CAMP server routines only (various).
 * 
 *  Revision history:
 *    09-Feb-2001  DJA Dump/Undump
 *    21-Dec-1999  TW  orig_trans (copy of transport structure) no longer
 *                     used.  All use of transports now in main thread
 *                     _exclusively_.  Only main thread handles (its own)
 *                     RPC transport structures.
 *    21-Dec-1999  TW  free_thread mutex and condition no longer used
 *                     find_free_thread (and try_find_free_thread) now 
 *                     check explicitly for a free thread.
 *
 *  $Log: camp_srv_svc_mod.c,v $
 *  Revision 1.1  2015/03/16 23:05:59  suz
 *  this file modified and renamed src/camp_srv_svc.c by Ted
 *
 *  Revision 1.4  2004/12/18 01:02:28  asnd
 *  Alter debugging messages
 *
 *  Revision 1.3  2001/02/10 07:14:49  asnd
 *  DJA: insIfDump / insIfUndump API routines
 *
 */

#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include "camp_srv.h"

#ifndef MULTITHREADED
static struct svc_req* current_rqstp = NULL;
static SVCXPRT* current_transp = NULL;
/* static SVCXPRT orig_trans; */  /* 16-Dec-1999  TW  Added for proper timeout check - multithreading only */
static timeval_t tv_start;
static char* pResult;       /* 17-Dec-1999  TW  For compatibility with single threading, not used */
static short svc_served;    /* 17-Dec-1999  TW  For compatibility with single threading, not used */
#endif /* !MULTITHREADED */

typedef union {
                FILE_req campsrv_sysload_13_arg;
                FILE_req campsrv_syssave_13_arg;
                FILE_req campsrv_sysdir_13_arg;
                INS_ADD_req campsrv_insadd_13_arg;
                DATA_req campsrv_insdel_13_arg;
                INS_LOCK_req campsrv_inslock_13_arg;
                INS_LINE_req campsrv_insline_13_arg;
                INS_FILE_req campsrv_insload_13_arg;
                INS_FILE_req campsrv_inssave_13_arg;
                INS_IF_req campsrv_insifset_13_arg;
                INS_READ_req campsrv_insifread_13_arg;
                INS_WRITE_req campsrv_insifwrite_13_arg;
                INS_DUMP_req campsrv_insifdump_13_arg;
                INS_UNDUMP_req campsrv_insifundump_13_arg;
                DATA_req campsrv_insifon_13_arg;
                DATA_req campsrv_insifoff_13_arg;
                DATA_SET_req campsrv_varset_13_arg;
                DATA_POLL_req campsrv_varpoll_13_arg;
                DATA_ALARM_req campsrv_varalarm_13_arg;
                DATA_LOG_req campsrv_varlog_13_arg;
                DATA_req campsrv_varzero_13_arg;
                DATA_SET_req campsrv_vardoset_13_arg;
                DATA_req campsrv_varread_13_arg;
                DATA_SET_req campsrv_varlnkset_13_arg;
                DATA_GET_req campsrv_varget_13_arg;
                CMD_req campsrv_cmd_13_arg;
} argument_t;

#ifdef MULTITHREADED

/*
 *  Possible thread states
 */
#define ST_INIT 0
#define ST_RUNNING 1
#define ST_FINISHED 2

/*
 *  Thread private data structure
 */
typedef struct {
    pthread_t thread_handle;
    pthread_mutex_t mutex_handle;
    short state;               /* ST_INIT, ST_RUNNING or ST_FINISHED */
    short type;                /* MAIN_THREAD_TYPE, SVC_THREAD_TYPE or
                                  POLL_THREAD_TYPE */
    struct svc_req* rqstp;     /* need this to call camp_srv_proc's from
                                  camp_tcl (many) and camp_cfg_write
                                  (campsrv_inssave_13()) */
    SVCXPRT* transp;
    /* SVCXPRT orig_trans; */  /* 16-Dec-1999  TW  Copy of original transport */
    argument_t argument;
    char* pResult;             /* 17-Dec-1999  TW  Result of RPC call filled
                                  by the SVC thread and used by the main thread
                                  to send a response to the client program */
    u_long xdr_flag;           /* how to xdr for this thread */
    char ident[LEN_IDENT+1];   /* instrument ident */
    char* msg;                 /* allocate dynamically */
    REQ* pReq;                 /* for poll threads */
    timeval_t tv_start;        /* for svc threads */
    Tcl_Interp* interp;        /* separate Tcl interpreter for thread */
    fd_set readfds;            /* 17-Dec-1999  TW  Thread does all RPC work */
    short served;              /* for svc threads */
    short haveGlobalMutexCount;  /* 19-Dec-1999  TW  Count the number of times
                                  this thread has claimed the global mutex
                                  In implementation, this is used to restrict
                                  the number of times to a maximum of 1 */
} THREAD_DATA;

/*
 *  Declare the thread private data
 */
#define NUM_THREADS 13
#define MAX_NUM_POLL_THREADS (NUM_THREADS-3)   /* Guarantee 2 SVC threads */
static THREAD_DATA thread_data[NUM_THREADS];

/*
 *  Types of threads can be:  main (the main server thread), SVC (handle RPC
 *  requests) and Poll (do variable polling).
 */
#define MAIN_THREAD_TYPE 0 
#define SVC_THREAD_TYPE 1
#define POLL_THREAD_TYPE 2 

static pthread_key_t thread_key;

/*  21-Dec-1999  TW  free_thread condition no longer used */
/* static pthread_mutex_t free_thread_mutex; */
/* static pthread_cond_t  free_thread_cv; */
/* static int             free_thread; */

/*
 *  The CAMP global mutex.  This is very important.  It allows restriction
 *  of processing to one thread at a time during operations which are not
 *  assured of being safe under multithreading conditions and also to block
 *  the use of resources to one thread.
 */
static pthread_mutex_t global_mutex;
/*  19-Dec-1999  TW  No longer used.  Was used to restrict access
                     to main Tcl interpreter.  Now use 'main_interp_mutex'
                     for this.
static int             global_mutex_noChange;
*/

/*
 *  Mutex that governs access to the shutdown_flag that signals the CAMP
 *  server to shutdown.
 */
static pthread_mutex_t shutdown_mutex;

/*
 *  19-Dec-1999  TW  main_interp_mutex for exclusive use of the main Tcl
 *                   interpreter.
 */
static pthread_mutex_t main_interp_mutex;

void thread_key_destructor( THREAD_DATA* pThread_data );
static void do_cleanup( int index, int final_state );
static void do_cleanup2( THREAD_DATA* pThread_data, int final_state );
static int find_free_thread( int* index, int type );
int try_find_free_thread( void );
pthread_addr_t svc_thread( pthread_addr_t arg );
pthread_addr_t rpc_svc_thread( pthread_addr_t arg );  /* 17-Dec-1999 TW */
pthread_addr_t poll_thread( pthread_addr_t arg );
static void start_svc_thread( struct svc_req *rqstp, SVCXPRT *transp );
void end_svc_thread( int arg );
static void do_service( struct svc_req *rqstp, SVCXPRT *transp );
#endif /* MULTITHREADED */

/*
 *  Used for multi- and single-threaded versions
 */
static int shutdown_flag = FALSE;
void srv_catch_timeout( void );
void srv_send_timeout_reply( struct svc_req *rqstp, SVCXPRT* transp );
void get_rq_proc( u_long rq_proc, bool_t (**pxdr_argument)(), 
		 bool_t (**pxdr_result)(), char *(**plocal)() );
bool_t test_lost_client( SVCXPRT* transp );
void srv_handle_served_svc_threads( void );


#ifdef MULTITHREADED
int
camp_thread_mutex_lock( pthread_mutex_t* mutex )
{
  int s; 

  while( ( s = pthread_mutex_trylock( mutex ) ) != 1 ) 
    {
      if( s == -1 ) return( -1 );

      camp_fsleep( 0.01 ); /* Wait for 10 ms */
    }
  return( 0 );
}
#endif /* MULTITHREADED */


void
thread_lock_global_np( void )
{
#ifdef MULTITHREADED
  THREAD_DATA* pThread_data;
  pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

/*  19-Dec-1999  TW  Changed method.  All CAMP mutex implementations (i.e., 
                     POSIX pthreads and VxWorks semaphores) are recursive,
                     so there is no need to avoid a global mutex being given
                     up in this way.
    if( global_mutex_noChange ) return;
*/
#if CAMP_DEBUG
  if( camp_debug > 2 )
    {
      if( pThread_data->haveGlobalMutexCount > 0 ) 
	{
	  if( pThread_data->type == MAIN_THREAD_TYPE ) printf( "main thread already has global lock\n" );
	  else if( pThread_data->type == SVC_THREAD_TYPE ) printf( "svc thread already has global lock\n" );
	}
      else
	{
	  if( pThread_data->type == MAIN_THREAD_TYPE ) printf( "main thread getting global lock\n" );
	  else if( pThread_data->type == SVC_THREAD_TYPE ) printf( "svc thread getting global lock\n" );
	}
    }
#endif
  if( pThread_data->haveGlobalMutexCount > 0 ) return;
  else
    {
      camp_thread_mutex_lock( &global_mutex );
      pThread_data->haveGlobalMutexCount++;
    }
#endif /* MULTITHREADED */
}


void
thread_unlock_global_np( void )
{
#ifdef MULTITHREADED
  THREAD_DATA* pThread_data;
  pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

/*  19-Dec-1999  TW  Changed method.  All CAMP mutex implementations (i.e., 
                     POSIX pthreads and VxWorks semaphores) are recursive,
                     so there is no need to avoid a global mutex being given
                     up in this way.
    if( global_mutex_noChange ) return;
*/
#if CAMP_DEBUG
  if( camp_debug > 2 )
    {
      if( pThread_data->haveGlobalMutexCount == 0 )
	{
	  if( pThread_data->type == MAIN_THREAD_TYPE ) printf( "main thread has no global lock to give\n" );
	  else if( pThread_data->type == SVC_THREAD_TYPE ) printf( "svc thread has no global lock to give\n" );
	}
      else
	{
	  if( pThread_data->type == MAIN_THREAD_TYPE ) printf( "main thread giving global lock\n" );
	  else if( pThread_data->type == SVC_THREAD_TYPE ) printf( "svc thread giving global lock\n" );
	}
    }
#endif
  if( pThread_data->haveGlobalMutexCount == 0 ) return;
  else
    {
      pthread_mutex_unlock( &global_mutex );
      pThread_data->haveGlobalMutexCount--;
    }
#endif /* MULTITHREADED */
}


void
set_global_mutex_noChange( int val )
{
/*  19-Dec-1999  TW  Changed method.
    global_mutex_noChange = val;
*/
#ifdef MULTITHREADED
  int i, count;
  THREAD_DATA* pThread_data;
  pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );
  /*
   *  19-Dec-1999  TW  Do this better.  New mutex (main_interp_mutex)
   *                   guards exclusive use of the main Tcl interpreter.
   */
#if CAMP_DEBUG
  if( camp_debug > 2 )
    {
      if( val == TRUE ) {
	if( pThread_data->type == MAIN_THREAD_TYPE ) printf( "main thread getting main interp lock\n" );
	else if( pThread_data->type == SVC_THREAD_TYPE ) printf( "svc thread getting main interp lock\n" );
      } else {
	if( pThread_data->type == MAIN_THREAD_TYPE ) printf( "main thread giving main interp lock\n" );
	else if( pThread_data->type == SVC_THREAD_TYPE ) printf( "svc thread giving main interp lock\n" );
      }
    }
#endif /* CAMP_DEBUG */

  if( val == TRUE )
    {
      if( ( count = pThread_data->haveGlobalMutexCount ) > 0 )
	{
#if CAMP_DEBUG
	  if( camp_debug > 2 ) printf( "thread temporarily giving global lock %d times\n", count );
#endif /* CAMP_DEBUG */
	  for( i = 0; i < count; i++ )  pthread_mutex_unlock( &global_mutex );
	}
      camp_thread_mutex_lock( &main_interp_mutex );
      if( count > 0 )
	{
	  for( i = 0; i < count; i++ )  pthread_mutex_lock( &global_mutex );
	}
    }
  else if( val == FALSE )
    {
      pthread_mutex_unlock( &main_interp_mutex );
    }
#endif /* MULTITHREADED */
}


struct svc_req*
get_current_rqstp( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( pThread_data->rqstp );
#else
    return( current_rqstp );
#endif /* MULTITHREADED */
}

void
set_current_rqstp( struct svc_req* rqstp )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    pThread_data->rqstp = rqstp;
#else
    current_rqstp = rqstp;
#endif /* MULTITHREADED */
}


timeval_t*
get_tv_start( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( &pThread_data->tv_start );
#else
    return( &tv_start );
#endif /* MULTITHREADED */
}

void
set_tv_start( timeval_t* ptv )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    copytimeval( ptv, &pThread_data->tv_start );
#else
    copytimeval( ptv, &tv_start );
#endif /* MULTITHREADED */
}


SVCXPRT*
get_current_transp( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( pThread_data->transp );
#else
    return( current_transp );
#endif /* MULTITHREADED */
}

void
set_current_transp( SVCXPRT* transp )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    pThread_data->transp = transp;
#else
    current_transp = transp;
#endif /* MULTITHREADED */
}


/*
 *  16-Dec-1999  TW  Added for proper timeout check
 *  21-Dec-1999  TW  orig_trans no longer used
 */
#ifdef NOT_USED
SVCXPRT*
get_orig_transp( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( &(pThread_data->orig_trans) );
#else
    return( &orig_trans );
#endif /* MULTITHREADED */
}
#endif /* NOT_USED */

/*
 *  16-Dec-1999  TW  Added for proper timeout check
 *  21-Dec-1999  TW  orig_trans no longer used
 */
#ifdef NOT_USED
void
set_orig_transp( SVCXPRT* transp )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    bcopy( transp, &(pThread_data->orig_trans), sizeof( SVCXPRT ) );
#else
    bcopy( transp, &orig_trans, sizeof( SVCXPRT ) );
#endif /* MULTITHREADED */
}
#endif /* NOT_USED */


int
check_shutdown( void )
{
    int shutdown;

#ifdef MULTITHREADED
    camp_thread_mutex_lock( &shutdown_mutex );
#endif /* MULTITHREADED */

    shutdown = shutdown_flag;

#ifdef MULTITHREADED
    pthread_mutex_unlock( &shutdown_mutex );
#endif /* MULTITHREADED */

    return( shutdown );
}


void
set_shutdown( void )
{
#ifdef MULTITHREADED
    camp_thread_mutex_lock( &shutdown_mutex );
#endif /* MULTITHREADED */

    shutdown_flag = TRUE;

#ifdef MULTITHREADED
    pthread_mutex_unlock( &shutdown_mutex );
#endif /* MULTITHREADED */
}


Tcl_Interp*
get_thread_interp( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( pThread_data->interp );
#else
    return( camp_tclInterp() );
#endif /* MULTITHREADED */
}


void
set_thread_interp( Tcl_Interp* interp )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    pThread_data->interp = interp;
#endif /* MULTITHREADED */
}


#ifdef MULTITHREADED

void
thread_key_destructor( THREAD_DATA* pThread_data )
{
}


u_long
get_thread_xdr_flag( void )
{
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( pThread_data->xdr_flag );
}


void
set_thread_xdr_flag( u_long flag )
{
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    pThread_data->xdr_flag = flag;
}


char*
get_thread_ident( void )
{
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( pThread_data->ident );
}


void
set_thread_ident( char* ident )
{
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    strcpy( pThread_data->ident, ident );
}


char*
get_thread_msg( void )
{
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    return( pThread_data->msg );
}


void
set_thread_msg( char* msg )
{
    THREAD_DATA* pThread_data;
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );

    strcpy( pThread_data->msg, msg );
}


/*
 *  do_cleanup -  clean up a thread's private data structure before it exits
 */
static void
do_cleanup( int index, int final_state )
{
    do_cleanup2( &thread_data[index], final_state );
}


static void
do_cleanup2( THREAD_DATA* pThread_data, int final_state )
{
    camp_thread_mutex_lock( &pThread_data->mutex_handle );
    pThread_data->state = final_state;
    pThread_data->ident[0] = '\0';
    _free( pThread_data->msg );
    pThread_data->xdr_flag = CAMP_XDR_ALL;
    pThread_data->interp = NULL;
    pthread_mutex_unlock( &pThread_data->mutex_handle );

    /*  21-Dec-1999  TW  free_thread condition no longer used */
    /*
    camp_thread_mutex_lock( &free_thread_mutex );
    free_thread = TRUE;
    pthread_cond_signal( &free_thread_cv );
    pthread_mutex_unlock( &free_thread_mutex );
    */
}


/*
 *  find_free_thread
 *
 *  Wait indefinitely until a free thread is found.  The return is the
 *  integer ID of the free thread which is also the index to the array
 *  of private thread data structures.
 *
 *  Note:  can wait indefinitely for a thread to become free
 *         therefore, must be called without global lock
 */
static int
find_free_thread( int* index, int type )
{
    int i;
    int found = FALSE;
    int retry = FALSE;

    do 
    {
        /*
	 *  17-Dec-1999  TW  Fix possible deadlock here.
	 *                   Main thread can get into indefinite loop
	 *                   inside find_free_threads.  And, since the
	 *                   main thread now cleans up after svc threads
	 *                   could be a problem where no served threads
	 *                   are recovered while main thread waits in here.
	 *                   So, just check for handling served threads here.
	 *  21-Dec-1999  TW  Keep this here, but should no longer need it.
	 *                   In srv_loop_thread, RPC calls are no longer
	 *                   dispatched unless there is a free thread
	 *                   (tested with try_find_free_thread).
	 */
        if( retry ) 
	  {
#if CAMP_DEBUG
	    thread_lock_global_np();
	    if( camp_debug > 1 ) printf( "main thread waiting for free thread\n" );
	    thread_unlock_global_np();
#endif
	    camp_fsleep( 0.1 );    /* 100ms */
	    srv_handle_served_svc_threads();
	  }

	retry = TRUE;
	if( ( i = try_find_free_thread() ) > -1 ) found = TRUE;

    } while( !found );

    /*
     *  found must be TRUE here, in this implementation
     */
    if( found )
    {
        *index = i;

#if CAMP_DEBUG
	thread_lock_global_np();
	if( camp_debug > 1 ) printf( "main thread found free thread %d\n", i );
	thread_unlock_global_np();
#endif

        camp_thread_mutex_lock( &thread_data[i].mutex_handle );
        /*
         *  Allocate status message string
         */
        thread_data[i].msg = (char*)zalloc( MAX_LEN_MSG+1 );
        thread_data[i].type = type;
        /*
         *  Use main Tcl interpreter by default
         */
        thread_data[i].interp = camp_tclInterp();
        pthread_mutex_unlock( &thread_data[i].mutex_handle );
    }

    return( found );
}


/*
 *  try_find_free_thread
 *  
 *  Check if there are any free threads.  Return the integer
 *  corresponding to the first free thread index.
 *
 *  This routine does not wait indefinitely like find_free_thread
 */
int
try_find_free_thread( void )
{
    int i;
    int found;

    /*
     *  Check if there are any free threads
     */
    for( i = 0, found = FALSE; i < NUM_THREADS; i++ )
      {
	if( pthread_mutex_trylock( &(thread_data[i].mutex_handle) ) == 1 ) 
	  {
	    found = ( thread_data[i].state != ST_RUNNING );
	    pthread_mutex_unlock( &(thread_data[i].mutex_handle) );
	  }

	if( found ) return( i );
      }

    return( -1 );
}


/*
 *  kill_all_threads
 *
 *  In implementation, doesn't actually kill the threads, but waits until
 *  they have all finished normally.  Could consider a delay in the loop
 *  to relieve the processor a little.
 */
void
kill_all_threads( void )
{
    int i;
    bool_t running;

    /*
     *  Wait for threads to complete
     *  (doesn't actually cancel or join, pthread_join
     *  wasn't working, I don't know why).
     */
    for( i = 1; i < NUM_THREADS; i++ )
    {
      for( running = TRUE; running; )
	{
	  camp_thread_mutex_lock( &thread_data[i].mutex_handle );
	  if( thread_data[i].state != ST_RUNNING ) 
	    {
	      running = FALSE;
	    }
	  pthread_mutex_unlock( &thread_data[i].mutex_handle );
	}
    }

    for( i = 1; i < NUM_THREADS; i++ )
    {
	  camp_thread_mutex_lock( &thread_data[i].mutex_handle );
	  if( thread_data[i].state == ST_RUNNING ) 
	    {
	      printf( "error: thread %d is still running\n", i );
	    }
	  pthread_mutex_unlock( &thread_data[i].mutex_handle );
    }
}


/*
 *  Initialize the thread aspects of the main server thread (the first
 *  thread of execution)
 */
int
srv_init_main_thread( void )
{
    int i;
    int priority;

    find_free_thread( &i, MAIN_THREAD_TYPE );

    if( pthread_setspecific( thread_key, 
                             (pthread_addr_t)(&(thread_data[i])) ) != 0 )
    {
        camp_logMsg( "error setting main thread key" );
        return( CAMP_FAILURE );
    }

    if( camp_thread_mutex_lock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error locking main thread mutex" );
        return( CAMP_FAILURE );
    }

    thread_data[i].state = ST_RUNNING;

    if( pthread_mutex_unlock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error unlocking main thread mutex" );
        return( CAMP_FAILURE );
    }

    /*
     *  The main thread is now effectively just another thread
     *  Set the scheduling policy and priority 
     *  explicitely to the defaults.
     */
/*
    ERROR using this - I guess pthread_self() isn't working for the 
    main thread.  Oh well, I just wanted default behaviour anyway.

    priority = (PRI_FG_MAX_NP+PRI_FG_MIN_NP)/2;

    if( pthread_setscheduler( pthread_self(), SCHED_FG_NP, priority ) != 0 )
    {
        camp_logMsg( "error setting scheduling" );
        return( CAMP_FAILURE );
    }
*/

    return( CAMP_SUCCESS );
}


/*
 *  A routine that is called only once for all threads
 *
 *  This routine initializes the general multithreading state of the program.
 */
void
srv_init_thread_once( void )
{
    int i;

#ifdef VMS
    /*
     *  Set reentrancy behaviour properly for VMS
     */
#if (defined(__DECC)||defined(__DECCXX)) && defined(MULTITHREADED)
#include <reentrancy.h>
    DECC$SET_REENTRANCY( C$C_MULTITHREAD );
#endif /* __DECC && MULTITHREADED */
#endif /* VMS */

    /*
     *  Initialize thread data
     *  Thread data must be static so that
     *  find_free_thread methodology works
     *  (and also the shutdown methodology).
     */
    for( i = 0; i < NUM_THREADS; i++ )
    {
        if( pthread_mutex_init( &thread_data[i].mutex_handle,
                                pthread_mutexattr_default ) != 0 )
        {
            fprintf( stderr, "error initializing thread %d mutex\n", i );
            exit( 0 );
        }
        thread_data[i].state = ST_INIT;
        thread_data[i].ident[0] = '\0';
        thread_data[i].msg = NULL;
        thread_data[i].xdr_flag = CAMP_XDR_ALL;
	thread_data[i].haveGlobalMutexCount = 0;
    }

    /*
     *  Free thread mutex & condition value
     *  21-Dec-1999  TW  free_thread mutex/condition no longer used
     */
    /*
    if( pthread_mutex_init( &free_thread_mutex, pthread_mutexattr_default ) 
        != 0 )
    {
        fprintf( stderr, "error initializing free_thread_mutex\n" );
        exit( 0 );
    }
    if( pthread_cond_init( &free_thread_cv, pthread_condattr_default ) != 0 )
    {
        fprintf( stderr, "error initializing free_thread_cv\n" );
        exit( 0 );
    }
    */

    /*
     *  Initialize global mutex
     */
    if( pthread_mutex_init( &global_mutex, pthread_mutexattr_default ) 
        != 0 )
    {
        fprintf( stderr, "error initializing global_mutex\n" );
        exit( 0 );
    }
    /*  Not used  19-Dec-1999  TW
    global_mutex_noChange = FALSE;
    */

    /*
     *  Shutdown mutex & flag
     */
    if( pthread_mutex_init( &shutdown_mutex, pthread_mutexattr_default ) 
        != 0 )
    {
        fprintf( stderr, "error initializing shutdown_mutex\n" );
        exit( 0 );
    }
    shutdown_flag = FALSE;

    /*
     *  19-Dec-1999  TW  main_interp_mutex for exclusive use of the main Tcl interpreter.
     */
    if( pthread_mutex_init( &main_interp_mutex, pthread_mutexattr_default ) != 0 )
    {
        fprintf( stderr, "error initializing main_interp_mutex\n" );
        exit( 0 );
    }

    if( pthread_keycreate( &thread_key, thread_key_destructor ) != 0 )
    {
        fprintf( stderr, "error creating thread key\n" );
        exit( 0 );
    }

    /*
     *  In VxWorks, set scheduling policy once 
     *  for all tasks.
     *  I'm just guessing on a value here.
     */
#ifdef VXWORKS
    kernelTimeSlice( sysClkRateGet()*0.01 );  /* 0.01 seconds */
#endif /* VXWORKS */
}


/*
 *  srv_end_thread  -  clean up all thread related storage
 *  
 *  Called from main thread before program exits.
 */
void
srv_end_thread( void )
{
    int i;

    /*
     *  21-Dec-1999  TW  free_thread mutex/condition no longer used 
     */
    /*
    if( pthread_mutex_destroy( &free_thread_mutex ) != 0 )
    {
        fprintf( stderr, "failed deleting free_thread_mutex\n" );
    }
    if( pthread_cond_destroy( &free_thread_cv ) != 0 )
    {
        fprintf( stderr, "failed deleting free_thread_cv\n" );
    }
    */

    if( pthread_mutex_destroy( &global_mutex ) != 0 )
    {
        fprintf( stderr, "failed deleting global_mutex\n" );
    }

    if( pthread_mutex_destroy( &shutdown_mutex ) != 0 )
    {
        fprintf( stderr, "failed deleting shutdown_mutex\n" );
    }

    if( pthread_mutex_destroy( &main_interp_mutex ) != 0 )
    {
        fprintf( stderr, "failed deleting main_interp_mutex\n" );
    }

    for( i = 0; i < NUM_THREADS; i++ )
    {
        if( pthread_mutex_destroy( &thread_data[i].mutex_handle ) != 0 )
        {
            fprintf( stderr, "failed deleting thread %d mutex\n", i );
	}

        _free( thread_data[i].msg );
    }
}


/*
 *  start_poll_thread  -  start a poll thread, called from the main thread
 */
void
start_poll_thread( REQ* pReq )
{
    int i;
    pthread_attr_t thread_attr;

    /* 
     *  Allocate thread index for thread
     *  Note that the global lock is on here, so we'd
     *  better unlock it in case there are no free threads
     */
    thread_unlock_global_np();
    find_free_thread( &i, POLL_THREAD_TYPE );
    thread_lock_global_np();

    if( camp_thread_mutex_lock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error locking poll thread mutex" );
        return;
    }

    thread_data[i].pReq = pReq;
    thread_data[i].state = ST_RUNNING;

    if( pthread_mutex_unlock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error unlocking poll thread mutex" );
        return;
    }

    if( pthread_attr_create( &thread_attr ) != 0 )
    {
        camp_logMsg( "error creating poll thread attributes" );
        return;
    }

    /*
     *  Explicitely set to inherit scheduling
     *  policy and priority from creating thread
     */
    if( pthread_attr_setinheritsched( &thread_attr, 
                                      PTHREAD_INHERIT_SCHED ) != 0 )
    {
        camp_logMsg( "error setting poll thread attributes" );
        return;
    }

    if( pthread_create( &(thread_data[i].thread_handle), 
                        /* pthread_attr_default, */ thread_attr,
                        poll_thread, 
                        (pthread_addr_t)i ) != 0 )
    {
        camp_logMsg( "error creating poll thread" );
        return;
    }

    /*
     *  from DECthreads manual: "Threads that were created
     *  using this thread attributes object are not affected 
     *  by the deletion of the thread attributes object."
     */
    if( pthread_attr_delete( &thread_attr ) != 0 )
    {
        camp_logMsg( "error deleting poll thread attributes" );
        return;
    }

    /*
     *  Reclaim internal storage for thread
     *  immediately upon thread exit
     */
    if( pthread_detach( &(thread_data[i].thread_handle) ) != 0 )
    {
        camp_logMsg( "error detaching poll thread" );
        return;
    }
}


/*
 *  poll_thread  -  poll thread start point (where a poll thread begins
 *                  execution)
 */
pthread_addr_t
poll_thread( pthread_addr_t arg )
{
    int i = (int)arg;
    CAMP_VAR* pInsVar;
    int status;
    REQ* pReq;

    if( pthread_setspecific( thread_key, 
                             (pthread_addr_t)(&(thread_data[i])) ) != 0 )
    {
    	camp_logMsg( "poll: error setting poll thread key data" );
    }

/*  Tried using this to stop killing threads
    that you want to complete before exiting.
    Now do it another way.

    pthread_setcancel( CANCEL_OFF );
*/

    /*
     *  Bug here - what if one poll request node (in the
     *             linked list of requests) has two threads 
     *             pending (due to some hold up)
     *             and the first cancels the request, then
     *             the second will point to nothing -> CRASH
     *  Fixed -    have only one thread allowed per request
     */

    /*
     *  Call poll request with global lock ON
     */
    thread_lock_global_np();

    pReq = thread_data[i].pReq;
    pInsVar = varGetpIns( pReq->pVar );

    /*
     *  Call poll request with instrument lock ON
     */
    get_ins_thread_lock( pInsVar );

    /*
     *  Call the poll request procedure
     */
    status = (*pReq->procs.retProc)( pReq );

    release_ins_thread_lock( pInsVar );

    thread_unlock_global_np();

    /*
     *  Clean up thread specific data structure
     */
    do_cleanup( i, ST_FINISHED );

/*
    pthread_setcancel( CANCEL_ON );
    pthread_testcancel();
*/
/*
    pthread_exit( arg );
*/
    /*
     *  Return a value, this program doesn't check it.
     */
    return( (pthread_addr_t)arg );
}


/*
 *  Check to see if an RPC transport is associated with any active
 *  SVC thread
 */
int
check_transport_in_use( SVCXPRT* transp )
{
  int i;
  bool_t transport_in_use;

  transport_in_use = FALSE;
  for( i = 0; i < NUM_THREADS; i++ )
    {
      camp_thread_mutex_lock( &(thread_data[i].mutex_handle) );
      if( ( thread_data[i].type == SVC_THREAD_TYPE ) &&
	 ( thread_data[i].state == ST_RUNNING ) &&
	 ( thread_data[i].transp == transp ) )
	{
	  transport_in_use = TRUE;
	}
      pthread_mutex_unlock( &(thread_data[i].mutex_handle) );
      if( transport_in_use ) break;
    }

  if( transport_in_use ) return( i );
  else return( -1 );
}


/*
 *  srv_handle_served_svc_threads  -  CAMP server main thread calls this
 *         routine to check if there are any SVC threads that have finished
 *         processing the RPC request.  If so, the main thread sends the
 *         reply to the RPC client program and cleans up the thread data
 *         structure.
 *
 *  Preconditions:
 *              The global lock must be OFF.
 *
 *  Postconditions:
 *              The global lock is OFF again before returning to the calling
 *              routine.
 */
void
srv_handle_served_svc_threads( void )
{
  int i;
  SVCXPRT *transp;

#if CAMP_DEBUG
  if( camp_debug > 2 ) printf( "server in srv_handle_served_svc_threads\n" );
#endif

  for( i = 0; i < NUM_THREADS; i++ )
    {
      /*
       *  Take the thread lock before accessing the thread private data
       */
      if( pthread_mutex_trylock( &(thread_data[i].mutex_handle) ) == 1 ) 
	{
          /*
           *  Check for SVC threads that are active (ST_RUNNING) but also
           *  served (finished processing)
           */
	  if( ( thread_data[i].type == SVC_THREAD_TYPE ) &&
	     ( thread_data[i].state == ST_RUNNING ) &&
	     ( thread_data[i].served == TRUE ) )
	    {
#if CAMP_DEBUG
	      if( camp_debug > 1 ) printf( "server ending thread %d\n", i );
#endif

              /*
               *  Unlock a thread lock before taking the global lock
               *  This relieves possible deadlocks
               */
	      pthread_mutex_unlock( &(thread_data[i].mutex_handle) );

              /*
               *  Take global lock before calling dangerous RPC calls in
               *  end_svc_thread and SVC_STAT/SVC_DESTROY
               */
	      thread_lock_global_np();
	      
              /*
               *  Reply to the RPC client program with the result of the
               *  request and clean up the SVC thread data structure and state.
               */
	      end_svc_thread( i );

	      /*
	       *  This moved from svc_getreqset
	       *  checks whether transport has died
	       *  If so, destroy it.
	       */
	      transp = thread_data[i].transp;
#if CAMP_DEBUG
	      if( camp_debug > 1 ) printf( "main thread checking thread transport\n" );
#endif

	      /*
	       *  Might not need this here.  SVC_DESTROY in my_svc_getreqset
               *  may be sufficient.  But, it does not hurt.
	       */
	      if( SVC_STAT( transp ) == XPRT_DIED )
		{
#if CAMP_DEBUG
		  if( camp_debug > 1 ) printf( "main thread destroying thread transport\n" );
#endif
		  SVC_DESTROY( transp );
		}

	      thread_unlock_global_np();
	    }
	  else
	    {
	      pthread_mutex_unlock( &(thread_data[i].mutex_handle) );
	    }
	}
    }

#if CAMP_DEBUG
  if( camp_debug > 2 ) printf( "server leaving srv_handle_served_svc_threads\n" );
#endif

}


/*
 *  start_svc_thread  -  start an svc (service) thread from the main thread
 *
 *  Preconditions:
 *              The global lock must be ON.
 *
 *  Postconditions:
 *              The global lock is ON again before returning to the calling
 *              routine.
 */
static void
start_svc_thread( struct svc_req *rqstp, SVCXPRT *transp )
{
    int i;
    pthread_attr_t thread_attr;
    struct authunix_parms* aupp;
    bool_t (*xdr_argument)(), (*xdr_result)();
    char *(*local)();
    timeval_t tv;

    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &local );

    /* 
     *  Allocate thread index for thread
     *  This routine is called within global lock, so unlock
     *  global lock while calling find_free_thread
     */
    thread_unlock_global_np();
    find_free_thread( &i, SVC_THREAD_TYPE );
    thread_lock_global_np();

    if( camp_thread_mutex_lock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error locking svc thread mutex" );
        return;
    }

    /*  
     *  16-Dec-1999  TW  Be sure to initialize all thread parameters
     *                   before spawning thread 
     */
    gettimeval( &tv );
    copytimeval( &tv, &(thread_data[i].tv_start) ); /* Added 16-Dec-1999 TW */
    thread_data[i].served = FALSE;
    thread_data[i].state = ST_RUNNING;
    thread_data[i].transp = transp;
    /* bcopy( transp, &(thread_data[i].orig_trans), sizeof( SVCXPRT ) ); */ /*  16-Dec-1999  TW  */
    strcpy( thread_data[i].msg, "" );   /* 16-Dec-1999  TW */
    thread_data[i].xdr_flag = CAMP_XDR_ALL;    /* was set_current_xdr_flag( CAMP_XDR_ALL ); */

    /*
     *  Make a copy of rqstp
     *  This is necessary even when managing our own svc_getreqset
     *  because some of the authunix structure is managed inside the
     *  internal RPC dispatch (between svc_getreqset and camp_srv_13).
     */
    thread_data[i].rqstp = (struct svc_req*)zalloc( sizeof( struct svc_req ) );
    bcopy( rqstp, thread_data[i].rqstp, sizeof( struct svc_req ) );
    thread_data[i].rqstp->rq_clntcred = 
        (caddr_t)zalloc( sizeof( struct authunix_parms ) );
    bcopy( rqstp->rq_clntcred, thread_data[i].rqstp->rq_clntcred,
        sizeof( struct authunix_parms ) );
    aupp = (struct authunix_parms*)thread_data[i].rqstp->rq_clntcred;
    aupp->aup_machname = strdup( 
        ((struct authunix_parms*)rqstp->rq_clntcred)->aup_machname );
    aupp->aup_gids = (int*)zalloc( (aupp->aup_len)*sizeof( int ) );
    bcopy( ((struct authunix_parms*)rqstp->rq_clntcred)->aup_gids,
           aupp->aup_gids, 
           (aupp->aup_len)*sizeof( int ) );

    /* 
     *  Get args
     *  RPC library is non-reentrant, must be inside global lock
     *  (start_svc_thread routine called from within global lock)
     *  
     *  Call svc_getargs() from the main thread (svc listener) only.
     *  Under VxWorks, calling svc_getargs() from a separate
     *  task doesn't work (doesn't inherit RPC server capabilities).
     *  Luckily, svc_sendreply does work from other threads or tasks.
     */
    bzero( &(thread_data[i].argument), sizeof( argument_t ) );
    if (!svc_getargs(transp, xdr_argument, &(thread_data[i].argument) )) {
        svcerr_decode(transp);
        return;
    }

    if( pthread_mutex_unlock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error unlocking svc thread mutex" );
        return;
    }

    if( pthread_attr_create( &thread_attr ) != 0 )
    {
        camp_logMsg( "error creating svc thread attributes" );
        return;
    }

    /*
     *  Explicitely set to inherit scheduling
     *  policy and priority from creating thread
     */
    if( pthread_attr_setinheritsched( &thread_attr, 
                                      PTHREAD_INHERIT_SCHED ) != 0 )
    {
        camp_logMsg( "error setting svc thread attributes" );
        return;
    }

    if( pthread_create( &(thread_data[i].thread_handle), 
                        thread_attr, /* pthread_attr_default, */ 
                        svc_thread, 
                        (pthread_addr_t)i ) != 0 )
    {
        camp_logMsg( "error creating svc thread" );
        return;
    }

    /*
     *  from DECthreads manual: "Threads that were created
     *  using this thread attributes object are not affected 
     *  by the deletion of the thread attributes object."
     */
    if( pthread_attr_delete( &thread_attr ) != 0 )
    {
        camp_logMsg( "error deleting svc thread attributes" );
        return;
    }

    /*
     *  Reclaim internal storage for thread
     *  immediately upon thread exit
     */
    if( pthread_detach( &(thread_data[i].thread_handle) ) != 0 )
    {
        camp_logMsg( "error detaching thread" );
        return;
    }
}


/*
 *  end_svc_thread() - finish processing service thread
 *                     called from main thread
 *
 *  Purpose:  Send RPC reply, then clean up after an svc thread
 *            Done from the main thread because it is safer to 
 *            call all RPC from the main thread on VxWorks
 *
 *  Preconditions:
 *              The global lock must be ON.
 *
 *  Postconditions:
 *              The global lock is ON again before returning to the calling
 *              routine.
 */
void 
end_svc_thread( int arg )
{
    int i = (int)arg;
    char *result;
    bool_t (*xdr_argument)(), (*xdr_result)();
    char *(*local)();
    THREAD_DATA* pThread_data;
    argument_t* pArgument;
    bool_t lost_client;
    XDR x;
    struct authunix_parms* aupp;
    struct svc_req *rqstp;
    SVCXPRT *transp;

    if( i == -1 )
      {
	/*  Use this when called from svc thread (obsolete) */
	pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );
      }
    else
      {  
	/*  Use this when called from main thread */
	pThread_data = &(thread_data[i]);
      }
    
    pArgument = &(pThread_data->argument);
    transp = pThread_data->transp;
    rqstp = pThread_data->rqstp;
    result = pThread_data->pResult;

    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &local );

    if( !pThread_data->served )
      {
	camp_logMsg( "error ending thread that hasn't been served" );
	return;
      }

    /*
     *  Test whether client still exists
     *  Sending reply to absent client can cause unwanted behaviour
     */
    /* lost_client = test_lost_client( transp, &(pThread_data->orig_trans) ); */
    lost_client = test_lost_client( transp );

#if CAMP_DEBUG
    if( camp_debug > 3 )
      {
	printf( "lost_client = %d\n", lost_client );

	printf( "before svc_sendreply\n" );
	printf( "served             = %d\n", pThread_data->served );
	printf( "transp             = %p\n", transp );
	printf( "transp->xp_sock    = %d\n", transp->xp_sock );
	printf( "transp->xp_port    = %d\n", transp->xp_port );
	printf( "transp->xp_ops     = %p\n", transp->xp_ops );
	printf( "transp->xp_addrlen = %d\n", transp->xp_addrlen );
	printf( "transp->xp_raddr.sin_family = %d\n", transp->xp_raddr.sin_family );
	printf( "transp->xp_raddr.sin_port   = %d\n", transp->xp_raddr.sin_port );
	printf( "transp->xp_raddr.sin_addr.s_addr = %x\n", transp->xp_raddr.sin_addr.s_addr );
	printf( "transp->xp_p1      = %p\n", transp->xp_p1 );
	printf( "SVC_STAT( transp ) = %d\n", SVC_STAT( transp ) );
      }
#endif /* CAMP_DEBUG */

    if( lost_client )
      {
#if CAMP_DEBUG
	if( camp_debug > 1 )
	  {
	    camp_logMsg( "lost client connection, not sending reply" );
	    printf( "lost client connection, not sending reply\n" );
	  }
#endif /* CAMP_DEBUG */
      }
    else
      {
	/*
	 *  CAUTION.  We are in the main thread.  But, we want
	 *  the xdr_flag to be equal to the thread's xdr_flag when
	 *  sending the RPC reply.  So, temporarilly set the main
	 *  thread's xdr_flag to the svc thread's for the svc_sendreply.
	 *  Afterwards, set back to CAMP_XDR_ALL
	 */
	set_current_xdr_flag( pThread_data->xdr_flag );

	/* 
	 *  Send reply
	 *  No need for global lock around RPC library calls here
	 *  do_service is called inside global lock
	 */
	if( ( result != NULL ) && !svc_sendreply( transp, xdr_result, result ) )
	  {
	    svcerr_systemerr(transp);
	  }

	set_current_xdr_flag( CAMP_XDR_ALL );
      }

    _free( result );

    /* 
     *  Free RPC arguments
     */
    if( !svc_freeargs( transp, xdr_argument, pArgument ) ) 
      {
	camp_logMsg( "unable to free arguments" );
      }

    /*
     *  Free up RPC request storage allocated in start_svc_thread
     */
    aupp = (struct authunix_parms*)(pThread_data->rqstp->rq_clntcred);
    free( aupp->aup_machname );
    free( aupp->aup_gids );
    free( aupp );
    free( pThread_data->rqstp );

    pThread_data->state = ST_FINISHED;
    pThread_data->ident[0] = '\0';
    _free( pThread_data->msg );
    pThread_data->xdr_flag = CAMP_XDR_ALL;
    pThread_data->interp = NULL;

    /*  21-Dec-1999  TW  free_thread condition no longer used */
    /*
    camp_thread_mutex_lock( &free_thread_mutex );
    free_thread = TRUE;
    pthread_cond_signal( &free_thread_cv );
    pthread_mutex_unlock( &free_thread_mutex );
    */
 
    camp_setMsg( "" );
}


/*
 *  svc_thread  -  starting point of a service thread (where an SVC thread
 *                 begins execution)
 */
pthread_addr_t
svc_thread( pthread_addr_t arg )
{
     int i = (int)arg;

    /*
     *  Set the thread_key to the pointer to the thread data structure
     *  This allows access to the structure from within the thread
     */
    if( pthread_setspecific( thread_key, 
                             (pthread_addr_t)(&(thread_data[i])) ) != 0 )
    {
    	camp_logMsg( "error setting thread key data" );
        return( (pthread_addr_t)(-1) );
    }

#ifdef VXWORKS
     /*  17-Dec-1999  TW  No longer needed.  Only main thread calls RPCs */
     /* rpcTaskInit(); */   /* must be done for every task that uses RPCs */
#endif /* VXWORKS */

    /*
     *  do_service (i.e., service processing) must be called from 
     *  within global lock.  This is required because we cannot be
     *  sure that the Tcl and RPC libraries (etc.) are thread safe.
     *  The global lock is released within the service during periods
     *  of lengthy waiting (RS232 and GPIB reads/writes).
     */
    thread_lock_global_np();

#if CAMP_DEBUG
    if( camp_debug > 1 ) printf( "svc thread %d calling do_service\n", i );
#endif

    do_service( thread_data[i].rqstp, thread_data[i].transp );

#if CAMP_DEBUG
    if( camp_debug > 1 ) printf( "svc thread %d finished\n", i );
#endif

    thread_unlock_global_np();

#if CAMP_DEBUG
    if( camp_debug > 1 ) printf( "svc thread %d finished and unlocked global mutex\n", i );
#endif

    /*
     *  Return a value, this program doesn't check it.
     */
    return( (pthread_addr_t)arg );
}

#endif /* MULTITHREADED */


/*
 *  Get the RPC parameters for the specific RPC call.  These are used
 *  to manage the arguments and the result of the call through XDR and
 *  to get the pointer to the right dispatch entry point.
 *
 *  Used for both multithreaded and single-threaded
 */
void
get_rq_proc( u_long rq_proc, 
	    bool_t (**pxdr_argument)(), bool_t (**pxdr_result)(),
	    char *(**plocal)() )
{
    switch( rq_proc ) {
        case CAMPSRV_SYSRUNDOWN:
                *pxdr_argument = xdr_void;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_sysrundown_13;
                break;

        case CAMPSRV_SYSUPDATE:
                *pxdr_argument = xdr_void;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_sysupdate_13;
                break;

        case CAMPSRV_SYSLOAD:
                *pxdr_argument = xdr_FILE_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_sysload_13;
                break;

        case CAMPSRV_SYSSAVE:
                *pxdr_argument = xdr_FILE_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_syssave_13;
                break;

        case CAMPSRV_SYSGET:
                *pxdr_argument = xdr_void;
                *pxdr_result = xdr_CAMP_SYS_res;
                *plocal = (char *(*)()) campsrv_sysget_13;
                break;

        case CAMPSRV_SYSGETDYNA:
                *pxdr_argument = xdr_void;
                *pxdr_result = xdr_SYS_DYNAMIC_res;
                *plocal = (char *(*)()) campsrv_sysgetdyna_13;
                break;

        case CAMPSRV_SYSDIR:
                *pxdr_argument = xdr_FILE_req;
                *pxdr_result = xdr_DIR_res;
                *plocal = (char *(*)()) campsrv_sysdir_13;
                break;

        case CAMPSRV_INSADD:
                *pxdr_argument = xdr_INS_ADD_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insadd_13;
                break;

        case CAMPSRV_INSDEL:
                *pxdr_argument = xdr_DATA_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insdel_13;
                break;

        case CAMPSRV_INSLOCK:
                *pxdr_argument = xdr_INS_LOCK_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_inslock_13;
                break;

        case CAMPSRV_INSLINE:
                *pxdr_argument = xdr_INS_LINE_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insline_13;
                break;

        case CAMPSRV_INSLOAD:
                *pxdr_argument = xdr_INS_FILE_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insload_13;
                break;

        case CAMPSRV_INSSAVE:
                *pxdr_argument = xdr_INS_FILE_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_inssave_13;
                break;

        case CAMPSRV_INSIFSET:
                *pxdr_argument = xdr_INS_IF_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insifset_13;
                break;

        case CAMPSRV_INSIFREAD:
                *pxdr_argument = xdr_INS_READ_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insifread_13;
                break;

        case CAMPSRV_INSIFWRITE:
                *pxdr_argument = xdr_INS_WRITE_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insifwrite_13;
                break;

        case CAMPSRV_INSIFDUMP:
                *pxdr_argument = xdr_INS_DUMP_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insifdump_13;
                break;

        case CAMPSRV_INSIFUNDUMP:
                *pxdr_argument = xdr_INS_UNDUMP_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insifundump_13;
                break;

        case CAMPSRV_INSIFON:
                *pxdr_argument = xdr_DATA_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insifon_13;
                break;

        case CAMPSRV_INSIFOFF:
                *pxdr_argument = xdr_DATA_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_insifoff_13;
                break;

        case CAMPSRV_VARSET:
                *pxdr_argument = xdr_DATA_SET_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_varset_13;
                break;

        case CAMPSRV_VARPOLL:
                *pxdr_argument = xdr_DATA_POLL_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_varpoll_13;
                break;

        case CAMPSRV_VARALARM:
                *pxdr_argument = xdr_DATA_ALARM_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_varalarm_13;
                break;

        case CAMPSRV_VARLOG:
                *pxdr_argument = xdr_DATA_LOG_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_varlog_13;
                break;

        case CAMPSRV_VARZERO:
                *pxdr_argument = xdr_DATA_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_varzero_13;
                break;

        case CAMPSRV_VARDOSET:
                *pxdr_argument = xdr_DATA_SET_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_vardoset_13;
                break;

        case CAMPSRV_VARREAD:
                *pxdr_argument = xdr_DATA_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_varread_13;
                break;

        case CAMPSRV_VARLNKSET:
                *pxdr_argument = xdr_DATA_SET_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_varlnkset_13;
                break;

        case CAMPSRV_VARGET:
                *pxdr_argument = xdr_DATA_GET_req;
                *pxdr_result = xdr_CAMP_VAR_res;
                *plocal = (char *(*)()) campsrv_varget_13;
                break;

        case CAMPSRV_CMD:
                *pxdr_argument = xdr_CMD_req;
                *pxdr_result = xdr_RES;
                *plocal = (char *(*)()) campsrv_cmd_13;
                break;

        default:
                *pxdr_argument = xdr_void;
                *pxdr_result = xdr_void;
                *plocal = (char *(*)()) NULL;
                return;
        }
}


#ifdef MULTITHREADED
/*
 *  camp_srv_13
 *
 *  Multithreaded version:
 *  The routine that is registered with the RPC server as the entry point
 *  for RPC dispatches from my_svc_getreqset.
 *
 *  This routine starts an SVC thread that asynchronously processes the RPC
 *  request.
 */
void
camp_srv_13( struct svc_req *rqstp, SVCXPRT *transp )
{
    start_svc_thread( rqstp, transp );
}


/*
 *  do_service
 *
 *  Provides the actual processing of the RPC request within an SVC thread.
 *  Calls the appropriate routine corresponding to the particular request.
 *
 *  Reports that the processing is completed in the thread private data
 *  structure before returning.
 */
static void
do_service( struct svc_req *rqstp, SVCXPRT *transp )
{
    char *result;
    bool_t (*xdr_argument)(), (*xdr_result)();
    char *(*local)();
    THREAD_DATA* pThread_data;
    argument_t* pArgument;

    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &local );

    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );
    pArgument = &(pThread_data->argument);

#if CAMP_DEBUG
    if( camp_debug > 3 )
      {
	printf( "before processing request (%x)\n", rqstp->rq_proc );
	if( rqstp->rq_proc==CAMPSRV_CMD ) {
	  printf( "command :  %s\n", pThread_data->argument );
	  printf( "interp    = %x\n", pThread_data->interp ); 
	}
	printf( "served             = %d\n", pThread_data->served );
        printf( "transp             = %p\n", transp );
        printf( "transp->xp_sock    = %d\n", transp->xp_sock );
        printf( "transp->xp_port    = %d\n", transp->xp_port );
        printf( "transp->xp_ops     = %p\n", transp->xp_ops );
        printf( "transp->xp_addrlen = %d\n", transp->xp_addrlen );
	printf( "transp->xp_raddr.sin_family = %d\n", transp->xp_raddr.sin_family );
	printf( "transp->xp_raddr.sin_port   = %d\n", transp->xp_raddr.sin_port );
	printf( "transp->xp_raddr.sin_addr.s_addr = %x\n", transp->xp_raddr.sin_addr.s_addr );
        printf( "transp->xp_p1      = %p\n", transp->xp_p1 );
	printf( "SVC_STAT( transp ) = %d\n", SVC_STAT( transp ) );
      }
#endif /* CAMP_DEBUG */

    /*
     *  Process the request
     */
    result = (*local)( pArgument, rqstp );

    if( result != NULL ) 
    {
        ((RES*)result)->msg = camp_getMsg();
        if( _failure( ((RES*)result)->status ) )
        {
            camp_logMsg( ((RES*)result)->msg );
        }
    }

    camp_thread_mutex_lock( &(pThread_data->mutex_handle) );
    pThread_data->pResult = result;
    /*
     *  Notify main thread that this thread has been served
     *  Main thread will then make the RPC reply and clean up
     */
    pThread_data->served = TRUE;
    pthread_mutex_unlock( &(pThread_data->mutex_handle) );

/*  Moved to srv_handle_served_svc_threads
    end_svc_thread( -1 );
*/
}
#endif /* MULTITHREADED */


#ifndef MULTITHREADED
/*
 *  camp_srv_13
 *
 *  Single-threaded version:
 *  The routine that is registered with the RPC server as the entry point
 *  for RPC dispatches from svc_getreqset.
 *
 *  This routine processes the request synchronously and sends the result
 *  to the CAMP client program.
 */
void
camp_srv_13( struct svc_req *rqstp, SVCXPRT *transp )
{
    argument_t argument;
    char *result;
    bool_t (*xdr_argument)(), (*xdr_result)();
    char *(*local)();
    argument_t* pArgument;
    timeval_t tv;

    /*
     *  Set these 'global' variables so that 
     *  other routines may access it.
     *  These variables are global for non-multithreading
     *  and part of the thread_data structure for 
     *  multithreading.
     */
    gettimeval( &tv );
    set_tv_start( &tv );
    set_current_rqstp( rqstp );
    set_current_transp( transp );
    /* set_orig_transp( transp ); */
    camp_setMsg( "" );

    get_rq_proc( rqstp->rq_proc, &xdr_argument, &xdr_result, &local );

    pArgument = &argument;
    bzero( &argument, sizeof( argument_t ) );

    set_current_xdr_flag( CAMP_XDR_ALL );

    /* 
     *  Get args
     *  We are already in a global lock to ensure that
     *  this is the only thread calling this routine.
     */
    if (!svc_getargs(transp, xdr_argument, &argument)) {
        svcerr_decode(transp);
        return;
    }

    /*
     *  Process the request
     */
    result = (*local)( pArgument, rqstp );

    if( result != NULL ) 
    {
        ((RES*)result)->msg = camp_getMsg();
        if( _failure( ((RES*)result)->status ) )
        {
            camp_logMsg( ((RES*)result)->msg );
        }
    }

    /* 
     *  Send reply
     */
    if( ( result != NULL ) && !svc_sendreply( transp, xdr_result, result ) )
      {
	svcerr_systemerr(transp);
      }

    _free( result );

    /* 
     *  Free args
     */
    if( !svc_freeargs( transp, xdr_argument, pArgument ) ) 
      {
	camp_logMsg( "unable to free arguments" );
      }

    set_current_xdr_flag( CAMP_XDR_ALL );

    camp_setMsg( "" );
}
#endif /* !MULTITHREADED */


/*
 *  16-Dec-1999  TW  Had trouble using srv_check_timeout, so make
 *                   it a dummy call.
 *                   This was supposed to return a meaningful message
 *                   rather than having an RPC timeout.  It mostly
 *                   worked, but was still causing some problems
 *                   with conflicting/stale RPC transports between
 *                   threads.
 *  21-Dec-1999  TW  srv_check_timeout no longer used
 */
/*
void
srv_check_timeout( void )
{
  return;
}
*/


/*
 *  17-Dec-1999  TW  Made robust.  Main thread makes all RPC calls
 *                   now.  As a result the SVC_STAT check works properly.
 *
 *  OLD COMMENT:
 *    Test whether client
 *    has timed out.  This is not a good enough
 *    check, but sometimes works.  I think that
 *    it works unless another client comes in and
 *    starts using the same transp structure that
 *    the timed out client had been using.  Then,
 *    the server goes to reply to the wrong client
 *    request.
 *    How do I know if the client connection is lost?
 *    (i.e., if client timed out)
 *    Comparing the transport xp_sock seems to work every 
 *    time for VxWorks, but others might be OK too (like sin_port).
 *
 *    If the client is lost, then will not calling svc_sendreply
 *    and svc_freeargs cause some storage to not be freed
 *    properly.
 */
bool_t
test_lost_client( SVCXPRT* transp )
{
  if( SVC_STAT( transp ) == XPRT_DIED ) return( TRUE );

/*  This was old way
  if( ( bcmp( transp, orig_transp, sizeof( SVCXPRT) ) != 0 ) )
    {
      return( TRUE );
    }
  else
    {
      if( SVC_STAT( transp ) != XPRT_IDLE )
	{
	  return( TRUE );
	}
    }
*/

  return( FALSE );
}


