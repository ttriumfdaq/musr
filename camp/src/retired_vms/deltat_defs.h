/*
**			DELTAT_DEFS.H
**
** Include file generated from DELTAT_DEFS.OBJ
**
**              22-JUL-1994 16:56:31.17
*/

#define	    CAMIF__BIRA                   0x5
#define	    CAMIF__CCP                    0x9
#define	    CAMIF__CES                    0x4
#define	    CAMIF__GEC0                   0x3
#define	    CAMIF__GEC1                   0x6
#define	    CAMIF__GEC2                   0x7
#define	    CAMIF__GEC3                   0x8
#define	    CAMIF__ILLPAR                 0xC
#define	    CAMIF__JCC                    0x1
#define	    CAMIF__KCBD                   0xB
#define	    CAMIF__KVCC                   0xD
#define	    CAMIF__M_XQ                   0x3
#define	    CAMIF__NOX_NOQ                0x3
#define	    CAMIF__NOX_Q                  0x2
#define	    CAMIF__NO_CAMIF               0x4
#define	    CAMIF__OS9                    0xA
#define	    CAMIF__RPTFAIL                0x14
#define	    CAMIF__SEMTMO                 0x18
#define	    CAMIF__TMOUT                  0x8
#define	    CAMIF__VAN                    0xC
#define	    CAMIF__X_NOQ                  0x1
#define	    CAMIF__X_Q                    0x0
#define	    HMSRV_CLOSE                   0x101
#define	    HMSRV_CNCT                    0x1
#define	    HMSRV_CONFIG                  0x2
#define	    HMSRV_DBG                     0x6
#define	    HMSRV_DECONFIG                0x3
#define	    HMSRV_EXIT                    0x4
#define	    HMSRV_INH                     0x102
#define	    HMSRV_IOREG                   0x103
#define	    HMSRV_READ                    0x104
#define	    HMSRV_SET_TDC                 0x105
#define	    HMSRV_SHOW                    0x106
#define	    HMSRV_STATUS                  0x5
#define	    HMSRV_WRITE                   0x107
#define	    HMSRV_ZERO                    0x108
#define	    INH_CLR                       0x2
#define	    INH_SET                       0x1
#define	    INH_TST                       0x3
#define	    IO_CLR                        0x2
#define	    IO_PULSE                      0x3
#define	    IO_SET                        0x1
#define	    MAP__CAMAC_FIELD_0            0x0
#define	    MAP__CAMAC_FIELD_1            0x1
#define	    MAP__CAMAC_FIELD_2            0x2
#define	    MAP__CAMAC_FIELD_CSR          0x3
#define	    MAP__CAMAC_SEMAPHORE          0x4
#define	    MAP__DELTAT_CB                0x5
#define	    MAP__DELTAT_SCALERS           0x6
#define	    SCAL__MAX                     0x6
#define	    SCAL__OFF_NSCAL               0x0
#define	    SCAL__OFF_SCALERS             0x4
#define	    SEM_BIT                       0x0
#define	    SEM_CNT0                      0x1C
#define	    SEM_CNT1                      0x20
#define	    SEM_CNT2                      0x24
#define	    SEM_CNT3                      0x28
#define	    SEM_IDNT                      0xC
#define	    SEM_PID                       0x4
#define	    SEM_SIZE                      0x40
#define	    SEM_SUB_PID                   0x2C
#define	    SEM_WFLG                      0x8
#define	    SEM__CAMAC_3344               0xC0
#define	    SEM__CAMAC_CSR                0x0
#define	    SEM__CAMAC_GPIB               0x80
#define	    SEM__CAMAC_IVG                0x40
#define	    SEM__DELTAT_CB                0x0
#define	    SEM__HMSR_REQ                 0x40
