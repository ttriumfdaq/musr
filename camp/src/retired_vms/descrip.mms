#
# Makefile for CAMP - VAX/VMS version (easily modified for Alpha/VMS)
#
# Use the macros CAMIF, CAMRPC, CAMSCSI to distinguish the 
# CAMAC library to link with.  The default is CAMIF.
#
# Note: I found by trial and error that the MultiNet RPC library
#       works with D-Floating doubles (the default) not
#       G-Floating
#
# Note: the Tandem library and the MultiNet RPC library
#       need to link with the VAX C RTL.  Use vaxc2decc, not
#       vaxcrtl, because the DEC C RTL is DECthread compatible
#       and the RPC library is used outside of global thread
#       locking.  (The Tandem library should all be safely 
#       inside global locks, I hope).
#
#CAMRPC = 1
#CAMSCSI = 1

SRC_DIR = 	dasdevpc_musr:[camp.src]
DRV_DIR =	dasdevpc_musr:[camp.drv]
DEST_DIR =	dasdevpc_musr:[camp.vax-vms]
TCL_DIR =	dsk1:[whidden.tcl.tcl73]
TK_DIR =	dsk1:[whidden.tcl.tk36]
TW_DIR = 	dasdevpc_musr:[libc_tw.src]
CAMAC_DIR = 	dasdevpc_musr:[vxworks.camac]
INSTALL_ROOT =	camp
KIT_VERSION =	camp013
KIT_DIR = 	dsk1:[whidden.camp.kit]

TCL_LIB = 	$(TCL_DIR)libtcl/lib
TK_LIB = 	$(TK_DIR)libtk/lib
TW_LIB = 	dasdevpc_musr:[libc_tw.vax-vms-decc]libc_tw/lib
RPC_LIB =	$(SRC_DIR)rpc_multinet/opt
#RPC_LIB =	$(SRC_DIR)rpc_ucx/opt
DECTHREAD_LIB = $(SRC_DIR)decthreads/opt
C_RTL = 	$(SRC_DIR)vaxc2decc/opt
CURSES_LIB =	

CDEFS = 	DONT_DECLARE_MALLOC,MULTINET,MULTITHREADED
CFLAGS =	/incl=($(TW_DIR),$(CAMAC_DIR),$(TCL_DIR),$(TK_DIR))/define=($(CDEFS))
#CFLAGS =	/noopt/debug/incl=($(TW_DIR),$(TCL_DIR))/define=($(CDEFS)) #debug
LINKFLAGS =	$(LINKFLAGS)				#production
#LINKFLAGS =	$(LINKFLAGS)/debug			#debug 
#LINKFLAGS =	$(LINKFLAGS)/debug=SYS$LIBRARY:PCA$OBJ.OBJ  #PCA 
CC =		cc/decc/standard=vaxc
LINK =		$(LINK)

CLNT_LIBS =	$(DEST_DIR)camp_clnt/lib,$(TW_LIB),$(RPC_LIB),$(C_RTL)

CUI_LIBS =	$(DEST_DIR)camp_clnt/lib,$(TW_LIB),$(RPC_LIB),$(C_RTL)

#
#  CAMAC libraries
#
CAMRPC_LIB =    ed:[lib]deltat_no_camif/lib,ed:[lib]tandem_no_eso/lib,\
		$(SRC_DIR)errcom/opt,\
		dasdevpc_musr:[camac.rpc]libcamac_rpc/lib
CAMSCSI_LIB =   ed:[lib]deltat_no_camif/lib,ed:[lib]tandem_no_eso/lib,\
		$(SRC_DIR)errcom/opt,\
		dasdevpc_musr:[camac.scsi]libcamac_scsi/lib
CAMIF_LIB =     ed:[lib]deltat/lib,tandem_vms:[vmslib]tandem/lib

SRV_LIBS =      $(TW_LIB),$(TCL_LIB),$(RPC_LIB),$(DECTHREAD_LIB),$(C_RTL)

#		camp_lib:camif.obj,\
#		camp_lib:cb_log_utility.obj,\
#		camp_lib:gpib_package.obj,\
#		camp_lib:gpib_package_1.obj,\
#		camp_lib:deltat_defs.obj,\

######################################################################
#
#  DJA: remove servers from "all"
#	camp_srv.olb, camp_srv_camif.exe, camp_srv_camrpc.exe, camp_srv_camscsi.exe, \

all :	\
	camp_clnt.olb, \
	camp_cui.exe, camp_cmd.exe, \ 
        wish_camp.exe, tclsh_camp.exe
    @ continue

#
#  Common headers
#
GEN_HEADS =	$(SRC_DIR)camp.h

SRV_HEADS =	$(SRC_DIR)camp_srv.h

CLNT_HEADS = 	$(SRC_DIR)camp_clnt.h

COMMON_OBJS_1 =	xdr_timeval.obj,\
		camp_sys_utils.obj,\
		camp_ins_utils.obj,\
		camp_if_utils.obj,\
		camp_path_utils.obj,\
		camp_token.obj

COMMON_OBJS = 	$(COMMON_OBJS_1)

#camp_types.h, camp_types_xdr.c : camp_types.x
#    rpcgen -c -o camp_types_xdr.c camp_types.x
#    rpcgen -h -o camp_types.h camp_types.x
#    ifdef camp_types.h

$(COMMON_OBJS_1) : $(GEN_HEADS)

xdr_timeval.obj : $(SRC_DIR)xdr_timeval.c
camp_sys_utils.obj : $(SRC_DIR)camp_sys_utils.c
camp_ins_utils.obj : $(SRC_DIR)camp_ins_utils.c
camp_if_utils.obj : $(SRC_DIR)camp_if_utils.c
camp_path_utils.obj : $(SRC_DIR)camp_path_utils.c
camp_token.obj : $(SRC_DIR)camp_token.c

#
#  camp_clnt.obj
#
CLNT_OBJS =	camp_api_proc.obj,\
		camp_srv_clnt_mod.obj,\
		camp_types_xdr_mod_clnt.obj,\
		camp_var_utils_clnt.obj,\
		camp_msg_utils_clnt.obj

$(CLNT_OBJS) : $(GEN_HEADS),$(SRV_HEADS),$(CLNT_HEADS)

camp_srv_clnt_mod.obj : $(SRC_DIR)camp_srv_clnt_mod.c
camp_types_xdr_mod_clnt.obj : $(SRC_DIR)camp_types_xdr_mod.c
	$(CC)$(CFLAGS)/def=($(CDEFS),RPC_CLIENT)/obj=$* $(SRC_DIR)camp_types_xdr_mod.c
camp_var_utils_clnt.obj : $(SRC_DIR)camp_var_utils.c
	$(CC)$(CFLAGS)/def=($(CDEFS),RPC_CLIENT)/obj=$* $(SRC_DIR)camp_var_utils.c
camp_msg_utils_clnt.obj : $(SRC_DIR)camp_msg_utils.c
	$(CC)$(CFLAGS)/def=($(CDEFS),RPC_CLIENT)/obj=$* $(SRC_DIR)camp_msg_utils.c
camp_api_proc.obj : $(SRC_DIR)camp_api_proc.c

camp_clnt.olb : $(CLNT_OBJS),$(COMMON_OBJS)
    $(LIBR)/create $@ $+

#
#  camp_srv.obj
#
SRV_LIB_OBJS =	camp_types_xdr_mod_srv.obj,\
		camp_var_utils_srv.obj,\
		camp_msg_utils_srv.obj,\
		camp_msg_priv.obj,\
		camp_srv_proc.obj,\
		camp_srv_utils.obj,\
		camp_write.obj,\
		camp_def_write.obj,\
		camp_ini_write.obj,\
		camp_cfg_write.obj,\
		camp_sys_priv.obj,\
		camp_var_priv.obj,\
		camp_if_priv.obj,\
		camp_ins_priv.obj,\
		camp_req.obj,\
		camp_log_priv.obj,\
		camp_alarm_priv.obj,\
		camp_tcl.obj,\
		camp_if_rs232_vms_term.obj,\
		camp_if_gpib_deltat.obj,\
		camp_if_camac_deltat.obj,\
		camp_ins_tcl.obj

$(SRV_LIB_OBJS) : $(GEN_HEADS)

camp_types_xdr_mod_srv.obj : $(SRC_DIR)camp_types_xdr_mod.c
	$(CC)$(CFLAGS)/def=($(CDEFS),RPC_SERVER)/obj=$* $(SRC_DIR)camp_types_xdr_mod.c
camp_var_utils_srv.obj : $(SRC_DIR)camp_var_utils.c
	$(CC)$(CFLAGS)/def=($(CDEFS),RPC_SERVER)/obj=$* $(SRC_DIR)camp_var_utils.c
camp_msg_utils_srv.obj : $(SRC_DIR)camp_msg_utils.c
	$(CC)$(CFLAGS)/def=($(CDEFS),RPC_SERVER)/obj=$* $(SRC_DIR)camp_msg_utils.c
camp_msg_priv.obj : $(SRC_DIR)camp_msg_priv.c
camp_srv_proc.obj : $(SRC_DIR)camp_srv_proc.c
camp_srv_utils.obj : $(SRC_DIR)camp_srv_utils.c
camp_write.obj : $(SRC_DIR)camp_write.c
camp_def_write.obj : $(SRC_DIR)camp_def_write.c
camp_ini_write.obj : $(SRC_DIR)camp_ini_write.c
camp_cfg_write.obj : $(SRC_DIR)camp_cfg_write.c
camp_sys_priv.obj : $(SRC_DIR)camp_sys_priv.c
camp_var_priv.obj : $(SRC_DIR)camp_var_priv.c
camp_if_priv.obj : $(SRC_DIR)camp_if_priv.c
camp_ins_priv.obj : $(SRC_DIR)camp_ins_priv.c
camp_req.obj : $(SRC_DIR)camp_req.c
camp_log_priv.obj : $(SRC_DIR)camp_log_priv.c
camp_alarm_priv.obj : $(SRC_DIR)camp_alarm_priv.c
camp_tcl.obj : $(SRC_DIR)camp_tcl.c
camp_if_rs232_vms_term.obj : $(SRC_DIR)camp_if_rs232_vms_term.c
camp_if_gpib_deltat.obj : $(SRC_DIR)camp_if_gpib_deltat.c
camp_if_camac_deltat.obj : $(SRC_DIR)camp_if_camac_deltat.c
camp_ins_tcl.obj : $(SRC_DIR)camp_ins_tcl.c

camp_srv.olb : $(COMMON_OBJS), $(SRV_LIB_OBJS)
    $(LIBR)/create $@ $+

#
#  camp_srv.exe  -  CAMP Server
#
SRV_OBJS =	camp_srv_main.obj,\
		camp_srv_svc_mod.obj

#camp_srv.h : camp_srv.x
#    rpcgen -h -o camp_srv.h camp_srv.x
#    rpcgen -m -o camp_srv_svc.c camp_srv.x
#    rpcgen -l -o camp_srv_clnt.c camp_srv.x
#    ifdef camp_srv.h

$(SRV_OBJS) : $(GEN_HEADS)
 
camp_srv_main.obj : $(SRC_DIR)camp_srv_main.c
camp_srv_svc_mod.obj  : $(SRC_DIR)camp_srv_svc_mod.c

camp_srv_camif.exe : $(SRV_OBJS) $(DEST_DIR)camp_srv.olb
    $(LINK)$(LINKFLAGS)/exe=$@ $(SRV_OBJS), $(DEST_DIR)camp_srv/lib,\
	$(CAMIF_LIB), $(SRV_LIBS)

camp_srv_camrpc.exe : $(SRV_OBJS) $(DEST_DIR)camp_srv.olb
    $(LINK)$(LINKFLAGS)/exe=$@ $(SRV_OBJS), $(DEST_DIR)camp_srv/lib,\
	$(CAMRPC_LIB), $(SRV_LIBS)

camp_srv_camscsi.exe : $(SRV_OBJS) $(DEST_DIR)camp_srv.olb
    $(LINK)$(LINKFLAGS)/exe=$@ $(SRV_OBJS), $(DEST_DIR)camp_srv/lib,\
	$(CAMSCSI_LIB), $(SRV_LIBS)

#
#  camp_cui.exe  -  CAMP Character-cell User Interface
#
CUI_HEADS =	$(SRC_DIR)camp_cui.h

CUI_OBJS =	camp_cui_main.obj,\ 
		camp_cui_data.obj,\ 
		camp_cui_input.obj,\
		camp_cui_input_macros.obj,\
		camp_cui_menucb.obj,\
		camp_cui_status.obj,\
		camp_cui_if.obj,\
		camp_cui_keyboard.obj,\
		camp_cui_alert.obj,\
		camp_cui_help.obj,\
		camp_cui_varinfo.obj,\
		camp_cui_keywin.obj,\
		camp_cui_msgwin.obj

$(CUI_OBJS) : $(CUI_HEADS),$(GEN_HEADS),$(CLNT_HEADS)
# camp_cui_main.obj : $(CUI_HEADS),$(GEN_HEADS),$(CLNT_HEADS)

camp_cui_main.obj : $(SRC_DIR)camp_cui_main.c
camp_cui_data.obj : $(SRC_DIR)camp_cui_data.c
camp_cui_input.obj : $(SRC_DIR)camp_cui_input.c
camp_cui_input_macros.obj : $(SRC_DIR)camp_cui_input_macros.c
camp_cui_menucb.obj : $(SRC_DIR)camp_cui_menucb.c
camp_cui_status.obj : $(SRC_DIR)camp_cui_status.c
camp_cui_if.obj : $(SRC_DIR)camp_cui_if.c
camp_cui_keyboard.obj : $(SRC_DIR)camp_cui_keyboard.c
camp_cui_alert.obj : $(SRC_DIR)camp_cui_alert.c
camp_cui_help.obj : $(SRC_DIR)camp_cui_help.c
camp_cui_varinfo.obj : $(SRC_DIR)camp_cui_varinfo.c
camp_cui_keywin.obj : $(SRC_DIR)camp_cui_keywin.c
camp_cui_msgwin.obj : $(SRC_DIR)camp_cui_msgwin.c

camp_cui.exe : $(CUI_OBJS), $(DEST_DIR)camp_clnt.olb 
    $(LINK)$(LINKFLAGS)/exe=$@ $(CUI_OBJS), $(CUI_LIBS)

#
#  camp_reader.exe  -  Device driver file reader
#
#READER_OBJS =	camp_reader.obj
#
#camp_reader.obj : $(GEN_HEADS)
#
#camp_reader.exe : camp_srv.olb, $(READER_OBJS)
#    $(LINK)$(LINKFLAGS)/exe=$@ $(READER_OBJS), $(SRV_LIBS)
#

#
#  camp_cmd.exe  -  CAMP Command-line Interface
#
CMD_OBJS =	camp_cmd.obj

$(CMD_OBJS) : $(GEN_HEADS), $(CLNT_HEADS)

camp_cmd.obj : $(SRC_DIR)camp_cmd.c

camp_cmd.exe : camp_clnt.olb, $(CMD_OBJS)
    $(LINK)$(LINKFLAGS)/exe=$@ $(CMD_OBJS), $(CLNT_LIBS)

#
#  wish_camp  -  wish with camp client commands
#
WISH_OBJS = camp_tcl_client.obj, tkappinit_camp.obj

camp_tcl_client.obj : $(SRC_DIR)camp_tcl_client.c
tkappinit_camp.obj : $(SRC_DIR)tkappinit_camp.c

wish_camp.exe : $(DEST_DIR)camp_clnt.olb, $(WISH_OBJS)
    define/user vaxcrtl sys$library:vaxc$empty.exe
    $(LINK)$(LINKFLAGS)/notrace $(WISH_OBJS), \
      $(TK_LIB), $(TCL_LIB), $(SRC_DIR)tk-vax/opt, $(CLNT_LIBS)

#
#  tclsh_camp  -  wish with camp client commands
#
TCLSH_OBJS = camp_tcl_client.obj, tclappinit_camp.obj

camp_tcl_client.obj : $(SRC_DIR)camp_tcl_client.c
tclappinit_camp.obj : $(SRC_DIR)tclappinit_camp.c

tclsh_camp.exe : $(DEST_DIR)camp_clnt.olb, $(TCLSH_OBJS)
    $(LINK)$(LINKFLAGS)/notrace $(TCLSH_OBJS), $(TCL_LIB), $(CLNT_LIBS)

#
#  Text libraries
#
#camp_clnt.tlb : $(ALL_CLNT_HEADS)
#    $(LIBR)/create/text $@ $+
#
#camp_srv.tlb : $(ALL_SRV_HEADS)
#    $(LIBR)/create/text $@ $+
#

#
#  install
#
INSTALL_COM =	$(SRC_DIR)camp_startup.com,\
		$(SRC_DIR)camp_login.com,\
		$(SRC_DIR)camp_srv.com,\
                $(SRC_DIR)update_camp_files.com
INSTALL_DRV = 	$(DRV_DIR)*.tcl
INSTALL_BIN = 	camp_srv*.exe,camp_cui.exe,camp_cmd.exe,\
                tclsh_camp.exe, wish_camp.exe
INSTALL_DAT = 	$(SRC_DIR)camp.ini
INSTALL_LIB = 	camp.hlb,camp_clnt.olb

install : 
    delete $(INSTALL_ROOT):[000000]*.com;*
    copy $(INSTALL_COM) $(INSTALL_ROOT):[000000]
    delete $(INSTALL_ROOT):[drv]*.*;*
    copy $(INSTALL_DRV) $(INSTALL_ROOT):[drv]
    delete $(INSTALL_ROOT):[vax-vms]*.*;*
    copy $(INSTALL_BIN) $(INSTALL_ROOT):[vax-vms]
    delete $(INSTALL_ROOT):[dat]camp.ini;*
    copy $(INSTALL_DAT) $(INSTALL_ROOT):[dat]
    delete $(INSTALL_ROOT):[lib]*.*;*
    copy $(INSTALL_LIB) $(INSTALL_ROOT):[lib]

KIT_FILES = 	\
		camp_srv.exe,camp_cui.exe,camp_cmd.exe,\
                tclsh_camp.exe, wish_camp.exe,\
    		$(SRC_DIR)kitinstal.com,\
		$(SRC_DIR)camp_startup.com,\
		$(SRC_DIR)camp_srv.com,\
		$(SRC_DIR)camp_login.com,\
		$(SRC_DIR)camp*.tcl

#		camp.hlb,\

kit : $(KIT_DIR)$(KIT_VERSION).a
    @ continue

$(KIT_DIR)$(KIT_VERSION).a :
    @sys$update:spkitbld $(KIT_VERSION) $(KIT_DIR) $(KIT_FILES)

#
#  Cleanup
#
tidy :
	rm camp*.obj;*,camp*.lis;*

clean :
	rm camp*.obj;*,camp*.lis;*,camp*.exe;*,camp*.olb;*,*sh_camp.exe;*


