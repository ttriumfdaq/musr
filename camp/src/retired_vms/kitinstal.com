!-------------------------------------------------------------------------------
!
!  CAMP kitinstal.com
!
!-------------------------------------------------------------------------------
$ on control_y then vmi$callback control_y
$ on warning then exit $status
$ say := write sys$output
$ if p1 .eqs. "VMI$_INSTALL" then goto camp_install
$ if p1 .eqs. "VMI$_POSTINSTALL" then goto camp_postinstall
$ if p1 .eqs. "VMI$_IVP" then goto camp_ivp
$ exit VMI$_UNSUPPORTED
$camp_install:
$ camp_tmp = f$type( VMI$ARCHITECTURE )
$ camp_arch = 0
$ if camp_tmp .eqs. "" then camp_arch = 0
$ if camp_tmp .eqs. "INTEGER" then camp_arch = 0
$ if camp_tmp .nes. "STRING" then goto done_arch
$ if VMI$ARCHITECTURE .eqs. "AXP" then camp_arch = 1
$ if VMI$ARCHITECTURE .eqs. "VAX" then camp_arch = 0
$ !
$done_arch:
$ !
$ !  At this point camp_arch = 0 for VAX or = 1 for AXP
$ !
$ if camp_arch .eq. 1 then exit VMI$_UNSUPPORTED
$ say ""
$ say "$!  Courtesy of:  TRIUMF Data Acquisition Software"
$ say "$!       Author:  Ted Whidden"
$ say ""
$check_vms_version:
$ !
$ ! Check VMS version
$ !
$ min_vms_version = "6.1"
$ vmi$callback check_vms_version vms_ok 'min_vms_version'
$ if vms_ok then goto vms_version_ok
$ vmi$callback message e version -
  "This kit must be installed on an existing OpenVMS ''min_vms_version' system"
$ exit VMI$_FAILURE
$vms_version_ok:
$ !
$ ! Check disk space
$ !
$ vmi$callback check_net_utilization camp_space 4000
$ if .not. camp_space then exit VMI$_FAILURE
$ !
$ vmi$callback set purge ask
$ vmi$callback set ivp ask
$ !
$ vmi$callback create_directory user sys$sysdevice:[camp]
$ vmi$callback create_directory user sys$sysdevice:[camp.lib]
$ vmi$callback create_directory user sys$sysdevice:[camp.dat]
$ vmi$callback create_directory user sys$sysdevice:[camp.log]
$ !
$ vmi$callback provide_file camp_login camp_login.com sys$sysdevice:[camp]
$ vmi$callback provide_file camp_srv_com camp_srv.com sys$sysdevice:[camp]
$ vmi$callback provide_image camp_srv camp_srv.exe sys$sysdevice:[camp]
$ vmi$callback provide_image camp_cui camp_cui.exe sys$sysdevice:[camp]
$ vmi$callback provide_image camp_cmd camp_cmd.exe sys$sysdevice:[camp]
$ vmi$callback provide_image tclsh_camp tclsh_camp.exe sys$sysdevice:[camp]
$ vmi$callback provide_image wish_camp tclsh_camp.exe sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_bird4421 camp_ins_bird4421.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_cryo_ps120 camp_ins_cryo_ps120.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_dummy_mag camp_ins_dummy_mag.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_group3_dtm141 camp_ins_group3_dtm141.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_joerger_da16 camp_ins_joerger_da16.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_lake330 camp_ins_lake330.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_lake622 camp_ins_lake622.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_hp59303a camp_ins_hp59303a.tcl sys$sysdevice:[camp]
$ vmi$callback provide_file camp_ins_metrolab_pt3020 camp_ins_metrolab_pt3020.tcl sys$sysdevice:[camp]
$ !
$ vmi$callback provide_file camp_ivp camp_ivp.com vmi$root:[systest] c
$ vmi$callback provide_file camp_startup camp_startup.com vmi$root:[sys$startup]
$ !
$ vmi$callback set startup camp_startup.com
$ !
$ exit VMI$_SUCCESS
$ !
$camp_postinstall:
$ !
$ exit VMI$_SUCCESS
$ !
$camp_ivp:
$ !
$ @vmi$kwd:camp_ivp.com
$ !
$ exit $status
