$ delete/noconf dsk1:[camp]*.com;* /exc=update_camp_files.com
$ copy  dasdev::dsk1:[camp]*.com dsk1:[camp]
$ purge/nolog   dsk1:[camp]update_camp_files.com
$ delete/noconf dsk1:[camp.dat]*.dat;*,*.spl;*,*.ini;*
$ copy  dasdev::dsk1:[camp.dat]*.dat,*.spl,*.ini dsk1:[camp.dat]
$ delete/noconf dsk1:[camp.drv]*.tcl;*
$ copy  dasdev::dsk1:[camp.drv]*.tcl dsk1:[camp.drv]
$ delete/noconf dsk1:[camp.lib]*.*;*
$ copy  dasdev::dsk1:[camp.lib]*.* dsk1:[camp.lib]
$ delete/noconf dsk1:[camp.vax-vms]*.*;*
$ copy  dasdev::dsk1:[camp.vax-vms]*.exe dsk1:[camp.vax-vms]
$ exit
$ ! 
$ ! update_camp_files.com
$ ! 
$ !   Update CAMP system files from the development node
$ ! 
