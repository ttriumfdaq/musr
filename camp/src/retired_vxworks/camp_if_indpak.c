/*
 *  $Id: camp_if_indpak.c,v 1.1 2015/03/16 22:07:59 suz Exp $
 *
 *  $Revision: 1.1 $
 *
 *  Purpose:    Provides an Industry Pack (INDPAK) interface type.
 *              Interface routines implementing a generic library.
 *
 *              Note that indpak I/O implementation in CAMP is by way of
 *              ASCII command strings which are interpreted in a Tcl
 *              interpreter.  The routines if_indpak_write and if_indpak_read
 *              supplied here simply pass valid ASCII command strings to
 *              the interpreter.  Consequently, there doesn't need to be
 *              anything specific to a particular library in this file.
 *
 *              A CAMP INDPAK interface definition must provide the following
 *              routines:
 *                int if_indpak_init ( void );
 *                int if_indpak_online ( CAMP_IF *pIF );
 *                int if_indpak_offline ( CAMP_IF *pIF );
 *                int if_indpak_read( REQ* pReq );
 *                int if_indpak_write( REQ* pReq );
 *
 *  Called by:  camp_if_priv.c
 * 
 *  Preconditions:
 *              A particular interface type's procs are set to these
 *              routines in camp_tcl.c (camp_tcl_sys) using the Tcl
 *              command sysAddIfType which is normally called from the
 *              server initialization file camp.ini .
 *
 *              read and write routines must be called with the global
 *              lock on in multithreading mode.  It is up to the routines
 *              to decide if is is safe to unlock the global mutex during
 *              periods of long waiting (i.e., during communications).
 *
 *              In the implementation of CAMP INDPAK interfaces one must be
 *              careful in considering the removal of the global lock during
 *              interpretation of the Tcl command.  The Tcl interpreter
 *              could be the main interpreter or an intrument interpreter
 *              and one must be sure about the safeness of the underlying
 *              INDPAK library.  For these reasons, INDPAK commands are called
 *              with the lock on.  This has not been a problem because
 *              there are normally few INDPAK commands in CAMP drivers, and
 *              they execute very quickly in comparison with other CAMP
 *              device type (RS232, GPIB).
 *
 *  Revision history:
 *
 *
 *  $Log: camp_if_indpak.c,v $
 *  Revision 1.1  2015/03/16 22:07:59  suz
 *  these files modified and renamed by Ted  - see AAA_README
 *
 *  Revision 1.2  2004/01/28 03:25:50  asnd
 *  Fix comments.
 *
 *  Revision 1.1  2000/12/22 22:52:01  David.Morris
 *  Copied from camp_if_camac_gen.c. Initial version has no action for
 *  read and write methods. Always return success. These need to be fleshed out.
 *
 *
 */

#include <stdio.h>
#include "camp_srv.h"


int
if_indpak_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    int indpak_if_type;

    pIF_t = camp_ifGetpIF_t( "indpak" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /*
     *  Interperate interface configuration info here
     *  (none for generic).
     */

    return( CAMP_SUCCESS );
}


int
if_indpak_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}


int
if_indpak_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}


int
if_indpak_read( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

/*
    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
*/
    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
/*   if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed INDPAK read: %s", interp->result );
      return( CAMP_FAILURE );
    }
*/
    return( CAMP_SUCCESS );
}


int
if_indpak_write( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

/*
    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
*/
    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  Must lock this interpreter while using it.
     */
/*
    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
      camp_appendMsg( "Failed INDPAK write: %s", interp->result );
      return( CAMP_FAILURE );
    }
*/
    return( CAMP_SUCCESS );
}
