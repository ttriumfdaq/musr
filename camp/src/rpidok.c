/*
 *  Name:       rpidok
 *
 *  Purpose:    Try to send a command to a remote node to test whether
 *              a process ID exists.  This is used in CAMP to control
 *              instrument locking by a remote user.
 *
 *              This routine has been implemented to run on VxWorks only,
 *              and only for remote nodes running VMS or a BSD Unix
 *              operating system.
 *
 *  Called by:  checkPidOk (camp_srv_utils.c)
 * 
 *  Inputs:     hostname (string), process id (integer), and a string
 *              identifying the remote operating system (bsd, vms)
 *
 *  Outputs:    Status of whether the pid was found on the remote node.
 *
 *  Revision history:
 *              As of now, return TRUE for all but vxworks.
 */

#ifdef VXWORKS
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <vxWorks.h>
#include "remLib.h"
#include "ioLib.h"
#endif

int
rpidok( char* host, int pid, const char* ros )
{

#ifdef VXWORKS
    char user[MAX_IDENTITY_LEN];
    char passwd[MAX_IDENTITY_LEN];
    int fd;
    char buf[256];
    int nbytes;
    int found;
    char cmd[64];
    char pidstr[16];
    char os[16];

    if( ros == NULL )
    {
        strcpy( os, "bsd" );
    }
    else
    {
        strcpy( os, ros );
    }

    remCurIdGet( user, passwd );

    if( strcmp( os, "bsd" ) == 0 )
    {
        sprintf( cmd, "ps %d", pid );
        if( pid <= 9999 )
        {
            sprintf( pidstr, " %d ", pid );
        }
        else
        {
 	    sprintf( pidstr, "%d ", pid );
        }
    }
    else if( strcmp( os, "vms" ) == 0 )
    {
        sprintf( cmd, "write sys$output f$getjpi(\"%X\",\"PID\")", pid );
        sprintf( pidstr, "%08X", pid );
    }
    else
    {
        /*
	 *  Operating system ID unknown
	 */
        return( FALSE );
    }

    fd = rcmd( host, 514, user, user, cmd, NULL );
    if( fd == ERROR )
    {
        return( FALSE );
    }

    found = FALSE;
    buf[0] = '\0';
    while( ( nbytes = read( fd, buf, 256 ) ) != ERROR )
    {
	// this was a bad check - didn't properly parse the PID - relied on assumed space delimiters
	if( strstr( buf, pidstr ) != NULL ) 
	{
	    found = TRUE;
            break;
	}
        buf[0] = '\0';
        if( nbytes == 0 ) break;
    }

    close( fd );

    return( found );
#else
    return( 1 );
#endif
}

