/*
 *  Name:       tkappinit_camp.c
 *
 *  Purpose:    Provides a camp version of the Tcl_AppInit procedure with
 *              Tk.
 *
 *              Calls the routine campTclClientInit which adds the commands
 *              "camp_cmd" and "date".  "camp_cmd" sends CAMP Tcl commands
 *              to the CAMP server.
 *
 *  Called by:  Tk application initialization routine (e.g., wish)
 * 
 *  Revision history:
 *    14-Dec-1999  TW  Fix tcl_rcFileName definition for Tk compatibility
 *    15-Dec-2000  TW  Update for Tk 8.0
 *
 *  $Log: tkappinit_camp.c,v $
 *  Revision 1.6  2015/04/21 03:47:14  asnd
 *  Major Camp revision by Ted using mtrpc on Linux and string-length passing in API, and plenty more. Revisions up to 2014/06/12 13:32
 *
 *  Revision 1.3  2001/04/20 02:41:30  asnd
 *  Fixes for VMS build
 *
 */

#include "tk.h"
#ifdef NOT_NEEDED
#include "tkPhoto.h"
#endif
#include "camp_tcl_client.h"

#ifdef VMS
/*
 * The following variable is a special hack that allows applications
 * to be linked using the procedure "main" from the Tk library.  The
 * variable generates a reference to "main", which causes main to
 * be brought in from the library (and all of Tk and Tcl with it).
 */

extern int main();
int *tclDummyMainPtr = (int *) main;

#else /* !VMS */

/*
 *  15-Dec-2000  TW  Update for Tk 8.0
 */
/*
 *----------------------------------------------------------------------
 *
 * main --
 *
 *	This is the main program for the application.
 *
 * Results:
 *	None: Tk_Main never returns here, so this procedure never
 *	returns either.
 *
 * Side effects:
 *	Whatever the application does.
 *
 *----------------------------------------------------------------------
 */

int
main(argc, argv)
    int argc;			/* Number of command-line arguments. */
    char **argv;		/* Values of command-line arguments. */
{
    Tk_Main(argc, argv, Tcl_AppInit);
    return 0;			/* Needed only to prevent compiler warning. */
}

#endif /* VMS */


/*
 *----------------------------------------------------------------------
 *
 * Tcl_AppInit --
 *
 *	This procedure performs application-specific initialization.
 *	Most applications, especially those that incorporate additional
 *	packages, will have their own version of this procedure.
 *
 * Results:
 *	Returns a standard Tcl completion code, and leaves an error
 *	message in interp->result if an error occurs.
 *
 * Side effects:
 *	Depends on the startup script.
 *
 *----------------------------------------------------------------------
 */

int
Tcl_AppInit(interp)
    Tcl_Interp *interp;		/* Interpreter for application. */
{

/*
 *  15-Dec-2000  TW  This not done here for Tk 8.0
 *
    Tk_Window main;

    main = Tk_MainWindow(interp);
 */
  
    /*
     * Call the init procedures for included packages.  Each call should
     * look like this:
     *
     * if (Mod_Init(interp) == TCL_ERROR) {
     *     return TCL_ERROR;
     * }
     *
     * where "Mod" is the name of the module.
     */
/*
    if (Photo_Init(interp) == TCL_ERROR) {
     	return TCL_ERROR;
    }
*/
    if (Tcl_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
    }
    if (Tk_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
    }

    if( campTclClientInit( interp ) == TCL_ERROR ) {
        return TCL_ERROR;
    }

    /*
     * Call Tcl_CreateCommand for application-specific commands, if
     * they weren't already created by the init procedures called above.
     */

    /*
     * Specify a user-specific startup file to invoke if the application
     * is run interactively.  Typically the startup file is "~/.apprc"
     * where "app" is the name of the application.  If this line is deleted
     * then no user-specific startup file will be run under any conditions.
     */

    /* Changed 14-Dec-1999 TW  For new Tcl */
#ifdef THIS_WAS_THE_OLD_WAY
    tcl_RcFileName = "~/.wishrc";
#else
    Tcl_SetVar(interp, "tcl_rcFileName", "~/.wishrc", TCL_GLOBAL_ONLY);
#endif
    return TCL_OK;
}
