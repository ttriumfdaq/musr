/*
 *  Name:       vx_pthread.c
 *
 *  Purpose:    Minimal implementation of POSIX pthread library for VxWorks
 *
 *              VxWorks tasks and semaphores are implemented to replace
 *              POSIX pthread threads and mutexes
 *
 *  Uses:       taskLib, taskVarLib, semLib, semBLib, semMLib
 *              (VxWorks libraries)
 *
 *  Called by:  CAMP server routines
 * 
 *  Revision history:
 *    20140217  TW  global_mutex renamed to global_mutex_vxworks (name conflicts
 *                  global_mutex in camp_srv_svc_mod.c
 *  Apr 2011:   DJA:  The original version implemented a subset of DECthreads,
 *              or POSIX 1003.4a Draft 4.  Change now to the form of POSIX
 *              Standard.  If DECthreads are ever needed again, a new .h file
 *              (not written) can perform conversion.
 *              
 *
 */

#include "vx_pthread.h"
#ifdef VXWORKS
#include "sysLib.h" // sysClkRateGet
#endif // VXWORKS

/*
 *  this mutex 'global_mutex_vxworks' corresponds to the builtin
 *  pthreads mutex used by pthread_un/lock_global_np, not the CAMP
 *  global_mutex.
 */
static pthread_mutex_t global_mutex_vxworks = NULL; // SEM_ID
/* 
 *  Used for priority and stackSize in pthread_create.  May not be used.
 */
static pthread_attr_t pthread_attr_default; // TASK_DESC


/* 
 *  Implement the global mutex with our
 *  own mutex.  Must be initialized and
 *  deleted by the application.
 */
int
pthread_lock_global_np( void ) 
{
    if( global_mutex_vxworks == NULL ) global_mutex_vxworks = semMCreate( SEM_Q_FIFO );

    semTake( global_mutex_vxworks, WAIT_FOREVER );

    return( 0 );
}

int
pthread_unlock_global_np( void ) 
{
    semGive( global_mutex_vxworks );

    return( 0 );
}

int
pthread_once( pthread_once_t* pOnce, void (* init)(void) )
{
    init();

    /*
     *  Initialize default attributes to main thread
     */
    taskInfoGet( taskIdSelf(), &pthread_attr_default );

    return( 0 );
}

pthread_t
pthread_self( void )
{
    return( taskIdSelf() );
}

/*
 *  Thread attributes (was pthread_attr_create)
 * 
 *  Used for priority and stackSize in pthread_create
 */
int
pthread_attr_init( pthread_attr_t* pAttr )
{
    /* 
     *  Start with inherited attributes
     */
    taskInfoGet( taskIdSelf(), pAttr );

    return( 0 );
}

int
pthread_attr_destroy( pthread_attr_t* pAttr )
{
    return( 0 );
}

int
pthread_attr_setinheritsched( pthread_attr_t* pAttr, int inherit )
{
    // todo: inherit=PTHREAD_INHERIT_SCHED inherits priority, etc.

    return( 0 );
}

/*
 *  vxworks threads are compatible with a detached thread state
 */
int 
pthread_attr_setdetachstate( pthread_attr_t* pAttr, int detachstate )
{
    return( 0 );
}

int 
pthread_setschedparam( pthread_t thread, int scheduler, const struct sched_param *param )
{
    int priority = param->sched_priority;
    taskPrioritySet( thread, priority );

    return( 0 );
}

/*
 *  Thread creation/deletion
 */
int
pthread_create( pthread_t* pThread, pthread_attr_t* pAttr, void* (*start_routine)(void*), void* arg )
{
    TASK_DESC* pTaskDesc = pAttr;

    if( ( *pThread = taskSpawn( NULL, pTaskDesc->td_priority, VX_FP_TASK, 
                       pTaskDesc->td_stackSize, (FUNCPTR)start_routine, 
                       (int)arg, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ) == ERROR )
    {
        return( -1 );
    }

    return( 0 );
}

/*
 *  "Tasks implicitly call exit() if the entry routine
 *  specified during task creation returns."  This is 
 *  compatible with pthreads.
 *  No need to free task storage.
 */
int
pthread_detach( pthread_t thread )
{
    return( 0 );
}

int 
pthread_cancel( pthread_t thread )
{
    if( taskDelete( thread ) == ERROR ) return( -1 );

    return( 0 );
}

int
pthread_setcancelstate( int state, int *pOldstate ) /* was pthread_setcancel */
{
    if( state == PTHREAD_CANCEL_ENABLE ) 
    {
        if( taskUnsafe() == ERROR ) return( -1 );
    }
    else if( state == PTHREAD_CANCEL_DISABLE ) 
    {
        if( taskSafe() == ERROR ) return( -1 );
    }
    return( 0 );
}

void
pthread_testcancel( void )
{
    return;
}

void
pthread_exit( void* value )
{
    exit( 0 );
}

int
pthread_join( pthread_t thread, void** value )
{
    /* wait until the task ID doesn't exist */
    for(;;) if( taskIdVerify( thread ) == ERROR ) break;
    return( 0 );
}

/* 
 *  Only allowed one 4-byte task specific variable
 */
int 
pthread_key_create( pthread_key_t* pKey, void (*destructor)(void*) )
{
    return( 0 );
}

int 
pthread_key_delete( pthread_key_t key )
{
    return( 0 );
}

int
vx_pthread_setspecific( pthread_key_t* pKey, void* value )
{
    if( taskVarAdd( 0, (int*)pKey ) == ERROR ) return( -1 );

    *pKey = (pthread_key_t)value;

    return( 0 );
}

void*
pthread_getspecific( pthread_key_t key )
{
    return( (void *)key );
}


/*
 *  mutexes
 */
/* was pthread_mutexattr_create */
int
pthread_mutexattr_init( pthread_mutexattr_t* pAttr ) 
{
    return( 0 );
}

/* was pthread_mutexattr_delete */
int
pthread_mutexattr_destroy( pthread_mutexattr_t* pAttr ) 
{
    return( 0 );
}

/* was pthread_mutexattr_setkind_np */
/*
 *  this is used in CAMP to set a recursive mutex.  The mutual-exclusion
 *  semaphores created for VxWorks are recursive by default.
 */
int
pthread_mutexattr_settype( pthread_mutexattr_t* pAttr, int type )
{
    return( 0 );
}

/* was pthread_mutexattr_getkind_np */
int
pthread_mutexattr_gettype( pthread_mutexattr_t* pAttr, int* type )
{
    return( 0 );
}

/*
 *  pthread_mutex_init
 *
 *     Initialize a mutex (semaphore).  For VxWorks purposes, create a 
 *     mutual exclusion semaphore.  VxWorks mutual exclusion semaphores
 *     can be taken recursively which is the default behaviour for pthread
 *     mutexes, and the desired behaviour for CAMP.
 */
int
pthread_mutex_init( pthread_mutex_t* pMutex, pthread_mutexattr_t* pAttr )
{
    if( ( *pMutex = semMCreate( SEM_Q_FIFO ) ) == NULL ) return( -1 );

    return( 0 );
}

int
pthread_mutex_destroy( pthread_mutex_t* pMutex )
{
    if( semDelete( *pMutex ) == ERROR ) return( -1 );

    return( 0 );
}

int
pthread_mutex_lock( pthread_mutex_t* pMutex )
{
#ifdef NOT_USED_apparently_this_was_blocking_other_threads

    if( semTake( *pMutex, WAIT_FOREVER ) == ERROR ) return( -1 );

    return( 0 );

#endif /* NOT_USED */

    /*
     *  20140220  TW  moved this workaround from mutex_lock in to this VxWorks version. 
     *                exact reason for workaround unknown when writing this comment, 
     *                but it is assumed that VxWorks was causing a thread to block
     *                when doing semTake( WAIT_FOREVER )
     *
     *  note: semTake can be called with a timeout, but this will likely block
     *        in the same way as semTake( WAIT_FOREVER )
     */

    while( semTake( *pMutex, NO_WAIT ) != OK ) 
    {
	taskDelay( 1 ); // 1 tick = 1/60 sec = 16.67ms
	// camp_fsleep( 0.01 ); /* Wait for 10 ms, rounded up to nearest tick for VxWorks */
    }

    return( 0 );
}

/*
 * DA, 2012: This is odd.  Original had returned 0 on failure, 1 on success, but
 * standard pthread specifies 0 on success and error code otherwise.  Moreover,
 * in camp_srv_svc_mod.c, the expected values are 1 for success, -1 for fatal error,
 * and other for soft failure.  
 * Here, change to normal 0 for success.  Change to standard elesewhere.
 */
int
pthread_mutex_trylock( pthread_mutex_t* pMutex )
{
    /*
     *  Can't tell whether it is invalid or timed out, so return 
     *  "timed out" for any error.
     */
    if( semTake( *pMutex, NO_WAIT ) == ERROR ) return( 1 );

    return( 0 );
}

int
pthread_mutex_unlock( pthread_mutex_t* pMutex )
{
    if( semGive( *pMutex ) == ERROR ) return( -1 );

    return( 0 );
}


/*
 *  Conditions
 */
int
pthread_condattr_init( pthread_condattr_t* pAttr )
{
    return( 0 );
}

int
pthread_condattr_destroy( pthread_condattr_t* pAttr )
{
    return( 0 );
}

int
pthread_cond_init( pthread_cond_t* pCond, pthread_condattr_t *pAttr )
{
    if( ( *pCond = semBCreate( SEM_Q_FIFO, SEM_FULL ) ) == NULL ) return( -1 );

    return( 0 );
}

int 
pthread_cond_destroy( pthread_cond_t* pCond )
{
    if( semDelete( *pCond ) == ERROR ) return( -1 );

    return( 0 );
}

int 
pthread_cond_signal( pthread_cond_t* pCond )
{
    if( semGive( *pCond ) == ERROR ) return( -1 );

    return( 0 );
}

int 
pthread_cond_wait( pthread_cond_t* pCond, pthread_mutex_t* pMutex )
{
    /*
     *  Test every 100msec
     */
    while( semTake( *pCond, sysClkRateGet()*0.1 ) == ERROR )
    {
        if( semGive( *pMutex ) == ERROR ) return( -1 );
        if( semTake( *pMutex, WAIT_FOREVER ) == ERROR ) return( -1 );
    }
    return( 0 );
}










