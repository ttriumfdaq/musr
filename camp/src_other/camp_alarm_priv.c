
#include <stdio.h>
#include "camp_srv.h"


/*
 *  camp_alarmCheck()
 *
 *  This routine is (ultimately) called from the Tcl command
 *  varTestAlert which is normally called from variable
 *  read/write procs in the driver.
 */
void
camp_alarmCheck( char* alarmAction )
{
    int status;
    ALARM_ACT* pAlarmAct;
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    /*
     *  Use the current thread interpreter
     *  Special care if this is the main interpreter
     */
    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    pAlarmAct = camp_sysGetpAlarmAct( alarmAction );
    if( pAlarmAct == NULL ) return;

    pAlarmAct->status &= ~CAMP_ALARM_ON;

    /*
     *  Check to see if any data are ALERTed
     */
    camp_varDoProc_recursive( alarm_update, pVarList );

    if( pAlarmAct->status & CAMP_ALARM_ON )
    {
        /* 
         *  Set alarm ON if it is not already
         */
        if( !( pAlarmAct->status & CAMP_ALARM_WAS_ON ) )
        {
            if( useMainInterp ) set_global_mutex_noChange( TRUE );
	    status = Tcl_VarEval( interp, pAlarmAct->proc, " {1}", NULL );
            if( useMainInterp ) set_global_mutex_noChange( FALSE );
            if( status == TCL_ERROR ) 
            {
                camp_appendMsg( "camp_alarmCheck: failure: alertProc (%s)", 
                            pAlarmAct->ident );
            }
            else
            {
                pAlarmAct->status |= CAMP_ALARM_WAS_ON;
            }
        }
    }
    else
    {
        /* 
         *  Set alarm OFF if it is not already
         */
        if( pAlarmAct->status & CAMP_ALARM_WAS_ON )
        {
            if( useMainInterp ) set_global_mutex_noChange( TRUE );
	    status = Tcl_VarEval( interp, pAlarmAct->proc, " {0}", NULL );
            if( useMainInterp ) set_global_mutex_noChange( FALSE );
            if( status == TCL_ERROR ) 
            {
                camp_appendMsg( "camp_alarmCheck: failure: alertProc (%s)", 
                            pAlarmAct->ident );
            }
            else
            {
                pAlarmAct->status &= ~CAMP_ALARM_WAS_ON;
            }
        }
    }
}


void
alarm_update( CAMP_VAR* pVar )
{
    if( pVar->core.status & CAMP_VAR_ATTR_ALERT )
    {
        ALARM_ACT* pAlarmAct;

        pAlarmAct = camp_sysGetpAlarmAct( pVar->core.alarmAction );
        if( pAlarmAct == NULL ) return;

        pAlarmAct->status |= CAMP_ALARM_ON;
    }
}


