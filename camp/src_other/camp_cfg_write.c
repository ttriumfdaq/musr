/*
 *  camp_write_cfg.c,  write configuration file
 */

#include "camp_srv.h"


int
campCfg_write( char* filename )
{
    save_all_ini();

    if( !openOutput( filename ) ) return( CAMP_INVAL_FILE );

    /* 
     *  Init things
     */
    reinitialize( filename, NULL, NULL );

    cfgWrite_insadd();

    cfgWrite_insset();

    print( "return -code ok\n" );

    closeOutput();

    return( CAMP_SUCCESS );
}


void
save_all_ini( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;
    RES* pRes;
    INS_FILE_req insFile_req;
    char filename[LEN_FILENAME];

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pIns = pVar->spec.CAMP_SPEC_u.pIns;
        /*
         *  Make sure an INI file exists for the Instrument
         */
        if( ( pIns->iniFile == NULL ) || ( strlen( pIns->iniFile ) == 0 ) )
        {
            sprintf( filename, INI_PFMT, pVar->core.ident );
        }
        else
        {
            strcpy( filename, pIns->iniFile );
        }
        insFile_req.dreq.path = pVar->core.path;
        insFile_req.flag = 1;
        insFile_req.datFile = filename;
        pRes = campsrv_inssave_13( &insFile_req, get_current_rqstp() );
        _free( pRes );
    }
}


void 
cfgWrite_insadd( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pIns = pVar->spec.CAMP_SPEC_u.pIns;
        print_tab( "%s %s %s\n", toktostr( TOK_INS_ADD ), 
                    pIns->typeIdent, pVar->core.ident );
    }
}


void 
cfgWrite_insset( void )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns;

    for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
    {
        pIns = pVar->spec.CAMP_SPEC_u.pIns;

        if( ( pIns->iniFile != NULL ) && ( strlen( pIns->iniFile ) > 0 ) )
        {
            print_tab( "catch {%s %s {%s}}\n", toktostr( TOK_INS_LOAD ), 
                        pVar->core.path, pIns->iniFile );
	}
        add_tab();
        camp_varDoProc_recursive( cfgWrite_varLink, pVar->pChild );
        del_tab();
    }
}


int
cfgWrite_if( CAMP_IF* pIF )
{
    print( "%s %s %f %s ", 
                toktostr( TOK_OPT_IF_SET ),
		pIF->typeIdent,
                pIF->accessDelay,
                pIF->defn );

    return( CAMP_SUCCESS );
}


void 
cfgWrite_varLink( CAMP_VAR* pVar )
{
    if( pVar->spec.varType == CAMP_VAR_TYPE_LINK )
    {
        CAMP_LINK* pLnk = pVar->spec.CAMP_SPEC_u.pLnk;

        print( "%s %s %s\n", toktostr( TOK_LNK_SET ), 
                pVar->core.path, pLnk->path );
    }
}


