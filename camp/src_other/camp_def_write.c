/*
 *  camp_def_write.c,  data file utilities
 */

#include <stdio.h>
#include "camp_srv.h"

static char curr_path[LEN_PATH+1];


/*
 *  campDef_write(),        
 */
int
campDef_write( char* filename, CAMP_VAR* pVar )
{
    if( !openOutput( filename ) ) return( CAMP_INVAL_FILE );

    /* 
     *  Init things
     */
    reinitialize( filename, NULL, NULL );

    camp_pathInit( curr_path );
    camp_pathDown( curr_path, pVar->core.ident );

    defWrite_var( pVar );

    print( "return -code ok\n" );

    closeOutput();

    return( CAMP_SUCCESS );
}


void 
defWrite_var( CAMP_VAR* pVar )
{
    defWrite_stat( &pVar->core );
    defWrite_dyna( &pVar->core );
    defWrite_spec( pVar );
}


void 
defWrite_stat( CAMP_CORE* pCore )
{
    char str[LEN_STR+1];

    camp_varGetTypeStr( pCore->path, str );
    print_tab( "%s ", str );
    print( "%s ", curr_path );

    if( pCore->attributes & CAMP_VAR_ATTR_SHOW ) 
        print( "%s ", toktostr( TOK_SHOW_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_SET ) 
        print( "%s ", toktostr( TOK_SET_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_READ ) 
        print( "%s ", toktostr( TOK_READ_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_POLL ) 
        print( "%s ", toktostr( TOK_POLL_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_LOG ) 
        print( "%s ", toktostr( TOK_LOG_ATTR ) );
    if( pCore->attributes & CAMP_VAR_ATTR_ALARM ) 
        print( "%s ", toktostr( TOK_ALARM_ATTR ) );

    if( pCore->title[0] != '\0' )
    {
        print( "%s \"%s\" ", toktostr( TOK_TITLE ), pCore->title );
    }
    if( pCore->help[0] != '\0' )
    {
        print( "%s \"%s\" ", toktostr( TOK_HELP ), pCore->help );
    }
    if( pCore->readProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_READPROC ), pCore->readProc );
    }
    if( pCore->writeProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_WRITEPROC ), pCore->writeProc );
    }
}


void 
defWrite_dyna( CAMP_CORE* pCore )
{
    if( pCore->status & CAMP_VAR_ATTR_SHOW ) 
    {
        print( "%s %s ", toktostr( TOK_SHOW_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->status & CAMP_VAR_ATTR_SET ) 
    {
        print( "%s %s ", toktostr( TOK_SET_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->status & CAMP_VAR_ATTR_READ ) 
    {
        print( "%s %s ", toktostr( TOK_READ_FLAG ), toktostr( TOK_ON ) );
    }

/*  This can cause major problems
    Sorry, it'll have to be done individually

    if( pCore->status & CAMP_VAR_ATTR_POLL ) 
    {
        print( "%s %s ", toktostr( TOK_POLL_FLAG ), toktostr( TOK_ON ) );
    }
*/
    if( pCore->pollInterval > 0.0 ) 
    {
        print( "%s %f ", toktostr( TOK_POLL_INTERVAL ), pCore->pollInterval );
    }

    if( pCore->status & CAMP_VAR_ATTR_LOG ) 
    {
        print( "%s %s ", toktostr( TOK_LOG_FLAG ), toktostr( TOK_ON ) );
    }
    if( !streq( pCore->logAction, "" ) )
    {
        print( "%s %s ", toktostr( TOK_LOG_ACTION ), pCore->logAction );
    }

    if( pCore->status & CAMP_VAR_ATTR_ALARM ) 
    {
        print( "%s %s ", toktostr( TOK_ALARM_FLAG ), toktostr( TOK_ON ) );
    }
    if( !streq( pCore->alarmAction, "" ) )
    {
        print( "%s %s ", toktostr( TOK_ALARM_ACTION ), pCore->alarmAction );
    }
}


void 
defWrite_spec( CAMP_VAR* pVar )
{
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        defWrite_Numeric( pVar );
        break;
      case CAMP_VAR_TYPE_SELECTION:
        defWrite_Selection( pVar );
        break;
      case CAMP_VAR_TYPE_STRING:
        defWrite_String( pVar );
        break;
      case CAMP_VAR_TYPE_ARRAY:
        defWrite_Array( pVar );
        break;
      case CAMP_VAR_TYPE_STRUCTURE:
        defWrite_Structure( pVar );
        break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        defWrite_Facility( pVar );
        break;
      case CAMP_VAR_TYPE_LINK:
        defWrite_Link( pVar );
        break;
    }
    return;
}


void 
defWrite_Numeric( CAMP_VAR* pVar )
{
  CAMP_NUMERIC* pNum = pVar->spec.CAMP_SPEC_u.pNum;
  char str[LEN_STR+1];
  
  if( pNum->tol >= 0.0 ) 
    {
      print( "%s %d ", toktostr( TOK_ALARM_TOLTYPE ), pNum->tolType );
      print( "%s %f ", toktostr( TOK_ALARM_TOL ), pNum->tol );
    }
  
  if( !streq( pNum->units, "" ) )
    {
      print( "%s %s ", toktostr( TOK_UNITS ), pNum->units );
    }
  
  if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
     ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
      numGetValStr( pVar->core.varType, pNum->val, str );
      print( "%s %s ", toktostr( TOK_VALUE_FLAG ), str );
    }
  print( "\n" );
}


void 
defWrite_Selection( CAMP_VAR* pVar )
{
    CAMP_SELECTION* pSel = pVar->spec.CAMP_SPEC_u.pSel;
    CAMP_SELECTION_ITEM* pSelItem;

    print( "%s ", toktostr( TOK_SELECTIONS ) );
    for( pSelItem = pSel->pItems; pSelItem != NULL; pSelItem = pSelItem->pNext )
    {
        print( "%s ", pSelItem->label );
    }

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s %d ", toktostr( TOK_VALUE_FLAG ), pSel->val );
    }
    print( "\n" );
}


void 
defWrite_String( CAMP_VAR* pVar )
{
    CAMP_STRING* pStr = pVar->spec.CAMP_SPEC_u.pStr;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s \"%s\" ", toktostr( TOK_VALUE_FLAG ), pStr->val );
    }
    print( "\n" );
}


void 
defWrite_Array( CAMP_VAR* pVar )
{
    CAMP_ARRAY* pArr = pVar->spec.CAMP_SPEC_u.pArr;
    int tokKind;
    int i;

    switch( pArr->varType )
    {
      case CAMP_VAR_TYPE_INT: tokKind = TOK_INT; break;
      case CAMP_VAR_TYPE_FLOAT: tokKind = TOK_FLOAT; break;
      case CAMP_VAR_TYPE_STRING: tokKind = TOK_STRING; break;
    }

    print( "%s %s %d %d ", toktostr( TOK_ARRAYDEF ), toktostr( tokKind ),
                           pArr->elemSize, pArr->dim );

    for( i = 0; i < pArr->dim; i++ )
    {
        print( "%d ", pArr->dimSize[i] );
    }

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        /* Not done */
    }
    print( "\n" );
}


void 
defWrite_Structure( CAMP_VAR* pStcVar )
{
    CAMP_VAR* pVar;

    print( "\n" );

    add_tab();

    for( pVar = pStcVar->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        camp_pathDown( curr_path, pVar->core.ident );
        defWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();
}


void 
defWrite_Facility( CAMP_VAR* pInsVar )
{
    CAMP_VAR* pVar;
    CAMP_INSTRUMENT* pIns = pInsVar->spec.CAMP_SPEC_u.pIns;

    print( "%s %s ", toktostr( TOK_INSTYPE ), pIns->typeIdent );

    if( pIns->initProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_INITPROC ), pIns->initProc );
    }

    if( pIns->deleteProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_DELETEPROC ), 
                pIns->deleteProc );
    }

    if( pIns->onlineProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_ONLINEPROC ), 
                pIns->onlineProc );
    }

    if( pIns->offlineProc[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_OPT_OFFLINEPROC ), 
                pIns->offlineProc );
    }
    print( "\n" );

    add_tab();

    for( pVar = pInsVar->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        camp_pathDown( curr_path, pVar->core.ident );
        defWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();
}


void 
defWrite_Link( CAMP_VAR* pVar )
{
    CAMP_LINK* pLnk = pVar->spec.CAMP_SPEC_u.pLnk;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s %s ", toktostr( TOK_VALUE_FLAG ), pLnk->path );
    }
    print( "\n" );
}


