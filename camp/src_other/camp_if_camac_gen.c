/*
 *  camp_if_camac_gen.c,  generic CAMP CAMAC interface
 *
 */

#include <stdio.h>
#include "camp_srv.h"


int
if_camac_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;
    int camac_if_type;

    pIF_t = camp_ifGetpIF_t( "camac" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /*
     *  Interperate interface configuration info here
     *  (none for generic).
     */

    return( CAMP_SUCCESS );
}


int
if_camac_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_read( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    cmd = pReq->spec.REQ_SPEC_u.read.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  I don't remember right now why I had to do this!
     */
    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_camac_write( REQ* pReq )
{
    int status;
    char* cmd; 
    Tcl_Interp* interp;
    int useMainInterp = FALSE;

    if( ( interp = get_thread_interp() ) == camp_tclInterp() )
      {
        useMainInterp = TRUE;
      }

    cmd = pReq->spec.REQ_SPEC_u.write.cmd;

    /*
     *  If called from the main tcl Interpreter (i.e.,
     *  the interpreter in the main thread of execution)
     *  then lock the global mutex.
     *  I don't remember right now why I had to do this!
     */
    if( useMainInterp ) set_global_mutex_noChange( TRUE );
    status = Tcl_Eval( interp, cmd );
    if( useMainInterp ) set_global_mutex_noChange( FALSE );
    if( status == TCL_ERROR )
    {
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}
