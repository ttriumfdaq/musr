/*
 *  camp_if_gpib_mvip300.c,  utilities for operating GPIB instruments
 *                           via MVME MVIP300 Industry Pack
 *
 *  timeout = 1 to 17 (time = 1e((timeout+1)/2)usec, see ugpib.h)
 *
 *  INI file configuration string:
 *    none
 *
 *  Meaning of private (driver-specific) variables:
 *   pIF_t->priv == 1 (gpib available)
 *               == 0 (gpib unavailable)
 *   pIF->priv   - pointer to GPIB_PHYS_DEV structure
 *
 */

#include <stdio.h>
#include "camp_srv.h"
#include "ugpib.h"
#define GPIBLIB_H    /* bring in global variables */
#include "gpibLib.h"

#define if_gpib_ok  (pIF_t->priv!=0)

static void getTerm( char* term_in, char* term_out, int* term_len );


int
if_gpib_init( void )
{
    int status;
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( "gpib" );
    if( pIF_t == NULL)
    {
        return( CAMP_FAILURE );
    }

    /* 
     *  priv holds the GPIB_PHYS_DEV pointer
     *  The value is zero if GPIB is unavailable
     */
    pIF_t->priv = 0; 

    /*
     *  Do initialization in MVME startup command file
     *  So assume it's OK
     *  Could I check for it somehow?
     */

    pIF_t->priv = 1;

    return( CAMP_SUCCESS );
}


int
if_gpib_online( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;
    int addr;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    if( ( ( addr = camp_getIfGpibAddr( pIF->defn ) ) < 0 ) ||
          ( addr > 31 ) )
    {
        camp_appendMsg( "invalid gpib address" );
        return( CAMP_FAILURE );
    }

    pIF->priv = (long)gpibPhysDevCreate( addr );
    if( pIF->priv == 0 )
    {
        camp_appendMsg( "failed allocating gpib device" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


int
if_gpib_offline( CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;
    int status;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    status = gpibPhysDevDelete( (GPIB_PHYS_DEV*)pIF->priv );
    if( status == ERROR )
    {
        camp_appendMsg( "failed deleting gpib device" );
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_read,  write command, read response
 */
int 
if_gpib_read( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char* buf;
    int buf_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    GPIB_PHYS_DEV* pGpibItem;
    char* command;
    int maxReadCount;
    int count;
    int readCount;
    int endOfString;
    char term_test;
#define MAX_READ 0xFFFE /* Same as IBMAXIO */

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    pGpibItem = (GPIB_PHYS_DEV*)pIF->priv;
    cmd = pReq->spec.REQ_SPEC_u.read.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.read.cmd_len;
    buf = pReq->spec.REQ_SPEC_u.read.buf;
    buf_len = pReq->spec.REQ_SPEC_u.read.buf_len;

    getTerm( camp_getIfGpibWriteTerm( pIF->defn, str ), term, &term_len );

    command = (char*)malloc( cmd_len+term_len+1 );
    strncpy( command, cmd, cmd_len );
    strncpy( &command[cmd_len], term, term_len );
    command[cmd_len+term_len] = '\0';

    /*
     *  Can't get this to work.
     *  Just disable.
     */
/*    endOfString = ( (REOS) << 8 ) | term[0]; */
    endOfString = 0;
    ibeos( endOfString );

    if( camp_debug > 0 ) 
    {
      printf( "eos = %04X\n", endOfString );
    }

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    semTake( semGPIB, WAIT_FOREVER );
    count = cmd_len + term_len; 
    gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
    gpibError = iberr;
    /*
     *  Often get addressing error, but
     *  works if repeat command
     */
    if( ( gpibStatus & ERR ) && ( gpibError == EADR ) )
    {
      gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
      gpibError = iberr;
    }
    semGive( semGPIB );
    status = ( gpibStatus & ERR ) ? ERROR : OK;

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    if( status != ERROR )
      {
        if( camp_debug > 0 )
        {
          int i;

          printf( "gpib: wrote %d characters, '", count );
          for( i = 0; i < count; i++ )
    	  {
    	    if( isprint( command[i] ) ) printf( "%c", command[i] );
            else printf( "\\%03o", command[i] );
    	  }
          printf( "'\n" );
        }
      }

    free( command );

    if( status == ERROR )
    {
	if( gpibStatus & TIMO )
	  {
	    camp_appendMsg( "gpib timeout on write" );
	  }
	else
	  {
	    camp_appendMsg( "failed gpib write" );
	  }

        if( camp_debug > 0 )
        {
          printf( "gpib write failed, status = %08X\n", gpibStatus );
        }

        return( CAMP_FAILURE );
    }

    readCount = 0;

    getTerm( camp_getIfGpibReadTerm( pIF->defn, str ), term, &term_len );

    /*
     *  Can't get this to work.
     *  Just disable.
     */
/*    endOfString = ( (REOS) << 8 ) | term[0]; */
    endOfString = 0;
    if( camp_debug > 0 ) 
    {
      printf( "eos = %04X\n", endOfString );
    }

    while( readCount < buf_len )
    {
      maxReadCount = ( buf_len-readCount < MAX_READ ) ? 
                       buf_len-readCount : MAX_READ;

#ifdef MULTITHREADED
      thread_unlock_global_np();
#endif /* MULTITHREADED */

      semTake( semGPIB, WAIT_FOREVER );

      ibeos( endOfString );

      gpibStatus = dvrd( pGpibItem->iPriBusId, &buf[readCount], maxReadCount );
      gpibError = iberr;
      count = ibcnt;
      semGive( semGPIB );
      status = ( gpibStatus & ERR ) ? ERROR : OK;

#ifdef MULTITHREADED
      thread_lock_global_np();
#endif /* MULTITHREADED */

      if( status == ERROR )
      {
	if( gpibStatus & TIMO )
	  {
	    camp_appendMsg( "gpib timeout on read" );
	  }
	else
	  {
	    camp_appendMsg( "failed gpib read" );
	  }
        return( CAMP_FAILURE );
      }
      else
      {
        if( camp_debug > 0 )
        {
          int i;

          printf( "gpib: read %d characters, '", count );
          for( i = 0; i < count; i++ )
    	  {
    	    if( isprint( buf[readCount+i] ) ) printf( "%c", buf[readCount+i] );
            else printf( "\\%03o", buf[readCount+i] );
    	  }
          printf( "'\n" );
        }

        readCount += count;
      }
      /* 
       *  Could put check for termination here,
       *  instead just quit after one read
       */
      break;
    }

    /*
     *  Return number of bytes read
     */
    pReq->spec.REQ_SPEC_u.read.read_len = readCount;

    return( CAMP_SUCCESS );
}


/*
 *  if_gpib_write,  
 */
int 
if_gpib_write( REQ* pReq )
{
    int status;
    CAMP_IF* pIF;
    CAMP_IF_t* pIF_t;
    char* cmd;
    int cmd_len;
    char term[3];
    int term_len;
    char str[LEN_STR+1];
    long addr;
    GPIB_PHYS_DEV* pGpibItem;
    char* command;
    int endOfString;
    int count;

    pIF = pReq->spec.REQ_SPEC_u.read.pIF;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        return( CAMP_INVAL_IF_TYPE );
    }

    if( !if_gpib_ok ) 
    {
        camp_appendMsg( "gpib unavailable" );
        return( CAMP_FAILURE );
    }

    pGpibItem = (GPIB_PHYS_DEV*)pIF->priv;
    cmd = pReq->spec.REQ_SPEC_u.write.cmd;
    cmd_len = pReq->spec.REQ_SPEC_u.write.cmd_len;

    getTerm( camp_getIfGpibWriteTerm( pIF->defn, str ), term, &term_len );

    /*
     *  Can't get this to work.
     *  Just disable.
     */
/*    endOfString = ( (REOS) << 8 ) | term[0]; */
    endOfString = 0;
    ibeos( endOfString );
    if( camp_debug > 0 ) 
    {
      printf( "eos = %04X\n", endOfString );
    }

    command = (char*)malloc( cmd_len+term_len+1 );
    strncpy( command, cmd, cmd_len );
    strncpy( &command[cmd_len], term, term_len );
    command[cmd_len+term_len] = '\0';

#ifdef MULTITHREADED
    thread_unlock_global_np();
#endif /* MULTITHREADED */

    semTake( semGPIB, WAIT_FOREVER );
    count = cmd_len + term_len;
    gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
    gpibError = iberr;
    /*
     *  Often get addressing error, but
     *  works if repeat command
     */
    if( ( gpibStatus & ERR ) && ( gpibError == EADR ) )
    {
      gpibStatus = dvwrt( pGpibItem->iPriBusId, command, count );
      gpibError = iberr;
    }
    semGive( semGPIB );
    status = ( gpibStatus & ERR ) ? ERROR : OK;

#ifdef MULTITHREADED
    thread_lock_global_np();
#endif /* MULTITHREADED */

    if( status != ERROR )
      {
        if( camp_debug > 0 )
        {
          int i;

          printf( "gpib: wrote %d characters, '", count );
          for( i = 0; i < count; i++ )
    	  {
    	    if( isprint( command[i] ) ) printf( "%c", command[i] );
            else printf( "\\%03o", command[i] );
    	  }
          printf( "'\n" );
        }
      }

    free( command );

    if( status == ERROR )
    {
	if( gpibStatus & TIMO )
	  {
	    camp_appendMsg( "gpib timeout on write" );
	  }
	else
	  {
	    camp_appendMsg( "failed gpib write" );
	  }
        return( CAMP_FAILURE );
    }

    return( CAMP_SUCCESS );
}


static void
getTerm( char* term_in, char* term_out, int* term_len )
{
    TOKEN tok;

    findToken( term_in, &tok );

    switch( tok.kind )
    {
        case TOK_LF:   strcpy( term_out, "\012" );     *term_len = 1; return;
        case TOK_CR:   strcpy( term_out, "\015" );     *term_len = 1; return;
        case TOK_CRLF: strcpy( term_out, "\015\012" ); *term_len = 2; return;
        case TOK_LFCR: strcpy( term_out, "\012\015" ); *term_len = 2; return;
        case TOK_NONE: strcpy( term_out, "" );         *term_len = 0; return;
        default:       strcpy( term_out, "" );         *term_len = 0; return;
    }
}


