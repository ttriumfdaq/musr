
#include <stdio.h>
#include <math.h>
#include "camp_srv.h"


int
camp_ifSet( CAMP_IF** ppIF, CAMP_IF* pIF )
{
    CAMP_IF_t* pIF_t;

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        camp_appendMsg( msg_inval_if_t, pIF->typeIdent );
        return( CAMP_INVAL_IF_TYPE );
    }

    if( *ppIF == NULL )
    {
        *ppIF = (CAMP_IF*)zalloc( sizeof( CAMP_IF ) );
    }
    else
    {
        if( (*ppIF)->status & CAMP_IF_ONLINE )
        {
            camp_appendMsg( 
                "camp_ifSet: failure: can't reset an online interface %s", 
                pIF->typeIdent );
            return( CAMP_CANT_SET_IF );
        }

        xdr_free( xdr_CAMP_IF, (char*)*ppIF );
        bzero( *ppIF, sizeof( CAMP_IF ) );
    }

    (*ppIF)->typeIdent = strdup( pIF->typeIdent );
    (*ppIF)->accessDelay = pIF->accessDelay;
    (*ppIF)->defn = strdup( pIF->defn );
    (*ppIF)->typeID = camp_getIfTypeID( pIF->typeIdent );

    return( CAMP_SUCCESS );
}


int
camp_ifOnline( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;

    if( pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, get_current_ident() );
        return( CAMP_INVAL_IF );
    }

    if( pIF->status & CAMP_IF_ONLINE ) 
    {
        return( CAMP_SUCCESS );
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        camp_appendMsg( msg_inval_if_t, pIF->typeIdent );
        return( CAMP_INVAL_IF_TYPE );
    }

    if( pIF_t->procs.onlineProc != NULL )
    {
        status = (*pIF_t->procs.onlineProc)( pIF );
        if( _failure( status ) )
        {
            return( status );
        }
    }

    pIF->status |= CAMP_IF_ONLINE;
    pIF->numConsecReadErrors = 0;
    pIF->numConsecWriteErrors = 0;

    return( CAMP_SUCCESS );
}


int
camp_ifOffline( CAMP_IF* pIF )
{
    int status;
    CAMP_IF_t* pIF_t;

    if( pIF == NULL ) 
    {
/*
        camp_appendMsg( msg_inval_if, get_current_ident() );
        return( CAMP_INVAL_IF );
*/
        /*
         *  Make this a successful request, since
         *  it is harmless, but could cause an application
         *  to bomb unnecessarily
         */
        return( CAMP_SUCCESS );
    }

    if( !( pIF->status & CAMP_IF_ONLINE ) )
    {
        return( CAMP_SUCCESS );
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        camp_appendMsg( msg_inval_if_t, pIF->typeIdent );
        return( CAMP_INVAL_IF_TYPE );
    }

    if( pIF_t->procs.offlineProc != NULL )
    {
        status = (*pIF_t->procs.offlineProc)( pIF );
        if( _failure( status ) )
        {
            return( status );
        }
    }

    pIF->status &= ~CAMP_IF_ONLINE;

    return( CAMP_SUCCESS );
}


int
camp_ifRead( CAMP_IF* pIF, CAMP_VAR* pVar, char* cmd, int cmd_len, 
             char* buf, int buf_len, int* pRead_len )
{
    int status;
    REQ* pReq;
    CAMP_IF_t* pIF_t;
    double delay;
    timeval_t tv;

    if( pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, get_current_ident() );
        return( CAMP_INVAL_IF );
    }

    if( pVar == NULL )
    {
        camp_appendMsg( msg_inval_var, "?" );
        return( CAMP_INVAL_VAR );
    }

    if( !( pIF->status & CAMP_IF_ONLINE ) )
    {
        camp_appendMsg( msg_if_notconn, get_current_ident() );
        return( CAMP_NOT_CONN );
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        camp_appendMsg( msg_inval_if_t, pIF->typeIdent );
        return( CAMP_INVAL_IF_TYPE );
    }

    if( pIF_t->procs.readProc != NULL )
    {
        pReq = (REQ*)zalloc( sizeof( REQ ) );

        pReq->pVar = pVar;
        pReq->spec.type = IO_READ;
        pReq->spec.REQ_SPEC_u.read.pIF = pIF;
        pReq->spec.REQ_SPEC_u.read.cmd = cmd;
        pReq->spec.REQ_SPEC_u.read.cmd_len = cmd_len;
        pReq->spec.REQ_SPEC_u.read.buf = buf;
        pReq->spec.REQ_SPEC_u.read.buf_len = buf_len;
/*
        REQ_add( pReq );
*/

        if( pIF->accessDelay < 0 ) 
        {
            thread_unlock_global_np();
            camp_fsleep( fabs( pIF->accessDelay ) );
            thread_lock_global_np();
        }
        else if( pIF->accessDelay > 0 )
        {
            gettimeval( &tv );
            delay = pIF->accessDelay - difftimeval( &tv, &pIF->lastAccess );
            if( delay > 0.0 )
            {
                thread_unlock_global_np();
                camp_fsleep( delay );
                thread_lock_global_np();
            }
        }

        status = (*pIF_t->procs.readProc)( pReq );
        pIF->numReads++;
        gettimeval( &pIF->lastAccess );
        if( _failure( status ) )
        {
            _free( pReq );
            *pRead_len = 0;
            pIF->numReadErrors++;
            pIF->numConsecReadErrors++;

            if( pIF->numConsecReadErrors >= MAX_CONSECUTIVE_IF_ERRORS )
            {
                camp_appendMsg( "Too many read errors, setting interface %s offline", pVar->core.path );
                camp_ifOffline( pIF );
            }

            return( status );
        }
        else
        {
            pIF->numConsecReadErrors = 0;
        }

        *pRead_len = pReq->spec.REQ_SPEC_u.read.read_len;
        _free( pReq );
    }

    return( CAMP_SUCCESS );
}


int
camp_ifWrite( CAMP_IF* pIF, CAMP_VAR* pVar, char* cmd, int cmd_len )
{
    int status;
    REQ* pReq;
    CAMP_IF_t* pIF_t;
    timeval_t tv;
    float delay;

    if( pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, get_current_ident() );
        return( CAMP_INVAL_IF );
    }

    if( pVar == NULL )
    {
        camp_appendMsg( msg_inval_var, "?" );
        return( CAMP_INVAL_VAR );
    }

    if( !( pIF->status & CAMP_IF_ONLINE ) )
    {
        camp_appendMsg( msg_if_notconn, get_current_ident() );
        return( CAMP_NOT_CONN );
    }

    pIF_t = camp_ifGetpIF_t( pIF->typeIdent );
    if( pIF_t == NULL ) 
    {
        camp_appendMsg( msg_inval_if_t, pIF->typeIdent );
        return( CAMP_INVAL_IF_TYPE );
    }

    if( pIF_t->procs.writeProc != NULL )
    {
        pReq = (REQ*)zalloc( sizeof( REQ ) );
        pReq->pVar = pVar;
        pReq->spec.type = IO_WRITE;
        pReq->spec.REQ_SPEC_u.write.pIF = pIF;
        pReq->spec.REQ_SPEC_u.write.cmd = cmd;
        pReq->spec.REQ_SPEC_u.write.cmd_len = cmd_len;
    /*
        REQ_add( pReq );
    */
        if( pIF->accessDelay < 0 ) 
        {
            thread_unlock_global_np();
            camp_fsleep( fabs( pIF->accessDelay ) );
            thread_lock_global_np();
        }
        else if( pIF->accessDelay > 0 )
        {
            gettimeval( &tv );
            delay = pIF->accessDelay - difftimeval( &tv, &pIF->lastAccess );
            if( delay > 0.0 )
            {
                thread_unlock_global_np();
                camp_fsleep( delay );
                thread_lock_global_np();
            }
        }

        status = (*pIF_t->procs.writeProc)( pReq );
        pIF->numWrites++;
        gettimeval( &pIF->lastAccess );
        if( _failure( status ) )
        {
            _free( pReq );
            pIF->numWriteErrors++;
            pIF->numConsecWriteErrors++;

            if( pIF->numConsecWriteErrors >= MAX_CONSECUTIVE_IF_ERRORS )
            {
                camp_appendMsg( "Too many write errors, setting interface %s offline", pVar->core.path );
                camp_ifOffline( pIF );
            }

            return( status );
        }
        else
        {
            pIF->numConsecWriteErrors = 0;
        }

        _free( pReq );
    }

    return( CAMP_SUCCESS );
}



