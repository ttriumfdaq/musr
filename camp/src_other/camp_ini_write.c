/*
 *  camp_ini_write.c,  ini file writer
 */

#include "camp_srv.h"

static char curr_path[LEN_PATH+1];


/*
 *  campIni_write(),        
 */
int
campIni_write( char* filename, CAMP_VAR* pVar )
{
    if( !openOutput( filename ) ) 
        return( CAMP_INVAL_FILE );

    /* 
     *  Init things
     */
    reinitialize( filename, NULL, NULL );

    camp_pathInit( curr_path );
    camp_pathDown( curr_path, pVar->core.ident );

    iniWrite_var( pVar );

    print( "return -code ok\n" );

    closeOutput();

    return( CAMP_SUCCESS );
}


void 
iniWrite_stat( CAMP_CORE* pCore )
{
/*  13-Nov-1996  TW  Changed TOK_VAR_SET to TOK_VAR_DOSET
                     Done this to stop redundant setting problems
                     But made need to reconsider

    print_tab( "%s ", toktostr( TOK_VAR_SET ) );
*/
    print_tab( "%s ", toktostr( TOK_VAR_DOSET ) );
    print( "%s ", curr_path );
}


void 
iniWrite_dyna( CAMP_CORE* pCore )
{
    /*
     *  Polling info
     */
/* This can cause major problems
   Sorry, it'll have to be done individually

    if( pCore->status & CAMP_VAR_ATTR_POLL ) 
    {
        print( "%s %s ", toktostr( TOK_POLL_FLAG ), toktostr( TOK_ON ) );
    }
*/
    if( pCore->pollInterval > 0.0 ) 
    {
        print( "%s %f ", toktostr( TOK_POLL_INTERVAL ), pCore->pollInterval );
    }

    /*
     *  Logging info
     */
    if( pCore->status & CAMP_VAR_ATTR_LOG ) 
    {
        print( "%s %s ", toktostr( TOK_LOG_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->logAction[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_LOG_ACTION ), pCore->logAction );
    }

    /*
     *  Alarm info
     */
    if( pCore->status & CAMP_VAR_ATTR_ALARM ) 
    {
        print( "%s %s ", toktostr( TOK_ALARM_FLAG ), toktostr( TOK_ON ) );
    }
    if( pCore->alarmAction[0] != '\0' )
    {
        print( "%s {%s} ", toktostr( TOK_ALARM_ACTION ), pCore->alarmAction );
    }
}


bool_t 
ini_check_dyna( CAMP_CORE* pCore )
{
    if( ( pCore->status & CAMP_VAR_ATTR_POLL ) ||
        ( pCore->pollInterval > 0.0 ) ||
        ( pCore->status & CAMP_VAR_ATTR_LOG ) ||
        ( pCore->logAction[0] != '\0' ) ||
        ( pCore->status & CAMP_VAR_ATTR_ALARM ) ||
        ( pCore->alarmAction[0] != '\0' ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_num( CAMP_VAR* pVar )
{
    if( ( pVar->spec.CAMP_SPEC_u.pNum->tol >= 0.0 ) ||
        ( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
          ( pVar->core.status & CAMP_VAR_ATTR_SET ) ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_sel( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_str( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_arr( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


bool_t 
ini_check_lnk( CAMP_VAR* pVar )
{
    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        return( TRUE );
    }

    return( FALSE );
}


void 
iniWrite_var( CAMP_VAR* pVar )
{
    switch( pVar->core.varType & CAMP_MAJOR_VAR_TYPE_MASK )
    {
      case CAMP_VAR_TYPE_NUMERIC:
        if( ini_check_dyna( &pVar->core ) || ini_check_num( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            defWrite_Numeric( pVar );
        }
        break;
      case CAMP_VAR_TYPE_SELECTION:
        if( ini_check_dyna( &pVar->core ) || ini_check_sel( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            iniWrite_Selection( pVar );
        }
        break;
      case CAMP_VAR_TYPE_STRING:
        if( ini_check_dyna( &pVar->core ) || ini_check_str( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            defWrite_String( pVar );
        }
        break;
      case CAMP_VAR_TYPE_ARRAY:
        if( ini_check_dyna( &pVar->core ) || ini_check_arr( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            iniWrite_Array( pVar );
        }
        break;
      case CAMP_VAR_TYPE_STRUCTURE:
        iniWrite_Structure( pVar );
        break;
      case CAMP_VAR_TYPE_INSTRUMENT:
        iniWrite_Instrument( pVar );
        break;
      case CAMP_VAR_TYPE_LINK:
        if( ini_check_dyna( &pVar->core ) || ini_check_lnk( pVar ) )
        {
            iniWrite_stat( &pVar->core );
            iniWrite_dyna( &pVar->core );
            defWrite_Link( pVar );
        }
        break;
    }
}


void 
iniWrite_Selection( CAMP_VAR* pVar )
{
    CAMP_SELECTION* pSel = pVar->spec.CAMP_SPEC_u.pSel;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        print( "%s %d ", toktostr( TOK_VALUE_FLAG ), pSel->val );
    }
    print( "\n" );
}


void 
iniWrite_Array( CAMP_VAR* pVar )
{
    CAMP_ARRAY* pArr = pVar->spec.CAMP_SPEC_u.pArr;

    if( ( pVar->core.status & CAMP_VAR_ATTR_IS_SET ) &&
        ( pVar->core.status & CAMP_VAR_ATTR_SET ) )
    {
        /* Not done */
    }
    print( "\n" );
}


void 
iniWrite_Structure( CAMP_VAR* pStcVar )
{
    CAMP_VAR* pVar;

    add_tab();

    for( pVar = pStcVar->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        camp_pathDown( curr_path, pVar->core.ident );
        iniWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();
}


void 
iniWrite_Instrument( CAMP_VAR* pInsVar )
{
    CAMP_INSTRUMENT* pIns = pInsVar->spec.CAMP_SPEC_u.pIns;
    CAMP_VAR* pVar;

    if( pIns->pIF != NULL )
    {
        print_tab( "%s %s ", toktostr( TOK_INS_SET ), curr_path );
        cfgWrite_if( pIns->pIF );
        if( pIns->pIF->status & CAMP_IF_ONLINE )
            print( "%s", toktostr( TOK_OPT_ONLINE ) );
        print( "\n" );
    }

    add_tab();

    for( pVar = pInsVar->pChild; pVar != NULL; pVar = pVar->pNext )
    {
        camp_pathDown( curr_path, pVar->core.ident );
        iniWrite_var( pVar );
        camp_pathUp( curr_path );
    }

    del_tab();
}




