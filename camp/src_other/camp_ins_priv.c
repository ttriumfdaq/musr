/*
 *  camp_ins_priv.c -- 
 *  
 *        Routines to initialize the system instrument 
 *      types and the available instrument instances.
 *      Also, routines to call the instrument driver's
 *      procedures (initProc, deleteProc, onlineProc, 
 *        offlineProc).
 *  
 */

#include <stdio.h>
#include "camp_srv.h"


/*
 *  camp_insInit -- call an instrument's "initProc"
 */
int
camp_insInit( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.initProc != NULL )
    {
        status = (*pInsType->procs.initProc)( pVar );
        if( _failure( status ) )
        {
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insInit: failure: initProc %s",
                            pVar->core.path );
            }
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_insDelete -- call an instruments "deleteProc"
 */
int
camp_insDelete( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.deleteProc != NULL )
    {
        status = (*pInsType->procs.deleteProc)( pVar );
        if( _failure( status ) )
        {
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insDelete: failure: deleteProc %s",
                        pVar->core.path );
            }
            return( status );
        }
    }
    else
    {
        /*
         *  Default action is to try to call the "offlineProc"
         */
        status = camp_insOffline( pVar );
        if( _failure( status ) )
        {
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insDelete: failure: camp_insOffline %s",
                        pVar->core.path );
            }
        }
    }

    /* 
     *  Make sure it is really offline
     *  Do this to try and ensure that the interface
     *  is freed (i.e., port deallocated, etc.).
     */
    pIns = pVar->spec.CAMP_SPEC_u.pIns;
    if( ( pIns->pIF != NULL ) && ( pIns->pIF->status & CAMP_IF_ONLINE ) )
    {
        status = camp_ifOffline( pIns->pIF );
        if( _failure( status ) )
        {
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insDelete: failure: camp_ifOffline  %s",
                        pVar->core.path );
            }
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_insOnline -- call an instruments "onlineProc"
 */
int
camp_insOnline( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

/*  Don't worry about an undefined interface here
    if( pIns->pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, pVar->core.path );
        return( CAMP_INVAL_IF );
    }
*/
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.onlineProc != NULL )
    {
        status = (*pInsType->procs.onlineProc)( pVar );
        if( _failure( status ) )
        {
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insOnline: failure: onlineProc %s",
                            pVar->core.path );
            }
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


/*
 *  camp_insOffline -- call an instruments "offlineProc"
 */
int 
camp_insOffline( CAMP_VAR* pVar )
{
    int status;
    CAMP_INSTRUMENT* pIns;
    INS_TYPE* pInsType;

    pIns = pVar->spec.CAMP_SPEC_u.pIns;

/*  Don't worry about an undefined interface here
    if( pIns->pIF == NULL ) 
    {
        camp_appendMsg( msg_inval_if, pVar->core.path );
        return( CAMP_INVAL_IF );
    }
*/
    pInsType = camp_sysGetpInsType( pIns->typeIdent );
    if( pInsType == NULL ) 
    {        
        camp_appendMsg( msg_inval_ins_type, pIns->typeIdent );
        return( CAMP_INVAL_INS_TYPE );
    }

    set_current_ident( pVar->core.ident );

    if( pInsType->procs.offlineProc != NULL )
    {
        status = (*pInsType->procs.offlineProc)( pVar );
        if( _failure( status ) && ( status != CAMP_INVAL_IF ) )
        {
            if( camp_debug > 0 )
            {
                camp_appendMsg( "camp_insOffline: failure: offlineProc %s",
                            pVar->core.ident );
            }
            return( status );
        }
    }

    return( CAMP_SUCCESS );
}


