
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include "camp_srv.h"

#ifndef MULTITHREADED
static struct svc_req* current_rqstp = NULL;
static SVCXPRT* current_transp = NULL;
static int reply_sent;
static timeval_t tv_start;
#endif /* !MULTITHREADED */

typedef union {
                FILE_req campsrv_sysload_13_arg;
                FILE_req campsrv_syssave_13_arg;
                FILE_req campsrv_sysdir_13_arg;
                INS_ADD_req campsrv_insadd_13_arg;
                DATA_req campsrv_insdel_13_arg;
                INS_LOCK_req campsrv_inslock_13_arg;
                INS_LINE_req campsrv_insline_13_arg;
                INS_FILE_req campsrv_insload_13_arg;
                INS_FILE_req campsrv_inssave_13_arg;
                INS_IF_req campsrv_insifset_13_arg;
                INS_READ_req campsrv_insifread_13_arg;
                INS_WRITE_req campsrv_insifwrite_13_arg;
                DATA_req campsrv_insifon_13_arg;
                DATA_req campsrv_insifoff_13_arg;
                DATA_SET_req campsrv_varset_13_arg;
                DATA_POLL_req campsrv_varpoll_13_arg;
                DATA_ALARM_req campsrv_varalarm_13_arg;
                DATA_LOG_req campsrv_varlog_13_arg;
                DATA_req campsrv_varzero_13_arg;
                DATA_SET_req campsrv_vardoset_13_arg;
                DATA_req campsrv_varread_13_arg;
                DATA_SET_req campsrv_varlnkset_13_arg;
                DATA_GET_req campsrv_varget_13_arg;
                CMD_req campsrv_cmd_13_arg;
} argument_t;

#ifdef MULTITHREADED

/*
 *  Possible thread states
 */
#define ST_INIT 0
#define ST_RUNNING 1
#define ST_FINISHED 2

typedef struct {
    pthread_t thread_handle;
    pthread_mutex_t mutex_handle;
    int state;                 /* ST_INIT, ST_RUNNING or ST_FINISHED */
    int type;                  /* MAIN_THREAD_TYPE, SVC_THREAD_TYPE or POLL_THREAD_TYPE */
    struct svc_req* rqstp;     /* need this to call camp_srv_proc's from
                                  camp_tcl (many) and camp_cfg_write (campsrv_inssave_13()) */
    SVCXPRT* transp;
    argument_t argument;
/*
    caddr_t pResult;
    bool_t (*xdr_result)();
    bool_t (*xdr_argument)();
    char *(*local)();
*/
    u_long xdr_flag;           /* how to xdr for this thread */
    char ident[LEN_IDENT+1];   /* instrument ident */
    char* msg;                 /* allocate dynamically */
    REQ* pReq;                 /* for poll threads */
    int served;                /* for svc threads */
    int reply_sent;            /* for svc threads */
    timeval_t tv_start;        /* for svc threads */
    Tcl_Interp* interp;        /* separate Tcl interpreter for thread */
} THREAD_DATA;

#define NUM_THREADS 13
#define MAX_NUM_POLL_THREADS (NUM_THREADS-3)   /* Guarantee 2 SVC threads */
static THREAD_DATA thread_data[NUM_THREADS];

#define MAIN_THREAD_TYPE 0 
#define SVC_THREAD_TYPE 1
#define POLL_THREAD_TYPE 2 

static pthread_key_t thread_key;

static pthread_mutex_t free_thread_mutex;
static pthread_cond_t  free_thread_cv;
static int             free_thread;

static pthread_mutex_t global_mutex;
static int             global_mutex_noChange;

static pthread_mutex_t shutdown_mutex;

void thread_key_destructor( THREAD_DATA* pThread_data );
static void do_cleanup( int index, int final_state );
static void do_cleanup2( THREAD_DATA* pThread_data, int final_state );
static int find_free_thread( int* index, int type );
pthread_addr_t svc_thread( pthread_addr_t arg );
pthread_addr_t poll_thread( pthread_addr_t arg );
static void start_svc_thread( struct svc_req *rqstp, SVCXPRT *transp );
static void do_service( struct svc_req *rqstp, SVCXPRT *transp );

#endif /* MULTITHREADED */

/*
 *  Used for multi- and single-threaded versions
 */
static int             shutdown_flag = FALSE;
void srv_catch_timeout( void );
void srv_send_timeout_reply( struct svc_req *rqstp, SVCXPRT* transp );


int
camp_thread_mutex_lock( pthread_mutex_t* mutex )
{
  int s; 

  while( ( s = pthread_mutex_trylock( mutex ) ) != 1 ) 
    {
      if( s == -1 ) return( -1 );
      srv_check_timeout();
      camp_fsleep( 0.01 );
    }
  return( 0 );
}


void
thread_lock_global_np( void )
{
#ifdef MULTITHREADED
    if( global_mutex_noChange ) return;
    camp_thread_mutex_lock( &global_mutex );
#endif /* MULTITHREADED */
}


void
thread_unlock_global_np( void )
{
#ifdef MULTITHREADED
    if( global_mutex_noChange ) return;
    pthread_mutex_unlock( &global_mutex );
#endif /* MULTITHREADED */
}


void
set_global_mutex_noChange( int val )
{
    global_mutex_noChange = val;
}


struct svc_req*
get_current_rqstp( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( NULL );
    }

    return( pThread_data->rqstp );
#else
    return( current_rqstp );
#endif /* MULTITHREADED */
}

void
set_current_rqstp( struct svc_req* rqstp )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    pThread_data->rqstp = rqstp;
#else
    current_rqstp = rqstp;
#endif /* MULTITHREADED */
}


timeval_t*
get_tv_start( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( NULL );
    }

    return( &pThread_data->tv_start );
#else
    return( &tv_start );
#endif /* MULTITHREADED */
}

void
set_tv_start( timeval_t* ptv )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    copytimeval( ptv, &pThread_data->tv_start );
#else
    copytimeval( ptv, &tv_start );
#endif /* MULTITHREADED */
}


SVCXPRT*
get_current_transp( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( NULL );
    }

    return( pThread_data->transp );
#else
    return( current_transp );
#endif /* MULTITHREADED */
}

void
set_current_transp( SVCXPRT* transp )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    pThread_data->transp = transp;
#else
    current_transp = transp;
#endif /* MULTITHREADED */
}


int
get_reply_sent( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( NULL );
    }

    return( pThread_data->reply_sent );
#else
    return( reply_sent );
#endif /* MULTITHREADED */
}

void
set_reply_sent( int value )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    pThread_data->reply_sent = value;
#else
    reply_sent = value;
#endif /* MULTITHREADED */
}


int
check_shutdown( void )
{
    int shutdown;

#ifdef MULTITHREADED
    camp_thread_mutex_lock( &shutdown_mutex );
#endif /* MULTITHREADED */

    shutdown = shutdown_flag;

#ifdef MULTITHREADED
    pthread_mutex_unlock( &shutdown_mutex );
#endif /* MULTITHREADED */

    return( shutdown );
}


void
set_shutdown( void )
{
#ifdef MULTITHREADED
    camp_thread_mutex_lock( &shutdown_mutex );
#endif /* MULTITHREADED */

    shutdown_flag = TRUE;

#ifdef MULTITHREADED
    pthread_mutex_unlock( &shutdown_mutex );
#endif /* MULTITHREADED */
}


Tcl_Interp*
get_thread_interp( void )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( NULL );
    }

    return( pThread_data->interp );
#else
    return( camp_tclInterp() );
#endif /* MULTITHREADED */
}


void
set_thread_interp( Tcl_Interp* interp )
{
#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    pThread_data->interp = interp;
#endif /* MULTITHREADED */
}


#ifdef MULTITHREADED

void
thread_key_destructor( THREAD_DATA* pThread_data )
{
}


u_long
get_thread_xdr_flag( void )
{
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( 0 );
    }

    return( pThread_data->xdr_flag );
}


void
set_thread_xdr_flag( u_long flag )
{
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    pThread_data->xdr_flag = flag;
}


char*
get_thread_ident( void )
{
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( NULL );
    }

    return( pThread_data->ident );
}


void
set_thread_ident( char* ident )
{
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    strcpy( pThread_data->ident, ident );
}


char*
get_thread_msg( void )
{
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return( NULL );
    }

    return( pThread_data->msg );
}


void
set_thread_msg( char* msg )
{
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    strcpy( pThread_data->msg, msg );
}


static void
do_cleanup( int index, int final_state )
{
    do_cleanup2( &thread_data[index], final_state );
}


static void
do_cleanup2( THREAD_DATA* pThread_data, int final_state )
{
    camp_thread_mutex_lock( &free_thread_mutex );

    camp_thread_mutex_lock( &pThread_data->mutex_handle );
    pThread_data->state = final_state;
    pThread_data->ident[0] = '\0';
    _free( pThread_data->msg );
    pThread_data->xdr_flag = CAMP_XDR_ALL;
    pThread_data->interp = NULL;
    pthread_mutex_unlock( &pThread_data->mutex_handle );

    free_thread = TRUE;
    pthread_cond_signal( &free_thread_cv );
    pthread_mutex_unlock( &free_thread_mutex );
}


static int
find_free_thread( int* index, int type )
{
    int i;
    int found;
    int retry = FALSE;

    do 
    {
        /*
         *  This mutex is safe, can't get stuck
         *  (used here and in do_cleanup())
         */
        camp_thread_mutex_lock( &free_thread_mutex );

        /*
         *  The thread mutexes are only locked for
         *  short times, and not outside of a global lock
         *  therefore can't get stuck
         */
        for( i = 0, found = FALSE; i < NUM_THREADS; i++ )
        {
            camp_thread_mutex_lock( &thread_data[i].mutex_handle );
            found = ( thread_data[i].state != ST_RUNNING );
            pthread_mutex_unlock( &thread_data[i].mutex_handle );

            if( found ) break;
        }

        if( found ) 
        {
            retry = FALSE;
        }
        else
        {
            /* 
             *  Keep retrying forever
             *  free_thread gets set TRUE in do_cleanup()
             *  (i.e., when any thread finishes)
             */
            retry = TRUE;
            free_thread = FALSE;
            while( !free_thread ) 
            {
                /* 
                 *  This frees free_thread_mutex and waits for
                 *  do_cleanup to signal free_thread_cv
                 */
                pthread_cond_wait( &free_thread_cv,
                                   &free_thread_mutex );
            }
        }

        /*
         *  Must unlock (come out of pthread_cond_wait with
         *  free_thread_mutex locked)
         */
        pthread_mutex_unlock( &free_thread_mutex );

    } while( retry );

    /*
     *  found must be TRUE here, in this implementation
     */
    if( found )
    {
        /*
         *  Reclaim internal storage for old thread
         */
/* This is done upon creation
        if( pthread_detach( &(thread_data[i].thread_handle) ) != 0 )
        {
            camp_logMsg( "error detaching thread" );
        }
*/
        *index = i;

        camp_thread_mutex_lock( &thread_data[i].mutex_handle );
        /*
         *  Allocate status message string
         */
        thread_data[i].msg = (char*)zalloc( MAX_LEN_MSG+1 );
        thread_data[i].type = type;
        /*
         *  Use main Tcl interpreter by default
         */
        thread_data[i].interp = camp_tclInterp();
        pthread_mutex_unlock( &thread_data[i].mutex_handle );
    }

    return( found );
}


void
kill_all_threads( void )
{
    int i;
    bool_t running;

    /*
     *  Wait for threads to complete
     *  (doesn't actually cancel or join, pthread_join
     *  wasn't working, I don't know why).
     */
    for( i = 1; i < NUM_THREADS; i++ )
    {
      for( running = TRUE; running; )
	{
	  camp_thread_mutex_lock( &thread_data[i].mutex_handle );
	  if( thread_data[i].state != ST_RUNNING ) 
	    {
	      running = FALSE;
	    }
	  pthread_mutex_unlock( &thread_data[i].mutex_handle );
	}
    }

    for( i = 1; i < NUM_THREADS; i++ )
    {
	  camp_thread_mutex_lock( &thread_data[i].mutex_handle );
	  if( thread_data[i].state == ST_RUNNING ) 
	    {
	      printf( "error: thread %d is still running\n", i );
	    }
	  pthread_mutex_unlock( &thread_data[i].mutex_handle );
    }
}

int
srv_init_main_thread( void )
{
    int i;
    int priority;

    if( !find_free_thread( &i, MAIN_THREAD_TYPE ) )
    {
        camp_logMsg( "error finding a free main thread" );
        return( CAMP_FAILURE );
    }

    if( pthread_setspecific( thread_key, 
                             (pthread_addr_t)(&(thread_data[i])) ) != 0 )
    {
        camp_logMsg( "error setting main thread key" );
        return( CAMP_FAILURE );
    }

    if( camp_thread_mutex_lock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error locking main thread mutex" );
        return( CAMP_FAILURE );
    }

    thread_data[i].state = ST_RUNNING;

    if( pthread_mutex_unlock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error unlocking main thread mutex" );
        return( CAMP_FAILURE );
    }

    /*
     *  The main thread is now effectively just another thread
     *  Set the scheduling policy and priority 
     *  explicitely to the defaults.
     */
/*
    ERROR using this - I guess pthread_self() isn't working for the 
    main thread.  Oh well, I just wanted default behaviour anyway.

    priority = (PRI_FG_MAX_NP+PRI_FG_MIN_NP)/2;

    if( pthread_setscheduler( pthread_self(), SCHED_FG_NP, priority ) != 0 )
    {
        camp_logMsg( "error setting scheduling" );
        return( CAMP_FAILURE );
    }
*/

    return( CAMP_SUCCESS );
}


void
srv_init_thread_once( void )
{
    int i;

#ifdef VMS
    /*
     *  Set reentrancy behaviour properly for VMS
     */
#if (defined(__DECC)||defined(__DECCXX)) && defined(MULTITHREADED)
#include <reentrancy.h>
    DECC$SET_REENTRANCY( C$C_MULTITHREAD );
#endif /* __DECC && MULTITHREADED */
#endif /* VMS */

    /*
     *  Initialize thread data
     *  Thread data must be static so that
     *  find_free_thread methodology works
     *  (and also the shutdown methodology).
     */
    for( i = 0; i < NUM_THREADS; i++ )
    {
        if( pthread_mutex_init( &thread_data[i].mutex_handle,
                                pthread_mutexattr_default ) != 0 )
        {
            fprintf( stderr, "error initializing thread %d mutex\n", i );
            exit( 0 );
        }
        thread_data[i].state = ST_INIT;
        thread_data[i].ident[0] = '\0';
        thread_data[i].msg = NULL;
        thread_data[i].xdr_flag = CAMP_XDR_ALL;
    }

    /*
     *  Free thread mutex & condition value
     */
    if( pthread_mutex_init( &free_thread_mutex, pthread_mutexattr_default ) 
        != 0 )
    {
        fprintf( stderr, "error initializing free_thread_mutex\n" );
        exit( 0 );
    }

    if( pthread_cond_init( &free_thread_cv, pthread_condattr_default ) != 0 )
    {
        fprintf( stderr, "error initializing free_thread_cv\n" );
        exit( 0 );
    }

    /*
     *  Initialize global mutex
     */
    if( pthread_mutex_init( &global_mutex, pthread_mutexattr_default ) 
        != 0 )
    {
        fprintf( stderr, "error initializing global_mutex\n" );
        exit( 0 );
    }
    global_mutex_noChange = FALSE;

    /*
     *  Shutdown mutex & flag
     */
    if( pthread_mutex_init( &shutdown_mutex, pthread_mutexattr_default ) 
        != 0 )
    {
        fprintf( stderr, "error initializing shutdown_mutex\n" );
        exit( 0 );
    }
    shutdown_flag = FALSE;

    if( pthread_keycreate( &thread_key, thread_key_destructor ) != 0 )
    {
        fprintf( stderr, "error creating thread key\n" );
        exit( 0 );
    }

    /*
     *  In VxWorks, set scheduling policy once 
     *  for all tasks.
     *  I'm just guessing on a value here.
     */
#ifdef VXWORKS
    kernelTimeSlice( sysClkRateGet()*0.01 );  /* 0.01 seconds */
#endif /* VXWORKS */
}


/*
 *  srv_end_thread  -  clean up all thread related storage
 *  
 *  Called from main thread before program exits.
 */
void
srv_end_thread( void )
{
    int i;

    if( pthread_mutex_destroy( &free_thread_mutex ) != 0 )
    {
        fprintf( stderr, "failed deleting free_thread_mutex\n" );
    }

    if( pthread_cond_destroy( &free_thread_cv ) != 0 )
    {
        fprintf( stderr, "failed deleting free_thread_cv\n" );
    }

    if( pthread_mutex_destroy( &global_mutex ) != 0 )
    {
        fprintf( stderr, "failed deleting global_mutex\n" );
    }

    if( pthread_mutex_destroy( &shutdown_mutex ) != 0 )
    {
        fprintf( stderr, "failed deleting shutdown_mutex\n" );
    }

    for( i = 0; i < NUM_THREADS; i++ )
    {
        if( pthread_mutex_destroy( &thread_data[i].mutex_handle ) != 0 )
        {
            fprintf( stderr, "failed deleting thread %d mutex\n", i );
	}

        _free( thread_data[i].msg );
    }
}


void
start_poll_thread( REQ* pReq )
{
    int i;
    pthread_attr_t thread_attr;

    /* 
     *  Allocate thread index for thread
     *  Note that the global lock is on here, so we'd
     *  better unlock it in case there are no free threads
     */
    if( !find_free_thread( &i, POLL_THREAD_TYPE ) )
    {
        camp_logMsg( "error finding a free poll thread" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "found free poll thread" );
    }

    if( camp_thread_mutex_lock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error locking poll thread mutex" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "locked poll thread" );
    }

    thread_data[i].pReq = pReq;
    thread_data[i].state = ST_RUNNING;

    if( pthread_mutex_unlock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error unlocking poll thread mutex" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "unlocked poll thread" );
    }

    if( pthread_attr_create( &thread_attr ) != 0 )
    {
        camp_logMsg( "error creating poll thread attributes" );
        return;
    }

    /*
     *  Explicitely set to inherit scheduling
     *  policy and priority from creating thread
     */
    if( pthread_attr_setinheritsched( &thread_attr, 
                                      PTHREAD_INHERIT_SCHED ) != 0 )
    {
        camp_logMsg( "error setting poll thread attributes" );
        return;
    }

    if( pthread_create( &(thread_data[i].thread_handle), 
                        /* pthread_attr_default, */ thread_attr,
                        poll_thread, 
                        (pthread_addr_t)i ) != 0 )
    {
        camp_logMsg( "error creating poll thread" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "created poll thread" );
    }

    /*
     *  from DECthreads manual: "Threads that were created
     *  using this thread attributes object are not affected 
     *  by the deletion of the thread attributes object."
     */
    if( pthread_attr_delete( &thread_attr ) != 0 )
    {
        camp_logMsg( "error deleting poll thread attributes" );
        return;
    }

    /*
     *  Reclaim internal storage for thread
     *  immediately upon thread exit
     */
    if( pthread_detach( &(thread_data[i].thread_handle) ) != 0 )
    {
        camp_logMsg( "error detaching poll thread" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "detached poll thread" );
    }
}


pthread_addr_t
poll_thread( pthread_addr_t arg )
{
    int i = (int)arg;
    CAMP_VAR* pInsVar;
    int status;
    REQ* pReq;

    if( pthread_setspecific( thread_key, 
                             (pthread_addr_t)(&(thread_data[i])) ) != 0 )
    {
    	camp_logMsg( "poll: error setting poll thread key data" );
    }

/*  Tried using this to stop killing threads
    that you want to complete before exiting.
    Now do it another way.

    pthread_setcancel( CANCEL_OFF );
*/

    /*
     *  Bug here - what if one poll request node (in the
     *             linked list of requests) has two threads 
     *             pending (due to some hold up)
     *             and the first cancels the request, then
     *             the second will point to nothing -> CRASH
     *  Fixed -    have only one thread allowed per request
     */

    thread_lock_global_np();

/*
    printf( "poll thread %d: started\n", i );
*/

    pReq = thread_data[i].pReq;
    pInsVar = varGetpIns( pReq->pVar );

    get_ins_thread_lock( pInsVar );

    status = (*pReq->procs.retProc)( pReq );

    release_ins_thread_lock( pInsVar );

/*
    printf( "poll thread %d: done\n", i );
*/

    thread_unlock_global_np();

    do_cleanup( i, ST_FINISHED );

/*
    pthread_setcancel( CANCEL_ON );
    pthread_testcancel();
*/
/*
    pthread_exit( arg );
*/
    /*
     *  Return a value, this program doesn't check it.
     */
    return( (pthread_addr_t)arg );
}


/*
 *  start_svc_thread  -  start an svc (service) thread
 *                       from the main thread
 *
 *  Note: global lock is on around this call
 */
static void
start_svc_thread( struct svc_req *rqstp, SVCXPRT *transp )
{
    int i;
    pthread_attr_t thread_attr;
    struct authunix_parms* aupp;
    bool_t (*xdr_argument)(), (*xdr_result)();
    char *(*local)();

    switch (rqstp->rq_proc) {
        case NULLPROC:
                set_reply_sent( TRUE );
                svc_sendreply(transp, xdr_void, NULL);
                return;

        case CAMPSRV_SYSRUNDOWN:
                xdr_argument = xdr_void;
                break;

        case CAMPSRV_SYSUPDATE:
                xdr_argument = xdr_void;
                break;

        case CAMPSRV_SYSLOAD:
                xdr_argument = xdr_FILE_req;
                break;

        case CAMPSRV_SYSSAVE:
                xdr_argument = xdr_FILE_req;
                break;

        case CAMPSRV_SYSGET:
                xdr_argument = xdr_void;
                break;

        case CAMPSRV_SYSGETDYNA:
                xdr_argument = xdr_void;
                break;

        case CAMPSRV_SYSDIR:
                xdr_argument = xdr_FILE_req;
                break;

        case CAMPSRV_INSADD:
                xdr_argument = xdr_INS_ADD_req;
                break;

        case CAMPSRV_INSDEL:
                xdr_argument = xdr_DATA_req;
                break;

        case CAMPSRV_INSLOCK:
                xdr_argument = xdr_INS_LOCK_req;
                break;

        case CAMPSRV_INSLINE:
                xdr_argument = xdr_INS_LINE_req;
                break;

        case CAMPSRV_INSLOAD:
                xdr_argument = xdr_INS_FILE_req;
                break;

        case CAMPSRV_INSSAVE:
                xdr_argument = xdr_INS_FILE_req;
                break;

        case CAMPSRV_INSIFSET:
                xdr_argument = xdr_INS_IF_req;
                break;

        case CAMPSRV_INSIFREAD:
                xdr_argument = xdr_INS_READ_req;
                break;

        case CAMPSRV_INSIFWRITE:
                xdr_argument = xdr_INS_WRITE_req;
                break;

        case CAMPSRV_INSIFON:
                xdr_argument = xdr_DATA_req;
                break;

        case CAMPSRV_INSIFOFF:
                xdr_argument = xdr_DATA_req;
                break;

        case CAMPSRV_VARSET:
                xdr_argument = xdr_DATA_SET_req;
                break;

        case CAMPSRV_VARPOLL:
                xdr_argument = xdr_DATA_POLL_req;
                break;

        case CAMPSRV_VARALARM:
                xdr_argument = xdr_DATA_ALARM_req;
                break;

        case CAMPSRV_VARLOG:
                xdr_argument = xdr_DATA_LOG_req;
                break;

        case CAMPSRV_VARZERO:
                xdr_argument = xdr_DATA_req;
                break;

        case CAMPSRV_VARDOSET:
                xdr_argument = xdr_DATA_SET_req;
                break;

        case CAMPSRV_VARREAD:
                xdr_argument = xdr_DATA_req;
                break;

        case CAMPSRV_VARLNKSET:
                xdr_argument = xdr_DATA_SET_req;
                break;

        case CAMPSRV_VARGET:
                xdr_argument = xdr_DATA_GET_req;
                break;

        case CAMPSRV_CMD:
                xdr_argument = xdr_CMD_req;
                break;

        default:
                svcerr_noproc(transp);
                return;
    }

    /* 
     *  Allocate thread index for thread
     */
    if( !find_free_thread( &i, SVC_THREAD_TYPE ) )
    {
        camp_logMsg( "error finding a free svc thread" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "found free svc thread" );
    }

    if( camp_thread_mutex_lock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error locking svc thread mutex" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "locked svc thread" );
    }

    thread_data[i].served = FALSE;
    thread_data[i].state = ST_RUNNING;
    thread_data[i].transp = transp;
    set_reply_sent( FALSE );

    /*
     *  Make a copy of rqstp
     */
    thread_data[i].rqstp = (struct svc_req*)zalloc( sizeof( struct svc_req ) );
    bcopy( rqstp, thread_data[i].rqstp, sizeof( struct svc_req ) );
    thread_data[i].rqstp->rq_clntcred = 
        (caddr_t)zalloc( sizeof( struct authunix_parms ) );
    bcopy( rqstp->rq_clntcred, thread_data[i].rqstp->rq_clntcred,
        sizeof( struct authunix_parms ) );
    aupp = (struct authunix_parms*)thread_data[i].rqstp->rq_clntcred;
    aupp->aup_machname = strdup( 
        ((struct authunix_parms*)rqstp->rq_clntcred)->aup_machname );
    aupp->aup_gids = (int*)zalloc( (aupp->aup_len)*sizeof( int ) );
    bcopy( ((struct authunix_parms*)rqstp->rq_clntcred)->aup_gids,
           aupp->aup_gids, 
           (aupp->aup_len)*sizeof( int ) );

    bzero( &(thread_data[i].argument), sizeof( argument_t ) );

    set_current_xdr_flag( CAMP_XDR_ALL );

    /* 
     *  Get args
     *  RPC library is non-reentrant, must be inside global lock
     *  
     *  Call svc_getargs() from the main thread (svc listener) only.
     *  Under VxWorks, calling svc_getargs() from a separate
     *  task doesn't work (doesn't inherit RPC server capabilities).
     *  Luckily, svc_sendreply does work from other threads or tasks.
     */

/* Routine called from within global lock
    thread_lock_global_np();
*/

    if (!svc_getargs(transp, xdr_argument, &(thread_data[i].argument) )) {
        svcerr_decode(transp);
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "got svc thread arguments" );
    }

/*  Not needed here
    thread_unlock_global_np();
*/

    if( pthread_mutex_unlock( &(thread_data[i].mutex_handle) ) != 0 )
    {
        camp_logMsg( "error unlocking svc thread mutex" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "unlocked svc thread" );
    }

    if( pthread_attr_create( &thread_attr ) != 0 )
    {
        camp_logMsg( "error creating svc thread attributes" );
        return;
    }

    /*
     *  Explicitely set to inherit scheduling
     *  policy and priority from creating thread
     */
    if( pthread_attr_setinheritsched( &thread_attr, 
                                      PTHREAD_INHERIT_SCHED ) != 0 )
    {
        camp_logMsg( "error setting svc thread attributes" );
        return;
    }

    if( pthread_create( &(thread_data[i].thread_handle), 
                        /* pthread_attr_default, */ thread_attr,
                        svc_thread, 
                        (pthread_addr_t)i ) != 0 )
    {
        camp_logMsg( "error creating svc thread" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "created svc thread" );
    }

    /*
     *  from DECthreads manual: "Threads that were created
     *  using this thread attributes object are not affected 
     *  by the deletion of the thread attributes object."
     */
    if( pthread_attr_delete( &thread_attr ) != 0 )
    {
        camp_logMsg( "error deleting svc thread attributes" );
        return;
    }

    /*
     *  Reclaim internal storage for thread
     *  immediately upon thread exit
     */
    if( pthread_detach( &(thread_data[i].thread_handle) ) != 0 )
    {
        camp_logMsg( "error detaching thread" );
        return;
    }
    else if( camp_debug > 1 )
    {
        camp_logMsg( "detached svc thread" );
    }
}


/*
 *  svc_thread  -  main routine of a service thread
 *
 */
pthread_addr_t
svc_thread( pthread_addr_t arg )
{
    int i = (int)arg;
    struct authunix_parms* aupp;

    if( pthread_setspecific( thread_key, 
                             (pthread_addr_t)(&(thread_data[i])) ) != 0 )
    {
    	camp_logMsg( "error setting thread key data" );
        return( (pthread_addr_t)(-1) );
    }

#ifdef VXWORKS
    rpcTaskInit();    /* must be done for every task that uses RPCs */
#endif /* VXWORKS */

/*
    pthread_setcancel( CANCEL_OFF );
*/

    thread_lock_global_np();

/*
    printf( "svc  thread %d: starting\n", i );
*/

    do_service( thread_data[i].rqstp, thread_data[i].transp );

    /*
     *  Free up RPC request storage allocated in start_svc_thread
     */
    aupp = (struct authunix_parms*)thread_data[i].rqstp->rq_clntcred;
    free( aupp->aup_machname );
    free( aupp->aup_gids );
    free( aupp );
    free( thread_data[i].rqstp );

/*
    printf( "svc  thread %d: done\n", i );
*/

    thread_unlock_global_np();

    do_cleanup( i, ST_FINISHED );

/*
    pthread_setcancel( CANCEL_ON );
    pthread_testcancel();
*/
/*
    pthread_exit( arg );
*/
    /*
     *  Return a value, this program doesn't check it.
     */
    return( (pthread_addr_t)arg );
}

#endif /* MULTITHREADED */


#ifdef MULTITHREADED
void
camp_srv_13( struct svc_req *rqstp, SVCXPRT *transp )
{
    start_svc_thread( rqstp, transp );
}

static void
do_service( struct svc_req *rqstp, SVCXPRT *transp )
#else /* !MULTITHREADED */
void
camp_srv_13( struct svc_req *rqstp, SVCXPRT *transp )
#endif /* MULTITHREADED */
{
    argument_t argument;
    char *result;
    bool_t (*xdr_argument)(), (*xdr_result)();
    char *(*local)();
    THREAD_DATA* pThread_data;
    argument_t* pArgument;
    bool_t lost_client;
    SVCXPRT trans_orig;
    timeval_t tv;

    /*
     *  Set these 'global' variables so that 
     *  other routines may access it.
     *  These variables are global for non-multithreading
     *  and part of the thread_data structure for 
     *  multithreading.
     */
    gettimeval( &tv );
    set_tv_start( &tv );
    set_current_rqstp( rqstp );
    set_current_transp( transp );
    set_reply_sent( FALSE );
    camp_setMsg( "" );

    switch (rqstp->rq_proc) {
        case NULLPROC:
                set_reply_sent( TRUE );
                svc_sendreply(transp, xdr_void, NULL);
                return;

        case CAMPSRV_SYSRUNDOWN:
                xdr_argument = xdr_void;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_sysrundown_13;
                break;

        case CAMPSRV_SYSUPDATE:
                xdr_argument = xdr_void;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_sysupdate_13;
                break;

        case CAMPSRV_SYSLOAD:
                xdr_argument = xdr_FILE_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_sysload_13;
                break;

        case CAMPSRV_SYSSAVE:
                xdr_argument = xdr_FILE_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_syssave_13;
                break;

        case CAMPSRV_SYSGET:
                xdr_argument = xdr_void;
                xdr_result = xdr_CAMP_SYS_res;
                local = (char *(*)()) campsrv_sysget_13;
                break;

        case CAMPSRV_SYSGETDYNA:
                xdr_argument = xdr_void;
                xdr_result = xdr_SYS_DYNAMIC_res;
                local = (char *(*)()) campsrv_sysgetdyna_13;
                break;

        case CAMPSRV_SYSDIR:
                xdr_argument = xdr_FILE_req;
                xdr_result = xdr_DIR_res;
                local = (char *(*)()) campsrv_sysdir_13;
                break;

        case CAMPSRV_INSADD:
                xdr_argument = xdr_INS_ADD_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insadd_13;
                break;

        case CAMPSRV_INSDEL:
                xdr_argument = xdr_DATA_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insdel_13;
                break;

        case CAMPSRV_INSLOCK:
                xdr_argument = xdr_INS_LOCK_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_inslock_13;
                break;

        case CAMPSRV_INSLINE:
                xdr_argument = xdr_INS_LINE_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insline_13;
                break;

        case CAMPSRV_INSLOAD:
                xdr_argument = xdr_INS_FILE_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insload_13;
                break;

        case CAMPSRV_INSSAVE:
                xdr_argument = xdr_INS_FILE_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_inssave_13;
                break;

        case CAMPSRV_INSIFSET:
                xdr_argument = xdr_INS_IF_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insifset_13;
                break;

        case CAMPSRV_INSIFREAD:
                xdr_argument = xdr_INS_READ_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insifread_13;
                break;

        case CAMPSRV_INSIFWRITE:
                xdr_argument = xdr_INS_WRITE_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insifwrite_13;
                break;

        case CAMPSRV_INSIFON:
                xdr_argument = xdr_DATA_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insifon_13;
                break;

        case CAMPSRV_INSIFOFF:
                xdr_argument = xdr_DATA_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_insifoff_13;
                break;

        case CAMPSRV_VARSET:
                xdr_argument = xdr_DATA_SET_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_varset_13;
                break;

        case CAMPSRV_VARPOLL:
                xdr_argument = xdr_DATA_POLL_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_varpoll_13;
                break;

        case CAMPSRV_VARALARM:
                xdr_argument = xdr_DATA_ALARM_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_varalarm_13;
                break;

        case CAMPSRV_VARLOG:
                xdr_argument = xdr_DATA_LOG_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_varlog_13;
                break;

        case CAMPSRV_VARZERO:
                xdr_argument = xdr_DATA_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_varzero_13;
                break;

        case CAMPSRV_VARDOSET:
                xdr_argument = xdr_DATA_SET_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_vardoset_13;
                break;

        case CAMPSRV_VARREAD:
                xdr_argument = xdr_DATA_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_varread_13;
                break;

        case CAMPSRV_VARLNKSET:
                xdr_argument = xdr_DATA_SET_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_varlnkset_13;
                break;

        case CAMPSRV_VARGET:
                xdr_argument = xdr_DATA_GET_req;
                xdr_result = xdr_CAMP_VAR_res;
                local = (char *(*)()) campsrv_varget_13;
                break;

        case CAMPSRV_CMD:
                xdr_argument = xdr_CMD_req;
                xdr_result = xdr_RES;
                local = (char *(*)()) campsrv_cmd_13;
                break;

        default:
                svcerr_noproc(transp);
                return;
        }

#ifdef MULTITHREADED
    pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data );
    pArgument = &(pThread_data->argument);
#else /* !MULTITHREADED */
    pArgument = &argument;
    bzero( &argument, sizeof( argument_t ) );

    set_current_xdr_flag( CAMP_XDR_ALL );

    /* 
     *  Get args
     *  We are already in a global lock to ensure that
     *  this is the only thread calling this routine.
     */
    if (!svc_getargs(transp, xdr_argument, &argument)) {
        svcerr_decode(transp);
        return;
    }
#endif /* MULTITHREADED */

    /*
     *  Use this for testing whether client
     *  has timed out.  This is not a good enough
     *  check, but sometimes works.  I think that
     *  it works unless another client comes in and
     *  starts using the same transp structure that
     *  the timed out client had been using.  Then,
     *  the server goes to reply to the wrong client
     *  request.
     */ 
    lost_client = FALSE;
    bcopy( transp, &trans_orig, sizeof( SVCXPRT ) );

    if( camp_debug > 1 )
      {
	printf( "before processing request\n" );
        printf( "transp             = %p\n", transp );
        printf( "transp->xp_sock    = %d\n", transp->xp_sock );
        printf( "transp->xp_port    = %d\n", transp->xp_port );
        printf( "transp->xp_ops     = %p\n", transp->xp_ops );
        printf( "transp->xp_addrlen = %d\n", transp->xp_addrlen );
	printf( "transp->xp_raddr.sin_family = %d\n", transp->xp_raddr.sin_family );
	printf( "transp->xp_raddr.sin_port   = %d\n", transp->xp_raddr.sin_port );
	printf( "transp->xp_raddr.sin_addr.s_addr = %d\n", transp->xp_raddr.sin_addr.s_addr );
        printf( "transp->xp_p1      = %p\n", transp->xp_p1 );
	printf( "svc_stat( transp ) = %d\n", svc_stat( transp ) );
      }

    /*
     *  Process the request
     */
    result = (*local)( pArgument, rqstp );

    if( result != NULL ) 
    {
        ((RES*)result)->msg = camp_getMsg();
        if( _failure( ((RES*)result)->status ) )
        {
            camp_logMsg( ((RES*)result)->msg );
        }
    }

    /*
     *  How do I know if the client connection is lost?
     *  (i.e., if client timed out)
     *  Comparing the transport xp_sock seems to work every 
     *  time for VxWorks, but others might be OK too (like sin_port).
     *
     *  If the client is lost, then will not calling svc_sendreply
     *  and svc_freeargs cause some storage to not be freed
     *  properly.
     */
    if( bcmp( transp, &trans_orig, sizeof( SVCXPRT) ) != 0 ) lost_client = TRUE;

    if( camp_debug > 1 )
      {
	printf( "before svc_sendreply\n" );
        printf( "transp             = %p\n", transp );
        printf( "transp->xp_sock    = %d\n", transp->xp_sock );
        printf( "transp->xp_port    = %d\n", transp->xp_port );
        printf( "transp->xp_ops     = %p\n", transp->xp_ops );
        printf( "transp->xp_addrlen = %d\n", transp->xp_addrlen );
	printf( "transp->xp_raddr.sin_family = %d\n", transp->xp_raddr.sin_family );
	printf( "transp->xp_raddr.sin_port   = %d\n", transp->xp_raddr.sin_port );
	printf( "transp->xp_raddr.sin_addr.s_addr = %d\n", transp->xp_raddr.sin_addr.s_addr );
        printf( "transp->xp_p1      = %p\n", transp->xp_p1 );
	if( !lost_client )
	  {
	    printf( "svc_stat( transp ) = %d\n", svc_stat( transp ) );
	  }
      }

    if( lost_client )
      {
	camp_logMsg( "lost client connection, not sending reply" );
	if( camp_debug > 1 )
	  {
	    printf( "lost client connection, not sending reply\n" );
	  }
      }
    else
      {
	if( get_reply_sent() )
	  {
	    camp_logMsg( "already sent reply, not sending reply" );
	    if( camp_debug > 1 )
	      {
		printf( "already sent reply, not sending reply\n" );
	      }
	  }
	else
	  {
	    set_reply_sent( TRUE );
	    /* 
	     *  Send reply
	     */
	    if( ( result != NULL ) && !svc_sendreply( transp, xdr_result, result ) )
	      {
		if( camp_debug > 1 )
		  {
		    printf( "trouble replying to client\n" );
		  }
		
		svcerr_systemerr(transp);
	      }
	    
	    if( camp_debug > 1 )
	      {
		printf( "after svc_sendreply\n" );
	      }
	  }
      }

    _free( result );

    /* 
     *  Free args
     */
    if( !lost_client )
      {
	if( !svc_freeargs( transp, xdr_argument, pArgument ) ) 
	  {
	    camp_logMsg( "unable to free arguments" );
	  }
      }

    set_current_xdr_flag( CAMP_XDR_ALL );

    camp_setMsg( "" );
}


void
srv_check_timeout( void )
{
    static struct svc_req* rqstp;
    struct authunix_parms* aupp;
    int timeout;
    timeval_t tv_now;
    timeval_t *ptv_start;

#ifdef MULTITHREADED
    THREAD_DATA* pThread_data;

    if( pthread_getspecific( thread_key, (pthread_addr_t*)&pThread_data ) != 0 )
    {
        camp_logMsg( "error getting key value" );
        return;
    }

    /*
     *  This check is only for SVC (i.e., RPC service)
     *  type threads.
     */
    if( pThread_data->type != SVC_THREAD_TYPE ) return;
#else /* !MULTITHREADED */
    /*
     *  Not yet implemented for non-multithreading
     */
    return;
#endif /* MULTITHREADED */

    /*
     *  If reply already sent, don't need to check
     */
    if( get_reply_sent() ) return;

    rqstp = get_current_rqstp();
    aupp = (struct authunix_parms*)rqstp->rq_clntcred;
    timeout = aupp->aup_gids[2];

    ptv_start = get_tv_start();

    gettimeval( &tv_now );
    if( difftimeval( &tv_now, ptv_start ) > (double)timeout )
      {
	srv_catch_timeout();
      }
}


void
srv_catch_timeout( void )
{
    srv_send_timeout_reply( get_current_rqstp(), get_current_transp() );
}


void
srv_send_timeout_reply( struct svc_req *rqstp, SVCXPRT* transp )
{
    caddr_t pResult;
    RES res;
    CAMP_SYS_res sys_res;
    CAMP_VAR_res var_res;
    SYS_DYNAMIC_res sys_dynamic_res;
    DIR_res dir_res;
    bool_t (*xdr_result)();

    switch (rqstp->rq_proc) {
        case NULLPROC:
                xdr_result = xdr_void;
		pResult = NULL;
		break;

        case CAMPSRV_SYSGET:
                xdr_result = xdr_CAMP_SYS_res;
		pResult = (caddr_t)memset( &sys_res, 0, sizeof( sys_res ) );
                break;

        case CAMPSRV_SYSGETDYNA:
                xdr_result = xdr_SYS_DYNAMIC_res;
		pResult = (caddr_t)memset( &sys_dynamic_res, 0, sizeof( sys_dynamic_res ) );
                break;

        case CAMPSRV_SYSDIR:
                xdr_result = xdr_DIR_res;
		pResult = (caddr_t)memset( &dir_res, 0, sizeof( dir_res ) );
                break;

        case CAMPSRV_VARGET:
                xdr_result = xdr_CAMP_VAR_res;
		pResult = (caddr_t)memset( &var_res, 0, sizeof( var_res ) );
                break;

        case CAMPSRV_SYSRUNDOWN:
        case CAMPSRV_SYSUPDATE:
        case CAMPSRV_SYSLOAD:
        case CAMPSRV_SYSSAVE:
        case CAMPSRV_INSADD:
        case CAMPSRV_INSDEL:
        case CAMPSRV_INSLOCK:
        case CAMPSRV_INSLINE:
        case CAMPSRV_INSLOAD:
        case CAMPSRV_INSSAVE:
        case CAMPSRV_INSIFSET:
        case CAMPSRV_INSIFREAD:
        case CAMPSRV_INSIFWRITE:
        case CAMPSRV_INSIFON:
        case CAMPSRV_INSIFOFF:
        case CAMPSRV_VARSET:
        case CAMPSRV_VARPOLL:
        case CAMPSRV_VARALARM:
        case CAMPSRV_VARLOG:
        case CAMPSRV_VARZERO:
        case CAMPSRV_VARDOSET:
        case CAMPSRV_VARREAD:
        case CAMPSRV_VARLNKSET:
        case CAMPSRV_CMD:
                xdr_result = xdr_RES;
		pResult = (caddr_t)memset( &res, 0, sizeof( res ) );
                break;

        default:
                svcerr_noproc(transp);
                return;
        }

    ((RES*)pResult)->status = CAMP_SERVER_BUSY_TIMEOUT;
    ((RES*)pResult)->msg = "WARNING: CAMP server busy, will process request ASAP, return status unknown";
    set_reply_sent( TRUE );
    svc_sendreply( transp, xdr_result, pResult );
}



