/*
 *  camp_srv_utils.c - CAMP server utilities
 *
 *  Revision history:
 *   v1.2  17-May-1994  TW
 */

#include <string.h>
#include <stdio.h>
#include <math.h>
#ifdef VMS
#include <ssdef.h>
#endif /* VMS */
#include "camp_srv.h"

static int checkPidOk( char* host_in, int pid, char* os );


void 
srv_delAllInss( void )
{
    RES* pRes;
    DATA_req data_req;

    while( pVarList != NULL )
    {
        data_req.path = pVarList->core.path;
        pRes = campsrv_insdel_13( &data_req, NULL );
    	_free( pRes );
    }
}


int 
camp_checkInsLock( CAMP_VAR* pVar, struct svc_req* rq )
{
    int status;
    CAMP_VAR* pInsVar;
    CAMP_INSTRUMENT* pIns;
    struct authunix_parms* aup;
    char host1[LEN_NODENAME+1], host2[LEN_NODENAME+1];

       /*-----------------------------*/
      /*  Check if called by server  */
     /*  (server has privilege)     */
    /*-----------------------------*/
    if( rq == NULL ) return( CAMP_INS_NOT_LOCKED );

    /*
     *  Get pointer to instrument data
     */
    pInsVar = varGetpIns( pVar );
    if( pInsVar == NULL ) return( CAMP_INVAL_INS );

    if( !( pInsVar->core.status & CAMP_INS_ATTR_LOCKED ) )
    {
        return( CAMP_INS_NOT_LOCKED );
    }

      /*--------------------------------------*/
     /*  Check if locked by another process  */
    /*--------------------------------------*/
    aup = (struct authunix_parms*)rq->rq_clntcred;

    pIns = pInsVar->spec.CAMP_SPEC_u.pIns;

    if( ( aup->aup_gids[0] != pIns->lockPID ) ||
        !streq( stolower( strcpy( host1, aup->aup_machname ) ), 
                stolower( strcpy( host2, pIns->lockHost ) ) ) )
    {
          /*-----------------------------------*/
         /*  Check if locking process exists  */
        /*-----------------------------------*/
        status = checkPidOk( pIns->lockHost, pIns->lockPID, pIns->lockOs );
        if( status == CAMP_JOB_EXISTS )
        {
            status = CAMP_INS_LOCKED_OTHER;
        } 
        else if( status == CAMP_JOB_NONEXISTANT ) 
        {
            pIns->lockPID = 0;
            _free( pIns->lockHost );
            pIns->lockHost = strdup( "" );
	    _free( pIns->lockOs );
	    pIns->lockOs = strdup( "" );

            pInsVar->core.status &= ~CAMP_INS_ATTR_LOCKED;
        } 
        else
        {
            camp_appendMsg( "camp_checkInsLock: failure: checkPidOk" );
        }
    } 
    else 
    {
          /*-----------------------*/
         /*  Requestor is locker  */
        /*-----------------------*/
        status = CAMP_INS_LOCK_REQUESTOR;
    }
    return( status );
}


int 
camp_checkAllInssLock( struct svc_req* rq )
{
    int status;
    CAMP_VAR* pVar;

    /*
     *  If rq==NULL then call is not remote and has privilege,   
     *  else don't do anything if any are locked
     */
    if( rq != NULL )
    {
        for( pVar = pVarList; pVar != NULL; pVar = pVar->pNext )
        {
            status = camp_checkInsLock( pVar, rq );
            if( _failure( status ) ) return( status );
        }
    }

    return( CAMP_INS_NOT_LOCKED );
}


/*
 *  checkPidOk - check that a PID exists on a host
 *  Doesn't do remote hosts for VMS
 */
static int 
checkPidOk( char* host_in, int pid, char* os )
{
#if defined( VMS )
    int status;
    char host[LEN_NODENAME+1];

    strcpy( host, host_in );
    stolower( host );

    /*
     *  Allow only local processes to lock (no remote)
     *  This check should actually be redundant here.
     */
    if( !streq( pSys->hostname, host ) )
    {
        camp_appendMsg( "cannot lock instrument from remote node" );
        return( CAMP_FAILURE );
    }

    status = sys_getjpiw_pid( pid );
    if( status == SS$_NONEXPR ) return( CAMP_JOB_NONEXISTANT );
    else return( CAMP_JOB_EXISTS );

#else /* !VMS */

    /*
     *  Call my routine that uses rsh.
     *  Implicitly forces remote node to be trusted
     *  i.e., username of this process must have 
     *  r-service access to remote.
     */
    if( rpidok( host_in, pid, os ) ) return( CAMP_JOB_EXISTS );
    else return( CAMP_JOB_NONEXISTANT );

#endif /* VMS */

    return( CAMP_JOB_NONEXISTANT );
}


void
camp_sleep( timeval_t* ptv )
{
#ifdef MULTITHREADED
#if defined( _POSIX_THREADS )
    struct timespec ts;
    ts.tv_sec = ptv->tv_sec;
    ts.tv_nsec = 1000*ptv->tv_usec;
    pthread_delay_np( &ts );
#elif defined( VXWORKS )
    double delay = (double)ptv->tv_sec + ((double)ptv->tv_usec)/1.0e6;
    /*
     *  Get units from sysClkRateGet(), this
     *  is normally 60 ticks/second (i.e., minimum
     *  delay will be 1/60 seconds
     */
    taskDelay( (int)ceil( sysClkRateGet()*delay ) );
#endif /* _POSIX_THREADS */
#else  /* !MULTITHREADED */
/*
    float fsec = (float)((double)ptv->tv_sec + ((double)ptv->tv_usec)/1.0e6);
    lib_wait( fsec );
*/
    /* 
     *  Use the select call for portable sleep 
     */
    select( 1, NULL, NULL, NULL, ptv );
#endif /* MULTITHREADED */
}


void
camp_fsleep( double delay )
{
   timeval_t tv;
   double intPart;
   double fracPart;

   fracPart = modf( delay, &intPart );

   tv.tv_sec = (int)intPart;
   tv.tv_usec = (int)(1.0e6*fracPart);

   camp_sleep( &tv );
}
