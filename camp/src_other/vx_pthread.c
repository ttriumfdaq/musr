/*
 *  vx_pthread - minimal implementation of POSIX pthread library
 *               in VxWorks
 *
 *    Uses: taskLib, taskVarLib, semLib, semBLib, semMLib
 */

#include "vx_pthread.h"

SEM_ID global_mutex = NULL;
pthread_attr_t pthread_attr_default;


/* 
 *  Implement the global mutex with our
 *  own mutex.  Must be initialized and
 *  deleted by the application.
 */
void
pthread_lock_global_np( void ) 
{
    if( global_mutex == NULL ) global_mutex = semMCreate( SEM_Q_FIFO );

    semTake( global_mutex, WAIT_FOREVER );
}

void
pthread_unlock_global_np( void ) 
{
    semGive( global_mutex );
}

int
pthread_once( pthread_once_t* pOnce, pthread_initroutine_t init )
{
    init();

    /*
     *  Initialize default attributes to main thread
     */
    taskInfoGet( taskIdSelf(), &pthread_attr_default );
}

pthread_t
pthread_self( void )
{
    return( taskIdSelf() );
}

/*
 *  Thread attributes
 */
int
pthread_attr_create( pthread_attr_t* pAttr )
{
    /* 
     *  Start with inherited attributes
     */
    taskInfoGet( taskIdSelf(), pAttr );

    return( 0 );
}

int
pthread_attr_delete( pthread_attr_t* pAttr )
{
    return( 0 );
}

int
pthread_attr_setinheritsched( pthread_attr_t* pAttr, int inherit )
{
    return( 0 );
}

int 
pthread_setscheduler( pthread_t thread, int scheduler, int priority )
{
    taskPrioritySet( thread, priority );

    return( 0 );
}

/*
 *  Thread creation/deletion
 */
int
pthread_create( pthread_t* pThread, pthread_attr_t attr,
                pthread_startroutine_t start_routine,
	        pthread_addr_t arg )
{
    TASK_DESC* pTaskDesc;

    pTaskDesc = &attr;

    if( ( *pThread = taskSpawn( NULL, pTaskDesc->td_priority, VX_FP_TASK, 
                       pTaskDesc->td_stackSize, start_routine, 
                       (int)arg, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ) == ERROR )
    {
        return( -1 );
    }

    return( 0 );
}

/*
 *  "Tasks implicitly call exit() if the entry routine
 *  specified during task creation returns."  This is 
 *  compatible with pthreads.
 *  No need to free task storage.
 */
int
pthread_detach( pthread_t* pThread )
{
    return( 0 );
}

int 
pthread_cancel( pthread_t thread )
{
    if( taskDelete( thread ) == ERROR ) return( -1 );

    return( 0 );
}

int
pthread_setcancel( int state )
{
    if( state == CANCEL_ON ) 
    {
        if( taskUnsafe() == ERROR ) return( -1 );
    }
    else if( state == CANCEL_OFF ) 
    {
        if( taskSafe() == ERROR ) return( -1 );
    }
    return( 0 );
}

void
pthread_testcancel( void )
{
    return;
}

int
pthread_exit( pthread_addr_t* pStatus )
{
    exit( (int)*pStatus );
}

int
pthread_join( pthread_t thread, pthread_addr_t* pStatus )
{
    /* wait until the task ID doesn't exist */
    for(;;) if( taskIdVerify( thread ) == ERROR ) break;
    return( 0 );
}

/* 
 *  Only allowed one 4-byte task specific variable
 */
int 
pthread_keycreate( pthread_key_t* pKey, pthread_destructor_t destructor )
{
    return( 0 );
}

int
vx_pthread_setspecific( pthread_key_t* pKey, pthread_addr_t value )
{
    if( taskVarAdd( 0, (int*)pKey ) == ERROR ) return( -1 );

    *pKey = (pthread_key_t)value;

    return( 0 );
}

int
pthread_getspecific( pthread_key_t key, pthread_addr_t* pValue )
{
    *pValue = (pthread_addr_t)key;

    return( 0 );
}


/*
 *  mutexes
 */
int
pthread_mutexattr_create( pthread_mutexattr_t* pAttr )
{
    return( 0 );
}

int
pthread_mutexattr_delete( pthread_mutexattr_t* pAttr )
{
    return( 0 );
}

int
pthread_mutexattr_setkind_np( pthread_mutexattr_t* pAttr, int kind )
{
    return( 0 );
}

int
pthread_mutexattr_getkind_np( pthread_mutexattr_t attr )
{
    return( 0 );
}

int
pthread_mutex_init( pthread_mutex_t* pMutex, pthread_mutexattr_t attr )
{
    if( ( *pMutex = semMCreate( SEM_Q_FIFO ) ) == NULL ) return( -1 );

    return( 0 );
}

int
pthread_mutex_destroy( pthread_mutex_t* pMutex )
{
    if( semDelete( *pMutex ) == ERROR ) return( -1 );

    return( 0 );
}

int
pthread_mutex_lock( pthread_mutex_t* pMutex )
{
    if( semTake( *pMutex, WAIT_FOREVER ) == ERROR ) return( -1 );

    return( 0 );
}

int
pthread_mutex_trylock( pthread_mutex_t* pMutex )
{
    /*
     *  Can't tell whether it is invalid
     *  or timed out, so return timed out
     */
    if( semTake( *pMutex, NO_WAIT ) == ERROR ) return( 0 );

    return( 1 );
}

int
pthread_mutex_unlock( pthread_mutex_t* pMutex )
{
    if( semGive( *pMutex ) == ERROR ) return( -1 );

    return( 0 );
}


/*
 *  Conditions
 */
int
pthread_condattr_create( pthread_condattr_t* pAttr )
{
    return( 0 );
}

int
pthread_condattr_delete( pthread_condattr_t* pAttr )
{
    return( 0 );
}

int
pthread_cond_init( pthread_cond_t* pCond, pthread_condattr_t attr )
{
    if( ( *pCond = semBCreate( SEM_Q_FIFO, SEM_FULL ) ) == NULL ) return( -1 );

    return( 0 );
}

int 
pthread_cond_destroy( pthread_cond_t* pCond )
{
    if( semDelete( *pCond ) == ERROR ) return( -1 );

    return( 0 );
}

int 
pthread_cond_signal( pthread_cond_t* pCond )
{
    if( semGive( *pCond ) == ERROR ) return( -1 );

    return( 0 );
}

int 
pthread_cond_wait( pthread_cond_t* pCond, pthread_mutex_t* pMutex )
{
    /*
     *  Test every 100msec
     */
    while( semTake( *pCond, sysClkRateGet()*0.1 ) == ERROR )
    {
        if( semGive( *pMutex ) == ERROR ) return( -1 );
        if( semTake( *pMutex, WAIT_FOREVER ) == ERROR ) return( -1 );
    }
    return( 0 );
}










