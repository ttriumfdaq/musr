#ifndef _VX_PTHREAD_H_
#define _VX_PTHREAD_H_
/*
 *  vx_pthread.h - a minimal implementation of pthreads in VxWorks
 */

#include <vxWorks.h>
#include <taskLib.h>
#include <taskVarLib.h>
#include <semLib.h>

typedef char* (*VX_ROUTINE)();
typedef void (*VX_VOIDROUTINE)();

#define pthread_t                   int
#define pthread_attr_t              TASK_DESC
#define pthread_startroutine_t      VX_ROUTINE
#define pthread_addr_t              char*
#define pthread_key_t               char*
#define pthread_mutex_t             SEM_ID
#define pthread_cond_t              SEM_ID
#define pthread_mutexattr_t         int
#define pthread_condattr_t          int
#define pthread_mutexattr_default   SEM_Q_FIFO /* not used */
#define pthread_condattr_default    SEM_Q_FIFO /* not used */
#define pthread_once_t              int
#define pthread_once_init           0
#define pthread_destructor_t        VX_VOIDROUTINE
#define pthread_initroutine_t       VX_VOIDROUTINE

/*
 *  In VxWorks, scheduling is by default FIFO
 *  To set round-robin, use kernelTimeSlice() 
 *  which affects all tasks.
 */
#define SCHED_FIFO                  0  /* not used */
#define SCHED_RR                    0  /* not used */
#define SCHED_FG_NP                 0  /* not used */
#define SCHED_OTHER                 0  /* not used */
#define SCHED_BG_NP                 0  /* not used */

#define PRI_FIFO_MIN                255
#define PRI_FIFO_MAX                0
#define PRI_RR_MIN                  255
#define PRI_RR_MAX                  0
#define PRI_FG_MIN_NP               255
#define PRI_FG_MAX_NP               0
#define PRI_BG_MIN_NP               255
#define PRI_BG_MAX_NP               0
#define PRI_OTHER_MIN               255
#define PRI_OTHER_MAX               0

#define CANCEL_ON                   1
#define CANCEL_OFF                  0

#define PTHREAD_INHERIT_SCHED       0

#define MUTEX_RECURSIVE_NP          0  /* not used */

#define pthread_setspecific( k, v ) vx_pthread_setspecific( &k, v )

extern pthread_attr_t pthread_attr_default;

void pthread_lock_global_np( void );
void pthread_unlock_global_np( void );
int pthread_once( pthread_once_t* pOnce, pthread_initroutine_t init );
pthread_t pthread_self( void );
int pthread_attr_create( pthread_attr_t* pAttr );
int pthread_attr_delete( pthread_attr_t* pAttr );
int pthread_attr_setinheritsched( pthread_attr_t* pAttr, int inherit );
int pthread_setscheduler( pthread_t thread, int scheduler, int priority );
int pthread_create( pthread_t* pThread, pthread_attr_t attr,
                pthread_startroutine_t start_routine,
	        pthread_addr_t arg );
int pthread_detach( pthread_t* pThread );
int pthread_cancel( pthread_t thread );
int pthread_setcancel( int state );
void pthread_testcancel( void );
int pthread_exit( pthread_addr_t* pStatus );
int pthread_join( pthread_t thread, pthread_addr_t* pStatus );
int pthread_keycreate( pthread_key_t* pKey, pthread_destructor_t destructor );
int vx_pthread_setspecific( pthread_key_t* pKey, pthread_addr_t value );
int pthread_getspecific( pthread_key_t key, pthread_addr_t* pValue );
int pthread_mutexattr_create( pthread_mutexattr_t* pAttr );
int pthread_mutexattr_delete( pthread_mutexattr_t* pAttr );
int pthread_mutexattr_setkind_np( pthread_mutexattr_t* pAttr, int kind );
int pthread_mutexattr_getkind_np( pthread_mutexattr_t attr );
int pthread_mutex_init( pthread_mutex_t* pMutex, pthread_mutexattr_t attr );
int pthread_mutex_destroy( pthread_mutex_t* pMutex );
int pthread_mutex_lock( pthread_mutex_t* pMutex );
int pthread_mutex_trylock( pthread_mutex_t* pMutex );
int pthread_mutex_unlock( pthread_mutex_t* pMutex );
int pthread_condattr_create( pthread_condattr_t* pAttr );
int pthread_condattr_delete( pthread_condattr_t* pAttr );
int pthread_cond_init( pthread_cond_t* pCond, pthread_condattr_t attr );
int pthread_cond_destroy( pthread_cond_t* pCond );
int pthread_cond_signal( pthread_cond_t* pCond );
int pthread_cond_wait( pthread_cond_t* pCond, pthread_mutex_t* pMutex );

#endif /* _VX_PTHREAD_H_ */
