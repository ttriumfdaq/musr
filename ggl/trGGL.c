/*------------------------------------------------------------------*
 * trGGL.c 
 * Procedures for handling the Gate Generator Logic board
 * This is a VME board specified by Syd Kreitzman - TRIUMF
 * designed and produced by Triumf electronics group - Hubert Hui
 *
 * Converted to VMIC
 *
 * Id:$
 *------------------------------------------------------------------*/
#include "trGGL.h"

#include <stdio.h>
#include <string.h>
#if defined(OS_LINUX)
#include <unistd.h>
#endif


#ifdef MAIN_ENABLE
// For VMIC processor
#include "vmicvme.h"
#endif



int ddd; //debug
const float DacMin[6]={ 0,0,-5,-10,-2.5,-2.5};
const float DacMax[6]={ 5,10,5,10,2.5,7.5};
const float DacRange[6]={5,10,10,20,5,10};
const int DacMaxCounts = 0xFFFF;



/*------------------------------------------------------------------*/
/** gglInit
    Initialize the GGL 
    @memo Init GGL 
    @param base GGL VME base address
    @return void
*/
void gglInit(MVME_INTERFACE *mvme,  DWORD base)
{
  DWORD data,counts;
    
  /* initial values to write : */
#ifdef DEBUG
  INT delta=5000; /* 5us */
  INT delta1=200; /* 200ns */
  INT delta2=600; /* 600ns */
#else
  /* program these in counts */
  INT delta=1005; /* power-up values + 5 to make sure we write something */
  INT delta1=35; /*  */
  INT delta2=55; /*  */
#endif

  INT icounts=10000; /* preset 10000 counts */
  INT sr=3; /* S/R register value */
  INT timeH = 50; /* 50ms for pulser Hi time */
  INT timeL = 100; /* 100ms for pulser Lo time */
  INT range = 3; /* -10V to +10V */
  float voltage = -8.5; /* DAC voltage (volts) */

  if(ddd)printf("gglInit:\n");

  printf("Initializing values to:\n");
  printf("Preset Counter = %d;  Sample/Ref register = %d\n",
	 icounts,sr );
  printf("Delays: delta=%d ,delta1=%d  ,delta2=%d counts\n",
	 delta,delta1,delta2 );

  data = gglWritePreset (mvme, base, icounts);
  printf("Set Preset Down Counter to %d ; read back %lu \n",icounts,data);
  
/* Write to GGL_SR_ENABLE_RW reg */
  data=gglWriteSR (mvme, base, sr);
  printf("Set SR Enable reg. to %d ; read back %lu \n",sr,data);

  data = gglWriteDelta(mvme, base, delta);
  if(ddd)printf("Wrote %d counts to Delta delay register; read back %lu\n",delta,data);
  data = gglWriteDelta1(mvme, base, delta1);
  if(ddd)printf("Wrote %d counts to Delta delay register; read back %lu\n",delta1,data);
  data = gglWriteDelta2(mvme, base, delta2);
  if(ddd)printf("Wrote %d counts to Delta delay register; read back %lu\n",delta2,data);

  /* PULSER */
  printf("\nEnabling pulser\n");
  data = gglWritePulserEnable(mvme, base, 1);
  if (ddd) printf("Wrote 1 to enable pulser, read back %lu\n",data);
  
  printf("\nSetting pulser HI time to %d ms and LO time to %d ms\n",timeH,timeL);
  counts =  GetPulserCounts( (float) timeH);
  data = gglWritePulserHigh(mvme, base, counts);
  if(ddd) printf("Wrote High counts = %ld to pulser, read back %lu\n",counts,data);


  counts =  GetPulserCounts( (float) timeL);
  data = gglWritePulserLow(mvme, base, counts);
  if (ddd) printf("Wrote Low counts = %ld to pulser, read back %lu\n",counts,data);  

  /* DAC  */
  printf("\nSetting DAC to Range %d and voltage %f volts\n",
	 range,voltage);
  
 
  data = gglWriteDacRange(mvme, base, range);
  if (ddd) printf("Wrote DAC range=%d, read back %ld\n",range,data);
 
  counts = GetDacCounts (voltage, range);
  if (counts != -99)
  {
    data = gglWriteDacSetpoint(mvme, base,  counts);
    if(ddd) printf ("Set DAC counts = %lu, read back %lu counts\n",counts,data);
  }
  else
    printf("gglInit: error return from GetDacCounts\n");
  return;

}
/*------------------------------------------------------------------*/
/** gglReadAll
    Read back all  GGL registers 
    @memo read all GGL regs 
    @param base GGL VME base address
    @return void
*/
 void gglReadAll(MVME_INTERFACE *mvme, DWORD base)
{
  INT data,range;
  float DacVoltage, Time, Period;
  INT status;
  
  if(ddd) printf("gglReadAll:\n");
  data = gglReadSR(mvme,base);
  printf("SR Enable  Reg: 0x%x  or %d\n", data,data);
  
  gglReadDelays(mvme, base);
  
  
  data = gglReadPreset(mvme, base);
  printf("Preset Counter : 0x%x or %d \n",data,data);
  
  data = gglReadbackCounter(mvme, base);
  printf("Down Counter Readback: 0x%x or %d \n",data,data);

  data = gglReadPulserEnable(mvme, base);
  if(data>0)
    printf("Pulser is enabled\n");
  else
    printf("Pulser is disabled\n");
    
  data =  gglReadPulserLow(mvme, base);
  if(ddd) printf("Read Pulser L0 = %d counts\n",data);
  Period = (float)data;
  Time = GetPulserTime_ms(data);
  printf("Pulser Low Time Setpoint = 0x%x or %d counts, i.e.  %f ms \n",data,data,Time);

  data = gglReadPulserHigh(mvme, base);
  if(ddd) printf("Read Pulser HI = %d counts\n",data);   
  Time = GetPulserTime_ms(data);  
  printf("Pulser High Time Setpoint = 0x%x or %d counts, i.e. %f ms \n",data,data,Time);
  Period = (Period + (float)data) * 0.0001;
  printf("Period = %f seconds \n",Period);

  /* DAC */
  range = gglReadDacRange(mvme, base);
  data  =  gglReadDacSetpoint(mvme, base);
  status = GetDacVoltage(data, range, &DacVoltage);
  if(status != -99)
    printf("DAC: Range %d, counts=%d(0x%x)  %.5f Volts\n",range,data,data,DacVoltage );
  else
    printf("gglReadAll: Error return from GetDacVoltage\n");

 
}

/*------------------------------------------------------------------*/
/** gglResetCounter
    Reset Counter
    @memo Reset Counter to value in Counter Set Registers.
    @param base GGL VME base address
    @return value read from counter
*/
 DWORD  gglResetCounter(MVME_INTERFACE *mvme, DWORD base)
{
  INT data;
  
  data = gglRegWrite8 (mvme, base, GGL_RESET_COUNTER_RW, 0x1);
  /* read Counter Readback Register */
  return  gglReadbackCounter(mvme, base);
}

/*------------------------------------------------------------------*/
/** gglReset
    Reset
    @memo Reset GGL module
    @param base GGL VME base address
    @return 0
*/
 DWORD  gglReset(MVME_INTERFACE *mvme, DWORD base)
{
  INT data;
  
  data = gglRegWrite8 (mvme, base, GGL_RESET_WO, 0x1);
  return  data;
}


#ifdef DEBUG
/*------------------------------------------------------------------*/
/** gglWriteDelta_ns
    Write a Delta Delay value in nanoseconds.
    @memo 16bit write.
    @param base\_adr GGL VME base address
    @param value (nanoseconds) to be written 
    @return masked read back value in nanoseconds
*/
 INT gglWriteDelta_ns(MVME_INTERFACE *mvme, DWORD base, const WORD value_ns)
{
    /*
    delay = register value * 10ns
    minimum value = 2 (20ns); maximum = 0x7ff (2047) i.e.  20470 ns (20.47 us) 
   */

  INT data,ival;

  ival = value_ns / 10 ; /* value to write */
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  data = gglRegWrite(mvme, base, GGL_DELTA_DELAY_RW, ival);
  return ( 10* (data &  DELTA_DELAY_MSK)); /* return masked value in ns */
}

/*------------------------------------------------------------------*/
/** gglWriteDelta1_ns
    Write a Delta1 delay value in nanoseconds
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param value (ns)  to be written 
    @return masked read back value in nanoseconds
*/
 INT gglWriteDelta1_ns(MVME_INTERFACE *mvme, DWORD base, const WORD value_ns)
{
      /*
    delta1_delay = register value * 10ns
    minimum value = 2 (20ns); maximum = 0x7f (127)  i.e. 1270ns
   */

  INT data,ival;
  
  ival = value_ns / 10;
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  data = gglRegWrite8(mvme, base, GGL_DELTA1_DELAY_RW , ival);
  return ( 10 * (data &  DELTA8_DELAY_MSK)); /* return masked value in ns */
}

/*------------------------------------------------------------------*/
/** gglWriteDelta2_ns
    Write to register Delta2
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param value (ns) to be written 
    @return masked read back value in nanoseconds
*/
 INT gglWriteDelta2_ns(MVME_INTERFACE *mvme, DWORD base, const WORD value_ns)
{
  /*
    delta1_delay = register value * 10ns
    minimum value = 2 (20ns); maximum = 0x7f (127)  i.e. 1270ns
  */

  INT data,ival;
  
  ival = value_ns / 10 ;
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  data = gglRegWrite8(mvme, base, GGL_DELTA2_DELAY_RW , ival);
  return (10* (data &  DELTA8_DELAY_MSK) ); /* return masked value in ns */
}
#endif

/*------------------------------------------------------------------*/
/** gglReadDelays
    Read 3 Delta Delay Registers
    @memo Read delay Registers.
    @param base GGL VME base address
    @return 1
*/
 INT gglReadDelays(MVME_INTERFACE *mvme, DWORD base)
{
  INT delta,delta1,delta2;

  delta =  ( gglRegRead  (mvme, base, GGL_DELTA_DELAY_RW )  & DELTA_DELAY_MSK  )  ;
  delta1 = ( gglRegRead8 (mvme, base, GGL_DELTA1_DELAY_RW)  & DELTA8_DELAY_MSK )  ;
  delta2 = ( gglRegRead8 (mvme, base, GGL_DELTA2_DELAY_RW)  & DELTA8_DELAY_MSK )  ;


  if(ddd)
  {
    printf("\ngglReadDelays:\n");
    printf("Register values:\n");
    printf("delta = %d (0x%x) delta1 = %d (0x%x)  delta2 = %d (0x%x) \n\n",
	   delta,delta,delta1,delta1,delta2,delta2);
  }
#ifdef DEBUG
  printf("Delays: Delta = %d ns ; Delta1 = %d ns ; Delta2 = %d ns\n",
	 delta*10, delta1*10, delta2 *10);
#else
  printf("Delays: Delta = %d counts ; Delta1 = %d counts ; Delta2 = %d counts\n",
	 delta, delta1, delta2);
#endif
  return 1 ;
}


/*------------------------------------------------------------------*/
/** gglReadbackCounter
    Read Read Down Counter Readback reg.
    @memo Read Down  Counter Readback reg.
    @param base GGL VME base address
    @return data
*/
 DWORD gglReadbackCounter(MVME_INTERFACE *mvme, DWORD base)
{
  INT dataH,dataL;
  DWORD data;

  dataH = gglRegRead (mvme, base,  GGL_COUNTER_READBACK_HI_RO);
  dataL = gglRegRead (mvme, base,  GGL_COUNTER_READBACK_LO_RO);
  if(ddd)printf("dataH = 0x%x dataL = 0x%x\n",dataH,dataL);
  data = (DWORD)( dataH << 16 | dataL);
  if(ddd)printf("returning read Counter Readback  Reg = 0x%lx\n",data); 
  return data ;
}

/*------------------------------------------------------------------*/

/** gglReadPreset
    Read Down Counter Preset Reg
    @memo Read Down Counter Preset Reg.
    @param base GGL VME base address
    @return
*/
 DWORD gglReadPreset(MVME_INTERFACE *mvme, DWORD base)
{
  INT dataH,dataL;
  DWORD data;
  dataH = gglRegRead (mvme, base, GGL_PRESET_HI_RW);
  dataL = gglRegRead (mvme, base, GGL_PRESET_LO_RW);
  if(ddd)printf("dataH = 0x%x dataL = 0x%x\n",dataH,dataL);
  data = (DWORD)( dataH << 16 | dataL);
  if(ddd)printf("returning Read Preset  Reg = 0x%lx\n",data); 
  return data ;
}
/*------------------------------------------------------------------*/
/** gglWritePreset
    Write Preset Counter
    @memo Write Preset Down Counter.
    @param base GGL VME base address value
    @return data
*/
 DWORD gglWritePreset(MVME_INTERFACE *mvme, DWORD base, const DWORD value)
{
  INT dataH,dataL,dataHr,dataLr;
  DWORD data;

  data = value;
  dataH = ( data & 0xFFFF0000 ) >> 16 ;
  dataL = ( data & 0xFFFF );
  if(ddd)printf("Preset data = 0x%lx; dataH = 0x%x, dataL = 0x%x\n",data,dataH,dataL);
  dataHr = gglRegWrite (mvme, base, GGL_PRESET_HI_RW, dataH );
  dataLr = gglRegWrite (mvme, base, GGL_PRESET_LO_RW, dataL );
  if(ddd)printf("Read back Preset data as dataHr = 0x%x dataLr = 0x%x\n",dataHr,dataLr);
  data = (DWORD)( dataHr << 16 | dataLr);
  if(ddd)printf("Wrote preset=0x%lx; read back 0x%lx\n",value,data); 
  return data ;
}


/*------------------------------------------------------------------*/
/** gglRegWrite
    Write 16bit to register.
    @memo 16bit write.
    @param base\_adr GGL VME base address
    @param reg\_offset register offset
    @param value to be written 
    @return read back value
*/
 INT gglRegWrite(MVME_INTERFACE *mvme, DWORD base, const DWORD reg_offset,
                            const WORD value)
{
  INT csr;
  
  mvme_set_am(mvme, MVME_AM_A16_ND); // set A16
  mvme_set_dmode(mvme, MVME_DMODE_D16); // set D16

  mvme_write_value(mvme, base + reg_offset, value);
  csr =  gglRegRead(mvme, base, reg_offset);
  return (csr);
}



/*------------------------------------------------------------------*/
/** gglRegWrite8
    Write 8bit to register.
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param reg\_offset register offset
    @param value to be written 
    @return read back value
*/
 INT gglRegWrite8(MVME_INTERFACE *mvme, DWORD base, const DWORD reg_offset,
                           const BYTE value)
{
  INT  csr;
  
  mvme_set_am(mvme, MVME_AM_A16_ND); // set A16
  mvme_set_dmode(mvme, MVME_DMODE_D8); // set D8
  mvme_write_value(mvme, base + reg_offset, value);

  csr =  gglRegRead8(mvme, base, reg_offset);
  return (csr & 0xFF);
}



/*------------------------------------------------------------------*/
/** gglRegRead
    Read 16bit to register.
    @memo 16bit read.
    @param base\_adr GGL VME base address
    @param reg\_offset register offset
    @return read back value
*/
 INT gglRegRead(MVME_INTERFACE *mvme, DWORD base, DWORD reg_offset)
{
  DWORD csr;
  INT data;

  mvme_set_am(mvme, MVME_AM_A16_ND); // set A16
  mvme_set_dmode(mvme, MVME_DMODE_D16); // set D16

  csr = mvme_read_value(mvme, base + reg_offset);
  data = (INT)(csr & 0xFFFF);
  return data;
}


/*------------------------------------------------------------------*/
/** gglRegRead8
    Read 8bit to register.
    @memo 8bit read.
    @param base\_adr GGL VME base address
    @param reg\_offset register offset
    @return read back value
*/
 INT gglRegRead8(MVME_INTERFACE *mvme, DWORD base, DWORD reg_offset)
{
  DWORD csr;
  INT data;
  
  mvme_set_am(mvme, MVME_AM_A16_ND); // set A16
  mvme_set_dmode(mvme, MVME_DMODE_D8); // set D8

  csr = mvme_read_value(mvme, base + reg_offset);
  data = (INT)(csr & 0xFF);
  return data;
}


/*------------------------------------------------------------------*/
/** gglWriteSR
    Write to S/R register
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param value to be written 
    @return masked read back value
*/
 INT gglWriteSR(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{

  INT data;
  
  data = gglRegWrite8(mvme, base, GGL_SR_ENABLE_RW , value);
  return (data &  SR_ENABLE_MSK); /* return masked value */

}

/*------------------------------------------------------------------*/
/** gglReadSR
    Readback S/R register
    @memo 8bit read.
    @param base\_adr GGL VME base address
    @return masked read back value
*/
 INT gglReadSR(MVME_INTERFACE *mvme, DWORD base)
{
  INT data;
  
  data = gglRegRead8(mvme, base,GGL_SR_ENABLE_RW);
  return (data &  SR_ENABLE_MSK); /* return masked value */

}

/*------------------------------------------------------------------*/
/** gglWriteDelta
    Write 16bit to register Delta Delay.
    @memo 16bit write.
    @param base\_adr GGL VME base address
    @param value to be written 
    @return masked read back value
*/
 INT gglWriteDelta(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{
  INT data,ival;
  ival = value;
  
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  data = gglRegWrite(mvme, base, GGL_DELTA_DELAY_RW, ival);
  return (data &  DELTA_DELAY_MSK); /* return masked value */
}

/*------------------------------------------------------------------*/
/** gglWriteDelta1
    Write to register Delta1
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param value to be written 
    @return masked read back value
*/
 INT gglWriteDelta1(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{
  INT data,ival;
  
  ival = value;
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  data = gglRegWrite8(mvme, base, GGL_DELTA1_DELAY_RW , ival);
  return (data &  DELTA8_DELAY_MSK); /* return masked value */
}

/*------------------------------------------------------------------*/
/** gglWriteDelta2
    Write to register Delta2
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param value to be written 
    @return masked read back value
*/
 INT gglWriteDelta2(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{
  INT data,ival;
  
  ival = value;
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  data = gglRegWrite8(mvme, base, GGL_DELTA2_DELAY_RW , ival);
  return (data &  DELTA8_DELAY_MSK); /* return masked value */
}
 

/*------------------------------------------------------------------*/
/** gglWritePulserEnable
    Write enable/disable (1/0)
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param value to be written (1/0) 
    @return masked read back value
*/
 INT gglWritePulserEnable(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{
  INT data;
  
  data = gglRegWrite8(mvme, base, GGL_PULSER_ENABLE_RW , value);
  return ( data &  PULSER_ENABLE_MSK); /* return masked value */
}

/*------------------------------------------------------------------*/
/** gglReadPulserEnable
    Read if pulser enabled/disabled (1/0)
    @memo 8bit write.
    @param base\_adr GGL VME base address 
    @return masked read back value
*/
 INT gglReadPulserEnable(MVME_INTERFACE *mvme, DWORD base)
{
  INT data;
  
  data = gglRegRead8(mvme, base, GGL_PULSER_ENABLE_RW);
  return ( data &  PULSER_ENABLE_MSK); /* return masked value */
}



/*------------------------------------------------------------------*/
/** gglWritePulserHigh
    Write Pulser High Time Setpoint Register (counts)
    @memo 16 bit write.
    @param base\_adr GGL VME base address
    @param value to be written
    @return masked read back value
*/
 INT gglWritePulserHigh(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{
  INT ival;
  ival = value;
  
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  
  return gglRegWrite(mvme, base, GGL_PULSER_HI_SETP_RW , ival);
}

/*------------------------------------------------------------------*/
/** gglWritePulserLow
    Write Pulser Low Time Setpoint Register (counts)
    @memo 16 bit write.
    @param base\_adr GGL VME base address
    @param value to be written
    @return masked read back value
*/
 INT gglWritePulserLow(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{
  INT ival;
  ival = value;
  
  if (ival < MIN_VALUE) ival = MIN_VALUE;
  
  return gglRegWrite(mvme, base, GGL_PULSER_LO_SETP_RW , ival);
}

/*------------------------------------------------------------------*/
/** gglReadPulserHigh
    Reac Pulser High Time Setpoint Register (counts)
    @memo 16 bit write.
    @param base\_adr GGL VME base address
    @return read back value
*/
 INT gglReadPulserHigh(MVME_INTERFACE *mvme, DWORD base)
{  
  return gglRegRead(mvme, base, GGL_PULSER_HI_SETP_RW);
}
/*------------------------------------------------------------------*/
/** gglReadPulserLow
    Read Pulser Low Time Setpoint Register (counts)
    @memo 16 bit write.
    @param base\_adr GGL VME base address
    @return masked read back value
*/
 INT gglReadPulserLow(MVME_INTERFACE *mvme, DWORD base)
{  
  return gglRegRead(mvme, base, GGL_PULSER_LO_SETP_RW);
}


/*------------------------------------------------------------------*/
INT GetPulserCounts(float time_ms)
{
    /* return  counts from input time in ms  */
  INT counts;
  counts = (INT)  (time_ms  /0.1) ; /* ms */
  if (counts > 0xFFFF) counts = 0xFFFF;
  if (counts < MIN_VALUE) counts = MIN_VALUE;
  return counts;
}



/*------------------------------------------------------------------*/
float GetPulserTime_ms(INT counts)
{
  /* return time in ms from counts */
  float time_ms;
  time_ms = (float)counts * .1;
  return time_ms;
}


/*------------------------------------------------------------------*/
/** gglWriteDacRange
    Write a DAC Range [0-5]
    @memo 8bit write.
    @param base\_adr GGL VME base address
    @param value to be written (binary range code) 
    @return masked read back value
*/
 INT gglWriteDacRange(MVME_INTERFACE *mvme, DWORD base, const WORD value)
{
      /*
      DAC ranges: 0:   0  to +5V
                  1:   0  to +10V
		  2:  -5  to +5V
                  3: -10  to +10V
		  4: -2.5 to +2.5V
		  5: -2.5 to +7.5V 
   */

  INT data;
  
  data = gglRegWrite8(mvme, base,GGL_DAC_RANGE_RW , value);
  return ( data &  DAC_RANGE_MSK); /* return masked value */
}

/*------------------------------------------------------------------*/
/** gglReadDacRange
    Read the DAC Range [0-5]
    @memo 8bit read.
    @param base\_adr GGL VME base address 
    @return masked read back value
*/
 INT gglReadDacRange(MVME_INTERFACE *mvme, DWORD base)
{
      /*
      DAC ranges: 0:   0  to +5V
                  1:   0  to +10V
		  2:  -5  to +5V
                  3: -10  to +10V
		  4: -2.5 to +2.5V
		  5: -2.5 to +7.5V 
   */

  INT data;
  
  data = gglRegRead8(mvme, base,GGL_DAC_RANGE_RW);
  return ( data &  DAC_RANGE_MSK); /* return masked value */
}


/*------------------------------------------------------------------*/
/** gglWriteDacSetpoint
    Write a DAC Setpoint (depends on DAC range; see manual for table)
    @memo 16bit write.
    @param base\_adr GGL VME base address
    @param value to be written 
    @return masked read back value
*/
 INT gglWriteDacSetpoint(MVME_INTERFACE *mvme, DWORD base, const DWORD value)
{
  
  return  gglRegWrite(mvme, base,GGL_DAC_SETPOINT_RW , value);
}

/*------------------------------------------------------------------*/
/** gglReadDacSetpoint
    Read a DAC Setpoint (depends on DAC range; see manual for table)
    @memo 16bit write.
    @param MVME\_INTERFACE *mvme : pointer to vmic
    @param base\_adr GGL VME base address
    @return masked read back value
*/
INT gglReadDacSetpoint(MVME_INTERFACE *mvme, DWORD base)
{  
  return  gglRegRead(mvme, base, GGL_DAC_SETPOINT_RW);
}



/*------------------------------------------------------------------*/
/** GetDacVoltage
    Translate DAC range and counts to a voltage value

    Input: DAC counts, range
    Returns: DAC voltage or -99 for failure
*/
INT GetDacVoltage(INT count, INT range, float *pvolts)
{

  float volts;
  
  if(range < 0 || range > 5)
  {
    printf("GetDacVoltage: Invalid range %d\n",range);
    return -99;
  }
  if(count < 0 || count > DacMaxCounts)
  {
    printf("GetDacVoltage: Invalid counts (%d)  (-1< counts < %d)\n",
	   count,DacMaxCounts);
    return -99;
  }

  if(ddd) printf("GetDacVoltage: count=%d, range=%d DacMax=%f DacMin=%f\n",count,range,DacMax[range],DacMin[range]);
   
  volts = DacMin[range] +  (float)count *  (DacMax[range]-DacMin[range])/65536.0 ;
  
  if(ddd)printf("GetDacVoltage: returning volts=%f\n",volts);
    
/*  volts = (float)count *  (DacMax[range]-DacMin[range])/DacMaxCounts +
    DacMin[range]; */
  *pvolts = volts;
  
  return 0; /* success */
}

/*------------------------------------------------------------------*/
/** GetDacCounts
    Calculate the DAC counts from the range and voltage
    
    Input: range,voltage
    Returns: DAC counts or -99 for failure
*/
INT GetDacCounts(float voltage, INT range)
{

  float fc;
  
  if(range < 0 || range > 5)
  {
    printf("GetDacCounts: Invalid range %d\n",range);
    return -99;
  }
  if(voltage < DacMin[range] || voltage > DacMax[range])
  {
    printf("GetDacCounts: Invalid voltage (%.4f) for this range  (%.2f< Voltage < %.2f) \n",
	   voltage,DacMin[range],DacMax[range]);
    return -99;
  }
  
  fc = (voltage -  DacMin[range]) / (  (DacMax[range]-DacMin[range])/DacMaxCounts);
  return (INT)fc;
}


INT TestGetDacRange(int ivolt1, int ivolt2)
{
  float volt1,volt2;
  int i;
  
  volt1 = (float) ivolt1;
  volt2 = (float) ivolt2;
  i = GetDacRange(volt1,volt2);
  return i;
}


/*------------------------------------------------------------------*/
/* Determine the best DAC range given the max and min voltage
      Max and Min can be entered in any order

      Parameters:
      Input:    max and min voltage
      Returns:  range or -99 for failure
*/
INT  GetDacRange (float volt1, float volt2)
{
  INT i,my_index,first;
  float min_voltage,max_voltage;
  
  
  first = 1;
  my_index = 0;
  if (volt1> volt2)
  {
    min_voltage=volt2;
    max_voltage=volt1;
  }
  else
  {
    max_voltage=volt2;
    min_voltage=volt1;
  }
  
  for (i=0; i< 6; i++)
  {
    if ( min_voltage > DacMin[i] && max_voltage < DacMax[i])
    {
      if(ddd) printf("GetDacRange: Volt range min=%f and max=%f at index %d is OK for values min=%f and max=%f\n",
	     DacMin[i],DacMax[i],i,min_voltage,max_voltage);
      /* this range is OK */
/*        printf("DacRange[i] = %f,  DacRange[my_index] = %f\n",
	  DacRange[i],DacRange[my_index]); */
      if (first)
      {   /* no range has yet been found */
	my_index=i;
	first=0;
      }
      else
      {
	if (DacRange[i] < DacRange[my_index])
	{
	  my_index=i;
	  if(ddd)printf("Range %f at index %d is better\n",DacRange[i],i);
	}
      }
    }
  }
  
  if(first == 1)
  {
    printf("GetDacRange: no range was valid\n");
    return (-99);
  }
  if(ddd)printf("GetDacRange: found best DAC range for voltage range %f to %f is range %d (%f to %f)\n",
	 min_voltage,max_voltage,my_index, DacMin[my_index],DacMax[my_index]);
  return(my_index);
  
}


#ifdef GONE
/* don't know what this was for */
/*------------------------------------------------------------------*/
INT GetPulserRange(float maxVolt, float minVolt)
{
    /* return a suitable  range   */
  float Volt_span;
  INT counts;
  
  Volt_span = maxVolt-minVolt;
  
  counts = (INT)  (time_ms  /0.1) ; /* ms */
  if (counts > 0xFFFF) counts = 0xFFFF;
  if (counts < MIN_VALUE) counts = MIN_VALUE;
  return counts;
}
#endif
/*****************************************************************/
/*-PAA- For test purpose only */

#ifdef MAIN_ENABLE


int main (int argc, char* argv[]) {

  INT status,  data;
  char cmd[]="hallo";
  int s;
  DWORD ival,reg,ldata;

  INT range=-1;
  float volts,DacVoltage;
  INT counts;
  MVME_INTERFACE *myvme;

  DWORD  GGL_BASE = 0x8000; /* default */

  if (argc>1) {
    sscanf(argv[1],"%lx",&GGL_BASE);
  }

  // Test under vmic
  status = mvme_open(&myvme, 0);

  // Set am to A16 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A16_ND); // set A16

  // Set dmode to D16
  mvme_set_dmode(myvme, MVME_DMODE_D16);

  printf("\nCalling gglInit...\n\n");
  gglInit(myvme,GGL_BASE); 
  GGLCmds();
   while ( isalpha(cmd[0]) ||  isdigit(cmd[0]) )
    {
      printf("\nEnter command (A-Y) X to exit?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];

      switch(s)
        {
        case ('A'):
          gglReset(myvme, GGL_BASE);
          break;

        case ('B'):
          gglInit(myvme,GGL_BASE ); // Initialize to some standard values
          break;  

        case ('C'):
          printf("Enter GGL Register : 0x");
          scanf("%lx",&reg);
          printf("Enter value to write (8 bits) : 0x");
          scanf("%lx",&ival);
          ival = ival & 0xFF;
          data = gglRegWrite8(myvme, GGL_BASE, reg, ival);
	  printf("Wrote: 0x%2.2lx Read back: 0x%2.2x\n",ival,data);
          break;

        case ('E'):
          printf("Enter GGL Register : 0x");
          scanf("%lx",&reg);
          data = gglRegRead8(myvme, GGL_BASE, reg);
	  printf("Read back: 0x%2.2x\n",data);
          break;

        case ('F'):
          printf("Enter GGL Register : 0x");
          scanf("%lx",&reg);
          printf("Enter value to write (16 bits) : 0x");
          scanf("%lx",&ival);
          ival = ival & 0xFFFF;
          data = gglRegWrite(myvme, GGL_BASE, reg, ival);
	  printf("Wrote: 0x%4.4lx Read back: 0x%4.4x\n",ival,data);
          break;

        case ('G'):
          printf("Enter GGL Register : 0x");
          scanf("%lx",&reg);
          data = gglRegRead(myvme, GGL_BASE, reg);
	  printf("Read back: 0x%4.4x\n",data);
          break;

        case ('I'):       
	  gglReadDelays(myvme, GGL_BASE);
          break;

        case ('J'):
          printf("Enter Delta delay in counts (2-2047 i.e. 20-20470ns) : ");
          scanf("%lu",&ival);
          if(ival > 2047)
	    ival=2047;
          status = gglWriteDelta(myvme, GGL_BASE, (WORD) ival);
          printf("Read back %d counts (%d ns)\n",data,(data*10));
          break;

        case ('K'):
          printf("Enter Delta1 delay in counts (2-127 i.e. 20-1270ns) : ");
          scanf("%lu",&ival);
          if(ival > 127)
	    ival=127;
          status = gglWriteDelta1(myvme, GGL_BASE, (WORD) ival);
          printf("Read back %d counts (%d ns)\n",data,(data*10));
          break;

        case ('L'):
          printf("Enter Delta2 delay in counts (2-127 i.e. 20-1270ns) : ");
          scanf("%lu",&ival);
          if(ival > 127)
	    ival=127;
          status = gglWriteDelta2(myvme, GGL_BASE, (WORD) ival);
          printf("Read back %d counts (%d ns)\n",data,(data*10));
          break;

        case ('M'):
          printf("Enter 32-bit Down Counter preset value in counts : ");
          scanf("%lu",&ival);
	  ldata = gglWritePreset(myvme, GGL_BASE, ival);
          printf("Set Preset of Down Counter to %lu; Read back  %lu\n",ival,ldata);
          break;

        case ('N'):
	  ldata = gglReadPreset(myvme, GGL_BASE);
          printf("Read Preset of Down Counter as %lu\n",ldata);
          break;
 
        case ('O'):
	  ldata = gglReadbackCounter(myvme, GGL_BASE);
          printf("Read back Down Counter as %lu\n",ldata);

        case ('R'):
	  ldata = gglResetCounter(myvme, GGL_BASE);
          printf("Read back Down Counter as %lu\n",ldata);
          break;

        case ('S'):
          gglReadAll( myvme,  GGL_BASE);  // read and display all registers (status)
          break;

        case ('T'):
          printf("Enter value to write to Sample/Reference Register: 0x");
          scanf("%lx",&ival);
          ival = ival & 7; // 3 bits for Write
	  data = gglWriteSR (myvme, GGL_BASE, (WORD)ival);
          printf("Wrote 0x%2.2lx; Read back 0x%2.2x\n",ival,data);
          break;

        case ('U'):
	  data = gglReadSR ( myvme, GGL_BASE);
          printf("Read back 0x%2.2x\n",data);
          break;

         case ('V'):
           printf("Enter value 1=enable 0=disable: ");
           scanf("%lu",&ival);
           if (ival > 0) 
	     ival = 1;
	   else
	     ival = 0;
           data = gglWritePulserEnable (myvme, GGL_BASE, (WORD)ival);
           printf("Wrote %lu ; read back %d\n",ival,data);
          break;

         case ('W'):
            data = gglReadPulserEnable (myvme, GGL_BASE);
            printf("Read back %d\n",data);
            break;


         case ('Y'):
             printf("Enter value for pulser High register (ms):  0x");
             scanf("%lx",&ival);

	     data = gglWritePulserHigh (myvme, GGL_BASE, (WORD)ival );
	     printf("Wrote 0x%lx; Read back 0x%x from Pulser High\n",ival,data);

            printf("Enter value for pulser Low register: 0x");
             scanf("%lx",&ival);
	     data = gglWritePulserLow (myvme, GGL_BASE, (WORD)ival );
	     printf("Wrote 0x%lx; Read back 0x%x from Pulser Low\n",ival,data);
            break;

         case ('Z'):
	     data = gglReadPulserHigh (myvme, GGL_BASE );
             ival = gglReadPulserLow (myvme, GGL_BASE  );
             printf("Read back 0x%lx from Pulser Low and 0x%x from Pulser High\n",ival,data);
             break;

         case ('1'):
	   printf("DAC ranges : 0:   0  to +5V\n");
	   printf("             1:   0  to +10V\n");
	   printf("		2:  -5  to +5V\n");
	   printf("             3: -10  to +10V\n");
	   printf("		4: -2.5 to +2.5V\n");
	   printf("		5: -2.5 to +7.5V\n"); 
           printf("Enter DAC range (0-5): ");
           scanf("%d",&range);
           if(ival < 0) ival=0;
           if(ival > 5) ival=5;
           data = gglWriteDacRange(myvme, GGL_BASE, range );    
           printf("Wrote DAC Range = %d; Read back %d\n",range,data);
           break;

         case ('2'):
           range = gglReadDacRange(myvme, GGL_BASE );
           printf("Read DAC Range: %d\n",range);
           break;

         case ('3'):

           printf("Enter DAC Voltage (float) :  ");
           scanf("%f",&volts);
           range = gglReadDacRange(myvme, GGL_BASE ); // read DAC range
           counts = GetDacCounts (volts, range);
           if (counts != -99)
           {
              data = gglWriteDacSetpoint(myvme, GGL_BASE,  counts);
              if(ddd) printf ("Set DAC counts = %d, read back %d counts\n",counts,data);
           }
           else
	     printf("Error return from GetDacCounts. Check voltage in range (range=%d)\n",range);
           break;

         case ('4'):
            range = gglReadDacRange(myvme, GGL_BASE );
            data  =  gglReadDacSetpoint(myvme, GGL_BASE );
            status = GetDacVoltage(data, range, &DacVoltage);
            if(status != -99)
               printf("DAC: Range %d, counts=%d(0x%x)  %.5f Volts\n",range,data,data,DacVoltage );
            else
	      printf("gglReadAll: Error return from GetDacVoltage; may be out of range (range = %d)\n",range);
            break;

         case ('D'):
          if (ddd)
            ddd=0;
          else
            ddd=1;
          break;
        case ('P'):
          GGLCmds();
          break;
        case ('H'):
          ggl();
        case ('X'):
        case ('Q'):
	  status = mvme_close(myvme);
	  return 1;
        default:
          break;
        }
    }


  status = mvme_close(myvme);
  return 1;
}
/*------------------------------------------------------------------*/
void  GGLCmds(void)
{
  // print brief list of commands
  printf("\nList of Commands: \n");
  printf("        A Reset               B Init  \n");
  printf("        S Status              D Debug  \n");  
  printf("        C WriteReg8           E ReadReg8  \n");
  printf("        F WriteReg            G ReadReg  \n");
  printf("  Delays: \n");
  printf("        I ReadDelays          J WriteDelta\n");
  printf("        K WriteDelta1         L WriteDelta2\n");
  printf("  Down Counter: \n");
  printf("        M WriteDownPreset     N ReadDownPreset\n");
  printf("        O ReadDownCounter     R ResetDownCounter\n");
  printf("  Sample/Reference : \n");
  printf("        T Write Sample/Ref    U Read Sample/Ref   \n");
  printf("  Pulser : \n");
  printf("        V Write En/Dis Pulser W Read Pulser En/Dis   \n");
  printf("        Y Write Pulser Hi/Lo  Z Read Pulser Hi/Lo   \n");
  printf("  DAC : \n");
  printf("        1 Select DAC Range    2 Read DAC Range   \n");
  printf("        3 Select DAC Voltage  4 Read DAC Voltage   \n");
  printf(" \n");
  printf("        H Help                P Print this list \n");
  printf("        X Exit                Q Quit\n");
  printf("\n");
}
/*------------------------------------------------------------------*/
void ggl(void)
{
  printf("\nGATE GENERATOR LOGIC BOARD CONTROL:\n");
  printf("     Basic Routines:\n");
  printf("gglRegWrite8(mvme, ggl_base, reg, value)\n");    /* write (byte) */
  printf("gglRegRead8(mvme, ggl_base, reg)\n");      /* read (byte) */
  printf("gglRegWrite(mvme, ggl_base, reg, value)\n");     /* write (word) */
  printf("gglRegRead(mvme, ggl_base, reg)\n\n");       /* read (word) */

 
  printf("     Delays:\n");
  printf("gglReadDelays( mvme, ggl_base)\n"); /* read all Delta Delay regs */

#ifdef DEBUG   /* we don't need the values in ns after all */
  /* write delays in ns */
  printf("gglWriteDelta_ns( mvme, ggl_base, value_ns)\n");
  printf("gglWriteDelta1_ns( mvme, ggl_base, value_ns)\n"); 
  printf("gglWriteDelta2_ns( mvme, ggl_base, value_ns)\n");
#endif

  /* write delays as counts */
  printf("gglWriteDelta( mvme, ggl_base, counts)\n");
  printf("gglWriteDelta1( mvme, ggl_base, counts)\n");
  printf("gglWriteDelta2( mvme, ggl_base, counts)\n");

  printf("    Down  Counter:\n");
  printf("gglReadPreset(mvme, ggl_base)\n");   /* read preset of Down counter reg (32 bits)*/
  printf("gglWritePreset ( mvme, ggl_base,value)\n");/* write preset of Down counter
							   (32 bits) */
  printf("gglReadbackCounter(mvme, ggl_base)\n");   /* read Down counter
							  readback reg (32
							  bits)*/  
  printf("gglResetCounter(mvme, ggl_base)\n");   /* Reset Down counter to preset value */   
    
  printf("     Sample/Reference:\n");
  printf("gglWriteSR ( mvme, ggl_base,value)\n");
  printf("gglReadSR ( mvme, ggl_base)\n\n");

  printf("     General:\n");
  printf("gglReset(mvme, ggl_base)\n");   /* Reset */ 
  printf("gglInit(mvme, ggl_base)\n");   /* Initialize to some standard values */
  printf("gglReadAll( mvme, ggl_base)\n");       /* Readback all registers */
 
  printf("\n For Pulser and DAC routines, use ggl_more()\n");
}
/*------------------------------------------------------------------*/
void ggl_more()
{
  printf("      Pulser:\n");

  printf("gglWritePulserEnable ( mvme, ggl_base, value [enab/disab 1/0] )\n");
  printf("gglReadPulserEnable ( mvme, ggl_base)\n");  
 
  printf("gglWritePulserHigh ( mvme, ggl_base, value)\n");
  printf("gglWritePulserLow  ( mvme, ggl_base, value)\n");
  
  printf("gglReadPulserHigh ( mvme, ggl_base)\n");
  printf("gglReadPulserLow  ( mvme, ggl_base)\n");
  
  printf("GetPulserCounts((float)time_ms)\n");
  printf("GetPulserTime_ms(counts)\n");



  printf("      DAC:\n");
  printf("DAC range :   0:   0 to +5V;   1:     0 to +10V;   2: -5   to +5V;\n");
  printf("   0-5        3: -10 to +10V;  4:  -2.5 to +2.5V;  5: -2.5 to +7.5V;\n");

  printf("gglWriteDacRange ( mvme, ggl_base, DAC range )\n");
  printf("gglReadDacRange  ( mvme, ggl_base)\n");

  printf("gglWriteDacSetpoint ( mvme, ggl_base, value)\n");
  printf("gglReadDacSetpoint  ( mvme, ggl_base)\n");

  printf("GetDacCounts (voltage, DAC range)\n");
  printf("GetDacVoltage(count,  DAC range &volt)\n");
  printf("GetDacRange(voltage1,voltage2)  [float voltages] \n");
  printf("TestGetDacRange(volt1,volt2) [int voltages] \n");
}
#endif // MAIN

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
