/*-----------------------------------------------------------------------------
 * Copyright (c) 2002      TRIUMF Data Acquistion Group
 * Please leave this header in any reproduction of that distribution
 * 
 * TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 * Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *        amaudruz@triumf.ca
 * ----------------------------------------------------------------------------
 *  
 * Description	: Header file for Gate Generator Logic Module
 *                VMIC version
 *
 * Author: Suzannah Daviel, TRIUMF
 *---------------------------------------------------------------------------*
 *Id:$
 *---------------------------------------------------------------------------*/
#ifndef _GGL_INCLUDE_
#define _GGL_INCLUDE_

#include <stdio.h>
#include <string.h>
#include <ctype.h>



#ifdef __cplusplus
extern "C" {
#endif

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef long unsigned int  DWORD;
typedef long unsigned int  BOOL;
#define SUCCESS 1
#endif /* MIDAS_TYPE_DEFINED */

#include "mvmestd.h"
#include "vmicvme.h"

//#define GGL_BASE 0x8000 /* set with Jumpers A15-4 on GGL board */

/* GGL register offset defines  (Rev 0)

          register                   offset     bits     access     
            name                                used     type        */
#define GGL_DELTA_DELAY_RW          0x00000   /* 11     word    RW  */
#define GGL_DELTA1_DELAY_RW         0x00002   /*  7     byte    RW  */
#define GGL_DELTA2_DELAY_RW         0x00003   /*  7     byte    RW  */
#define GGL_SR_ENABLE_RW            0x00004   /*  3     byte    RW  */
#define GGL_DAC_RANGE_RW            0x00005   /*  3     byte    RW  */
#define GGL_DAC_SETPOINT_RW         0x00006   /* 16     word    RW  */

#define GGL_PRESET_HI_RW            0x00008   /* 16     word    RW  */ 
#define GGL_PRESET_LO_RW            0x0000A   /* 16     word    RW  */
#define GGL_COUNTER_READBACK_HI_RO  0x0000C   /* 16     word    R   */
#define GGL_COUNTER_READBACK_LO_RO  0x0000E   /* 16     word    R   */

#define GGL_PULSER_HI_SETP_RW       0x00010   /* 16     word    RW  */
#define GGL_PULSER_LO_SETP_RW       0x00012   /* 16     word    RW  */
#define GGL_PULSER_ENABLE_RW        0x00014   /* 1      byte    RW  */

#define GGL_RESET_COUNTER_RW        0x0001D   /*  -     byte    RW  */
#define GGL_RESET_WO                0x0001F   /*  -     byte    W  */

/* bit masks for above registers */
#define DELTA_DELAY_MSK         0x07FF   /* 11 bits */
#define DELTA8_DELAY_MSK        0x007F   /* 7  bits */
#define SR_ENABLE_MSK           0x0007   /* 3 bits  */
#define PRESET_HI_MSK           0xFFFF   /* 16 bits */
#define PRESET_LO_MSK           0xFFFF   /* 16 bits */
#define DAC_RANGE_MSK           0x0007   /* 3  bits */
#define PULSER_ENABLE_MSK       0x0001   /* 1  bit  */


#define MIN_VALUE               2

/* trGGL.c prototypes:    */


INT  gglRegRead(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, DWORD reg_offset);
INT  gglRegWrite(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, DWORD reg_offset, const WORD value);

INT  gglRegWrite8(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const DWORD reg_offset, const BYTE value);
INT  gglRegRead8(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, DWORD reg_offset);

  DWORD  gglReset(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
  DWORD  gglResetCounter(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
  void gglInit(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
  void gglReadAll(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);

  INT gglWriteSR(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value);
  INT gglReadSR(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);

  INT gglWriteDelta (MVME_INTERFACE *mvme,const DWORD ggl_base_adr, const WORD value);
  INT gglWriteDelta1(MVME_INTERFACE *mvme,const DWORD ggl_base_adr, const WORD value);
  INT gglWriteDelta2(MVME_INTERFACE *mvme,const DWORD ggl_base_adr, const WORD value);

  INT gglReadDelays(MVME_INTERFACE *mvme,const DWORD ggl_base_adr);
  DWORD gglReadbackCounter(MVME_INTERFACE *mvme,const DWORD ggl_base_adr);
  DWORD gglReadPreset(MVME_INTERFACE *mvme,const DWORD ggl_base_adr);
  DWORD gglWritePreset(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const DWORD value);


INT gglWritePulserEnable(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value);
INT gglReadPulserEnable(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
INT gglWritePulserHigh(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value);
INT gglWritePulserLow(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value);
INT gglReadPulserHigh(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
INT gglReadPulserLow(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
float GetPulserTime_ms(INT counts);
INT GetPulserCounts(float time_ms);
INT gglWriteDacSetpoint(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const DWORD value);
INT gglReadDacSetpoint(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
INT gglReadDacRange(MVME_INTERFACE *mvme, const DWORD ggl_base_adr);
INT gglWriteDacRange(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value);
INT   GetDacCounts(float voltage, INT range);
INT   GetDacVoltage(INT count, INT range, float *pvoltage );
INT   GetDacRange (float volt1, float volt2);


#ifdef DEBUG
INT gglWriteDelta_ns (MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value_ns);
INT gglWriteDelta1_ns(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value_ns);
INT gglWriteDelta2_ns(MVME_INTERFACE *mvme, const DWORD ggl_base_adr, const WORD value_ns);
#endif

#ifdef MAIN_ENABLE
int main (int argc, char* argv[]);
void ggl(void);
void ggl_more(void);
void GGLCmds(void);
#endif

#ifdef __cplusplus
}
#endif

#endif  //  _GGL_INCLUDE_

/* emacs                                                                                                             
 * Local Variables:
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */





