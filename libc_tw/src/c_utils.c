/*
 *  c_utils.c -- some handy things
 *
 *  Revision history:
 *   v1.0  03-Feb-1994  TW  Initial version
 *   v1.1  12-Mar-1994  TW  Added mailbox utils
 *   v2.0  07-Jul-1994  TW  Separated from vaxc_utils.c
 *         09-Oct-1995  TW  Added reentrant versions of some time routines
 *                          warning: need source code for localtime_r
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "c_utils.h"

#ifdef VMS
#define bcopy(src,dest,len) memcpy(dest,src,len)
#define bcmp(src,dest,len)  memcmp(src,dest,len)
#define bzero(src,len)      memset(src,0,len)
#endif /* VMS */


      /*----------------------------------------*/
     /*  Memory allocation routines            */
    /*----------------------------------------*/

caddr_t
zalloc( size_t size )
{
    caddr_t ptr;

    if( ( ptr = (caddr_t)malloc( size ) ) == NULL ) 
    {
	perror( "malloc" );
/*    	return( NULL ); */
        exit( ENOMEM );
    }
    bzero( ptr, size );
    return( ptr );
}


caddr_t
rezalloc( caddr_t ptr, size_t size )
{
    caddr_t ptr2;

    if( ( ptr2 = (caddr_t)realloc( ptr, size ) ) == NULL ) 
    {
	perror( "realloc" );
/*    	return( NULL ); */
        exit( ENOMEM );
    }
    return( ptr2 );
}


      /*----------------------------------------*/
     /*  String handling routines              */
    /*----------------------------------------*/

/*
 *  string equality 
 *
 *    Returns 1 (true) or 0 (false)
 */
int 
streq( const char* a, const char* b )
{
    return( strcmp( a, b ) == 0 );
}


/*
 *----------------------------------------------------------------------
 *
 * strmatch --  Adapted from Tcl_StringMatch
 *              (added % character for VMS)
 *
 *	See if a particular string matches a particular pattern.
 *
 * Results:
 *	The return value is 1 if string matches pattern, and
 *	0 otherwise.  The matching operation permits the following
 *	special characters in the pattern: *?\[] (see the manual
 *	entry for details on what these mean).
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

int
strmatch( const char *string, const char *pattern )
{
    char c2;

    while (1) {
	/* See if we're at the end of both the pattern and the string.
	 * If so, we succeeded.  If we're at the end of the pattern
	 * but not at the end of the string, we failed.
	 */
	
	if (*pattern == 0) {
	    if (*string == 0) {
		return 1;
	    } else {
		return 0;
	    }
	}
	if ((*string == 0) && (*pattern != '*')) {
	    return 0;
	}

	/* Check for a "*" as the next pattern character.  It matches
	 * any substring.  We handle this by calling ourselves
	 * recursively for each postfix of string, until either we
	 * match or we reach the end of the string.
	 */
	
	if (*pattern == '*') {
	    pattern += 1;
	    if (*pattern == 0) {
		return 1;
	    }
	    while (1) {
		if (strmatch(string, pattern)) {
		    return 1;
		}
		if (*string == 0) {
		    return 0;
		}
		string += 1;
	    }
	}
    
	/* Check for a "?" as the next pattern character.  It matches
	 * any single character.
	 */

	if (*pattern == '?') {
	    goto thisCharOK;
	}

#ifdef VMS
	/* 
    	 * Check for a "%" as the next pattern character.  It matches
	 * any single character.
	 */

	if (*pattern == '%') {
	    goto thisCharOK;
	}
#endif /* VMS */

	/* Check for a "[" as the next pattern character.  It is followed
	 * by a list of characters that are acceptable, or by a range
	 * (two characters separated by "-").
	 */
	
	if (*pattern == '[') {
	    pattern += 1;
	    while (1) {
		if ((*pattern == ']') || (*pattern == 0)) {
		    return 0;
		}
		if (*pattern == *string) {
		    break;
		}
		if (pattern[1] == '-') {
		    c2 = pattern[2];
		    if (c2 == 0) {
			return 0;
		    }
		    if ((*pattern <= *string) && (c2 >= *string)) {
			break;
		    }
		    if ((*pattern >= *string) && (c2 <= *string)) {
			break;
		    }
		    pattern += 2;
		}
		pattern += 1;
	    }
	    while ((*pattern != ']') && (*pattern != 0)) {
		pattern += 1;
	    }
	    goto thisCharOK;
	}
    
	/* If the next pattern character is '/', just strip off the '/'
	 * so we do exact matching on the character that follows.
	 */
	
	if (*pattern == '\\') {
	    pattern += 1;
	    if (*pattern == 0) {
		return 0;
	    }
	}

	/* There's no special character.  Just make sure that the next
	 * characters of each string match.
	 */
	
	if (*pattern != *string) {
	    return 0;
	}

	thisCharOK: pattern += 1;
	string += 1;
    }
}


#ifndef linux

/*
 *  malloc and duplicate a string
 */
char*
strdup( const char* str )
{
/*
    char* buf;

    buf = (char*)malloc( strlen( str ) + 1 );
    strcpy( buf, str );
    return( buf );
*/
   return( strcpy( (char*)malloc( strlen( str ) + 1 ), str ) );
}

#endif /* linux */

/*
 *  malloc and duplicate part of a string
 */
char*
strndup( const char* str, size_t max_len )
{
    char* buf;
    int len;

    len = _min( max_len, strlen( str ) );
    buf = (char*)malloc( len + 1 );
    strncpy( buf, str, len );
    buf[len] = '\0';
    return( buf );
}

/*
 *  stoupper
 *  uppercase a character string
 */
char*
stoupper( char* inString )
{
    char *pos = inString;
    while( *pos = toupper( *pos ) ) pos++;
    return( inString );
}


/*
 *  stolower
 *  lowercase a character string
 */
char*
stolower( char* inString )
{
    char *pos = inString;
    while( *pos = tolower( *pos ) ) pos++;
    return( inString );
}


char*
stoprint( const char* str_in, char* str_out )
{
    const char* p_in;     
    char* p_out;

    for( p_in = str_in, p_out = str_out; *p_in != '\0'; p_in++, p_out++ ) 
    {
	*p_out = ( isprint( *p_in ) ) ? *p_in : ' ';
    }
    *p_out = '\0';

    return( str_out );
}


char*
stoprint_expand( const char* str_in, char* str_out )
{
    const char* p_in;     
    char* p_out;

    for( p_in = str_in, p_out = str_out; *p_in != '\0'; p_in++ ) 
    {
	if( !isascii( *p_in ) )
	{
	    sprintf( p_out, "\\%03o", (unsigned char)*p_in );
	    p_out += 4;
	}
	else if( !isprint( *p_in ) )
	{
	    sprintf( p_out, "\\%03o", (unsigned char)*p_in );
	    p_out += 4;
	}
	else if( *p_in == '\\' )
	{
	    sprintf( p_out, "\\\\", *p_in );
	    p_out += 2;
	}
	else
	{
	    *p_out = *p_in;
	    p_out++;
	}
    }
    *p_out = '\0';

    return( str_out );
}


/* 
 *  skipBlanks() - return a pointer to the first non-blank in the string
 */
char*
skipBlanks( char *ptr )
{
    while( ( *ptr != '\0' ) && !isgraph( *ptr ) ) 
    {
    	ptr++;
    }
    return( ptr );
}


/* 
 *  trimBlanks() - return a string with no leading or trailing blanks
 */
char*
trimBlanks( char *inStr, char* outStr )
{
    char* p1;
    char* p2;
    int len;

    p1 = skipBlanks( inStr );
    if( *p1 == '\0' )
    {
    	outStr[0] = '\0';
    	return( outStr );
    }

    for( p2 = &inStr[strlen(inStr)-1]; ( p2 > p1 ) && !isgraph( *p2 ); p2-- );
    len = p2 - p1 + 1;
    strncpy( outStr, p1, len );
    outStr[len] = '\0';

    return( outStr );
}


void
padString( char* s, char c, int len )
{
    int slen = strlen( s );
    memset( &s[slen], c, len-slen );
}


      /*----------------------------------------*/
     /*  Miscellaneous routines                */
    /*----------------------------------------*/


/*
 *  error() - Print an error message
 */
void 
error( char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );
    fprintf( stderr, "error: " );
    vfprintf( stderr, fmt, args );
    fprintf( stderr, "\n" );
    va_end( args );
}


/*
 *  fatal() - Print an error message and die
 */
void 
fatal( char* fmt, ... )
{
    va_list args;

    va_start( args, fmt );
    fprintf( stderr, "fatal: " );
    vfprintf( stderr, fmt, args );
    fprintf( stderr, "\n" );
    va_end( args );
    exit( errno );
}


/*
 *  Return the filename from a full path
 *
 *    basename --> basename_tw for linux
 */
char *
basename_tw( const char* file_in, char* file_out )
{
    file_nodir( file_in, file_out );
    // return( file_nover( file_out, file_out ) );
    // to get around benign valgrind errors:
    // why are we still handling VMS versions anyway?
    return( file_nover2( file_out ) );
}


char *
file_nodir( const char* file_in, char* file_out )
{
    char *p1, *p2;

    p1 = strrchr( file_in, ':' );	/* VMS */
    p2 = strrchr( file_in, ']' );	/* VMS */
    p1 = _max( p1, p2 );
    p2 = strrchr( file_in, '>' );	/* VMS */
    p1 = _max( p1, p2 );
    p2 = strrchr( file_in, '/' );	/* UNIX */
    p1 = _max( p1, p2 );
    p2 = strrchr( file_in, '\\' );	/* MS-DOS */
    p1 = _max( p1, p2 );

    if( p1 )
    {
      strcpy( file_out, p1+1 );
    }
    else
    {
      strcpy( file_out, file_in );
    }
    return( file_out );
}


/*
 *  Return the directory name
 */
char *
file_dir( const char* file_in, char* file_out )
{
    char *p1, *p2;

    strcpy( file_out, file_in );

    p1 = file_out;
    p2 = strrchr( file_out, ':' )+1;	/* VMS */
    p1 = _max( p1, p2 );
    p2 = strrchr( file_out, ']' )+1;	/* VMS */
    p1 = _max( p1, p2 );
    p2 = strrchr( file_out, '>' )+1;	/* VMS */
    p1 = _max( p1, p2 );
    p2 = strrchr( file_out, '/' )+1;	/* UNIX */
    p1 = _max( p1, p2 );
    p2 = strrchr( file_out, '\\' )+1;	/* MS-DOS */
    p1 = _max( p1, p2 );

    *p1 = '\0';

    return( file_out );
}


char*
file_nover( const char* file_in, char* file_out )
{
    char* p;

    if( ( p = strrchr( file_in, ';' ) ) != NULL )
    {
	strncpy( file_out, file_in, p-file_in );
	file_out[p-file_in] = '\0';
    }
    else
	strcpy( file_out, file_in );

    return( file_out );    
}


/*
 *  20140318  TW  get around valgrind complaints of source-destination overlap
 */
char*
file_nover2( char* file )
{
    char* p = strrchr( file, ';' );

    if( p != NULL )
    {
	*p = '\0';
    }

    return( file );    
}


char*
file_noext( const char* file_in, char* file_out )
{
    char* p;

    if( ( p = strrchr( file_in, '.' ) ) != NULL )
    {
	strncpy( file_out, file_in, p-file_in );
	file_out[p-file_in] = '\0';
    }
    else
	strcpy( file_out, file_in );

    return( file_out );    
}


