#ifndef _C_UTILS_H_
#define _C_UTILS_H_

/*
 *  c_utils.h,  Defines for C utilities
 *
 *  Modified
 *    19-jul-1999 Renee Poutissou - adapt for Linux
 *    20140318  TW  file_nover2, remove defined sizes
 */
#include <stddef.h>

#if !defined( VXWORKS ) && !defined( linux )

#if !defined(__CADDR_T) && !defined(CADDR_T) 
#ifndef __CADDR_T
#define __CADDR_T
#endif /* !__CADDR_T */
#ifndef CADDR_T
#define CADDR_T
#endif /* !CADDR_T */
typedef char *  caddr_t;
#endif  /* !__CADDR_T && !CADDR_T */

#endif /* !VXWORKS && !linux */

// #define  MAXSTRING		256
// #define  MAX_FILES		256
// #define  LEN_TIME_STRING        14

#define _max( a, b )			( ( (a) > (b) ) ? (a) : (b) ) 
#define _min( a, b )			( ( (a) < (b) ) ? (a) : (b) ) 

#define _free(objp)			if((void*)(objp)!=(void*)NULL){free((void*)(objp));objp=NULL;}

#define _strlen( s )			((s!=NULL) ? strlen( s ) : 0 )

#define	 _swap32( l )			(((unsigned long)l>>16)+((unsigned long)l<<16))
#define	 _swap16( s )			(((unsigned short)s>>8)+((unsigned short)s<<8))

#define  _roundUp( n, r )		( (r) * (int)( ((n)+(r)-1) / (r) ) ) 

/* c_utils.c */
caddr_t zalloc ( size_t size );
caddr_t rezalloc ( caddr_t ptr , size_t size );
int streq ( const char *a , const char *b );
int strmatch( const char *string, const char *pattern );
#ifndef linux
char *strdup ( const char *str );
#endif /* linux */
char *strndup ( const char *str, size_t max_len );
char *stoupper ( char *inString );
char *stolower ( char *inString );
char *stoprint ( const char *str_in , char *str_out );
char *skipBlanks ( char *ptr );
char *trimBlanks ( char *inStr , char *outStr );
void padString ( char *s , char c , int len );
void error ( char *fmt , ...);
void fatal ( char *fmt , ...);
char *basename_tw ( const char *file_in , char *file_out );
char *file_nodir ( const char *file_in , char *file_out );
char *file_dir ( const char *file_in , char *file_out );
char *file_nover ( const char *file_in , char *file_out );
char *file_nover2 ( char *file_out ); // 20140318  TW
char *file_noext ( const char *file_in , char *file_out );

#endif



