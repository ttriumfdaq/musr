/*
 *  timeval.c - routines for timeval_t 
 *
 *  Revision history:
 *   21-Apr-1994  TW  Added asctimeval
 *   06-Jun-1994  TW  Added tvtof
 *   09-Oct-1995  TW  Added asctimeval_t, reentrant version of asctimeval
 *   19-jul-1999 Renee P. Add linux support
 * $Log: timeval.c,v $
 * Revision 1.3  2015/03/13 23:26:51  suz
 * Ted's changes
 *
 * Revision 1.2  2000/10/11 04:24:43  midas
 * added cvs log
 *
 */


#include <stddef.h>
#if defined( VXWORKS )
#include <vxWorks.h>
#include <sys/times.h>
#include <timers.h>
#elif defined( VMS )
struct timeval {
        long    tv_sec;         /* seconds */
        long    tv_usec;        /* and microseconds */
};
#include <timeb.h>
#elif defined( linux )
// 20140407  TW  for goodness sake don't define struct timeval here
/* struct timeval { */
/*         long    tv_sec;         /\* seconds *\/ */
/*         long    tv_usec;        /\* and microseconds *\/ */
/* }; */
#include <time.h>
// #include <sys/timeb.h>
#else // other unix ?
#include <time.h>
#include <sys/timeb.h>
#endif
#include "timeval.h"

void
cleartimeval( timeval_t *pt )
{
    pt->tv_sec = 0;
    pt->tv_usec = 0;
}

void
gettimeval( timeval_t *pt )
{
#if defined( VXWORKS )
    struct timespec ts;

    clock_gettime( CLOCK_REALTIME, &ts );
    pt->tv_sec = (long)ts.tv_sec;
    pt->tv_usec = (long)ts.tv_nsec/1000;
#elif defined( linux )
    gettimeofday( pt, NULL ); // 20140407  TW
#else // vms, other unix 
    struct timeb tb;

    ftime( &tb );
    pt->tv_sec = (long)tb.time;
    pt->tv_usec = (long)(tb.millitm*1000);
#endif /* VXWORKS */
}

double
difftimeval( timeval_t *pt1, timeval_t *pt0 )
{
    /*
     *  be very careful to explicitly convert to double -
     *  the default is float, and it seems that the 
     *  implicit conversion from float to double doesn't work !
     */
    return( ((double)(pt1->tv_sec - pt0->tv_sec)) + 
	    ((double)(1.0e-6))*((double)(pt1->tv_usec - pt0->tv_usec)) );
}

void
copytimeval( timeval_t *pt1, timeval_t *pt2 )
{
    pt2->tv_sec = pt1->tv_sec;
    pt2->tv_usec = pt1->tv_usec;
}

char* 
asctimeval( timeval_t* pt )
{
    return( ctime( &pt->tv_sec ) );
}

#ifdef VXWORKS
/*
 *  Reentrant version of asctimeval
 *  depends on availability of ctime_r()
 */
int
asctimeval_r( timeval_t* pt, char* buf, size_t* pBuflen )
{
    return( strlen( ctime_r( &pt->tv_sec, buf, pBuflen ) ) );
}
#endif /* VXWORKS */

double
tvtof( timeval_t* ptv )
{
    return( (double)ptv->tv_sec + 1e-6*((double)ptv->tv_usec) );
}


