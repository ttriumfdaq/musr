/*
  $Log: timeval.h,v $
  Revision 1.3  2015/03/13 23:26:40  suz
  Ted's changes

  Revision 1.2  2000/10/11 04:24:43  midas
  added cvs log

*/
#ifndef _TIMEVAL_H_
#define _TIMEVAL_H_

#include <stddef.h>
#if defined( linux )
#include <sys/time.h>
#endif // linux
typedef struct timeval timeval_t;

/* timeval.c */
void cleartimeval ( timeval_t *pt );
void gettimeval ( timeval_t *pt );
double difftimeval ( timeval_t *pt1 , timeval_t *pt0 );
void copytimeval ( timeval_t *pt1 , timeval_t *pt2 );
char* asctimeval ( timeval_t* pt );
int asctimeval_r ( timeval_t *pt, char* buf, size_t* pBuflen );
double tvtof ( timeval_t *ptv );

#endif
