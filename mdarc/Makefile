#
# Makefile for mdarc - based on VMIC MUSR TD/Integral and BNMR/BNQR  combined version
#
#
## This version for VMIC MUSR only
#
#  $Log: Makefile,v $
#  Revision 1.25  2015/03/24 20:01:05  suz
#  Initial MUSR VMIC Version; runs with Ted's new CAMP package
#
#  Revision 1.24  2010/02/24 00:49:28  suz
#  remove local experim.h
#
#  Revision 1.23  2008/12/10 19:26:30  suz
#  link with efence and debug (Donald); use MHEADER to supply name of mheader file
#
#  Revision 1.22  2008/04/16 18:59:18  suz
#  add listing option
#
#  Revision 1.21  2007/09/21 22:15:32  suz
#  Midas 2.0.0; ca_epics from Midas area
#
#  Revision 1.20  2007/07/30 17:24:50  suz
#  add Epics and link with C++
#
#  Revision 1.19  2007/05/04 21:27:50  suz
#  add -m32 compiler flag for Midas compatibility when cpu changed to 64bit
#
#  Revision 1.18  2005/01/27 19:33:48  suz
#  Remove MIDAS_VERSION for older Midas, as now using Midas 1.9.5
#
#  Revision 1.17  2004/10/05 22:44:48  suz
#  use libmidas.so (-lmidas); previously both libmidas.a and libmidas.so were specified
#
#
#


ARCH = LINUX

# Build this on host machine (not VMIC)
ifneq (,$(findstring lx,$(HOST)))
# wrong host (i.e. a lx... machine)
$(error Error - wrong host "$(HOST)" - Build these frontends on linux box (not VMIC))
endif

# The MIDASSYS should be defined prior the use of this Makefile
ifndef MIDASSYS
missmidas::
	@echo "...";
	@echo "Missing definition of environment variable 'MIDASSYS' !";
	@echo "...";
endif


# BEAMLINE must be defined (e.g. in .login)
# It can be dev/m15/m20/m9b

ifeq ($(BEAMLINE),)
$(error Error - environment variable "BEAMLINE" is not defined)
endif

# check validity of beamline & assign TYPE as musr (or bnmr)
# so  bnmr or bnqr     -> type=bnmr
# and m15,m9b,m20,dev  -> type=musr

ifneq (,$(findstring bn,$(BEAMLINE)))
TYPE = bnmr
$(error Error - this version for MUSR only )
else
ifneq (,$(findstring m,$(BEAMLINE)))
TYPE = musr
else
ifneq (,$(findstring dev,$(BEAMLINE)))
TYPE = musr
else

$(error Error - environment variable "BEAMLINE" ($(BEAMLINE)) is not one of dev/m9b/m15/m20 (lower case))
endif

endif
endif

# compilers  C and C++  - vmic remove -m32
CC     = gcc  -DOS_LINUX -pthread 
CP = g++ 
#EFENCE = -lefence   not installed at the moment
EFENCE =

# location of MUD and CAMP in standard MUSR area
#MUSR_DIR = /home/musrdaq/vmic_musr
MUSR_STD =$(MUSR_DIR)

MY_DIR = .


MDARC_DIR = $(MUSR_DIR)/mdarc
SRC_DIR     = $(MDARC_DIR)/src

# The following four directories may be supplied by a calling Makefile
# BEAMLINE_DIR contains experim.h
ONLINE_DIR =  /home/$(BEAMLINE)/online
BEAMLINE_DIR =  $(ONLINE_DIR)/$(BEAMLINE)
BIN_DIR =  $(MDARC_DIR)/bin
OBJ_DIR     = $(MDARC_DIR)/lib

INSTALL_DIR =
# includes
MIDAS_INC   =  $(MIDASSYS)/include
TCL_INC     = /usr/include
LIBC_TW_INC = $(MUSR_DIR)/libc_tw/src
CAMP_INC    = $(MUSR_DIR)/camp/src
MUD_INC     = $(MUSR_DIR)/mud/src
MUDUTIL_INC = $(MUSR_DIR)/mud/util

#libraries
LIBC_DIR  = $(MUSR_DIR)/libc_tw/$(LINUX)
MIDAS_LIB =  $(MIDASSYS)/$(LINUX)/lib
TCL_LIB   = /usr/lib      
CAMP_LIB  = $(MUSR_DIR)/camp/$(LINUX)
MUD_LIB   = $(MUSR_DIR)/mud/lib
TW_LIB    = $(MUSR_DIR)/libc_tw/$(LINUX)


EPICS_DIR       = /home/midas/base/lib/Linux
EPICS_INC       = /home/midas/base/include
EPICS_DRV_DIR   = $(MIDASSYS)/drivers/device



ifeq ($(TYPE),musr)
# dev,m9b,m15 or m20
# uses standard musrdaq area to build everything

# no EPICS for MUSR
EPICS_DIR       = 
EPICS_INC       =
EPICS_DRV_DIR   = 


MY_DEFINES = -D MUSR
MY_SOURCES = $(SRC_DIR)/mdarcMusr.c  $(SRC_DIR)/bnmr_darc.c
MY_OBJS =  $(OBJ_DIR)/mdarcMusr.o  $(OBJ_DIR)/bnmr_darc.o  $(OBJ_DIR)/mdarc_subs.o
DRIVER = 
MHEADER = mheaderMusr
DRV_DIR   =  $(MIDASSYS)/drivers/bus
LINK = $(CC) -lrt
endif
#
# CAMP direct access defined for all beamlines (needed by mheader)
DEFINES = -D CAMP_ACCESS $(MY_DEFINES)

#
# NOTE experim.h
# *** There must be no experim.h present in SRC_DIR  *** 
#    it must be present in the directory defined by 
#    EXPERIM_INC. To make experim.h, use "make" from odbedit
EXPERIM_INC = $(BEAMLINE_DIR)



#-------------------------------------------------------------------
# Hardware driver can be (camacnul, kcs2926, kcs2927, hyt1331)
#   needed for mheader that links with mfe.o

#--------------------------------------------------------------
INCLUDES = -I $(MIDAS_INC) -I $(EXPERIM_INC) -I $(SRC_DIR) -I $(LIBC_TW_INC) -I $(CAMP_INC) -I $(MUD_INC) \
	-I $(MUDUTIL_INC) -I $(MUSR_DIR)/mtrpc/src
# -lm (include math library)
#LIBRARIES   = -lm -lutil -lnsl -lmidas -lc_tw -lmud $(CAMP_LIB)/libcampclnt.a
#LIBRARIES   = -lm -lutil -lnsl -lmidas -lc_tw -lmud $(CAMP_LIB)/camp_clnt.a
LIBRARIES   = $(CAMP_LIB)/$(CAMP_CLIENT_NAME) $(MUSR_DIR)/mtrpc/linux/libmtrpc.a -lm -lutil -lnsl -lmidas -lc_tw -lmud 

LIBS =	 -L$(TW_LIB) -L$(MUD_LIB) -L$(CAMP_LIB) -L$(MIDAS_LIB)
FLAGS = -I. -I$(DRV_DIR) $(EPICS_INCLUDES)  $(INCLUDES)
#
# C compiler flags
DEBUG  = -g
CFLAGS = $(DEBUG) $(DEFINES) $(FLAGS) -fcommon
#
# C++ compiler flags (EPICS libs is in C++)
CPFLAGS = -g $(FLAGS)
LDFLAGS =

# Targets
#
PROGS = $(BIN_DIR)/mdarc $(BIN_DIR)/cleanup $(BIN_DIR)/mheader

# Common between mdarc (BNMR and MUSR) and cleanup or mheader
COMMON_SOURCES =  $(SRC_DIR)/darc_files.c \
	$(SRC_DIR)/mdarc_subs.c  $(SRC_DIR)/client_flags.c
COMMON_OBJS =  $(OBJ_DIR)/darc_files.o \
	$(LIBC_DIR)/c_utils.o $(LIBC_DIR)/timeval.o 

# mdarc's sources and objects
OBJS =  $(MY_OBJS) $(COMMON_OBJS) $(EPICS_OBJS)  
SOURCES = $(MY_SOURCES) $(COMMON_SOURCES) $(EPICS_SOURCES)

# cleanup's sources and objects
OBJS_CL =  $(OBJ_DIR)/cleanup.o  $(COMMON_OBJS)
SOURCES_CL =  $(SRC_DIR)/cleanup.c  $(COMMON_SOURCES)

# mheader's sources and objects
OBJS_HE =  $(OBJ_DIR)/$(MHEADER).o   $(OBJ_DIR)/check_camp.o $(COMMON_OBJS) $(EPICS_OBJS)
SOURCES_HE =  $(SRC_DIR)/$(MHEADER).c   $(SRC_DIR)/check_camp.c    \
	$(COMMON_SOURCES) $(EPICS_SOURCES)



# Must clean or may use old objects compiled with(out) MUSR flag
all :   clean info $(BIN_DIR) $(OBJ_DIR) $(PROGS)

#####################################################################

#
# create library and binary directories
#
$(OBJ_DIR):
	@if [ ! -d  $(OBJ_DIR) ] ; then \
	 echo "Making directory $(OBJ_DIR)" ; \
         mkdir $(OBJ_DIR); \
	fi;

$(BIN_DIR):
	@if [ ! -d  $(BIN_DIR) ] ; then \
	 echo "Making directory $(BIN_DIR)" ; \
         mkdir $(BIN_DIR); \
	fi;

#
# applications binaries
#
# C++ for BNMR/BNQR (EPICS)  C for MUSR
$(BIN_DIR)/mdarc: $(SOURCES) $(OBJS) $(EFENCE)
	$(LINK)  -o $@ $(OBJS) $(EPICS_LIBS) $(LIBS) $(LIBRARIES)

# C always (no epics)
$(BIN_DIR)/cleanup: $(SOURCES_CL) $(OBJS_CL) 
	$(LINK)  $(CFLAGS) $(OSFLAGS) -o $@ $(OBJS_CL) $(LIBS) $(LIBRARIES)

mheader:	$(BIN_DIR)/mheader
$(BIN_DIR)/mheader: $(SOURCES_HE) $(OBJS_HE) $(MIDAS_LIB)/mfe.o
	$(LINK)  -o $@ $(OBJS_HE) $(MIDAS_LIB)/mfe.o $(EPICS_LIBS) $(LIBS) $(LIBRARIES)


$(OBJ_DIR)/camacnul.o: $(DRV_DIR)/bus/camacnul.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $(OBJ_DIR)/camacnul.o $(DRV_DIR)/bus/camacnul.c $(LIBS)


$(OBJ_DIR)/epics_ca.o: $(EPICS_DRV_DIR)/epics_ca.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@



#
# library objects
#
$(OBJ_DIR)/%.o:$(SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) $(OSFLAGS) -o $@ $<

#$(OBJ_DIR)/%.o:$(SRC_DIR)/%.c
#	$(CC) -c $(CFLAGS) $(OSFLAGS) -o $@ $<




info :
	@echo " ******************************************************************";
	@echo " *   Building  mdarc, mheader and cleanup for beamline \"$(BEAMLINE)\" (type \"$(TYPE)\") * ";
	@echo " *   using directory \"$(EXPERIM_INC)\" for experim.h                             *";
	@echo " *   and  $(MUSR_DIR) for camp/mud                       *";
	@echo " * beamline_dir = $(BEAMLINE_DIR)";
#	@echo " * epics_sources = $(EPICS_SOURCES)";
#	@echo " * epics_objs = $(EPICS_OBJS)";
	@echo " * ONLINE_DIR = $(ONLINE_DIR) DEST_DIR = $(DEST_DIR) OBJ_DIR= $(OBJ_DIR)   *" 
	@echo " * linker = $(LINK)";
	@echo " * Use make listing for listings in lib directory                             *"  
	@echo " ******************************************************************************";

clean :		
	rm -f $(BIN_DIR)/mdarc  $(BIN_DIR)/cleanup  $(BIN_DIR)/mheader $(OBJ_DIR)/*.o $(OBJ_DIR)/*.l
	rm -f $(MY_DIR)/experim.h

listing :
	 $(CC) -E -c $(CFLAGS) $(OSFLAGS)  $(SRC_DIR)/$(MHEADER).c > $(OBJ_DIR)/$(MHEADER).l
	 $(CC) -E -c $(CFLAGS) $(OSFLAGS)  $(SRC_DIR)/bnmr_darc.c > $(OBJ_DIR)/bnmr_darc.l
	 
	 

