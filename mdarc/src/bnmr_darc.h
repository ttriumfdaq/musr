/*
 $Log: bnmr_darc.h,v $
 Revision 1.16  2015/03/23 20:47:20  suz
 Changes after debugging new VMIC code; no changes for MUSR VMIC new CAMP; merged in bnmr_darc.h version 1.3 from bnmr's vmic_musr tree

 Revision 1.15  2015/03/23 20:37:40  suz
 Changes after debugging new VMIC code; merged in bnmr_darc.h version 1.2 from bnmr's vmic_musr tree

 Revision 1.14  2008/04/16 18:57:35  suz
 add prototype write_client_logging

 Revision 1.13  2007/07/27 20:11:14  suz
 mheader epics support

 Revision 1.12  2005/06/21 04:59:44  asnd
 Change dynamic run headers (temperature, field) so they report weighted
 averages and can report uncertainty.  Also move some code into subroutines.

 Revision 1.11  2004/11/09 07:56:34  asnd
 Weighted averages of Camp variables, based on number of events (still debugging)

 Revision 1.10  2004/04/20 17:14:14  suz
 move copy_item declaration to darc_odb.h for musr

 Revision 1.9  2004/04/08 18:04:29  suz
 use MAX_IHIS for imusr histograms to fix a bug for IMUSR

 Revision 1.8  2004/02/11 02:26:03  suz
 add I-MUSR support

 Revision 1.7  2003/06/25 00:06:49  suz
 Donald adds DARC_log_ppg_par for BNMR ppg params

 Revision 1.6  2002/12/04 20:52:09  suz
 add function prototypes

 Revision 1.5  2002/10/02 22:17:20  asnd
 Implement dynamic run headers for temperature and field

 Revision 1.4  2001/03/07 17:58:31  suz
 Some prototypes removed to darc_files.h

 Revision 1.3  2000/10/11 04:10:50  midas
 added cvs log

*/
#ifndef _BNMR_DARC_TD_H_
#define _BNMR_DARC_TD_H_


/* 
 * Data structure for keeping track of Camp variable statistics, for
 * calculating weighted averages.
 */
typedef struct {
  double num;
  double sum;
  double sumSquares;
  double sumCubes;
  double offset;
} CAMP_SUMS;

typedef struct _CAMP_VAR_STATS {
  struct _CAMP_VAR_STATS* pNext;
  char * path;
  CAMP_SUMS raw;
  CAMP_SUMS weighted;
} CAMP_VAR_STATS;

typedef struct {
  BOOL   init;
  HNDLE  area;
  HNDLE  num;
  HNDLE  name;
  HNDLE  mean;
  HNDLE  stddev;
} AVER_ODB_HANDLES;

DWORD bnmr_darc(caddr_t pHistData);
DWORD darc_get_odb(D_ODB *p_odb_data);
DWORD darc_write_odb(D_ODB *p_odb_data);
INT darc_check_files(D_ODB *p_odb_data, INT keep,   INT *next_version);
INT odb_save( INT run_number, char *odb_save_dir);
int DARC_write( D_ODB* p_odb_data,  MUD_SEC_GRP* pMUD_fileGrp);
int DARC_write_TD( D_ODB* p_odb_data, caddr_t pHistData);

int DARC_runDesc_TD( D_ODB* p_odb_data, MUD_SEC_GEN_RUN_DESC* pMUD_desc );
int DARC_scalers( D_ODB* p_odb_data, MUD_SEC_GRP* pMUD_scalGrp );  
int DARC_hists( D_ODB *p_odb_data, MUD_SEC_GRP* pMUD_histGrp,
	    caddr_t pHistData );
int DARC_hist_TD( int ind, D_ODB *p_odb_data, caddr_t pHistData, 
	   MUD_SEC_GEN_HIST_HDR* pMUD_histHdr, 
	   MUD_SEC_GEN_HIST_DAT* pMUD_histDat );
MUD_SEC_GRP* DARC_comments( D_ODB *p_odb_data );
void DARC_camp_log_TD( D_ODB* p_odb_data, int dtotal_save );
void DARC_camp_var ( CAMP_VAR *pVar );
void DARC_release_camp(void);
void DARC_odb_set_average( int num, MUD_SEC_GEN_IND_VAR* pindVar );

INT DARC_camp_connect ( char * camp_host );
int DARC_get_dynamic_headers( D_ODB *p_odb_data );
void DARC_head_num( char* str, int len, double val, double err, char* units, BOOL eflag );

void DARC_remove_camp_records( CAMP_VAR_STATS * pcamprec );
void DARC_zero_camp_sums( CAMP_VAR_STATS * prec );
void DARC_odb_set_stat( int num, MUD_SEC_GEN_IND_VAR * pindVar );
CAMP_VAR_STATS * DARC_new_camp_sums( );
MUD_SEC_GEN_IND_VAR* DARC_find_mud_indvar( char* campPath, MUD_SEC_GRP* pIndVarGrp );


#ifdef MUSR  /* IMUSR specific  */
int DARC_write_I( D_ODB* p_odb_data, caddr_t pHistData);
int DARC_runDesc_I( D_ODB* p_odb_data, MUD_SEC_TRI_TI_RUN_DESC* pMUD_desc );
int imdarc_camp( MUD_SEC_GRP* pMUD_indVarGrpI);
int DARC_hist_I( int ind, D_ODB *p_odb_data, caddr_t pHistData, 
	   MUD_SEC_GEN_HIST_HDR* pMUD_histHdr, MUD_SEC_GEN_HIST_DAT* pMUD_histDat );
#else
static void DARC_log_ppg_par(void);
#endif

#ifdef EPICS_ACCESS 
//   (bnmr/bnqr only)
static void DARC_epics_log_TD( );
INT write_client_logging(BOOL camp_flag, BOOL setval);
#endif

#endif

