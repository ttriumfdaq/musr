/* Check_camp.c 

  $Log: check_camp.c,v $
  Revision 1.7  2015/03/23 21:55:18  suz
  Changes for new VMIC code; this version also for MUSR VMIC new CAMP; merged in version 1.2 from bnmr's vmic_musr tree

  Revision 1.6  2015/03/23 19:05:08  suz
  last version running on vxworks musr (from midm20).

  Revision 1.5  2007/07/27 20:34:06  suz
  For c++ link with this file, no longer include the code. Not tested for MUSR

  Revision 1.3  2004/01/14 19:36:35  suz
  change path of mdarc/camp_hostname to mdarc/camp/camp_hostname to agree with MUSR

  Revision 1.2  2003/09/26 20:58:49  suz
  change a comment

  Revision 1.1  2003/07/29 18:33:57  suz
  original for mheader


  Was Revision 1.3  2003/01/23 19:06:03  used by fe_camp.
     Now moved to mdarc area for mheader


*/
#include <stdio.h>
#include <ctype.h>
/* camp includes come before midas.h */

#include "camp_clnt.h"
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif

#include "midas.h"
#include "experim.h"
//* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "mud_util.h"
#include "trii_fmt.h"
//#include "mdarc.h"
#define FAILURE 0
#define NEW

#ifdef BNMR
extern FIFO_ACQ_MDARC fmdarc;
#else
extern MUSR_TD_ACQ_MDARC fmdarc;
#endif
extern int camp_available;
extern char serverName[LEN_NODENAME+1];
extern BOOL debug;

void check_camp(void)
{
  /* Check camp host name is valid
     Fills global serverName
     Sets  global camp_available true or false
   */
  char ctemp[LEN_NODENAME+1];
  int len,j;
  INT stat;

  if(debug)printf("Check_camp: starting\n");
  /* Check there is a valid camp hostname */

  len = strlen(fmdarc.camp.camp_hostname);  
  if(len > 0)
    {
      fmdarc.camp.camp_hostname[len]='\0';    
      strcpy (ctemp,fmdarc.camp.camp_hostname );
      printf("check_camp:  CAMP hostname: %s\n",ctemp);

      for  (j=0; j< LEN_NODENAME ; j++) ctemp[j] = toupper (ctemp[j]); /* convert to upper case */                
      
      trimBlanks (ctemp,ctemp);
      if (strncmp(ctemp,"NONE",4) == 0)
	{
	  cm_msg(MERROR, "check_camp:", "camp hostname is not set. CAMP is not available");
	  camp_available = FALSE  ; /* name is NONE */
	} 
      else
	{
	  strcpy( serverName, ctemp);
	  if(debug)printf("check_camp : Camp hostname: %s\n",serverName );
	  camp_available = TRUE   ; /* camp is running */
	}
    }
  else
    {
      cm_msg(MERROR, "check_camp", "camp hostname is not set. No camp data available");
      camp_available = FALSE;
    }
  return;
}

INT ping_camp(void)
{
  /* must be called with camp_available true i.e. a valid CAMP hostname */
      
  /* ping the camp host because if someone turns off the crate, there is
     no timeout and it can waste a lot of time  */ 
  
  INT status;
  char cmd[256];

  if(debug) printf("ping_camp: starting\n");
  sprintf(cmd,"ping -c 1 %s &>/dev/null",serverName);
  if(debug) printf(" ping_camp: pinging camp host %s using command: %s\n",serverName,cmd);
  status=system(cmd);
  
  if(debug)printf("status after ping %d\n",status);
  if(status)
    {
      camp_available= FALSE ; /* no name specified */
      if(debug)printf("ping_camp: Camp host %s is unreachable. Camp data cannot be saved",
	     serverName);
      return  DB_NO_ACCESS;
    }
  return DB_SUCCESS;
}
  

