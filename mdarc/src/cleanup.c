/********************************************************************\
  
  Name:         cleanup.c
  Created by:   Suzannah Daviel (from mdarc.c)
  
  Contents:  Cleanup BNMR saved files
  
  Previous revision history
  $Log: cleanup.c,v $
  Revision 1.20  2015/12/17 01:34:22  asnd
  Fix by adding missing "else".

  Revision 1.19  2015/10/23 06:08:00  asnd
  Add M20C and M20D beamlines to checks

  Revision 1.18  2015/03/23 22:03:37  suz
  Changes for new VMIC code; this version also for MUSR VMIC new CAMP; merged in version 1.1 from bnmr's vmic_musr tree


  Revision 1.17  2007/07/27 20:13:54  suz
  added handles - possibly for c++ linking?

  add to cvs
 

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>


/* midas includes */
#include "midas.h"
#include "msystem.h"
#include "ybos.h"

#ifdef MUSR
/* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "trii_fmt.h"  /* needed by MUSR in mdarc.h */ 
#endif
#include "mdarc.h" /* common header for mdarc,bnmr_darc & musr_darc */
#include "experim.h" 
#include "darc_files.h" 
#include "run_numbers.h"


#define FAILURE 0

#ifndef MUSR
INT nH;
HNDLE hFS;
FIFO_ACQ_FRONTEND fifo_set;
#endif
INT setup_mdarc(); /* cd setup_hotlink in mdarc */
//void  hot_toggle (INT, INT, void * ); /* dummy here - should never be called */

HNDLE hDB;
INT rn;   // global run number to clean up
INT status;

HNDLE hMDarc; /* handle for /equipment/<eqp_name>/mdarc */
#ifdef MUSR
MUSR_TD_ACQ_MDARC fmdarc;
#else
FIFO_ACQ_MDARC fmdarc;
#endif

char expt_name[HOST_NAME_LENGTH];

INT cleanup_init(void)
{
  INT   size;
  char  str[128];
  KEY   hKey;
  HNDLE hSet;
  HNDLE hArea;
  char save_dir[20];
  char archive_dir[20];  
  char filename[128],archived_filename[128]; 
  INT run_state;
  INT run_number;
  BOOL archived;

  printf ("Cleanup starting ... \n");
  
  cm_get_experiment_database(&hDB, NULL);


  /* Make sure we are not going to cleanup the current run ... */

//  get the CURRENT run number
    size = sizeof(run_number);
    status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
    if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"tr_start","key not found  /Runinfo/Run number");
      write_message1(status,"tr_start");
      return (status);
    }
    if (rn == run_number)
      {

	/* get the run state to see if run is going */
	size = sizeof(run_state);
	status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"bnmr_darc","key not found /Runinfo/State");
	    write_message1(status,"bnmr_darc");
	    return (status);
	  }
	if(run_state == 3)
	  {
	    cm_msg(MERROR,"mdarc_cleanup","Run %d is still in progress; bad idea to clean it up now",run_number);
	    return(0);
	  }
      }

    status = setup_mdarc();  /* obtain mdarc record */
    if(status != DB_SUCCESS) return(0);

    printf(" cleanup: purging & renaming saved files for run  %d\n",rn);
    printf("        (i.e. end-of-run procedure) \n");

  sprintf(archiver,"%s",fmdarc.archiver_task) ;  // archiver is in mdarc.h and passed to darc_files


  status=check_beamline();
  if (status != SUCCESS) return status; 



/*  Run cleanup procedure (same as at end of run) */
    
       
    if(debug)
    {
      printf("cleanup: saved_data_directory: %s\n",fmdarc.saved_data_directory);
      printf("cleanup: archived_data_directory: %s\n",fmdarc.archived_data_directory);
      printf("cleanup: archiver= %s   \n",fmdarc.archiver_task);
      printf("calling darc_rename with run number %d   \n",rn);
 
    }
    if(debug_check)printf("debug_check is true\n");
    status = darc_rename_file(rn,fmdarc.saved_data_directory, fmdarc.archived_data_directory, filename,
			      archived_filename, &archived);
    if (status == SUCCESS)
    {
      if(debug) printf("cleanup: success from darc_rename_file; saved file:%s\n",filename);
      return (status);
    }
    else   /* may be no versions to purge. Check for .msr and if found, archive it */
      {
	if(!archived)
	  {
	    printf("Cleanup: checking for a MUD  (.msr) file to Archive ...\n"); 
	    if( rn < 30000 || rn >= 40000)
	      status = darc_archive_file(rn, FALSE, fmdarc.saved_data_directory, fmdarc.archived_data_directory  );
	    else
	      printf("cleanup: test runs are not archived\n");
	  }
      }

    return (status);
}

INT check_beamline(void)
{
  INT j;

  /* beamline needed for BNMR and MUSR to set directory for archive.txt
     and to check run number before archiving */

#ifndef MUSR     //BNMR or BNQR
  strcpy(beamline,expt_name);  // expt_name and beamline are identical
  if(debug) printf("BN*R Get beamline from expt_name: %s\n",expt_name);
#else
  /* the beamline for MUSR should have been copied into mdarc/histograms/musr/beamline
     by musr_config (for MUSR, expt_name=MUSR for all beamlines)  */
  
  strcpy(beamline,fmdarc.histograms.musr.beamline);
  if(debug) printf("MuSR Get beamline from mdarc area: %s\n",beamline);
#endif
  
  for (j=0; j<strlen(beamline); j++)
      beamline[j] = toupper (beamline[j]); /* convert to upper case */
  if(debug) printf(" beamline:  %s, run number=%d\n",beamline,rn);

  if (strlen(beamline) <= 0 )
    {
#ifndef MUSR     //BNMR or BNQR
      printf("check_beamline: no valid experiment name is supplied\n");
#else
      printf("check_beamline: no valid beamline is supplied in odb (..mdarc.histograms.musr.beamline) \n");
#endif
      return (DB_INVALID_PARAM);
    }

  
  /* check that the run number matches the beamline (real runs) */
  if ( rn >= MIN_M20 && rn <= MAX_M20 )        // old M20
    {
      if (strncmp(beamline,"M20",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M20 for this run number",rn,beamline);
	  return (DB_INVALID_PARAM);
	}
       else
	printf("M20 run detected\n");
    }
  else if ( rn >= MIN_M20C && rn <= MAX_M20C )        // M20C
    {
      if (strncmp(beamline,"M20C",4) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M20C for this run number",rn,beamline);
	  return (DB_INVALID_PARAM);
	}
       else
	printf("M20C run detected\n");
    }
  else if ( rn >= MIN_M20D && rn <= MAX_M20D )        // M20D
    {
      if (strncmp(beamline,"M20D",4) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M20D for this run number",rn,beamline);
	  return (DB_INVALID_PARAM);
	}
       else
	printf("M20D run detected\n");
    }
  else if ( rn >= MIN_M15 && rn <= MAX_M15 )      // M15
    {
      if (strncmp(beamline,"M15",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M15 for this run number",rn,beamline);
	  return (DB_INVALID_PARAM);
	}
      else
	printf("M15 run detected\n");
    }
  else if ( rn >= MIN_M9B && rn <= MAX_M9B)
    {
      if (strncmp(beamline,"M9B",3) != 0)  // M9B
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = M9B for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
       else
	printf("M9b run detected\n");
    }
  else if ( rn >= MIN_DEV && rn <= MAX_DEV  )    // DEV
    {
      if (strncmp(beamline,"DEV",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = DEV for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
      else
	printf("dev run detected\n");
    }
  else if ( rn >= MIN_BNMR && rn <= MAX_BNMR  )    // BNMR
    {
      if (strncmp(beamline,"BNMR",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = BNMR for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
    }
  else if ( rn >= MIN_BNQR && rn <= MAX_BNQR  )    // BNMR
    {
      if (strncmp(beamline,"BNQR",3) != 0)
	{
	  cm_msg(MERROR,"check_beamline",
		 "Mismatch between run number (%d) & beamline (%s); expect beamline = BNQR for this run number",rn, beamline);
	  return (DB_INVALID_PARAM);
	}
    }
  else if(  rn >= MIN_TEST &&  rn <= MAX_TEST  )   // test run (any beamline)
    {
      printf("Detected a test run\n");
    }
  else if(  rn < MIN_M9B &&  rn > MAX_M15  )   // M13
    {
      cm_msg(MERROR,"check_beamline",
	     "Run number (%d) matches obsolete beamline (M13)\n",rn);
      return (DB_INVALID_PARAM);
    }
  else
    {
      cm_msg(MERROR,"check_beamline",
	     "Requested run number (%d) does not match any valid beamline",rn);
      return (DB_INVALID_PARAM);
    }
  return SUCCESS;
}



/*---- setup_mdarc  ---------------------------------------------------*/

INT setup_mdarc()
{
  char str[128];
  INT stat;

#ifdef MUSR
  MUSR_TD_ACQ_MDARC_STR(acq_mdarc_str);
#else
  FIFO_ACQ_MDARC_STR(acq_mdarc_str); 
#endif
  if(debug) printf("setup_mdarc starting\n");


  /* Create record for mdarc area */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  
  status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
  if (status != DB_SUCCESS)
    {
       /* mdarc may be running - it has hotlinks on some mdarc records
	 which prevents create record from working */
       if ( cm_exist("mdarc",FALSE)== CM_NO_CLIENT)
	 {
	/* mdarc is NOT running so create record should have worked */
	  cm_msg(MERROR,"setup_mdarc","Failed to create record for %s  (%d).", str,status);
	  write_message1(status,"setup_mdarc");
	  return(status);
	}
    }	  
  /* get the key hMDarc  */
 
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_mdarc", "key %s not found (%d)", str,status);
    write_message1(status,"setup_mdarc");
    return (status);
  }



  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */





  
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_mdarc","Failed to retrieve %s record  (%d)",str);
    write_message1(status,"setup_mdarc");
    return(status);
  }
  return(status);
}  

 





/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT    status;
  DWORD  j, i, last_time_kb;
  HNDLE  hDB, hKey;
  char   host_name[HOST_NAME_LENGTH];
  char   ch;
  INT    msg;
  BOOL   cr=FALSE;
  char   clean_run[25];
  //  char   dbg[25];
  int    bug;


  /* set defaults for debug */
  debug = FALSE;
  debug_check = FALSE;

  /* set defaults for other globals */
   toggle = FALSE;  /* initial value */
   run_number = -1; /* global run number */

  /* for Midas 1.9.3 */
    cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
    
   /*     cm_get_environment (host_name, expt_name);    earlier Midas   */

  /* initialize global variables */
#ifdef MUSR
  sprintf(eqp_name,"MUSR_TD_acq");
  sprintf(feclient,"fev680");
#else 
#ifdef TWO_SCALERS  // BNMR has 2 scalers
  sprintf(feclient,"febnmr");
#else       // BNQR has 1 scaler
  sprintf(feclient,"febnqr");
#endif
  sprintf(eqp_name,"FIFO_acq");
#endif
  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {

    if (argv[i][0] == '-'&& argv[i][1] == 'd')
      debug = debug_check = TRUE;
    else if  (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
        goto usage;
      if (strncmp(argv[i],"-e",2) == 0)
        strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
        strcpy(host_name, argv[++i]);
      else if (strncmp(argv[i],"-q",2)==0)
        strcpy(eqp_name, argv[++i]);
      else if (strncmp(argv[i],"-f",2)==0)
        strcpy(feclient, argv[++i]);
       else if (strncmp(argv[i],"-r",2)==0)
      {
        strcpy(clean_run, argv[++i]);
        cr=TRUE;
      }
      
    }
    else
    {
   usage:
#ifdef MUSR
      printf("\ncleanup: purges, renames (if necessary) and archives MUSR run files\n\n"); 
#else
      printf("\ncleanup: purges, renames (Type2) and archives (Types 1&2) MUSR run files\n\n"); 
#endif
      printf("usage: cleanup  [-h Hostname] [-e Experiment]  [-r Runnumber]\n");
      printf("              [-f FE client name] (default:%s)\n",feclient);
      printf("              [-q FE equipment name] (default:%s)\n",eqp_name);
      printf("                      [-d ] (debug)   \n");
      return 0;
    }
    
  }
  if(cr)
  {
    rn = atoi(clean_run);
    if (rn <= 0)
    {
      printf("Invalid run number for cleanup; supplied run number was %s\n",clean_run);
      return 0;
    }
    else
      printf("About to run cleanup procedure for run %d\n",rn);
  }
  else
  {
    printf("Supply run number for cleanup\n");
    return 0;
  }
  if (debug)
    printf("debug is TRUE\n");    
  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "mdarc_cleanup", 0);
  if (status != CM_SUCCESS)
    return 1;
  
  if(debug)
  cm_set_watchdog_params(TRUE, 0);
  
  /* turn off message display, turn on message logging */
  // cm_set_msg_print(MT_ALL, 0, NULL);
  /* turn on message display, turn on message logging */
  cm_set_msg_print(MT_ALL, MT_ALL, msg_print);  

  /* connect to the database */
//  cm_get_experiment_database(&hDB, &hKey);
  
  status = cleanup_init();


  cm_disconnect_experiment();

}














