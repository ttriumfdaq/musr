/* client_flags.h
       function prototypes for client_flags.c

$Log: client_flags.h,v $
Revision 1.2  2008/12/10 18:37:31  suz
add hot link for mheader

Revision 1.1  2003/07/29 18:17:18  suz
original


*/

INT get_ClFlgs_record(void);
INT clear_client_flags();
INT check_transition(BOOL *trans);
INT check_client_flags(BOOL *tr_flag);
INT setup_hot_clients(void);
INT stop_run(void);
void  hot_frontend (INT, INT, void * );
void  hot_mdarc (INT, INT, void * );
void  hot_mheader (INT, INT, void * );
INT  close_client_hotlinks(void);
INT set_client_flag(char *client_name, BOOL value);

