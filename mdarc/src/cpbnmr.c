/********************************************************************\
  Name:          cpbnmr.c

This file is a copy of that in  /home/musrdaq (for cvs)

Musrdaq owns the files in the archive on /musr, nfs mounted
as /musr/dlog/bnmr

Compiled file must be in /home/musrdaq/bin
i.e. log in as musrdaq

cc cpbnmr.c -o bin/cpbnmr
chmod +s bin/cpbnmr   
 
  $Log: cpbnmr.c,v $
  Revision 1.4  2015/03/23 22:39:57  suz
  version with package for vmic and ted's camp

  Revision 1.3  2002/07/17 06:52:19  asnd
  Fix up copying to archive.

  Revision 1.2  2002/06/12 18:18:25  suz
  trigger_file added by Donald

  Revision 1.1  2001/12/07 22:24:08  suz
  original: archiver for bnmr; belongs to musrdaq

*/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
extern char **environ;

#ifndef NULL
#define NULL (char*) 0
#endif

int main ( int argc, char **argv )
{
  char fname[128];
  
  char data_file[128]; /* including directory */
  char trigger_file[128];
  char archive_dir[128];
  FILE *trig;

  if( argc != 3 ) goto error;
  
  if( strnlen( argv[1], 128 ) > 127) goto error;
  if( strnlen( argv[2], 128 ) > 115) goto error;

  if( sscanf( argv[1], "%s", data_file) != 1) goto error;
  //  printf("data dir: %s\n",data_file);

  if( sscanf( argv[2], "%s", archive_dir) != 1) goto error;
  //  printf("archive dir: %s\n",archive_dir);

  sprintf( trigger_file, "%s/../noa.arc", archive_dir );
  //  printf("trigger file: %s\n",trigger_file);

  /*
   * touch trigger file
   */
  trig = fopen( trigger_file, "w" );
  if( trig ) {
    fputc( '\0', trig );
    fclose( trig );
  }
  /*
   * execute copy command.  exists with the copy status
   */
  execl( "/bin/cp", "/bin/cp", "-b",data_file, archive_dir, NULL);

 error:
  fprintf( stderr, "Usage: cpbnmr <data_file> <archive_dir>\n" );
 
}



