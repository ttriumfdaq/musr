/*
 $Log: darc_files.h,v $
 Revision 1.5  2015/03/23 23:47:04  suz
 add an ifdef; this version with VMIC MUSR Ted's camp package

 Revision 1.4  2007/07/27 20:15:26  suz
 minor change

 Revision 1.3  2004/01/08 20:38:49  suz
 add parameter last_archived_filename to  darc_rename_file

 Revision 1.2  2001/12/18 19:37:49  suz
 added darc_archive_file for cleanup.c

 Revision 1.1  2001/03/06 21:46:20  suz
 Original. Prototypes for subroutines now in darc_files.c. These prototypes
 mostly were in bnmr_darc.h formerly.


*/
#ifndef darc_files_header
#define darc_files_header
void write_message(int status); /* old version - may use again */
void write_message1(INT status, char* name);
INT darc_find_saved_file(INT run_number, char *save_dir);
INT darc_kill_files(INT killed_run_number, char *save_dir);
INT darc_rename_file(INT run_number, char *save_dir, char *archive_dir, char *saved_filename, char *archived_filename,
		     BOOL *archived);
INT darc_set_symlink(INT run_number, char *save_dir, char *filename);
INT darc_unlink(INT run_number, char *save_dir);
void write_link_msg(INT status);
void write_rename_msg(INT status);
void write_remove_msg(INT status);
INT msg_print(const char *msg);
INT darc_archive_file(INT run_number, INT make_backup, char *save_dir, char *archive_dir);

#endif // darc_files_header
