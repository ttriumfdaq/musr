/* darc_odb.h

   Structure to store the odb data needed for bnmr_darc and musr_darc

   IMPORTANT - the sizes of these parameters MUST MATCH the sizes
   in the odb PARTICULARLY where hotlinks are used (db_create_record)
   otherwise get mis-match. 

  $Log: darc_odb.h,v $
  Revision 1.21  2015/03/23 23:59:00  suz
  his_total and total_save changed to double from float for BNMR/QR; this version same as version 1.3 bnmr's vmic_online file 2014/01/09

  Revision 1.20  2015/03/23 23:53:14  suz
  changes after debugging vmic code; this version same as version 1.2 bnmr's vmic_online file

  Revision 1.19  2015/03/23 23:51:34  suz
  add an ifdef; this version same as version 1.1 bnmr's vmic_online file

  Revision 1.18  2005/06/13 17:09:42  suz
  Donald enlarges temperature and field strings

  Revision 1.17  2004/09/11 09:53:00  asnd
  Save scaler rates in TD-muSR mud file (they were left out).

  Revision 1.16  2004/04/20 17:14:53  suz
  move copy_item declaration from bnmr_darc.h for musr

  Revision 1.15  2004/04/08 18:02:20  suz
  increase sizes of sample,comments etc.

  Revision 1.14  2004/02/11 02:32:05  suz
  add imusr support


  Revision 1.13  2002/12/04 20:56:24  suz
  BNMR needs temperature_variable & field_variable. Remove ifdef MUSR

  Revision 1.12  2002/10/02 22:17:20  asnd
  Implement dynamic run headers for temperature and field

  Revision 1.11  2002/05/10 17:55:05  suz
  increase no. scalers from 60 to 80

  Revision 1.10  2002/04/17 21:18:45  suz
  add 2 parameters needed by musr_config

  Revision 1.9  2002/04/17 20:21:54  suz
  max scalers changed for MUSR

  Revision 1.8  2001/09/28 19:30:32  suz
  increase length of run_title

  Revision 1.7  2001/09/14 18:58:07  suz
  support MUSR with ifdef MUSR

  Revision 1.6  2001/03/29 19:49:10  suz
  Increase max. scalers from 45 to 60 for second scaler module

  Revision 1.5  2000/11/03 21:10:29  midas
  change his_total and total_save to type float from long

  Revision 1.4  2000/10/27 19:27:31  midas
  MAX_HIS increased from 4 to 16.

  Revision 1.3  2000/10/11 04:10:50  midas
  added cvs log

*/
/* for combined IMUSR/TDMUSR version, use ifdef MUSR */

#ifndef darc_odb_header
#define darc_odb_header
void copy_item(char *p_struct_str,  INT struct_str_size, char *p_odb_str, char *name);

/* Maximum number of TD-MUSR histograms */
#define MAX_HIS 16     /* change from 4 to 16 to generalize for different xpts */ 
#define HIS_SIZE 32    /* size of histo titles (both MUSR & BNMR) */

#ifdef MUSR
/* maximum number of IMUSR histograms */
#define MAX_IHIS 19  /* IMUSR has max. 19 fixed histograms
		        PD,clk,totalrate, 4 front , 4 back
			[ 4 aux1 4 aux2 are optional ]  */
#define MAX_NSCALS 16  /* maximum no. of scalers  for MUSR */
// the following needed by musr_config and bnmr_darc.c (MUSR only):
#define MAX_CNTR MAX_HIS/2     // max number of counters supported

#else  // BNMR/BNQR

#define MAX_NSCALS 80  /* maximum no. of scalers for BNMR  
     this value must be >=  N_SCALER_TOT in frontend code */

#endif

typedef struct {
  char area[32]; 
  char rig[32]; 
  char mode[32];
  int  run_state;         /* state = STATE_RUNNING  if running */
  //
  int  run_number       ; /* /<area>/<rig>/parameters/mdarc/local_runnumber */
  int  experiment_number; /*                               /experiment_number */
  char experimenter[32];
  char run_title[128]; /* increase this from 80 */
  char sample[48]; /* increase this from 15 */
  char orientation[15];
  char field[32];
  char temperature[32];
  char start_time_str[32];
  int start_time_binary;
  char stop_time_str[32];
  int stop_time_binary;
  //
  /* Bnmr hardware information */
  float  dwell_time;      /* (ms)   c.f. MUSR tdc_res !! */
  float  beam_time;       /* (ms)  not used */
  int   ncycles;         /* not used */


  /* histogram information */
  int his_n;             /* number of histograms defined */
  int his_nbin;          /* number of histogram bins */
  int nchan;             /* number of scaler channels enabled */
  
  int his_bzero[MAX_HIS];     /* time zero */     
  int his_good_begin[MAX_HIS];/* first good bin */
  int his_good_end[MAX_HIS];  /* last good bin */
  int his_first_bkg[MAX_HIS]; /* first background bin */
  int his_last_bkg[MAX_HIS];  /* last background bin */
  
  char his_titles[MAX_HIS][HIS_SIZE]; /* histogram titles */
  
  int purge_after;           /* purge data files after n versions */
  char save_dir[128];         /* e.g. /bnmr/dlog */
  char camp_host[LEN_NODENAME]; /* LEN_NODENAME =127 (camp.h) */ 

  char temperature_variable[128];
  char field_variable[128];


  // scalers
  int nscal; /* number of scalers in use */
  int scaler_save[MAX_NSCALS];/* contains scaler data */
#ifdef MUSR // combined IMUSR/MUSR
  int scaler_rate[MAX_NSCALS];/* contains scaler rates */
#endif
  char scaler_titles[MAX_NSCALS][32];

  // darc writes these into odb:    
  char time_save[32];         /* outputs time when saved */
  char file_save[128];        /* outputs saved file name */

#ifdef MUSR
  float his_total[MAX_HIS];    /* outputs his_total */
  float total_save;            /*  output  total_saved */
#else  // BNMR
  double his_total[MAX_HIS];    /* outputs his_total */
  double total_save;            /*  output  total_saved */
#endif

#ifdef MUSR // combined IMUSR/MUSR
    // IMUSR specific
    char comment1[128]; /* increase all comments from 32 */
    char comment2[128];
    char comment3[128];
    char subtitle[128];
    char IMUSR_his_titles[MAX_IHIS][HIS_SIZE]; /* histogram titles; increase from 11
						  now same as TD his titles */
    float IMUSR_his_total[MAX_IHIS];
#endif
}D_ODB;


#endif // darc_odb_header




