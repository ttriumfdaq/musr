/* epics_log.h

  Information needed to log the epics values for mdarc/mheader

$Log: epics_log.h,v $
Revision 1.4  2015/03/24 00:04:02  suz
add an ifdef

Revision 1.3  2007/09/21 21:19:20  suz
increase max number of epics logged variables

Revision 1.2  2007/07/27 21:45:28  suz
correct bug

Revision 1.1  2007/07/27 21:32:05  suz
inital version: support for epics in mdarc/mheader

*/

#ifndef epics_log_header
#define epics_log_header

INT init_epics (BOOL stats);
INT open_epics_chan(char *name,  INT i);
INT open_epics_log(INT *pnum, BOOL stats);
INT read_epics_value(INT i, BOOL stats);
void clear_epics_log(INT i);
void set_epics_constants(void);
INT get_epics_constants(char *Rname);
void epics_close(void);
void print_epics_log(INT i);

#ifdef RAND
INT get_random_index( INT nval);
float ran0(long *idum);
#endif

#define NUM_EPICS_DEV 9 // size of epics_params (number of known EPICS devices)
#define MAX_EPICS 8 // size of array in odb (maximum number of EPICS devices to log)

typedef struct {
  int  Rchid;     /* ID for read; set to -1 if no access */
  char Name[25];  /* name of EPICS channel */
  INT  index;     /* index to epics_params structure */
  BOOL open;      /* Channel is open for read */
  float value;   /* latest value */
  double mean;    /* mean value */
  double stddev; /* standard deviation */
  double maximum; /* maximum value */
  double minimum; /* minimum value */
  double sum;
  double sumSquares;
  double sumCubes;
  double skew;
  double offset;
  INT    count;   /* number of times value has been read */
} EPICS_LOG;


/* define structure for units and title for known EPICS devices */
typedef struct
{
  char Rname[32];
  char Title[20];
  char Units[20];
} EPICS_PARAMS;

#endif //  epics_log_header
