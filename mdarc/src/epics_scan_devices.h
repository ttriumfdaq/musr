/* epics_scan_devices.h
 EPICS devices that might be scanned


//#define NUM_EPICS_DEV 7 // size of epics_params (number of known EPICS devices)
$Log: epics_scan_devices.h,v $
Revision 1.3  2015/03/24 00:06:57  suz
add an ifdef

Revision 1.2  2007/09/20 02:36:02  suz
add more Epics devices

Revision 1.1  2007/07/27 21:35:25  suz
inital version: support for epics in mdarc/mheader


*/
#ifndef epics_scan_header
#define epics_scan_header
EPICS_PARAMS epics_params[NUM_EPICS_DEV];

// Only define these once
char epics_devices_paths[NUM_EPICS_DEV][32] = {
  "ILE2:BIAS15:RDVOL", "ILE2A1:HH:RDCUR", "BNQR:HVBIAS:RDVOL",
  "BNMR:HVBIAS:POS:RDVOL", "BNMR:HVBIAS:NEG:RDVOL","ITE:BIAS:RDVOL",
  "ILE2:POLSW2:STATON","ILE2:POLSW2:STATOFF",""};

char epics_devices_titles[NUM_EPICS_DEV][32] = {
  "Sodium Cell", "Helmholtz MagCurr", "BNQR Platform Bias","BNMR Platform Bias+","BNMR Platform Bias-","Initial Beam energy",
  "HelicityOn", "HelicityOff",  "no name"};

char epics_devices_units[NUM_EPICS_DEV][32] = {
  "Volts","Amps","kV","kV","kV","kV",
  "none","none","unknown"};

#endif // epics_scan_header
