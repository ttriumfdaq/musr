/*  mdarc.h
    common (global) variables for mdarc & bnmr_darc.c

    $Log: mdarc.h,v $
    Revision 1.22  2015/03/24 00:15:51  suz
    Changes after debugging new VMIC code

    Revision 1.21  2015/03/23 19:03:32  suz
    last version running on vxworks musr (from midm20).

    Revision 1.20  2008/04/16 18:51:53  suz
    separation of CAMP and EPICS logged variables

    Revision 1.19  2007/07/27 20:11:32  suz
    mheader epics support

    Revision 1.18  2004/11/16 23:16:22  suz
    add serial no. check for BNMR

    Revision 1.17  2004/10/19 17:49:48  suz
    removed pHistData; it is a parameter to bnmr_darc.c

    Revision 1.16  2004/10/13 20:05:47  suz
    MUSR: add pSN, pointer to saved bank serial no. (TD-musr only)

    Revision 1.15  2004/02/11 02:32:45  suz
    add imusr support

    Revision 1.14  2004/01/08 20:37:54  suz
    add debug_cf for client flags

    Revision 1.13  2003/12/01 20:29:48  suz
     DA moved char beamline[6] from mdarc.c

    Revision 1.12  2003/07/15 16:44:08  suz
    add debug_ppg

    Revision 1.11  2003/05/05 20:02:58  suz
    Added midbnmr_darc prototypes and params for type 1

    Revision 1.10  2002/04/17 20:20:55  suz
    add perl_path and expt_name

    Revision 1.9  2001/09/14 18:58:17  suz
    support MUSR with ifdef MUSR

    Revision 1.8  2001/04/30 20:13:29  suz
    run_type[32] -> run_type[5]

    Revision 1.7  2001/03/07 01:28:09  suz
    Global run number, run type & add toggle. Needed for automatic run numbering.

    Revision 1.6  2001/01/08 19:17:33  suz
    Add global firstxTime for opening/closing camp connection only at BOR/EOR.
    Use this version with mdarc.c bnmr_darc.c cvs version 1.9 .

    Revision 1.5  2000/10/27 19:26:48  midas
    Add nhistBins and nhistBanks to allow variable no. of histograms

    Revision 1.4  2000/10/11 20:13:44  midas
    add feclient name

    Revision 1.3  2000/10/11 04:10:50  midas
    added cvs log

*/
/* pHistData should not be global to bnmr_darc.c as it is also the parameter
   i.e. call bnmr_darc(pHistData)
   It is now a global in mdarcMusr (or mdarcBnmr);
//caddr_t pHistData; 
*/
#ifndef mdarc_header
#define mdarc_header

INT nHistBins; // number of histogram bins
INT nHistBanks; // number of histograms (BNMR,TD-MUSR)
#ifdef MUSR
INT nHistograms; // number of histograms (IMUSR)
#endif
BOOL     firstxTime;
HNDLE   hDB, hKey, hMDarc;
BOOL    bGotEvent;
caddr_t pSN; // pointer to bank serial number stored in data array  (TD only)
INT     last_saved_SN;
#ifdef MUSR
BOOL    bGotCamp;  // IMUSR; got CAMP event
char i_eqp_name[32];
#endif
char    feclient[32], eqp_name[32];
INT     size;
char expt_name[HOST_NAME_LENGTH];
char beamline[6]; // was 16 for IMUSR; why? 
INT    run_number;
INT    old_run_number;
char   run_type[5];  /* test or real */
BOOL   toggle;


/* debugs */
BOOL debug ;
BOOL debug_mud;
#ifndef MUSR
BOOL debug_ppg; // BNMR
#endif
#ifdef EPICS_ACCESS
BOOL debug_epics;
#endif
BOOL debug_dump;
BOOL debug_check ;
BOOL debug_proc; // global for debug of process_*_event
BOOL debug_cf; // debug client flags routines 
INT  dump_begin;
INT  dump_length;
//BOOL timer ;    /* timing */
char perl_path[80]; /* perlscript path */
char archiver[80];
BOOL I_MUSR,TD_MUSR; /* flags indicate which MUSR type is running
			for BNMR these are both FALSE */
INT db,dl;

#ifdef MUSR 
/*        MUSR       */
DWORD scaler_counts[16];
HNDLE hMusrSet;
INT gbl_data_point; // the rest is for IMUSR
INT gbl_camp_data_point;
INT numCamp;// number of camp variables enabled in /equipment/camp/settings
/* Array of camp variables from CVAR event */
double *pCampData;

/* Array of scaler totals, allocated by realloc in M2MI_expand, not freed: */
IMUSR_CAMP_VAR *campVars ;
static FILE *Imusr_log_fileHandle = NULL ;

#else
/*   BNMR/BNQR       */
INT ppg_type;
// prototypes needed by midbnmr_darc.c for BNMR Type 1
void process_hist_event (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void process_darc_event  (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void process_camp_event  (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void process_epics_event  (HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
#endif 

#endif
