/********************************************************************\
  Name:         mdarcMusr.c
  Created by:   
  Contents:    data logger for Integral and TD MUSR ; does not support BNMR/BNQR  
               (MIDAS on linux)


  $Log: mdarcMusr.c,v $
  Revision 1.16  2015/03/23 22:34:07  suz
  More changes with new VMIC code; ybos gone; this version with MUSR VMIC version (Ted's camp)

  Revision 1.15  2015/03/23 22:29:45  suz
  Changes after debugging new VMIC code; merged in version 1.2 from bnmr's vmic_musr tree

  Revision 1.14  2015/03/23 19:02:39  suz
  last version running on vxworks musr (from midm20).

  Revision 1.13  2005/06/10 16:36:32  suz
  remove post-stop run number check for MUSR

  Revision 1.12  2004/11/10 18:17:56  suz
  clear suppress_save.. flag at BOR if left true

  Revision 1.11  2004/11/10 00:11:38  suz
  added tr_resume to save data; endrun_save_event flag renamed suppress_save_temporarily; pause now saves data again, unless suppress flag is true i.e. run about to be killed

  Revision 1.10  2004/11/08 22:18:29  suz
  add odb param so kill.pl can prevent save of last event

  Revision 1.9  2004/10/19 17:46:45  suz
  save a pending event before retriggering; free pointer after call to bnmr_darc

  Revision 1.8  2004/10/13 20:03:57  suz
  changes for Midas 1.9.5; add serial no. to data and fix delayed save problem

  Revision 1.7  2004/09/29 18:46:06  suz
  get rid of a warning

  Revision 1.6  2004/09/04 00:16:39  asnd
  (Suzannah) restore final save of data

  Revision 1.5  2004/06/10 19:07:59  suz
  perl output files now under ~midas/musr/log/<beamline>

  Revision 1.4  2004/04/21 19:14:26  suz
  send message (not checking rn at bor) only on debug

  Revision 1.3  2004/04/08 17:59:34  suz
  no longer automatically check run number at begin run. Post-stop check 3s after stop (not 30s)

  Revision 1.2  2004/03/30 22:47:11  suz
  remove copying of perl output file on error now information is appended

  Revision 1.1  2004/02/11 17:46:33  suz
  main pgm for MUSR; supports Integral and TD MUSR


# based on MUSR's imdarc.c (now obsolete); added support for TD to make a combined I/TD version
\*****************************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
/* needed for lstat */
//#include <sys/stat.h>
//#include <unistd.h>
#include <errno.h>


/* midas includes */
#include "midas.h"
#include "msystem.h"
//#include "ybos.h"  ybos is gone

/* the following needed for imusr: */
/* mud includes */
#define BOOL_DEFINED
#include "mud.h"
//#include "c_utils.h"
//#include "triumf_fmt.h"
#include "trii_fmt.h"
/* the following header files are shared with bnmr/bnqr (except experim.h) */
#include "mdarc.h" /* common header for mdarc,bnmr_darc */
#include "darc_files.h" /* for msg_print only */
#include "experim.h" 
#include "run_numbers.h"
#include "client_flags.h"  // contains prototypes
// note caddr_t is defined as char*

char ClientName[]="mdarc";  /* the client name of this program */


/* prototypes */
INT    tr_start(INT rn, char *error);
INT    tr_stop(INT rn, char *error);

INT    setup_hotlink();
INT    setup_hot_toggle();
void   update_Time(INT , INT , void *);
void  hot_toggle (INT, INT, void * );
void  hot_histo_arrays (INT, INT, void * );
INT print_file(char* filename);
/* TD only: */
void  process_event_TD(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
/* the following for imusr only : */
void  process_event_cvar(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void  process_event_camp(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
void  process_event_I(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent);
INT auto_run_number_check(INT rn);
static INT allocate_storage (void);
static void  allocate_camp_storage( void );
INT get_musr_type(BOOL *fe_flag);
INT save_data(INT *msg);
INT mdarc_create_record(void);
INT imusr_create_rec(void);
INT camp_create_rec(void);
INT scaler_create_rec(void);
INT check_run_number(INT rn);
INT update_histo_arrays();

// IMUSR probably can't trigger a save since we have to wait for the
// event and save each time anyway -  but manually triggers CVAR events from fe_camp
/* needed for mdarc_musr to trigger TD-MUSR events */
BOOL trigger_histo_event(INT *state_running);
// needed for TD-MUSR save now command
INT    setup_hot_save();
void   hot_save (INT, INT, void * );


/* needed for get_FE_client_name */
INT get_FE_client_name(char *pname); /* get frontend client name */
char FE_ClientName[256];    /* the actual client name of the frontend (fev680 or femusr)*/
char TD_ClientName[] = "fev680";
char I_ClientName[] = "femusr";
 
HNDLE  hSave;
BOOL   gbl_saving_now; // global flag needed for TD-MUSR hotlink

// this for MDARC stopping the run
HNDLE hCF;
MUSR_TD_ACQ_CLIENT_FLAGS fcf;
char client_str[132];
// end of MDARC stop run section

// globals
//          (globals that are shared with bnmr_darc.c are in mdarc.h)
caddr_t pHistData;
DWORD  time_save, last_time, start_time,stop_time=0;
INT    hBufEvent;
INT    request_id;
INT   status;
BOOL  mdarc_need_update;
DWORD mdarc_last_call;
BOOL gbl_rn_check=FALSE;
INT  last_processed_sn;

//HNDLE hMDarc; // handles for mdarc (in mdarc.h)
INT run_state;
BOOL write_data; // odb  mdarc flag enable_mdarc_logging
BOOL running_already = FALSE;
MUSR_TD_ACQ_MDARC fmdarc;
MUSR_I_ACQ_SETTINGS imusr;
MUSR_TD_ACQ_SETTINGS tdmusr;
CAMP_SETTINGS camp;
SCALER_SETTINGS scaler;
HNDLE hIS, hC, hScal, hTDS;
const INT musr_mode=2; /* needed for perl script get_next_run_number.pl 
			 both Integral and TD-MUSR use ppg_mode=2 (no midas logger) */ 

HNDLE hbz,hfgb, hlgb, hfbb, hlbb; // bin zero, first/last good bin first/last background bin
BOOL debug_mult;

char perl_script[80]; /* get_run_number.pl  Directory now read from odb in setup_hotlink */

/* initialize values (declared in mdarc.h) */
BOOL   bGotEvent = FALSE;
INT last_saved_SN=-1;
double *pCampData = NULL;
IMUSR_CAMP_VAR *campVars = NULL;

//temp
caddr_t pMaxPointer;
FILE *fileH; // temp

BOOL tr_flag;
BOOL fe_flag;  
char lc_beamline[6];


char gpcPrgName[] = "mdarcMusr";

#include "client_flags.c"  // code for client_flags handling
#include "get_musr_type.c"
/*----- prestart  ----------------------------------------------*/
INT tr_prestart(INT rn, char *error)
{

  int j;
  char str[128];

  /* Move the run number checking from tr_start to tr_prestart so we can be sure of stopping the
     run from starting if there is a failure 
      - still can't stop run reliably so implement client flags
*/

  printf("tr_prestart: starting\n");

  bGotEvent= FALSE;
  last_processed_sn = last_saved_SN=-1; // initialize

  /* Use of cm_exist:

 Purpose: Check if a MIDAS client exists in current experiment

  Input:
    char   name             Client name
    BOOL   bUnique          If true, look for the exact client name.
                            If false, look for namexxx where xxx is
                            a any number

  Output:
    none

  Function value:
    CM_SUCCESS              Client found
    CM_NO_CLIENT            No client with that name


     e.g.   status=cm_exist("mdarc",TRUE);
     Compare status with CM_NO_CLIENT or CM_SUCCESS

     or call with FALSE and "febnmr"  
     e.g.   status = cm_exist("febnmr", FALSE);
     in this case it will return CM_SUCCESS for febnmr, febnmr2, febnmr1

     To check febnmr2 specifically have to specify:
            status = cm_exist("febnmr2", TRUE);

 */
  status=cm_exist(ClientName,FALSE); 
  if(status == CM_SUCCESS)
  {
      cm_msg(MERROR, "tr_prestart","Another copy of mdarc_musr may be already running");
      
      if(debug_mult)
	printf("tr_prestart: debug for testing... allowing a second copy of mdarc\n");
      else
	return(CM_NO_CLIENT); /* return an error */
  }
  if (rn == 0)
    printf ("tr_prestart:  this is a dummy call for initialization\n");

  // client flags
  if(rn != 0)
    {   // do on a genuine run start only
      printf("tr_prestart: run %d is starting\n",run_number);
      start_time =  ss_time(); // set a timer when run was started
      tr_flag = FALSE; // and a flag for check_client_flags
      //  DO NOT CLEAR CLIENT FLAGS HERE ( this is what cleared musr_config's flag after it was set)
      //  they are cleared after a stop ready for next time.

      // get the latest value of this
      size = sizeof(fcf.enable_client_check);
      sprintf(str,"%s/enable client check",client_str);
      status = db_get_value(hDB, 0, str, &fcf.enable_client_check, &size, TID_BOOL, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"tr_prestart","could not get value for \"%s\"  (%d)",str,status);
	  return(status);
	}
      if(debug_cf)printf("Disable Client flag check = %d\n",fcf.enable_client_check);

    }
  else
    {
      start_time = 0; // dummy value
      if(run_state == STATE_STOPPED)
	{
	  printf("tr_prestart: dummy run start, run is STOPPED, clearing client flags\n");
	  status = clear_client_flags(); // clear the client flags of all clients that use a tr_start (+ mdarc's)
	  if(status != DB_SUCCESS)
	    return status;
	}
    }  

   status = update_histo_arrays(); // copy histograms arrays to mdarc area at begin of run
 

  /* update the whole mdarc record first to reflect any changes */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_prestart","Failed to retrieve mdarc record  (%d)",status);
    write_message1(status,"tr_prestart");
    return(status);
  }

  /* Imusr or TD-Musr */
  status = get_musr_type(&fe_flag);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR, "tr_prestart", "cannot determine MUSR run type (I or TD)");
      return DB_NO_ACCESS;
    }
  if(!fe_flag)
    {
      cm_msg(MERROR, "tr_prestart", "wrong frontend is booted");
      if(rn != 0) return DB_NO_ACCESS; /* error if genuine run start */
    }  

  printf("tr_prestart: frontend client name is \"%s\"\n",FE_ClientName);



    /* Get the beamline */
  
  /* the beamline for MUSR should have been copied into mdarc/histograms/musr/beamline
     by musr_config  (BNMR code is absent here) */


  strcpy(beamline,fmdarc.histograms.musr.beamline);
  for (j=0; j<strlen(beamline); j++)
    {
    beamline[j] = toupper (beamline[j]); /* convert to upper case */
    lc_beamline[j]= tolower  (beamline[j]); /* and lower case for opening perl txt files */
    }

  if(debug) printf(" beamline:  %s\n",beamline);
  
  if (strlen(beamline) <= 0 )
    {
      printf("No valid beamline is supplied\n");
      return (DB_INVALID_PARAM);
    }

  /* look for automatic run number check */
  if (fmdarc.disable_run_number_check)
    {
      status = check_run_number(rn);
      if (status != DB_SUCCESS)
	return (status); // check_run_number wrote an error message
    } 
  else    /* automatic run number checking is enabled */
    {
      status = auto_run_number_check(rn);
      if (status != DB_SUCCESS)
	return (status); // auto_run_number_check failed
      
    } // end of automatic run number checking
  
  /* make sure that suppress_save_temporarily flag has not been left set */
  if (fmdarc.suppress_save_temporarily)
    {
      printf("prestart: clearing suppress_save_temporarily flag\n");
      fmdarc.suppress_save_temporarily=FALSE;
      size = sizeof( fmdarc.suppress_save_temporarily);
      status = db_set_value(hDB, hMDarc, "suppress_save_temporarily", 
			    &fmdarc.suppress_save_temporarily, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tr_prestart", "cannot set mdarc flag  \"suppress_save_temporarily\" (%d) ",status);
	  return status;
	}   
    }
  printf("prestart is returning\n");

  return (status);
} // end of prestart
  
/*----- start  ----------------------------------------------*/
INT tr_start(INT rn, char *error)
{
  char str[128];
  char string[256];
  HNDLE hRM,  hKeyTmp;
  char  cmd[132];
  int j;
  int stat;


  /* called every new run and as a dummy when this program is started */
  //if(debug)
    printf("tr_start starting, setting firstxTime true\n");

  firstxTime = TRUE;  /* set global flag so bnmr_darc opens camp connection on begin run  */
  /*  stop_time = 0; // initialize  no longer needed for MUSR */
  sprintf(archiver,"%s",fmdarc.archiver_task) ;  // archiver is in mdarc.h and passed to darc_files
  
  printf("\ntr_start: mdarc parameters:\n");
  printf("   Time of last save: %s\n",fmdarc.time_of_last_save);
  printf("   Last saved filename: %s\n",fmdarc.last_saved_filename);
  printf("   Saved data directory: %s\n",fmdarc.saved_data_directory);
  printf("   # versions before purge: %d\n",fmdarc.num_versions_before_purge);
  printf("   End of run purge/rename/archive flag: %d\n",fmdarc.endrun_purge_and_archive);
  printf("   Suppress save of data temporarily flag: %d\n",fmdarc.suppress_save_temporarily);
  printf("   Archived data directory: %s\n",fmdarc.archived_data_directory);
  printf("   Last archived file:  %s\n",fmdarc.last_archived_filename);
  printf("   Save interval (sec): %d\n",fmdarc.save_interval_sec_);
  printf("   Archiver is : %s\n",archiver);
  printf("\n");

  // get_musr_type was called by prestart
  if(I_MUSR)
    {      /* update the imusr record to reflect any changes */
      size = sizeof(imusr);
      status = db_get_record (hDB, hIS, &imusr, &size, 0);/* get the whole record for imusr */
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"tr_start","Failed to retrieve imusr record  (%d)",status);
	  write_message1(status,"tr_start");
	  return(status);
	}
      
      /* update the camp record  to reflect any changes */
      size = sizeof(camp);
      status = db_get_record (hDB, hC, &camp, &size, 0);/* get the whole record for camp */
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"tr_start","Failed to retrieve camp record  (%d)",status);
	  write_message1(status,"tr_start");
	  return(status);
	}
    }
  else
    {   // TD-MUSR
      /* get the number of bins & banks  from the mdarc area (updated by musr_config at
         begin-of-run for both real and test mode */
      nHistBins =  fmdarc.histograms.num_bins; //fmdarc set up by setup_hotlink
      nHistBanks =  fmdarc.histograms.number_defined;  
      if(debug)printf("tr_start: TDMUSR - number of bins = %d;  no. defined histograms = %d\n",nHistBins,nHistBanks);      
    }

  if(rn != 0)
    cm_msg(MINFO,"tr_start","In the process of starting run %d",
           run_number);

  write_data = fmdarc.enable_mdarc_logging;
  if(write_data)
    {
      if(debug)printf ("tr_start: logging is enabled\n");
      
      /* set up hot link on toggle on a genuine run start (or if run is going) 
	 hot link on toggle is closed by tr_stop
      */
      if (rn != 0  ||   run_state == STATE_RUNNING )
	{
	  if(debug)printf("tr_start: Calling setup_hot_toggle\n");
	  status = setup_hot_toggle();
	  if(status!=DB_SUCCESS)
	    printf("tr_start: Failure from setup_hot_toggle. Can still save data, but toggle won't work\n");
	  

	      
	  /* set up hot link on "save now" on a genuine run start (or if run is going) 
	     hot link on is closed by tr_stop
	  */
	  
	  if(debug)printf("tr_start: Calling setup_hot_save\n");
	  status = setup_hot_save();
	  if(status!=DB_SUCCESS)
	    printf("tr_start: Failure from setup_hot_save. Can still save data, but hot key won't work\n");
	  
	  if(I_MUSR)
	    {
	      /* IMUSR : allocate the space for saving the data points */
	      status = allocate_storage(); // returns the number of bytes required for histos
	      if (pHistData == NULL)
		{
		  cm_msg(MERROR,"tr_start","Error allocating total space for histograms (%d bytes)"
			 ,status);
		  return DB_INVALID_PARAM;
		}
	      
	      /* IMUSR : open or re-open the data log file */
	      
	      if (Imusr_log_fileHandle) 
		fclose (Imusr_log_fileHandle);
	      Imusr_log_fileHandle = NULL;
	      if (*imusr.imdarc.log_file)
		{
		  Imusr_log_fileHandle = fopen (imusr.imdarc.log_file, "w");
		  if ( Imusr_log_fileHandle == NULL ) 
		    {
		      cm_msg(MERROR,"tr_start","Error opening log file %s for writing.",
			     imusr.imdarc.log_file);
		    }
		  else
		    printf("tr_start: successfully opened log file %s for writing \n",imusr.imdarc.log_file);
		}
	      fileH=Imusr_log_fileHandle; // temp
	    } // end of I_MUSR only
	  
	}
    } /* if write_data */
  else
      printf ("tr_start: logging is currently disabled\n");
    

  /* set up hot link on frontend client flag on a genuine run start (or if run is going) 
     hot link on frontend client flag is closed by tr_stop
  */
  if (rn != 0  ||   run_state == STATE_RUNNING )
    {
      if(debug_cf)printf("tr_start: Calling setup_hot_clients\n");
      stat =  setup_hot_clients();
      if (stat != DB_SUCCESS)
	{
	  return(status); // failure from hot links
	}
      
      
      /* set mdarc's client flag to SUCCESS now */
      if(debug_cf)printf("tr_start: setting mdarc client flag TRUE\n");
      fcf.mdarc=TRUE;
      size = sizeof(fcf.mdarc);
      status = db_set_value(hDB, hCF, "mdarc", &fcf.mdarc, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tr_start", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
	  return status;
	}   
    }
  printf("tr_start: returning success\n");
  return DB_SUCCESS;
}

/* ----  resume  ------------------------------ */
INT tr_resume(INT rn, char *error)
{
  int i;
  
  /* an event is sent automatically by the frontend at resume transitions
   */
  
  printf("tr_resume is starting... \n");
  
  if(!write_data)
    {
      printf("tr_resume : data logging is disabled in odb \n");
      return (DB_SUCCESS);  /* not writing data */
    }
  if ( fmdarc.suppress_save_temporarily)
    {
      cm_msg(MINFO,"tr_resume","odb flag \"suppress_save_temporarily\" is set. Data will not be saved");
      return (DB_SUCCESS);   
    }

    
  if (gbl_saving_now)
    {
      cm_msg(MINFO,"tr_resume","bnmr_darc is already in the process of saving the data");
      return SUCCESS;
    }  
  
  gbl_saving_now = TRUE; // set a flag to tell hot_save that a save is in progress
  last_time = ss_time(); // reset the time
  
  /* I-MUSR  */
  if(I_MUSR)
    {  
      cm_msg(MINFO,"tr_resume","Run has been resumed... calling bnmr_darc to save the I-MUSR data");
      status = bnmr_darc(pHistData);
      gbl_saving_now = FALSE;
      return(SUCCESS);
    }
  
  // TD-MUSR
  cm_yield(2000); // wait 2s for the data triggered by the resume transition
  
  if (TD_wait_data(7))
    {
      // bGotEvent is true; we have data
      cm_msg(MINFO,"tr_resume","Run has been resumed... calling bnmr_darc to save the data");
      /* save the event */ 
      status = TD_call_bnmr_darc();
    }
  gbl_saving_now = FALSE;
  
  return SUCCESS;
}


/* ----  pause  ------------------------------ */
INT tr_pause(INT rn, char *error)
{
  int i;

  /* an event is sent automatically by the frontend at pause transitions
     if run is about to be killed, suppress_save_temporarily flag should be set by the mui
  */

  printf("tr_pause is starting... \n");
  
  if(!write_data)
    {
      printf("tr_pause : data logging is disabled in odb \n");
      return (DB_SUCCESS);  /* not writing data */
    }
  
  
  if( fmdarc.suppress_save_temporarily)
    {
      if (gbl_saving_now)
	{
	  cm_msg(MINFO,"tr_pause",
		 "suppress_save_temporarily flag set, but bnmr_darc already in process of saving data");
	  /* we are probably going to kill the run anyway. tr_stop will flush buffers etc */
	}
      else
	{ /* gbl_saving_now is false */
	  cm_msg(MINFO,"tr_pause","event will not be saved (suppress_save_temporarily flag is set)");
	  if(pHistData) 
	    {   
	      printf("tr_stop: abandoning last event, freeing pHistData =%p and setting it to NULL\n",pHistData);
	      free (pHistData); 
	      pHistData = NULL;
	    }
	  if (hBufEvent)
	    {
	      if(debug)printf("tr_pause: flushing the cache to get rid of abandoned event\n");
	      status = bm_flush_cache(hBufEvent, TRUE);
	      if(status != SUCCESS)
		printf("tr_pause: bm_flush_cache returns status %d\n",status);
	    }
	}
    }
  else
    {      
      if (gbl_saving_now)
	{
	  cm_msg(MINFO,"tr_pause","bnmr_darc is already in the process of saving the data");
	  return SUCCESS;
	}  
      
      gbl_saving_now = TRUE; // set a flag to tell hot_save that a save is in progress
      last_time = ss_time(); // reset the time
      
      /* I-MUSR  */
      if(I_MUSR)
	{  
	  cm_msg(MINFO,"tr_pause","Run has been paused... calling bnmr_darc to save the I-MUSR data");
	  status = bnmr_darc(pHistData);
	  gbl_saving_now = FALSE;
	  return(SUCCESS);
	}
      
      // TD-MUSR
      cm_yield(2000); // wait 2s for the data triggered by the pause transition
      
      if (TD_wait_data(7))
	{
	  // bGotEvent is true; we have data
	  cm_msg(MINFO,"tr_pause","Run has been paused... calling bnmr_darc to save the data");
	  /* save the event */ 
	  status = TD_call_bnmr_darc();
	}
      gbl_saving_now = FALSE;
    }
  return SUCCESS;
}

/*----- stop  ----------------------------------------------*/
INT tr_stop(INT rn, char *error)
{
  
  char str[128], filename[128], archived_filename[128] ;/* standard length 128 */
  INT next, nfile;
  BOOL flag;
  BOOL archived;
  HNDLE hktmp;
  INT i;

  /* remove hot link on "save now" */
  if(hSave) 
    {
      if(debug)
	printf("tr_stop: closing record for \"save now\", hSave=%d\n",hSave);
      db_close_record(hDB, hSave);
    }
    
  /* remove hot link on client flags while the run is off */  
  status = close_client_hotlinks();
  if(status != DB_SUCCESS)
    printf("tr_stop: error closing client hotlinks\n");

   start_time = 0; /* make sure check_client_flags is not called if run stopped soon after
		      starting. */  
  /* clear client success flags for next time */
  status = clear_client_flags();
  if(status != DB_SUCCESS)
    printf("tr_stop: error clearing client success flags\n");


  if(!write_data) return SUCCESS;

  if(debug)printf("tr_stop: In the process of stopping the run %d\n", rn);

  if (hBufEvent)
    {
      if(debug)printf("tr_stop: flushing the cache\n");
      status = bm_flush_cache(hBufEvent, TRUE);
      if(status != SUCCESS)
	printf("tr_stop: bm_flush_cache returns status %d\n",status);
    }
  /*  if (hBufEvent)
    {
      INT n_bytes, n_try=0;
      do
	{
	  bm_get_buffer_level(hBufEvent, &n_bytes);
	  cm_yield(100);
	} while (n_try++ < 5);
    }
  */
 // no need to set gbl_saving_now flag as hotlink is closed   

  if( fmdarc.suppress_save_temporarily)
    {  // do not save the last event
      if(debug) printf("tr_stop: suppress_save_temporarily flag (%d)  is TRUE\n"
		       ,fmdarc.suppress_save_temporarily);
      cm_msg(MINFO,"tr_stop","Last event will not be saved (kill button sets this flag)");
      if(pHistData) 
	{   
	  printf("tr_stop: abandoning last event, freeing pHistData =%p and setting it to NULL\n",pHistData);
	  free (pHistData); 
	  pHistData = NULL;
	} 
    }
  else  // save the last event
    {

      /* I-MUSR  */
      if(I_MUSR)
	{  
	  if(debug)printf("tr_stop: Calling bnmr_darc to save the I-MUSR data\n");
	  status = bnmr_darc(pHistData);
	}
      else /* TD-MUSR */
	{
	  /* The final event is sent by frontend at EOR - no need to trigger (TD-MUSR) */
	  if (TD_wait_data(7))
	    {         // bGotEvent is true; we have data
	      cm_msg(MINFO,"tr_stop","Calling bnmr_darc to save the final data for this run");	
	      status = TD_call_bnmr_darc();
	    }
	  else
	    cm_msg(MINFO,"tr_stop","No final data is available for this run");
	} // else TD-MUSR
    } // end else save_last_event  
  
  /*
    TOGGLE should be available only for TD-MUSR
    if the toggle flag is set, we are in the middle of the toggle procedure,
    and haven't saved a file with the new run number 
    - this will happen if no valid data is available  */
  
  /* check whether toggle flag is still set  */
  if(toggle)
    {
      cm_msg(MINFO,"tr_stop","Warning - toggle is set. There may not be a final saved file\n");
      
      toggle = FALSE;  /* set global toggle false */
      /* turn toggle bit off in odb  */
      size = sizeof(toggle);
      status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "tr_stop", "cannot clear toggle flag ");
      
      if(debug) printf ("tr_stop:  Toggle is cleared \n");
      
    }     // if toggle
  
  /* remove hot link on toggle while the run is off */
  if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
    {
      if(debug)printf("tr_stop: Closing toggle hotlink\n");
      db_close_record(hDB, hktmp);
    }
  
  /* kill script also sets this flag */
  if( ! fmdarc.endrun_purge_and_archive)
    {
      if(debug) printf("tr_stop: End of run purge/rename/archive procedure flag (%d)  is FALSE\n"
		       ,fmdarc.endrun_purge_and_archive);
      cm_msg(MINFO,"tr_stop","Saved file WILL NOT be purged/renamed/archived (kill button sets this flag)");
    }
  else
    {
      if(debug)
	{
	  printf(" End of run purge/rename/archive procedure flag (%d)  is TRUE\n"
		 ,fmdarc.endrun_purge_and_archive);
	  printf("tr_stop: saved_data_directory: %s\n",fmdarc.saved_data_directory);
	  printf("tr_stop: archived_data_directory: %s\n",fmdarc.archived_data_directory);
	  printf("calling darc_rename with run number %d   \n",rn);
	}
      
      /* purge/rename final run file  & archive */    
      status = darc_rename_file(rn, fmdarc.saved_data_directory
				, fmdarc.archived_data_directory, filename, archived_filename, &archived);
      if (status == SUCCESS)
	{      
	  /* update filename in odb 
	     &  make sure we write correct length string to odb */
	  sprintf(fmdarc.last_saved_filename, "%s", filename);
	  sprintf(str,"last_saved_filename");
	  if(debug) printf(" tr_stop: writing filename= %s to odb key %s \n",
			   fmdarc.last_saved_filename, str);
	  size=sizeof(fmdarc.last_saved_filename);
	  status = db_set_value(hDB, hMDarc, str, fmdarc.last_saved_filename, size, 
				1, TID_STRING);
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"tr_stop","cannot write to odb key /equipment/%s/mdarc/%s", eqp_name,str);
	      write_message1(status,"tr_stop");
	    }
	  if(archived)
	    {
	      /* update last archived filename in odb 
		 &  make sure we write correct length string to odb */
	      sprintf(fmdarc.last_archived_filename, "%s", archived_filename);
	      sprintf(str,"last_archived_filename");
	      if(debug) printf(" tr_stop: writing filename= %s to odb key %s \n",
			       fmdarc.last_archived_filename, str);
	      size=sizeof(fmdarc.last_archived_filename);
	      status = db_set_value(hDB, hMDarc, str, fmdarc.last_archived_filename, size, 
				    1, TID_STRING);
	      if (status != DB_SUCCESS)
		{
		  cm_msg(MERROR,"tr_stop","cannot write to odb key /equipment/%s/mdarc/%s", eqp_name,str);
		  write_message1(status,"tr_stop");
		}
	    } // if archived
	} // status==success
    } // end of purge/rename flag set
  
  if (debug) 
    printf("tr_stop: releasing camp connection and saving odb\n");
  DARC_release_camp(); /* close the camp connection on end of run */

  if(I_MUSR)
    {
      /* Close the plain-text data log file, if any */
      if (Imusr_log_fileHandle) 
	fclose (Imusr_log_fileHandle);
      Imusr_log_fileHandle = NULL;
    }

  odb_save(rn, fmdarc.saved_data_directory);  // save odb

  /* MUSR no longer check run number after stop... MUI checks at BOR 
  stop_time = ss_time(); // set a timer when run was stopped (if saving data only)
  //                          this should get automatic run number checker to run after the run is ended
  */

  return SUCCESS;
}

/*----- process_event for I-MUSR ----------------------------------------------*/
void  process_event_I(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
  char  bk[5], banklist[STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  double * pdata;
  INT    bn, ii, jj, i,j,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    num_HistBanks;
  INT    num_HistBins; // TD-MUSR
  INT    bank_length; /* local variable for I-MUSR */
  char   type[10];
  char   str[128];
  DWORD  itemp;


  
  pmbh = (BANK_HEADER *) pevent;
  
  if(debug_proc)
    printf("\n process_event_I Starting with Ser: %ld, ID: %d, size: %ld and bGotEvent %d\n",
	   pheader->serial_number, pheader->event_id, pheader->data_size,bGotEvent);
  
  first = TRUE; 
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) printf("process_event_I #banks:%i Bank list:-%s-\n", nbk, banklist);
  
  /*
    process histogram bank
  */
  
  /* For imusr we are looking for the following banks:
     ONE IDAT  scaler data bank  - sent by frontend, then save data
     DARC header BOR EOR
     CAMP info  IMUSR_CAMP_VAR camp_info; BOR and EOR
     CVAR values   periodic, read when running, update ODB - manually trigger ?
     
  */
  
  
  
  num_HistBanks=0; /* zero number of histogram banks in this event (local)
		      no. of variables in the bank */
  
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
  {
    /* bank name */
    strncpy(bk, &banklist[jj], 4);
    bk[4] = '\0';
    if (  (strncmp(bk,"IDAT",4) == 0)  )
    {
      if(debug_proc)printf ("Found bankname IDAT , bk=%s\n",bk);
      num_HistBanks++;
      
      /* Find the length of the bank  */
      
      status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
      
      
      if(debug_proc)
	printf("bank type = %d, length = %d \n",bktyp,bklen);
      if (bktyp != TID_DOUBLE )
      {
	cm_msg(MERROR,"process_event_I","Error - Data type %d illegal for Histogram banks\n",bktyp);
	return;
      }
      bank_length = bk_locate(pmbh, bk, &pdata); /* bank length */
      if (bank_length == 0)
      {
	cm_msg(MERROR,"process_event_I","Error - Bank %s length = 0; Bank not found", bk);
	return;
      }
      else
	if(debug_proc)printf("Number of words in bank is %d\n",bank_length);
    }
    else
      if(debug_proc)printf ("bankname is NOT IDAT , bk=%s\n",bk); 
  } // end of for loop
  


  if(debug_proc)printf("process_event_I event contains %d front-end data banks\n",num_HistBanks);  
  if(num_HistBanks != 1)
  {
    /* only 1 bank allowed for IMUSR */
    cm_msg(MERROR,"process_event_I","Error - More than 1 (i.e. %d) IDAT Banks found in the event", num_HistBanks);
    return;
  }
  if(debug_proc)
    {
      for (i=0; i<bank_length; i++)
	printf("index %d  contents = %f -> %d \n",i, pdata[i],  (DWORD)pdata[i] );
    }
  /* read number of words expected in bank from odb */
  size = sizeof(imusr.imdarc.num_datawords_in_bank);
  status = db_get_value(hDB, hIS, "imdarc/num datawords in bank", &imusr.imdarc.num_datawords_in_bank, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"process_event_I","could not get value for \"../imdarc/num datawords in bank\" (%d)",status);
    return;
  }
  /* For IMUSR, the number of words in the IDAT bank determines the number of histograms saved */
  
  nHistograms = imusr.imdarc.num_datawords_in_bank -1 ; // first value (data point counter) is not histogrammed
  
  if(debug_proc)printf("process_event_I: No. data words expected (from odb): %d; no. data in bank:%d\n",
		       imusr.imdarc.num_datawords_in_bank,bank_length);
  
  
  if( bank_length != imusr.imdarc.num_datawords_in_bank )  /* compare with expected value supplied by odb  */
  {
    cm_msg(MERROR, "process_event_I", "Frontend has sent %d words in bank IDAT. Expected %d",
	   bank_length, imusr.imdarc.num_datawords_in_bank);
    return;
  }
  
    
  if(!write_data)
  {
    /* bail out at this point if we are not saving the data */
    if(debug || debug_proc) printf("process_event_I not saving data since logging is disabled\n"); 
    return;
  }
  /* check we have some storage available */
  if (pHistData == NULL)
  {
    cm_msg(MERROR,"process_event_I", "Space for histogram storage has not been allocated" );
    return;
  }
  
  
  /* store the data into the array pointed at by pHistData */
  gbl_data_point = (INT) (pdata[0]);
  if(debug_proc)printf("Data point : %d \n",gbl_data_point);
// data point gives the index into the arrays and the number of bins in the histogram
  nHistBins =  gbl_data_point +1;
  
  if (nHistBins >  imusr.imdarc.maximum_datapoints)
  {
    cm_msg(MERROR, "process_event_I", "Front end has sent %d data points this run, which exceeds maximum allowed (%d)",
	   nHistBins, imusr.imdarc.maximum_datapoints);
    cm_msg(MERROR, "process_event_I", "***  No more datapoints can be stored this run. Stop run and restart! *** ");
    /* set mdarc's client flag to FAILURE so run will be stopped */
    if(debug_cf)printf("process_event_I: setting mdarc client flag FALSE\n");
    fcf.mdarc=FALSE;
    size = sizeof(fcf.mdarc);
    status = db_set_value(hDB, hCF, "mdarc", &fcf.mdarc, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "process_event_I", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
    return;
  }
  // gbl_data_point gives the number of bins in each histogram to be saved
  
  /* write to plain-text data log file */
  // TEMP:
  if(Imusr_log_fileHandle != fileH)printf("*** error fileH has changed\n");
  if ( Imusr_log_fileHandle )
    fprintf (Imusr_log_fileHandle,"%4d",gbl_data_point);


  pdata++; // skip over no. of points - not histogrammed.
  

  for(i=0; i<nHistograms; i++)  // for the number of histograms
  {
    offset =  i * imusr.imdarc.maximum_datapoints ; /* offset in words into
						       pHistData */
    itemp = (DWORD) ((*pdata)+0.49) ;

    ( (int*)(pHistData + (offset*sizeof(int))))[gbl_data_point] = itemp;
    if(debug_proc)
      printf("Index %d  offset into pHistData=%d : Copied *pdata = %f ->  0x%x (%d)  to pHistData[%d]=0x%x\n",
	   i,offset , *pdata, itemp,itemp, gbl_data_point+offset,
	   ((int*)pHistData)[gbl_data_point +offset]);
    pdata++;


    if(Imusr_log_fileHandle != fileH)printf("*** error fileH has changed (2)\n");

    /* write to plain-text data log file */
    if ( Imusr_log_fileHandle )
      fprintf (Imusr_log_fileHandle," %8d",itemp);

  } // done all histograms

  if ( Imusr_log_fileHandle ) 
    { 
      fprintf (Imusr_log_fileHandle,"\n");
      fflush( Imusr_log_fileHandle );
    }

  if(debug_dump)
    {
      /* check the input params each time so our array dump can increase
	 as more data points are added */
      db=dump_begin;
      dl=dump_length;
      /* check parameters dump_begin, dump_length are valid */
      if(db < 0 || db > gbl_data_point) db = 0; // begin of dump
      if(dl < 1 || (dl + db) > gbl_data_point)
	dl = gbl_data_point + 1  - db;
      if((db+dl) <= gbl_data_point) db=gbl_data_point-dl+1;
      printf("debug_dump -  Dump_length is  %d, dump_begin=%d\n",
	     dl,db);
      
      printf("%s bank: data point %d, offset in individual copied array will be: %d\n",
	     bk,gbl_data_point ,db);
      
      printf("Dumping contents of arrays : \n");
      for (j=0; j<nHistograms; j++)
	{
	  offset =  j * imusr.imdarc.maximum_datapoints; /* offset in words to start of histogram in pHistData */
	  printf("Histogram %d starting at Offset = %d\n",j,offset);
	    for (i=db; i< (dl + db) ;i++)
	      {
		printf("Data point %d:  pHistData[%d]=0x%x (%d)\n",
		       i, (i+offset),  ((int*)pHistData)[i+offset], ((int*)pHistData)[i+offset]) ;
	      }
	}
      printf("gbl_data_point = %d\n",gbl_data_point);
    } // end of debug_dump
  if(debug_proc)
    printf("process_event_I  setting bGotEvent TRUE\n");
  
  bGotEvent = TRUE;
  
  return;
}

/*----- process_event for TD-MUSR  ----------------------------------------------*/
void  process_event_TD(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
  char  bk[5], banklist[STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  DWORD * pdata;
  WORD  * pWdata;
  INT    bn, ii, jj, i,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    num_HistBanks; /* local counter */
  INT    num_HistBins; /* local counter */
  char   type[10];
  char   str[128];
  INT    num_bytes;
  
  pmbh = (BANK_HEADER *) pevent;

  if(debug_proc)
    printf("\n process_event_TD: Starting with Ser: %ld, ID: %d, size: %ld and bGotEvent %d\n",
		  pheader->serial_number, pheader->event_id, pheader->data_size,bGotEvent);
  else
    printf("\n");
  printf(" +++++ process_event_TD:  processing event SerNum: %d +++++\n",
	 pheader->serial_number);
  last_processed_sn =  pheader->serial_number;
  first = TRUE; 
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) printf("process_event_TD: #banks:%i Bank list:-%s-\n", nbk, banklist);

  /*
    process histogram bank
  */
  
/* Assuming each bank name is 4 characters only &   all histogram banks begin with HI or HM.   
     All histogram banks have the same length.
     There may be banks present that are not histograms  */
  
  num_HistBanks=0; /* zero number of histogram banks in this event (local) */
  
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
    {
      /* bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"HM",2) == 0) ||  (strncmp(bk,"HI",2) == 0) )
	{
	  /*if(debug_proc)printf ("Found bankname HI or HM , bk=%s\n",bk);*/
	  num_HistBanks++;
	  /* Use the length of the bank to allocate space */
	  
          if (first)  /* assumes all banks are the same length */
	    {
	      first = FALSE;
	      status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);

              if(debug_proc)
		printf("bank type = %d, length = %d bytes\n",bktyp,bklen);
              if (bktyp != TID_WORD && bktyp != TID_DWORD )
		{
		  cm_msg(MERROR,"process_event_TD","Error - Data type %d illegal for Histogram banks\n",bktyp);
		  return;
		}
	      num_HistBins = bk_locate(pmbh, bk, &pdata); /* bank length */
              if (num_HistBins == 0)
		{
		  cm_msg(MERROR,"process_event_TD","Error - Bank %s length = 0; Bank not found", bk);
		  return;
		}    
	    }
          
	}
      /* else
	 if(debug_proc)printf ("bankname did NOT contain HI or HM , bk=%s\n",bk); */
      
    }


  if(debug_proc)printf("process_event_TD: event contains %d front-end histogram banks\n",num_HistBanks);  

  if(num_HistBanks != nHistBanks)  /* compare with expected value from mdarc area */
  {
    /* At BOR  process_event_TD gets called before tr_start has updated the record (MUSR trigger event only) 
       Read the sizes from odb to check
    */
    if(debug_proc)printf("process_event_TD: Front end has sent %d histogram banks. Expected %d\n",
			 num_HistBanks,nHistBanks);
    size = sizeof(nHistBanks);
    status = db_get_value(hDB, hMDarc, "histograms/number defined", &nHistBanks, &size, TID_INT, FALSE);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"process_event_TD","could not get value for \"../mdarc/histograms/number defined\" (%d)",status);
	return;
      }
    if(debug_proc)printf("process_event_TD: Reread nHistBanks from odb as %d\n",nHistBanks);
    /* now check again */
    if(num_HistBanks != nHistBanks)  /* compare with expected value from mdarc area */
      {
	cm_msg(MERROR, "process_event_TD", "Front end has sent %d histogram banks. Expected %d",
	       num_HistBanks,nHistBanks);
	return;
      }
  }
  
  if(num_HistBins != nHistBins)  /* compare with expected value from mdarc area */
    {
      
      /* At BOR process_event_TD gets called before tr_start has updated the record (MUSR only)
	 Read the sizes from odb to check
      */
      if(debug_proc)printf("process_event_TD: Front end has sent %d histogram bins. Expected %d\n",
			   num_HistBins,nHistBins);
      size = sizeof(nHistBins);
      status = db_get_value(hDB, hMDarc, "histograms/num bins", &nHistBins, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"process_event_TD","could not get value for \"../mdarc/histograms/num bins\" (%d)",status);
	  return;
	}
      if(debug_proc)printf("process_event_TD: Reread nHistBins from odb as %d\n",nHistBins);
      /* now check again */
      
      if(num_HistBins != nHistBins)  /* compare with expected value from mdarc area */
	{
	  cm_msg(MERROR, "process_event_TD", "Front end has sent %d histogram bins. Expected %d",
		 num_HistBins,nHistBins);
	  return;
	}
    }
	 
  if(pHistData)
    {
      cm_msg(MINFO,"process_event_TD", "pHistData is not NULL...  an event may escape being saved");
      printf("process_event_TD: freeing pHistData=%p to avoid possible memory leak\n",pHistData);	
      free(pHistData); pHistData = NULL;
    }

   /* assume histogram banks are all of length nHistBins. This will be checked later. 
      allocate full space once only

      Note for MUSR: data is stored as DWORD in pHistData even if the bank is WORD; 
     DARC_write only deals with DWORD - this ought to be changed so it can deal with WORD 

     add an extra word for the serial number of the bank
 */
  num_bytes = ( nHistBins * sizeof(DWORD) * nHistBanks) + sizeof(DWORD);
  if(debug_proc) 
    printf("Histogram allocation %d bytes \n",num_bytes);
  pHistData = (caddr_t) malloc(num_bytes ); /* nHistBanks histograms */
  if (pHistData == NULL)
    {
      cm_msg(MERROR,"process_event_TD","Error allocating total space for TD histograms (%d bytes)"
	     ,num_bytes);
      return;
    }
  else
    printf("process_event_TD: pHistData = %p and serial no = %d \n",pHistData, pheader->serial_number);
  /* remember pointer to where the serial number is stored (after all the data) */
  pSN = (caddr_t)(pHistData + num_bytes - sizeof(DWORD)); /* address of stored serial number */
  if(debug)
    printf("pSN = %p ; pHistData = %p; num_bytes=%d; sizeof(DWORD)=%d\n",
	   pSN,pHistData,num_bytes,  sizeof(DWORD));
  ((int*)pSN)[0] =  pheader->serial_number  ;


  /* assuming that each bank has always 4 char */
  hn=0;  /* initialize histogram counter */

  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4) 
    {
      /* get bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"HM",2) == 0) ||  (strncmp(bk,"HI",2) == 0) )
	{
	  /* if(debug_proc)printf ("Got bankname HI or HM , bk=%s\n",bk);*/
	  
	  if(debug_proc)printf("working on histogram bank %s\n",bk);
	  status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
	  bkitem = bk_locate(pmbh, bk, &pdata);
          if (bktyp == TID_WORD)
            sprintf(type,"TID_WORD");
          else if (bktyp == TID_DWORD)
            sprintf(type,"TID_DWORD");
          else
	    {
	      cm_msg(MERROR,"process_event_TD",
		     "Error - unexpected bank type (%d) for bank %s", bktyp,bk);
	      return;
	    }
	  if (debug_proc) printf(
				 "process_event_TD: found bank %s length %d type %s\n",
                                 bk, bkitem,type);
	  if (bkitem == 0)
	    {
	      cm_msg(MERROR,"process_event_TD","Error - Bank %s length = 0; Bank not found", bk);
	      return;
	    }
	  
          if (bkitem != nHistBins)
	    {
	      cm_msg(MERROR,"process_event_TD","Unexpected bank length (%d) for bank %s; expected # bins = %d",
		     bkitem, bk, nHistBins);
	      return;
	    }
	  
	  offset = hn * nHistBins; /* offset in words into pHistData */
	  hn++;      /* increment histogram number for next time */
	  if(debug_proc)printf("Offset in words into pHistData for histogram %s is %d\n",bk,offset);
	  if (bktyp == TID_DWORD)
	    memcpy(pHistData + offset*sizeof(int),  pdata, nHistBins*sizeof(DWORD) );
	  else   // WORD data copied to DWORD array
	    {
	      if(debug_proc) printf("Copying %d words to  (DWORD)pHistData\n",nHistBins);
	      pWdata = (WORD *) pdata;
	      for(i=0; i<nHistBins; i++)
		{
		  ( (int*)(pHistData + (offset*sizeof(int))))[i] = *pWdata;
		  pWdata++;
		}
	      //printf("copy done\n");
	    }
          
	  if(debug_dump)
	    {
	      /* check parameters dump_begin, dump_length are valid */
	      if(dump_begin < 0) dump_begin = 0;
	      if( (dump_length + dump_begin) > bkitem)
		{
		  dump_length = bkitem   - dump_begin;
		  printf("debug_dump - Invalid dump_length. Setting dump_length to %d\n",dump_length);
		}
	      printf("%s bank: offset in copied array will be: %d\n",bk ,offset);
	    
	      for (i=dump_begin; i< (dump_length + dump_begin) ;i++)
		{
		  printf("%s bank: Copied pdata[%d] = 0x%x to pHistData[%d]=0x%x\n",
                         bk , i, pdata[i], i+offset, ((int*)pHistData)[i+offset]);
		}
	    }
	}
      /*  else
	  if(debug_proc)printf("Bankname %s did not contain HM or HI\n",bk); */ 
    }

  if(hn != nHistBanks)
    {
      cm_msg(MERROR,"process_event_TD","Unexpected error in # histograms detected. Expected %d, got %d",
	     nHistBanks,hn);
      return;
    }
  
  if(debug_proc)printf("process_event_TD: setting bGotEvent TRUE\n");
  bGotEvent = TRUE;
  
  return;
}


/*---- HOT_LINKS ---------------------------------------------------*/
INT setup_hotlink()
{
  /* does all initialization including setting up hotlinks */
  char str[128];
  HNDLE hktmp;
  char  *s;
  INT i,j;
  
  if(debug) printf("setup_hotlink starting\n");

  status=cm_exist("mdarc",FALSE); 
  //printf("status after cm_exist for mdarc = %d (0x%x)\n",status,status);
  if(status == CM_SUCCESS)
    {
      cm_msg(MERROR, "setup_hotlink","Another mdarc client has been detected");
      if(debug_mult)
	printf("setup_hotlink: debug... allowing a 2nd copy of mdarc\n");
      else
	return(CM_NO_CLIENT); /* return an error */
    }



  status=mdarc_create_record();
  if(status != DB_SUCCESS)
    return status;
  
  /*
    get the path for the perl script run_number_check.pl
  */
  sprintf(perl_script,"%s",fmdarc.perlscript_path);
  trimBlanks(perl_script,perl_script);
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_script,'/');
  i= (int) ( s - perl_script );
  j= strlen( perl_script );
 
  //printf("setup_hotlink: string length of perl_script %s  = %d, last occurrence of / = %d\n", perl_script, j,i);
  if ( i == (j-1) )
  {
    //if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ... \n");
    perl_script[i]='\0';
  }
  strcpy(perl_path,perl_script); /* save in global; needed later (also by bnmr_darc for MUSR) */
  strcat(perl_script,"/get_next_run_number.pl");
  if(debug)printf("setup_hotlink: using perl_script = %s\n",perl_script);

/* create the record for IMUSR */
  status = imusr_create_rec();
  if (status != DB_SUCCESS)
  {
    if(debug)
    printf("setup_hotlink: Failed call to imusr_create_rec (%d)\n",status);
    return(status);
  }
  //new
/* create the record for TDMUSR */
  status = tdmusr_create_rec();
  if (status != DB_SUCCESS)
  {
    if(debug)
    printf("setup_hotlink: Failed call to tdmusr_create_rec (%d)\n",status);
    return(status);
  }


/* create the record for camp */
  status = camp_create_rec();
  if (status != DB_SUCCESS)
  {
    if(debug)
    printf("setup_hotlink: Failed call to camp_create_rec (%d)\n",status);
    return(status);
  }

 /* create the record for scalers */
  status = scaler_create_rec();
  if (status != DB_SUCCESS)
  {
    if(debug)
    printf("setup_hotlink: Failed call to scaler_create_rec (%d)\n",status);
    return(status);
  } 
  /*  S E T   U P   H O T   L I N K S */

  if(debug_mult)
    {
      printf("setup_hotlink: debug... not setting up hotlinks\n");
      return SUCCESS;
    }

  /* set hot link on num_versions_before_purge  */
  if (db_find_key(hDB, hMDarc, "num_versions_before_purge", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.num_versions_before_purge);
    status = db_open_record(hDB, hktmp, &fmdarc.num_versions_before_purge, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for num_versions_before_purge (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
    else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for num_versions_before_purge   (%d)", status );

  
  /* set hot link on endrun_purge_and_archive  */
  if (db_find_key(hDB, hMDarc, "endrun_purge_and_archive", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.endrun_purge_and_archive);
    status = db_open_record(hDB, hktmp, &fmdarc.endrun_purge_and_archive, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for endrun_purge_and_archive (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for endrun_purge_and_archive (%d)", status );


   
  /* set hot link on suppress_save_temporarily  */
  if (db_find_key(hDB, hMDarc, "suppress_save_temporarily", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.suppress_save_temporarily);
    status = db_open_record(hDB, hktmp, &fmdarc.suppress_save_temporarily, size, MODE_READ, NULL, NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for suppress_save_temporarily (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for suppress_save_temporarily (%d)", status );
 
    
  /* set hot link on save_interval  */
  if (db_find_key(hDB, hMDarc, "save_interval(sec)", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fmdarc.save_interval_sec_);
    status = db_open_record(hDB, hktmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for save_interval (%d)", status );
      write_message1(status,"setup_hotlink");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hotlink","Failed to find key for save_interval (%d)", status );

  /* saving previous save_Time for limit check later in update_Time */
  time_save = fmdarc.save_interval_sec_;
  if((INT)time_save < 1 || (INT)time_save > 24*3600 )                 /* time_save > 1sec, < 1 day */
  {
    fmdarc.save_interval_sec_ = time_save = 60;
    size=sizeof(fmdarc.save_interval_sec_);
    status = db_set_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
  }

  gbl_saving_now = FALSE; // initialize - only used for MUSR

  if (db_find_key(hDB, hMDarc, "histograms/musr/save now", &hSave) != DB_SUCCESS)
    {
      hSave=0;
      cm_msg(MERROR,"setup_hotlink","Failed to find key for \"save now\" (%d)", status );
      cm_msg(MINFO,"setup_hotlink","Hot link to save data is not available");
    }
  else
    if(debug)printf("setup_hotlink: got key for hSave=%d\n",hSave);
  /* hot link is actually opened at begin of run */


  if(debug)
  {
    printf("setup_hotlink: Updated parameters from odb\n");
    printf("    time_save = %d \n",time_save);
    printf("    arch_dir = %s\n", fmdarc.archived_data_directory);
    printf("    save_dir = %s\n", fmdarc.saved_data_directory);
    if(fmdarc.endrun_purge_and_archive)
      printf("  eor purge/archive flag is set : %d\n", fmdarc.endrun_purge_and_archive);
    else
      printf("  eor purge/archive flag is NOT set : %d\n", fmdarc.endrun_purge_and_archive);
    if(fmdarc.suppress_save_temporarily)
      printf("  suppress_save_temporarily flag is set : %d\n", fmdarc.suppress_save_temporarily);
    else
      printf("  suppress_save_temporarily flag is NOT set : %d\n", fmdarc.suppress_save_temporarily);
    
}

  status = create_ClFlgs_record(); // get the record for the client flags
  if (status != DB_SUCCESS)
    printf("setup_hotlink: error return from create_ClFlgs_record (%d)\n",status);


  /* set up hot link on musr_config flag */
  if (db_find_key(hDB, hCF, "musr_config", &hktmp) == DB_SUCCESS)
  {
    size = sizeof(fcf.musr_config);
    status = db_open_record(hDB, hktmp, &fcf.musr_config, size, MODE_READ, hot_musr_config , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for client flag musr_config (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for client flag musr_config (%d)", status );
      return(status);
    }


  // set up hotlinks on /Equipment/MUSR_TD_ACQ/Settings/Mode/Histograms area
  if (hTDS == 0)
    { 
      cm_msg(MERROR, "setup_hotlink","Cannot set up hotlinks in /Equipment/MUSR_TD_ACQ/Settings/Mode/Histograms area; Key hTDS is zero");
  return status;
    }

 /* 1. set up hot link on bin zero */
  status = db_find_key(hDB, hTDS, "mode/histograms/bin zero", &hbz);
  if(status == DB_SUCCESS)
  {
    size = sizeof(tdmusr.mode.histograms.bin_zero);
    status = db_open_record(hDB, hbz, &tdmusr.mode.histograms.bin_zero, size, MODE_READ, hot_histo_arrays , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for bin zero  (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for bin zero (%d) ", status );
      return(status);
    }



 /* 2. set up hot link on first good bin */
  status = db_find_key(hDB, hTDS, "mode/histograms/first good bin", &hfgb);
  if(status == DB_SUCCESS)
  {
    size = sizeof(tdmusr.mode.histograms.first_good_bin);
    status = db_open_record(hDB, hfgb, &tdmusr.mode.histograms.first_good_bin, size, MODE_READ, hot_histo_arrays , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for first good bin (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for first good bin (%d) ", status );
      return(status);
    }

   /* 3. set up hot link on last good bin */
  status = db_find_key(hDB, hTDS, "mode/histograms/last good bin", &hlgb);
  if(status == DB_SUCCESS)
  {
    size = sizeof(tdmusr.mode.histograms.last_good_bin);
    status = db_open_record(hDB, hlgb, &tdmusr.mode.histograms.last_good_bin, size, MODE_READ, hot_histo_arrays , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for last good bin (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for last good bin (%d) ", status );
      return(status);
    }

  /* 4. set up hot link on first background bin */
  status = db_find_key(hDB, hTDS, "mode/histograms/first background bin", &hfbb);
  if(status == DB_SUCCESS)
  {
    size = sizeof(tdmusr.mode.histograms.first_background_bin);
    status = db_open_record(hDB, hfbb, &tdmusr.mode.histograms.first_background_bin, size, MODE_READ, hot_histo_arrays , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for first background bin (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for first background bin (%d) ", status );
      return(status);
    }


  /* 5. set up hot link on last background bin */
  status = db_find_key(hDB, hTDS, "mode/histograms/last background bin", &hlbb);
  if(status == DB_SUCCESS)
  {
    size = sizeof(tdmusr.mode.histograms.last_background_bin);
    status = db_open_record(hDB, hlbb, &tdmusr.mode.histograms.last_background_bin, size, MODE_READ, hot_histo_arrays , NULL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for last background bin (%d)", status );
	write_message1(status,"setup_hotlink");
	return(status);
      }
  }
  else
    {
      cm_msg(MERROR,"setup_hotlink","Failed to find key for last background bin (%d) ", status );
      return(status);
    }
  printf("setup_hotlink: successfully set up hotlinks on ..musr_td_acq/settings/mode/histogram keys\n");
  return(status);
}

/* ----------------------------------------------------------------------------------------*/

void hot_histo_arrays(HNDLE hDB, HNDLE htmp ,void *info)
     
/* ----------------------------------------------------------------------------------------*/
     
{
  /*  
          If users update histo arrays during run, copy to mdarc area
          Replaces musr_update.pl
  */ 
  int i;
   
  INT my_data[16];
  INT size=sizeof(my_data);
  char str[128];
  INT status;
  
  if (run_state != STATE_RUNNING )
    return;


  status=db_get_data(hDB, htmp, &my_data, &size, TID_INT); 
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"hot_histo_arrays","failure reading data from key %d (%d)",htmp,status);
      return;
    }

  if(htmp == hbz) 
    {
      //      printf("hot_histo_arrays: Bin zero changed\n");
      sprintf(str, "histograms/bin zero"); 
    }
  else if (htmp == hfgb)
    {
      //printf("hot_histo_arrays: First good bin changed\n"); 
      sprintf(str, "histograms/first good bin"); 
    }
  else if (htmp == hlgb)
    {
      //printf("hot_histo_arrays: Last good bin changed\n");  
      sprintf(str, "histograms/last good bin"); 
    }
  else if (htmp ==hfbb)
    {
      //printf("hot_histo_arrays: First background bin changed\n"); 
      sprintf(str, "histograms/first background bin"); 
    }      
  else if (htmp ==hlbb)
    {
      //printf("hot_histo_arrays: Last background bin changed\n");  
      sprintf(str, "histograms/last background bin"); 
    }
  else
    {
      cm_msg(MERROR,"hot_histo_arrays","Error: something changed but what?");
      return;
    }

  //for (i=0; i<16; i++)
    //printf("hot_histo_arrays:  my_data[%d]=%d\n",my_data[i]);
  status = db_set_value(hDB, hMDarc, str, &my_data, sizeof(my_data), 16, TID_INT);
  
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"hot_histo_arrays","error from db_set_value for str=%d (%s)",str,status);


  // get the record again now it has been updated
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record  */
	  
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"hot_histo_arrays","Failed to get the whole record for %s (%d)",str,status );
      write_message1(status,"hot_histo_arrays");
    }


  return;
}

INT update_histo_arrays()
{
  
 /* copy histograms arrays to mdarc area in case of changes */

  // get record for /Equipment/musr_TD_acq/
  INT size,status;
  char str[128];
  if(hTDS==0)
    {
      cm_msg(MERROR,"update_histo_arrays","hTDS is zero. Cannot update histograms area");
      return DB_NO_ACCESS;
    }
  
  size = sizeof(tdmusr);
  status = db_get_record (hDB, hTDS, &tdmusr, &size, 0);/* get the whole record for tcmusr */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"update_histo_arrays","Failed to retrieve %s record  (%d)",str,status);
      write_message1(status,"tdmusr_create_rec");
      return(status);
    }

  // Update mdarc/histograms area

    sprintf(str, "histograms/bin zero"); 
    size=sizeof(tdmusr.mode.histograms.bin_zero);
    status = db_set_value(hDB, hMDarc, str, &tdmusr.mode.histograms.bin_zero, size, 16, TID_INT);
    if(status != DB_SUCCESS)
      {
	cm_msg(MERROR,"update_histo_arrays","Failed to write %s array to mdarc area  (%d)",str, status);
	return (status);
      }

    sprintf(str, "histograms/first good bin"); 
    size=sizeof(tdmusr.mode.histograms.first_good_bin);
    status = db_set_value(hDB, hMDarc, str, &tdmusr.mode.histograms.first_good_bin, size, 16, TID_INT);
    if(status != DB_SUCCESS)
      {
	cm_msg(MERROR,"update_histo_arrays","Failed to write %s array to mdarc area  (%d)",str, status);
	return (status);
      }

   sprintf(str, "histograms/last good bin"); 
    size=sizeof(tdmusr.mode.histograms.last_good_bin);
    status = db_set_value(hDB, hMDarc, str, &tdmusr.mode.histograms.last_good_bin, size, 16, TID_INT);
    if(status != DB_SUCCESS)
      {
	cm_msg(MERROR,"update_histo_arrays","Failed to write %s array to mdarc area  (%d)",str, status);
	return (status);
      }

   sprintf(str, "histograms/first background bin"); 
    size=sizeof(tdmusr.mode.histograms.first_background_bin);
    status = db_set_value(hDB, hMDarc, str, &tdmusr.mode.histograms.first_background_bin, size, 16, TID_INT);
    if(status != DB_SUCCESS)
      {
	cm_msg(MERROR,"update_histo_arrays","Failed to write %s array to mdarc area  (%d)",str, status);
	return (status);
      }

   sprintf(str, "histograms/last background bin"); 
    size=sizeof(tdmusr.mode.histograms.last_background_bin);
    status = db_set_value(hDB, hMDarc, str, &tdmusr.mode.histograms.last_background_bin, size, 16, TID_INT);
    if(status != DB_SUCCESS)
      {
	cm_msg(MERROR,"update_histo_arrays","Failed to write %s array to mdarc area  (%d)",str, status);
	return (status);
      }

    return status;
}

/*------------------------------------------------------------------*/
INT setup_hot_toggle()
{

  /* called by tr_start only on genuine run start, so toggle cannot be set before 
     a valid run number is established */
  char str[128];
  HNDLE hktmp;
  
  
  if(debug) printf("setup_hot_toggle starting\n");

  if(debug_mult)
    {
      printf("setup_hot_toggle: debug... not setting up hotlink\n");
      return SUCCESS;
    }
/* set up hot link on toggle flag */
  if (db_find_key(hDB, hMDarc, "toggle", &hktmp) == DB_SUCCESS)
  {
    /* make sure toggle bit is switched off to start with */
    toggle = FALSE ;  // global
    size = sizeof(toggle);
    status = db_set_value(hDB, hMDarc, "toggle", &toggle, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      cm_msg(MERROR, "setup_hot_toggle", "cannot initialize toggle flag ");
    
    status = db_open_record(hDB, hktmp, &fmdarc.toggle, size, MODE_READ, hot_toggle , NULL);
    if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hot_toggle","Failed to open record (hotlink) for toggle (%d)", status );
      write_message1(status,"setup_hot_toggle");
      return(status);
    }
  }
  else
    cm_msg(MERROR,"setup_hot_toggle","Failed to find key for toggle (%d)", status );
  
  return(status);
}


/*------------------------------------------------------------------*/

INT setup_hot_save()

/* ----------------------------------------------------------------------------------------*/
{
  /* called after by tr_start on genuine run start
     to  set up hot link on "save now" flag */

  char str[128];
  BOOL off = FALSE;
  
  
  if(debug)printf("setup_hot_save starting with hSave=%d\n",hSave);
  
  /* global key hSave has been found in setup_hotlink */

  if (hSave != 0)
    {

      /* make sure global flag "save now"  is set off to start with */
      status = db_set_value(hDB, hMDarc, "histograms/musr/save now", &off, sizeof(BOOL), 1, TID_BOOL);
      if (status != DB_SUCCESS)
	cm_msg(MERROR, "setup_hot_save", "cannot initialize \"save now\" flag ");
    
      status = db_open_record(hDB, hSave, &fmdarc.histograms.musr.save_now, size, MODE_READ, hot_save , NULL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hot_save","Failed to open record (hotlink) for \"save now\" (%d)", status );
	  write_message1(status,"setup_hot_save");
	  return(status);
	}
    }
  else
    cm_msg(MERROR,"setup_hot_save","No key for \"save now\" (%d)", status );
  
  return(status);
}

/* ----------------------------------------------------------------------------------------*/

void hot_save (HNDLE hDB, HNDLE hSave ,void *info)
     
/* ----------------------------------------------------------------------------------------*/
     
{
  /*  Save now is done by setting the flag ..mdarc/histograms/musr/save now
      may be set by  user button "Save" or by "Write data" in mui    
  */ 
  
  // uses BOOL save_now - a global 
  char str[128];
  char cmd[256];
  BOOL off=FALSE;
  INT i,state_running;
  
  printf ("\n ====== hot_save: starting .....\"save now\" has been touched, bGotEvent=%d\n",bGotEvent);
  /* "save now"  has been touched - user wants to save the histograms now */
  if(!write_data)
    {
      cm_msg(MINFO,"hot_save","Hot save CANNOT SAVE DATA since data logging is disabled in odb");
      return;
    }
  if ( fmdarc.suppress_save_temporarily)
    {
      cm_msg(MINFO,"hot_save","odb flag \"suppress_save_temporarily\" is set. Data will not be saved");
      return;   
    }

  if (gbl_saving_now)
    {
      cm_msg(MINFO,"hot_save","bnmr_darc is already in the process of saving the data");
      return;
    }
  
  gbl_saving_now = TRUE;   // set a flag to say data is being saved

  last_time = ss_time(); // reset the time

  if(I_MUSR)    /* I-MUSR */
    {   /* I-MUSR does not use a manual trigger */ 
      printf("calling bnmr_darc to save I-MUSR data, bGotEvent=%d, pHistData=%p \n",bGotEvent,pHistData);
      status = bnmr_darc(pHistData);    
      gbl_saving_now = FALSE;
      return;
    }


  // TD_MUSR
  if (bGotEvent)    
    {
      printf("hot_save: there is an event pending that has not yet been saved\n");
      cm_msg(MINFO,"hot_save","found unsaved event.... bGotEvent=%d, SN last processed : %d  SN last saved : %d;",
	     bGotEvent,last_processed_sn, last_saved_SN);
      /* call bnmr_darc to save pending event */
      cm_msg(MINFO,"hot_save","calling bnmr_darc to save pending event before triggering a new event");
      TD_call_bnmr_darc();
    }

 
  printf("hot_save: Calling trigger_histo_event \n");
  if( trigger_histo_event(&state_running) ) // an event has been triggered (write_data MUST also be true)
    {
      INT msg;
      printf("hot_save: event has been triggered\n");
      msg = cm_yield(1000); /* call yield */
      ss_sleep(2000); // wait 2 seconds
      if(TD_wait_data(7) )
	{
	  printf("\n");
	  cm_msg(MINFO,"hot_save","Hot save is calling bnmr_darc to save the data");
	  if(debug)printf("hot_save: Calling TD_call_bnmr_darc to save the data\n");
	  
	  /* save the event */ 	  
	  TD_call_bnmr_darc();
	}
    } // if trigger_histo_event
  else
    {
      printf("hot_save: could not trigger a TD event\n");
    }
 

  // TEMP trigger several more events to test
#ifdef TEMP
  printf(" TEMP... triggering 3 more events for testing ...\n");
  for (i=0; i<3; i++)
    {
      if(trigger_histo_event(&state_running))
	printf("hot_save: an event has been triggered\n");
      else
	printf("hot_save: failure triggering an event\n");
      cm_yield(8000);
    }
#endif
 
  gbl_saving_now = FALSE;


  return;
} 
 
INT TD_wait_data (INT ncount)
{
  /* input:   no. times to go around wait loop
    returns:  bGotEvent i.e. true if data, false otherwise */
  INT i;

  i=0;
  while(!bGotEvent)
    {
      printf("TD_wait_data: waiting for data from the frontend & process_event_TD to run (%d)\n",i);
      cm_yield(1500); /* call yield */
      ss_sleep(500); // sleep
      i++;
      if(i>ncount)
	{
	  printf("TR_wait_data:  ... given up waiting for bGotEvent\n");
	  break; // event is not coming for some reason
	}
    }

  if(!bGotEvent) 
    cm_msg(MINFO,"TD_wait_data","No new data is available to save (no event received from frontend)");
  return (bGotEvent);
}


INT TD_call_bnmr_darc(void)
{
  INT status;

  printf("TD_call_bnmr_darc: calling bnmr_darc with \n");
  printf("   bGotEvent=%d; SN last processed=%d; SN last saved=%d;pHistData=%p \n",
	 bGotEvent, last_processed_sn, last_saved_SN, pHistData);

  status = bnmr_darc(pHistData);
  if(status != SUCCESS) 
    printf("TD_call_bnmr_darc: Failure status (%d) from bnmr_darc\n",status);

  // if(debug)
    {
      printf("TD_call_bnmr_darc: after call to bnmr_darc,\n");
      printf("bGotEvent=%d, SN last processed  : %d SN last saved : %d, pHistData=%p\n\n",
	     bGotEvent, last_processed_sn, last_saved_SN, pHistData);
    }

  if(last_processed_sn != last_saved_SN)
    cm_msg(MINFO,"TD_call_bnmr_darc", "last processed and last saved event serial nos. are not the same");
  
  if(pHistData) 
    {   
      printf("TD_call_bnmr_darc: freeing pHistData =%p and setting it to NULL\n",pHistData);
      free (pHistData); 
      pHistData = NULL;
    } 
  return (status);
}

/*------------------------------------------------------------------*/
void update_Time(HNDLE hDB, HNDLE htmp, void *info)
{

  /* remove hot link */
  if(debug)printf("update_Time starting...present time_save=%d\n",time_save);
  
  db_close_record(hDB, htmp);
  
  /* get the new value */

  size = sizeof(fmdarc.save_interval_sec_);
  status = db_get_value(hDB, hMDarc, "save_interval(sec)", &fmdarc.save_interval_sec_, &size, TID_DWORD, FALSE);

  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"update_Time","could not get new value of save interval ");
      write_message1(status,"update_Time");
      return ;
    }
  if(debug)printf("update_Time: Time_save is to be updated to %d\n",fmdarc.save_interval_sec_);

  if((INT)fmdarc.save_interval_sec_ < 1
     || (INT)fmdarc.save_interval_sec_ > 24*3600 ) /* time_save > 1sec, < 1 day */
    {
      cm_msg(MINFO,"update_Time","Invalid time save interval (%d). Using previous value (%d sec)",
	     fmdarc.save_interval_sec_,time_save);
      fmdarc.save_interval_sec_ = time_save;
      
      /* update the odb with this value */
      if(debug)printf("update_Time: Restoring /Equipment/%s/mdarc/save_interval(sec) to %d\n"
		      , eqp_name, fmdarc.save_interval_sec_);
      size=sizeof(fmdarc.save_interval_sec_);
      status = db_set_value(hDB, hMDarc, "save_interval(sec)"
			    , &fmdarc.save_interval_sec_, size, 1, TID_DWORD);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"update_Time","cannot write to odb key /Equipment/%s/mdarc/save_interval(sec) %d"
		 , eqp_name, status );
	  write_message1(status,"update_Time");
	}   
    }  // end of bad value

  time_save = fmdarc.save_interval_sec_;
  if (debug)printf("update_Time: Time_save is now updated to %d\n",fmdarc.save_interval_sec_);


  /* restore Hot link */
  size = sizeof(fmdarc.save_interval_sec_);
  status = db_open_record(hDB, htmp, &fmdarc.save_interval_sec_, size, MODE_READ, update_Time , NULL);
  if (status != DB_SUCCESS)
  {
      cm_msg(MERROR,"update_Time","Failed to open record (hotlink) for mdarc (%d)", status );
      write_message1(status,"updata_Time");
      return;
  }

   return;
}

/*------------------------------------------------------------------*/

void hot_toggle (HNDLE hDB, HNDLE hktmp ,void *info)
{
/* toggle is done by user button TOGGLE - calls a perlscript toggle.pl which
   sets odb toggle flag on certain conditions - including that automatic run
   numbering in enabled */ 
  
  // uses BOOL toggle - a global in mdarc.h 
  char str[128];
  char cmd[256];
  char old_type[10];
  
  /* toggle bit has been touched - user wants to toggle between real and test
     runs  */
  printf ("hot_toggle: starting ..... toggle has been touched.  \n");
  if(debug)
    printf ("hot_toggle: global toggle flag = %d, run number = %d,  closing record\n",toggle,run_number);
  /* remove hot link while we toggle the run */
  db_close_record(hDB, hktmp);
  
  if (toggle) /* shouldn't happen if hot link is closed */
    {
      printf("hot_toggle: unexpected error - toggle global flag is already set\n");
      return;
    }
  
  
  toggle = TRUE;   /* global flag */
  
  old_run_number=run_number; /* save run number */
  strcpy(old_type,run_type);
  
  if (debug)
    printf("hot_toggle: old run number = %d, and  run type = %s\n",
           old_run_number, old_type);
  
  /* determine the run type */
  if(debug) printf ("hot_toggle: run_type = %s \n",run_type);
  if  (strncmp(run_type,"real",4) == 0)
    {
      if(debug) printf ("hot_toggle: detected present run type as real \n");
      strcpy(run_type,"test"); /* run is real so we now want a test run */
      //if (debug) printf ("now should be test;  run type=%s \n",run_type);
    }
  else if  (strncmp(run_type,"test",4) == 0)
    {
      if (debug) printf ("hot_toggle: detected present run type as test \n");
      strcpy(run_type,"real"); /* run is test so we now want a real run */
      //if (debug) printf ("now should be real;  run type=%s \n",run_type);
    }
  else
    {
      cm_msg(MERROR,"hot_toggle","Unknown run type detected (%s). Cannot toggle run \n",run_type);
      return; 
    }
  if(debug) printf ("Writing new run_type = %s to odb\n",run_type);
  sprintf(str, "/Equipment/%s/mdarc/run type",eqp_name);
  size = sizeof(run_type);
  if(size != sizeof(fmdarc.run_type))
    {
      cm_msg(MERROR,"hot_toggle","cannot update key %s; size mismatch",
	     str);
      return;
    }

  status = db_set_value(hDB, 0, str, run_type, size,1, TID_STRING);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
	     str,status);
      write_message1(status,"hot_toggle");
      return ;
    }    
  
  /*
    Get a new run number with perl script
  */
  if(debug) printf ("hot_toggle: getting a run number for a %s run...\n",run_type);
   

  /* add parameter perl_path */
  sprintf(cmd,"%s %s %s %d %s 0, %s %d",perl_script,perl_path,expt_name,run_number,eqp_name,lc_beamline,musr_mode);
  if(debug) printf("Hot_toggle: sending system command  cmd: %s\n",cmd);
  status =  system(cmd);
  if (status)
    /* cannot get a new run number */
    {
      cm_msg (MERROR," hot_toggle",
	      "A status of %d has been returned from perl script get_next_run_number.pl",status);
      cm_msg (MERROR," hot_toggle",
	      "Check perlscript output file \"/home/%s/online/%s/log/get_next_run_number.txt\" for details",lc_beamline,lc_beamline);
      /* note: if no recent addition to this file, there may be compilation errors in the perl script */
      
      if(debug) printf(" hot_toggle: Resetting run type to previous value (%s)",old_type);
      
      size = sizeof(old_type);
      if(size != sizeof(fmdarc.run_type))
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s; size mismatch",
		 str);
	  return;
	}

      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}
      toggle = FALSE;   /* clear global flag */
      cm_msg (MERROR," hot_toggle","Cannot toggle run. Continuing with run %d",run_number);
      return;
    }
  
  //  get the new run number - this is now a global value in mdarc.h
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"hot_toggle","key not found  /Runinfo/Run number");
      write_message1(status,"hot_toggle");
      return ;
    }
  if (run_number == old_run_number)
    {
      status=cm_msg(MERROR,"hot_toggle","??? Strange error: Run number not toggled after perlscript ");
      /* rewrite old run type */
      //sprintf(str, "/Experiment/edit on start/run type");
      size = sizeof(old_type);
      status = db_set_value(hDB, 0, str, old_type, size,1, TID_STRING);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"hot_toggle","cannot update key %s (%d)",
		 str,status);
	  write_message1(status,"hot_toggle");
	}    
      return;
      
    }
  else
    status=cm_msg(MINFO,"hot_toggle",
                  "* * * *  hot_toggle:  Run number has been toggled from %d to %d ( %s to %s) * * * * ",
                  old_run_number,run_number,old_type, run_type);
  
  return;
}

INT print_file(char* filename)
{
  FILE *FIN;
  char str[256];

  printf ("\n------------------------------------------------------------------------\n");
  printf ("   print_file: Contents of information file %s:\n",filename);
  printf ("--------------------------------------------------------------------------\n");
  FIN = fopen (filename,"r");
  if (FIN == NULL)
  {
      printf ("print_file : Error opening file %s\n",filename);
      return(1);     
  }
  else
  {
    while(fgets(str,256,FIN) != NULL)
      printf("%s",str);
  }
  fclose (FIN);
  printf ("--------------------------------------------------------------------------\n");
  printf ("   print_file: End of information file %s: \n",filename);
  printf ("--------------------------------------------------------------------------\n\n");
  return(0);
}


//---------------------------------------------------------------------------------------
INT get_run_state(INT *pval)
//----------------------------------------------------------------------------------
{
  INT my_run_state;

  *pval=0;

  /* get current run state */
  size = sizeof(my_run_state);
  /* get current run state */
  status = db_get_value(hDB, 0, "/runinfo/State", &my_run_state, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      printf("get_run_state:cannot access /runinfo/State (%d)... retrying after 500ms\n",status); 
      ss_sleep(500); /* wait for a moment */
      status = db_get_value(hDB, 0, "/runinfo/State", &my_run_state, &size, TID_INT, FALSE);
      if(status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_run_state","cannot access /runinfo/State (%d)",status);
	  return status;
	}
    }
  *pval = my_run_state;
  return status;
}


/*-----------------------------------------------------------------------------------------------*/
BOOL trigger_histo_event(INT * pRunstate)
/*-----------------------------------------------------------------------------------------------*/
{
  HNDLE hconn;
  INT run_state;
  BOOL event_triggered;
  
  event_triggered = FALSE;
  gbl_rn_check=FALSE;
  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"trigger_histo_event","key not found /Runinfo/State (%d)",status);
      return(FALSE); //Error
    }
  *pRunstate = run_state;
  if (run_state == STATE_RUNNING)
    {
      // Use the clientname found by get_FE_client_name called from tr_prestart
      status = cm_exist(FE_ClientName,TRUE);
      if(status == CM_SUCCESS)
	{
	  if(debug)printf("trigger_histo_event: trying to connect to the client %s\n",FE_ClientName);
	  status = cm_connect_client (FE_ClientName, &hconn);
	  if(status != RPC_SUCCESS)
	    cm_msg(MERROR,"trigger_histo_event","Cannot connect to frontend \"%s\" (%d)",FE_ClientName,status);
	  else	    {  // successfully connected to frontend client
	    rpc_client_call(hconn, RPC_MANUAL_TRIG, 2); // trigger an event
	    if (status != CM_SUCCESS)
	      cm_msg(MERROR,"trigger_histo_event","Error triggering event from frontend (%d)",status);
	    else
	      {  // successfully triggered event
		if(debug)printf("trigger_histo_event: success from rpc_client_call to trigger event\n");
		event_triggered=TRUE;
		status =cm_disconnect_client(hconn, FALSE);
		if (status != CM_SUCCESS)
		  cm_msg(MERROR,"trigger_histo_event","Error disconnecting client after event trigger(%d)",status);
	      }
	  }
	} // end of cm_exist
      else
	cm_msg(MERROR,"trigger_histo_event","Frontend client %s not running; not triggering an event (%d)",
	       FE_ClientName,status);
    } // end of if running
  return(event_triggered);
}



//--------------------------------------------------------------------------------
INT mdarc_create_record(void)
//---------------------------------------------------------------------------
{
  
  char str[128];
  INT struct_size;
 MUSR_TD_ACQ_MDARC_STR(acq_mdarc_str);

  /* For IMUSR, run state was determined just prior to calling this routine */
 if(TD_MUSR)
   {
     status = get_run_state(&run_state);
     if(status != DB_SUCCESS)
       return status;
   }

  /* find the key for mdarc */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name); 
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      hMDarc=0;
      if(debug) printf("setup_hotlink: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_hotlink","Failure creating mdarc record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hMDarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMDarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "setup_hotlink", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}

      struct_size =   sizeof(MUSR_TD_ACQ_MDARC);

      
      printf("setup_hotlink:Info - size of mdarc saved structure: %d, size of mdarc record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"setup_hotlink","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, str , strcomb(acq_mdarc_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"setup_hotlink","Could not create mdarc record (%d)\n",status);
          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"setup_hotlink","May be due to open records while running. Stop the run and try again");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }
  
  /* try again to get the key hMDarc  */
  
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_hotlink", "key %s not found (%d)", str,status);
    write_message1(status,"setup_hotlink");
    return (status);
  }

  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"setup_hotlink");
    return(status);
  }
  printf("setup_hotlink: returning with status - %d\n",status);
  return(status);
}

//-----------------------------------------------------------------------------------

INT imusr_create_rec(void)
//------------------------------------------------------------------------------
{
  /* create record for IMUSR area - called by setup_hotlink */
  
  
  MUSR_I_ACQ_SETTINGS_STR (musr_i_acq_settings_str);
  char str[128];
  INT size,struct_size;  
  INT status, stat1;


  if(debug)
    printf("imusr_create_rec is starting...\n");
//  I - MUSR
    /* find the key for I-musr area */
  sprintf(str,"/Equipment/%s/Settings",i_eqp_name); 
  status = db_find_key(hDB, 0, str, &hIS);
  if (status != DB_SUCCESS)
    {
      hIS=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for I-musr settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(musr_i_acq_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"imusr_create_rec","Failure creating I_musr settings area (%d)",status);
          write_message1(status,"imusr_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hIS has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hIS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "imusr_create_rec", "error during get_record_size (%d) for imusr/settings record",status);
          write_message1(status,"imusr_create_rec");
	  return status;
	}
      struct_size =   sizeof(MUSR_I_ACQ_SETTINGS);
      
      if(debug)
	printf("imusr_create_rec:Info - size of IMUSR Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("imusr_create_rec:Info - creating IMUSR record due to size mismatch\n");
        cm_msg(MINFO,"imusr_create_rec","creating record (I_musr Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(musr_i_acq_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"imusr_create_rec","Could not create imusr settings record (%d)",status);
          write_message1(status,"imusr_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"imusr_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hIS  */
  
  status = db_find_key(hDB, 0, str, &hIS);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "imusr_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"imusr_create_rec");
    return (status);
  }

  size = sizeof(imusr);
  status = db_get_record (hDB, hIS, &imusr, &size, 0);/* get the whole record for imusr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"imusr_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"imusr_create_rec");
    return(status);
  }

  // print something out from imusr record area
  if(debug)
    printf("imusr_create_rec: current sweep device  is %s\n",imusr.input.sweep_device);
  
  return(SUCCESS);
}



//-----------------------------------------------------------------------------------

INT tdmusr_create_rec(void)
//------------------------------------------------------------------------------
{
  /* create record for TDMUSR settings area - called by setup_hotlink */
  
  
  MUSR_TD_ACQ_SETTINGS_STR (musr_td_acq_settings_str);
  char str[128];
  INT size,struct_size;  
  INT status, stat1;


  if(debug)
    printf("tdmusr_create_rec is starting...\n");
//  TD - MUSR
    /* find the key for TD-musr area */
  sprintf(str,"/Equipment/%s/Settings",eqp_name); 
  status = db_find_key(hDB, 0, str, &hTDS);
  if (status != DB_SUCCESS)
    {
      hTDS=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for TD-musr settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"tdmusr_create_rec","Failure creating TD_musr settings area (%d)",status);
          write_message1(status,"tdmusr_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hTDS has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hTDS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "tdmusr_create_rec", "error during get_record_size (%d) for tdmusr/settings record",status);
          write_message1(status,"tdmusr_create_rec");
	  return status;
	}
      struct_size =   sizeof(MUSR_TD_ACQ_SETTINGS);
      
      if(debug)
	printf("tdmusr_create_rec:Info - size of TDMUSR Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("tdmusr_create_rec:Info - creating TDMUSR record due to size mismatch\n");
        cm_msg(MINFO,"tdmusr_create_rec","creating record (TD_musr Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"tdmusr_create_rec","Could not create tdmusr settings record (%d)",status);
          write_message1(status,"tdmusr_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"tdmusr_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hTDS  */
  

  status = db_find_key(hDB, 0, str, &hTDS);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "tdmusr_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"tdmusr_create_rec");
    return (status);
  }

  size = sizeof(tdmusr);
  status = db_get_record (hDB, hTDS, &tdmusr, &size, 0);/* get the whole record for tcmusr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tdmusr_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"tdmusr_create_rec");
    return(status);
  }

  // print something out from tdmusr.mode.histograms area
  // if(debug)
    printf("tdmusr_create_rec: bin_zero[3]  is %d\n",tdmusr.mode.histograms.bin_zero[3]);
    
  return(SUCCESS);
}

//-----------------------------------------------------------------------------------

INT camp_create_rec(void)
//------------------------------------------------------------------------------
{
  /* create record for CAMP equipment area - called by setup_hotlink */
  
  
  CAMP_SETTINGS_STR (camp_settings_str);
  char str[128];
  INT size,struct_size;  
  INT status, stat1;


  if(debug)
    printf("camp_create_rec is starting...\n");
  /* find a key for camp equipment area */
  sprintf(str,"/Equipment/Camp/Settings"); 
  status = db_find_key(hDB, 0, str, &hC);
  if (status != DB_SUCCESS)
    {
      hC=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for Camp settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(camp_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"camp_create_rec","Failure creating camp settings area (%d)",status);
          write_message1(status,"camp_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hC has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hC, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "camp_create_rec", "error during get_record_size (%d) for camp/settings record",status);
          write_message1(status,"camp_create_rec");
	  return status;
	}
      struct_size =   sizeof(CAMP_SETTINGS);
      
      if(debug)
	printf("camp_create_rec:Info - size of CAMP Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("camp_create_rec:Info - creating CAMP record due to size mismatch\n");
        cm_msg(MINFO,"camp_create_rec","creating record (Camp Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(camp_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"camp_create_rec","Could not create camp settings record (%d)",status);
          write_message1(status,"camp_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"camp_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hC  */
  
  status = db_find_key(hDB, 0, str, &hC);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "camp_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"camp_create_rec");
    return (status);
  }

  size = sizeof(camp);
  status = db_get_record (hDB, hC, &camp, &size, 0);/* get the whole record for camp */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"camp_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"camp_create_rec");
    return(status);
  }

  // print something out from camp record area
  if(debug)
    printf("camp_create_rec: number of camp variables is %d\n",camp.n_var_logged);
  
  return(SUCCESS);
}

//-----------------------------------------------------------------------------------

INT scaler_create_rec(void)
//------------------------------------------------------------------------------
{
  /* create record for scaler equipment area */
  
  
  SCALER_SETTINGS_STR  (scaler_settings_str);
  char str[128];
  INT size,struct_size;  
  INT status, stat1;


  if(debug)
    printf("scaler_create_rec is starting...\n");
  /* find a key for scaler equipment area */
  sprintf(str,"/Equipment/Scaler/Settings"); 
  status = db_find_key(hDB, 0, str, &hScal);
  if (status != DB_SUCCESS)
    {
      hScal=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for Scaler settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(scaler_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"scaler_create_rec","Failure creating scaler settings area (%d)",status);
          write_message1(status,"scaler_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hScal has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hScal, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "scaler_create_rec", "error during get_record_size (%d) for scaler/settings record",status);
          write_message1(status,"scaler_create_rec");
	  return status;
	}
      struct_size =   sizeof(SCALER_SETTINGS);
      
      if(debug)
	printf("scaler_create_rec:Info - size of SCALER Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("scaler_create_rec:Info - creating SCALER record due to size mismatch\n");
        cm_msg(MINFO,"scaler_create_rec","creating record (Scaler Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(scaler_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"scaler_create_rec","Could not create scaler settings record (%d)",status);
          write_message1(status,"scaler_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"scaler_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hScal  */
  
  status = db_find_key(hDB, 0, str, &hScal);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "scaler_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"scaler_create_rec");
    return (status);
  }

  size = sizeof(scaler);
  status = db_get_record (hDB, hScal, &scaler, &size, 0);/* get the whole record for scaler */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"scaler_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"scaler_create_rec");
    return(status);
  }

  // print something out from scaler record area
  if(debug)
    printf("scaler_create_rec: number of scaler inputs is %d\n",scaler.num_inputs);
  
  return(SUCCESS);
}

//----------------------------------------------------------------------------------------
void  allocate_camp_storage( void )
//---------------------------------------------------------------------------------
{
  /* called from allocate_storage, which is called from tr_start */ 
// pCampData is a global
  DWORD nbytes;

  
  if (pCampData)    // CVAR data
    {
      free (pCampData); 
      pCampData = NULL;
    }

  /* allocate full storage space once only at BOR
     imusr.imdarc.maximum_datapoints is the maximum data points allowed
  */


  if(debug_proc)printf("allocate_camp_storage: maximum datapoints =  %d\n",  imusr.imdarc.maximum_datapoints);


  printf("allocate_camp_storage: number of Camp variables=%d\n",camp.n_var_logged);
  
  /* number of IMUSR CAMP histograms = number of camp variables  camp.n_var_logged (c.f. nHistograms)  */
  
  if(camp.n_var_logged <1)
    {
      printf("allocate_camp_storage: no camp variables to log\n");
      return;
    }

  nbytes =  imusr.imdarc.maximum_datapoints * sizeof(double) * camp.n_var_logged;    
  if(debug_proc)
    printf("Histogram allocation max datapoints * # bytes * camp.n_var_logged i.e. %d * %d *%d = %d bytes \n",
	  imusr.imdarc.maximum_datapoints,sizeof(double), camp.n_var_logged ,  nbytes);
  pCampData = (double *) malloc(nbytes ); /* allocate histogram space */
  if (pCampData == NULL)
    cm_msg(MERROR,"allocate_camp_storage","Error allocating total space for camp data %d"
	     ,nbytes);
  
  memset (pCampData,0,nbytes);
  printf("cleared pCampData\n");
  return ;
}
/*-----------------------------------------------------------------------------------*/
INT  allocate_storage( void)
/*-------------------------------------------------------------------------------------------*/
{
  /* allocate storage for IMUSR */

// pHistData is a global
  DWORD nbytes;
  
  if (pHistData) free(pHistData); pHistData = NULL;
 
  /* allocate full storage space once only at BOR
     Note for IMUSR: data is stored as DWORD in pHistData ; 
     DARC_write currently only deals with DWORD

     imusr.imdarc.maximum_datapoints is the maximum data points allowed
  */

  size = sizeof(imusr.imdarc.maximum_datapoints);
  status = db_get_value(hDB, hIS, "imdarc/maximum datapoints", &imusr.imdarc.maximum_datapoints, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"allocate_storage","could not get value for \"../imdarc/maximum datapoints\" (%d)",status);
    return;
  }
  if(debug_proc)printf("allocate_storage: Read maximum datapoints from odb as %d\n", imusr.imdarc.maximum_datapoints);

  /* Calculate the number of IMUSR histograms: this is called nHistograms */
  
  nHistograms = imusr.imdarc.num_datawords_in_bank  -1 ; /* data point counter is not
						     histogrammed ; this gives the NUMBER OF HISTOGRAMS */
  //printf("size of int is %d size of DWORD is %d\n",sizeof(int),sizeof(DWORD) );
  nbytes = imusr.imdarc.maximum_datapoints * sizeof(int) * nHistograms;    
  if(debug_proc)
    printf("Histogram allocation max datapoints * # bytes * nHistos i.e. %d * %d *%d = %d bytes \n",
	 imusr.imdarc.maximum_datapoints,sizeof(int), nHistograms ,  nbytes);
  pHistData = (caddr_t) malloc(nbytes ); /* allocate histogram space */
  if (pHistData == NULL)      cm_msg(MERROR,"allocate_storage","Error allocating total space for histograms %d"
	     ,nbytes);
  pMaxPointer = pHistData + nbytes;
  memset (pHistData,0,nbytes);
  printf("cleared pHistData\n");
  if(debug_proc)
      printf("allocate_storage: maximum pointer for phistData is %p\n",pMaxPointer);

  allocate_camp_storage();
  return (nbytes);

}

void  process_event_cvar(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
  char  bk[5], banklist[STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  double * pdata;
  INT    bn, ii, jj, i,j,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    numBanks;
  INT    bank_length; /* local variable */
  
  
  pmbh = (BANK_HEADER *) pevent;
  
  if(debug_proc)
    printf("\n process_event_cvar: Starting with Ser: %ld, ID: %d, size: %ld \n",
	   pheader->serial_number, pheader->event_id, pheader->data_size);

  /* The serial number should be the same as the IDAT event - can use it to point to correct place in the array */
  
  
  first = TRUE; 
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) 
    printf("process_event_cvar: #banks:%i Bank list:-%s-\n", nbk, banklist);
  numBanks=0;
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
    {
      /* bank name */
      strncpy(bk, &banklist[jj], 4);
      bk[4] = '\0';
      if (  (strncmp(bk,"CVAR",4) == 0)  )
	{
	  if(debug_proc)
	    printf ("Found bankname CVAR , bk=%s\n",bk);
	  numBanks++;
	  
	  /* Find the length of the bank  */
	  
	  status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
	  
	  
	  if(debug_proc)
	    printf("bank type = %d, length = %d bytes\n",bktyp,bklen);
	  if (bktyp != TID_DOUBLE )
	    {
	      cm_msg(MERROR,"process_event_cvar","Error - Data type %d illegal for Camp  banks\n",bktyp);
	      return;
	    }
	  bank_length = bk_locate(pmbh, bk, &pdata); /* bank length */
	  if (bank_length == 0)
	    {
	      cm_msg(MERROR,"process_event_cvar","Error - Bank %s length = 0; Bank not found", bk);
	      return;
	    }
	  else if (bank_length != camp.n_var_logged)
	    {
	      cm_msg(MERROR,"process_event_cvar","Error - Bank %s length = %d; No. of camp variables logged = %d", bk,bank_length,camp.n_var_logged);
	      return;
	    }
	  else
	    {
	      if(debug_proc)
		printf("Number of words in CVAR bank is %d\n",bank_length);
	    }
	}
      else
	{
	  if(debug_proc)
	    printf ("bankname is NOT CVAR , bk=%s\n",bk);
	} 
    } // end of for loop
  
  if(numBanks > 1)
    {
      cm_msg(MERROR,"process_event_cvar","Error - More than 1 (i.e. %d)   %s Banks found ", numBanks,bk);
      return;
    }
  if(debug_proc)
    {
      for (i=0; i<bank_length; i++)
	printf("index %d  contents = %f -> %u \n",i, pdata[i],  (unsigned int)pdata[i] );
    }
  
  if(!write_data)
    {
      /* bail out at this point if we are not saving the data */
      if(debug || debug_proc) 
	printf("process_event_cvar: not saving data since logging is disabled\n"); 
      return;
    }
  
  /* check we have some storage available */
  if (pCampData == NULL)
    {
      cm_msg(MERROR,"process_event_cvar", "Space for camp data storage has not been allocated" );
      return;
    }
  else
    {
    if (debug_proc)
      printf("pCampData=%p\n",pCampData);
    }

  //   pheader->serial_number starts at 1 for first event
  //gbl_camp_data_point =   pheader->serial_number -1 ; // cf gbl_data_pointer which starts at 0
  //     ...it seems to start at zero now...
  gbl_camp_data_point =   pheader->serial_number  ; // cf gbl_data_pointer which starts at 0
  
  if ( pheader->serial_number  >   imusr.imdarc.maximum_datapoints)
    {
      cm_msg(MERROR, "process_event_cvar", "Front end has sent %d camp data points this run, which exceeds maximum allowed (%d)",
	     pheader->serial_number  ,  imusr.imdarc.maximum_datapoints);
      cm_msg(MERROR, "process_event_cvar", "***  No more datapoints can be stored this run. Stop run and restart! *** ");
      /* set mdarc's client flag to FAILURE so run will be stopped */
      if(debug_cf)printf("process_event_cvar: setting mdarc client flag FALSE\n");
      fcf.mdarc=FALSE;
      size = sizeof(fcf.mdarc);
      status = db_set_value(hDB, hCF, "mdarc", &fcf.mdarc, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	  cm_msg(MERROR, "process_event_cvar", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
      return;
    }
  
  for( i=0; i<camp.n_var_logged; i++ )
    {
      offset =  i *  imusr.imdarc.maximum_datapoints ; /* offset in words into
							  pCampData */
      //  if(debug_proc)
	printf("offset=%d;  gbl_camp_data_point=%d\n",offset,gbl_camp_data_point);

      printf("process_event_cvar: i=%d pdata[%d] = %f  \n",i,i,pdata[i]);

//    pCampData[offset + gbl_camp_data_point] =  *((int*)&f); /* float not converted */
      pCampData[offset + gbl_camp_data_point] =  pdata[i];
      if(debug_proc)
	printf("Camp variable %d :  offset %d  gbl_camp_data_point=%d : Copied pdata[%d] = %f   to pCampData[%d]= %f\n",
	       i,offset ,gbl_camp_data_point,i,pdata[i], ( gbl_camp_data_point+offset), pCampData[offset + gbl_camp_data_point]);
      
    }
  
   
  if(debug_proc)
    printf("process_event_cvar:  setting bGotCamp TRUE\n");
  bGotCamp = TRUE;
  
  return;
}
#ifdef GONE
void  process_event_darc(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
}
#endif

void  process_event_camp(HNDLE hBuf, HNDLE request_id, EVENT_HEADER *pheader, void *pevent)
{
   char  bk[5], banklist[STRING_BANKLIST_MAX];
  BANK  *pmbk;
  BANK_HEADER *pmbh;
  DWORD * pdata;
  INT    bn, ii, jj, i,j,  nbk, bkitem, hn;
  DWORD  bklen, bktyp;
  DWORD    offset;
  BOOL   first;
  INT    numBanks;
  INT    bank_length; /* local variable */
  float f;
  INT    my_numCamp;
  
  pmbh = (BANK_HEADER *) pevent;
  
  if(debug_proc)
    printf("\n process_event_camp: Starting with Ser: %ld, ID: %d, size: %ld \n",
	   pheader->serial_number, pheader->event_id, pheader->data_size);
  
  
  first = TRUE; 
  
  nbk = bk_list(pmbh, banklist);
  if(debug_proc) printf("process_event_camp: #banks:%i Bank list:-%s-\n", nbk, banklist);
  for(bn=0, jj=0 ; bn<nbk ; bn++, jj+=4)
  {
    /* bank name */
    strncpy(bk, &banklist[jj], 4);
    bk[4] = '\0';
    if (  (strncmp(bk,"CAMP",4) == 0)  )
    {
      if(debug_proc)printf ("Found bankname CAMP , bk=%s\n",bk);
      numBanks++;
      
      /* Find the length of the bank  */
      
      status = bk_find (pmbh, bk, &bklen, &bktyp, (void *)&pmbk);
      
      
      if(debug_proc)
	printf("bank type = %d, length = %d bytes\n",bktyp,bklen);
      if (bktyp != TID_CHAR )
      {
	cm_msg(MERROR,"process_event_camp","Error - Data type %d illegal for Camp header bank\n",bktyp);
	return;
      } 
      bank_length = bk_locate(pmbh, bk, &pdata); /* bank length */
      if (bank_length == 0)
      {
	cm_msg(MERROR,"process_event_camp","Error - Bank %s length = 0; Bank not found", bk);
	return;
      }

      if(!write_data)
	{
	  /* bail out at this point if we are not saving the data */
	  if(debug || debug_proc) printf("process_event_camp: not saving data since logging is disabled\n"); 
	  return;
	}

      if (bank_length > 0)
	{
	  if( campVars == NULL )	
	    {
	      my_numCamp = bank_length/sizeof(IMUSR_CAMP_VAR);
	      if (my_numCamp != camp.n_var_logged)
		{ 
		cm_msg(MERROR,"process_event_camp",
		       "Num camp variables (%d) from bank %s different from odb (%d)", my_numCamp,bk,camp.n_var_logged);
		return;
		}
	      printf("process_event_camp: reallocating campVars\n");
	      
	  campVars = realloc( (void*) campVars, (size_t) bank_length );
	  if( !campVars )
	    {
	      cm_msg(MERROR,"process_event_camp","Error - could not re-allocate memory for campVars"); 
	      return;
	    }
	  /*  OK, remember number globally */
	  numCamp = my_numCamp;
	}
	else
	  {
	    if( bank_length !=camp.n_var_logged *sizeof(IMUSR_CAMP_VAR) )
	      {
		cm_msg(MERROR,"process_event_camp","Error in %s banklength (%d) ",bk,bank_length); 
		free (campVars);
		campVars = NULL;
		numCamp = 0;
		return;
	      }
	  }
      }
      /*
       * Copy camp variable strucure.  Note that structure contains fixed length 
       * strings, not just pointers, so memcpy does copy everything.
       */
      memcpy( campVars, (char*) pdata, bank_length );
      printf("Copied CAMP header to campVars (%p)\n",campVars);

    }
    else
    {
      if(debug_proc)printf ("bankname is NOT CAMP , bk=%s\n",bk); 
    }
  } // end of for loop
    
}

/*---------------------------------------------------------------------------*/
INT check_run_number(INT rn)
/*  ---------------------------------------------------------------------------*/
{
 
  /* Checks run number and saved directories when automatic run number check is turned off. 
     Perlscripts NOT called in this mode
  */

  INT status;
  INT i,j,len,len1;
  char str[128];
  
  cm_msg(MINFO,"check_run_number","* * WARNING:  Automatic run numbering is disabled. This mode    * *");
  cm_msg(MINFO,"check_run_number","* *           is NOT RECOMMENDED except for emergencies         * *");

  //  get the actual run number;  (run_number is a global in mdarc.h)
  size = sizeof(run_number);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"check_run_number","key not found  /Runinfo/Run number");
      write_message1(status,"check_run_number");
      return (status);
    }
  printf (" Run number = %d\n",run_number);
      
  /* check run number against beamline when automatic run number checking is
     disabled  ONLY  for a genuine run start */
  //printf("rn=%d, run_state=%d\n",rn,run_state);
      
  if ( rn !=0  ||   run_state == STATE_RUNNING )    /* rn not 0 or  run is going already  */
    {      /* check run number against beamline */
      strcpy(run_type,fmdarc.run_type);
      for  (j=0; j< (strlen(run_type) ) ; j++)
	run_type[j] = tolower(run_type[j]); /* convert to lower case */
      trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */  
      
      /* check for test run first */
      printf("Automatic run numbering is DISABLED: checking run number against beamline\n");
      if ( run_number  >= MIN_TEST && run_number <= MAX_TEST  ) 
	{
	  /* test run for any beamline */
	  if  (strncmp(run_type,"test",4) != 0)
	    {
	      cm_msg(MERROR,"check_run_number","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
	      cm_msg(MINFO,"check_run_number","Test runs must be in range %d-%d",MIN_TEST,MAX_TEST);
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number. ");
	      return (DB_INVALID_PARAM);
	    }
	  cm_msg(MINFO,"check_run_number","Starting a TEST data run (%d)",run_number);
	}
      
      else   /* real run */
	{
	  if(run_number  >= MIN_M20 && run_number <= MAX_M20 )        // M20
	    {
	      if (strncmp(beamline,"M20",3) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = M20 for this run number",run_number,beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number ");
		  return (DB_INVALID_PARAM);
		}
	    }
	  else if(  run_number >= MIN_M15 &&  run_number <= MAX_M15 )      // M15
	    {
	      if (strncmp(beamline,"M15",3) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = M15 for this run number",run_number,beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number");
		  return (DB_INVALID_PARAM);
		}
	    }
	  else if(  run_number < MIN_M9B)   // M13
	    {
	      cm_msg(MERROR,"check_run_number",
		     "Run number (%d) matches obsolete beamline (M13)\n",run_number);
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
	    }
	  else if ( run_number  >= MIN_M9B &&  run_number <= MAX_M9B)
	    {
	      if (strncmp(beamline,"M9B",3) != 0)  // M9B
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = M9B for this run number",run_number, beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
		  return (DB_INVALID_PARAM);
		}
	    }
	  else if ( run_number  >= MIN_DEV &&  run_number <= MAX_DEV  )    // DEV
	    {
	      if (strncmp(beamline,"DEV",3) != 0)
		{
		  cm_msg(MERROR,"check_run_number",
			 "Mismatch between run number (%d) & beamline (%s); expect beamline = DEV for this run number",run_number, beamline);
		  cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
		  return (DB_INVALID_PARAM);
		}
	    }

	  else
	    {
	      cm_msg(MERROR,"check_run_number","Run number (%d) does not match a valid beamline\n",run_number); 
	      cm_msg(MINFO,"check_run_number","Test runs must be in range %d to %d",MIN_TEST,MAX_TEST);
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
	    }
	  printf("detected run %d for beamline %s \n",run_number,beamline);
	  /*  real run */
	  if  (strncmp(run_type,"real",4) != 0)
	    {
	      cm_msg(MERROR,"check_run_number","Mismatch between run_type (%s) and run number (%d)",run_type, run_number);
	      cm_msg(MINFO,"check_run_number","Test runs must be in range %d-%d",MIN_TEST,MAX_TEST);
	      cm_msg(MINFO,"check_run_number","Data logging cannot proceed with this run number.");
	      return (DB_INVALID_PARAM);
	    }
	  else
	    cm_msg(MINFO,"check_run_number","Starting a REAL data run (%d), (beamline %s) ",run_number,beamline);  
	}  // end of else real run
      
      
    } /* end of run number check on genuine start or run already going when mdarc starts */
  
      
  return (status);
}


/* ----------------------------------------------------------*/ 
INT auto_run_number_check(INT rn)
/* ----------------------------------------------------------*/ 
{
  char  cmd[132];
  INT j;
  BOOL check;

  printf("auto_run_number_check starting\n");

  check=TRUE;  // default

  if (! fmdarc.enable_prestart_rn_check)
    {   /* automatic rn check at begin run is disabled */
      if (!gbl_rn_check)
	{  // gbl_rn_check failed last check. ( It is set up by auto_run_number_check)
	  cm_msg(MINFO,"auto_run_number_check", "INFO:  rechecking run numbers...");
	}
      else
	check=FALSE;
    }

  if (check)
    {
      printf("calling get_next_run_number.pl and  setting gbl_rn_check FALSE\n");
      
      gbl_rn_check=FALSE; // set flag in case of failure
      
      /*
	perl script to find next valid run number and write it to /Runinfo/run number
	It (should) handle all eventualities  including mdarc restarted midway through a
	run.  It uses run_type to determine what type of run (test/real) the user
	wants, and assigns a run number accordingly.
      */
      sprintf(cmd,"%s %s %s %d %s 0, %s %d",perl_script, perl_path, expt_name,rn,eqp_name,lc_beamline,musr_mode); 
      if(debug) printf("auto_run_number_check: Sending system command  cmd: %s\n",cmd);
      status =  system(cmd);
      if(debug)printf("after system(cmd) status=%d\n",status);
      
      
      if (status)
	{
	  cm_msg (MERROR,"auto_run_number_check","Perl script get_next_run_number.pl returns failure status (%d)",status);
	  cm_msg (MERROR,"auto_run_number_check",
		  "Check perlscript output file \"/home/%s/online/%s/log/get_next_run_number.txt\" for details",lc_beamline,lc_beamline);
	  /* note: if no recent addition to this file, there may be compilation errors in the perl script */
	  return (DB_INVALID_PARAM); // bad status from perl script
	}
      else
	{
	  printf("auto_run_number_check: success, from get_next_run_number.pl,  setting flag TRUE\n");
	  gbl_rn_check=TRUE; // success
	}
    }  // end of if (check)
  else
    {  // don't check the run numbering with the perlscript
      if(debug)printf("auto_run_number_check: NOT checking run number since flag gbl_rn_check=%d\n",gbl_rn_check);
    }
		
      // Remember: 
      //     db_get_value formerly creates the key if it doesn't exist (i.e. returns
      //           no error)
      // Midas 9.0 does not create key if last param is FALSE
      
      //  get the run number which may have been changed by get_next_run_number or by run start
      //  this is now a global value in mdarc.h
      size = sizeof(run_number);
      status = db_get_value(hDB,0, "/Runinfo/Run number",  &run_number, &size, TID_INT, FALSE);
      if (status != DB_SUCCESS)
	{
	  status=cm_msg(MERROR,"auto_run_number_check","key not found  /Runinfo/Run number");
	  write_message1(status,"auto_run_number_check");
	  return (status);
	}
      
      /* and the run type (a global)  */
      strcpy(run_type,fmdarc.run_type);
      for  (j=0; j< (strlen(run_type) ) ; j++)
	run_type[j] = tolower(run_type[j]); /* convert to lower case */
     
      trimBlanks (run_type,run_type); /* note - perl script has checked validity of run_type */  
      printf("auto_run_number_check: run number = %d, run_type=%s\n",run_number,run_type);
      return(status);
}


/* -------------------- save_data --------------------------------*/
INT save_data(INT *msg)
/* ------------------------------------------------------------------*/
{
  BOOL event_triggered=FALSE;
  INT i,status,itmp;


  printf("\n = = = = = save_data: starting sequence to save the data = = = = = = \n");
  if(!write_data)
    {
      if(debug)printf("save_data : data logging is disabled in odb ... not calling bnmr_darc \n");
      return (DB_SUCCESS);  /* not writing data */
    }  
  if ( fmdarc.suppress_save_temporarily)
    {
      cm_msg(MINFO,"save_data","odb flag \"suppress_save_temporarily\" is set. Data will not be saved");
      return (DB_SUCCESS);   
    }


  if(gbl_saving_now)  // a global
    {
      printf("save_data: flag is set to say data is ALREADY being saved (by hotlink \"save now\"\n");
      return (DB_SUCCESS);
    }

  gbl_saving_now = TRUE; // set a flag to tell hot_save that a save is in progress

  /* I-MUSR  */
  if(I_MUSR)
    {  
      if(debug)printf("     save_data: Calling bnmr_darc to save the I-MUSR data\n");
      status = bnmr_darc(pHistData);
      gbl_saving_now = FALSE;
      fflush(stdout); /* output may be sent to a file */
      *msg=itmp;
      return(status);
    }


  // TD-MUSR 
  if(pHistData) 
    printf("save_data:  pHistData = %p bGotEvent=%d \n",pHistData,bGotEvent);

  if (bGotEvent)
    {
      printf("\n %%%%%% save_data: there seems to be an event pending that has not been saved\n");
      cm_msg(MINFO,"save_data","found unsaved event...  bGotEvent=%d, SN last saved : %d; SN last processed : %d",
	     bGotEvent, last_saved_SN,last_processed_sn);
      /* call bnmr_darc to save pending event */
	   cm_msg(MINFO,"save_data","calling bnmr_darc to save pending event\n");
	   TD_call_bnmr_darc();
    }

      
  /* TD MUSR : trigger an event from the frontend  */
  if(debug)printf("save_data: Calling trigger_histo_event; \n");
  event_triggered = trigger_histo_event(&run_state);
  if(debug)
    printf("save_data: returned from trigger_histo_event (event_triggered=%d, run_state=%d)\n",
	   event_triggered,run_state);
  if(event_triggered)  // an event has been triggered (write_data MUST also be true)
    {
      if(debug)printf("save_data: an event has been triggered\n");
      event_triggered = FALSE; // reset flag
      
      cm_yield(1000); // wait 1s for the event to be sent
      
      /* has process_event_TD run yet? (test on bGotEvent) */
      if(TD_wait_data(15) )
	{
	  //if(debug)
	  printf("save_data: Calling TD_call_bnmr_darc to save the TD-MUSR data, pHistData=%p\n",pHistData);
	  TD_call_bnmr_darc();
	}
    } // event_triggered
  else
    {  // event was not triggered
      if (run_state == STATE_RUNNING)  // only send message if we are running
	cm_msg(MINFO,"save_data",
	       "Failure to trigger event from frontend  ... no TD data can be saved \n");
    }
      
  gbl_saving_now = FALSE;  
  fflush(stdout); /* output may be sent to a file */
  *msg=itmp;
  return(status);
}



/*-------------MAIN PROGRAM ----------------------------------------*/

int main(unsigned int argc,char **argv)

/*------------------------------------------------------------------*/
{
  INT     size;
  char   host_name[HOST_NAME_LENGTH];
  char   ch;
  BOOL   daemon=FALSE;
  int j=0;  
  char   dbg[25];
  int    bug;
  int    i;
  INT   msg;

  printf("\n mdarc : Data Logger saves data for both I-MUSR and TD-MUSR experiments \n\n"); 
 
  /* set defaults for globals */
  debug = debug_proc = debug_mud = debug_check = debug_dump = debug_cf = FALSE;
  debug_mult=0; // multiple copies of mdarc
  gbl_saving_now = FALSE;
  dump_begin = 0;
  dump_length = 5;
  run_number = -1; /* initialize to a silly value for go_toggle */ 
  toggle = FALSE;  /* initial value */
  /* end defaults */
  
 
    cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);

  /* initialize global variables */

    sprintf(eqp_name,"MUSR_TD_acq");
    sprintf(i_eqp_name,"MUSR_I_acq");
    sprintf(feclient,"empty");
        
    /* get parameters */
    /* parse command line parameters */
    for (i=1 ; i<argc ; i++)
      {
	/* Run as a daemon */    
	if (argv[i][0] == '-' && argv[i][1] == 'D')
	  daemon = TRUE;
	
	else if (argv[i][0] == '-')
	  {
	    if (i+1 >= argc || argv[i+1][0] == '-')
	      goto usage;
	    if (strncmp(argv[i],"-e",2) == 0)
	      strcpy(expt_name, argv[++i]);
	    else if (strncmp(argv[i],"-h",2)==0)
	      strcpy(host_name, argv[++i]);
	    else if (strncmp(argv[i],"-d",2)==0)
	      {
		strcpy(dbg, argv[++i]);  // dbg = debug level 
		debug=TRUE;
	      }
	    else if (strncmp(argv[i],"-b",2)==0)  /* in mdarc.h (these are debug parameters) */
	      dump_begin =  atoi(argv[++i]);
	    else if (strncmp(argv[i],"-l",2)==0)
	      dump_length =  atoi(argv[++i]);
	  }
	else
	  {
	  usage:
	    printf("usage: mdarc_musr  [-h Hostname] [-e Experiment]\n");
	    printf("              [-D ] (become a daemon)\n");
	    /* don't tell users about 7; this debug level is for testing & will be removed later  
	    printf("              [-d debug level] (0=debug 1=mud/camp 2=save files 3=dump 4=process event 5=client flags 6=all 7= >1 mdarc \n");
	    */
	    printf("              [-d debug level] (0=debug 1=mud/camp 2=save files 3=dump 4=process event 5=client flags 6=all\n");
	    printf("              [-b begin] [-l length] (histo dump params for debug level=3. Default b=0 l=5. \n");
	    printf(" e.g.  mdarc_musr  -e musr  \n");
	    return 0;
	  }
      }

    if (daemon)
      {
	printf("Becoming a daemon...\n");
	ss_daemon_init(FALSE);
      }
    
    // using "musr"  for combined expt name
    for (j=0; j<strlen(expt_name); j++) expt_name[j]=tolower (expt_name[j]); 
    /* make sure we have the right version of mdarc_musr for this experiment */
    
    // MUSR experiments are now named m15, m20d etc or dev rather than musr
    if (  (strncmp(expt_name,"m15",3)==0) ||  (strncmp(expt_name,"dev",3)==0) ||  (strncmp(expt_name,"m9b",3)==0) ||
	  (strncmp(expt_name,"m9a",3)==0) || (strncmp(expt_name,"m20c",4)==0) || (strncmp(expt_name,"m20d",4)==0))

	printf("This version of mdarc_musr for VMIC MUSR (both TD and Integral) DOES support experiment %s\n",expt_name);
    else
      {
	printf("This version of mdarc_musr for VMIC MUSR (both TD and Integral) does not support experiment %s\n",expt_name);
	goto error;
      }
    
    
    if (debug)   
      {
	bug = atoi(dbg);
	switch (bug)
	  {
	  case 0:      /* level 0  Just debug */
	    break;   
	  case 1:      /* level 1  Just debug_mud  */
	    debug_mud = TRUE;
	    debug = FALSE;
	    break;   
	  case 2:      /* level 2 Just debug_check */
	    debug_check = TRUE;
	    debug = FALSE;
	    break;
	  case 3:
	    debug_dump = TRUE;
	    break;
	  case 4:
	    debug_proc = TRUE;
	    break;
	  case 5:
	    debug_cf = TRUE;
	    debug=FALSE;
	    break;
	  case 6:
	    debug_dump=debug_mud=debug_check=debug_proc=debug_cf=TRUE;
	    break;       
	  case 7:
	    debug=FALSE;
	    debug_mult=TRUE;
	  default: 
	    break;
	  }
      }
    if(debug_dump) printf("main: data dump will begin at bin = %d, length = %d\n",dump_begin,dump_length);
    
    /* connect to experiment */
    printf("calling cm_connect_experiment with host_name \"%s\", expt_name \"%s\" and client name = \"%s\"\n",
	   host_name,expt_name, ClientName);
    status = cm_connect_experiment(host_name, expt_name, ClientName, 0);
    if (status != CM_SUCCESS)
      goto error;
    
    /* turn off watchdog if in debug mode */
    if (debug)
      cm_set_watchdog_params(TRUE, 0);
     
    /* turn on message display, turn on message logging */
    cm_set_msg_print(MT_ALL, MT_ALL, msg_print);
    
    /* open the "system" buffer  */
     bm_open_buffer("SYSTEM", 2*MAX_EVENT_SIZE,  &hBufEvent);
    
    /* set the buffer cache size */
    bm_set_cache_size(hBufEvent, 100000, 0);
    
    /* place a request for a specific event id */
    /* IMUSR needs to process at BOR/EOR :
       HEADER    (CAMP)  ID 14  
       and while running : 
       SEND_DATA (IDAT)   ID 7  data
       CAMP      (CVAR)   ID 13 (CAMP variables)
       
       TD-MUSR needs to process: 
       HISTO              ID 2
      
       Scalers are accessed from the odb.

    Either IMUSR events will be sent or TD-MUSR  

    I-MUSR events:  */
    bm_request_event(hBufEvent, 7, TRIGGER_ALL, GET_ALL, &request_id,
		     process_event_I);
    bm_request_event(hBufEvent, 13, TRIGGER_ALL, GET_ALL, &request_id,
		     process_event_cvar);  
    bm_request_event(hBufEvent, 14, TRIGGER_ALL, GET_ALL, &request_id,
		     process_event_camp);

    /* TD- MUSR events:  
       TD-MUSR uses fragmented events so needs GET_ALL  */
    bm_request_event(hBufEvent, 2, TRIGGER_ALL, GET_ALL, &request_id, process_event_TD);
  
    /* Register to the Transition */
    /* frontend start is 500,  stop is 500 so prestart before frontend, start (poststart) after frontend, post stop after frontend) */
    if (
	cm_register_transition(TR_START, tr_start, 500) != CM_SUCCESS ||
	cm_register_transition(TR_STOP, tr_stop, 600) != CM_SUCCESS ||
	cm_register_transition(TR_START, tr_prestart,400) != CM_SUCCESS ||
	cm_register_transition(TR_PAUSE, tr_pause, 600) != CM_SUCCESS   ||
	cm_register_transition(TR_RESUME, tr_resume, 600) != CM_SUCCESS  )
      {
	
	printf("Failed to register\n");
	goto error;
      }
    
    /* connect to ODB */
    status = cm_get_experiment_database(&hDB, &hKey);
    if(status != CM_SUCCESS)
      {
	cm_msg(MERROR,"Mdarc","failure getting experiment database (%d)\n",status);
	return 0;
      }      

    // this should disappear with later midas; problems with odb lock for this midas
    
    printf("calling get_run_state \n");
    /* get the run state to see if run is going */
    status = get_run_state(&run_state);
    if(status != DB_SUCCESS) 
      {
	cm_msg(MERROR,"Mdarc","cannot determine run state (%d)\n",status);
	cm_disconnect_experiment(); /* abort the program. Cannot save the data */
	return 0;
      }

  /* Determine type:  Imusr or TD-Musr; sets flags I_MUSR or TD_MUSR */
    status = get_musr_type(&fe_flag);
    if(status != DB_SUCCESS)
      {
	cm_msg(MERROR, "main", "cannot determine MUSR run type (I or TD)");
	return DB_NO_ACCESS;
      }
    if(!fe_flag)
      {
	cm_msg(MINFO, "main", "WARNING: frontend code for other MUSR run type (I or TD) is booted");
	if (run_state != STATE_STOPPED)
	  return 0; /* if run is in progress abort because wrong FE is booted */
      }
#ifdef GONE    
    if(I_MUSR)
      {
	if (run_state != STATE_STOPPED)
	  {
	    cm_msg(MERROR,"mdarc","cannot save imusr data when a run is already in progress\n");
	    cm_disconnect_experiment(); /* abort the program. Cannot save the data */
	    return 0;
	  }
      }
#endif
    
    /* Create and setup <eqp_name>/settings */
    if(debug)printf ("calling setup_hotlink\n");  
    status = setup_hotlink(); /* initialization including set up keys hMDarc,hSave and open hot links */
    if(status!=DB_SUCCESS)
      {
	cm_disconnect_experiment(); /* abort the program. Cannot save the data */
	return 0;
      }
    
    /* Init sequence through tr_prestart once */
    {
      char str[256];
      if(tr_prestart(0, str) !=DB_SUCCESS)
	{
	  printf("Failure from tr_prestart\n");
	  cm_disconnect_experiment(); /* abort the program. Cannot save the data */
	  return 0;
	}
      
      if(tr_start(0, str) !=DB_SUCCESS)
	{
	  printf("Failure from tr_start\n");
	  cm_disconnect_experiment(); /* abort the program. Cannot save the data */
	  return 0;
	}
    }
    
    if(debug)
      {
	printf("\n");
	printf("mdarc parameters : \n");
	
	printf("Camp hostname: %s\n",fmdarc.camp.camp_hostname);
	
	printf("Time of last save: %s\n",fmdarc.time_of_last_save);
	printf("Last saved filename: %s\n",fmdarc.last_saved_filename);
	printf("Saved data directory: %s\n",fmdarc.saved_data_directory);
	printf("# versions before purge: %d\n",fmdarc.num_versions_before_purge);
	printf("End of run purge/rename flag: %d\n",fmdarc.endrun_purge_and_archive);
	printf("End of run save last event flag: %d\n",fmdarc.suppress_save_temporarily);

	printf("Archived data directory: %s\n",fmdarc.archived_data_directory);
	printf("Save interval (sec): %d\n",fmdarc.save_interval_sec_);
      }
    
    
    /* initialize ss_getchar() */
    ss_getchar(0);
    
    last_time = 0;
    
    do
      {
	/* call yield once every second */
	msg = cm_yield(1000);
	
	if(start_time > 0) // run has recently started 
	  {
	    if(ss_time() - start_time > 20) // allow 20s for everything to start
	      {
		if(debug_cf)
		  printf("main: ***  about to call check_client_flag, start_time=%d, ss_time=%d, tr_flag=%d\n",
			 start_time,ss_time(),tr_flag);
		/* if !fcf.enable_client_check, then check_client_flag will issue a warning message
		   but will not stop the run */
		status = check_client_flags(&tr_flag); // may stop the run
		if(status == DB_SUCCESS)
		  {
		    if(debug_cf) printf("main: check_client_flag returns status=%d and tr_flag=%d\n",status,tr_flag);
		    if(tr_flag) // run is in transition; leave the flag set for next time
		      printf("main:run is still in transition... waiting to recheck client flags\n");
		    else
		      {
			if(debug_cf)printf("main:resetting start_time to 0 to prevent check_client_flags being called again\n");
			start_time = 0; // reset so we don't come here again
		      }
		  }	    
	      }
	  } // start_time > 0

#ifdef GONE
	/* MUSR no longer check run number after stop... MUI checks at BOR */	
	if(stop_time > 0) // run has recently stopped
	  if(ss_time() - stop_time > 3) // allow 3s for everything to have stopped
	    {
	      /* run the run number checker ready for next time */
	      if (!fmdarc.disable_run_number_check)
		{
		  printf("\n\n mdarc: calling auto_run_number_check 3s after a stop\n");
		  gbl_rn_check = FALSE;
		  status = auto_run_number_check(0);
		  if (status != DB_SUCCESS)
		    cm_msg(MERROR,"mdarc","Warning: post-stop run number check failed"); // auto_run_number_check failed
		  else
		    printf("mdarc main: success from post-stop run number check\n"); // auto_run_number_check worked
		  stop_time = 0; // reset so we don't come here again
		}
	    }
#endif      
	/* time to save data */
	if (ss_time() - last_time > time_save )
	  {
	    last_time = ss_time();
	    /* get the run state to see if run is going */
	    status = get_run_state(&run_state);
	    if (run_state == STATE_RUNNING)
	      status = save_data(&msg);
       	  } // end of if(ss_time() ...  )
	
	
	    /* check keyboard */
	ch = 0;
	if (ss_kbhit())
	  {
	    ch = ss_getchar(0);
	    if (ch == -1)
	      ch = getchar();
	    if ((char) ch == '!')
	      break;
	  }
      }
    while (msg != RPC_SHUTDOWN && msg != SS_ABORT);
    
 error:
    /* reset terminal */
    //  ss_getchar(TRUE);
    
    DARC_release_camp();
    printf("calling db_close_all_records\n");
    status = db_close_all_records(); // TEMP should be called by cm_disconnect_experiment
    if(status != DB_SUCCESS)printf("failure from db_close_all_records (%d)\n",status);
    cm_disconnect_experiment();
}


