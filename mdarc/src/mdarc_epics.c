/* 

EPICS routines for use by mheader and mdarc
BNMR/BNQR only


$Log: mdarc_epics.c,v $
Revision 1.6  2008/12/10 18:41:45  suz
reduce verbiage by making more printfs debug_epics

Revision 1.5  2008/04/16 18:52:49  suz
separation of CAMP and EPICS logged variables

Revision 1.4  2007/09/21 21:29:20  suz
get rid of warning with seed

Revision 1.3  2007/09/20 02:29:44  suz
close epics if unexpectedly open

Revision 1.2  2007/07/27 21:45:16  suz
correct bug

Revision 1.1  2007/07/27 21:23:18  suz
initial version to add epics to mdarc,mheader for Bnmr
*/


#ifdef EPICS_ACCESS
#include <stdio.h>
#include <ctype.h>
#include <math.h>
/* camp includes come before midas.h */

#include "camp_clnt.h" // _min,_max
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif

#include "midas.h"
#include "experim.h"
#include "epics_log.h"
//#include "mdarc.h"
#include "mdarc_epics.h"
#include "epics_scan_devices.h"
#ifdef RAND
#include "trandom.h"
#endif
extern BOOL epics_available;
extern EPICSLOG_SETTINGS epicslog_settings;
extern HNDLE hDB;
extern INT n_epics;
extern INT debug_epics;
extern EPICS_LOG epics_log[MAX_EPICS];


/*------------------------------------------------------------*/
INT setup_epics_record()
//-------------------------------------------------------------
{
  /*  creates or gets epicslog equipment record (used for Epics for Type 2 runs)
      returns Handle hEpics
  */

  char str[80];
  EPICSLOG_SETTINGS_STR(epicslog_settings_str); //  I-MUSR epicslog equipment includes epics settings
  INT status,size;
  HNDLE hEpics;

  
  if(debug_epics)printf("setup_epics_record: starting\n");
  /* 
     create epicslog record if necessary (needed for EPICS)
  */
  sprintf(str,"/Equipment/Epicslog/Settings");
  //printf("str: %s\n",str);

  status = db_find_key(hDB, 0, str, &hEpics);
  if (status != DB_SUCCESS)
    {
      if(debug_epics) printf("setup_epics_record: Failed to find the key %s (%d) ",str,status);
      
      /* Create record for epicslog/settings area */     
      if(debug_epics) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB,0,str,  strcomb(epicslog_settings_str));
      if(status!= DB_SUCCESS)
	{
	  cm_msg(MERROR,"setup_epics_record","Create Record fails for %s (%d) ",status,str);
	  return(0); // returns hKey=0 meaning error
	}
      else
	if(debug_epics) printf("Success from create record for %s\n",str);
    }    
  else  /* key hEpics has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hEpics, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "setup_epics_record", "error during get_record_size (%d) for epicslog record",status);
	  return 0;  // returns hKey=0 meaning error
	}
        if(debug_epics)printf("Size of epicslog saved structure: %d, size of epicslog record: %d\n", sizeof(EPICSLOG_SETTINGS) ,size);
      if (sizeof(EPICSLOG_SETTINGS) != size) 
	{
	  cm_msg(MINFO,"setup_epics_record","creating record (epicslog); mismatch between size of structure (%d) & record size (%d)", sizeof(EPICSLOG_SETTINGS) ,size);
	  /* create record */
	  status = db_create_record(hDB,0,str,  strcomb(epicslog_settings_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"setup_epics_record","Could not create %s record (%d)\n",str,status);
	      return 0;  // returns hKey=0 meaning error
	    }
	  else
	    if (debug_epics)printf("Success from create record for %s\n",str);
	}
    }
  
  /* try again to get the key hEpics  */

  status = db_find_key(hDB,0, str, &hEpics);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "setup_epics_record", "cannot find Epicslog key for %s (%d)",str,status);
      return 0;  // returns hKey=0 meaning error
    }

  if(debug_epics)printf("Got the key hEpics\n");

  /* get the record */
  size = sizeof (epicslog_settings);
  if(debug_epics) printf("hEpics = %d, size of record = %d \n",hEpics,size);

  status = db_get_record(hDB,hEpics, &epicslog_settings, &size, 0);
  if(status!= DB_SUCCESS)
  {
    cm_msg(MERROR, "setup_epics_record", "cannot retrieve %s record",str);
    return 0;  // returns hKey=0 meaning error
  }
  
  if(debug_epics)printf("setup_epics_record: got the record, number of epics values to be logged is %d\n",
	 epicslog_settings.output.num_log_ioc); // filled by check_epics.csh 
  return hEpics; // returns the key
}



//--------------------------------------------------------
INT init_epics(BOOL stats)
//---------------------------------------------------------
{
  INT status;
  if(debug_epics) printf("\n init_epics: starting with epics_available = %d\n",epics_available);
  if(epics_available)
    {
      printf("init_epics: epics_available is unexpectedly TRUE");
      epics_close();
    }

  if(debug_epics)printf("\n init_epics: Calling set_epics_constants\n");
  set_epics_constants(); // fill epics_params array

  /* now check the EPICS access for logged variables */
  if ( epicslog_settings.output.num_log_ioc > 0) //filled by check_epics.csh
    { 
      status = open_epics_log(&n_epics,stats);
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"init_epics","EPICS cannot be logged"); 
	  return status;
	}
    }
  else
    cm_msg(MINFO,"init_epics","No epics variables are selected to be logged");


  if(debug_epics)printf("init_epics: returning with epics_available = %d\n",epics_available);
  return SUCCESS;
}



/*---------------------------------------------------------------------------*/

INT open_epics_log(INT *pnum, BOOL stats)

/*---------------------------------------------------------------------------*/
{
  INT status,i,j;
  //  char * ca_name (int chan);

  i=0; // number of epics channels connected
  epics_available = FALSE;
  status = caInit();
  
  if(status == -1)
    {
      epics_available = FALSE;
      cm_msg(MERROR,"open_epics_log","error initializing Epics");
      return DB_NO_ACCESS;
    }
  
  for (j=0; j< epicslog_settings.output.num_log_ioc; j++)
    {
      clear_epics_log(i); // clear all the values in the epics_log structure

      status = open_epics_chan(epicslog_settings.output.epics_log_ioc[j], i);
      if (status == SUCCESS)
	{
	  epics_log[i].open=TRUE; // channel is open
	  status = read_epics_value(i,stats);
	  if(status == SUCCESS)
	    i++;   
	}

    } // end of for loop

  *pnum=i; // returns number of open channels

  if(i==j)
    {
      cm_msg(MINFO,"open_epics_log","all %d EPICS channels are connected for logging",j);
      epics_available = TRUE;
    }
  else if (i==0)
    {
      cm_msg(MINFO,"open_epics_log","no EPICS channels are connected for logging");
      return DB_NO_ACCESS;
    }
  else
    {
      epics_available = TRUE;  
      cm_msg(MINFO,"open_epics_log","only %d out of %d EPICS channels are connected for logging",i,j);
    }
  return SUCCESS;
}

/*---------------------------------------------------------------------------*/

INT open_epics_chan(char *name,  INT i)

/*-----------------------------------------------------------------------*/
{
  // open the channel
  INT status;

  if(debug_epics)printf("open_epics_chan: trying to open EPICS channel %s\n",name);
  status = caGetSingleId(name, &epics_log[i].Rchid);
  if(status == -1)
    {
      cm_msg(MINFO,"open_epics_chan",
	     "error status attempting to get channel ID for %s\n",name);
      return DB_NO_ACCESS;
    }
  else
    if(debug_epics)
      printf("open_epics_chan: caGetSingleId returns Rchid = %d for %s\n",
	     epics_log[i].Rchid,name);
      
  if (caCheck( epics_log[i].Rchid))
    {
      strncpy(epics_log[i].Name,name,strlen(name));
      if(debug_epics)printf("open_epics_chan: caCheck says channel %s is connected\n",epics_log[i].Name);
      epics_log[i].open = TRUE;
      epics_log[i].index = get_epics_constants(name);
    }
  else
    {
      if(debug_epics)printf("open_epics_chan: caCheck says channel %s is NOT connected\n",name);
      return DB_NO_ACCESS;
    }
  return SUCCESS;
}

/*---------------------------------------------------------------------------*/

INT read_epics_value(INT i, BOOL stats)

/*---------------------------------------------------------------------------*/
{
  INT status,num;
  double val,var,mean;
  double delta;

  if(!epics_log[i].open)
    {   // try to open this channel
      status = open_epics_chan(epics_log[i].Name, i);
      if (status != SUCCESS) return status; // cannot open channel
    }
  else
  {  // open flag is true... check it  hasn't timed-out and disconnected
    if (caCheck( epics_log[i].Rchid))
      {
	if(debug_epics)printf("read_epics_value: caCheck says channel %s is connected\n",epics_log[i].Name);
      }
    else
      {
	if(debug_epics)printf("read_epics_value: caCheck says channel %s is NOT connected\n",epics_log[i].Name);
	epics_log[i].open=FALSE;
	{   // try to reopen channel
	  status = open_epics_chan(epics_log[i].Name,i);
	  if (status != SUCCESS) return status; // cannot open channel
	}
      }
  }

  // Epics Channel is open
  if(debug_epics)
    printf("\nread_epics_value: Now calling caRead for %s  with chan_id=%d\n",epics_log[i].Name, epics_log[i].Rchid);
  status=caRead( epics_log[i].Rchid,&epics_log[i].value);
  if(status==-1)
    {
      cm_msg(MINFO,"read_epics_value","Bad status after caRead for %s", ca_name( epics_log[i].Rchid));
      return DB_NO_ACCESS;
    }
  
  if(debug_epics)
    printf("read_epics_value: Read value from %s as %8.3f\n", ca_name( epics_log[i].Rchid),epics_log[i].value);
  epics_log[i].count++;
  
  
#ifdef RAND
      // Testing with helicity switch; Generate some uncertainty for statistics calcs. 
      INT iran;
      INT nval=10;
      float ran;
      iran=get_random_index(nval); /* get a random number, iran is an integer between 0 & nval-1 */
      if(debug_epics)
        printf("nval = %d  random no. iran (between 0 and %d) = %d\n",nval,(nval-1),iran);
      ran = 0.1 * ((float)iran);
      if(ran >= 0.5)
	  ran=(ran-0.5)*-1;
      if(debug_epics)
	printf("Adjusting read value %f to %f\n", epics_log[i].value,  epics_log[i].value+ran);
      epics_log[i].value+= ran;  // give the value a random deviation
#endif

  if(stats)
    {  // Type 2 run only; midbnmr deals with statistics for Type 1 runs
      val =  (double)epics_log[i].value;
      num = epics_log[i].count; // number of events read

      if (debug_epics)
	printf("\n ===  read_epics_value: Calculating stats... working on count %d  i=%d val=%f ==== \n",num,i,val);

      if ( num ==0 )
	{  // should be at least one
	  cm_msg(MERROR,"read_epics_value"," Cannot fill statistics for  epics_log[%d]; count=0",i);
	  return DB_INVALID_PARAM;
	}
      else if (num == 1) // we just started a run
	epics_log[i].offset= epics_log[i].maximum=epics_log[i].minimum=val;
      
      epics_log[i].minimum = _min(epics_log[i].minimum , val );
      epics_log[i].maximum = _max(  epics_log[i].maximum, val );
      delta = val - epics_log[i].offset;
      epics_log[i].sum += delta;
      epics_log[i].sumSquares += pow( delta, (double)2.0 );
      epics_log[i].sumCubes += pow( delta, (double)3.0 );
      
      
      
      mean = (  epics_log[i].sum == 0.0 ) ? 0.0 :  epics_log[i].sum/num;
      
      
      /*
       *  Calculate standard deviation
       */
      if( num == 1 ) 
	{
	  epics_log[i].stddev = 0.0;
	}
      else
	{
	  var = (  epics_log[i].sumSquares - ( ( epics_log[i].sum==0.0)?0.0:(pow( epics_log[i].sum,2)/num) ) )/
	    ( num-1 );
	  epics_log[i].stddev = sqrt( fabs( var ) );
	}
      
      /*
       *  Calculate skew
       */
      if(  epics_log[i].stddev == 0.0 ) 
	{
	  epics_log[i].skew = 0.0;
	}
      else
	{
	  epics_log[i].skew = (  epics_log[i].sumCubes - 3* mean* epics_log[i].sumSquares + 
				 2*( ( epics_log[i].sum==0.0)?0.0:pow( epics_log[i].sum,3) )/( pow(num,2) ) )/
	    ( num*pow( epics_log[i].stddev,3) );
	}
      epics_log[i].mean = mean + epics_log[i].offset;

    }
  if(debug_epics)
    {
      if(num==1)
	print_epics_log(i);
    }

  if(debug_epics)printf("read_epics_value: returning SUCCESS\n");
  return SUCCESS;
}

/*---------------------------------------------------------------------------*/

void print_epics_log(INT i)

/*---------------------------------------------------------------------------*/
{
  printf("Rchid  = %d\n",epics_log[i].Rchid); /* ID for read; set to -1 if no access */
  printf("Name   = %s\n",epics_log[i].Name);  /* name of EPICS channel */
  printf("Index  = %d\n",epics_log[i].index);     /* index to epics_params structure */
  printf("Open   = %d\n",epics_log[i].open);      /* Channel is open for read */
  printf("Value  = %f\n",epics_log[i].value);   /* latest value */
  printf("Mean   = %f\n",epics_log[i].mean);    /* mean value */
  printf("Stddev = %f\n",epics_log[i].stddev); /* standard deviation */
  printf("Max    = %f\n",epics_log[i].maximum); /* maximum value */
  printf("Min    = %f\n",epics_log[i].minimum); /* minimum value */
  printf("Sum    = %f\n",epics_log[i].sum);
  printf("SumSquares   = %f\n",epics_log[i].sumSquares);
  printf("SumCubes   = %f\n",epics_log[i].sumCubes);
  printf("Offset   = %f\n",epics_log[i].offset);
  printf("Skew   = %f\n",epics_log[i].skew);
}

/*---------------------------------------------------------------------------*/

void clear_epics_log(INT i)

/*---------------------------------------------------------------------------*/
{ 
  if(debug_epics)printf("clear_epics_log: clearing epics_log of Epics information\n");

  epics_log[i].Rchid=-1;  
  epics_log[i].Name[0]='\0';
  epics_log[i].open=FALSE;  
  epics_log[i].count =  epics_log[i].index=0;
  epics_log[i].index =  NUM_EPICS_DEV-1;
  epics_log[i].value = epics_log[i].mean = epics_log[i].minimum =  epics_log[i].offset = 0.0; 
  epics_log[i].sum   = epics_log[i].sumSquares    = epics_log[i].stddev  = 0.0;
  epics_log[i].sumCubes  = epics_log[i].maximum = epics_log[i].skew= 0.0;
  if(debug_epics) print_epics_log(i);
  return;
}



/*---------------------------------------------------------------------------*/

void set_epics_constants(void)

/*---------------------------------------------------------------------------*/
{ 
  INT i;

  if(debug_epics)printf("Set_epics_constants:\n");

  for(i=0; i< NUM_EPICS_DEV; i++)
  {
    sprintf (epics_params[i].Rname,epics_devices_paths[i]);
    sprintf (epics_params[i].Title,epics_devices_titles[i]);
    sprintf (epics_params[i].Units,epics_devices_units[i]);
    if(debug_epics)
      printf("i=%d Rname=%s Title=%s Units=%s\n",i,
	   epics_params[i].Rname,
	   epics_params[i].Title, 
	   epics_params[i].Units );
  }

 return;
}
/*---------------------------------------------------------------------------*/

INT get_epics_constants(char *Rname)

/*---------------------------------------------------------------------------*/
{
  INT index,len;
  len = strlen (Rname);

  if(debug_epics)printf("get_epics_constants: looking for %s\n",Rname);
  for (index=0; index< NUM_EPICS_DEV; index++)
    if(strncmp (Rname,  epics_params[index].Rname, len)==0) break;

  if(index >=  NUM_EPICS_DEV)
    index = NUM_EPICS_DEV-1;

  if(debug_epics)printf("get_epics_constants: returning index=%d\n",index);
  return index;

}


/*------------------- epics_close ------------------*/
void epics_close(void)
/* ----------------------------------------------*/
{
  if(epics_available)
    {
      epics_available=FALSE;
      caExit();
    }
  return;
}



#ifdef RAND
INT get_random_index( INT nval)
{
  /* returns a random integer between 0 and nval-1
     this will be used as the index of the index array
     index array contains the indexes of the frequency array
 */
  INT i;
  float fran,ftmp;
  long int lseed;
  lseed = (long)seed; // get rid of warning
  fran = ran0(&lseed);
  seed = (DWORD)lseed;
  ftmp = fran * (float)nval;
  i = (INT)ftmp; /* truncate fractional part .. integers go from 0 to nint-1 */
  if(dr)
    printf("get_random_index:  random no. = %f ftmp=%f -> %d\n",fran,ftmp,i);
  return i;
}

float ran0(long *idum)

/*
   Minimal  random number generator of Park and Miller. Returns a uniform
   random deviate between 0.0 and 1.0. Set or reset idum to any integer value
   (except the unlikely value MASK) to initialize the sequence; idum must not
   be altered between calls for successive deviates in a sequence. */
{
  long k;
  float ans;

  *idum ^= MASK;
  k=(*idum)/IQ;
  *idum=IA*(*idum-k*IQ)-IR*k;
  if (*idum < 0) *idum += IM; ans=AM*(*idum);
  *idum ^= MASK;
  return ans;
}


#endif // RAND

#endif // epics
