/* mdarc_epics.h

 function prototypes


$Log: mdarc_epics.h,v $
Revision 1.1  2007/07/27 21:46:39  suz
initial version


*/

#ifdef EPICS
INT init_epics(void);
INT open_epics_log(INT *pnum_epics); // prototype
INT open_epics_chan(char *name,  INT i);
INT read_epics_value(INT i);
void clear_epics_log(INT i);
void zero_epics_log(INT i);
void set_epics_constants(void);
INT get_epics_constants(char *Rname);
void epics_close(void);
#endif
