/* This file is included by bnmr_darc.c and by musr_darc.c
   i.e. for TD-type BNMR only and both types of MUSR

   uses ifdef MUSR 
*/

/*
 $Log: mdarc_subs.c,v $
 Revision 1.20  2015/03/24 00:42:21  suz
 this version for VMIC MUSR. Similar to BNMR's VMIC_MUSR version except for client flag code

 Revision 1.19  2015/03/23 19:00:48  suz
 last version running on vxworks musr (from midm20).

 Revision 1.18  2012/10/11 17:15:11  suz
 add a message

 Revision 1.17  2007/07/27 21:18:55  suz
 For c++ link with this file, no longer include the code. Not tested for MUSR

 Revision 1.16  2007/05/24 17:33:05  suz
 corrected directory for perlscript output text files

Revision 1.15  2005/06/10 16:39:54  suz
 fix a comment

 Revision 1.14  2004/12/11 07:38:53  asnd
 Fill in "rig" and "mode" for ImuSR mud files

 Revision 1.13  2004/09/11 09:53:00  asnd
 Save scaler rates in TD-muSR mud file (they were left out).

 Revision 1.12  2004/09/04 00:11:23  asnd
 Suzannah's uncommitted clean-up of messages

 Revision 1.11  2004/06/10 19:10:35  suz
 try to continue if musr_update fails

 Revision 1.10  2004/04/20 17:15:43  suz
 fix bug for bnmr

 Revision 1.9  2004/04/15 20:18:19  suz
 remove imusr run title; not needed after all

 Revision 1.8  2004/04/08 17:54:50  suz
 add imusr run_title & copy_item. Add more stringlength checks

 Revision 1.7  2004/03/30 22:46:42  suz
 do not delete perl output file (now appending) & update path for this file

 Revision 1.6  2004/02/11 02:33:19  suz
 add imusr support

 Revision 1.5  2003/12/01 21:33:50  suz
 fix messages & a bug

 Revision 1.4  2003/12/01 20:22:27  suz
 Change area name

 Revision 1.3  2003/07/31 21:03:38  suz
 change client flag to int as alarm didn't like bool

 Revision 1.2  2003/07/29 18:13:25  suz
 added set_client_flag

 Revision 1.1  2003/06/25 00:02:06  suz
 common subroutines included by bnmr_darc.c and mheader.c


*/

#include <stdio.h>
#include <ctype.h>
#include <math.h> // fabs


/* camp includes come before midas.h */
#include "camp_clnt.h"
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif


#include "midas.h"
#include "experim.h" /* odb structure */

//* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "mud_util.h"
#ifdef MUSR
#include "trii_fmt.h"  // uncomment for IMUSR_CAMP_VAR (musr)
#endif
#include "darc_odb.h"
#include "mdarc.h"

#ifndef MUSR 
extern HNDLE hFS;
extern FIFO_ACQ_MDARC fmdarc;
extern FIFO_ACQ_FRONTEND fifo_set;
#else
extern MUSR_TD_ACQ_MDARC fmdarc;
extern MUSR_I_ACQ_SETTINGS imusr; // added this
extern SCALER_SETTINGS fscaler;   // and this after removing include mdarc_subs from bnmr_darc
#endif

extern INT nH;
extern char eqp_name[];


/* pre-processor macro for getting run-header string */


#define get_run_header_item(item) \
  size = sizeof(p_odb_data->item); \
  status = db_get_value(hDB, hTmp, #item, &p_odb_data->item, &size, TID_STRING, FALSE); \
  if (status == DB_TRUNCATED) \
  { \
    cm_msg(MINFO,"darc_get_odb","truncated string %s/" #item, str); \
    write_message1(status,"darc_get_odb"); \
  } \
  else if (status != DB_SUCCESS) \
  { \
    cm_msg(MERROR,"darc_get_odb","cannot retrieve %s/" #item, str); \
    write_message1(status,"darc_get_odb"); \
    return status; \
  }



/*------------- darc_get_odb ----------------*/
DWORD darc_get_odb(D_ODB *p_odb_data)
/*---------------------------------------------*/
{

  INT   status, size,size1, i, j, itemp;
  char str[128];
  HNDLE hSet, hScaler, hTmp;
  KEY   hKScaler;
  double temp_scalers[MAX_NSCALS];
  static double save_rates[MAX_NSCALS] = { 0.0 };
  char  *s;
  char  mdarc[32];
  INT rn;  /* temp for run number check */
  BOOL enabled;
  char tempString[256];
  BOOL any_rate;

#ifdef MUSR
  char  cmd[132];
#endif
  debug=0; 
  if(debug)printf("darc_get_odb starting ... \n");
#ifdef MUSR
  printf(" darc_get_odb: MUSR is defined ");
  if(TD_MUSR)
    printf("and this is a TD-MUSR run\n");
  else if (I_MUSR)
    {
      printf("and this is an I-MUSR run\n");
    }
  else
    {
      cm_msg (MERROR,"darc_get_odb","...neither I-MUSR nor TD-MUSR is defined\n");
      return(DB_INVALID_PARAM);
    }
#else
  if(debug)
    printf(" darc_get_odb: BNMR or BNQR is defined (i.e. NOT MUSR)\n");
  if (I_MUSR || TD_MUSR )
    {
      cm_msg(MERROR,"darc_get_odb","ERROR.... I_MUSR (%d) or TD_MUSR (%d) is true for BNMR/BNQR\n",
				I_MUSR,TD_MUSR);
      return (DB_INVALID_PARAM);
    }
#endif


  /* get the run state */
  size = sizeof(p_odb_data->run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &p_odb_data->run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/State");
    write_message1(status,"darc_get_odb");
    return (status);
  }

  /* get the time the run started */
  size = sizeof(p_odb_data->start_time_binary);
  status = db_get_value(hDB, 0, "/Runinfo/Start time binary", &p_odb_data->start_time_binary, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/Start time binary");
    write_message1(status,"darc_get_odb");
    return (status);
  }
  
  size=sizeof(tempString);
  status = db_get_value(hDB,0, "/Runinfo/Start time",  &tempString, &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/Start time");
    write_message1(status,"darc_get_odb");
    return (status);
  }
  size = sizeof(p_odb_data->start_time_str);
  copy_item(p_odb_data->start_time_str, size, tempString, "Start time"); // truncates if necessary

  /* and the time the run last stopped */
  size = sizeof(p_odb_data->stop_time_binary);
  status = db_get_value(hDB, 0, "/Runinfo/Stop time binary", &p_odb_data->stop_time_binary, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found  /Runinfo/Stop time binary");
    write_message1(status,"darc_get_odb");
    return (status);
  }
  
  size = sizeof(p_odb_data->stop_time_str);
  status = db_get_value(hDB,0, "/Runinfo/Stop time",  &p_odb_data->stop_time_str, &size, TID_STRING, FALSE);

  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key not found /Runinfo/Stop time ");
    write_message1(status,"darc_get_odb");
    return (status);
  }


// The run number is a global value, got by tr_start
  p_odb_data->run_number = run_number;

//  get the run number from /Runinfo in case it has somehow changed!
  size = sizeof(rn);
  status = db_get_value(hDB,0, "/Runinfo/Run number",  &rn, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    status=cm_msg(MERROR,"darc_get_odb","key not found  /Runinfo/Run number");
    write_message1(status,"darc_get_odb");
    return (status);
  }
  /* check the run number */
  if (run_number != rn)
  {
    status = cm_msg(MERROR,"darc_get_odb",
                    "Run number in odb has changed to %d (expected %d). Probably run start failed",
		    run_number,rn);
    status = cm_msg(MERROR,"darc_get_odb","Or if run not now stopped,  stop run and press TEST or REAL button before restarting");
    printf("darc_get_odb","Problem with the run number. Run Start may have failed");
    return  (DB_INVALID_PARAM);  
  }
  
  /* these global values have been set up and  checked in tr_start and process_event
     They cannot be changed during a run.
  */
  p_odb_data->his_n    = nH; /* no. of histograms  in this event (global)  */
  p_odb_data->his_nbin = nHistBins;  /* no. of histogram bins found in this event (global)   */ 
  if (debug)
  printf("\n ====*****   darc_get_odb: num histo bins : %d; num histograms: %d ===== ******\n\n",
	 p_odb_data->his_nbin,  p_odb_data->his_n   );
  
  
/*       G E T     M D A R C     R E C O R D   */

/* mdarc area must be present for all front end clients (mdarc.c has already checked this) */
  sprintf(str, "/Equipment/%s/mdarc",eqp_name);  
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","key %s not found (%d)", str,status);
    write_message1(status,"darc_get_odb");
    return status;
  }

  
  /* The histogram parameters in the mdarc area (i.e. first good bin etc.) are allowed 
     to be changed during the run  (relevent to TD-type runs only) */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record  */
  
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","Failed to get the whole record for %s (%d)",str,status );
    write_message1(status,"darc_get_odb");
    return(status);
  }
  
/*       M U S R  only   */

#ifdef MUSR
  if(I_MUSR)
    {
      /* I M U S R only */
      if(debug)printf(" * * I-MUSR only * * \n");
      // copy_item truncates string if necessary
      copy_item(p_odb_data->area,
		sizeof(p_odb_data->area),
		fmdarc.histograms.musr.beamline,
		"area");
    }
  else
    {  /*     T D -  M U S R    only   
          update bin parameters from TD-musr area
       (BNMR's parameters are already in the right place in odb) */

      printf(" * * TD- MUSR only * * \n");
      /* this copy replaced by hotlinks */

      /* bin parameters may change during the run */
      /*   if(debug)
	printf("darc_get_odb: Calling musr_update.pl to check if bin parameters need updating... \n");

      sprintf(cmd,"%s/musr_update.pl %s %s %s",perl_path, perl_path, expt_name,
	      fmdarc.histograms.musr.beamline );
      if(debug) printf("darc_get_odb: sending system command  cmd: %s\n",cmd);
      status =  system(cmd);
      if (status)
	/* cannot update parameters */
      /*	{
	  cm_msg (MINFO,"darc_get_odb",
		  "Bad status from perl script musr_update.pl (%d). Bin params may not be current",status);
	  cm_msg (MERROR,"darc_get_odb",
		  "Check perlscript output file \"/home/midas/musr/log/%s/musr_update.txt\" for details",expt_name);
      /* note: if no recent addition to this file, there may be compilation errors in the perl script */
	  //  don't make this a fatal error; maybe we can still save the data
	  //return (DB_INVALID_PARAM); //  bad status from perl script
	  
      /*	}
      else
	{
	  if(debug)printf("darc_get_odb: successfully updated mode params\n");
	  // get the record again now it has been updated
	  size = sizeof(fmdarc);
	  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record  */
      /*  
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"darc_get_odb","Failed to again get the whole record for %s (%d)",str,status );
	      write_message1(status,"darc_get_odb");
	      return(status);
	    }
	}
      end of musr_update.pl call  */
      p_odb_data->dwell_time = (float) fmdarc.histograms.resolution_code; 

      /* These are needed in the histogram header for the mud files 
	 get area,rig,mode */

      sprintf(p_odb_data->rig,"%s",fmdarc.histograms.musr.current_rig);
      sprintf(p_odb_data->mode,"%s",fmdarc.histograms.musr.current_mode); 

    } // end of TD-MUSR only


#else
  /*       B N M R   or   B N Q R   only   */
  
  /* These are needed in the histogram header for the mud files 
     Fill header values for BNMR/BNQR

     Area and rig
  */
  copy_item( p_odb_data->area,  sizeof(p_odb_data->area), expt_name, "experiment name");

  for (j=0; j<strlen(p_odb_data->area); j++)
    p_odb_data->area[j] = toupper (p_odb_data->area[j]); /* BNMR or BNQR */
  
  sprintf(p_odb_data->rig,"%s",p_odb_data->area); // BNMR/BNQR rig=area
  
  /* mode */
  copy_item( p_odb_data->mode,  sizeof(p_odb_data->mode), 
	     fifo_set.input.experiment_name, "ppg mode"); 
  
  
  /*    Values specific to SIS multichannel scaler (i.e. BNMR experiment)
	check client name for now */
  
  /* BNMR uses dwell time (from mdarc  area) */
  p_odb_data->dwell_time = fmdarc.histograms.dwell_time__ms_; 
  
  /* find  "frontend" area  */
  sprintf(str, "/Equipment/%s/frontend",eqp_name);  
  status = db_find_key(hDB, 0, str, &hFS);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","key %s not found (%d)", str,status);
      write_message1(status,"darc_get_odb");
      return status;
    }
  
  size = sizeof(fifo_set);
  status = db_get_record (hDB, hFS, &fifo_set, &size, 0);/* get the whole record */
  
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","Failed to get the whole record for %s (%d)",str,status );
      write_message1(status,"darc_get_odb");
      return(status);
    }

#ifdef HAVE_SIS3801
  if (debug)
    {
      if(! fifo_set.sis_test_mode.sis3801.test_mode)
        printf("darc_get_odb: sis3801 REAL mode is true\n");
      else
        printf("darc_get_odb: sis3801 TEST mode is true\n");
    }
#endif
#ifdef HAVE_SIS3820
  if(fifo_set.hardware.sis3820.sis_mode == 2)
  printf("darc_get_odb: sis3820 REAL mode is true\n");
      else
        printf("darc_get_odb: sis3820 TEST mode is true\n");
#endif
  
#endif // BNMR/BNQR only

  if (debug)
    {
      printf("Current settings:\n");
      printf("  Beamline: %s\n",p_odb_data->area);
      printf("  Rig: %s    Mode: %s\n",
	     p_odb_data->rig,p_odb_data->mode);    
      printf("  No. bins: %d   No. histograms: %d\n",
	     p_odb_data->his_nbin,p_odb_data->his_n);
      
#ifdef MUSR
      if(TD_MUSR)printf("  Resolution code =  %f\n", p_odb_data->dwell_time);
#else
      /* BNMR/BNQR */
      printf("  Dwell time (ms) %f\n", p_odb_data->dwell_time);
#endif
    } // end of if(debug)


#ifdef MUSR
  if(I_MUSR)
    {
      size =  sizeof (p_odb_data->IMUSR_his_titles[0]);// could use HIS_SIZE instead
      for(i=0; i<p_odb_data->his_n; i++)
	{
	  copy_item(p_odb_data->IMUSR_his_titles[i],size, imusr.imdarc.histograms.titles[i],
		    "IMUSR_his_titles");
	  if(debug) 
	    printf("  imusr.imdarc.histograms.titles[%d]= %s;   ,p_odb_data->IMUSR_his_titles[%d]=%s\n",
			   i, imusr.imdarc.histograms.titles[i],i,p_odb_data->IMUSR_his_titles[i] );
	}
    }
#endif

  if(!I_MUSR)
    {  // TD-MUSR and BNMR use these parameters
      size =  sizeof (p_odb_data->his_titles[0]);// could use HIS_SIZE instead 
     
    
      for(i=0; i<p_odb_data->his_n; i++)
	{
	  copy_item(p_odb_data->his_titles[i],size, fmdarc.histograms.titles[i],
		    "his_titles");
	  if(debug) printf("p_odb_data->his_titles[%d]=%s\n",
			   i,p_odb_data->his_titles[i] );
	}
      
      for (i=0; i<p_odb_data->his_n ; i++) 
	{
	  p_odb_data->his_bzero[i]      =  fmdarc.histograms.bin_zero[i];
	  p_odb_data->his_good_begin[i] =  fmdarc.histograms.first_good_bin[i];
	  p_odb_data->his_good_end[i]   =  fmdarc.histograms.last_good_bin[i];
	  p_odb_data->his_first_bkg[i]  =  fmdarc.histograms.first_background_bin[i];
	  p_odb_data->his_last_bkg[i]   =  fmdarc.histograms.last_background_bin[i];
	  
	  if (p_odb_data->his_good_end[i] > p_odb_data->his_nbin)      
	    {
	      cm_msg(MINFO,"darc_get_odb"," Warning - histogram %d has last good bin (%d)  > # bins (%d)",
		     i,p_odb_data->his_good_end[i],p_odb_data->his_nbin);
	      printf("darc_get_odb","last good bin will be set to # bins\n");
	      p_odb_data->his_good_end[i] = p_odb_data->his_nbin;
	    }
	}
    } // end of TD_MUSR and BNMR's histogram parameters
  
#ifdef MUSR
  
  //   MUSR Scalers (TD and I)    //
  
  /* extract the scaler names from /Equipment/scaler/Settings 
     Scaler names are set by the user for TD ; it's best
     to check the length 

 */
  /* find  "frontend" area  */
  sprintf(str, "/Equipment/scaler/settings");
  status = db_find_key(hDB, 0, str, &hTmp);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","key %s not found (%d)", str,status);
      write_message1(status,"darc_get_odb");
      return status;
    }
  
  size = sizeof(fscaler);
  status = db_get_record (hDB, hTmp, &fscaler, &size, 0);/* get the whole record */
  
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"darc_get_odb","Failed to get the whole record for %s (%d)",str,status );
      write_message1(status,"darc_get_odb");
      return(status);
    }
   
  if (fscaler.enabled)
    {
      printf("  Scaler is enabled\n");
      
      /* First, the scaler titles */
      size = sizeof(p_odb_data->scaler_titles[0]);

      if(debug)printf("fscaler.num_inputs = %d; string length=%d\n",fscaler.num_inputs,size);
      for(i=0; i<fscaler.num_inputs; i++)
	{
	  copy_item(p_odb_data->scaler_titles[i], size, fscaler.titles[i], "scaler titles");
	  if(debug) printf("  fscaler.titles[%d]= %s;   ,p_odb_data->scaler_titles[%d]=%s\n",
			   i, fscaler.titles[i],i,p_odb_data->scaler_titles[i] );
	}


      /* Second, the scaler totals, in the ...scaler/variables area */
      sprintf(str, "/Equipment/scaler");
      status = db_find_key(hDB, 0, str, &hSet);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb"," key %s not found", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}      
      size = sizeof(temp_scalers); /* these are stored as DOUBLE in odb */
      status = db_get_value(hDB, hSet, "/Variables/SCLR", temp_scalers, &size, TID_DOUBLE, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","cannot retrieve scaler data from %s/Variables/SCLR", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      
      status = db_find_key(hDB, hSet, "/Variables/SCLR", &hScaler);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","cannot find key %s/Variables/SCLR", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      status = db_get_key(hDB, hScaler, &hKScaler);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","cannot get key %s/Variables/SCLR", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      if(fscaler.num_inputs >  hKScaler.num_values)
	{
	  cm_msg(MERROR,"darc_get_odb",
		 "excessive no. of scalers (%d) > odb array length (%d)\n",
		 fscaler.num_inputs,  hKScaler.num_values);
	  return (DB_INVALID_PARAM);
	}
      p_odb_data->nscal = fscaler.num_inputs; /* no. of scalers defined by Front End  */
      if (debug)printf("darc_get_odb: no. scalers enabled nscal = %d\n",p_odb_data->nscal);
      for (i=0; i< p_odb_data->nscal ; i++)
	{
	  p_odb_data->scaler_save[i] = (int) temp_scalers[i];
	}

      /* Third, the scaler rates, in the ...rscal/variables area */
      sprintf(str, "/Equipment/Rscal");
      status = db_find_key(hDB, 0, str, &hSet);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb"," key %s not found", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      size = sizeof(temp_scalers); /* these are stored as DOUBLE in odb */
      status = db_get_value(hDB, hSet, "/Variables/RATE", temp_scalers, &size, TID_DOUBLE, FALSE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","cannot retrieve scaler rates from %s/Variables/RATE", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      /* Check if all the rates are zero.  If so, retain previous rates; once only */
      /* This preserves the recent rates in the final saved version at the end of a run */
      any_rate = FALSE;
      for (i=0; i< p_odb_data->nscal ; i++)
        {
          if( fabs(temp_scalers[i]) > 0.01 ) 
            {
              any_rate = TRUE;
              break;
            }
        }
      
      for (i=0; i< p_odb_data->nscal ; i++)
	{
	  if( any_rate) 
            p_odb_data->scaler_rate[i] = (int) temp_scalers[i];
          else
            p_odb_data->scaler_rate[i] = (int) save_rates[i];

	  if(debug) printf("scaler_titles[%d] = %s  data = %d  rate = %d\n",
                           i,p_odb_data->scaler_titles[i],p_odb_data->scaler_save[i],p_odb_data->scaler_rate[i] );
	}

      for (i=0; i< p_odb_data->nscal ; i++)
        {
          save_rates[i] = temp_scalers[i];
        }

    }
  else
    {
      printf("  Scaler is disabled\n");
      p_odb_data->nscal=0;
    }

#else  // BNMR only 
  //  BNMR Cycle Scalers //
  /* extract the scaler names from /Equipment/Cycle_scalers/Settings  */
  sprintf(str, "/Equipment/Cycle_scalers");
  status = db_find_key(hDB, 0, str, &hSet);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb"," key %s not found", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  /* scaler titles are fixed for BNMR/BNQR, so shouldn't increase in length.
     this code does not check the sizes...
  */
  size = sizeof(p_odb_data->scaler_titles);
  status = db_get_value(hDB, hSet, "/Settings/names", p_odb_data->scaler_titles, &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","Cannot retrieve scaler names from  %s/Settings/names", str);
    write_message1(status,"darc_get_odb");
    cm_msg(MERROR,"darc_get_odb",
           "# scalers defined by frontend may be > maximum currently allowed by mdarc");
    return status;
  }
  
  
  size = sizeof(temp_scalers); /* these are stored as DOUBLE in odb */
  status = db_get_value(hDB, hSet, "/Variables/HSCL", temp_scalers, &size, TID_DOUBLE, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot retrieve scaler data from %s/Variables/HSCL", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  
  status = db_find_key(hDB, hSet, "/Variables/HSCL", &hScaler);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot find key %s/Variables/HSCL", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  status = db_get_key(hDB, hScaler, &hKScaler);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot get key %s/Variables/HSCL", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  
  p_odb_data->nscal = hKScaler.num_values; /* find out total no. of scalers
                                              defined by Front End  */
  if (debug)printf("darc_get_odb: nscal = %d\n", hKScaler.num_values);
  for (i=0; i< p_odb_data->nscal ; i++)
  {
    p_odb_data->scaler_save[i] = (int) temp_scalers[i];
    if(debug) printf("scaler_titles[%d] = %s  data = %d \n",i,p_odb_data->scaler_titles[i],p_odb_data->scaler_save[i] );
  }

#endif // BNMR

/* Get the common parameters from /Experiment/Edit on start */
  sprintf(str,"/Experiment/Edit on start");
  status = db_find_key(hDB, 0, str, &hTmp);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","darc_get_odb: key %s not found", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
  

  /* get experiment number */
  size = sizeof(p_odb_data->experiment_number);
  status = db_get_value(hDB, hTmp, "experiment number", &p_odb_data->experiment_number, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"darc_get_odb","cannot retrieve %s/experiment number", str);
    write_message1(status,"darc_get_odb");
    return status;
  }
    
  /* get experimenter, temperature, orientation, field and sample */
  
  get_run_header_item(experimenter);
  get_run_header_item(orientation);
  get_run_header_item(sample);
  get_run_header_item(run_title);
  if(debug)
    {
      printf("DARC_get_odb: common items:\n");
      printf("DARC_get_odb: run_title = %s\n",p_odb_data->run_title);
      printf("DARC_get_odb: experimenter = %s\n",p_odb_data->experimenter);
      printf("DARC_get_odb: orientation = %s\n",p_odb_data->orientation);
      printf("DARC_get_odb: sample = %s\n",p_odb_data->sample);
    }
  if (!I_MUSR)
    {   // BNMR and TD-MUSR only
      get_run_header_item(field);
      get_run_header_item(temperature);
      if(debug)
	{
	  printf("DARC_get_odb: BNMR/BNQR & TDMUSR specific items:\n");
	  printf("DARC_get_odb: field = %s\n",p_odb_data->field);
	  printf("DARC_get_odb: temperature = %s\n",p_odb_data->temperature);
	}      
    }
#ifdef MUSR
  if(I_MUSR)
    {
      //  I M U S R
      /* Get parameters from  /Equipment/MUSR_I_Acq/Settings/imdarc  */
      sprintf(str, "/Equipment/MUSR_I_Acq/Settings/imdarc/edit");
      status = db_find_key(hDB, 0, str, &hTmp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","darc_get_odb: key %s not found", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      
      /* get IMUSR subtitle */
      get_run_header_item(subtitle);

      /* get  IMUSR comments */
      get_run_header_item(comment1);      
      get_run_header_item(comment2);
      get_run_header_item(comment3);

      /* Get "rig" parameter from  /Equipment/MUSR_I_Acq/Settings  */
      sprintf(str, "/Equipment/MUSR_I_Acq/Settings");
      status = db_find_key(hDB, 0, str, &hTmp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","darc_get_odb: key %s not found", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      /* use IMUSR setup name as "rig" */
      size = sizeof(p_odb_data->rig);
      status = db_get_value(hDB, hTmp, "config name", &p_odb_data->rig, &size, TID_STRING, FALSE);
      p_odb_data->rig[sizeof(p_odb_data->rig)-1] = '\0';
      if (status != DB_TRUNCATED && status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","cannot retrieve %s/config name", str );
	  write_message1(status,"darc_get_odb"); 
	  return status;
	}

      /* lookup /Equipment/MUSR_I_Acq/Settings/hardware/sweep device */
      sprintf(str, "/Equipment/MUSR_I_Acq/Settings/hardware/sweep device");
      status = db_find_key(hDB, 0, str, &hTmp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","darc_get_odb: key %s not found", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}
      /* use IMUSR scan title as "mode" */
      size = sizeof(p_odb_data->mode);
      status = db_get_value(hDB, hTmp, "scan title", &p_odb_data->mode, &size, TID_STRING, FALSE);
      p_odb_data->mode[sizeof(p_odb_data->mode)-1] = '\0';
      if (status != DB_TRUNCATED && status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"darc_get_odb","cannot retrieve %s/scan title", str);
	  write_message1(status,"darc_get_odb");
	  return status;
	}

      if(debug)
	{
	  printf("DARC_get_odb: IMUSR specific items:\n");
	  printf("DARC_get_odb: subtitle  = %s\n",p_odb_data->subtitle);
	  printf("DARC_get_odb: comment1  = %s\n",p_odb_data->comment1);
	  printf("DARC_get_odb: comment2  = %s\n",p_odb_data->comment2);
	  printf("DARC_get_odb: comment3  = %s\n",p_odb_data->comment3);
	  printf("DARC_get_odb: setup/rig = %s\n",p_odb_data->rig);
	  printf("DARC_get_odb: scan mode = %s\n",p_odb_data->mode);
	}
    }
#endif // MUSR
  /* get camp host name  */ 
  size = sizeof(p_odb_data->camp_host);
  copy_item(p_odb_data->camp_host, size, fmdarc.camp.camp_hostname , "camp hostname");
  if(!I_MUSR)
    {
      size=sizeof(p_odb_data->temperature_variable);
      copy_item(p_odb_data->temperature_variable, size, 
		fmdarc.camp.temperature_variable, "camp temperature variable");
      size = sizeof(p_odb_data->field_variable);
      copy_item(p_odb_data->field_variable, size, 
		fmdarc.camp.field_variable, "camp field variable");
    }
  if(debug) printf("DARC_get_odb: Camp hostname = %s\n",p_odb_data->camp_host);
  
  /*      get mdarc parameters */
  
  p_odb_data->purge_after =  fmdarc.num_versions_before_purge;

  size = sizeof(p_odb_data->save_dir);
  copy_item(p_odb_data->save_dir, size, 
	    fmdarc.saved_data_directory, "saved data directory");
  trimBlanks(p_odb_data->save_dir,p_odb_data->save_dir);
  /* if there is a trailing '/', remove it */
  s = strrchr(p_odb_data->save_dir,'/');
  i= (int) ( s - p_odb_data->save_dir );
  j= strlen( p_odb_data->save_dir );
  if (debug_check)
    printf("darc_get_odb: string length of saved_dir %s  = %d, last occurrence of / = %d\n", p_odb_data->save_dir, j,i);
  if ( i == (j-1) )
    {
      if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ... \n");
      p_odb_data->save_dir[i]='\0';
    }
  /* check purge_after >= 2 (see darc_check_params) */
  if(p_odb_data->purge_after < 2)
    {
      cm_msg(MINFO,"darc_get_odb"," parameter %s/num_versions_before_purge is out of range",
	     p_odb_data->purge_after);
      cm_msg(MINFO,"darc_get_odb","It must be greater than 1;  a value of 2 will be used");
      p_odb_data->purge_after = 2 ;
    }
  
  if(debug || debug_check)
    {
      printf("darc_get_odb:       Mdarc variables:\n");
      printf("Directory for saving the data: %s\n",p_odb_data->save_dir);
      printf("Number of versions of saved files kept for each run will be %d\n",p_odb_data->purge_after);
    }
  return(SUCCESS); 
}


/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/fifo_acq/client flags/febnmr to indicate to mdarc that is should stop the run
     set the flag in /equipment/fifo_acq/client flags/client alarm to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in febnmr.c for use of VxWorks frontend - later can try to just have 
     one routine with ifdefs 
  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status;
  debug =0;
  if(debug)printf("set_client_flag: starting\n");

  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",eqp_name,client_name );
  if(debug)
    printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 

  /* Set the client flag to TRUE (success) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,my_value,status);
      return status;
    }


  /* Set the alarm flag; TRUE - alarm should go off, if FALSE,  alarm stays off
     Note that in odb, 
        /alarm/alarms/client alarm/condition is set to "/equipment/fifo_acq/client flags/client alarm >0"

  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",eqp_name );
  if(debug)
    printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  size = sizeof(client_flag);
  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }
  return CM_SUCCESS ;
}

/*---------------------------------------------------------*/
void copy_item(char *p_struct,INT size, char *p_odb,  char *name) 

/*---------------------------------------------------------*/
{
  /* copies a string into the structure; truncates string if it's too long
     ... similar to get_run_header_item except a key is not needed
  */

  INT len;
  char tempString[128];
  debug=0;
  if(debug)
    printf("check_item_size: starting with string %s = %s; max size=%d \n",name,p_odb,size); 
  len = strlen(p_odb);
  if(debug)
    printf("maximum length in structure = %d; string length =%d\n",size,len); 
  if(size < len)
  { 
    strncpy( tempString, p_odb, size);
    printf("copy_item:truncating string \"%s\" at %d chars\n",name,size ); 
    cm_msg(MINFO,"darc_get_odb","truncated string \"%s\" at %d chars",name,size ); 
    tempString[size] = '\0';
    trimBlanks( tempString, tempString );
    p_struct = strdup (tempString);
  }
  else
    sprintf(p_struct,"%s",p_odb);
  if(debug)printf("copy_item: returning %s string as \"%s\"\n",name,p_struct);
  return;   
}

#ifdef BNMR
// stop_run in client_flags.c for musr
/*-----------------------------------------------------------------------------------------------------*/
INT stop_run(void)
/*-----------------------------------------------------------------------------------------------------*/
{
  char str[128];
  INT status,size;
  INT rstate;
  
  /* check for current run state; if not stopped, stop it */
  size = sizeof(rstate);
  status = db_get_value(hDB, 0, "/runinfo/State", &rstate, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","cannot get /runinfo/State (%d)",status);
      printf("run_state: Cannot determine present run state.\n");
    }
  else
    {
      //      if(rstate != run_state)
      //	printf("stop_run: (extern)run_state is %d while rstate is %d\n",run_state,rstate);
      
      if (rstate == STATE_STOPPED)
	{  
	  printf("stop_run: Run is already stopped\n");
	  return SUCCESS;
	}
    }

  printf("stop_run: attempting to stop the run...\n");

  status = cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0);
  if(status == CM_DEFERRED_TRANSITION)
    cm_msg(MINFO,"stop_run","Deferred transition is set");
  else if((status !=  CM_SUCCESS) && (status != CM_DEFERRED_TRANSITION))
    cm_msg(MERROR, "stop_run", "cannot stop run immediately: %s (%d)", str, status);
  else
    cm_msg(MINFO,"stop_run","run should now be stopped");

  
  status = db_get_value(hDB, 0, "/runinfo/State", &rstate, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","cannot get /runinfo/State (%d)",status);
      printf("run_state: Cannot determine present run state.\n");
      return status;
    }
  
  //  if(rstate != run_state)
  //    printf("stop_run: (extern)run_state is %d while rstate is %d\n",run_state,rstate);
      
  if(rstate != STATE_STOPPED)
    printf("stop_run: run state is not STOPPED\n");
  else
     printf("stop_run: run is now  STOPPED\n");

  return status;

}
#endif
