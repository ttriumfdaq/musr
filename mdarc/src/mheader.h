/* prototypes */

//INT camp_read(char *pevent, INT off);   /* retired */
INT camp_var_read(char *pevent, INT off);
#ifndef MUSR
INT epics_var_read(char *pevent, INT off);
#endif
INT camp_path_read(char *pevent, INT off);
INT header_info_read(char *pevent, INT off);
INT setup_record(void); // CAMP get/create record
#ifndef MUSR
INT get_ppg_type(void);
#endif

DWORD darc_get_odb(D_ODB *p_odb_data);
void camp_close(void);
void write_logging_flag(INT flag, BOOL setval );
#ifdef CAMP_ACCESS
INT init_camp(void);
void check_camp(void);
INT ping_camp(void);
#endif

#ifdef EPICS_ACCESS
#include "mdarc_epics.h"
#endif
