/********************************************************************\

  Name:         mheaderMusr.c	
  Created by:   Suzannah Daviel, TRIUMF

    - combined version of older programs fe_header and fe_camp

        This version for MUSR only - use mheaderBnmr for BNMR/BNQR (was 1.13 of mheader.c)

  Contents:     Frontend program for  IMUSR ( for Type 1 )
                Sends DARC header information and CAMP data and CAMP path information
                to make saved MUD  histogram files.

                Defines two equipments, CAMP and HEADER 

		CAMP equipment produces one bank periodically i.e.
		       CVAR bank contains values read from camp
		HEADER equipment produces two banks DARC and CAMP  on transitions (BOR/EOR)

	        The camp paths are read from /equipment/camp/settings in odb.
                They are checked out by a perlscript, camp.pl executed
                at begin-of-run.

$Log: mheaderMusr.c,v $
Revision 1.2  2015/03/24 20:13:38  suz
added cast of (void **) to bk_created to get rid of warnings

Revision 1.1  2015/03/24 00:46:31  suz
new to cvs; special version for MUSR expts based on mheader.h version 1.13. Use mheaderBnmr.c for bnmr expts
  

\********************************************************************/

#include <stdio.h>
#include <ctype.h>

/* camp includes come before midas.h */

#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#ifdef INLINE
#undef INLINE
#endif

#include "midas.h"
#include "experim.h"
//* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "mud_util.h"
#include "trii_fmt.h"
#include "mdarc.h"
#include "darc_odb.h"
#include "mheader.h" // prototypes

#define FAILURE 0
#define NEW

INT setup_record(void);
/*-- Globals -------------------------------------------------------*/
//HNDLE hDB=0, hCamp=0,  hMDarc=0, hFS=0;
HNDLE hCamp=0,  hFS=0; // hDB hMdarc defined in mdarc.h
CAMP_SETTINGS camp_settings;


MUSR_TD_ACQ_MDARC fmdarc;
char i_eqp_name[]="MUSR_I_ACQ";
char eqp_name[]="MUSR_TD_ACQ"; // needed for mdarc area
/* frontend client names */
char TD_ClientName[] = "fev680";
char I_ClientName[] = "femusr";
char FE_ClientName[256];    /* the actual client name of the frontend (fev680 or femusr)*/


int status;
int firstTime = TRUE;
int campInit  = FALSE;
int camp_available;
char serverName[LEN_NODENAME+1];
char perl_script[80] ; 
char perl_cmd[132];
FILE *FIN;
BOOL end_run;
char str[256];
char beamline[6];
char lc_beamline[6];

char gpcPrgName[] = "mHeader";

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "mheader";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms    */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 50000;

/* maximum event size (for fragmented events only) */
INT max_event_size_frag = 5*1024*1024;


/* buffer size to hold events */
INT event_buffer_size =0x200000;// DEFAULT_EVENT_BUFFER_SIZE;  


/* prototypes */

/*-- Equipment list ------------------------------------------------*/
EQUIPMENT equipment[] = {
  { "CAMP",                 /* equipment name */
    { 13, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_MANUAL_TRIG,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING |         /* read when running  */
     RO_ODB,               /* and update ODB */ 
    60000,                /* read every 60 sec */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    10,                   /* log history every event */
      "", "", "",},
    camp_var_read,        /* readout routine */
    NULL,                 /* class driver main routine */
    NULL,                 /* device driver list */
    NULL,                 /* init string */
  },

  { "HEADER",               /* equipment name */
    { 14, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    ( RO_BOR | RO_EOR),    /* read on BOR or EOR transitions only */
    30000,                /* read every 30 sec (need RO_RUNNING above) */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    0,                    /* log history every event  (if non-zero event is sent to
                             odb) */
      "", "", "",},
    header_info_read,       /* readout routine */
    NULL,                 /* class driver main routine */
    NULL,                 /* device driver list */
    NULL,                 /* init string */
  },
  
  { "" }
};

//#include "check_camp.c"
#include "get_musr_type.c" // include code shared with mdarcMusr.c



/*-- Dummy routines ------------------------------------------------*/

INT  poll_event(INT source[], INT count, BOOL test) {return 1;};
INT  interrupt_configure(INT cmd, INT source[], PTYPE adr) {return 1;};


 
/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  char str[256];
  int size,i,j, len;
  CAMP_SETTINGS_STR(camp_settings_str);
  MUSR_TD_ACQ_MDARC_STR(mdarc_str);
  BOOL fe_flag;
  INT run_state;
  char *s;
  char cmd[128];
  
  debug = TRUE; 
  campInit = FALSE;
  camp_available = FALSE; 
  hDB=hMDarc=0; // globals

  cm_get_experiment_database(&hDB, NULL);

  /* find out our experiment name */
  size = sizeof (expt_name);
  status = db_get_value(hDB,0, "/Experiment/name",  expt_name, &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"frontend_init","error accessing key /Experiment/name");
    write_message1(status,"frontend_init");
    return (status);
  }
  printf("Experiment name: %s\n",expt_name);



  /* make sure there's only one copy of this program running */
  status=cm_exist("mheader",FALSE); 
  if(debug) printf("status after cm_exist for client mheader = %d \n",status);
  if(status == CM_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init","Another copy of mheader is already running");
      return (CM_NO_CLIENT);  /* return an error */
    }




  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","key not found /Runinfo/State (%d)",status);
      return(status);
    }



  /* get the key hMDarc  */
  sprintf(str,"/Equipment/%s/mdarc",eqp_name);
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      hMDarc=0;
      if(debug) printf("frontend_init: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(mdarc_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"frontend_init","Failure creating mdarc record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"frontend_init","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key mdarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMDarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "frontend_init", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}

      printf("Size of mdarc saved structure: %d, size of mdarc record: %d\n", sizeof(MUSR_TD_ACQ_MDARC) ,size);  
      
      if (sizeof(MUSR_TD_ACQ_MDARC) != size) 
      {
	cm_msg(MINFO,"frontend_init","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)", sizeof(MUSR_TD_ACQ_MDARC)
	       ,size);

	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(mdarc_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"frontend_init","Could not create mdarc record (%d)\n",status);
	      if (run_state == STATE_RUNNING )
		cm_msg(MINFO,"frontend_init","May be due to open records while running. Stop the run and try again");
	      return status;
	    }
	  else
	    if (debug)printf("Success from create record for %s\n",str);
	}
    }
  
  /* try again to get the key hMDarc  */
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","Failed to get the key %s (%d)",str,status);
      return(status);
    }

  size = sizeof (fmdarc);
  if(debug) printf("hMDarc = %d, size of record = %d \n",hMDarc,size);
  
  status = db_get_record(hDB, hMDarc, &fmdarc, &size, 0);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot retrieve %s record (%d)",str,status);
      return DB_NO_ACCESS;
    }
  
  status = get_musr_type(&fe_flag);// get_musr_type

  if(status!=DB_SUCCESS)
    {
      cm_msg (MERROR,"frontend_init","Cannot determine MUSR type (TD or Integral)");
      return (status);
    } 


  
  //  get the run state
  size = sizeof(run_state);
  status = db_get_value(hDB,0, "/Runinfo/State",  &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"frontend_init","error from db_get_value  /Runinfo/State (%d)",status);
      return (status);
    }

  printf("frontend_init: run state is %d\n",run_state);


#ifdef CAMP_ACCESS
  printf("Camp hostname = %s\n",fmdarc.camp.camp_hostname);
#endif

  hCamp = setup_record();//  creates camp record if necessary (needed for CAMP )
  if(hCamp == 0) // error
    return DB_INVALID_PARAM;

#ifdef CAMP_ACCESS

  /*
             Camp.pl  perlscript 
  camp_settings.perl_script contains only the filename 
*/
  len = strlen(camp_settings.perl_script );
  
  if(len <= 0)
    {
      cm_msg(MERROR, "frontend_init","no perlscript name supplied in /equipment/camp/settings/perl_script");
      return(DB_INVALID_PARAM);
    }
  
  //  if(debug)
  printf("perlscript file name: %s (len=%d)\n",camp_settings.perl_script,len);
  /*
    get the path of the perl scripts 
  */
  sprintf(perl_path,"%s",fmdarc.perlscript_path); /* perl_path (in mdarc.h) is needed as an input parameter (to perl script)  */
  trimBlanks(perl_path,perl_path);
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_path,'/');
  i= (int) ( s - perl_path );
  j= strlen( perl_path );
  
  if(debug)
    printf("string length of perl_path %s  = %d, last occurrence of / = %d\n", perl_path, j,i);
  if ( i == (j-1) )
    perl_path[i]='\0';
  
  strcpy(perl_script,perl_path); /* perl_path needed as a parameter  */
  strcat(perl_script,"/"); // add a "/" 
  strcat(perl_script,camp_settings.perl_script);
  trimBlanks(perl_script,perl_script);
  
  if(debug) {
    printf ("Perl script: %s\n", perl_script);
    printf ("Perl path:   %s\n", perl_path);
  }
#endif

  /* Get the beamline for MUSR - this is a constant value for each experiment.
       beamline for MUSR should be copied into mdarc/histograms/musr/beamline
       by musr_config  (for BNMR get_ppg_type got the beamline) */


  strcpy(beamline,fmdarc.histograms.musr.beamline);
  for (j=0; j<strlen(beamline); j++)
    {
    beamline[j] = toupper (beamline[j]); /* convert to upper case */
    lc_beamline[j]= tolower  (beamline[j]); /* and lower case for opening perl txt files */
    }

  if(debug) printf(" beamline:  %s\n",beamline);
  if (strlen(beamline) <= 0 )
    {
      cm_msg(MERROR, "frontend_init", 
	     "No valid beamline supplied; make sure musr_config has run & updated mdarc area");
      return (DB_INVALID_PARAM);
    }


  /* Check if a run is in progress*/
  if(run_state != STATE_STOPPED)
    {
      printf("A run IS in progress (run_state=%d)\n",run_state);

      if(TD_MUSR)

	{
	  printf("TD-type run in progress; no further action\n");
	  return(CM_SUCCESS);
	}

      printf("I-MUSR type of run in progress\n");

      printf("frontend_init: Info: epics access is not defined in this code\n");


#ifdef CAMP_ACCESS
      status = init_camp();
      if(status != SUCCESS) return status;
#else  /* no CAMP access */
  printf("frontend_init: Warning...camp access is not defined in this code\n");
#endif // no CAMP ACCESS
  
    }
  else
    printf("frontend_init: Success.... waiting for run to start\n");

  printf("\nfrontend_init: returning SUCCESS\n");
  return CM_SUCCESS;
}


/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  if(debug) printf("frontend_exit: starting\n");
#ifdef CAMP_ACCESS
  if (camp_available)
    {
      if(campInit)
	{
	  if(debug)printf("frontend_exit : calling camp_clntEnd()\n");
	  camp_clntEnd();  
	}
      else
	if(debug) printf("frontend_exit: INFO: camp connection is already closed \n");
    }
  else
	if(debug) printf("frontend_exit: INFO: camp connection was never opened \n");
#endif


  return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  cm_yield(500);
  return CM_SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number_local, char *error)
{
    char client_str[128];
    BOOL client_flag=TRUE; // mdarc sets the client flag for mheader FALSE on a prestart
    char cmd[128];
    BOOL fe_flag;
  end_run=FALSE; /* make sure flag is cleared */

  cm_msg(MINFO,"mheader","mheader detects begin of run\n");
  /* note: run number could have been changed on a prestart by run number checker 
*/  

  run_number=run_number_local;  // not sure why this is done any more

  /* Get the run type of this new run */

  status = get_musr_type(&fe_flag);


  if(status!=DB_SUCCESS)
    {
      cm_msg (MERROR,"begin_of_run","Cannot determine run type (Integral or TD)");
      return (status);
    }

  if (!fe_flag)
    {
      cm_msg (MERROR,"begin_of_run","Wrong frontend (Integral or TD) has been booted in ppc");
      return (DB_NO_ACCESS);
    }
  if(TD_MUSR)
    {
      printf("begin_of_run: INFO: TD-type run has been detected; setting client status flag for success & returning\n");
      status = set_client_flag("mheader",SUCCESS); // set client flag to success
      return(status);
    }


  /*  I-MUSR type run is starting 

      Update the whole mdarc record first to reflect any changes   */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"begin_of_run","Failed to retrieve mdarc record  (%d)",status);
      write_message1(status,"begin_of_run");
      return(status);
    }

#ifndef MUSR
  /* These are used by darc_get_odb in  mdarc_subs   (BNMR/BNQR only)
     not needed for MUSR
  */
  nHistBins =  fmdarc.histograms.num_bins; //fmdarc set up by setup_hotlink
  nHistBanks = nH =  fmdarc.histograms.number_defined; 
  printf(": begin_of_run: Number of bins = %d;  no. defined histograms = %d\n",nHistBins,nH);
#endif

  /* update the camp record */
  size = sizeof (camp_settings);
  if(debug) printf("hCamp = %d, size of record = %d \n",hCamp,size);
  
  status = db_get_record(hDB,hCamp, &camp_settings, &size, 0);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin-of-run", "failed to retrieve Camp record (%d)",status);
      return status;
    }



#ifdef CAMP_ACCESS
  status = init_camp();
  if(status != SUCCESS)return status;
#endif // CAMP access
  
  printf("begin_of_run: setting client flag for mheader true (success)\n");
  status = set_client_flag("mheader",SUCCESS);
  
  return status ; /* SUCCESSful return */
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  if(debug) printf("end_of_run: setting flag end_run\n");
/* set a flag to inform header_info_read that run is ending
   so camp port must be closed  */
  end_run=TRUE;
  
  return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}



/*---Read info for data archiver: BNMR/BNQR  ---------------------------------------------------------------*/
INT 
header_info_read(char *pevent, INT off)

/*-------------------------------------------------------------*/
{
#ifndef MUSR
  D_ODB *p_odb_data;
  D_ODB odb_data;
#endif

/* for camp bank */
  char str1[512], tempString[80];
  INT len_darc;
  INT i, size;
  char *pdata;
  INT len;
  BOOL camp_bank;

/*  this structure is in   ../mud/util/trii_fmt.h
    typedef struct {
    INT16 type;
    INT16 len_path;
    char path[50];
    INT16 len_title;
    char title[19];
    INT16 len_units;
    char units[8];
    } IMUSR_CAMP_VAR;
*/
  IMUSR_CAMP_VAR camp_info;
 
  INT transition;
  CAMP_VAR_TYPE type;
  char *pstr;
  camp_bank=FALSE; // camp_bank not yet created
  debug=TRUE;
  if(debug) printf("\nheader_info_read: starting \n");

#ifndef MUSR
  p_odb_data = &odb_data;

  printf("\n");
/* read info into a fixed event */
  if(debug) printf("header_info_read: starting \n");
  
  if(ppg_type !=1)
    {
      if(debug)printf("header_info_read: type %d run; no action\n",ppg_type);
      return 0;
    }

  /* clear memory area for odb_data */
  size = sizeof(odb_data);
  if(debug)printf("size of odb_data : %d\n",size);
  memset(p_odb_data,0,sizeof(odb_data) );

  
  if(debug) printf("On entry, pevent = %p\n",pevent);
  bk_init(pevent);
  
  /* create DARC bank */
  bk_create(pevent, "DARC", TID_CHAR, (void **) &pdata);
  if(debug) printf("After bk_create, pevent=%p,pdata = %p\n",pevent,pdata);
  
  
  
  /*
   *    Read the parameters from odb into the structure
   */
  
  
  
  status = darc_get_odb(p_odb_data);
  if (status == DB_SUCCESS)  
    {
      
      if(debug)
	{
	  printf("run number = %d\n",odb_data.run_number);
	  printf("area = %s; rig = %s; mode = %s\n",odb_data.area,odb_data.rig,odb_data.mode);
	}
      size = sizeof(odb_data);
      if(debug)printf("size of odb_data : %d\n",size);
      /* now send the bank out */
      if(debug) printf("Copying data at pointer pdata=%p\n",pdata);
      memcpy(pdata, (char *) &odb_data, size);
      pdata += size;
      bk_close(pevent, pdata);
      if(debug) printf("closed darc bank, pevent = %p, pdata= %p, bk_size(pevent) = %d\n",pevent, pdata,bk_size(pevent));
      printf("header_info_read: Darc header bank  (ID=14) has length %d\n",bk_size(pevent)); 
      len_darc = bk_size(pevent);  // length of DARC event
    }
  else
    {
      cm_msg(MERROR,"header_info_read","Failure to get the odb data needed; cannot send DARC bank");
      return 0; /* size of event is zero; no event */
      len_darc = 0;
    }
#else  // MUSR
  if(TD_MUSR)
    {
      if(debug)printf("header_info_read: TD-MUSR run; no action\n");
      return 0;
    }
  len_darc = 0; /* no DARC header for MUSR */
#endif


#ifdef CAMP_ACCESS
  /*
    Now Read camp info for data archiver 
  */
  
  if ( !camp_available)  // handles case of no camp variables defined
    {
      //if(debug) 
      printf("header_info_read: Camp is not available or no camp variables defined, no CAMP bank sent\n");
      goto cont; // may be some EPICS data      
    }
  
  if (! campInit)  // just in case...
    {
      status=reconnect();
      if (status != CAMP_SUCCESS)
	{
	  goto cont; // may be some EPICS data
	}
    }
  
  if (camp_settings.n_var_logged <= 0) // just in case...
    {
      printf("header_info_read: No CAMP bank; no. camp variables to be logged=0\n");
      if (end_run) camp_close();
      goto cont; // may be some EPICS data
    }
   
  /* CAMP data should be available */
  size = sizeof(camp_info);
  memset(&camp_info,0,size); // clear structure to start
  

  if(debug)
    {
      if (end_run)printf("INFO - Run is ending\n");
      printf("Getting an update of camp database...\n");
    }

   
  /*
   *    Get an update of the CAMP database
   */
  
  status = camp_clntUpdate();
  if( status != CAMP_SUCCESS )
  {
    cm_msg(MERROR,"header_info_read:", "failed call to retrieve CAMP data" );
    /* try to reconnect */    
      status=reconnect();
      if (status != CAMP_SUCCESS)
	{
	  goto cont; // may be some EPICS data
	}

      status = camp_clntUpdate();
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MERROR,"camp_info_read", "failed call to retrieve CAMP data after reconnect" );
	  campInit=FALSE;
	  goto cont; // may be some EPICS data
	}
      if(debug)printf("Returned from camp_update after reconnect with CAMP_SUCCESS\n");
  }
  if(debug) printf("header_info_read: success from camp_clntUpdate()\n");

  /*
  /* create the bank for CAMP 
  */
  if(debug) printf("On entry, pevent = %p\n",pevent);

  bk_init(pevent); /* no header bank for MUSR */

  bk_create(pevent, "CAMP", TID_CHAR, (void **) &pdata);
  if(debug) printf("opened CAMP bank ( pevent=%p, pdata=%p) \n",pevent,pdata);
  camp_bank = TRUE;
/* 
      now get the camp information for camp_info 
*/

//    MAIN LOOP on each camp variable
  for (i=0;  i < camp_settings.n_var_logged; i++)
  {
    if(debug) printf("\nLOOP %d, calling campSrv_varGet with path=%s\n",i,camp_settings.var_path[i]);
    
    status=campSrv_varGet(camp_settings.var_path[i],2);
    if( status != CAMP_SUCCESS )
    {
      cm_msg(MINFO,"header_info_read","failed call campSrv_VarGet with path= %s (%d)",camp_settings.var_path[i]),status;

      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      camp_bank = FALSE;
      goto cont; // may be some EPICS data
    }
    
    /*  path */
    strncpy(tempString, camp_settings.var_path[i] ,50);
    tempString[50] = '\0';
    trimBlanks( tempString, tempString );
    strcpy(camp_info.path,tempString);
    camp_info.len_path=strlen( camp_info.path );
    
    /* type */
    status=camp_varGetType(camp_settings.var_path[i],&type);
    if( status != CAMP_SUCCESS )
    {
      if(status==CAMP_INVAL_VAR) printf("Got CAMP invalid variable type\n");
      cm_msg(MINFO,"header_info_read","failed call camp_varGetType with path= %s (0x%x)",camp_settings.var_path[i]),status;
      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      camp_bank=FALSE;
      goto cont; // may be some EPICS data
    }
    camp_info.type=type;

    /* title */
    status=camp_varGetTitle (camp_settings.var_path[i],str1, sizeof( str1 ));
    if( status != CAMP_SUCCESS )
    {
      cm_msg(MINFO,"header_info_read","failed call camp_varGetTitle with path= %s (%d)",camp_settings.var_path[i]),status;
      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      camp_bank=FALSE;
      goto cont; // may be some EPICS data
    }
    strncpy(tempString,str1,19);
    tempString[19] = '\0';
    trimBlanks( tempString, tempString );
    strcpy( camp_info.title,tempString );
    camp_info.len_title=strlen( camp_info.title );
    
    /* units */
    status=camp_varNumGetUnits(camp_settings.var_path[i],str1, sizeof( str1 ));
    if( status != CAMP_SUCCESS )
    {
      cm_msg(MINFO,"header_info_read","failed call camp_varGetUnits with path= %s (%d)",camp_settings.var_path[i]),status;
      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      camp_bank=FALSE;
      goto cont; // may be some EPICS data
    }
    strncpy(tempString,str1,19);
    tempString[19] = '\0';
    trimBlanks( tempString, tempString );
    strcpy(camp_info.units,tempString );

    camp_info.len_units=strlen( camp_info.units );

    //    if(debug)
    {
      printf(" -- ********** CAMP *************** -- \n");
      printf("header_info_read: Values in structure camp_info for loop %d :\n",i);
      printf("path %s, type %d units %s title %s\n",camp_info.path, camp_info.type,
             camp_info.units, camp_info.title);
      printf("len_path %d len_title %d len_units %d\n",camp_info.len_path,
             camp_info.len_title, camp_info.len_units);
      printf(" -- ************************* -- \n");

    }

  


    /* copy the structure into the bank */
    //    printf("Before memcopy for CAMP, pdata = %p\n",pdata);
    memcpy((char *)pdata, (char *) &camp_info, sizeof(camp_info));
    pdata += sizeof(camp_info);
    if(debug) printf("Size of camp_info=%d copied to bank; pdata=%p \n",sizeof(camp_info),
		     pdata);
    
  } // end of MAIN LOOP on each CAMP variable


  if(debug)
    printf("closing bank CAMP with pevent=%p, pdata=%p\n",pevent,pdata);
  bk_close(pevent, pdata );
  
  if(debug) 
    printf("closed camp bank,  pevent = %p, pdata = %p, bank size = %d, event_size = %d\n\n",
                   pevent, pdata, (bk_size(pevent)-len_darc),bk_size(pevent));

    printf("header_info_read: Camp_Info bank sent of length %d (event size = %d) \n", 
	   (bk_size(pevent)-len_darc),bk_size(pevent) );


  if (end_run)
      camp_close();

#endif // CAMP_ACCESS

cont: // continue 

  if(camp_bank)
    return bk_size(pevent);
  else
    return  len_darc; // no CAMP event
  

  if(debug)printf("closing bank CAMP with pevent=%p, pdata=%p\n",pevent,pdata);
  bk_close(pevent, pdata );
  
  if(debug) 
    printf("closed camp bank,  pevent = %p, pdata = %p, bank size = %d, event_size = %d\n\n",
	   pevent, pdata, (bk_size(pevent)-len_darc),bk_size(pevent));
  
  printf("header_info_read: Camp_Info bank sent of length %d (event size = %d) \n", 
	 (bk_size(pevent)-len_darc),bk_size(pevent) );
  
  return bk_size(pevent);
}


/*------------------- camp_close ------------------*/
void 
camp_close(void)
/* ----------------------------------------------*/
{
#ifdef CAMP_ACCESS
     /* close the camp port when run stops */
  if(debug) printf("camp_close: starting\n");
  if (camp_available)
    {
      
      if(campInit)
	{
	  if(debug)printf("camp_close : calling camp_clntEnd()\n");
	  camp_clntEnd();  
	}
      else
	if(debug) printf("camp_close: INFO: camp connection is already closed. \n");
    }
  else
    if(debug) printf("camp_close: INFO: camp connection was never opened \n");
  
  campInit = FALSE;
  end_run = FALSE;
#endif
} 

/*---Read Camp variables ---------------------------------------------------------------*/
/* read camp variables into a variable length float event*/
INT camp_var_read(char *pevent, INT off)
{
  double *pdata;
  INT i,size, nerror;
  int j=0;
  INT  error_flag,len_camp;
  double camp_data;
  BOOL bki, cvar_bank;
  
  
  len_camp = 0;
  bki=cvar_bank = FALSE;
  
#ifdef CAMP_ACCESS
  if(debug) printf("camp_var_read: starting \n");
  /* read camp into a fixed event */

  if(TD_MUSR)
      {
	if(debug)printf("camp_var_read; TD-type, no action\n");
	return 0;
      }
  
  printf("camp_var_read: camp_available=%d \n",
	 camp_available);

  if(!camp_available) // handles case of no camp variables defined
    {
      if(debug)printf("camp_var_read: No camp is available or no camp variables have been defined\n");
      goto cont1;
    }
  if(!campInit)
    {
      status=reconnect();
      if (status != CAMP_SUCCESS)
	goto cont1;
    }
  
  if(debug) printf("On entry, pevent = %p\n",pevent);
  bk_init(pevent);
  printf("camp_var_read: calling bk_init for CVAR\n");
  bki=TRUE;
  bk_create(pevent, "CVAR", TID_DOUBLE, (void **) &pdata);
  cvar_bank = TRUE;
  
  
  /*
   *    Get an update of the CAMP database
   */
  
  if(debug) printf("camp_var_read: Calling camp_clntUpdate()\n");
  status = camp_clntUpdate();
  if( status != CAMP_SUCCESS )
    {
      cm_msg(MERROR,"camp_var_read", "failed call to retrieve CAMP data" );
      
      /* try to reconnect */
      status=reconnect();
      if (status != CAMP_SUCCESS)
	{
	  status = bk_delete(pevent, "CVAR"); /* delete the event */
	  cvar_bank = FALSE;
	  goto cont1; // cancel the event
	}
      status = camp_clntUpdate();
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MERROR,"camp_var_read", "failed call to retrieve CAMP data after reconnect" );
	  campInit=FALSE;
	  status = bk_delete(pevent, "CVAR"); /* delete the event */
	  cvar_bank = FALSE;
	  goto cont1; /* cancels the event */
	}
      if(debug)printf("Returned from camp_update after reconnect with CAMP_SUCCESS\n");
      
    }
  nerror=0; /* initialize error count */
  error_flag=FALSE; /* and error flag */
  
  for (i=0;  i < camp_settings.n_var_logged; i++)
    {
      //      printf("calling campSrv_varGet with path=%s\n",camp_settings.var_path[i]);
      status=campSrv_varGet(camp_settings.var_path[i],2);
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MINFO,"camp_var_read","failed call campSrv_VarGet with path= %s (%d)",camp_settings.var_path[i]),status;
	  /* handle this by setting value to zero  */
	  error_flag = TRUE;
	  nerror++;
	}
      else
	{
	  //     printf("calling camp_varNumGetVal with path=%s\n",camp_settings.var_path[i]);
	  
	  status=camp_varNumGetVal(camp_settings.var_path[i], &camp_data);
	  if( status != CAMP_SUCCESS )
	    {
	      cm_msg(MINFO,"camp_var_read","failed call camp_VarNumGetValue with path= %s (%d)",camp_settings.var_path[i], status);
	      error_flag = TRUE;
	      nerror++;
	    }
	  if(debug)printf("Value read from camp path %s  : %f (index=%d)\n", camp_settings.var_path[i], camp_data, i);
	  if (error_flag)
	    *pdata++ = 0.0 ; /* could not read value */
	  else
	    *pdata++ = ( (double) camp_data);
	  error_flag=FALSE;
	}
    }

 cont1:

#endif
  
#ifndef EPICS_ACCESS
  printf("camp_var_read: EPICS access is not defined\n");
  if(cvar_bank)
    {
      bk_close(pevent, pdata);
       if(debug) printf("closed cvar bank, pevent = %p, pdata= %p, bk_size(pevent) = %d\n",
			pevent, pdata,bk_size(pevent));
       return (bk_size(pevent));
    }
  else
    return 0; // no CVAR bank

#else
  
  if(debug) printf("camp_var_read: EPICS ACCESS is defined\n");
  if(!epics_available || n_epics <= 0) 
    {
      printf("camp_var_read: EPICS is not available or no epics variables are selected\n");
      if(cvar_bank)
	{
	  bk_close(pevent, pdata);
	  if(debug) printf("closed cvar bank, pevent = %p, pdata= %p, bk_size(pevent) = %d\n",
			   pevent, pdata,bk_size(pevent));
	  return (bk_size(pevent));
	}
      else
	return 0; // no CVAR bank
   
    }
  
  if(!bki)
    {
      printf("camp_var_read: calling bk_init\n");
      bk_init(pevent);
    }
  
  if( !cvar_bank)
    {
      bk_create(pevent, "CVAR", TID_DOUBLE, (void **) &pdata);
      cvar_bank = TRUE;
      printf("camp_var_read: created CVAR bank and filling with %d CAMP dummy values\n", 
	     camp_settings.n_var_logged);
      if(camp_settings.n_var_logged > 0)
	{
	  for (i=0; i< camp_settings.n_var_logged; i++)
	    *pdata++ = 0.0; /* could not read CAMP value (if any) */
	}
    }
  nerror=0; /* initialize error count */
   
  
  for (i=0; i<n_epics; i++)
    {
      status = read_epics_value(i,FALSE); // no statistics
      if(status != SUCCESS)
	{
	  *pdata++ = 0.0; /* could not read value */
	  nerror++;
	}
      else
	{
	  *pdata++ = (double)  epics_log[i].value;
	  printf("camp_var_read:  i=%d  added %f to EVAR bank\n",i, epics_log[i].value);
	}
    }
  printf(" returning bk_size:%d\n",bk_size(pevent) );
  size = bk_size(pevent);  // total size including camp data 
  
  bk_close(pevent, pdata);
  if(debug) 
    printf("camp_var_read: closed CVAR bank,  pevent = %p, pdata = %p, bank size = %d\n",
	   pevent, pdata, bk_size(pevent));
  
  printf("camp_var_read: CVAR bank sent of length %d  \n", 
	 bk_size(pevent) );
  return  bk_size(pevent);
  
#endif // EPICS access
  
}


#ifdef CAMP_ACCESS
INT reconnect(void)
{
  INT i;
  /* try to reconnect */
  campInit=FALSE;
  camp_close();
  for(i=0; i<20; i++)
    {
      if (debug) printf("reconnect: trying to reconnect to camp... calling camp_clntInit \n");
      status = camp_clntInit( serverName, 10 );
      if(status == CAMP_SUCCESS)
	{
	  if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS\n");
	  campInit=TRUE;
	  return status;
	}
      if(debug)printf("reconnect: failure from camp_clntInit; waiting 10s and retrying\n");
      cm_yield(5000);
      ss_sleep(5000);
    }
  cm_msg(MERROR,"reconnect","Cannot reconnect to CAMP\n");
  return status;
}
#endif // CAMP_ACCESS




/* NOTE: this is also in mdarc_subs. Include a copy here because MUSR (unlike BNMR) does
   not include mdarc_subs in this file (darc_write_odb is not wanted here */
/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/fifo_acq/client flags/febnmr to indicate to mdarc that is should stop the run
     set the flag in /equipment/fifo_acq/client flags/client alarm to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in febnmr.c for use of VxWorks frontend - later can try to just have 
     one routine with ifdefs 
  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status;
  if(debug)printf("set_client_flag: starting\n");

  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",eqp_name,client_name );
  if(debug)
    printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 

  /* Set the client flag to TRUE (success) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,my_value,status);
      return status;
    }


  /* Set the alarm flag; TRUE - alarm should go off, if FALSE,  alarm stays off
     Note that in odb, 
        /alarm/alarms/client alarm/condition is set to "/equipment/fifo_acq/client flags/client alarm >0"

  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",eqp_name );
  if(debug)
    printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  size = sizeof(client_flag);
  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }
  return CM_SUCCESS ;
}




#ifdef CAMP_ACCESS
INT init_camp(void)
{
  INT i,j,status;

  /* Check there is a valid number of camp variables to be logged */
  if (camp_settings.n_var_logged <= 0)
    {
      cm_msg(MINFO, "init_camp", "No camp variables are selected to be logged in /equipment/camp/settings");
      printf("init_camp: No camp variables are selected to be logged in /equipment/camp/settings\n");
      camp_available = campInit = FALSE; /* make sure these flags are false */
      
      return SUCCESS;
    }
  
   
      
  if(debug)
    {
      printf("init_camp: number of camp values = %d\n",camp_settings.n_var_logged);
      for (j=0; j< camp_settings.n_var_logged; j++)
	printf("init_camp: camp path  = %s\n",camp_settings.var_path[j]);
    }
      
      
  /* Check there is a valid camp hostname */
  check_camp(); // sets camp_available
  if (!camp_available)
    {  cm_msg(MERROR,"init_camp","Camp hostname %s is invalid; no camp data can be saved "
	      ,serverName);
    return DB_INVALID_PARAM;
    }

	  
  /* We have a valid CAMP hostname - but is it active?  
     ( ping the camp host because if someone turns off the crate, there is
     no timeout and it can waste a lot of time)  */ 
  status = ping_camp();
  if (status == DB_NO_ACCESS)
    {
      cm_msg(MERROR,"init_camp","Camp host %s is unreachable. Camp data cannot be saved",
	     serverName);
      camp_available=FALSE;
      return status;
    }
  
  if (debug) printf ("Camp host is responding to ping\n");
  
  /* now execute perlscript to check the camp paths, write the polling intervals */
  if(debug)
    {
      printf ("init_camp: Perl script is %s\n", perl_script);
      printf ("init_camp: Perl path is   %s\n", perl_path);
    }

  sprintf(perl_cmd,"%s %s %s %s %s",perl_script,perl_path,expt_name,eqp_name,lc_beamline); 
  printf("init_camp: Executing perl script to check camp paths for logged variables...\n");
	  
  printf("init_camp: Sending system command  cmd: %s\n",perl_cmd);
  status =  system(perl_cmd);  /* now open camp connection */
  if (status)
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR,"init_camp","Perl script %s returns failure status (%d)",perl_script,status);
      cm_msg (MINFO,"init_camp","Check information file \"/home/%s/online/%s/log/camp.txt\" for details",lc_beamline, lc_beamline);
      
      /* Note:
	 if no recent additions to log file, there may be compilation errors in the perl script;check mheader window");
      */
      cm_msg (MERROR,"init_camp","Stop run, fix error in camp logged variables, then restart run");
      return (DB_INVALID_PARAM); // bad status from perl script
    }
  if(debug)printf("init_camp : Success from perl script to check camp paths\n");
	  
	  
	  
  /* Initialize CAMP connection */
	  
	
  if (!campInit) 
    {   // camp_clntInit not yet called; camp port is not open
      if (debug) printf("init_camp: calling camp_clntInit \n");
      status = camp_clntInit( serverName, 10 );
      if(status == CAMP_SUCCESS)
	{
	  campInit = TRUE;
	  if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS\n");
	}
      else
	{
	  cm_msg(MERROR,"init_camp","Failed initialize call to CAMP (called because run is in progress)");
	  if(debug) printf("init_camp: Returned from camp_clntInit with CAMP_FAILURE\n");
	  return DB_INVALID_PARAM;
	}
    }
  else
    cm_msg(MINFO,"init_camp","Camp port is already open. It will NOT  be closed and re-opened");

  printf("init_camp: CAMP has been successfully initialized\n");
  return SUCCESS;
}
#endif  // CAMP_ACCESS


INT setup_record()
//-------------------------------------------------------------
{
  /*  creates or gets camp equipment record 
      returns Handle hCamp or 0 for error
  */
 char str[80];
  CAMP_SETTINGS_STR(camp_settings_str); //  I-MUSR camp equipment includes epics settings
  INT status,size;
  HNDLE HCamp=0;

 sprintf(str,"/Equipment/Camp/Settings");
  //printf("str: %s\n",str);

  status = db_find_key(hDB, 0, str, &HCamp);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("main: Failed to find the key %s (%d) ",str,status);

      /* Create record for camp/settings area */
      if(debug) printf("Attempting to create record for %s\n",str);

      status = db_create_record(hDB,0,str,  strcomb(camp_settings_str));
      if(status!= DB_SUCCESS)
        {
          cm_msg(MERROR,"frontend_init","Create Record fails for %s (%d) ",status,str);
          return(0);
        }
      else
       if(debug) printf("Success from create record for %s\n",str);
    }
  else  /* key HCamp has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, HCamp, 0, &size);
      if (status != DB_SUCCESS)
        {
          cm_msg(MERROR, "frontend_init", "error during get_record_size (%d) for camp record",status);
          return 0;
        }
      printf("Size of camp saved structure: %d, size of camp record: %d\n", sizeof(CAMP_SETTINGS) ,size);
      if (sizeof(CAMP_SETTINGS) != size)
        {
          cm_msg(MINFO,"frontend_init","creating record (camp); mismatch between size of structure (%d) & record size (%d)", sizeof(CAMP_SETTINGS) ,size);
          /* create record */
          status = db_create_record(hDB,0,str,  strcomb(camp_settings_str));
          if (status != DB_SUCCESS)
           {
              cm_msg(MERROR,"frontend_init","Could not create %s record (%d)\n",str,status);
              return 0;
            }
          else
            if (debug)printf("Success from create record for %s\n",str);
        }
    }

  /* try again to get the key HCamp  */

  status = db_find_key(hDB,0, str, &HCamp);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot find Camp key for %s (%d)",str,status);
      return 0;
   }

  /* get the camp record */
  size = sizeof (camp_settings);
  if(debug) printf("HCamp = %d, size of record = %d \n",HCamp,size);

  status = db_get_record(hDB,HCamp, &camp_settings, &size, 0);
  if(status!= DB_SUCCESS)
  {
    cm_msg(MERROR, "frontend_init", "cannot retrieve %s record",str);
    return 0;
  }
  return HCamp;
}

