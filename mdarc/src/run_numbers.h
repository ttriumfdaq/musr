/* run_numbers.h

  $Log: run_numbers.h,v $
  Revision 1.6  2015/10/23 05:28:17  asnd
  Fix m20 overlap

  Revision 1.5  2015/05/01 23:01:17  suz
  retain MIN and MAX_M20 for now

  Revision 1.4  2015/04/29 21:35:58  suz
  update run number ranges for new beamlines

  Revision 1.3  2015/03/24 00:49:05  suz
  add ifndef

  Revision 1.1  2013/01/21 21:42:38  suz
  initial VMIC version to cvs

  Revision 1.2  2005/06/10 16:35:21  suz
  increment min value on all to avoid user confusion

  Revision 1.1  2003/07/29 18:25:19  suz
  original

*/
#ifndef rn_header
#define rn_header

//
// Define the valid run numbers
// Test runs common to all beamlines
#define MAX_TEST  34999  
#define MIN_TEST  30001 

// BNMR run numbers 
#define MAX_BNMR  44499  
#define MIN_BNMR  40001 
// BNQR  run numbers 
#define MIN_BNQR  45001 
#define MAX_BNQR  49999  

// MUSR beamlines
#define MIN_M20       1 //   Old beamline not used
#define MAX_M20    4999  
#define MIN_M15    5001 
#define MAX_M15    9999 
#define MIN_M9A   10001  // new beamline M9A
#define MAX_M9A   14999
#define MIN_M9B   15001 
#define MAX_M9B   19999 
#define MIN_M20C  20001 
#define MAX_M20C  24999 
#define MIN_M20D  25001
#define MAX_M20D  29999
#define MIN_DEV   35001
#define MAX_DEV   39999
#endif  // rn_header


