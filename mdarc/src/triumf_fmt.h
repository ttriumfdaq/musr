/*
 * Run Parameters block for TD_muSR data.

 $Log: triumf_fmt.h,v $
 Revision 1.3  2015/03/24 00:53:29  suz
 changes for VMIC; including long changed to int

 Revision 1.2  2000/10/11 04:10:50  midas
 added cvs log


*/
#ifndef triumf_fmt_header
#define  triumf_fmt_header

typedef struct {
	  short	     mrun;
	  short	     mhists;
	  short	     msclr;
	  short	     msupd;
	  int	     jtsc[18];
	  int	     jdsc[18];
	  short	     mmin;
	  short	     msec;
	  short	     mtnew[6];
	  short	     mtend[6];
	  short	     mlston[4];
	  short	     mcmcsc;
	  short	     mlocsc[2][6];
	  short	     mrsta;
	  int	     acqtsk;
	  char	     logfil[10];
	  short	     muic;
	  int	      nevtot;
	  short	     mhsts;
	  short	     mbins;
	  short	     mshft;
	  short	     mspare[7];
	  char	     title[40];
	  char	     sclbl[72];
	  char	     coment[144];
} TMF_F_HDR;

typedef struct {
	union {
	    struct {
		short	     ihist;
		short	     length;
		int	     nevtot;
		short	     ntpbin;
		int	     mask;
		short	     nt0;
		short	     nt1;
		short	     nt2;
		char	     htitl[10];
		char	     id[2];
		char	     fill[32];
		short	     head_bin;
	    } h;
	    short	     data[256];
	} u;
} TMF_H_RECD;


#endif // triumf_fmt_header
