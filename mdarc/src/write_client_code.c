/* This file included be frontend and mdarc 

CVS log information:
$Log: write_client_code.c,v $
Revision 1.2  2008/12/10 18:58:28  suz
add function clear_client_code

Revision 1.1  2008/04/17 17:49:16  suz
original to cvs

*/
INT write_client_code(INT bit, BOOL set, char * name)
{
     /* set/clear client error codes 

client_error_codes.h  
       bit pattern
   bit=0 clears all bits

  #define CAMP_LOG_ERR   4 /* CAMP logged variables 
  #define EPICS_LOG_ERR  5 /* EPICS logged variables 

other pgms might write these :
  #define EPICS_ERR 1   /* EPICS scan 
  #define CAMP_ERR  2    /* CAMP scan 
  #define RF_ERR    3    /* RF_CONFIG (parameters or bytecode.dat) 

  #define LIMIT_ERR    6 /* frontend 
 
  If set is true, bit is set
            false, bit is cleared
*/


  static DWORD bitpat=0;
  DWORD temp,shift,my_bit,size;
  INT status=0;
  char str[80];
  char my_name[30];
 
#ifdef MUSR
  return SUCCESS;
#endif

  strncpy(my_name,name,30);


  size=sizeof(bitpat);

  /* This code included by frontend as well as mdarc/mheader */
#ifdef FRONTEND
  sprintf(str,"/Equipment/%s/client flags/error code bitpat",equipment[FIFO].name);
#else
  sprintf(str,"/Equipment/%s/client flags/error code bitpat",eqp_name);
#endif

  printf("write_client_code: bitpat=0x%x\n",bitpat);

  status = db_get_value(hDB, 0, str, &bitpat, &size, TID_DWORD, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"write_client_code","error reading error code at \"%s\" (%d)",str,status);
      return;
    }
      
  temp=bitpat; /* remember present bit pattern */
  shift=1<<bit;
  my_bit = bitpat & shift;
      
      
  if(set)
    {
      printf("write_client_code: set bit %d of pattern 0x%x \n",bit,bitpat);
      if (!my_bit)
	{
	  printf("write_client_code: bit %d currently false (shift=0x%x)\n",bit,shift);
	  bitpat = bitpat | shift;
	}
    }
  else
    {
      printf("write_client_code: clear bit %d of pattern 0x%x \n",bit,bitpat);
      
      if(my_bit)
	{
	  printf("bit %d is currently true (shift=0x%x)\n",bit,shift);
	  shift = shift ^ 0xFFFFFFFF;
	  bitpat = bitpat & shift;
	}
    }


  if(bitpat != temp)
    { /* update if bitpat changed */
      status = db_set_value(hDB,0,str,&bitpat,size,1,TID_DWORD);
      if(status != DB_SUCCESS)
	cm_msg(MERROR,"write_client_code","Error writing code=0x%x to \"%s\"(%d)",bitpat,str,status);
      
      
#ifdef FRONTEND
      sprintf(str,"/Equipment/%s/client flags/error code from",equipment[FIFO].name); 
 
#else
      sprintf(str,"/Equipment/%s/client flags/error code from",eqp_name); 

#endif     

      status = db_set_value(hDB,0,str,my_name,sizeof(my_name),1,TID_STRING);
      if(status != DB_SUCCESS)
	cm_msg(MERROR,"write_client_code","Error writing my_name=%s to \"%s\"(%d)",my_name,str,status);
      
    }
  return status;
}

INT clear_client_code(char * name)
{
  /* clear client code bitpattern */

  DWORD bitpat;
  INT status;

  char my_name[30];
  char str1[80];
  char str2[80];
#ifdef MUSR
  return SUCCESS;
#endif

  strncpy(my_name,name,30);
  bitpat = 0;
  /* This code included by frontend as well as mdarc/mheader */
#ifdef FRONTEND
  sprintf(str1,"/Equipment/%s/client flags/error code bitpat",equipment[FIFO].name);
  sprintf(str2,"/Equipment/%s/client flags/error code from",equipment[FIFO].name);
#else
  sprintf(str1,"/Equipment/%s/client flags/error code bitpat",eqp_name);
  sprintf(str2,"/Equipment/%s/client flags/error code from",eqp_name);
#endif


  status = db_set_value(hDB,0,str1,&bitpat,sizeof(bitpat),1,TID_DWORD);
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"clear_client_code","Error writing code=0x%x to \"%s\"(%d)",bitpat,str1,status);
  status = db_set_value(hDB,0,str2,my_name,sizeof(my_name),1,TID_STRING);
  if(status != DB_SUCCESS)
    cm_msg(MERROR,"clear_client_code","Error writing  my_name=%s to \"%s\"(%d)",my_name,str2,status);


  /* temp for debugging */
  cm_msg(MINFO,"clear_client_code","Cleared client code");
  return SUCCESS;
}
