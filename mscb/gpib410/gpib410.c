/********************************************************************\

  Name:         mscb_gpib_410.c
  Created by:   Bryerton Shaw


  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol
                for the MSCB-GPIB for F410
 w 1 "INP:TRIG:MODE AUTO"
 w 1 ":PULS:TIM:PER 1E-3"

  $Id: gpib410.c,v 1.1 2015/04/22 04:03:42 asnd Exp $

  20140410  TW  changes for musr/camp: no assumption of trailing linefeed,
                and use CTL as a flag for commands that expect a reply
  20140414  TW  CTL flag of 3 instead of 1, avoid conflict with existing use
  20140425  TW  don't call enter() if send() timed-out

\********************************************************************/

#include <c8051F410.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "Time.h"
#include "SmaRTC.h"
#include "mscbemb.h"

extern bit FREEZE_MODE;
extern bit DEBUG_MODE;

char code node_name[] = "MSCB_410_GPIB";

/* declare number of sub-addresses to framework */
unsigned char idata _n_sub_addr = 1;
bit output_flag, ctl_flag, loop_flag, control_flag;
char xdata mydate[24];

unsigned long int xdata t;

/*---- Define variable parameters returned to CMD_GET_INFO command ----*/
struct {
   char output[64];
   char input[64];
   char control;
   char srq;
   char ctl;
   char gpib_adr;
   char date[24];
   unsigned long mytime;
} xdata user_data;

MSCB_INFO_VAR code vars[] = {
   1,  UNIT_ASCII,  0, 0, MSCBF_DATALESS, "GPIB",                       0, // 0
   64, UNIT_STRING, 0, 0,              0, "Output",  &user_data.output[0], // 1
   64, UNIT_STRING, 0, 0,              0, "Input",    &user_data.input[0], // 2
   1,  UNIT_BYTE,   0, 0,              0, "Control",   &user_data.control, // 3
   1,  UNIT_BYTE,   0, 0,              0, "SRQ",           &user_data.srq, // 4
   1,  UNIT_BYTE,   0, 0,              0, "CTL",           &user_data.ctl, // 5
   1,  UNIT_BYTE,   0, 0,              0, "GPIB Adr", &user_data.gpib_adr, // 6
   24, UNIT_STRING, 0, 0,              0, "Date",        &user_data.date[0], // 7
   4,  UNIT_BYTE,   0, 0,   MSCBF_HIDDEN, "Time" ,        &user_data.mytime, // 8
   0
};

MSCB_INFO_VAR *variables = vars;


#define GPIB_DATA P2

/* GPIB control/status bits DB24 */
sbit GPIB_REN  = P1 ^ 0;          // REMOTE ENABLE, 	Pin 9
sbit GPIB_EOI  = P1 ^ 1;          // END-OR-IDENTIFY, 	Pin 10
sbit GPIB_DAV  = P1 ^ 2;          // DATA VALID,		Pin 11
sbit GPIB_NRFD = P1 ^ 3;          // NOT READY FOR DATA,Pin 12
sbit GPIB_NDAC = P1 ^ 4;          // NOT DATA ACCEPTED,	Pin 13
sbit GPIB_IFC  = P1 ^ 5;          // INTERFACE CLEAR,	Pin 14
sbit GPIB_SRQ  = P1 ^ 6;          // SERVICE REQUEST,	Pin 15
sbit GPIB_ATN  = P1 ^ 7;          // ATTENTION,			Pin 16

sbit BUF_CLE   = P0 ^ 0;   // P0.
sbit BUF_DATAE = P0 ^ 3;   // P0.3


/********************************************************************\

  Application specific init and input/output routines

\********************************************************************/

void user_write(unsigned char index) reentrant;
unsigned char send(unsigned char adr, char *str);
unsigned char send_byte(unsigned char b);

/*---- User init function ------------------------------------------*/

extern SYS_INFO sys_info;

void user_init(unsigned char init)
{

//	P0MDIN = 0xFF;						// default 0xFF all digital pins
// 	P1MDIN = 0xFF;
//	P2MDIN = 0xFF;

    P0MDOUT = 0x50;					// Default OD 485TX, TXD
//	P1MDOUT = 0x00;					// OD
//  P2MDOUT = 0xFF;					// OD
	
  P0 = 0x04;    // default 0xFF
//	P1 = 0xFF;
//	P2 = 0xFF;

  /* set initial state of lines */
  GPIB_DATA = 0xFF;
  GPIB_EOI = 1;
  GPIB_DAV = 1;
  GPIB_NRFD = 1;
  GPIB_NDAC = 1;
  GPIB_IFC = 1;
  GPIB_SRQ = 1;
  GPIB_ATN = 1;
  GPIB_REN = 1;

  BUF_CLE = 0;  
  BUF_DATAE = 0;


  /* initialize GPIB */
  GPIB_IFC = 0;
  delay_ms(1);
  GPIB_IFC = 1;

  GPIB_ATN = 0;
  send_byte(0x14);             // DCL
  GPIB_ATN = 1;

   /* initial nonzero EEPROM values */
   if (init) {
		/* default most settings to zero */
      memset(&user_data, 0, sizeof(user_data));

      user_data.gpib_adr = 10;
		user_data.control = 0;

	   /* set default group address */
	   if (sys_info.group_addr == 0xFFFF)
	      sys_info.group_addr = 0xFF00;

	   if (sys_info.node_addr == 0xFFFF) 
	   	  sys_info.node_addr = 0x01;
   }

   user_data.ctl = 0x0;
   
//   SmaRTCInit();

 
}

/*---- User write function -----------------------------------------*/

#pragma NOAREGS 


void user_write(unsigned char index) reentrant
{
   if (index == 5)
      ctl_flag = 1;
   if (index == 1)
      output_flag = 1;
   if (index == 3)
      control_flag = 1;
//   if (index == 7)
//      SmaRTCSetTime(user_data.mytime);

		return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
   if (index);
   return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
   /* echo input data */
   data_out[0] = data_in[0];
   data_out[1] = data_in[1];
   return 2;
}

/*---- Functions for GPIB port -------------------------------------*/

unsigned char send_byte(unsigned char b)
{
   unsigned int i;

   //   yield();

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++) {
       delay_us(2);
       if (GPIB_NRFD == 1)
         break;
   }

   if (GPIB_NRFD == 0)
      return 0;

   GPIB_DATA = ~b;              // negate
   delay_us(30);                 // let signals settle
   GPIB_DAV = 0;

   /* wait for NDAC go high */
   for (i = 0; i < 1000; i++) {
      delay_us(30);
      if (GPIB_NDAC == 1)
         break;
   }

   if (GPIB_NDAC == 0) {
      GPIB_DAV = 1;
      GPIB_DATA = 0xFF;
      return 0;                 // timeout
   }

   GPIB_DAV = 1;
   GPIB_DATA = 0xFF;            // prepare for input

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++) {
      delay_us(2);
      if (GPIB_NRFD == 1)
         break;
   }
   
   if (GPIB_NRFD == 0)
      return 0;

   return 1;
}

/*------------------------------------------------------------------*/

unsigned char send(unsigned char adr, char *str)
{
   unsigned char i;
   char s;


  /*---- address cycle ----*/

   GPIB_REN = 0;
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | adr);       // listen device
   send_byte(0x40 | 21);        // talk 21
   GPIB_ATN = 1;                // remove attention
 
  /*---- data cycles ----*/

//   len = strlen(str);
   for (i = 0; str[i] > 0; i++) {
      delay_us(10);
      s = send_byte(str[i]);
      if (s == 0) {
        GPIB_REN = 1;
        return 0;
      }
   }


   GPIB_EOI = 0;
   // 20140410  TW  don't append LF character
   // send_byte(0x0A);             // NL
   GPIB_EOI = 1;
   GPIB_REN = 1;

   return i;
}

/*------------------------------------------------------------------*/

unsigned char enter(unsigned char adr, char *str, unsigned char maxlen)
{
   unsigned long t;
   unsigned char i, flag;
   unsigned int j;

  /*---- address cycle ----*/

   GPIB_REN = 0;                // Remote Enable 
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | 21);        // listen 21
   send_byte(0x40 | adr);       // talk device
   GPIB_ATN = 1;                // remove attention

  /*---- data cycles ----*/

   GPIB_NDAC = 0;               // init NDAC line

//   memset(str, 0, maxlen);
 for(i=0;i<maxlen;i++) str[i] = 0;

   for (i = 0; i < maxlen; i++) {
      yield();

      GPIB_NRFD = 1;            // singal ready for data

      /* wait 1s for DAV go low */
      t = time();
      do {
         if (GPIB_DAV == 0)
            break;

         yield();

      } while (time() - t < 100);

      if (GPIB_DAV == 1) {
         GPIB_NDAC = 1;
         GPIB_NRFD = 1;
         GPIB_REN = 1;
        return 0;              // timeout
      }

//      led_blink(1, 1, 100);     // singal data received

      GPIB_NRFD = 0;            // signal busy

      str[i] = ~GPIB_DATA;      // read negated data

      flag = GPIB_EOI;          // read EOI flag

      GPIB_NDAC = 1;            // signal acknowledge

      /* wait for DAV go high */
      for (j = 0; j < 1000; j++) {
        delay_us(1);
         if (GPIB_DAV == 1)
            break;
      }
      GPIB_NDAC = 0;            // remove acknowledge

      if (flag == 0)            // stop if end of data
         break;
   }

   GPIB_NDAC = 1;               // release handshake lines
   GPIB_NRFD = 1;

   /* stop talker */
   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   GPIB_ATN = 1;                // remove attention
   GPIB_REN = 1;

   return i;
}

/*---- User loop function ------------------------------------------*/

void user_loop(void)
{
  unsigned char question=0;
//  unsigned long int ltime;
  unsigned char send_count;
  unsigned int output_len;
  
  //	watchdog_refresh(0);
   if (output_flag || loop_flag) {
     output_flag = 0;
     loop_flag = 0;

     /* send buffer */
     output_len = strlen( user_data.output );
     send_count = send(user_data.gpib_adr, user_data.output);
 //    question = 0;
 //    for (i = 0; user_data.output[i] > 0; i++) {
 //      if (user_data.output[i] == '?') question = 1;
 //    }

//     if (question) {
      /* receive buffer */
      // 20140410  TW  use the CTL variable as a flag indicating reply expected
      if( (send_count == output_len) && (user_data.ctl == 0x03) ) // 0x03 so as not to conflict with other uses during testing
      {
         enter(user_data.gpib_adr, user_data.input, sizeof(user_data.input));
      }
      led_blink(1, 2, 50);

      // 20140410  TW  don't strip LF character
      /* stip NL */
//       if (strlen(user_data.input) > 0 &&
//           user_data.input[strlen(user_data.input) - 1] == 10)
//          user_data.input[strlen(user_data.input) - 1] = 0;
//    }
   }

   if (control_flag) {
      control_flag = 0;

      GPIB_ATN = 0;             // assert attention
      send_byte(0x3F);          // unlisten
      send_byte(0x5F);          // untalk
      send_byte(0x20 | user_data.gpib_adr);     // listen device
      send_byte(0x40 | 21);     // talk 21
      send_byte(user_data.control);     // send control
      send_byte(0x3F);          // unlisten
      send_byte(0x5F);          // untalk
      GPIB_ATN = 1;             // remove attention
   }

/* Loop for Voltage measurement */

   // 20140410  TW  removed for musr/camp
//    if (user_data.ctl == 0x01) {
//      // Manual range 100V@1mV
//      sprintf(user_data.output, "MEAS:VOLT:DC? 100,0.001");
//      loop_flag = 1;
//      led_blink(0, 1, 100);     // singal data received
//      delay_ms(250);
//    }
//    if (user_data.ctl == 0x02) {
//      // Manual range 0.1V @0.01mV
//      sprintf(user_data.output, "MEAS:VOLT:DC? 0.1,.00001");
//      loop_flag = 1;
//    }

//     ltime = SmaRTCRead();
//     user_data.mytime = ltime;
//     ascTime(&mydate[0], ltime);
//     sprintf(user_data.date, "%s", mydate);
     
//     delay_ms(250);
     led_blink(1, 1, 50);     // singal data received
}
