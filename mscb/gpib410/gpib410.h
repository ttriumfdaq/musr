/********************************************************************\

  Name:     GPIB410.h
  $Id: gpib410.h,v 1.1 2015/04/22 04:03:42 asnd Exp $

\********************************************************************/

#ifndef _GPIB_H
#define _GPIB_H

#include "../../common/mscbemb.h"

// Global definition
// Global ON / OFF definition
#define ON     1
#define DONE   1
#define SET    1
#define OFF    0
#define CLEAR  0

#define LED_GREEN 1
#define LED_RED   0

#define N_LAKES   8
//
//--- MSCB structure
typedef struct {
   char output[64];
   char input[64];
   char control;
   char srq;
   char ctl;
   char gpib_adr;
   float temp[N_LAKES];
   char date[24];
   unsigned long mytime;
} MSCB_USER_DATA;

unsigned char send_byte(unsigned char b);
unsigned char send(unsigned char adr, char *str);
unsigned char enter(unsigned char adr, char *str, unsigned char maxlen);

#endif // _GPIB_H
