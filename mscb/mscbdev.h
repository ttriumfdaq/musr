/********************************************************************\

  Name:         mscbdev.h
  Created by:   Stefan Ritt

  Contents:     Device driver function declarations for MSCB device

  $Id: mscbdev.h,v 1.1 2015/03/18 20:28:26 suz Exp $

\********************************************************************/

INT mscbdev(INT cmd, ...);
