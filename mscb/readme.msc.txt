
build msc

  $ cd mscb
  $ make msc

upload gpib410 firmware

  $ ./msc -d mscb506.triumf.ca
  > addr 1                         (attach to address 1 on mscb bus)
  node1(0x1)> upload gpib410.hex   (upload firmware)

other useful msc commands

  $ ./msc -d mscb506.triumf.ca
  > help
  > addr 1                         (attach to address 1 on mscb bus)
  node1(0x1)> read                 (show gpib410 variables)
  node1(0x1)> write 6 12           (set gpib address)
  node1(0x1)> write 1 "*IDN?"      (send command)
  node1(0x1)> read 2               (show readback)
