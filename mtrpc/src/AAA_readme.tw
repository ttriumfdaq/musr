
20140327  TW  summary of changes to date
	      changes to clnt_perr.c,clnt_udp.c,create_xid.c,makefile,key_call.c for compilation
	      changes to rpc_thread.h,rpc_thread.c for clarity
	      changes to svc.c,svc_run.c for functionality and improvement (real changes)

changes by file

  - clnt_perr.c
    - clnt_sperror, clnt_spcreateerror
      - don't use the return value of strerror_r - it's not the same 
	in all implementations

  - clnt_udp.c
    - include <asm/types.h> for __u32

  - create_xid.c
    - instead of calling __libc_lock_define_initialized/__libc_lock_lock/__libc_lock_unlock,
      use a local pthreads mutex
    - don't define _IO_MTSAFE_IO

  - makefile
    - remove all .c depends on .h lines
    - add -DMTRPC_POLL_TIMEOUT_MS to LOCAL_CFLAGS
      (MTRPC_POLL_TIMEOUT_MS is used in svc_run.c and svc.c)

  - key_call.c
    - same changes as create_xid.c

  - rpc_thread.c
    - comment-out unused parts to avoid confusion
      - mtrpc does not maintain an rpc_thread_variables structure per thread, 
        it maintains just one for all, initialized by __rpc_thread_variables.
	This is worrying because multiple threads may be writing to this area.
	We have to trust that it's safe for our
	architecture (single-threaded clients, and
	one server thread that manages fdset/pollfd/xports variables for its
	simple child threads).  The author of mtrpc seems to have been aiming
	at the same architecture.  But what about variables like rpc_createerr_s
	and auth* are they okay?

  - rpc_thread.h
    - as per rpc_thread.c

  - svc.c
    - svc_getreq_poll
      - nested loop was reusing the outer loop's counter variable "i"
	this was causing the outer loop to exit early in cases when execution
	got to the inner loop. probably benign, but did not seem to be
	intentional, or desired
      - minor issue with loop counter maximum changing within loop
    - svc_getreq_common
      - poll now takes compile-time timeout variable as per svc_run
      - MOST IMPORTANT CHANGE TO MTRPC!!:
	- poll returning -1, with errno=EINTR was previously exiting
	  without cleaning up (was not calling SVC_DESTROY(xprt))
	  - this left sockets open and memory leaks due to xprt un-free'd
	    (see svctcp_destroy for what is done by SVC_DESTROY)
        - according to the internets, the correct this to do is 
	  ignore an EINTR and repeat the poll()

  - svc_run.c
    - realloc my_pollfd instead of malloc/free on each pass
      - this is how the glibc version works, and I can't see any reason
	why the multithreaded situation would need this
      - this changed can be reversed, it just seemed ugly as it was
    - __poll now takes compile-time timeout variable MTRPC_POLL_TIMEOUT_MS
      instead of one second timeout
      - glibc version uses infinite timeout (-1), and this works well in testing
