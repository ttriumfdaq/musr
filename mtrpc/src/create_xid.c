/* Copyright (c) 1998, 2000 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   Contributed by Thorsten Kukuk <kukuk@vt.uni-paderborn.de>, 1998.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

// #define _LIBC
//tw #define _IO_MTSAFE_IO // 20140312 TW causes problems in libc-lock

#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
//#include <bits/libc-lock.h>
#include "libc_lock_replacement.h"
#include <rpc/rpc.h>

/* The RPC code is not threadsafe, but new code should be threadsafe. */

//tw __libc_lock_define_initialized(static, createxid_lock)
pthread_mutex_t	createxid_lock = PTHREAD_MUTEX_INITIALIZER; //tw
static int is_initialized;
static struct drand48_data __rpc_lrand48_data;

unsigned long _create_xid(void)
{
	long res;

//tw	__libc_lock_lock(createxid_lock);
	pthread_mutex_lock( &createxid_lock ); //tw

	if (!is_initialized)
	{
		struct timeval now;

		gettimeofday(&now, (struct timezone *) 0);
		srand48_r(now.tv_sec ^ now.tv_usec, &__rpc_lrand48_data);
		is_initialized = 1;
	}

	lrand48_r(&__rpc_lrand48_data, &res);

//tw	__libc_lock_unlock(createxid_lock);
	pthread_mutex_unlock( &createxid_lock ); //tw

	return res;
}
