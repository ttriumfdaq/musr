#ifndef LIBC_LOCK_REPLACEMENT
#define LIBC_LOCK_REPLACEMENT

// Replacement for that isn't in new Ubuntu
//#include <bits/libc-lock.h>

/* Define once control variable.  */
#if PTHREAD_ONCE_INIT == 0
/* Special case for static variables where we can avoid the initialization
   if it is zero.  */
# define __libc_once_define(CLASS, NAME) \
  CLASS pthread_once_t NAME
#else
# define __libc_once_define(CLASS, NAME) \
  CLASS pthread_once_t NAME = PTHREAD_ONCE_INIT
#endif

/* Call handler iff the first call.  */
#define __libc_once(ONCE_CONTROL, INIT_FUNCTION) \
  do {                                                                        \
    if (pthread_once != NULL)                                               \
      pthread_once (&(ONCE_CONTROL), (INIT_FUNCTION));                      \
    else if ((ONCE_CONTROL) == PTHREAD_ONCE_INIT) {                           \
      INIT_FUNCTION ();                                                       \
      (ONCE_CONTROL) = 2;                                                     \
    }                                                                         \
  } while (0)

#endif
