/*
 * Sun RPC is a product of Sun Microsystems, Inc. and is provided for
 * unrestricted use provided that this legend is included on all tape
 * media and as a part of the software program in whole or part.  Users
 * may copy or modify Sun RPC without charge, but are not authorized
 * to license or distribute it to anyone else except as part of a product or
 * program developed by the user.
 *
 * SUN RPC IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING THE
 * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 *
 * Sun RPC is provided with no support and without any obligation on the
 * part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 *
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY SUN RPC
 * OR ANY PART THEREOF.
 *
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even if
 * Sun has been advised of the possibility of such damages.
 *
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */
/*
 * This is the rpc server side idle loop
 * Wait for input, call server program.
 */

#include <errno.h>
#include <unistd.h>
#include <libintl.h>
#include <sys/poll.h>
#include <rpc/rpc.h>

#include "newdef.h"
#include "errlog.h"

extern char *gpcPrgName;

/* This function can be used as a signal handler to terminate the
   server loop.  */
void svc_exit(void)
{
	free(svc_pollfd);
	svc_pollfd = NULL;
	svc_max_pollfd = 0;
}

void print_pollfds( char* msg, struct pollfd* ppollfds, int pollfds_num )
{
    int i;
    errlog(IM_LOG, "%s:%s: print pollfd's (max:%d):\n", gpcPrgName, msg, pollfds_num);
    for (i = 0; i < pollfds_num; ++i)
    {
	struct pollfd* ppollfd = &ppollfds[i];

	//if( ppollfd->fd != -1 )
	{
	    errlog(IM_LOG, "%s:%s:   pollfd %d: fd:%d events:%X revents:%X\n", gpcPrgName, msg, i, ppollfd->fd, ppollfd->events, ppollfd->revents);
	}
    }
}

void svc_run(void)
{
	int i;
	struct pollfd *my_pollfd = NULL;
	int my_pollfd_size = 0;

#ifdef TRACE_ON_TW
	errlog(IM_LOG, "%s:%s: Private\n", gpcPrgName, __FUNCTION__);
#endif

	for (;;)
	{
		int my_pollfd_size_new = svc_max_pollfd;

		if (svc_max_pollfd == 0 && svc_pollfd == NULL)
			return;

		//  20140321  TW  realloc rather than malloc, as other svc_run implementations do

		// my_pollfd = malloc(sizeof(struct pollfd) * svc_max_pollfd);

		if( my_pollfd_size != my_pollfd_size_new )
		{
		    my_pollfd = realloc( my_pollfd, sizeof( struct pollfd ) * my_pollfd_size_new );
		    my_pollfd_size = my_pollfd_size_new;
		}

		for (i = 0; i < my_pollfd_size; ++i)
		{
			my_pollfd[i].fd = svc_pollfd[i].fd;
			my_pollfd[i].events = svc_pollfd[i].events;
			my_pollfd[i].revents = 0;
		}

/* #ifdef TRACE_ON_TW */
/* 		print_pollfds( "svc_run (before poll)", my_pollfd, my_pollfd_size ); */
/* #endif */

		// mtrpc uses 1 sec timeout, glibc sunrpc uses infinite
#ifndef MTRPC_POLL_TIMEOUT_MS
#define MTRPC_POLL_TIMEOUT_MS 1000
#endif // MTRPC_POLL_TIMEOUT_MS

		switch (i = poll(my_pollfd, my_pollfd_size, MTRPC_POLL_TIMEOUT_MS))
		{
			case -1:
			        // free(my_pollfd);
				if (errno == EINTR)
					continue;
				perror(_("svc_run: - poll failed"));
				goto return_;
			case 0:
			        // free(my_pollfd);
				continue;
			default:
				svc_getreq_poll(my_pollfd, i);
				// free(my_pollfd);
		}
	}

return_:
	if( my_pollfd ) free( my_pollfd );
}
