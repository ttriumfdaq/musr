#!/usr/bin/perl -w
# above is magic first line to invoke perl
# or for debug


# check 
#   that beamline and user are identical
#   that we have a valid beamline for this host
# e.g. m15 for midm15 
#  or dev is allowed on any host
#
my $username =  $ENV{'USER'}; 
my $beamline = $ENV{'BEAMLINE'}; 
my $hostname = $ENV{'HOSTNAME'};
my $expt = $ENV{'MIDAS_EXPT_NAME'};

print "username: $username \n";
print "beamline: $beamline \n";
print "host: $hostname \n";
print "experiment: $expt \n";

if ( $beamline eq $username)
{ print " beamline  $beamline and username $username are identical\n";}
else
{ print " beamline  $beamline and username $username should be identical\n";
    exit 5;
}

if ( $beamline eq $expt)
{ print " beamline  $beamline and experiment name $expt are identical\n";}
else
{ print " beamline  $beamline and experiment name  $expt should be identical\n";
    exit 5;
}


if ( $beamline =~/dev/ ||  $hostname =~/$beamline/i)
{ print "Valid beamline  $beamline for $hostname\n";}
else
{ print "Invalid beamline $beamline for $hostname\n";
    exit 5;
}
exit 1;
