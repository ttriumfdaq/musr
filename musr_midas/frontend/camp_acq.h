/* camp_acq.h

   This version works with Ted's new CAMP

 $Log: camp_acq.h,v $
 Revision 1.2  2015/03/18 01:11:03  suz
 These files work with Ted's new Camp (comment added)

 Revision 1.1  2015/03/18 00:43:39  suz
 new to cvs; versions for VMIC musr

 Revision 1.1  2013/01/21 20:58:10  suz
 initial VMIC version

 Revision 1.3  2003/09/30 19:50:27  suz
 added support for imusr

 Revision 1.2  2003/08/14 20:25:09  suz
 add support for POL's Camp DAC device

 Revision 1.1  2003/05/01 21:30:52  suz
 initial bnmr version

 Revision 1.1  2002/06/05 00:51:16  suz
 prototypes and structure

*/

#ifndef _CAMP_ACQ_HEADER
#define  _CAMP_ACQ_HEADER
/* for Camp Sweep Device */
typedef struct {
  char serverName[LEN_NODENAME+1];
#ifdef IMUSR
  char SweepDevice[20];
  char InsPath[80];
#else
  char SweepDevice[10];
  char InsPath[36];
#endif
  char InsType[36];
  char IfMod[36];
  char setPath[128];
  char DevDepPath[128];
#ifdef IMUSR
  char	 units[20];
#else	
   char units[10];
#endif	
 float maximum_value;
  float minimum_value;
  INT   conversion_factor; /* multiply sweep value by this factor -> integer in CYCL bank */ 
} CAMP_PARAMS;
 
 /* prototype */
INT init_sweep_device( CAMP_PARAMS camp_params  );
#ifdef IMUSR
INT camp_initPath( CAMP_PARAMS camp_params, BOOL *camp_active);
#else
INT camp_initPath( CAMP_PARAMS camp_params);
#endif
INT set_sweep_device( float setpoint, CAMP_PARAMS camp_params );
INT read_sweep_device ( float* setpoint, CAMP_PARAMS camp_params );
INT camp_end( void );
INT camp_readRampStatusMG (float* rampStatus, CAMP_PARAMS camp_params);
void sendMsg(char* Name);
INT camp_setVal( float setpoint,  CAMP_PARAMS camp_params );
INT camp_getVal( float* setpoint, CAMP_PARAMS camp_params );
INT camp_init( CAMP_PARAMS camp_params );
INT camp_readDevDepParam_MG (float *param, CAMP_PARAMS camp_params);
/* globals */
BOOL dc; /* TRUE = debug */

#endif // _CAMP_ACQ_HEADER




