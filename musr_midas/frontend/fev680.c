/********************************************************************\
  Name:         fev680.c

  Created by:   Suzannah Daviel, TRIUMF

       VMIC Version

  Based on :   1. fev680.c    MUSR VxWorks frontend
               2. frontend.c  by Stefan Ritt
               3. v680.c      by Pierre Amaudruz

  Contents:     Midas-type  frontend for MUSR 

  $Log: fev680.c,v $
  Revision 1.38  2015/05/08 20:17:01  suz
  create record only if record size does not match

  Revision 1.37  2015/05/08 00:32:59  suz
  add more checks on tdc initialization

  Revision 1.36  2015/03/26 22:07:48  suz
  correct error char* rather than BYTE* for VMIC

  Revision 1.35  2015/03/18 01:05:43  suz
  get rid of some warnings with casts

  Revision 1.34  2015/03/18 00:35:45  suz
  change SYNC to TR_SYNC

  Revision 1.33  2015/03/18 00:16:41  suz
  VMIC version of TD-MUSR frontend. Runs on VMIC

  Revision 1.32  2004/11/10 18:23:08  suz
  send an event on PAUSE and RESUME transitions; mdarc now uses suppress_save_temporarily flag if run is being killed

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "mcstd.h" 
// For VMIC processor
#include "vmicvme.h"

#include "v680.h"
#include "experim.h"
#include "musr_common.h" /* common with imusr (femusr) */
#include "sis3803.h"
#include "musrvmeio.h"
#include "trGGL.h" 
#include "musr_common_subs.h"
/* make frontend functions callable from the C framework */
#ifdef __cplusplus
ls extern "C" { }
#endif

extern INT debug;   // use command line parameter "-d" for debug
INT run_state; /* run state */

// VMIC
MVME_INTERFACE *myvme;

/*-- Globals -------------------------------------------------------*/
INT poll_val  = 10; /* .1% */


/* constants and default values */
#define CLEAR_ALL       0xffff
#define FOUR_BYTES_PER_BIN      4   
#define TWO_BYTES_PER_BIN       2
#define DEF_NBINS      4096
#define MAX_CHANNELS     8
#define CHANNEL_FIELD  (1<<MAX_CHANNELS)-1
#define DATA_FIELD       14
#define DATA_SHIFT        0

#define POLL_EQUIPMENTS   5000 /* every 5s */
#define POLL_HISTO       300000 /* every 5m */


int sep, next;
int ext_polling_time = 500;
int Address, flatmode;


char histo_order[128];  

/* internal */
typedef struct {
  int shift;
  int bit_pattern;
  int nbins;
  int bytes_per_bin;
  int ioreg_bit_pattern;
} INFO;
INFO    info;

int     data_field, ovfl_mask, data_mask, total_byte, total_bin, memory_size, data_shift;
int     channel_mask, max_channels; 
char display_string[70]=" ";

/* Globals */
INT     pol_time;
DWORD     Total_Bin; /* memory size available in bins */

/*  note - Total_Bin will be calculated from  Total_Byte ( assigned by  call to
    memFindMax()  )*/

/* larger histograms are stored internally (i.e. by this program) than are sent out.
   Those sent out are (at present) limited by max_event_size rather than Total_Byte 
   max_his_byte is the smaller of Total_Byte and max_event_size
*/



DWORD     max_his_byte; /* maximum bytes for histograms (external use) */

INT start_poll;

/* Channel Lookup Table */
static int channel_table[16] = { 0 , 1 , 2 , 3
				 , 4 , 5 , 6 , 7
				 ,-1 ,-1 ,-1 ,-1
				 ,-1 ,-1 ,-1 ,-1 };
int  channel;  

INT HM_word_offset[16]; /* needed for MUSR counter mapping */

static BYTE  *pbchn, *pbl;
static int   *pch_table;

/* VMEIO */
/*static DWORD  *vmeio_wrtplse, *vmeio_wrtlatch ;*/

/* memory */
WORD  *pmemBase2 = NULL;
DWORD *pmemBase4 = NULL;

static WORD *pdataBase;
static WORD *pData16_H, *pData16_M, *pData16_L;
static DWORD *pData32;

/* Counters */
static DWORD  svhits[4+16];
static DWORD    hits[4+16];
/* Hit Counters */
static DWORD  *hit_hit, *hit_mlt, *hit_dbl, *hit_mis;
static DWORD  *hit_ovf, *hit_ivc;
DWORD   *hitsAddress ; 
static WORD    dataH, dataM, dataL, hit, ctl, dbl, mis;  // unsigned short int


static WORD tempChan[16] ; // count number of hits for each REAL channel

/* end of stuff from V680.c */

DIAG_SETTINGS_STR(diag_settings_str);  

/* handles for BOR hotlinks so they can be closed at EOR */
HNDLE hDelta,hDelta1,hDelta2;

float This_Version=2.0; /* VMIC  later match SVN version perhaps */

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fev680";

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period =0;

/* maximum event size produced by this frontend
   must be less than MAX_EVENT_SIZE 0x400000 4MB (midas.h)*/

//INT max_event_size = 1050000*2; // > 1 Mbyte   
INT max_event_size = 1048576*4; // == 4194304 == 4 Mbyte   

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;


/* buffer size to hold events
   event_buffer_size must be >= 2*max_event_size (checked in mfe.c  */

//INT event_buffer_size = 10*1050000*2; 
INT event_buffer_size = 8*1048576*2*2; 

//DWORD Total_Byte =  1048576*2 ;   /* memory size available in bytes  (fits into max_event_size) */
//DWORD Total_Byte =  4194048 ;   /* memory size available in bytes  (fits into max_event_size) */
DWORD Total_Byte = 1048576*4; /* if it goes all the way up, it crashes FE, but any smaller cuts usable size by half */

/* Total_Byte < max_event_size with at least 120 bytes to spare. Needed now without fragmented events */


/* scaler global */
DWORD scaler_counts[MAX_SCALER];

INT gbl_run_number; 



/*-- TD Function declarations -----------------------------------------*/
void  hot_set_clock_hang (INT, INT, void * );
void  hot_restart (INT, INT, void * );

void  hot_clear_clock_hang (INT, INT, void * );
void  hot_dump_regs (INT, INT, void * );
void  hot_zero_run (INT, INT, void * );
void  hot_zero_some (INT, INT, void * );
void  hot_zero_scaler (INT, INT, void * );
void  hot_zero_scaler_some (INT, INT, void * );    
INT   create_records(void);
INT   v680_init(BOOL flag);

void  v680_mem_clear(int clear_pat);
void  v680_start(void);
void  v680_stop(void);
void  v680_restart(void);

void  v680_display(int onoff);
void  v680_update(void);

INT   mem_cleanup(void);
INT   mem_init(void);
void scaler_setup_test(void);
INT scaler_clear_rates(void);
void vmeio_on(void);
void vmeio_off(void);
INT set_params(void);       /* fills info record from v680 odb area */
void set_default_params(); 
INT setup_histo_index(void);
void hot_delta (HNDLE hDB, HNDLE hDelta ,void *info);
void hot_delta1 (HNDLE hDB, HNDLE hDelta1 ,void *info);
void hot_delta2 (HNDLE hDB, HNDLE hDelta2 ,void *info);
    
#ifdef DEBUG    /* define for major histogram debugging only */
void  v680_mem_dbg(int i);
void hm_dump(INT pbins);
void hm_write(INT data);
INT hm_check(INT data);
void hm_dump4(INT pbins);
void hm_write4(INT data);
INT hm_check4(INT data);
void hm_dump2(INT pbins);
void hm_write2(INT data);
INT hm_check2(INT data);
#endif

/*-- Equipment list ------------------------------------------------*/

#undef USE_INT
EQUIPMENT equipment[] = {
  /* note: this trigger event IS NEEDED to run the v680  polling loop */
  { "MUSR_TD_Acq",            /* equipment name */
    { 1, 0,                 /* event ID, trigger mask    added extra bracket SD */
    "SYSTEM",             /* event buffer */
#ifdef USE_INT
    EQ_INTERRUPT,         /* equipment type */
#else
    EQ_POLLED,            /* equipment type */
#endif
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* must be enabled or  */
    RO_RUNNING,           /* read only when running */
    100,                  /* poll for 100ms */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub event */
    0,                    /* don't log history */
      "", "", "",},       // added extra bracket SD 
    trigger_read,   /* readout routine */
    NULL, NULL,NULL       /* keep null */
  },

  { "Histo",             /* equipment name */
    {2, 0,                 /* event ID, trigger mask */
     "SYSTEM",             /* event buffer */
     EQ_MANUAL_TRIG,    /* manual trigger, NOT fragmented  */
     0,                    /* event source */
     "MIDAS",               /* format */
     TRUE,                 /* enabled */
     RO_RUNNING | RO_EOR | 
     RO_PAUSE | RO_RESUME, /* read when running and on end run,pause,resume transition */
     0,                    /* not polled */
     0,                    /* stop run after this event limit */
     0,                    /* number of sub event */
     0,                    /* log history */
     "", "", "",},
    histo_read,     /* readout routine */
    NULL,NULL,NULL       /* keep null */
  },

  { "Scaler",             /* equipment name */
    {3, 0,                 /* event ID, trigger mask */
     "SYSTEM",             /* event buffer */
    /*"",         */          /* Don't send data */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",               /* format */
    TRUE,                 /* enabled */
    //FALSE,                 /* disabled */
    RO_RUNNING |
    RO_TRANSITIONS |      /* read when running and on transitions */
    RO_ODB,               /* and update ODB */ 
    POLL_EQUIPMENTS,      /* read every so often */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub event */
    0,                    /* log history */
     "", "", "",},
    scaler_read,    /* readout routine */
    NULL,NULL,NULL       /* keep null */
  },

 { "Diag",            /* equipment name */
   {4, 0,                 /* event ID, trigger mask */
    "",             /* don't send out in event buffer */
    EQ_PERIODIC,            /* equipment type */
    0,                    /* event source */
    "MIDAS",               /* format */
    TRUE,                 /* enabled */
    RO_RUNNING | RO_ODB,  /* read only when running  and update ODB */ 
    POLL_EQUIPMENTS,      /* read out every so often */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub event */
    0,                    /* don't log history */
    "", "", "",},
    diag_read,            /* readout routine */
    NULL,NULL,NULL        /* keep null */
  },

 { "Rscal",            /* equipment name */
   {5, 0,                 /* event ID, trigger mask */
    "",             /* don't send out in event buffer */
    EQ_PERIODIC,            /* equipment type */
    0,                    /* event source */
    "MIDAS",               /* format */
    TRUE,                 /* enabled */
    RO_ALWAYS | RO_ODB,   /* read always so updates odb when run paused */ 
    POLL_EQUIPMENTS,      /* read out every so often */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub event */
    0,                    /* don't log history */
    "", "", "",},
    scaler_rates,            /* readout routine */
    NULL,NULL,NULL        /* keep null */
  },


  { "" }
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
 Callback routines for system transitions
                                                                     
These routines are called whenever a system transition like start/
stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.
  
  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
 
  int vmic_status;
  char str[128];

  printf("\n");
  printf("Front End code for TD-MUSR (fev680 version %.2f for VMIC ) now running ...\n",This_Version);

  //dd=1;
  // printf("Debugging dd turned on\n");



  /* get ODB  parameters  */
  status=cm_get_experiment_database(&hDB, NULL);
  if(status != CM_SUCCESS)
  {
    cm_msg(MERROR,"frontend_init","could not connect to experiment");
    return status;
  }

  if(debug)printf("MUSR-TD equipment name: %s\n",td_eqp_name);

   I_MUSR=TD_MUSR=FALSE; /* filled by get_musr_type */

   status = get_musr_type();
   if(status != DB_SUCCESS)
     {
       cm_msg(MERROR, "frontend_init", "cannot determine MUSR run type (I or TD)");
       return DB_NO_ACCESS;
     }


  /* get current run state */

  display_run_state();  // updates run_state
  if (TD_MUSR   && run_state != STATE_STOPPED)  // Only stop run if it is a TD run
  {
    char str[128];
    /*  Stop the run ... */ 
    if (cm_transition(TR_STOP, 0, str, sizeof(str), TR_SYNC, FALSE) != CM_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot stop run: %s", str);
      return FE_ERR_HW;
    }
    cm_msg(MINFO,"frontend_init","Forced run to stop");
    ss_sleep(500); /* wait for 500 ms */
  }
   display_run_state();  // updates run_state
   if(run_state != STATE_STOPPED)
     {
       cm_msg(MERROR,"frontend_init","cannot stop the (TD-MUSR) run to restart fev680.c");
       return  DB_NO_ACCESS;
     }

  status = enable_equipments(FALSE);  // Disable TD Equipments
  if(status != SUCCESS)
    return status;
  gbl_equipments_enabled = FALSE;

  status = create_records(); // create v680, settings record and read data
    if (status != DB_SUCCESS)
    return status;

  status = setup_hotlink();
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
  
  /* these cannot go at BOR due to a problem with db_close; for now they are always open */
  setup_hotlink_BOR(); /* setup hotlinks that are open when running */
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
  
  printf("Success from setup_hotlink_BOR\n");
  
  
  
  max_his_byte = Total_Byte;
  //if(debug)
  printf("frontend_init:Max Free space for histograms has been set to  %d  (0x%x) bytes for VMIC \n",
	 Total_Byte,Total_Byte);
  
  if(debug)
    {
      printf("frontend_init: Maximum bytes available for histograms =  %d  (0x%x) bytes\n",max_his_byte,max_his_byte);
      printf("frontend_init:  event_buffer_size = %d \n", event_buffer_size);
    }  
  
  cm_msg(MINFO,"frontend_init","Maximum bytes available for histograms =  %d  (0x%x) bytes\n",max_his_byte,max_his_byte);
  sprintf(str, "/Equipment/%s/v680/maxbyte",td_eqp_name);
  size = sizeof(max_his_byte);
  status=db_set_value (hDB, hV, "maxbyte", &max_his_byte, size, 1, TID_DWORD);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","could not set value=%d to %s (%d) ",max_his_byte,str,status);
      return FE_ERR_ODB;
    }
  if(debug)printf ("Success: maximum available bytes for histograms (%d) has been written to odb\n", max_his_byte); 
  

  set_default_params(); /* get DEFAULT params into info */
  
  
  // Running under vmic
  vmic_status = mvme_open(&myvme, 0);
  if(vmic_status != MVME_SUCCESS)
    { 
      cm_msg(MERROR, "frontend_init","Cannot access VME with VMIC (%d)",vmic_status);
      return FE_ERR_HW;
    }
  
  
  
  /* Note: Addressing Modes of VME Modules used for MUSR
     A16 D16/D8 GGL
     A16 D16    V680  
     A24/D32    SIS3803
     A24/D32    MUSRVMEIO

  */

 
  if(run_state == STATE_STOPPED)
    {  // initialize hardware only if not running 
      status = v680_init(FALSE); /* call with FALSE from frontend_init */
      if (status != SUCCESS)
	{
	  cm_msg(MERROR, "frontend_init", " error return from v680_init ");
	  printf("frontend_init: error return from v680_init    \n");
	  return FE_ERR_ODB;
	}
      
      // done in v680_init
      // v680RegWrite(myvme, V680_BASE, V680_RESET, V680_BRESET);
     
      v680DumpRegs(myvme, V680_BASE, 1);/*  dump all regs */
      
      //  A24/D32 for VMEIO selected by VMEIO driver
      status=vmeio_init(); /* initialize vmeio module */
      if(status != SUCCESS)
	{
	  cm_msg(MERROR,"frontend_init","failed to initialize vmeio module");
	  return FE_ERR_HW;
	}
      
      
      /* sis3803 hardware init  (A24/D32 set by SIS3803 driver)  */
      status = scaler_init();
      if (status != SUCCESS)
	{
	  cm_msg(MERROR,"frontend_init","error return from scaler_init; status %d", status);
	  return FE_ERR_HW;
	}

      /* ggl reset */
      status = gglReset(myvme, ggl_base); /* reset the ggl board */

  
#ifdef GONE
      /* debugging */
      printf("frontend_init calling hm_check \n");
      hm_check(0);
      
      printf("\nfrontend_init: calling hm_write with data = 1\n");
      hm_write(1);
      
      printf("\nfrontend_init calling hm_dump\n");
      hm_dump(8); /* dump only the first 8 bins */
      
      printf("\nfrontend_init calling hm_check with data =1\n");
      hm_check(1);
#endif
    } // initialize hardware

  printf("Frontend_init: returning success\n");
  return SUCCESS;
}


/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  v680_stop(); /* stop the v680 */
  mem_cleanup();
  status = enable_equipments(FALSE);  // Disable TD Equipments
  if(status != SUCCESS)
    return status;
 
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  int status;
  int max=10; /* max time in sec for check_update_time */
  char str_set[128];
  BOOL flag;

  I_MUSR=TD_MUSR=FALSE; /* filled by get_musr_type */

  status = get_musr_type();
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin_of_run", "cannot determine MUSR run type (I or TD)");
      return DB_NO_ACCESS;
    }
  if (!TD_MUSR)
    {
      mem_cleanup(); // free memory
      status = enable_equipments(FALSE);  // Disable TD Equipments
      if(status != SUCCESS)
	return status;
      gbl_equipments_enabled = FALSE;

      cm_msg(MINFO, "begin_of_run", "I_MUSR selected: no action needed");
      return DB_SUCCESS;
    }

  status = enable_equipments(TRUE);  // Enable TD Equipments
  if(status != SUCCESS)
    return status;
  gbl_equipments_enabled = TRUE;

  /* NOTE:  if enabled, mdarc has initialized frontend flag in ODB to FAILURE  on prestart */
  //gbl_status=SUCCESS; /* not used... IMUSR only.  set to success for frontend_loop */
  gbl_run_number = run_number;

/* Turn the v680 display OFF and clear the screen */
  v680_Display = msg_cnt = 0;
  ss_clear_screen();
  if(debug)printf ("\n\n *******   begin of run starting   ****** \n");

 
  /* Get the flag that allows mdarc to stop the run (from client_flag area of odb)  */
  size = sizeof(client_check); 
  sprintf(str_set,"/Equipment/%s/client flags/enable client check",td_eqp_name); 
  status = db_get_value(hDB, 0, str_set, &client_check, &size, TID_BOOL, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MINFO,"begin_of_run","cannot get value of \"%s\" (%d)",str_set,status);
      return (DB_NO_ACCESS);
    }
  if(client_check)  /* who_stops_run is used for messages only */
    sprintf(who_stops_run,"mdarc");
  else
    sprintf(who_stops_run,"user");
 

  /* Get current v680 settings */
  sprintf(str_set,"/Equipment/%s/v680",td_eqp_name); 
  size = sizeof(vs);
  status = db_get_record(hDB, hV, &vs, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "begin_of_run", "cannot retrieve %s record (size of vs=%d)", str_set,size);
    return DB_NO_ACCESS;
  }
  /* check that musr_config has run recently ( < max seconds ago) */
  status = check_update_time(max);
  if(debug)printf("begin_of_run: check_update_time returns with status %d\n",status);
  if(status != SUCCESS)
    return FE_ERR_HW;    /* error return */

  /* get the latest parameters from v680 area */
  status = set_params();
  if (status != SUCCESS)
    return (status);
    

  status = v680_init(TRUE); /* call with TRUE from begin of run */
  if (debug) printf("begin_of_run: returned from v680_init (%d)\n",status);
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from v680_init (%d)",status);
    return (status);
  }

  // A16 set inside ggl_init()
  status = ggl_init(); /* loads gate lengths  */
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from ggl_init; status %d", status);
    return FE_ERR_HW;
  }

  status = scaler_init();
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","scaler_init not successful (%d)", status);
    return FE_ERR_HW;
  }
  status = vmeio_init();
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from vmeio_init; status %d", status);
    return FE_ERR_HW;
  }

  // TEMP
  int i;
  for(i=0;i<16; i++)
    tempChan[i]=0;
    

  set_v680_display(v680_Display,disp_offset,last_message); /* send info to V680.c */

  //   sis3803_all_clear(myvme, SIS_base_adr);  

#ifdef DEBUG
  if(debug)
    {
      printf("begin run: calling hm_write and hm_dump\n");
      hm_write(1); /* write some test data */
      hm_dump(8); /* dump the data (8 words only by default) */
    }
#endif  
  status = scaler_start(); /* start the scaler */
  if (status != SUCCESS)
    {
      cm_msg(MERROR,"begin_run","Scaler disabled. Could not start scaler");
      return  FE_ERR_HW;
    }
  v680_start(); /* start the v680 */
  vmeio_on(); /* set levels on vmeio  */
  if(debug)printf("begin_of_run: started the v680\n");
  
  
  if(debug)printf("begin_of_run: returning success\n");

  ss_sleep(3000); /* wait 3s so we can read the info. */
  /*
    Now  enable v680 display ONLY if key v680/display is enabled (hot linked)
    AFTER any message have been written by above subroutines
  */
  if (v680_Display )
    v680_display(1); /* call this with onoff=1 */

  /* set client status flag for frontend to SUCCESS */
  flag=SUCCESS;
  status = set_client_flag("frontend",flag) ; /* set client flag to success */  
  if(status != DB_SUCCESS)  
    return status;
   
  return SUCCESS;  
  
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{

  if(TD_MUSR)
    {
      v680_stop(); /* stop the v680 */
      scaler_stop(); /* stop the scaler */
      
      /* remove hotlinks that are only active during the run */
      /*  close_hotlinks(); db_close not working */
    }
      return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  if(TD_MUSR)
    {
      /* set IO reg to disable incoming signals */
      
      /* disable the scaler */
      scaler_stop();
    }
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  if(TD_MUSR)
    {
      if (fs.enabled)   /* if scaler is enabled */
	{
	  if(dss)printf("resume_run: enabling scaler\n");
	  sis3803_all_enable (myvme, SIS_base_adr);    /* enable global counting so module acquires counts */ 
	  
	}
    }
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  if(!TD_MUSR)
    return SUCCESS;

  /*
    Note: v680/display is normally true... use ss_printf or cm_msg except for debug
  */ 
  INT   j, temp;
  
  /* cm_yield(1); may not need */
  
  
  /*  if(dd)printf("frontend_loop starting \n"); */ 
  
  /* code from polling_loop in original v680.c */
  
  temp =  ss_millitime();
  local = ss_millitime() - last_display;
  
  if ( (local > v680_Display_period) || first_time_display )
  {
   if(dd)
    {
      printf("frontend_loop: Time to check for clock hang  and display\n");
      printf("frontend_loop: Current values of dataL: 0x%x; dataM: 0x%x; pData32: 0x%x; channel: %d\n",
             dataL,dataM,*pData32,channel);  
    }

    /*  time to update display if enabled */

    
    first_time_display = FALSE;
    last_display = ss_millitime();
    if (v680_Display != 0)
    {
        if(dd) printf("frontend_loop:updating display with v680_Display=%d\n",
                      v680_Display);
        v680_update(); /*  updates current run state (run_state)  */    
        /* reset msg_cnt used by poll_event to limit messages */
        msg_cnt=0;
    }    
    else
    {  /* v680_update not called */

	/* debug only  - print all hit counters and rates if display is not
	   enabled */
#ifdef DEBUG
#ifdef GONE /* I don't want this at present */
	if (run_state == STATE_RUNNING)
	{
            /* temp : print the rates if running */
            for (j=0; j<4+max_channels; j++) 
                printf( "%8d ",hits[j]);
            printf("\n");
            for (j=0; j<4+max_channels; j++) 
                printf( "%8d ",(hits[j]-svhits[j])/(v680_Display_period/1000));
            printf("\n");
            
            printf(" Poll  Cur_pol  #Poll  Status\n");
            printf("%d,     %d,    %d,    %s  \n",ext_polling_time,pol_time,gbl_count,str_status[run_state]);
	}
#endif
#endif
	
    }
    /* printf("frontend_loop: run_state=%d\n",run_state); */
    /* time to check for a clock hang */
    if (run_state != STATE_STOPPED) /* don't bother if not running */
    {
      if(dd)printf("frontend_loop: checking for clock hang... poll_time(%d) millitime(%d) \n",
                     pol_time,temp);
      v680ClearClockHang(myvme, V680_BASE); /* if there is no TDC module, (e.g. testing) comment this out */
    }
    
    
    
    /* save counter for rate measurement */
/*    for (j=4; j<4+max_channels; j++)  DEBUG - save from j=0 */
    for (j=0; j<4+max_channels; j++)
        svhits[j] = hits[j]; 
    
    
    if(dd)
    {
        printf("frontend_loop: Non-zero hit counters only are listed\n");
        for (j=0; j<4+max_channels; j++)
            if(hits[j] != 0) printf("frontend_loop: Hits[%d]=%d\n",j,hits[j]);
    }
  }
  
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\
  
  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
{


  /* hit is a global (STATIC WORD) */
  DWORD    counter;
  //  INT   tmp;
 
 
  /*  printf("poll_event\n"); */
  gbl_count=count; /* remember count for display */
  
  /* code from v680_Mue_loop(const DWORD count) */
  start_poll = ss_millitime();
  
  for (counter=0 ; counter<count ; counter++)
  {
    hit=0;
    /* poll on bit 15 of ueReg */
    hit =  v680RegRead(myvme, V680_BASE,V680_MUEREG);  /* hit = *muE_reg; */
    
     if (hit < 0xfff0)  // hit is WORD (unsigned short int) F15 flag This statement does not agree with manual
    {            /* hit present */
      if(dd) printf("poll_event: got a hit! hit=0x%x\n",hit);

      
      /* With MuE only 19 bit is provided */                /* dataH = *pData16_H = *datH_reg; */
      /* 2x16 bit VME access */
      dataM= *pData16_M = v680RegRead(myvme, V680_BASE,V680_MUEDATA_M);/*	  dataM = *pData16_M = *datM_reg; */
      dataL= *pData16_L = v680RegRead(myvme, V680_BASE,V680_MUEDATA_L);/*   dataL = *pData16_L = *datL_reg; */
    
      /* the byte contains only the channel number with the ref reg.
	 0000Rccc => use directly the content as channel number 
	 Go through lookup table for channel reassignment
      */
      
      channel = *(pch_table + (int)(*pbchn));
      if(dd)printf("dataM= %4.4x dataL=  %4.4x channel=%d *pbchn=%d\n",dataM,dataL,channel, (int)(*pbchn));
      //  WORD mc = dataM >> 8;
      //  tempChan[mc]++; // count number of hits on REAL channels   TEMP
      //  printf("hit=0x%x dataM=0x%x dataL=0x%x ch=%d (int)(*pbchn)=0x%x   channel=%d  Real channel hits:",hit, dataM, dataL, mc,(int)(*pbchn),channel); 
      // TEMP
      // int i;
      // for(i=0;i<8;i++)
      //	printf("%d ",tempChan[i]);
      //  printf("\n");

      /* bugfix: later try masking the channel to keep the data
	 if (*pbchn > 15) print...
	 channel = *(pch_table + (int)(*pbchn & 0xF)
      */	  
      /* check validity of channel if -1 => channel disable */
      /* bugfix for now put in an extra check */
      if (channel < 0 || channel > 15)
	    {
	      /* Bad Channel */
	      if(v680_Display)
	      {
		if(msg_cnt == 0) /* message only once per display period */
		{
		  sprintf(display_string,"Invalid channel: %d  ------------------------",*pbchn);
		  msg_cnt++;
		}
	      }
              if(dd)printf("Invalid channel: %d\n",*pbchn);
	      (*(hit_ivc))++;  /*invalid channel hit counter */	       
	    } /* end of bad channel */
      else
      {
	/* good hit channel counter */
	(*(hits+4+channel))++;
	/* mask TDC region of interest. Shift right first, then ... */
	*pData32 >>= data_shift;
	
	/* check for overflow before masking off the requested data range */
	if (*pData32 & ovfl_mask)
	{
	  /* Data Overflow */
	  if(v680_Display)
	  {
	    if(msg_cnt == 0) /* message only once per display period */
	    {
	      sprintf(display_string,"Data overflow 0x%x > 0x%x \n",*pData32, data_mask);
	      msg_cnt++;
	    }
	  }
	  if(dd)printf("Data overflow 0x%x > 0x%x \n",*pData32, data_mask);
	  (*(hit_ovf))++;
	} /* end of  data overflow */ 
	else
	{   /* good hit */
	  /* Mask off to data field */
	  *pData32 &= data_mask;
	  /* concatenate reassigned channel number in front of the data */
	  *pData32 |= (channel<<data_field);
      
	  /* debugging */
	  if (flatmode)
	    *pData32  = (*hit_hit & data_mask);
      
	  /* increment bin */
	  if (info.bytes_per_bin == TWO_BYTES_PER_BIN)  
	    *(pmemBase2 + *pData32) += 1;
	  else
	    *(pmemBase4 + *pData32) += 1;
      
	  (*(hit_hit))++;
          if(dd)printf("Good hit \n");
	} /* end of good hit */
      }
    } /* end of hit present */
    else   /* no hit */
    {
      (*(hit_mis))++;   /* missing hit counter */
      // if(dd)printf("poll_event: Missing hit...  hit=0x%x\n",hit);
    }
    
    
  } /* end of counter loop */  
  pol_time = ss_millitime() - start_poll;
  return FALSE;
}

/*-- Interrupt configuration for trigger event ---------------------*/
INT interrupt_configure(INT cmd, INT source[], PTYPE adr)
{
  /*
    switch(cmd)
    {
    }
  */
  
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT diag_read(char *pevent, INT off)
{
  /* send hit counters out as a diagnostic event */
  DWORD *pdata;
  INT j;
  
  if(ddd) printf ("diag_read: starting \n");

/* init bank structure */
  bk_init(pevent);
    /* create DIAG bank */
  bk_create(pevent, "DIAG", TID_DWORD, (void **) &pdata);
  
  /* copy hit counters into the event */
  
  *pdata++ = 0; // code=0 indicates TDMUSR data
  *pdata++ = disp_offset; 

  // Number of counters
  *pdata++ = (4+max_channels); // variable length

  if(ddd) printf ("Hit counters:  index   data: \n");
  
  for (j=0; j<4+max_channels; j++)
  {
    if(ddd) printf("                     %d      %d \n",j,hits[j]);
    *pdata++ = hits[j];
    *pdata++ = (DWORD) (hits[j]-svhits[j])/(v680_Display_period/1000);
  }

  bk_close(pevent, pdata);
  if(ddd)printf("diag_read returning event size %d\n", bk_size(pevent));
  
  return bk_size(pevent);
}


/*-- Scaler event --------------------------------------------------*/

INT scaler_read(char *pevent, INT off)
{
  double *pdata;
  INT is,j,i=0;
  DWORD count,overflow;
  char str[128];
  DWORD temp[MAX_SCALER];
  //  INT k;

  if(dss) printf("scaler_read: starting\n");
  if (! fs.enabled)
  {
    if (dss) printf("scaler_read: returning as scaler is disabled\n");
    return(0); /* no event */
  }

  if (vs.flags.test_pulses)    /* test pulse mode  */
    sis3803_test_pulse(myvme, SIS_base_adr);

  

  /* init bank structure */
  bk_init(pevent);

  /* create SCLR bank */
  bk_create(pevent, "SCLR", TID_DOUBLE, (void **) &pdata);



  /* only selected channels are enabled but all channels' data are output;
      disabled channels read 0
  */


  /* will read out max_scaler_input+1 words from the scaler */
  if(dss)
    {
      printf("Reading %d values from the scaler\n",max_scaler_input+1);
      printf (" index  input  data: \n");
    }
  for (j=0;j<max_scaler_input+1;j++)
  {

    sis3803_counter_read(myvme, SIS_base_adr, j, &count);
    temp[j] = count;
  
    if(dss) 
      printf(" %2d      %2d   %d \n",j,j,temp[j]);
  }

  /* strip away any unused scalers from the data array */
  //  nscal=0; /* scaler counter */
  /*for (i=0; i < max_scaler_input; i++)
    {
    if (fs.bit_pattern & (1 << i) )
    { 
    if(dss)printf(" Index %d (i.e. channel %d) is enabled, data = %d\n",i,i+1,temp[i]);
    scaler_counts[nscal] = temp[i];
    nscal++;
    }
    }
  */

  /* Send the data out in scaler input order */
  for (i=0; i < fs.num_inputs; i++)
    {
      is = fs.inputs[i];
      if(is<0 || is > max_scaler_input)
	{
	  printf("Error: data index (%d) out of range (0<index<%d); scaler bitpat & scaler input array may be incompatible\n",
		 is,max_scaler_input);
	  cm_msg(MERROR,"scaler_write","Scaler data index (%d) out of range (0<index<%d)",is,max_scaler_input);
	  return (0);
	}
       if(dss) printf("Scaler Input[%d]=%d  : copying %d counts into scaler_counts[%d]\n",i,is,temp[is],i); 
      scaler_counts[i] = temp[is];

    }

  if(dss)
      printf(  "Sorted Scaler Index     counts\n");
  
  for  (i=0; i < fs.num_inputs; i++)
    {
      if (dss) 
	printf("                %2d        %d\n",i,scaler_counts[i]);
      /* add this data to the bank */
      *pdata++ = (double)scaler_counts[i];
    }

  /* check the overflow  */

  sis3803_OVFL_grp1_read(myvme, SIS_base_adr, &overflow);
  if(fs.num_inputs > 8)
  {
    sis3803_OVFL_grp2_read(myvme, SIS_base_adr, &count);
    overflow = overflow || (count << 8);
  }

  
  bk_close(pevent, pdata);


  /*    write the overflow into odb */
  sprintf(str,"/equipment/scaler/variables/overflow");
  status=db_set_value (hDB, 0, str,
		       &overflow,
		       sizeof(overflow), 1, TID_DWORD);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"scaler_read","could not set %s (%d) ",str,status);
  
  /* check for overflow
     we would have to clear the scaler channels to clear the overflow bits; we are
     not using read & clear
  */
  if(overflow)
    {
      if(first_overflow)
	{
	  cm_msg(MINFO,"scaler_read",
		 "scaler channel(s) have overflowed (bitpat=0x%x)\n",overflow);
	  first_overflow = FALSE;
	}
    }
  return bk_size(pevent);

}




INT scaler_rates(char *pevent, INT off)
{
  double *pdata;
  float period;
  int j,i=0;
  DWORD last;
  double rate;
  //  char str[128];
  double diff;
  
  
  if(dss) printf("scaler_rates: starting\n");
  if (! fs.enabled)
  {
    if (dss) printf("scaler_rates: returning as scaler is disabled\n");
    return(0); /* no event */
  }

  last =  time_last_rates;  /* last time rates were calculated (or scaler was started/cleared) */
  time_last_rates = ss_millitime();
  period = (float)(time_last_rates - last);
  
  if (dss)printf ("scaler_rates: time since rates last calculated: %f ms\n", period); 

  /* init bank structure */
  bk_init(pevent);

  /* create RATE bank */
  bk_create(pevent, "RATE", TID_DOUBLE,  (void **)&pdata);

  /*  Make sure we have a meaningful interval and that period won't give a 
   *  divide by zero error.  The "last" time is zero when it was initialized
   *  arbitrarily, so ignore it in that case (even though it will randomly be
   *  zero once every 4 billion tries) */

  if ( last == 0 || period <= 1.0 )   /* cannot calculate the rate */
  {
    if(dss)printf("scaler rates not calculated, last time rates calculated=%d period=%f\n",last,period);
    /* set the rates to 0 */
    rate = 0.0;
    for (i=0; i < fs.num_inputs; i++)
      *pdata++ = (double)rate;
  }
  else
  {
    /* to calculate the rate, take the difference between current and last counts, divide by 
       period
    */
    for (i=0; i < fs.num_inputs; i++) 
    {
      rate = 0.0;  /* default */
      diff =  (double) scaler_counts[i] - (double) old_counts[i] ;

      /* round the scaler rate, even though it could be fractional; integers are less surprising */
      /* round() not available...     rate = round( 1000.0 * ( diff / (double)period) ); */
      rate = (double) ( (int) (diff / ((double)period/1000.0) + 0.5) ) ;

      *pdata++ = (double)rate;
      if(dss)printf("index %d: data is %12d, previous %12d, difference =%lf, rate is %lf \n",
		    i,scaler_counts[i],old_counts[i], diff,rate);
    }
  }

  bk_close(pevent, pdata);
  if(dss)printf("scaler_rate returning event size %d\n", bk_size(pevent));

  for (j = 0 ;  j < fs.num_inputs ; j++)
    old_counts[j] = scaler_counts[j];

  return bk_size(pevent);
}
/* ----------------------------------------------------------------------
 */
INT setup_histo_index()
{
  /* setup for indexing histograms in musr counter order
     Called after calling mem_init  (called by v680_init)
   */
  INT i, j;
  INT offset,len; 
  //  char str[132];
  //INT size, status; 
  char stmp[3];
  
   /* global max_channels (set up in mem_init) contains the
     number of histograms) */
  if(debug)
  {
    printf("\nsetup_histo_index: starting");
    printf("%d histograms; %d bins;  %d bytes_per_bin\n", max_channels, info.nbins,info.bytes_per_bin);
  }

  /* NOTE:   musr_config has written arrays TDC_inputs and index_into_hm into
     v680/histograms area
  */
  sprintf(histo_order,"Histos sent in counter order: Hit"); 
  if(debug)
    printf("Index  TDC input  HM index  HM offset\n");
  for (i=0; i<max_channels; i++)
  {
    if ( vs.histograms.index_into_hm[i] < 0 ||  vs.histograms.index_into_hm[i] > 15)
    {
      cm_msg(MERROR,"setup_histo_index","Invalid value for histo index  vs.histograms.index_into_hm[%d]=%d ",i, vs.histograms.index_into_hm[i]);
      return (DB_INVALID_PARAM); /* fail */
    }
      HM_word_offset[i]= vs.histograms.index_into_hm[i]*info.nbins;
    
    if(debug)
      printf(" %2d     %2d       %d      %d\n",
	     i, vs.histograms.tdc_inputs[i], vs.histograms.index_into_hm[i], HM_word_offset[i]);
    /* fill string histo_order for display */
    sprintf(stmp,"[%d],", vs.histograms.index_into_hm[i]);
    strcat(histo_order,stmp);
  }
  len=strlen(histo_order);
  histo_order[len-1]='\0';
  if(debug)
    printf("%s\n",histo_order);

  /* check we have reasonable numbers in HM_word_offset */
  j=0;
  while (j < max_channels)
  {
    offset = HM_word_offset[j];
    for (i=j+1; i<max_channels; i++)
    {
      if (offset == HM_word_offset[i])
      {
        /* if  vs.histograms.index_into_hm contains duplicate, we get duplicates in HM_word_offset */
        cm_msg(MERROR,"setup_histo_index",
               "Duplicate values at  hm_index[%d] and  hm_index[%d] (%d) ",
               i,j, vs.histograms.index_into_hm[i]);
      return (DB_INVALID_PARAM); /* fail */
      }
    }
    if ( offset < 0 || offset > (max_channels * info.nbins) )
    {
      cm_msg(MERROR,"setup_histo_index",
             "Offset (%d)  at  hm_index[%d]=%d is out of range ( <0 or >%d ) ",
             offset,j, vs.histograms.index_into_hm[j], (max_channels * info.nbins) );
      return (DB_INVALID_PARAM); /* fail */
    }
    j++;
  }
  return(DB_SUCCESS);
  
}
#ifdef GONE  // not sure what this was for...
INT xdiag_read (char *pevent, INT off)
{
  return (0);
}
#endif
/*-- Histogram event --------------------------------------------------*/

INT histo_read (char *pevent, INT off)
{

  WORD  *pdata2, *pmem2; 
  DWORD *pdata4, *pmem4, *ptmp4;
  //INT index, unmap_pat, map_pat;
  INT i;
  char hist_name[10];
  DWORD offset; /* offset in words into HM */

/* histos must be sent out IN COUNTER ORDER
   - use hm_index (i.e. vs.histograms.index_into_hm)  as offsets into the HM

   Send out ONLY the number of bins requested (vs.number_of_bins), rather than
    the number stored in HM (info.nbins)
  */


  if(ddd)
    {
      printf("\nhisto_read: starting with info.nbins is %u, vs.number_of_bins %u\n",
         info.nbins, vs.number_of_bins);
      printf("\n  pmemBase4=%p, pmemBase2=%p, \n", pmemBase4,pmemBase2);
    }

  
  /* init bank structure */
  bk_init32(pevent);
  
  for (i=0; i<max_channels; i++)   /* for each histogram */
    
  {
    sprintf(hist_name, "HI%02d", i);
    offset = HM_word_offset[i];
    if (ddd) printf( "i=%d hist %s offset in words=%d\n", i,hist_name,offset);      
    /* create HHHH bank, case Base2 */
    
    if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
    {
      /*
	TWO BYTES PER BIN
      */      
      bk_create(pevent, hist_name , TID_WORD,  (void **)&pdata2);
      pmem2 = pmemBase2 +  offset; /* do not cast offset ! */ 
     if(ddd)printf("Created WORD bank %s, initial HM pointer = %p, length will be %u bins\n",
           hist_name,pmem2,vs.number_of_bins);

#ifdef GONE
     /* temp: for testing only 
	put some data in the first 10 channels of each histo */
     {
       WORD *ptmp;
       ptmp = pmem2;
       for(j=0; j<10; j++) {
	 *ptmp = 32768+j+i;
	 ptmp++;
       }
     }
#endif
      /* send out the number of bins requested (NOT the rounded up number info.nbins)*/
      memcpy(pdata2, pmem2,  vs.number_of_bins * sizeof(WORD));
      pdata2 +=   vs.number_of_bins;/* do not cast no. bins ! */
      bk_close(pevent, pdata2);
      
    }
    else
      /*
	FOUR_BYTES_PER_BIN
      */
    {
      bk_create(pevent, hist_name , TID_DWORD,  (void **)&pdata4);
      pmem4 = pmemBase4 + offset; /* add word offset - do not cast! */
      
      if(ddd)
      {
        printf("Created DWORD bank %s, initial HM pointer = %p, length will be %u bins\n",
          hist_name,pmem4,vs.number_of_bins);
        printf("Histo %d: word offset from pmemBase4 =%d\n",i,offset);
      }
#ifdef GONE
           /* temp: for testing only 
	      put some known data in the first 10 channels of each histo */
     {
       DWORD *ptmp;
       ptmp = pmem4;
       for(j=0; j<10; j++) {
	 *ptmp = 32768+j+i;
	 ptmp++;
       }
     }  
#endif
      /* send out the number of bins requested (NOT the rounded up number info.nbins)*/
      ptmp4=memcpy(pdata4, pmem4, vs.number_of_bins * sizeof(DWORD));
      pdata4 +=  vs.number_of_bins; /* do not cast no. bins! */
      if(ddd)printf("After adding no. bins (%d or 0x%x), pdata4 = %p\n",vs.number_of_bins,vs.number_of_bins, pdata4);     
      bk_close(pevent, pdata4);
    }    
  } /* end of loop on all channels (histograms) */

  if (ddd) printf ("\n\n pevent size is %d (0x%x) bytes \n",
		   bk_size(pevent), bk_size(pevent)  );


  return bk_size(pevent);
}



/*-mem_init------------------------------------------------------*/
INT mem_init(void)
{
  int i;


  if(debug)printf("mem_init starting\n");
  disp_offset = 0 ;
  /* Retrieve # active channels */
  if (info.bit_pattern != 0)
    {
      max_channels = 0;
      for (i=0 ; i<16 ; i++)
	{
	  if (info.bit_pattern & (0x1<<i))
	    channel_table[i] = (++max_channels - 1);
	  else
	    channel_table[i] = -1;
          if(debug)printf("channel_table[%d]=%d\n",i,channel_table[i]);
	}
      if ( (info.bit_pattern & 0xff00) == 0 )
        { /* NOT dual-spectra mode.  Duplicate SAMPLE hist mapping to REF bank */
	  if(debug)printf("Copy low channel_table to high (map ref->sample)\n");
	  for (i=0;i<8;i++)
	    {
	      channel_table[i+8] = channel_table[i] ;
	    }
	}
      if (max_channels > 8 ) disp_offset = 3 ; /* need extra 3 lines for display */ 
    }
   if(debug)printf("max_channels = %d\n",max_channels);
   /* first make sure we can ship out these histograms */ 
   total_byte   =  max_channels * info.nbins * info.bytes_per_bin; /* borrow total_byte temporarily */
   if(debug)printf("Checking total_byte (%d) <= max_his_byte (%d)\n",total_byte,max_his_byte);
   if (total_byte > max_his_byte )     /* we can't fit these histograms in midas's buffer */
     {
       cm_msg(MERROR,"mem_init"," # bytes requested (%d) exceeds maximum that can be buffered (%d)",
	      total_byte,max_his_byte);
       printf( "mem_init: # bytes requested (%d) exceeds maximum: %d\n",
	        total_byte,max_his_byte);
       return (FE_ERR_HW);
     }


  
  /* assign entries */
  data_shift   = info.shift;
  i = 0;
  while ((0x1<<i++) < info.nbins);
  if(debug)printf("index i=%d, nbins=%d\n",i,info.nbins);
  data_field = --i;
  if(debug)printf("data field=%d\n",data_field);
  data_mask    =  (0x1<< data_field) - 1;
  if(debug)printf("data mask=0x%x\n",data_mask);
  ovfl_mask = ~(data_mask) & (0xffffff >> data_shift);
  if(debug)printf("ovfl mask=0x%x\n",ovfl_mask);

  /* make sure we can store these histograms internally (more bins are stored than are sent out) */
  if (info.nbins != data_mask+1)
  {
    /* debugging - send this info to logger */
    if(debug)
      {
	cm_msg(MINFO,"mem_init","nbins requested (%d) adjusted to %d; %d histos requested",
	       info.nbins, data_mask+1, max_channels); 
	printf( "mem_init: nbins requested (%d) adjusted to %d\n",info.nbins, data_mask+1);
      }
    info.nbins = data_mask+1;
  }
   
  total_bin    = ((data_mask+1) * max_channels);
  total_byte   =  total_bin*info.bytes_per_bin;
  if(debug)
  {
    printf("mem_init: bins requested for all histos: %d; Total_Bin available: %d\n",total_bin,Total_Bin);
  
    printf("mem_init: nbytes requested for all histos: %d (0x%x); Total_Bytes available for histos: %d\n",
	   total_byte, total_byte,Total_Byte);
  }
  if (total_bin > Total_Bin)     /* catch this before malloc */
    {
      cm_msg(MERROR,"mem_init"," # bins requested (%d) exceeds maximum permitted: %d",total_bin,Total_Bin);
      printf( "mem_init: Error: # bins requested (%d) exceeds maximum permitted:  %d\n",total_bin,Total_Bin);
      return (FE_ERR_HW);
    }



  i = 0;
  while ( (1<<i++) < max_channels);
  if(debug)printf("index i=%d\n",i);
  channel_mask = (0x1<<--i) - 1;
  if(debug)printf("channel mask=0x%x\n",channel_mask);

  if(debug)printf("bytes_per_bin =%d\n",info.bytes_per_bin);

  
  /* histogram location */
  if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
  {
    memory_size  =  total_bin * 2;
    pmemBase2 = (WORD *) malloc(memory_size);
    if (pmemBase2 == NULL )
    {
      cm_msg(MERROR, "mem_init","malloc fails attempting to allocate histogram memory");
      printf( "mem_init: malloc fails attempting to allocate histogram memory\n");
      return (DB_NO_MEMORY); /* fail */
    }    
    else
      if(debug)printf ("mem_init: dbg malloc allocated %d bytes; pmemBase2: 0x%x\n",memory_size,(unsigned int)pmemBase2);
    
  }
  else
  {    /* four bytes/bin */
    memory_size  =  total_bin * 4;
    pmemBase4 = (DWORD *) malloc(memory_size);
    if (pmemBase4 == NULL )
    {
      cm_msg(MERROR, "mem_init","malloc fails attempting to allocate histogram memory");
      printf( "mem_init: malloc fails attempting to allocate histogram memory\n");
      return (DB_NO_MEMORY); /* fail */
    }
    else
      if(debug)printf ("mem_init: dbg malloc allocated %d bytes; pmemBase4: 0x%x\n",
		       memory_size, (unsigned int)pmemBase4);
    
  }


  if(debug)
  {
    printf("info.nbins         : %d\n",info.nbins);
    printf("info.pattn         : 0x%x\n",info.bit_pattern);
    printf("info.shift         : %d\n",info.shift);
    printf("info.bytes_per_bin : %d\n",info.bytes_per_bin);
    printf("data_shift         : 0x%x\n",data_shift);
    printf("channel_mask       : 0x%x\n",channel_mask);
    printf("ovfl mask          : 0x%x\n",ovfl_mask);
    printf("data_field         : 0x%x\n",data_field);
    printf("data_mask          : 0x%x\n",data_mask);
    printf("No. bins used      : %d, 0x%x\n",total_bin,total_bin);
    printf("No. channels used  : %d\n",max_channels);
    printf("No. bytes used     : %d, 0x%x\n",memory_size,memory_size);
    printf("Max_total_bytes    : %d, 0x%x\n",Total_Byte,Total_Byte);
    printf("Max_total_bin      : %d, 0x%x\n",Total_Bin,Total_Bin);
  }
  /* data location */
  if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
  {
    memset((char *) pmemBase2,0, memory_size);
    Address = (DWORD) pmemBase2;
  }
  else
  { /* four bytes/bin */
    memset((char *) pmemBase4,0, memory_size);
    Address = (DWORD) pmemBase4;    
  }
#ifdef GONE
  printf ("mem_init: calling hm_check directly after memset\n");
  hm_check(0);
#endif

  pdataBase = (WORD *)malloc (3*2);
  if (pdataBase == NULL )
  {
    printf( "mem_init: malloc fails attempting to allocate pdataBase\n");
    cm_msg(MERROR,"mem_init","malloc fails attempting to allocate pdataBase");
    return (DB_NO_MEMORY); /* fail */
  }
  memset ((char *) pdataBase,0,3*2);
  
  if(debug)printf ("mem_init: dbg after memset, Address: 0x%x; pdataBase: 0x%x\n",
		   Address,(unsigned int)pdataBase);


  /* precomputed pointers to the data registers */
#ifdef VXWORKS
  pData16_H = pdataBase+0;
  pData16_M = pdataBase+1;
  pData16_L = pdataBase+2;
  /* map to TDC value in 32 bit */
  pData32 = (DWORD *) pData16_M;
#else // VMIC  endian difference
  pData16_H = pdataBase+2;
  pData16_M = pdataBase+1;
  pData16_L = pdataBase+0;
  /* map to TDC value in 32 bit */
  pData32 = (DWORD *) pData16_L;
#endif


  if(debug)printf("pdataBase:  0x%x; pData16_M:  0x%x; pData16_L:  0x%x; pData32 :  0x%x\n",
         (unsigned int)pdataBase, (unsigned int)pData16_M, (unsigned int)pData16_L,(unsigned int)pData32);

  
  /* Hit register shortcuts */
      hit_hit = &(hits[0]);
      hit_ivc = &(hits[1]);
      hit_ovf = &(hits[2]);
      hit_mis = &(hits[3]);
  /* calibrate polling loop */

  return SUCCESS;
}


/*-mem_cleanup---------------------------------------------------*/
INT mem_cleanup(void)
{
  
  if (pmemBase2 != NULL )
  {
    if(debug) printf("mem_cleanup: dbg freeing pmemBase2 = 0x%x\n",(unsigned int)pmemBase2); 
    free (pmemBase2);
    pmemBase2 = NULL;
    if(debug) printf("mem_cleanup: dbg Now  pmemBase2 = 0x%x\n",(unsigned int)pmemBase2); 
  }
  if (pmemBase4 != NULL )
  {
    if(debug) printf("mem_cleanup: dbg freeing pmemBase4 = 0x%x\n",(unsigned int)pmemBase4);
    free (pmemBase4);
    pmemBase4 = NULL;
    if(debug) printf("mem_cleanup: dbg Now  pmemBase4 = 0x%x\n",(unsigned int)pmemBase4);
  }
  if (pdataBase != NULL)
  {
    if(debug)printf("mem_cleanup: dbg freeing pdataBase = 0x%x\n",(unsigned int)pdataBase); 
    free (pdataBase);
    pdataBase = NULL;
    if (debug) printf("mem_cleanup: dbg Now pdataBase = 0x%x\n",(unsigned int)pdataBase); 
  }
  return 1;
}



/*-display update ------------------------------------------------------*/
void v680_update(void)
{
  INT j;
  //  INT temp;
  
  /* get current run state  */

  if (disp_offset)     /* max_channels > 8 so need extra lines for display */
  {
      for (j=4; j<4+8; j++) 
      {
          ss_printf(9*(j-4), LINE_7,"%8d",hits[j]);
          ss_printf(9*(j-4), LINE_8,"%8d",(hits[j]-svhits[j])/(v680_Display_period/1000));
      }
      for (j=4+8; j<4+max_channels; j++)
      {
          ss_printf(9*(j-12), LINE_7+disp_offset,"%8d",hits[j]);
          ss_printf(9*(j-12), LINE_8+disp_offset,"%8d",(hits[j]-svhits[j])/(v680_Display_period/1000));
      }
  }
  else
  {
    for (j=4; j<4+max_channels; j++) 
    {
      ss_printf(9*(j-4), LINE_7,"%8d",hits[j]);
      ss_printf(9*(j-4), LINE_8,"%8d",(hits[j]-svhits[j])/(v680_Display_period/1000));
    }
  }
  
  
  if (channel < max_channels)
  {
    ss_printf( 0,LINE_11+disp_offset,"%4.4x     N/A      %4.4x     %4.4x     %p   0x%8.8x  %p"
               ,channel, dataM, dataL
               ,((info.bytes_per_bin == TWO_BYTES_PER_BIN) ? (DWORD *) pmemBase2 : pmemBase4) 
               ,*pData32
               ,((info.bytes_per_bin == TWO_BYTES_PER_BIN) ? (DWORD *) (pmemBase2 + *pData32) :
                 (pmemBase4 + *pData32)));
  }
  else
    ss_printf( 0,LINE_11+disp_offset," N/A                                                                      ");
  
  ss_printf( 0,LINE_14+disp_offset,"%d",hits[0]);
  ss_printf(10,LINE_14+disp_offset,"%6d",hits[1]);
  ss_printf(20,LINE_14+disp_offset,"%6d",hits[2]);
  ss_printf(30,LINE_14+disp_offset,"%4.2g",(double)hits[3]);
  
  ss_printf(40,LINE_14+disp_offset,"%d",ext_polling_time);
  ss_printf(50,LINE_14+disp_offset,"%d",pol_time);
  ss_printf(60,LINE_14+disp_offset,"%d",gbl_count); /* same as count in poll_event */
  ss_printf(70,LINE_14+disp_offset,"%s    ",str_status[run_state]);
  
  if (strlen(display_string) > 0 )
  {
    ss_printf(15, LINE_16+disp_offset,"%s  ",display_string);
    display_string[0]='\0'; /* clear string */
    //sprintf(display_string,"%s","");  /* clear string */
  }
  /* these are written sometimes by V680.c */
  ss_printf(0, LINE_17+disp_offset,"                                                                                  "); 
  ss_printf(0, LINE_19+disp_offset,"                                                                                  "); 
}

/*-display on screen ---------------------------------------------------*/
void v680_display(int onoff)
{
  printf("v680_display starting with onoff=%d\n",onoff);
  if (onoff >= 0)
  {
    if (onoff)
      v680_Display = onoff;
    else if (onoff == 0)
      v680_Display = onoff;
  }
  if(debug)printf("v680_display: v680_Display = %d\n",v680_Display);
  ss_clear_screen();
  ss_printf( 0, LINE_7,"                                                                                ");
  ss_printf( 0, LINE_8,"                                                                                ");
  ss_printf( 0, LINE_6+disp_offset,"                                                                                ");
  ss_printf( 0, LINE_8+disp_offset,"                                                                                ");
  ss_printf( 0,LINE_11+disp_offset,"                                                                                ");
  ss_printf( 0,LINE_14+disp_offset,"                                                                                ");
  v680_update();
  ss_printf( 0, LINE_3,"%d",max_channels);
  ss_printf(11, LINE_3,"%d",data_field);
  ss_printf(24, LINE_3,"%8.8x",data_mask);
  ss_printf(36, LINE_3,"%d", data_shift);
  ss_printf(50, LINE_3,"%d",total_bin);
  ss_printf(60, LINE_3,"%d",memory_size);
  ss_printf( 0, LINE_1,"==== v%.2f TD-MUSR Display === V680 TDC Micro Engine ON=========================",This_Version);
  ss_printf( 0, LINE_2,"#Chan.     D_field      D_mask      D_Shft        #bin      #bytes              ");
  ss_printf( 0, LINE_4,"%s",histo_order);
  ss_printf( 0, LINE_5
            ,"Map:0>%d 1>%d 2>%d 3>%d 4>%d 5>%d 6>%d 7>%d 8>%d 9>%d A>%d B>%d C>%d D>%d E>%d F>%d"
            ,*(pch_table),*(pch_table+1),*(pch_table+2),*(pch_table+3)
            ,*(pch_table+4),*(pch_table+5),*(pch_table+6),*(pch_table+7)
            ,*(pch_table+8),*(pch_table+9),*(pch_table+10),*(pch_table+11)
            ,*(pch_table+12),*(pch_table+13),*(pch_table+14),*(pch_table+15));

            ss_printf( 3, LINE_6,"Hit[0]   Hit[1]   Hit[2]   Hit[3]   Hit[4]   Hit[5]   Hit[6]   Hit[7]");
  if (disp_offset)
    ss_printf(3, LINE_6+disp_offset,"Hit[8]   Hit[9]   Hit[10]  Hit[11]  Hit[12]  Hit[13]  Hit[14]  Hit[15]");
  ss_printf( 0, LINE_9+disp_offset,"--------------------------------------------------------------------------------");
  ss_printf( 0, LINE_10+disp_offset,"Chan.    Reg_H    Reg_M    Reg_L    Base       Data        Incr.Add");
  ss_printf( 0,LINE_12+disp_offset,"--------------------------------------------------------------------------------");
  ss_printf( 0,LINE_13+disp_offset,"#Hits     #Inval    #OverF    #Mis      Poll      Cur_pol   #Poll     Status    ");
  ss_printf( 0,LINE_15+disp_offset,"--------------------------------------------------------------------------------");
  ss_printf( 0,LINE_16+disp_offset,"Last Message:  ------------------------------------------------------------------");
  last_message = LINE_16;  /* remember last message line for V680.h */
  ss_printf( 0,LINE_18+disp_offset,"=================================================================================\n");
}


/*- v680 initialization ---------------------------------------------------*/
INT v680_init(BOOL flag)
{
  /* called from frontend_init with flag = FALSE
             and begin_of_run  with flag = TRUE

     v680 display should be  turned off
   */
  
  INT status;
  
  first_time_display = TRUE; /* set flag so clock hang test works even if v680_Display is off */
  if(debug)printf("v680_init starting\n");

#ifdef VXWORKS
  pbchn = (char *)(&dataM);
#else // VMIC bytes reversed
  pbl =  (char *)(&dataM);
  pbchn = ++pbl;
#endif
  pch_table  = channel_table;
  
  /* Reset all channels + gate + uengine */
  v680RegWrite(myvme, V680_BASE, V680_RESET, V680_BRESET);
  printf("v680_init: wrote 0x%x to V680_RESET reg at offset 0x%x\n",
	     V680_BRESET,V680_RESET);

  if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
    Total_Bin = Total_Byte  >> 1 ;
  else
    Total_Bin = Total_Byte  >> 2 ; 
  if(debug)
    {
      printf("v680_init: maximum bins available Total_Bin:  %d\n", Total_Bin);
      printf("v680_init: calling mem_cleanup ...\n"); 
    }

  /* cleanup histo memory */
  status = mem_cleanup();
  if(debug)printf("v680_init: returned from  mem_cleanup ...\n"); 
  if (status != SUCCESS) 
  {
    printf("v680_init: failure return from mem_cleanup\n");
    return(status);
  }
  
  /* init histo memory */
  if(debug)printf("v680_init: calling mem_init ...\n"); 
  status = mem_init();
  if (status != SUCCESS) 
  {
    printf("v680_init: failure return from mem_init\n");
    return(status);
  }
  if(flag) /* frontend_init does not care about counter mapping */
  {
    status = setup_histo_index();
    if (status != SUCCESS) 
    {
      printf("v680_init: failure return from setup_histo_index\n");
    }
  }
  /* clear memory + counters */
  if(debug)printf("v680_init: calling v680_mem_clear...\n"); 
  v680_mem_clear(CLEAR_ALL);
   if(debug)printf("v680_init: dbg returned from v680_mem_clear...\n"); 

   
   
  hitsAddress = (DWORD *) hits; 
  local = 0;
  last_display = 0;


  
/* init positive time + internal clock */

  WORD data=v680RegWrite(myvme, V680_BASE, V680_CTL,
               (WORD)(V680_BPOS | V680_BSYNC | V680_BMUEON) );
  /*  *ctl_reg  = (WORD)(V680_BPOS | V680_BSYNC | V680_BMUEON); VxWorks */
  printf("v680_init: wrote 0x%x to control reg (offset 0x%x). Read back 0x%x\n",
	 (V680_BPOS | V680_BSYNC | V680_BMUEON),V680_CTL,data); 

  v680CntrlStatus(myvme, V680_BASE);

  return(SUCCESS);
}

/*- v680 memory + counter clear ---------------------------------------------------*/
void  v680_mem_clear(int unmap_pat)
{
  INT i, j;
  INT offset; 
  DWORD map_pat = 0;
  
  if(debug)
  {
    printf("\nv680_mem_clear starting with unmap_pat=0x%x\n",unmap_pat);
    printf("    and with pmemBase4=%p, pmemBase2=%p\n",pmemBase4,pmemBase2);
  }
  /* translate unmapped to mapped channel# */
  for (i=0;i<16;i++)
    if (unmap_pat & (1<<i))
      map_pat |= 1<<channel_table[i];
  
  if(debug)
  {
    printf("unmap_pat:%x map_pat:%x info.nbins:0x%x\n",unmap_pat, map_pat,
           info.nbins);
    for(j=0;j<16;j++)
      printf("Table[%i]=%x\n",j,channel_table[j]); 
  }
  for (i=0;i<16;i++)
  {   	     
    
    if (map_pat & (1<<i))
    {
      
      /* clear histo */
      offset=i*4*info.nbins;
      
      if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
      {
        memset((char *) pmemBase2+(i*2*info.nbins),0, 2*info.nbins);
        if(debug)
	  printf("clearing histo i: %i offset:%i start address %p\n",
		 i,offset, pmemBase2+(i*2*info.nbins));
      }
      else
      {
        memset((char *) pmemBase4+(i*4*info.nbins),0, 4*info.nbins);
        if(debug)
	  printf("clearing histo i: %i offset:%i start address %p\n",
		 i,offset, pmemBase4+(i*4*info.nbins));
      }
      /* clear corresponding counter */
      hits[4+i] = 0;
      svhits[4+i] = 0;
    }
  }
  
  /* Clear histo memory */
/*-PAA
  memset((char *) pmemBase,0, memory_size);
*/

  if(unmap_pat == CLEAR_ALL)
  {
    /* Clear first 4 hit counters as well */
    
    for (j=0;j<4;j++)
      hits[j]=0;
  }

  if(debug)
    printf("v680_mem_clear ending\n");
}
/* ------------------------------------------------------------------------- */

void v680_restart (void)
{
  /* Reset TDC and Re-enable*/
  WORD  value=0; /* ctl is a global, STATIC WORD */


  ctl =  v680RegRead(myvme, V680_BASE, V680_CTL); /* read the control register (message below) */

  /*  Reset all channels + gate + uengine */
  v680RegWrite(myvme, V680_BASE, V680_RESET, V680_BRESET);  /* reset */
  value = v680RegWrite(myvme, V680_BASE, V680_CTL, (WORD)(V680_BPOS | V680_BSYNC | V680_BMUEON) ); /* re-enable */

  /* Old Code:
     *reset_reg = (WORD) V680_BRESET;
     *ctl_reg  = (WORD)(V680_BPOS | V680_BSYNC | V680_BMUEON);
  */
  printf("v680_restart: Enabling gate, ctl reg previously 0x%x; now 0x%x\n",ctl,value);

  return;
}

/* ------------------------------------------------------------------------- */
void v680_start (void)
{
  WORD  value=0; /* ctl is a global, STATIC WORD */

  
  /* get ctl reg */

  /* enable gate */
  ctl =  v680RegRead(myvme, V680_BASE, V680_CTL);
  value= v680RegWrite(myvme, V680_BASE, V680_CTL,( ctl | (WORD)(V680_BGATE))  );

  /* old code:
    ctl = *ctl_reg; 
    *ctl_reg =  ctl | (WORD)(V680_BGATE);
  */

  if(debug) printf("v680_start: Enabling gate, ctl reg was 0x%x, now 0x%x\n",ctl,value);
  return;
}
 
/*- v680 register disable ---------------------------------------------------*/
void v680_stop(void)
{
   WORD  value=0; /* ctl is a global, STATIC WORD */
  
  /* get ctl reg */

   ctl =  v680RegRead(myvme, V680_BASE, V680_CTL);
  /* disable gate */
  /*
      ctl = *ctl_reg; 
      *ctl_reg =  ctl & ~(WORD)(V680_BGATE);
  */
   value= v680RegWrite(myvme, V680_BASE, V680_CTL,( ctl & ~(WORD)(V680_BGATE)   )  );

  if(debug) printf("v680_stop: Disabling gate, ctl reg was 0x%x, now 0x%x\n",ctl,value);
}

/* - set_params() -----------------------------------------------------------*/
INT set_params()
{
  /* called from  begin_of_run

     get_record must be called for the latest settings before
     this routine

   */
  
  /* for now, keep the structure info */
  info.nbins   = vs.number_of_bins;
  info.bit_pattern = vs.counter_bit_pattern;
  if(debug)
    printf("set_params: vs.counter_bit_pattern = 0x%x (%d) , info.bit_pattern = 0x%x (%d)\n",
	   vs.counter_bit_pattern, vs.counter_bit_pattern, info.bit_pattern, info.bit_pattern);

  info.shift = vs.shift; /* or tdc_res -16 */
  info.bytes_per_bin = vs.bytes_per_bin;
  info.ioreg_bit_pattern = vs.ioreg_bit_pattern;
  v680_Display  =  vs.display;
    
  
  printf ("Settings:\n");
  printf ("Number of bins     = %d\n",info.nbins);
  printf ("TDC Bit pattern    = 0x%x (%d) \n",info.bit_pattern,info.bit_pattern);
  printf ("Shift              = %d\n" ,info.shift);
  printf ("Bytes per bin      = %d\n",info.bytes_per_bin);
  printf ("IO reg bit pattern = 0x%x (%d) \n",
	  info.ioreg_bit_pattern, info.ioreg_bit_pattern);

  printf("  data gate      : 0x%x (%d)counts \n",vs.delta,vs.delta);
  printf("  TDC gate       : 0x%x (%d)counts \n",vs.delta1,vs.delta1);
  printf("  pileup gate       : 0x%x (%d)counts \n",vs.delta2,vs.delta2);


  if(v680_Display==1)
    printf("v680 display   = %d (enabled)\n",v680_Display);
  else
    printf("v680 display   = %d (disabled)\n",v680_Display);

  if(info.nbins <= 0 )
  {
    cm_msg(MERROR,"set_params","invalid number of bins (%d)",info.nbins);
    status = FE_ERR_HW;
  }
  if(info.bit_pattern == 0 || info.bit_pattern > 0xFFFF)
  {
    cm_msg(MERROR,"set_params","invalid bit pattern 0x%x (%d)",info.bit_pattern,info.bit_pattern);
    status = FE_ERR_HW;
  }
  if(info.shift < 0 )
  {
    cm_msg(MERROR,"set_params","invalid shift (%d)",info.shift);
    status = FE_ERR_HW;
  }
  if(info.bytes_per_bin != 2 && info.bytes_per_bin != 4  )
  {
    cm_msg(MERROR,"set_params","invalid bytes per bin (%d)",info.bytes_per_bin);
    status = FE_ERR_HW;
  }
  if(debug)
    printf("set_params returning \n");
  return (status);
}

/* ---------------------------------------------------------------------- */


/* - set_default_params() -----------------------------------------------------------*/
void set_default_params()
{
  /* called from frontend_init

     set DEFAULT parameters so frontend does not fail because of
     bad parameters on bootup
     
   */
  
  info.nbins   = 100;
  info.bit_pattern = 0x01;
  info.shift = 0; /* or tdc_res -16 */
  info.bytes_per_bin = 4;
  info.ioreg_bit_pattern = 0;
  v680_Display  =  0;
    
  if(debug)
  {
    printf ("Default Settings:\n");
    printf ("Number of bins = %d\n",info.nbins);
    printf ("Bit pattern    = 0x%x (%d) \n",info.bit_pattern,info.bit_pattern);
    printf ("Shift          = %d\n" ,info.shift);
    printf ("Bytes per bin  = %d\n",info.bytes_per_bin);
    printf ("IO reg bit pattern = 0x%x\n",info.ioreg_bit_pattern);
    if(v680_Display==1)
      printf("v680 display   = %d (enabled)\n",v680_Display);
    else
      printf("v680 display   = %d (disabled)\n",v680_Display);

    printf("set_default_params returning \n");
  }
  return;
}
/* ------------------------------------------------------------------- */
INT scaler_start()
{
  /* Called at begin-of-run */
  DWORD i;
  char str[128];

  status = SUCCESS;
  if (fs.enabled)
  {

    if (dss)printf("scaler_start: clearing and enabling scaler\n");

    sis3803_all_clear(myvme,  SIS_base_adr);     /* clear just in case */
    sis3803_all_enable(myvme, SIS_base_adr);    /* enable global counting so module acquires counts */ 
    if (vs.flags.test_pulses)    /* test pulse mode */
      sis3803_test_pulse(myvme, SIS_base_adr); /* pulse scaler; pulsed again each scaler read */

      time_last_rates = ss_millitime(); /* get present time for rates calculation */
    /* clear the overflow */
    i=0;
    sprintf(str,"/equipment/scaler/variables/overflow");
    status=db_set_value (hDB, 0, str,
                         &i,sizeof(i), 1, TID_DWORD);
    if (status != DB_SUCCESS)
      cm_msg(MINFO,"scaler_start","could not set %s (%d) ",str,status);

    first_overflow=TRUE; /* flag to avoid repeat messages on overflow */
  }
  else
    if (dss)printf("scaler_start: scaler is disabled\n");

  if(dss)SIS3803_CSR_read(myvme, SIS_base_adr);

  return status;
}

INT scaler_clear_rates(void)
{
  /* clear scaler rates in odb */
  double zero[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  char str[128];
  HNDLE hScaler;
  KEY hKScaler;
  //  INT i;

  sprintf(str,"/equipment/rscal/variables/rate");
  status = db_find_key(hDB, 0, str, &hScaler); 
  if (status != DB_SUCCESS)
    {
      cm_msg(MINFO,"scaler_clear_rates","could not find key for %s (%d) ",str,status);
      return (status);
    }
    
  status = db_get_key(hDB, hScaler, &hKScaler);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"scaler_clear_rates","cannot get key %s (%d)", str,status);
      return status;
    }
  if(debug)
    printf("scaler_clear_rates: scaler rate array currently has %d elements\n",hKScaler.num_values);

  if (hKScaler.num_values > 0)
    {
      /* zero the scaler array; hKScaler.num_values gives the number of values  */
      status=db_set_value (hDB, 0, "/equipment/rscal/variables/rate",
			   &zero, hKScaler.num_values * sizeof(double), hKScaler.num_values , TID_DOUBLE);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MINFO,"scaler_clear_rates","could not set value %s for %d elements (%d) ",str, hKScaler.num_values,status);
	  return status;
	}
      else if (debug)
	printf("scaler_clear_rates: zeroed scaler rates\n");	
    }
  return status;
}

void scaler_stop()
{
  /* called at end-of-run and pause */
  if(fs.enabled)
  {
    if(dss)printf("scaler_stop: disable scaler\n");

    sis3803_all_disable(myvme, SIS_base_adr);    /* disable global counting */ 

  }
  return;
}

/*----scaler_init() -----------------------------------------------*/
INT scaler_init () 
{
/* called from frontend_init and begin-of-run */
  
  int i = 0;
  //  int chan_field;
  char str[128];
  INT size,struct_size;
  
  

/* the initial scaler info is copied into /equipment/scaler/settings
   by musr_conf
*/
  time_last_rates = 0 ; /* initialize time rates were last calculated */
  for (i=0; i< MAX_SCALER; i++)
    {
      scaler_counts[i]=0;
      old_counts[i]=0;
    }
  status = scaler_clear_rates();
  if(status != DB_SUCCESS) return status;

  sprintf(str,"/equipment/scaler/settings");
  
  /* check the record size is as expected => size */
  status = db_get_record_size(hDB, hSet, 0, &size);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "scaler_init", "error during get_record_size (%d) for %s",status,str);
    return status;
  }
  
  
/* get the record for the scaler settings */
  struct_size = sizeof(fs);
  status = db_get_record(hDB, hSet, &fs, &struct_size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "scaler_init", "cannot retrieve %s record (size = %d, structure size =%d)",
           str,size,struct_size);
    return status;
  }

  /* reset and clear scaler */

  sis3803_module_reset(myvme, SIS_base_adr);
  sis3803_all_clear(myvme, SIS_base_adr);
  sis3803_input_mode(myvme, SIS_base_adr, 1);            /* mode 1 ONLY */
 
  if(fs.enabled)
  {
    if(dss)printf ("scaler_init: scaler is  enabled (%d)\n", fs.enabled); 
    
    /* check the number of scaler channels */
    if (fs.num_inputs < 0 || fs.num_inputs > 16  ) /* top 4 scaler inputs may be reserved for IMUSR */
    {
      cm_msg(MERROR,"scaler_init","scaler is enabled with illegal number of inputs (%d)\n",fs.num_inputs);
      return (-1);
    }
    

    
    /* find the largest scaler channel to be read out from the bit pattern */
    for(i=15; i>=0; i--) /* max input index is 15 (channel 16) */
      {
	if( fs.bit_pattern & ( 1 << i) )    /* scaler bit pattern calculated by musr_config */
	  break;
      }
    max_scaler_input = i;    /* will readout max_scaler_input words from the scaler */
    if(dss)printf("scaler bit pattern=0x%x, largest scaler channel = %d\n",fs.bit_pattern,max_scaler_input );
    
    /* selective enable pattern depends on num channels */
    if(dss)printf("calling sis3803_channel_enable\n");
    sis3803_channel_enable(myvme, SIS_base_adr,fs.bit_pattern); 


    if(dss)printf("calling scaler_setup_test\n");

    /* check for test modes and enable if selected */ 
    scaler_setup_test();
 

    if(debug)
      SIS3803_CSR_read(myvme, SIS_base_adr);

    /* do not enable scaler yet - done by scaler_start() */


    /* clear the scaler overflow in odb */
    i=0;
    sprintf(str,"/equipment/scaler/variables/overflow");
    status=db_set_value (hDB, 0, str,
                         &i,sizeof(i), 1, TID_DWORD);
    if (status != DB_SUCCESS)
      cm_msg(MINFO,"scaler_init","could not set %s (%d) ",str,status);

    if(dss)printf("end of scaler enabled\n");

  } /* end of if fs.enabled */
  
  else   /* scaler is disabled */
  {
    if(dss)printf ("scaler_init: scaler is disabled  (%d)\n", fs.enabled); 
    fs.num_inputs = fs.bit_pattern = 0; /* set to  0 if disabled */ 
    sis3803_all_disable(myvme, SIS_base_adr); /* disable global counting */ 
  }
  
  if(dss)printf("scaler_init ending\n");
  
  return SUCCESS;
}


/*----vmeio_init() -----------------------------------------------*/
INT vmeio_init () 
{
  INT i;
  DWORD state;// mode
/* called from frontend_init
   Initialize vmeio and set all levels OFF 
   Even if vmeio is not present in crate, we get no errors
 */

  if(debug)printf("vmeio_init starting\n");

  musrVmeioReset(); /* reset all software definitions (must be followed by musrVmeioInit) */
  musrVmeioInit(vmeio_base); /* sets VME base address in structure */

  for (i=0; i < 16 ; i++)  /* MUSR is using maximum of 16 output channels of IO register  */
    {
      status = musrVmeioLatchSet(myvme, i); /* initialize - set all the channels to Latch mode */
      if(status != UVIO_SUCCESS)
      {
	printf("vmeio_init: bad status (%d) after selecting latch mode for channel %d\n",status,i);
	return(status);
      }
      status = musrVmeioOff(myvme, i);  /* turn all channels OFF */
      if(status != UVIO_SUCCESS)
      {
	printf("vmeio_init: bad status (%d) after setting Latch Off for channel %d\n",status,i);
	return(status);
      }
    }

    /* there seems to be no way to check the module is present
       except musrVmeioInputState() seems to give 0xffffff if module not in crate instead of 0
       There is no hardware readback of the state of the output channels 
   */

  state =  musrVmeioInputState(myvme); /* hardware access */
  if( state != 0 )
  {
    printf("vmeio_init: read input state = 0x%x; expect 0\n",state);
    cm_msg(MERROR,"vmeio_init","IO board is not responding");
    return(-1);
  }
  
  /* Both these should be zero for Latch, all OFF (software only no hardware access) */
  if(debug)
    printf("vmeio_init: State:0x%x  Mode:0x%x\n", musrVmeioOutputState(), musrVmeioOutputMode() );

  printf("vmeio_init: set all output channels to OFF\n");
  return(SUCCESS);
}
/*-----------vmeio_on()-------------------- */
void vmeio_on()
{
  INT i;

  /* Called at begin-of-run and resume 
     set ON the output levels of the ioreg according to bit pattern 
     (includes dual spectra mode bit (bit 15) )
  */

  if(info.ioreg_bit_pattern == 0) /* io register is disabled */
  {
    if(debug)printf("vmeio_on: returning as ioreg_bit_pattern = 0\n");
    return;
  }
  for (i=0; i < 16 ; i++)  /* Output register 16 channels max */
    {
      if (info.ioreg_bit_pattern & (1 << i) )
	{ 
	  if(debug)
	    printf("vmeio_on: setting ON output reg channel %d \n",i);

	  status = musrVmeioOn(myvme, i);
	  if(status != UVIO_SUCCESS)
	  {
	    printf("vmeio_on: bad status (%d) after setting Latch On for channel %d\n",status,i);
	     cm_msg(MERROR,"vmeio_on"," Warning: bad status (%d) after setting Latch On for channel %d",
		    status,i) ;
	     return;
	  }
	}
    }
  /* State and bitpat should be identical but cannot read back hardware state (i.e. software check only) */
  printf("vmeio_on: Bit pattern of output channels set ON :0x%x  (State:0x%x; Mode:0x%x) \n",
	  info.ioreg_bit_pattern, musrVmeioOutputState(), musrVmeioOutputMode() );

  return;
}


/*-----------vmeout_off()-------------------- */
void vmeio_off()
{
  int i;
  //  DWORD state;
  /* set OFF the output levels of the ioreg according to the bit pattern
     (includes dual spectra mode bit)
     NOT called by pause and end-of-run (MUSR prefer it like this)
  */
  if(info.ioreg_bit_pattern == 0) /* io register is disabled */
  {
    if(debug)printf("vmeio_off: returning as ioreg_bit_pattern = 0\n");
    return;
  }

  for (i=0; i < 16 ; i++)  /* Output register 16 channels max */
    {
      if (info.ioreg_bit_pattern & (1 << i) )
	{ 
	  if(debug)
	    printf("vmeio_off: setting OFF output reg channel %d \n",i);

	  status = musrVmeioOff(myvme, i);
	  if(status != UVIO_SUCCESS)
	  {
	    printf("vmeio_off: bad status (%d) after setting Latch Off for channel %d\n",status,i);
	    cm_msg(MERROR,"vmeio_off"," Warning: bad status (%d) after setting Latch Off for channel %d",
		   status,i) ;
	    return;
	  }
	}
    }
  
  printf("vmeio_off: State:0x%x  Mode:0x%x\n",musrVmeioOutputState(),musrVmeioOutputMode());
  return;
}

/*---------hot_display ---------------------------------------------*/
void hot_display (HNDLE hDB, HNDLE hktmp ,void *info)
{
  INT onoff;

  
  /* key "display" has been touched - update display
       */
  if(debug)cm_msg(MINFO,"hot_display","key v680/display has been touched"); 
  onoff=vs.display;
  v680_display(onoff);
  set_v680_display(v680_Display,disp_offset,last_message); /* send info to V680.c */
}

/*---------hot_restart ---------------------------------------------*/
void hot_restart (HNDLE hDB, HNDLE hktmp ,void *info)
{

  /* key "restart" has been touched 
  */
  
  printf("hot_restart: key v680/commands/restart has been touched\n"); 


  if (run_state != STATE_STOPPED)
  {
      cm_msg(MERROR,"hot_restart","cannot restart clock unless run is STOPPED");
      return;
  }
  v680_restart();
}
/*---------hot_set_clock_hang ---------------------------------------------*/
void  hot_set_clock_hang (HNDLE hDB, HNDLE hktmp ,void *info  )
{
  INT type; /*  type=0,1,2,3 (MUE OFF FGATE ON GTEST ON TTEST ON) */
  /* key set clock hang has been touched 
       */
  if(debug)cm_msg(MINFO,"hot_set_clock_hang","key set clock hang has been touched"); 
  type = vs.commands.set_clock_hang;
  if(type < 0  || type > 3)
    printf("hot_set_clock_hang: invalid clock hang type (%d) (0-3) \n",type);

  else
    v680MakeClockHang(myvme, V680_BASE, type); /*  type=0,1,2,3 (MUE OFF FGATE ON GTEST ON TTEST ON) */

}
/*---------hot_clear_clock_hang ---------------------------------------------*/
void  hot_clear_clock_hang (HNDLE hDB, HNDLE hktmp ,void *info  )
{
  /* key clear clock hang has been touched 
       */
  if(debug)cm_msg(MINFO,"hot_clear_clock_hang","key clear clock hang has been touched");

  v680ClearClockHang(myvme, V680_BASE); /* clear clock hang */

}
/*---------hot_dump_regs ---------------------------------------------*/
void  hot_dump_regs (HNDLE hDB, HNDLE hktmp ,void *info  )
{
  INT all;
  
  /* key dump regs has been touched 
       */
  if(debug)cm_msg(MINFO,"hot_dump_regs","starting ..... key dump regs has been touched.   \n");
  all = vs.commands.dump_regs;
  
  if ( all != 1)
    all=0;   /* MUE only */

  v680DumpRegs(myvme, V680_BASE, all); /* 0 = dump MUE regs only, 1=all */

}
/*---------hot_zero_run ---------------------------------------------*/
void  hot_zero_run (HNDLE hDB, HNDLE hktmp ,void *info  )
{
  /* key zero run has been touched
     clear all histograms
 */

  if(debug)cm_msg(MINFO,"hot_zero_run","key zero run has been touched"); 
  
  v680_mem_clear(CLEAR_ALL);

/*---------hot_zero_some ---------------------------------------------*/  
}
void  hot_zero_some (HNDLE hDB, HNDLE hktmp ,void *info  )
{
  /* key "zero some" has been touched */
  INT size,status; 
  if(debug)cm_msg(MINFO,"hot_zero_some","key \"zero some\" has been touched"); 
  
  /* value is the bitpattern of TDC inputs to be cleared */
  size = sizeof(vs.commands.zero_some);
  status=db_get_value (hDB, hV , "commands/zero some", &vs.commands.zero_some,
		       &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg (MERROR,"hot_zero_some","Error getting value for key /equipment/v680/commands/zero some (%d) ",status);
    return ;
  }
  printf("hot_zero_some: calling v680_mem_clear with bitpattern 0x%d\n",vs.commands.zero_some);
  v680_mem_clear(vs.commands.zero_some);
}
  
/*---------hot_zero_scaler ---------------------------------------------*/  
void  hot_zero_scaler (HNDLE hDB, HNDLE hktmp ,void *info  )
{
  INT i;
  /* key zero scaler has been touched


  Clear all scaler channels
  Don't clear overflow in odb - the next scaler event should update overflow 

 */

  sis3803_all_clear(myvme, SIS_base_adr);

  cm_msg(MINFO,"hot_zero_scaler","clearing all scaler channels");
  time_last_rates =  ss_millitime();  /* initialize time for rates  */
  for (i=0; i< MAX_SCALER; i++)
  {
    scaler_counts[i]=0;
    old_counts[i]=0;
  }
}

/*---------hot_zero_scaler_some ---------------------------------------------*/
void  hot_zero_scaler_some (HNDLE hDB, HNDLE hktmp ,void *info  )
{
  /* key "zero scaler some" has been touched

  Clear one or more scalers according to the bit pattern
  Don't clear overflow in odb - the next scaler event should update overflow 

 */
  INT size,status,i; 
  if(debug)printf("hot_zero_scaler_some: key \"zero scaler some\" has been touched\n"); 
  
  /* value is the bitpattern of scaler channels to be cleared */
  size = sizeof(vs.commands.zero_scaler_some);
  status=db_get_value (hDB, hV , "commands/zero scaler some",
		       &vs.commands.zero_scaler_some, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg (MERROR,"hot_zero_scaler_some",
            "Error getting value for key /equipment/v680/commands/zero scaler some (%d) ",status);
    return ;
  }
  if(vs.commands.zero_scaler_some == 0)
    return;   /* nothing to clear */

  for (i=0;i<16;i++)
  {
    if (vs.commands.zero_scaler_some  & (1<<i))
    {
      cm_msg(MINFO,"hot_zero_scaler_some","clearing scaler channel %d",i);
      sis3803_single_clear(myvme, SIS_base_adr, i);
    }
  } 
}

/*---------hot_delta ---------------------------------------------*/
void hot_delta (HNDLE hDB, HNDLE hDelta ,void *info)
{
  /* key "delta" has been touched - update mode area and write new value to ggl */
  INT data;

  if(debug)cm_msg(MINFO,"hot_delta","key v680/delta has been touched"); 

  data = gglWriteDelta(myvme, ggl_base, vs.delta); /* send info to ggl */
  if(ddd)printf("Wrote %d counts to Delta delay register; read back %d\n",vs.delta,data);
  cm_msg(MINFO,"hot_delta","wrote Data Gate delay delta=%d to GGL (read back %d) ",vs.delta,data);

}

/*---------hot_delta1 ---------------------------------------------*/
void hot_delta1 (HNDLE hDB, HNDLE hDelta1 ,void *info)
{
  /* key "delta1" has been touched - update mode area and write new value to ggl */
  INT data;

  if(debug)cm_msg(MINFO,"hot_delta1","key v680/delta1 has been touched"); 
  data = gglWriteDelta1(myvme, ggl_base, vs.delta1); /* send info to ggl */
  if(ddd)printf("Wrote %d counts to Delta1 delay register; read back %d\n",vs.delta1,data);
  cm_msg(MINFO,"hot_delta1","wrote TDC  Gate delay delta1=%d to GGL (read back %d) ",vs.delta1,data);
}

/*---------hot_delta2 ---------------------------------------------*/
void hot_delta2 (HNDLE hDB, HNDLE hDelta2 ,void *info)
{
  /* key "delta2" has been touched - update mode area and write new value to ggl */
  INT data;

  if(debug)cm_msg(MINFO,"hot_delta2","key v680/delta2 has been touched"); 
  data = gglWriteDelta2(myvme, ggl_base, vs.delta2); /* send info to ggl */
  if(ddd)printf("Wrote %d counts to Delta2 delay register; read back %d\n",vs.delta2,data);
  cm_msg(MINFO,"hot_delta2","wrote Pileup gate delay delta2=%d to GGL (read back %d) ",vs.delta2,data);
}

#ifdef GONE /* now in musr_common_subs */
/*--------- write_message1 ---------------------------------------------*/  
void
write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (
    status == DB_NO_KEY) sprintf (str,"because   key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}

#endif


INT setup_hotlink(void)
{
  HNDLE hktmp;
  
  /* set a hot link on v680 odb key "display"  */

  status=db_find_key(hDB, hV, "display", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key for v680 display   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.display);
  status = db_open_record(hDB, hktmp, &vs.display, size, MODE_READ, hot_display, NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: v680/display  (%d)", status );
    return(status);
  }

  /* set a hot link on odb key "dump regs"  */

  status=db_find_key(hDB, hV, "commands/dump regs", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key: commands/dump regs   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.dump_regs);
  status = db_open_record(hDB, hktmp, &vs.commands.dump_regs, size, MODE_READ, hot_dump_regs, NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: commands/dump regs  (%d)", status );
    return(status);
  }

 /* set a hot link on odb key "commands/restart clock"  */
  status=db_find_key(hDB, hV, "commands/restart clock", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key \"commands/restart clock\" (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.restart_clock);
  status = db_open_record(hDB, hktmp, &vs.commands.restart_clock, size, MODE_READ, hot_restart, NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key \"commands/restart clock\" (%d)", status );
    return(status);
  }

  /* set a hot link on odb key "commands/set clock hang"  */

  status=db_find_key(hDB, hV, "commands/set clock hang", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key: commands/set clock hang   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.set_clock_hang);
  status = db_open_record(hDB, hktmp, &vs.commands.set_clock_hang, size, MODE_READ, hot_set_clock_hang, NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: commands/set clock hang  (%d)", status );
    return(status);
  }

  /* set a hot link on odb key "commands/clear clock hang"  */

  status=db_find_key(hDB, hV, "commands/clear clock hang", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key: commands/clear clock hang   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.clear_clock_hang);
  status = db_open_record(hDB, hktmp, &vs.commands.clear_clock_hang, size, MODE_READ, hot_clear_clock_hang, NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: commands/clear clock hang  (%d)", status );
    return(status);
  }


 /* set a hot link on odb key "commands/zero run"  */

  status=db_find_key(hDB, hV, "commands/zero run", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key: commands/zero run   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.zero_run);
  status = db_open_record(hDB, hktmp, &vs.commands.zero_run, size, MODE_READ, hot_zero_run , NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: commands/zero run  (%d)", status );
    return(status);
  }

 /* set a hot link on odb key "commands/zero some"  */

  status=db_find_key(hDB, hV, "commands/zero some", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key: commands/zero some   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.zero_some);
  status = db_open_record(hDB, hktmp, &vs.commands.zero_some, size, MODE_READ, hot_zero_some , NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: commands/zero some  (%d)", status );
    return(status);
  }

   /* set a hot link on odb key "commands/zero scaler"  */

  status=db_find_key(hDB, hV, "commands/zero scaler", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key: commands/zero scaler  (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.zero_scaler);
  status = db_open_record(hDB, hktmp, &vs.commands.zero_scaler, size, MODE_READ, hot_zero_scaler , NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: commands/zero scaler (%d)", status );
    return(status);
  }

 /* set a hot link on odb key "commands/zero scaler some"  */

  status=db_find_key(hDB, hV, "commands/zero scaler some", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key: commands/zero scaler some   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.commands.zero_scaler_some);
  status = db_open_record(hDB, hktmp, &vs.commands.zero_scaler_some, size, MODE_READ, hot_zero_scaler_some , NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink",
           "Failed to open record (hotlink) for key: commands/zero scaler some  (%d)", status );
    return(status);
  }

  return SUCCESS;
}

INT setup_hotlink_BOR(void)
{
/*  

   These hotlinks should be opened by BOR and closed at EOR

*/
  printf("setup_hotlink_BOR: Setting up hotlinks for delta,delta1,delta2\n");
  /* delta */
  status=db_find_key(hDB, hV, "delta", &hDelta);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key  \"delta \"   (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }   

  size = sizeof(vs.delta);
  status = db_open_record(hDB, hDelta, &vs.delta, size, MODE_READ, hot_delta, NULL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: v680/delta  (%d)", status );
      return(status);
    }
 
  /* delta1 */
  status=db_find_key(hDB, hV, "delta1", &hDelta1);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key  \"delta1 \"   (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }   

  size = sizeof(vs.delta1);
  status = db_open_record(hDB, hDelta1, &vs.delta1, size, MODE_READ, hot_delta1, NULL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: v680/delta1  (%d)", status );
      return(status);
    }

   /* delta2 */
  status=db_find_key(hDB, hV, "delta2", &hDelta2);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key  \"delta2 \"   (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }   

  size = sizeof(vs.delta2);
  status = db_open_record(hDB, hDelta2, &vs.delta2, size, MODE_READ, hot_delta2, NULL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: v680/delta2  (%d)", status );
      return(status);
    }
 
  return SUCCESS;
}

/* ---------------------------------------------------------------------------------------------------- */

void  close_hotlinks()
{
  /* called at EOR to close BOR hotlinks */

  /* remove hot link on "delta" */
  
  if(hDelta) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"delta\", hDelta=%d\n",hDelta);
    status = db_close_record(hDB, hDelta);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for delta(%d)\n",status);

  }

  /* remove hot link on "delta1" */

  if(hDelta1) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"delta1\", hDelta1=%d\n",hDelta1);
    status = db_close_record(hDB, hDelta1);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for delta1(%d)\n",status);

  }

  /* remove hot link on "delta2" */

  if(hDelta2) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"delta2\", hDelta2=%d\n",hDelta2);
    status = db_close_record(hDB, hDelta2);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for delta2(%d)\n",status);

  }

  hDelta=hDelta1=hDelta2=0;
}

/*-- Check  musr_config has recently updated odb area ---------------------------------- ------*/
INT check_update_time(INT max_sec)
{
/*  Called by begin_of_run

  **** Call AFTER set_params because set_params updates the v680 record ****

          
   Checks time since odb area 
   /equipment/musr_td_acq/settings/output/selected_histograms
   (and other areas)
         were last updated by musr_config

  Input: number of seconds above which check fails
                (musr_config must have run < max_sec seconds ago) 
         
  returns: success or fail status
         
       note: get the present binary time; the ascii time does not seem to be
             local time, but the binary time can be used
      */

  /* needed to get present time */
  time_t timbuf;
  char timbuf_ascii[30];
  INT elapsed_time;
  
  BOOL check;
  char str[132];
  INT size,status;
  HNDLE htmp;

  if(debug) printf ("check_update_time: starting with max_sec = %d \n",max_sec);

  
  if(max_sec <=0 )
  {
      cm_msg(MERROR,"check_update_time",
             "Invalid input parameter max_sec (%d) must be > 0",max_sec);
      return (DB_INVALID_PARAM);
  }

/* get the (latest value) of flag "check musr config". V680 record not yet
   updated at begin-of-run by set_params  */
  sprintf(str,"/Equipment/MUSR_TD_acq/v680/flags");
  /* find the key */
  status = db_find_key(hDB, 0, str, &htmp);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "check_update_time", "Cannot find key %s (%d) ",str,status);
    return FE_ERR_ODB;
  }
  size = sizeof(check);
  status = db_get_value(hDB, htmp, "check musr config" , &check, &size,
			TID_BOOL, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"check_update_time","cannot get value %s/check musr config (%d)",str,status);
    return(status);  /*  error return */
  }
  if(debug) printf ("check_update_time: check flag = %d \n",check);


  /* these contain  the ascii & binary time from odb : 
         vs.histograms.validated_at_time  (ascii)
         vs.histograms.binary_time        (binary)
  */
  

  /* get the present time */
  time(&timbuf);
  if(debug)
  {
    strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) );         
    printf ("Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf);   
    printf("According to odb, musr_config last updated parameters at:  %s, binary=%d\n",
           vs.histograms.validated_at_time, vs.histograms.binary_time );
  }
  
  elapsed_time= (INT) ( timbuf -  vs.histograms.binary_time );
  if(debug)
      printf("Time since file last updated: %d seconds, timbuf (%d)\n",
             elapsed_time,(int)timbuf);


  /* check elapsed time against maximum */
  if(elapsed_time > max_sec)
  {
    if(check)    /* check on musr_config running IS ENABLED :
                    don't let the run start if
                    file is not recent */
    {
      cm_msg(MERROR,"check_update_time",
       "According to odb, musr_config last updated parameters at: %s; elapsed time=%ds",
           vs.histograms.validated_at_time,elapsed_time);
      cm_msg(MERROR,"check_update_time","Make sure musr_config is running\n");
      return FE_ERR_HW;
    }
    else            /* send an informational message because check flag is off */
      cm_msg(MINFO,"check_update_time","Calculated run params not updated recently; musr_config may not be running\n");
  }
  return (SUCCESS);
}

/*----ggl_init() -----------------------------------------------*/
INT ggl_init (void) 
{
  INT data;

/* called at begin run
   Initialize gate generator logic board and load initial values for TD-MUSR
 */

  /* Write to SR_ENABLE reg */
  if ( (info.bit_pattern & 0xff00) != 0 )
    { /*  dual-spectra mode */
      data=gglWriteSR (myvme, ggl_base,1);
      printf("ggl_init:Dual Spectra Mode enabled, set SR Enable reg. to 1 ; read back %d \n",data);
    }
  else
    { /* NOT dual-spectra mode */
      data=gglWriteSR (myvme, ggl_base,0);
      printf("ggl_init:Dual Spectra Mode disabled, set SR Enable reg. to 0 ; read back %d \n",data);
    }
      

  printf("ggl_init: loading delays delta=%d counts,delta1=%d counts ,delta2=%d counts\n",
	 vs.delta,vs.delta1,vs.delta2 );

  data = gglWriteDelta(myvme, ggl_base, vs.delta);
  if(ddd)printf("Wrote %d counts to Delta delay register; read back %d\n",vs.delta,data);
  data = gglWriteDelta1(myvme, ggl_base, vs.delta1);
  if(ddd)printf("Wrote %d counts to Delta1 delay register; read back %d\n",vs.delta1,data);
  data = gglWriteDelta2(myvme, ggl_base, vs.delta2);
  if(ddd)printf("Wrote %d counts to Delta2 delay register; read back %d\n",vs.delta2,data);

  gglReadAll(myvme, ggl_base);
    
    return(status);
}

/*-- Event readout -------------------------------------------------*/

INT trigger_read(char *pevent, INT off)
{
  return (0);
}

/*-- Event readout imusr_acq   ------------------------------*/
INT imusr_acq (char *pevent, INT off)
{     /* IMUSR - dummy in this code */
  return (0);
}

/*-- Event readout send_data_point     ----------------------*/
INT send_data_point(char *pevent, INT off)
{      /* IMUSR - dummy in this code */
  return (0);
}

/*-- Event readout info_odb      ----------------------*/
INT info_odb (char *pevent, INT off)
{      /* IMUSR - dummy in this code */
  return (0);
}

#include "musr_common_subs.c" /* include subroutines common to IMUSR and TDMUSR */


INT enable_equipments(BOOL ival)
{
  // Enable/Disable td equipments depending on musr type running
  //       MUSR_TD_Acq, Histo, Scaler, Diag, Rscal
  INT status,status1,status2,status3,status4, status5, size;
  char str1[]="/Equipment/MUSR_TD_Acq/Common/Enabled";
  char str2[]="/Equipment/Histo/Common/Enabled";
  char str3[]="/Equipment/Scaler/Common/Enabled";
  char str4[]="/Equipment/Diag/Common/Enabled";
  char str5[]="/Equipment/Rscal/Common/Enabled";
  BOOL value;
  
  value=ival;
  size=sizeof(value);

  status1=db_set_value (hDB, 0, str1, &value, size, 1, TID_BOOL);
  status2=db_set_value (hDB, 0, str2, &value, size, 1, TID_BOOL);
  status3=db_set_value (hDB, 0, str3, &value, size, 1, TID_BOOL);
  status4=db_set_value (hDB, 0, str4, &value, size, 1, TID_BOOL);
  status5=db_set_value (hDB, 0, str5, &value, size, 1, TID_BOOL);
  
  status=status1 || status2 || status3 || status4 || status5 ;
  if (status != DB_SUCCESS)
    {
      if(status1 != DB_SUCCESS)
	cm_msg(MERROR,"enable_equipments","error writing value=%d to %s (%d) ",value,str1,status1);
      if(status2 != DB_SUCCESS)
	cm_msg(MERROR,"enable_equipments","error writing value=%d to %s (%d) ",value,str2,status1);
      if(status3 != DB_SUCCESS)
	cm_msg(MERROR,"enable_equipments","error writing value=%d to %s (%d) ",value,str3,status1);
      if(status4 != DB_SUCCESS)
	cm_msg(MERROR,"enable_equipments","error writing value=%d to %s (%d) ",value,str4,status1);
      if(status5 != DB_SUCCESS)
	cm_msg(MERROR,"enable_equipments","error writing value=%d to %s (%d) ",value,str5,status1);
      return FE_ERR_ODB;
    }

  return SUCCESS;
}


INT create_records()
{
  char str_set[128];
  INT size, struct_size;


  sprintf(str_set,"/Equipment/%s/v680",td_eqp_name); 

  /* find the key hV for v680 area */
  status = db_find_key(hDB, 0, str_set, &hV);
  if (status != DB_SUCCESS)
    {
      printf("create_records: Cannot find key %s (%d) \n",str_set,status);
      hV=0;
      printf("create_records: Attempting to create record for v680 area\n");
    
      /* create record "/Equipment/MUSR_TD_acq/v680" to make sure it exists  */
 
      status = db_create_record(hDB, 0, str_set, strcomb(musr_td_acq_v680_str)); 
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"create_records","failure to create record for %s (%d)",str_set,status);
	  return FE_ERR_ODB;
	}
      else
	printf("create_records: created record for v680 area\n");

    }
  else // key hV has been found
    {

   /* check that the record size is as expected */
      status = db_get_record_size(hDB, hV, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "create_records", "error during get_record_size (%d) for v680 record",status);
	  return status;
	}

      struct_size =   sizeof(MUSR_TD_ACQ_V680);

      
      printf("create_records: Info - size of v680 saved structure: %d, size of v680 record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"create_records","creating record (v680); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, str_set, strcomb(musr_td_acq_v680_str)); 
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"create_records","Could not create v680 record (%d)\n",status);
          return status;
        }
        else
          printf("create_records: success from create record for %s\n",str_set);
      }
    }
  
  /* try again to get the key hV  */
  
  status = db_find_key(hDB, 0, str_set, &hV);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "create_records", "key %s not found (%d)", str_set,status);
    return (status);
  }


  /* Get the record for v680 area */
  size = sizeof(vs);
  status = db_get_record(hDB, hV, &vs, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "create_records", "cannot retrieve %s record; struct size=%d (%d)",
	     str_set,size,status);
      return FE_ERR_ODB;
    }
  
  //===========================================================

  /* create record "/Equipment/Diag/Settings" to make sure it exists */
  sprintf(str_set, "/Equipment/Diag/Settings");
  status = db_create_record(hDB, 0, str_set, strcomb(diag_settings_str)); 
  
  if (status != DB_SUCCESS)
    { 
      cm_msg(MERROR,"create_records","db_create_record fails %s status(%d)",str_set,status);
      return FE_ERR_ODB;
    }
  if(debug)
    printf("\n*** created record for %s ***\n",str_set);
  

  // ================================================================

 /* find the (global) key for scaler area */
  sprintf(str_set, "/Equipment/Scaler/Settings");
  status = db_find_key(hDB, 0, str_set, &hSet);
  if (status != DB_SUCCESS)
    {
      printf(" create_records: could not find key %s (%d) \n",str_set,status);
      hSet=0;
      
      printf("create_records: attempting to create record for %s\n",str_set);
      /* create record "/Equipment/Scaler/Settings"  */
      status = db_create_record(hDB, 0, str_set, strcomb(scaler_settings_str)); 
      
      if (status != DB_SUCCESS)
	{ 
	  cm_msg(MERROR,"create_records","db_create_record fails for %s (%d)",str_set,status);
	  return FE_ERR_ODB;
	}
      printf("Success from create record for %s\n",str_set);
    }
  else // key hSet has been found
    {
      
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hSet, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "create_records", "error during get_record_size (%d) for Scaler record",status);
	  return status;
	}

      struct_size =   sizeof(SCALER_SETTINGS);

      
      printf("setup_hotlink:Info - size of SCALER saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
	{
	  cm_msg(MINFO,"create_records","creating record (scaler); mismatch between size of structure (%d) & record size (%d)",
		 struct_size ,size);
	  /* create record */
	  status = db_create_record(hDB, 0, str_set, strcomb(scaler_settings_str)); 
      
	  if (status != DB_SUCCESS)
	    { 
	      cm_msg(MERROR,"create_records","db_create_record fails for %s (%d)",str_set,status);
	      return FE_ERR_ODB;
	    }
	  printf("Success from create record for %s\n",str_set);
	}
    }

  // Try again to get the key hSet
  status = db_find_key(hDB, 0, str_set, &hSet);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "create_records", "key %s still not found (%d)", str_set,status);
      return status;
    }

  size = sizeof(fs);
  status = db_get_record (hDB, hSet, &fs, &size, 0);/* get the whole record for Scaler */
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"create_records","Failed to retrieve %s record  (%d)",str_set,status);
      return(status);
    }

  //===================================================================

  /* create record "/Equipment/Rscal/Settings" to make sure it exists */
  sprintf(str_set, "/Equipment/Rscal/Settings");
  status = db_create_record(hDB, 0, str_set, strcomb(rscal_settings_str)); 

  if (status != DB_SUCCESS)
   { 
     cm_msg(MERROR,"create_records","db_create_record fails %s status(%d)",str_set,status);
     return FE_ERR_ODB;
   }


  return status;
}





/* ------------------------------------------------------------------------------------

   D E B U G G I N G

   ------------------------------------------------------------------------------------
*/
#ifdef DEBUG

/*- v680 memory + counter clear ---------------------------------------------------*/
void  v680_mem_dbg(int unmap_pat)
{
  INT i, j;
  INT offset; 
  DWORD map_pat = 0;
  
  if(debug)printf("\nv680_mem_dbg starting with unmap_pat=0x%x\n",unmap_pat);
  
  /* translate unmapped to mapped channel# */
  for (i=0;i<16;i++)
    if (unmap_pat & (1<<i))
      map_pat |= 1<<channel_table[i];
  
  if(debug)
  {
    printf("unmap_pat:%x map_pat:%x info.nbins:0x%x\n",unmap_pat, map_pat,
           info.nbins);
    for(j=0;j<16;j++)
      printf("Table[%i]=%x\n",j,channel_table[j]); 
  }
  for (i=0;i<16;i++)
  {   	     
    
    if (map_pat & (1<<i))
    {
      
      /* Number  each histogram  */
      offset=i*4*info.nbins;
      printf("writing %d to  histo %i  offset:%i\n",i,offset);
      
      if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
        * (pmemBase2+(i*2*info.nbins)) =i;
      else
      {
        *(pmemBase4+(i*4*info.nbins) ) =i;
        *(pmemBase4+(i*4*info.nbins) + 1 ) =i*2;
      }
    }
  }
  
  if(debug)
    printf("v680_mem_dbg ending\n");
}


/* ----dump hm contents ---------------------------------------------------------------------------*/
void hm_dump(INT pbins)
{
  INT i,j,h;
  INT len;
  DWORD *pdata4,*pend;
  WORD *pdata2;
  
  printf("hm_dump: pbins = %d\n",pbins);

  if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
    hm_dump2(pbins);
  else
    hm_dump4(pbins);
  return;
}

/* ------check hm contents --------------------------------------------------------------------------*/
INT hm_check(INT data)
{
  /* see if any of hm has changed from input value "data"
   */   
  
  if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
    hm_check2(data) ;
  else
    hm_check4(data);
  return;
}

/* ----------write data to histograms -------------------------------------------------------------------*/
void hm_write(INT data)
{
/*
  - write histogram number into first bin and counter number shifted 
  - increment each histogram's data
  */
  INT i,j,h;
  INT len;
  DWORD *pdata4;
  WORD *pdata2;
  
  if (info.bytes_per_bin == TWO_BYTES_PER_BIN)
    hm_write2(data);
  else
    hm_write4(data);
  
  return ;
}


/* ----dump hm contents : 4 bytes/bin  ---------------------------------------------------------------------------*/
void hm_dump4(INT pbins)
{
  INT i,j,h,k;
  INT len,my_bins,bins;
  DWORD *pdata4,*pend;
  
  bins = info.nbins;
   len = info.nbins * max_channels  ; /* maximum length in words */
  pend = pmemBase4 + (DWORD)len -1 ;
  printf("hm_dump4: nbins %d max_channels %d length in words %d pmemBase4 %p pend %p\n ",
         info.nbins,max_channels,len,pmemBase4,pend);
  pdata4 =  pmemBase4;
  
  printf("hm_dump4: pbins = %d, bins = %d\n",pbins,bins);

  /* bins = no. bins, pbins = no. bins to print out */
  if (pbins >= bins)
   {
      my_bins = bins;
      printf("hm_dump4: all %d bins will be dumped\n",my_bins);
    }
  else
    {
    my_bins = pbins;
    printf("hm_dump4: dumping only %d bins ....\n",my_bins);
    }
  
  j=8;         /* elements within line */
  i=1;         /* data counter */
  h=0;         /* histo counter */
  k=0;         /* bin counter if not all bins are to be printed */
  printf("hm_dump4:  HM_word_offset[0] %d at pointer %p\n",
         HM_word_offset[h],(pmemBase4 + (DWORD) HM_word_offset[h]));
  while(pdata4 <  (pmemBase4 + len  ) )
  {
    if (pdata4 == pmemBase4 + (DWORD) info.nbins * h )
    {
      printf("\n Histogram %d at pointer %p\n",h,pdata4);
      h++;
    }              
    if(j>7)
    {
      printf("\n%p %4i-> ",pdata4,i);
      j=0;
      i+=8;
    }
    printf("0x%8.8x ",*pdata4);
    pdata4++;
    j++;

    if(my_bins != bins) 
      {
	k++;  /* no. bins printed */
	if(k >= my_bins)
	  {
	    k=0;
	    pdata4 = pmemBase4 + (DWORD) info.nbins * h ; /* set pointer to next histo */
	    i=bins * h; /* set data counter to correct value for next histo */
	  }
      }
  }

  printf("\n");
}

/* ----write data to  hm : 4 bytes/bin  ---------------------------------------------------------------------------*/
void hm_write4 (INT data)
{
  /* write data to histograms  (4 bytes/bin) - 
     - write histogram number into first bin and counter number shifted 
     - increment each histogram's data
  */
  INT i,j,h;
  INT len;
  DWORD *pdata4;
  
  len = info.nbins * max_channels  ; /* total length of data */
  printf("hm_write4: nbins %d max_channels %d length in words %d pmemBase4 %p\n ",
         info.nbins,max_channels,len,pmemBase4);
  pdata4 =  pmemBase4;
  printf("Writing data %d to hm\n",data);
  
  h=0;         /* histo counter   */
  for (j=0; j< max_channels; j++)
    printf("hm_write4: HM_word_offset[%d] = %d with  pointer %p\n",
           j,HM_word_offset[j],(pmemBase4 + (DWORD)HM_word_offset[j]));
  
  while(pdata4 <  (pmemBase4 + (DWORD)len  ) )
  {
    /* new histogram  - encode Histo no. and counter no. (i.e. HM_word_offset index)
       into first word of histogram */
    if (pdata4 == pmemBase4 + (DWORD) (h * info.nbins) )
    {   /* new histogram */
      printf("\n Writing %d to Histogram %d starting  at pointer %p\n",
             (h+data), h, pdata4);
      i=h;
      /* look for this offset in HM_word_offset array */
      for (j=0; j<max_channels;  j++)
      {
        if (  pdata4  ==  (pmemBase4 + (DWORD) HM_word_offset[j]))
        {
          i = h | (j<<4) ;
          printf("Found Histogram %d is for counter %d (i.e. HM_word_offset[%d]) ; encoded bin 1 = 0x%x)\n",
                 h,j,j,i);
        }
      }
      /* fill first bin */
      *pdata4 = i ;
      pdata4++;
      h++; 
      
    }
    else
    {
      /* fill histogram data */
      *pdata4 = (h-1) + data; /* set some data according to histogram number */
      pdata4++;
    }
  }
  
  printf("\n");
  return;
}




/* ----check  hm contents : 4 bytes/bin  ---------------------------------------------------------------------------*/
INT hm_check4(INT data)
{
  /* see if any of hm has changed from input value "data"
     if data = 0 checks ALL elements are 0
     
     if data > 0 assumes elements have been written by hm_write,
     therefore checks that each histogram increments by 1 .
     Note: hm_write writes histo number and counter number to first bin */

   
  INT i,j,h;
  INT len;
  DWORD *pdata4, *pend;
  
  len = info.nbins * max_channels  ; /* total length in words */
  pend  = pmemBase4 + (DWORD)len  ; /* end pointer */
  
  printf("hm_check: nbins %d max_channels %d length in words %d pmemBase4 %p, pend %p\n ",
         info.nbins,max_channels,len,pmemBase4, pend);
  pdata4 =  pmemBase4;
  j=0;  /* element counter */
  i=0; /* data counter */
  h=0; /* histo counter */

  /* check all data is zero - hm_write was not used to write data */
  if(data == 0 )
  {
    while(pdata4 <  pend)
    {
      if(*pdata4 != data)
      {
        if(j<5)
          printf("pointer %p, data array element %d : expect 0x%x; got  0x%x\n",
                 pdata4,i,data,*pdata4);
        j++;
      }
      pdata4++;
      i++;
    }
    if(j>0)
      printf("hm_check:  * * * *  %d elements have changed from 0x%x (only first 5 printed) \n",j,data);
    else
      printf("hm_check:  no changes \n",);
  }
  
  else  /* hm_write was used to write data */
  {
    printf(" Assuming data has been written using hm_write. Histogram data increments &\n");
    printf(" expect 1st array element to contain histo number & counter number\n");
    while(pdata4 <  pend)
    {
      if(i == info.nbins *h )
      {
        printf("Histo %d starting at pointer %p; checking data is %d\n",
               h,pdata4, (data+h));
        printf("Bin 1 contains 0x%x; Hist %d, counter %d\n",
               *pdata4, *pdata4 & 0xF, (*pdata4 & 0xF0) >>4 );
        h++;
      }
      else
      {
        if(*pdata4 != data+(h-1))
        {
        
          printf("pointer %p, data array element %d : expect 0x%x; got  0x%x\n",
                 pdata4,i,data,*pdata4);
          j++;
        }
      }
      pdata4++;
      i++;
    }
    if(j>0)
      printf("hm_check:  * * * *  %d elements have changed from 0x%x  \n",data);
    else
      printf("hm_check:  no changes \n");
  }
  return (j);
}




/* ----check hm contents : 2 bytes/bin  ---------------------------------------------------------------------------*/
INT hm_check2(INT data)
{
  /* see if any of hm has changed from input value "data"
     if data = 0 checks ALL elements are 0
     
     if data > 0 assumes elements have been written by hm_write,
     therefore checks that each histogram increments by 1 .
     Note: hm_write writes histo number and counter number to first bin */

   
  INT i,j,h;
  INT len;
  WORD *pdata2, *pend;
  
  len = info.nbins * max_channels  ; /* total length in words */
  pend  = pmemBase2 + (WORD)len  ; /* end pointer */
  
  printf("hm_check: nbins %d max_channels %d length in words %d pmemBase2 %p, pend %p\n ",
         info.nbins,max_channels,len,pmemBase2, pend);
  pdata2 =  pmemBase2;
  j=0;  /* element counter */
  i=0; /* data counter */
  h=0; /* histo counter */

  /* check all data is zero - hm_write was not used to write data */
  if(data == 0 )
  {
    while(pdata2 <  pend)
    {
      if(*pdata2 != data)
      {
        if(j<5)
          printf("pointer %p, data array element %d : expect 0x%x; got  0x%x\n",
                 pdata2,i,data,*pdata2);
        j++;
      }
      pdata2++;
      i++;
    }
    if(j>0)
      printf("hm_check:  * * * *  %d elements have changed from 0x%x (only first 5 printed) \n",j,data);
    else
      printf("hm_check:  no changes \n");
  }
  
  else  /* hm_write was used to write data */
  {
    printf(" Assuming data has been written using hm_write. Histogram data increments &\n");
    printf(" expect 1st array element to contain histo number & counter number\n");
    while(pdata2 <  pend)
    {
      if(i == info.nbins *h )
      {
        printf("Histo %d starting at pointer %p; checking data is %d\n",
               h,pdata2, (data+h));
        printf("Bin 1 contains 0x%x; Hist %d, counter %d\n",
               *pdata2, *pdata2 & 0xF, (*pdata2 & 0xF0) >>4 );
        h++;
      }
      else
      {
        if(*pdata2 != data+(h-1))
        {
        
          printf("pointer %p, data array element %d : expect 0x%x; got  0x%x\n",
                 pdata2,i,data,*pdata2);
          j++;
        }
      }
      pdata2++;
      i++;
    }
    if(j>0)
      printf("hm_check:  * * * *  %d elements have changed from 0x%x  \n",data);
    else
      printf("hm_check:  no changes \n");
  }
  return (j);
}



/* ----write  hm contents : 2 bytes/bin  ---------------------------------------------------------------------------*/
void hm_write2 (INT data)
{
  /* write data to histograms  (2 bytes/bin) - 
     - write histogram number into first bin and counter number shifted 
     - increment each histogram's data
  */
  INT i,j,h;
  INT len;
  WORD *pdata2;
  
  len = info.nbins * max_channels  ; /* total length of data */
  printf("hm_write2: nbins %d max_channels %d length in words %d pmemBase2 %p\n ",
         info.nbins,max_channels,len,pmemBase2);
  pdata2 =  pmemBase2;
  printf("Writing data %d to hm\n",data);
  
  h=0;         /* histo counter   */
  for (j=0; j< max_channels; j++)
    printf("hm_write2: HM_word_offset[%d] = %d with  pointer %p\n",
           j,HM_word_offset[j],(pmemBase2 + (WORD)HM_word_offset[j]));
  
  while(pdata2 <  (pmemBase2 + (WORD)len  ) )
  {
    /* new histogram  - encode Histo no. and counter no. (i.e. HM_word_offset index)
       into first word of histogram */
    if (pdata2 == pmemBase2 + (WORD) (h * info.nbins) )
    {   /* new histogram */
      printf("\n Writing %d to Histogram %d starting  at pointer %p\n",
             (h+data), h, pdata2);
      i=h;
      /* look for this offset in HM_word_offset array */
      for (j=0; j<max_channels;  j++)
      {
        if (  pdata2  ==  (pmemBase2 + (WORD) HM_word_offset[j]))
        {
          i = h | (j<<4) ;
          printf("Found Histogram %d is for counter %d (i.e. HM_word_offset[%d]) ; encoded bin 1 = 0x%x)\n",
                 h,j,j,i);
        }
      }
      /* fill first bin */
      *pdata2 = i ;
      pdata2++;
      h++; 
      
    }
    else
    {
      /* fill histogram data */
      *pdata2 = (h-1) + data; /* set some data according to histogram number */
      pdata2++;
    }
  }
  
  printf("\n");
  return;
}

/* ----dump hm contents : 2 bytes/bin  ---------------------------------------------------------------------------*/
void hm_dump2(INT pbins)
{
  INT i,j,h,k;
  INT len,my_bins,bins;
  WORD *pdata2,*pend;

  bins = info.nbins;
  len = info.nbins * max_channels  ; /* maximum length in words */
  pend = pmemBase2 + (WORD)len -1 ;
  printf("hm_dump: nbins %d max_channels %d length in words %d pmemBase2 %p pend %p\n ",
         info.nbins,max_channels,len,pmemBase2,pend);
  pdata2 =  pmemBase2;

  /* bins = no. bins, pbins = no. bins to print out */
  if (pbins >= bins)
    {
      my_bins = bins;
      printf("hm_dump2: all %d bins will be dumped\n",my_bins);
    }
  else
    {
      my_bins = pbins;
      printf("hm_dump2: dumping only %d bins ....\n",my_bins);
    } 
  
  j=8;         /* elements within line */
  i=1;         /* data counter */
  h=0;         /* histo counter */
  printf("hm_dump2:  HM_word_offset[0] %d at pointer %p\n",
         HM_word_offset[h],(pmemBase2 + (WORD) HM_word_offset[h]));
  while(pdata2 <  (pmemBase2 + len  ) )
  {
    if (pdata2 == pmemBase2 + (WORD) info.nbins * h )
    {
      printf("\n Histogram %d at pointer %p\n",h,pdata2);
      h++;
    }              
    if(j>7)
    {
      printf("\n%p %4i-> ",pdata2,i);
      j=0;
      i+=8;
    }
    printf("0x%8.8x ",*pdata2);
    pdata2++;
    j++;
   if(my_bins != bins)
      {
	k++;  /* no. bins printed */
	if(k >= my_bins)
	  {
	    k=0;
	    pdata2 = pmemBase2 + (WORD) info.nbins * h  ; /* set pointer to next histo */
	    i=bins * h; /* set data counter to correct value for next histo */
	  }
      }
  }

  printf("\n");
}

#endif   /* end of ifdef DEBUG for debugging only */
