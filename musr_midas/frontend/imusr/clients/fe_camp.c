/********************************************************************\

  Name:         fe_camp.c	
  Created by:   Suzannah Daviel, TRIUMF

  based on  frontend.c by Stefan Ritt

  Contents:     Frontend program for BNMR1 (Type 1 - IMUSR)
                Sends CAMP data and CAMP path information
                needed by mud to make saved histogram files.


                Defines two equipments, CAMP and INFO.

		CAMP equipment produces one bank periodically
		       CVAR bank contains values read from camp
		INFO equipment produces one bank CAMP on transitions

	        The camp paths are read from /equipment/camp/settings in odb.
                They are checked out by a perlscript, camp.pl executed
                at begin-of-run.

  $Log: fe_camp.c,v $
  Revision 1.3  2003/11/19 20:38:34  suz
  wait 5s not 15 before reopening camp etc.

  Revision 1.2  2003/10/15 18:58:28  suz
  add reconnect to camp and mdarc stopping the run (client flags)

  Revision 1.1  2003/09/30 17:07:04  suz
  original to cvs for imusr

  Revision 1.14  2003/01/23 19:06:03  suz
  add support for IMUSR

  Revision 1.13  2003/01/17 20:34:03  suz
  change message

  Revision 1.12  2003/01/09 18:50:08  suz
  add include path; get perl path from mdarc area; perlscript name in settings no longer includes the path

  Revision 1.11  2003/01/08 20:21:36  suz
  set frontend_call_loop TRUE so cm_yield is executed

  Revision 1.10  2002/11/18 22:06:04  suz
  add max_event_size_frag

  Revision 1.9  2002/05/10 17:34:43  suz
  separate check and ping camp so rf_config can use them; add param to db_get_value for midas 1.9

  Revision 1.8  2002/04/29 17:31:40  suz
  expt_name now in mdarc.h

  Revision 1.7  2001/10/30 23:56:43  suz
  take out RO_RUNNING (debug) for camp_info

  Revision 1.6  2001/10/30 23:43:57  suz
  allow 0 camp variables; create record only if size has changed

  Revision 1.5  2001/05/14 20:56:37  suz
  fix small bug

  Revision 1.4  2001/05/14 20:51:12  suz
  add debug statements

  Revision 1.3  2001/05/02 18:32:11  suz
  check on other clients before create_record for mdarc

  Revision 1.2  2001/05/01 21:23:51  suz
  Camp only - remove DARC bank to fe_header.c

  Revision 1.1  2001/04/25 19:20:22  suz
  Added to cvs





\********************************************************************/

#include <stdio.h>

/* camp includes come before midas.h */

#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif


#include "midas.h"
#include "experim.h"
#include "mdarc.h"
//#include "darc_odb.h"
//* mud includes */
#define BOOL_DEFINED
#include "mud.h"
#include "mud_util.h"
#include "trii_fmt.h"

#define FAILURE 0
#define NEW
/*-- Globals -------------------------------------------------------*/
HNDLE hDB=0, hCamp=0, hMDarc=0, hFS=0;
CAMP_SETTINGS camp_settings;

#ifdef IMUSR
MUSR_TD_ACQ_MDARC fmdarc;
char eqp_name[]="MUSR_I_ACQ";
char td_eqp_name[]="MUSR_TD_ACQ"; // needed for mdarc area
#else  // BNMR
FIFO_ACQ_MDARC fmdarc;
char eqp_name[]="FIFO_acq"; 
#endif

int status;
int firstTime = TRUE;
int campInit  = FALSE;
int camp_available;
char serverName[LEN_NODENAME+1];
char perl_script[80] ; 
char outfile[]="/var/log/midas/camp.txt";
char errfile[]="/var/log/midas/camp.lasterr";
char perl_cmd[132];
FILE *FIN;
BOOL end_run;
char str[256];

/* prototypes */
INT camp_read(char *pevent, INT off);   /* retired */
INT camp_var_read(char *pevent, INT off);
INT camp_path_read(char *pevent, INT off);
INT camp_info_read(char *pevent, INT off);
//DWORD darc_get_odb(D_ODB *p_odb_data);
void write_message1(int status, char* name);
void camp_close(void);
INT ping_camp(void);
void check_camp(void);
/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fe_camp";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms    */
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 50000;

/* maximum event size (for fragmented events only) */
INT max_event_size_frag = 5*1024*1024;

/* buffer size to hold events */
INT event_buffer_size = DEFAULT_EVENT_BUFFER_SIZE;


/* prototypes */

/*-- Equipment list ------------------------------------------------*/
EQUIPMENT equipment[] = {
  { "CAMP",                 /* equipment name */
    13, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
#ifdef IMUSR
    EQ_MANUAL_TRIG,          /* equipment type */
#else
    EQ_PERIODIC,
#endif
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING |         /* read when running  */
     RO_ODB,               /* and update ODB */ 
    60000,                /* read every 60 sec */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    10,                   /* log history every event */
    "", "", "",
    camp_var_read,        /* readout routine */
    NULL,                 /* class driver main routine */
    NULL,                 /* device driver list */
    NULL,                 /* init string */
  },

  { "Camp_Info",               /* equipment name */
    15, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    ( RO_BOR | RO_EOR),    /* read on BOR or EOR transitions only */
    30000,                /* read every 30 sec (need RO_RUNNING above) */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    0,                    /* log history every event  (if non-zero event is sent to
                             odb) */
    "", "", "",
    camp_info_read,       /* readout routine */
    NULL,                 /* class driver main routine */
    NULL,                 /* device driver list */
    NULL,                 /* init string */
  },
  
  
  { "" }
};

#include "check_camp.c"   // include code shared with rf_config.c, i.e. check and ping_camp 
#include "set_client_flag.c" // this is also in mdarc_subs for BNMR

/*-- Dummy routines ------------------------------------------------*/

INT  poll_event(INT source[], INT count, BOOL test) {return 1;};
INT  interrupt_configure(INT cmd, INT source[], PTYPE adr) {return 1;};

 
/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int size,i,j, len;
  CAMP_SETTINGS_STR(camp_settings_str);
#ifdef IMUSR
  MUSR_TD_ACQ_MDARC_STR(mdarc_str);
#else  // BNMR
  FIFO_ACQ_MDARC_STR(mdarc_str);
#endif
  INT run_state;
  char *s;
  char cmd[128];
  
  debug = TRUE; 
  campInit = FALSE;
  camp_available = FALSE; 
#ifdef IMUSR
  printf("fe_camp is starting for IMUSR\n");
#else
  printf("fe_camp: this version should not longer be used for anything except IMUSR\n");
  return  DB_INVALID_PARAM;
#endif


  printf("fe_camp is being initialized (frontend_init) \n");
  cm_get_experiment_database(&hDB, NULL);

  /* find out our experiment name */
  size = sizeof (expt_name);
  status = db_get_value(hDB,0, "/Experiment/name",  expt_name, &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"frontend_init","error accessing key /Experiment/name");
    write_message1(status,"frontend_init");
    return (status);
  }
  printf("Experiment name: %s\n",expt_name);

  /* make sure there's only one copy of this program running */
  status=cm_exist("fe_camp",FALSE); 
  if(debug) printf("status after cm_exist for client fe_camp = %d \n",status);
  if(status == CM_SUCCESS)
    {
      cm_msg(MERROR, "tr_start","Another copy of fe_camp is already running");
      return status;
    }
  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","key not found /Runinfo/State (%d)",status);
      return(status);
    }


  /* get the key hMDarc  */
#ifdef IMUSR
  sprintf(str,"/Equipment/%s/mdarc",td_eqp_name);
#else
  sprintf(str,"/Equipment/%s/mdarc",eqp_name);
#endif
  
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      hMDarc=0;
      if(debug) printf("frontend_init: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(mdarc_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"frontend_init","Failure creating mdarc record (%d)",status);
	  if (run_state == STATE_RUNNING )
	    cm_msg(MINFO,"frontend_init","May be due to open records while running. Stop the run and try again");
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key mdarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMDarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "frontend_init", "error during get_record_size (%d) for mdarc record",status);
	  return status;
	}
#ifdef IMUSR
      printf("Size of mdarc saved structure: %d, size of mdarc record: %d\n", sizeof(MUSR_TD_ACQ_MDARC) ,size);  
      
      if (sizeof(MUSR_TD_ACQ_MDARC) != size) 
      {
	cm_msg(MINFO,"frontend_init","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)", sizeof(MUSR_TD_ACQ_MDARC)
	       ,size);
#else
	// BNMR
      printf("Size of mdarc saved structure: %d, size of mdarc record: %d\n", sizeof(FIFO_ACQ_MDARC) ,size);
      if (sizeof(FIFO_ACQ_MDARC) != size) 
	{
	  cm_msg(MINFO,"frontend_init","creating record (mdarc); mismatch between size of structure (%d) & record size (%d)", sizeof(FIFO_ACQ_MDARC)
		 ,size);
#endif
	  /* create record */
	  status = db_create_record(hDB, 0, str , strcomb(mdarc_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"frontend_init","Could not create mdarc record (%d)\n",status);
	      if (run_state == STATE_RUNNING )
		cm_msg(MINFO,"frontend_init","May be due to open records while running. Stop the run and try again");
	      return status;
	    }
	  else
	    if (debug)printf("Success from create record for %s\n",str);
	}
    }
  
  /* try again to get the key hMDarc  */
  status = db_find_key(hDB, 0, str, &hMDarc);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","Failed to get the key %s (%d)",str,status);
      return(status);
    }

  size = sizeof (fmdarc);
  if(debug) printf("hMDarc = %d, size of record = %d \n",hMDarc,size);
  
  status = db_get_record(hDB, hMDarc, &fmdarc, &size, 0);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot retrieve %s record (%d)",str,status);
      return DB_NO_ACCESS;
    }
#ifdef IMUSR
  printf("Camp hostname = %s\n",fmdarc.camp.camp_hostname);
#else // BNMR
  printf("Camp hostname = %s\n",fmdarc.camp_hostname);
#endif

  /* 

     create camp record if necessary 

  */
  sprintf(str,"/Equipment/Camp/Settings");
  //printf("str: %s\n",str);

  status = db_find_key(hDB, 0, str, &hCamp);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("main: Failed to find the key %s (%d) ",str,status);
      
      /* Create record for camp/settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB,0,str,  strcomb(camp_settings_str));
      if(status!= DB_SUCCESS)
	{
	  cm_msg(MERROR,"frontend_init","Create Record fails for %s (%d) ",status,str);
	  return(status);
	}
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hCamp has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hCamp, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "frontend_init", "error during get_record_size (%d) for camp record",status);
	  return status;
	}
      printf("Size of camp saved structure: %d, size of camp record: %d\n", sizeof(CAMP_SETTINGS) ,size);
      if (sizeof(CAMP_SETTINGS) != size) 
	{
	  cm_msg(MINFO,"frontend_init","creating record (camp); mismatch between size of structure (%d) & record size (%d)", sizeof(CAMP_SETTINGS) ,size);
	  /* create record */
	  status = db_create_record(hDB,0,str,  strcomb(camp_settings_str));
	  if (status != DB_SUCCESS)
	    {
	      cm_msg(MERROR,"frontend_init","Could not create %s record (%d)\n",str,status);
	      return status;
	    }
	  else
	    if (debug)printf("Success from create record for %s\n",str);
	}
    }
  
  /* try again to get the key hMDarc  */

  status = db_find_key(hDB,0, str, &hCamp);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot find Camp key for %s (%d)",str,status);
      return status;
    }

  /* get the camp record */
  size = sizeof (camp_settings);
  if(debug) printf("hCamp = %d, size of record = %d \n",hCamp,size);

  status = db_get_record(hDB,hCamp, &camp_settings, &size, 0);
  if(status!= DB_SUCCESS)
  {
    cm_msg(MERROR, "frontend_init", "cannot retrieve %s record",str);
    return DB_NO_ACCESS;
  }

  /* get the path of the perlscript */

  /*  camp_settings.perl_script contains only the filename */
  len = strlen(camp_settings.perl_script );
  
  if(len <= 0)
    {
      cm_msg(MERROR, "frontend_init","no perlscript name supplied in /equipment/camp/settings/perl_script");
      return(DB_INVALID_PARAM);
    }
  
  if(debug)printf("perlscript file name: %s (len=%d)\n",camp_settings.perl_script,len);
  /*
    get the path of the perl scripts 
  */
  sprintf(perl_path,"%s",fmdarc.perlscript_path); /* perl_path (in mdarc.h) is needed as an input parameter (to perl script)  */
  trimBlanks(perl_path,perl_path);
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_path,'/');
  i= (int) ( s - perl_path );
  j= strlen( perl_path );
  
  if(debug)
    printf("string length of perl_path %s  = %d, last occurrence of / = %d\n", perl_path, j,i);
  if ( i == (j-1) )
    perl_path[i]='\0';

  strcpy(perl_script,perl_path); /* perl_path needed as a parameter  */
  strcat(perl_script,"/"); // add a "/" 
  strcat(perl_script,camp_settings.perl_script);
  trimBlanks(perl_script,perl_script);
  
  if(debug) {
    printf ("Perl script: %s\n", perl_script);
    printf ("Perl path:   %s\n", perl_path);
  }


  /* Check there is a valid number of camp variables to be logged */
  if (camp_settings.n_var_logged <= 0)
    {
      cm_msg(MINFO, "frontend_init", "No camp variables are selected to be logged in /equipment/camp/settings");
      printf("frontend_init: No camp variables are selected to be logged in /equipment/camp/settings\n");
      camp_available = campInit = FALSE; /* make sure these flags false */
      return CM_SUCCESS;  /* allow this case */
    }

  if(debug)
    {
      printf("frontend_init: number of camp values = %d\n",camp_settings.n_var_logged);
      for (j=0; j< camp_settings.n_var_logged; j++)
        printf("frontend_init: camp path  = %s\n",camp_settings.var_path[j]);
    }

  
  /* Check there is a valid camp hostname */
  check_camp(); // sets camp_available
  if (!camp_available)
    return DB_INVALID_PARAM;
    
  /* We have a valid CAMP hostname - but is it active?  */
  status = ping_camp();
  if (status == DB_NO_ACCESS)
    {
      cm_msg(MERROR,"frontend_init","Camp host %s is unreachable. Camp data cannot be saved",
	     serverName);
      return status;
    }
  else
    if (debug) printf ("Camp host is responding to ping\n");
  /* Check if a run is in progress*/
  
  //  get the run state
  size = sizeof(run_state);
  status = db_get_value(hDB,0, "/Runinfo/State",  &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
    {
      status=cm_msg(MERROR,"frontend_init","error from db_get_value  /Runinfo/State (%d)",status);
	  return (status);
    }
 


  if(run_state !=  (RO_STOPPED || RO_EOR) )
    {
      /* execute perlscript to check the camp paths, write the polling intervals */
 
      unlink(outfile); /* delete any old copy of perl output file */
      sprintf(perl_cmd,"%s %s %s %s",perl_script,perl_path,expt_name,eqp_name); 
      printf("frontend_init: Executing perl script to check camp paths...\n");
  
      printf("frontend_init: Sending system command  cmd: %s\n",perl_cmd);
      status =  system(perl_cmd);  /* now open camp connection */
      if (status)
      {
	  printf (" ========================================================================================\n");
	  cm_msg (MERROR,"frontend_init","Perl script camp.pl returns failure status (%d)",status);
	  FIN = fopen (outfile,"r");
	  if (FIN == NULL)
              cm_msg (MERROR,"frontend_init","There may be compilation errors in the perl script;  check fe_camp window for details");
          else
          { // make a copy of the error file as the outfile is overwritten each time
              sprintf(cmd,"cp %s %s",outfile,errfile);
              status = system(cmd);
              if(status)cm_msg(MINFO,"frontend_init","failure from system command: %s",cmd);
          }

          return (DB_INVALID_PARAM); // bad status from perl script
	  
      }
      else
	if(debug)printf("frontend_init : Success from perl script to check camp paths\n");
      
      
      
      /* Initialize CAMP connection - normally done at Begin_of_run, but we are (or may be)  running 
       *  (timeout of 10 sec.)
       */
      
      
      if (debug) printf("frontend_init: Detected run is in progress... calling camp_clntInit \n");
      status = camp_clntInit( serverName, 10 );
      if(status == CAMP_SUCCESS)
	{
	  campInit = TRUE;
	  if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS\n");
	}
      else
	{
	  cm_msg(MERROR,"frontend_init","Failed initialize call to CAMP (called because run is in progress)");
	  if(debug) printf("frontend_init: Returned from camp_clntInit with CAMP_FAILURE\n");
	  return status;
	}
    }
  else  // run is stopped
    printf("frontend_init: Success.... waiting for run to start\n");
  return CM_SUCCESS;
}


/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  if(debug) printf("frontend_exit: starting\n");
  if (camp_available)
    {
      if(campInit)
	{
	  if(debug)printf("frontend_exit : calling camp_clntEnd()\n");
	  camp_clntEnd();  
	}
      else
	if(debug) printf("frontend_exit: INFO: camp connection is already closed \n");
    }
  else
	if(debug) printf("frontend_exit: INFO: camp connection was never opened \n");

  return CM_SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  cm_yield(500);
  return CM_SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number_local, char *error)
{
    char client_str[128];
    BOOL client_flag=TRUE; // mdarc sets the client flag for mheader FALSE on a prestart
    char cmd[128];
    
  end_run=FALSE; /* make sure flag is cleared */

  printf("Run %d starting\n",run_number_local);
  run_number=run_number_local;

  /* update the whole mdarc record first to reflect any changes (e.g. no. bins)  */ 
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMDarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","Failed to retrieve mdarc record  (%d)",status);
    write_message1(status,"begin_of_run");
    return(status);
  }

  nHistBins =  fmdarc.histograms.num_bins; //fmdarc set up by setup_hotlink
  nHistBanks =  fmdarc.histograms.number_defined;
  
  printf(": begin_of_run: Number of bins = %d;  no. defined histograms = %d\n",nHistBins,nHistBanks);
  
  /* update the camp record */
  size = sizeof (camp_settings);
  if(debug) printf("hCamp = %d, size of record = %d \n",hCamp,size);

  status = db_get_record(hDB,hCamp, &camp_settings, &size, 0);
  if(status!= DB_SUCCESS)
    {
      cm_msg(MERROR, "begin-of-run", "failed to retrieve Camp record (%d)",status);
      return DB_NO_ACCESS;
    }

 /* Check there is a valid number of camp variables to be logged */
  if (camp_settings.n_var_logged <= 0)
  {
      cm_msg(MINFO, "begin-of-run", "No camp variables are selected to be logged in /equipment/camp/settings");
      printf("frontend_init: No camp variables are selected to be logged in /equipment/camp/settings\n");
      camp_available = campInit = FALSE; /* make sure these flags are false */
      printf("begin_of_run: setting client status flag for fe_camp true (success)\n");
      status = set_client_flag("fe_camp",SUCCESS);
      return status;  /* SUCCESSful return  */            
    }

  
  
  check_camp();    // see if camp is available (hostname may have changed)
  if ( !camp_available)
    return DB_INVALID_PARAM; // camp not available (bad camp host name)
  
  /* We have a valid CAMP hostname - but is it active?  */
  /*  ( ping the camp host because if someone turns off the crate, there is
      no timeout and it can waste a lot of time)  */ 
  status = ping_camp();
  if (status == DB_NO_ACCESS)
    {
      cm_msg(MERROR,"begin-of-run","Camp host %s is unreachable. Camp data cannot be saved",
	     serverName);
      return status;
    }
  
  /* now execute perlscript to check the camp paths, write the polling intervals and
     update odb with the units and title of each path */

  // perl_script and perl_path were set up in frontend_init. These are expected to be constants
    printf ("Perl script: %s\n", perl_script);
    printf ("Perl path:   %s\n", perl_path);

  unlink(outfile); /* delete any old copy of perl output file */
  sprintf(perl_cmd,"%s %s %s %s",perl_script,perl_path,expt_name,eqp_name); 
  printf("begin_of_run: Sending system command  cmd: %s\n",perl_cmd);
  status =  system(perl_cmd);  /* now open camp connection */
  if (status)
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR,"begin_of_run","Perl script camp.pl returns failure status (%d)",status);
      FIN = fopen (outfile,"r");
      if (FIN == NULL)
        cm_msg (MERROR,"begin_of_run","There may be compilation errors in the perl script; check fe_camp window for details");
      else
      { // make a copy of the error file as the outfile is overwritten each time
          sprintf(cmd,"cp %s %s",outfile,errfile);
          status = system(cmd);
          if(status)cm_msg(MINFO,"begin_of_run","failure from system command: %s",cmd);
      }
      return (DB_INVALID_PARAM); // bad status from perl script
    }
  else
    if(debug)printf("begin_of_run : Success from perl script to check camp paths\n");
  
  
  if(campInit)
    {
      cm_msg(MINFO,"begin_of_run","Camp port is already open. It will be closed and re-opened");
      /* this is not good. Closing and opening port several times results in a
         hang. Port should normally be closed, but it might be open if frontend_init opened it thinking
         the run was on */
      if(debug)printf("begin_of_run : calling camp_clntEnd()\n");
      camp_clntEnd();
      campInit = FALSE;
      cm_msg(MINFO,"begin_of_run","Waiting for 5s  before reopening camp connection...");
      ss_sleep(5000);  /* maybe should wait for a while before re-opening? Try waiting 15 sec */
    }
  
  /*  Initialize CAMP connection  
   *  (timeout of 10 sec.)
   */
  
  if(debug)printf("begin_of_run:  calling camp_clntInit\n");
  status = camp_clntInit( serverName, 10 );
  if(status == CAMP_SUCCESS)
    {
      campInit = TRUE;
      if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS\n");
    }
  else
    {
      cm_msg(MERROR,"begin-of-run","Failed initialize call to CAMP");
      if(debug)printf("begin-of-run: Returned from camp_clntInit with CAMP_FAILURE\n");
      campInit = FALSE;
      return status;
    }

  printf("begin_of_run: setting client flag for mheader true (success)\n");
  status = set_client_flag("fe_camp",SUCCESS);
  return status; 
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  if(debug) printf("end_of_run: setting flag end_run\n");
/* set a flag to inform camp_info_read that run is ending
   so camp port must be closed  */
  end_run=TRUE;
  
  return CM_SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return CM_SUCCESS;
}



/*---Read info for data archiver ---------------------------------------------------------------*/
INT camp_info_read(char *pevent, INT off)
{

  INT i, size;
  char *pdata;
/* for camp bank */
  char str1[512], tempString[80];
  INT len;

/*  this structure is in   ../mud/util/trii_fmt.h
    typedef struct {
    INT16 type;
    INT16 len_path;
    char path[50];
    INT16 len_title;
    char title[19];
    INT16 len_units;
    char units[8];
    } IMUSR_CAMP_VAR;
*/
  IMUSR_CAMP_VAR camp_info;

  
  INT type,transition;
  char *pstr;

  if(debug) printf("\ncamp_info_read: starting \n");

  if ( !camp_available)  // handles case of no camp variables defined
    {
      if(debug) 
	printf("camp_info_read: Camp is not available or no camp variables defined, no CAMP bank sent\n");
      return 0; /* no event */
    }

  if (! campInit)  // just in case...
    {
      status=reconnect();
      if (status != CAMP_SUCCESS)
	return 0; // cancel the event
    }


  if (camp_settings.n_var_logged <= 0) // just in case...
    {
      printf("camp_info_read: No CAMP bank; no. camp variables to be logged=0\n");
      if (end_run) camp_close();
      return 0; /* no event*/
    }
   
  /* CAMP data should be available */
  size = sizeof(camp_info);
  memset(&camp_info,0,size); // clear structure to start
  

  if(debug)
    {
      if (end_run)printf("INFO - Run is ending\n");
      printf("Getting an update of camp database...\n");
    }

   
  /*
   *    Get an update of the CAMP database
   */
  
  status = camp_clntUpdate();
  if( status != CAMP_SUCCESS )
  {
    cm_msg(MERROR,"camp_info_read:", "failed call to retrieve CAMP data" );
    /* try to reconnect */    
      status=reconnect();
      if (status == CAMP_SUCCESS)
	status = camp_clntUpdate();
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MERROR,"camp_info_read", "failed again to retrieve CAMP data." );
	  campInit=FALSE;
	  return 0; /* cancels the event */
	}
      if(debug)printf("Returned from camp_update after reconnect with CAMP_SUCCESS\n");
  }

  if(debug) printf("camp_info_read: success from camp_clntUpdate()\n");

  /*
  /* create the bank for CAMP 
  */
  if(debug) printf("On entry, pevent = %p\n",pevent);
  bk_init(pevent);
  bk_create(pevent, "CAMP", TID_CHAR, &pdata);
  if(debug) printf("opened CAMP bank ( pevent=%p, pdata=%p) \n",pevent,pdata);

/* 
      now get the camp information for camp_info 
*/

//    MAIN LOOP on each camp variable
  for (i=0;  i < camp_settings.n_var_logged; i++)
  {
    if(debug) printf("\nLOOP %d, calling campSrv_varGet with path=%s\n",i,camp_settings.var_path[i]);
    
    status=campSrv_varGet(camp_settings.var_path[i],2);
    if( status != CAMP_SUCCESS )
    {
      cm_msg(MINFO,"camp_info_read","failed call campSrv_VarGet with path= %s (%d)",camp_settings.var_path[i]),status;

      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      return 0; /* no event */
    }
    
    /*  path */
    strncpy(tempString, camp_settings.var_path[i] ,50);
    tempString[50] = '\0';
    trimBlanks( tempString, tempString );
    strcpy(camp_info.path,tempString);
    camp_info.len_path=strlen( camp_info.path );
    
    /* type */
    status=camp_varGetType(camp_settings.var_path[i],&type);
    if( status != CAMP_SUCCESS )
    {
      if(status==CAMP_INVAL_VAR) printf("Got CAMP invalid variable type\n");
      cm_msg(MINFO,"camp_info_read","failed call camp_varGetType with path= %s (0x%x)",camp_settings.var_path[i]),status;
      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      return 0; /* no event */
    }
    camp_info.type=type;

    /* title */
    status=camp_varGetTitle (camp_settings.var_path[i],str1);
    if( status != CAMP_SUCCESS )
    {
      cm_msg(MINFO,"camp_info_read","failed call camp_varGetTitle with path= %s (%d)",camp_settings.var_path[i]),status;
      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      return 0; /* no event */
    }
    strncpy(tempString,str1,19);
    tempString[19] = '\0';
    trimBlanks( tempString, tempString );
    strcpy( camp_info.title,tempString );
    camp_info.len_title=strlen( camp_info.title );
    
    /* units */
    status=camp_varNumGetUnits(camp_settings.var_path[i],str1);
    if( status != CAMP_SUCCESS )
    {
      cm_msg(MINFO,"camp_info_read","failed call camp_varGetUnits with path= %s (%d)",camp_settings.var_path[i]),status;
      if (end_run) camp_close();
      status = bk_delete(pevent, "CAMP"); /* delete the event */
      return 0; /* no event */
    }
    strncpy(tempString,str1,19);
    tempString[19] = '\0';
    trimBlanks( tempString, tempString );
    strcpy(camp_info.units,tempString );

    camp_info.len_units=strlen( camp_info.units );

    if(debug)
    {
      printf("Values in structure camp_info for loop %d :\n",i);
      printf("path %s, type %d units %s title %s\n",camp_info.path, camp_info.type,
             camp_info.units, camp_info.title);
      printf("len_path %d len_title %d len_units %d\n",camp_info.len_path,
             camp_info.len_title, camp_info.len_units);
    }

  


    /* copy the structure into the bank */
    //    printf("Before memcopy for CAMP, pdata = %p\n",pdata);
    memcpy((char *)pdata, (char *) &camp_info, sizeof(camp_info));
    (char *)pdata += sizeof(camp_info);
    if(debug) printf("Size of camp_info=%d copied to bank; pdata=%p \n",sizeof(camp_info),
		     pdata);
    
  } // end of MAIN LOOP on each CAMP variable

  //  if(debug)printf("closing bank CAMP with pevent=%p, pdata=%p\n",pevent,pdata);
  bk_close(pevent, pdata );
  
  if(debug) printf("closed camp bank,  pevent = %p, pdata = %p, size = %d\n\n",
                   pevent, pdata, bk_size(pevent));
  if (end_run)
    {
      camp_close();
      printf("camp_info_read: Camp_Info event sent (ID=15) of length %d (at end-of-run) \n",bk_size(pevent) );
    }
  else
    printf("camp_info_read: Camp_Info event sent (ID=15) of length %d \n",bk_size(pevent) );
  return bk_size(pevent);
}

void camp_close(void)
{
     /* close the camp port when run stops */
  if(debug) printf("camp_close: starting\n");
  if (camp_available)
    {
      
      if(campInit)
	{
	  if(debug)printf("camp_close : calling camp_clntEnd()\n");
	  camp_clntEnd();  
	}
      else
	if(debug) printf("camp_close: INFO: camp connection is already closed. \n");
    }
  else
    if(debug) printf("frontend_exit: INFO: camp connection was never opened \n");
  
  campInit = FALSE;
  end_run = FALSE;
} 

/*---Read Camp variables ---------------------------------------------------------------*/
/* read camp variables into a variable length float event*/
INT camp_var_read(char *pevent, INT off)
{
  double *pdata;
  INT i,size, nerror;
  int j=0;
  int  error_flag;
  double camp_data;

  if(debug) printf("camp_var_read: starting \n");
  /* read camp into a fixed event */
  
  if(!camp_available) // handles case of no camp variables defined
  {
    if(debug)printf("camp_var_read: No camp is available or no camp variables have been defined\n");
    return 0;
  }
  if(!campInit)
    {
      status=reconnect();
      if (status != CAMP_SUCCESS)
	return 0; // cancel the event
    }
  
  if(debug) printf("On entry, pevent = %p\n",pevent);
  bk_init(pevent);
  bk_create(pevent, "CVAR", TID_DOUBLE, &pdata);
  
  
  
  /*
   *    Get an update of the CAMP database
   */
  
  if(debug) printf("camp_var_read: Calling camp_clntUpdate()\n");
  status = camp_clntUpdate();
  if( status != CAMP_SUCCESS )
    {
      cm_msg(MERROR,"camp_var_read", "failed call to retrieve CAMP data" );
      
      /* try to reconnect */
      status=reconnect();
      if (status != CAMP_SUCCESS)
	return 0; // cancel the event
      status = camp_clntUpdate();
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MERROR,"camp_var_read", "failed call to retrieve CAMP data after reconnect" );
	  campInit=FALSE;
	  return 0; /* cancels the event */
	}
      if(debug)printf("Returned from camp_update after reconnect with CAMP_SUCCESS\n");
      
    }
  nerror=0; /* initialize error count */
  error_flag=FALSE; /* and error flag */
  
  for (i=0;  i < camp_settings.n_var_logged; i++)
    {
      //      printf("calling campSrv_varGet with path=%s\n",camp_settings.var_path[i]);
      status=campSrv_varGet(camp_settings.var_path[i],2);
      if( status != CAMP_SUCCESS )
	{
	  cm_msg(MINFO,"camp_var_read","failed call campSrv_VarGet with path= %s (%d)",camp_settings.var_path[i]),status;
	  /* handle this by setting value to zero  */
	  error_flag = TRUE;
	  nerror++;
	}
      else
	{
	  //     printf("calling camp_varNumGetVal with path=%s\n",camp_settings.var_path[i]);
	  
	  status=camp_varNumGetVal(camp_settings.var_path[i], &camp_data);
	  if( status != CAMP_SUCCESS )
	    {
	      cm_msg(MINFO,"camp_var_read","failed call camp_VarNumGetValue with path= %s (%d)",camp_settings.var_path[i], status);
	      error_flag = TRUE;
	      nerror++;
	    }
	  if(debug)printf("Value read from camp path %s  : %f (index=%d)\n", camp_settings.var_path[i], camp_data, i);
	  if (error_flag)
          *pdata++ = 0.0 ; /* could not read value */
	  else
	    *pdata++ = ( (double) camp_data);
	  error_flag=FALSE;
	}
    }
  
  bk_close(pevent, pdata );
  size=bk_size(pevent);
  if(debug)
    printf("camp_var_read: Returning after bk_close with pevent=%p, pdata=%p \n",pevent,pdata);
  printf("camp_var_read: CAMP event sent (ID=13) of length %d\n",size);
  return size;
}




void
write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (status == DB_NO_KEY) sprintf (str,"because key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}


INT reconnect(void)
{
  INT i;
  /* try to reconnect */
  campInit=FALSE;
  camp_close();
  for(i=0; i<20; i++)
    {
      if (debug) printf("reconnect: trying to reconnect to camp... calling camp_clntInit \n");
      status = camp_clntInit( serverName, 10 );
      if(status == CAMP_SUCCESS)
	{
	  if(debug)printf("Returned from camp_clntInit with CAMP_SUCCESS\n");
	  campInit=TRUE;
	  return status;
	}
      if(debug)printf("reconnect: failure from camp_clntInit; waiting 10s and retrying\n");
      cm_yield(5000);
      ss_sleep(5000);
    }
  cm_msg(MERROR,"reconnect","Cannot reconnect to CAMP\n");
  return status;
}

