/*
    $Log: set_client_flag.c,v $
    Revision 1.1  2003/10/15 19:48:12  suz
    original

*/

/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/fifo_acq/client flags/febnmr to indicate to mdarc that is should stop the run
     set the flag in /equipment/fifo_acq/client flags/client alarm to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in febnmr.c for use of VxWorks frontend - later can try to just have 
     one routine with ifdefs 
  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status;
  if(debug)printf("set_client_flag: starting\n");

  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",td_eqp_name,client_name );
  if(debug)
    printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 

  /* Set the client flag to TRUE (success) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,my_value,status);
      return status;
    }

  
  /*
    Set the alarm flag; TRUE - alarm should go off, if FALSE,  alarm stays off
     Note that in odb, 
        /alarm/alarms/client alarm/condition is set to "/equipment/fifo_acq/client flags/client alarm >0"

  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",td_eqp_name );
  if(debug)
    printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  size = sizeof(client_flag);
  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }
  return CM_SUCCESS ;
}
