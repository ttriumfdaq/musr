/*
     taskSpawn ("mfe",150,spTaskOptions,50000,mfe,"dasdevpc","imusr")
     </home1/midas/vw-ppc/config/m9bhmvw/startup.cmd
   
 */
/********************************************************************\
  Name:         femusr.c

  Created by:   Suzannah Daviel, TRIUMF
  Based on :   1. frontend.c  by Stefan Ritt
               2. v680.c      by Pierre Amaudruz
	       3. febnmr1.c   by Pierre Amaudruz/Renee Poutissou
  Contents:     Midas-type  frontend for I-MUSR and TD-MUSR 

  $Log: femusr.c,v $
  Revision 1.7  2003/11/19 20:34:56  suz
  test on difference in counts changed to 4; max age of bytecode.dat changed to 60s

  Revision 1.6  2003/11/18 20:32:16  suz
  remove call to gglResetCounter so Hardware START starts ggl

  Revision 1.5  2003/10/15 19:54:55  suz
  fix small bug

  Revision 1.4  2003/10/15 18:57:45  suz
  add reconnect to camp and mdarc stopping the run (client flags)

  Revision 1.3  2003/10/07 10:49:28  asnd
  Use conversion factor!

  Revision 1.2  2003/10/02 04:11:07  suz
  fix excessive redrawing of consol display

  Revision 1.1  2003/09/26 19:47:46  suz
  original to cvs

 

\********************************************************************/

#include <stdio.h>
/* put in diff from frontend.c */
#include <stdlib.h>
#include <math.h>
  /* camp includes come before midas.h */
#include "camp_clnt.h"
/* to avoid conflict with midas defines, undefine these
   - they will be redefined by midas the same as camp defined them */
#ifdef TRUE 
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#define failure_status CAMP_FAILURE
#define IO_RETRIES 15
 /* end of CAMP defines */

#include "midas.h"

#include "msystem.h"/* #include "ybos.h"
/* comment out diff from frontend.c 
#include "esone.h" */
#include "mcstd.h" 
/*#include "V680.h" not needed */
#include "experim.h"
#include "sis3803.h"
#include "trGGL.h"
#include "musrvmeio.h"
#include "imusr_inputs.h" /* IO reg and Scaler fixed inputs */
#include "imusr_scaler_sum.h" /* offsets into scaler_sum array */
#include "musr_common_subs.h" /* function prototypes */

/* direct CAMP access by the frontend (ppc) for scanning CAMP device(s) */
/* CAMP parameters */
#include "camp_acq.h" /* parameters and prototypes for camp_acq 
		         note: must come after midas.h  */
/* structure camp_params is defined in camp_acq.h */
static CAMP_PARAMS camp_params;
BOOL dc = 0 ; /* internal debug for camp routines */
INT initCamp(BOOL* camp_active);
/* these for CAMP equipment (camp logging) */
BOOL gotCamp;
HNDLE hconn; /* handle for manual trigger (->fe_camp) */
char ClientName[256]="fe_camp";
BOOL fecamp_exists; /* TRUE if task fe_camp exists */
INT camp_errcount;
BOOL reconnect;
/* end of CAMP */
/* client flag support */
BOOL client_check = TRUE; /* if true, mdarc will stop the run on error (assuming mdarc is running!) */
char who_stops_run[7]; /* mdarc or user */
BOOL gbl_status =TRUE; /* set false when run should be stopped */
 
/* make frontend functions callable from the C framework */
#ifdef __cplusplus
ls extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/
INT poll_val  = 10; /* .1% */
/*INT tr1 = 16, tr2 = 200; */
    
/* VME base addresses */
#define V680_BASE     0xc000    
#define VMEIO_BASE    0x780000
#define GGL_BASE      0x8000
#define SIS_BASE      0x383800
INT   SIS_base_adr = SIS_BASE; /* base address for sis3803 scaler VME module */
INT ggl_base=GGL_BASE; /* Gate Generator Logic Board base address */ 
INT v680_base= V680_BASE; /* store to pass to show_commands.c */     
INT vmeio_base = VMEIO_BASE; /* base address for musrvmeio VMiE board */


    
#define N_SCLR 1
#define MAX_SCALER     16  /* max real scaler channels */
#define MAX_SCALER_SUMS  (SCALER_SUM_CHANNELS + FM_SUM_CHANNELS)  /* 10+8 max size of array to sum real scaler channels according
			    to toggle states -> virtual scaler channels */
#define NUM_EXTRA_DATA 2 /* 2 extra data words in IDAT bank: datapoint  counter and setpoint */
    
#define POLL_INFO   2500 /* every 2.5s */
#define POLL_SEND_DATA  100 /* every 100ms */


    /* line numbers for v680 display */
#define LINE_1  1
#define LINE_2  2
#define LINE_3  3
#define LINE_4  4
#define LINE_4_5 5
#define LINE_4_75 6   /* Key  */   
#define LINE_5  7
#define LINE_6  8
#define LINE_7  9
#define LINE_8  10
#define LINE_9  11
#define LINE_9_5 12
#define LINE_10 13
#define LINE_11 14
#define LINE_12 15
#define LINE_13 16
#define LINE_14 17
#define LINE_15 18  /* currently "last message" line */
#define LINE_16 19
#define LINE_17 20
#define LINE_18 21
#define LINE_18_5 22    
#define LINE_19 23
#define LINE_20 24
#define LINE_21 25
#define LINE_22 26
#define LINE_23 27
#define LINE_24 28
#define LINE_25 29   

#define POLL_EQUIPMENTS   5000 /* every 5s  TD only */
    
/* int ext_polling_time = 500; not used */
int ext_th, last_display, local;
int Address, flatmode;

char td_eqp_name[] = "MUSR_TD_acq"; /* TD_MUSR area - used also for IMUSR */
char imusr_eqp_name[] = "MUSR_I_acq"; /* I_MUSR area - used ONLY for IMUSR */   
  

  /*    
	#ifndef OS_VXWORKS
	extern run_state;
	#else
	INT run_state;
	#endif
	/* internal */
  
  extern run_state;


  /* TEMP */
INT delay;

char *str_status[] = {"  N/A  ","STOPPED", "PAUSED"," RUNNING "};
char display_string[132]=" "; /* store status string for the display;
				   now also used to write a status message into info_odb */
INT  display_len=130; /* length of last display_string */
char display_last[50]=" ";
BOOL display_flag=FALSE; /* may be set by hot_tolerance etc. */    

/* Globals */
DWORD gbl_count;
DWORD n_camp_logged; /* number of camp variables to be logged */
    
INT     v680_Display_period = 3000; /* default milliseconds  - now an odb parameter */
INT camp_Watchdog_period = 30000; /* camp watchdog period; default is 30s */
INT last_camp;

INT msg_cnt=0;
 

/* VMEIO */
/*static DWORD  *vmeio_wrtplse, *vmeio_wrtlatch ;*/

float This_Version=2.0;

INT first_time_display=TRUE; /* needed for first clock hang check (TD musr)*/


/* The frontend name (client name) as seen by other MIDAS clients   */
#ifdef OS_VXWORKS
char *frontend_name = "femusr";
#else
char *frontend_name = "fe_musr";
#endif
    
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period =0;

/* maximum event size produced by this frontend
   must be less than MAX_EVENT_SIZE 0x80000 512K or 524288 (midas.h)*/

INT max_event_size = 10000;    
/* maximum event size produced by this frontend for fragmented events */
  /*#ifndef dev
    INT max_event_size_frag = 5*1024*1024 + 200 ;*/  /* 32Meg ppc (allow 200 for EVENT HEADER overhead) */
  /* #else */
INT max_event_size_frag = 4300000  ; /* -PAA-(5*1024*1024) - 512*1024; 16Meg ppc (allow 101 for overhead) */ 
  /* #endif */


/* buffer size to hold events
   event_buffer_size must be >= 2*max_event_size (checked in mfe.c  */

INT event_buffer_size = 2*10000; 


/* scaler globals */


INT   time_last_rates;    /* last time scaler was read */
DWORD scaler_counts[MAX_SCALER_SUMS]; /* IMUSR uses this to store the counts
					before they are summed for v680_display */ 
DWORD old_counts[MAX_SCALER];
BOOL  first_overflow;
INT   max_scaler_input;

  INT rstate; /* run state */

/* Shared with TD MUSR */
HNDLE hDB,hV,hSet;
MUSR_TD_ACQ_V680_STR (musr_td_acq_v680_str);
MUSR_TD_ACQ_V680 vs;
SCALER_SETTINGS_STR(scaler_settings_str);
SCALER_SETTINGS fs;

/* IMUSR only */
HNDLE hIS,hOS;
MUSR_I_ACQ_SETTINGS_STR (musr_i_acq_settings_str);
MUSR_I_ACQ_SETTINGS is;    
INFO_ODB_EVENT_STR(info_odb_event_str);
INFO_ODB_EVENT os;

/* handles for BOR hotlinks so they can be closed at EOR */
HNDLE hStart,hStop,hTol,hTolD,hRn;

INT size;
INT status; /* status  */

BOOL I_MUSR=FALSE; /* TRUE if I_MUSR is running */
BOOL TD_MUSR=FALSE; /* TRUE if TD_MUSR is running */

INT disp_offset=0; 
INT v680_Display=0 ; /* now an odb parameter */
INT last_message = 0;

    
INT debug=0; /* general debug */
INT dss=0; /* debug for scaler event building */

INT di=0;
INT dii=0; /* debug for imusr */
INT dx=0;

    
/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop(); /* called periodically by MIDAS if frontend_call_loop=TRUE
			updates v680 display, checks for clock hang */
INT poll_event(INT source, INT count, BOOL test); /* 
	   Main loop processes v680 data; only gets called by MIDAS
           if equipment MUSR_TD_ACQ is enabled (the trigger event). */
void  hot_display (INT, INT, void * );
INT trigger_read(char *pevent, INT off);     /* dummy */
INT scaler_read(char *pevent, INT off);  /* dummy */
INT imusr_acq(char *pevent, INT off);    
INT setup_hotlink(void);
INT setup_hotlink_BOR(void);
void close_hotlinks();
INT scaler_init(void);
INT scaler_start(void);
INT scaler_stop(void);

INT vmeio_init(void);
void vmeio_on(void);
void vmeio_off(void);
INT check_update_time(INT max_sec);
INT create_records(void);    
INT get_run_state(void);

INT info_odb(char *pevent, INT off);
    
INT send_data_point(char *pevent, INT off);
void  imusr_display(int onoff);
void  imusr_update(void);
float smin(float a, float b);
float smax(float a, float b);
INT get_limits (char *pinner, char *pouter, float start,float stop, float innertogval, float outertogval,float *pmin, float*pmax);
INT start_sweep_cycle(void) ;   
INT scaler_sum(void);  /* read/clear/sum scalers */  
void keep_on_toggling(void);
  INT check_input_toggle_states(void);

/* general sweep routines */

void  hot_sweep_step (HNDLE hDB, HNDLE hktmp ,void *info  );
void  hot_sweep_stop (HNDLE hDB, HNDLE hktmp ,void *info  );
void  hot_sweep_start (HNDLE hDB, HNDLE hktmp ,void *info  );
INT validate_sweep_values(void); /* make sure parameters are valid (general) */
INT init_sweep_dev(char * primdev);
INT set_sweep_dev(char * primdev, float setval);
INT check_sweep_params(char * primdev, float start_val, float stop_val); /* calls specific device routine to check sweep params are within
					      range for this device */
    
/* specific device sweep routines */
INT  check_sweep_params_CAMP(); /* dummy presently */
INT check_sweep_params_DAC(float start_val, float stop_val );   
INT init_DAC(); /* nothing to do here */
INT set_DAC(float setval);
    
BOOL trigger_cvar_event(INT *state_running);
    /* IMUSR */

/* CAMP */
INT camp_create_rec(void);
INT camp_get_rec(void);
INT camp_update_params(void);
INT check_ramp_status(INT *ramp_status);
INT camp_reconnect(void);
INT camp_watchdog(void);

void close_cvar_handle(void);
void get_cvar_handle(void);


INT gbl_num_virtual_scaler_chans;  /* number of virtual scaler channels to be sent out */
INT gbl_len_data; /* no. words collected in gbl_data_array (10 or 20 for FAST MODE TRUE)*/
INT gbl_len_data_bank; /* no. words in data bank IDAT to be sent out (no. depends on toggle and fast mod conditions) 
			the valid data are selected from gbl_data_array */    
 
  float gbl_sweep_start; /* start value of sweep - calculated using calibration  */
  float gbl_sweep_stop; /* end value of sweep - calculated using calibration   */
  float gbl_sweep_step; /* step value of sweep - calculated using calibration   */

  float gbl_sweep_value; /* current sweep value */
  float gbl_conversion_factor; /* conversion from sweep setting to saved sweep value */ 
   
  /* actual value of sweep device given by gbl_actual_SD_value */
  float gbl_actual_SD_value;/* actual value set on Sweep Device (calculated
				 from the current sweep value depending on
				 toggle selection ) */

  float gbl_modified_sweep_value; /* sweep value modified by
				       outer_toggle_cycle */
  
  INT gbl_sweep_direction; /* direction of sweep (+1 or -1) */
  INT tog_off=0,tog_on=1;  /* toggle states correspond to bit patterns of IOREG bits
			      which we will call tog_off and tog_on   */
   
    /* counters */
    INT gbl_data_point_counter,gbl_sweep_counter,gbl_inner_toggle_counter;
    INT gbl_preset_counter, gbl_outer_toggle_counter;

    INT gbl_inner_toggle_state,gbl_outer_toggle_state ;

    INT gbl_poll_counter=0; /* number of times poll_event got called before data
			     was ready */
    INT gbl_sweep_inc; /* count the sweep increments for display only */

    INT gbl_outol_counter, gbl_intol_counter;
    
/* flags */
    BOOL gbl_out_of_tolerance_flag=TRUE; /* true if out of tolerance */

    BOOL gbl_active_flag; /* flag that indicates status of IOREG bit
			    IO_ACQ_ACTIVE i.e. is true if running AND in
			    tolerance */
    BOOL gbl_in_sweep,gbl_in_inner_toggle,gbl_in_preset; /* frontend_loop uses */
    BOOL gbl_in_outer_toggle;                            /* these to restart the
							    cycle */
    
    BOOL gbl_hold_flag; /* true if run is paused */
    
    BOOL gbl_timeout_flag; /* indicates timeout in poll_event */
    BOOL gbl_pause_midcycle_flag; /* indicates we paused while
				     waiting for a data point */
    BOOL gbl_pflag; /* flag to indicate we are in the process of continuing
		       through toggle cycle until we get back into tolerance
		       - used in imusr_acq if keep_toggling_out-of-tol flag is true */
    BOOL gbl_fast_mod; /* set if fast_modulation is enabled and 2 extra scaler channels are present */
  BOOL gbl_BOR_flag = TRUE; /* indicate to preset_cycle a call from BOR */
  BOOL gbl_redo_display = FALSE; /* flag indicates fixed part of display needs to be redone */
  BOOL BOR_paused = FALSE; /* if starting in paused mode, generates a message on v680 screen */

/* normalization */
    INT gbl_ticks0=0; /* normalization rate, must be initialized to 0
			 calculate rate by setting this to 0 initially */
    DWORD gbl_read_preset_value=-1; /* value read from preset counter (gbl for display) */

    DWORD gbl_norm_time; /* normalization time, calculated by setting gbl_ticks0=0 initially */
    DWORD gbl_new_ticks; /* value read from clock scaler by poll_event */
    DWORD gbl_total_rate; /* total rate read from scaler in imusr_acq */
    DWORD gbl_TotR0; /* Total rate during normalization, used for constant time mode instead of gbl_norm_time */
    char gbl_inner_toggle_type[5];
    INT gbl_num_inner_toggles;
    char gbl_outer_toggle_type[5];
    INT gbl_num_outer_toggles;    

    float gbl_tol; /* tolerance level as a fraction */
  float gbl_outol_delay;

    BOOL waiteqp[4]={FALSE,FALSE,FALSE,FALSE};

  BOOL gbl_norm_timeout=FALSE; /* indicates a timeout during normalization */
  INT gbl_norm_timeout_cntr; /* number of times normalization has been restarted due to timing out */

    INT pp; /*temp */

    /* timers
       All timers must be DWORD (for ss_millitime)
     */

    /* Timers for the whole run */
    DWORD gbl_run_start_time; /* time when run started */
    DWORD gbl_total_pause_time;
    DWORD gbl_total_active_time; /* total on - total paused time
			 i.e. gbl_run_start_time - gbl_total_pause_time */
    
    DWORD gbl_pause_start; /* time when run is paused or goes out of tolerance
			      set to zero when daq resumes  */
    DWORD gbl_total_run_time; /* gbl_run_start_time - present time */
    /* timers for polling and for display/debugging */
    DWORD gbl_start_acq=0; /* time when preset_cycle starts the acquisition,
			      i.e. we start  waiting for the data to be
				    acquired  (MUST init to zero for frontend_init to
				    calibrate polling loop) */

    DWORD gbl_acq_cycle_time; /* total time taken by this acquistion cycle
			       (may include several times round the polling loop
			       = gbl_poll_time + time spent out of polling loop */
   
    DWORD gbl_start_poll; /* time when  polling loop (poll_event) started */
    DWORD gbl_poll_time; /* time spent actually polling (may include several polling loops) in poll_event */			    
    DWORD gbl_last_sent; /* for display */

  DWORD gbl_poll_test_time; /* time to run through the polling loop */
  DWORD gbl_send_time,gbl_wait_time; /* TEMP for display */
    
    /* scaler */
    double gbl_scaler_sums[MAX_SCALER_SUMS]; /* double precision floating point */
    double gbl_data_array[MAX_SCALER_SUMS + NUM_EXTRA_DATA]; /* 2 extra ; data point counter & set point */
    /* Sweep Device specific :
       VME DAC */
    INT gbl_DAC_range;
    
    BOOL gbl_check; /* true if MUSR_CONFIG flag is enabled  */
		       
    char str_out[80];

  char disp_units[2];
    
    /* equipment defaults */
#define MUSR 0
#define SEND_DATA 1
#define CYCLE 2
#define DIAG 3
    
    
/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
    EQUIPMENT equipment[] = {
      { "MUSR_I_Acq",            /* equipment name */
	6, 0,                 /* event ID, trigger mask */
	"",             /* event buffer  */
	EQ_POLLED,            /* equipment type */
	0,                    /* event source */
	"MIDAS",              /* format */
	TRUE,                 /* must be enabled or  */
	RO_RUNNING | RO_PAUSED,   /* read when running or paused */
	5,           /* poll for 5ms  (poll as short a time as possible) */
	0,                    /* stop run after this event limit */
	0,                    /* number of sub event */
	0,                    /* don't log history */
	"", "", "",
	imusr_acq,           /* readout routine */
	NULL, NULL,NULL       /* keep null */
      },
      
      { "Send_Data",             /* equipment name */
	7, 0,                 /* event ID, trigger mask */
	"SYSTEM",             /* event buffer */
	EQ_PERIODIC,       /* equipment type */
	0,                    /* event source */
	"MIDAS",               /* format */
	TRUE,                 /* enabled */
	RO_RUNNING,       /* read when running */
	POLL_SEND_DATA,      /* read every so often */
	0,                    /* stop run after this event limit */
	0,                    /* number of sub event */
	0,                    /* log history */
	"", "", "",
	send_data_point,     /* readout routine */
	NULL,NULL,NULL       /* keep null */
      },
      
      { "Info_ODB",            /* equipment name */
	8, 0,                 /* event ID, trigger mask */
	"",             /* don't send out in event buffer */
	EQ_PERIODIC,            /* equipment type */
	0,                    /* event source */
	"FIXED",               /* format */
	TRUE,                 /* enabled */
	RO_RUNNING | RO_ODB,  /* read only when running  and update ODB */
	POLL_INFO,      /* read out every so often */
	0,                    /* stop run after this event limit */
	0,                    /* number of sub event */
	0,                    /* don't log history */
	"", "", "",
	info_odb,            /* readout routine */
	NULL,NULL,NULL        /* keep null */
      },

      /* when we combine we will need these... */
      { "MUSR_TD_Acq",            /* equipment name */
	1, 0,                 /* event ID, trigger mask */
	"SYSTEM",             /* event buffer */
#ifdef USE_INT
	EQ_INTERRUPT,         /* equipment type */
#else
	EQ_POLLED,            /* equipment type */
#endif
	0,                    /* event source */
	"MIDAS",              /* format */
	FALSE,                 /* disabled  */
	RO_RUNNING,           /* read only when running */
	100,                  /* poll for 100ms */
	0,                    /* stop run after this event limit */
	0,                    /* number of sub event */
	0,                    /* don't log history */
	"", "", "",
	trigger_read,   /* readout routine */
	NULL, NULL,NULL       /* keep null */
      },
      { "Scaler",             /* equipment name */
	3, 0,                 /* event ID, trigger mask */
	"SYSTEM",             /* event buffer */
	/*"",         */          /* Don't send data */
	EQ_PERIODIC,          /* equipment type */
	0,                    /* event source */
	"MIDAS",               /* format */
	/* TRUE,                 /* enabled */
	FALSE,                 /* disabled */
	RO_RUNNING |
	RO_TRANSITIONS |      /* read when running and on transitions */
	RO_ODB,               /* and update ODB */ 
	POLL_EQUIPMENTS,      /* read every so often */
	0,                    /* stop run after this event limit */
	0,                    /* number of sub event */
	0,                    /* log history */
	"", "", "",
	scaler_read,    /* readout routine */
	NULL,NULL,NULL       /* keep null */
      },
      
      
      { "" }
    };
    
    
#ifdef __cplusplus
}
#endif

#include "musr_common_subs.c" /* include common subroutines */

/********************************************************************\
 Callback routines for system transitions
                                                                     
These routines are called whenever a system transition like start/
stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.
  
  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  char str_set[128];
  HNDLE hktmp;  

  KEY key;
  HNDLE hKeyS;
  int i = 0;
  char myKey[256];

  gotCamp=FALSE;
  gbl_start_acq=0; /* IMPORTANT - must be zero for calibration of poll_event */

/* Turn the v680 display OFF and clear the screen */
  v680_Display = 0;
  /*  ss_clear_screen(); */


  
  printf("\n");
  printf("Front End code for MUSR ( femusr version %.2f) now running ...\n",This_Version);
  cm_msg(MINFO,"frontend_init","connecting to experiment...");


  /* to get ODB  parameters  */
  status=cm_get_experiment_database(&hDB, NULL);
  if(status != CM_SUCCESS)
  {
    cm_msg(MERROR,"frontend_init","could not connect to experiment");
    return CM_UNDEF_EXP;
  }

  

  if(debug)
    {
      printf("TD-MUSR equipment name: %s\n",td_eqp_name);
      printf(" I-MUSR  equipment name: %s\n",imusr_eqp_name);
    }
  
  /* check for current run state if not stopped stop it */
  /* get current run state */
  rstate=get_run_state();
  if(rstate==-99)
    {
      cm_msg(MERROR,"frontend_init","cannot access /runinfo/State , odb may be locked");
      return  FE_ERR_HW;
    }

  
  printf("frontend_init:  Run statue rstate = %d\n", rstate);
  if (rstate != STATE_STOPPED) 
    {
      char str[128];
      /* stop run */
      /* May 2001 and June 2003 - This mechanism does not work
	  All I can do is 
	 write a message to warn the user  ... */ 
      /*      if (cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, FALSE) != CM_SUCCESS)
	      {
	      cm_msg(MERROR, "frontend_init", "cannot stop run: %s", str);
	      return FE_ERR_HW;
	      }
      

	      cm_msg(MINFO,"frontend_init","Forced run to stop");
	      ss_sleep(500); /* wait for 500 ms */
#ifdef GONE
      {  /* future... set a flag */
	BOOL bval=TRUE;
	size = sizeof(bval);
	status=db_set_value (hDB, 0,"/equipment/info_odb/variables/stop the run", &bval, size, 1, TID_BOOL);
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"frontend_init","could not set flag \"/equipment/info_odb/variables/stop the run\" (%d) ",status);
	    return FE_ERR_ODB;
	  }
      }
#endif
      cm_msg(MERROR, "frontend_init", " ***  Stop run and reboot *** ");
      return FE_ERR_HW;
    }
  
  
  status = create_records();
  if (status != DB_SUCCESS)
    return status;

/* initialize the IO register */
  status = vmeio_init(); /* turns all IO Reg  channels OFF */
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"frontend_init","error return from vmeio_init; status %d", status);
    return FE_ERR_HW;
  }

  status = setup_hotlink();  /* open hotlink(s) that are always open */
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
  
  printf("Success from setup_hotlink\n");

  /* Temp : setup these hotlinks now instead of at BOR as close_record doesn't seems to work */
  setup_hotlink_BOR(); /* setup hotlinks that are open when running */
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;
  
  printf("Success from setup_hotlink_BOR\n");
  
  if (vs.display_period__s_ > 1) /* minimum value 1s */    
    v680_Display_period = vs.display_period__s_ * 1000; /* ms;  use default otherwise */
     
  ss_sleep(500);

  printf(" \n");
  if(vs.display==1)
    printf("v680 display is enabled with update period of %d sec \n",v680_Display_period/1000);
  else
    printf("v680 display is disabled\n");
  
  /* This is done in camp_acq routines
     do this when camp is used for the sweep  
     #ifdef OS_VXWORKS
     printf("frontend_init: calling rpcInit....\n");
     rpcInit( );
     printf("frontend_init: calling rpcTaskInit....\n");
     rpcTaskInit( );
     printf("frontend_init:  rpcTaskInit done\n");
     #endif
     status = camp_clntInit( "m9bvw", 200 );
    
     printf("status after camp_clntInit = %d\n",status);
  */

  printf("Frontend_init: returning success, camp not yet initialized\n");
  
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  int max=60;
  int i,j,len;
  char str_set[128];
  float sweep_max,sweep_min;
  BOOL flag;

  printf("begin_of_run starting\n");

  /* NOTE:  if enabled, mdarc has initialized frontend flag in ODB to FAILURE  on prestart */
  gbl_status=SUCCESS; /* set to success for frontend_loop */

  
  /* Turn the v680 display OFF and clear the screen */
  /* v680_Display = msg_cnt = 0;*/
  /* later ... ss_clear_screen(); */
  v680_Display  =  vs.display;
  if (vs.display_period__s_ > 1)    
    v680_Display_period = vs.display_period__s_ * 1000; /* ms; use default otherwise */
  printf("BOR: v680_Display = %d\n",v680_Display);

  
  status = get_musr_type();
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR, "begin_of_run", "cannot determine MUSR run type (I or TD)");
    return DB_NO_ACCESS;
  }
  if (!I_MUSR)
  {
    cm_msg(MERROR, "begin_of_run", "MUSR run type must be I_MUSR for this frontend code");
    return DB_NO_ACCESS;
  }
 
  /* Get the flag that allows mdarc to stop the run (from client_flag area of odb)  */
  size = sizeof(client_check); 
  sprintf(str_set,"/Equipment/%s/client flags/enable client check",td_eqp_name); 
  status = db_get_value(hDB, 0, str_set, &client_check, &size, TID_BOOL, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MINFO,"begin_of_run","cannot get value of \"%s\" (%d)",str_set,status);
      return (DB_NO_ACCESS);
    }
  if(client_check)  /* who_stops_run is used for messages only */
    sprintf(who_stops_run,"mdarc");
  else
    sprintf(who_stops_run,"user");
 


  /*   VMS "toggle" didn't seem to get used
   gbl_sweep_counter =  VMS "sweep"
   gbl_inner_toggle_counter = VMS  toggle2
   get gbl_inner_toggle_type
  */

/* Start in paused condition */
  gbl_hold_flag = TRUE; 
  gbl_out_of_tolerance_flag = TRUE;
  os.in_tolerance = FALSE ;
  gbl_active_flag = FALSE; /* VMS labels this as hardware toggle flag */

  
  /* initialize a load of globals */
  gbl_sweep_counter = gbl_inner_toggle_counter  = gbl_preset_counter =0;
  gbl_data_point_counter =  os.total_data_points = os.timeout_counter = 0;
  gbl_sweep_inc = 0; /* for display only */
  gbl_sweep_value = gbl_total_run_time = gbl_total_active_time = os.total_counts = os.total_mucounts =  0.0; 
  gbl_norm_time = gbl_TotR0 = gbl_ticks0 = gbl_new_ticks= gbl_total_rate=0; /* start by renormalizing */
  gbl_norm_timeout = FALSE;
  gbl_norm_timeout_cntr=0;
  gbl_in_sweep = gbl_in_preset = FALSE;  
  gbl_in_inner_toggle = gbl_in_outer_toggle = FALSE;
  gbl_outol_counter = gbl_intol_counter= os.num_camp_triggered=os.total_data_points=0;

  waiteqp[MUSR] = waiteqp[DIAG]=waiteqp[SEND_DATA] = FALSE;


     /* Get current v680 settings  - needed for various parameters in
     check_update_time so get_rec must come BEFORE check on musr_config*/
  sprintf(str_set,"/Equipment/%s/v680",td_eqp_name); 
  size = sizeof(vs);
  status = db_get_record(hDB, hV, &vs, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "begin_of_run", "cannot retrieve %s record (size of vs=%d)", str_set,size);
    return DB_NO_ACCESS;
  }
  
 
  /* check that musr_config has run recently ( < max seconds ago) */
  status = check_update_time(max);
  if(debug)printf("begin_of_run: check_update_time returns with status %d\n");
  if(status != SUCCESS)
    return FE_ERR_HW;    /* error return */



  
  /* Get current IMUSR  settings - we don't use ioreg bit pattern */
  /* Get the record for IMUSR/Settings area */
  
  sprintf(str_set,"/Equipment/%s/settings",imusr_eqp_name); /* for error message */
  size = sizeof(is);
  status = db_get_record(hDB, hIS, &is, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "begin_of_run", "cannot retrieve %s record; struct size=%d (%d)",
           str_set,size,status);
    return FE_ERR_ODB;
  }
  if(di)
  {
    printf("begin_of_run: sweep start=%f stop=%f step=%f \n",
	   is.input.sweep_start,is.input.sweep_stop,is.input.sweep_step);
    printf("begin_of_run: sweep device %s, num toggles %d,presets %d,sweeps %d, toggle type %s\n",
	   is.input.sweep_device , is.input.num_inner_toggle_cycles , is.input.num_presets , is.input.num_sweeps , is.input.inner_toggle_type);
    printf("begin_of_run: tolerance %f, out-of-tol delay %f, \nSettling times: toggle %f,step %f, toggle value %f\n",
	   is.input.tolerance____ , is.input.out_of_tolerance_delay__s_ , is.input.inner_toggle_settling_time__ms_ , is.input.step_settling_time__ms_, is.input.inner_toggle_value);
  
  }
  gbl_outol_delay = is.input.out_of_tolerance_delay__s_; /* remember this value for hotlink */ 
  gbl_tol = is.input.tolerance____ / 100.0; /* calculate as a fraction */
  
  status = scaler_init();  /* gets record for scaler */
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","scaler_init not successful (%d)", status);
    return FE_ERR_HW;
  }

 
  /*
    Calculate the number of virtual scaler channels gbl_num_virtual_scaler_chans.
    Must be done AFTER scaler_init which sets up gbl_fast_mod 


     front/back scaler counts split into  8 virtual scaler channels in
     gbl_scaler_sum  and scaler_counts arrays.

     Total no. of virtual scalers for FIXED inputs is
     SCALER_SUM_CHANNELS = 2 (clock, total_rate) + 8 (back/front) = 10
     or with FAST MOD ENABLED,
     SCALER_SUM_CHANNELS = 2 (clock, total_rate) + 8 (back/front FMOD=OFF ) + 8 (back/front FMOD ON) = 18
  */
  gbl_num_virtual_scaler_chans =  SCALER_SUM_CHANNELS ; /* */
  disp_offset = 0;
  
  if(gbl_fast_mod)
  {
    disp_offset=3;  /* need extra 3 lines for display */
    /* add FM_SUM_CHANNELS virtual scaler channels  i.e. 8  */
    gbl_num_virtual_scaler_chans +=  FM_SUM_CHANNELS; 
  }

  /* add NUM_EXTRA_DATA=2 words (datapoint counter & setpoint ) */
  gbl_len_data = gbl_num_virtual_scaler_chans + NUM_EXTRA_DATA;
  /*
    IMUSR display is of scaler channels mapped according to toggle type.
    gbl_num_virtual_scaler_chans will be used for scaler for display
  */


  status = vmeio_init(); /* turns all IO Reg  channels OFF */
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from vmeio_init; status %d", status);
    return FE_ERR_HW;
  }
 

  status = ggl_init(); /* loads preset value for IMUSR (down counter preset)  */


  if (status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from ggl_init; status %d", status);
    return FE_ERR_HW;
  }

  /* Set IO reg channels ON  according to input parameters
     All are currently OFF (vmeio_init)
   */
#ifdef OS_VXWORKS
  if (is.input.normalize_on_e_rate)
    musrVmeioOn(IO_NORM_E);

  /* scaler_init has set up gbl_fast_mod  */
  if(gbl_fast_mod)
    musrVmeioOn(IO_FAST_MOD);

  if(is.input.constant_time)
    musrVmeioOn(IO_CONST_TIME);  
#endif
  


  gbl_data_point_counter = 0;


  /* get the toggle types from odb */
  sprintf(gbl_inner_toggle_type,"%s",is.input.inner_toggle_type);
  len=strlen(gbl_inner_toggle_type);
  for(j=0; j<len; j++)
    gbl_inner_toggle_type[j] = toupper (gbl_inner_toggle_type[j]); /* convert to upper case */

  
  sprintf(gbl_outer_toggle_type,"%s",is.input.outer_toggle_type);
  len=strlen(gbl_outer_toggle_type);
  for(j=0; j<len; j++)
    gbl_outer_toggle_type[j] = toupper (gbl_outer_toggle_type[j]); /* convert to upper case */    
  
  
  gbl_num_inner_toggles = is.input.num_inner_toggle_cycles;  /* cf VMS "toggles" - logical name */
  gbl_num_outer_toggles = is.input.num_outer_toggle_cycles; 

  /* Toggle Modes:

     NONE  -  tog_state is always tog_off 
     HARD     cycle IO MOD  bits between tog_off and tog_on when toggle changes
     For now, SOFT / REF also cycle bits
  */
  
  /* initialize toggle state for all toggle states */
  gbl_inner_toggle_state = gbl_outer_toggle_state = tog_off; /* IO_MOD bits ON */
  


  /* try to cut down on checking since musr_config should be running and should  have done 
     all this checking  */ 
  if(!gbl_check)
    {
      printf("begin_of_run: Calling check_input_toggle_states since musr_config is not running\n");
      status = check_input_toggle_states();
      if(status != SUCCESS ) return status;
    }


  /* now calculate how many data words will actually be sent out in the data bank 
     (depends on toggle types) - should have been done already by imusr_config 
  */

  
  /*  Note:  outer toggle must also be NONE if inner toggle is NONE */
  if ( strcmp("NONE",gbl_inner_toggle_type) == 0)
    gbl_len_data_bank=2;
  else
    {
      if ( strcmp("NONE",gbl_outer_toggle_type) == 0)
	gbl_len_data_bank=4;
      else
	gbl_len_data_bank=8;
    }
  if(gbl_fast_mod)
    gbl_len_data_bank=gbl_len_data_bank*2; /* double the words */
  gbl_len_data_bank=gbl_len_data_bank+4; /* add in data point counter, sweep value, clock, total_rate */
  printf("Number of words in data array = %d, no. words in data bank = %d\n",gbl_len_data,gbl_len_data_bank);
	 
  /* check value stored in odb for imdarc */
  
  if(is.imdarc.num_datawords_in_bank != gbl_len_data_bank)
    {
      if(gbl_check)
	{      /* musr_config should have updated this value in odb */
	  
	  cm_msg(MINFO, "begin_of_run", "Calculated no. data words in sum scaler bank (%d) disagrees with odb (%d)",
		 gbl_len_data_bank,is.imdarc.num_datawords_in_bank);
	  cm_msg(MINFO,"begin_of_run","imusr_config should have updated this. Updating odb now and continuing");
	  
	}
      
      /* update the odb */
      is.imdarc.num_datawords_in_bank = gbl_len_data_bank;
      size=sizeof(is.imdarc.num_datawords_in_bank);
      status=db_set_value (hDB, hIS, "imdarc/num datawords in bank", &is.imdarc.num_datawords_in_bank, size, 1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"begin_of_run","could not set value=%d to \"imdarc/num datawords in bank\" (%d) ",is.imdarc.num_datawords_in_bank,status);
	  return FE_ERR_ODB;
	}    
    }
  printf("Number of words in data bank will be %d\n",is.imdarc.num_datawords_in_bank);
 



  /* 
     Perform checks on sweep values (including toggle_values) 
     musr_config (if running) should also have done these checks
  */


  status = validate_sweep_values(); /* fills gbl_sweep_start etc. */
  if(status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from validate_sweep_values (%d)", status);
    return FE_ERR_HW;
  }



  /* get the limits of the sweep according to toggle type (adding in the toggle_value if necessary) */
  printf("begin_of_run: calling get_limits with inner:%s outer:%s start:%f stop:%f intogval:%f outtogval:%f \n",
	 gbl_inner_toggle_type, gbl_outer_toggle_type, 
	 gbl_sweep_start, gbl_sweep_stop,
	 is.input.inner_toggle_value,is.input.outer_toggle_value);
  
  if(get_limits (gbl_inner_toggle_type, gbl_outer_toggle_type, 
	 gbl_sweep_start, gbl_sweep_stop,
		 is.input.inner_toggle_value,is.input.outer_toggle_value,
		 &sweep_min,&sweep_max)==SUCCESS)
    printf(" BOR: after get_limits, sweep_max=%f,sweep_min=%f\n",sweep_max,sweep_min);
  else
    {
      cm_msg(MERROR,"begin_of_run","error return from get_limits; cannot check sweep device parameters");
      return FE_ERR_HW;
    }
 

  /* make sure params are valid for specific sweep device & determine any other
     parameters needed (e.g. range for DAC)  */
  status = check_sweep_params( is.input.sweep_device,
		   sweep_min, sweep_max); /* validate_sweep_values filled gbl_sweep_start and stop */
  if(status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from check_sweep_params (%d)", status);
    return FE_ERR_HW;
  }
  
  /*gbl_sweep_direction *= -1;  /* reverse sweep direction to satisfy start_sweep_cycle routine
                                   don't think this is needed*/
  
 
  gbl_sweep_value = gbl_sweep_start;
  
  /* printf("begin_of_run:  Selected %d  sweeps starting with %f %s by steps of %f %s\n",
	  is.input.num_sweeps, gbl_sweep_start, disp_units, gbl_sweep_step, disp_units );
  */

  if(is.input.num_sweeps > 0)
    {
      if(client_check)  
	{  /* we assume imdarc is running  */
	  cm_msg(MINFO,"begin_of_run","Mdarc will stop the run after %d sweeps are complete",
		 is.input.num_sweeps);	    
	}
      else
	{
	  cm_msg(MINFO,"begin_of_run",
		 "Mdarc auto stop is disabled. User must stop run when indicated (after %d sweeps are completed)",
	     is.input.num_sweeps);	
	} 
    }
  
  /* Initialize sweep device (SD) */
  status = init_sweep_dev(is.input.sweep_device); 
  if(status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","Error initializing sweep device" );
    return FE_ERR_HW;
  }


  /* Set SD to initial value */
  status = set_sweep_dev (is.input.sweep_device,gbl_sweep_value); /* do not wait step_settletime */
  if(status != SUCCESS)
    { /* set_sweep_dev retries before giving up & setting flag for mdarc to stop the run */
    cm_msg(MERROR,"begin_of_run","Error setting sweep device  to %f", gbl_sweep_value );
    return FE_ERR_HW;
  }
  gbl_actual_SD_value = gbl_sweep_value; /* for display only */ 
  /*printf("begin_of_run: set SD to %f\n",gbl_actual_SD_value); */
  
 

  scaler_start(); /* start the scaler */
  gbl_sweep_counter++;  /* first sweep */
  
  gbl_total_pause_time = 0; /* initialize run timers */
  gbl_run_start_time  = ss_millitime(); /* time when run was started */
  gbl_pause_start = gbl_run_start_time; /* run is started in paused state */

  /* 
     BOR calls start_sweep_cycle to start the sweep
  */
  gbl_BOR_flag = TRUE; /* set a flag for preset_cycle to check and then clear */
  status = start_sweep_cycle(); /* start the sweep */
  if (status != SUCCESS)
  {
    /* Run should be stopped on failure */
    cm_msg(MERROR,"begin_of_run","Error return from start_sweep_cycle; stop the run and restart!! (%d) ",status);
    return(status);
  }
  

  size = sizeof(n_camp_logged);
  sprintf(str_set,"/equipment/camp/settings/n_var_logged");
  status = db_get_value(hDB, 0, str_set, &n_camp_logged, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","cannot access \"%s\"",str_set);
    n_camp_logged=0;
  }
  printf("begin_of_run:Number of camp variables to be logged=%d\n",n_camp_logged);
  hconn = -1; /* initialize handle (global) */
  if(n_camp_logged > 0)
    get_cvar_handle();

    ss_sleep(3000); /* wait 3s so we can read the info. */


  /* Temp : setup these hotlinks in frontend_init instead of at BOR as close_record doesn't seems to work */
  /*  setup_hotlink_BOR(); /* setup hotlinks that are open when running */
  /*  if (status != DB_SUCCESS)
      return FE_ERR_ODB;

     printf("Success from setup_hotlink_BOR\n");
  */
  /*
    Now  enable v680 display ONLY if key v680/display is enabled (hot linked)
    AFTER any message have been written by above subroutines
  */
  if (v680_Display )
    {
      printf("BOR: v680_Display is true\n");
    gbl_redo_display = TRUE; /* set a flag */
  /*    imusr_display(1);  call this with onoff=1 */
    }
  
  /* set client status flag for frontend to SUCCESS */
  flag=SUCCESS;
  status = set_client_flag("frontend",flag) ; /* set client flag to success */  
  if(status != DB_SUCCESS)  
    return status;

  if(di)printf("begin_of_run: returning success\n");
  return SUCCESS;  
  
  

}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  scaler_stop(); /* stop the scaler */
  /* remove hotlinks that are only active during the run */
  /*  close_hotlinks();   TEMP db_close not working (Midas) */
  if(n_camp_logged > 0)
    close_cvar_handle();

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  DWORD time;

  /* set IO reg to disable incoming signals */
  cm_msg(MINFO,"pause_run","Pausing run");
  /* printf("pause_run: pausing by setting hold flag & IO_ACQ_ACTIVE off\n");*/
  
#ifdef OS_VXWORKS
  status = musrVmeioOff(IO_ACQ_ACTIVE);
#endif
  gbl_active_flag = FALSE;
  gbl_hold_flag = TRUE;
  gbl_pause_start = ss_millitime(); /* time when we paused */
  if(waiteqp[MUSR])
    gbl_pause_midcycle_flag = TRUE; /* set a flag as we are waiting for data
				       i.e. in the middle of polling  */
  BOR_paused=FALSE; /* clear this flag; gives a message */
/* disable the scaler */
/*  scaler_stop(); */
  
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  gbl_hold_flag = FALSE;
  BOR_paused = FALSE;
  /* if tolerance check is enabled, imusr_acq will calculate total_pause_time when
   beam is back in tolerance */
  if(!is.input.tolerance_check)
  {  /* no tolerance check */
    
    /* calculate how long we were out of tolerance
       and add this preset cycle time to the total paused time */
    gbl_total_pause_time += (ss_millitime() - gbl_pause_start);
    gbl_pause_start = 0; /* indicate not longer paused/out-of-tol */
  }

  printf("resume_run: hold_flag %d; total_pause_time=%d\n",
	 gbl_hold_flag,gbl_total_pause_time);
/*  if (fs.enabled)   /* if scaler is enabled */
/*    {
      if(dss)printf("resume_run: enabling scaler\n");
      #ifdef OS_VXWORKS
      sis3803_all_enable(SIS_base_adr);    /* enable global counting so module acquires counts */ 
/*# endif
  }
*/
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  
  /* Main function for restarting a cycle */
  
  /* We have three MAIN cycles :
                          SWEEP i.e. a complete sweep of the sweep device between gbl_sweep_start and gbl_sweep_stop
                                     comprises a sweep_cycle - at the end of this the direction of the sweep is reversed
                          TOGGLE a complete outer and inner toggle cycles (at a particular sweep voltage) includes all the preset cycles and
			             finishes with the data being sent out to archive.
		           PRESET several preset cycles are done at one sweep and toggle value before the data is archived

			This routine runs continuously and keeps restarting the cycles
			
 */

  /*
    Note: v680/display is normally true... use ss_printf or cm_msg except for debug
  */ 
  INT   j, temp, first;
  INT   local2;

  
  temp =  ss_millitime();
  local = ss_millitime() - last_display;
  
  if ( (local > v680_Display_period) || first_time_display )
    {
      
      if (di)
	{
	  printf("frontend_loop: Time to check for display\n"); 
	  printf("frontend_loop: starting.. total_active_time=%d, total pause time=%d, gotCamp=%d\n",
		 gbl_total_active_time,gbl_total_pause_time,gotCamp);
	  printf("frontend_loop: poll_count=%d, loop called %d times; time in polling loop(s) %dms; poll cycle time = %dms\n",
		 gbl_count,gbl_poll_counter,gbl_poll_time,gbl_acq_cycle_time);
	  
	  /*printf("gbl_start_poll = %d, gbl_start_acq = %d, gbl_poll_counter=%d\n", 
	    gbl_start_poll,gbl_start_acq,gbl_poll_counter); */ 
	}
      if(gotCamp || reconnect)
	{   /* camp connection is open or we need to reconnect */
	  local2 = ss_millitime() - last_camp;
	  if(  local2 >  camp_Watchdog_period)
	    {
	      if(!reconnect)
		{
		  if(di)printf("frontend_loop: CAMP watchdog accessing CAMP, camp_errcount=%d\n",
			       camp_errcount);
		  status=camp_watchdog();
		  if(status != CAMP_SUCCESS)
		    {
		      printf("frontend_loop: FAILURE from CAMP watchdog, camp_errcount=%d\n",camp_errcount);
		      camp_errcount++;
		      if(camp_errcount > 2)
			reconnect=TRUE;
		    }
		}
	      if(reconnect)
		{ /* reconnect only when run is off */
		 
		  /* get current run state */
		  rstate=get_run_state();  /* when running, set_sweep_val will reconnect */
		  if(rstate==STATE_STOPPED)
		    {
		      if(di)printf("calling camp_reconnect, reconnect=%d,camp_errcount=%d\n",
				   camp_reconnect,camp_errcount);
		      status = camp_reconnect();/* try to reconnect to CAMP */
		      if(status==SUCCESS)
			{
			  camp_errcount=0;
			  printf("Successfully reconnected to CAMP, gotCamp=%d\n",gotCamp);
			  reconnect=FALSE;
			}
		      else  /* gotCamp will now be false */
			{
			  printf("frontend_loop: failure reconnecting to CAMP, gotCamp=%d\n",gotCamp);
			  if(camp_errcount> 25)
			    {  /* give up */
			      printf("frontend_loop: cannot reconnect to camp; giving up\n");
			      reconnect=FALSE;
			      gotCamp=FALSE; 
			    }

			}
		    } /* end of not running */
		} /* reconnect */
		
	      last_camp =  ss_millitime();
	    }
	} /* end of open Camp connection */
      
      /*  time to update display if enabled */
      
      
      first_time_display = FALSE;
      last_display = ss_millitime();
      if (v680_Display != 0)
	{
	  if(di) printf("frontend_loop:updating display with v680_Display=%d\n",
			v680_Display);
	  if(gbl_redo_display)
	    {
	      imusr_display(1);
	      gbl_redo_display = FALSE;
	    }
	  imusr_update(); /*  updates display  */    
	  /* reset msg_cnt used by TD poll_event to limit messages */
	  msg_cnt=0;
	}  
      
    }
  
  /* check for current run state;     
     run_state is external 
  */
  /* get current run state */
  rstate=get_run_state();
  if(rstate==-99)
    {
      cm_msg(MERROR,"frontend_loop","cannot access /runinfo/State; odb may be locked");
      return status;
    }

  if (rstate == STATE_RUNNING || rstate == STATE_PAUSED)
    { /* RUNNING    or PAUSED as well, maybe */
	  /* check for problem while calibrating */

      if(!gbl_status)
	{
	  printf("frontend_loop: Waiting for run to be stopped\n");
	  cm_yield(1000);
	  return status;
	}

      if(di)
	{
	  if(waiteqp[MUSR])
	    printf("frontend_loop: waiteqp[MUSR] is true; waiting for data\n");
	}
  
      /* gbl_start_acq = 0 if actually running - gbl_ticks0=0 for normalizing */
      if( gbl_start_acq > 0 && gbl_ticks0 == 0) /* we are running & normalizing */
	{ 
	  if(di)printf("frontend_loop: checking for normalization timeout\n");

	  {
	    DWORD time1,time2;
	    time1 = ss_millitime();
	    time2 = time1 - gbl_start_acq;
	    if ( time2 > is.input.normalization_timeout__s_ * 1000)
	      {
		/*	cm_msg(MINFO,"Frontend_loop","Normalization has timed out after %1.3lf seconds; restarting normalization",
			(float)time2/1000.0);
			sprintf(display_string,"Normalization timed out .... restarting normalization ");*/ 
		/* restart normalization */

		gbl_norm_timeout=TRUE;
		/* printf("gbl_in_preset=%d, setting it FALSE\n",gbl_in_preset);*/
		gbl_in_preset=FALSE;
		
	      }
	  } /* end of timeout */
	} /* end of running and normalizing */
	      
    
      /* check if cycle is finished AND histo update being done */
      
      /*    printf("gbl_in_sweep = %d; in_outer_toggle = %d;  in_inner_toggle = %d ; in_preset = %d\n",
	    gbl_in_sweep,gbl_in_outer_toggle,gbl_in_inner_toggle,gbl_in_preset);*/
      if (!gbl_in_sweep)
	{ /* not in sweep */
	  if(waiteqp[SEND_DATA] )
	    {
	      if(dx)printf("frontend_loop: not in sweep but waiting for SEND_DATA equipment to run\n");
	      sprintf(display_string,"Waiting for SEND_DATA equipment to run");
	      return SUCCESS;
	    }
	  else
	    { /* not in sweep */
	      if(dx)printf("frontend_loop: starting a new sweep cycle\n"); 
	      status = sweep_cycle_end(); /* restart the sweep cycle - don't wait for
					     data to be sent out  - we are waiting at present*/
	      if (status != SUCCESS)
		{
		  /* Run should be restarted on error */
		  cm_msg(MERROR,"frontend_loop","Error return from sweep_cycle_end (%d). May need to restart run",status);
		  return(status);
		}
	      
	    } 
	} /* end of not in sweep */
      else if (!gbl_in_outer_toggle)
	{
	  if(di)printf("frontend_loop: calling outer_toggle_cycle\n");
	  status = outer_toggle_cycle(); /* restart the toggle cycle */
	  if (status != SUCCESS)
	    {
	      /* Run should be stopped */
	      cm_msg(MERROR,"frontend_loop","Error return from outer_toggle_cycle (%d). May need to stop run and restart",status);
	      return(status);
	    }
	  
	} /* not in outer toggle */
      else if (!gbl_in_inner_toggle)
	{
	  if(di)printf("frontend_loop: calling inner_toggle_cycle\n");
	  status = inner_toggle_cycle(); /* restart the toggle cycle */
	  if (status != SUCCESS)
	    {
	      /* Run should be stopped */
	      cm_msg(MERROR,"frontend_loop","Error return from inner_toggle_cycle (%d). May need to stop run and restart",status);
	      return(status);
	    }
	  
	}  /* not in inner toggle */
      else if (!gbl_in_preset)
	{
	  if(di)printf("frontend_loop: calling preset_cycle\n");
	  status = preset_cycle();
	  if (status != SUCCESS)
	    {
	      /* Run should be stopped */
	      cm_msg(MERROR,"frontend_loop","Error return from preset_cycle (%d). May need to stop run and restart",status);
	      return(status);
	    }
	} /* end of not in preset */
      
      else
	if(di)printf("frontend_loop: Running in cycle - nothing to do\n");	 
    } /* end of running/paused */  

    else
      {
      sprintf(display_string,"run is stopped");	
      }
  
      return SUCCESS;
}


INT sweep_cycle_end(void)
{
  /* called by frontend_loop if gbl_in_sweep is FALSE
     
  If a sweep cycle has ended and the data has been sent out,
  this routine sets a new sweep value and restarts the sweep cycle
  by calling start_sweep_cycle
  
  */
  
  float next_sweep_value;
  BOOL midsweep,flag;
  
  if(di)printf("sweep_cycle_end: starting\n");
  
  /* Do this AFTER last data point has been sent out by SEND_DATA equipment */
  if (waiteqp [SEND_DATA])
    {
      if(!v680_Display)printf("sweep_cycle_end: waiting for SEND_DATA equipment to run\n");
      return SUCCESS;
    }
  
  /* for now we wait for point to be sent out */  
  
  
  gbl_in_sweep = TRUE;  
  gbl_data_point_counter++ ; /* working on data point n_data_point */
  
  if(dx)printf("sweep_cycle_end: sweep counter = %d  working on data point %d\n",
	       gbl_sweep_counter,gbl_data_point_counter);
  
  
  
  /* calculate next sweep value */
  next_sweep_value =  gbl_sweep_value +  gbl_sweep_direction * gbl_sweep_step;
  midsweep = FALSE;
  
  
  if ( (next_sweep_value >= smin (gbl_sweep_start, gbl_sweep_stop)) &&
       (next_sweep_value <= smax (gbl_sweep_start, gbl_sweep_stop)))
    midsweep = TRUE; /* mid sweep */
  
  /* check in case (hotlinked) start or stop values have been changed midsweep */
  else if ((next_sweep_value < gbl_sweep_start) &&
	   (next_sweep_value < gbl_sweep_stop))
    {
      if (gbl_sweep_value < next_sweep_value)
	midsweep=TRUE; /* keep going in this direction */
    }
  else if ((next_sweep_value > gbl_sweep_start) &&
	   (next_sweep_value > gbl_sweep_stop))
  {
    if (gbl_sweep_value > next_sweep_value)
      midsweep=TRUE; /* keep going in this direction */
  }
  
  if (midsweep)
    {  /* do not reverse sweep direction */
      gbl_sweep_inc++; /* increment sweep increment counter (display only) */
    gbl_sweep_value = next_sweep_value;  /* increment sweep value */
    status = set_sweep_dev(is.input.sweep_device, gbl_sweep_value);
    if(status != SUCCESS)
      { /* set_sweep_device retries before giving up  & setting flag for mdarc to stop the run */
	cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	return status;
      }
    
    /* write current sweep value  to odb output area */
    size = sizeof (gbl_sweep_value);
    status=db_set_value (hDB, hOS, "current sweep setting", &gbl_sweep_value, size, 1, TID_FLOAT);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"sweep_cycle_end","could not write value=%f to \"%s/current sweep setting\" (%d) ",gbl_sweep_value,str_out,status);
	return FE_ERR_ODB;
      }
    gbl_actual_SD_value = gbl_sweep_value ; 
    if(di)printf("sweep_cycle_end:  set SD to %f\n",gbl_actual_SD_value);
    
    ss_sleep(is.input.step_settling_time__ms_); /* wait step_settletime */   
    }
  else
    {
      /*
	end of sweep  - don't change sweep value but DO change sweep direction
      */
      gbl_sweep_counter++; /* next sweep */
      gbl_sweep_inc=1; /* first sweep incremnt (display only) */
      if(dx) cm_msg(MINFO,"sweep_cycle_end","End of sweep %d, changing sweep direction",gbl_sweep_counter);
      sprintf(display_string,"End of sweep %d, changing sweep direction",
	      gbl_sweep_counter);
      gbl_sweep_direction *= -1;
      
      
      
      /* add code for mdarc to stop run here */
      
      if(gbl_sweep_counter > is.input.num_sweeps  && is.input.num_sweeps != 0) 
	{  /* fixed number of cycles per run is enabled
	      Stop run after num_sweeps   */
	  flag=FALSE;
	  status = set_client_flag("frontend",flag); /* mdarc should stop the run (hot link on client flag). */
	  cm_msg(MINFO,"sweep_cycle_end","waiting for %s to stop the run after completing %d sweeps...\n",
		 who_stops_run,is.input.num_sweeps);	
	  sprintf(display_string,"Waiting for %s to stop the run after completing %d sweeps\n",
		  who_stops_run,is.input.num_sweeps);
	  
#ifdef OS_VXWORKS
	  status = musrVmeioOff(IO_ACQ_ACTIVE);
#endif
	  gbl_active_flag = FALSE; 
	  gbl_hold_flag = TRUE; /* this causes out-of-tol condition */
	  gbl_pause_start = ss_millitime(); /* reinitialize pause start time */ 
	  gbl_pause_midcycle_flag = TRUE; /* set this flag */
	  cm_yield(1000); /* let mdarc have a chance to stop this run */
	}
      
      else
	{
	  if(dx)
	    {
	      printf("sweep_cycle_end: end-of-sweep %d - changing sweep direction to %d, gbl_sweep_value is unchanged at %f\n",
		     gbl_sweep_counter,gbl_sweep_direction,gbl_sweep_value);       
	      printf("sweep_cycle_end: starting on data point %d at SD value %f\n",
		     gbl_data_point_counter,gbl_sweep_value);
	    }
	}
      
    }
  status = start_sweep_cycle();
  if (status != SUCCESS)
    {
      cm_msg(MERROR,"sweep_cycle_end","Error return from start_sweep_cycle (%d) ",status);
      return(status);
    }
  
  return(status);
}


/********************************************************************\
  
  Readout routines for different events

********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
{
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
  
  DWORD    counter;
  INT i=0;
  DWORD time1, time2;
  float tol,ftmp;
  
  if(di)printf(" in polling, test=%d,  waiteqp[MUSR]=%d,count:%d\n",
	       test,waiteqp[MUSR],count);
  
  if(!test)
    {
      if (!waiteqp[MUSR])
	return 0; /* not waiting for data, do nothing */
#ifdef OS_VXWORKS
      /* musrVmeioOn(1); /* TEMP */
#endif
    }
  gbl_start_poll = ss_millitime();
  gbl_count=count; /* remember count for display  */ 
  gbl_timeout_flag = FALSE;
  gbl_new_ticks=gbl_read_preset_value=-1; /* initialize to -1 */
  if(gbl_poll_counter==0)gbl_poll_time=0; /* actual time spent in polling loops */
  gbl_poll_counter++;
  
  for (counter=0 ; counter<count ; counter++)
    {
      ss_sleep(delay);
#ifdef OS_VXWORKS
      gbl_read_preset_value = gglReadbackCounter(ggl_base);
      sis3803_counter_read(SIS_base_adr, SC_CLOCK, &gbl_new_ticks);
      /* read the total rate (for checking and for display) */
      sis3803_counter_read(SIS_base_adr, SC_TOTAL_RATE, &gbl_total_rate);
#endif    
      if(!test)
	{        
	  if(gbl_read_preset_value == 0)
	    {   /* preset has finished - all scalers (including clock) have stopped
		   counting */
	      
	
#ifdef OS_VXWORKS
	      /* musrVmeioOff(1);	/* TEMP  */
#endif
	      /* calculate time in polling loop (for display) */
	      gbl_poll_time+= ss_millitime() - gbl_start_poll;
	      
	      return 1 ; /* counting is done; return TRUE to trigger a call to
			    imusr_acq  */
	    }
	  else if (count == 0)
	    {
	      if(di)
		printf("poll_event: at count=0, preset value: %d clock: %d total_rate: %d\n",
		       gbl_read_preset_value,gbl_new_ticks,gbl_total_rate);
	    }
	}    
    } /* end of count loop */
  
  if(di) printf("poll_event: end of count loop\n");
  
  
  /* Prescaler did not get to zero yet */    
  if(test)
    {
      /* calculate time in polling loop (for display) */
      gbl_poll_test_time = ss_millitime() - gbl_start_poll;
      return 0;
    }
  
  
  if(gbl_ticks0 !=0 )
    {    /* we are not calibrating */
      if(is.input.tolerance_check)
	{ /* tolerance checking is enabled 
	     timeout when time > tolerated value */
	  ftmp =  (float) gbl_ticks0 * (1.0 + gbl_tol + 0.1);      
	  if(gbl_new_ticks > (int) ftmp) 
	    {
	      gbl_timeout_flag = TRUE;
	      sprintf(display_string,"polling timed out with ticks0=%d, new_ticks=%d, preset value %d ",
			gbl_ticks0,gbl_new_ticks,gbl_read_preset_value);
	      
	      /* too many messages when display is off */
	      /*	printf("poll_event: tol check ENABLED, Timed Out with gbl_ticks0=%d, gbl_new_ticks=%d, and preset_value %d\n",
				gbl_ticks0,gbl_new_ticks,gbl_read_preset_value); */	  
	      
	    }
	  else
	    if(dx)
	      printf("poll_event: tol check ENABLED, NOT Timed Out with gbl_new_ticks=%d, gbl_ticks0=%d and preset_value %d\n",
		     gbl_new_ticks,gbl_ticks0,gbl_read_preset_value);
	}
      else
	{  /* tolerance checking is disabled; set a very long timeout  */
	  if (gbl_new_ticks > 10 * gbl_ticks0)
	    {
	      gbl_timeout_flag = TRUE;
	      sprintf(display_string,"polling timed out with ticks0=%d, new_ticks=%d, preset value=%d ",
			gbl_ticks0,gbl_new_ticks,gbl_read_preset_value);
	      
	      /* printf("poll_event: tol check DISABLED, Timed Out with  gbl_ticks0=%d, gbl_new_ticks=%d, preset_value=%d \n",
		 gbl_ticks0,gbl_new_ticks,gbl_read_preset_value); */
	    }
	}
    }
  else
    {
      /* we are calibrating */
      /*	sprintf(display_string,"calibrating ... still waiting for counts, preset_value=%d, new_ticks=%d, total_rate=%d ", 
		gbl_read_preset_value,gbl_new_ticks,gbl_total_rate); */
      sprintf(display_string,"calibrating ... still waiting for counts, restarted calibration %d times ",gbl_norm_timeout_cntr); 
    }
#ifdef OS_VXWORKS
  /* musrVmeioOff(1); /* TEMP */
#endif
  
  
  /* calculate time actually in polling loop (for display) */
  gbl_poll_time+= ss_millitime() - gbl_start_poll;
  
  if(gbl_timeout_flag) return 1; /* call imusr_acq with timeout */
  
  return 0; /* keep polling */
}


  
/*-- Interrupt configuration for trigger event ---------------------*/
INT interrupt_configure(INT cmd, INT source[], PTYPE adr)
{  
  return SUCCESS;
}
/*-- Event readout -------------------------------------------------*/

INT trigger_read(char *pevent, INT off)
{
  return (0);
}
/*-- Scaler event --------------------------------------------------*/

INT scaler_read(char *pevent, INT off)
{
    return (0);
}

/*-- Start a sweep cycle ---------------------------------------------------*/
INT start_sweep_cycle(void)
{
  /* called at by begin_of_run  and by sweep_cycle_end


    Start a whole new sweep cycle
     (comprising preset and toggle cycles)
     at this sweep value.
          
  */

  INT i;
  
  /* cf starts at 10. */
  
  if(di)printf("start_sweep_cycle: starting\n");
  gbl_in_sweep = TRUE;
  /* info_odb will write current sweep etc.  to output area */
  
  if(dx)printf("start_sweep_cycle: --- Sweep %d : starting on sweep increment %d value %f  --- \n",
	 gbl_sweep_counter,gbl_sweep_inc,gbl_sweep_value);
  
  
  /* clear sum scalers */
  for(i=0; i < MAX_SCALER_SUMS; i++)
    gbl_scaler_sums[i]=0;
  
  if(gbl_num_inner_toggles)
  { /* check inner toggle state is in phase */
    if (gbl_inner_toggle_state != tog_off)
    {
      cm_msg(MERROR,"start_sweep_cycle","I-MUSR inner toggle phase error");
      return (FE_ERR_HW);
    }
  }
  else if(gbl_num_outer_toggles)
  { /* check outer toggle state is in phase */
    if (gbl_inner_toggle_state != tog_off)
    {
      cm_msg(MERROR,"start_sweep_cycle","I-MUSR outer toggle phase error");
      return (FE_ERR_HW);
    }
  }

  
  gbl_inner_toggle_counter=0; /* VMS toggle2 */
  gbl_outer_toggle_counter=0;    

  status = outer_toggle_cycle();
  if (status != SUCCESS)
  {
    cm_msg(MERROR,"start_sweep_cycle","Error return from outer_toggle_cycle (%d). May need to stop run and restart",status);
    return(status);
  }

  return(status);
}

/*----------------------------------------------------------------------------*/
void keep_on_toggling()
{
  /* called by imusr_acq if is.input.keep_toggling_out_of_tol flag is TRUE

     The idea is to
      continue round preset/toggle cycle when beam is out-of-tolerance or paused
      until we get to the right place in the cycle to try again for tolerance
  */
  
  gbl_preset_counter++;
  if (gbl_preset_counter < is.input.num_presets)
    {
      gbl_in_preset = FALSE; /* go to 30 (causes preset_cycle to run) */
      return;
    }
  
  /* increase gbl_inner_toggle_counter so gbl_outer_toggle will run as well */
  gbl_inner_toggle_counter++ ;  /* cf toggle2 */ 
  if(gbl_inner_toggle_counter < 2 * gbl_num_inner_toggles) /* not true for toggle = NONE */
    { 
      gbl_in_inner_toggle = FALSE; /* go to 20 (causes inner_toggle_cycle to run) */
      return;
    }
  
  gbl_in_outer_toggle = FALSE; /* causes outer_toggle_cycle to run */
  return;
  
}

/*-- Outer Toggle cycle --------------------------------------------------*/

INT outer_toggle_cycle()
{
  /* called by frontend_loop if gbl_in_outer_toggle is FALSE
       and by start_sweep_cycle */


  if(dii)
    printf("outer_toggle_cycle is starting\n");
  gbl_in_outer_toggle = TRUE;
  gbl_in_inner_toggle = TRUE;/*  prevent restarting of other cycles while  */
  gbl_in_preset = TRUE;      /*  outer_toggle_cycle is running */

  /* Toggle outer_toggle_state for all toggle states except NONE */
  if( gbl_num_outer_toggles > 0)
  {  /* HARD or SOFT or REF outer toggle have gbl_num_outer_toggles > 0 */
    if(gbl_outer_toggle_state == tog_off)
      gbl_outer_toggle_state=tog_on;
    else if(gbl_outer_toggle_state == tog_on)
      gbl_outer_toggle_state=tog_off;
    else
    {
      cm_msg(MERROR,"outer_toggle_cycle","invalid toggle state %d",gbl_outer_toggle_state);
      return(-1);
    }
    if(gbl_outer_toggle_state == tog_off)
      if(dii)printf("outer_toggle_cycle: toggled gbl_outer_toggle_state to ON\n");
    else
      if(dii)printf("outer_toggle_cycle: toggled gbl_outer_toggle_state to OFF\n"); 
  }
  /* outer toggle = NONE */
  else
    gbl_outer_toggle_state = tog_off;  /* set NONE toggle state */

  gbl_actual_SD_value = gbl_sweep_value;


  /* SOFT toggle */
  if(strcmp("SOFT",gbl_outer_toggle_type) == 0)
  {
    if(gbl_outer_toggle_state == tog_off)
    {
      gbl_actual_SD_value = gbl_actual_SD_value  + is.input.outer_toggle_value; 
      if(dii)printf("outer_toggle_cycle: Software toggle ON  -> set SD to %f\n",gbl_actual_SD_value);
      status =set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */ 
      if(status != SUCCESS)
	{ /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	  cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	  return status;
	}
 
    }
    else
    {
      gbl_actual_SD_value = gbl_actual_SD_value  - is.input.outer_toggle_value; 
      status = set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */
      if(status != SUCCESS)
	{ /* set_sweep_device retries before giving up  & setting flag for mdarc to stop the run*/
	  cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	  return status;
	}
      if(dii)printf("outer_toggle_cycle: Software toggle OFF  -> set SD to %f\n",gbl_actual_SD_value);
    }
  }
  /* REF toggle */
  else if (strcmp("REF",gbl_outer_toggle_type) == 0)
  {
    if(gbl_outer_toggle_state == tog_off)
    { 
      status =set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */
      if(status != SUCCESS)
	{ /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	  cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	  return status;
	}
 
      if(dii)printf("outer_toggle_cycle: REF toggle ON  -> set SD to %f\n",gbl_actual_SD_value);
    }
    else
    {
      gbl_actual_SD_value = is.input.outer_toggle_value;
      status =set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */
      if(status != SUCCESS)
	{ /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	  cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	  return status;
	}
      if(dii)printf("outer_toggle_cycle: REF toggle OFF  -> set SD to %f\n",gbl_actual_SD_value);
    }
  } 

  if(dii)printf("outer_toggle_cycle: toggle cycle %d, counter %d toggle state %d;  set SD to %f \n",
	       gbl_outer_toggle_counter/2, gbl_outer_toggle_counter,gbl_outer_toggle_state, gbl_actual_SD_value);

#ifdef OS_VXWORKS
  if(gbl_outer_toggle_state == tog_off)
    musrVmeioOff(IO_MOD_OUTER);
  else
    musrVmeioOn(IO_MOD_OUTER);
#endif

  
  /* reset some counters */
  gbl_inner_toggle_counter=0;
  gbl_preset_counter = 0;
  if( gbl_num_outer_toggles > 0) /* HARD or SOFT or REF outer toggle have gbl_num_outer_toggles > 0 */
    ss_sleep(is.input.outer_toggle_settling_time__ms_); /* have just toggled */

  gbl_modified_sweep_value = gbl_actual_SD_value; /* remember the value set by
				                     outer_toggle  */
  if(dii)printf("outer_toggle_cycle: calling inner_toggle_cycle\n");
  status = inner_toggle_cycle();
  if (status != SUCCESS)
  {
    /* Run should be stopped */
    cm_msg(MERROR,"outer_toggle_cycle","Error return from inner_toggle_cycle (%d). Stop run and restart",status);
    return(status);
  }

  
  if(di)printf("outer_toggle_cycle: returning (%d)\n",status);

  return(status);

}



/*-- Inner Toggle cycle --------------------------------------------------*/

INT inner_toggle_cycle()
{
  /* called by frontend_loop if gbl_in_inner_toggle is FALSE
        and by outer_toggle_cycle    */

/* Starts at 20. */
  if(di)printf("inner_toggle_cycle is starting with modified_sweep_value %f\n",
	       gbl_modified_sweep_value);
  
  gbl_in_inner_toggle = TRUE;
  gbl_in_preset = TRUE; /* prevent restarting of preset_cycle while inner_toggle_cycle is running */

  /* Toggle inner_toggle_state for all toggle states except NONE */
  if( gbl_num_inner_toggles > 0)
  {  /* HARD or SOFT or REF inner toggle have gbl_num_inner_toggles > 0 */
    if(gbl_inner_toggle_state == tog_off)
      gbl_inner_toggle_state=tog_on;
    else if(gbl_inner_toggle_state == tog_on)
      gbl_inner_toggle_state=tog_off;
    else
    {
      cm_msg(MERROR,"inner_toggle_cycle","invalid toggle state %d",gbl_inner_toggle_state);
      return(-1);
    }
    if(gbl_inner_toggle_state == tog_off)
      if(di)printf("inner_toggle_cycle: toggled gbl_inner_toggle_state to ON\n");
    else
      if(di)printf("inner_toggle_cycle: toggled gbl_inner_toggle_state to OFF\n"); 
  }
  /* inner toggle = NONE */
  else
    gbl_inner_toggle_state = tog_off;  /* set NONE toggle state */

  gbl_actual_SD_value = gbl_modified_sweep_value; /* sweep value may be 
			        	modified by outer_toggle */


  /* SOFT toggle */
  if(strcmp("SOFT",gbl_inner_toggle_type) == 0)
  {
    if(gbl_inner_toggle_state == tog_off)
    {
      gbl_actual_SD_value = gbl_actual_SD_value  + is.input.inner_toggle_value; 
      if(di)printf("inner_toggle_cycle: Software toggle ON  -> set SD to %f\n",gbl_actual_SD_value);
      status =set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */ 
      if(status != SUCCESS)
	{ /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	  cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	  return status;
	}
    }
    else
    {
      gbl_actual_SD_value = gbl_actual_SD_value  - is.input.inner_toggle_value; 
      status =set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */
      if(status != SUCCESS)
	{ /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	  cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	  return status;
	}
      if(di)printf("inner_toggle_cycle: Software toggle OFF  -> set SD to %f\n",gbl_actual_SD_value);
    }
  }
  /* REF toggle */
  else if (strcmp("REF",gbl_inner_toggle_type) == 0)
  {
    if(gbl_inner_toggle_state == tog_off)
    { 
      status =set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */
      if(status != SUCCESS)
	{ /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	  cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	  return status;
	}
      if(di)printf("inner_toggle_cycle: REF toggle ON  -> set SD to %f\n",gbl_actual_SD_value);
    }
    else
      {
	gbl_actual_SD_value = is.input.inner_toggle_value;
	status=set_sweep_dev (is.input.sweep_device, gbl_actual_SD_value); /* do not wait step_settletime */
	if(status != SUCCESS)
	  { /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	    cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	    return status;
	  }	
	if(di)printf("inner_toggle_cycle: REF toggle OFF  -> set SD to %f\n",gbl_actual_SD_value);
      }
  } 
  
  if(di)printf("inner_toggle_cycle: toggle cycle %d, counter %d toggle state %d;  set SD to %f \n",
	       gbl_inner_toggle_counter/2, gbl_inner_toggle_counter,gbl_inner_toggle_state, gbl_actual_SD_value);

  /* this is set in preset_cycle 
#ifdef OS_VXWORKS
  if(gbl_inner_toggle_state == tog_off)
    musrVmeioOff(IO_MOD_INNER);
  else
    musrVmeioOn(IO_MOD_INNER);
#endif
  */
  gbl_preset_counter = 0;
  ss_sleep(is.input.inner_toggle_settling_time__ms_); /* have just toggled */

  if(di)printf("inner_toggle_cycle: calling preset_cycle\n");
  status = preset_cycle();
  if (status != SUCCESS)
  {
    /* Run should be stopped */
    cm_msg(MERROR,"inner_toggle_cycle","Error return from preset_cycle (%d). May need to stop run and restart",status);
    return(status);
  }

  if(di)printf("inner_toggle_cycle: returning (%d)\n",status);

  return(status);
    
}




/*-- Preset cycle --------------------------------------------------*/

INT preset_cycle()
{
  /* called by frontend_loop if gbl_in_preset is FALSE
            and by inner_toggle_cycle */
  
  INT transition;
  DWORD preset_value; /* temp */
  int a=0;
  

/* Starts at 30. */
  
  
  if(di)printf("preset_cycle: starting\n");
  
  gbl_in_preset = TRUE; /* prevent routine being called again */
  
  
  
  /*
    Check for normalizing
  */
 
  if(gbl_ticks0 == 0)
    {   /* gbl_ticke=0 here  ONLY at BOR or when normalization has timed out (gbl_norm_timeout=TRUE) */
      if (gbl_BOR_flag) /* the first time through */
	{ 
	  if(is.input.start_run_in_paused_state)
	    {
	      printf("preset_cycle: ****   At BOR .... Starting in PAUSED state... NOT Raising gbl_hold_flag  ***** \n");
	      BOR_paused=TRUE;
	    }
	  else
	    {
	      gbl_hold_flag = FALSE;
	      printf("preset_cycle: ****   First time through at BOR .... Raising gbl_hold_flag  ***** \n");
	    }
	  
	  /* Do we want to normalize at BOR? */
	  if(!is.input.normalize_at_begin_run)
	    { /* no; use previous normalization */
	      printf("preset_cycle: NOT normalizing at BOR; using previous values of gbl_ticks0=%d, gbl_TotR0=%d and norm time=%d\n",
		     os.normalization_clock_ticks, os.normalization_total_rate, os.normalization_time__ms_);
	      gbl_ticks0= os.normalization_clock_ticks;
	      gbl_TotR0= os.normalization_total_rate;
	      gbl_norm_time=os.normalization_time__ms_ ;
	    }	     
	  gbl_BOR_flag = FALSE; /* clear the flag */
	} /* end of gbl_BOR_flag */
      else
	{
	  if(gbl_norm_timeout)
	    {
	      if(!v680_Display)
		printf("preset_cycle: calibrating ... still waiting for counts, restarted calibration %d times\n",gbl_norm_timeout_cntr); 
	      gbl_norm_timeout_cntr++;
	      if(dx)printf("preset_cycle: Restarting normalization due to previous timeout\n");
	      gbl_TotR0 = 0;
	      gbl_norm_timeout=FALSE;
	    }
	  else
	    {
	      cm_msg(MERROR,"preset_cycle","ERROR: expect to be at BOR i.e. gbl_ticks0=0,flag=TRUE (gbl_ticks0=%d,gbl_BOR_flag=%d)\n",
		     gbl_ticks0,gbl_BOR_flag);
	      cm_msg(MERROR,"preset_cycle","gbl_ticks0 i.e. clock scaler may not be counting\n");
	      cm_msg(MERROR,"preset_cycle","  NOT RAISING gbl_hold_flag ... RUN CANNOT PROCEED ... stop run and restart\n");
	      sprintf(display_string,"Error; gbl_ticks0=0 but not BOR; check clock is counting; restart run ");
	      return FE_ERR_ODB;
	    }
	}
    }
  
  /* check also for renormalizing */
  
if(is.input.renormalize)
  {  /* normalizing */
    if(dx)printf("preset_cycle: Normalizing...\n");
    sprintf(display_string,"Normalizing...  ");
    gbl_ticks0 = 0;
    gbl_TotR0 = 0;
    is.input.renormalize = FALSE;
    size = sizeof(is.input.renormalize);
    status=db_set_value (hDB, hIS, "input/renormalize", &is.input.renormalize, size, 1, TID_BOOL);
    if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"preset_cycle","could not clear flag \"...input/renormalize\" (%d) ",status);
	return FE_ERR_ODB;
      }
  }
  
  
  
  
#ifdef OS_VXWORKS
  /* Set toggle IOREG MOD bits */
  if (gbl_inner_toggle_state == tog_off)
    musrVmeioOff(IO_MOD_INNER);
  else if (gbl_inner_toggle_state == tog_on)
    musrVmeioOn(IO_MOD_INNER);

  /* Clear scaler before each preset cycle */
  sis3803_all_clear(SIS_base_adr);

#endif

  
  /*printf("preset_cycle: Preset_counter=%d, cleared scalers, ..... waiting
  for data acquistion to end\n",gbl_preset_counter); */
  
  
  /*printf("\r SC:%4.4d  T:%4.4d TC:%4.4d TS:%2.2d PC:%4.4d DP=%4.4d SV:%7f SD:%7f ",
    gbl_sweep_counter,gbl_inner_toggle_counter/2,gbl_inner_toggle_counter,gbl_inner_toggle_state,
    gbl_preset_counter,gbl_data_point_counter,gbl_sweep_value,gbl_actual_SD_value);
  */



  if( v680_Display == 0)
  {
    if (gbl_active_flag)
      printf("  ** Swp:%2.2d SInc:%2.2d PFlg:%1.1d ouTC:%2.2d ouTSt:%1.1d inTC:%2.2d inTSt:%1.1d PC:%2.2d DPnt=%4.4d SVal:%7.3f MSVal:%7.3f SD:%7.3f **\n",
	     gbl_sweep_counter,gbl_sweep_inc,gbl_pflag,
	     gbl_outer_toggle_counter,gbl_outer_toggle_state,
	     gbl_inner_toggle_counter,gbl_inner_toggle_state,
	     gbl_preset_counter,gbl_data_point_counter,
	     gbl_sweep_value,gbl_modified_sweep_value,gbl_actual_SD_value);
    else
      printf("     Swp:%2.2d SInc:%2.2d PFlg:%1.1d ouTC:%2.2d ouTSt:%1.1d inTC:%2.2d inTSt:%1.1d PC:%2.2d DPnt=%4.4d SVal:%7.3f MSVal:%7.3f SD:%7.3f\n",       
	     gbl_sweep_counter,gbl_sweep_inc,gbl_pflag,
	     gbl_outer_toggle_counter,gbl_outer_toggle_state,
	     gbl_inner_toggle_counter,gbl_inner_toggle_state,
	     gbl_preset_counter,gbl_data_point_counter,
	     gbl_sweep_value,gbl_modified_sweep_value,gbl_actual_SD_value);
    
  }
  
  

  /* Signal start of acquisition */
  
  gbl_start_acq = ss_millitime(); /* remember the time when we start acquiring   */
    
#ifdef OS_VXWORKS
  /* Reset the Preset Counter to preset value */
  /* Now done by Hardware START pulse from IO board
     status = gglResetCounter ( ggl_base); */
  if(di)printf("preset_cycle: resetting GGL Preset Counter to %d\n",status);
  preset_value = gglReadbackCounter(ggl_base);
  /* Trigger start of acquistion  (a pulse)   */
  status = musrVmeioPulse(IO_START_ACQ);
  if(status != UVIO_SUCCESS)
    cm_msg(MERROR,"preset_cycle","bad status (%d) after pulsing IO_START_ACQ (channel %d)",status,IO_START_ACQ);
#endif
  if(di) 
    printf("preset_cycle: started acquiring at gbl_start_acq = %d; preset_value=%d\n",gbl_start_acq,preset_value);
   
  /* old version sleeps for some time while counts are accumulating 
    ss_sleep(500);  */
  
  gbl_poll_counter = 0; /* initialize counter for no. times polling loop called
			 */  
  waiteqp[MUSR]= TRUE;
  
  if(di)printf("preset_cycle: returning after setting waiteqp[MUSR]=TRUE\n");
  return(SUCCESS);
  
}



/*-- Event readout -------------------------------------------------*/

INT imusr_acq(char *pevent, INT off)
{
  /* Trigger Routine : called when polling_loop returns TRUE */   

  float ftmp;
  float tol_level;
  int i,j;
  static INT my_inner_toggle_state, my_outer_toggle_state;
  static INT my_inner_toggle_counter, my_preset_counter;
  DWORD start,stop,time;
  
  
  /*TEMP */
  float a,b,c,f;
#ifdef OS_VXWORKS
  /*  status = musrVmeioOff(0);  /* temp */
#endif
  waiteqp[MUSR]=FALSE;
  
/* Turn off START_ACQ bit as we have now stopped acquiring */
#ifdef OS_VXWORKS
  status = musrVmeioOff(IO_START_ACQ);
  /* read the total rate as a check */
/*  sis3803_counter_read(SIS_base_adr, SC_TOTAL_RATE, &gbl_total_rate);*/
#endif

  if(gbl_new_ticks == 0)
    {
      cm_msg(MERROR,"imusr_acq","gbl_new_ticks=0, may indicate a problem!!");
    }

  if(gbl_ticks0==0)
  { /* calibrating or normalizing .. all need to be written to odb (not in this routine) */
    gbl_norm_time = ss_millitime() - gbl_start_acq; /* time to acquire the data */
    gbl_ticks0 = gbl_new_ticks;
    gbl_TotR0 = gbl_total_rate; /* Total Rate during normalization, used for constant time mode 
				       where time is constant & rate varies */
    sprintf(display_string,"Calibrated...  gbl_poll_counter=%d, normalization time =%d and clock (gbl_ticks0) = %d; total rate = %d \n",
	    gbl_poll_counter,gbl_norm_time,gbl_ticks0,gbl_total_rate);

    if(!v680_Display)
      printf("imusr_acq: Calibrated...  gbl_poll_counter=%d, normalization time =%d and clock (gbl_ticks0) = %d; total rate = %d \n",
	     gbl_poll_counter,gbl_norm_time,gbl_ticks0,gbl_total_rate);
    gbl_norm_timeout_cntr=0; /* clear normalization timeout counter */
  }
  
  time =  ss_millitime();
  gbl_acq_cycle_time =   ss_millitime() - gbl_start_acq; /* time to get this
							    data (or to timeout) */
/* store a snapshot */
  if(v680_Display)
  {
    sprintf(display_last,"%-3.3d  %-5.5d  %-5.5d  %-5.5d ",
	    gbl_poll_counter,gbl_new_ticks,gbl_read_preset_value,gbl_total_rate);
  }
  os.last_clock_ticks=gbl_new_ticks; /* store for info_odb */
/*  printf("imusr_acq: got timeout flag:%1.1d gbl_poll_counter: %d time now: %d, actual polling time: %d; total acq time: %d \n",
    gbl_timeout_flag,gbl_poll_counter,time,gbl_poll_time,gbl_acq_cycle_time);
*/
  if(gbl_timeout_flag)
  {
    os.timeout_counter++;
    
#ifdef OS_VXWORKS
    /* TIMEOUT  (a pulse)   */
    status = musrVmeioPulse(IO_TIMEOUT);
    if(status != UVIO_SUCCESS)
      cm_msg(MERROR,"imusr_acq","bad status (%d) after pulsing IO_TIMEOUT (channel %d)",status,IO_TIMEOUT);
#endif
  }
  
  /* write out hotlinked values used in this routine (debug) */
  if(dii)
    {
      printf("imusr_acq: hot linked values :\n");
      printf("   tolerance (%) %f  , out-of-tolerance delay (s) %f\n",
	     is.input.tolerance____,is.input.out_of_tolerance_delay__s_);
    }
  
  
  /* out-of-tolerance action depends on flag  */
  if(is.input.keep_toggling_out_of_tol)
  {
    
    /* if paused, gbl_hold_flag is set
       - keep cycling as if out of tolerance
    */
    if (gbl_pflag )
    {
      if (! gbl_hold_flag) /* we want to come back into tolerance */
      {
	if(dii)
	{
	  printf("imusr_acq: MY inner toggle state =%d, outer toggle state =%d, inner toggle counter =%d,  my preset counter\n",
		 my_inner_toggle_state, my_outer_toggle_state,
		 my_inner_toggle_counter, my_preset_counter);
	  printf("imusr_acq: GBL inner toggle state =%d, outer toggle state =%d, inner toggle counter =%d,  my preset counter\n",
		 gbl_inner_toggle_state,  gbl_outer_toggle_state,
		 gbl_inner_toggle_counter ,gbl_preset_counter);
	}
	
	if (my_inner_toggle_state ==   gbl_inner_toggle_state &&
	    my_outer_toggle_state ==   gbl_outer_toggle_state &&
	    my_inner_toggle_counter == gbl_inner_toggle_counter &&
	    my_preset_counter == gbl_preset_counter)
	{/* we have cycled round again - try getting back into tolerance */
	  gbl_pflag = FALSE;
	  printf("imusr_acq: (keep toggling)  exiting from wait loop at preset_counter=%d, toggle state: inner=%d outer=%d\n",
		 gbl_preset_counter,gbl_inner_toggle_state,gbl_outer_toggle_state);
	}
      }
      
      /* keep going round toggle cycle */
      if(dii) printf("imusr_acq: calling keep_on_toggling\n");
      keep_on_toggling();
      gbl_outol_counter++;
      return 0;
    }
  }
  
  
  /* if paused, gbl_hold_flag is set
     - keep cycling as if out of tolerance
  */
  if ( gbl_hold_flag)
  {
    sprintf(display_string,"Run is on hold ");
    if(dii)printf("imusr_acq: gbl_hold_flag is ON\n");
/* check whether run is active - if it is we should have just started a run */     
/* make sure gbl_active_flag is FALSE etc */
    if (gbl_active_flag)
    {
      gbl_active_flag = FALSE;
#ifdef OS_VXWORKS
      status = musrVmeioOff(IO_ACQ_ACTIVE);
#endif
    }
    
    /*
      JUST PAUSED
    */
    if (gbl_pause_midcycle_flag) /* set by pause_run - we have just paused */
    {
      /* pause_run initialized gbl_pause_start - but if we were in the
	 middle of taking a data point when the run was paused, we need to
	 add gbl_acq_cycle_time and re-initialize pause_start
      */
      gbl_pause_midcycle_flag=FALSE;
      gbl_total_pause_time += gbl_acq_cycle_time; /* add last cycle time */
      gbl_pause_start = ss_millitime(); /* reinitialize pause start time */
      
     
     
      /* out-of-tolerance action depends on flag  */
      if(is.input.keep_toggling_out_of_tol)
      {
	sprintf(display_string,"Just paused, continuing to toggle "); 
	gbl_pflag = TRUE; /* set a flag */
	my_preset_counter = gbl_preset_counter; /* remember where we are */
	my_inner_toggle_state = gbl_inner_toggle_state;
	my_outer_toggle_state = gbl_outer_toggle_state;
	my_inner_toggle_counter = gbl_inner_toggle_counter;
	if(dii)
	{
	    printf("imusr_acq: (keep toggling) entering wait loop at preset %d and toggle state: inner %d outer %d\n",
		   gbl_preset_counter,gbl_inner_toggle_state,gbl_outer_toggle_state);
	    printf("imusr_acq: calling keep_on_toggling\n");
	}
	keep_on_toggling();
	gbl_outol_counter++;
	return 0;
      }
    }
    if (is.input.keep_toggling_out_of_tol)
    { /* keep toggling when paused */
      printf("imusr_acq:(keep toggling)  NOT just paused - should not get this message\n");
      gbl_outol_counter++;
      return 0;
    }
    else
    { /* repeat this preset value until run is resumed */
      gbl_in_preset = FALSE;
      sprintf(display_string,"Paused, repeating this preset value ");
      if(dii)printf("imusr_acq: Paused, repeating preset -> returning with gbl_in_preset FALSE as gbl_hold_flag=TRUE\n");
      gbl_outol_counter++;
      return(0);
    }
  } /* end of gbl_hold_flag set */
  
  
  /* Run is not paused
     
     CHECK TOLERANCE
     
  */
  
  if(is.input.tolerance_check)
  {
    
    /* Tolerance checking is enabled
       check against previous value
    */
    if(is.input.constant_time)
      {
	/* constant time mode; clock ticks should be identical to gbl_ticks0;
	   check tolerance on total Rate */
	f= fabs( (float)gbl_total_rate - (float)gbl_TotR0) ;
	tol_level = gbl_tol * (float) gbl_TotR0 - f;
	if(dx)printf("imusr_acq: checking tolerance (constT), f=%f  gbl_tol=%f, gbl_timeout=%1.1d, gbl_poll_counter=%d, gbl_ticks0=%d, gbl_new_ticks=%d, gbl_TotR0=%d, totalR=%d \n",
		     f,gbl_tol,gbl_timeout_flag,gbl_poll_counter, gbl_ticks0,gbl_new_ticks,
		     gbl_TotR0, gbl_total_rate);
      }
    else
      {
	/* constant rate mode; check on clock ticks */
	/* Counts in clock scaler read from last polling loop =  gbl_new_ticks */
	f= fabs( (float)gbl_new_ticks - (float)gbl_ticks0) ;
	tol_level = gbl_tol * (float) gbl_ticks0 - f;
	if(dx)printf("imusr_acq: checking tolerance (constR), f=%f  gbl_tol=%f, gbl_timeout=%1.1d, gbl_poll_counter=%d, gbl_ticks0=%d, gbl_new_ticks=%d, gbl_TotR0=%d, totalR=%d \n",
		     f,gbl_tol,gbl_timeout_flag,gbl_poll_counter, gbl_ticks0,gbl_new_ticks,
		     gbl_TotR0, gbl_total_rate);
      }

    
    
    /* TEMP */
    /*(if(dii)printf("imusr_acq: run is not paused, tol_level=%f\n",tol_level); */    
    if(tol_level <= 0.0)
    {
      /*
	BEAM IS OUT OF TOLERANCE
      */
      
      if(dx)
      {
	printf("\n");
	printf("imusr_acq: ****  Beam is out of tol, gbl_ticks0=%d, gbl_new_ticks=%d, 
 gbl_TotR0=%d, totalR=%d, gbl_tol_level = %f **** \n",
	       gbl_ticks0,gbl_new_ticks, gbl_TotR0, gbl_total_rate, tol_level);
      }
      /*      
	sprintf(display_string,"Beam is out-of-tolerance, ticks0=%d, new_ticks=%d, total_r=%d ",
		gbl_ticks0,gbl_new_ticks,gbl_total_rate);
      */
      if ( ! gbl_out_of_tolerance_flag)
      {
	/* JUST GONE OUT OF TOLERANCE */ 
	gbl_out_of_tolerance_flag = TRUE;
	gbl_pause_start = ss_millitime();
	if(dx) cm_msg(MINFO,"imusr_acq","   Just gone out of tolerance at time %d... entering wait loop",gbl_pause_start); 
	sprintf(display_string,"Beam just gone  out-of-tolerance, ticks0=%d, new_ticks=%d, gbl_TotR0=%d, total_rate =%d  ",
		gbl_ticks0,gbl_new_ticks,gbl_TotR0, gbl_total_rate);
	
	gbl_active_flag = FALSE;
#ifdef OS_VXWORKS
	status = musrVmeioOff(IO_ACQ_ACTIVE);
#endif
	
	if (is.input.keep_toggling_out_of_tol)
	{ /* keep toggling when out of tol  */
	  gbl_pflag = TRUE; /* set a flag */
	  my_preset_counter = gbl_preset_counter; /* remember where we are */
	  my_inner_toggle_state = gbl_inner_toggle_state; /* and what toggle
							     state */
	  if(dx)printf("imusr_acq: out of tol, calling keep_on_toggling\n");
	  keep_on_toggling();
	  gbl_outol_counter++;
	  return 0; 
	}
      } /* end of just gone out of tolerance */
      
      /* repeat this preset when out of tol */
      if(dx)printf("imusr_acq: returning with gbl_in_preset FALSE as beam out of tol \n");     
      gbl_in_preset = FALSE; /* c.f. go to 30 */
      gbl_outol_counter++;
      return(0);
      
      
    } /* end of beam out of tolerance */
    
    
    /*
      BEAM IS IN TOLERANCE
    */
    if(gbl_timeout_flag)
    {
      cm_msg(MERROR,"imusr_acq","Beam in tolerance but gbl_timeout_flag is raised! ");
      printf("imusr_acq: +++++   Beam in tolerance but polling loop timed out; this shouldn't happen ++++\n");
    } 
    
    if(gbl_out_of_tolerance_flag)
    {
      /*
	JUST CAME BACK INTO TOLERANCE
      */
      if(dx)printf("imusr_acq: Beam came back into tolerance... entering wait loop\n");
      sprintf(display_string,"Beam just came back into tolerance, ticks0=%d, new_ticks=%d, totalR0=%d,  total_r=%d ",
	      gbl_ticks0,gbl_new_ticks,gbl_TotR0, gbl_total_rate);
      
      /* cm_msg(MINFO,"imusr_acq","Beam came back into tolerance... entering wait loop"); */
      gbl_out_of_tolerance_flag = FALSE;
      /* Wait out-of-tolerance delay (s) */
      ss_sleep (is.input.out_of_tolerance_delay__s_ * 1000);
      
      /* calculate how long we were out of tolerance and add to the total paused time */
      stop =  (ss_millitime() - gbl_pause_start);
      if(dx)printf("imusr_acq: total time out of tolerance/paused = %d ms\n",stop);
      gbl_total_pause_time += (ss_millitime() - gbl_pause_start);
      gbl_pause_start = 0; /* indicate we are back in tolerance  */
      
      gbl_active_flag = TRUE;
#ifdef OS_VXWORKS
      status = musrVmeioOn(IO_ACQ_ACTIVE);
#endif
      
      if(dx)printf("imusr_acq: returning with gbl_in_preset FALSE as just back in tol \n");     
      
      gbl_in_preset = FALSE; /* go to 30 */
      gbl_outol_counter++;
      return(0);
    } 
    
    
    /*
      TOLERANCE WAS  OK LAST TIME AND IS STILL OK
    */
    if(!gbl_active_flag)
    {  /* the run may have been paused, and is now unpaused */
      if(dx)printf("imusr_acq: skipping this preset as gbl_active_flag was FALSE (maybe continuing the run)\n");
      /* calculate how long run was paused  and add to the total paused time */
      stop =  (ss_millitime() - gbl_pause_start);
      if(dx)printf("imusr_acq: total time paused (while in tolerance) = %d ms\n",stop);
      gbl_total_pause_time += (ss_millitime() - gbl_pause_start);
      gbl_pause_start = 0; /* indicate we are not paused */
      
      gbl_active_flag=TRUE;
#ifdef OS_VXWORKS
      status = musrVmeioOn(IO_ACQ_ACTIVE);
#endif
      gbl_in_preset = FALSE; /* go to 30 */
      gbl_outol_counter++;
      return(0);
    }    
    
/* 
      sprintf(display_string,"Beam is in-tolerance, ticks0=%d, new_ticks=%d, total_r=%d ",
	      gbl_ticks0,gbl_new_ticks,gbl_total_rate);
*/  
  }  /* end of tolerance checking */
  else
  {
    if(dx)printf("imusr_acq: no tolerance checking\n");
    /*
      NO TOLERANCE CHECKING
    */  
    if(!gbl_active_flag)
    {     
      gbl_active_flag = TRUE;
#ifdef OS_VXWORKS
      status = musrVmeioOn(IO_ACQ_ACTIVE);
#endif
    }
    /* but what about a timeout with no tol checking? */
    
  } 
  
  gbl_intol_counter++;

/* 40. */
/*
  Read & clear scalers, sum the data
*/
  if(dii)printf("imusr_acq: calling scaler_sum to read/clear scalers\n");
  status =  scaler_sum();
  
  gbl_preset_counter++;
  if (gbl_preset_counter < is.input.num_presets)
  {
    gbl_in_preset = FALSE; /* go to 30 (causes preset_cycle to run) */
    return(0);
  }
  
  gbl_inner_toggle_counter++ ;  /* cf toggle2 */
  if(gbl_inner_toggle_counter < 2 * gbl_num_inner_toggles) /* not true for toggle = NONE */
  { 
    gbl_in_inner_toggle = FALSE; /* go to 20 (causes inner_toggle_cycle to run) */
    return(0);
  }

  if(dii)printf("inner toggle counter done (inner=%d outer=%d)\n",
	 gbl_inner_toggle_counter,gbl_outer_toggle_counter);


  
  gbl_outer_toggle_counter++ ;
  if(gbl_outer_toggle_counter < 2 * gbl_num_outer_toggles) /* not true for toggle = NONE */
  { 
    gbl_in_outer_toggle = FALSE; /* go to 20 (causes inner_toggle_cycle to run) */
    return(0);
  }
  if(dii) printf("outer toggle counter done (%d)\n",gbl_outer_toggle_counter);
  
  /*
    All toggles cycles are finished
  */ 
  
  /* As a check make sure the previous point got sent out */
  if   (waiteqp[SEND_DATA])
  {
    for(i=0; i<20; i++)
    {
      ss_sleep(50);
      if (!waiteqp[SEND_DATA]) break;
    }
    if (waiteqp[SEND_DATA])
    {
      cm_msg(MERROR,"imusr_acq","Previous data point (%d) didn't get sent out after waiting 1s",gbl_data_point_counter-1);
      cm_msg(MERROR,"imusr_acq","Run is in Idle mode .... PLEASE RESTART RUN with a slower clock");
      return(0); 
    }
    else
      cm_msg(MERROR,"imusr_acq","Had to wait %d ms for previous data point to get sent out",(i*50));
  }
  
  /*
    We have a data point!!
    Store the data so we don't have to wait for data point to be sent out

  - for now we are waiting for each data point to be sent out before continuing 
  */
  gbl_data_array[0] =  (double) gbl_data_point_counter;
  gbl_data_array[1] =  (double) gbl_sweep_value * gbl_conversion_factor ;
  if(dx)
    {
      printf(  " Index     data_array \n");
      for (i=0; i<NUM_EXTRA_DATA;  i++)
	printf("  %2d        %f\n",i,gbl_data_array[i]);
      /*    printf(  " Index     data_array    sums   scaler  \n"); */
    }

  for (i=0; i<gbl_len_data-NUM_EXTRA_DATA;  i++)
    {
      gbl_data_array[i+NUM_EXTRA_DATA] = gbl_scaler_sums[i];
 
  
/*   if(dx)printf("  %2d        %f     %f    %d \n",i,gbl_data_array[i], gbl_scaler_sums[i],scaler_counts[i]);*/
      if(dx)printf("  %2d        %f\n",i+NUM_EXTRA_DATA,gbl_data_array[i+NUM_EXTRA_DATA]);
      /* add this data to the bank */
 
    }
  
  /* printf("imusr_acq: sending point out by setting SEND_DATA true\n"); */
  waiteqp[SEND_DATA] = TRUE;  /* SEND_DATA will send the point out  */
  gbl_wait_time =ss_millitime(); /* TEMP timer */
  gbl_in_sweep = FALSE; /* will cause sweep_cycle_end to run */
  return (0);
}


/*-- Event readout -------------------------------------------------*/

INT info_odb(char *pevent, INT off)
{
  /* IMUSR only
   update output area occasionally as a diagnostic event
  */
  
  DWORD *pdata;
  INT i,len;
  float fsec;
  char tempString[132];
  
  if(di)printf ("info_odb: starting with pevent=%p\n",pevent);


  os.current_sweep_num = gbl_sweep_counter;
  os.current_sweep_setting = gbl_sweep_value;
  os.actual_sweep_setting = gbl_actual_SD_value;
  os.current_data_point = gbl_data_point_counter  ;
  os.current_preset   =  gbl_preset_counter;

  gbl_total_run_time = ss_millitime() - gbl_run_start_time;
  /* total run time (s)  = gbl_total_run_time/1000 */
  fsec = ( (float) gbl_total_run_time) / 1000.0;  /* float &  convert to seconds */
  os.total_run_time__s_ = fsec;
  
  /* Because we are updating and saving gbl_total_active_time here, we must calculate
     total pause time if we are paused or out-of-tolerance */
  if(gbl_pause_start > 0) /* >0 if paused/out-of-tol */
  {
    gbl_total_pause_time  += (ss_millitime() - gbl_pause_start);
    gbl_pause_start = ss_millitime(); /* get a new start time */
  }
  gbl_total_active_time =  gbl_total_run_time - gbl_total_pause_time;
  fsec = ( (float) gbl_total_active_time) / 1000.0;  /* float &  convert to
							seconds */
  os.acquire_active_time__s_ = fsec;
  
  fsec = (float)  gbl_total_pause_time/1000.0;/* float &  convert to
							seconds */
  os.total_paused_time__s_ = fsec;
    
  os.current_outer_toggle = gbl_outer_toggle_counter;
  os.current_inner_toggle = gbl_inner_toggle_counter;
  /* already current : 
     os.total_data_points
     os.total_counts
     os.total_mucounts
     os.timeout_counter
  */
  
  os.normalization_time__ms_ = gbl_norm_time ;
  os.normalization_clock_ticks = gbl_ticks0;
  os.normalization_total_rate = gbl_TotR0;
  os.poll_loop_count = gbl_count;
  os.no__times_polling_loop_called = gbl_poll_counter;
  os.time__ms__in_last_poll_loop = gbl_poll_time;
  os.acq_cycle_time__ms_ = gbl_acq_cycle_time;

  /* these are hot-linked and may have changed */
  os.sweep_start = gbl_sweep_start;
  os.sweep_stop = gbl_sweep_stop;
  os.sweep_step = gbl_sweep_step;
  os.tolerance____ =     is.input.tolerance____;
  os.out_of_tolerance_delay__s_ = is.input.out_of_tolerance_delay__s_;
  os.in_tolerance =  ! ( gbl_out_of_tolerance_flag ) ;
  
  /* printf("Size of os = %d\n",sizeof(os)); */

  len = strlen(display_string);
  if (len > 0 ) /* retain the last message if there's nothing new */
    {  
      strcpy (tempString,display_string);
      trimBlanks(tempString,tempString);
      sprintf(os.status_message,"%s",tempString);
    }

  memcpy(pevent, (char *)&(os.current_sweep_num), sizeof(os));
  pevent += sizeof(os); 

  /* printf("info_odb: returning size of os as %d\n",sizeof(os));*/ 
  return (sizeof(os)); 


}


/* ------------------------------------------------------------------- */
INT scaler_start()
{
  /* Called at begin-of-run to start the scaler */
  DWORD i;
  char str[128];

  if (fs.enabled)
  {
    if (dss)printf("scaler_start: clearing and enabling scaler\n");
#ifdef OS_VXWORKS
    sis3803_all_clear(SIS_base_adr);     /* clear just in case */
    sis3803_all_enable(SIS_base_adr);    /* enable global counting so module acquires counts */ 
    if (vs.flags.test_pulses)    /* test pulse mode */
      sis3803_test_pulse(SIS_base_adr); /* pulse scaler; pulsed again each scaler read */
# endif
      time_last_rates = ss_millitime(); /* get present time for rates calculation */
    /* clear the overflow */
    i=0;
    sprintf(str,"/equipment/scaler/variables/overflow");
    status=db_set_value (hDB, 0, str,
                         &i,sizeof(i), 1, TID_DWORD);
    if (status != DB_SUCCESS)
      cm_msg(MINFO,"scaler_start","could not set %s (%d) ",str,status);

    first_overflow=TRUE; /* flag to avoid repeat messages on overflow */
  }
  else
    if (dss)printf("scaler_start: scaler is disabled\n");
#ifdef OS_VXWORKS
  if(dss)SIS3803_CSR_read(SIS_base_adr);
#endif
}



INT scaler_stop()
{
  /* called at end-of-run and pause */
  if(fs.enabled)
  {
    if(dss)printf("scaler_stop: disable scaler\n");
#ifdef OS_VXWORKS
    sis3803_all_disable(SIS_base_adr);    /* disable global counting */ 
#endif 
  }
}

/*----scaler_init() -----------------------------------------------*/
INT scaler_init () 
{
/* called from frontend_init and begin-of-run

   IMUSR VERSION
 */
  
  int i = 0;
  char str[128];
  INT size,struct_size;
  BOOL b_flag,f_flag;
  
  
/* the initial scaler info is copied into /equipment/scaler/settings
   by musr_conf
*/
  time_last_rates = 0 ; /* initialize time rates were last calculated */
  for (i=0; i< MAX_SCALER; i++)
    {
      scaler_counts[i]=0;
      old_counts[i]=0;
    }

/*
#ifdef TDMUSR
  scaler_clear_rates();
#endif
*/
  sprintf(str,"/equipment/scaler/settings");
  
  /* check the record size is as expected => size */
  status = db_get_record_size(hDB, hSet, 0, &size);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "scaler_init", "error during get_record_size (%d) for %s",status,str);
    return status;
  }
  if(dss)
    printf("scaler_init: scaler/settings record size=%d\n",size);
  
/* get the record for the scaler settings */
  struct_size = sizeof(fs);
  if(dss) printf("scaler_init: scaler/settings structure size=%d\n",struct_size);
  status = db_get_record(hDB, hSet, &fs, &struct_size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "scaler_init", "cannot retrieve %s record (size = %d, structure size =%d)",
           str,size,struct_size);
    return status;
  }
  
  
  /* reset and clear scaler */
#ifdef OS_VXWORKS  
  if(dss)printf("Resetting and clearing scaler\n");
  sis3803_module_reset(SIS_base_adr);
  sis3803_all_clear(SIS_base_adr);
  sis3803_input_mode(SIS_base_adr, 1);            /* mode 1 ONLY */
#endif
  if(dss)printf("Reset and cleared scaler\n");  
  if(fs.enabled)
  {
    
    if(dss)printf ("scaler_init: scaler is  enabled (%d), no. inputs = %d,fast modulation = %d\n",
		   fs.enabled,fs.num_inputs,is.input.fast_modulation); 
    
    /* check the number of scaler channels */
    if ((fs.num_inputs !=  SC_NUM_FIXED_INPUTS) &&
	(fs.num_inputs != (SC_NUM_FIXED_INPUTS+ SC_MAX_FM_INPUTS) )) /* top 6 scaler inputs are reserved for IMUSR;
								      presently no other scaler channels are supported */
    {
      cm_msg(MERROR,"scaler_init","scaler is enabled with illegal number of inputs (%d) for IMUSR\n",fs.num_inputs);
      return (-1);
    }
    if (is.input.fast_modulation)
    {
      if( fs.num_inputs != (SC_NUM_FIXED_INPUTS+ SC_MAX_FM_INPUTS))
      {
	cm_msg(MERROR,"scaler_init","scaler is enabled for FAST MOD with illegal number of inputs (%d)\n",fs.num_inputs);
	return (-1);
      }
    }
    else
    {
      if( fs.num_inputs != SC_NUM_FIXED_INPUTS)
      {
	cm_msg(MERROR,"scaler_init","scaler is enabled with illegal number of inputs (%d) (FAST MOD disabled)\n",fs.num_inputs);
	return (-1);
      }
    }
    
    gbl_fast_mod = FALSE;
    
    /* check we have the fixed channels present, and check for fast mod
       channels */
    
    i= ( fs.bit_pattern & ( SC_FIXED_BITPAT | SC_FMOD_BITPAT)) ;
    printf("scaler_init: bit pattern = 0x%x, i=0x%x\n",fs.bit_pattern,i);

    switch (i)
    {
    case  SC_FMOD_BITPAT:
      if (dss) printf("scaler_init: bit pattern Ox%x has fixed and fmod inputs enabled\n",fs.bit_pattern);
      if (!is.input.fast_modulation)
      {
	cm_msg(MERROR,"scaler_init","Scaler bitpattern (0x%x); FAST MOD should be enabled; i=0x%x\n",
	       fs.bit_pattern,i);
	return(-1);
      }
      gbl_fast_mod = TRUE;
      break;
    case  SC_FIXED_BITPAT :
      if (dss) printf("scaler_init: bit pattern Ox%x has fixed inputs enabled\n",fs.bit_pattern);
      if (is.input.fast_modulation)
      {
	cm_msg(MERROR,"scaler_init","Scaler bitpattern (0x%x); FAST MOD should NOT be enabled; i=0x%x\n",
	       fs.bit_pattern,i);
	return(-1);
      }
      break;
    default: 
      cm_msg(MERROR,"scaler_init","Scaler bitpattern (0x%x) does not include all the MUSR fixed inputs (bitpat=0x%x),i=0x%x\n",
	     fs.bit_pattern,SC_FIXED_BITPAT,i);
      return(-1);      
    } /* switch */
    

    /* selective enable pattern depends on num channels */
#ifdef OS_VXWORKS
    if(dss)printf("scaler_init: enabling scaler\n");
    sis3803_channel_enable(SIS_base_adr,fs.bit_pattern); 
#endif
    
    /* check for test modes and enable if selected */ 
    if(dss)printf("scaler_init: calling scaler_setup_test\n");
    scaler_setup_test();
    
#ifdef OS_VXWORKS  
    SIS3803_CSR_read(SIS_base_adr);
#endif
    /* do not enable scaler yet - done by scaler_start() */
    
    
    /* clear the scaler overflow in odb */
    i=0;
    sprintf(str,"/equipment/scaler/variables/overflow");
    status=db_set_value (hDB, 0, str,
			 &i,sizeof(i), 1, TID_DWORD);
    if (status != DB_SUCCESS)
      cm_msg(MINFO,"scaler_init","could not set %s (%d) ",str,status);
    
    
    
  } /* end of if fs.enabled */
  
  else   /* scaler is disabled */
  {
    cm_msg(MERROR,"scaler_init","Scaler is disabled in odb; cannot run IMUSR\n");
    return(-1);
  }
  
  
  if(dss)printf("scaler_init ending\n");
  
  return SUCCESS;
}



/*----vmeio_init() -----------------------------------------------*/
INT vmeio_init () 
{
  INT i;
  DWORD mode,state;
/* called from frontend_init
   Initialize vmeio and set all levels OFF 
   Even if vmeio is not present in crate, we get no errors
 */

#ifdef OS_VXWORKS
  status = musrVmeioInit(vmeio_base); /* initialize driver and set VME base address */
  if(status != 1) 
    {
      cm_msg(MERROR,"vmeio_init","error return from musrVmeioInit");
      return(status);
    }

  for (i=FIRST_OUTPUT; i < MAX_N_OUTPUT ; i++)  /* MUSR is using the highest output channels of IO register  */
    {
      status = musrVmeioLatchSet(i); /* initialize - set all the channels to Latch mode */
      if(status != UVIO_SUCCESS)
      {
	cm_msg(MERROR,"vmeio_init","bad status (%d) after selecting latch mode for channel %d",status,i);
	return(status);
      }
      status = musrVmeioOff(i);  /* turn all channels OFF */
      if(status != UVIO_SUCCESS)
      {
	cm_msg(MERROR,"vmeio_init","bad status (%d) after setting Latch Off for channel %d",status,i);
	return(status);
      }
    }
  

    /* there seems to be no way to check the module is present
       except musrVmeioInputState() seems to give 0xffffff if module not in crate instead of 0
       There is no hardware readback of the state of the output channels 
   */
  state =  musrVmeioInputState(); /* hardware access */
  if( state != 0 )
  {
    printf("vmeio_init: read input state = 0x%x; expect 0\n",state);
    cm_msg(MERROR,"vmeio_init","IO board is not responding");
    return(-1);
  }
  
  /* Both these should be zero for Latch, all OFF (software only no hardware access) */
  if(debug)
    printf("vmeio_init: State:0x%x  Mode:0x%x\n", musrVmeioOutputState(), musrVmeioOutputMode() );

/* IMUSR uses pulse mode on channels IO_START_ACQ and IO_TIMEOUT */
  status = musrVmeioPulseSet(IO_START_ACQ);
  if(status != UVIO_SUCCESS)
  {
    cm_msg(MERROR,"vmeio_init","bad status (%d) after selecting pulse mode for channel %d",status,IO_START_ACQ);
    return(status);
  }
  
  status = musrVmeioPulseSet(IO_TIMEOUT);
  if(status != UVIO_SUCCESS)
  {
    cm_msg(MERROR,"vmeio_init","bad status (%d) after selecting pulse mode for channel %d",status,IO_TIMEOUT);
    return(status);
  }
  
   /* State should still be zero (all OFF) but Mode should now be 0x2400 */
  if(debug)
    printf("vmeio_init: State:0x%x  Mode:0x%x ; expect 0 and 0x2400\n", musrVmeioOutputState(), musrVmeioOutputMode() );

#endif
  if(debug)printf("vmeio_init: set all latched output channels to OFF\n");
  return(SUCCESS);
}
/*-----------vmeio_on()-------------------- */
void vmeio_on()
{ /* not used for imusr; musrVmeioOn is used instead */
  return;
}
/*-----------vmeio_on()-------------------- */
void vmeio_off()
{ /* not used for imusr; musrVmeioOff is used instead  */
  return;
}




/*-- Scaler read/clear enabled real scaler channels and sum into gbl_scaler_sums  array  -----------*/

INT scaler_sum(void)
{
  /* called by imusr_acq
     returns error if overflow
   */
  DWORD *pdata;
  INT js,i=0;
  DWORD counts,overflow;
  char str[128];
  INT combined_toggle;

 
  if(dss) printf("scaler_sum: starting\n");
  if (! fs.enabled)
  {
    if (dss) printf("scaler_sum: returning as scaler is disabled\n");
    return(0); /* no event */
  }
  
#ifdef OS_VXWORKS
  if (vs.flags.test_pulses)    /* test pulse mode  */
    sis3803_test_pulse(SIS_base_adr);
#endif
  

  if(dss)
    printf("scaler_sum: checking for overflow\n");

  /* check the overflow  - can't sum if there's an overflow */
#ifdef OS_VXWORKS
  sis3803_OVFL_grp1_read(SIS_base_adr, &overflow);
  if(max_scaler_input > 8)
  {
    sis3803_OVFL_grp2_read(SIS_base_adr, &counts);
    overflow = overflow || (counts << 8);
  }
#else
  overflow=0;
#endif
  
  if(overflow)
  {
    cm_msg(MINFO,"scaler_sum",
	   "scaler channel(s) have overflowed (bitpat=0x%x)\n",overflow);
    return(FE_ERR_HW);
  }

 
  if(dss)
    printf("scaler_sum: Reading %d values from the scaler in input order\n",fs.num_inputs);


  /* calculate the combined toggle state of inner and outer toggles */
  combined_toggle =  gbl_outer_toggle_state <<1 |  gbl_inner_toggle_state;
  
  
  /* Readout the data
     Scaler input channels for IMUSR are fixed in imusr_inputs.h 
   */
  for (i=0; i < fs.num_inputs; i++) /* number of real inputs */
  {
    js = fs.inputs[i];
    /*
    if(js<0 || js > max_scaler_input)
    {
      printf("Error: data index (%d) out of range (0<index<%d); scaler bitpat & scaler input array may be incompatible\n",
	     js,max_scaler_input);
      cm_msg(MERROR,"scaler_sum","Scaler data index (%d) out of range (0<index<%d)",js,max_scaler_input);
      return (0);
    }
    */
    
    counts=0;
#ifdef OS_VXWORKS
    sis3803_counter_read(SIS_base_adr,js, &counts); /* read */
    sis3803_single_clear(SIS_base_adr, js);  /* and clear */
#else 
    counts = i+1;   /* just for testing under linux*/
#endif
    if(dss)printf("scaler_sum: Index %d, working on scaler input %d, counts=%d\n",
      i,js,counts);
    switch (js)
    {
    case SC_CLOCK:
      gbl_scaler_sums[CLOCK_INDEX ]  += (double) counts;
      scaler_counts[CLOCK_INDEX] = counts; /* store for v680_display */
      if(dss)
	printf("scaler_sum: Outog=%1.1d Intog=%1.1d both=%1.1d;  CLOCK data=%d->scaler_sums[%d]\n",
	gbl_outer_toggle_state, gbl_inner_toggle_state,combined_toggle,counts,CLOCK_INDEX );  

      if(is.input.constant_time)
	{
	  /* constant time mode */
	  
	  /* just check that the counting did stop as it should have */
	  if ( abs(counts-gbl_ticks0) > 4)  /* may be a few counts difference */
	    {
	      sprintf(display_string,"problem with wiring?  Clock counts (%d) have changed from gbl_ticks0 (%d) ",
		      counts,gbl_ticks0);
	      if(!v680_Display)
		printf("scaler_sum: WARNING possible problem with wiring; clock counts (%d) have changed from gbl_ticks0 (%d)\n", 
		       counts,gbl_ticks0);
	    }
	}

      break;
      
    case SC_TOTAL_RATE:
      gbl_scaler_sums[TOTALR_INDEX ]  += (double) counts;
      scaler_counts[TOTALR_INDEX] = counts; /* store for v680_display */
       if(dss)
	printf("scaler_sum: Outog=%1.1d Intog=%1.1d both=%1.1d; TotalRate data=%d->scaler_sums[%d]\n",
	       gbl_outer_toggle_state, gbl_inner_toggle_state,combined_toggle,counts,TOTALR_INDEX );  
       /* os.total_mucounts += (double)counts; odb doesn't like double */
       os.total_mucounts += (float)counts;
       
       if(!is.input.constant_time)
	 {
	   /* constant rate mode */

	   /* just check that the counting did stop as it should have */
	   if(abs(counts-gbl_total_rate) > 2)  /* seems to be a couple of counts difference */
	     {
	       sprintf(display_string,"scaler didn't stop counting; total rate counts (%d) have changed from gbl_total_rate (%d) ",
		       counts,gbl_total_rate);
	       if(!v680_Display)
		 printf("scaler_sum: WARNING scaler didn't stop counting; total rate counts (%d) have changed from gbl_total_rate (%d)\n", 
			counts,gbl_total_rate);
	     }
	 }
       break;
      
    case SC_FRONT_FM_NEG:
      gbl_scaler_sums[FRONT_INDEX + combined_toggle ]  += (double) counts;
      scaler_counts[FRONT_INDEX + combined_toggle ] = counts; /* store for v680_display */
       if(dss)
	printf("scaler_sum: toggle Out(%1.1d) In(%1.1d) both(%1.1d);Input %d FRONT data=%d->scaler_sums[%d]\n",
	       gbl_outer_toggle_state, gbl_inner_toggle_state,combined_toggle,js,counts,(FRONT_INDEX+combined_toggle) );  
      os.total_counts += (double)counts; /* add to sum */
       break;
      
    case SC_BACK_FM_NEG:
      gbl_scaler_sums[BACK_INDEX + combined_toggle ] += (double)  counts;
      scaler_counts[BACK_INDEX + combined_toggle ] = counts; /* store for v680_display */
     if(dss)
	printf("scaler_sum: toggle Out(%1.1d) In(%1.1d) both(%1.1d);Input %d BACK data=%d->scaler_sums[%d]\n",
	       gbl_outer_toggle_state, gbl_inner_toggle_state,combined_toggle,js,counts,(BACK_INDEX+combined_toggle) );  
       os.total_counts += (double)counts; /* add to sum */
      break;
      
    case SC_BACK_FM_POS:
      if(!gbl_fast_mod)
      {
	 cm_msg(MERROR,"scaler_sum","Back Fast Mod scaler channel (%d) detected when not found by scaler_init\n",
	       js);
      
	return(FE_ERR_HW);
      }
      gbl_scaler_sums[BACK_FM_INDEX + combined_toggle ]  += (double) counts;
      scaler_counts[BACK_FM_INDEX + combined_toggle ] = counts; /* store for v680_display */
       if(dss)
	printf("scaler_sum: toggle Out(%1.1d) In(%1.1d) both(%1.1d);Input %d back_FM data=%d->scaler_sums[%d]\n",
	     gbl_outer_toggle_state, gbl_inner_toggle_state,combined_toggle,js,counts,BACK_FM_INDEX );  
      os.total_counts += (double)counts; /* add to sum  */
      break;
      
    case SC_FRONT_FM_POS:
      if(!gbl_fast_mod)
      {
	cm_msg(MERROR,"scaler_sum","Front Fast Mod  scaler channel (%d) detected when not found by scaler_init\n",
	       js);
	return(FE_ERR_HW);
      }
      gbl_scaler_sums[FRONT_FM_INDEX + combined_toggle ]  += (double) counts;
      scaler_counts[FRONT_FM_INDEX + combined_toggle ] = counts; /* store for v680_display */
      if(dss)
	printf("scaler_sum: toggle Out(%1.1d) In(%1.1d) both(%1.1d);Input %d front_FM data=%d->scaler_sums[%d]\n",
 	     gbl_outer_toggle_state, gbl_inner_toggle_state,combined_toggle,js,counts,(FRONT_FM_INDEX+combined_toggle) );  
      /*    os.total_counts += (double)counts; /* add to sum   odb doesn't like double */
       os.total_counts += (float)counts;
      break;
      
    default:
      printf("scaler_sum: unexpected scaler input %d detected\n",js);
      

    } /* end of switch */

  } /* end of for loop */
/*
  for (i=0; i<gbl_num_virtual_scaler_chans; i++)
    printf("Scaler_counts[%d]=%d\n",i,scaler_counts[i]);
*/
  if (dss)printf("Total counts : %f ; total mucounts (total_rate scaler) : %f\n",
		 os.total_counts,os.total_mucounts);
  
  
  return (SUCCESS);
}
/*------------------------------------------------------------------------------------------*/

INT get_limits (char *pinner, char *pouter, float start,float stop, float innertogval, float outertogval,float *pmin, float*pmax)
{
  
  float iref,oref;
  int IREF,OREF;
  float xstart,xstop, it_val,ot_val;
  float min,max;
 
  if(debug)
    printf("get_limits: inner:%s outer:%s start:%f stop:%f intogval:%f outtogval:%f \n",
	   pinner, pouter, start, stop,  innertogval, outertogval);
  
    
  IREF=OREF=0;
  it_val=iref=0;
  ot_val=oref=0;
  
  
  if (strcmp (pinner,"REF")==0 )
    {
      if(debug)
	printf("get_limits: inner is REF toggle\n");
      IREF=1;
      iref=innertogval;
    }
  else if (strcmp (pinner,"SOFT")==0 )
    {
      if(debug)
	printf("get_limits: inner is SOFT toggle\n");
      it_val=innertogval;
    }
  else if( (strcmp (pinner,"HARD")==0 ) || (strcmp (pinner,"NONE")==0))
    {
      if(debug)
	printf("get_limits: inner toggle (%s) is HARD or NONE\n",pinner);
    }
  else
    {
      printf("get_limits: illegal inner toggle type %s\n",pinner);
      return(-1);
    }
  

  if (strcmp (pouter,"REF")==0 )
    {
      if(debug)
	printf("get_limits: outer is REF toggle\n");
      OREF=1;
      oref=outertogval;
    }
  else if (strcmp (pouter,"SOFT")==0 )
    {
      if(debug)
	printf("get_limits: outer is SOFT toggle\n");
      ot_val=outertogval;
    }
  else if( (strcmp (pouter,"HARD")==0 ) || (strcmp (pouter,"NONE")==0))
    {
      if(debug)
	printf("get_limits: outer toggle (%s) is HARD or NONE\n",pouter);
    }
  else
    {
      printf("get_limits: illegal outer toggle type %s\n",pouter);
      return(-1);
    }
  
  /* use soft toggle values */
  
  xstart = fabs(start) + fabs(it_val) + fabs(ot_val);
  xstop = fabs (stop) + fabs(it_val) + fabs(ot_val);
  
  if(start < 0) 
    xstart = xstart * -1;
  if(stop < 0) 
    xstop = xstop * -1;
  
  if(debug)
    printf("get_limits: soft : start=%f, stop=%f, it_val=%f,ot_val=%f,  xstart=%f, xstop=%f\n",
	 start,stop,it_val,ot_val,xstart,xstop);
  min = smin(xstart,xstop);
  max = smax(xstart,xstop);
  
  printf("get_limits: Min is %f, max is %f\n",min,max);
  
  /* Now check for ref */
  if(IREF)
    {
      if(debug)
	printf("get_limits: REF:  inner_tog_val=%f\n",iref);
      
      min = smin(min,iref);
      max = smax(max,iref);
    }
  if(OREF)
    {
      if(debug)
	printf("get_limits: REF:  outer_tog_val=%f\n",oref);
      
      min = smin(min,oref);
      max = smax(max,oref);
    }
  
  if(debug)
    printf("get_limits:  Returning with Min= %f, max =%f\n",min,max);
  *pmin=min;
  *pmax=max;
  return(SUCCESS);
}


/*------------------------------------------------------------------------------------------*/


float smin(float a, float b)
{
  if ( a < b)
    return a;
  else
    return b;
}

/*------------------------------------------------------------------------------------------*/

float smax(float a, float b)
{
  if ( a > b)
    return a;
  else
    return b;
}

/*------------------------------------------------------------------------------------------*/
/*   GENERAL SWEEP ROUTINES
     
 */


/*------------------------------------------------------------------------------------------*/

INT check_sweep_params(char * primdev, float sweep_start, float sweep_stop)
{
/* Make sure the sweep params are within range for specific sweep device

   It is assumed primdev is in UPPER CASE 

*/

/* first check they are in range of user's maximum and minimum values in odb */
  if(( smin(sweep_start,sweep_stop) < is.hardware.sweep_device.minimum) ||
     (smax(sweep_start,sweep_stop) > is.hardware.sweep_device.maximum))
    {
      cm_msg(MERROR,"check_sweep_params",
	     "max or min sweep values (%f or %f) are out of range of max (%f) or min (%f) specified in odb for sweep device",
	     sweep_start,sweep_stop, is.hardware.sweep_device.maximum,is.hardware.sweep_device.minimum);
      return(DB_INVALID_PARAM);
    }

  if(strcmp(primdev,"DAC") == 0)
  {
    printf("Detected VME DAC device\n"); 
    /* DAC is in VME not CAMP */
    status = check_sweep_params_DAC(sweep_start,sweep_stop);
  }
  else
  {
    printf("Detected Camp device\n"); 
    /* CAMP devices */
    status = check_sweep_params_CAMP();  
  }
  return status;
}
/*------------------------------------------------------------------------------------------*/


INT init_sweep_dev(char * primdev)
{
    if(dc)
	printf("init_sweep_dev: starting with primdev = %s\n",primdev);
    gbl_conversion_factor = 1.0 ;
    if(strcmp(primdev,"DAC") == 0)
    {
	/* DAC is in VME not CAMP */
	status = init_DAC();
    }
    else
    {
	if(dc)printf("init_sweep_dev: primdev= %s, calling initCamp \n",primdev);
	status = initCamp(&gotCamp);
	printf("after initCamp, gotCamp=%d\n",gotCamp);
    }
    return status;
}

/*--------------------------------------------------------------*/

INT initCamp(BOOL *camp_active)
{
  /*
    Called from init_sweep_device
   */
    if(dc)printf("initCamp starting\n");
    
    status = camp_update_params(); /* scan start,stop and incr.  are not included; they are in the input tree */

    if (status != SUCCESS)
      return(status);
    printf("initCamp: about to call camp_initPath\n");
    status = camp_initPath(camp_params, camp_active); /* minimal initiailization */ 
    
  
    if(dc) printf("initCamp: after camp_initPath camp_active=%d  status=%d\n",camp_active,status);
    if (status != CAMP_SUCCESS)
    {
      printf("initCamp: check CAMP status; retry run start\n"); 
      return (FE_ERR_HW);
    }
  else
    /*if(dc)*/
    printf("initCamp: successfully initialized camp, camp_active=%d \n",*camp_active);

  return(SUCCESS);

}

/*------------------------------------------------------------------------------------------*/

INT set_sweep_dev(char * primdev, float setval)
{
  INT max_err=25; /* retry 25 times then give up */
  INT icount;
  BOOL fail_flag=FALSE;
  INT camp_status;
  


  if(dc)printf("set_sweep_dev starting with primdev =%s,setval=%f\n",primdev,setval);
  
  if(strcmp(primdev,"DAC") == 0)
    {
      /* DAC is in VME */
      status = set_DAC(setval);
      return (status);
    }
  else
    {   /* sweep dev is Camp */
      status =  set_sweep_device(setval, camp_params);      
      if(status == CAMP_SUCCESS)
	{
	  if(di)printf("set_sweep_dev:successfully set primdev %s, setval=%f \n",primdev,setval);  
	  return(SUCCESS);
	}

      cm_msg(MERROR,"set_sweep_dev","Error attempting to set CAMP value of %f %s (%d)",
	     setval,camp_params.units,status);
      if(!v680_Display)
	printf("set_sweep_dev: Error from CAMP set_sweep_device (%d) \n",status);
      sprintf(display_string,"trying to set the sweep device to %f %s\n",setval,camp_params.units);

      for(icount=0; icount < max_err; icount++)
	{ 
	  gbl_out_of_tolerance_flag=TRUE; /* set this flag to indicate a problem */
	  if(gotCamp) /* do we still have Camp ? */
	    {
	      status=camp_watchdog(); /* can we talk to CAMP? */
	      if(status == CAMP_SUCCESS)
		{  /* try to set the device again */
		  if(!v680_Display)
		    printf("Retrying set_sweep_device (count=%d)...\n",icount);
		  status = set_sweep_device(setval,camp_params);
		  if(status == CAMP_SUCCESS)
		    {
		      gbl_out_of_tolerance_flag=FALSE;
		      cm_msg(MINFO,"set_sweep_dev","successfully set primdev %s, setval=%f after %d 
retries\n",primdev,setval,icount);
		      if (v680_Display != 0)
			imusr_display(1);
		      return(SUCCESS);
		    }
		}
	    } /* camp_watchdog failed... reconnect */
	  gotCamp=FALSE;
	  if(!v680_Display)
	    printf("set_sweep_dev: calling camp_reconnect...(count=%d)\n",icount);
	  status = camp_reconnect(); /* disconnects from CAMP then reconnects */
	  if(status == SUCCESS)
	    {
	      if(dc)
		printf("camp_reconnect was successful...retrying set_sweep_device...(count=%d)\n",icount);
	      status = set_sweep_device(setval,camp_params);
	      if(status == CAMP_SUCCESS)
		{
		  cm_msg(MINFO,"set_sweep_dev","successfully set primdev %s, setval=%f after %d retries \n",
			 primdev,setval,icount);  
		  gbl_out_of_tolerance_flag=FALSE;
		  if (v680_Display != 0)
		    imusr_display(1);
		  return(SUCCESS);
		}
	      
	      printf("set_sweep_dev:failed to  set primdev %s, setval=%f (retry=%d)\n",
		     primdev,setval,icount);  
	      
	    }
	  printf("set_sweep_dev: waiting 10s then retrying\n");
	  cm_yield(5000);
	  ss_sleep(5000);
	} /* end of for loop */
    
      camp_status = status; /* remember the status return */
      
      cm_msg(MERROR,"set_sweep_dev","Cannot set primdev %s to setval=%f after %d retries",
	     primdev,setval,icount);  
      /* stop the run */
      gbl_status=FALSE; /* frontend loop won't restart anything. Can't toggle without sweep dev */
      status = set_client_flag("frontend",gbl_status); /* mdarc should stop the run (hot link on client flag). */
#ifdef OS_VXWORKS
      status = musrVmeioOff(IO_ACQ_ACTIVE);
#endif
      gbl_active_flag = FALSE; 
      gbl_hold_flag = TRUE; /* this causes out-of-tol condition */
      gbl_pause_start = ss_millitime(); /* reinitialize pause start time */ 
      gbl_pause_midcycle_flag = TRUE; /* set this flag */
      cm_yield(1000); /* let mdarc have a chance to stop this run */
    }
  
  return camp_status; 
	
}

/*------------------------------------------------------------------------------------------*/

/*   SWEEP ROUTINES FOR SPECIFIC DEVICES

     
 */

/*------------------------------------------------------------------------------------------*/

INT  check_sweep_params_CAMP()
{ /* dummy for now  - likely use a script */
  return SUCCESS;
}


/*------------------------------------------------------------------------------------------*/


INT check_sweep_params_DAC(float sweep_start, float sweep_stop)
{
  /* DAC specific; sweep_start and stop are in counts
     
  Dummy - we don't need to do anything */
  
 
 
 return SUCCESS;
} 
/*------------------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------------------*/

INT init_DAC()
{
  /*
    Set the DAC to the appropriate range according to odb parameter
    
  */
  INT range;
  
  range = gbl_DAC_range = is.hardware.gate_generator.dac_range;
  if(di)
    printf("init_DAC : Setting DAC range to %d \n",gbl_DAC_range);
  
  
  /* set the DAC range */
#ifdef OS_VXWORKS
  range = gglWriteDacRange(ggl_base, gbl_DAC_range);
#endif
  if(di)printf("init_DAC:  Wrote DAC range=%d, read back %d\n",gbl_DAC_range,range);
  
  sprintf (disp_units,"C"); /* units are counts */ 
  
  if(di)printf("init_DAC: no specific initialize needed for DAC\n");  
  return SUCCESS;
}

/*------------------------------------------------------------------------------------------*/

INT set_DAC(float setval)
{
  /* set the DAC ; setval is in counts */
  INT DAC_counts,data;
  
  if(di)printf("set_DAC: starting with setval = %f counts\n",setval);
  DAC_counts = (INT) setval;
  if(DAC_counts >=0 && DAC_counts <= 0xFFFF)
  {
#ifdef OS_VXWORKS
  data = gglWriteDacSetpoint(ggl_base,  DAC_counts);
#else
  data = DAC_counts;
#endif
    if(di)printf ("set_DAC: Set DAC counts = %d, read back %d counts\n",DAC_counts,data);
  }
  else
    {
      cm_msg(MERROR,"set_DAC","Invalid DAC counts (%d); valid range = 0 - 65535",DAC_counts);
      return (DB_INVALID_PARAM);
    }
  return SUCCESS;
}

/*------------------------------------------------------------------------------------------*/


/*

  End of specific sweep routines

 */
   

/*------------------------------------------------------------------------------------------*/

INT send_data_point (char *pevent, INT off)
{
  /* send out a data point (which consists of the sum scalers and the SD value)
     archiver program will read relevent camp values
   */
  
  double *pdata;
  INT i,j,k;
  BOOL camp_triggered,first;
  double temp_data[25];




  camp_triggered=FALSE;
  
  if (!waiteqp[SEND_DATA])
    {
      if(dx)printf("send_data_point: starting with pevent=%p, nothing to do, returning\n",pevent);
      return 0; /* no data ready */
    }
  else
    if(dx)printf("send_data_point: starting, waiting for data, pevent=%p\n",pevent);
/* TEMP */
#ifdef OS_VXWORKS
  /*  status = musrVmeioOff(2); */
  /* status = musrVmeioOn(3); */
#endif

  if(dx)printf("send_data_point: starting, sending out data point %d\n",gbl_data_array[0]);


  if(n_camp_logged>0) /* if we have some camp variables to log */
  {
    if(gbl_data_array[0]==0)first=TRUE; /* initialize for message */
    if(debug)printf("send_data_point: Calling trigger_cvar_event\n");
    camp_triggered = trigger_cvar_event(&rstate) ; /* (returns rstate) - an event has been triggered
						    (write_data MUST also be true) */

    if (camp_triggered)
      os.num_camp_triggered++; /* increment counter of camp events triggered */  
    else
      {
	/* send this message only once */
	if(first)
	  {
	    first=FALSE;
	    if(fecamp_exists)
	      cm_msg(MINFO,"send_data_point","could not trigger a camp event");
	    else
	      cm_msg(MINFO,"send_data_point","could not trigger a camp event; client \"fe_camp\" is not running");
	  } 
      }
  }
  
  /* init bank structure */
  bk_init(pevent);
  
  /* create DATA bank */
  bk_create(pevent, "IDAT", TID_DOUBLE, &pdata);

  if(dx)printf("Creating bank for data point %f at sweep value %f\n",
	 gbl_data_array[0],gbl_data_array[1]);
    
    


  /* now pick out what data we want to send out in the bank 

  would copy data straight into pdata except we want to print
  out the bank data for debugging
*/


  /* if inner and outer toggle both enabled, it's straightforward ... */
  if ( strcmp("NONE",gbl_inner_toggle_type) != 0 &&
       strcmp("NONE",gbl_outer_toggle_type) != 0)
    {
      for (i=0; i < gbl_len_data; i++) 
	  temp_data[i] = gbl_data_array[i];
      k=i-1; /* last filled index */
    }
  else
    { /* pick and choose the data

      /* copy first four fixed values (data point, sweep value, clock, total_rate */
      for (i=0; i < 4; i++)
	  temp_data[i]=gbl_data_array[i]; /* later put straight into pdata */
      k=i-1;  /* last filled index */
      j=2; /* offset for data point, sweep value */
      
      /*  Note:  outer toggle must also be NONE if inner toggle is NONE */
      if ( strcmp("NONE",gbl_inner_toggle_type) == 0)
	{
	  temp_data[i]=gbl_data_array[j+FRONT_INDEX];
	  temp_data[i+1]=gbl_data_array[j+BACK_INDEX];
	  k+=2;
	  if(gbl_fast_mod)
	    {
	      temp_data[i+2]=gbl_data_array[j+FRONT_FM_INDEX];
	      temp_data[i+3]=gbl_data_array[j+BACK_FM_INDEX];
	      k+=2;
	    }
	}
      else
	{
	  /* inner toggle is enabled */
	  if( strcmp("NONE",gbl_outer_toggle_type) == 0)
	    {   /* outer toggle is disabled */
	      temp_data[i]=gbl_data_array[j+FRONT_INDEX];
	      temp_data[i+1]=gbl_data_array[j+FRONT_INDEX+1];
	      
	      temp_data[i+2]=gbl_data_array[j+BACK_INDEX];
	      temp_data[i+3]=gbl_data_array[j+BACK_INDEX+1];

	      k+=4;
	      if(gbl_fast_mod)
		{
		  temp_data[i+4]=gbl_data_array[j+FRONT_FM_INDEX];
		  temp_data[i+5]=gbl_data_array[j+FRONT_FM_INDEX+1];
		  
		  temp_data[i+6]=gbl_data_array[j+BACK_FM_INDEX];
		  temp_data[i+7]=gbl_data_array[j+BACK_FM_INDEX+1];
		  
		  k+=4;
		}
	    } /* end of no outer toggle */
	}  /* end of no inner toggle */
    }
  
  if(dx)
    printf("length of raw data %d , length of data bank: %d, last filled index %d\n",gbl_len_data,gbl_len_data_bank,k);
  
  for (i=0; i < gbl_len_data_bank; i++) 
    *pdata++ = (double)temp_data[i]; 
  
  
  if( v680_Display == 0)
    {
      printf(  " Index     data        bank\n");
      for (i=0; i < gbl_len_data; i++) 
	{
	  if ( i > k)
	    printf("  %2d        %6.2f \n",i,gbl_data_array[i]);
	  else
	    printf("  %2d        %6.2f         %6.2f\n",i,gbl_data_array[i],temp_data[i]);
	}
    }



  bk_close(pevent, pdata);
  if(dx)printf("send_data_point: closed bank with pointer=%p\n",pdata);

  /* after sending out data */
  os.total_data_points++; /* total number of data points sent out */

/* update output area */
  size = sizeof (os.total_data_points);
  status=db_set_value (hDB, hOS, "total data points", &os.total_data_points, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"send_data_point","could not write value=%d to \"%s/total data points\" (%f) ",os.total_data_points,str_out,status);

  size = sizeof (os.num_camp_triggered);
  status=db_set_value (hDB, hOS, "num camp triggered", &os.num_camp_triggered, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"send_data_point","could not write value=%d to \"%s/num camp triggered\" (%f) ",os.num_camp_triggered,str_out,status);


  if (dx)
    printf("Size of bank DATA is %d\n",bk_size(pevent));

  if(camp_triggered)
    {
    sprintf(display_string,"Sent bank for data point %6.0f (sweep value %f); camp event triggered ",
	    gbl_data_array[0],gbl_data_array[1]);
    }
  else
    sprintf(display_string,"Sent bank for data point %6.0f (sweep value %f) sent; camp event NOT triggered ",
	    gbl_data_array[0],gbl_data_array[1]);
  
/* TEMP */
#ifdef OS_VXWORKS
  /*  status = musrVmeioOff(3); */
#endif
  gbl_send_time = ss_millitime() - gbl_wait_time;
  
  waiteqp[SEND_DATA]= FALSE;
  return bk_size(pevent);
}

/*---------hot_display ---------------------------------------------*/
void hot_display_period (HNDLE hDB, HNDLE hktmp ,void *info)
{
  
  /* key "display period (s)" has been touched - update display period
  */

  /* if(debug) */
  cm_msg(MINFO,"hot_display_period","key v680/display period (s) has been touched");

  if (vs.display_period__s_ > 1)    
    v680_Display_period = vs.display_period__s_ * 1000; /* ms; use default otherwise */

}
/*---------hot_display ---------------------------------------------*/
void hot_display (HNDLE hDB, HNDLE hktmp ,void *info)
{
  INT onoff;

 
  /* key "display" has been touched (actually toggles onoff) - update display   */
  if(debug)cm_msg(MINFO,"hot_display","key v680/display has been touched");

  
  onoff=vs.display; /* vs.display automatically updated by the hotlink */
  imusr_display(onoff);
  
  if (TD_MUSR)
  {
#ifdef OS_VXWORKS
    set_v680_display(v680_Display,disp_offset,last_message); /* send info to V680.c */
#endif
  }

}



/* IMUSR hot links */

/*---------hot_sweep_step ---------------------------------------------*/
void  hot_sweep_step (HNDLE hDB, HNDLE hStep ,void *info  )
{
  /* key "sweep step" has been touched; automatically updated by hotlink */

  INT size,status,i; 
  if(debug)
  {
    printf("hot_sweep_step:hot key \"sweep step\" has been touched\n");     
    /* printf("               closing record for \"sweep step\", hStep=%d\n",hStep); */
  }

  if( is.input.sweep_step == 0 )
  {
    cm_msg(MERROR,"hot_sweep_step","New sweep increment value is invalid %f. Retaining original",
	   is.input.sweep_step,gbl_sweep_step);
    return;
  }
  /* if gbl values have not yet been assigned do nothing (i.e. just started) */
  if (gbl_sweep_stop ==0 && gbl_sweep_start ==0)return;
  if (gbl_sweep_step == is.input.sweep_step ) return; /* value has not changed */
  
  /* make sure we have at least one step */
  if (  (fabs( gbl_sweep_stop - gbl_sweep_start))   / fabs (is.input.sweep_step)  < 1)
  {
    cm_msg(MERROR,"hot_sweep_step","invalid sweep step size (%f) ; must have at least one step",
	   is.input.sweep_step);
    return;
  }
  

  /* since these hotlinks are open all the time (temp since close_record not working)
     make sure these don't try to update the display at BOR when MUI writes all the values
     - we can use gbl_BOR_flag to indicate BOR; */

  if(! gbl_BOR_flag)
    {
      sprintf(display_string,"Sweep step changed from %f to  %f ",gbl_sweep_step, is.input.sweep_step);
      cm_msg(MINFO,"hot_sweep_step","Sweep step changed from %f to %f",gbl_sweep_step, is.input.sweep_step);      
    }
  gbl_sweep_step =  is.input.sweep_step; /* set the new value */

  if(v680_Display)
    gbl_redo_display =  TRUE; /* set a flag */

  /* info_odb will update the output area periodically */

  return;
}

/*---------hot_sweep_start---------------------------------------------*/
void  hot_sweep_start (HNDLE hDB, HNDLE hStart ,void *info  )
{
  /* key "sweep start" has been touched; value automatically updated by hotlink */

 
  INT size,status,i; 

  if(debug)
    printf("hot_sweep_start: hot key \"sweep start\" has been touched\n"); 

  if(gbl_sweep_step == 0)
  {  /* gbl_sweep_step hasn't been setup yet; do nothing */
      return;
  }
  if(gbl_sweep_start ==  is.input.sweep_start) return; /* no change */
      
  /* make sure we still would have at least one step */
  if ( ( fabs(gbl_sweep_stop - is.input.sweep_start)/ fabs(gbl_sweep_step) )  < 1)
  {
    cm_msg(MERROR,"hot_sweep_start","invalid sweep start (%f) ; must have at least one step",
	   is.input.sweep_start);
    return;
  }
  
  /* make sure params are STILL valid for specific sweep device & determine any other
     parameters needed (e.g. range for DAC) which may have changed  */
  status = check_sweep_params(is.input.sweep_device,
			      is.input.sweep_start, gbl_sweep_stop );
  if(status != SUCCESS)
  {
    cm_msg(MERROR,"hot_sweep_start","invalid sweep start (%f); out of range for sweep device",
	   is.input.sweep_start);
    return;
  }


  /* since these hotlinks are open all the time (temp since close_record not working)
     make sure these don't try to update the display at BOR when MUI writes all the values
     - set a flag gbl_redo_display so display gets updated only once  */

  if (!gbl_BOR_flag)  
    {
      sprintf(display_string,"sweep start changed from %f to %f ",gbl_sweep_start,is.input.sweep_start);	
      cm_msg(MINFO,"hot_sweep_start","sweep start changed from %f to %f\n",gbl_sweep_start,is.input.sweep_start);
    }
  gbl_sweep_start = is.input.sweep_start; /* update working value */

  if(v680_Display)
    gbl_redo_display = TRUE;
  
  /* info_odb will update the output area periodically */
  
  return;
}





/*---------hot_sweep_stop ---------------------------------------------*/
void  hot_sweep_stop (HNDLE hDB, HNDLE hStop ,void *info  )
{
  /* key "sweep stop" has been touched; updated automatically by hotlink

 */
  INT size,status,i;
  if(debug)
    printf("hot_sweep_stop:hot key \"sweep stop\" has been touched\n"); 

  if(gbl_sweep_step == 0)
  {  /* gbl_sweep_step hasn't been setup yet; do nothing */
      return;
  }
  if(gbl_sweep_stop ==  is.input.sweep_stop) return; /* no change */
 

  /* make sure we still would have at least one step */
  if ( ( fabs(is.input.sweep_stop - gbl_sweep_start)/ fabs(gbl_sweep_step) )  < 1)
  {
    cm_msg(MERROR,"hot_sweep_stop","invalid sweep stop (%f) ; must have at least one step",
	   is.input.sweep_stop);
    return;
  }

  /* make sure params are STILL valid for specific sweep device & determine any other
     parameters needed (e.g. range for DAC) which may have changed  */
  status = check_sweep_params(is.input.sweep_device,
			      gbl_sweep_start, is.input.sweep_stop);
  if(status != SUCCESS)
  {
    cm_msg(MERROR,"hot_sweep_stop","invalid sweep stop (%f); out of range for sweep device",
	   is.input.sweep_stop);
    return;
  }


  /* since these hotlinks are open all the time (temp since close_record not working)
     make sure these don't try to update the display at BOR when MUI writes all the values
     - set gbl_redo_flag */
  
  if (!gbl_BOR_flag)  
    {
      sprintf(display_string,"sweep stop changed from %f to %f",gbl_sweep_stop,is.input.sweep_stop);     
      cm_msg(MINFO,"hot_sweep_stop","sweep stop changed from %f to %f",gbl_sweep_stop,is.input.sweep_stop);
    }
  gbl_sweep_stop = is.input.sweep_stop; /* update working value */

  if(v680_Display)
    gbl_redo_display=TRUE;
  
  
  /* info_odb will update the output area periodically  */
  
  return;
}





/*---------hot_tolerance ---------------------------------------------*/
void  hot_tolerance (HNDLE hDB, HNDLE hTol ,void *info  )
{
  /* key  has been touched, automatically updated by hotlink 

 */
  INT size,status,i;

  if(debug)
    printf("hot_tolerance: hot key \"tolerance (%)\" has been touched"); 
 
  if((float)(gbl_tol * 100.0) ==  (float)is.input.tolerance____) return; /* no change */

  if(debug)printf("hot_tolerance: new value of tolerance (%) = %f\n",is.input.tolerance____);  
  if (!gbl_BOR_flag)  
    {  /* suppress messages at BOR */
      sprintf(display_string,"tolerance changed from %f % to %f %",gbl_tol*100.0, is.input.tolerance____);
      cm_msg(MINFO,"hot_tolerance","tolerance changed from %f % to %f %",gbl_tol*100.0, is.input.tolerance____);
    }
  /* Note: tolerance value does not appear on the v680 consol display */
  
  gbl_tol = is.input.tolerance____ / 100.0; /* calculate as a fraction */
  return;
}

/*---------hot_tol_delay ---------------------------------------------*/
void  hot_tol_delay (HNDLE hDB, HNDLE hTolD ,void *info  )

{
  /* key  has been touched;  automatically updated by hotlink


 */
  INT size,status,i; 
  if(debug)
    printf("hot_tol_delay: key \"out-of-tolerance delay (s)\" has been touched\n"); 
  if (gbl_outol_delay == is.input.out_of_tolerance_delay__s_) return; /* no change */



  if (!gbl_BOR_flag)  
    { /* suppress messages at BOR */
      sprintf(display_string,"out-of-tol delay changed from %f s to %f s",gbl_outol_delay,is.input.out_of_tolerance_delay__s_);
      cm_msg(MINFO,"hot_tol_delay","out-of-tol delay changed from %f s to %f s",gbl_outol_delay,is.input.out_of_tolerance_delay__s_);
    }
  gbl_outol_delay = is.input.out_of_tolerance_delay__s_; /* remember the value for next time */


  /* Note: tolerance delay does not appear on V680 consol display */  
  return;
}


/*---------hot_renormalize  ---------------------------------------------*/
void  hot_renormalize (HNDLE hDB, HNDLE hRn ,void *info  )
{
  /* key  has been touched; automatically updated by hotlink

 */
   
  cm_msg(MINFO,"hot_renormalize","hot key \"renormalize\" has been touched (value=%d)\n",is.input.renormalize );
  return ;
}


/* ----------------------------------------------------------------------------------------------------------- */


INT setup_hotlink(void)
{
  HNDLE hktmp;

  /* called from frontend_init */
  
  /* set a hot link on v680 odb key "display"  */

  status=db_find_key(hDB, hV, "display", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key for v680 display   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.display);
  status = db_open_record(hDB, hktmp, &vs.display, size, MODE_READ, hot_display, NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: v680/display  (%d)", status );
    return(status);
  }
  /* set a hot link on v680 odb key "display period (s)"  */
  status=db_find_key(hDB, hV, "display period (s)", &hktmp);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to find key for v680 display period (s)   (%d)", status );
    write_message1(status,"setup_hotlink");
    return (status);
  }   
  
  size = sizeof(vs.display_period__s_);
  status = db_open_record(hDB, hktmp, &vs.display_period__s_, size, MODE_READ, hot_display_period, NULL);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink","Failed to open record (hotlink) for key: v680/display period (s)  (%d)", status );
    return(status);
  }
  
}

/* ----------------------------------------------------------------------------------------------------------- */

INT setup_hotlink_BOR(void)
{
/* IMUSR hot links

   These should be opened by BOR and closed at EOR

*/
  HNDLE hStep;

  /* sweep step */
  status=db_find_key(hDB, hIS, "input/sweep step", &hStep);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key  \"input/sweep step \"   (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }   
  
  size = sizeof(is.input.sweep_step);
  status = db_open_record(hDB, hStep, &is.input.sweep_step, size, MODE_READ, hot_sweep_step , NULL);
  if (status != DB_SUCCESS)
  {
    hStep=0;
    cm_msg(MERROR,"setup_hotlink_BOR",
           "Failed to open record (hotlink) for key \"input/sweep step\"  (%d)", status );
    return(status);
  }
  printf("hStep=%d\n",hStep);
  
  /* sweep start */
  status=db_find_key(hDB, hIS, "input/sweep start", &hStart);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key \" input/sweep start \"   (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }   
  
  size = sizeof(is.input.sweep_start);
  status = db_open_record(hDB, hStart, &is.input.sweep_start, size, MODE_READ, hot_sweep_start , NULL);
  if (status != DB_SUCCESS)
  {
    hStart=0;
    cm_msg(MERROR,"setup_hotlink_BOR",
           "Failed to open record (hotlink) for key \"input/sweep start\"  (%d)", status );
    return(status);
  }
  
  /* sweep stop */
  status=db_find_key(hDB, hIS, "input/sweep stop", &hStop);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key: input/sweep stop   (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }
  size = sizeof(is.input.sweep_stop);
  status = db_open_record(hDB, hStop, &is.input.sweep_stop, size, MODE_READ, hot_sweep_stop , NULL);
  if (status != DB_SUCCESS)
  {
    hStop=0;
    cm_msg(MERROR,"setup_hotlink_BOR",
	   "Failed to open record (hotlink) for key \"input/sweep stop\"  (%d)", status );
    return(status);
  }

  
    /* tolerance */
  status=db_find_key(hDB, hIS, "input/tolerance (%)", &hTol);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key  \"input/tolerance (%)  \"  (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }   

  size = sizeof(is.input.tolerance____);
  status = db_open_record(hDB, hTol, &is.input.tolerance____, size, MODE_READ, hot_tolerance, NULL);
  if (status != DB_SUCCESS)
  {
    hTol=0;
    cm_msg(MERROR,"setup_hotlink_BOR",
           "Failed to open record (hotlink) for key \"input/tolerance (%)\"  (%d)", status );
    return(status);
  }

  /* out-of-tolerance delay */
  status=db_find_key(hDB, hIS, "input/out-of-tolerance delay (s)", &hTolD);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key \"input/out-of-tolerance delay (s)\"  (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }   
  
  size = sizeof(is.input.out_of_tolerance_delay__s_);
  status = db_open_record(hDB, hTolD, &is.input.out_of_tolerance_delay__s_, size, MODE_READ, hot_tol_delay , NULL);
  if (status != DB_SUCCESS)
  {
    hTolD=0;
    cm_msg(MERROR,"setup_hotlink_BOR",
           "Failed to open record (hotlink) for key \"input/out-of-tolerance delay (s)\"  (%d)", status );
    return(status);
  }
  
  /* renormalize */
  status=db_find_key(hDB, hIS, "input/renormalize", &hRn);
  if(status !=  DB_SUCCESS)
  {
    cm_msg(MERROR,"setup_hotlink_BOR","Failed to find key \"input/renormalize\"  (%d)", status );
    write_message1(status,"setup_hotlink_BOR");
    return (status);
  }  
  size = sizeof(is.input.renormalize);
  status = db_open_record(hDB, hRn, &is.input.renormalize, size, MODE_READ, hot_renormalize , NULL);
  if (status != DB_SUCCESS)
  {
    hRn=0;
    cm_msg(MERROR,"setup_hotlink_BOR",
           "Failed to open record (hotlink) for key \"input/renormalize\"  (%d)", status );
    return(status);
  }

}
/* ---------------------------------------------------------------------------------------------------- */

void  close_hotlinks()
{
  /* remove hot link on "sweep step" */
  HNDLE hStep;

  /* sweep step */
  status=db_find_key(hDB, hIS, "input/sweep step", &hStep);
  if(status !=  DB_SUCCESS)
    {
      cm_msg(MERROR,"close_hotlinks","Failed to find key  \"input/sweep step \"   (%d)", status );
      write_message1(status,"close_hotlinks");
      return ;
    }   
  if(debug)
      printf("close_hotlinks:hStep=%d\n",hStep);     

  if(hStep) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"sweep step\", hStep=%d\n",hStep);
    status = db_close_record(hDB, hStep);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for sweep step(%d)\n",status);

  }
  printf("after close rec, status=%d\n",status);


  /* remove hot link on "sweep stop" */
  if(hStop) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"sweep stop\", hStop=%d\n",hStop);
    status = db_close_record(hDB, hStop);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for sweep stop (%d)\n",status);
  }

    /* remove hot link on "sweep start" */
  if(hStart) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"sweep start\", hStart=%d\n",hStart);
    db_close_record(hDB, hStart);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for sweep start (%d)\n",status);
  }

    /* remove hot link on "tolerance" */
  if(hTol) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"tolerance\", hTol=%d\n",hTol);
    status = db_close_record(hDB, hTol);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for tolerance (%d)\n",status);

  }

    /* remove hot link on "out-of-tol delay" */
  if(hTolD) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"out-of-tol delay\", hTolD=%d\n",hTolD);
    status = db_close_record(hDB,hTolD);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for out-of-tol delay (%d)\n",status);    
  }

     /* remove hot link on "re-normalize" */
  if(hRn) 
  {
    if(debug)
      printf("close_hotlinks: closing record for \"re-normalize\", hRn=%d\n",hRn);
    status = db_close_record(hDB, hRn);
    if(status != DB_SUCCESS)
      cm_msg(MINFO,"close_hotlinks","Error closing hotlink for re-normalize (%d)\n",status);
  }
  
}



/*-- Check  musr_config has recently updated odb area ---------------------------------- ------*/
INT check_update_time(INT max_sec)
{
/*  Called by begin_of_run
          AFTER it has got the latest value of the v680 record !
	  
   Checks time since odb area 
   /equipment/musr_td_acq/settings/output/selected_histograms
   (and other areas)
         were last updated by musr_config

  Input: number of seconds above which check fails
                (musr_config must have run < max_sec seconds ago) 
         
  returns: success or fail status
         
       note: get the present binary time; the ascii time does not seem to be
             local time, but the binary time can be used
      */

  /* needed to get present time */
  time_t timbuf;
  char timbuf_ascii[30];
  INT elapsed_time;
  INT ntry;

  char str[132];
  INT size,status;
  HNDLE htmp;

  if(debug) printf ("check_update_time: starting with max_sec = %d \n",max_sec);

  
  if(max_sec <=0 )
  {
      cm_msg(MERROR,"check_update_time",
             "Invalid input parameter max_sec (%d) must be > 0",str);
      return (DB_INVALID_PARAM);
  }

/* get the (latest value) of flag "check musr config" */
  sprintf(str,"/Equipment/MUSR_TD_acq/v680/flags");
  /* find the key */
  status = db_find_key(hDB, 0, str, &htmp);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "check_update_time", "Cannot find key %s (%d) ",str,status);
    return FE_ERR_ODB;
  }
  size = sizeof(gbl_check);
  status = db_get_value(hDB, htmp, "check musr config" , &gbl_check, &size,
			TID_BOOL, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"check_update_time","cannot get value %s/check musr config (%d)",str,status);
    return(status);  /*  error return */
  }
  if(debug) printf ("check_update_time: check flag = %d \n",gbl_check);


  /* these contain  the ascii & binary time from odb : 
         vs.histograms.validated_at_time  (ascii)
         vs.histograms.binary_time        (binary)
  */
  ntry=0;
  while ( ntry < 2)
  {
      /* get the present time */
      time(&timbuf);
      if(debug)
      {
	  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) );         
	  printf ("Present time:  %s or (binary) %d \n",timbuf_ascii,timbuf);   
	  printf("According to odb, musr_config last updated parameters at:  %s, binary=%d\n",
		 vs.histograms.validated_at_time, vs.histograms.binary_time );
      }
      
      elapsed_time= (INT) ( timbuf -  vs.histograms.binary_time );
      if(debug)
	  printf("Time since file last updated: %d seconds, timbuf (%d)\n",
		 elapsed_time,timbuf);
      
      
      /* check elapsed time against maximum */
      if(elapsed_time <= max_sec)
	  break;
      
      if(!gbl_check)
	  break;
      
      
      printf("check_update_time: According to odb, musr_config last updated parameters at: %s",
	     vs.histograms.validated_at_time);
      printf("                 waiting 2s...\n");
      ss_sleep(2000);
      
      /* now reread the time values from odb */
      size = sizeof(vs);
      status = db_get_record(hDB, hV, &vs, &size, 0);
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR, "check_update_time", "cannot retrieve v680 record (size of vs=%d)",size);
	  return DB_NO_ACCESS;
      }
      ntry++;      
  }
  
/* check on musr_config running IS ENABLED :
   don't let the run start if
   file is not recent */
  if(gbl_check)
  {
      if(ntry>= 2)
      {
	  
	  cm_msg(MERROR,"check_update_time",
		 "According to odb, musr_config last updated parameters at: %s",
		 vs.histograms.validated_at_time);
	  cm_msg(MERROR,"check_update_time","Make sure musr_config is running\n");
	  return FE_ERR_HW;
      }
  }
  else            /* send an informational message because check flag is off */
      cm_msg(MINFO,"check_update_time",
	     "musr_config check flag is false; musr_config may not have updated input params\n");
  
  printf("check_update_time: returns after %d tries\n",ntry);
  return (SUCCESS);
}


/*-----------------------------------------------------------------------------------------------*/

INT create_records(void)

/*-----------------------------------------------------------------------------------------------*/
{
  char str_set[128];  
  MUSR_TD_ACQ_MDARC_STR (musr_td_acq_mdarc_str);
  
  printf("\n");
  printf("Create records starting\n");
  
  
  /* create record "/Equipment/MUSR_TD_acq/v680" to make sure it exists
   */
  sprintf(str_set,"/Equipment/%s/v680",td_eqp_name); 
  status = db_create_record(hDB, 0, str_set, strcomb(musr_td_acq_v680_str)); 
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"create_records","db_create_record fails for %s (%d)",str_set,status);
    return FE_ERR_ODB;
  }
  /* find the key hV */
  status = db_find_key(hDB, 0, str_set, &hV);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "create_records", "Cannot find key %s (%d) ",str_set,status);
    return FE_ERR_ODB;
  }
  /* Get the record for v680 area */
  size = sizeof(vs);
  status = db_get_record(hDB, hV, &vs, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "create_records", "cannot retrieve %s record; struct size=%d (%d)",
           str_set,size,status);
    return FE_ERR_ODB;
  }

  printf("Created record for v680\n");
    

  /* create record "/Equipment/MUSR_TD_acq/mdarc" to make sure it exists
     we need the parameter to enable/disable logging 
   */
  
  sprintf(str_set,"/Equipment/%s/mdarc",td_eqp_name); 
  status = db_create_record(hDB, 0, str_set, strcomb(musr_td_acq_mdarc_str)); 
  if (status != DB_SUCCESS)
  {
    if(status == DB_OPEN_RECORD)
    {
      cm_msg(MINFO,"create_records","not creating record for %s due to open records (imdarc may be running) (%d)",str_set,status);
    }
    else
    {
    cm_msg(MERROR,"create_records","db_create_record fails for %s (%d)",str_set,status);
    return FE_ERR_ODB;
    }
  }

  
  /* create record "/Equipment/MUSR_I_acq/Settings" to make sure it exists
   */
  sprintf(str_set,"/Equipment/%s/settings",imusr_eqp_name); 
  status = db_create_record(hDB, 0, str_set, strcomb(musr_i_acq_settings_str)); 
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"create_records","db_create_record fails for %s (%d)",str_set,status);
    return FE_ERR_ODB;
  }
  /* find the key hIS */
  status = db_find_key(hDB, 0, str_set, &hIS);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "create_records", "Cannot find key %s (%d) ",str_set,status);
    return FE_ERR_ODB;
  }
  printf("Created record for IMUSR settings area\n");

  /* Get the record for IMUSR/Settings area */
  size = sizeof(is);
  status = db_get_record(hDB, hIS, &is, &size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "create_records", "cannot retrieve %s record; struct size=%d (%d)",
           str_set,size,status);
    return FE_ERR_ODB;
  }

  printf("Got the  record for IMUSR settings area\n");


  /* create record /Equipment/info_odb/Variables to make sure it exists  */
  sprintf(str_out,"/Equipment/info_odb/Variables");
  status = db_create_record(hDB, 0, str_out, strcomb(info_odb_event_str));
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"create_records","db_create_record fails for %s (%d)",str_out,status);
    return FE_ERR_ODB;
  }
  
  /* find the key hOS */
  status = db_find_key(hDB, 0, str_out, &hOS);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "create_records", "Cannot find key %s (%d) ",str_out,status);
    return FE_ERR_ODB;
  }
  printf("Created record for info odb area\n");
  
   
   /* Create record "/Equipment/Scaler/Settings" to make sure it exists */
  sprintf(str_set, "/Equipment/Scaler/Settings");
  status = db_create_record(hDB, 0, str_set, strcomb(scaler_settings_str)); 

  if (status != DB_SUCCESS)
   { 
     cm_msg(MERROR,"create_records:","db_create_record fails %s status(%d)",str_set,status);
     return FE_ERR_ODB;
   }
  /* find the (global) key for scaler area */
  status = db_find_key(hDB, 0, str_set, &hSet);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR," create_records: ","could not find key %s (%d) ",str_set,status);
    return (FE_ERR_ODB);
  }
  
  return (DB_SUCCESS);
}


/*-------------------------------------------------------------------------------------------------------*/

INT validate_sweep_values(void)
{
  /* get the sweep values and check they are reasonable */
  
  printf("validate_sweep_values: sweep start=%f stop=%f step=%f \n",
	 is.input.sweep_start,is.input.sweep_stop,is.input.sweep_step);
  
  
  gbl_sweep_start = is.input.sweep_start;
  gbl_sweep_stop  = is.input.sweep_stop;
  gbl_sweep_step  = is.input.sweep_step; /* is.input.sweep_step is hotlinked */
  gbl_sweep_direction = 1;
  
  
  if( gbl_sweep_step == 0 )
  {
    cm_msg(MERROR,"validate_sweep_values","Invalid sweep increment value: %f",
	   gbl_sweep_step);
    return FE_ERR_HW;
  }
  
  if( ((gbl_sweep_step > 0) && (gbl_sweep_stop < gbl_sweep_start)) ||
      ((gbl_sweep_step < 0) && (gbl_sweep_start < gbl_sweep_stop)) )
    gbl_sweep_direction *= -1;
  
  
  /* make sure we have at least one step */
  if ( ( fabs(gbl_sweep_stop - gbl_sweep_start)/ fabs(gbl_sweep_step) )  < 1)
  {
    cm_msg(MERROR,"validate_sweep_values","invalid sweep step size (%f) ; must have at least one step",
	   gbl_sweep_step);
    return FE_ERR_HW;
  }



  return(SUCCESS);
}


/*-----------------------------------------------------------------------------------------------*/

void get_cvar_handle(void)

/*-----------------------------------------------------------------------------------------------*/
{
  /* set up a handle for manual trigger of cvar event 
     called from begin_of_run only if n_camp_logged > 0
  */

  
  /* Use the hard-coded clientname (fe_camp) */
  status = cm_exist(ClientName,TRUE);
  if (status == CM_SUCCESS)
    {
      if(debug)printf("get_cvar_handle: trying to connect to the client %s\n",ClientName);
      status = cm_connect_client (ClientName, &hconn);
      if(status != RPC_SUCCESS)
	{
	  cm_msg(MERROR,"get_cvar_handle","Cannot connect to client:  \"%s\" (%d)",ClientName,status);
	  cm_msg(MINFO,"get_cvar_handle","cannot log CAMP values\n");
	  hconn=-1;
	}
      else
	{
	  fecamp_exists=TRUE;
	  if(debug)
	    printf("get_cvar_handle: handle hconn = %d\n",hconn);
	}
    }
  else
    {
      fecamp_exists=FALSE; /* suppress messages at EOR */
      cm_msg(MINFO,"get_cvar_handle","could not find client fe_camp; cannot log CAMP values");
    }
  return;     
}


/*-----------------------------------------------------------------------------------------------*/

void close_cvar_handle(void)
     
/*-----------------------------------------------------------------------------------------------*/
{
  /* close the handle for manual trigger of cvar event 
     called from end_of_run only if n_camp_logged > 0
  */
  if(hconn != -1)
    {
      status =cm_disconnect_client(hconn, FALSE);
      if (status != CM_SUCCESS)
	cm_msg(MERROR,"close_cvar_handle","Error closing client handle %d (%d)",hconn,status);
      hconn=-1;
    }
  else
    {
      if (fecamp_exists)
	cm_msg(MINFO,"close_cvar_handle","invalid handle supplied; client fe_camp may not be running");
    }
}

/*-----------------------------------------------------------------------------------------------*/

BOOL trigger_cvar_event(INT * pRunstate)

/*-----------------------------------------------------------------------------------------------*/     
{
  /* hconn has be got at BOR by get_cvar_handle */
  
  BOOL event_triggered;
  
  event_triggered = FALSE;

  rstate=get_run_state();
  if(rstate==-99)
    {
      cm_msg(MERROR,"trigger_cvar_event","cannot access run state, odb may be locked; cvar event not triggered");
      return(event_triggered);
    }
  *pRunstate = rstate;

  if (rstate == STATE_RUNNING)
    {
      if(hconn != -1)
	{
	  rpc_client_call(hconn, RPC_MANUAL_TRIG, 13); /* trigger an event */
	  if (status != CM_SUCCESS)
	    {
	      cm_msg(MERROR,"trigger_cvar_event","Error triggering event from fe_camp, hconn=%d (%d)",hconn,status);
	      /* check if client has died  */
	      status = cm_exist(ClientName,TRUE);
	      if (status != CM_SUCCESS)
		{
		  printf("trigger_cvar_event: client %s is no longer present; camp logging no longer available\n");
		  close_cvar_handle();
		  fecamp_exists=FALSE;
		}
	    }
	  else
	    {  /* successfully triggered event */
	      if(debug)printf("trigger_cvar_event: success from rpc_client_call to trigger event\n");
	      event_triggered=TRUE;
	    }
	  
	} /* end of hconn != 1 */
      else
	{
	  if (fecamp_exists)
	    if(debug)printf("trigger_cvar_event: invalid handle, event not triggered\n");
	}
    } /* end of if running */
  return(event_triggered);
}


/*-----------------------------------------------------------------------------------------------*/
INT check_input_toggle_states(void)
/*-----------------------------------------------------------------------------------------------*/

/* called only if musr_config is not running - it checks this stuff */
{

  /*
    I N N E R     T O G G L E
  */
  if( gbl_num_inner_toggles <= 0)
  {
    if ( strcmp("NONE",gbl_inner_toggle_type) != 0)
    {
      /* musr_config (if running) should have fixed this previously and updated the odb */
      cm_msg(MINFO,"check_input_toggle_states","Inner toggle mode changed to \"NONE\" since num toggles <= 0");
      sprintf(gbl_inner_toggle_type,"NONE");

      sprintf( is.input.inner_toggle_type,"NONE");      
      size = sizeof (is.input.inner_toggle_type);
      status=db_set_value (hDB, hIS, "input/inner toggle type", &is.input.inner_toggle_type, size, 1, TID_STRING);
      if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"check_input_toggle_states","could not write value=%s to \"input/inner_toggle_type\" (%d) ",
	       is.input.inner_toggle_type,status);
	return FE_ERR_ODB;
      }
    }
  }
  if (strcmp("NONE",gbl_inner_toggle_type) == 0)
  {
    if(gbl_num_inner_toggles != 0)
    {
      /* musr_config (if running) should have fixed this previously and updated the odb */
      gbl_num_inner_toggles = 0; /* set no. toggles to 0 - no toggling */
      cm_msg(MINFO,"check_input_toggle_states","Number of Inner toggle cycles set to 0 since toggle type is \"NONE\" ");
    
      is.input.num_inner_toggle_cycles = 0;
      size = sizeof ( is.input.num_inner_toggle_cycles);
      status=db_set_value (hDB, hIS, "input/num inner toggle cycles", &is.input.num_inner_toggle_cycles, size, 1, TID_INT);
      if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"check_input_toggle_states","could not write value=%d to \"input/num inner toggle cycles\" (%d) ",
	       is.input.num_inner_toggle_cycles,status);
	return FE_ERR_ODB;
      } 
    } 
    printf("Inner toggle type = NONE  number of toggle cycles = %d, initial toggle state = %d\n",
	   gbl_num_inner_toggles,gbl_inner_toggle_state);
  }
  
  else if (strcmp("HARD",gbl_inner_toggle_type) == 0)
    printf("Inner toggle type = HARD  number of toggle cycles = %d, initial toggle state = %d\n",
	   gbl_num_inner_toggles,gbl_inner_toggle_state);
  
  
  else if( strcmp("SOFT",gbl_inner_toggle_type) == 0) 
  {
    printf("check_input_toggle_states: Inner toggle type = SOFT ;offset=%f; num toggle cycles = %d; initial toggle state = %d\n",
	   is.input.inner_toggle_value, gbl_num_inner_toggles,gbl_inner_toggle_state );
    status=set_sweep_dev (is.input.sweep_device, gbl_sweep_value + is.input.inner_toggle_value ); /* do not wait step_settletime */
    if(status != SUCCESS)
      { /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	return status;
      }    gbl_actual_SD_value = gbl_sweep_value + is.input.inner_toggle_value; 
    /*printf("check_input_toggle_states: SOFT toggle -> set SD to %f\n",gbl_actual_SD_value); */
    
  }
  else if  (strcmp("REF",gbl_inner_toggle_type) == 0)
    printf("check_input_toggle_states: Inner toggle type = REF ;offset=%f; num toggle cycles = %d; initial toggle state = %d\n",
	   is.input.inner_toggle_value, gbl_num_inner_toggles,gbl_inner_toggle_state );
  
  
  else
  {
    cm_msg(MERROR,"check_input_toggle_states","Invalid Inner toggle type supplied (%s)\n",gbl_inner_toggle_type);
    return(FE_ERR_HW);
  }
  
  

  /*
        O U T E R     T O G G L E
  */
  if( gbl_num_outer_toggles <= 0)
  {
    if ( strcmp("NONE",gbl_outer_toggle_type) != 0)
    {
      /* musr_config (if running) should have fixed this previously and updated the odb */
      cm_msg(MINFO,"check_input_toggle_states","Outer toggle mode changed to \"NONE\" since num toggles <= 0");
      sprintf(gbl_outer_toggle_type,"NONE");
      
      sprintf( is.input.outer_toggle_type,"NONE");      
      size = sizeof (is.input.outer_toggle_type);
      status=db_set_value (hDB, hIS, "input/outer toggle type", &is.input.outer_toggle_type, size, 1, TID_STRING);
      if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"check_input_toggle_states","could not write value=%s to \"input/outer_toggle_type\" (%d) ",
	       is.input.outer_toggle_type,status);
	return FE_ERR_ODB;
      }      
    }
  }

  /* check for inner =NONE and outer != NONE - this is not allowed */
  if ( (strcmp("NONE",gbl_inner_toggle_type) == 0) && (strcmp("NONE",gbl_outer_toggle_type) != 0) )
  {
    cm_msg(MERROR,"check_input_toggle_states","cannot have an outer toggle set (%s)  without also having an inner toggle set",
	   gbl_outer_toggle_type);
    return FE_ERR_ODB;
  }
  
  
  if (strcmp("NONE",gbl_outer_toggle_type) == 0)
  {
    if(gbl_num_outer_toggles != 0)
    {
      /* write also to odb - musr_config (if running) should have fixed this previously */
      gbl_num_outer_toggles = 0; /* set no. toggles to 0 - no toggling */
      cm_msg(MINFO,"check_input_toggle_states","Number of Outer toggle cycles set to 0 since toggle type is \"NONE\" ");
      
      is.input.num_outer_toggle_cycles = 0;
      size = sizeof ( is.input.num_outer_toggle_cycles);
      status=db_set_value (hDB, hIS, "input/num outer toggle cycles", &is.input.num_outer_toggle_cycles, size, 1, TID_INT);
      if (status != DB_SUCCESS)
      {
	cm_msg(MERROR,"check_input_toggle_states","could not write value=%d to \"input/num outer toggle cycles\" (%d) ",
	       is.input.num_outer_toggle_cycles,status);
	return FE_ERR_ODB;
      }
    } 
    gbl_num_outer_toggles =0; 
    printf("Outer toggle type = NONE  number of toggle cycles = %d, initial toggle state = %d\n",
	   gbl_num_outer_toggles,gbl_outer_toggle_state);
  }
  
  else if (strcmp("HARD",gbl_outer_toggle_type) == 0)
    printf("Outer toggle type = HARD, number of toggle cycles = %d, initial toggle state = %d\n",
	   gbl_num_outer_toggles,gbl_outer_toggle_state);
  
  
  else if (strcmp("SOFT",gbl_outer_toggle_type) == 0) 
  {
    printf("check_input_toggle_states: Outer toggle type = SOFT ;offset=%f; num toggle cycles = %d; initial toggle state = %d\n",
	   is.input.outer_toggle_value, gbl_num_outer_toggles,gbl_outer_toggle_state );
    status=set_sweep_dev (is.input.sweep_device, gbl_sweep_value + is.input.outer_toggle_value ); /* do not wait step_settletime */
    if(status != SUCCESS)
      { /* set_sweep_device retries before giving up & setting flag for mdarc to stop the run */
	cm_msg(MERROR,"sweep_cycle_end","failure setting sweep device to %f\n",gbl_sweep_value);
	return status;
      }    gbl_actual_SD_value = gbl_sweep_value + is.input.outer_toggle_value; 
    /*printf("check_input_toggle_states: SOFT toggle -> set SD to %f\n",gbl_actual_SD_value); */
    
  }
  else if  (strcmp("REF",gbl_outer_toggle_type) == 0)
    printf("check_input_toggle_states: Outer toggle type = REF ;ref. val=%f; num toggle cycles = %d; initial toggle state = %d\n",
	   is.input.outer_toggle_value, gbl_num_outer_toggles,gbl_outer_toggle_state );
  
  else
  {
    cm_msg(MERROR,"check_input_toggle_states","Invalid Outer toggle type supplied (%s)\n",gbl_outer_toggle_type);
    return(FE_ERR_HW);
  }
  return (SUCCESS);
}


/*-display update ------------------------------------------------------*/
void imusr_update(void)
{
  INT j,nf;
  INT temp;
  float ftmp;
  INT len,ival;
  
  /* get current run state */
  rstate=get_run_state();
  if(rstate==-99)
    {
      cm_msg(MERROR,"imusr_update","cannot access /runinfo/State , odb may be locked");
      return;
    }

  if (disp_offset)     /*   extra scaler channels present so need extra lines for display */
  {
    for (j=0; j<10; j++) 
    {
      ss_printf(4+12*(j), LINE_6,"%d      ",scaler_counts[j]);
      ss_printf(4+12*(j), LINE_7,"%g      ",gbl_scaler_sums[j]);
    }
    for (j=10; j<gbl_num_virtual_scaler_chans; j++)
    {
      ss_printf(28+12*(j-10), LINE_6+disp_offset,"%d      ",scaler_counts[j]);
      ss_printf(28+12*(j-10), LINE_7+disp_offset,"%g      ",gbl_scaler_sums[j]);
    }
  }
  else
  {
    for (j=0; j<gbl_num_virtual_scaler_chans; j++) 
    {
      ss_printf(4+12*(j), LINE_6,"%d      ",scaler_counts[j]);
      ss_printf(4+12*(j), LINE_7,"%g      ",gbl_scaler_sums[j]);
    }
  }

  ss_printf( 0,LINE_10+disp_offset,"%5d ",gbl_sweep_counter );
  ss_printf( 8,LINE_10+disp_offset," %5d ",gbl_inner_toggle_counter);   
  ss_printf( 16,LINE_10+disp_offset," %5d ",gbl_outer_toggle_counter);
  ss_printf( 24,LINE_10+disp_offset," %5d ",gbl_preset_counter);
  ss_printf( 32,LINE_10+disp_offset," %5d ",gbl_data_point_counter); /* current data point */
  ss_printf( 40,LINE_10+disp_offset," %5d ", os.total_data_points);   /* # data points sent out */

  ss_printf( 46,LINE_10+disp_offset," %5d ",os.num_camp_triggered);
  
  ss_printf( 61,LINE_10+disp_offset,"%2d ",gbl_sweep_direction);
  ss_printf( 66,LINE_10+disp_offset," %g    ", gbl_sweep_value);
  ss_printf( 74,LINE_10+disp_offset," %g    ", gbl_actual_SD_value);
  if(gbl_inner_toggle_state == tog_off) 
    ss_printf( 84,LINE_10+disp_offset," - ");
  else
    ss_printf( 84,LINE_10+disp_offset," + ");
  if(gbl_outer_toggle_state == tog_off) 
    ss_printf( 88,LINE_10+disp_offset," - ");
  else
    ss_printf( 88,LINE_10+disp_offset," + ");

/* Flags: */ 
   ss_printf( 7,LINE_13+disp_offset," %1.1d ",gbl_hold_flag);	    
   ss_printf(17,LINE_13+disp_offset,"%1.1d ",  gbl_out_of_tolerance_flag);
   ss_printf(24,LINE_13+disp_offset,"%1.1d ",gbl_active_flag );
    nf=0;
   if(gbl_ticks0==0 ||  is.input.renormalize) nf=1;
   ss_printf(37,LINE_13+disp_offset,"%1.1d ", nf);
   ss_printf(45,LINE_13+disp_offset," %1.1d ",gbl_in_sweep );
   ss_printf(55,LINE_13+disp_offset,"%1.1d ", gbl_in_preset);
   ss_printf(65,LINE_13+disp_offset,"%1.1d ", gbl_in_inner_toggle);
   ss_printf(75,LINE_13+disp_offset,"%1.1d ", gbl_in_outer_toggle);
   ss_printf(84,LINE_13+disp_offset,"%1.1d ", gbl_pflag);
  


   /* Timers:  */
   ss_printf(12,LINE_16+disp_offset,"%d    ", gbl_poll_time); /* time spent polling */     
   ss_printf(23,LINE_16+disp_offset,"%d    ", gbl_acq_cycle_time);   
   ss_printf(35,LINE_16+disp_offset,"%d    ", gbl_norm_time );
   ss_printf(45,LINE_16+disp_offset,"%6.1f  ", os.total_paused_time__s_);   
   ss_printf(56,LINE_16+disp_offset,"%6.1f  ", os.acquire_active_time__s_);
   ss_printf(69,LINE_16+disp_offset,"%6.1f  ",os.total_run_time__s_);
   ss_printf(84,LINE_16+disp_offset,"%d  ",gbl_send_time); /* TEMP */




   /* Counters: */  
   ss_printf(6,LINE_19+disp_offset,"%-4.4d  ", gbl_poll_counter);
   ss_printf(13,LINE_19+disp_offset,"%-5.5d  ", gbl_new_ticks );
   ss_printf(22,LINE_19+disp_offset,"%-5.5d ", gbl_read_preset_value );   
   ss_printf(29,LINE_19+disp_offset,"%-5.5d ", gbl_total_rate );
   /* values saved by imusr_acq */
   ss_printf(37,LINE_19+disp_offset,"  %s   ",display_last);

   ss_printf(66,LINE_19+disp_offset,"%5.5d   ", gbl_ticks0);
   ss_printf(72,LINE_19+disp_offset,"%5.5d      ", gbl_TotR0);



   ss_printf(79,LINE_19+disp_offset,"%4d     ", gbl_intol_counter);
   ss_printf(83,LINE_19+disp_offset,"%4d     ",   gbl_outol_counter);

   ss_printf(88,LINE_19+disp_offset,"%4d    ",os.timeout_counter);
   ival=92; /* start of status message */
   ss_printf(ival,LINE_19+disp_offset,"%s in tol",str_status[rstate]);
   
   if(rstate == STATE_STOPPED)
     ss_printf(ival,LINE_19+disp_offset,"%s              ",str_status[rstate]);
   else
   {
     if(nf)
       ss_printf(ival,LINE_19+disp_offset,"%s normalizing",str_status[rstate]);
     else if (BOR_paused)
       {
	ss_printf(ival,LINE_19+disp_offset,"Waiting for PAUSE/RESUME",str_status[rstate]); 
       }
     else
     {
       if(!gbl_out_of_tolerance_flag)
	 ss_printf(ival,LINE_19+disp_offset,"%s in tol     ",str_status[rstate]);     
       else
	 ss_printf(ival,LINE_19+disp_offset,"%s out of tol ",str_status[rstate]); 
     }
   }

   len = strlen(display_string);
  if (len > 0 )
  {
    if(display_len > len) /* was previous message longer? */
      {  /* pad with blanks to wipe out all of previous message */
	padString( display_string,' ',display_len );
      }
    ss_printf(15, LINE_21+disp_offset,"%s",display_string);
    display_len = len; /* remember length for next time */
    sprintf(display_string,"");  /* clear string */
  }

  
  /* these are written sometimes by program */
  ss_printf(0, LINE_23+disp_offset,"                                                                                                    \n"); 
      
  
}

/*-display on screen ---------------------------------------------------*/
void imusr_display(int onoff)
{
  printf("imusr_display starting with onoff=%d\n",onoff);
  if (onoff >= 0)
  {
    if (onoff)
      v680_Display = onoff;
    else if (onoff == 0)
      v680_Display = onoff;
  }
  /* if(debug) */
  printf("imusr_display: v680_Display = %d\n",v680_Display);
  ss_clear_screen();
  /* take these out to speed up the display
     ss_printf( 0, LINE_6,"                                                                                ");
     ss_printf( 0, LINE_7,"                                                                                ");
     ss_printf( 0, LINE_6+disp_offset,"                                                                                ");
     ss_printf( 0, LINE_7+disp_offset,"                                                                                ");
     ss_printf( 0,LINE_10+disp_offset,"                                                                                ");
     ss_printf( 0,LINE_13+disp_offset,"                                                                                ");
  */
  if(onoff==0)imusr_update();  /* otherwise imusr_update is called after this routine in frontend_loop */
  ss_printf( 1, LINE_4,"%d " ,fs.num_inputs);
  ss_printf( 5, LINE_4,"%d " , gbl_num_virtual_scaler_chans);
  ss_printf(8, LINE_4,"%7d" , is.hardware.gate_generator.down_counter_preset );
  ss_printf(19, LINE_4,"%d  ",gbl_num_inner_toggles);
  ss_printf(22, LINE_4,"%s  ",gbl_inner_toggle_type);
  ss_printf(29, LINE_4,"%d  ",gbl_num_outer_toggles);
  ss_printf(33, LINE_4,"%s  ",gbl_outer_toggle_type);

  ss_printf(40, LINE_4,"%d  ",is.input.num_presets);
  if(gbl_fast_mod)
    ss_printf(48, LINE_4,"Y");
  else
    ss_printf(48, LINE_4,"N");
  
/*  ss_printf(48, LINE_4,"%d",is.input.num_sweeps); #sweep cycles - presently 0 */
  ss_printf(54, LINE_4,"%g ",gbl_sweep_start);
  ss_printf(63, LINE_4,"%g ",gbl_sweep_stop);
  ss_printf(68, LINE_4,"%g ",gbl_sweep_step);
  ss_printf(76, LINE_4,"%s",is.input.sweep_device);

  ss_printf(82, LINE_4,"%s ",disp_units);
  ss_printf(88, LINE_4,"%1.1d", gbl_DAC_range);

  /* determined at frontend_init */
  ss_printf( 94,LINE_4,"%d    ",gbl_poll_test_time); /* time to execute polling loop (when test=TRUE) */   


  ss_printf( 0, LINE_1,"= v%.2f ============================ IMUSR Display ========================================================",This_Version);
  ss_printf( 0, LINE_2,"#Scalers   GGL     InnTog    OutTog   #Preset Fmod    Sweep Sweep Sweep     SweepDevice   Poll for");
  ss_printf( 0, LINE_3,"Real Virt Preset #Cyc Type #Cyc Type  Cycles          Start  Stop  Step   Name Units Range   (ms)");
  ss_printf( 0, LINE_4_5,"--------------------------------------------------------------------------------------------------------------------");
  ss_printf( 5, LINE_4_75,"Key: e.g F-+- : Front counts with  OuterTogState=OFF; InnerTogState=ON;   FastModeState=OFF;  PC=counts/preset");
  ss_printf( 0, LINE_5, "    Clock       Total       F---        F-+-        F+--        F++-        B---        B-+-        B+--        B++-");
  if (disp_offset)
  {
     ss_printf(0, LINE_5+disp_offset,
	                "                            F--+        F-++        F+-+        F+++        B--+        B-++        B+-+        B+++ ");
    ss_printf( 0, LINE_6+disp_offset,
	                  "PC :");
    ss_printf( 0, LINE_7+disp_offset,"Sum:");
  }
  ss_printf( 0, LINE_6,"PC :");
  ss_printf( 0, LINE_7,"Sum:");
  

   
  ss_printf( 0, LINE_8+disp_offset,"-----------------------------------------------------------------------------------------------------------");
  ss_printf( 0, LINE_9+disp_offset,  "Sweep#  InTog# OutTog# Preset# DataPoint# Num_events_sent:  Sweep  Sweep_Value:   InTog OutTog");
  ss_printf( 0, LINE_9_5+disp_offset,"                                            Data   Camp     Dir.  Nominal Actual  State State ");
  ss_printf( 0,LINE_11+disp_offset,"------------------------------------------------------------------------------------------------------");
  ss_printf( 0,LINE_12+disp_offset,"Flags: Hold   OutTol  Active   Normalize InSweep InPreset InInnerTog InOuterTog Pflag  ");
  ss_printf( 0,LINE_14+disp_offset,"-----------------------------------------------------------------------------------------");
  ss_printf( 0,LINE_15+disp_offset,"Timers: PollTime(ms) AcqCycle(ms) NormTime(ms) Paused(s) Acquiring(s) Running(s) SendTime(ms)");
  ss_printf( 0,LINE_17+disp_offset,"---------------------------------------------------------------------------------------");


  ss_printf( 0,LINE_18+disp_offset,  "Cntrs:      C U R R E N T          |  L A S T  C O M P L E T E  | Normalized  | In  Out Time State");
  ss_printf( 0,LINE_18_5+disp_offset,"      Poll#  NewTik  Preset  TotR  | Poll#  NewTik Preset TotR  | Clock  TotR | Tol Tol Outs "); 
  ss_printf( 0,LINE_20+disp_offset,"-------------------------------------------------------------------------------------------------------------");

  
  ss_printf( 0,LINE_21+disp_offset,"Last Message:                                                                                              ");
  last_message = LINE_21;  /* remember last message line for V680.h */
  ss_printf( 0,LINE_22+disp_offset,"==========================================================================================================\n");

}



/* ------------------------------------------------------------------*/
INT camp_update_params(void)
{
  char str[128];
  INT size,status,len,j;
  char hostname[128];
  char cstr[79];
  char string[30];

  if(dc)printf("camp_update_params is starting\n");
  strcpy(string,is.input.sweep_device);
  len=strlen(string);
  for(j=0; j<len; j++)
    string[j] = toupper (string[j]); /* convert to upper case */

  if(  strncmp(string, "CAMP",4 ) != 0)
  {
      cm_msg(MINFO,"camp_update_params","Error: sweep device (\"%s\")is not \"camp\"",
	     is.input.sweep_device);
      return(DB_INVALID_PARAM);
  }


  /* we assume that mui interfaces has checked the parameters already and initialized the device */
  strncpy(camp_params.SweepDevice, is.hardware.sweep_device.scan_title,20);
  
  sprintf(cstr,"/%s", is.hardware.sweep_device.camp_instrument_name); 
  /* sprintf(cstr,"/alarmDacNIM"); */
  strncpy(camp_params.InsPath, cstr,80);
  /* InsType,IfMod. DevDepPath not supplied */

  strncpy(camp_params.setPath, is.hardware.sweep_device.camp_scan_path,128);
  strncpy(camp_params.units, is.hardware.sweep_device.scan_var_units,20); 
  camp_params.maximum_value = is.hardware.sweep_device.maximum;
  camp_params.minimum_value = is.hardware.sweep_device.minimum;
  camp_params.conversion_factor = is.hardware.sweep_device.setting_divide_by ;
  gbl_conversion_factor = camp_params.conversion_factor ;
  strncpy(disp_units,camp_params.units,1); /* single letter units for display */
  toupper(disp_units[0]);
  printf("units for display = %s\n");

  /* Get the camp hostname from the mdarc area of odb  */
  size = sizeof(hostname); 
  sprintf(str,"/Equipment/%s/mdarc/camp/camp hostname",td_eqp_name); 
  status = db_get_value(hDB, 0, str, hostname, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"camp_update_params","cannot get Camp hostname at %s (%d)",str,status);
    return FE_ERR_ODB;
  }


  printf("Hostname: %s\n",hostname);
  strncpy(camp_params.serverName, hostname,LEN_NODENAME);
  if(dc)
    {
      printf("camp parameter settings:\n");
      printf("camp hostname: %s\n",camp_params.serverName);
      printf("sweep device: %s\n",camp_params.SweepDevice);
      printf("InsPath: %s\n",camp_params.InsPath);
      printf("setPath: %s\n",camp_params.setPath);
      printf("units: %s\n",camp_params.units);
      printf("maximum: %f\n",camp_params.maximum_value);
      printf("minimum: %f\n",camp_params.minimum_value);
      printf("conversion factor: %d\n",camp_params.conversion_factor);
    }
  return(SUCCESS);

}

INT get_run_state(void)
{
  INT run_state;


  /* get current run state */
  size = sizeof(run_state);
  /* get current run state */
  status = db_get_value(hDB, 0, "/runinfo/State", &run_state, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      printf("get_run_state:cannot access /runinfo/State (%d)... retrying after 500ms\n",status); 
      ss_sleep(500); /* wait for a moment */
      status = db_get_value(hDB, 0, "/runinfo/State", &run_state, &size, TID_INT, FALSE);
      if(status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_run_state","cannot access /runinfo/State (%d)",status);
	  return -99;
	}
    }
  return run_state;
}


INT camp_reconnect(void)
{
  char *msg;
  status = camp_clntEnd(); 
  if(status != CAMP_SUCCESS) 
    {
      msg = camp_getMsg();
      if( *msg != '\0' ) 
	{
	  printf("camp_reconnect: failure from camp_clntEnd\n");
	  printf( "CAMP error msg is \"%s\"\n", msg );
	  cm_msg(MERROR,"camp_reconnect","Failure from camp_clntEnd. Camp error message:\"%s\"",msg);
	}
    }
  else
    printf("camp_reconnect: success from camp_clntEnd\n");

  status = initCamp(&gotCamp);
  printf("camp_reconnect: after initCamp, gotCamp=%d\n",gotCamp);
  if(status != SUCCESS)
    printf("camp_reconnect: could not reconnect to camp");
  return (status);
}

INT camp_watchdog(void)
{
  /* Access camp to keep connection alive */
  char *msg;

  status = campSrv_cmd("sysGetLogActs");
  if(status != CAMP_SUCCESS) 
    {
      camp_errcount++;
      msg = camp_getMsg();
      if( *msg != '\0' ) 
	{
	  printf("camp_watchdog: failure from campSrv_cmd\n");
	  printf( "CAMP error msg is \"%s\"\n", msg );
	  cm_msg(MERROR,"camp_watchdog","Failure from campSrv_cmd; camp error message:\"%s\"",msg);
	}
    }
  return (status);
}


/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/<eqp_name>/client flags/febnmr to indicate to mdarc that is should stop the run
     set the flag  "/equipment/<eqp_name>/client flags/client alarm" to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in mdarc_subs.c for use of linux clients;  later can try to just have 
     one routine with ifdefs

  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status,size;
  if (di) printf("set_client_flag: starting\n");

  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",td_eqp_name,client_name );
  if(di)printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 

  /* Set the client flag to my_value 
                 value =  TRUE (success)  or FALSE (failure) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }

  /* Set the alarm flag; TRUE - alarm should go off, FALSE alarm stays off  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",td_eqp_name );
  if(di)printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }

  return CM_SUCCESS ;
}
