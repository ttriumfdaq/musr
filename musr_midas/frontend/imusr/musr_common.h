/* common parameters between imusr and musr 

  $Log: musr_common.h,v $
  Revision 1.1  2003/09/26 20:39:52  suz
  original to cvs

*/
HNDLE hDB,hV,hSet;
//MUSR_TD_ACQ_V680_STR (musr_td_acq_v680_str);
MUSR_TD_ACQ_V680 vs;
SCALER_SETTINGS_STR(scaler_settings_str);
SCALER_SETTINGS fs;
DIAG_SETTINGS_STR(diag_settings_str);

#ifdef IMUSR
/* IMUSR only */
HNDLE hIS,hOS;
MUSR_I_ACQ_SETTINGS_STR (musr_i_acq_settings_str);
MUSR_I_ACQ_SETTINGS is;    
INFO_ODB_EVENT_STR(info_odb_event_str);
INFO_ODB_EVENT os;
#endif

INT size;
INT status; /* status  */

BOOL I_MUSR=FALSE; /* TRUE if I_MUSR is running */
BOOL TD_MUSR=FALSE; /* TRUE if TD_MUSR is running */

INT disp_offset=0; 
INT v680_Display=0 ; /* now an odb parameter */
INT last_message = 0;

INT debug=0; /* general debug */
INT ddd=0; /* debug for event building (except scaler) */
INT dss=0; /* debug for scaler event building */
INT  dd=0; /* debug for frontend loop  etc */
INT di=0;
INT dii=1; /* debug for imusr */
