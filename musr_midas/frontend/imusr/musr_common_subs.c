/* musr_common_subs.c

  Common subroutines between IMUSR and TDMUSR to be included
    - these were in V680.c

    this file of common routines is included into femusr.c (or later fev680.c )

    $Log: musr_common_subs.c,v $
    Revision 1.1  2003/09/26 20:45:00  suz
    original to cvs

*/

void set_v680_display(INT display_enabled, INT display_offset, INT start_line)
{
  /* offset needed if more than 8 channels are enabled
     start_line indicates "last message:" line  */
  disp_offset = display_offset;
  v680_Display =  display_enabled;
  last_message   =  start_line;
  /* printf("set_v680_display: v680_Display=%d offset=%d last_message=%d\n",
     v680_Display,disp_offset,last_message); */
}

INLINE DWORD GetMaxFree(void)
{
  DWORD ival=0;
  /* return the maximum free memory space */
#ifdef OS_VXWORKS
  ival=memFindMax();
#endif
  return (ival);
}


/*--  find whether running I or TD MUSR ----------------------------------------*/
INT get_musr_type(void)
{
  char musr_type[10];
  char str[128];
  int j,len;
  
  I_MUSR=TD_MUSR=FALSE; /* global flags */

  if(di)printf("get_musr_type starting\n");
  
  sprintf(str,"/experiment/edit on start/musr type");
  size = sizeof(musr_type);
  status = db_get_value(hDB, 0, str,
                        &musr_type , &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"get_musr_type","cannot access key %s (%d)",
           str,status);
    write_message1(status,"running_IMUSR");
    return status;
  }

  len=strlen(musr_type);
  for(j=0; j<len; j++)
    musr_type[j] = toupper (musr_type[j]); /* convert to upper case */

  if(di)printf("len:%d, string %s\n",len,musr_type);

  if (strchr(musr_type,'I'))
    I_MUSR=TRUE;
  if (strchr(musr_type,'T') )
    TD_MUSR=TRUE;
  
  if ( (I_MUSR == FALSE && TD_MUSR == FALSE) ||
       (I_MUSR == TRUE  && TD_MUSR == TRUE) )
  {
    cm_msg(MERROR,"get_musr_type","Bad value of key %s (%s). Must contain \"I\" or \"TD\" ",
           str,musr_type);

    return(DB_INVALID_PARAM);
  }
  return status;
}


/*--------- write_message1 ---------------------------------------------*/  
void
write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (
    status == DB_NO_KEY) sprintf (str,"because   key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}

/*----ggl_init() -----------------------------------------------*/
INT ggl_init (void) 
{
  INT data;

/* called at begin run
   Initialize gate generator logic board and load initial values 
 */

#ifdef OS_VXWORKS
#ifdef TDMUSR
  printf("ggl_init: loading delays delta=%d ns,delta1=%d ns ,delta2=%d ns\n",
	 delta,delta1,delta1 );
  
  ns = gglWriteDelta_ns(ggl_base, delta);
  if(ddd)printf("Wrote %d ns to Delta delay register; read back %d\n",delta,ns);
  ns = gglWriteDelta1_ns(ggl_base, delta1);
  if(ddd)printf("Wrote %d ns to Delta delay register; read back %d\n",delta1,ns);
  ns = gglWriteDelta2_ns(ggl_base, delta2);
  if(ddd)printf("Wrote %d ns to Delta delay register; read back %d\n",delta2,ns);  
  
#else /* I_MUSR */  
    data = gglWritePreset (ggl_base, is.hardware.gate_generator.down_counter_preset);
    printf("Set Preset Down Counter to %d ; read back %d \n",
		  is.hardware.gate_generator.down_counter_preset,data);

    /* Set up pulser according to odb parameters */
  data = gglWritePulserEnable(ggl_base, is.hardware.gate_generator.pulser_enable);
  if (di) printf("Wrote %d to  enable/disable pulser, read back %d\n",data);
  
  if(di)printf("\nSetting pulser HI time to %d ms and LO time to %d ms\n",
		is.hardware.gate_generator.pulser_high__counts_,is.hardware.gate_generator.pulser_low__counts_);
  data = gglWritePulserHigh(ggl_base, is.hardware.gate_generator.pulser_high__counts_ );

  if(di) 
    printf("Wrote pulser High counts = %d to pulser, read back %d\n",
	   is.hardware.gate_generator.pulser_high__counts_,data);

  data = gglWritePulserLow(ggl_base, is.hardware.gate_generator.pulser_low__counts_ );
  if(di)
    printf("Wrote pulser High counts = %d to pulser, read back %d\n",
	   is.hardware.gate_generator.pulser_low__counts_,data);

    
  gglReadAll(ggl_base);
#endif /* IMUSR */
  
#endif /* OS_VXWORKS */
    
    return(status);
}

/*--------------------------------------------------------------------------------------------*/
void scaler_setup_test(void)
{

/*
 * Check for scaler Test modes:
 */
  
  if( !fs.enabled)
  {
    printf("scaler_setup_test: scaler is disabled\n");
    return;
  }
    
/*  Single pulse into all enabled channels
 * ------------------------------------------
 *   sis3803_test_mode_enable   enable test mode,
 *   sis3803_test_pulse         successive writes generate single pulses
 *
 *    NOTE : 25MHz pulser disabled */
  if (vs.flags.test_pulses)
  {
    printf("scaler_setup_test: test pulse mode detected - scaler will increment each readout\n");
#ifdef OS_VXWORKS  
    sis3803_test_mode_enable(SIS_base_adr);   /* enable test mode */
#endif
    return;
  }
  
  
/* 25MHz reference pulses go into all enabled channels
 * ------------------------------------------------------
 *    sis3803_test_mode_enable   enable test mode
 *    sis3803_25MHz_enable       enable 25MHz pulser
 */
  if (vs.flags._5mhz_pulses)
  {      
    printf("scaler_setup_test: 25MHz pulse mode detected - scaler channels will count up\n");
#ifdef OS_VXWORKS
    sis3803_test_mode_enable(SIS_base_adr);   /* enable test mode */
    sis3803_25MHz_enable(SIS_base_adr);       /* enable 25MHz pulser */
#endif
    return;
  }
  
/* Reference pulse into first channel (0) (in NORMAL mode)
 * ------------------------------------------------
 *   sis3803_25MHz_enable       enable 25MHz ref pulser
 *   sis3803_ref1               enable Ref 1
 *
 *    Note: test mode disabled
 */
  if (vs.flags.ref1_pulses)
  {
    printf("scaler_setup_test: Ref 1 mode detected - channel 0  will count up as a reference\n");
    if (fs.bit_pattern & 1) 
    { /* enable ref 1 and 25Mhz */
#ifdef OS_VXWORKS  
      sis3803_ref1(SIS_base_adr, ENABLE_REF_CH1);
      sis3803_25MHz_enable(SIS_base_adr);       /* enable 25MHz pulser */
#endif
    }
    else
    {
      cm_msg(MINFO,"scaler_setup_test",
             " channel 0 is disabled - Ref 1 mode cannot be enabled (bitpat=0x%x)\n",fs.bit_pattern);
    }
    return;
  }
  
/*  REAL MODE - no test pulses
 *  -------------------------- 
 */     
  else /* real mode */
    printf("scaler_init: real input mode detected - all test modes are disabled\n");
  return;
}
