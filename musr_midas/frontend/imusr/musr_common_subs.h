/* function prototypes for musr_common_subs.c 

  $Log: musr_common_subs.h,v $
  Revision 1.1  2003/09/26 20:44:52  suz
  original to cvs

*/
INLINE DWORD GetMaxFree(void);
void   set_v680_display(INT display_enabled, INT display_offset, INT start_line);
void write_message1(INT status, char *name);
INT get_musr_type(void);
INT ggl_init(void);
void scaler_setup_test(void);
