/* Fixed scaler and ioreg input channels for IMUSR

   Included by frontend and musr_config

  $Log: imusr_inputs.h,v $
  Revision 1.2  2015/03/18 00:19:27  suz
  VMIC version. Added some ifdefs

  Revision 1.1  2004/02/09 20:37:31  suz
  identical to 1.1 IMUSR; moved to musr area for combined TD & Imusr



*/
#ifndef __IMUSR_INPUTS_INCLUDE__
#define __IMUSR_INPUTS_INCLUDE__

#ifdef __cplusplus
extern "C" {
#endif


    /* Fixed IOREG outputs */
#define IO_START_ACQ 10
#define IO_MOD_INNER 11   
#define IO_MOD_OUTER 12
#define IO_TIMEOUT 13
#define IO_ACQ_ACTIVE 14
#define IO_FAST_MOD 15  /* TRUE = Fast Mod ON; FALSE = Fast Mod OFF */
#define IO_CONST_TIME 16 /* TRUE = Const time;    FALSE= Const rate */
#define IO_NORM_E  17 /* TRUE = normalize on const E rate; FALSE =  const MU rate */

/*
  Fixed Scaler Inputs

  If these inputs are changed, SC_FIXED_BITPAT must be changed also
*/
#define SC_FRONT_FM_POS 10    /* these were Auxiliary scalers in VMS */
#define SC_BACK_FM_POS 11     /* now they are enabled when Fast Mod is enabled */

#define SC_FRONT_FM_NEG 12
#define SC_BACK_FM_NEG 13

#define SC_TOTAL_RATE 14
#define SC_CLOCK 15        /* clock scaler input number */

/* these fixed values exclude the Fast Mod inputs which are only needed when
   fast modulation is selected:
*/
#define SC_FIXED_BITPAT 0xF000 /* bit pattern of fixed scaler inputs listed
				  above (Fast Mod is DISABLED) */
#define SC_FMOD_BITPAT 0xFC00 /* bit pattern of fixed plus EXTRA scaler inputs used when
				 Fast Mod is ENABLED */
#define SC_NUM_FIXED_INPUTS  4 /* number of fixed scaler inputs listed above */

#define SC_MAX_FM_INPUTS 2   /* maximum extra (FastMod) inputs allowed */


#ifdef __cplusplus
}
#endif

#endif // __IMUSR_INPUTS_INCLUDE__






