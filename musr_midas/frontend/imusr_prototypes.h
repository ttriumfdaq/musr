/* function prototypes for femusr.c */

#ifndef __FEMUSR_PROTO_INCLUDE__
#define __FEMUSR_PROTO_INCLUDE__

#ifdef __cplusplus
extern "C" {
#endif

INT sweep_cycle_end(void);
INT outer_toggle_cycle(void);
INT inner_toggle_cycle(void);
INT preset_cycle(void);


#ifdef __cplusplus
}
#endif
#endif //   __FEMUSR_PROTO_INCLUDE__
