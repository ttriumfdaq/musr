/* imusr_scaler_sum.h

  $Log: imusr_scaler_sum.h,v $
  Revision 1.2  2015/03/18 00:26:13  suz
  VMIC version; add some ifdefs

  Revision 1.1  2004/02/09 20:36:22  suz
  identical to 1.1 IMUSR; moved to musr area for combined TD & Imusr

*/
#ifndef __IMUSR_SCALER_SUM_INCLUDE__
#define __IMUSR_SCALER_SUM_INCLUDE__

#ifdef __cplusplus
extern "C" {
#endif


/* Indices into scaler_sum array
   
   -> offset into data bank of scaler sums sent out by frontend */


#define CLOCK_INDEX 0 /*  index for clock into array */
#define TOTALR_INDEX 1/*  index for total rate into array */
#define FRONT_INDEX 2 /*  start index for front into array */
#define BACK_INDEX 6  /*  start index for back  into array */
#define FRONT_FM_INDEX 10 /*  start index for front FM into array */
#define BACK_FM_INDEX 14 /*  start index for back FM into array */
#define BANK_OFFSET 2 /* adjust later if necessary */


#define SCALER_SUM_CHANNELS (8+2) /* front and back are split into 8 channels
				 (in scaler_sum array)
				 according to inner/outer toggle states
				   + 2 channels for clock,total_r */
#define FM_SUM_CHANNELS 8 /* if fast modulation is enabled, these will add
			       another 8 channels to array */

/* Default histogram titles - may not be used */
const char imusr_his_titles[19][32]={"SweepVal","clock","Total",
		"F---","F-+-","F+--","F++-","B---","B-+-","B+--","B++-",
		"F--+","F-++","F+-+","F+++","B--+","B-++","B+-+","B+++" };


/* Default scaler titles (REAL Scaler Inputs starting at 15 and decreasing )  */
const char imusr_sc_titles[6][32]={"clock","total rate","front_fm-","back_fm-","front_fm+","back_fm+"};
/*               Real Scaler Input   15        14           13          12	 10         11 

		 
  e.g.   F-+-  means   Front Outer Tog = 0 Inner Tog = 1 Fast Mode = 0
         B--+  means   Back  Outer Tog = 0 Inner Tog = 0 Fast Mode = 1
	 
Fast   Tog  Tog   Toggle  Real Scaler      scaler_sum     histo      default histo
Mode  Outer Inner Tot       Input          array index    index       title (odb)
                                                  
---------------------------------------------------------------------------------
-       -    -     -        -                 -             0        SweepVal
x       x    x     x     SC_CLOCK             0             1        Clock
x       x    x     x     SC_TOTAL_RATE        1             2        Total       

0       0    0     0     SC_FRONT_FM_NEG      2             3        F---      
0       0    1     1     SC_FRONT_FM_NEG      3             4        F-+-
0       1    0     2     SC_FRONT_FM_NEG      4             5        F+--
0       1    1     3     SC_FRONT_FM_NEG      5             6        F++-

0       0    0     0     SC_BACK_FM_NEG       6             7        B---
0       0    1     1     SC_BACK_FM_NEG       7             8        B-+-
0       1    0     2     SC_BACK_FM_NEG       8             9        B+--
0       1    1     3     SC_BACK_FM_NEG       9             10       B++-

1       0    0     0     SC_FRONT_FM_POS      10            11       F--+ 
1       0    1     1     SC_FRONT_FM_POS      11            12       F-++
1       1    0     2     SC_FRONT_FM_POS      12            13       F+-+
1       1    1     3     SC_FRONT_FM_POS      13            14       F+++

1       0    0     0     SC_BACK_FM_POS       14            15       B--+ 
1       0    1     1     SC_BACK_FM_POS       15            16       B-++
1       1    0     2     SC_BACK_FM_POS       16            17       B+-+
1       1    1     3     SC_BACK_FM_POS       17            18       B+++

Note:
There will be 1 histogram for the Sweep Device Value whose
title depends on the primary device (e.g. DAC,RF etc).
  
*/
#ifdef __cplusplus
}
#endif
#endif  // __IMUSR_SCALER_SUM_INCLUDE__
