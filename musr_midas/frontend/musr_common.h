/* common parameters between imusr and musr 

  $Log: musr_common.h,v $
  Revision 1.4  2015/03/18 00:25:20  suz
  new for VMIC version

  Revision 1.3  2004/09/14 05:04:59  asnd
  Minor fixes to rates: calc milliseconds as DWORD, rates as float

  Revision 1.2  2004/04/15 20:27:39  suz
  add declaration for rscal create rec

  Revision 1.1  2004/02/09 20:41:12  suz
  combined TD & Imusr version; based on v1.1 for IMUSR


*/

#ifndef __MUSR_COMMON_INCLUDE__
#define __MUSR_COMMON_INCLUDE__

#ifdef __cplusplus
extern "C" {
#endif


#define MAX_SCALER        16
#define N_SCLR 1

char td_eqp_name[] = "MUSR_TD_acq"; /* TD_MUSR area - used for TD and IMUSR */
char imusr_eqp_name[] = "MUSR_I_acq"; /* I_MUSR area - used ONLY for IMUSR */   

HNDLE hDB,hV,hSet;
MUSR_TD_ACQ_V680_STR (musr_td_acq_v680_str);
MUSR_TD_ACQ_V680 vs;
SCALER_SETTINGS_STR(scaler_settings_str);
SCALER_SETTINGS fs;
RSCAL_SETTINGS_STR(rscal_settings_str);


INT size;
INT status; /* status  */

BOOL I_MUSR=FALSE; /* TRUE if I_MUSR is running */
BOOL TD_MUSR=FALSE; /* TRUE if TD_MUSR is running */

/* v680 display - defined as extern in V680.c */ 
INT disp_offset=0; 
INT v680_Display=0 ; /* now an odb parameter */
INT last_message = 0;

/* client flag support */
BOOL client_check = TRUE; /* if true, mdarc will stop the run on error (assuming mdarc is running!) */
char who_stops_run[7]; /* mdarc or user */
BOOL gbl_status =TRUE; /* set false when run should be stopped */
 
  //INT debug=0; /* general debug */
INT ddd=0; /* debug for event building (except scaler) */
INT dss=0; /* debug for scaler event building */
INT  dd=0; /* debug for frontend loop  etc */

DWORD gbl_count;
INT     v680_Display_period = 3000; /* default milliseconds  - now an odb parameter */
INT msg_cnt=0;
INT first_time_display=TRUE;
int  ext_th, last_display, local;
char *str_status[] = {"  N/A  ","STOPPED", "PAUSED"," RUNNING "};

  /* scaler globals */
DWORD time_last_rates;    /* last time scaler was read */  
DWORD old_counts[MAX_SCALER];
BOOL  first_overflow;
INT   max_scaler_input, min_scaler_input;

/* VME base addresses  */
#define V680_BASE     0xc000    
#define VMEIO_BASE    0x780000
#define GGL_BASE      0x8000
#define SIS_BASE      0x383800
INT   SIS_base_adr = SIS_BASE; /* base address for sis3803 scaler VME module */
INT ggl_base=GGL_BASE; /* Gate Generator Logic Board base address */ 
INT v680_base= V680_BASE; /* store to pass to show_commands.c */     
INT vmeio_base = VMEIO_BASE; /* base address for musrvmeio VMiE board */

BOOL gbl_equipments_enabled; // flag indicates if equipments are enabled.


    /* line numbers for v680 display */
#define LINE_1   1
#define LINE_2   2
#define LINE_3   3
#define LINE_4   4
#define LINE_5   5
#define LINE_6   6   
#define LINE_7   7
#define LINE_8   8
#define LINE_9   9
#define LINE_10 10
#define LINE_11 11
#define LINE_12 12
#define LINE_13 13
#define LINE_14 14
#define LINE_15 15
#define LINE_16 16
#define LINE_17 17
#define LINE_18 18
#define LINE_19 19
#define LINE_20 20
#define LINE_21 21
#define LINE_22 22    
#define LINE_23 23
#define LINE_24 24
#define LINE_25 25
#define LINE_26 26
#define LINE_27 27
#define LINE_28 28
#define LINE_29 29   

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop(); /* called periodically by MIDAS is frontend_call_loop=TRUE
		       updates v680 display, checks for clock hang */
INT poll_event(INT source, INT count, BOOL test); /* 
	   Main loop processes v680 data; only gets called by MIDAS
           if equipment MUSR_TD_ACQ is enabled (the trigger event). */
INT imusr_acq(char *pevent, INT off);     
INT send_data_point(char *pevent, INT off);
INT info_odb(char *pevent, INT off);
INT trigger_read(char *pevent, INT off);
INT scaler_read(char *pevent, INT off);
INT scaler_rates(char *pevent, INT off); 
INT diag_read(char *pevent, INT off);
INT histo_read(char *pevent, INT off);

void  hot_display (INT, INT, void * );    
INT setup_hotlink(void);
INT scaler_init(void);
INT scaler_start(void);
void scaler_stop(void);

INT vmeio_init(void);

INT check_update_time(INT max_sec);
INT setup_hotlink_BOR(void);
void close_hotlinks();
INT enable_equipments(BOOL ival);
#ifdef __cplusplus
}
#endif
#endif //  __MUSR_COMMON_INCLUDE__
