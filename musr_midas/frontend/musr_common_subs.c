/* musr_common_subs.c

  Common subroutines between IMUSR and TDMUSR to be included
    - these were in V680.c

    this file of common routines is included into femusr.c (or later fev680.c )

    $Log: musr_common_subs.c,v $
    Revision 1.2  2015/03/18 00:36:43  suz
    VMIC Version. Also changed SYNC to TR_SYNC

    Revision 1.1  2004/02/09 20:43:27  suz
    combined TD & Imusr version; based on v1.1 for IMUSR


*/

void set_v680_display(INT display_enabled, INT display_offset, INT start_line)
{
  /* offset needed if more than 8 channels are enabled
     start_line indicates "last message:" line  */
  disp_offset = display_offset;
  v680_Display =  display_enabled;
  last_message   =  start_line;
  /* printf("set_v680_display: v680_Display=%d offset=%d last_message=%d\n",
     v680_Display,disp_offset,last_message); */
}

INLINE DWORD GetMaxFree(void)
{
  DWORD ival=0;
  /* return the maximum free memory space */
#ifdef OS_VXWORKS
  ival=memFindMax();
#endif
  return (ival);
}


/*--  find whether running I or TD MUSR ----------------------------------------*/
INT get_musr_type(void)
{
  char musr_type[10];
  char str[128];
  int j,len;
  
  I_MUSR=TD_MUSR=FALSE; /* global flags */

  if(debug)printf("get_musr_type starting\n");
  
  sprintf(str,"/experiment/edit on start/musr type");
  size = sizeof(musr_type);
  status = db_get_value(hDB, 0, str,
                        &musr_type , &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"get_musr_type","cannot access key %s (%d)",
           str,status);
    write_message1(status,"get_musr_type");
    return status;
  }

  len=strlen(musr_type);
  for(j=0; j<len; j++)
    musr_type[j] = toupper (musr_type[j]); /* convert to upper case */

  if(debug)printf("len:%d, string %s\n",len,musr_type);

  if (strchr(musr_type,'I'))
    I_MUSR=TRUE;
  if (strchr(musr_type,'T') )
    TD_MUSR=TRUE;
  
  if ( (I_MUSR == FALSE && TD_MUSR == FALSE) ||
       (I_MUSR == TRUE  && TD_MUSR == TRUE) )
  {
    cm_msg(MERROR,"get_musr_type","Bad value of key %s (%s). Must contain \"I\" or \"TD\" ",
           str,musr_type);

    return(DB_INVALID_PARAM);
  }
  return status;
}


/*--------- write_message1 ---------------------------------------------*/  
void
write_message1(INT status, char *name)
{
  /* messages for the most common return values from db_* routines */
  
  char str[60];
  str[0]= '\0';

  if (status == DB_INVALID_HANDLE) sprintf(str,"because of invalid database or key handle"); 
  else if (
    status == DB_NO_KEY) sprintf (str,"because   key_name does not exist");
  else if (status == DB_NO_ACCESS) sprintf (str,"because Key has no read access");
  else if (status == DB_TYPE_MISMATCH) sprintf(str,"because type does not match type in ODB");
  else if (status == DB_TRUNCATED) sprintf(str,"because data does not fit in buffer and has been truncated");
  else if (status == DB_STRUCT_SIZE_MISMATCH) sprintf (str,"because structure size does not match sub-tree size");
  else if (status == DB_OUT_OF_RANGE) sprintf (str,"because odb parameter is out of range");
  else if (status == DB_OPEN_RECORD) sprintf (str,"could not open record");   
  if (strlen(str) > 1 ) cm_msg(MERROR,name,"%s",str );
}


/*------------------------------------------------------------------*/
INT set_client_flag(char *client_name, BOOL value)
/*------------------------------------------------------------------*/
{
  /* set the flag in /equipment/<eqp_name>/client flags/febnmr to indicate to mdarc that is should stop the run
     set the flag  "/equipment/<eqp_name>/client flags/client alarm" to get browser mhttpd to put up an alarm banner

     Note that an almost identical routine is in mdarc_subs.c for use of linux clients;  
     

  */
  char client_str[128];
  INT client_flag;
  BOOL my_value;
  INT status,size;
  if (debug) printf("set_client_flag: starting\n");

  my_value = value;
  sprintf(client_str,"/equipment/%s/client flags/%s",td_eqp_name,client_name );
  if(debug)printf("set_client_flag: setting client flag for client %s to %d\n",client_name,my_value); 

  /* Set the client flag to my_value 
                 value =  TRUE (success)  or FALSE (failure) */
  size = sizeof(my_value);
  status = db_set_value(hDB, 0, client_str, &my_value, size, 1, TID_BOOL);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client status flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }

  /* Set the alarm flag; TRUE - alarm should go off, FALSE alarm stays off  */
  size = sizeof(client_flag);
  if(value) 
    client_flag = 0;
  else
    client_flag = 1;

  sprintf(client_str,"/equipment/%s/client flags/client alarm",td_eqp_name );
  if(debug)printf("set_client_flag: setting alarm flag to %d\n",client_flag); 

  status = db_set_value(hDB, 0, client_str, &client_flag, size, 1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "set_client_flag", "cannot set client alarm flag at path \"%s\" to %d (%d) ",
	     client_str,client_flag,status);
      return status;
    }

  return CM_SUCCESS ;
}



/*--------------------------------------------------------------------------------------------*/
void scaler_setup_test(void)
{

/*
 * Check for scaler Test modes:
 */
  
  if(dss)
    printf("scaler_setup_test starting, scaler_enabled=%d\n",fs.enabled);
  if( !fs.enabled)
  {
    printf("scaler_setup_test: scaler is disabled\n");
    return;
  }
    
/*  Single pulse into all enabled channels
 * ------------------------------------------
 *   sis3803_test_mode_enable   enable test mode,
 *   sis3803_test_pulse         successive writes generate single pulses
 *
 *    NOTE : 25MHz pulser disabled */
  if (vs.flags.test_pulses)
  {
    printf("scaler_setup_test: test pulse mode detected - scaler will increment each readout\n");
#ifdef OS_VXWORKS  
    sis3803_test_mode_enable(SIS_base_adr);   /* enable test mode */
#endif
    return;
  }
  
  
/* 25MHz reference pulses go into all enabled channels
 * ------------------------------------------------------
 *    sis3803_test_mode_enable   enable test mode
 *    sis3803_25MHz_enable       enable 25MHz pulser
 */
  if (vs.flags._5mhz_pulses)
  {      
    printf("scaler_setup_test: 25MHz pulse mode detected - scaler channels will count up\n");
#ifdef OS_VXWORKS
    sis3803_test_mode_enable(SIS_base_adr);   /* enable test mode */
    sis3803_25MHz_enable(SIS_base_adr);       /* enable 25MHz pulser */
#endif
    return;
  }
  
/* Reference pulse into first channel (0) (in NORMAL mode)
 * ------------------------------------------------
 *   sis3803_25MHz_enable       enable 25MHz ref pulser
 *   sis3803_ref1               enable Ref 1
 *
 *    Note: test mode disabled
 */
  if (vs.flags.ref1_pulses)
  {
    printf("scaler_setup_test: Ref 1 mode detected - channel 0  will count up as a reference\n");
    if (fs.bit_pattern & 1) 
    { /* enable ref 1 and 25Mhz */
#ifdef OS_VXWORKS  
      sis3803_ref1(SIS_base_adr, ENABLE_REF_CH1);
      sis3803_25MHz_enable(SIS_base_adr);       /* enable 25MHz pulser */
#endif
    }
    else
    {
      cm_msg(MINFO,"scaler_setup_test",
             " channel 0 is disabled - Ref 1 mode cannot be enabled (bitpat=0x%x)\n",fs.bit_pattern);
    }
    return;
  }
  
/*  REAL MODE - no test pulses
 *  -------------------------- 
 */     
  else /* real mode */
    {
      if (debug)
	printf("scaler_setup_test: real input mode detected - all test modes are disabled\n");
    }
  return;
}



INT get_run_state(void)
{
  // run_state is global
  
  INT size,status;
  size=sizeof(run_state);
  
  status = db_get_value(hDB, 0, "/Runinfo/state", &run_state, &size, TID_INT, FALSE);
  if(status != SUCCESS)
    {
      cm_msg(MINFO,"get_run_state","cannot get value of \"/Runinfo/state \" (%d)",status);
      return (status);
    }
  printf("get_run_state: run_state is %d\n",run_state);
  return status;
}

void display_run_state(void)
{
  // run_state is global
  get_run_state(); // updates run_state
  switch (run_state)
    {
    case STATE_STOPPED:
      printf("Run state is \"Stopped\"\n");
      break;
    case STATE_PAUSED:
      printf("Run state is \"Paused\"\n");
      break;
    case STATE_RUNNING:
      printf("Run state is \"Running\"\n");
      break;
    default:
     printf("Run is in unknown state\n");
    }
  return;
}

/*-----------------------------------------------------------------------------------------------------*/
INT stop_run(void)
/*-----------------------------------------------------------------------------------------------------*/
{
  char str[128];
  INT status,size;
  

  gbl_status = TRUE; // Indicates fatal error - run in being stopped

  /* check for current run state; if not stopped, stop it */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/runinfo/State", &run_state, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","cannot get /runinfo/State (%d)",status);
      printf("run_state: Cannot determine present run state.\n");
      return status;
    }
  
  
  if (run_state == STATE_STOPPED)
    {
      printf("stop_run: Run is already stopped\n");
      return SUCCESS;
    }
  
   
 
  printf("run_state: attempting to stop the run immediately...\n");
  
  status = cm_transition(TR_STOP | TR_DEFERRED, 0, str, sizeof(str), TR_SYNC, 0);
  if(status == CM_DEFERRED_TRANSITION)
    cm_msg(MINFO,"stop_run","Deferred transition is set");
  else if((status !=  CM_SUCCESS) && (status != CM_DEFERRED_TRANSITION))
    cm_msg(MERROR, "stop_run", "cannot stop run immediately: %s (%d)", str, status);
  else
    cm_msg(MINFO,"stop_run","run should now be stopped");

  status = set_client_flag("frontend",0); /* indicate the frontend stopped the run */
  cm_msg(MERROR,"stop_run","frontend program stopped the run due to error");


  status = db_get_value(hDB, 0, "/runinfo/State", &run_state, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"stop_run","cannot get /runinfo/State (%d)",status);
      printf("run_state: Cannot determine present run state.\n");
      return status;
    }
  

  if(run_state != STATE_STOPPED)
    printf("stop_run: run  is not STOPPED\n");
  else
     printf("stop_run: run is now  STOPPED\n");

  return status;

}

