/* function prototypes for musr_common_subs.c 

  $Log: musr_common_subs.h,v $
  Revision 1.2  2015/03/18 00:27:06  suz
  VMIC version; add some ifdefs

  Revision 1.1  2004/02/09 20:42:48  suz
  combined TD & Imusr version; based on v1.1 for IMUSR


*/

#ifndef __MUSR_COMMON_SUBS_INCLUDE__
#define __MUSR_COMMON_SUBS_INCLUDE__

#ifdef __cplusplus
extern "C" {
#endif

INLINE DWORD GetMaxFree(void);
void   set_v680_display(INT display_enabled, INT display_offset, INT start_line);
void write_message1(INT status, char *name);
INT get_musr_type(void);
INT ggl_init(void);
void scaler_setup_test(void);
INT set_client_flag(char *client_name, BOOL value);
INT get_run_state(void);
void display_run_state(void);
INT stop_run(void);
#ifdef __cplusplus
}
#endif
#endif //   __MUSR_COMMON_SUBS_INCLUDE__
