/*  Midas_Tcl_AppInit.c
 *
 *  Version of Tcl_AppInit.c including extra calls to initialize midas
 *  interpreter
 *  
 *  Called by defining TK_LOCAL_APPINIT as Midas_Tcl_AppInit in Makefile
 *
 *  This code derived from Tcl_AppInit in  /usr/lib/tk8.3/tkAppInit.c
 */

/*
  $Log: Midas_Tcl_AppInit.c,v $
  Revision 1.5  2013/02/16 01:02:24  asnd
  Updated for SL6 and 64-bit: add MFLAG which is "", "-m32", or "-m64".
  Do not use system  tkAppInit; main() is now in Midas_Tcl_AppInit.c.

  Revision 1.4  2007/09/27 04:29:57  asnd
  REMOVE camp_cmd ability, unless CAMP_CLIENT is defined during build.

  Revision 1.3  2002/09/19 04:23:16  asnd
  Allow Daemon mode (like tclsh) when there is no DISPLAY

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tk.h>
#include <locale.h>

/*
 *----------------------------------------------------------------------
 *
 * main --
 *
 *      This is the main program for the application.
 *
 * Results:
 *      None: Tk_Main never returns here, so this procedure never returns
 *      either.
 *
 * Side effects:
 *      Whatever the application does.
 *
 *----------------------------------------------------------------------
 */

int
main(
    int argc,                   /* Number of command-line arguments. */
    char **argv)                /* Values of command-line arguments. */
{
    /*
     * The following #if block allows you to change the AppInit function by
     * using a #define of TCL_LOCAL_APPINIT instead of rewriting this entire
     * file. The #if checks for that #define and uses Tcl_AppInit if it
     * doesn't exist.
     */

#ifndef TK_LOCAL_APPINIT
#define TK_LOCAL_APPINIT Midas_Tcl_AppInit
#endif
    extern int TK_LOCAL_APPINIT _ANSI_ARGS_((Tcl_Interp *interp));

    /*
     * The following #if block allows you to change how Tcl finds the startup
     * script, prime the library or encoding paths, fiddle with the argv,
     * etc., without needing to rewrite Tk_Main()
     */

#ifdef TK_LOCAL_MAIN_HOOK
    extern int TK_LOCAL_MAIN_HOOK _ANSI_ARGS_((int *argc, char ***argv));
    TK_LOCAL_MAIN_HOOK(&argc, &argv);
#endif

    Tk_Main(argc, argv, TK_LOCAL_APPINIT);
    return 0;                   /* Needed only to prevent compiler warning. */
}

int
Midas_Tcl_AppInit(interp)
    Tcl_Interp *interp;		/* Interpreter for application. */
{
    char * disp;
    /*
     * Only load Tk if display exists.  This allows operation as a Daemon.
     * If DISPLAY is defined but invalid, then there is an error.
     */
    disp = getenv("DISPLAY") ;
    
//  printf("Midas_Tcl_AppInit starting\n");
    if (Tcl_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
    }
    if (disp) { /* Only load Tk if display exists */
      if (Tk_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
      }
      Tcl_StaticPackage(interp, "Tk", Tk_Init, Tk_SafeInit);
    }
#ifdef TK_TEST
    if (Tcltest_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
    }
    Tcl_StaticPackage(interp, "Tcltest", Tcltest_Init,
            (Tcl_PackageInitProc *) NULL);
    if (disp) { /* Only load Tk if display exists */
      if (Tktest_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
      }
      Tcl_StaticPackage(interp, "Tktest", Tktest_Init,
			(Tcl_PackageInitProc *) NULL);
    }
#endif /* TK_TEST */


    /*
     * Call the init procedures for included packages.  Each call should
     * look like this:
     *
     * if (Mod_Init(interp) == TCL_ERROR) {
     *     return TCL_ERROR;
     * }
     *
     * where "Mod" is the name of the module.
     */

    /*
     * Only preload BLT if Tk is loaded (display exists).
     * Note that you can use blt::bgexec even in Daemon mode by declaring
     * package require BLT
     */
    if (disp) { 
      /* Load standard BLT extensions to Tk */
      if (Blt_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
      }
    }

    /* Initialize Midas functions */
    if (Midas_Init (interp) == TCL_ERROR) {
      return TCL_ERROR;
    }

#ifdef CAMP_CLIENT    
    /* Initialize as a camp client (camp_cmd) */
    if( campTclClientInit( interp ) == TCL_ERROR ) {
        return TCL_ERROR;
    }
#endif

    /*
     * Call Tcl_CreateCommand for application-specific commands, if
     * they weren't already created by the init procedures called above.
     */

    /*
     * Specify a user-specific startup file to invoke if the application
     * is run interactively.  Typically the startup file is "~/.apprc"
     * where "app" is the name of the application.  If this line is deleted
     * then no user-specific startup file will be run under any conditions.
     */

    if (disp) { 
      Tcl_SetVar(interp, "tcl_rcFileName", "~/.wishrc", TCL_GLOBAL_ONLY);
    }
    else {
      Tcl_SetVar(interp, "tcl_rcFileName", "~/.tclshrc", TCL_GLOBAL_ONLY);
    }
    return TCL_OK;
}
