/********************************************************************/
/*                                                                  */
/*  TCL/Midas language interface                                    */
/*                                                                  */
/*  author:    Suzannah Daviel, TRIUMF                              */
/*   based on original by  G.J. Hofman  U. Colorado March 00        */
/*  date  :      April 01                                           */
/*                                                                  */
/*  Contents: allows TCL to make midas calls                        */
/********************************************************************/
/*
  $Log: midas_tcl.c,v $
  Revision 1.19  2016/05/12 01:14:48  asnd
  Repeat lost edits for setting TID_DOUBLE vars, and mistaken sizeof(TID_*)

  Revision 1.18  2015/03/19 23:42:41  suz
  extensive changes: later midas version(s), stop now again etc

  Revision 1.17  2006/11/21 22:45:10  asnd
  After kill, restore the purge+archive flag, in case it got clobbered

  Revision 1.16  2006/09/21 08:24:07  asnd
  Adjust some types for Tcl 8.4 Api

  Revision 1.15  2004/11/09 23:58:43  suz
  add parameter to tpause to suppress save; mdarc param endrun_save_last_event changed to suppress_save_temporarily

  Revision 1.14  2004/11/08 20:55:05  suz
  Add endrun save param for kill script; Donald added extra messages etc

  Revision 1.13  2004/08/04 06:43:51  asnd
  Plug memory leak in tget_data

  Revision 1.12  2004/06/01 20:32:04  suz
  small changes to fix warnings for RedHat 9

  Revision 1.11  2004/05/18 05:15:10  asnd
  Spruce up tget_value with better conversion of floats, and better error messages

  Revision 1.10  2004/04/29 02:32:36  asnd
  Add a "midas scl" command to avoid running odbedit and parsing its output.

  Revision 1.9  2004/04/23 18:58:51  suz
  remove debugging statement

  Revision 1.8  2004/04/23 18:53:25  suz
  add extra params for perlscripts kill/toggle; different location for perlscript text output files

  Revision 1.7  2004/02/20 23:10:36  asnd
  Add extra parameter for real/test_run.pl

  Revision 1.6  2003/10/15 09:19:12  asnd
  Add [midas yield] function, returning "OK" or "SHUTDOWN".

  Revision 1.5  2003/04/25 04:40:37  asnd
  Change set_data_array and get_data to interact properly with Tcl lists.

  Revision 1.4  2002/06/12 21:20:07  suz
  latest midas version & camp client

  Revision 1.3  2002/05/06 18:04:36  suz
  remove local declarations of str_buff

  Revision 1.2  2002/05/03 02:03:21  asnd
  Fix string overflow in tset_data_array

  Revision 1.1  2002/04/24 19:55:29  suz
  Original


*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h" /* defines INLINE so place before tcl.h */
#include <tcl.h>
#include "msystem.h"

#define MAXSTRING 512  /* maximum size expected for odb key for a string */

static HNDLE hDB, hKey;
static INT status;
static char str_buff[MAXSTRING];
static char exp_name[NAME_LENGTH];
static char host_name[100];

int set_key(HNDLE hDB, HNDLE hKey, int index1, int index2, char *value);

/* ---------     globals:     ------- */

BOOL debug=0; /*  set to 0 for no debug, 1 for debug. Use set_debug from TCL. */


#define _DB_ERROR(type) \
  sprintf (str_buff,"error for (%s) key %s: (%d) %s", type , argv[1], status, db_message_text(status));\
  Tcl_AppendResult (interp, str_buff, (char *) NULL);\
  return TCL_ERROR

static char * db_message_text( INT status ) 
{
  switch (status) {
  case DB_NO_MEMORY: return ("Not enough memory");
  case DB_INVALID_NAME: return ("Database name is invalid");
  case DB_INVALID_HANDLE: return ("Database handle is invalid");
  case DB_NO_SLOT: return ("Maximum number of clients exceeded");
  case DB_MEMSIZE_MISMATCH: return ("Database size mismatch");
  case DB_INVALID_PARAM: return ("Invalid parameter");
  case DB_FULL: return ("Shared memory is full");
  case DB_KEY_EXIST: return ("Key exists already");
  case DB_NO_KEY: return ("Key does not exist");
  case DB_TRUNCATED: return ("Data was truncated");
  case DB_TYPE_MISMATCH: return ("Type mismatch");
  case DB_NO_MORE_SUBKEYS: return ("No more sub-keys");
  case DB_NO_ACCESS: return ("No such access allowed for key");
  case DB_STRUCT_SIZE_MISMATCH: return ("Structure size mismatch");
  case DB_OPEN_RECORD: return ("Key is open by another client");
  case DB_OUT_OF_RANGE: return ("Index is out of array range");
  case DB_INVALID_LINK: return ("Invalid link");
  }
  return ("Unlisted error");
}


/* only 1 command is created- midas. All midas commands are effectivelly
   parameters to this new Tcl/Tk command                               */


int Midas_Init ( Tcl_Interp *interp ) {
  
  
  extern int MidasCmd _ANSI_ARGS_(( ClientData clientData,
				   Tcl_Interp *interp,
				   int argc,
				   char *argv[]
				   ));
  
  Tcl_CreateCommand(interp, "midas",
		    (Tcl_CmdProc *) MidasCmd,
		    (ClientData) NULL,
		    (Tcl_CmdDeleteProc *) NULL );
  
  return TCL_OK;
}


/**********************************************************************/
int tconnect_experiment  
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
  
  /* input parameters:
     hostname            name of midas host computer 
     experiment          name of midas experiment 
     client name         the name that will be given to the client 

     Returns:
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message
  */   

  INT len;
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  char   client_name[HOST_NAME_LENGTH];

  // Use environment variables unless values are supplied as input params
  cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH); 
  //printf("argc=%d   \n",argc); // number of arguments. First one is "connect experiment"
  
  if (argc > 1) 
	strcpy(host_name, argv[1]);
  if (argc > 2)
	strcpy(expt_name, argv[2]);
  if (argc > 3)
        strcpy( client_name, argv[3]);
  else
    strcpy( client_name, "anonymous");

  if(debug)printf("connect_experiment: Host name: %s Experiment name: %s  Client name: %s\n",host_name,expt_name,client_name);

  /* connect to midas expt */
  // status = cm_connect_experiment(argv[1],argv[2],argv[3],0);
  status = cm_connect_experiment(host_name, expt_name, client_name,0);
  if (status != CM_SUCCESS)
    {
      // sprintf (str_buff,"midas_tcl: Can't connect to experiment %s %s (%d) ", argv[1],argv[2],status );
      sprintf (str_buff,"midas_tcl: Can't connect to experiment %s %s (%d) ", host_name,expt_name,status );
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }

  cm_enable_watchdog(FALSE);

  /* might need these later  */
#ifdef later
  {
    len = strlen(argv[1]);
    strncpy (host_name,argv[1],len);
    host_name[len]='\0';
    
    len = strlen(argv[2]);
    strncpy (exp_name,argv[2],len);
    exp_name[len]='\0';
    
    if(debug)printf("tconnect_experiment: host_name : %s  exp_name: %s\n",host_name,exp_name);
  }
#endif


  return TCL_OK;
}

/**********************************************************************/
int tdisconnect_experiment  
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
  
  

  /* disconnect from midas expt

     Parameters:
        none

     Returns:   
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message
   */
  status = cm_disconnect_experiment();
  if (status != CM_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl: Can't disconnect from experiment (%d)",status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      return TCL_ERROR;
    }

  return TCL_OK;
}


/**********************************************************************/
int tget_experiment_database 
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     


  /* connect to midas database

     
     Parameters:
        none

     Returns:   
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message
   */
  status=cm_get_experiment_database (&hDB,&hKey);
  if (status != CM_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl: Can't connect to Midas database (%d)",status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      return TCL_ERROR;
    }


  return TCL_OK;
}
/************************************************************************/
int tset_debug
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
  /* set value of debug flag

     parameters :
     debug flag        1 = ON or 0 = OFF

     Returns:
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message

  */
  if (argc != 2)
  {
    Tcl_AppendResult (interp, "tset_debug: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }

  debug = atoi(argv[1]); /*  true  */
  sprintf(str_buff,"midas_tcl tset_debug: set debug flag to %d\n",debug);
  Tcl_AppendResult (interp, str_buff  , (char *) NULL );
  return TCL_OK;  
}
/************************************************************************/
int tshow_debug
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
  /* show value of debug flag

     Parameters : none
     Returns:
     TCL_OK              success
  */
 
  if(debug)
    sprintf(str_buff,"midas_tcl tset_debug: debug flag is set (%d)\n",debug);
  else
    sprintf(str_buff,"midas_tcl tset_debug: debug flag is clear (%d)\n",debug);

  Tcl_AppendResult (interp, str_buff  , (char *) NULL );
  return TCL_OK;  
}
/************************************************************************/

int tget_RunNum
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
  /* get the next valid run number for TEST or REAL run 

     parameters :
   argv[1]     type              test/real
       [2]     experiment        name of midas experiment (musr2 or musr currently)
       [3]     beamline          name of beamline (M20, DEV etc)
       [4]     perlscript path   directory path where perlscripts are on disk   

     Calls perl scripts test_run.pl or real_run.pl depending on run type. These
     call get_next_run_number.pl to determine next run number.
     Updates run_type and run number in the odb
  
     Returns:
     TCL_OK     success  
     TCL_ERROR  fail     & string containing failure message

     The next run number can be determined from "/Runinfo/Run number"
  */
  char outfile[48];
  char perl_name[32];
  char perl_script[80];
  char  *s;
  INT i,j;
  char  cmd[132];
  
  if (argc != 5)
  {
    Tcl_AppendResult (interp, "tget_runNum: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }
  
  if(debug) printf("tget_RunNum starting with run_type=%s, expt=%s, beamline=%s, perlscript path%s\n",
                   argv[1],argv[2],argv[3],argv[4]);
  
  sprintf(perl_script,"%s",argv[4]);
/*  trimBlanks(perl_script,perl_script);  */
  
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_script,'/');
  i= (int) ( s - perl_script );
  j= strlen( perl_script );
  
  /* printf("setup_hotlink: string length of perl_script %s  = %d, last occurrence of / = %d\n", perl_script, j,i);*/
  if ( i == (j-1) )
  {
    /*if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ...  \n"); */
    perl_script[i]='\0';
  }
  sprintf(perl_name,"/%s_run.pl",argv[1]);
  strcat(perl_script,perl_name);
  if(debug)printf("tget_runNum: using perl_script = %s\n",perl_script);
  sprintf(outfile,"/var/log/midas/%s/%s_run.txt",argv[1],argv[2]);
  if(debug)printf("tget_runNum: outfile = %s\n",outfile);
  
  
  
  /*
    perl script to find next valid run number and write it to /Runinfo/run number
    It (should) handle all eventualities  including mdarc restarted midway through a
    run.  It uses run_type to determine what type of run (test/real) the user
    wants, and assigns a run number accordingly.
  */
  /*  parameters to real_run.pl  expt dis_run_no_check beamline 2 */
  /*  The final "2" indicates musr, as opposed to bnmr            */
  unlink(outfile); /* delete any old copy of perl output file in case of failure */
  sprintf(cmd,"%s %s %s n %s %d",perl_script,argv[4],argv[2],argv[3],2); 
  if(debug) printf("tget_runNum: Sending system command  cmd: %s\n",cmd);
  status =  system(cmd);
  
  
  if (status)
  {
    printf (" ========================================================================================\n");
    cm_msg (MERROR,"tget_runNum","Perl script %s returns failure status (%d)",perl_name,status);
    sprintf(str_buff,
            "See file %s for info - if no file, there may be compilation errors in the perl script",outfile); /* no file to print */
    Tcl_AppendResult (interp, str_buff  , (char *) NULL );
    return TCL_ERROR; /* bad status from perl script */
  }
  else
    if(debug)printf("tget_runNum : Success from perl script to get next run number\n");

  Tcl_AppendResult (interp, str_buff  , (char *) NULL );
  return TCL_OK;  
}
/************************************************************************/

int ttoggle
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc, char *argv[]))
{
  /* toggle run between TEST and REAL. Will not toggle if run is paused (or stopped). 
     
  parameters :
  argv
  [1]     experiment        name of midas experiment (musr currently)
  [2]     beamline          name of beamline (M20, DEV etc)
  [3]     perlscript path   directory path where perlscripts are on disk   
  
  Calls perl scripts toggle.pl 
  
  Returns:
  TCL_OK     success  
  TCL_ERROR  fail     & string containing failure message
  
  The next run number can be determined from "/Runinfo/Run number"
  */
  char outfile[48];
  char perl_name[]="/toggle.pl";
  char perl_script[80];
  char  *s;
  INT i,j;
  char  cmd[132];
  HNDLE hKeyS;
  char str[128];
  char check[2],logging[2]; 
  BOOL rn_check;
  INT size;
  
  if (argc != 4)
    {
      Tcl_AppendResult (interp, "midas_tcl ttoggle: not enough parameters supplied", (char *) NULL); 
      return TCL_ERROR; 
    }
  
  if(debug) printf("ttoggle starting with expt=%s, beamline=%s, perlscript path%s\n",
                   argv[1],argv[2],argv[3]);
  
  sprintf(perl_script,"%s",argv[3]);
  /*   trimBlanks(perl_script,perl_script);  */
  
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_script,'/');
  i= (int) ( s - perl_script );
  j= strlen( perl_script );
  
  /* printf("setup_hotlink: string length of perl_script %s  = %d, last occurrence of / = %d\n", perl_script, j,i);*/
  if ( i == (j-1) )
    {
      /*if(debug_check) printf("darc_get_odb: Found a trailing /. Removing it ...  \n"); */
      perl_script[i]='\0';
    }
  sprintf(outfile,"/var/log/midas/%s/toggle.txt",argv[1]);

  strcat(perl_script,perl_name);
  if(debug)printf("ttoggle: using perl_script = %s\n",perl_script);
  if(debug)printf("ttoggle: outfile = %s\n",outfile);
  
  
  
  /*
    perl script to find next valid run number and write it to /Runinfo/run number
    It (should) handle all eventualities  including mdarc not running, run paused etc.
    
  */

  /* NOTE: db_get_value for midas 1.9.0 no longer creates the key if the key
     does not exist if last parameter is FALSE. So we don't need the db_find_key any
     more - may take them all out later */
  
  /*  get the parameter disable_run_no_check from odb   */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/disable run number check");
  status =   db_find_key(hDB, 0, str ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl ttoggle: Can't find key for %s (%d) ",str,status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  size = sizeof(rn_check);
  status=db_get_value (hDB, 0, str, &rn_check,&size ,TID_BOOL,FALSE);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl ttoggle: error from get_value for (BOOL) %s (%d) %s",
	       str,status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  
  if (rn_check)
    strcpy(check,"y");
  else
    strcpy(check,"n");
  
  /*   get the parameter "enable mdarc logging" from odb   */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/enable mdarc logging");
  status =   db_find_key(hDB, 0, str ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl ttoggle: Can't find key for %s (%d) ",str,status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  size = sizeof(rn_check);
  status=db_get_value (hDB, 0, str, &rn_check,&size ,TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl ttoggle: error from get_value for (BOOL) %s (%d) %s",
	       str,status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  
  if (rn_check)
    strcpy(logging,"y");
  else
    strcpy(logging,"n");
  /*  parameters to toggle.pl,  expt, dis_run_no_check, hold_flag, beamline */
  
  /* NOTE: hold_flag is a dummy because MUSR is using Midas PAUSE instead */ 
  unlink(outfile); /* delete any old copy of perl output file in case of failure */
/* argv[1] = experiment  argv[2] = beamline  argv[3]  perlscript path
   toggle.pl takes arguments :
include_path; experiment; disable_run_num_check; hold flag; enable logging flag; beamline ; dummy mode
    argv[3] ;   argv[1] ;  check               ;      n   ;       logging      ;   argv[2];   2
                           (read from odb)                  (read from odb) 
NOTE:	 togggle.pl checkes whether run is paused for MUSR only (hold_flag is a dummmy) ; BNMR uses hold_flag
*/
  sprintf(cmd,"%s %s %s %s n %s %s 2",perl_script,argv[3],argv[1], check, logging, argv[2]); 
  if(debug) printf("ttoggle: Sending system command  cmd: %s\n",cmd);
  status =  system(cmd);
  
  
  if (status)
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR,"ttoggle","Perl script %s returns failure status (%d)",perl_name,status);
      sprintf(str_buff,
	      "See file %s for info - if no file, there may be compilation errors in the perl script",outfile); /* no file to print */
      Tcl_AppendResult (interp, str_buff  , (char *) NULL );
      return TCL_ERROR; /* bad status from perl script */
    }
  else
    if(debug)printf("ttoggle : Success from perl script to toggle  run number\n");
  
  Tcl_AppendResult (interp, str_buff  , (char *) NULL );
  return TCL_OK;  
}
/************************************************************************/


int tkill
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
  /* kill this run; run must be paused
     
  parameters :
  argv[1]     experiment        name of midas experiment (musr2 or musr currently)
      [2]     run number        present run number
      [3]     beamline          name of beamline (M20, DEV etc)
      [4]     perlscript path   directory path where perlscripts are on disk   
  
  Calls perl scripts kill.pl 
  
  Returns:
  TCL_OK     success  
  TCL_ERROR  fail     & string containing failure message
  
 
  */
  char outfile[48];
  char perl_name[]="/kill.pl";
  char perl_script[80];
  char  *s;
  INT i,j;
  char  cmd[132];
  HNDLE hKeyS;
  char str[128];
  char check[2]; 
  char toggle[2];
  char purge[2], save[2];
  char saved_dir[132];
  BOOL ibool, ipurge;
  INT size;
  INT run_state;
  
  if (argc != 5)
    {
      Tcl_AppendResult (interp, "midas_tcl tkill: not enough parameters supplied", (char *) NULL); 
      return TCL_ERROR; 
    }
  
  if(debug) printf("tkill starting with expt=%s, run number=%s, beamline=%s, perlscript path%s\n",
                   argv[1],argv[2],argv[3],argv[4]);
  
  sprintf(perl_script,"%s",argv[4]);
  /*  trimBlanks(perl_script,perl_script);  */
  
  /* if there is a trailing '/', remove it */
  s = strrchr(perl_script,'/');
  i= (int) ( s - perl_script );
  j= strlen( perl_script );
  
  /* printf("setup_hotlink: string length of perl_script %s  = %d, last occurrence of / = %d\n", perl_script, j,i);*/
  if ( i == (j-1) )
    {
      perl_script[i]='\0';
    }

  strcat(perl_script,perl_name);
  if(debug)printf("tkill: using perl_script = %s\n",perl_script);
  sprintf(outfile,"/home/midas/musr/log/%s/kill.txt",argv[1]);
  if(debug)printf("tkill: outfile = %s\n",outfile);
  
  
  
  /*
    perl script to find next valid run number and write it to /Runinfo/run number
    It (should) handle all eventualities  including mdarc not running, run paused etc.
    
  */

  
  /*  get the parameter disable_run_number_check from odb  */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/disable run number check");
  status =   db_find_key(hDB, 0, str ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: Can't find key for %s (%d) ",str,status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  size = sizeof(ibool);
  status=db_get_value (hDB, 0, str, &ibool,&size ,TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: error from get_value for (BOOL) %s (%d) %s",
	       str,status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  
  if (ibool)
    strcpy(check,"y");
  else
    strcpy(check,"n");

  /*  get the parameter "toggle"  from odb  */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/toggle");
  status =   db_find_key(hDB, 0, str ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: Can't find key for %s (%d) ",str,status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  size = sizeof(ibool);
  status=db_get_value (hDB, 0, str, &ibool,&size ,TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: error from get_value for (BOOL) %s (%d) %s",
	       str,status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  
  if (ibool)
    strcpy(toggle,"y");
  else
    strcpy(toggle,"n");

  /*   get the parameter purge flag  from odb  */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/endrun_purge_and_archive");
  status =   db_find_key(hDB, 0, str ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: Can't find key for %s (%d) ",str,status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  size = sizeof(ibool);
  status=db_get_value (hDB, 0, str, &ibool,&size ,TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: error from get_value for (BOOL) %s (%d) %s",
	       str,status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  
  if (ibool)
    strcpy(purge,"y");
  else
    strcpy(purge,"n");

  ipurge = ibool;

  /*   get the parameter "save last event"  from odb  */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/suppress_save_temporarily");
  status =   db_find_key(hDB, 0, str ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: Can't find key for %s (%d) ",str,status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  size = sizeof(ibool);
  status=db_get_value (hDB, 0, str, &ibool,&size ,TID_BOOL, FALSE);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: error from get_value for (BOOL) %s (%d) %s",
	       str,status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  
  if (ibool)
    strcpy(save,"y");
  else
    strcpy(save,"n");


  /*  get the parameter saved data directory  from odb  */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/saved_data_directory");
  status =   db_find_key(hDB, 0, str ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: Can't find key for %s (%d) ",str,status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  size = sizeof(saved_dir);
  status=db_get_value (hDB, 0, str, &saved_dir ,&size ,TID_STRING, FALSE);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tkill: error from get_value for (STRING) %s (%d) %s",
	       str,status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }

  /* get the run state */
  size = sizeof(run_state);
  status=db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
  {
    sprintf (str_buff,"midas_tcl tkill: Can't get value for /Runinfo/State (%d)",status);
    Tcl_AppendResult (interp, str_buff, (char *) NULL);         
    return TCL_ERROR; 
  }

  /* parameters to kill.pl: include_path,  experiment, run_number, saved_dir, disable_run_no_check,
   *                         purge_flag, hold_flag, beamline, toggle_flag, save_flag, dummy mode=2
   *
   * NOTE: run state is sent instead of hold_flag  because MUSR is using Midas PAUSE instead
   */ 
  unlink(outfile); /* delete any old copy of perl output file in case of failure */
  sprintf(cmd,"%s %s %s %s %s %s %s %d %s %s %s 2",perl_script, argv[4], argv[1],argv[2],
	  saved_dir, check, purge,  run_state, argv[3], toggle, save); 
  if(debug)printf("tkill: Sending system command  cmd: %s\n",cmd);

  status =  system(cmd);

  /*   Restore the purge flag parameter in the odb, in case the perl script messed it up  */
  sprintf(str,"/equipment/MUSR_TD_acq/mdarc/endrun_purge_and_archive");
  i = db_set_value (hDB, 0, str, &ipurge, sizeof(ipurge), 1, TID_BOOL);

  /*  Check status return from perl script */
  if (status)
    {
      printf (" ========================================================================================\n");
      cm_msg (MERROR,"tkill","Perl script %s returns failure status (%d)",perl_name,status);
      sprintf(str_buff,
	      "See file %s for info - if no file, there may be compilation errors in the perl script",outfile); /* no file to print */
      Tcl_AppendResult (interp, str_buff  , (char *) NULL );
      return TCL_ERROR; /* bad status from perl script */
    }
  else
    if(debug)printf("tkill : Success from perl script to kill this run\n");
  
  Tcl_AppendResult (interp, str_buff, (char *) NULL );
  return TCL_OK;
}
/************************************************************************/

int tget_value
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
/* This routine gets the data from a single value (NOT an array)

  ** The type of the data is handled automatically **

      input parameters:
      key name          odb key 

      Returns:
       
     TCL_OK     success  & string containing value read from key name
     TCL_ERROR  fail     & string containing failure message
  
   */
  KEY key;
  HNDLE hKeyS;

  INT    data_INT;
  DWORD  data_DWORD;  
  float  data_FLOAT;
  double data_DOUBLE;
  BOOL   data_BOOL;
  INT size;
  
  if (argc != 2)
  {
    Tcl_AppendResult (interp, "midas get_value: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }
  
  /* to get a value, we need to know its type in the midas data base 
     even though it will always be a string in Tcl/Tk  
     So: retrieve the key structure for this data, */
  
  if(debug)
    printf("tget_value: starting with keyname: %s\n",argv[1]);
  
  status =   db_find_key(hDB, 0, argv[1] ,&hKeyS);

  if (status==DB_SUCCESS)
    {
      status=db_get_key(hDB, hKeyS, &key);  /* get key info, not the key ! */
      if(status != DB_SUCCESS)
      {
        sprintf (str_buff,"midas get_value: Can't get key for %s (%d) %s",
		 argv[1],status,db_message_text(status));
	Tcl_AppendResult (interp, str_buff, (char *) NULL);
        return TCL_ERROR; 
      }
    } 
  else 
    {
      sprintf (str_buff,"midas get_value: Can't find key for %s (%d) %s",
	       argv[1],status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      return TCL_ERROR;
    }

  if (key.num_values != 1)
    {
      sprintf (str_buff,
          "midas get_value: variable %s is an array with %d values. Use midas get_data instead",
	  argv[1],key.num_values);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      return TCL_ERROR;
    }

  /* Use db_get_data because db_get_value re-searches for key! */

  /* is there a clever way of doing this?  Could use db_sprintf */

  if (key.type==TID_INT) 
  {
    status=db_get_data (hDB,hKeyS,&data_INT, &key.item_size,key.type);
    if (status != DB_SUCCESS) { _DB_ERROR("INT"); }
    sprintf (str_buff,"%d",data_INT);
  }
  else if (key.type==TID_DWORD)
  {
    status=db_get_data (hDB,hKeyS,&data_DWORD, &key.item_size,key.type);
    if (status != DB_SUCCESS) { _DB_ERROR("DWORD"); }
    sprintf (str_buff,"%d", data_DWORD); 
  }
  else if (key.type==TID_FLOAT) 
  {
    status=db_get_data (hDB,hKeyS,&data_FLOAT, &key.item_size,key.type);
    if (status != DB_SUCCESS) { _DB_ERROR("FLOAT"); }
    sprintf (str_buff,"%g",data_FLOAT); 
  }
  else if (key.type==TID_DOUBLE)
  {
    status=db_get_data (hDB,hKeyS,&data_DOUBLE, &key.item_size,key.type);
    if (status != DB_SUCCESS) { _DB_ERROR("DOUBLE"); }
    sprintf (str_buff,"%lg",data_DOUBLE); 
  }
  else if (key.type==TID_BOOL) 
  {
    status=db_get_data (hDB,hKeyS,&data_BOOL, &key.item_size,key.type);
    if (status != DB_SUCCESS) { _DB_ERROR("BOOL"); }
    sprintf (str_buff,"%d",data_BOOL); 
  }
  else if (key.type==TID_STRING) 
  {
    size = key.item_size;
    if(debug)
      printf("tget_value: key.item_size=%d, buffer length=%d\n",size,MAXSTRING);
      
    if ( size > sizeof(str_buff) )
    {
      sprintf (str_buff,"midas get_value: key size (%d) exceeds buffer maximum (%d)",
	       size,sizeof(str_buff));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      return TCL_ERROR;
    }
    status=db_get_data (hDB,hKeyS,str_buff, &size, key.type);
    if (status != DB_SUCCESS) { _DB_ERROR("STRING"); }
    str_buff[sizeof(str_buff)-1] = '\0';
    if ( size<sizeof(str_buff) ) str_buff[size] = '\0';

    if(debug)
      printf ("tget_value: string=%s, size=%d, string length=%d\n",
	      str_buff,size,strlen(str_buff));
  }
  else
  {
    sprintf (str_buff,"midas get_value: odb key type not supported in TCL for key %s",argv[1]);
    Tcl_AppendResult (interp, str_buff, (char *) NULL);
    return TCL_ERROR;
  }

  Tcl_AppendResult (interp, str_buff, (char *) NULL);   
  return TCL_OK;
}


/************************************************************************/

int tget_data
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{

  /* This routine gets the data from an ARRAY
   Call from TCL with the following parameters:
      argv[1]   key name

  The type of the data is handled automatically
  input parameters:
  key name  (string)    name of midas odb key ( refers to an array)


     Returns:
     TCL_OK              success  & string containing values read from key name
     TCL_ERROR           fail     & string containing failure message

     (The return string is the string representation of a Tcl list.  This
     program does not use the newer object API for Tcl.)
  */

  KEY key;
  HNDLE hKeyS;

  INT size;
  int i;
  int flags;
  int size_e;

  /* 
   * Thread-unsafe static allocation for pdata.  If we go multi-threaded, then use 
   * temporary (not static) pdata, and free it before return.
   */
  static char *pdata = NULL;
  static int  allsize = -1;

  if (argc != 2)
  {
    Tcl_AppendResult (interp, "tget_data: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }


  /*
   * To get a value, we need to know its type in the midas data base 
   * even though it will always be a string in Tcl/Tk  
   * So: retrieve the key structure for this data, 
   */

  status = db_find_key(hDB, 0, argv[1] ,&hKeyS);
  if (status==DB_SUCCESS)
    {
      status=db_get_key(hDB, hKeyS, &key);  /* get key info, not the key ! */
      if(status != DB_SUCCESS)
      {
        sprintf (str_buff,"midas get_data: Can't get key for %s: %s",
		 argv[1],db_message_text(status));        
        Tcl_AppendResult (interp, str_buff, (char *) NULL);     
        return TCL_ERROR; 
      }
    } 
  else 
    {
      sprintf (str_buff,"midas get_data: Can't find key for %s: %s",
	       argv[1],db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
 
  /* Is there a clever way of doing this? Probably with db_sprintf */

  size = key.num_values * key.item_size;
  if( size > allsize ) /* use high-water-mark allocation */
    {
      if( pdata ) free( pdata );
      allsize = size+128;
      /* printf( "get_data allocates array storage of %d.\n", allsize); */
      pdata = malloc(allsize);
    }

  if(   key.type != TID_INT   &&  key.type != TID_DWORD &&  key.type != TID_STRING 
     && key.type != TID_FLOAT &&  key.type != TID_DOUBLE )
    {
      sprintf (str_buff,"midas get_data: key type (%d) not supported key %s",key.type,argv[1]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }

  status = db_get_data(hDB, hKeyS, pdata, &size, key.type);
  if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas get_data: error retrieving %s (%d) %s",
	       argv[1],status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }

  /* now copy the values into the output buffer for Tcl */
  for (i=0; i < key.num_values; i++)
    {
      if (key.type==TID_INT)
	sprintf (str_buff, "%d ",  *((INT *)(pdata+(i*key.item_size))));

      else if (key.type==TID_DWORD)
	sprintf (str_buff, "%d ",  *((DWORD *)(pdata+(i*key.item_size))));

      else if (key.type==TID_FLOAT) 
	sprintf (str_buff, "%g ",  *((float *)(pdata+(i*key.item_size))));

      else if (key.type==TID_DOUBLE)
	sprintf (str_buff, "%.14lg ", *((double *)(pdata+(i*key.item_size))));

      else if (key.type==TID_STRING)
      {
	size_e = Tcl_ScanElement((pdata+(i*key.item_size)), &flags);
	if (size_e > 254 )
	{
	    sprintf (str_buff,"midas get_data: string list element too large to handle (%d)",size_e);
	    Tcl_AppendResult (interp, str_buff, (char *) NULL);
	    return TCL_ERROR;
	}
	Tcl_ConvertElement((pdata+(i*key.item_size)), str_buff, flags);
	strcat(str_buff, " ");
      }

      Tcl_AppendResult (interp, str_buff, (char *) NULL); /* add each element */              
    }

  return TCL_OK;
}

/************************************************************************/

int tset_data_array
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
/* This routine writes data values to  array elements starting at index 0.
   Call from TCL with the following parameters:
      key name
      list of N values to write separated by spaces

      If less values are supplied than existing array elements in the odb, the first N
      elements will be written.
      If more values are supplied than existing array elements in the odb, the
      data will be truncated. Extra values will be ignored.

     Returns:
       
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message

*/
  
  KEY key;
  HNDLE hKeyS;

  INT size;
  INT index;
  int i;
  char *pdata = NULL;

  INT    data_INT;
  DWORD  data_DWORD;  
  float  data_FLOAT;
  double data_DOUBLE;
  int    listc, code;
  char **listv;
  char   arg_STRING [MAXSTRING] ;

  char temp[10];

  if (argc != 3)
  {
    Tcl_AppendResult (interp, "tset_data_array: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }



  /* to set a value, we need to know its type in the midas data base 
     even though it will always be a string in Tcl/Tk  
     So: retrieve the key structure for this data, */
  

  if(debug)
     {
       Tcl_AppendResult (interp,"tset_data_array starting", (char *) NULL);     
       sprintf(str_buff,"  with argv[1] (key name) = %s",argv[1]);
       Tcl_AppendResult (interp,str_buff, (char *) NULL);     
       sprintf(str_buff,"  and  argv[2]= (values) %s",argv[2]);
       Tcl_AppendResult (interp, str_buff, (char *) NULL);            
     }

  /* find hKeyS for call to set_key */
  status = db_find_key(hDB, 0, argv[1] ,&hKeyS);
  
  if (status==DB_SUCCESS)
    {
      db_get_key(hDB, hKeyS, &key);  /* get key info, not they key ! */
    } 
  else 
    {
      sprintf (str_buff,"midas_tcl tset_data_array: Can't get key for %s (%d) \n ",argv[1],status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  

  size = strnlen( argv[2], MAXSTRING-1 ) ;

  if(debug)
    {
      sprintf(str_buff,"midas_tcl tset_data_array: data string: %s",argv[2]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
    }

  code = Tcl_SplitList( interp, argv[2], &listc, (CONST char ***)&listv );
  if ( code == TCL_ERROR )
      return TCL_ERROR;
  
  if(debug)
    {
      sprintf(str_buff,"midas_tcl tset_data_array: Split list \"%s\" into %d elements",argv[2],listc);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
    }

  for ( index=0; index<listc; index++ )
  {
    if( index >= key.num_values )
    {
      if(debug)
        {
          sprintf(str_buff,"midas_tcl tset_data_array: Max. # elements (%d) of array %s reached.",
                  key.num_values,argv[1]);
          Tcl_AppendResult (interp, str_buff, (char *) NULL);
        }
      break;
    }
    if(debug)
    {
      strcpy(temp,listv[index]);
      sprintf(str_buff,"midas_tcl tset_data_array: element %d = %s",index,temp);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    }

    /* note - set_key (see below) is borrowed from odbedit. It handles data types etc. */
    status = set_key(hDB, hKeyS, index, index, listv[index]);
    if(status != DB_SUCCESS)
    {
      if (status == DB_NO_ACCESS)
      {
        sprintf(str_buff,"Write access not allowed");
        Tcl_AppendResult (interp, str_buff, (char *) NULL);
	Tcl_Free((char *) listv);
	return TCL_ERROR;
      }
      else
      {
        sprintf(str_buff,"midas_tcl tset_data_array: error returned by set_key for array index %d. Status= %d",index,status);
        Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	Tcl_Free((char *) listv);
        return TCL_ERROR;
      }
    }
  }
  Tcl_Free((char *) listv);
  return TCL_OK;
}
/**************************************************************************/


int tset_data
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{
/* This routine writes the same data word to array element(s) starting at index1,
   ending at index2.
   
   Call from TCL with the following parameters:
      argv[1]   key name
      argv[2]   start array index
      argv[3]   end array index
      argv[4]   data to write (same data written to all selected elements)

      The type of the data is handled automatically

     Returns:
       
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message

      
*/
  
  KEY key;
  HNDLE hKeyS;

  INT size;
  INT index1, index2;

  char data[80];


  if (argc != 5)
  {
    Tcl_AppendResult (interp, "tset_data: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }
  


  /* to set a value, we need to know its type in the midas data base 
     eventhough it will always be a string in Tcl/Tk  
     So: retrieve the key structure for this data, */
  
  
  if(debug)
     {
      sprintf(str_buff,"tset_data starting ");
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  with argv[1] (key name)         = %s",argv[1]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  and  argv[2]= (start at element)= %s",argv[2]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  and  argv[3]= (end   at element)= %s",argv[3]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  and  argv[4]= (data to write   )= %s",argv[4]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    }

  /* find hKeyS for call to set_key */
  status = db_find_key(hDB, 0, argv[1] ,&hKeyS) ;
  if (status==DB_SUCCESS)
  {
    status=db_get_key(hDB, hKeyS, &key);  /* get key info, not the key ! */
    if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tset_data: Can't get key for %s (%d)",argv[1],status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
    }
  } 
  else 
  {
    sprintf (str_buff,"midas_tcl tset_data: Can't find key for %s (%d) ",argv[1],status);
    Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    return TCL_ERROR;
  }

   
  size = strlen(argv[4]);
  strncpy (data,argv[4],size);
  data[size]='\0';
  size = sizeof(data);

  index1=atoi(argv[2]);
  index2=atoi(argv[3]);
  if(debug)
    {
      sprintf(str_buff,"midas_tcl tset_data: index1: %d index2: %d",index1,index2);
        Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    }
  /* note - set_key (see below) is borrowed from odbedit. It handles data types etc. */
  status = set_key(hDB, hKeyS, index1, index2, data);
  if(status != DB_SUCCESS)
  {
    sprintf(str_buff,"midas_tcl tset_data: Error return from set_key. Status= %d",index,status);
    Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    return TCL_ERROR;
  }
  return TCL_OK;
}
/**************************************************************************/

int tstop
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  KEY key;
  HNDLE hKeyS;
  
  char str[256], tmpstr[10];
  INT state,size,i,old_run_number,new_run_number;
  INT mthread_flag=FALSE;
  INT debug_flag, now_flag, again_flag;
  
  /* stop run
     "stop", "stop now" and "stop again "  and "stop again now" are supported
           
     input parameters:   (can be in any order)
     modifier           -> stop    (blank i.e. no modifier)
     now   -> stop now
     again -> stop again
     again now -> stop now again
     verbose -> stop verbose (debug)  or e.g. stop now again verbose
     
     Returns:
     
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message
      
     argc = no. arguments received
  */
  str_buff[0]='\0';

  debug_flag=now_flag=again_flag=0;

  if(debug)
    printf("no. of arguments argc = %d\n",argc);

  if(argc == 1)
    {
      if(debug)printf("tstop: received command stop (no arguments)\n");
    }
  else // argc > 1
    {
      if(debug)printf("tstop: received command stop with arguments: ");
      for(i=1; i<argc; i++)
	{
	  if(debug)printf("%s ",argv[i]);
	  if (argv[i][0] == 'v')
	    debug_flag=1;
	  else if (argv[i][0] == 'a')
	    again_flag=1;
	  else if (argv[i][0] == 'n')
	    now_flag=1;
	}
      if(debug)printf("\n");      
    }

  if(debug)printf("debug_flag=%d now_flag=%d again_flag=%d\n", debug_flag, now_flag, again_flag);
 
  /* this code lifted from odbedit.c (stop)  */
  
  
  /* check if run is stopped */
  state = STATE_STOPPED;
  size = sizeof(i);
  status=db_get_value(hDB, 0, "/Runinfo/State", &state, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tstop: Can't get value for /Runinfo/State (%d)",status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
    }
  if(debug)printf("debug is true\n");
  if (state == STATE_STOPPED)
    {
      printf("state is STOPPED; argc=%d \n",argc);
      if( again_flag  ) /* stop again  */
	{           
	  if(debug)
	    printf("midas_tcl tstop: Stop again is detected\n");
	}
      else
	{
          printf(" Run is already stopped\n");
	  sprintf(str_buff,
		  "midas_tcl tstop: Run is already stopped. To stop again, use stop again command");
	  
	  Tcl_AppendResult (interp, str_buff, (char *) NULL);         
	  return TCL_OK;
	} 
      
    } // end of STATE_STOPPED
      

  
  
  if(  now_flag  )   
    {  // stop now
      if(debug)printf("stop now detected; \n");
      Tcl_AppendResult (interp, "tstop: run will be stopped immediately", (char *) NULL);    
      status =
	cm_transition(TR_STOP | TR_DEFERRED, 0, str, sizeof(str), mthread_flag?TR_MTHREAD|TR_SYNC:TR_SYNC, debug_flag);
    }
  else
    {  // stop
      if(debug)printf("stopping run\n");
      Tcl_AppendResult (interp, "tstop: run will be stopped ", (char *) NULL);  
      status = cm_transition(TR_STOP, 0, str, sizeof(str), mthread_flag?TR_MTHREAD|TR_SYNC:TR_SYNC, debug_flag);
      
      if (status == CM_DEFERRED_TRANSITION)
	{
	  sprintf (str_buff,"midas_tcl tstop: %s (%d)",str,status);
	  Tcl_AppendResult (interp, str_buff, (char *) NULL);    
	  return TCL_ERROR; 
	}
      else if (status == CM_TRANSITION_IN_PROGRESS)
	{
	  if(debug)printf("deferred stop detected\n");
	  sprintf
	    (str_buff, "midas_tcl tstop: Deferred stop already in progress, enter \"stop now\" to force stop\n");
	  Tcl_AppendResult (interp, str_buff, (char *) NULL);  
	  return TCL_ERROR; 
	}
      else if (status != CM_SUCCESS)
	{
	  if(debug)printf("error : %s (%d)\n",str,status);
	  sprintf (str_buff,"midas_tcl tstop: Error -  %s (%d)",str,status);
	  Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	  return TCL_ERROR; 
	}
    }
  return TCL_OK;
}


/************************************************************************/

int tstart
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  
  KEY key;
  HNDLE hKeyS;
  
  char  str[256], tmpstr[10];
  INT size,i,old_run_number,new_run_number;
  INT mthread_flag=FALSE;
  INT debug_flag;
  INT stat;

/* start run NOW
   NOTE : calling pgm should fill edit-on-start parameters

   input parameters:
      verbose (debug)

   Returns:
       
     TCL_OK     success  & string containing value read from key name
     TCL_ERROR  fail     & string containing failure message
*/

  if(argc > 1 && argv[1][0] == 'v') 
    debug_flag=1;
  else
    debug_flag=0;
  
  /*   this code lifted from midas's odbedit.c (start now)  */
  
  if(debug) printf("tstart: starting\n");
  
  /* check if run is already started */
  size = sizeof(i);
  i = STATE_STOPPED;
  status = db_get_value(hDB, 0, "/Runinfo/State", &i, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
  {
    sprintf (str_buff,"midas_tcl tstart: Can't get value for /Runinfo/State (%d)",status);
    Tcl_AppendResult (interp, str_buff, (char *) NULL);         
    return TCL_ERROR; 
  }
  
  
  if (i == STATE_RUNNING)
  {
    sprintf(str_buff,"midas_tcl tstart: Run is already started");
    Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    return TCL_OK;
  }
  else if (i == STATE_PAUSED)
  {
    sprintf(str_buff,"midas_tcl tstart: Run is paused, please use \"resume\"");
    Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    return TCL_OK;       
  }

  /* check if transition in progress */
  i = 0;
  status = db_get_value(hDB, 0, "/Runinfo/Transition in progress", &i, &size,
			TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tstart: Can't get value for /Runinfo/Transition in progress (%d)",status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
    }
  if (i == 1)
    {
      sprintf(str_buff,
	      "midas_tcl tstart: Start/Stop is already in progress. \n Please try later or set \"/Runinfo/Transition in progress\" manually to zero.");
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;       
    }
  
  /* perform a run number check */
  
  /* get present run number */
  old_run_number = 0;
  status = db_get_value(hDB, 0, "/Runinfo/Run number", &old_run_number, &size,
			TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tstart: Can't get value for /Runinfo/Run number (%d)",status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
    }
  
  /* command has got to be   "start now"  */
  new_run_number = old_run_number + 1;  // increment run number
  /* start run */
  if(debug)printf("Starting run #%d\n", new_run_number);
  sprintf(str_buff,"Starting run #%d\n", new_run_number);
  Tcl_AppendResult (interp, str_buff, (char *) NULL); 
  
  status = cm_transition(TR_START, new_run_number, str, sizeof(str),  mthread_flag?TR_MTHREAD|TR_SYNC:TR_SYNC, debug_flag);
  if (status != CM_SUCCESS)
  {
     if(debug)printf("tstart: Error after cm_transition: %s (%d)\n",str,status);

     /* there is an error from cm_transition */
     sprintf(str_buff,"midas_tcl tstart: Error - %s (%d)",str,status);
     Tcl_AppendResult (interp, str_buff, (char *) NULL);         
     
    /* in case of error, reset run number */
    stat = db_set_value(hDB, 0, "/Runinfo/Run number", &old_run_number, size, 1, TID_INT);
    if(stat != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tstart: Can't set value for /Runinfo/Run number (%d)",stat);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
    }
  }
 
  if (status  != CM_SUCCESS || stat != CM_SUCCESS)
    return TCL_ERROR;
  else
    return TCL_OK;
}


/********************************************************************/
/* this function borrowed from odbedit.c, by  Stefan Ritt
   changed to return status (void->int) and explanation of parameters added
 */
int set_key(HNDLE hDB, HNDLE hKey, int index1, int index2, char *value)
{
/* This routine (from odbedit.c) writes one data value to an array in the odb,
   handling the key type automatically by a call to db_get_key.
   
   To write the data to one element only, index1 = index2 = array element
   To write the (same) data to > one element, index1 = start element, index2 = end element

   Inputs:
   HNDLE hDB          Handle to online database
   HNDLE hKey         Handle of key to set
   int index1         First array element to be written
   int index2         Last array element to be written
   char *value        Data (one value only)

To write different data to different elements, this routine must be called repeatedly.  
 */  
KEY  key;
char data[1000];
int  i, size;


  status = db_get_key(hDB, hKey, &key);
  if(status != DB_SUCCESS)
    return (status); 

  
  memset(data, 0, sizeof(data));
  db_sscanf(value, data, &size, 0, key.type);

  /* extend data size for single string if necessary */
  if ((key.type == TID_STRING || key.type == TID_LINK) 
        && (int) strlen(data)+1 > key.item_size &&
      key.num_values == 1)
    key.item_size = strlen(data)+1;

  if (key.item_size == 0)
    key.item_size = rpc_tid_size(key.type);

  if (key.num_values > 1 && index1 == -1)
    {
    for (i=0 ; i<key.num_values ; i++)
      status = db_set_data_index(hDB, hKey, data, key.item_size, i, key.type);
    }
  else if (key.num_values > 1 && index2>index1)
    {
    for (i=index1 ; i<key.num_values && i<=index2; i++)
      status = db_set_data_index(hDB, hKey, data, key.item_size, i, key.type);
    }
  else if (key.num_values > 1 || index1 > 0)
    status = db_set_data_index(hDB, hKey, data, key.item_size, index1, key.type);
  else
    status = db_set_data(hDB, hKey, data, key.item_size, 1, key.type);
  return(status);
}
   



/************************************************************************/

int tsave
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  KEY key;
  HNDLE hKeyS;

  INT    data_INT;
  DWORD  data_DWORD;  
  float  data_FLOAT;
  BOOL   data_BOOL;
  double data_DOUBLE;
  INT size;


  /* save the database at current postion into a file in ASCII format

     Input parameters:
       key        "current" position at which to save database
       filename   file in which to save database

     Returns:
       
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message


          code lifted from midas's odbedit.c

  */
  if (argc != 3)
  {
    Tcl_AppendResult (interp, "tsave: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }
  
  if(debug) 
    printf("tsave: starting with arguments: %s\n     and %s\n", argv[1], argv[2]);
  
  status =   db_find_key(hDB, 0, argv[1] ,&hKeyS);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tsave: Can't find key  %s,(%d)",argv[1],status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
    }
 else
   if(debug)printf("tsave: found key %s\n", argv[1]);


  status = db_save(hDB, hKeyS, argv[2], FALSE);
  if(status != DB_SUCCESS)
  {
    sprintf (str_buff,
             "midas_tcl tsave: Can't save odb structure at key %s\n  into file %s,(%d)",
             argv[1], argv[2], status);
    Tcl_AppendResult(interp, str_buff, (char *) NULL);
    return TCL_ERROR;
  }
  sprintf (str_buff,
           "midas_tcl tsave: Saved odb structure at key %s \n into file %s",
           argv[1], argv[2]);
  Tcl_AppendResult(interp, str_buff, (char *) NULL);
  return TCL_OK;
  
}
/************************************************************************/


int tload
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  KEY key;
  HNDLE hKeyS;

  INT    data_INT;
  DWORD  data_DWORD;  
  float  data_FLOAT;
  BOOL   data_BOOL;
  double data_DOUBLE;
  INT size;


  /* load the database from file 
      Note: the odb file being loaded contains the absolute pathname

     Input parameters:
     filename   file from which to load database
     
     Returns:
       
     TCL_OK     success  & string containing value read from key name
     TCL_ERROR  fail     & string containing failure message
       

      code lifted from midas's odbedit.c

  */
  if (argc != 2)
  {
    Tcl_AppendResult (interp, "tload: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }

  if(debug) 
    { 
      sprintf(str_buff, "argument 1(filename) is %s", argv[1]);
      Tcl_AppendResult(interp, str_buff, (char *) NULL);
    }
  

  
  /* the  file being loaded should  contain the absolute pathname, and therefore will load into
     the right place (creating keys if they don't exist)
  */

  status =   db_find_key(hDB, 0, "/" ,&hKeyS); /* key for toplevel */
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tload: Error finding key for path \"/\" (%d) %s",
	       argv[1],status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
    }
 

  status = db_load(hDB, hKeyS, argv[1], FALSE);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff, "midas_tcl tload: Can't load file %s,(%d)", argv[1], status);
      Tcl_AppendResult(interp, str_buff, (char *) NULL);
      return TCL_ERROR;
    }

  sprintf (str_buff,"midas_tcl tload: odb has been loaded from file %s",argv[1]); 
  return TCL_OK;
}
/************************************************************************/


int tpause
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
/* Pause the run 
       "pause", "pause now" are supported
       
      input parameters:
      modifier           -> pause    (blank i.e. no modifier)
                   now   -> pause now
		   suppress -> pause but suppress saving the data by mdarc
                                (i.e. run is about to be killed)
                   verbose   -> pause verbose (debug)  or pause now/suppress verbose
     
     Returns:
       
     TCL_OK     success  & string containing informational message
     TCL_ERROR  fail     & string containing failure message
       

     NOTE: for midas 1.9.0 there is an extra parameter added to cm_transition to
     allow debugging. This hardcoded as FALSE at present, but it may be added later
     as another parameter. 
 
      
   argc = no. arguments received

   this code lifted from odbedit.c (pause)  

*/
  INT i,size,state;
  char str[256];
  KEY key;
  HNDLE hKeyS;
  
  char str2[]="/Equipment/MUSR_TD_acq/mdarc/suppress_save_temporarily";
  BOOL data_BOOL = 1;
  INT debug_flag, now_flag, suppress_flag;
  
  str_buff[0]='\0';


  debug_flag=now_flag=suppress_flag=0;
  if(debug)
    printf("no. of arguments argc = %d\n",argc);

  if(argc == 1)
    {
      if(debug)printf("tpause: received command pause (no arguments)\n");
    }
  else // argc > 1
    {
      if(debug)printf("tpause: received command pause with arguments: ");
      for(i=1; i<argc; i++)
	{
	  if(debug)printf("%s ",argv[i]);
	  if (argv[i][0] == 'v')
	    debug_flag=1;
	  else if (argv[i][0] == 's')
	    suppress_flag=1;
	  else if (argv[i][0] == 'n')
	    now_flag=1;
	}
      if(debug)printf("\n");      
    }

  if(debug)printf("debug_flag=%d now_flag=%d suppress_flag=%d\n", debug_flag, now_flag, suppress_flag);
  

  /* check if run is started */
  i = STATE_STOPPED;
  size = sizeof(i);
  status = db_get_value(hDB, 0, "/Runinfo/State", &i, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tpause: Can't get value for /Runinfo/State (%d)",status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
    }
  
  if (i != STATE_RUNNING)
    {
      sprintf(str_buff,"midas_tcl tpause: Run is not started\n");
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_OK;
    }
  

  if ( suppress_flag )
    {   /* set the key for mdarc to suppress the save (suppress_save_temporarily) */
      if(debug)printf("suppressing save, setting %s to 1\n",str2);
      status = db_find_key(hDB, 0, str2 ,&hKeyS);
      if (status==DB_SUCCESS)
	{
	  status=db_get_key(hDB, hKeyS, &key);  /* get key info, not the key ! */
	  if(status != DB_SUCCESS)
	    {
	      sprintf (str_buff,"midas_tcl tpause: Can't get key for %s (%d)",str2,status);
	      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
	      return TCL_ERROR; 
	    }
	} 
      else 
	{
	  sprintf (str_buff,"midas_tcl tpause: Can't find key for %s (%d) ",str2,status);
	  Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	  return TCL_ERROR;
	}
      
      
      if (key.type != TID_BOOL) 
	{
	  sprintf (str_buff,"midas_tcl tpause: expect %s to be BOOL",str2,status);
	  Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	  return TCL_ERROR;
	}
      size = sizeof(data_BOOL);
      status = db_set_value (hDB,0,str2, &data_BOOL, size, 1, key.type);
      if (status != DB_SUCCESS)
	{
	  sprintf (str_buff,"midas_tcl tpause: error from set_value for \"%s\", (%d)",str2,status);
	  Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	  return TCL_ERROR; 
	}
    } /* key suppress_save_temporarily is set 
         it will be cleared again by kill.pl after run is killed  */
  
  

  if ( now_flag )
    {    
      if(debug)printf("pause now... sending TR_PAUSE|TR_DEFERRED\n");
      status = cm_transition(TR_PAUSE | TR_DEFERRED, 0, str, sizeof(str), TR_SYNC, debug_flag);
    }
  else
    {
       status = cm_transition(TR_PAUSE, 0, str, sizeof(str), TR_SYNC, debug_flag);
    }

  if (status == CM_DEFERRED_TRANSITION)
    {       /*  printf("%s\n", str); */
      sprintf(str_buff,"midas_tcl tpause: %s",str);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_OK;
    }
  else if (status == CM_TRANSITION_IN_PROGRESS)
    {
      sprintf(str_buff,
	      "midas_tcl tpause: Deferred pause already in progress, enter \"pause now\" to force pause\n");
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_OK;
    }
  else if (status != CM_SUCCESS)
    {
      sprintf(str_buff,"midas_tcl tpause: Error: %s", str);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR;
    }

  return TCL_OK;
}

/************************************************************************/

int tresume
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  
  INT i,size,state;
  char str[256];
  INT debug_flag;


/* Resume the run 
       "resume"
       
      input parameters:
      verbose
     
     Returns:
       
     TCL_OK     success  & string containing informational message
     TCL_ERROR  fail     & string containing failure message
       

     NOTE: for midas 1.9.0 there is an extra parameter added to cm_transition to
     allow debugging. This hardcoded as FALSE at present, but it may be added later
     as another parameter. 
 
      
   argc = no. arguments received
*/
  
  str_buff[0]='\0';
  if(debug)
  {
      printf("tresume: no. of arguments argc = %d\n",argc);
      if(argc == 1)
          printf("midas_tcl tresume: received command \"resume\" \n");
      else
          printf("midas_tcl tresume: argument \"%s\"  \n",argv[1]);
  }
  
  if(argc > 1 && argv[1][0] == 'v') 
    debug_flag=1;
  else
    debug_flag=0;


  /* this code lifted from odbedit.c (resume)  */


  /* resume */
  
  /* check if run is paused */
  i = STATE_STOPPED;
  size = sizeof(i);
  status = db_get_value(hDB, 0, "/Runinfo/State", &i, &size, TID_INT, FALSE);
  if(status != DB_SUCCESS)
  {
      sprintf (str_buff,"midas_tcl tresume: Can't get value for /Runinfo/State (%d)",status);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_ERROR; 
  }

  if (i != STATE_PAUSED)
  {
      sprintf (str_buff,"midas_tcl tresume: Run is not paused\n");
      Tcl_AppendResult (interp, str_buff, (char *) NULL);         
      return TCL_OK;
  }
  else
  {
      status = cm_transition(TR_RESUME, 0, str, sizeof(str), TR_SYNC, debug_flag );
      if (status != CM_SUCCESS)
      {
          sprintf(str_buff,"midas_tcl tresume: Error: %s", str);
          Tcl_AppendResult (interp, str_buff, (char *) NULL);         
          return TCL_ERROR;
      }
  }
  return TCL_OK;
}

/************************************************************************/

int todb_cmd
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     

  /* Send command: odbedit -h host -e experiment -c "command"

  This command is for testing
   no need to get the database etc first because odb command mode handles that
   
    Input parameters:
       host            hostname
       experiment      experiment
       command_string  odb command to execute

       Returns:
       
     TCL_OK     success  & string containing informational message
     TCL_ERROR  fail     & string containing failure message
     
   Information returned is ONLY printed in mtcl debug window
    - it is NOT RETURNED to TCL calling program

  */

  INT i,size,state;
  char str[256];
  INT pid;
      
 
/*      
   argc = no. arguments received
*/
  if (argc < 4)
    {
      Tcl_AppendResult (interp, "todb_cmd: not enough parameters supplied", (char *) NULL); 
      return TCL_ERROR; 
    }
  if(debug)printf("Required input parameters: host=%s, experiment=%s \n",argv[1],argv[2]);
  pid = ss_gettid();
  if(debug)printf("pid=%d\n",pid);

  /*  sprintf(str,"odbedit -e musr2 -c \'");
    for (i=1; i<argc; i++)
    strcat(str,argv[i]);
  strcat(str,"\'");
  */
  sprintf(str, "odbedit -h %s -e %s  -c \'",argv[1],argv[2]);
  for (i=3; i<argc; i++)
    strcat(str,argv[i]);
  strcat(str,"\'");
  if(debug)printf("Sending system command:\"%s\"\n",str);
  
   status =  system(str);
   if(status)
   {
     printf("bad status (%d) returned from system command\n",status);
     return TCL_ERROR;
   }  
  
  return TCL_OK;
}
/************************************************************************/

/************************************************************************/

int tenv_odb_cmd
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     

  /* Send command: odbedit  -c "command"
        Environment variables MIDAS_SERVER_HOST and MIDAS_EXPT_NAME are set up

  This command is for testing
   no need to get the database etc first because odb command mode handles that
   
    Input parameters:
       command_string  odb command to execute

       Returns:
       
     TCL_OK     success  & string containing informational message
     TCL_ERROR  fail     & string containing failure message
     
   Information returned is ONLY printed in mtcl debug window
    - it is NOT RETURNED to TCL calling program

  */

  INT i,size,state;
  char str[256];
  INT pid;
      
 
/*      
   argc = no. arguments received
*/
  if (argc < 2)
    {
      Tcl_AppendResult (interp, "tenv_odb_cmd: odb command not supplied", (char *) NULL); 
      return TCL_ERROR; 
    }
 
  pid = ss_gettid();
  if(debug)printf("pid=%d\n",pid);

 
  sprintf(str, "odbedit  -c \'");
  for (i=1; i<argc; i++)
    strcat(str,argv[i]);
  strcat(str,"\'");
  if(debug)printf("Sending system command:\"%s\"\n",str);
  
   status =  system(str);
   if(status)
   {
     printf("bad status (%d) returned from system command\n",status);
     return TCL_ERROR;
   }  
  
  return TCL_OK;
}
/************************************************************************/





/************************************************************************/


int tmsg
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  
  INT i,size,state, msg_type;
  char message[256];
  char user_name[80] = "";

/* Use Midas msg command - sends a message and writes it in midas.log
    this pgm uses odb command mode
    

       
      3 input parameters:
      type   DIGIT    one of MT_ERROR, MT_INFO, MT_LOG, MT_TALK, MT_USER etc.
                      default = MT_USER=2
      user   string   user name  default = "script"
      msg    string   message string
    
      2 input paramsters:
      user   string   user name   
      msg    string   message string

      1 input paramsters:
      msg    string   message string
     
      
     Returns:
       
     TCL_OK     success 
     TCL_ERROR  fail     & string containing failure message
       

 
      
   argc = no. arguments received
*/
  
  str_buff[0]='\0';
  if(debug)
  {
      printf("tmsg: no. of arguments argc = %d\n",argc);
      if(argc <2)
      {
          sprintf(str_buff,"midas_tcl tmsg: msg expects at least 1 argument");
          Tcl_AppendResult (interp, str_buff, (char *) NULL);         
          return TCL_ERROR;
      }
  }
  
  
  /* this code lifted from odbedit.c (msg) 
       - cmd_mode is set
  */

  /*  interactive user message type by default */
  msg_type = MT_USER;
  message[0] = 0;

  strcpy(user_name, "script");

  if(argc > 3)   /*  msg plus all 3 arguments supplied */
  {
      msg_type = atoi(argv[1]);
      strcpy(user_name, argv[2]);
      strcpy(message, argv[3]);
      if(debug)
	printf("tmsg: 3 arguments, msg_type=%d,user_name=\"%s\", message=\"%s\"\n",
	     msg_type,user_name,message);
      if (msg_type < 1) 
	{
	  printf("tmsg: msg_type is invalid (%d); setting it to MT_INFO\n",msg_type);
	  msg_type = MT_INFO; /* eliminate bad parameter */
	}
  }
  else if (argc == 3) /*  msg plus two arguments */
    {
      strcpy(user_name, argv[1]);
      strcpy(message, argv[2]);
      if(debug)
	printf("tmsg: 2 arguments, user_name=\"%s\", message=\"%s\"\n",user_name,message);
    }

  else if (argc == 2) /*  msg plus one argument */
    {
      strcpy(message, argv[1]);
      if(debug) printf("tmsg: 1 arguments, message=\"%s\"\n",message);
    }


  if (message[0])
      cm_msg(msg_type, __FILE__, __LINE__, user_name, message);
  else
    if(debug) printf("tmsg: message blank - message not sent\n");
  return TCL_OK;
}

/************************************************************************/

int thot_save
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     

  /*   write a MUD saved_file now
       To do this, write to "/Equipment/MUSR_TD_acq/mdarc/histograms/musr/save now" which is hot-linked
       by mdarc

       ---------------------------------------------------------------------------------------
       - NOTE: this will only result in a saved file if the run is going, and mdarc is running -
       ---------------------------------------------------------------------------------------

     Input parameters:
       none
       
       Returns:
     
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message
       
       
       code lifted from midas odbedit.c

  */
       KEY key;
     HNDLE hKeyS;
     
     INT size;
     char str[]="/Equipment/MUSR_TD_acq/mdarc/histograms/musr/save now";
     BOOL data_BOOL = 1;
     
     
     if(debug) 
       printf("thot_save: starting\n");
     
     status = db_find_key(hDB, 0, str ,&hKeyS);
     if (status==DB_SUCCESS)
       {
	 status=db_get_key(hDB, hKeyS, &key);  /* get key info, not the key ! */
	 if(status != DB_SUCCESS)
	   {
	     sprintf (str_buff,"midas_tcl thot_save: Can't get key for %s (%d)",str,status);
	     Tcl_AppendResult (interp, str_buff, (char *) NULL);         
	     return TCL_ERROR; 
	   }
       } 
     else 
       {
	 sprintf (str_buff,"midas_tcl thot_save: Can't find key for %s (%d) ",str,status);
	 Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	 return TCL_ERROR;
       }
     
     
     if (key.type !=TID_BOOL) 
       {
	 sprintf (str_buff,"midas_tcl thot_save: expect %s to be BOOL",str,status);
	 Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	 return TCL_ERROR;   
       }
     
     size = sizeof(data_BOOL);
     status=db_set_value (hDB,0,str, &data_BOOL, size, 1, key.type);
     if (status != DB_SUCCESS)
       {
	 sprintf (str_buff,"midas_tcl thot_save: error from set_value for \"%s\", (%d)",str,status);
	 Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	 return TCL_ERROR; 
       }
     
     return TCL_OK;
     
}
/************************************************************************/



int tset_value
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  KEY key;
  HNDLE hKeyS;

  INT    data_INT;
  DWORD  data_DWORD;  
  float  data_FLOAT;
  BOOL   data_BOOL;
  double data_DOUBLE;
  INT size;



  /* to set a value, we need to know its type in the midas data base 
     even though it will always be a string in Tcl/Tk  
     So: retrieve the key structure for this data

     Input parameters:
     key name     name of midas odb key ( refers to a single value)
     value        value to write

     Returns:
     TCL_OK     success
     TCL_ERROR  fail     & string containing failure message
  */
  if (argc != 3)
  {
    Tcl_AppendResult (interp, "tset_value: not enough parameters supplied", (char *) NULL); 
    return TCL_ERROR; 
  }

  
  sprintf(str_buff,"%s",argv[2]); /* may be overwritten by error message */
  if(debug)
    {
      printf("tset_value starting with argv[1]= %s and argv[2]= %s\n",
        argv[1],argv[2]);
    }

  status = db_find_key(hDB, 0, argv[1] ,&hKeyS);
  if (status==DB_SUCCESS)
    {
      status=db_get_key(hDB, hKeyS, &key);  /* get key info, not the key ! */
      if(status != DB_SUCCESS)
      {
        sprintf (str_buff,"midas_tcl tset_value: Can't get key for %s (%d) %s",
		 argv[1],status,db_message_text(status));
	Tcl_AppendResult (interp, str_buff, (char *) NULL);         
        return TCL_ERROR;
      }
    } 
  else 
    {
      sprintf (str_buff,"midas_tcl tset_value: Can't find key for %s (%d) %s",
	       argv[1],status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
   
  /* is there a clever way of doing this ? Probably, but I am not so smart */

  if (key.type==TID_INT) 
  {
    data_INT = atoi (argv[2]);
    if(debug)
    {
      sprintf(str_buff,"data_INT = %d",data_INT);
      Tcl_AppendResult (interp, str_buff, (char *) NULL); 
    }
    size = sizeof(data_INT);
    status=db_set_value (hDB,0,argv[1], &data_INT, size, 1, key.type);
    if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tset_value: error from db_set_value (INT), (%d) %s",
	       status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR;
    }
  }
  else if (key.type==TID_DWORD) 
  {
    data_DWORD = atol (argv[2]);
    if(debug)
    {
      sprintf(str_buff,"data_DWORD = %d",data_DWORD);
      Tcl_AppendResult (interp, str_buff, (char *) NULL); 
    }
    size = sizeof(data_DWORD);
    status=db_set_value (hDB,0,argv[1], &data_DWORD, size, 1, key.type);
    if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tset_value: error from db_set_value (DWORD), (%d) %s",
	       status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
        return TCL_ERROR; 
    }
  }
  else if (key.type==TID_FLOAT) 
  {
    data_FLOAT = atof (argv[2]);
    if(debug) {
      sprintf(str_buff,"data_FLOAT = %f",data_FLOAT);
      Tcl_AppendResult (interp, str_buff, (char *) NULL); 
    }
    size = sizeof(data_FLOAT);
    status=db_set_value (hDB,0,argv[1], &data_FLOAT, size, 1, key.type);
    if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tset_value: error from db_set_value (FLOAT), (%d) %s",
	       status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      return TCL_ERROR; 
    }
  }
  else if (key.type==TID_DOUBLE) 
  {
    data_DOUBLE = atof (argv[2]);
    if(debug) {
      sprintf(str_buff,"data_DOUBLE = %lf",data_DOUBLE);
      Tcl_AppendResult (interp, str_buff, (char *) NULL); 
    }
    size = sizeof(data_DOUBLE);
    status=db_set_value (hDB,0,argv[1], &data_DOUBLE, size, 1, key.type);
    if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tset_value: error from db_set_value (DOUBLE), (%d) %s",
	       status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR; 
    }
  }  
  else if (key.type==TID_STRING) 
  {
    char  data_STRING[MAXSTRING];
    INT   len;
    BOOL  flag;
    
    size = key.item_size; /* size of key in odb */
    len = strlen(argv[2]); /* size of string to write */
    if (debug)
    {
      printf ("tset_value: key type is a STRING\n");
      printf ("   key.item_size = %d, string length =%d, MAXSTRING=%d\n",
              size, len, MAXSTRING);
    }
    
    if(size > MAXSTRING)   /*  if key size > temporary string length */
    {     /* return an error of key size will be changed permanently in odb */
      sprintf(str_buff,
              "midas_tcl tset_value: Error - odb key size (%d) > max buffer size (%d)",
              size,MAXSTRING);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      return TCL_ERROR;
    }

    if(len > size)
    {   /* truncate the string before writing */
      len=size;
      sprintf(str_buff,"midas_tcl tset_value: Warning - string truncated to key size (%d)",size);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
    }
   
    strncpy (data_STRING,argv[2],len);
    data_STRING[len]='\0';
   
    if(debug)
      printf ("data string: %s; size = %d string length %d\n",
              data_STRING, size, strlen(data_STRING)); 
    
    status=db_set_value (hDB,0,argv[1], data_STRING, size, 1, key.type);
    if (status != DB_SUCCESS)
    {
      sprintf (str_buff,"midas_tcl tset_value: error from db_set_value (STRING), (%d) %s",
	       status,db_message_text(status));
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      return TCL_ERROR; 
    }
    
  }
  else if (key.type==TID_BOOL) 
  {
    
    data_BOOL = atoi (argv[2]);
    if(debug)
      {
        sprintf(str_buff,"data_BOOL = %i",data_BOOL);
        Tcl_AppendResult (interp, str_buff, (char *) NULL); 
      }
    size = sizeof(data_BOOL);
    status=db_set_value (hDB,0,argv[1], &data_BOOL, size, 1, key.type);
    if (status != DB_SUCCESS)
      {
	sprintf (str_buff,"midas_tcl tset_value: error from db_set_value (BOOL), (%d) %s",
		 status,db_message_text(status));
	Tcl_AppendResult (interp, str_buff, (char *) NULL);     
        return TCL_ERROR; 
      }
    }
  else
  {
    sprintf (str_buff,"midas_tcl tset_value: error - var \"%s\" data type (%d) not yet handled in TCL", argv[1], key.type);
    Tcl_AppendResult (interp, str_buff, (char *) NULL); 
    return TCL_ERROR; 
  }
  
  
  return TCL_OK;
}


/************************************************************************/

int tscl
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  /* This routine performs the scl (show clients) command of odbedit.  Code taken
   * from odbedit.c
   */
  /* input parameters: (none, but argc and argv are passed anyway)

     Returns    For      With tcl_result
     TCL_OK     success  flat list of client, host pairs
     TCL_ERROR  fail     string containing failure message
   */
  int i, size;
  HNDLE hSubkey;
  int size_e;
  int flags;
  char name[256];

  status = db_find_key(hDB, 0, "System/Clients", &hKey);
  if (status != DB_SUCCESS) {
    Tcl_AppendResult (interp, "cannot find System/Clients entry in database", (char *) NULL); 
    return TCL_ERROR; 
  }
  else
  {
    /* search database for clients with transition mask set */
    for (i = 0, status = 0;; i++) 
      {
        status = db_enum_key(hDB, hKey, i, &hSubkey);
        if (status == DB_NO_MORE_SUBKEYS)
          break;

        if (status == DB_SUCCESS) 
          {
            size = sizeof(name);
            db_get_value(hDB, hSubkey, "Name", name, &size, TID_STRING, TRUE);
            name[63]='\0';
            size_e = Tcl_ScanElement(name, &flags);
            if ( size_e > sizeof(str_buff) )
              {
                sprintf (str_buff,"midas_tcl tscl: string list element too large to handle (%d)",size_e);
                Tcl_AppendResult (interp, str_buff, (char *) NULL);
                return TCL_ERROR;
              }
            Tcl_ConvertElement(name, str_buff, flags);
            strcat(str_buff, " ");
            Tcl_AppendResult (interp, str_buff, (char *) NULL); /* add each element */              

            size = sizeof(name);
            db_get_value(hDB, hSubkey, "Host", name, &size, TID_STRING, TRUE);
            name[63]='\0';
            size_e = Tcl_ScanElement(name, &flags);
            if ( size_e > sizeof(str_buff) )
              {
                sprintf (str_buff,"midas_tcl tscl: string list element too large to handle (%d)",size_e);
                Tcl_AppendResult (interp, str_buff, (char *) NULL);
                return TCL_ERROR;
              }
            Tcl_ConvertElement(name, str_buff, flags);
            strcat(str_buff, " ");
            Tcl_AppendResult (interp, str_buff, (char *) NULL); /* add each element */              

          }
      }

    return TCL_OK;
  }
}

/************************************************************************/

int tyield
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{     
  /* This routine performs the yield() function required of good Midas citizens.
   * It should be invoked at short regular intervals by a midas_tcl program (command
   * [midas yield]) and the program should disconnect and exit when the result is
   * "SHUTDOWN".
   */
  /* input parameters: (none, but argc and argv are passed anyway)

     Returns    For      With tcl_result
     TCL_OK     success  string result: OK or SHUTDOWN
     TCL_ERROR  fail     string containing failure message
   */   

  /*
   *  The time last checked is static to compare between calls: we will not check
   *  more often than every 200.
   *  The status is static so it is a permanent toggle: once a shutdown request
   *  is detected, that's what will always be returned.
   */

  static DWORD last_checked = 0;
  static INT mstat = SS_SUCCESS;

  if ( mstat != RPC_SHUTDOWN && mstat != SS_ABORT )
  {
      if ( ss_millitime() - last_checked > 200)
      {
          mstat = cm_yield(0);
          last_checked = ss_millitime();
      }
  }
  if ( mstat == RPC_SHUTDOWN || mstat == SS_ABORT ) {
      Tcl_AppendResult (interp, "SHUTDOWN", (char *) NULL);
      return TCL_OK;
  }
  if ( mstat != SS_SUCCESS && mstat != SS_TIMEOUT ) {
      sprintf( str_buff, "midas_tcl tyield: error from cm_yield (%d)", mstat );
      Tcl_AppendResult (interp, str_buff, (char *) NULL);
      mstat = SS_SUCCESS;
      return TCL_ERROR;
  }

  Tcl_AppendResult (interp, "OK", (char *) NULL);
  mstat = SS_SUCCESS ;
  return TCL_OK;
}


/*   do we really need these ?  */
/************************************************************************/

int tget_n_bins
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{ 
  /*takes two argument, argv[1] tdc_res code sec/bin and argv[2] histogram length(in microseconds)
    in which case the length in num of bins is returned.
  */
  
  KEY key;
  HNDLE hKeyS;

  INT    data_INT;
  DWORD  data_DWORD;  
  float  data_FLOAT;
  BOOL   data_BOOL;
  double data_DOUBLE;
  INT size;
  

  double length;
  double tdc_res;
  int n_bins;
    
    
  if(debug)
    {
      sprintf(str_buff,"tget_n_bins starting");
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  with argv[1]= %s",argv[1]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  and  argv[2]= %s",argv[2]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    }

  tdc_res = atof(argv[1]);
  length = atof(argv[2]);
  n_bins = length / tdc_res;


  /* if success */

  sprintf (str_buff, "%i", n_bins);
  Tcl_AppendResult (interp, str_buff, (char *) NULL);
  return TCL_OK;

}

int tget_length
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{ 
  /*takes two argument, argv[1] tdc_res code sec/bin and argv [2] histogram length(num of bins)
    in which case the length in seconds is returned.
  */
  
  KEY key;
  HNDLE hKeyS;

  INT    data_INT;
  DWORD  data_DWORD;  
  float  data_FLOAT;
  BOOL   data_BOOL;
  double data_DOUBLE;
  INT size;

  double length;
  double tdc_res;
  int n_bins;
        
  if(debug)
    {
      sprintf(str_buff,"tget_length starting");
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  with argv[1]= %s",argv[1]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
      sprintf(str_buff,"  and  argv[2]= %s",argv[2]);
      Tcl_AppendResult (interp, str_buff, (char *) NULL);     
    }

  tdc_res = atof(argv[1]);
  n_bins = atoi(argv[2]);
  length = tdc_res * n_bins;

  /* if success */

  if(debug)printf ("length is %e", length);
  sprintf (str_buff, "%e", length);
  Tcl_AppendResult (interp, str_buff, (char *) NULL);
  return TCL_OK;

}


/*  And do we need this ???? 
/************************************************************************/


int tbin_round_up
_ANSI_ARGS_((ClientData clientData, Tcl_Interp *interp, int argc,char *argv[]))
{ 
  /*takes one argument, argv[1], int, number of bins,
    rounded up num of bins is returned.
  */
 
  
  int nbins, data_field, data_mask,  overfl_mask, i;
  
    
    if(debug) 
      { 
	sprintf(str_buff,"tbin_round_up starting");
	Tcl_AppendResult (interp, str_buff, (char *) NULL);     
	sprintf(str_buff,"  with argv[1]= %s",argv[1]);   
	Tcl_AppendResult (interp, str_buff, (char *) NULL); 
      }
    

  nbins = atof (argv[1]) ;

  i = 0;
  while ((0x1<<i++) < nbins);
  if(debug)printf("index i=%d, nbins=%d\n",i,nbins);
  data_field = --i;
  if(debug)printf("data field=%d\n",data_field);
  data_mask    =  (0x1<< data_field) - 1;
  if(debug)printf("data mask=0x%x\n",data_mask);

  if (nbins != data_mask+1)
  {
    /*cm_msg(MINFO,"mem_init","nbins requested (%d) adjusted to %d",info.nbins, data_mask+1);*/
    if(debug)printf( "tbin_round_up: nbins requested (%d) adjusted to %d\n",nbins, data_mask+1);
    nbins = data_mask+1;
  }


  /*  if success */

    if(debug)printf ("nbins is %i", nbins);
    sprintf (str_buff, "%i", nbins);
    Tcl_AppendResult (interp, str_buff, (char *) NULL);
    return TCL_OK;

}


/**********************************************************************/


int MidasCmd _ANSI_ARGS_(( ClientData clientData,
			      Tcl_Interp *interp,
			       int argc,
			       char *argv[]
			       )) 
{
/*      Input parameters:
 *      command     name of midas command
 *      
 *      Returns       For       With Tcl Result
 *      TCL_OK        success   result value, if any
 *      TCL_ERROR     failure   string containing failure message
 */
  char result[100]; 

  if (argc < 2)
  {
    Tcl_AppendResult (interp, "MidasCmd: no command supplied", (char *) NULL); 
    return TCL_ERROR; 
  }
  
 if(debug) printf("MidasCmd: starting with  argv[1] = %s\n",argv[1] ); 
 if( *argv[1]=='c' && !strcmp(argv[1],"connect_experiment")){
   return tconnect_experiment( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='d' && !strcmp(argv[1],"disconnect_experiment")){
   return tdisconnect_experiment( clientData, interp, argc-1,argv+1 );
 }


 if( *argv[1]=='g') {
   if( !strcmp(argv[1],"get_experiment_database"))
     return tget_experiment_database( clientData, interp, argc-1,argv+1 );
   else if(  !strcmp(argv[1],"get_value"))
     return tget_value( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"get_data"))
     return tget_data( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"get_RunNum"))
     return tget_RunNum( clientData, interp, argc-1,argv+1 );
 }
/* hope these may not be needed in future ... omitted now!
 if( *argv[1]=='g' && !strcmp(argv[1],"get_length")){
   return tget_length( clientData, interp, argc-1,argv+1 );
 }

 if( *argv[1]=='g' && !strcmp(argv[1],"get_n_bins")){
   return tget_n_bins( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='b' && !strcmp(argv[1],"bin_round_up")){
   return tbin_round_up( clientData, interp, argc-1,argv+1 );
 }
 
/* end of dubious routines */
 if( *argv[1]=='e' && !strcmp(argv[1],"env_odb_cmd")){
     return tenv_odb_cmd( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='h' && !strcmp(argv[1],"hot_save")) {
   return thot_save( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='k' && !strcmp(argv[1],"kill")) {
   return tkill( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='l' && !strcmp(argv[1],"load")) {
   return tload( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='m' && !strcmp(argv[1],"msg")){
   return tmsg( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='o' && !strcmp(argv[1],"odb_cmd")){
     return todb_cmd( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='p' && !strcmp(argv[1],"pause")){
     return tpause( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='r' && !strcmp(argv[1],"resume")){
     return tresume( clientData, interp, argc-1,argv+1 );
 }

 if( *argv[1]=='s')
 {
   if(!strcmp(argv[1],"set_value")){
     if(debug) printf("MidasCmd: about to call tset_value\n");
     return tset_value( clientData, interp, argc-1,argv+1 );
   }
   else if(  !strcmp(argv[1],"save"))
     return tsave( clientData, interp, argc-1,argv+1 );

   else if( !strcmp(argv[1],"start"))
     return tstart( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"stop"))
     return tstop( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"set_data_array"))
     return tset_data_array( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"set_data"))
     return tset_data( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"set_debug"))
     return tset_debug( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"show_debug"))
     return tshow_debug( clientData, interp, argc-1,argv+1 );
   else if( !strcmp(argv[1],"scl"))
     return tscl( clientData, interp, argc-1,argv+1 );

 }
 if( *argv[1]=='t' && !strcmp(argv[1],"toggle")) {
   return ttoggle( clientData, interp, argc-1,argv+1 );
 }
 if( *argv[1]=='y' && !strcmp(argv[1],"yield")) {
   return tyield( clientData, interp, argc-1,argv+1 );
 }
 /* if( *argv[1]=='t' && !strcmp(argv[1],"test_func")){
  *   return test_func( clientData, interp, argc-1,argv+1 );
  * }
  */
 
 sprintf(str_buff,"midas_tcl: Error: unknown midas command \"%s\"",argv[1]);
 Tcl_SetResult(interp,str_buff,TCL_STATIC);
 /* error so disconnet experiment */
 tdisconnect_experiment( clientData, interp, argc-1,argv+1 );
 return TCL_ERROR;
}
 

