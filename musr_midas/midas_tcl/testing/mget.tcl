#!/bin/sh
#-*-tcl-*-
# the next line restarts using wish  \
exec mtcl "$0" -- ${1+"$@"}
#
#=====================================================================
# Test a midas command using mtcl
#=====================================================================
#
#  $Log: mget.tcl,v $
#  Revision 1.1  2002/04/24 21:33:41  suz
#  original
#
#
# connect to the experiment: host, expt and program name 
#  host,expt, and front end name must be parameters to this pgm
# defaults
set host "dasdevpc"
set expt "musr"
# future frontend client name 
set fename "mtcl"
# frontend equipment name
set eqp_name "MUSR_TD_acq" 

if {  [ catch { midas connect_experiment $host $expt  tcl_UI } result ]} {
    puts stderr "Error after connect_experiment: $result"
    exit
}

# connect to the database - no handler returned, so only 1 database
# can be accessed from Tcl/Tk !
if {  [ catch { midas get_experiment_database } result ]} {
    puts stderr "Error after get_experiment_database: $result"
    exit
}
set mdarc "/equipment/$eqp_name/mdarc"

#get a floating point value from midas odb:
set odb_item "$mdarc/histograms/dwell time (ms)"
if {  [ catch { midas get_value $odb_item } dwell ]} {
    puts stderr "Error after get_value: $dwell"
    exit
}
puts "dwell time:$dwell"


#get an integer  value from midas:
set odb_item "Runinfo/Run number"
if {  [ catch { midas get_value $odb_item } run_no ]} {
    puts stderr "Error after get_value: $run_no"
    exit
}
puts "Present run number:$run_no"
set mdarc_last_good_bin    "$mdarc/histograms/last good bin"
set mdarc_titles           "$mdarc/histograms/titles"

# get the data
if {  [ catch { midas get_data $mdarc_titles} titles ]} {
    puts stderr "Error: $titles"
   exit
}
puts "histogram titles: $titles"
set length [llength $titles]
puts "no. elements in titles  = $length"

# get the data
if {  [ catch { midas get_data $mdarc_last_good_bin} lgb ]} {
    puts stderr "Error: $lgb"
   exit
}
puts "histogram last good bin: $lgb"
set length [llength $lgb]
puts "no. elements  = $length"

set path "/Equipment/MUSR_TD_acq/mdarc/save_interval(sec)"
if {  [ catch {  midas get_data $path} result ]} {
    puts stderr "Error: $result"
} else {
    puts " The present save interval is $result"
}

# get the scaler values
set path "/Equipment/Scaler/Variables/SCLR"
if {  [ catch {  midas get_data $path} result ]} {
    puts stderr "Error: $result"
} else {
    puts " The present scaler values are: $result"
}
# get the scaler rates
set path "/Equipment/Rscal/Variables/RATE"
if {  [ catch {  midas get_data $path} result ]} {
    puts stderr "Error: $result"
} else {
    puts " The present scaler rates are: $result"
}

# neatly disconnect
midas disconnect_experiment

exit

