#!/bin/sh
#-*-tcl-*-
# the next line restarts using wish  \
exec mtcl "$0" -- ${1+"$@"}
#
#=====================================================================
# Test a midas command using mtcl
#=====================================================================
#
#  $Log: mgetrn.tcl,v $
#  Revision 1.1  2002/04/24 21:33:41  suz
#  original
#
#

# connect to the experiment: host, expt and program name 
#  host,expt, and front end name must be parameters to this pgm
# defaults
set host "dasdevpc"
set expt "musr"
set eqp_name "MUSR_TD_acq"
set beamline "dev"

# frontend equipment name
if {  [ catch { midas connect_experiment $host $expt  mtcl } result ]} {
    puts stderr "Error after connect_experiment: $result"
    exit
}

# connect to the database - no handler returned, so only 1 database
# can be accessed from Tcl/Tk !
if {  [ catch { midas get_experiment_database } result ]} {
    puts stderr "Error after get_experiment_database: $result"
    exit
}
#midas set_debug 1

#set debug [ midas show_debug]
#puts $debug
#get run number  from midas:
set odb_item "Runinfo/Run number"
if {  [ catch { midas get_value $odb_item } run_no ]} {
    puts stderr "Error after get_value: $run_no"
    midas disconnect_experiment
    exit
}
puts "Present run number: $run_no"

puts " --------------------------------------------------- "
puts " using get_value to get perlscript path"
set mdarc "/equipment/$eqp_name/mdarc"
set perl_path "$mdarc/perlscript path"
if {  [ catch { midas get_value $perl_path } perl ]} {
    puts stderr "Error after get_value: $perl"
    midas disconnect_experiment
    exit
}

puts "perl script path: $perl"
puts " ----------------------------------------------- "
puts " Getting a REAL run number: "
puts " ----------------------------------------------- "
puts "calling get_RunNum with parameters: real $expt $beamline $perl"

if {  [ catch { midas get_RunNum real $expt $beamline $perl } result ]} {
    puts stderr "Error - $result"
    midas disconnect_experiment
    exit
}

puts " ----------------------------- "
puts " re-reading run number "
#get run number  from midas:
set odb_item "Runinfo/Run number"
if {  [ catch { midas get_value $odb_item } run_no ]} {
    puts stderr "Error after get_value: $run_no"
    exit
}
puts " and the run type "
puts " --------------------------------------------------- "
set type_path "$mdarc/run type"
if {  [ catch { midas get_value $type_path } run_type ]} {
    puts stderr "Error after get_value: $run_type"
    midas disconnect_experiment
    exit
}
puts "run number is $run_no and type is $run_type"

puts " ----------------------------- "
puts " Getting a TEST run number "
puts " ----------------------------- "

puts "calling get_RunNum with parameters: test $expt $beamline $perl"

if {  [ catch { midas get_RunNum test $expt $beamline $perl } result ]} {
    puts stderr "Error - $result"
    midas disconnect_experiment
    exit
}

puts " ----------------------------- "
puts " re-reading run number "
#get run number  from midas:
set odb_item "Runinfo/Run number"
if {  [ catch { midas get_value $odb_item } run_no ]} {
    puts stderr "Error after get_value: $run_no"
    exit
}
puts " and the run type "
puts " --------------------------------------------------- "
set type_path "$mdarc/run type"
if {  [ catch { midas get_value $type_path } run_type ]} {
    puts stderr "Error after get_value: $run_type"
    midas disconnect_experiment
    exit
}
puts "run number is $run_no and type is $run_type"


# neatly disconnect
midas disconnect_experiment

exit

