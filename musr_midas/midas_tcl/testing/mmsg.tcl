#!/bin/sh
#-*-tcl-*-
# the next line restarts using wish  \
exec mtcl "$0" -- ${1+"$@"}
#
#=====================================================================
# Test a midas command using mtcl
#=====================================================================
#
#  $Log: mmsg.tcl,v $
#  Revision 1.1  2002/04/24 21:33:41  suz
#  original
#
#

# connect to the experiment: host, expt and program name 
#  host,expt, and front end name must be parameters to this pgm
# defaults
set host "dasdevpc"
set expt "musr"
# frontend equipment name
if {  [ catch { midas connect_experiment $host $expt  mtcl } result ]} {
    puts stderr "Error after connect_experiment: $result"
    exit
}

# connect to the database - no handler returned, so only 1 database
# can be accessed from Tcl/Tk !
if {  [ catch { midas get_experiment_database } result ]} {
    puts stderr "Error after get_experiment_database: $result"
    exit
}
# assign midas message types
set MT_ERROR 1; set MT_INFO 2; set MT_DEBUG 4;
set MT_USER 8;  set MT_LOG 16; set MT_TALK 32;
set MT_CALL 64; set MT_ALL   255;

#set debug [ midas show_debug]
#puts $debug
set message "This is test message 1 (1 parameter)"

# Send a message using msg with 1 parameter:
#  By default, message_type  = MT_USER = 8
puts "Sending command msg with 1 param:  $message "

if {  [ catch { midas msg $message  } result ]} {
    puts stderr "Error: $result"
}

# Send a message using msg with 3 parameters:
set message "This is test message 2 (2 parameters)"
#  By default, message_type  = MT_USER = 8
set user_name "fred";
puts "Sending command msg with 2 params: $user_name $message "


if {  [ catch { midas msg $user_name $message  } result ]} {
    puts stderr "Error: $result"
}

# Send a message using msg with 3 parameters : 
#   Set message_type  = MT_ERROR 
set message "This is test message 3 (3 params including MT_ERROR)"
puts "Sending command msg with 3 params: $MT_ERROR $user_name $message"

if {  [ catch { midas msg $MT_ERROR $user_name $message  } result ]} {
    puts stderr "Error: $result"
}

# Send another message using msg with 3 parameters: 
#   Set message_type  = MT_INFO 
set message "This is a test message 4 (3 params including MT_INFO)"
puts "Sending command msg with 3 params: $MT_INFO $user_name $message"

if {  [ catch { midas msg $MT_INFO $user_name $message  } result ]} {
    puts stderr "Error: $result"
}

# Send another message using msg with 3 parameters: 
#   Set message_type  = MT_LOG
set message "This is a test message 5 (3 params including MT_LOG)"
puts "Sending command msg with 3 params: $MT_LOG $user_name $message"

if {  [ catch { midas msg $MT_LOG $user_name $message  } result ]} {
    puts stderr "Error: $result"
}

# Send another message using msg with 3 parameters: 
#   Set message_type  = MT_USER
set message "This is a test message 6 (3 params including MT_USER)"
puts "Sending command msg with 3 params: $MT_USER $user_name $message"

if {  [ catch { midas msg $MT_USER $user_name $message  } result ]} {
    puts stderr "Error: $result"
}
# neatly disconnect
midas disconnect_experiment

exit

