#!/bin/sh
#-*-tcl-*-
# the next line restarts using wish  \
exec mtcl "$0" -- ${1+"$@"}


#
#=====================================================================
# Test a midas command using mtcl
#=====================================================================
#
#  $Log: msetval.tcl,v $
#  Revision 1.1  2002/04/24 21:33:41  suz
#  original
#
#
# connect to the experiment: host, expt and program name 
#  host,expt, and front end name must be parameters to this pgm
# defaults
set host "dasdevpc"
set expt "musr"
# frontend equipment name
if {  [ catch { midas connect_experiment $host $expt  mtcl } result ]} {
    puts stderr "Error after connect_experiment: $result"
    exit
}

# connect to the database - no handler returned, so only 1 database
# can be accessed from Tcl/Tk !
if {  [ catch { midas get_experiment_database } result ]} {
    puts stderr "Error after get_experiment_database: $result"
    exit
}
midas set_debug 0

set debug [ midas show_debug]
puts $debug
set path "/test/string"
if {  [ catch { midas get_value $path } string ]} {
    puts stderr "Error after get_value: $string"
    midas disconnect_experiment
    exit
}

puts "string: $string"
puts " ----------------------------- "
set value "this is a very long test string to be written to odb "
if {  [ catch { midas set_value $path $value } string ]} {
    puts stderr "Error after get_value: $string"
    midas disconnect_experiment
    exit
}
puts "Result: $string"
puts " ----------------------------- "


set string " "
puts "Reading string back"
puts "ok"
if {  [ catch { midas get_value $path } string ]} {
    puts stderr "Error after set_value: $string"
    midas disconnect_experiment
    exit
}
puts "string: $string"

set value "zero one twothreefourfivesiz"

puts " ----------------------------- "
set path "/test/try"
puts "writing array $path with string: $value"
if {  [ catch { midas set_data_array $path $value } string ]} {
    puts stderr "Error after set_data_array: $string"
    midas disconnect_experiment
    exit
}
puts "Result: $string"
puts " ----------------------------- "

puts "reading array $path: "
if {  [ catch { midas get_data $path  } string ]} {
    puts stderr "Error after get_data (array): $string"
    midas disconnect_experiment
    exit
}
puts "Result: $string"

# neatly disconnect
midas disconnect_experiment

exit

