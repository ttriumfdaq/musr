#!/bin/sh
#-*-tcl-*-
# the next line restarts using wish  \
exec mtcl "$0" -- ${1+"$@"}
#
#=====================================================================
# Test a midas command using mtcl
#=====================================================================
#
#  $Log: mstart.tcl,v $
#  Revision 1.1  2002/04/24 21:33:41  suz
#  original
#
#


# connect to the experiment: host, expt and program name 
#  host,expt, and front end name must be parameters to this pgm
# defaults
set host "dasdevpc"
set expt "musr"
# frontend equipment name
if {  [ catch { midas connect_experiment $host $expt  mtcl } result ]} {
    puts stderr "Error after connect_experiment: $result"
    exit
}

# connect to the database - no handler returned, so only 1 database
# can be accessed from Tcl/Tk !
if {  [ catch { midas get_experiment_database } result ]} {
    puts stderr "Error after get_experiment_database: $result"
    exit
}

puts "Sending command  start"
if {  [ catch { midas start } result ]} {
    puts stderr "Error: $result"
} else {
    puts "result: $result"
}


# neatly disconnect
midas disconnect_experiment

exit

