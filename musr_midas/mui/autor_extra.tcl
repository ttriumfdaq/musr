#   autoruns_extra.tcl
#   Procedures for mui windows that control autoruns

#   $Log: autor_extra.tcl,v $
#   Revision 1.24  2015/03/20 00:58:38  suz
#   changes by Donald. This version part of package for new VMIC frontends
#
#   Revision 1.23  2009/09/10 04:26:53  asnd
#   Provide option of keeping edited plan even if it has errors (for renumbering e.g.)
#
#   Revision 1.22  2009/05/23 10:40:51  asnd
#   Auto-apply histogram selection
#
#   Revision 1.21  2008/11/19 06:24:15  asnd
#   Clearer prompt when applying plan edits
#
#   Revision 1.20  2008/05/13 23:32:38  asnd
#   Fix some glitches in autorun.
#
#   Revision 1.19  2008/04/30 01:56:24  asnd
#   Convert to bnmr-style /autorun parameters in odb
#
#   Revision 1.18  2008/04/15 01:36:10  asnd
#   Various small adjustments
#
#   Revision 1.17  2008/03/06 04:27:10  asnd
#   Changes supporting counting single histogram for autorun ending.
#
#   Revision 1.16  2007/10/03 02:47:12  asnd
#   Minor regexp change
#
#   Revision 1.15  2006/11/22 21:10:27  asnd
#   Initialization of var
#
#   Revision 1.14  2006/09/22 03:03:08  asnd
#   Add kludged epics-setting ability, and conditional blocks of Camp settings
#
#   Revision 1.13  2006/07/08 01:27:22  asnd
#   Configurable mu character for window titles, to handle bug with versions in Sci Linux.
#
#   Revision 1.12  2006/05/26 16:58:02  asnd
#   Accumulated changes.  Some new autorun features. Better synching of variables and gui.
#
#   Revision 1.11  2005/07/21 07:20:12  asnd
#   Better handling of scratch plan files and their synchronization.
#
#   Revision 1.10  2005/07/02 05:16:42  asnd
#   Change display of some parameters in autoruns window
#
#   Revision 1.9  2005/06/22 15:27:27  asnd
#   Add "after" to plan commands
#
#   Revision 1.8  2004/11/07 08:19:13  asnd
#   Addition of when and after to plan vocabulary.
#
#   Revision 1.7  2004/10/29 06:52:03  asnd
#   Minor touch-ups to behaviour on autorun control page
#
#   Revision 1.6  2004/08/18 03:29:25  asnd
#   Clean up autorun interactions, incl using temporary plan file for editing.
#
#   Revision 1.5  2004/04/27 11:47:07  asnd
#   Minor change of behavior
#
#   Revision 1.4  2004/04/21 05:20:52  asnd
#   Minor edit
#
#   Revision 1.3  2004/04/16 22:40:10  asnd
#   Minor bugfix
#
#   Revision 1.2  2004/04/15 09:16:56  asnd
#   Add feature of renumbering existing run plan
#
#   Revision 1.1  2004/04/10 07:00:07  asnd
#   Add auto-run control
#


if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}
package require BLT

source [file join $muisrc readplan.tcl]

#array set editorChoices {emacs emacs nedit nedit kedit kedit \
#        pico {xterm -fn 9x15 -fb 9x15bold -e pico} \
#        vi {xterm -fn 9x15 -fb 9x15bold -e vi} \
#    }

# Initialize the autorun-control window.
  
proc autor_initialize { } {
    global autor mauto autorwin

    set arw [td_win_name $autorwin]

    if { [ string length $autorwin ] } {
        # We are in a real application,
        if { [winfo exists $arw] } {
            wm deiconify $arw
            raise $arw
        } else {
            toplevel $arw
            autor_ui $autorwin
        }
    }

    # Bind help
    ar_bind_help ar_enable_b "Enable or disable autoruns"
    ar_bind_help ar_plan_b "Pick the plan file (Alt-P)"
    ar_bind_help ar_plan_e "Type the plan file name"
    ar_bind_help ar_editor_mb "Pick the editing program (Alt-D)"
    ar_bind_help ar_editor_e "The editor for editing the plan"
    ar_bind_help ar_edit_b "Edit the run plan (Alt-E)"
    ar_bind_help ar_check_b "Check the current run plan (Alt-C)"
    ar_bind_help ar_help_b "Show information on plan format (Alt-H)"
    ar_bind_help ar_pausing_b "Whether to pause runs in progress"
    ar_bind_help ar_refresh_e "Refresh period of autorun server"
    ar_bind_help ar_renumber_b "Resequence run numbers in plan (Alt-N)"
    ar_bind_help ar_counts_e "Number of counts to acquire, or num M"
    ar_bind_help ar_counting_b "Which histogram(s) to take counts from"
    ar_bind_help ar_timelimit_e "Time limit for run, may use h,m,s units"
    ar_bind_help ar_apply_b "Apply changed settings (Alt-A)"
    ar_bind_help ar_OK_b "Apply and close (Alt-O)"
    ar_bind_help ar_quit_b "Forget changes and close (Alt-Q)"
    ar_bind_help ar_revert_b "Forget changes (Alt-R)"

    wm title $arw "Autorun Settings"
    bind $arw <Alt-a> "$autorwin.ar_apply_b invoke"
    bind $arw <Alt-c> "$autorwin.ar_check_b invoke"
    # bind $arw <Alt-d> "$autorwin.ar_editor_mb invoke"
    bind $arw <Alt-e> "$autorwin.ar_edit_b invoke"
    bind $arw <Alt-h> "$autorwin.ar_help_b invoke"
    bind $arw <Alt-k> "$autorwin.ar_OK_b invoke; break"
    bind $arw <Alt-n> "$autorwin.ar_renumber_b invoke"
    bind $arw <Alt-o> "$autorwin.ar_OK_b invoke; break"
    bind $arw <Alt-p> "$autorwin.ar_plan_b invoke"
    bind $arw <Alt-q> "$autorwin.ar_quit_b invoke; break"
    bind $arw <Alt-r> "$autorwin.ar_revert_b invoke"

    foreach w [list ar_plan_e ar_refresh_e ar_counts_e ar_timelimit_e] {# Enable "note changes" only after gui is built
        $autorwin.$w configure -validate key -validatecommand {autor_note_change %W %s %P}
        bind $autorwin.$w <Key-Return> autor_apply
    }

    radiomenu $autorwin.ar_enable_b -textvariable autor(oper) -variable autor(oper) \
            -values {Disabled Enabled} -command autor_apply_enable -indicatoron 0

    radiomenu $autorwin.ar_pausing_b -textvariable autor(pausE) -variable autor(pausE) \
            -values {Disabled Enabled} -command autor_apply -indicatoron 0

    radiomenu $autorwin.ar_counting_b -indicatoron 0 -command autor_apply \
            -textvariable mauto(counthistname) -variable mauto(counthistname) \
            -values {Totals 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16}

    autor_make_menu

    autor_revert
}


#   Initialize/restore displayed parameters from the ODB
proc autor_revert { } {
    global autor mauto autorwin
    td_odb odb_get_autor "get autorun status"
    array set mauto [array get autor]
    if { $mauto(counts)%1000000 == 0 && $mauto(counts) > 1 } {
        set mauto(target) "[expr {$mauto(counts)/1000000}]M"
    } else {
        set mauto(target) $mauto(counts)
    }
    if { $mauto(counthist) < 1 } {
        set mauto(counthistname) "Totals"
    } else {
        set mauto(counthistname) $mauto(counthist)
    }

    set autor(entry_at) 0
    catch { $autorwin.ar_apply_b configure -fg $PREF::fgcol }
}

#   Apply autorun parameters, particularly the "enable/disable" state. 
proc autor_apply_enable { } {
    global midas autor
    set autor(enabled) [lsearch {Disabled Enabled} $autor(oper)]
    set midas(clt_checked_at) 0
    autor_apply
    after 0 catch mui_check_clients
}

#   Apply plan and autorun parameters.  Returns 1 on success
proc autor_apply { } {
    global autor mauto autorwin midas

    # These two variables don't have "mauto" versions, so duplicate
    set mauto(oper) $autor(oper)
    set mauto(pausE) $autor(pausE)

    # Warn about non-existent file, but only when file is specified and it
    # has been changed.
    if { [string length $mauto(filename)] && \
             $mauto(filename) ne $autor(filename) } {
        if { [string first "online/$midas(expt)/plantmp" $mauto(filename)] >= 0 } {
            set fixpf [string map {"/plantmp" ""} $mauto(filename)]
            set mes "Plan files may not be located in the \"plantmp\" working directory"
            beep
            if { [file exists $mauto(filename)] } {
                set fname [file tail $mauto(filename)]
                switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                            -type okcancel -default ok -icon warning \
                            -title "bad tmp dir" -message \
                            "$mes\nCopy $fname to its parent directory?" ] {
                                ok {
                                    file copy -force $mauto(filename) $fixpf
                                    set mauto(filename) $fixpf
                                }
                                cancel { return 0 }
                            }
            } else {
                timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                    -type ok -default ok -icon warning \
                    -title "bad tmp dir" -message "$mes\nUsing $fixpf instead."
                set mauto(filename) $fixpf
            }
        } elseif { ! [file exists $mauto(filename)] } {
            beep
            switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                        -type okcancel -default ok -icon warning \
                        -title "No such plan" -message \
                        "Plan file $mauto(filename) does not exist." ] {
                            ok { }
                            cancel { return 0 }
                        }
        }
    }

    # Counts and counthist
    if { [string length $mauto(target)] == 0 && !$autor(enabled) } {
        set mauto(target) 0
    } else {
        set n [scan $mauto(target) " %f %s" c u]
        if { $n == 2 && $u == "M" } {
            set c [expr {1000000*$c}]
        }
        if { $n < 1 || $c < 0 || $c > 2.0e9 } {
            beep
            switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                        -type okcancel -default ok -icon warning \
                        -title "Invalid Counts" -message \
                        "Invalid Counts number: enter a number (or a number followed by M)" ] {
                            ok { return 1 }
                            cancel { return 0 }
                        }
        }
        set mauto(counts) [expr { round($c) }]

        set mauto(counthist) 0
        scan $mauto(counthistname) %d mauto(counthist)
    }

    set mauto(elapsed) 0
    if { [scan $mauto(timelimit) " %f %s" e u] == 1 } { # Pure number -> minutes
        set mauto(elapsed) [expr { round(60.0*$e) }]
    } else {# delta-time spec
        catch { set mauto(elapsed) [deltaTime $mauto(timelimit)] }
    }

    if { ![string is integer -strict $mauto(refresh_period)] || \
            $mauto(refresh_period) < 2 || $mauto(refresh_period) > 200 } {
        switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type okcancel -default ok -icon warning \
                -title "Invalid Refresh" -message \
                "Refresh period is out of range.  Using 10 seconds." ] {
            cancel { return 0 }
        }
        set mauto(refresh_period) 10
    }

    # Copy from temporary plan file, if one is active
    if { [info exists autoed(temp_file)] && [autor_if_revised $autoed(temp_file)] } {
        autor_apply_temp_plan $autoed(temp_file)
    }

    # If enabled but idle, tell autorun processor to reload plan file, even if it hasn't changed.
    if { $autor(enabled) && $mauto(state) eq "idle" } {
        set mauto(state) "reload"
    }

    # Put local copies into main array, apply the array; if it applies without error,
    # then retrieve the values to ensire consistency.
    array set autor [array get mauto]
    if { [td_odb odb_set_autor_cli "apply autorun parameters"] } {
        autor_revert
    }

    return 1
}

proc autor_ok { } {
    if { [autor_apply] } { autor_quit }
}

proc autor_quit { } {
    global autorwin
    catch { destroy [td_win_name $autorwin] }
}

proc autor_note_change { win old new } {
    global autor autorwin
    if { $old ne $new } {
        set autor(entry_at) [clock seconds]
        $autorwin.ar_apply_b configure -fg $PREF::brightcol
    }
    return 1
}

proc autor_make_menu { } {
    global autorwin

    if { ![winfo exists $autorwin.ar_editor_mb.menu] } {
        [menu $autorwin.ar_editor_mb.menu] configure -tearoff 0
    }
    $autorwin.ar_editor_mb.menu delete 0 last
    $autorwin.ar_editor_mb.menu add command -label GUI \
                -command [list set autoed(editor) GUI] -underline 0 -state disabled
    foreach { ed prog } [array get cfg::editorChoices] {
        $autorwin.ar_editor_mb.menu add command -label $ed -underline -1 \
                -command [list set autoed(editor) $ed]
    }
}

proc autor_show_help { } {
    global muisrc
    # Display help page (html)
    blt::bgexec KonqErrr konqueror file://$muisrc/autohelp.html >& /dev/null &
}



#   Chooser for the plan file (needs replacing or fixing for choosing
#   new file!)

proc autor_pick_plan { } {
    global mauto autorwin
    set d [file dirname $mauto(filename)]
    set f [file tail $mauto(filename)]
    set f [tk_getOpenFile -initialdir $d -initialfile $f \
            -parent [p_win_name $autorwin] -title "Select plan file"]
    if { [string length $f] } {
        set mauto(filename) $f
    }
}

proc autor_edit_plan { } {
    global mauto autorwin autoed

    # Choose temporary plan file
    if { [info exists autoed(temp_file)] } {
        set temp_p $autoed(temp_file)
    } else {
        if { [catch {autor_choose_temp} msg] } {
            beep
            timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                    -type ok -default ok -icon error \
                    -title "No plan" -message \
                    "Could not locate a work area:\n$msg"
            return
        } else {
            set temp_p $msg
        }
    }

#   set ec GUI
    set ec $autoed(editor)
    catch { set ec $cfg::editorChoices($autoed(editor)) }
    if { $ec eq "GUI" } {
        puts "GUI plan editor not implemented yet"
    } else {
        # Execute editor
        # Make sure we have a file chosen
        if { [string length $mauto(filename)] == 0 } {
            beep
            timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                    -type ok -default ok -icon error \
                    -title "No plan" -message \
                    "There is no plan file!  Enter a file name before editing."
            return
        }
        # If plan file does not exist, create it
        if { ![file exists $mauto(filename)] } {
            if { [catch {exec touch [file normalize $mauto(filename)]} msg] } {
                beep
                timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                    -type ok -default ok -icon error \
                    -title "No plan" -message \
                    "Failed to create new plan file $mauto(filename):\n$msg"
                return
            }
        }
        # if temporary work file does not exist, create it
        if { ![file exists $temp_p] } {
            if { [autor_mirror_plan $temp_p ] == 0 } {
                return
            }
        }

        set autoed(temp_file) $temp_p
        set autoed(temp_mtime) [file mtime $autoed(temp_file)]
        set autoed(plan_mtime) [file mtime $mauto(filename)]
        autor_watch_temp_plan $autoed(temp_file)

        catch { eval blt::bgexec ::editorError $ec [list $autoed(temp_file)] }

        # after a longer interval than the autor_watch_temp_plan repeat
        after 1000 { autor_finish_edit }
    }
}

# Watch the temporary file for updates: then check and apply plan
# Watch plan file for updates: mirror changes into temporary file
# Stop watching when a file-name variable disappears, or when 
# the temp file name changes.
proc autor_watch_temp_plan { tfile } {
    global autoed mauto
    if { ![info exists mauto(filename)] || ![info exists autoed(temp_file)] || \
             $autoed(temp_file) ne $tfile } {
        puts "Cancel watching when components disappear"
        # Should exit the editor at this point, and/or delete the scratch file?
        return
    }

    set pfile [file normalize $mauto(filename)]

    if { [file exists $tfile] } {
        set tmt [file mtime $tfile]
        if { $tmt != $autoed(temp_mtime) } {
            set autoed(temp_mtime) $tmt
            puts "Working file has been updated.  Check it!"
            after 500 autor_check_plan $tfile
        } elseif { [file exists $pfile] } {
            set pmt [file mtime $pfile]
            if { $pmt != $autoed(plan_mtime) } {
                set autoed(plan_mtime) $pmt
                if { [catch {exec diff -bq -- $pfile $tfile}] } {
                    puts "Plan file updated elsewhere, so mirror"
                    autor_mirror_plan $tfile
                }
            }
        }
        after 800 autor_watch_temp_plan $tfile
    }
}

proc autor_finish_edit { } {
    global autoed mauto autorwin
    if { ! [info exists autoed(temp_file)] } {
        catch { unset autoed(mtime) }
        return 
    }
    set temp_file $autoed(temp_file)
    if { [autor_if_revised $temp_file] } {
        beep
        switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type yesnocancel -default cancel -icon question -title "Unsaved changes" \
                -message "There are unsaved changes in the plan.\nApply them now?\n(Cancel to forget them)"] {
            yes {
                autor_apply_temp_plan $temp_file
            }
            no {
                return
            }
        }
    }
    catch { file delete $temp_file }
    catch { unset autoed(temp_file) }
    catch { unset autoed(mtime) }
}

# Copy the temporary working plan, $file, onto the auto-run plan file.

proc autor_apply_temp_plan { file } {
    global mauto
    #puts "Apply temp plan $file -> $mauto(filename)"
    if { $file ne $mauto(filename) && [string length $mauto(filename)] } {
        if { [catch {file copy -force $file $mauto(filename)} msg] } {
            beep
	    timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type ok -default ok -icon error -title "Failed copy" \
                -message "Failed to apply changes to plan!\n$msg"
	}
        set mauto(revised_at) [clock seconds]
        set ::autor(revised_at) $mauto(revised_at)
        set ::autoed(plan_mtime) [file mtime $mauto(filename)]
    }
}

# Boolean test if temporary work file contains a revised plan to update the
# true plan file.

proc autor_if_revised { file } {
    global mauto
    if { $file ne $mauto(filename) && [string length $mauto(filename)] } {
        if { [file exists $file] } {
            if { ![file exists $mauto(filename)] } { 
                return 1
            }
            if { [file mtime $file] > [file mtime $mauto(filename)] } { 
                if { [catch {exec diff -bq -- [file normalize $file] [file normalize $mauto(filename)] }] } {
                    return 1
                }
            }
        }
    }
    return 0
}

proc autor_check_plan { file } {
    global mauto autorwin autor
    if { [string length $file] == 0 || ![file exists $file] } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type ok -default ok -icon error \
                -title "No plan" -message "No plan file to check: \"$file\""
        return
    }

    set ::plan::parse_error_action continue
    set ::plan::parse_error_count -1

    catch {parse_plan [read_plan $file] -99 } text

    if { $::plan::parse_error_count == -1} {
        set ::plan::parse_error_messages $text
        set msg "Failed to read plan file properly."
    } elseif { $::plan::parse_error_count == 1} {
        set msg "Run plan failed."
    } elseif { $::plan::parse_error_count } {
        set msg "Run plan failed with $::plan::parse_error_count error[plural $::plan::parse_error_count]."
    } else {
        if { $::plan::parse_stale_runs } {
            set msg "Run plan succeeds with warning: $::plan::parse_error_messages"
        } else {
            set msg "Run plan parsed successfully."
        }

        if { $file eq $mauto(filename) } {
            beep
            timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                    -type ok -default ok -icon info -title "Success" \
                    -message $msg
        } else {
            # Only offer to apply plan if it is revised or if it has not already been applied recently
            if { [clock seconds]-$mauto(revised_at) > 30 || [autor_if_revised $file] } {
                beep
                switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                        -type yesno -default yes -icon question -title "Success" \
                        -message "$msg\nCopy changes into actual run-plan file now?"] {
                    yes {
                        autor_apply_temp_plan $file
                    }
                    no {
                        return
                    }
                }
            } else {
                beep
                timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                        -type ok -default ok -icon info -title "Success" \
                        -message "$msg\n(Same plan as previous.)"
            }
            # Some silliness to deal with possible inconsistencies in local vs odb values
            catch { odb_get_auto }
            set mauto(enabled) $autor(enabled)
            if { !$mauto(enabled) } {
                beep
                switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                        -type yesno -default no -icon question -title "Enable autoruns?" \
                        -message "Autoruns are disabled.\nEnable them now?"] {
                    yes {
                        set autor(oper) Enabled
                        autor_apply_enable
                    }
                }
            }
        }
        return
    }
    autor_results $file $msg $::plan::parse_error_messages
}

#   
proc autor_renumber_plan { } {
    global mauto autorwin run
    if { [string length $mauto(filename)] == 0 || ![file exists $mauto(filename)] } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type ok -default ok -icon error \
                -title "No plan" -message \
                "No plan file to check: \"$mauto(filename)\""
        return
    }
    odb_get_runinfo
    set rn $run(number)
    if { $::run(in_progress) } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type ok -default ok -icon error \
                -title "Active run" -message \
                "A run is in progress!\nYou may only renumber the plan between runs."
        return
    }

    set fh [open $mauto(filename) r]
    set theplan [split [read $fh] "\n"]
    close $fh

    set newplan [list]
    set changes ""
    foreach line $theplan {
        if { [regexp -nocase {^\s?run:?\s+(\d+)} $line -- orn] } {
            lappend newplan "Run: [incr rn]"
            append changes "${orn}->${rn}, "
        } else {
            lappend newplan $line
        }
    }

    if { [string length $changes] } {
        switch [timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type okcancel -default ok -icon question \
                -title "Confirm renumbering" -message \
                "Ready to renumber run plan:\n$changes\nPlease confirm."] {
            ok {
                set fh [open $mauto(filename) w]
                puts $fh [join $newplan "\n"]
                close $fh
            }
        }
    } else {
        timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
                -type ok -default ok -icon warning \
                -title "No runs in plan" -message \
                "There were no runs found in the plan"
    }
    return
}



#    # Check plan (run check in an xterm for now)
#    set af [after 200000 { set ::editorError {timeout 0 666 timeout} }] 
#    blt::bgexec ::editorError xterm -fn 9x15 -fb 9x15bold \
#            -e tclsh [file join $::muisrc checkplan.tcl] $mauto(filename) wait
#    after cancel $af


proc autor_results { file msg text } {
    global mauto
    if { [winfo exists .autoerrors.text] } {
        #puts "Erase previous messages"
        .autoerrors.text configure -state normal
        .autoerrors.text delete 1.0 end
    } else {
        #puts "Open Error display window"
        set prevtext ""
        toplevel .autoerrors
        wm title .autoerrors "Check Run Plan"
        label .autoerrors.head -anchor c -justify left -text "Check Run Plan" -font muTitleFont
	scrollbar .autoerrors.scroll_bar -command ".autoerrors.text yview" -width 16
        text .autoerrors.text -pady 5 -padx 3 -takefocus 0 \
                -relief sunken -borderwidth 1 -highlightthickness 0 \
                -width 80 -wrap word -height 15 -yscrollcommand ".autoerrors.scroll_bar set"
        label .autoerrors.state -anchor w -text $msg
        canvas .autoerrors.sep -height 0 -width 0 -relief sunken -borderwidth 1
        frame  .autoerrors.buttons
        button .autoerrors.buttons.apply -text "Use It Anyway" -width 20 \
	        -command "autor_apply_temp_plan {$file} ; after 10 {destroy .autoerrors}"
        button .autoerrors.buttons.ok -text OK -width 20 -default active \
                -command {after 10 {destroy .autoerrors}}
	if { $file ne $mauto(filename) && [autor_if_revised $file] } {
	    pack .autoerrors.buttons.apply -padx 15 -side left
	}
        pack .autoerrors.buttons.ok -padx 15 -side right

        bind .autoerrors <Return> {destroy .autoerrors ; break}

        grid .autoerrors.head  x  -pady 8 
        grid .autoerrors.text  .autoerrors.scroll_bar  -padx 3 -sticky nsew
        grid .autoerrors.state - -padx 9 -pady 5 -sticky w
        grid .autoerrors.sep   - -padx 0 -pady 5 -sticky ew
        grid .autoerrors.buttons  -  -padx 2 -pady 3

        grid rowconfigure .autoerrors 1 -weight 1
        grid columnconfigure .autoerrors 0 -weight 1

    }
    .autoerrors.state configure -text $msg
    .autoerrors.text insert 1.0 [string trim $text]
    .autoerrors.text configure -state disabled
}

# Choose a temporary file name.  Returns the file name, or error.

proc autor_choose_temp { } {
    global env midas

    set name plan[clock seconds].[pid]
 
    set tmp [file join $env(HOME) online $midas(expt) plantmp]
    if { [file isdirectory ~/online/$midas(expt)] && [file owned ~/online/$midas(expt)]} {
        if { ![file exists $tmp] } {
            file mkdir $tmp
        }
        return [file join $tmp $name]
    }
    return [file join $env(HOME) $name]
}

# Copy the plan file into a temporary file.  Returns boolean 1 for success

proc autor_mirror_plan { temp_p } {
    global mauto autoed autorwin
    if { [catch {file copy -force $mauto(filename) $temp_p} msg] } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $autorwin] \
            -type ok -default ok -icon error \
            -title "No plan" -message \
            "Could not create temporary plan file:\n$msg"
        return 0
    }
    # Remember this modification time
    set autoed(temp_mtime) [file mtime $temp_p]
    return 1
}

#  THE HELP LINE:

proc ar_bind_help { w h } {
    global autorwin
    bind $autorwin.$w <Enter>    [list set autor_help_text "$h"]
    bind $autorwin.$w <FocusIn>  [list set autor_help_text "$h"]
    bind $autorwin.$w <Leave>    [list set autor_help_text {}]
    bind $autorwin.$w <FocusOut> [list set autor_help_text {}]
}

# time when plan was revised
set mauto(revised_at) 0
# time when a textual parameter was typed
set autor(entry_at) 0
# Default all hist totals
set mauto(counthist) 0

if { $autorwin == "" } {
    # stand-alone testing, so start immediately!
    array set autor {enabled 1 state idle counts 0 pausing 0}
    autor_initialize
}

