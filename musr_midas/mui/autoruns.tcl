#!/bin/sh
# the next line restarts using mtcl \
exec $MUSR_DIR/musr_midas/midas_tcl/mtcl "$0" "$@"
#
# 
#   $Log: autoruns.tcl,v $
#   Revision 1.23  2016/09/06 22:10:53  asnd
#   Add more beamline control to autoruns, and fix some bugs
#
#   Revision 1.22  2015/09/28 00:56:05  asnd
#   Restart new frontends; new "finally" command; minor things
#
#   Revision 1.21  2015/05/22 05:02:03  asnd
#   Map username for epicsset over ssh (m20* -> m20)
#
#   Revision 1.20  2015/05/19 22:03:12  asnd
#   Fix missing brace error.
#
#   Revision 1.19  2015/04/18 00:09:04  asnd
#   Better handling of Midas connections, and preven runaway creation
#
#   Revision 1.18  2015/03/20 00:56:56  suz
#   changes by Donald. This version part of package for new VMIC frontends
#
#   Revision 1.17  2008/05/13 23:32:38  asnd
#   Fix some glitches in autorun.
#
#   Revision 1.16  2008/04/30 01:56:24  asnd
#   Convert to bnmr-style /autorun parameters in odb
#
#   Revision 1.15  2007/11/21 23:50:39  asnd
#   Fix a bug of bnmr code not used in musr.  Change a warning.
#
#   Revision 1.14  2007/10/03 02:34:27  asnd
#   Changes to support bNMR
#
#   Revision 1.13  2006/11/23 02:57:34  asnd
#   maxwait parsing error fix; default to 60 min.  Change error messaging
#
#   Revision 1.12  2006/11/22 21:45:15  asnd
#   add email notifier
#
#   Revision 1.11  2006/10/28 01:43:08  asnd
#   Fix run increment bug
#
#   Revision 1.10  2006/09/22 03:03:08  asnd
#   Add kludged epics-setting ability, and conditional blocks of Camp settings
#
#   Revision 1.9  2006/05/26 16:58:02  asnd
#   Accumulated changes.  Some new autorun features. Better synching of variables and gui.
#
#   Revision 1.8  2005/06/22 15:28:58  asnd
#   Forced reloading of plan, keep going when titles change, new "after" command
#
#   Revision 1.7  2004/11/07 08:19:13  asnd
#   Addition of when and after to plan vocabulary.
#
#   Revision 1.6  2004/08/18 03:31:40  asnd
#   Clean up autorun interactions; don't believe when Midas sets run number 0.
#
#   Revision 1.5  2004/05/06 02:20:45  asnd
#   Fix [info exists $...] typos
#
#   Revision 1.4  2004/04/27 11:48:32  asnd
#   More warnings
#
#   Revision 1.3  2004/04/15 03:06:03  asnd
#   Add CVS log tag
#
#
################  Operation-control parameters:
# 
# These are recorded in the odb
# 
# enable: boolean (yes/no) (user to server)
# 
# state: int -- really a "selection" variable with values
#   0 disabled
#   1 idle 
#   2 acquiring
#   3 paused
#   4 ending
#   5 stopped
#   6 setting
#   7 changing
#   8 starting
#   9 reload
# (server feedback to user, except 9)
# 
# message:   string(128) - last error message from the autoserver
# 
# plan file: string(64) - filename (full path) of plan file (user to server)
#
# refresh seconds: int -- refresh period (user to server)
#
# tot counts: int -- requested total counts: feedback from autorun server
#                when it starts run, and user to server for changing 
#                value of run in progress.
#
# enable pausing: boolean -- tell autoruns to pause run when requirements
#                not satisfied in mid-run
#
# arun::delayed_set_tags -- list of after-id tags for delayed settings, so they can be cancelled.
# arun::afterid -- the after-id tag for the autoruns main loop
# arun::paused_at -- time of pausing automatically.  We will not pause rapidly, 
#                    and will not resume from manual pause

puts "\nBegin autorun client at [clock format [clock seconds]]"
cd ~

namespace eval ::arun {
    variable enabled 0
    variable state reload
    variable anomalies 0
    variable settitle ""
    variable usedtitle ""
    variable wait_until [expr [clock seconds]+9000]
    variable runnumber -1
    variable afterid {}
    variable delayed_set_tags {}
    variable noprogress 0
    variable paused_at 0
    variable busy_main 0
}
set ::arun::config(max_wait) 9000

namespace eval ::plan {
    variable parse_error_action error
    variable filename ""
    variable modtime 0
}

set midas(application) autorun
set midas(connected) 0
set midas(host) ""

if { [info exists env(BEAMLINE)] } {
    set midas(beamline) $env(BEAMLINE)
} elseif { [info exists env(USER)] } {
    set midas(beamline) $env(USER)
} else {
    set midas(beamline) {}
}

if { [info exists env(MIDAS_EXPT_NAME)] } {
    set midas(expt) $env(MIDAS_EXPT_NAME)
} elseif { [info exists env(USER)] } {
    set midas(expt) $env(USER)
} else {
    set midas(expt) musr
}

set midas(musr) [expr { ! [string match "b*" $midas(expt)]} ]
set midas(ppc_hm) [string equal $midas(expt) "musr"]

set midas(disconnum) 0
set midas(connat) [clock seconds]

set midas(data_dir) .
set midas(camplogging) 0
set idef(full_tol_test) "set a 1"
set idef(num_sweeps) 0

set autor(refresh_period) 10
set autor(plan_seek_time) 3

set autor(email_delay_m)  2
set autor(email_separ_m)  10
set autor(email_extra)    [list {asnd@triumf.ca}]
set autor(email_pending)  0
set autor(email_sent_at)  0
set ::arun::config(email) {}

# set some default parameters (for use before odb is read)
array set run [list  \
      state 9      starting 0        test_mode -1  in_progress 0  \
      paused 0     menustate ""      starting 0    start_sec 0   \
      number ""    start_record {}   acqoff_record {}  \
      startingat [clock seconds] \
]

puts "Experiment $midas(expt), beamline $midas(beamline)"

array set autor [list   \
   enabled 1   state reload   message "Waking up"   filename "plan.plan" \
   refresh_period 15   counts 10000000   counthist -1   pausing 0 \
]

set muisrc [file dirname [info script]]

set tcl_precision 15

# Use gui for error messages, if it is available (not required).
catch { package require Tk }

#source [file join $muisrc verbose_emissary.tcl]; # ??? DEBUG ???

source [file join $muisrc mui_utils.tcl]
source [file join $muisrc readplan.tcl]

set midas(camp_host) ""

if { [info commands midas] == "" } {
    proc midas args { 
        puts "Midas command: $args"
    }
}

proc autorun_main { } {
    puts "[clock format [clock seconds]] autorun_main -- begin autorun handler"
    global autor run midas 
    catch {
        wm withdraw .
    }

    auto_sole_client

    set ::plan::parse_error_action error

    set ::arun::afterid [after 1000 {auto_main_loop 0}]
    after 3000 look_for_plan
    vwait forever...
    exit
}


###########################
# look_for_plan  - The loop that looks for a revised run plan
#
# Parameters: none
# Returns:    none
# Globals:    plan::filename  -- the plan file
#             plan::modtime   -- the time when the plan file was changed
#             plan::afterid   -- ident for next look_for_plan
#             autor(plan_seek_time) -- how often we look for the plan
#             arun::state     -- the state of the main control loop
#             arun::enabled   -- enabling boolean
# Note: look_for_plan re-submits itself every plan_seek_time.
#       Setting the global variables based on the Odb is done elsewhere.

proc look_for_plan { } {
    # puts "Look for plan"
    global autor
    auto_odb_reconnect
    catch odb_get_autor
    # reschedule
    after cancel $plan::afterid
    set ::plan::afterid [after [expr {round(1000*$autor(plan_seek_time))}] look_for_plan]
    # check for changed settings
    set new [expr {$autor(filename) ne $::plan::filename}]
    if { $new || $autor(state) eq "reload" } {
        set ::plan::filename $autor(filename)
        set ::plan::modtime 0
    }
    # if recently enabled, do main iteration asap
    if { !$::arun::enabled } {
	if { $autor(enabled) } { # Just notice enable now
	    after cancel $::arun::afterid
	    set ::arun::afterid [after 1 [list auto_main_loop 0]]
	}
	return
    }
    # If no plan, then finished
    if { $::plan::filename eq "" } { return }
    #
    #puts "Look for plan $::plan::filename"
    if { ![file readable $::plan::filename] } {
        set autor(message) "Failed to read file $::plan::filename at [atstamp]"
        puts $autor(message)
        catch odb_set_autor_srv
        return
    }
    # Don't retain email address forever, reset for each new plan (Now unneeded)
    if { $new } {
        set ::arun::config(email) {}
    }
    
    catch {
        switch -- $::arun::state {
            idle -
            acquiring -
            paused - 
            stopped -
            changing -
            reload {
                set mt [file mtime $::plan::filename]
                if { $mt > $::plan::modtime } {
                    set ::plan::modtime $mt
                    # We read the plan after a slight delay, just in case we caught
                    # it while it is partially written.
                    if { $::plan::modtime == 0 } {
                        puts "Reload plan per request at [atstamp]"
                    } else {
                        puts "Reload plan because plan file has been updated at [atstamp]"
                    }
                    after 400 { load_plan }
                }
            }
            setting -
            starting -
            ending {
                if { $::arun::repeats > 4 && $::plan::modtime == 0 } {
                    after 5000 { puts "Reload plan per user request at [atstamp]"; load_plan }
                }
            }
        }
    }
}
set plan::afterid {}

proc load_plan { } {
    global midas run autor
    midas msg 2 autorun "Read plan file $::plan::filename"
    puts "!!!!!!!!!!!!!!!!! READ PLAN $::plan::filename at [atstamp] !!!!!!!!!!!!!!!!"
    if { [catch { read_plan $::plan::filename } plan] } {
        puts "load_plan: read_plan gave error $plan"
        autorun_error $plan
        return
    }
    catch {
        file copy -force $::plan::filename ~/.planarchive/[file tail $::plan::filename].[clock seconds]
    }
    catch {
        odb_get_general
        odb_get_runinfo
        odb_get_autor
    }
    #puts "Current run $run(number) active? $run(in_progress)"
    if { $run(in_progress) } {
        set r $run(number)
    } else {
        if { [catch {
            # get the next run number.  However, odb_get_runnum (midas get_RunNum...
            # get_next_run_number.pl) sets the actual experiment run number!
            # Therefore, we restore it after checking it.
	    set rr $run(number)
            odb_get_runnum
            set r [expr {$run(number)+1}]
	    #puts "After  odb_get_runnum, run number = $run(number); next will be $r"
            set run(number) $rr
            midas set_value "/Runinfo/Run number" $run(number)
            #puts "Restored original run number $run(number)."
        } msg ] } {
            autorun_error "Failed to determine run number:\n$msg"
            return
        }
    }

    # Stop autorun operations while we get new plan; If plan fails, then we will 
    # either continue with the old plan for the current run, or stay idle.
    set prevstate $::arun::state
    set ::arun::state idle
    foreach v [list titles requirements campset odbset epicsset config] {
        catch { unset ::plan::${v} }
    }
    #puts "Now parse_plan from run $r"
    set stat [catch { parse_plan $plan $r } msg]
    if { $stat == 4 } { # continue
        autorun_warning $msg
    } elseif { $stat } { # fail -- erase partial new plan
        foreach v [list titles requirements campset odbset epicsset config] {
            catch { unset ::plan::${v} }
        }
        if { $prevstate eq "changing" || $prevstate eq "starting"} {
            set ::arun::state $prevstate
        }
        autorun_error $msg
        return
    }

    set autor(message) "Loaded plan from $::plan::filename at [atstamp]"
    puts "!!!!!!!!!!!!!!!!!!!!!!!! got plan !!!!!!!!!!!!!!!!!!!!!!!!"

    # Got whole plan. Now deal with the part for the current (or next) run

    if { ![info exists ::plan::config($r)] } {
        if { $run(in_progress) } { 
            autorun_error "Run $r is in progress, but it is not in the run plan"
        } else {
            autorun_warning "Run $r should be next, but it is not in the run plan"
        }
        return
    }

    # Check if current Camp and Epics settings are changed. If so, repeat them ALL
    # (in future, maybe perform only changed settings). If no change, then retain the
    # state of settings-in-progress.
    set cchanged 0
    set echanged 0
    set preserve {}
    if { $prevstate eq "starting" } {
        set prevstate "changing"
    }
    if { $prevstate eq "changing" || $prevstate eq "setting"} {
        if { [llength $::arun::campmem] != [llength $::plan::campset($r)] } {
            set cchanged 1
        } else {
            foreach s1 $::arun::campmem s2 $::plan::campset($r) {
                if { $s1 ne $s2 } { set cchanged 1; break }
            }
        }
        if { [llength $::arun::epicsmem] != [llength $::plan::epicsset($r)] } {
            set echanged 1
        } else {
            foreach s1 $::arun::epicsmem s2 $::plan::epicsset($r) {
                if { $s1 ne $s2 } { set echanged 1; break }
            }
        }            
        if { $echanged } {
            set prevstate "stopped"
            puts "Epics settings for run $r have changed. They will all be performed fresh."
        } else {
            lappend preserve "epicsset"
        }
        if { $cchanged } {
            set prevstate "stopped"
            puts "Camp settings for run $r have changed. They will all be performed fresh."
        } else {
            lappend preserve "campset"
        }
    }

    copy_one_run_plan $r $preserve

    if { $run(in_progress) } {
        # Remove any requirements with attached actions (they should have been 
        # performed before the run began).
        # set ::arun::requirements [lsearch -all -inline -exact -index 6 $::arun::requirements {}]
        set rl [list]
        foreach req $::arun::requirements {
            if { [string length [lindex $req 6]] == 0 } {
                lappend rl $req
            }
        }
        set ::arun::requirements $rl
        # Check equivalence of titles
        set mismat [auto_run_titles_compare]
        if { [string length $mismat] } {
            if { $prevstate eq "idle" } {
                autorun_error "Run $r is in progress, but the header info doesn't match the plan (for $mismat).\nCheck the run numbering!"
                return
            } else {
                autorun_warning "Run $r is in progress, but the header info doesn't match the plan (for $mismat).\n(You may change them manually.)"
                foreach v $mismat {
                    if { $v eq "title" } {# title is stored differently
                        if { $::arun::title eq $::arun::usedtitle && [info exists run(runtitle)] && $run(runtitle) ne $::arun::settitle } {
                            catch {unset ::arun::title}
                            puts "Retain user's $v \"$run(runtitle)\""
                        }
                    } else {
                        if { [info exists run($v)] && [string length [string trim $run($v)]] } {
                            catch {unset ::arun::titles($v)}
                            puts "Retain user's $v \"$run($v)\""
                        }
                    }
                }
            }
        }
        set ::arun::state acquiring
    } elseif { $prevstate eq "changing" } {
        set ::arun::state changing
    } else {
        set ::arun::state stopped
    }
    set autor(state) $::arun::state

    # success, so get rid of old error messages
    after 100 autorun_close_error

    odb_set_autor_srv
}

####################################
# copy_one_run_plan
# Copy the run-plan parameters for one run ($rnum) from the main plan variables 
# into the variables controlling the current run.
#
# Parameters: rnum -- the run number (local, but used to set ::arun::runnumber)
#      (optinal) preserve -- list of groups to preserve (default empty)
# See notes on Data Structures.

proc copy_one_run_plan { rnum {preserve {}} } {
    global autor idef epics
    puts "Copy plan parameters for run $rnum (run-number was $::arun::runnumber)"

    if { [lsearch -exact $preserve config] < 0 } {
        # Copy new config info, replacing old. Config is sticky.
        # (load_plan should propagate email and max_wait through plan::config, but
        # duplicate effort here)
        set pemail $::arun::config(email)
        set pmaxwt $::arun::config(max_wait)
        catch { unset ::arun::config }
        array set ::arun::config [list \
            counts $autor(counts) counthist $autor(counthist) elapsed $autor(elapsed) \
            sweeps $idef(num_sweeps) email $pemail max_wait $pmaxwt]
        array set ::arun::config $::plan::config($rnum)

        # Apply revised target counts to odb
        set autor(counts) $::arun::config(counts)
        set autor(counthist) $::arun::config(counthist)
        set autor(elapsed) $::arun::config(elapsed)
        # Apply sweeps, but skip if we are in TD mode.
        if { [info exists ::arun::config(musrtype)] && ![string match $::arun::config(musrtype) "TD*"] } {
            if { $idef(num_sweeps) != $::arun::config(sweeps) } {
                puts "change sweeps from $idef(num_sweeps) to $::arun::config(sweeps)"
            }
            set idef(num_sweeps) $::arun::config(sweeps)
            catch { odb_set_idef }
        }
        catch { odb_set_autor_srv }
        #puts "Apply number of counts $autor(counts)"
    }

    if { [lsearch -exact $preserve titles] < 0 } {
        # Copy all titles info
        catch { unset ::arun::titles }
        array set ::arun::titles $::plan::titles($rnum)
        # Move run title into separate variable
        if { [info exists ::arun::titles(title)] } {
            set ::arun::title $::arun::titles(title)
            unset ::arun::titles(title)
        } else {
            # autorun_warning "Run plan gives no title for run $rnum"
            # (title from odb will be retained)
            if { [info exists ::arun::title] } {
                unset ::arun::title
            }
        }

        # Move automatic temperature into into proper variable
        if { [info exists ::arun::titles(temperature)] } {
            if { [string match "/?*" $::arun::titles(temperature)] } {
                set ::arun::titles(autoTvar) $::arun::titles(temperature)
                unset ::arun::titles(temperature)
            } else {
                set ::arun::titles(autoTvar) ""
            }
        }
        # Move automatic field into into proper variable
        if { [info exists ::arun::titles(field)] } {
            if { [string match "/?*" $::arun::titles(field)] } {
                set ::arun::titles(autoBvar) $::arun::titles(field)
                unset ::arun::titles(field)
            } else {
                set ::arun::titles(autoBvar) ""
            }
        }
    }

    if { [lsearch -exact $preserve campset] < 0 } {
        # Copy Camp settings
        set ::arun::campset $::plan::campset($rnum)
        set ::arun::campmem $::plan::campset($rnum)
        auto_cancel_settings camp
    }

    if { [lsearch -exact $preserve odbset] < 0 } {
        # Copy odb settings
        set ::arun::odbset $::plan::odbset($rnum)
    }

    if { [lsearch -exact $preserve epicsset] < 0 } {
        # Copy epics settings
        set ::arun::epicsset $::plan::epicsset($rnum)
        set ::arun::epicsmem $::plan::epicsset($rnum)
        # clear old queue ??? Should change how the slow epics settings work!
        set epics(queue) {}
        auto_cancel_settings epics
    }

    if { [lsearch -exact $preserve requirements] < 0 } {
        # Copy requirements.  Retain old variable logs where possible, or
        # put bogus bad log entries on new variables.
        set logs() ""
        if { $::arun::runnumber == $rnum && [info exists ::arun::requirements] } {
            foreach req $::arun::requirements {
                set logs([lindex $req 0]) [lindex $req 7]
            }
        }
        set before [clock seconds]
        set ::arun::requirements [list]
        foreach req $::plan::requirements($rnum) {
            if { [info exists logs([lindex $req 0])] } {
                puts "Preserve log of [lindex $req 0]"
                lappend req $logs([lindex $req 0]) {}
            } else {
                lappend req [list $before 99.99e9] {}
            }
            lappend ::arun::requirements $req
        }
    }

    # Set run number
    set ::arun::runnumber $rnum
    # puts [parray ::arun::titles]
    # puts [parray ::arun::config]
}


# Tcl regex does not support look-behind, so must capture sub-match with parens
# while { [regexp {<(/[^ >]+)>} $run(runtitle) token cvar] } { ... }
# or take whole-matches and trim the <>.

###########################################
#  auto_run_titles_compare 
#  Compare the titles fields in the run plan with the current (in-progress) run.
#  Return a string with the names of fields that do not match (title comes first).
#
#  The main run title is very tricky if it contains auto-substitution fields.
#
#  The original intent was:
#
#   (only perform each line if run plan specified that parameter...)
#
#   if no auto-T var: numerically check equality of Temperature (within .02)
#   if no auto-B var: numerically check equality of Field (within 1)
#   string-compare Sample
#   string-compare Orientation
#   string-compare Operator
#   numeric-compare Experiment
#   set RE "plan's title"
#     backslash-escape every non-alphanumeric in RE
#     replace each "<temperature>" with ".*" (for all fields)
#     surround it with "^....$"
#     regexp $RE $run_title
#   return OK only if all passed
#
#  but this version does straight string comparisons, except for the substitutions
#  in the main run title.

proc auto_run_titles_compare { } {
    global run
    set mismatches ""
    set fields [list sample operator experiment orientation]
    if { !$run(ImuSR) } {
        if { [info exists ::arun::title] } {
            # Compare the main run title, allowing loose matches for substituted fields, using regexp.
            # First, escape special characters in title string and delete spaces
            # regsub -all {[^[:alnum:]]} $::arun::title \\\\& tpattern
            set tpattern [string map \
                    {\\ \\\\ . \\. , \\, | \\| [ \\[ ] \\] \{ \\\{ \} \\\} * \\* ? \\? + \\+ ( \\( ) \\) ^ \\^ \$ \\\$ } \
                    $::arun::title]
            # Then substitute wildcards for the special header fields
            set tpattern [string map -nocase \
                    {<sample> .* <operator> .* <experiment> {\w*} <temperature> .* <field> .* <orientation> .*} \
                    $tpattern]
            regsub -all {\s+} $tpattern {\s*} tpattern
            # Perform regexp match of title-pattern against current run title.
            if { ![regexp "^\\s*$tpattern\\s*\$" $run(runtitle)] } {
                append mismatches " title"
                puts "Title mismatch between \"$::arun::title\" and \"$run(runtitle)\""
            }
        }
        # Compare other titles used only in TDmuSR: temperature, field.
        # Note that only one of temperature, autoTvar and of field, autoBvar can exist.
        lappend fields temperature field autoTvar autoBvar
    }
    # Compare other titles
    foreach v $fields {
        if { [info exists ::arun::titles($v)] && [string length $::arun::titles($v)] } {
            if { [string map {{ } {}} $::arun::titles($v)] ne [string map {{ } {}} $run($v)] } {
                append mismatches " $v"
                puts "Mismatch for $v between $::arun::titles($v) and $run($v)"
            }
        }
    }
    return [string trim $mismatches]
}

#########################################
# auto_apply_runtitle 
# re-set run title (using updated variables; and remember it in arun::usedtitle and arun::settitle)
# Assume run parameters are fresh.
# mdarc should (and someday will) do this conversion.

proc auto_apply_runtitle { } {
    global run
    # Do nothing if no title was specified (and not discarded in load_plan).
    if { [info exists ::arun::title] } {

        set conversion [list]
        foreach key [list sample temperature field orientation operator experiment subtitle] {
            if { [info exists run($key)] } {
                lappend conversion <$key> $run($key)
            }
        }
        foreach {tag var} [regexp -all -inline {<(/[^ <>]+)>} $run(runtitle)] {
            catch { # Poor way to do this, but don't bother with somewhat-better before mdarc takes over
                set val [mui_camp_cmd "varGetVal $var"]
                catch { set val [expr {$val}] }
                lappend conversion $tag [string trim $val]
            }
        }
        set arun::usedtitle $::arun::title
        set run(runtitle) [string map -nocase $conversion $::arun::title]
        catch { odb_set_runinfo }
    }
    set ::arun::settitle $run(runtitle)
}

######################################################
#  auto_imusr_pre_start  -  settings to make before starting an ImuSR run.
#  
#  This is code taken from imusr_run (imusr_extra.tcl) with the user-interaction
#  and some other things stripped out.
#
#  Returns "OK" or error message.

proc auto_imusr_pre_start { } {
    global run idef midas

    if { [string match -nocase Camp $idef(sweep_device)] } {
        set i $idef(sweep_ins_name)

        # determine idef(sweep_var_type)
        set stat [catch {mui_camp_cmd "varGetVal $idef(sweep_camp_var)"} v]
        if { $stat == 0 } {
            if { [string is integer -strict $v] } { 
                set idef(sweep_var_type) integer
            } elseif { [string is double -strict $v] } {
                set idef(sweep_var_type) double
            } else {
                set stat 1
            }
        }
        if { $stat } { 
            return "Sweep variable $idef(sweep_camp_var) is not working"
        }
        set v [expr { $idef(sweep_from)/double($idef(divide_by)) }]
        if { $idef(sweep_var_type) eq "integer" } {
            set v [expr { round($v) }]
        }
        if { [string length $idef(init_script)] } {
            #puts "Execute init script: set ins $i; set val $v; $idef(init_script)"
            if { [catch { mui_camp_cmd "set ins $i; set val $v; $idef(init_script)" } msg ] } {
                return "Failed to initialize sweep device: $msg"
            }
        }
        # Prepare tolerance-test command
        if { [string length $idef(tol_test)] } {
            set idef(full_tol_test) "set ins [list $i]; set val [list $v]; $idef(tol_test)"
        }
    }
    return "OK"
}

#################################
# sweep_ready  -- say whether ImuSR Camp sweep is ready.
# Parameters: none
# Returns:    0  when (type is ImuSR, with tolerance test, and test fails)
#             1  otherwise

proc sweep_ready { } {
    global idef run
    set intol 1
    if { $run(ImuSR) && [string length $idef(full_tol_test)] } {
        set intol 0
        if { [catch {mui_camp_cmd $idef(full_tol_test)} intol] } {
            autorun_error $intol
            return 0
        }
    }
    return $intol
}


#################################
# imusr_auto_set_runtitle - set the "runtitle" for ImuSR runs
# This is copied from imusr_extra.tcl, changing "rtitles" to "run"

set run(other) ""

proc imusr_auto_set_runtitle { } {
    global run idef 
    idef_init_sweep_kind
    set run(subtitle) $run(other)
    if { $idef(sweep_kind) ne "T" && \
             [string length $run(temperature)] && \
             [string first $run(temperature) $run(subtitle)] < 0 } {
        set run(temperature) [mui_convert_temperature $run(temperature)]
        append run(subtitle) " " $run(temperature)
    }
    if { $idef(sweep_kind) ne "B" && \
             [string length $run(field)] && \
             [string first $run(field) $run(subtitle)] < 0 } {
        set run(field) [mui_convert_field $run(field)]
        append run(subtitle) " " $run(field)
    }
    set run(subtitle) [string trim $run(subtitle)]
    set run(runtitle) [join [list $run(sample) \
                $idef(sweep_from)-$idef(sweep_to):$idef(sweep_incr) \
                $run(subtitle)] "  "]
    odb_set_runinfo
}

#   Initialize the non-ODB sweep-kind parameter (can get rid of this
#   if we put sweep_kind in odb).
proc idef_init_sweep_kind { } {
    global idef imusr_inst_defs
    if { ![info exists imusr_inst_defs] } { catch { idef_read_inst_def } }
    if { ![info exists idef(sweep_kind)] } {
        set idef(sweep_kind) "B"
        if { [string match -nocase "camp" $idef(sweep_device)] && \
                [info exists imusr_inst_defs($idef(sweep_ins_type))] } {
            set idef(sweep_kind) [lindex $imusr_inst_defs($idef(sweep_ins_type)) 1]
            puts "Initialize sweep_kind to $idef(sweep_kind)"
        }
    }
}


#################################
# auto_start_of_run  -- actions when run is ready to start
# Parameters: none
# Returns:    uncaught errors, if any.

proc auto_start_of_run { } {
    #puts "Auto start of run"
    global run autor rstat midas
    # apply odb settings
    #puts "Perform Odb settings: $::arun::odbset"
    foreach {var val} $::arun::odbset {
        if { [catch {midas set_value $var $val} msg] } {
            autorun_warning "Failed to set ODB variable $var: $msg"
        }
    }
    # set run titles (except main runtitle)
    odb_get_runinfo
    array set run [array get ::arun::titles]
    odb_set_runinfo
    # ensure some special variables are being logged by Camp
    set allLV {}
    catch { set allLV [mui_camp_cmd sysGetLoggedVars] }
    set specialV [regexp -all -inline {</[^ <>]+>} $run(runtitle)]
    lappend specialV $run(autoTvar) $run(autoBvar)
    foreach vv $specialV {
        set vv [string trim $vv " <>"]
        if { [string match "/?*" $vv] && [lsearch -exact $allLV $vv] < 0 } {
            catch { mui_camp_cmd "varDoSet $vv -l on -l_act log_mdarc" }
        }
    }
    # then set run title (with magic substitutions)
    set arun::usedtitle ""
    if { $run(ImuSR) && $midas(musr) } {
        imusr_auto_set_runtitle
    } else {
        auto_apply_runtitle
    }
    # Zero Camp statistics
    catch { mui_camp_cmd { foreach v [sysGetInsNames] {varDoSet /$v -z} }}
    # Zero histogram totals (because problem in bnmr; this may be temporary).
    if { $midas(musr) } {
        midas set_value "/Equipment/MUSR_TD_acq/mdarc/histograms/output/total saved" 0
        midas set_value "/Equipment/MUSR_I_acq/settings/input/num points" 0
    } else {
        midas set_value "/Equipment/FIFO_acq/mdarc/histograms/output/total saved" 0
    }

    set run(startingat) [clock seconds]

    # Tell Midas to start
    set autor(message) "Begin run $::arun::runnumber at [atstamp]"
    puts "Begin run $::arun::runnumber (previous $run(number)) at [atstamp] !!!!!!!!!!!!!!!!!!!!!!!"
    odb_begin_run
}

#################################
# auto_end_of_run  -- actions when run is ready to end (or it has already ended)
# Parameters: none
# Returns:    boolean for caught errors

proc auto_end_of_run { } {
    global autor
    #puts "auto_end_of_run"
    if { [catch {
        set ::arun::paused_at 0
        odb_end_run
        set ::arun::state ending
        set autor(state) $::arun::state
        set autor(message) "End run $::run(number) at [atstamp]"
	puts $autor(message)
        odb_set_autor_srv
    } msg ] } {
	puts "auto_end_of_run fails with error: $msg"
    }
}

########################################
# auto_main_loop - the main engine of the autoruns daemon (midas client)
#
proc auto_main_loop { repeats } {
    global run autor idef rstat epics midas

    #puts "Autorun main loop ($repeats).  State: $::arun::state"
    #puts "wait_until = $::arun::wait_until  (now [clock seconds])"

    # Get Midas update
    auto_odb_reconnect
    if { [catch {
        odb_get_general
        odb_get_autor
        if {$autor(enabled)} {
            odb_get_runinfo
            if { $run(ImuSR) } { odb_get_idef }
        }
    } msg ] } {
        puts "Failed to get odb parameters: $msg "
        if { $repeats > 3 } {
            autorun_warning "Failed to get odb parameters: $msg "
            set repeats -1
        }
        after cancel $::arun::afterid
        set ::arun::afterid [after 20000 [list auto_main_loop [incr repeats]]]
        return
    }
    if { $autor(refresh_period) < 3 } { 
        set autor(refresh_period) 5 
    }
    if { $autor(plan_seek_time) > $autor(refresh_period) } { 
        set autor(plan_seek_time) $autor(refresh_period) 
    }

    if { $::arun::busy_main } {
        if { $repeats < 6 } {
            puts "Skip main loop iteration -- previous still busy"
            after cancel $::arun::afterid
            set ::arun::afterid [after [expr {1000*$autor(refresh_period)}] [list auto_main_loop [incr repeats]]]
            return
        } else {
            autorun_warning "Main loop stuck over $repeats iterations"
            set repeats -1
        }
    }

    after cancel $::arun::afterid
    set ::arun::afterid [after [expr {1000*$autor(refresh_period)}] [list auto_main_loop 0 ]]
    set ::arun::busy_main 1
    if { [catch { auto_iterate } msg] } {
        puts "main loop iteration failed with error $msg\ntrace:\n$::errorInfo"
    }
    set ::arun::busy_main 0
}

proc auto_iterate { } {
    global run autor idef rstat epics midas
    #puts "Autorun [lindex {disabled enabled} $autor(enabled)]  state: $autor(state)  file: $autor(filename)"
    # check, and handle, filename changes
    #puts "! [string equal  $autor(filename) $::plan::filename] || [string equal $autor(state) reload]"
    if { $autor(filename) ne $::plan::filename || $autor(state) eq "reload" } {
        set ::plan::filename $autor(filename)
        set ::plan::modtime 0
        after 0 look_for_plan
    }

    # check if we have just been enabled/disabled
    if { $autor(enabled) != $::arun::enabled } {
        set ::arun::enabled $autor(enabled)
        if { $autor(enabled) } {
            set autor(message) "Autoruns enabled at [atstamp]"
            midas msg 2 autorun "Autoruns enabled"
            set ::plan::modtime 0
            set ::plan::filename $autor(filename)
            set ::arun::state idle
            after 0 look_for_plan
        } else {
            set autor(message) "Autoruns disabled at [atstamp]"
            midas msg 2 autorun "Autoruns disabled"
            set ::arun::state disabled
            auto_cancel_settings
        }
    }

    # set autor(message) ""

    switch $::arun::state {
        disabled {
	}
        idle {
	}
        paused -
        acquiring {
            if { $run(number) != $::arun::runnumber } {
                if { $run(number) == 0 } {
                    return [autorun_anomaly "Run number changed to 0; expected $::arun::runnumber" ]
                } else {
                    return [autorun_abort "Run number was changed (see $run(number) expected $::arun::runnumber) when acquiring" ]
                }
            }

            if { [catch { odb_get_stat } msg] } {
                puts "FAILED TO GET STATUS: $msg"
            }

            # puts "acquiring, $run(expertype) ($run(ImuSR))"
            if { $run(ImuSR) } {
                set ::arun::config(sweeps) $idef(num_sweeps)
                # check if sweeps or elapsed >= requested, or if run ended on its own
                if { !$run(in_progress) } {
                    set autor(message) "TI run ended naturally"
                    puts $autor(message)
                    return [auto_end_of_run]
                }
                if { $idef(num_sweeps) > 0 && $rstat(sweep_num) > $idef(num_sweeps) } {
                    set autor(message) "End TI run after $idef(num_sweeps) sweeps"
                    puts $autor(message)
                    return [auto_end_of_run]
                }
                if { $autor(elapsed) > 0 && [clock seconds]-$run(start_sec) > $autor(elapsed) } {
                    set autor(message) "End TI run after elapsed time [expr {round(([clock seconds]-$run(start_sec))/60.0)}] min"
                    puts $autor(message)
                    return [auto_end_of_run]
                }
            } else { # TD-
                # For TD-muSR, run ending implies problem or manual intervention; for TD-bnmr the run might
                # have been ended by mdarc, so keep on trucking (but don't move on immediately).
                if { $run(in_progress) } {# reset no-progress counter
                    set ::arun::noprogress 0
                } else {
                    if { $midas(musr) } {
                        set ::arun::state idle
                        return [autorun_warning "Run was stopped manually. Autoruns aborted." "info"]
                    } elseif { [incr ::arun::noprogress] >= 4 } {
                        set ::arun::state stopped
                        return [autorun_warning "Run was stopped manually. Autoruns continue." "info"]
                    }
                }
                # If dynamic title:
                if { [info exists ::arun::title] && [string match "*<*>*" $::arun::title] } {# potentially dynamic title
                    if { $::arun::title ne $::arun::usedtitle } {
                        #puts "Plan title has changed. Use it."
                        auto_apply_runtitle
                    } elseif { $::arun::title ne $::arun::settitle } {# Plan title is same, but dynamic
                        if { $run(runtitle) eq $::arun::settitle } {
                            #puts "Current title is what we remember setting, so re-set it with updated parameters"
                            auto_apply_runtitle
                        } elseif { [string match "title*" [auto_run_titles_compare]] } {# title will be first mismatch listed
                            #puts "Mismatched run title. Do not update"
                        } else {
                            #puts "Existing title was changed, but has the form of what we set, so do dynamic setting"
                            auto_apply_runtitle
                        }
                    }
                }
                #   check if counts or elapsed >= requested
                if { $autor(counts) > 0 && $autor(counthist) > $rstat(nhist) } {
                    autorun_warning "Histogram selected for counts totals ($autor(counthist)) does not exist!\nUse all histograms."
                    set autor(counthist) -1
                }
                if { $autor(counthist) > 0 } {
                    set tot [lindex $rstat(htotals) [expr {$autor(counthist)-1}]]
                } else {
                    set tot $run(hist_total)
                }
                if { $autor(counts) > 0 && $tot >= $autor(counts) } {
                    set autor(message) "End TD run when counts $tot >= $autor(counts)"
                    puts $autor(message)
                    return [auto_end_of_run]
                }
                if { $autor(elapsed) > 0 && [clock seconds]-$run(start_sec) >= $autor(elapsed) } {
                    #puts "Found that { [clock seconds]-$run(start_sec) >= $autor(elapsed) }"
                    set autor(message) "End TD run after elapsed time [expr {[clock seconds]-$run(start_sec)}] sec"
                    puts $autor(message)
                    return [auto_end_of_run]
                }
            }
            # If configured to do so, automatically pause and resume run while in progress
            if { $autor(pausing) } {
                if { $run(paused) && $::arun::paused_at > 0 } {
                    #puts "Run is paused/hold; are requirements satisfied? [requirements_satisfied]"
                    if { [requirements_satisfied] } {
                        set autor(message) "Resume run when requirements satisfied"
                        puts $autor(message)
                        catch { odb_resume_run }
                        set ::arun::state acquiring
                        set ::arun::paused_at 0
                    }
                } else {
                    #puts "Run is acquiring; are requirements satisfied? [requirements_satisfied]"
                    if { ![requirements_satisfied] } {
                        set autor(message) "Pause run when requirements not satisfied ($::arun::what_unsat)"
                        puts $autor(message)
                        if { [clock seconds] - $::arun::paused_at > 60 } { 
                            catch { odb_pause_run }
                            set ::arun::paused_at [clock seconds]
                        }
                        set ::arun::state paused
                    }
                }
            }
        }
        ending {
            if { $run(in_progress) } {
                if { [incr ::arun::repeats] > 50 } {
                    return [autorun_abort "Failed to end run -- aborting" ]
                }
                catch { odb_end_run }
            } else {
                set ::arun::state stopped
            }
	}
        stopped {
            set ::arun::repeats 0
            auto_cancel_settings
            if { $run(in_progress) } {
                return [autorun_anomaly "Run was started mysteriously" ]
            }
            # check: Is there another run in the plan (next number)?
            set r [expr {$run(number) + 1}]
            if { ! [info exists ::plan::config($r)] } {
                set ::arun::state idle
                #puts "Runs in plan: [array names ::plan::config]"
                #parray ::plan::config
                return [autorun_warning "Run plan is finished" info]
	    }
            # Copy info for next run from the Plan (sets ::arun::runnumber = $r = $run(number) + 1)
	    copy_one_run_plan $r

            # Change experiment type, if necessary
            if { [info exists ::arun::config(musrtype)] } {    
                if { $run(ImuSR) } {
                    if { [string match -nocase "T*" $::arun::config(musrtype)] } {
                        set autor(message) "Switch data acquisition type to TD-\u00b5SR at [atstamp]"
                        catch { odb_set_exper_type "TD-\u00b5SR" "tdmusr" }
                        # start the (new) frontend
                        puts "Start tdmusr frontend"
			set ie [catch { exec $::env(MUSR_DIR)/musr_midas/bin/start_fe &} msg]
			puts "catch reports $ie $msg"
                        return
                    }
                } else {
                    if { [string match -nocase "I*" $::arun::config(musrtype)] } {
                        set autor(message) "Switch data acquisition type to I-\u00b5SR at [atstamp]"
                        catch { odb_set_exper_type "I-\u00b5SR" "imusr" }
                        # start the (new) frontend
                        puts "Start Imusr frontend"
			set ie [catch { exec $::env(MUSR_DIR)/musr_midas/bin/start_fe &} msg]
			puts "catch reports $ie $msg"
                        return
                    }
                }

            }
            # Set config parameters
            set autor(message) "Make settings for run $r at [atstamp]"
	    puts $autor(message)
            if { $run(ImuSR) } {
                # Set, in order, any of: load setup, set sweep range, set tolerance, set num sweeps
                if { [info exists ::arun::config(setup)] } {
                    set f [file join $idef(setup_path) $::arun::config(setup).idef]
                    if { ![file exists $f] || [catch { odb_load_file $f }] } {
                        return [autorun_abort "No such I-\u00b5SR setup: \"$::arun::config(setup)\"" ]
                    }
                }
                if { [catch { 
                    if { [info exists ::arun::config(sweeprange)] } {
                        lassign $::arun::config(sweeprange) idef(sweep_from) idef(sweep_to) idef(sweep_incr)
                    }
                    if { [info exists ::arun::config(tolerance)] } {
                        set idef(tolerance) $::arun::config(tolerance)
                    }
                    if { [info exists ::arun::config(sweeps)] } {
                        set idef(num_sweeps) $::arun::config(sweeps)
                    }
                    set idef(final_point) 0
                    odb_set_idef 
                } msg ] } { return [autorun_error "Failed to set up sweeps: $msg"] }
                #
                #  For the moment, we do not validate the sweep limits, but rely
                #  on the frontends to do that.  (Later, we might copy the necessary
                #  functions from the mui.)
                # set stat [imusr_check_sweep_limits]
                # if { $stat ne "OK" } {
                #     return [autorun_warning $stat]
                # }
                if { $midas(musr) } {
                    # Perform pre-run actions, but only for musr (not bnmr)
                    set stat [auto_imusr_pre_start]
                    if { $stat ne "OK" } {
                        return [autorun_abort $stat]
                    }
                }
            } else {# TD-muSR
                if { [info exists ::arun::config(mode)] && $midas(musr) } {
                    if { [catch {
			puts "Setting mode to $::arun::config(mode)"
                        global rig
                        odb_get_rig
                        set f [file join $rig(rigpath) $rig(rigname) $::arun::config(mode).mode]
                        if { ![file exists $f] || [catch { odb_load_file $f }] } {
                            return [autorun_abort "No such mode as \"$::arun::config(mode)\" for rig $rig(rigname)" ]
                        }
                        odb_get_mode
                    } msg ] } { return [autorun_error "Failed to set mode: $msg"]}
                }
            }
            set epics(retries) 0
            set ::arun::repeats 0
            # Next stage will be "setting", but...
            set ::arun::state setting
            # ...Check first if there are actually settings to be made.
            if { [llength $::arun::epicsset] + [llength $::arun::campset] == 0 } { # then skip "setting" stage
                set ::arun::state changing
                after cancel $::arun::afterid
                set ::arun::afterid [after 500 [list auto_main_loop 0 ]]
                set ::arun::wait_until [expr {[clock seconds] + $::arun::config(max_wait)}]
            }
        }
        setting {
	    # puts "Make settings now"
            set s "changing"
            if { $run(in_progress) } {
                set mismat [auto_run_titles_compare]
                if { [string length $mismat] } {
                    return [autorun_abort "User started run manually, but the header info doesn't match the plan (for $mismat)"]
                }
                set ::arun::state acquiring
                set autor(message) "User started run manually ([atstamp])"
                puts $autor(message)
                return
            }
            # Perform epics settings.   If some slow settings are in progress, abort
            # this iteration and wait for next.

            if { [llength $::arun::epicsset] } {
                if { $epics(busy) } {
                    puts "Can't make epics settings yet -- busy at [atstamp]"
                    return
                }
                puts "Perform Epics settings at [atstamp]."
                set i 0
                foreach { delay ecmd } $::arun::epicsset {
                    incr i 2
                    if { $delay <= 0 } {
                        if { [catch {
			    puts "Perform epics setting $ecmd"
                            autor_epics_cmd $ecmd
                        } msg] } {
                            if { [incr epics(retries)] >= $epics(maxtries) } { # will not retry; remove from list
                                autorun_error "Epics command \"$ecmd\" failed ($epics(retries) times):\n$msg\nForget it!"
                                incr i -2
                                set ::arun::epicsset [lreplace $::arun::epicsset $i [expr {$i+1}]]
                            } else { # will retry
                                set s "setting"
                                if { $epics(retries) % 5 == 0 } { # occasional error vs warning
                                    autorun_error "Epics command \"$ecmd\" failed ($epics(retries) times); will retry"
                                } else {
                                    autorun_warning "Epics command \"$ecmd\" failed ($epics(retries) times); will retry"
                                }
                            }
                        } else { # remove successful command from list
                            incr i -2
                            set ::arun::epicsset [lreplace $::arun::epicsset $i [expr {$i+1}]]
                        }
                    }
                }
            }

            if { [llength $::arun::campset] } {
                puts "Perform Camp settings ($::arun::repeats) at [atstamp]."
                set i 0
                foreach {delay ccmd} $::arun::campset {
                    incr i 2
                    if { $delay <= 0 } {
                        puts "Camp command $ccmd"
                        if { [catch {
                            mui_camp_cmd $ccmd
			    puts "Perform Camp setting $ccmd"
                        } msg] } { # failed:
                            set s "setting"
                            if { $::arun::repeats == 20 } {
                                autorun_error "Camp command $ccmd failed ($::arun::repeats times)\n$msg"
                            } elseif { $::arun::repeats % 5 == 1 } {
                                autorun_warning "Camp command $ccmd failed ($::arun::repeats times)\n$msg"
                            }
                        } else { # remove successful command from list
                            incr i -2
                            set ::arun::campset [lreplace $::arun::campset $i [expr {$i+1}]]
                        }
                    }
                }
            }

            # Only schedule delayed commands once, when immediate commands were successful
            if { $s eq "changing" } {
                puts "Completed the immediate settings at [atstamp]."
                if { [llength $::arun::campset] } {
                    puts "Schedule the delayed Camp settings."
                    foreach {delay ccmd} $::arun::campset {
                        if { $delay > 0 } {
                            lappend ::arun::delayed_set_tags [after $delay [list delayed_setting_cmd $ccmd]]
                        }
                    }
                }
                if { [llength $::arun::epicsset] } {
                    puts "Schedule the delayed Epics settings."
                    foreach {delay ecmd} $::arun::epicsset {
                        if { $delay > 0 } {
                            lappend ::arun::delayed_set_tags [after $delay [list delayed_setting_cmd $ecmd]]
                        }
                    }
                }
            } elseif { [incr ::arun::repeats] > 25 } {
                autorun_error "Failed to perform all settings!"
                set s "changing"
            }

            # initialize timeout for "changing" state
            set ::arun::wait_until [expr {[clock seconds] + $::arun::config(max_wait)}]
	    #puts "Now [clock seconds]. Set wait_until = $::arun::wait_until,  next state: $s"
            set ::arun::state $s
	}
        changing {
            if { $run(in_progress) } {
                set mismat [auto_run_titles_compare]
                if { [string length $mismat] } {
                    return [autorun_abort "User started run manually, but the header info doesn't match the plan (for $mismat)"]
                }
                set ::arun::state acquiring
                set autor(message) "User started run manually ([atstamp])"
                puts $autor(message)
            } else {
                set sat [requirements_satisfied]
                # previous line may be slow, so update run status 
                catch {odb_get_general; odb_get_runinfo}
                if { $sat == -1 } {
                    set ::arun::state idle
                    return [autorun_warning "Run plan is finished" info]
                } elseif { ($sat == 1 && [sweep_ready]) || ([clock seconds] > $::arun::wait_until) } {
		    if { [clock seconds] > $::arun::wait_until } {
			if { $::arun::config(max_wait) > 200 } {
			    set w "[expr {$::arun::config(max_wait)/60}] minutes"
			} else {
			    set w "$::arun::config(max_wait) seconds"
			}
                        autorun_error "Settings would not stabilize after $w. Start run $::arun::runnumber anyway!"
		    }
                    if { $run(in_progress) || $run(transition) } {
                        set autor(message) "Run already in progress ([atstamp])"
                        puts "Run already in progress ([atstamp])"
                        set ::arun::state starting
                        set ::arun::repeats 0
                        set run(startingat) [clock seconds]
                    } else {
                        if { $run(number) + 1 != $::arun::runnumber } {
                            autorun_error "Strange run number (see $run(number), expected [expr {$::arun::runnumber-1}]) when changing"
                        }
                        if { [catch { auto_start_of_run } msg] } {
                            autorun_error "Failed to start run: $msg"
                        } else {
                            set ::arun::state starting
                            set ::arun::repeats 0
                        }
                    }
                } else {
                    set autor(message) "Wait for requirements to be satisfied ($::arun::what_unsat)"
                }
            }
        }
        starting {
            set ::arun::noprogress 0
            if { $run(in_progress) } {
                puts "Run is in progress!"
                set ::arun::state acquiring
                set ::arun::repeats 0
            } elseif { [incr ::arun::repeats] > 5 } {
                set sec [expr {[clock seconds] - $run(startingat) + 1}]
                if { $::arun::repeats >= 7 && $sec > 30 } {
                    if {[catch {set msg [recent_midas_messages 11 $sec 20 .*]} msg]} {
                        autorun_error "Failed to start run." "Could not retrieve error messages;\n$msg\nSee midas.log instead."
                    } else {
                        autorun_error "Failed to start run." "Messages:\n$msg"
		    }
                    odb_cancel_transition
                    # Go back to re-try starting run.
                    set ::arun::wait_until [expr {[clock seconds] + $::arun::config(max_wait)}]
                    set ::arun::state changing
                }
            }
	}
    }
    set autor(state) $::arun::state
    if { [catch { odb_set_autor_srv } msg] } {
        puts "Failed odb_set_autor_srv: $msg"
    }
    set ::arun::anomalies 0
    # puts "autor(message) = $autor(message)"
    return
}


########################################
# requirements_satisfied -- determine if external conditions are good yet.
# Parameters: none
# Globals: ::arun::requirements - a list of requirements (each is a list)
# Returns: 1 when OK
#          0 if requirements not satisfied, or if Camp alarm is ringing, or
#            epics command is running
#         -1 if we are finalizing (stopping)
# Sets:    ::arun::unsatisfied - a deduplicated list of unsatisfied req's.
#          ::arun::what_unsat - shortened text version of the list.
# Actions: When a requirement with an attached action (When command) is
#          first satisfied, the action(s) are executed (as commands in global
#          scope), and then that requirement is removed from the list,
#          so it will not be checked again (and the run can begin, even if 
#          the condition becomes unsatisfied later).  
# 
#          To minimize network delays, all Camp variables are read with
#          a single access; likewise for all Epics variables.
#
#          Question: what to return on errors?
#
proc requirements_satisfied { } {
    global epics
    # puts "Test all requirements"
    set satisfied 1
    set didset 0
    set ::arun::unsatisfied [list]
    set newreq $::arun::requirements
    set now [clock seconds]

    # First, loop over all requirements to extract list(s) of all relevant variables
    foreach req $::arun::requirements {
        # Pull apart this requirement as separate variables
        lassign $req var is at equal err time action log target

        if { $var eq "finalize" } { # We are finished
            set satisfied 0
            return -1
        } elseif { [string match {[-A-Z0-9_]*:*} $var] } { # Epics variable
            set eval($var) {}
        } elseif { [string match {/?*} $var] } { # Camp variable 
            set cval($var) {}
            if { [string length $is] } {
                # For string comparison, $equal holds "varSelGetValLabel" or "varGetVal".
                set ccmd($var) $equal
            } else {
                set ccmd($var) "varGetVal"
            }
        }
        if { [string match {[-A-Z0-9_]*:*} $equal] } { # Epics variable
            set eval($equal) {}
        } elseif { [string match {/?*} $equal] } { # Camp variable 
            set cval($equal) {}
            set ccmd($equal) "varGetVal"
        }
    }

    # Construct and execute necessary Camp command
    if { [array exists cval] } {
        set cvarlist [array names cval]
        set request "list"
        foreach var $cvarlist {
            append request " \[$ccmd($var) $var\]"
        }
        if { [catch {
            mui_camp_cmd $request
        } valstat ] } {
            set satisfied 0
            set ::arun::unsatisfied [list [set ::arun::what_unsat "connection to Camp"]]
            autorun_warning $valstat
            return 0
        }
        # Cache the returned values
        foreach var $cvarlist val $valstat { set cval($var) $val }
    }

    # Construct and execute necessary Epics command
    if { [array exists eval] } {
        if { $epics(busy) } {
            # Can't check because setting in progress
            set ::arun::what_unsat "Epics setting"
            set ::arun::unsatisfied "EpicsSet"
            return 0
        }
        set evarlist [array names eval]
        set request "caget [join $evarlist { }]"
        if { [catch {autor_epics_cmd $request} valstat ] || \
                 [string match "*Channel connect timed out*" $valstat] } {
            autorun_warning "Failed Epics read: $valstat"
            set satisfied 0
            return 0
        }
        # Cache the returned values (yes, in the cval array; call it "cached values" now)
	#puts "Epics returned:\n$valstat"
        foreach line [split $valstat "\n"] {
            if { [scan "$line\n" "%s %\[^\n\]" var val] == 2 } {
                if { [info exists eval($var)] } {
		    #puts "set cval($var) {[string trim $val]}"
                    set cval($var) [string trim $val]
                } else {
                    autorun_warning "Unexpected epics var $var ($val)"
                }
            } else {
                puts "Unusable epics return line \"$line\""
            }
        }
    }

    # Again, loop over all requirements, evaluating them using cached values cval()
    set i 0
    foreach req $::arun::requirements {

        set newlog [list]
        set abovebelow 0

        # Pull apart this requirement as separate variables
        lassign $req var is at equal err time action log target

        # If run in progress, tests for pausing use no history
        if { $::run(in_progress) } { 
            set time 1
        }

        if { ! [info exists cval($var)] } {
            puts "Could not acquire value for $var"
            # set satisfied 0
            continue
        }
        set val $cval($var)

        if { [string length $is] } {  # Handle comparison separately for strings ("is")

            set sat1 [string equal [string trim $is] [string trim $val]]

        } else {  # Now we handle the usual case of numeric comparisons.

            # append value to the list with timestamp
            lappend log $now $val
        
            # Determine what number to use as the target
            set target $val
            if { [string length $at] } {
                set target $at
                # Variable $equal may give relation for above/below comparisons
                set abovebelow [string match {[<>]} $equal]
            } elseif { [info exists cval($equal)] } {
                set target $cval($equal)
            }
            # Trim log list to retain only entries within the alotted time,
            # and notice logged values outside of errors from target
            set sat1 1
            foreach {t v} $log {
                if { $t >= $now-$time } {
                    lappend newlog $t $v
                    if { $abovebelow } {
                        # note use of quotes in expr, so $equal expands to < or >.
                        set sat1 [expr "$sat1 && ( $v $equal $target)"]
                    } else {
                        set sat1 [expr { $sat1 && ( abs($v-$target) <= $err ) }]
                    }
                }
            }
        }
        if { $sat1 } {
	    puts "Satisfied $var requirement."
	} else {
            set satisfied 0
            lappend ::arun::unsatisfied $var
        }
        if { $sat1 && [llength $action] } {
            # When a test with action is passed, perform the action(s), then erase 
            # this requirement.
            foreach {delay scmd} $action {
                # ignore null actions...
                if { $scmd ne "" && $scmd ne "null" } {
                    if { $delay > 0 } {
                        puts "When $var satisfied, after [expr 0.001*$delay]s do $scmd"
                        lappend ::arun::delayed_set_tags [after $delay [list delayed_setting_cmd $scmd]]
                    } else {
                        puts "When $var satisfied, do $scmd"
                        delayed_setting_cmd $scmd
                    }
                    set didset 1
                }
            }
	    set newreq [lreplace $newreq $i $i]
        } else {
            # Did everything for this requirement.  Now copy the updated log
            # into the main requirements list (in future, can use lset command)
            set newreq [lreplace $newreq $i $i \
                   [list $var $is $at $equal $err $time $action $newlog $target]]
            incr i
        }
    }
    # ...end of loop over requirements

    set ::arun::requirements $newreq

    # That finishes testing all requirements.  Now also check for alarms.

    if { ![catch { mui_camp_cmd "sysGetAlertVars" } stat ] } {
        if { [string length $stat] } {
            set satisfied 0
            lappend ::arun::unsatisfied "Camp alarm"
        }
    }

    # Check for uncompleted beam tuning being performed asynchronously, or other epics script

    if { $epics(tuning_status) == "tuning" } {
        #puts "Not satisfied because still tuning"
        set satisfied 0
        lappend ::arun::unsatisfied "Beam tuning"
    } elseif { [string length $epics(tuning_status)] } {
        puts "[atstamp] Beam tuning completed with status $epics(tuning_status)"
        set epics(tuning_status) ""
    }

    # Prevent immediate finish when last requirement had an action

    if { $satisfied && $didset } {
        set satisfied 0
        lappend ::arun::unsatisfied "Setting"
    }

    if { $satisfied } {
        set ::arun::what_unsat ""
    } else {
        set ::arun::unsatisfied [lsort -unique $::arun::unsatisfied]
        set ::arun::what_unsat [llength $::arun::unsatisfied]
        if { $::arun::what_unsat < 3 } {
            set ::arun::what_unsat [join $::arun::unsatisfied " and "]
        }
    }

    return $satisfied
}

# A wrapper for camp_cmd or epics caput or epics scripted command.
# It only runs when autoruns are operating.
proc delayed_setting_cmd { scmd } {
    global epics
    if { $::arun::enabled && $::arun::state ne "idle" } {
        switch -glob -- $scmd {
            "caput *" { # epics setting (caput = epicsset only)
                if { $epics(busy) } {
                    queue_epics_cmd $scmd
                    return
                }
                if { [catch {
                    puts "Executing delayed epics command $scmd at [atstamp]"
                    autor_epics_cmd $scmd
                }] } {
                    autorun_warning "Scheduled Epics command $scmd failed. Skipping."
                }
            }
            "perl *" -
            "??sh -e *" { # epics scripted action (uses bash or tcsh or perl)
                if { $epics(busy) } {
                    queue_epics_cmd $scmd
                    return
                }
                if { [catch {
                    puts "Executing delayed epics script $scmd at [atstamp]"
                    autor_epics_cmd $scmd
                }] } {
                    autorun_warning "Scheduled Epics script $scmd failed. Skipping."
                }
            }
            default { # camp
                if { [catch {
                    puts "Executing delayed camp command $scmd at [atstamp]"
                    mui_camp_cmd $scmd
                } msg ] } {
                    puts "Scheduled Camp command $scmd failed:\n$::errorInfo"
                    autorun_warning "Scheduled Camp command $scmd failed. Skipping."
                }
            }
        }
    }
}

# Use trace to queue an epics setting command. In future, maybe I'll have an
# epics subserver like I use for camp, but camp version is not great.
proc queue_epics_cmd { ecmd } {
    global epics
    if { $epics(busy) } {
        if { [llength [trace info variable epics(busy)]] == 0 } {
            trace add variable epics(busy) write dequeue_epics_cmd
        }
        lappend epics(queue) $ecmd
    } else {
        delayed_setting_cmd $ecmd
    }
}

proc dequeue_epics_cmd { args } {
    global epics
    if { $epics(busy) == 0 && [llength $epics(queue)] } {
        set t [after 0 delayed_setting_cmd [lindex $epics(queue) 0]]
        lappend ::arun::delayed_set_tags $t
        set epics(queue) [lreplace $epics(queue) 0 0]
    }
}

# Cancel the delayed settings (if any pending).
# Note that the IDs are not (presently) being removed from the list when
# they are executed; it is simpler to erase them all together.  Moreover,
# cancelling an unknown id does nothing (but getting info is an error).
proc auto_cancel_settings { {which *} } {
    if { $which eq "*" || $which eq "epics" } {
        set ::epics(queue) {}
        set epics(cmd_status) {cancelled 0 666 cancelled}
    }
    if { [llength $::arun::delayed_set_tags] <= 0 } {
        return
    }
    set survivors {}
    set num 0
    foreach tag $::arun::delayed_set_tags {
        catch {
            lassign [lindex [after info $tag] 0] proc scmd
            if { $proc ne "delayed_setting_cmd" } { continue }
            switch -glob -- $scmd {
                "caput *" - "perl *" - "??sh -e *" {
                    set type epics
                }
                default {
                    set type camp
                }
            }
            if { [string match $which $type] } {
                after cancel $tag
                incr num
            } else {
                lappend survivors $tag
            }
        }
    }
    set ::arun::delayed_set_tags $survivors
    # The remainder is just for formatting the message
    if { $which eq "*" } {
        set which ""
    } else {
        append which " "
    }
    if { $num > 0 } {
        puts "$num delayed ${which}setting[plural $num] cancelled."
    }
}

#########################################
#
#  wrapper for odb_reconnect that never returns Tcl error, but will exit after
#  prolonged loss of connection.

proc auto_odb_reconnect { } {
    global midas

    catch odb_reconnect

    if { $midas(connected) } {
        set midas(disconnum) 0
        set midas(connat) [clock seconds]
    } else {
        if { [incr midas(disconnum)] > 50 && [clock seconds] - $midas(connat) > 180 } {
            autorun_error "Prolonged failure to connect to midas system.  Exiting soon..."
            after 60000 {if {!$midas(connected)} { exit }}
        }
    }
}

#########################################
#
# Check midas client list to guard against multiple instances
# This is performed right at start-up
# If there are 2 instances, we just raise an error.
# If more, we exit.

proc auto_sole_client { } {
    global midas env

    auto_odb_reconnect

    set n 0
    set cl {}
    # Require No errors, and client list ($cl) has even number of elements
    if { $midas(connected) && ! [catch { midas scl } cl] && ([llength $cl] % 2) == 0 } {
        #puts "Valid client list"
        set txt ""
        foreach {c h} $cl {
            if { [string match "$midas(application)*" $c] } {
                incr n
                append txt "\n" $c " on " $h
            }
        }
	if { $n > 1 } {
	    append txt ".\nYou should shut down [expr {$n-1}] (or all) of them."
            if { $n < 3 } {
                autorun_error "Multiple instances of autorun are connected: ${txt}\n(This instance continues running.)"
            } else {
                autorun_error "Multiple instances of autorun are connected: ${txt}"
                odb_disconnect
                after 15000 "odb_disconnect; exit"
            }
        }
    } else { # Failed to get proper client list
        autorun_error "Failed to check on Midas clients:\n$cl\nSomething might be seriously wrong!"
        odb_disconnect
        after 15000 "odb_disconnect; exit"
    }
    return $n
}

############## Error handling/messaging ##############################################

# Give a time-stamp
proc atstamp { } {
    return [clock format [clock seconds] -format {%H:%M:%S}]
}


#########################################
#
# Display autorun errors on the console, in the Midas log, and in a custom popup
# (if Tk available).  If the popup is already active, add this message to whatever
# is there.  Additional verbose messages may be given after the primary message;
# they are appended to the first message for the pop-up display, but *not* for
# the midas error log.

proc autorun_error { msg args } {
    global autor
    puts "Autorun error at [atstamp]:\n$msg [join $args]\nAutoruns are now $::arun::state\n"

    set shortm $msg
    if { [string length $msg] < 235 } { 
        append shortm " (see midas log)"
    }
    if { [string length $shortm] > 250 } {
        set shortm "[string range $shortm 0 250]..."
    }
    midas msg 1 autorun $shortm

    if { [string length $shortm] > 127 } { 
        set shortm "[string range $shortm 0 125]..."
    }

    set msg "$msg [join $args]"

    autorun_warning $msg "error"

    if { [llength [info commands toplevel]] } {
        catch {
            if { [winfo exists .autoerrors.text] } {
                # Error display exists; add this error to the message
                raise .autoerrors
                set prevtext [.autoerrors.text cget -text]
                set prevl [split $prevtext "\n"]
                if { [llength $prevl] > 33 } {
                    #puts "throw away earlier messages, length [llength $prevl] [join $prevl |]"
                    set prevtext [join [linsert [lrange $prevl end-20 end] 0 {...} ] "\n"]
                }
                if { [string length $prevtext] } {
                    set msg "$prevtext\n$msg"
                }
            } else {
                # Open Error display window
                set prevtext ""
                toplevel .autoerrors
                wm title .autoerrors "Autorun Error"
                label .autoerrors.head -anchor w -justify left \
                        -text "Autorun Error" -font muTitleFont
                label .autoerrors.text -anchor w -justify left -pady 5 \
                        -relief sunken -borderwidth 1 -wraplength 800 -text ""
                label .autoerrors.state -anchor w -text ""
                canvas .autoerrors.sep -height 0 -width 0 -relief sunken -borderwidth 1
                button .autoerrors.ok -text OK -width 8 -default active \
                        -command {after 10 {destroy .autoerrors}}

                bind .autoerrors <Return> {destroy .autoerrors ; break}
                grid .autoerrors.head  -padx 9 -pady 5 -sticky w
                grid .autoerrors.text  -padx 8 -sticky ew
                grid .autoerrors.state -padx 9 -pady 5 -sticky w
                grid .autoerrors.sep   -padx 0 -pady 5 -sticky ew
                grid .autoerrors.ok
            }
            # Apply text
            .autoerrors.text configure -text $msg
            .autoerrors.state configure -text "Autoruns: $::arun::state   at  [atstamp]"
            bell
        }
    }
}

proc autorun_close_error { } {
    if { [llength [info commands toplevel]] } {
        catch {
            if { [winfo exists .autoerrors.text] } {
                destroy .autoerrors
            }
        }
    }
}

proc autorun_abort { msg } {
    set ::arun::state idle
    autorun_error "Abort autoruns: $msg"
}

#  Here is a gentler wrapper for autorun_abort that tries to get past temporary Midas errors
#  without aborting.

proc autorun_anomaly { msg } {
    if { [incr ::arun::anomalies] < 3 } {
        odb_disconnect
        # coma (deeeep sleep) for 2 sec
        after 2000
    } else {
        return [autorun_abort $msg]
    }
}

# Produce an autorun warning.
# If email is requested, schedule emailing of the message, with
# some delay to group multiple messages: 
# TODO: add configurability to what level message gets emailed!

proc autorun_warning { msg {level warning}} {
    global autor
    puts "Autorun $level: $msg"
    catch {
        odb_get_autor
        set autor(state) $::arun::state
        set autor(message) [string range $msg 0 127]
        odb_set_autor_srv
    }
    if { [llength [info commands toplevel]] } {
        # Don't wait for the user to dismiss the warning, but do wait to 
        # see if autorun error popup is visible.
        after 50 [list after idle [list autorun_msgbox_maybe $level $msg]]
    }
    if { ! [info exists ::arun::config(email)] } {
        set ::arun::config(email) {}
        puts "ERROR - undefined email value"
    }
    switch -glob $level {
        info* { set addr $::arun::config(email) }
        err* { set addr [concat $::arun::config(email) $autor(email_extra)] }
        default { set addr [concat $::arun::config(email) $autor(email_extra)] }
    }
    set addr [lsort -unique $addr]
    if { [llength $addr] } {
        append ::arun::emessage "\nAutorun $level\n" $msg "\n"
	#puts "Append (email) message 'Autorun error:\n $msg'"
	#puts "Email will be sent to $addr"
        if { ! $autor(email_pending) } {
            set delaym [bounded $autor(email_delay_m) \
                 "$autor(email_separ_m)-([clock seconds]-$autor(email_sent_at))/60.0" \
                 30 ]
	    puts [format "Email warning to be sent in %.2f min" $delaym]
	    set delayms [expr {round(60000*$delaym)}]
            after $delayms {puts "Delayed email being sent now"; autorun_send_email}
            set autor(email_pending) 1
        }
    } else {
	puts "No email will be sent"
    }
}

# Pop up a timed messagebox with autorun warning. Do not wait for user
# response.  Skip popup when the error is visible in the .autoerrors window.

proc autorun_msgbox_maybe { level msg } {
    catch {
        bell
        if { $level != "error" || ![winfo ismapped .autoerrors] } {
            timed_messageBox -timeout 90000 -type ok -default ok \
                -message $msg -title "Autorun $level" -icon $level
        }
    }
}

proc autorun_send_email { } {
    global autor
    if { ! [info exists ::arun::config(email)] } { set ::arun::config(email) {} }
    if { ! [info exists autor(email_extra)] } { set autor(email_extra) {} }
    set addr [lsort -unique [concat $::arun::config(email) $autor(email_extra)]]
    puts "Email to $addr:\n$::arun::emessage"
    if { [llength $addr] && [info exists ::arun::emessage] && [string length $::arun::emessage]} {
        if { [catch {
            eval [list exec mail -s "Autorun error or notification" ] \
                $addr [list << [string trim $::arun::emessage]]
            # (with Tcl 8.5 will be able to use {*}.)
        } err] } {
            puts "Failed to send email to address $addr.\n$err.\nDisabled email"
            set ::arun::config(email) {}
            after 1000 autorun_error "Failed to send email to address $addr.  Disabled email."
        }
        set autor(email_sent_at) [clock seconds]
    } else {
	puts "No email requested"
    }
    set ::arun::emessage ""
    set autor(email_pending)  0
}

# Perform epics command, but only if enabled & not idle
proc autor_epics_cmd { cmd } {
    global midas epics
    #puts "autor_epics_cmd {$cmd}"
    if { ! $::arun::enabled } {
	#puts "Prevent command because autoruns not enabled."
	return ""
    }
    if { $::arun::state eq "idle" && ![string match "caget*" $cmd] } {
	#puts "Prevent command because settings not allowed when idle."
        return ""
    }
    if { $epics(busy) } {
	puts "Prevent command because epics is busy (concurrent command)."
        return ""
    }
    # Multiplet Tuning is handled differently
    if { [string match {*autotune/multiplet*} $cmd] } { # This will run asynchronously
        if { [string length $epics(tuning_status)] } {
            puts "Skip multiplet beam tuning because tuning appears to be active."
            return -code error "multiplet tuning already active"
        }
        set epics(tuning_status) "tuning"
        puts "BeamTuning: $cmd"
        if { [catch {blt::bgexec epics(tuning_status) -output epics(tuning_output) -error epics(cmd_errors) \
                         ssh -x -o PasswordAuthentication=no $epics(user)@$epics(host) $cmd &} msg] } {
            set epics(tuning_status) ""
            puts "Failed to start multiplet beam tuning: $msg"
            return -code error "multiplet tuning failed"
        }
        return ""
    }
    # Other Epics commands (caget, caput, or scripts) are performed synchronously
    puts "Epics command: $cmd"
    mui_epics_cmd $cmd
}

#   Display messages extracted from midas.log.
# Parameters:
#   num    Display (at most) this many messages
#   sec    Choose the latest, within this many seconds
#   nread  Look at only this many lines at the end of midas.log
#   client Choose messages from this client (or ".*")
# Returns:
#   Text of selected messages.
#   (or error)

proc recent_midas_messages { num sec nread client } {
    global midas
    #puts "Look for messages in midas log: $midas(log_file)"
    if {[catch {exec tail -n $nread $midas(log_file)} log]} {
        puts "error reading midas log file"
        return $log
    }
    set ret [list]
    set now [clock seconds]
    foreach line [split $log "\n"] {
        if { [regexp "^(.*?)\\\[$client\\\]\\s*(.*)\$" $line -- dddd msg] } {
            # [clock scan] cannot yet handle fractional seconds!!
            regsub {(\:\d\d)\.\d\d\d } $dddd {\1 } date
	    if { [catch {clock scan $date} when] } {
		puts "Error scanning time $date: $when"
                set when $now
            }
            if { $now - [clock scan $date] < $sec } {
                if { [lindex $ret end] ne $msg } {
                    lappend ret $msg
                }
            }
        }
    }
    return [join [lrange $ret end-[incr num -1] end] "\n"]
}


############################################################################
#
#    S T A R T
#
############################################################################

autorun_main


# -----------------------------------------------------------------------
#
# Data structure:
#
# The run plan is held in several array variables indexed by the run number,
# all in the namespace plan::
#
# When a particular run becomes current (its number is up), its values
# are copied to more convenient variables in the arun:: namespace.
# Some are copied straightforward (list -> list) and some are coverted 
# to arrays (list of key value -> array).  When any list variable is 
# copied from the plan:: to be an array in the arun::, the arun:: array 
# must be cleaned out first!
#
#
# plan::titles(rnum) --  dict/list of key-value pairs suitable for [array set],
#       including just what was given for the fields: sample, operator,
#       experiment, temperature, field, orientation, comment1, comment2,
#       extra, title. Ones not given are absent. (MAYBE title should should
#       be retained if none given????)
# 
# arun::titles(key) -- array var derived from plan::titles, with some 
#       modifications: title is omitted; temperature is omitted if it 
#       is an autoT path; field is omitted if it is an autoB path (both
#       will be filled in with proper values as the run progresses).
#       Every entry in arun::titles provides a pseudo-variable for
#       substitution into the run title.  Missing title entries will 
#       be read from the odb, so we will have a complete set of 
#       pseudo variables.
# 
# arun::title -- Run title as entered by user in plan
# arun::usedtitle -- previously used run title (may containg keyword patterns)
# arun::settitle -- previously used run title after keyword substitution
# 
# plan::config(rnum) -- dict/list of key-value pairs suitable for [array set],
#       with entries for whichever of the following were specified:
#       number musrtype setup mode sweeprange tolerance counts counthist 
#       elapsed max_wait email. The run "number" is always set each run, and
#       is always first. Both email and max_wait are explicitly copied
#       run-to-run (in parse_plan). The rest persist run-to-run through 
#       the odb settings.
# 
# arun::config(key) -- array made from plan::config(<this run>)
# 
# plan::campset(rnum) -- a list of pairs: delays and Camp commands to 
#       execute; derived from all the campset and camp_cmd requests for 
#       this run.  Delays are in ms.
# 
# arun::campset -- a direct copy; elements removed when performed
# arun::campmem -- a direct copy of initial campset list
# 
# plan::odbset(rnum) -- a list of pairs (not a list of lists) 
#       odb_variable  value
# 
# arun::odbset -- a direct copy.  
# 
# plan::epicsset(rnum) -- Epics commands to execute; derived from all the 
#       epicsset and loadtune requests for this run.  Pairs, with delays in ms.
# 
# arun::epicsset -- a direct copy; elements removed when performed
# arun::epicsmem -- a direct copy of initial
# 
# plan::requirements(rnum) -- readback requirements for the run.  Each entry
#       is a list (arbitrary length) of individual requirements.  Each
#       requirement is itself a list, with elements:
#          Camp var path or Epics PV name
#          comparison string (for "is", or null)
#          comparison value (for "at", or null)
#          camp path for comparison (for "equal", or null; or command name for "is" case)
#          error bar
#          time period (sec)
#          action list:
#             for "require" the action list is null
#             for "when", an even-length list, where each two elements are:
#                 delay: milliseconds to delay executing the command
#                 cmd: Camp or Epics command to execute.
# 
# arun::requirements -- is a similar list of requirements, each being a
#       list as well.  A couple of values are appended, making the list:
#          Camp var path or Epics PV name
#          comparison string (for "is", or null)
#          comparison value (for "at", or null)
#          camp path for comparison (for "equal", or null)
#          error bar
#          time period (sec)
#          action list (for "when"; null for "require")
#          value log: list of time value pairs
#          target: the current/previous comparison value as used.
# 
# When one run's requrements is copied from plan:: to arun::, the existing value log
# will be retained if the run number is the same (reloading plan) but discarded if
# the run changed (start autorun or progressing to next run).
