#   camp_inter_extra.tcl
#   Procedures for setting interface parameters for Camp instruments.

#   $Log: camp_inter_extra.tcl,v $
#   Revision 1.5  2006/06/29 06:26:13  asnd
#   Add a tcpip interface type
#
#   Revision 1.4  2006/05/26 16:59:52  asnd
#   Accumulated changes over winter shutdown.
#
#   Revision 1.3  2004/04/21 05:22:45  asnd
#   Add VME interface type
#
#   Revision 1.2  2003/11/20 03:27:44  asnd
#   Minor changes to appearance
#
#   Revision 1.1  2003/09/19 06:23:26  asnd
#   ImuSR and many other revisions.
#



if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

# Initialize the camp interface window.  There is one (optional) parameter:
# ins: The Camp instrument name to configure.  If not specified, then allow
# entry of device name/type.
  
proc cis_initialize { {ins {}} } {
    global ciswin cinter

    array unset cinter
    set cinter(online) 0
    set cinter(name) $ins
    set cinter(devinit) 0

    set cisw [td_win_name $ciswin]

    if { [ string length $ciswin ] } {
        # We are in a real application,
        if { [winfo exists $cisw] } {
            wm deiconify $cisw
            raise $cisw
        } else {
            toplevel $cisw
            camp_inter_ui $ciswin
        }
    }

    # Bind help
    cis_bind_help cis_devname_e "Unique Camp instrument name for device"
    cis_bind_help cis_online_cb "Instrument connection status"
    cis_bind_help cis_devtype_b "Camp instrument type"
    cis_bind_help cis_interface_b "Type of communications interface for device"
    cis_bind_help cis_delay_e "Minumum time between accesses (sec)"
    cis_bind_help cis_timeout_e "Maximum time (sec) to wait for response"
    cis_bind_help cis_port_b "RS232 port name (check label on wire)"
    cis_bind_help cis_baud_b "Communication rate: bits per second"
    cis_bind_help cis_parity_b "Use of parity bit in communications"
    cis_bind_help cis_databits_b "Number of data bits per byte"
    cis_bind_help cis_stopbits_b "Number of stop bits per byte"
    cis_bind_help cis_flush_cb "Whether to flush incoming text before reads (yes)"
    cis_bind_help cis_rs_readterm_b "Termination character(s) for reading device"
    cis_bind_help cis_rs_writeterm_b "Termination character(s) for writing to device"
    cis_bind_help cis_ieee_address_e "IEEE (GPIB) address of instrument"
    cis_bind_help cis_ieee_readterm_b "Termination character(s) for reading device"
    cis_bind_help cis_ieee_writeterm_b "Termination character(s) for writing to device"
    cis_bind_help cis_branch_e  "Number of the Camac branch (cable) (usually 0)"
    cis_bind_help cis_crate_e "Number of the Camac crate on the branch (usually 0)"
    cis_bind_help cis_slot_e "Slot number in the Camac crate"
    cis_bind_help cis_base_e "VME base address for instrument"
    cis_bind_help cis_ip_addr_e "IP address for networked instrument"
    cis_bind_help cis_ip_port_e "TCP/IP port number"
    cis_bind_help cis_persistent_cb "Use single persistent connection, or reopen"
    cis_bind_help cis_ip_readterm_b "Termination character(s) for reading device"
    cis_bind_help cis_ip_writeterm_b "Termination character(s) for writing to device"
    cis_bind_help cis_apply_b "Apply interface changes"
    cis_bind_help cis_quit_b "Close window - abandon any changes"
    cis_bind_help cis_OK_b "Apply any changes, and close window"

    wm title $cisw "Camp Interface"
    bind $cisw <Alt-l> [list $ciswin.cis_online_cb invoke]
    bind $cisw <Alt-a> [list $ciswin.cis_apply_b invoke]
    bind $cisw <Alt-q> [list $ciswin.cis_quit_b invoke]
    bind $cisw <Alt-o> [list $ciswin.cis_OK_b invoke]
    bind $cisw <Alt-k> [list $ciswin.cis_OK_b invoke]

    bind $ciswin.cis_devname_e <<ApplyEntry>> cis_devinit

    grid remove $ciswin.cis_rs232_f
    grid remove $ciswin.cis_gpib_f
    grid remove $ciswin.cis_camac_f
    grid remove $ciswin.cis_vme_f
    grid remove $ciswin.cis_tcpip_f

    radiomenu $ciswin.cis_interface_b -textvariable cinter(interface) -variable cinter(interface) \
            -values [list rs232 gpib camac vme tcpip none]  -indicatoron 0 -command cis_inter_type_set

    set ports [list]
    catch { set ports [mui_camp_cmd {sysGetIfConf rs232}] }
    radiomenu $ciswin.cis_port_b -textvariable cinter(port) -variable cinter(port) \
            -values $ports -indicatoron 0
    radiomenu $ciswin.cis_baud_b -textvariable cinter(baud) -variable cinter(baud) \
            -values [list 110 300 600 1200 2400 4800 9600 19200] -indicatoron 0
    radiomenu $ciswin.cis_parity_b -textvariable cinter(parity) -variable cinter(parity) \
            -values [list none odd even] -indicatoron 0
    radiomenu $ciswin.cis_databits_b -textvariable cinter(databits) -variable cinter(databits) \
            -values [list 7 8] -indicatoron 0
    radiomenu $ciswin.cis_stopbits_b -textvariable cinter(stopbits) -variable cinter(stopbits) \
            -values [list 1 2] -indicatoron 0
    radiomenu $ciswin.cis_rs_readterm_b -textvariable cinter(readterm) -variable cinter(readterm) \
            -values [list none CR LF CRLF] -indicatoron 0
    radiomenu $ciswin.cis_rs_writeterm_b -textvariable cinter(writeterm) -variable cinter(writeterm) \
            -values [list none CR LF CRLF] -indicatoron 0

    radiomenu $ciswin.cis_ieee_readterm_b -textvariable cinter(readterm) -variable cinter(readterm) \
            -values [list none CR LF CRLF] -indicatoron 0
    radiomenu $ciswin.cis_ieee_writeterm_b -textvariable cinter(writeterm) -variable cinter(writeterm) \
            -values [list none CR LF CRLF] -indicatoron 0

    radiomenu $ciswin.cis_ip_readterm_b -textvariable cinter(readterm) -variable cinter(readterm) \
            -values [list none CR LF CRLF] -indicatoron 0
    radiomenu $ciswin.cis_ip_writeterm_b -textvariable cinter(writeterm) -variable cinter(writeterm) \
            -values [list none CR LF CRLF] -indicatoron 0

#    after 0 after idle cis_devinit
    cis_devinit
}


#   Initialization settings to be made once valid device is known.

proc cis_devinit { } {
    global ciswin cinter
    
    set ins $cinter(name)
    if { [ string length $ins] } {
        if { [catch {retried_camp_cmd sysGetInsNames} insnames] } { cis_close }
        if { [lsearch -exact $insnames $ins] < 0 } {
            # An unknown instrument name has been specified.  If there is an instrument type
            # selected, then create new instrument.
            if { [catch {
                retried_camp_cmd "sysGetInsTypes" "get list of Camp instrument types"
            } instypes] } { return }
            if { [lsearch -exact $instypes $cinter(type)] < 0 } { return }
            if { [catch {
                retried_camp_cmd "insAdd $cinter(type) $ins" "add instrument $ins"
            } ] } { return }
        } else {
            # A known instrument is specified; load its settings, and disable
            if { [catch {retried_camp_cmd "insGetTypeIdent /$ins"} instype] } { return }
            set cinter(type) $instype
        }
        deactivateEntry $ciswin.cis_devname_e
        $ciswin.cis_devtype_b configure -state disabled -relief flat -takefocus 0 \
                -highlightcolor $::PREF::bgcol -bg $::PREF::bgcol -disabledforeground $::PREF::fgcol
        set cinter(devinit) 1
        cis_inter_get $ins
    }
}

#   Chooser for the Camp instrument type (for new instruments)

proc cis_choose_type { } {
    global ciswin cinter
    set ctypes {}
    if { [catch { 
        retried_camp_cmd sysGetInsTypes "read list of Camp instrument types" 
    } ctypes] } { return }
    set type [popup_selector "Choose type" "Choose Camp instrument type:" \
            $ctypes {} single false 90]
    if { [string length $type] == 0 } { return }
    if { [lsearch -exact $ctypes $type] < 0 } {
        beep
        timed_messageBox -timeout 30000 -type ok -parent [p_win_name $ciswin] \
                -message "Invalid instrument type!\nYou must select one of the displayed types." \
                -icon error -title "Failed Camp Interface"
        return
    }
    set cinter(type) $type
    cis_devinit
}

# Get the current communication parameters for instrument $ins, by
# requesting values from the Camp server.  Save values in the variables
# used by the Gui widgets.

proc cis_inter_get { ins } {
    global ciswin cinter

    # puts "Get interface parameters (cis_inter_get)"
    grid remove $ciswin.cis_rs232_f
    grid remove $ciswin.cis_gpib_f
    grid remove $ciswin.cis_camac_f
    grid remove $ciswin.cis_vme_f
    grid remove $ciswin.cis_tcpip_f

    # Request all interface parameters in one camp request:
    set ifpars [join [list \
            \[insGetIfTypeIdent /$ins\] \
            \[insGetIfDelay /$ins\] \
            \[insGetIfTimeout /$ins\] \
            \[insGetIfStatus /$ins\] \
            \[insGetIf /$ins\] \
            ] " " ]
    if { [catch {retried_camp_cmd "return \"$ifpars\""} ifpars] } { return }
    lassign $ifpars cinter(interface) cinter(delay) cinter(timeout) cinter(online)
    catch { set cinter(delay) [expr { $cinter(delay) }] }
    catch { set cinter(timeout) [expr { $cinter(timeout) }] }
    set ifpars [lrange $ifpars 4 end]

    switch -glob -- $cinter(interface) {
        rs232* {
            set cinter(flush) [expr {$cinter(timeout)>0.0}]
            set cinter(timeout) [expr { abs($cinter(timeout)) }]
            grid $ciswin.cis_rs232_f
            lassign $ifpars \
                cinter(port) cinter(baud) cinter(databits) cinter(parity) cinter(stopbits) \
                cinter(readterm) cinter(writeterm)
        }
        gpib* {
            grid $ciswin.cis_gpib_f
            lassign $ifpars \
                cinter(address) cinter(readterm) cinter(writeterm)
        }
        camac {
            grid $ciswin.cis_camac_f
            lassign $ifpars \
                cinter(branch) cinter(crate) cinter(slot)
        }
        vme {
            grid $ciswin.cis_vme_f
            lassign $ifpars cinter(base_add)
        }
	tcpip {
            set cinter(persistent) [expr {$cinter(timeout)<0.0}]
            set cinter(timeout) [expr { abs($cinter(timeout)) }]
            grid $ciswin.cis_tcpip_f
            lassign $ifpars cinter(IPaddr) cinter(IPport) \
		cinter(readterm) cinter(writeterm)
	}
        default {
        }
    }
}

# Display the panel appropriate to the current interface type.  Hide
# other panels.

proc cis_inter_type_set { } {
    global ciswin cinter

    set ins $cinter(name)
    if { $cinter(devinit) } {
        if { [catch {retried_camp_cmd "insIfOff /$ins"} ] } { return }
    }        
    grid remove $ciswin.cis_rs232_f
    grid remove $ciswin.cis_gpib_f
    grid remove $ciswin.cis_camac_f
    grid remove $ciswin.cis_vme_f
    grid remove $ciswin.cis_tcpip_f

    catch { grid $ciswin.cis_$cinter(interface)_f }
}


#   Apply the interface settings.  Returns 1 for success, 0 for failure.
proc cis_apply { } {
    global ciswin cinter

    set ins $cinter(name)

    if { !$cinter(devinit) } {
        beep
        timed_messageBox -timeout 30000 -type ok -parent [p_win_name $ciswin] \
                -message "Please select a valid device name and type first!" \
                -icon error -title "Failed Camp Interface"
        return 0
    }

    set timeout $cinter(timeout)
    switch -- $cinter(interface) {
        rs232 {
            set timeout [expr {$cinter(timeout) * ($cinter(flush) ? 1.0 : -1.0)}]
            set ifpars [join [list $cinter(port) $cinter(baud) $cinter(databits) $cinter(parity) \
                    $cinter(stopbits) $cinter(readterm) $cinter(writeterm)] " "]
        }
        gpib {
            set ifpars [join [list $cinter(address) $cinter(readterm) $cinter(writeterm)] " "]
        }
        camac {
            set ifpars [join [list $cinter(branch) $cinter(crate) $cinter(slot)] " "]
        }
        vme {
            set ifpars [string trim $cinter(base_add)]
        }
        tcpip {
	    # First, check the IP address.  We don't force the raw format for the user's entry
	    # because we here try to do nameserver lookup and conversion.
	    if { [scan $cinter(IPaddr) {%u.%u.%u.%u %c} ia1 ia2 ia3 ia4 extra] == 4 } {
		if { $ia1 > 255 || $ia2 > 255 || $ia3 > 255 || $ia4 > 255 } {
		    mui_alert $ciswin.cis_ip_addr_e
		    return 0
		}
	    } else {
		if { [catch { exec host $cinter(IPaddr) } result] || \
		     [scan $result {%*s has address %s %c} numeric extra] != 1 } {
		    mui_alert $ciswin.cis_ip_addr_e
		    return 0
		}
		set cinter(IPaddr) $numeric
	    }
	    # then apply
            set timeout [expr {$cinter(timeout) * ($cinter(persistent) ? -1.0 : 1.0)}]
            set ifpars [join [list $cinter(IPaddr) $cinter(IPport) \
		    $cinter(readterm) $cinter(writeterm)] " "]
        }
        default {
            set ifpars {}
            set cinter(interface) none
        }
    }

    # Apply the selected interface settings
    puts "Set interface: insSet /$ins -if $cinter(interface) $cinter(delay) $timeout $ifpars"
    return 1
    if { [catch {
        mui_camp_cmd "insIfOff /$ins ; insSet /$ins -if $cinter(interface) $cinter(delay) $timeout $ifpars"
    } ans ] } {
        beep
        timed_messageBox -timeout 30000 -type ok -parent [p_win_name $ciswin] \
                -message "Interface settings were rejected!\nResponse was:\n$ans" \
                -icon error -title "Failed Camp Interface"
        return 0
    }

    # Attempt to switch on-line if that is selected
    if { $cinter(online) } {
        if { [catch { mui_camp_cmd "insSet /$ins -line on" } ans ] } {
            beep
            timed_messageBox -timeout 30000 -type ok -parent [p_win_name $ciswin] \
                    -message "Instrument failed to go on-line!\nResponse was:\n$ans" \
                    -icon error -title "Failed Camp Interface"
            return 0
        }
    }
    return 1
}

#  Procedure executed when "Online" checkbutton is changed (i.e., on or off)
proc cis_online { args } {
    global ciswin cinter
    if { $cinter(devinit) } {
        if { $cinter(online) } {
            if { ! [cis_apply] } {
                set cinter(online) 0
            }
        } else {
            catch { mui_camp_cmd "insIfOff /$cinter(name)" }
        }
    }
}

#  Close window, and destroy array so previous values won't get into next use.
#  Ensure the array is unset after all the widgets that refer to it have been
#  destroyed.

proc cis_close { } {
    global ciswin cinter
    destroy [td_win_name $ciswin]
}


#  THE HELP LINE:

proc cis_bind_help { w h } {
    global ciswin
    bind $ciswin.$w <Enter>    [list set cis_help_text "$h"]
    bind $ciswin.$w <FocusIn>  [list set cis_help_text "$h"]
    bind $ciswin.$w <Leave>    [list set cis_help_text {}]
    bind $ciswin.$w <FocusOut> [list set cis_help_text {}]
}

global cinter
set cinter(devinit) 0

if { $ciswin == "" } {
    # stand-alone testing, so start immediately!

    cis_initialize
    
}

