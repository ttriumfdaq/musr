#!/usr/bin/tclsh
#! really needs tclsh_camp! 
# This is an intermediary between the mui (or campvarlog) client and the
# camp server; a "subserver".  It reads camp commands from stdin, executes
# them using the built in camp_cmd, and writes the results to stdout.
# It normally runs in a pipeline rather than interactively.
#
# This may seem rather pointless, but the camp server has been crashing
# in a strange way that locks up clients (the pending rpcs never time out!)
# and I have not been able to find the problem.  This subserver will bear 
# the brunt of such lock-ups, leaving the mui in a functional state able to
# restart both the main camp server and this subserver.
#
# If and when there are no camp server hang-ups, this subserver can be
# dropped.
#
# Each request comes with a numeric identity code, so the result can remain
# associated with the request. 
#
# Command line options:
#    -d   or  -d filename   for debugging/logging output.
#    -v                     for verbose

set debf ""
set verb 1
set i [lsearch -exact $argv "-v"]
if { $i >= 0 } {
    set verb 10
    set argv [lreplace $argv $i $i]
}
set i [lsearch -exact $argv "-d"]
if { $i < 0 } {
    proc plog { args } { }
} else {
    proc plog { v msg } {
        global debf verb
        if { $debf ne "" && $v >= $verb } {
            catch { puts $debf "[clock format [clock seconds] -format {%Y/%m/%d %H:%M:%S}]: $msg" }
        }
    }
    if { $i+1 == [llength $argv] } {
        set debf [open camp_subserver.log a]
    } else {
        set debf [open [lindex $argv [incr i]] a]
    }
    fconfigure $debf -buffering line
}

plog 0 "Now in tclsh_camp for server"
plog 0 "Start"
plog 0 "Camp host: $env(CAMP_HOST)"
catch {camp_cmd sysGetInsNames} inst
plog 0 "Instruments connected are: $inst"

proc main { } {
    global debf verb
    while 1 {
        plog 2 "Wait for command."
        set n [gets stdin buf]
        plog 2 "read $n char: |$buf|"
        if { $n < 0 } {
            plog 0 "End of input. EXIT NOW."
            exit
        }
        set ll 0
        catch { set ll [llength $buf] }
        if { $ll != 2 } { 
            plog 0 "Ignore improper request |$buf|"
            continue
        }
        foreach { id cmd } $buf {}
        plog 1 "$id $cmd ..."
        set code [catch {camp_cmd $cmd} resp]
        # Because we read line-by-line, concatenate multiline camp responses.
        # (Currently, they are left that way, but we could choose a more distinctive
        # separator string than "|" and have the client map back to newlines.)
        set resp [string map {"\n" "|"} $resp]
        if { $code == 0 } {
            plog 1 "OK: $id $code $resp"
        } else {
            plog 0 "ERROR for $id $cmd: $code $resp"
        }
        puts [list $id $code $resp]
    }
}

main

