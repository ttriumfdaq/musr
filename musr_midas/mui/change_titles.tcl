#! /usr/bin/wish
#
#  change_titles.tcl
#
#  GUI for changing the headers in an existing mud (musr data) file.  It is a
#  wrapper for the change_titles program, and can be run stand-alone or from
#  within the musr daq "mui".
#
#   $Log: change_titles.tcl,v $
#   Revision 1.9  2015/05/18 19:14:32  asnd
#   Fix blocking read / hang-up
#
#   Revision 1.8  2015/04/18 00:06:03  asnd
#   Watch for eof on exec pipe
#
#   Revision 1.7  2015/03/20 00:51:18  suz
#   changes by Donald. This version part of package for new VMIC frontends
#
#   Revision 1.6  2008/04/30 01:56:24  asnd
#   Convert to bnmr-style /autorun parameters in odb
#
#   Revision 1.5  2006/09/22 03:06:47  asnd
#   Time displays fixed.
#
#   Revision 1.4  2006/05/26 16:59:52  asnd
#   Accumulated changes over winter shutdown.
#
#   Revision 1.3  2005/07/21 07:17:46  asnd
#   Early version; initial CVS log
#
#
#console show 

namespace eval CT {

variable program $::env(MUSR_DIR)/mud/bin/change_titles

# Validation for an integer
# Use -validate key -vcmd {valInt %W %P %v}
proc valInt {win val type} {
    if { [string is integer $val] } { return 1 }
    beep
    after idle [list $win config -validate $type]
    invalid_entry_alert $win
    return 0
}

# Validation for a list of integers
# Use -validate key -vcmd {valLint %W %P %v}
proc valLint {win val type} {
    foreach i [split $val ","] {
	if { ! [string is integer $i] } {
	    beep
	    after idle [list $win config -validate $type]
	    invalid_entry_alert $win
	    return 0
	}
    }
    return 1
}

# Try to apply changes. Return boolean for success
proc Apply { } {
    variable filename
    variable program
    variable listFieldNames
    variable entryValues
    variable fileValues
    variable archive
    variable top
    global env

    set errors {Time error:}
    set nerr 0
    foreach t {Start End} {
	if { [catch {set entryValues(${t}Sec) [clock scan $entryValues(${t}Time)]} mes] } {
	    append errors "\n$mes"
	    incr nerr
	}
    }
    if { $nerr } {
        puts "Time error $errors"
	beep
	switch [timed_messageBox -icon error -type okcancel -default cancel -timeout 30000 \
		    -parent $top -title "Time error" -message $errors] {
	    cancel { return 0 }
	}
    }

    set begin 1
    foreach {tag text} $listFieldNames {
	if { [info exists entryValues($tag)] && [info exists fileValues($tag)] } {
	    if { [string compare $entryValues($tag) $fileValues($tag)] } {

                if { $begin } {
                    #puts "Found first change"
                    set begin 0
                    # Detected first changed entry, so start up the changing
                    if { [catch { open "|$program $filename" r+ } pipe] } {
                        beep
                        timed_messageBox -icon error -timeout 30000 -type ok -parent $top \
                             -title "Failed apply" -message "Failed to apply changes:\n$pipe"
                        return 0
                    }
                    fconfigure $pipe -buffering none
                    #puts "Pipe is opened as $pipe"
                    # scan up to point for entering changes:
                    while { ![eof $pipe] && ![string match "Now enter replacement lines*" [gets $pipe]] } { }
                }
                # Apply this changed entry
		puts $pipe "$tag $entryValues($tag)"
		#puts "change $tag $entryValues($tag)"
		set fileValues($tag) $entryValues($tag)
	    }
	}
    }

    # Did all tags.  If no changes were made, then we're done
    if { $begin } {
        return 1
    }
    # We did make some changes. Check for error messages.
    puts $pipe "show"
    # Blank command gives a recognizable marker at end
    puts $pipe ""
    set errors {}
    set nerr 0
    while { ![eof $pipe] } {
        set line [gets $pipe]
        if { [string match "*Now enter replacement lines*" $line] || \
             [string match "*Type exit to apply*" $line] } {
            break
        }
	if { [string match "Error: *" $line] } {
            puts "Error: \n$line"
	    append errors "\n$line"
	    incr nerr
        }
    }
    if { $nerr } {
	if { $nerr > 1 } {
	    set mes "There were errors applying values:$errors"
	} else {
	    set mes "There was an error applying values:$errors"
	}
	beep
	switch [timed_messageBox -icon error -type okcancel -default cancel -timeout 30000 \
		    -parent $top -title "Titles errors" -message $mes] {
	    ok {
		puts $pipe "exit"
	    }
	    cancel {
		puts $pipe "quit"
		catch { close $pipe }
		return 0
	    }
	}
    } else { # no errors
        puts $pipe "exit"
    }
    # Done.
    if { [catch { close $pipe } mes] } {
	timed_messageBox -icon error -type ok -timeout 30000 \
		    -parent $top -title "Final error" -message "Final error:\n$mes"
	return 0
    }

    # Optionally archive changed data file
    set archiver "$env(MUSR_DIR)/mdarc/bin/cpbnmr"
    catch { set archiver [exec odbedit -c "ls -v '/Equipment/MUSR_TD_acq/mdarc/archiver task'"] }
    set archdir "/musr/dlog/$env(USER)"
    catch { set archdir [exec odbedit -c "ls -v '/Equipment/MUSR_TD_acq/mdarc/archived_data_directory'"] }
    #puts "archiver $archiver,   archdir $archdir"
    if { $archive } { # && [file executable $archiver] && [file isdirectory $archdir] 
        # only archive files with good file names 
        if { [scan [file tail $filename] {%d.%s} n e] == 2 && [string equal $e "msr"] \
                && ($n < 30000 || $n >= 35000) } {
            switch [timed_messageBox -timeout 30000 -icon question -type yesno -default no \
                        -parent $top -title "Archive it?" -message "Archive the revised run?"] {
                yes {
		    #puts [concat exec $archiver [list $filename $archdir]]
                    if { [catch [concat exec $archiver [list $filename $archdir]] mes] } {
                        beep
                        timed_messageBox -timeout 30000 -icon error -type ok -parent $top \
                            -title "Archive error" -message "Archive returned error:\n$mes"
                        return 0
                    }
                }
	    }
	}
    }
    return 1
}

proc Revert { } {
    variable listFieldNames
    variable fieldNames
    variable program
    variable textTitles
    variable fileValues
    variable entryValues
    variable filename 

    set listFieldNames {
	RunNumber {Run number}
	Operator {Operator}    Experiment {Experiment num}
	Startsec  {Start time}    Endsec    {End time}
	StartTime {Start time}    EndTime   {End time}
	Rig Rig    Mode Mode
	Title Title 
	Subtitle Subtitle 
	Sample Sample   Orient Orientation
	Field Field    Temperature Temperature
	Beamline Beamline
	Cmt1 Comment1
	Cmt2 Comment2
	Cmt3 Comment3
	Elapsedsec {Elapsed seconds}
	HTitles {Histogram titles}
	t0Bins  {Time zero bin}
	t0Ps    {Time zero picosec}
	t1Bins  {First good bin}
	t2Bins  {Last good bin}
	Bg1Bins {First background bin}
	Bg2Bins {Last background bin}
    }
    array set fieldNames $listFieldNames

    if 1 {
        #puts [list open "|$program $filename" r+]
	set pipe [open "|$program $filename" r+]
	fconfigure $pipe -buffering none
	puts $pipe quit
	set textTitles [split [read $pipe] "\n"]
	catch { close $pipe }
    } else { # For testing
        set textTitles [split {
RunNumber    5678
Experiment   777 
Operator     rfk,kim,rm
Title        V3Si TF=0.856 kG\\100 T=3.76K
Sample       V3Si
Orient       001
Temperature  2.8K
Field        856 G
Beamline     M20
Rig          Helios
Mode         heltf.781
Startsec     960736741
Endsec       960744674
Elapsedsec   7933
HTitles      UpR,UpL,DnR,DnL
t0Bins       246,246,248,246
t0Ps         191797,191797,191790,191797
t1Bins       300,310,300,315
t2Bins       8959,8959,8959,8959
Bg1Bins      25,26,25,25
Bg2Bins      200,200,200,200

Now enter replacement lines or one of: quit, exit, show, help

ct>
        } \n ]
    }

    if { [info exists fileValues] } { unset fileValues ; set fileValues(.) {}}
    if { [info exists entryValues] } { unset entryValues ; set entryValues(.) {}}

    foreach tline $textTitles {
        set val {}
	if { [scan $tline "%s %\[^\n\]" tag val] >= 1 } {
	    if { [info exists fieldNames($tag)] } {
		set fileValues($tag) $val
		set entryValues($tag) $val
	    }
	}
    }
    set entryValues(StartTime) [clock format $fileValues(Startsec) -format "%H:%M:%S %b %d, %Y"]
    set entryValues(EndTime) [clock format $fileValues(Endsec) -format "%H:%M:%S %b %d, %Y"]
}


proc change_titles { file } {
    variable top
    variable base
    variable listFieldNames
    variable fieldNames
    variable program 
    variable textTitles
    variable fileValues
    variable entryValues
    variable filename $file

    wm title $top "Change run titles"
    bind $top <Alt-r> [list $base.bRevert invoke]
    bind $top <Alt-a> [list $base.bApply invoke]
    bind $top <Alt-n> [list $base.bOther invoke]
    bind $top <Alt-q> [list $base.bQuit invoke]
    bind $top <Alt-o> [list $base.bExit invoke]
    bind $top <Alt-k> [list $base.bExit invoke]

    grid columnconfigure $top 0 -weight 1

    frame $base.titl -relief flat -borderwidth 4
    grid columnconfigure $base.titl 1 -weight 1
    grid propagate $base.titl

    frame $base.acct -relief flat -borderwidth 4
    grid columnconfigure $base.acct 1 -weight 1
    grid columnconfigure $base.acct 4 -weight 1
    grid columnconfigure $base.acct 2 -minsize 25
    grid propagate $base.acct

    frame $base.head -relief flat -borderwidth 4
    grid columnconfigure $base.head 1 -weight 1
    grid columnconfigure $base.head 4 -weight 1
    grid columnconfigure $base.head 2 -minsize 25
    grid propagate $base.head

    frame $base.hist -relief flat -borderwidth 4
    grid columnconfigure $base.hist 1 -weight 1
    grid propagate $base.hist

    frame $base.butt -relief flat -borderwidth 4
    grid columnconfigure $base.butt 0 -weight 1
    grid columnconfigure $base.butt 1 -weight 1
    grid columnconfigure $base.butt 2 -weight 1
    grid columnconfigure $base.butt 3 -weight 1
    grid columnconfigure $base.butt 4 -weight 1
    grid propagate $base.butt

    Revert

    label $base.title -text "Change Titles" -font muTitleFont -relief ridge -borderwidth 3 -pady 6

    grid $base.title - -in $base.titl -sticky ew -pady 6

    label $base.lfile -text "File: "
    label $base.filename -textvar CT::filename -justify left -anchor w
    grid $base.lfile $base.filename - - - -in $base.acct -sticky w -pady 3

    foreach {tag text} $listFieldNames {
	if { [info exists entryValues($tag)] } {
	    label $base.l$tag -text "$text:  " -justify left -anchor w -borderwidth 3
	    entry $base.e$tag -textvar CT::entryValues($tag) -width 0
	}
    }

    $base.eRig configure -width 22

    label $base.rnmes -text "Changing run number does not change the file name" \
	-justify left -anchor w -borderwidth 3

    $base.eRunNumber configure -validate key -vcmd {CT::valInt %W %P %v}
    grid $base.lRunNumber $base.eRunNumber  $base.rnmes - - -in $base.acct -sticky ew -pady 3
    $base.eExperiment configure -validate key -vcmd {CT::valInt %W %P %v}
    grid $base.lOperator $base.eOperator x $base.lExperiment $base.eExperiment -in $base.acct -sticky ew -pady 3
    grid $base.lStartTime $base.eStartTime x $base.lEndTime $base.eEndTime -in $base.acct -sticky ew -pady 3
    grid $base.lRig $base.eRig x $base.lMode $base.eMode -in $base.acct -sticky ew -pady 3

    grid $base.lTitle $base.eTitle - - - -in $base.head -sticky ew -pady 3
    if { [info exists fileValues(Subtitle)] } {
	grid $base.lSubtitle $base.eSubtitle - - - -in $base.head -sticky ew -pady 3
    }
    grid $base.lSample $base.eSample x $base.lOrient $base.eOrient -in $base.head -sticky ew -pady 3
    if { [info exists fileValues(Temperature)] } {
	grid $base.lField $base.eField x $base.lTemperature $base.eTemperature \
	    -in $base.head -sticky ew -pady 3
    }
    if { [info exists fileValues(Cmt1)] } {
	grid $base.lCmt1 $base.eCmt1 - - - -in $base.head -sticky ew -pady 3
	grid $base.lCmt2 $base.eCmt2 - - - -in $base.head -sticky ew -pady 3
	grid $base.lCmt3 $base.eCmt3 - - - -in $base.head -sticky ew -pady 3
    }

    grid $base.lHTitles $base.eHTitles -in $base.hist -sticky ew -pady 3
    if { [info exists fileValues(t0Bins)] } {
	foreach w {t0Bins t0Ps t1Bins t2Bins Bg1Bins Bg2Bins} {
	    $base.e$w configure -validate key -vcmd {CT::valLint %W %P %v}
	    grid $base.l$w $base.e$w -in $base.hist -sticky ew -pady 3
	}
    }

    button $base.bRevert -width 10 -text Revert -underline 0 \
	-command { CT::Revert }
    button  $base.bApply -width 10 -text Apply -underline 0 \
	-command { CT::Apply }
    button  $base.bOther -width 10 -text New -underline 0 \
	-command { CT::change_old_titles }
    button $base.bQuit -width 10 -text Quit -underline 0 \
	-command { destroy $CT::top }
    button $base.bExit -width 10 -text OK -underline 0 \
	-command { if { [CT::Apply] } { destroy $CT::top } }

    grid $base.bRevert $base.bApply $base.bOther $base.bQuit $base.bExit \
	-in $base.butt -sticky ew -padx 6 -pady 6

    grid $base.titl -sticky ew -padx 10 -pady 8
    grid $base.acct -sticky ew -padx 10 -pady 0
    grid $base.head -sticky ew -padx 10 -pady 8
    grid $base.hist -sticky ew -padx 10 -pady 0
    grid $base.butt -sticky ew -padx 10 -pady 8

    wm deiconify $top
}

proc change_old_titles { } {
    global run
    variable top
    variable base

    if { [winfo exists $top] } {
        eval destroy [winfo children $top]
    } else {
        toplevel $top
        wm title $top "Change old titles"
    }

    label $base.message -text "Enter Mud file name or run number:"
    entry $base.fname -textvar CT::filename
    bind $base.fname <Key-Return> [list $base.ok invoke]
    button $base.browse -text Browse -width 10 -default normal -command CT::get_fname
    button $base.cancel -text Cancel -width 10 -default normal -command CT::cancel
    button $base.ok -text OK -width 10 -default active -command CT::start_change
    grid $base.message - - -sticky w
    grid $base.fname - - -sticky ew
    grid $base.browse $base.cancel $base.ok -sticky ew
    focus $base.fname
}

proc cancel { } {
    destroy $CT::top
}

proc get_fname { } {
    global run midas
    variable top
    variable filename

    set ddir [defaultDir]

    # Use any directory specified in the entry field as the default directory
    if { [string length $filename] } {
        if { [file isdirectory $filename] } {
            set ddir $filename
        } elseif { [llength [file split $filename]] > 1 && \
                       [file isdirectory [file dirname $filename]] } {
            set ddir [file dirname $filename]
        }
    }

    set fname [tk_getOpenFile -defaultextension ".msr"  -filetypes {{{Mud files} {.msr}}} \
		   -initialdir $ddir -title "Select Mud file" -parent $top ]

    if { [string length $fname] } {
	set CT::filename $fname
        CT::start_change
    }
    return
}

proc defaultDir { } {
    global midas env

    if { [info exists midas(data_dir)] } {
	set ddir $midas(data_dir)
    } elseif { [info exists env(HOSTNAME)] && [info exists env(USER)] && \
                   [string match "mid$env(USER)*" $env(HOSTNAME)] } {
	set ddir "/data/$env(USER)/[clock format [clock seconds] -format %Y]"
        if { ! [file isdirectory $ddir] } {
            set ddir [pwd]
        }
    } else {
	set ddir [pwd]
    }
    return $ddir
}

proc queryLocalCopy { title message } {
    variable filename
    variable top
    global env

    beep
    switch [timed_messageBox -icon warning -type yesnocancel -default cancel -timeout 30000 \
                -message "$message\nShall we change a local copy (in $env(HOME))?" \
                -parent $top -title $title ] {
        yes {
            exec cp $filename $env(HOME)
            set filename [file join $env(HOME) [file tail $filename]]
        }
        no {
            return -code return
        }
        cancel {
            cancel
            return -code return
        }
    }
}

#  start_change:  look for a file by interpreting $filename; first of:
#  if integer (run number):
#     00filename.msr
#     data_dir/00filename.msr
#  fi
#  if no .msr:
#     filename.msr
#  fi
#  if no directory, and we have a data_dir:
#     data_dir/filename
proc start_change { } {
    variable top
    variable filename
    variable archive 0
    global run midas env

    if { [string length $filename] == 0 } {
	beep
	switch [timed_messageBox -icon warning -type okcancel -default cancel -timeout 30000 \
		    -parent $top -title "No Mud file" \
                    -message "Please select a Mud file, or cancel"] {
            cancel { cancel ; return }
	    ok { return }
	}
    }

    set ddir [defaultDir]

    if { [scan $filename {%d %c} rnum c] == 1} { # name is a run number
	set filename [format "%06d.msr" $rnum]
    } else {
        set rnum -999
    }

    set fname $filename

    if { ![file exists $fname] } {
	if { [file exists "$fname.msr"] } {
	    append fname ".msr"
	} elseif { [file exists [file join $ddir $fname]] } {
            set fname [file join $ddir $fname]
        } elseif { [file exists [file join $ddir "$fname.msr"]] } {
            set fname [file join $ddir "$fname.msr"]
	}
    }
    if { ![file exists $fname] } {
	beep
	switch [timed_messageBox -icon error -type okcancel -default cancel -timeout 30000 \
		    -parent $top -title "No Mud file" -message "Run/File not found: $filename"] {
                        cancel { cancel ; return }
	    ok { return }
	}
    }

    # Got a valid file name.  Check ... 

    set filename $fname

    # ... if it is writable
    if { ![file writable $filename] } {
        queryLocalCopy "No Access" "You do not have write access to $filename"
    }

    # ... if it is the run in progress

    if { $rnum == -999 && [regexp {\m\d+\.msr} $filename m]} {
	scan $m {%d.msr} rnum
    }

    if { [info exists run(in_progress)] && [info exists run(number)] && \
	     $run(in_progress) && ($run(number) == $rnum) } {
	beep
	switch [timed_messageBox -icon error -type okcancel -default cancel -timeout 30000 \
		    -message "That run is in progress!
Change the titles on the main page before ending it, or select an old run."\
		    -parent $top -title "Run active" ] {
            cancel { cancel ; return }
	    ok { return }
	}
    }

    # ... if it is in the archive and old
    switch [file pathtype $filename] {
	absolute {
	    set dir [file dirname $filename]
	}
	relative {
	    set dir [file join [pwd] [file dirname $filename]]
	}
    }
    set archive 0
    set now [clock seconds]
    if { [string match /data/* $dir] } {
	set archive 1	
	if { $now - [file mtime $filename] > 3*24*3600 } {
	    set archive 0
            queryLocalCopy "Old file" \
                "File $filename is in the main data log, but is old.\n(Check with facility support for updating old run files.)"
	}
    }

    # OK, so change the file.  Metamorphose gui
    eval destroy [winfo children $top]

    change_titles $filename
}

    
namespace export change_old_titles
}
namespace import CT::change_old_titles

if { [info exists mui_utils_loaded] } {
    #puts "Running as a component"
    set CT::top [td_win_name $::chtitwin]
    set CT::base $::chtitwin
} else {
    #puts "Stand-alone"
    set CT::top "."
    set CT::base ""
    set run(in_progress) 0
    set run(number) 0
    set clogv(camp_only) 1 ;# This tells mui_utils to not set up midas things
    set iam [info script]
    while { [file type $iam]=="link" ] } {
        set iam [file readlink $iam]
    }
    source [file join [file dirname $iam] mui_utils.tcl]
    if { $argc > 0 } {
        set CT::filename [lindex $argv 0]
        change_old_titles
        CT::start_change
    } else {
        change_old_titles
    }
}

