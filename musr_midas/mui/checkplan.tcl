#! /bin/sh
# Shell lib setting: \
#export LD_LIBRARY_PATH=~musrdaq/lib
# the next line restarts using mtcl but is ignored by mtcl\
DISPLAY="" exec $MUSR_DIR/musr_midas/midas_tcl/mtcl "$0" "$@"


#################################################################################
#
#     Check the run-plan for syntax errors; stand-alone and without midas.
#
#################################################################################

set usage "Usage: $argv0 <filename> \[<initial_run_number>\] \[wait\]"

set initial -99
set wait 0

if { $argc < 1 } {
   puts stderr "Error: no run-plan file name\n$usage"
   exit
} else {
   foreach a [lrange $argv 1 3] {
      if { [string is integer -strict $a] } {
	  set initial $a
      } elseif { $a == "wait" } {
          set wait = 1
      } else {
	  puts stderr "Error: invalid arguments\n$usage"
          exit
      }
   }
}

set filename [lindex $argv 0]
if { ![file exists $filename] } {
    puts stderr "Error: no such file: $filename ([file nativename $filename])"
    exit
}

# No Midas required (no Camp required either, actually)
set clogv(camp_only) 1
source [file join [file dirname [info script]] readplan.tcl]
source [file join [file dirname [info script]] mui_utils.tcl]

catch { set midas(camp_host) $env(CAMP_HOST) }

proc midas { args } {
    return 1
}

#proc mui_camp_cmd { args } {
#    return 2
#}

# Continue parsing after errors:
set ::plan::parse_error_action continue
set ::plan::parse_error_count -1

set e [catch {parse_plan [read_plan $filename] $initial } msg]

if { $::plan::parse_error_count == -1} {
    puts "Read or parse failed: $msg"
} elseif { $::plan::parse_stale_runs } {
    puts "Run plan succeeds with warning: $::plan::parse_error_messages"
} elseif { $::plan::parse_error_count == 1} {
    puts "$::plan::parse_error_messages\nRun plan failed."
} elseif { $::plan::parse_error_count } {
    puts "$::plan::parse_error_messages\nRun plan failed with $::plan::parse_error_count error[plural $::plan::parse_error_count]."
} else {
    puts "Plan succeeds"
}

if { [lindex $argv 1] == "wait" } {
    puts -nonewline "Ready? "
    flush stdout
    gets stdin zz
}

exit


