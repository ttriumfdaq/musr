###################################################################
#  
#  DRAG AND DROP    da_dragndrop.tcl
#
#  Various drag-and-drop procedures: Some procedures for dragging 
#  information among widgets in a group of related widgets, and
#  others for more generic movement.
#
#  For each widget in such a group, you should declare 
#
#    group_drag_bind group_name widget variable_name(s) ?display_proc?
#
#  where the variable_names(s) is a single variable name or a list
#  of variable names associated with that widget.  They should be
#  global variables or fully qualified namespace variables.  The
#  display_proc, if given, provides text to display during the drag,
#  as explained below.
#
#  The default action on a drag-and-drop is to copy the values from
#  the source variable list into the destination variable list.
#  To perform a different action, one may optionally declare 
#
#    group_drag_bind_proc group_name procedure
#
#  to associate a new procedure with a group.  The procedure is a tcl 
#  proc taking two parameters, the first being the variable list from
#  the source (grab) widget and the second from the destination (drop).
#  (Thus, the lists of variable names must have the same length within
#  a group.)  If this declaration is omitted, the default procedure for
#  the group is drag_copy_group_var (see below).
#
#  Remove the bindings and procedures created by either of the previous
#  procedures using:
# 
#  group_drag_unbind group_name widget_name 
#
#  which also deletes the internal variables...  For each group, there 
#  is a namespaced array named "dadnd::<group_name>_drbind", with entries
#  named w_<widget_name>, for each widget in the group, and corresponding
#  values being the (list of) associated variable(s)
#
#  The procedures described above are implemented using some more generic
#  dragging procedures, which you can use directly by means of:
#
#  declare_drag widget callback ?disp_proc?
#
#  "widget" is the Tk window name, 
#  "callback" is a proc, or a proc with some leading arguments, invoked for
#  the "drop" operation.  Specifically, it will be evaluated in the global
#  scope with four (more) arguments appended: start_x start_y end_x end_y
#  giving the screen locations for the start and end of the drag operation.
#  "disp_proc" is optional, and it is used to show text during the drag.
#  It is evaluated similarly to the callback function, with the added pars
#  start_x start_y current_x current_y, and is expected to return a string
#  for display.  It may well ignore those parameters (use args) and return
#  the value of data being dragged.
#
#  $Log: da_dragndrop.tcl,v $
#  Revision 1.3  2006/09/22 03:10:46  asnd
#  Various small patches.
#
#  Revision 1.2  2006/05/26 16:50:00  asnd
#  Accumulated changes over shutdown; notably fixed y-axis scales on logged-variables graph
#
#  Revision 1.1  2002/06/12 01:26:39  asnd
#  Starting versions of Mui files.
#

if { ![info exists muisrc] } { set muisrc {} }

global grabCursor dropCursor

set grabCursor "@[file join $muisrc hand-closed-data.xbm] [file join $muisrc hand-closed-mask.xbm] white black"
set dropCursor "@[file join $muisrc hand-open-data.xbm] [file join $muisrc hand-open-mask.xbm] white black"

namespace eval dadnd {

variable grabx 0
variable graby 0
variable dragFuzz 4
variable clickms 300
variable afterid ""

catch { set clickms $::PREF::clickms }

#   
proc group_drag_bind { group widg vars {dispp {}}} {
    variable ${group}_drbind
    set ${group}_drbind(w_$widg) $vars

    #if { [llength $vars] && $dispp == "" && ![info exists ${group}_drbind(proc)] } {
    #    set dispp [list group_disp_value $vars]
    #}

    declare_drag $widg [list ::dadnd::group_drop $group $widg $vars] $dispp
}

proc group_drag_unbind { group widg } {
#   Configure cursor and button presses to be ordinary for a widget.
    variable ${group}_drbind
    catch { unset ${group}_drbind(w_$widg) }
    $widg configure -cursor {}
#   Remove mouse-button bindings
    bind $widg <ButtonPress-1> ""
    bind $widg <ButtonRelease-1> ""
}

proc group_drag_bind_proc { group p } {
    variable ${group}_drbind
    set ${group}_drbind(proc) $p
}


#  Simple proc for displaying values during a group drag_copy.  Use it if you want.
proc group_disp_value { vars args } {
    uplevel \#0 return "\$[join $vars {  $}]"
}

#  Simple proc for displaying a fixed string during a drag.  Use it at will.
proc drag_disp_string { string args } {
    return $string
}


#   Procedure executed on buttonrelease, to drop the values to another widget.
#
proc group_drop { group widg grabvar grabx graby x y } {
    variable dragFuzz
    variable ${group}_drbind
    global dropCursor

    #
    #   Check if release position is significantly different from press 
    #   position and outside the original widget.  Binding and position-
    #   offsets of Release events are the same as for the Press event, 
    #   even if the mouse has moved (dragged) off the widget (can give 
    #   negative positions).
    #
    set w [winfo containing $x $y]
    if { abs($x-$grabx) + abs($y-$graby) > $dragFuzz && ![string equal $w $widg] } {
        # Check if drop widget is part of group
        if { [catch { set ${group}_drbind(w_$w) } dropvar ] != 1 && $dropvar != ""} {
            # check if group has a proc
            if { [info exists ${group}_drbind(proc) ] } {
                [set ${group}_drbind(proc)] $grabvar $dropvar
            } else {
                #   Use default "copy" procedure when no procedure is defined for this group
                drag_copy_group_var $grabvar $dropvar
            }
        }
    }
    $widg configure -cursor $dropCursor
}


#   Standard drop procedure: copies variable-list
proc drag_copy_group_var { grabVarList dropVarList } {
    foreach dropV $dropVarList grabV $grabVarList {
        uplevel \#0 "set {$dropV} \${$grabV}"
    }
}

variable popupText 

#  Set up the bindings for dragging widget $widg.  Invoke the $callback with parameters
#  win grabx graby dropx dropy (in pixels) after the drag.  Use the proc $dispp to
#  create a display string during the drag; it should be a proc that takes four
#  (more) args: grabx graby dropx dropy
#
proc declare_drag { widg callback {dispp {}} } {
    global dropCursor
    bind $widg <ButtonPress-1> [list dadnd::start_drag %W $dispp %X %Y]
    bind $widg <ButtonRelease-1> [list dadnd::end_drag %W $callback %X %Y]
    $widg configure -cursor $dropCursor
}

proc start_drag { widg dispp x y } {
    variable grabx
    variable graby
    variable clickms
    variable afterid
    global grabCursor

    set grabx $x
    set graby $y

    # Perhaps this should be done after a delay too...
    $widg configure -cursor $grabCursor

    after cancel $afterid
    set afterid [after $clickms [list ::dadnd::show_drag $widg $dispp $grabx $graby]]

    bind $widg <B1-Motion> [list dadnd::mid_drag $widg $dispp %X %Y]

    # Enable a check if something else grabs control in the middle of a drag
    bind all <Motion> [list dadnd::interrupt_drag $widg %W]

}

proc show_drag { widg dispp x y } {
    variable afterid
    variable popupText

    after cancel $afterid
    set afterid ""
    set popupText ""

    # If either an image or a conversion is specified, then construct popup window
    set img {}
    catch { set img [$widg cget -image] }
    if { [string length "${dispp}${img}" ] } {
        catch { destroy .dadnd_popup_win }
        toplevel .dadnd_popup_win -height 1 -width 1
        wm overrideredirect .dadnd_popup_win 1
        set bg [$widg cget -background]
        if { [string length $img] } {
            label .dadnd_popup_win.img -text {} -image $img -bg $bg -width 0 -padx 0 -pady 0
            pack  .dadnd_popup_win.img -side left
        }
        if { [string length $dispp] } {
            label .dadnd_popup_win.value -textvar ::dadnd::popupText -width 0 -justify left -pady 0
            pack  .dadnd_popup_win.value -side left
        }
    }
    mid_drag $widg $dispp $x $y
}

proc mid_drag { widg dispp x y } {
    variable popupText
    variable grabx
    variable graby
    variable afterid
    variable dragFuzz

    if { [string length $dispp] } {
        catch {
            set popupText [eval $dispp $grabx $graby $x $y]
        }
    }

    #  If position is significantly different from start position and if
    #  adornments have not been displayed yet, then display them.
    if { [string length $afterid] && (abs($x-$grabx) + abs($y-$graby) > $dragFuzz) } {
        show_drag $widg $dispp $x $y
        return
    }

    #  Update display of adornments, if they are used.
    if { [winfo exists .dadnd_popup_win] } {
        # Calculate width and height to center img on pointer, or display text to the right of pointer
        # Phantom width of non-existent icon = 24
        set w -24
        set h 2
        if { [winfo exists .dadnd_popup_win.img] } {
            set w [winfo reqwidth .dadnd_popup_win.img]
            set h [winfo reqheight .dadnd_popup_win.img]
        }
        if { [winfo exists .dadnd_popup_win.value] } {
            set h2 [winfo reqheight .dadnd_popup_win.value]
            set h [expr { $h > $h2 ? $h : $h2 }]
        }
        set gx [expr { $w/2 > $x ? 0 : $x-($w/2) }]
        set gy [expr { $h/2 > $y ? 0 : $y-($h/2) }]
        wm geometry .dadnd_popup_win [format {%+d%+d} $gx $gy]
        raise .dadnd_popup_win
    }
}

proc end_drag { widg callback x y } {
    variable grabx
    variable graby

    cancel_drag $widg

    after 0 "$callback $grabx $graby $x $y"
}

proc cancel_drag { widg } {
    variable afterid
    global dropCursor

    after cancel $afterid
    set afterid ""

    $widg configure -cursor $dropCursor

    bind $widg <B1-Motion> {}
    bind all <Motion> {}

    catch { destroy .dadnd_popup_win }
}

# Cancel drag if something else (say, a transient dialog) grabs the pointer
proc interrupt_drag { widg W } {
    if { ![string equal $widg $W] } {
        cancel_drag $widg
    }
}

namespace export group_drag_bind_proc group_drag_bind group_drag_unbind
namespace export declare_drag 
namespace export drag_disp_string group_disp_value

}

namespace import dadnd::*


# Testing
if { [info exists argv0] && [string equal $argv0 [info script]] } {
    button .b1 -text A -command {} 
    entry .e1 -textvar e1var
    group_drag_bind Test .b1 e1var
    button .b2 -text B -command {} 
    entry .e2 -textvar e2var
    group_drag_bind Test .b2 e2var
    button .b3 -text C -command {} 
    entry .e3 -textvar e3var
    group_drag_bind Test .b3 e3var
    
    grid .b1 .e1
    grid .b2 .e2
    grid .b3 .e3

    proc cbtest { var args } {
        #puts "Final answer: $args"
        set ::$var $args
    }
    
    proc contest { gx gy x y } {
        expr { ($gy-$y)/980.0 }
    }

    image create photo locked_icon  -file [file join $muisrc lock-closed.gif]
    button .lock -text {} -image locked_icon
    label .ll -textvar locky
    grid .lock .ll
    button .bd -text X
    label .ld -textvar Xy
    grid .bd .ld

    declare_drag .lock {cbtest locky} contest  
    declare_drag .bd {cbtest Xy} contest  

}
