#   details_extra.tcl
#   Ancilliary procedures for sweep configuration expert parameters

#   $Log: details_extra.tcl,v $
#   Revision 1.4  2004/08/06 04:15:27  asnd
#   Fix problems with unstable grid geometry management
#
#   Revision 1.3  2003/11/20 03:27:44  asnd
#   Minor changes to appearance
#
#   Revision 1.2  2003/10/15 09:28:28  asnd
#   Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#   Revision 1.1  2003/09/26 00:39:03  asnd
#   Recent gui changes (sweep details, ggl pulser...)
#
#


if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
    set detwin ""
}

proc details_initialize { type } {

    global detwin miinsdefs idef imusr_inst_defs

    #puts "Initialize setup for details of $type"
    #puts "Known types are [array names imusr_inst_defs]"

    set dw [td_win_name $detwin]
    if { [ string length $detwin ] } {
        # We are in a real application, where the setup window is not main
        if { [winfo exists $detwin.pu_OK_b] } {
            # if window exists, make it visible
            wm deiconify $dw
            raise $dw
        } else {
            # window is not built yet.  Build main window
            toplevel $dw
            details_ui $detwin
        }
    }

    wm title $dw "Sweep Details"

    det_bind_help det_cancel_b "Close window; abandon changes."
    det_bind_help det_OK_b  "Apply changes and close window."

    det_bind_help det_title_e  "Enter a readable name or description for the instrument here; for example, \"Oxford IPS120-10 Magnet PS\"."
    det_bind_help det_name_e  "Identify the sweep parameter, for example \"Field\" or \"Current\"."
    det_bind_help det_tag_e  "Identify the sweep type: B, RF, or T."
    det_bind_help det_units_e  "The units of the sweep variable, when expressed as an integer.\nFor example, \"G\" if the Camp variable uses Tesla."
    det_bind_help det_scale_e  "The sweep setting is saved as an integer.  Enter a scale factor to convert from the Camp variable units to integer units. E.g., 10000 for T -> G."
    det_bind_help det_min_e  "Enter the minimum valid setting (integer units) OR a Camp variable giving the minimum in raw Camp units."
    det_bind_help det_max_e  "Enter the maximum valid setting (integer units) OR a Camp variable giving the maximum in raw Camp units."
    det_bind_help det_path_e  "Enter the Camp variable path for the sweep setting.  Use /~ for the instrument name."
    det_bind_help det_init_e  "Enter a Tcl initialization script to be executed in the Camp host at the start of a run.  Use /~ for the instrument and \$val for the initial setting."
    det_bind_help det_tol_e  "Enter a Tcl script to test if the sweep device is ready to start a run.  Use /~ for the instrument and \$val for the initial setting."

#   Unlike with menu-buttons, regular buttons with -underline still need to 
#   have an alt-key binding added explicitly.
    bind $dw <Alt-q> "$detwin.det_cancel_b invoke ; break"
    bind $dw <Alt-o> "$detwin.det_OK_b invoke ; break"

    bind $detwin.det_head_l <Configure> {after 10 after idle det_adjust_wrap}
    after 10 after idle det_adjust_wrap

    # Un-do a bad spectcl binding
    bind $dw <Destroy> {}

    # Get previous values
    set miinsdefs(device_type) $type
    if { [info exists imusr_inst_defs($type)] } {
	foreach var [list descr kind what units scaling set_var min max init_script tol_test] val $imusr_inst_defs($type) {
	    #puts "Initially, $var = $val"
	    set miinsdefs($var) $val
	}
    }
}

#  THE HELP LINE:
proc det_bind_help { w h } {
    global detwin
    bind $detwin.$w <Enter> [list set miinsdef_help_text "$h"]
    bind $detwin.$w <FocusIn> [list set miinsdef_help_text "$h"]
    bind $detwin.$w <Leave> [list set miinsdef_help_text {}]
    bind $detwin.$w <FocusOut> [list set miinsdef_help_text {}]
}

#   Apply any changes in the details set window, and close it.
proc det_OK { } {
    global imusr_inst_defs miinsdefs
    set defs [list]
    foreach var [list descr kind what units scaling set_var min max init_script tol_test] {
	lappend defs $miinsdefs($var)
    }
    set imusr_inst_defs($miinsdefs(device_type)) $defs
    after 10 { after idle {idef_sweep_device $midef(sweep_device) $midef(sweep_ins_name)} }

    det_cancel
}

#   Quit the details set window, abandoning any changes
proc det_cancel { } {
    global detwin
    destroy [td_win_name $detwin]
}

# Adjust the word-wrap width of the large multi-line labels.
# (Maybe this should be a text widget that 
proc det_adjust_wrap { } {
    global detwin
    scan [winfo geometry $detwin.det_info_l] {%d} w
    incr w -5
    if { $w > 50 } {
        incr w -8
        $detwin.det_info_l configure -wraplength $w
        $detwin.det_help_l configure -wraplength $w
    } else {
        after 50 after idle det_adjust_wrap
    }
}

if { $detwin == "" } {
    # stand-alone testing, so start immediately!
    details_initialize {}
}

