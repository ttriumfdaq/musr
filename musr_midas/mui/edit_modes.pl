#!/usr/bin/perl
#
# edit_modes.pl
# 
# Version 1.0     21-Apr-2002    DA     Edit mode files to follow changes to rig.
#
# $Log: edit_modes.pl,v $
# Revision 1.3  2003/10/29 14:44:35  asnd
# cleanup debug
#
# Revision 1.2  2002/07/16 06:00:53  asnd
# Changes up to Jul 15
#
# Revision 1.1  2002/06/12 01:26:39  asnd
# Starting versions of Mui files.
#
#
# Parameters:  path [ from to [ ... ] ]
#
# This program edits all mode files on the path, converting selected-counter
# names "from" -> "to".  Multiple pairs of (from, to) will be specified, one
# for every counter that retained identity through the rig change.  Counters
# that keep the same name should still be listed ("Right Right").  Counters
# in the original file, but not listed on the command line will be changed to
# ".".  The order of the selected counter list is preserved, including
# the place-holders for undefined counters, so that the numbers in the lists
# of good-bin-ranges are still applied to the correct counter.

#print "edit_modes.pl run with arguments @ARGV\n";

if ( scalar @ARGV < 3 ) {
    die "No counter mapping specified" ;
}

$path = shift ;
%counters = @ARGV ;

FILE: while ( <$path/*.mode> ) {
        $modefile = $_ ;
        # print "Do file $modefile\n";
        open (MODE, "$modefile" ) ;
        open (TEMP, ">$path/temp.odb") ;

        do {
            $line = <MODE> ;
            print TEMP "$line";
        } until ( eof ||
            ( $line =~ /^select counters to histogram = STRING/ ) );

        if ( eof ) {
            close MODE ;
            close TEMP ;
            print "Premature end of file $modefile!\n";
            system ( "/bin/rm $path/temp.odb" ) ;
            next FILE
        }

        while ( ($line = <MODE>) =~ /^\[(\d+)\] ([^\n]*)/ ) {
            if ( exists $counters{$2} ) {
                print TEMP "[$1] $counters{$2}\n" ;
            }
            else {
                print TEMP "[$1] .\n" ;
            }
        }
        print TEMP "$line" ;

        unless ( eof ) {
            print TEMP <MODE> ;
        }

        close MODE ;
        close TEMP ;
        system ( "/bin/mv $path/temp.odb $modefile" ) ;

    next FILE
}

