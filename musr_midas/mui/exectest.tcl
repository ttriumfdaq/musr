#! /bin/bash
# The next line is executed by sh, not Tcl; extend PATH locally: \
export PATH=$PATH:/home/musrdaq/musr/musr_midas/midas_tcl:$CAMP_DIR
# The next line is executed by sh, not Tcl \
exec /home/musrdaq/musr/musr_midas/midas_tcl/mtcl "$0" "$@"


if { [catch {wm withdraw .} msg] } {
   puts stderr "This is a graphical program, but no functioning graphics display is available."
   exit
}

set muisrc [file dirname [info script]]
if { ![file exists [file join $muisrc mui_utils.tcl]] } {
    set dirlist [glob -directory $muisrc -nocomplain [file join * mui_utils.tcl]]
    if { [llength $dirlist] > 0 } {
        set muisrc [file dirname [lindex $dirlist 0]]
    } else {
        set muisrc ~musrdaq/musr/musr_midas/mui
    }
    unset dirlist
}
puts "muisrc: $muisrc"

proc testexec { } {
    if { [catch {exec /bin/grep -F "!   time " /home/bnmr/.loggedcampvars | /usr/bin/tail -n 1} result] } {
        puts "Failure: $result\n>>$::errorCode\n>>$::errorInfo"
    } else {
        puts "Success: $result"
    }
}
testexec

set tcl_precision 15

set midas(application) camplog
set midas(connected) 0
set midas(host) ""
set midas(data_dir) .
set midas(camplogging) 1

set clogv(camp_only) 0

if { [info exists env(MIDAS_EXPT_NAME)] } {
    set midas(expt) $env(MIDAS_EXPT_NAME)
} else {
    # Declare that we are running Camp only, we have no Midas DAQ.
    set midas(expt) ""
    set clogv(camp_only) 1
    if { [info exists env(DAQ_HOST)] } {
        if { [lindex [split $env(DAQ_HOST) .] 0] eq [lindex [split [info hostname] .] 0] } {
            puts "No run information is available. To display run information, log on to $env(DAQ_HOST)"
        } else {
            puts "No run information is available. Check MIDAS_EXPT_NAME"
        }
    }
}

if { [info exists env(BEAMLINE)] } {
    set midas(beamline) $env(BEAMLINE)
} else {
    set clogv(camp_only) 1
    set midas(beamline) {}
    puts "No run information is available. Check BEAMLINE environment variable."
}

set midas(musr) 0 ;# [expr { ! [string match "b*" $midas(expt)]} ]

set run(expertype) TD-bnmr
set run(ImuSR) 0

set idef(full_tol_test) "set a 1"
set idef(num_sweeps) 0

# set some default parameters (for use before odb is read)
set run(state) 1
set run(starting) 0
set run(test_mode) -1
set run(in_progress) 0
set run(paused) 0
set run(menustate) {}
set run(starting) 0
set run(number) {}
set run(start_sec) 0
set run(start_record) {}
set run(acqoff_record) {}
set run(in_progress) 0

set wtstat(enabled) 0

source [file join $muisrc mui_utils.tcl]
set logwin ".log"
testexec

if { $clogv(camp_only) } {
    proc odb_get_general { } { }
    proc odb_get_runinfo { } { }
    proc odb_init_acqrecord { } { }
    proc odb_reconnect { } { }
    proc odb_disconnect { } { }
} else {
    source [file join $muisrc odb_bnmr.tcl]
}
source [file join $muisrc logvar_extra.tcl]
source [file join $muisrc logvar.ui.tcl]

# disable some DAQ functions, and use the stand-alone page layout.
proc idef_declare_changed { } { }
proc idef_propagate { } { }
proc imusr_check_polling { v i } { }
proc odb_get_wt_stat { } { }
testexec
# Update parameters at about pentuple the file-log frequency.
set ::updateID {}
proc sched_update { } {
    global updateID
    puts "sched_update"
    set delay 1000
    catch { expr { round(200*$::clogv(varlog_interval)) } } delay
    set delay [expr { $delay < 2000 ? 2000 : ( $delay > 20000 ? 20000 : $delay ) }]
    after cancel $updateID
    set updateID [after $delay { sched_update }]
    puts "$updateID : after $delay { sched_update }"
    catch { odb_get_general ; odb_get_runinfo } m1
    catch { logvar_update } m2
    puts "sched_update done: $m1 ; $m2"
}

puts "Connect to experiment $env(MIDAS_EXPT_NAME)"

#midas connect_experiment "" $env(MIDAS_EXPT_NAME) "camplog"
#testexec
#puts "get experiment database"
#midas get_experiment_database

odb_connect

testexec

    if { [catch { odb_get_general } mes] } { puts "Failed odb_get_general: $mes\n>>$::errorCode\n>>$::errorInfo" }
testexec
    if { [catch { odb_init_acqrecord } mes] } { puts "Failed odb_init_acqrecord: $mes\n>>$::errorCode\n>>$::errorInfo" }
testexec

    proc mui_heartbeat { } {
	after 2000 mui_heartbeat
	catch { odb_reconnect } 
    }
    #after 5000 mui_heartbeat


puts "disconnect from midas"
#midas disconnect_experiment
odb_disconnect

testexec

exit
