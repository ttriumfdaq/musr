#  $Log: expert_extra.tcl,v $
#  Revision 1.13  2015/03/20 00:51:18  suz
#  changes by Donald. This version part of package for new VMIC frontends
#
#  Revision 1.12  2013/10/30 06:04:55  asnd
#  misc. changes.
#
#  Revision 1.11  2008/05/15 09:50:02  asnd
#  Fix typo in hotkey bindings
#
#  Revision 1.10  2008/03/06 04:27:10  asnd
#  Changes supporting counting single histogram for autorun ending.
#
#  Revision 1.9  2006/11/22 20:20:13  asnd
#  Unlimited saved versions (say 500)
#
#  Revision 1.8  2006/07/08 01:27:22  asnd
#  Configurable mu character for window titles, to handle bug with versions in Sci Linux.
#
#  Revision 1.7  2006/06/29 07:29:57  asnd
#  Use safer p_win_name (from mui_utils.tcl) for dialogs
#
#  Revision 1.6  2006/05/26 17:01:35  asnd
#  Accumulated changes.  Notably user control over noisy alerts.
#
#  Revision 1.5  2005/06/22 15:25:16  asnd
#  Add control of variable weighting, title checking, and confirmation prompting
#
#  Revision 1.4  2004/08/05 02:34:10  asnd
#  Allow zero to turn off client checks
#
#  Revision 1.3  2003/10/15 09:28:28  asnd
#  Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#  Revision 1.2  2003/09/19 06:23:26  asnd
#  ImuSR and many other revisions.
#
#  Revision 1.1  2002/06/12 01:26:39  asnd
#  Starting versions of Mui files.
#

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

global tdxwin expert

if { [catch {set tdxwin}] } {
    set tdxwin ""
    source [file join $muisrc expert.ui.tcl]
}

set expert(list) [list camp_host keep_versions archive_to save_sec]

# Fire up the expert setup window. When used for real, tdx_initialize is invoked by
# menus.  When testing the gui it can be invoked at the end of this source file.

proc tdx_initialize { } {
    global tdxwin midas expert mode

    if { [ string length $tdxwin ] } {
        # We are in a real application, where the window is not main
        if { [winfo exists $tdxwin.x_head_l] } {
            # window already exists: make it visible
            wm deiconify [td_win_name $tdxwin]
            raise [td_win_name $tdxwin]
        } else {
            # window is not built yet. 
            toplevel $tdxwin
            expert_ui $tdxwin
        }
    }

    odb_get_general

    wm title [td_win_name $tdxwin] "${PREF::muchar}SR Advanced Config"
    set expert(camp_host) $midas(camp_host)
    set expert(keep_versions) $midas(keep_versions)
    set expert(archive_to) $midas(archive_to)
    set expert(save_sec) $midas(save_sec)
    set expert(check_clients) $midas(check_clients)
    set expert(refresh_sec) [expr {( $midas(refresh_ms) + 500 )/ 1000 } ]
    set expert(num_back) $mode(num_back)
    set expert(validateTitles) $::rtitles(validate)
    set expert(confirm_stop) $::run(confirm_stop)
    odb_get_wt_stat
    set expert(wtstat) $::wtstat(enabled)
    set expert(beep) $::PREF::enableBeep
    puts "muMainFont [font config muMainFont]"
    puts "muEntryFont [font config muEntryFont]"
    puts "muMonoFont [font config muMonoFont]"
    puts "muTitleFont [font config muTitleFont]"
    array set fontconfig [font config muMainFont]
    puts "Font size $fontconfig(-size), basesize $PREF::fontsize, currfontsize $PREF::currfontsize"
    foreach rb {0 1 2 3 4} {
        set s [expr {abs($PREF::fontsize + 2*($rb-2))}]
        set fontconfig(-size) -$s
        $tdxwin.x_fontsize${rb}_rb config -font [array get fontconfig] -value $s
    }
    set expert(fontsize) $PREF::currfontsize
    bind [td_win_name $tdxwin] <Alt-c> "tdx_cancel; break"
    bind [td_win_name $tdxwin] <Alt-o> "tdx_ok"
}

proc tdx_ok { } {
    global tdxwin midas expert mode wtstat run rstat

#   Validate some things:

    if { $wtstat(enabled) != $expert(wtstat) } {
        set ans "yes"
        if { $run(in_progress) } {
            catch odb_get_stat
            if { $run(hist_total) > -1 } {
                if { $expert(wtstat) } {
                    set msg "Weighting by counts will not be retroactively applied to data already taken."
                } else {
                    set msg "Disabling weighting will force simple averaging of data already taken."
                }
                beep
                set ans [timed_messageBox -timeout 30000 -type yesnocancel -default yes \
                             -icon warning -title "Toggle Weighting" -parent [p_win_name $tdxwin] \
                             -message "A run is in progress!\n$msg\nApply weighting change?" ]
            }
        }
        # Only apply the change if OK.
        switch $ans {
            yes { # apply setting
                set wtstat(enabled) $expert(wtstat)
                td_odb odb_set_wt_stat "control weighted averaging"
                logvar_update
            }
            no { # don't change the weighting setting, but apply other changes
                set expert(wtstat) $wtstat(enabled)
            }
            cancel { # Don't "OK" anything; don't close window
                set expert(wtstat) $wtstat(enabled)
                return
            }
        }
    }
    set midas(camp_host) $expert(camp_host)

    scan $expert(keep_versions) {%d} midas(keep_versions)
    set midas(keep_versions) [bounded $midas(keep_versions) 1 500]

    if { [ file isdirectory $expert(archive_to) ] } {
        set midas(archive_to) $expert(archive_to)
    }

    scan $expert(save_sec) {%d} midas(save_sec)
    set midas(save_sec) [bounded $midas(save_sec) 20 3600]
#
    odb_set_general

    set ::rtitles(validate) $expert(validateTitles)

    # Values 0 (few), 1 (quit gui, kill run, switch exp type), 
    # 2 (abrupt run stop, zero run (when counts), stop logging var), 
    # 3 (end run, begin test run, switch to test mode... ie, pointless nagging)
    # Default is 2.
    set run(confirm_stop) $expert(confirm_stop)

    set mode(num_back) [bounded $expert(num_back) 0 [llength $mode(backups)]]

    set rs [expr {($midas(refresh_ms) + 500 ) / 1000} ]
    scan $expert(refresh_sec) {%d} rs
    set midas(refresh_ms) [bounded [expr {$rs * 1000}] 2000 90000]

    set ds [expr {$midas(check_clients)+0}]
    scan $expert(check_clients) {%d} ds
    if { $ds <= 0 } {
        set midas(check_clients) 0
    } else {
        set midas(check_clients) [bounded $ds 60 99999999]
    }

    set ::PREF::enableBeep $expert(beep)

    puts "Selected font size $expert(fontsize), basesize $PREF::fontsize, currfontsize $PREF::currfontsize"
    if { $expert(fontsize) != $PREF::currfontsize } {
        puts "Reconfigure font sizes"
        set PREF::currfontsize $expert(fontsize)
        mui_font_size configure $expert(fontsize)
    }

    td_background_update

    tdx_cancel
}

proc tdx_cancel { } {
    global tdxwin
    destroy [ td_win_name $tdxwin ]
}


set rtitles(validate) 1
set run(confirm_stop) 2
set wtstat(enabled) 1


#  DEBUG
#
#global midas
#set midas(camp_host) "m15vw"
#set midas(keep_versions) "6"
#set midas(archive_to) "/musr/incoming/m15"
#set midas(save_sec) "120"
#
# END DEBUG


if { $tdxwin == "" } {
    # stand-alone testing, so start immediately!
    proc logvar_update {} {}
    proc td_background_update {} {}
    tdx_initialize
    set wtstat(enabled) 0
}


