#!/usr/bin/perl
#
# extract_branch.pl
# 
# Version 1.0     18-Sept-2002    DA     Extract mode parameters from an odb file
#
# $Log: extract_branch.pl,v $
# Revision 1.1  2002/09/21 04:39:22  asnd
# Loading modes from previous runs
#
#
# Parameters:  input_file output_file odb_branch
#
# This program edits an odb file, extracting only the "mode" section.

#print "extract_branch.pl run with arguments @ARGV\n";

if ( scalar @ARGV < 3 ) {
    die "Usage: extract_branch.pl input_file output_file odb_branch" ;
}

$inputf = shift ;
$outputf= shift ;
$branch = shift ;

open (INF, "$inputf" ) ;
open (OUTF, ">$outputf") ;

do {
    $line = <INF> ;
} until ( eof ||
        ( $line =~ /^\[$branch\]/ ) );

if ( eof ) {
    close INF ;
    close OUTF ;
    system ( "/bin/rm $outputf" ) ;
    die "Premature end of file $inputf: \"$branch\" not found";
}

print OUTF $line ;

while (1) {
    $line = <INF> ;
    if ( $line =~ m;^\[$branch; ) {
        print OUTF $line ;
    }
    elsif ( $line =~ m;^\[/; ) {
        last ;
    }
    else {
        print OUTF $line ;
    }
}

close INF ;
close OUTF ;

