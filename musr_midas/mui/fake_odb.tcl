#   Fake odb interaction functions for testing somewhere that has no
#   data acquisition isnstalled...  By 2014, lags behind changes to 
#   odb_musr.tcl

puts "Using fake odb for testing"

proc odb_connect { } {
    global midas
    set midas(connected) 1
}

proc odb_disconnect { } {
    global midas
    set midas(connected) 0
}

proc odb_reconnect { } {
    global midas
    set midas(connected) 1
}

proc odb_set_exper_type { exp mexp } {
    global midas run 
    odb_get_runinfo
    set run(expertype) $exp
    odb_set_runinfo
}

proc odb_set_general { } {
    global midas
    set f [open odb_general.fake w]

    puts $f "array set midas \[list [array get midas]\]"

    close $f

    return ""
}

proc odb_set_auto_head { } {
}

proc odb_get_general { } {
    global midas
#
    source odb_general.fake
#
    return ""
}

#  Get scaler readings and histogram totals.  There is no "set".
proc odb_set_runinfo { } {
    global run midas
#
    set f [open odb_runinfo.fake w]
#
    puts $f "array set run \[list [array get run]\]"
#
    close $f

    return ""
}


proc odb_get_runinfo { } {
    global run midas

#puts "Get runinfo"
#
    set prev_number $run(number)
    set sav_state $run(state)
    set sav_start $run(starting)
    set prev_start $run(start_sec)
    set sav_how $run(how_start_I)
#
    source odb_runinfo.fake
#
    set run(how_start_I) $sav_how
    set run(state) $sav_state
    set run(starting) $sav_start
    set run(in_progress) [expr {$run(state) > 1} ]
    set run(paused) [expr {$run(state) == 2} ]

#   When we see the run is active, we force not-"starting" 
#   THIS IS PROPER FOR IMUSR ONLY IF IMUSR RUNS BEGIN IN THE PAUSED STATE

    if { $run(state) == 3 } { 
	set run(starting) 0
    }

#   run(starting) has values: 0 = not starting, 1 while Midas starting run, 2 while
#   prompting user for ImuSR sweep range, 3 while ramping ImuSR sweep device.
#   Ordinarily, $run(starting)==0 and the state label comes from $run(state).

    set run(state_label) [lindex [list \
			[lindex [list bad Stopped Paused Running] $run(state)] \
			Starting Preparing Initializing ] $run(starting) ]
#   puts "Get label for state $run(state); starting $run(starting): $run(state_label)"

#
    set run(test_mode) [expr [string compare $run(runtype) real] ]
    if { $run(in_progress) } {
        set run(ssat_text) "Started at:"
        set run(ssat_time) $run(start_time)
    } else {
        set run(ssat_text) "Stopped at:"
        set run(ssat_time) $run(stop_time)
    }
#
#    set run(expertype) "I-�SR"
    set run(ImuSR) [string match "I*" $run(expertype)]
#
    return ""
}

proc odb_set_mode { } {
    global mode rig midas
#
    set f [open odb_mode.fake w]

    puts $f "array set mode \[list [array get mode]\]"

    close $f

    file copy -force odb_mode.fake "$rig(rigpath)/$rig(rigname)/$mode(modename).mode"

    return
}

proc odb_get_mode { } {
    global mode midas
#
    source odb_mode.fake

    set mode(maxbins) [expr 1024*1024]

#   Others based on the above:
    catch {
        set mode(numhist) [ expr { $mode(numsel) + $mode(numsel) * $mode(dual_enabled) } ]
        set div [ expr round(pow(2,26.0-$mode(resolution))) ]
        set mode(binsize) [ expr 50.0/$div ]
        if { $mode(resolution) > 18 } {
            set mode(binsize_label) " $mode(binsize)  ns  "
        } else {
            set mode(binsize_label) " [expr 1000.0 * $mode(binsize)]  ps  "
        }
        set mode(hlen_us) [format {%.4f} [expr $mode(binsize)*$mode(hlen)/1000.0 ] ]
        set mode(hlen_label) "$mode(hlen) bins  ($mode(hlen_us) �s)"
    }
#
    return ""
}

proc odb_set_rig { } {
    global rig midas
#
    set f [open odb_rig.fake w]

    puts $f "array set rig \[list [array get rig]\]"

    close $f

    file copy -force odb_rig.fake "$rig(rigpath)/$rig(rigname)/$rig(rigname).rig"

    return 1
}

proc odb_get_rig { } {
    global rig midas
#
    source odb_rig.fake

#   Look for inconsistencies:
    if { $rig(enable_scalers) == 0} { set rig(num_scalers) 0 }
    if { $rig(num_scalers) == 0} { set rig(enable_scalers) 0 }
    set rig(rigpath) [string trimright $rig(rigpath) "/"]

    return ""
}

proc odb_get_idef { } {
    global idef

    set s $idef(changed_at)
    source odb_idef.fake
    set idef(changed_at) $s
    return ""
}

proc odb_set_idef { } {
    global idef

    set f [open odb_idef.fake w]

    puts $f "array set idef \[list [array get idef]\]"

    close $f

    return 1
}

proc odb_get_stat { } {
    global run rstat rig mode midas

    set run(hist_total) 12345678
    set rstat(htotals) "23452 234123 6546 6652 12347 8654"
    set rstat(nhist) 2
    set rstat(hnames) "foo bar"
#
    return ""
}

proc odb_set_wt_stat { } { }
proc odb_get_wt_stat { } { }

proc odb_get_auto { } {
    global autor

    source odb_auto.fake
    return ""
}

proc odb_set_auto { } {
    global autor

    set f [open odb_autor.fake w]

    puts $f "array set autor \[list [array get autor]\]"

    close $f

    return 1
}

proc odb_load_file { f } {
    global rig mode midas run
    source $f
    odb_set_rig
    odb_set_mode
}

proc odb_begin_run { } {
    global run

    puts "Simulate begin of run"
#   get next run number (really use midas script)
    incr run(number)

    set run(in_progress) 1
    set run(paused) 0
    set run(starting) 1
    odb_set_runinfo
    after 6000 { set run(state) 2 } ;#  ???? Just an ImuSR test

    return
}

proc odb_get_runnum  { } {
    global run
    incr run(number)
}

proc odb_cancel_transition { } { }

proc odb_end_run { } {
    global run

    set run(in_progress) 0
    set run(paused) 0
    set run(state) 0
    odb_set_runinfo
}

proc odb_kill_run { } {
    global run midas

    odb_get_runinfo

    if { !$run(transition) && !$run(in_progress) } {
        return -code error "No run to kill"
    }

    set run(did_stop_at) [expr {[clock seconds] + 10}]
    odb_end_run
    incr run(number) -1
    odb_set_runinfo

    return ""
}

proc odb_abrupt_stop { } {
    odb_end_run
}

proc odb_pause_run { } {
    global run
    odb_get_runinfo
    if { $run(in_progress) } {
        set run(state) 2
        odb_set_runinfo
        odb_get_runinfo
        odb_set_runinfo
    }
    return ""
}

proc odb_resume_run { } {
    global run
    odb_get_runinfo
    if { $run(in_progress) } {
        set run(state) 3
        odb_set_runinfo
        odb_get_runinfo
        odb_set_runinfo
    }
    return ""
}

proc odb_save_data { } { }

proc odb_set_run_type { type } { }

proc odb_renormalize {} {}

proc odb_zero_hists { } { }

proc odb_zero_ref_hists { } { }

proc odb_zero_one_hist { hist } { }

proc odb_zero_scalers { } { }

proc odb_hmdisplay { yn } { }

proc odb_tdc_clear { } { }

proc mode_list_modes { except } {
    global rig
    set modes [list]
    foreach f [glob -nocomplain $rig(rigpath)/$rig(rigname)/*.mode] {
        set m [file rootname [file tail $f] ]
        if { [string compare $m $except] } {
            lappend modes $m
        }
    }
    return [lsort $modes]
}

set run(starting) 0
set run(did_stop_at) [clock seconds]
set autor(counts_typed_at) 0

proc td_all_update { } { }
proc rig_update { } { }
proc mode_update { } { }
proc runstat_update { } { }



set mode(changing) 0
set mode(binsize) 0.0
set mode(resolution) 20
set mode(dual_enabled) 0
set mode(dual_titles) "SAMPLE REFERENCE"
set mode(dual_suffs) "_s _r"
set mode(savedir) ""
set mode(modename) any
set mode(onlybins) ""
set mode(TDCgate_code) 45
set mode(PUgate_code) 45
set mode(modefile) "./test.odb"
set mode(backups) \
    [list Previous_mode Next_previous Third_previous Fourth_previous Fifth_previous]
set mode(num_back) 3
set mode(selcounters) [list Backward Forward Up Down]
set mode(numsel) 4

set rig(counter_names) [list Backward Forward Left Right Up Down]
set rig(num_counters) 6
set rig(TDC_in) [list 0 1 2 3 6 7]
set rig(OR_out) [list 6 5 1 2 13 14]
set rig(enable_OR) 1
set rig(enable_scalers) 1
set rig(num_scalers) 2
set rig(scaler_names) [list uinc ugate]
set rig(scaler_in) [list 5 7]
set rig(changing) 0
set rig(rigname) "therig"
set rig(rigpath) "/tmp/mui/rigs"

set midas(connected) 0
set midas(host) {}
set midas(expt) "musr"
catch { set midas(expt) $env(MIDAS_EXPT_NAME) }
set midas(check_clients) 0

set run(state) 1
set run(state_label) [lindex [list bad Stopped Paused Running] $run(state)]
set run(in_progress) [expr {$run(state) > 1 }]
set run(paused) [expr {$run(state) == 1 }]
set run(test_mode) 0
set run(runtype) real
set run(expertype) "I-muSR" ; set run(ImuSR) 1
set run(number) 1233
set run(start_sec) [clock seconds]
set run(start_time) [clock format $run(start_sec)]
set run(stop_sec) $run(start_sec) 
set run(stop_time) $run(start_time)
set run(how_start_I) automatic

set rstat(htots) "12345 678910  2342 235425 34"
set rstat(nhist) "8"
set rstat(stots) "12345 678910"
set rstat(srates) "3.14159 2.718"

set idef(setup_path) {}
set idef(ggl_dac_range) 1

