#  $Log: hmutil_extra.tcl,v $
#  Revision 1.25  2015/05/18 21:15:06  asnd
#  Proper backgrounding of exec
#
#  Revision 1.24  2015/05/10 03:14:59  asnd
#  Handle client windows in screen sessions; no more seyon.
#
#  Revision 1.23  2015/04/18 03:32:24  asnd
#  Improve monitoring of frontend startup.
#
#  Revision 1.22  2015/03/20 00:49:11  suz
#  changes by Donald. This version part of package for new VMIC frontends
#
#  Revision 1.21  2008/04/30 01:56:24  asnd
#  Convert to bnmr-style /autorun parameters in odb
#
#  Revision 1.20  2007/10/03 02:36:25  asnd
#  Camp access uses emissary sub-server
#
#  Revision 1.19  2006/09/22 03:10:46  asnd
#  Various small patches.
#
#  Revision 1.18  2006/07/08 01:27:22  asnd
#  Configurable mu character for window titles, to handle bug with versions in Sci Linux.
#
#  Revision 1.17  2006/06/29 07:29:57  asnd
#  Use safer p_win_name (from mui_utils.tcl) for dialogs
#
#  Revision 1.16  2005/06/22 15:23:08  asnd
#  Don't close and re-open seyon during reboot
#
#  Revision 1.15  2004/11/07 08:24:08  asnd
#  Rewrite client checks; watch for multiple tasks.  Allow FE reboot during
#  unattended autoruns & deal with run termination.
#
#  Revision 1.14  2004/04/21 05:14:29  asnd
#  Prevent double-reboot when change experiment
#
#  Revision 1.13  2004/03/11 05:17:39  asnd
#  Wording
#
#  Revision 1.12  2004/02/20 23:16:12  asnd
#  Restore line accidently removed in last edit.
#
#  Revision 1.11  2004/02/20 23:08:06  asnd
#  Single musr Midas experiment.  Handle year changes.
#
#  Revision 1.10  2003/10/29 16:16:38  asnd
#  Better feedback during FE reboot
#
#  Revision 1.9  2003/10/29 14:52:14  asnd
#  Recognize failure of Telnet connection
#
#  Revision 1.8  2003/09/19 06:23:26  asnd
#  ImuSR and many other revisions.
#
#  Revision 1.7  2002/11/17 09:53:44  asnd
#  Improve reboot sequencing
#
#  Revision 1.6  2002/11/14 00:06:42  asnd
#  Close ps pipe when done
#
#  Revision 1.5  2002/09/06 03:29:07  asnd
#  Break reboot sequence up to let gui stay alive during waits.
#  Alter timings and appearance.
#
#  Revision 1.4  2002/07/27 06:31:58  asnd
#  Periodic check on midas clients
#
#  Revision 1.3  2002/07/26 06:21:28  asnd
#  First working version for rebooting HMVW computer
#
#  Revision 1.2  2002/07/23 23:51:09  asnd
#  HM active display uses 1/0, not y/n
#
#  Revision 1.1  2002/07/23 09:41:00  asnd
#  Add HMVW utilities window
#
#

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

global hmuwin

if { [llength [info command winfo]] == 0 } {
    set hmuwin none
} elseif { ! [info exists hmuwin] } {
    set hmuwin ""
    source [file join $muisrc hmutil.ui.tcl]
}

# Fire up the hm utility window. When used for real, hmu_initialize is invoked by
# menus.  When testing the gui it can be invoked at the end of this source file.

proc hmu_initialize { } {
    global hmuwin midas

    set hmuw [td_win_name $hmuwin]
    if { [ string length $hmuwin ] } {
        # We are in a real application, where the window is not main
        if { [winfo exists $hmuwin.hu_head_l] } {
            # window already exists: make it visible
            wm deiconify $hmuw
            raise $hmuw
        } else {
            # window is not built yet. 
            toplevel $hmuwin
            wm title $hmuw "${PREF::muchar}SR HM Utility"
            hmutil_ui $hmuwin
        }
    }

    hmu_bind_help hm_disp_start_b "Start active events display on HMVW console"
    hmu_bind_help hm_disp_stop_b "Stop events display on HM console"
    hmu_bind_help hm_disp_ref_b "Refresh events display on HM console"
    hmu_bind_help hm_clear_b "Clear any clock hang-up"
    hmu_bind_help hm_restart_b "Restart the HM frontend"
    hmu_bind_help hm_quit_b "Quit the HM utilities"

    bind $hmuw <Alt-t> "$hmuwin.hm_disp_start_b invoke"
    bind $hmuw <Alt-p> "$hmuwin.hm_disp_stop_b invoke"
    bind $hmuw <Alt-r> "$hmuwin.hm_disp_ref_b invoke"
    bind $hmuw <Alt-e> "$hmuwin.hm_clear_b invoke"
    bind $hmuw <Alt-b> "$hmuwin.hm_restart_b invoke"
    bind $hmuw <Alt-q> "$hmuwin.hm_quit_b invoke ; break"

    hmu_bind_help hm_cons_open_b "Open a console window for the HM frontend"
    hmu_bind_help hm_cons_close_b "Close the HM console window"
    bind $hmuw <Alt-o> "$hmuwin.hm_cons_open_b invoke"
    bind $hmuw <Alt-l> "$hmuwin.hm_cons_close_b invoke"
    bind $hmuw <Alt-g> "$hmuwin.hm_cons_grab_b invoke"
}

#  THE HELP LINE:
#
#  Many widgets have a binding for mouse-entry and mouse-exit where some
#  help text is displayed while the cursor is on that widget.

proc hmu_bind_help { w h } {
    global hmuwin
    bind $hmuwin.$w <Enter> [list set hmu_help_text $h]
    bind $hmuwin.$w <FocusIn> [list set hmu_help_text $h]
    bind $hmuwin.$w <Leave> [list set hmu_help_text {}]
    bind $hmuwin.$w <FocusOut> [list set hmu_help_text {}]
}

# Quit the hm util page
proc hm_quit { } {
    global hmuwin
    destroy [td_win_name $hmuwin]
}

# Open an xterm with a screen-view of the VMIC frontend.
proc hm_cons_open {} {
    global env midas 
    #puts "exec $env(MUSR_DIR)/musr_midas/bin/show-windows vmic &"
    catch { exec $env(MUSR_DIR)/musr_midas/bin/show-windows vmic &}
}

# Close any existing 
proc hm_cons_close {} {
    global midas 
    set status [catch {exec ssh -x $midas(hm_host) /usr/bin/screen -d $midas(expt)_LXFE} msg]
    # Maybe do something with status and msg
}

# Unnecessary now because "open" will grab all by itself...
proc hm_cons_grab {} {
    hm_cons_close
    hm_cons_open
}

global hmutil
set hmutil(display_active) 1

proc hm_disp_start {} {
    global hmutil
    set hmutil(display_active) 1
    td_odb {odb_hmdisplay 1} {Enable HM Display}
}

proc hm_disp_stop {} {
    global hmutil
    set hmutil(display_active) 0
    td_odb {odb_hmdisplay 0} {Disable HM Display}
}

proc hm_disp_ref {} {
    global hmutil
    td_odb [list odb_hmdisplay $hmutil(display_active)] {Refresh HM Display}
}

proc hm_clear_hang {} {
    td_odb odb_tdc_clear {Clear clock hang}
}

#  Re-starting the frontend processor.
#  For a vxworks frontend, we reboot to start the frontend programs.

#  hm_restart_hm services the {Restart HM FE} button on the HM Utils page.

proc hm_restart_hm { } {
    global run hmuwin midas

    if { [string equal $midas(expt) "musr"] } { 
        set text {Do you really want to reboot the Histogramming Memory computer?}
    } else {
        set text {Do you really want to restart the Histogramming Memory frontend client?}
    }
    if { $run(in_progress) } {
        append text "\nRun $run(number) will be forcibly terminated."
    }
    set midas(clt_defer) [expr {[clock seconds] + 50}]
    if { [string compare [ timed_messageBox -timeout 30000 \
            -type okcancel -default cancel -icon question -parent [p_win_name $hmuwin] \
            -title "Confirm HM Restart" -message $text ] "ok" ] } {
        return
    }
    if { $run(in_progress) } {
        odb_abrupt_stop
        after 10000 [list mui_cleanup_for_restart end $run(number)]
    }

    hm_do_restart 
}

# hm_do_restart is invoked by mui_query_restart (which is invoked by client checks,
# sometimes triggered by odb_set_exper_type [mui_set_experiment]) or by hm_restart_hm
# (the button on the hm utils page).

proc hm_do_restart { } {
    global midas hmutil hmuwin run

    set hmutil(message) {}

    if { [string equal $midas(expt) "musr"] } { 
        hm_popup_mes "Attempt to re-start frontend $midas(hm_host)..."
    } else {
        hm_popup_mes "Re-starting frontend process..."
    }
  
    catch {
        odb_get_runinfo
        if { $run(in_progress) } {
            if { $run(transition) } {
                odb_cancel_transition
            }
        }
    }
#   Stop the automatic events display, and wait for the final display
#   to complete
    hm_disp_stop
    after 100 hm_do_restart1
}

proc hm_do_restart_vmic { {iter 0} } {
    global env midas

    if { $iter > 11 } {
        hm_popup_mes "\nCould not connect with frontend host $midas(hm_host)!"
        after 10 hm_watch_restart 100
        return
    }
    if { [catch { exec ping -c1 -W1 $midas(hm_host) } msg] } {
        if { $iter % 2 == 0 } {
            hm_popup_mes "\nFrontend host $midas(hm_host) not responding to ping (yet)..."
        }
        if { $iter == 0 } {
            hm_popup_mes "\nMaybe it is in the process of rebooting."
        }
        after 5000 [list hm_do_restart_vmic [incr iter]]
        return
    }
    hm_popup_mes "\nExecute start_fe script."
    if { [catch { exec $env(MUSR_DIR)/musr_midas/bin/start_fe &} msg ] } { 
        hm_popup_mes "\nError from start_fe script.\nInformation: $::errorInfo"
        after 5000 [list hm_do_restart_vmic [expr {$iter < 11 ? 11 : $iter+1}]]
        return
    }

    #set infile [open $tempFileName r]
    #while { [gets $infile line] >= 0 } {
    #	hm_popup_mes "\n mtdfe: [string trim $line]"
    #}
    #close $infile

    hm_popup_mes "\nWait for frontend to connect."
    after 500 hm_watch_restart 12
    
}

#  For Linux VMIC, we just execute the restart script, and wait for a reconnect.
#  For vxworks PPC, we send the string "reboot" to the front-end computer via telnet.
#  Actually, we ignore telnet protocols!  We just open a socket and write to that!

proc hm_do_restart1 { } {
    global midas

    if { [string length $midas(hm_host)] == 0 } {
        hm_popup_mes "\nThere is no frontend processor host computer defined!"
        return
    }
    #
    #   odb shutdown frontend task (don't wait for it to finish because it might take long)
    set clist [mui_get_midas_clients]

    set midas(clt_defer) [expr {[clock seconds] + 50 }]
    set af [ after 3000 { set midas(clt_stat) {timeout 666 666}} ]
    foreach clt [list femusr fev680] {
        if { [mui_check_a_client cll msg $clt $clist {} 0] != 2 } {
            #   Found client in Midas list, so shut it down.
            foreach c $cll {
                set midas(clt_stat) {Booting 666 666} 
                hm_popup_mes "\nShutting down front-end client $clt ($c)"
                catch { blt::bgexec midas(clt_stat) \
                        odb -h $midas(host) -e $midas(expt) -c "shutdown $c" }
            }
        }
    }
#   Clean up, assuming it is now fixed.
    catch { blt::bgexec midas(clt_stat) \
            odb -h $midas(host) -e $midas(expt) -c "cleanup"
    }
    after cancel $af

#   Prevent client checking while shut down:
    set midas(clt_checked_at) [expr {[clock seconds] + 70}]

    if { [string equal $midas(expt) "musr"] } {#  Older experiment with PPC (needs reboot) 
        after 200
#
#       Try giving the reboot command via telnet
#
        hm_popup_mes "\nTry sending \"reboot\" via Telnet..."
        if { ! [ catch { hm_telnet_cmd $midas(hm_host) "reboot" } status ] } {
            hm_popup_mes "\nThe $midas(hm_host) computer should be rebooting now.\nPlease wait until it finishes."
            hm_cons_open
            after 4000 hm_watch_restart
            return
        } elseif { $status == "System engaged" } {
            hm_popup_mes "\nSystem console is locked by a Telnet connection."
            hm_popup_mes "\nYou will have to type \"reboot\" in that telnet window,\nor push the Reset button on $midas(hm_host)"
            return
        }
        hm_popup_mes "\nFailed: $status."

        #   Failed reboot by telnet, so try the serial port
        hm_popup_mes "\nTry rebooting via serial port..."
        #   Try to free-up the serial port, stop display, and wait for a refresh to get done
        catch { hm_disp_stop ; hm_cons_close }
        after 1500 hm_do_reboot2

    } else {#  Newer experiment with VMIC

        hm_do_restart_vmic

    }
}

proc hm_do_reboot2 { } {
#   Open serial port for reading/writing
    if { [catch { open /dev/modem r+ } port] } {
        hm_popup_mes "\nFailed to connect to serial port: $port"
        hm_do_reboot_err
    } else {
        fconfigure $port -buffering none -blocking 0 -mode 9600,n,8,1
        # hm_popup_mes "\nConnected to serial port: $port"
        hm_read_data $port
        after 50 [list puts $port {whoami}]
        after 500 [list hm_do_reboot3 $port]
    }
}

proc hm_do_reboot3 { port } {
    set iam [hm_read_data $port]
    #puts "I am $iam on hmvw"
    if { [string first midas $iam] == -1 } {
	if { [string first system $iam] == -1 } {
	    hm_popup_mes "\nNo response to query on the serial port.\nPlease check the blue switch box.\n(Will try blindly in 10 seconds.)"
	    after 10000 [list hm_do_reboot4 $port {}]
	} else {
	    hm_popup_mes "\nThe serial port is not connected to $midas(hm_host).\nCheck the blue switch box and try again."
	    close $port
	    hm_do_reboot_err
	}
    } else {
	after 1000 [list hm_do_reboot4 $port "reboot"]
    }
}

proc hm_do_reboot4 { port cmd } {
    puts $port $cmd
    close $port
    hm_popup_mes "\nThe VW frontend processor should be rebooting now.\nPlease wait until it finishes."
    hm_cons_open
    after 5000 hm_watch_reboot
}

proc hm_do_reboot_err {} {
    global midas
    hm_popup_mes "\nAutomatic reboot failed.\nTry typing Control-X in the "
    hm_popup_mes "$midas(hm_host) console window, or you may need to push "
    hm_popup_mes "the Reset button on $midas(hm_host)."
    set midas(clt_defer) 0
    hm_cons_open
}

proc hm_telnet_cmd { server cmd } {
    if { [catch { socket $server 23 } sock] } {
        return -code error "Failed to connect to ${server}: $sock"
    }
    #puts "Connected to $server on port 23"
    fconfigure $sock -buffering none -blocking 0
    puts $sock ""
    after 100
    set response [hm_read_data $sock]
    if { [string first {this system is engaged} $response] < 0 } {
        after 200 "puts $sock \{$cmd\} ; after 800 \{close $sock\}"
    } else {
        close $sock
        return -code error "System engaged"
    }
}

proc hm_read_data { sock } {
    set data x
    set prev {}
    while {[string length $data]} {
        #puts "hm_read_data reads $data"
        set prev $data
        set data [read $sock 4096]
        if {[eof $sock]} {
            set data {}
        }
    }
    #puts "hm_read_data returns $prev"
    return $prev
}

if { [info commands winfo] == "" } {

    proc hm_popup_mes { text } {
        puts $text
    }

} else {

    proc hm_popup_mes { text } {
        global hmutil midas
        catch { after cancel $hmutil(popup_end_id) }

        if { [winfo exists .hmupopup] } {
            raise .hmupopup
        } else {
            toplevel .hmupopup
            set hmutil(message) ""
            wm title .hmupopup {Restart HM}
            label .hmupopup.text -anchor nw -height 15 -justify left -padx 7 -pady 7 -textvariable hmutil(message)
            label .hmupopup.strut -padx 7 -pady 0 -width 50 -height 0 -text {}
            pack .hmupopup.text .hmupopup.strut -anchor nw -in .hmupopup -expand yes -fill both
        }
        append hmutil(message) $text
        update idletasks
        set hmutil(popup_end_id) [after 20000 hm_restart_done]
    }
}

proc hm_restart_done { } {
    global midas
    catch { destroy .hmupopup }
    set midas(clt_checked_at) 0
    set midas(clt_defer) 0
    #puts "Perform post-restart immediate client check"
    after 300 mui_check_clients
}

#   This procedure is scheduled every 2 seconds to watch for the frontend
#   re-appearing after a restart.  
proc hm_watch_restart { {iter 0} } {
    global midas run
    after cancel $midas(wait4fe_id)
    set clist [mui_get_midas_clients]
    set feclient [lindex [list fev680 femusr] $run(ImuSR)]
    # puts "Watch restart:"
    if { [mui_check_a_client clmat msg $feclient $clist {} 0] } {
        if { [incr iter] > 25 } {
            hm_popup_mes "\nRestart has not completed.  Something is wrong."
            hm_popup_mes "\nTry typing start-all."
            hm_popup_mes "\nIf that fails, push RESET on the $midas(hm_host) computer\nin the VME crate."
        } else {
            hm_popup_mes "."
            set midas(wait4fe_id) [after 2000 [list hm_watch_restart $iter]]
        }
    } else {
        # frontend has reappeared!
        #puts "Front-end $midas(hm_host) has reconnected"
        hm_popup_mes "\nFront-end $midas(hm_host) has reconnected"
        after 4000 { hm_restart_done }
    }
}

set midas(wait4fe_id) {}

if { $hmuwin == "" } {
    # stand-alone testing, so start immediately!
    hmu_initialize
}

