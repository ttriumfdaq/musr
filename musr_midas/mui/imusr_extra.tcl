#   imusr_extra.tcl
#   Procedures for the main mui window taking ImuSR runs and for the
#   quick-setup window

#   $Log: imusr_extra.tcl,v $
#   Revision 1.20  2015/09/28 00:47:06  asnd
#   Hide disabled "Logbook"
#
#   Revision 1.19  2009/05/21 05:21:59  asnd
#   Fix: define ins variable for init script
#
#   Revision 1.18  2008/10/29 06:15:08  asnd
#   Earlier and clearer sweep-device checking
#
#   Revision 1.17  2007/10/03 02:20:04  asnd
#   Alter validation in imusr setup (off when disabled/readonly)
#
#   Revision 1.16  2006/07/08 01:27:22  asnd
#   Configurable mu character for window titles, to handle bug with versions in Sci Linux.
#
#   Revision 1.15  2006/06/29 07:29:57  asnd
#   Use safer p_win_name (from mui_utils.tcl) for dialogs
#
#   Revision 1.14  2006/05/26 16:47:49  asnd
#   Accumulated changes over winter shutdown.  Notably changes to how changes are tracked on the ImuSR setup pages
#
#   Revision 1.13  2005/06/22 15:30:32  asnd
#   preferences for confirmation, monitor renormalization
#
#   Revision 1.12  2004/08/05 07:34:15  asnd
#   Fix paste-o in removal of variable traces
#
#   Revision 1.11  2004/08/05 07:04:59  asnd
#   Fix "change modulation". Improve interaction between quick and full setup windows.
#
#   Revision 1.10  2004/05/18 00:49:39  asnd
#   Fix start-when-ready and fix a "\ " typo
#
#   Revision 1.9  2004/04/15 02:59:44  asnd
#   Enable ImuSR "end run after..." features
#
#   Revision 1.8  2004/02/20 23:08:06  asnd
#   Single musr Midas experiment.  Handle year changes.
#
#   Revision 1.7  2003/11/24 05:43:26  asnd
#   Handle year changes.
#
#   Revision 1.6  2003/11/04 16:02:24  asnd
#   Tearoff run menu labelling
#
#   Revision 1.5  2003/10/29 14:54:52  asnd
#   Proper hiding vs disabling of button.
#
#   Revision 1.4  2003/10/15 09:28:28  asnd
#   Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#   Revision 1.3  2003/10/01 09:56:12  asnd
#   Minor changes (use "other" instead of "field" for field sweeps)
#
#   Revision 1.2  2003/09/26 00:39:03  asnd
#   Recent gui changes (sweep details, ggl pulser...)
#
#   Revision 1.1  2003/09/19 06:23:26  asnd
#   ImuSR and many other revisions.
#



if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

# Build the (context-sensitive) RUN menu, and also handle 
# activation/deactivation of the "Write Data" button (it is really 
# a short-cut to the menu item).
proc imusr_make_run_menu { } {
    global tdwin run idef

    $tdwin.td_run_mb.menu delete 0 last
    if { $PREF::menutearoff } { run_tearoff_title }

    set writeb_shown [string equal raised [$tdwin.td_write_b cget -relief]]
    set writeb_show 0
    set renormb_shown [string equal raised [$tdwin.td_renorm_b cget -relief]]
    set renormb_show 0

    if { ! $run(in_progress) } {

        $tdwin.td_run_mb.menu add command -label "Begin run" \
                -command {imusr_run begin} -underline 0 -state active

        $tdwin.td_run_mb.menu add separator

    } else {

        set writeb_show 1
        $tdwin.td_run_mb.menu add command -label "Write data" \
                -command {td_run write} -underline 0
        $tdwin.td_run_mb.menu add separator

        $tdwin.td_run_mb.menu add cascade -label "End run ..." \
                -menu $tdwin.td_run_mb.menu.endI -underline 0 -state active
        $tdwin.td_run_mb.menu.endI configure -tearoff 0

        $tdwin.td_run_mb.menu.endI delete 0 last
        $tdwin.td_run_mb.menu.endI add command -label "Immediately" \
                -command {imusr_run end} -underline 0
        $tdwin.td_run_mb.menu.endI add command -label "Finish this point" \
                -command {imusr_run endafterpoint} -underline 0
        $tdwin.td_run_mb.menu.endI add command -label "After this Sweep" \
                -command {imusr_run endaftersweep} -underline 0

        if { $run(paused) } {
            $tdwin.td_run_mb.menu add command -label "Continue" \
                    -command {imusr_run continue} -underline 0
        } else {
            $tdwin.td_run_mb.menu add command -label "Pause run" \
                    -command {imusr_run pause} -underline 0
        }
        $tdwin.td_run_mb.menu add command -label "Kill run" \
                -command {imusr_run kill} -underline 0

        $tdwin.td_run_mb.menu add command -label "Abrupt Stop" \
                -command {imusr_run quickstop}

        $tdwin.td_run_mb.menu add separator

	if { $idef(toler_enable) } {

            set renormb_show 1
	    $tdwin.td_run_mb.menu add command -label "Renormalize rate" \
                -command toler_renormalize -underline 0
	    $tdwin.td_run_mb.menu add command -label "Set Tolerance" \
                -command {iq_initialize toler} -underline 4
        }
        $tdwin.td_run_mb.menu add command -label "Set Scan range" \
                -command {iq_initialize sweep} -underline 4
    }

    $tdwin.td_run_mb.menu add command -label "Modify Titles" \
            -command titles_begin_change -underline 0

#   $tdwin.td_run_mb.menu add command -label "Logbook" -command {} -underline 0 -state disabled

    $tdwin.td_run_mb.menu add separator

    if { $run(test_mode) } {
        $tdwin.td_run_mb.menu add command -label "Acquire Real data" \
                -command {td_testmode real} -underline 0
    } else {
        $tdwin.td_run_mb.menu add command -label "Acquire Test data" \
                -command {td_testmode test} -underline 0
    }

    # Now enable/disable the "Write Data" and "Renormalize" buttons as appropriate
    if { $writeb_shown != $writeb_show } {
        if { $writeb_show } {
            showButton $tdwin.td_write_b -disabledforeground $::PREF::disabledfg
            td_bind_help td_write_b "Write data to disk, and archive"
        } else {
            hideButton $tdwin.td_write_b -disabledforeground $::PREF::bgcol
            td_bind_help td_write_b ""
        }
    }
    if { $renormb_shown != $renormb_show } {
        if { $renormb_show } {
            showButton $tdwin.td_renorm_b
            td_bind_help td_renorm_b "Renormalize rate now"
        } else {
            hideButton $tdwin.td_renorm_b
            td_bind_help td_renorm_b ""
        }
    }
}


proc iq_initialize { args } {
    global iqwin run midef

    set iqw [td_win_name $iqwin]

    #puts "Initializing quick setup for $args"

    # Flag to say whether we add the indicated panel(s) or
    # display only the indicated panels, removing old.  When
    # we create the window, all panels are displayed by default,
    # so we flag "only" then
    set only 0

    if { [ string length $iqwin ] } {
        # We are in a real application,
        if { [winfo exists $iqw] } {
            wm deiconify $iqw
            raise $iqw
        } else {
            toplevel $iqw
            iquick_ui $iqwin
	    set only 1
        }
    }

    # Bind help
    iq_bind_help iq_swfrom_e "Start point of (first) scan"
    iq_bind_help iq_swto_e "End point of scan"
    iq_bind_help iq_incr_e "Increment between scan points"
    iq_bind_help iq_numsw_e "Number of scans per run (0 for unlimited)"
    iq_bind_help iq_num1_e "Number of toggle cycles for main (inner) modulation"
    iq_bind_help iq_settle1_e "Delay, in ms, after each inner toggle"
    iq_bind_help iq_num2_e "Number of cycles for secondary (slower) modulation"
    iq_bind_help iq_settle2_e "Delay, in ms, after each outer toggle"
    iq_bind_help iq_fastmod_cb "Check to enable fast asynchronous modulation"
    iq_bind_help iq_counts_e "Number of counts (or ticks) to count down on preset scaler"
    iq_bind_help iq_numpre_e "Repetitions of preset-count-down per toggle"
    iq_bind_help iq_toler_cb "Check to pause acquisition when beam out of tolerance"
    iq_bind_help iq_toler_e  "Percentage tolerance for beam-rate fluctuations"
    iq_bind_help iq_renorm_b "Reset tolerance range to present beam rate"
    iq_bind_help iq_toldelay_e "Delay (seconds) after coming back into tolerance"
    iq_bind_help iq_manual_rb "Make acquisition begin when manually un-paused"
    iq_bind_help iq_automatic_rb "Acquisition will start when sweep device reaches start point"
    iq_bind_help iq_immediate_rb "Acquisition will begin immediately at start (click OK)"
    iq_bind_help iq_details_b "Configure all acquisition parameters"
    iq_bind_help iq_quit_b "Cancel quick settings"

    wm title $iqw "I-${PREF::muchar}SR Quick Set"
    bind $iqw <Alt-d> [list $iqwin.iq_details_b invoke]
    bind $iqw <Alt-q> [list $iqwin.iq_quit_b invoke]
    bind $iqw <Alt-o> [list $iqwin.iq_OK_b invoke]
    bind $iqw <Alt-k> [list $iqwin.iq_OK_b invoke]
    focus $iqwin.iq_OK_b

    idef_do_update
    # stabilize displayed values, as if the user is typing. 
    # set midef(changed_at) [clock seconds]

    # Check for "-only" keyword amongst the panel names
    if { [ set i [lsearch -exact $args "-only"]] >=0 } {
	set only 1
	set args [lreplace $args $i $i]
    }

    # Now enable the listed panels (perhaps disable others)
    foreach frame [list toler mod sweep preset start] {
	if { [lsearch -exact $args $frame] >= 0 } {
	    grid $iqwin.iq_${frame}_f
            switch -- $frame {
                start {
                    bind $iqw <Alt-l> [list $iqwin.iq_manual_rb invoke]
                    bind $iqw <Alt-a> [list $iqwin.iq_automatic_rb invoke]
                    bind $iqw <Alt-i> [list $iqwin.iq_immediate_rb invoke]
                    iq_bind_help iq_OK_b "Approve settings - Start run"
                    $iqwin.iq_OK_b configure -text "OK Start" -width 0
		    set run(starting) 2
                }
                mod {
                    bind $iqw <Alt-f> [list $iqwin.iq_fastmod_cb invoke]
                    # For this, we do some tricky synchronization because having spurious
                    # modulation parameters shown is too confusing.  Adjust display when
                    # variables change.  Doing this with traces on a variable (midef) that
                    # gets set so often is BAD. Plan to change it !???!
                    iq_mod_pane_adjust 1 midef mod_type_1 w
                    iq_mod_pane_adjust 2 midef mod_type_2 w
                    #trace variable midef(mod_type_1) w {iq_mod_pane_adjust 1}
                    trace variable midef(mod_num_1) w {iq_mod_pane_adjust 1}
                    #trace variable midef(mod_type_2) w {iq_mod_pane_adjust 2}
                    trace variable midef(mod_num_2) w {iq_mod_pane_adjust 2}
                }
                toler {
                    bind $iqw <Alt-e> [list $iqwin.iq_toler_cb invoke]
                    bind $iqw <Alt-r> [list $iqwin.iq_renorm_b invoke]
                }
            }
	} elseif { $only } {
	    grid remove $iqwin.iq_${frame}_f
            switch -- $frame {
                start {
                    bind $iqw <Alt-l> {}
                    bind $iqw <Alt-a> {}
                    bind $iqw <Alt-i> {}
                    iq_bind_help iq_OK_b "Approve settings"
                    $iqwin.iq_OK_b configure -text "OK" -width 7
                }
                mod {
                    bind $iqw <Alt-f> {}
                }
                toler {
                    bind $iqw <Alt-e> {}
                    bind $iqw <Alt-r> {}
                }
            }
	}
    }
}

#  Adjust the appearance of the quick modulation parameters panel, based on
#  the current modulation parameters.  Also propagate and synchronize between
#  the modulation type and the number of modulations (the number has primitive
#  control over the type in this window).  It is further complicated by the
#  desire to synchronize with the "define everything" panel (idefwin).
#  This is usually invoked as a variable trace.

proc iq_mod_pane_adjust { mnum var elem op } {
    global iqwin midef idef idefwin
    if { [winfo exists $iqwin.iq_mod_f] } {
        # puts "Pane adjust for modulation $mnum element $elem"
        set n 0
        catch { scan $midef(mod_num_$mnum) %d n }
        # Propagate values from one variable to the other:
        switch -glob -- $elem {
            mod_type_* {
                if { [string match -nocase "None" $midef($elem)] } {
                    set n 0
                } else {
                    set n [bounded $idef(mod_num_$mnum) 1 1000]
                }
                set midef(mod_num_$mnum) $n
            }
            default {
                if { [string length $midef(mod_num_$mnum) ] == 0 } { return }
                if { $n > 0 } {
                    if { [string match -nocase "None" $midef(mod_type_$mnum)] } {
                        set midef(mod_type_$mnum) $idef(mod_type_$mnum)
                    }
                    if { [string match -nocase "None" $midef(mod_type_$mnum)] } {
                        set midef(mod_type_$mnum) "Hard"
                    }
                } elseif { $n == 0 && ![string match -nocase "None" $midef(mod_type_$mnum)] } {
                    set midef(mod_type_$mnum) "None"
                }
            }
        }
        # Then handle widget display
        if { $n < 1 } {
            #puts "Disable fields for modulation $mnum"
            configEntry $iqwin.iq_value${mnum}_e disabled
            $iqwin.iq_value${mnum}_l configure -state disabled
            configEntry $iqwin.iq_settle${mnum}_e disabled
            $iqwin.iq_settle${mnum}_l configure -state disabled
            $iqwin.iq_ms${mnum}_l configure -state disabled
        } else {
            configEntry $iqwin.iq_settle${mnum}_e normal
            $iqwin.iq_settle${mnum}_l configure -state normal
            $iqwin.iq_ms${mnum}_l configure -state normal
            switch -- [string tolower $midef(mod_type_$mnum)] {
                soft -
                ref {
                    #puts "Enable all fields for modulation $mnum"
                    configEntry $iqwin.iq_value${mnum}_e normal
                    $iqwin.iq_value${mnum}_l configure -state normal
                }
                default {
                    #puts "Enable settle fields for modulation $mnum"
                    configEntry $iqwin.iq_value${mnum}_e disabled
                    $iqwin.iq_value${mnum}_l configure -state disabled
                }
            }
        }
        if { [winfo exists $idefwin.tabs] } { 
            idef_propagate 
        }
    } else { # Window is gone, so remove traces.
        #puts "Remove traces"
        #trace vdelete midef(mod_type_1) w {iq_mod_pane_adjust 1}
        trace vdelete midef(mod_num_1) w {iq_mod_pane_adjust 1}
        #trace vdelete midef(mod_type_2) w {iq_mod_pane_adjust 2}
        trace vdelete midef(mod_num_2) w {iq_mod_pane_adjust 2}
    }
}

#  Procedure for the OK (alias "Start") button -- apply the parameters,
#  and, sometimes start the run (more of a pre-start).
proc iquick_OK { } {
    global iqwin defwin run idef
    if { [idef_apply] } { return }
    destroy [td_win_name $iqwin]
    if { $run(starting)==2 } {
	imusr_run start
    }
}

#  Bring up the "define everything" window, but retaining any changes we have
#  already made in the little window.  Also, bring up the right tab, if there
#  is just one.
proc iquick_detailed { } {
    global iqwin
    idef_declare_changed
    set tabs [list]
    foreach frame [list sweep mod preset toler] tab [list sweeps toggles presets toler] {
	if { [winfo exists $iqwin.iq_${frame}_f] && [winfo ismapped $iqwin.iq_${frame}_f] } {
            lappend tabs $tab
        }
    }
    isetup_initialize $tabs
}

proc iquick_cancel { } {
    global iqwin idefwin run midef

    if { [winfo exists [td_win_name $iqwin]] } {
        destroy [td_win_name $iqwin]
    }
    if { ![winfo exists $idefwin] } {
        set midef(changed_at) 0
    }
    if { $run(starting)==2 || $run(starting)==3 } {
        set run(starting) 0
    }
    titles_end_change
    after 0 after idle {td_background_update}
}


#  THE HELP LINE:

proc iq_bind_help { w h } {
    global iqwin
    bind $iqwin.$w <Enter>    [list set iquick_help_text "$h"]
    bind $iqwin.$w <FocusIn>  [list set iquick_help_text "$h"]
    bind $iqwin.$w <Leave>    [list set iquick_help_text {}]
    bind $iqwin.$w <FocusOut> [list set iquick_help_text {}]
}

#  Perform I-muSR run control.  The parameter ("command") is one of the 
#  run-control actions: 
#  begin start end endafterpoint endaftersweep kill write pause continue
#
#  The start-up is in two phases: begin and start 
#
# At menu-begin of run, "begin":
#   Apply setup parameters
#   check parameters for validity
#   get run number for next run
#   Pop-up all quick-set panels
#
# At [OK] button from quick setup "start":
#   Apply parameters
#   check parameters for validity
#   substitute init script and toler-test script (ins and val)
#   execute init script
#   poll for tolerance
#   start run (in active state)
#   if selected : "start immediately" then un-pause
#               : "start automatically" then start polling with toler-test
#               : "wait to start" then nil (or pop up a message box to continue?)

proc imusr_run { command } {
    global tdwin run midas rtitles idef midef idefwin

    if { [mui_check_transition] } { return }

    #puts "imusr_run $command "
    switch -- $command {
	begin {
            #  If display causes timeouts:    odb_hmdisplay 0
	    while { $midef(changed_at) } {
                beep
                switch [ timed_messageBox -timeout 20000 -type yesnocancel -default yes \
                             -message [join {
                                 "I-\u00b5SR acquisition parameters are being changed, but not yet applied."
                                 "Apply changes (yes/no) or cancel begin run?"
                             } "\n"] \
                             -title "Begin Run - apply" -icon question ] {
                    yes {# Confirm settings
                        if { [idef_apply] } { return }
                    }
                    no {# Leave changing
                        break
                    }
                    cancel {# Cancel run begin
                        return
                    }
                }
            }
	    odb_get_general
	    odb_get_runinfo
            #   If display causes timeouts:   odb_hmdisplay 0
            idef_do_update
            if { $run(in_progress) } {
                beep
                timed_messageBox -timeout 30000 -type ok \
		    -message "Cannot start run: A run is already in progress!" \
		    -icon error -title "Already Running"
                return
            }

            if { $midas(clt_checked_at) < [clock seconds] - 30 && ![winfo exists .hmupopup] } {
                set midas(clt_checked_at) 0
                mui_check_clients
            }

            if { $run(confirm_stop) >= 3 && $run(test_mode) } {
                beep
                if { [timed_messageBox -timeout 25000 -icon question -type yesno -default yes \
                          -message "Begin a test run, where data is not archived?" -title "Confirm test" ] == "no" } {
                    return
                }
            }

            mui_check_year

            # Restore parameters used to end run asynchronously.
            imusr_monitor_end

	    if { [string match -nocase Camp $idef(sweep_device)] } {
		#puts "Look for camp sweep device and put it online"
		if { ! [ imusr_camp_dev_ready $idef(sweep_ins_name) ] } { 
                    isetup_initialize sweeps
		    return
		}
	    }

	    set run(starting) 2
	    if { !$rtitles(changing) } { titles_begin_change }
	    odb_get_runnum
	    iq_initialize toler mod sweep preset start
	    return
	}
	start {
            #puts "start how: $run(how_start_I)"
	    set run(starting) 3
	    if { $rtitles(changing) } { 
                imusr_set_runtitle
            } else {
                titles_begin_change
            }

	    set idef(full_tol_test) ""

            odb_get_general
            odb_get_runinfo
            idef_do_update 0

            # For failure returns, want to cancel startup
	    set run(starting) 0
            set run(bg_start_out) ""

            # Check sweep range against camp-variables for min/max, if any for the device.
            set stat [imusr_check_sweep_limits]
            if { ! [string equal $stat "OK"] } {
                beep
                switch -- [ timed_messageBox -timeout 30000 -icon error \
			-message "$stat\nPlease check settings." \
			-type okcancel -default ok -title "Bad sweep settings" ] {
                    ok { after 1 { imusr_run begin } }
                }
                return
            }

	    # Lots of pre-processing for camp sweep devices:

	    if { [string match -nocase Camp $idef(sweep_device)] } {
		#puts "Look for camp sweep device \"$idef(sweep_ins_name)\" and put it online"
		if { ! [ imusr_camp_dev_ready $idef(sweep_ins_name) ] } {
                    isetup_initialize sweeps
		    return
		}
		# determine idef(sweep_var_type)
		set stat [catch {mui_camp_cmd "varGetVal $idef(sweep_camp_var)"} v]
		if { $stat == 0 } {
		    if { [string is integer -strict $v] } { 
			set idef(sweep_var_type) integer
		    } elseif { [string is double -strict $v] } {
			set idef(sweep_var_type) double
		    } else {
			set stat 1
		    }
		}
		if { $stat } {
                    beep
                    if { [ string length $midas(camp_host) ] && [ string compare "none" $midas(camp_host) ]} {
                        switch -- [timed_messageBox -timeout 30000 -icon error \
                                -message "Sweep variable $idef(sweep_camp_var) is not working:\n$v\nPlease fix sweep device." \
                                -type okcancel -default ok -title "Fix sweep device"] {
                            ok {
                                isetup_initialize sweeps
                            }
                        }
                    } else {
                        switch -- [timed_messageBox -timeout 30000 -icon error \
                                -message "Camp host is \"$midas(camp_host)\".\nFix it in Extras > Expert" \
                                -type okcancel -default ok -title "No Camp host"] {
                            ok {
                                tdx_initialize
                            }
                        }
                    }
                    return
		}
		# Execute initialization script
		# I might need to change this if there are rpc timeouts
		set val [expr { $idef(sweep_from)/double($idef(divide_by)) }]
		set ins $idef(sweep_ins_name)                
		if { [string equal $idef(sweep_var_type) integer] } {
		    set val [expr { round($val) }]
		}
		if { [string length $idef(init_script)] } {
		    set inicmd "set ins [list $ins]; set val [list $val]; $idef(init_script)"
		    #puts "Execute init script: $inicmd"
		    if { [logvar_catch { mui_camp_cmd $inicmd } ] } {
			return
		    }
		}

		# Prepare tolerance-test command
		if { [string length $idef(tol_test)] } {
		    set idef(full_tol_test) "set ins [list $ins]; set val [list $val]; $idef(tol_test)"
		    #puts "Tolerance test command: $idef(full_tol_test)"
		}
	    }

	    # Check logged variables
            if { $idef(num_logged) > 0 } {
                set i 0
                if { [ string length $midas(camp_host) ] && [ string compare "none" $midas(camp_host) ]} {
                    set bad ""
                    foreach var $idef(logged_vars) {
                        if { [imusr_check_polling $var $i] } {
                            append bad "$var "
                        }
                    }
                } else {
                    set bad "There is no Camp host defined!"
                }
                set idef(logged_poll) $midef(logged_poll)
                if { [string length $bad] } {
                    beep
                    switch -- [timed_messageBox -timeout 30000 -icon error \
                            -message "Some Camp variables cannot be logged:\n$bad\nPlease fix." \
                            -type okcancel -default ok -title "Bad logging"] {
                        ok { isetup_initialize log }
                    }
                    return
                }
            }

            # Begin run USED to be here!!!

	    # Now do the start-up in the appropriate style:
	    # automatic: start acquisition when sweep device reaches start value
	    # immediate: start acquisition right after run begins
	    # manual:    just leave the run paused
	    set run(starting) 3
	    set now [clock seconds]
	    switch -- $run(how_start_I) {
		automatic {
		    after 3000 imusr_monitor_start $now 300 5000
		}
		default {
		    set idef(full_tol_test) ""
		    after 500 imusr_monitor_start $now 30 1000
		}
	    }
	    td_act_cancelb show
	}
        end -
        endafterpoint -
        endaftersweep {
            if { $run(confirm_stop) >= 3 } {
                array set mapper {end now endafterpoint {after this point} endaftersweep {at the end of this sweep}}
                beep
                if { [timed_messageBox -timeout 15000 -icon question -type okcancel -default ok \
                          -message "OK to end run $mapper($command)?" -title "Confirm end" ] == "cancel" } {
                    return
                }
            }
            if { $rtitles(changing) } {
                beep
                switch [ timed_messageBox -timeout 30000 -icon question \
                        -type yesnocancel -default yes \
                        -message "Run titles are still being edited!\nApply titles before ending run?" \
                        -title "Confirm Titles" ] {
                    no {# Reject titles, but end run
                        titles_begin_change
                        titles_OK
                    }
                    cancel {# Cancel ending of run
                        return
                    }
                    default {# Confirm titles,
                        titles_OK
                    }
                }
                # The end-run perl scripts already perform the run archival; no need here.
            }
            if { [string equal $command "end"] } {
                td_odb odb_end_run "end run"
            } else {
                global rstat
                odb_get_stat
                idef_do_update
                if { [string equal $command "endaftersweep"] } {
                    if { $idef(num_sweeps_save) == -1 } {
                        set idef(num_sweeps_save) $idef(num_sweeps)
                        set midef(num_sweeps_save) $idef(num_sweeps)
                    }
                    # set in both midef and idef to avoid contamination
                    set idef(num_sweeps) $rstat(sweep_num)
                    set midef(num_sweeps) $rstat(sweep_num)
                } else {
                    set idef(final_point) 1
                    set midef(final_point) 1
                }
                td_odb odb_set_idef "flag endpoint"
                after 1000 imusr_monitor_end
            }
        }
        kill {
            catch { odb_get_runinfo }
            if { ! ($run(in_progress) || $run(starting)) } {
                beep
                timed_messageBox -timeout 30000 -type ok -icon warning \
                        -message "There is no run to kill!" -title "No run to kill"
            } else {
                if { $run(starting) > 1 } {
                    set run(starting) 0
                    catch { odb_get_runinfo }
                }
                if { $run(starting) == 1 } {
                    set run(starting) 0
                    odb_cancel_transition
                }
                if { $run(in_progress) } {
                    if { $run(last_file) == "" } {
                        titles_OK
                        td_odb odb_end_run "kill run"
                    } else {
                        if { $run(confirm_stop) >= 1 } {
                            beep
                            if { [timed_messageBox -timeout 30000 -type yesno -default no -icon question \
                                      -message "Really kill run $run(number)?\nAll data will be lost!" \
                                      -title "Confirm Kill" ] == "no" } {
                                return
                            }
                        }
                        titles_OK
                        td_odb odb_kill_run "kill run"
                    }
                }
            }
            iquick_cancel
        }
        quickstop {
            if { $run(confirm_stop) >= 2 } {
                beep
                if { [timed_messageBox -timeout 30000 -type yesno -default no -icon question \
                          -message "Really stop run $run(number),\nwithout finalizing or archiving?" \
                          -title "Confirm Quick Stop" ] == "no" } {
                    return
                }
            }
            titles_OK
            td_odb odb_abrupt_stop "stop run"
        }
        write {
            mui_write_data 0 80 500 $run(last_file)
        }
        pause {
            odb_pause_run
        }
        continue {
            odb_resume_run
        }
    }
    td_all_update
    td_make_menus
    after 2000 { td_background_update }
    after 6000 { set clogv(PrevGraphUp) 0 ; td_background_update }
    after 15000 { set clogv(PrevGraphUp) 0; td_background_update }
}

#   Repeatedly check if run is ready.  Run was "Initialized" at $from sec,
#   keep checking for $for sec, and repeat $every ms.

proc imusr_monitor_start { from for every } {
    global tdwin run midas rtitles idef

    catch { odb_get_runinfo }
    #puts "imusr_monitor_start $from $for $every | $run(in_progress) $run(transition) | $run(starting) $run(state)"

    if { $run(starting) == 2 } {
        puts "Preparing.  Should never happen.  Stop monitor."
	return
    }

    if { $run(starting) == 1 } {# Midas executing "start now"
	if { $run(in_progress) && !$run(transition) } {# Midas completed starting run
	    set run(starting) 0
	    catch { odb_get_runinfo }
            #puts "Startup completed sucessfully"
	}
    }

    if { $run(starting) == 3 } {# "Initializing" (or ramping) Successfully started.  Maybe check that ramp is finished.
	# Note that full_tol_test is null unless we are starting "automatic"
	if { [string length $idef(full_tol_test)] } {
	    set intol 0
	    catch {set intol [mui_camp_cmd $idef(full_tol_test)] }
	} else {# This block is Changed -- see saved_imusr_extra.tcl
	    set intol [ expr { ![string equal "manual" $run(how_start_I)]}]
            if { [string equal "manual" $run(how_start_I)] } {
                beep
                switch [ tk_messageBox -type okcancel -icon question -default ok -title "Start Acq" \
                        -message "Ready?\nClick OK to start acquisition\nor Cancel to cancel run" \
                        -parent [p_win_name $tdwin] ] {
                    ok {
                        set intol 1
                        set run(starting) 1
                    }
                    cancel {
                        iquick_cancel
                        #set run(starting) 0
                        #catch { odb_get_runinfo }
                        return
                    }
                }
            }
	}
	# Now un-pause (actually start) run if "immediate" or if "automatic" and in tolerance
        #puts "Monitor initializing: how start: $run(how_start_I).  tol_test: $idef(full_tol_test),  in tol: $intol"
	if { $intol } {
            # start timer for out-of-tolerance alerts
            set ::rstat(toler_time) [clock seconds]
	    # Begin the run in Midas.  I don't want to do this here, but earlier.
	    # See saved_imusr_extra.tcl
            #puts "Ready!  Now begin run!"
	    td_odb odb_begin_run "Begin run"
            # Zero Camp statistics
            catch { mui_camp_cmd { foreach v [sysGetInsNames] {varDoSet /$v -z} }}
            # Poll the midas-begin frequently:
            set for 45
            set from [clock seconds]
            set every 500
            # What I want:
	    # set run(starting) 0
	    # imusr_run continue
	}
    }
    # If still starting, check for program output or time-out; otherwise continue monitoring.
    set now [clock seconds]
    if { $run(starting) } {
	if { $now < $from + $for && $run(bg_start_out) == "" } {
	    after $every [list imusr_monitor_start $from $for $every]
	} else {# Timed out.  Action depends on how failed.
	    if { $run(starting) == 3 } {# Failed to set sweep device
                beep
		timed_messageBox -timeout 30000 -type ok -icon error -title "Failed run start" \
		    -message "There was a problem initializing the sweep device\n(it has not reached the start value).\nPlease check it and continue run." 
		after $every [list imusr_monitor_start $now $for $every]
	    } else {# Failed Midas begin run
		mui_handle_start_error { imusr_run start }
	    }
	}
    }
}

#   Wait for end of run and then restore number-of-points and number-of-sweeps
#   in the ODB to their proper values.  This is used when ending the imusr run
#   "after" point or sweep.  Also used at begin run, just in case it didn't
#   get executed properly at end.

proc imusr_monitor_end { } {
    uplevel \#0 {
        if { $run(in_progress) } {
            after 2000 imusr_monitor_end
        } else {
            # Restore parameters used to end run asynchronously.
            odb_get_idef
            if { $idef(num_sweeps_save) >= 0 } {
                set idef(num_sweeps) $idef(num_sweeps_save)
                set midef(num_sweeps) $idef(num_sweeps_save)
                set idef(num_sweeps_save) -1
                set midef(num_sweeps_save) -1
            }
            set idef(final_point) 0
            set midef(final_point) 0
            odb_set_idef
        }
    }
}

#   Check that the sweep device exists and is online.  If it is offline,
#   try to put it online.  It will notify the user, and return boolean online 
#   status. 
proc imusr_camp_dev_ready { ins } {
    set online 0
    set absent [catch {mui_camp_cmd "insGetIfStatus /$ins" } online ]
    if { $absent==0 && $online == 0 } {# Not absent and not online, so put online
	set stat [catch {mui_camp_cmd "insSet /$ins -line on"} result]
	if {$stat && [string match -nocase *timeout* $result]} { after 1000 }
	set absent [catch {mui_camp_cmd "insGetIfStatus /$ins" } online ]
    }
    if { $absent } {
        beep
	timed_messageBox -timeout 30000 -icon error \
	    -message "Sweep instrument $ins does not exist in Camp.\nPlease choose an existing instrument (check spelling) or create one named $ins." \
	    -type ok -title "Fix sweep device"
	return 0
    }
    if { $absent==0 && $online == 0 } {# Not absent and not online
        beep
	timed_messageBox -timeout 30000 -icon error \
	    -message "Sweep instrument $ins is offline.\nPlease correct any problem putting it online.  Message was:\n$result" \
	    -type ok -title "Fix sweep device"
	return 0
    }
    return 1
}

# Strict validation of sweep range.
# Return "OK" or reason for error.
# This test midef() values, so at beginning of run must make sure
# they are synched to idef.
# We use this at the beginning of a run.  Do we also want to use when 
# definitions are applied?
proc imusr_check_sweep_limits { } {
    global midef imusr_inst_defs

    set valid 0

    # First perform the standard validation, idef_validate_sweep.
    if { [idef_validate_sweep] == 0 } {
        return "Sweep range was invalid."
    }

    # Now, for soft toggles, make sure none of the instantaneous values
    # exceed limits.  We do a third check below, using $togg again, and 
    # a test of ref toggle below that.
    set togg 0
    foreach n {1 2} {
        if { $midef(mod${n}_enable) && [string match -nocase soft $midef(mod_type_$n)]} {
            set togg [expr {$togg + abs($midef(mod_value_$n))}]
        }
    }
    set xmax [expr {$midef(max) - $togg}]
    set xmin [expr {$midef(min) + $togg}]
    set valid 1
    idef_test_val f sweep_from $xmin $xmax $xmin
    idef_test_val f sweep_to $xmin $xmax $midef(sweep_from)
    if { !$valid } { 
        return "Combined sweep range and soft-toggle magnitude would exceed stated limits."
    }

    set xmax $midef(max)
    set xmin $midef(min)

    # Now, for Camp sweep device, compare limits against any Camp variables for min/max.   
    if { [string match -nocase Camp $midef(sweep_device)] } {

        if { ![array exists imusr_inst_defs] } {
            idef_read_inst_def
        }
        set type $midef(sweep_ins_type)
        if { [info exists imusr_inst_defs($type)] } {
            if { [catch {
                lassign $imusr_inst_defs($type) \
                    descr kind what units scaling set_var min max init_script tol_test
                foreach m [list min max] op {+ -} {
                    set v [set $m]
                    if { [string match "/~/*" $v] } {
                        set path "/$midef(sweep_ins_name)/[string range $v 3 end]"
                        set v [expr {round( ([mui_camp_cmd "varGetVal $path"])*$midef(divide_by))}]
                        if "${op}($midef(sweep_from) - $v) < $togg || ${op}($midef(sweep_to) - $v) < $togg" {
                            return "Camp variable $path says sweep range is invalid."
                        }
                    }
                    set v [bounded $v $xmin $xmax]
                    set x$m $v
                }
            } mes ] } {# return "invalid" when error caught
                return "Error validating Camp sweep device limits: $mes."
            }
        }
    }
    foreach n {1 2} {
        if { $midef(mod${n}_enable) && [string match -nocase ref* $midef(mod_type_$n)]} {
            idef_test_val f mod_value_$n $xmin $xmax $midef(mod_value_$n)
            if { !$valid } { 
                return "Modulation $n reference point is outside limits."
            }
        }
    }
    return "OK"
}

#   Check that a Camp variable (for logging) is accessible and is being polled
#   properly.  If not polling, apply the stated poll interval.  If it is not pollable,
#   then assume it is a setpoint variable, and is OK.   The Camp variable
#   and also its index in the ODB list of variables are given as parameters.
#   Return 0 for OK, 1 for failure (like catch)
proc imusr_check_polling { var index } {
    global ilogv idef midef
    set ilogv(valid$var) 0
    if { ![info exists ilogv(poll$var)] } {
        set ilogv(poll$var) [lindex $midef(logged_poll) $index]
    }
    while { [llength $midef(logged_poll)] < $index + 1 } {
        #puts "Bad poll list index $index of: $midef(logged_poll)\nfor vars $midef(logged_vars)"    
        lappend midef(logged_poll) 5
    }
    # check device online; DON'T try to switch online.
    set dev [join [lrange [split $var "/"] 0 1] "/"]
    set online 0
    set stat [catch { mui_camp_cmd "insGetIfStatus $dev" } online]
    if { $stat == 0 && $online == 0 } {
        # It may be worth it to attempt switching devices online, but not like
        # this.  We need multi-threading client to do it well.
        set stat 1
	# set stat [catch { mui_camp_cmd "insSet $dev -line on" } ]
    }
    if { $stat } { return 1 }
    # Check if var is pollable, polling, and how often; Start polling.
    # Apply odb poll interval only if current camp polling is poor
    set ans {0,0,0}
    set stat [catch { mui_camp_cmd "return \"\[varGetAttributes $var\],\[varGetStatus $var\],\[varGetPollInterval $var\]\"" } ans]
    if { $stat } { return 1 }
    lassign [split $ans ,] pattr pstat pint
    if { !( [string is integer -strict $pattr] && [string is integer -strict $pstat] ) } { return 1}
    if { !($pattr & 32) } { return 1 } ;# Variable is NOT loggable
    # It looks passably valid, so accept it.  Failures from here on are quibbles.
    set ilogv(valid$var) 1

    if { ($pattr & 8) } { # Variable is pollable
        #  ensure polling on with good poll interval
        if { ($pstat & 8 ) == 0 || $pint < 1 || $pint > 45 } {
            if { $pint < 1 || $pint > 45 } {
                set pint [lindex $midef(logged_poll) $index]
            }
            set pint [bounded $pint 1 90]
            set stat [catch { mui_camp_cmd "varDoSet $var -p off;varDoSet $var -p on -p_int $pint" } ]
        }
    } else {
        set pint 0
    }
    set ilogv(poll$var) $pint
    set midef(logged_poll) [lreplace $midef(logged_poll) $index $index $pint]
    return 0
}

#   The following could provide some confirmation too.

proc toler_renormalize { } {
    global rstat
    after cancel $rstat(renorm_id)
    if { [td_odb odb_renormalize "request renormalization"] } {
        set rstat(renorm_id) [after 800 {check_toler_renorm 40 800 0} ]
    }
}
# Check if renormalization has finished: watch for "renormalize" flag false and norm_rate not 0
# Check for $iter iterations, once every $delay milliseconds.  The $started flag is a boolean
# saying whether we have seen the renormalize flag get set yet.
proc check_toler_renorm { iter delay started } {
    global rstat
    set rstat(renorm_id) {}
    catch { odb_get_stat }
    #puts "check_toler_renorm $iter $delay : $rstat(renormalize)==0 && $rstat(norm_rate) > 0 && $rstat(in_tolerance)?"
    if { !$started } {
        set started $rstat(renormalize)
    }
    if { !$started } {
        catch { odb_renormalize }
    }
    if { $started && $rstat(renormalize)==0 && $rstat(norm_rate) > 0 } {
        if { !$rstat(in_tolerance) } {
            puts "Renormalization Failure"
            beep
            switch [timed_messageBox -timeout 20000 \
                    -type retrycancel -default cancel -icon warning \
                    -message "Renormalization failed:\nbeam still out of tolerance.\nCheck beam rates." \
                    -title "Renormalization Failure" ] {
                retry { toler_renormalize }
            }
        }
    } elseif { [incr iter -1] } {
        set rstat(renorm_id) [after $delay [list check_toler_renorm $iter $delay $started] ]
    } else {
        puts "Renormalization Timeout: $iter"
        beep
        timed_messageBox -timeout 20000 \
                -type ok -default ok -icon warning \
                -message "Renormalization still not complete.\nPlease watch it." \
                -title "Renormalization Timeout"
    }
}
set rstat(renorm_id) {}
set rstat(renormalize) 0
set rstat(norm_rate) 0
set rstat(in_tolerance) 0


# Generate the run title, using other header fields plus the sweep range.

proc imusr_set_runtitle { } {
    global rtitles idef 
    set rtitles(runtitle) [join [list $rtitles(sample) \
                $idef(sweep_from)-$idef(sweep_to):$idef(sweep_incr) \
                $rtitles(temperature) $rtitles(field)] "  "]
    set rtitles(subtitle) "$rtitles(temperature) $rtitles(field)"
}

#   Some initializations.

set idef(num_sweeps_save) -1
set idef(final_point) 0
set midef(changed_at) 0
set idef(toler_enable) 1
set idef(logged_poll) {}
set run(how_start_I) automatic

if { $iqwin == "" } {
    # stand-alone testing, so start I-quick window immediately!

    set midef(min) 0
    set midef(max) 10000
    set midef(mod_type_1) None
    set midef(mod_type_2) None
    iq_initialize start toler mod sweep preset
    
}

