#   isetup_extra.tcl
#   Procedures for defining the imusr setup.  The setup window
#   will contain a "tabset" -- a set of pages with tabs -- with
#   different aspects of the setup.  Each pane is defined in a
#   different specTcl ui file, but all the ui.tcl files should 
#   be concatenated into isetup_parts.tcl. (Is this better than 
#   sourcing each in turn?  I am guessing.)
#
#   $Log: isetup_extra.tcl,v $
#   Revision 1.14  2016/09/06 22:12:02  asnd
#   Minor cleanup
#
#   Revision 1.13  2015/04/18 00:03:21  asnd
#   Increase max tolerance setting, and fix a missing global declaration
#
#   Revision 1.12  2007/10/03 02:20:04  asnd
#   Alter validation in imusr setup (off when disabled/readonly)
#
#   Revision 1.11  2006/09/22 03:46:39  asnd
#   Handle sweep device menu better. (Also Tk 8.4)
#
#   Revision 1.10  2006/06/04 16:05:47  asnd
#   Remove debugging messages
#
#   Revision 1.9  2006/05/26 16:47:49  asnd
#   Accumulated changes over winter shutdown.  Notably changes to how changes are tracked on the ImuSR setup pages
#
#   Revision 1.8  2004/08/05 07:04:59  asnd
#   Fix "change modulation". Improve interaction between quick and full setup windows.
#
#   Revision 1.7  2004/08/05 02:25:21  asnd
#   Better handling when no Camp server
#
#   Revision 1.6  2004/03/11 05:16:22  asnd
#   Updates to offline testing
#
#   Revision 1.5  2004/02/20 23:08:06  asnd
#   Single musr Midas experiment.  Handle year changes.
#
#   Revision 1.4  2003/10/15 09:28:28  asnd
#   Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#   Revision 1.3  2003/10/01 09:56:12  asnd
#   Minor changes (use "other" instead of "field" for field sweeps)
#
#   Revision 1.2  2003/09/26 00:39:03  asnd
#   Recent gui changes (sweep details, ggl pulser...)
#
#   Revision 1.1  2003/09/19 06:23:26  asnd
#   ImuSR and many other revisions.
#

package require BLT

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

if { ![string length [info procs sweeps_ui]] } { 
    source [file join [file dirname [info script]] isetup_parts.tcl]
}

group_drag_bind_proc ilog_move_var ilog_rearrange

proc isetup_initialize { args } {
#   Draw the imusr setup window, and then insert the tab set.

    global idefwin idef midef ilogv clogv sweep_menu_made_at

    set idw [td_win_name $idefwin]
    if { [ string length $idefwin ] } {
        # We are in a real application, where the setup window is not main
        if { [winfo exists $idefwin.id_OK_b] } {
            # if window exists, make it visible
            wm deiconify $idw
            raise $idw
        } else {
            # window is not built yet.  Build main window
            toplevel $idw
            imusr_setup_ui $idefwin
            # Define the tab set.
            idef_maketabs $idefwin
        }
    } else {
        # Test app, main window already exists; make tabset
        idef_maketabs $idefwin
    }

    if { [llength $args] == 1 } {
        set i [lsearch -exact [list sweeps toggles presets toler log ] [lindex $args 0]]
        #puts "Pre-select tab $args = $i"
        if { $i >= 0 } {
            $idefwin.tabs invoke $i
            $idefwin.tabs select $i
        }
    }

    wm title $idw "I-${PREF::muchar}SR Setup"

    # NO!  array unset ilogv *
    # Instead: 
    set ilogv(init) 0

    set clogv(insnames) {}

    idef_bind_help id_revert_b "Refresh/restore values from Midas run control"
    idef_bind_help id_apply_b  "Apply changed settings"
    idef_bind_help id_load_b   "Load settings from a save-file or previous run"
    idef_bind_help id_saveas_b "Save settings to a file"
    idef_bind_help id_delete_b "Delete some other saved settings file"
    idef_bind_help id_cancel_b "Close I-\u00b5SR Definitions window (Cancel any changes)"
    idef_bind_help id_OK_b  "Close I-\u00b5SR Definitions window (Apply any changes)"
    #
    set p tabs.presets.preset
    idef_bind_help ${p}_const_counts_rb \
	"Select counting fixed number of events for normalization"
    idef_bind_help ${p}_const_time_rb "Select counting clock ticks for normalization"
    idef_bind_help ${p}_norm_mu_rb "Normalization uses muon counts"
    idef_bind_help ${p}_norm_e_rb "Normalization uses total positron counts"
    idef_bind_help ${p}_num_e "Number repetitions of preset counts"
    idef_bind_help ${p}_set_e "Number of counts to make one preset"
    #
    set p tabs.sweeps.sw
    idef_bind_help ${p}_sweep_dev_mb "Primary sweep device (click to change)"
    idef_bind_help ${p}_details_b "View or reconfigure sweep-device properties"
    idef_bind_help ${p}_htitle_e "Title of the sweep array in data file"
    idef_bind_help ${p}_min_e "Minimum allowed setpoint for sweep device"
    idef_bind_help ${p}_max_e "Maximum allowed setpoint for sweep device"
    idef_bind_help ${p}_settle_e "Settling time (ms) after a sweep step"
    idef_bind_help ${p}_num_sweeps_e "Number of sweeps for run (enter 0 for no limit)"
    idef_bind_help ${p}_from_e "Start-point of scan"
    idef_bind_help ${p}_to_e   "End-point for run scan"
    idef_bind_help ${p}_step_e "Sweep increment"
    #
    set p tabs.toggles.mod
    idef_bind_help ${p}1_type_mb "Select type of modulation (toggles)"
    idef_bind_help ${p}1_num_e "Number of toggle cycles"
    idef_bind_help ${p}1_settle_e "Time to wait after toggling"
    idef_bind_help ${p}1_value_e "Alternate setpoint (value or difference)"
    idef_bind_help ${p}1_suf1_e "Suffix on counter name in array label"
    idef_bind_help ${p}1_suf2_e "Suffix on counter name in array label"
    idef_bind_help ${p}2_type_mb "Select type of modulation (toggles)"
    idef_bind_help ${p}2_num_e "Number of toggle cycles"
    idef_bind_help ${p}2_settle_e "Time to wait after toggling"
    idef_bind_help ${p}2_value_e "Alternate setpoint (value or difference)"
    idef_bind_help ${p}2_suf1_e "Suffix on counter name in array label"
    idef_bind_help ${p}2_suf2_e "Suffix on counter name in array label"
    idef_bind_help ${p}_fast_enable_rb "Enable continuous fast state toggle"
    idef_bind_help ${p}_fast_disable_rb "Disable continuous fast state toggle"
    idef_bind_help ${p}_fast_suf1_e "Suffix on counter name in array label"
    idef_bind_help ${p}_fast_suf2_e "Suffix on counter name in array label"
    idef_bind_help ${p}_toggle_always_rb "Keep performing inner toggle when paused (RF)"
    idef_bind_help ${p}_toggle_wait_rb "Wait for beam in appropriate toggle state"
    idef_bind_help ${p}_exchange_b "Exchange settings for inner and outer toggles"
    #
    set p tabs.toler.toler
    idef_bind_help ${p}_const_counts_rb \
	"Select counting fixed number of events for normalization"
    idef_bind_help ${p}_const_time_rb "Select counting clock ticks for normalization"
    idef_bind_help ${p}_toggle_always_rb "Keep performing inner toggle when paused (RF)"
    idef_bind_help ${p}_toggle_wait_rb "Wait for beam in appropriate toggle state"
    idef_bind_help ${p}_enable_cb "Enable or disable checks for constant counting rate"
    idef_bind_help ${p}_timeout_e "Maximum time for counting determining normalization count rate"
    idef_bind_help ${p}_delay_e "Delay of acquisition after coming back in tolerance"
    idef_bind_help ${p}_set_e "Percentage tolerance on acceptable count rate"
    idef_bind_help ${p}_renorm_b "Click to reset Normalzation now"
    #
    set p tabs.log.ilog
    idef_bind_help ${p}_configure_mb "Menu for defining list of logged Camp variables" 

#   Unlike with menu-buttons, regular buttons with -underline still need to 
#   have an alt-key binding added explicitly.
    bind $idw <Alt-r> "$idefwin.id_revert_b invoke ; break"
    bind $idw <Alt-a> "$idefwin.id_apply_b invoke ; break"
    bind $idw <Alt-l> "$idefwin.id_load_b invoke ; break"
    bind $idw <Alt-s> "$idefwin.id_saveas_b invoke ; break"
    bind $idw <Alt-d> "$idefwin.id_delete_b invoke ; break"
    bind $idw <Alt-q> "$idefwin.id_cancel_b invoke ; break"
    bind $idw <Alt-o> "$idefwin.id_OK_b invoke ; break"

    # Un-do a bad spectcl binding
    bind $idw <Destroy> {}

    # add our own destroy binding, but not to the toplevel!
    bind $idefwin.id_OK_b <Destroy> { idef_quit }

    # Get fresh parameters from ODB if no changes, or no recent changes;
    # propagate and initialize widget variables

    # Perform ImuSR initialization update, forcing the sweep device menu be built,
    # and propagate between variables
    set sweep_menu_made_at 0
    idef_do_update
    idef_propagate

    #  !!!????  Perhaps put the sweep_kind in the ODB and omit this:

    idef_init_sweep_kind
}

# Define the tab set.  If creation fails, fall back to a frame
proc idef_maketabs { win } {
    set ::clogv(idef_displayed_vars) [list]

    if { [ set use_frames [ catch {  
            ::blt::tabset $win.tabs -samewidth true -relief flat \
                    -takefocus 1 -selectpad 3 -gap 2 -tearoff 0 -highlightthickness 1
    } ] ] } { # fail, so pack all into a frame
        frame $win.tabs
    }

    frame $win.tabs.sweeps
    frame $win.tabs.toggles
    frame $win.tabs.presets
    frame $win.tabs.toler
    frame $win.tabs.log
    sweeps_ui $win.tabs.sweeps
    toggles_ui $win.tabs.toggles
    presets_ui $win.tabs.presets
    toler_ui $win.tabs.toler
    ilogged_ui $win.tabs.log
    if {$use_frames } {
        pack $win.tabs.sweeps $win.tabs.toggles $win.tabs.presets \
	    $win.tabs.toler $win.tabs.log -in  $win.tabs
    } else { # load individual tabs to tabset
        foreach {t n} [list sweeps Sweeps toggles Modulation \
			   presets Preset toler Tolerance log Logging] {
	    $win.tabs insert end $t -text $n
            $win.tabs tab configure $t -window $win.tabs.$t \
                    -fill both -ipadx 4 -ipady 4 \
                    -background $::PREF::dimbgcol -selectbackground $::PREF::bgcol 
        }
    }
    # Put the tab set on the screen
    grid $win.tabs -in [td_win_name $win] -row 2 -column 1 \
	-padx 0 -pady 1 -sticky ew
    # set the traversal order on some:
    raise $win.tabs.toler.toler_const_time_rb $win.tabs.toler.toler_const_counts_rb
    raise $win.tabs.toler.toler_enable_cb $win.tabs.toler.toler_toggle_wait_rb
    set b $win.tabs.toggles
    foreach n [list 1 2] {
        raise $b.mod${n}_num_e $b.mod${n}_type_mb
        raise $b.mod${n}_settle_e $b.mod${n}_num_e
        raise $b.mod${n}_value_e $b.mod${n}_settle_e
        raise $b.mod${n}_suf1_e $b.mod${n}_value_e
        raise $b.mod${n}_suf2_e $b.mod${n}_suf1_e
    }
    raise $b.mod_toggle_always_rb $b.mod_fast_suf2_e
    raise $b.mod_toggle_wait_rb $b.mod_toggle_always_rb
    raise $win.tabs.presets.preset_const_time_rb \
	$win.tabs.presets.preset_const_counts_rb
}

#   ODB parameter updates versus display of user's changes, possibly displayed
#   in both full-setup and quick-setup windows.  The model chosen, whereby the
#   entry fields always look (and are) active, is disappointing; it would be better
#   to behave like the TD mode and rig pages, and the run titles, where disabled
#   widgets look like labels until the user clicks the [Change] button.  Think 
#   about converting later.
#
#   Now, the behaviour is that the values on both the quick and full setup
#   pages normlly reflect the true ODB values, but when the user changes any
#   of them they become frozen for a period of time, until the user applies
#   or cancels the changes, or until they time-out.  Changes are noted by the
#   proc idef_declare_changed, invoked by most of the widgets, and recorded
#   in the value of midef(changed_at), which contains either 0 (for no changes)
#   or the time of the last unapplied change.
#
#   Normally, idef_do_update reads the ODB's imusr setup (to $idef), and copies
#   values into the mirror ($midef) which is displayed on screen.  idef_do_update
#   is invoked  by the [Refresh] button*, by isetup_initialize, iq_initialize, 
#   idef_load* and idef_apply* (when cancelled), imusr_run (begin, start*, end),
#   and by td_all_update.
#
#   Parameter (optional): time (as from [clock seconds]) indicating when a
#   relevant change took place.  If a recent time is specified, then we retain
#   user-modified parameters.  If omitted, it defaults to $midef(changed_at).
#   Calls marked with * above use [idef_do_update 0] to force an update, and 
#   erase any unapplied modifications made by the user.
#
proc idef_do_update { {time -1} } {
    global idefwin iqwin logwin midef idef ilogv run


    if { [ td_odb odb_get_idef "get I-${PREF::muchar}SR parameters" ] == 0 } { return }

    if { $time < 0 } { set time $midef(changed_at) }

    #  No reason to freeze $midef values when there are no setup windows:
    if { ![winfo exists [td_win_name $iqwin]] && 
         ![winfo exists [td_win_name $idefwin]] && 
         ![winfo exists [td_win_name $logwin]]} {
        set time 0
    }

    #puts "Check for stale changes: [clock seconds] vs $time + $PREF::memoryLapse ([expr {$time + $PREF::memoryLapse}]), $::run(confirm_stop)"
    # Use a while loop instead of [if] just so we can [break] out from it.
    
    while { [clock seconds] > $time + $PREF::memoryLapse } {

        if { $time > 0 && $run(confirm_stop) >= 1 } {
            beep
            switch -- [timed_messageBox -timeout 15000 -icon question -type yesnocancel \
                           -default [expr { $run(confirm_stop) < 2 ? "cancel" : "no" }] \
                           -message [join {
                               "There are unapplied changes to the I-\u00b5SR setup parameters."
                               "Apply them now? (yes/no) or choose cancel to forget them." 
                           } "\n" ] \
                           -title "Apply stale changes" ] \
            {
                yes { idef_apply }
                no { 
                    incr midef(changed_at) $PREF::memoryLapse
                    set time $midef(changed_at)
                    break
                }
                cancel { }
            }
        }

        # Note whether the sweep device has changed.
	#puts "Compare sweep dev: $idef(sweep_device)+$idef(sweep_ins_name) ==? $midef(sweep_device)+$midef(sweep_ins_name)"
        set sweep_dev_changed [string compare "$idef(sweep_device)+$idef(sweep_ins_name)" \
	       "$midef(sweep_device)+$midef(sweep_ins_name)" ]

        set idef(ggl_range_label) [lindex $midef(ggl_dac_range_list) $idef(ggl_dac_range) ]

        # Copy server params to display
        array set midef [array get idef]

        # This block can be omitted if every device definition parameter gets into the ODB
        # ---------------
        if { $sweep_dev_changed } {
            # Load up parameters for sweep device $idef(sweep_device)
            idef_sweep_device $idef(sweep_device) $idef(sweep_ins_name)
            array set midef [array get idef]
        }
        # --------------
        set midef(changed_at) 0
        break
    }

    # Only do the following if there are no changes in progress
    if { [clock seconds] > $time + $PREF::memoryLapse } {
        foreach var $midef(logged_vars) poll $midef(logged_poll) {
            set ilogv(poll$var) $poll
        }

        # Should we do anything with the return value from idef_validate?
        set stat [idef_validate]
        #puts "validate says $stat"

        idef_propagate

        array set idef [array get midef]

        # Indicate that displayed values are unchanged; only if window is active
        if { [winfo exists $idefwin.id_apply_b] } {
            $idefwin.id_apply_b configure -state disabled
            $idefwin.id_revert_b configure -text Refresh
        }
        global logwin
        if { [winfo exists $logwin.lv_autohead_b] } {
            $logwin.lv_autohead_b configure -state disabled
        }
        set midef(changed_at) 0
    }

    # Make menus, but try not to rebuild them every second!  Just do it when 
    # the window is created and at rare intervals later.
    if { [winfo exists $idefwin.id_apply_b] && \
	     [clock seconds] > $::sweep_menu_made_at + $PREF::memoryLapse } {
	# puts "schedule idef_make_sweep_menu"
	after 1 after idle idef_make_sweep_menu
	after 300 after idle logvar_make_menu $idefwin.tabs.log.ilog_configure_mb
    }
}

#   Propagate values to others for widget labels, and activate widgets.
#   This is invoked by idef_do_update, and when various entries are changed
#   (so it only does limited propagation; some other propagation is done in 
#   idef_apply and idef_validate).
proc idef_propagate { } {
    global idefwin midef clogv

    #puts "idef_propagate"
    #   Although the sweep range is shown on the sweep device page, don't
    #   validate it until applying it.
    #   Sweep device:
    if { [winfo exists $idefwin.tabs.sweeps.sw_range_inter_b] } {
        if { [string match -nocase "camp" $midef(sweep_device)] && \
                [string length $midef(sweep_ins_name)] } {
            set midef(sweep_device_label) "/$midef(sweep_ins_name)"
            if { ![string equal "Interface" [$idefwin.tabs.sweeps.sw_range_inter_b cget -text]] } {
                idef_bind_help tabs.sweeps.sw_range_inter_b "Configure Camp instrument interface"
                $idefwin.tabs.sweeps.sw_range_inter_b configure -text "Interface" \
                        -command {cis_initialize $midef(sweep_ins_name)}
            }
        } else {
            set midef(sweep_device_label) $midef(sweep_device)
            set midef(sweep_ins_name) $midef(sweep_device)
            if { ![string equal "DAC Range" [$idefwin.tabs.sweeps.sw_range_inter_b cget -text]] } {
                cis_close
                idef_bind_help tabs.sweeps.sw_range_inter_b "Set GGL DAC voltage range"
                radiomenu $idefwin.tabs.sweeps.sw_range_inter_b -text "DAC Range" \
                        -values $midef(ggl_dac_range_list) -variable midef(ggl_range_label) \
                        -indicatoron 1 -command {ggl_dac_range ; idef_changed}
            }
        }
    }
    # modulation type
    if { ! [string match -nocase "none" $midef(mod_type_1)] } {
	idef_test_val d mod_num_1 1 1000 1
    }
    if { ! [string match -nocase "none" $midef(mod_type_2)] } {
	idef_test_val d mod_num_2 1 1000 1
    }

    # tolerance enable/disable
    if { $midef(toler_enable) } {
	set midef(tolerance_now) "(now enabled)"
	set s normal
    } else {
	set midef(tolerance_now) "(now disabled)"
	set s disabled
    }
    if { [winfo exists $idefwin.tabs] } {
	foreach w [list timeout_l timeout_un_l delay_l delay_un_l set_l set_un_l renorm_b] {
	    $idefwin.tabs.toler.toler_$w configure -state $s
	}
	foreach w [list timeout_e delay_e set_e] {
            configEntry $idefwin.tabs.toler.toler_$w $s
	}
	foreach n [list 1 2] {
	    set s [lindex [list normal disabled] \
		       [string match -nocase "none" $midef(mod_type_$n)]]
	    foreach w [list mod${n}_num_l mod${n}_settle_l mod${n}_suff_l] {
		$idefwin.tabs.toggles.$w configure -state $s
	    }
	    foreach w [list mod${n}_num_e mod${n}_settle_e mod${n}_suf1_e mod${n}_suf2_e] {
		configEntry $idefwin.tabs.toggles.$w $s
	    }
	    switch -- [string tolower $midef(mod_type_$n)] {
		soft {
                    configEntry $idefwin.tabs.toggles.mod${n}_value_e normal
                    $idefwin.tabs.toggles.mod${n}_value_l configure -state normal \
			-text "Mod Amplitude:"
		}
		ref {
                    configEntry $idefwin.tabs.toggles.mod${n}_value_e normal
                    $idefwin.tabs.toggles.mod${n}_value_l configure -state normal \
			-text "Reference value:"
		}
		default {
                    configEntry $idefwin.tabs.toggles.mod${n}_value_e disabled
                    $idefwin.tabs.toggles.mod${n}_value_l configure -state disabled \
			-text "Value:"
		}
	    }
	}
        set s [lindex [list disabled normal] [expr {$midef(mod_fast_enable) > 0}]]
        configEntry $idefwin.tabs.toggles.mod_fast_suf1_e $s
        configEntry $idefwin.tabs.toggles.mod_fast_suf2_e $s
        $idefwin.tabs.toggles.mod_fast_suff_l configure -state $s

	#  Presets counting mu or e
        if { ![string is integer $midef(constant_time)] } {
            set midef(constant_time) 0
        }
	#  Logged Camp variables
        global ilogv

        set midef(num_logged) [llength $midef(logged_vars)]
	set clogv(loggedVars) [linsert $midef(logged_vars) 0 none]
	#
        set new_poll [list]
	set i 0
	foreach var $midef(logged_vars) {
	    # puts "Logged var $i  $var   ----------- "
            # In case ilogv has not been built up yet, just quit
            if { ![info exists ilogv(poll$var)] } { return }
            lappend new_poll $ilogv(poll$var)
            if { [lindex $midef(logged_poll) $i] != $ilogv(poll$var) } {
                #puts "Poll interval for variable $var changed from [lindex $midef(logged_poll) $i] to $ilogv(poll$var)"
                idef_declare_changed
            }
	    if { ![string equal $var [lindex $clogv(idef_displayed_vars) $i]] } {
		#puts "Is new/different"
		if { ![winfo exists $idefwin.tabs.log.ilog_vars_f.var$i] } {
		    idef_add_camp_row $i
		}
                if { ![info exists ilogv(valid$var)] } {
                    catch { imusr_check_polling $var $i }
                }
                if { $ilogv(valid$var) } {
                    set col $::PREF::fgcol
                } else {
                    set col $::PREF::brightcol
                }
		$idefwin.tabs.log.ilog_vars_f.var$i configure -text $var -fg $col
		$idefwin.tabs.log.ilog_vars_f.poll$i configure -textvariable ilogv(poll$var)
	    }
	    incr i
	}
        set midef(logged_poll) $new_poll
	# Delete rows that have been abandoned
	set numdisp [llength $clogv(idef_displayed_vars)]
	while { $i < $numdisp } {
	    idef_delete_camp_row $i
	    incr i
	}
        set clogv(idef_displayed_vars) $midef(logged_vars)
        # Forget about variables that have been de-listed
        set tags [array names ilogv valid*]
        if { [llength $tags] > $midef(num_logged) } {
            foreach tag $tags {
                set var [string range $tag 5 end]
                if { [lsearch -exact $midef(logged_vars) $var] == -1 } {
                    after 10 [list after idle [list unset ilogv(valid$var) ilogv(poll$var)]]
                }
            }
        }
    } ;# end of displayable processing
};# end of idef_propagate

# Build menu of sweep devices.  Leave DAC as item 0.
# Append entries for current Camp instruments that are known to be useful
# sweep devices (known in imusr_inst_defs).  Finally, make
# an entry for creating a new Camp device.

proc idef_make_sweep_menu { } {
    global idefwin midas midef imusr_inst_defs sweep_menu_made_at

    if { [winfo exists $idefwin.tabs.sweeps.sw_sweep_dev_mb] } {
        #puts "Try $midas(camp_host) != \"\" && $midas(camp_host) != none && !\[catch {mui_camp_cmd sysGetInsNames} names\]"
        if { $midas(camp_host) != "" && $midas(camp_host) != "none" && \
                ![catch {mui_camp_cmd sysGetInsNames} names] } {
            #puts "Got names $names"
            if { ![catch { mui_camp_cmd \
               {set l {}; foreach i [sysGetInsNames] {lappend l [insGetTypeIdent /$i]}; set l}
            } types ] } {
                #puts "Got types: $types"
                #puts "Known types: [array names imusr_inst_defs]"
                $idefwin.tabs.sweeps.sw_sweep_dev_mb.menu delete 1 last
                $idefwin.tabs.sweeps.sw_sweep_dev_mb.menu add separator 
                foreach n $names t $types {
                    if { [info exists imusr_inst_defs($t)] } {
                        $idefwin.tabs.sweeps.sw_sweep_dev_mb.menu add command -label "/$n" \
                                -command [list idef_sweep_device Camp $n] 
                    }
                }
		set sweep_menu_made_at [clock seconds]
            } else {
                puts "Error: Failed to get instrument types: $types"
            }
            $idefwin.tabs.sweeps.sw_sweep_dev_mb.menu add separator 
            $idefwin.tabs.sweeps.sw_sweep_dev_mb.menu add command -label "New" \
                    -command [list idef_sweep_device New {}] -underline 0 
        }
    }
}

proc idef_exchange_mod { } {
    global midef
    swap_vars  midef(mod1_enable)  midef(mod2_enable)
    swap_vars  midef(mod_type_1)   midef(mod_type_2)
    swap_vars  midef(mod_num_1)    midef(mod_num_2)
    swap_vars  midef(mod_settle_1) midef(mod_settle_2)
    swap_vars  midef(mod_value_1)  midef(mod_value_2)
    swap_vars  midef(mod1_suffix1) midef(mod2_suffix1)
    swap_vars  midef(mod1_suffix2) midef(mod2_suffix2)
    idef_changed
}

#   Apply changes.  First ensure correctness, then save to odb, then read back from odb
#   Returns 0 usually, but 1 if cancelled.

proc idef_apply {  } {
    global idefwin run idef midef rtitles ilogv

    #   Validate parameters, and give notification if repairs were made
    if { [idef_validate] == 0 } {
        beep
        switch [timed_messageBox -timeout 20000 -type okcancel -icon warning \
                -title "Corrections Made" -parent [p_win_name $idefwin] -default ok \
                -message "Some corrections were made to the setup parameters.\n$midef(fixlist)"] {
            cancel { return 1 }
        }
    }
    # Determine array ("histogram") number and titles based on the modulation(s).
    # Remove spaces and braces from scan title:
    set h [ join [split $midef(scan_title) { {} } ] {} ]
    set histit [list [string range $h 0 9] Clock] ;# ??? Adjust for larger limits
    lappend histit [lindex [list Muons Total] $midef(norm_e)]
    set n 3
    set lc [list F B]
    foreach m [list 2 1 _fast] {
        if { $midef(mod${m}_enable) > 0 } {
            set lm${m} [list $midef(mod${m}_suffix1) $midef(mod${m}_suffix2)]
        } else {
            set lm${m} [list {}]
        }
    }

    ################ BUG WORKAROUND:  ALWAYS LIST MODS 1 AND 2:  #################
    # foreach m [list 2 1] {
    #     set lm${m} [list $midef(mod${m}_suffix1) $midef(mod${m}_suffix2)]
    #}
    ##############################################################################

    foreach mfast $lm_fast {
        foreach c $lc {
            foreach m2 $lm2 {
                foreach m1 $lm1 {
                    set h [ join [split "${c}${m1}${m2}${mfast}" { {}} ] {} ]
                    lappend histit [string range $h 0 9] ;# ??? Adjust for larger limits
                    # puts "Histogram $n is $h"
                    incr n
                }
            }
        }
    }
    set midef(num_hist) $n
    set midef(hist_titles) $histit
    # puts "There are $midef(num_hist) histograms:\n $midef(hist_titles)"

    # Apply poll intervals
    set midef(logged_poll) [list]
    foreach var $midef(logged_vars) {
        lappend midef(logged_poll) $ilogv(poll$var)
    }

    # Copy the parameters that can be changed any time, and do not
    # require a backup version.  Also copy parameters that aren't in 
    # ODB, including temporary parameters.
    foreach item [list sweep_kind sweep_from sweep_to sweep_incr sweep_settle num_sweeps \
	    scan_title sweep_device_label tolerance toler_delay tolerance_now toler_enable \
	    mod_settle_1 mod_settle_2 full_tol_test setup_name logged_poll changed_at \
	    ggl_dac_range_list fixlist] {
	set idef($item) $midef($item)
    }

    # Now check if any of the other parameters changed
    set bigchange 0
    foreach item [array names idef] {
	if { ![string equal $idef($item) $midef($item)] } {
	    set bigchange 1
	    # puts "Big change on $item: $idef($item) / $midef($item)"
            break
	}
    }

    if { $bigchange } {
	catch { odb_get_runinfo }
	if { $run(in_progress) } {
	    # We need to do a limited apply: only the values copied already, and
	    # allow the user to reverse even that.
            beep
	    switch [timed_messageBox -timeout 20000 -type okcancel -icon info \
		    -title "Run in progress" -parent [p_win_name $idefwin] -default ok \
		    -message "A run is in progress, so only the sweep range and tolerance will be applied." \
		       ] {
			   cancel { idef_do_update 0 ; return 1 }
			   ok     { set bigchange 0 }
		       }
	}
    }

    if { $bigchange } { 
	#puts "For big change, Copy all parameters"
	idef_make_backup
	array set idef [array get midef]
    }

    td_odb odb_set_idef "save I-${PREF::muchar}SR setup"

#    set rtitles(runtitle) [join [list $rtitles(sample) \
#            $idef(sweep_from)-$idef(sweep_to):$idef(sweep_incr) \
#            $rtitles(temperature) ] "  "]
#    if { !$rtitles(changing) } { titles_begin_change }

#   The forced idef_do_update will disable the apply button 
    idef_do_update 0

    return 0
}


#  Validate ImuSR setup parameters.  This is necessary because the
#  individual setting widgets allow invalid or inconsistent values
#  so the user can change them one at a time.  Here we test them all.
#  We return a boolean to say whether the values were originally
#  valid (1) or needed washing (0).

proc idef_validate { } {
    global idef midef

    set valid 1
    set midef(fixlist) {}

    if { [string length $midef(scan_title)] < 1 } {
	set midef(scan_title) $midef(sweep_device)
        append midef(fixlist) " " scan_title
        set valid 0
    }
    #
    #  Validate modulation:
    foreach n [list 1 2] {
        set i [lsearch -exact [list none hard soft ref] [string tolower $midef(mod_type_$n)]]
	if { $i < 1 } {
            #puts "Mod type $n $midef(mod_type_$n) sets number to 0"
	    set midef(mod_num_$n) 0
	}
	idef_test_val d mod_num_$n 0 1000 0
        set midef(mod${n}_enable) [expr {$midef(mod_num_$n) > 0}]
	if { $midef(mod${n}_enable) } {
	    if { [string equal $midef(mod${n}_suffix1) $midef(mod${n}_suffix2)] } {
		set midef(mod${n}_suffix1) -
		set midef(mod${n}_suffix2) +
	    }
	    idef_test_val f mod_settle_$n 0 10000 50
	    if { $i == 2 || $i == 3 } {
		idef_test_val f mod_value_$n $midef(min) $midef(max) 0
	    }
	} else {
            #puts "Mod $n enable == $midef(mod${n}_enable) forces type to None"
	    set midef(mod_type_$n) None
	}
    }
    if { $midef(mod2_enable) && !$midef(mod1_enable) } {
	idef_exchange_mod
    }
    idef_test_val d mod_fast_enable 0 1 0
    if { $midef(mod_fast_enable) } {
	if { [string equal $midef(mod_fast_suffix1) $midef(mod_fast_suffix2)] } {
	    set midef(mod_fast_suffix1) +
	    set midef(mod_fast_suffix2) -
	}
    }
    idef_test_val d toggle_always 0 1 0
    #
    # Validate sweeps -- uses toggles!
    set valid [expr {$valid && [idef_validate_sweep]}]
    #
    # Presets
    idef_test_val d constant_time 0 1 0
    idef_test_val d norm_e 0 1 1
    idef_test_val d norm_timeout 0 300 30
    idef_test_val d num_presets 1 100 1
    idef_test_val f preset_counts 1 2.0e9 1e6
    set midef(preset_counts) [expr {round($midef(preset_counts))}]
    #
    idef_test_val d toler_enable 0 1 1
    idef_test_val f toler_delay 0 200 10
    idef_test_val f tolerance 1 1000 30
    #
    # temporary debug:
    if {!$valid} {puts "Corrections were made"}
    return $valid
}

proc idef_validate_sweep { } {
    global midef
    set valid 1
    idef_test_val f min -9.9e19 9.9e19 -9.0e19
    idef_test_val f max -9.9e19 9.9e19 9.0e19
    idef_test_val f max $midef(min) 9.1e19 9.0e19
    idef_test_val d sweep_settle 0 100000 200
    idef_test_val d num_sweeps 0 1000 0
    idef_test_val f sweep_incr 0.0 [expr {abs($midef(max)-$midef(min))}] 1
    idef_test_val f sweep_from $midef(min) $midef(max) $midef(min)
    idef_test_val f sweep_to $midef(min) $midef(max) [expr {$midef(sweep_from)+$midef(sweep_incr)}]
    idef_test_val f sweep_to $midef(min) $midef(max) $midef(max)
    return $valid
}

#   Test a particular value in the global midef() array.
#   If original was invalid, correct it to default ($dflt) and clear the
#   boolean variable "valid" in the caller.
proc idef_test_val { type item min max dflt } {
    global midef
    if { [scan $midef($item) "%${type} %c" v c] == 1 } {
	if { $v >= $min && $v <= $max } {
	    if { $type == "d" } {
		# Avoid octal numbers in integers
		set midef($item) $v
	    }
            #puts "Validated midef($item) value $v in range $min to $max" 
	    return
	}
    }
    # puts "Had to change midef($item) from \"$::midef($item)\" to \"$dflt\""
    set midef($item) $dflt
    uplevel 1 { set valid 0 }
    append midef(fixlist) " " $item
    return
}

#   Set the sweep device from the menu.  $dev is DAC, Camp, or New, and $name
#   is the Camp instrument name.  The instrument properties are lists in the 
#   array imusr_inst_defs, which is read from file imusrins.def. The min and max
#   values can be numbers or Camp variable paths.  Any of min, max, and the scripts
#   init_script and tol_test may use "~" for the instrument name.  The scripts can
#   also use $ins for the instrument and $val for the setpoint value.

proc idef_sweep_device { dev name } {
    global midas midef idef imusr_inst_defs

    if { ![array exists imusr_inst_defs] } {
        if { [catch { idef_read_inst_def } msg] } {
            timed_messageBox -timeout 20000 -icon warning \
                -message "Failed to load standard instrument definitions:\n$msg" \
                -type ok -title "No Inst Defs"
        }
    }

    switch -- $dev {
	Camp {
	    if { [catch {mui_camp_cmd "insGetTypeIdent {/$name}"} type ] } {
                beep
                if { [ string length $midas(camp_host) ] && [ string compare "none" $midas(camp_host) ]} {
                    timed_messageBox -timeout 30000 -icon error \
                            -message [join [list "Device $name not recognized: $type" \
                            "Check if Camp is OK and instrument" \
                            "$name is defined."] "\n"] \
                            -type ok -title "Device not recognized"
                } else {
                    timed_messageBox -timeout 30000 -icon error \
                            -message "There is no Camp host defined!\nYou need a Camp host to use Camp devices!" \
                            -type ok -title "No Camp"
                }
		return
	    } else {
		if { [info exists imusr_inst_defs($type)] } {
                    if { [catch {
                        foreach { descr kind what units scaling set_var min max init_script tol_test } \
                                $imusr_inst_defs($type) {break}

                        if { [string match "/~/*" $min] } {
                            set path "/${name}/[string range $min 3 end]"
                            set min [mui_camp_cmd "varGetVal $path"]
                            set min [expr { round($min*$scaling) }]
                        }
                        set midef(min) $min

                        if { [string match "/~/*" $max] } {
                            set path "/${name}/[string range $max 3 end]"
                            set max [mui_camp_cmd "varGetVal $path"]
                            set max [expr { round($max*$scaling) }]
                        }
                        set midef(max) $max

                        if { [string match "/~/*" $set_var] } {
                            set midef(sweep_camp_var) "/${name}/[string range $set_var 3 end]"
                        } else {
                            # Bad device definition file!
                            set midef(sweep_camp_var) $set_var
                        }
                    } ] } {
                        beep
                        timed_messageBox -timeout 30000 -type ok -icon error \
                                -title "Bad definitions" -default ok \
                                -message "ERROR: device type $type is not configured properly.\nConfigure \"Details\""
                    } else {
                        regsub -all {\/\~} $init_script {/${ins}} midef(init_script)
                        regsub -all {\/\~} $tol_test    {/${ins}} midef(tol_test)
                        set midef(sweep_device) $dev
                        set midef(sweep_ins_name) $name
                        set midef(sweep_ins_type) $type
                        set midef(scan_title) [string trim "$what/$units" " /"]
                        set midef(sweep_units) $units
                        set midef(divide_by) $scaling
                        set midef(sweep_kind) $kind
                        # Any components that do not get listed in ODB are assigned to idef:
                        set idef(sweep_kind) $midef(sweep_kind)
#		        set idef(init_script) $midef(init_script)
#		        set idef(tol_test) $midef(tol_test)
                    }
		} else {
                    beep
                    timed_messageBox -timeout 30000 -type ok -icon warning \
                            -title "Undefined device" -default ok \
                            -message "Warning: device type $type not defined.\nConfigure \"Details\""
		}
	    }
	}
	New {
	    # Get the instrument type for the new instrument
            # First, get list of all possible Camp instrument types.
            set ctypes [list]
            if { [catch { 
                retried_camp_cmd sysGetInsTypes "read list of Camp instrument types" 
            } ctypes] } { return }

            # Have the user pick one from those that have been declared for ImuSR, 
            set itypes [list]
            foreach t $ctypes {
                if { [info exists imusr_inst_defs($t)] } {
                    lappend itypes $t
                }
            }
            lappend itypes "LIST ALL..."
            set type [popup_selector "Choose type" "Choose instrument type for sweep device:" \
                    $itypes {} single false 90]
            if { [string length $type] == 0 } { return }

            # Or select from all possible instrument types, if requested.
            if { [string equal $type "LIST ALL..."] } {
                set type [popup_selector "Choose type" "Choose instrument type for sweep device:" \
                        $ctypes {} single false 90]
            }
            if { [string length $type] == 0 } { return }

            # Choose an instrument name (show existing names to user)
            set cnames {}
            while { [catch { mui_camp_cmd sysGetInsNames } cnames] } {
                beep
                set response [ timed_messageBox -timeout 30000 -title "Failure" \
                        -type retrycancel -default cancel -icon error \
                        -message "Failed to read list of Camp instruments:\n$cnames" ]
                if { "$response" == "cancel" } { return }
            }
	    # Prompt user for an instrument name for the new instrument; sanitize it.
	    set response "retry"
	    while { $response == "retry" } {
                set response "done"
		set name [popup_selector "Choose name" \
			 "Choose a Unique name for the new instrument\n(avoid the listed names):" \
                         $cnames {} single false 90]
		set name [ join [split $name { `~!@$%^#&*()-+={}[];:'",<>?/} ] {} ]
		if { [string length $name] == 0 } { return }
		if { [lsearch -exact $cnames $name] >= 0 } {
		    beep
		    set response [ timed_messageBox -timeout 30000 -title "Duplicate instrument" \
				 -type retrycancel -default cancel -icon error \
				 -message "Bad instrument name: $name already exists." ]
		}
                if { $response == "cancel" } { return }
	    }
	    # Now add the instrument in Camp, then configure its interface
            if { [td_odb [list mui_camp_cmd [list insAdd $type $name]] "Create instrument"] } {
                cis_initialize $name
	    }
            # If instrument type was not a known ImuSR instrument, load some default configuration
            # and bring up details page
            if { ![info exists imusr_inst_defs($type)] } {
                set imusr_inst_defs($type) [list {} {} {} {} {1} {/~/???} {0} {99999} {varSet /~/??? -v $val} {}]
                details_initialize $type
            }
            after 10 after idle idef_sweep_device Camp $name
            after 1000 after idle idef_make_sweep_menu
	}
	DAC {
            set type GGL_DAC
            if { [info exists imusr_inst_defs($type)] } {
                foreach { descr kind what units scaling set_var min max init_script tol_test } \
			$imusr_inst_defs($type) {break}
                set midef(min) $min
                set midef(max) $max
                set midef(sweep_camp_var) {}
                set midef(sweep_device) $dev
                set midef(sweep_ins_name) {}
                set midef(sweep_ins_type) $type
                set midef(scan_title) [string trim "$what / $units" " /"]
                set midef(sweep_units) $units
                set midef(divide_by) $scaling
                set midef(init_script) {}
                set midef(tol_test) {}
                set midef(sweep_kind) $kind
                # Any components that do not get listed in ODB are assigned to idef:
                set idef(sweep_kind) $midef(sweep_kind)
            }
	}
    }
    idef_changed
    idef_TBO_labels
}

#   Set run-titles labels depending on Imusr sweep kind.
proc idef_TBO_labels { } {
    global idef rtitles

    idef_init_sweep_kind
    set rtitles(Field_lab) "Field:"
    set rtitles(Temp_lab) "Temperature:"
    switch -- $idef(sweep_kind) {
        T { set rtitles(Temp_lab) "Other:" }
        B { set rtitles(Field_lab) "Other:" }
    }
}

#   Initialize the non-ODB sweep-kind parameter (can get rid of this
#   if we put sweep_kind in odb).
proc idef_init_sweep_kind { } {
    global idef imusr_inst_defs
    if { ![info exists imusr_inst_defs] } { catch { idef_read_inst_def } }
    if { ![info exists idef(sweep_kind)] } {
        set idef(sweep_kind) "B"
        if { [string match -nocase "camp" $idef(sweep_device)] && \
                [info exists imusr_inst_defs($idef(sweep_ins_type))] } {
            set idef(sweep_kind) [lindex $imusr_inst_defs($idef(sweep_ins_type)) 1]
            # puts "Initialize sweep_kind to $idef(sweep_kind)"
        }
    }
}


#  Read instrument definition file, and put into array imusr_inst_defs.
#  The file format is just columns of values, so is prone to errors.
#  For the future, we should have a good file format and put the procedure
#  to read it here.  The file is imusrins.def in the source directory.
#  We may want to put it in the setups directory.

proc idef_read_inst_def { } {
    global imusr_inst_defs
    puts "Read instrument definitions from file imusrins.def"
    set fname [file join $::muisrc imusrins.def]
    set fhand [open $fname RDONLY]
    set err [catch {
        set inslines [string map [list "\\\n" " "] [read $fhand] ]
        set inslines [split $inslines \n]
        foreach insline $inslines {
            set insline [string trim $insline]
            if {[string first \# $insline] != 0} {# skip comments
                #puts "Instrument [lindex $insline 0]   [llength $insline]"
                if {[llength $insline] == 11} {
                    set imusr_inst_defs([lindex $insline 0]) [lrange $insline 1 end]
                }                    
            }
        }
    } response ]

    close $fhand

    if {$err} {
        puts "Error during read: $response"
        return -code error "Error during read: $response"
    }
}

proc idef_set_details { } {
    global midef
    if { [string match -nocase DAC $midef(sweep_device)] } {
        set midef(sweep_ins_type) GGL_DAC
    }
    details_initialize $midef(sweep_ins_type)
}


#  THE HELP LINE:
proc idef_bind_help { w h } {
    global idefwin
    bind $idefwin.$w <Enter> [list set midef_help_text "$h"]
    bind $idefwin.$w <FocusIn> [list set midef_help_text "$h"]
    bind $idefwin.$w <Leave> [list set midef_help_text {}]
    bind $idefwin.$w <FocusOut> [list set midef_help_text {}]
}


#   "Validation" procedure for imusr string entry widgets.  It just 
#   flags any changes to parameters, and approves all values. 
#
proc valmidefStr { new old howval } {
    if { ![string equal $old $new] && [string equal $howval "key"] } {
        idef_declare_changed
    }
    if { [string match $howval "focus*"] } {
        # puts "Focus $howval triggers valmidefStr $new"
        after 1 idef_propagate
    }
    return 1
}

#   Validation procedure for imusr numeric  entry widgets.  It is mostly
#   a call to [validateNumber] but also flags changes to parameters.  This
#   is used as the -validatecommand for numeric entry widgets on both the 
#   full setup page and the quick-change page.
#
#   Question:  Should we skip validateNumber when value is "forced"???
#
proc valmidefNum { type min max act old new win typeval howval tag } {
    global midef

    set valid [validateNumber $type $min $max $act $old $new $win $typeval $howval midef($tag) midef_help_text]

    if { $valid } {
        if { ![string equal $old $new] && [string equal $howval "key"] } {
            idef_declare_changed
        }
        if { [string match $howval "focus*"] } {
            after 1 idef_propagate
        }
    } else {  # Make error messages applicable to both windows
        set ::iquick_help_text $::midef_help_text
    }

    return $valid
}

#   Remember the fact that a parameter has changed in the ImuSR setup window.
#   Remember when it changed and activate the [apply] button.
#
proc idef_declare_changed { } {
    global idefwin idef midef
    # puts "Declare midef changed at [clock seconds]"
    if { [winfo exists $idefwin.id_apply_b] } {
        $idefwin.id_apply_b configure -state normal
	$idefwin.id_revert_b configure -text Revert
    }
    set midef(changed_at) [clock seconds]
    if { [winfo exists $::logwin.lv_autohead_b] } {
        catch logvar_update
    }
    # NO! not idef_propagate.  
    # Many, but not all, uses of idef_declare_changed should be followed by idef_propagate
    # (note that idef_propagate may execute idef_declare_changed).  See idef_changed next.
}

#  Here is a simple shorthand to execute both idef_declare_changed and idef_propagate
#
proc idef_changed { } {
    idef_declare_changed
    idef_propagate
}

#   Apply any changes in the ImuSR setup window, and close it.
proc idef_OK { } {
    idef_apply
    idef_quit
}

#   Revert/refresh ImuSR setup pages
proc idef_revert { } {
    global ilogv
    array unset ilogv *
    idef_do_update 0
    catch logvar_update
}

#   Quit the ImuSR setup window, abandoning any changes
proc idef_quit { } {
    global idefwin midef idef
    set idw [td_win_name $idefwin]
    if { $midef(changed_at) && [winfo exists $idw] } {
        #  Perhaps prompt for verification here
    }
    #  Is it correct to say "unchanged" when some changes may have been made
    #  in the quick-set window?
    set midef(changed_at) 0

    destroy $idw
}

#   Prepare a list of saved definitions excluding the one indicated by $skip.
proc idef_list_others { skip } {
    global midef
    set defs [list]
    foreach d [lsort [glob -nocomplain [file join $midef(setup_path) *.idef] ] ] {
        set d [file rootname [file tail $d]]
        if { [string compare $d $skip] } {
            lappend defs $d
        }
    }
    return $defs
}

#   Load saved ImuSR Setup
proc idef_load { } {
    global midef idef idefwin run

    if { ![td_odb odb_get_runinfo "get run state"] } { return }
    if { $run(in_progress) } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $idefwin] \
                -type ok -icon error -title "Run in progress!" \
                -message "Saved definitions cannot be loaded\nwhile a run is in progress!"
        # Update load button because obviously it was out of synch.
        td_background_update
        catch { idef_do_update 0 }
        return
    }
    set previdef $midef(setup_name)
    set idefs [ idef_list_others $previdef ]
    set newidef [ popup_selector \
        "Choose Setup" "Choose I-\u00b5SR setup to load:" \
        $idefs $midef(setup_name) single true 90 ]
    if { [ string equal $newidef "" ] } { return }
    if { [ lsearch -exact $idefs $newidef ] < 0 } { 
	beep
        timed_messageBox -timeout 20000 \
                -type ok -icon error -title "Unknown setup" \
                -message "No such I-\u00b5SR setup: \"$newidef\""
	return 
    }

    if { ![string match "Previous*" $newidef] } {
        set idef(setup_name) $newidef
    }

    #puts "Loading imusr definitions $idef(setup_path)/$newidef.idef"
    if { ! [td_odb [list odb_load_file [file join $idef(setup_path) $newidef.idef]] \
            "load I-${PREF::muchar}SR setup file"] } {
        return
    }

    idef_do_update 0

}

#   Save current imusr definitions under a new name.

proc idef_saveas { } {
    global midef idefwin

    # Ask for new idef name (displaying list of existing idefs)
    set idefs [ idef_list_others {Previous} ]
    set newidef [ popup_selector "Save As" "Choose name to save I-\u00b5SR setup:" \
            $idefs "" single false 90 ]

    # fix up name to allow only sensible characters
    set newidef [ file tail $newidef ]
    set newidef [ join [split $newidef { `~!@#$%^&*()-+={}[];:'",<>?/} ] {} ]

    if { [string length $newidef] == 0 } {
        return
    }

    # Use new name
    set midef(setup_name) $newidef

    # save current values
    idef_apply

    return
}

#   Delete imusr setup(s).  Make a list of all setup names, excluding current.
#   Ask user to select one.  Check the selection (because non-existant file
#   may have been typed in selection box).

proc idef_delete { } {
    global midef idef

    # Prompt with list of other setup names:
    set idefs [ idef_list_others $midef(setup_name) ]

    # Ask user for selections.
    set deldefs [ popup_selector \
	"Select for deletion" "Choose old I-\u00b5SR setups to delete:" \
        $idefs "" multiple true 180 ]

    # Delete each, but only if it is on valid-selection list!
    # Also, explicitly check again for matches to the current name (in case
    # name has changed under us)
    foreach m $deldefs {
        if { [lsearch -exact $idefs $m ] >= 0 && [string compare $m $midef(setup_name)] } {
            file delete -- [file join $idef(setup_path) $m.idef]
        }
    }
}

#   Make a (single) backup of the current (soon previous) ImuSR settings
proc idef_make_backup { } {
    global idef

    if { [string length $idef(setup_name)] } {
        set s [file join $idef(setup_path) $idef(setup_name).idef]
        #puts "Make backup copy $s -> [file join $idef(setup_path) Previous.idef]"
        if { [ file exists $s ] } {
            file copy -force $s [file join $idef(setup_path) Previous.idef]
        }            
    }
}

#   idef_add_camp_row: make a row for a camp variable.
#   SpecTcl uses grid rows/columns starting at 1 instead of 0.  Used rows are 
#   numbered 2,3,... (after the headings in row 1) $n is the number of the 
#   variable: 0,1... 
proc idef_add_camp_row { n } {
    global idefwin
    set f tabs.log.ilog_vars_f
    set row [expr {$n+2}]

    #puts "Add Camp row $n ($row)"
    foreach b [list del handle] im [list delete_x grab_handle] {
        [button $idefwin.$f.$b$n] configure -bg "$::PREF::revfgcol" -borderwidth 1 \
                -padx 1 -pady 1 -activebackground "$::PREF::revfgcol" \
                -highlightcolor "$::PREF::brightcol" -highlightthickness 1 \
                -image {} -text {}
    }
    $idefwin.$f.del$n configure -image delete_x -takefocus 1 -command [list idef_stop_log $n]
    $idefwin.$f.handle$n configure -image grab_handle -takefocus 0 -command {}
    idef_bind_help $f.del$n "Stop logging variable"
    idef_bind_help $f.handle$n "Drag to rearrange order"
    group_drag_bind ilog_move_var $idefwin.$f.handle$n $n [list ilog_disp_drag $n]

    label $idefwin.$f.num$n -padx 4 -text "[expr {$n+1}]."
    label $idefwin.$f.var$n -padx 7 -text {}
    idef_bind_help $f.var$n "Camp path for logged variable"
    entry $idefwin.$f.poll$n -width 4
    idef_bind_help $f.poll$n "Poll interval"
    bind $idefwin.$f.poll$n <<ApplyEntry>> idef_propagate
    grid x $idefwin.$f.del$n $idefwin.$f.handle$n -in $idefwin.$f \
            -row $row -ipadx 1 -ipady 1 -sticky nesw
    grid x x x $idefwin.$f.num$n $idefwin.$f.var$n $idefwin.$f.poll$n -in $idefwin.$f \
            -row $row -sticky w
}

#   Eliminate the n'th row of the logged-var display (should always be the last!!!)
proc idef_delete_camp_row { n } {
    global idefwin
    set f tabs.log.ilog_vars_f
    set row [expr {$n+2}]
    destroy $idefwin.$f.del$n $idefwin.$f.handle$n $idefwin.$f.num$n $idefwin.$f.var$n $idefwin.$f.poll$n
    catch { logvar_update }
}

#   Drop the $n'th logged variable
proc idef_stop_log { n } {
    global midef ilogv camplogvars

    set var [lindex $midef(logged_vars) $n]
    if { [info exists ilogv(poll$var)] } { unset ilogv(poll$var) }
    if { [info exists ilogv(valid$var)] } { unset ilogv(valid$var) }
    if { $n >= 0 } {
        set midef(logged_vars) [lreplace $midef(logged_vars) $n $n]
        set midef(logged_poll) [lreplace $midef(logged_poll) $n $n]
    }
    set midef(num_logged) [llength $midef(logged_vars)]
    set camplogvars($var) 0
    idef_changed
    catch { logvar_update }
}

#   Change order of logged variables by dragging brass knob.
proc ilog_rearrange { from to } {
    global midef
    # puts "ilog_rearrange (drag) $from $to"

    set nlv [llength $midef(logged_vars)]
    if { $from < 0 || $from >= $nlv || $to < 0 || $to >= $nlv || $from == $to } { return }
    set name [lindex $midef(logged_vars) $from]
    set poll [lindex $midef(logged_poll) $from]
    set midef(logged_vars) [linsert [lreplace $midef(logged_vars) $from $from] $to $name]
    set midef(logged_poll) [linsert [lreplace $midef(logged_poll) $from $from] $to $poll]
    idef_changed
    catch { logvar_update }
}

proc ilog_disp_drag { index args } {
    global midef
    return "  [lindex $midef(logged_vars) $index]"
}


#proc ggl_initialize { } {
#    global gglwin idefwin midef
#
#    if { [winfo exists $gglwin.ggl_head_l] } {
#        # if window exists, make it visible
#        wm deiconify $gglwin
#        raise $gglwin
#    } else {
#        # window is not built yet.  Build main window
#        toplevel $gglwin
#        wm transient $gglwin [td_win_name $idefwin]
#        ggl_details_ui $gglwin
#    }
#    focus $gglwin.ggl_OK_b
#    wm title $gglwin "GGL DAC"
#}

#proc ggl_close { } {
#    global gglwin 
#    if { [winfo exists $gglwin] } { destroy $gglwin }
#}

proc ggl_dac_range { } {
    global midef
    set midef(ggl_dac_range) [lsearch -exact $midef(ggl_dac_range_list) $midef(ggl_range_label) ]
}

#  !!!!! IMPORTANT !!!!!  When adding new items to the idef/midef arrays, they may need 
#                         to be added to the list used in proc idef_apply!

set idef(sweep_device) "DAC"
set idef(sweep_ins_name) ""
set idef(logged_vars) [list]
set idef(num_logged) 0
set idef(scan_title) ""
set idef(full_tol_test) ""
set idef(setup_path) "./"
set midef(sweep_device) "DAC"
set midef(sweep_device_label) "DAC"
set midef(sweep_ins_name) ""
set midef(sweep_ins_type) "GGL_DAC"
set midef(logged_vars) [list]
set midef(num_logged) 0
set midef(scan_title) ""
set midef(ggl_dac_range) 1
set midef(ggl_dac_range_list) [list \
        {  0 to 5 V} {  0 to 10 V} { -5 to 5 V} { -10 to 10 V} { -2.5 to 2.5 V} { -2.5 to 7.5 V}]
set midef(fixlist) {}

array set ilogv {}

if { $idefwin == "" } {
    # Testing, so provide some values
    set midef(changed_at) 0
    set idef(sweep_device) DAC
    set idef(sweep_ins_name) {}
    set idef(setup_path) {saves}
    set idef(setup_name) {test}
    set run(in_progress) 0
    set rtitles(sample) ""
    set rtitles(temperature) ""

    if { [string length [info proc odb_set_idef]] == 0 } {
        proc odb_set_idef {} {}
    }

    set idef(logged_vars) [mui_camp_cmd sysGetLoggedVars]

    set logwin .logwin
    source logvar_extra.tcl
    set gglwin $idefwin.ggl
    set idef(ggl_dac_range) 1
    source [file join $muisrc ggl_details.ui.tcl]


    # stand-alone testing, so start immediately!
    isetup_initialize

}

