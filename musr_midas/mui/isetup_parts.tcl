#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
if { [catch { set mui_utils_loaded }] } {
    source "mui_utils.tcl"
}
# interface generated by SpecTcl version 1.2 from /home/asnd/SpecTcl/mui/sweeps.ui
#   root     is the parent window for this user interface

proc sweeps_ui {root args} {

	# this treats "." as a special case

	if {$root == "."} {
	    set base ""
	} else {
	    set base $root
	}
    
	frame $base.frame#1

	frame $base.frame#2 \
		-borderwidth 2 \
		-height 2 \
		-relief sunken

	frame $base.frame#3 \
		-borderwidth 2 \
		-height 2 \
		-relief sunken

	label $base.sw_head_l \
		-borderwidth 3 \
		-text {Sweeps Configuration}
	catch {
		$base.sw_head_l configure \
			-font muTitleFont
	}

	label $base.sw_sweep_dev_l \
		-text {Sweep device:}

	menubutton $base.sw_sweep_dev_mb \
		-highlightcolor $PREF::brightcol \
		-highlightthickness 1 \
		-justify left \
		-menu "$base.sw_sweep_dev_mb.menu" \
		-pady 4 \
		-relief raised \
		-takefocus 1 \
		-textvariable midef(sweep_device_label)

	button $base.sw_range_inter_b \
		-padx 4 \
		-pady 3 \
		-takefocus 1 \
		-text Int/range

	button $base.sw_details_b \
		-command idef_set_details \
		-padx 4 \
		-pady 3 \
		-takefocus 1 \
		-text Details

	label $base.sw_htitle_l \
		-text {Label for scan values:}

	entry $base.sw_htitle_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(scan_title) \
		-width 10

	label $base.sw_min_l \
		-text {Set Limits:          Min:}

	entry $base.sw_min_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(min) \
		-validate all \
		-validatecommand "valmidefNum f {} {} %d %s %P $base.sw_min_e %v %V midef(min)" \
		-width 10

	label $base.sw_max_l \
		-text { Max:}

	entry $base.sw_max_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(max) \
		-validate all \
		-validatecommand "valmidefNum f {} {} %d %s %P $base.sw_max_e %v %V midef(max)" \
		-width 10

	label $base.sw_settle_l \
		-text {Step settle time:}

	entry $base.sw_settle_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_settle) \
		-validate all \
		-validatecommand "valmidefNum d 0 100000 %d %s %P $base.sw_settle_e %v %V midef(sweep_settle)" \
		-width 10

	label $base.sw_ms_l \
		-text ms

	label $base.sw_number_l \
		-text {Number of sweeps:}

	entry $base.sw_num_sweeps_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(num_sweeps) \
		-validate all \
		-validatecommand "valmidefNum d 0 1000 %d %s %P $base.sw_num_sweeps_e %v %V midef(num_sweeps)" \
		-width 10

	label $base.sw_continuous_l \
		-text { 0 for continuous}

	label $base.sw_from_l \
		-text {Sweep from:}

	entry $base.sw_from_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_from) \
		-validate all \
		-validatecommand "valmidefNum f \$midef(min) \$midef(max) %d %s %P $base.sw_from_e %v %V midef(sweep_from)" \
		-width 15

	label $base.sw_from_un_l \
		-padx 3 \
		-textvariable midef(sweep_units)

	label $base.sw_to_l \
		-text {Sweep to:}

	entry $base.sw_to_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_to) \
		-validate all \
		-validatecommand "valmidefNum f \$midef(min) \$midef(max) %d %s %P $base.sw_to_e %v %V midef(sweep_to)" \
		-width 15

	label $base.sw_to_un_l \
		-padx 3 \
		-textvariable midef(sweep_units)

	label $base.sw_step_l \
		-text {Sweep step:}

	entry $base.sw_step_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_incr) \
		-validate all \
		-validatecommand "valmidefNum f \$midef(min) \$midef(max) %d %s %P $base.sw_step_e %v %V midef(sweep_incr)" \
		-width 15

	label $base.sw_step_un_l \
		-padx 3 \
		-textvariable midef(sweep_units)

	menu $base.sw_sweep_dev_mb.menu \
		-tearoff 0


	# Add contents to menus

	$base.sw_sweep_dev_mb.menu add command\
		-command {idef_sweep_device DAC {}}\
		-label {DAC       }
	$base.sw_sweep_dev_mb.menu add separator

	# Geometry management

	grid $base.frame#1 -in $root	-row 2 -column 3  \
		-columnspan 3 \
		-sticky ew
	grid $base.frame#2 -in $root	-row 3 -column 2  \
		-columnspan 4 \
		-pady 8 \
		-sticky ew
	grid $base.frame#3 -in $root	-row 7 -column 2  \
		-columnspan 4 \
		-pady 8 \
		-sticky ew
	grid $base.sw_head_l -in $root	-row 1 -column 2  \
		-columnspan 4 \
		-ipadx 10 \
		-ipady 7 \
		-pady 3 \
		-sticky ew
	grid $base.sw_sweep_dev_l -in $root	-row 2 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.sw_sweep_dev_mb -in $base.frame#1	-row 1 -column 1  \
		-sticky ew
	grid $base.sw_range_inter_b -in $base.frame#1	-row 1 -column 2  \
		-sticky e
	grid $base.sw_details_b -in $base.frame#1	-row 1 -column 3  \
		-sticky e
	grid $base.sw_htitle_l -in $root	-row 4 -column 2  \
		-sticky w
	grid $base.sw_htitle_e -in $root	-row 4 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_min_l -in $root	-row 5 -column 2  \
		-sticky w
	grid $base.sw_min_e -in $root	-row 5 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_max_l -in $root	-row 5 -column 4  \
		-sticky w
	grid $base.sw_max_e -in $root	-row 5 -column 5  \
		-pady 2 \
		-sticky ew
	grid $base.sw_settle_l -in $root	-row 6 -column 2  \
		-sticky w
	grid $base.sw_settle_e -in $root	-row 6 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_ms_l -in $root	-row 6 -column 4  \
		-sticky w
	grid $base.sw_number_l -in $root	-row 8 -column 2  \
		-sticky w
	grid $base.sw_num_sweeps_e -in $root	-row 8 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_continuous_l -in $root	-row 8 -column 4  \
		-columnspan 2 \
		-sticky w
	grid $base.sw_from_l -in $root	-row 9 -column 2  \
		-sticky w
	grid $base.sw_from_e -in $root	-row 9 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.sw_from_un_l -in $root	-row 9 -column 5  \
		-sticky w
	grid $base.sw_to_l -in $root	-row 10 -column 2  \
		-sticky w
	grid $base.sw_to_e -in $root	-row 10 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.sw_to_un_l -in $root	-row 10 -column 5  \
		-sticky w
	grid $base.sw_step_l -in $root	-row 11 -column 2  \
		-sticky w
	grid $base.sw_step_e -in $root	-row 11 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.sw_step_un_l -in $root	-row 11 -column 5  \
		-sticky w

	# Resize behavior management

	grid rowconfigure $root 1 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 2 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 3 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 5 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 6 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 7 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 8 -weight 0 -minsize 9 -pad 0
	grid rowconfigure $root 9 -weight 0 -minsize 8 -pad 0
	grid rowconfigure $root 10 -weight 0 -minsize 8 -pad 0
	grid rowconfigure $root 11 -weight 0 -minsize 10 -pad 0
	grid rowconfigure $root 12 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $root 1 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $root 2 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $root 3 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 4 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $root 5 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 6 -weight 0 -minsize 16 -pad 0

	grid rowconfigure $base.frame#1 1 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $base.frame#1 1 -weight 1 -minsize 34 -pad 0
	grid columnconfigure $base.frame#1 2 -weight 1 -minsize 85 -pad 0
	grid columnconfigure $base.frame#1 3 -weight 0 -minsize 2 -pad 0
# additional interface code

# end additional interface code

}


# Allow interface to be run "stand-alone" for testing

catch {
    if [info exists embed_args] {
	# we are running in the plugin
	sweeps_ui .
    } else {
	# we are running in stand-alone mode
	if {$argv0 == [info script]} {
	    wm title . "Testing sweeps_ui"
	    sweeps_ui .
	}
    }
}
#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
if { [catch { set mui_utils_loaded }] } {
    source "mui_utils.tcl"
}
# interface generated by SpecTcl version 1.2 from /home/asnd/SpecTcl/mui/toggles.ui
#   root     is the parent window for this user interface

proc toggles_ui {root args} {

	# this treats "." as a special case

	if {$root == "."} {
	    set base ""
	} else {
	    set base $root
	}
    
	frame $base.frame#1 \
		-borderwidth 1 \
		-relief sunken \
		-width 2

	frame $base.frame#2 \
		-borderwidth 1 \
		-relief sunken \
		-width 2

	label $base.mod_head_l \
		-borderwidth 3 \
		-text {Modulation Configuration}
	catch {
		$base.mod_head_l configure \
			-font muTitleFont
	}

	label $base.mod1_l \
		-text {Main (inner) modulation}

	label $base.mod2_l \
		-text {Second (outer) modulation}

	label $base.mod1_type_l \
		-text {Modulation type: }

	menubutton $base.mod1_type_mb \
		-highlightcolor $PREF::brightcol \
		-highlightthickness 1 \
		-justify left \
		-menu "$base.mod1_type_mb.menu" \
		-relief raised \
		-takefocus 1 \
		-textvariable midef(mod_type_1)

	label $base.mod2_type_l \
		-text {Modulation type: }

	menubutton $base.mod2_type_mb \
		-highlightcolor $PREF::brightcol \
		-highlightthickness 1 \
		-justify left \
		-menu "$base.mod2_type_mb.menu" \
		-relief raised \
		-takefocus 1 \
		-textvariable midef(mod_type_2)

	label $base.mod1_num_l \
		-text {Number of toggles: }

	entry $base.mod1_num_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(mod_num_1) \
		-validate all \
		-validatecommand "valmidefNum d 0 1000 %d %s %P $base.mod1_num_e %v %V mod_num_1" \
		-width 10

	label $base.mod2_num_l \
		-text {Number of toggles: }

	entry $base.mod2_num_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod_num_2) \
		-validate all \
		-validatecommand "valmidefNum d 0 100 %d %s %P $base.mod2_num_e %v %V mod_num_2" \
		-width 12

	label $base.mod1_settle_l \
		-text {Settling time (ms):}

	entry $base.mod1_settle_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod_settle_1) \
		-validate all \
		-validatecommand "valmidefNum f 0 10000 %d %s %P $base.mod1_settle_e %v %V mod_settle_1" \
		-width 10

	label $base.mod2_settle_l \
		-text {Settling time (ms):}

	entry $base.mod2_settle_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod_settle_2) \
		-validate all \
		-validatecommand "valmidefNum f 0 10000 %d %s %P $base.mod2_settle_e %v %V mod_settle_2" \
		-width 12

	label $base.mod1_value_l \
		-text Value:

	entry $base.mod1_value_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod_value_1) \
		-validate all \
		-validatecommand "valmidefNum f -2e9 2e9 %d %s %P $base.mod1_value_e %v %V mod_value_1" \
		-width 12

	label $base.mod2_value_l \
		-text Value:

	entry $base.mod2_value_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod_value_2) \
		-validate all \
		-validatecommand "valmidefNum f -2e9 2e9 %d %s %P $base.mod2_value_e %v %V mod_value_2" \
		-width 12

	label $base.mod1_suff_l \
		-text Suffixes:

	entry $base.mod1_suf1_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod1_suffix1) \
		-validate key \
		-validatecommand {valmidefStr %P %s %V} \
		-width 5

	entry $base.mod1_suf2_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod1_suffix2) \
		-validate key \
		-validatecommand {valmidefStr %P %s %V} \
		-width 5

	label $base.mod2_suff_l \
		-text Suffixes:

	entry $base.mod2_suf1_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod2_suffix1) \
		-validate key \
		-validatecommand {valmidefStr %P %s %V} \
		-width 5

	entry $base.mod2_suf2_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod2_suffix2) \
		-validate key \
		-validatecommand {valmidefStr %P %s %V} \
		-width 5

	button $base.mod_exchange_b \
		-command idef_exchange_mod \
		-padx 9 \
		-pady 2 \
		-text {Exchange inner/outer}

	label $base.mod_fast_l \
		-text {Fast modulation:}

	radiobutton $base.mod_fast_enable_rb \
		-command idef_changed \
		-justify left \
		-pady 0 \
		-text Enable \
		-value 1 \
		-variable midef(mod_fast_enable)

	label $base.mod_out_tol_l \
		-text {When beam out of tolerance:}

	radiobutton $base.mod_fast_disable_rb \
		-command idef_changed \
		-justify left \
		-pady 0 \
		-text Disable \
		-value 0 \
		-variable midef(mod_fast_enable)

	radiobutton $base.mod_toggle_always_rb \
		-command idef_changed \
		-justify left \
		-pady 0 \
		-text {Keep toggling while waiting} \
		-value 1 \
		-variable midef(toggle_always)

	label $base.mod_fast_suff_l \
		-text Suffixes:

	entry $base.mod_fast_suf1_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod_fast_suffix1) \
		-validate key \
		-validatecommand {valmidefStr %P %s %V} \
		-width 5

	entry $base.mod_fast_suf2_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(mod_fast_suffix2) \
		-validate key \
		-validatecommand {valmidefStr %P %s %V} \
		-width 5

	radiobutton $base.mod_toggle_wait_rb \
		-command idef_changed \
		-justify left \
		-pady 0 \
		-text {Pause toggling} \
		-value 0 \
		-variable midef(toggle_always)

	button $base.mod_pulser_b \
		-command {pulser_initialize ; idef_changed} \
		-padx 9 \
		-pady 2 \
		-text {Set Pulser}

	menu $base.mod1_type_mb.menu \
		-tearoff 0

	menu $base.mod2_type_mb.menu \
		-tearoff 0


	# Add contents to menus

	$base.mod1_type_mb.menu add radiobutton\
		-value None\
		-variable midef(mod_type_1)\
		-command idef_changed\
		-label None\
		-underline 0
	$base.mod1_type_mb.menu add radiobutton\
		-value Hard\
		-variable midef(mod_type_1)\
		-command idef_changed\
		-label Hard\
		-underline 0
	$base.mod1_type_mb.menu add radiobutton\
		-value Soft\
		-variable midef(mod_type_1)\
		-command idef_changed\
		-label Soft\
		-underline 0
	$base.mod1_type_mb.menu add radiobutton\
		-value Ref\
		-variable midef(mod_type_1)\
		-command idef_changed\
		-label Reference\
		-underline 0

	$base.mod2_type_mb.menu add radiobutton\
		-value None\
		-variable midef(mod_type_2)\
		-command idef_changed\
		-label None\
		-underline 0
	$base.mod2_type_mb.menu add radiobutton\
		-value Hard\
		-variable midef(mod_type_2)\
		-command idef_changed\
		-label Hard\
		-underline 0
	$base.mod2_type_mb.menu add radiobutton\
		-value Soft\
		-variable midef(mod_type_2)\
		-command idef_changed\
		-label Soft\
		-underline 0
	$base.mod2_type_mb.menu add radiobutton\
		-value Ref\
		-variable midef(mod_type_2)\
		-command idef_changed\
		-label Reference\
		-underline 0

	# Geometry management

	grid $base.frame#1 -in $root	-row 2 -column 5  \
		-padx 12 \
		-pady 4 \
		-rowspan 6 \
		-sticky ns
	grid $base.frame#2 -in $root	-row 10 -column 5  \
		-padx 12 \
		-pady 2 \
		-rowspan 4 \
		-sticky ns
	grid $base.mod_head_l -in $root	-row 1 -column 2  \
		-columnspan 7 \
		-ipadx 10 \
		-ipady 7 \
		-pady 3
	grid $base.mod1_l -in $root	-row 2 -column 2  \
		-columnspan 3 \
		-pady 2 \
		-sticky w
	grid $base.mod2_l -in $root	-row 2 -column 6  \
		-columnspan 3 \
		-pady 2 \
		-sticky w
	grid $base.mod1_type_l -in $root	-row 3 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.mod1_type_mb -in $root	-row 3 -column 3  \
		-columnspan 2 \
		-sticky ew
	grid $base.mod2_type_l -in $root	-row 3 -column 6  \
		-pady 2 \
		-sticky w
	grid $base.mod2_type_mb -in $root	-row 3 -column 7  \
		-columnspan 2 \
		-sticky ew
	grid $base.mod1_num_l -in $root	-row 4 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.mod1_num_e -in $root	-row 4 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.mod2_num_l -in $root	-row 4 -column 6  \
		-pady 2 \
		-sticky w
	grid $base.mod2_num_e -in $root	-row 4 -column 7  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.mod1_settle_l -in $root	-row 5 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.mod1_settle_e -in $root	-row 5 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.mod2_settle_l -in $root	-row 5 -column 6  \
		-pady 2 \
		-sticky w
	grid $base.mod2_settle_e -in $root	-row 5 -column 7  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.mod1_value_l -in $root	-row 6 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.mod1_value_e -in $root	-row 6 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.mod2_value_l -in $root	-row 6 -column 6  \
		-pady 2 \
		-sticky w
	grid $base.mod2_value_e -in $root	-row 6 -column 7  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.mod1_suff_l -in $root	-row 7 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.mod1_suf1_e -in $root	-row 7 -column 3  \
		-pady 2 \
		-sticky w
	grid $base.mod1_suf2_e -in $root	-row 7 -column 4  \
		-pady 2 \
		-sticky e
	grid $base.mod2_suff_l -in $root	-row 7 -column 6  \
		-pady 2 \
		-sticky w
	grid $base.mod2_suf1_e -in $root	-row 7 -column 7  \
		-pady 2 \
		-sticky w
	grid $base.mod2_suf2_e -in $root	-row 7 -column 8  \
		-pady 2 \
		-sticky e
	grid $base.mod_exchange_b -in $root	-row 8 -column 2  \
		-columnspan 7 \
		-pady 3 \
		-sticky n
	grid $base.mod_fast_l -in $root	-row 10 -column 2  \
		-sticky w
	grid $base.mod_fast_enable_rb -in $root	-row 10 -column 3  \
		-columnspan 2 \
		-padx 2 \
		-sticky w
	grid $base.mod_out_tol_l -in $root	-row 10 -column 6  \
		-columnspan 3 \
		-sticky w
	grid $base.mod_fast_disable_rb -in $root	-row 11 -column 3  \
		-columnspan 2 \
		-padx 2 \
		-sticky w
	grid $base.mod_toggle_always_rb -in $root	-row 11 -column 6  \
		-columnspan 3 \
		-padx 10 \
		-sticky w
	grid $base.mod_fast_suff_l -in $root	-row 12 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.mod_fast_suf1_e -in $root	-row 12 -column 3  \
		-pady 2 \
		-sticky w
	grid $base.mod_fast_suf2_e -in $root	-row 12 -column 4  \
		-pady 2 \
		-sticky e
	grid $base.mod_toggle_wait_rb -in $root	-row 12 -column 6  \
		-columnspan 3 \
		-padx 10 \
		-sticky w
	grid $base.mod_pulser_b -in $root	-row 13 -column 3  \
		-columnspan 2 \
		-pady 3 \
		-sticky ew

	# Resize behavior management

	grid rowconfigure $root 1 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 2 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 3 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 5 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 6 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 7 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 8 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 9 -weight 0 -minsize 11 -pad 0
	grid rowconfigure $root 10 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 11 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 12 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 13 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 14 -weight 0 -minsize 13 -pad 0
	grid columnconfigure $root 1 -weight 0 -minsize 12 -pad 0
	grid columnconfigure $root 2 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 3 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 5 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 6 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 7 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 8 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 9 -weight 0 -minsize 12 -pad 0
# additional interface code



# end additional interface code

}


# Allow interface to be run "stand-alone" for testing

catch {
    if [info exists embed_args] {
	# we are running in the plugin
	toggles_ui .
    } else {
	# we are running in stand-alone mode
	if {$argv0 == [info script]} {
	    wm title . "Testing toggles_ui"
	    toggles_ui .
	}
    }
}
#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
if { [catch { set mui_utils_loaded }] } {
    source "mui_utils.tcl"
}
# interface generated by SpecTcl version 1.2 from /home/asnd/SpecTcl/mui/presets.ui
#   root     is the parent window for this user interface

proc presets_ui {root args} {

	# this treats "." as a special case

	if {$root == "."} {
	    set base ""
	} else {
	    set base $root
	}
    
	frame $base.frame#1 \
		-borderwidth 1 \
		-height 2 \
		-relief sunken

	frame $base.frame#2 \
		-borderwidth 1 \
		-height 2 \
		-relief sunken

	label $base.preset_head_l \
		-borderwidth 3 \
		-text {Preset Counts Configuration}
	catch {
		$base.preset_head_l configure \
			-font muTitleFont
	}

	label $base.preset_const_l \
		-justify left \
		-text {Preset scaler will count data
events or clock ticks}

	label $base.preset_source_l \
		-justify left \
		-text {Preset scaler will count muons
or total positrons}

	radiobutton $base.preset_const_counts_rb \
		-command idef_changed \
		-text {Constant Counts} \
		-value 0 \
		-variable midef(constant_time)

	radiobutton $base.preset_norm_e_rb \
		-command idef_changed \
		-text {Normalize on total e} \
		-value 1 \
		-variable midef(norm_e)

	radiobutton $base.preset_const_time_rb \
		-command idef_changed \
		-text {Constant Time} \
		-value 1 \
		-variable midef(constant_time)

	radiobutton $base.preset_norm_mu_rb \
		-command idef_changed \
		-text {Normalize on �} \
		-value 0 \
		-variable midef(norm_e)

	label $base.preset_num_l \
		-text {Number of presets: }

	entry $base.preset_num_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(num_presets) \
		-validate all \
		-validatecommand "valmidefNum d 1 100 %d %s %P $base.preset_num_e %v %V num_presets" \
		-width 10

	label $base.preset_set_l \
		-text {Preset counts:}

	entry $base.preset_set_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(preset_counts) \
		-validate all \
		-validatecommand "valmidefNum f 0 1.0e9 %d %s %P $base.preset_set_e %v %V preset_counts" \
		-width 10


	# Add contents to menus

	# Geometry management

	grid $base.frame#1 -in $root	-row 2 -column 2  \
		-columnspan 5 \
		-pady 10 \
		-sticky ew
	grid $base.frame#2 -in $root	-row 6 -column 2  \
		-columnspan 5 \
		-pady 10 \
		-sticky ew
	grid $base.preset_head_l -in $root	-row 1 -column 2  \
		-columnspan 5 \
		-ipadx 10 \
		-ipady 7 \
		-pady 3 \
		-sticky ew
	grid $base.preset_const_l -in $root	-row 3 -column 2  \
		-columnspan 2 \
		-pady 3 \
		-sticky w
	grid $base.preset_source_l -in $root	-row 3 -column 5  \
		-columnspan 2 \
		-pady 3 \
		-sticky w
	grid $base.preset_const_counts_rb -in $root	-row 4 -column 2  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_norm_e_rb -in $root	-row 4 -column 5  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_const_time_rb -in $root	-row 5 -column 2  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_norm_mu_rb -in $root	-row 5 -column 5  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_num_l -in $root	-row 7 -column 3  \
		-pady 2 \
		-sticky w
	grid $base.preset_num_e -in $root	-row 7 -column 5  \
		-pady 2 \
		-sticky ew
	grid $base.preset_set_l -in $root	-row 8 -column 3  \
		-pady 2 \
		-sticky w
	grid $base.preset_set_e -in $root	-row 8 -column 5  \
		-pady 2 \
		-sticky ew

	# Resize behavior management

	grid rowconfigure $root 1 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 2 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 3 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 5 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 6 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 7 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 8 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 9 -weight 0 -minsize 44 -pad 0
	grid columnconfigure $root 1 -weight 0 -minsize 12 -pad 0
	grid columnconfigure $root 2 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 3 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $root 4 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $root 5 -weight 0 -minsize 22 -pad 0
	grid columnconfigure $root 6 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 7 -weight 0 -minsize 12 -pad 0
# additional interface code

# end additional interface code

}


# Allow interface to be run "stand-alone" for testing

catch {
    if [info exists embed_args] {
	# we are running in the plugin
	presets_ui .
    } else {
	# we are running in stand-alone mode
	if {$argv0 == [info script]} {
	    wm title . "Testing presets_ui"
	    presets_ui .
	}
    }
}
#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
if { [catch { set mui_utils_loaded }] } {
    source "mui_utils.tcl"
}
# interface generated by SpecTcl version 1.2 from /home/asnd/SpecTcl/mui/toler.ui
#   root     is the parent window for this user interface

proc toler_ui {root args} {

	# this treats "." as a special case

	if {$root == "."} {
	    set base ""
	} else {
	    set base $root
	}
    
	frame $base.frame#1

	frame $base.frame#2 \
		-borderwidth 1 \
		-height 2 \
		-relief sunken

	frame $base.frame#3 \
		-borderwidth 1 \
		-height 2 \
		-relief sunken

	label $base.toler_head_l \
		-borderwidth 3 \
		-text {Rate Tolerance Configuration}
	catch {
		$base.toler_head_l configure \
			-font muTitleFont
	}

	label $base.toler_const_l \
		-justify left \
		-text {Preset scaler will count data
events or clock ticks}

	label $base.toler_source_l \
		-justify left \
		-text {When out of tolerance, inner
modulation should:}

	radiobutton $base.toler_const_counts_rb \
		-command idef_changed \
		-text {Constant Counts} \
		-value 0 \
		-variable midef(constant_time)

	radiobutton $base.toler_toggle_always_rb \
		-command idef_changed \
		-text {Continue toggling} \
		-value 1 \
		-variable midef(toggle_always)

	radiobutton $base.toler_const_time_rb \
		-command idef_changed \
		-text {Constant Time} \
		-value 1 \
		-variable midef(constant_time)

	radiobutton $base.toler_toggle_wait_rb \
		-command idef_changed \
		-text {Wait for beam} \
		-value 0 \
		-variable midef(toggle_always)

	checkbutton $base.toler_enable_cb \
		-command idef_changed \
		-text {Perform rate tolerance checks} \
		-variable midef(toler_enable)

	label $base.toler_enable_l \
		-justify left \
		-textvariable midef(tolerance_now)

	checkbutton $base.toler_renormbegin_cb \
		-command idef_changed \
		-text {Renormalize at start of run} \
		-variable midef(renorm_at_begin)

	label $base.toler_timeout_l \
		-justify left \
		-text {Normalization timeout:}

	entry $base.toler_timeout_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(norm_timeout) \
		-validate all \
		-validatecommand "valmidefNum d 0 300 %d %s %P $base.toler_timeout_e %v %V norm_timeout" \
		-width 10

	label $base.toler_timeout_un_l \
		-text s

	label $base.toler_delay_l \
		-text {Wait, after tolerance restored:}

	entry $base.toler_delay_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(toler_delay) \
		-validate all \
		-validatecommand "valmidefNum f 0 120 %d %s %P $base.toler_delay_e %v %V toler_delay" \
		-width 10

	label $base.toler_delay_un_l \
		-text s

	label $base.toler_set_l \
		-text Tolerance:

	entry $base.toler_set_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(tolerance) \
		-validate all \
		-validatecommand "valmidefNum f 0 300 %d %s %P $base.toler_set_e %v %V tolerance" \
		-width 10

	label $base.toler_set_un_l \
		-text %

	button $base.toler_renorm_b \
		-command toler_renormalize \
		-padx 9 \
		-pady 3 \
		-text {Renormalize Rate}

	button $base.toler_pulser_b \
		-command pulser_initialize \
		-padx 9 \
		-pady 3 \
		-text {Set Pulser}


	# Add contents to menus

	# Geometry management

	grid $base.frame#1 -in $root	-row 6 -column 2  \
		-columnspan 4 \
		-sticky ew
	grid $base.frame#2 -in $root	-row 5 -column 2  \
		-columnspan 4 \
		-pady 9 \
		-sticky ew
	grid $base.frame#3 -in $root	-row 7 -column 2  \
		-columnspan 4 \
		-pady 9 \
		-sticky ew
	grid $base.toler_head_l -in $root	-row 1 -column 2  \
		-columnspan 4 \
		-ipadx 10 \
		-ipady 7 \
		-pady 3 \
		-sticky ew
	grid $base.toler_const_l -in $root	-row 2 -column 2  \
		-pady 3 \
		-sticky w
	grid $base.toler_source_l -in $root	-row 2 -column 4  \
		-columnspan 2 \
		-pady 3 \
		-sticky w
	grid $base.toler_const_counts_rb -in $root	-row 3 -column 2  \
		-sticky w
	grid $base.toler_toggle_always_rb -in $root	-row 3 -column 4  \
		-columnspan 2 \
		-sticky w
	grid $base.toler_const_time_rb -in $root	-row 4 -column 2  \
		-sticky w
	grid $base.toler_toggle_wait_rb -in $root	-row 4 -column 4  \
		-columnspan 2 \
		-sticky w
	grid $base.toler_enable_cb -in $base.frame#1	-row 1 -column 1 
	grid $base.toler_enable_l -in $base.frame#1	-row 1 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.toler_renormbegin_cb -in $base.frame#1	-row 2 -column 1  \
		-sticky w
	grid $base.toler_timeout_l -in $root	-row 8 -column 2  \
		-pady 2 \
		-sticky e
	grid $base.toler_timeout_e -in $root	-row 8 -column 4  \
		-pady 2 \
		-sticky ew
	grid $base.toler_timeout_un_l -in $root	-row 8 -column 5  \
		-pady 2 \
		-sticky w
	grid $base.toler_delay_l -in $root	-row 9 -column 2  \
		-pady 2 \
		-sticky e
	grid $base.toler_delay_e -in $root	-row 9 -column 4  \
		-pady 2 \
		-sticky ew
	grid $base.toler_delay_un_l -in $root	-row 9 -column 5  \
		-pady 2 \
		-sticky w
	grid $base.toler_set_l -in $root	-row 10 -column 2  \
		-pady 2 \
		-sticky e
	grid $base.toler_set_e -in $root	-row 10 -column 4  \
		-pady 2 \
		-sticky ew
	grid $base.toler_set_un_l -in $root	-row 10 -column 5  \
		-pady 2 \
		-sticky w
	grid $base.toler_renorm_b -in $root	-row 11 -column 2  \
		-pady 9 \
		-sticky e
	grid $base.toler_pulser_b -in $root	-row 11 -column 4  \
		-pady 9 \
		-sticky ew

	# Resize behavior management

	grid rowconfigure $root 1 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 2 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 3 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 5 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 6 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 7 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 8 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 9 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 10 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 11 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $root 1 -weight 0 -minsize 13 -pad 0
	grid columnconfigure $root 2 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 3 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $root 4 -weight 0 -minsize 22 -pad 0
	grid columnconfigure $root 5 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 6 -weight 0 -minsize 12 -pad 0

	grid rowconfigure $base.frame#1 1 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $base.frame#1 2 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $base.frame#1 1 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $base.frame#1 2 -weight 1 -minsize 30 -pad 0
# additional interface code

# end additional interface code

}


# Allow interface to be run "stand-alone" for testing

catch {
    if [info exists embed_args] {
	# we are running in the plugin
	toler_ui .
    } else {
	# we are running in stand-alone mode
	if {$argv0 == [info script]} {
	    wm title . "Testing toler_ui"
	    toler_ui .
	}
    }
}
#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
if { [catch { set mui_utils_loaded }] } {
    source "mui_utils.tcl"
}
# interface generated by SpecTcl version 1.2 from /home/asnd/SpecTcl/mui/ilogged.ui
#   root     is the parent window for this user interface

proc ilogged_ui {root args} {

	# this treats "." as a special case

	if {$root == "."} {
	    set base ""
	} else {
	    set base $root
	}
    
	frame $base.ilog_vars_f

	frame $base.frame#3

	frame $base.frame#4 \
		-borderwidth 1 \
		-height 2 \
		-relief sunken

	label $base.ilog_title_l \
		-borderwidth 3 \
		-text {Logged Camp variables}
	catch {
		$base.ilog_title_l configure \
			-font muTitleFont
	}

	menubutton $base.ilog_configure_mb \
		-direction flush \
		-highlightcolor $PREF::brightcol \
		-highlightthickness 1 \
		-menu "$base.ilog_configure_mb.menu" \
		-padx 6 \
		-relief raised \
		-takefocus 1 \
		-text Variables

	label $base.label#10 \
		-text {Camp host:}

	label $base.ilog_camphost_l \
		-anchor w \
		-textvariable midas(camp_host)

	label $base.label#2 \
		-padx 3 \
		-pady 4

	label $base.label#17 \
		-padx 3 \
		-pady 4

	label $base.ilog_name_l \
		-justify left \
		-padx 8 \
		-text {Camp variable}

	label $base.ilog_poll_l \
		-justify left \
		-text {Poll int}

	menu $base.ilog_configure_mb.menu \
		-tearoff 0


	# Add contents to menus

	$base.ilog_configure_mb.menu add command

	# Geometry management

	grid $base.ilog_vars_f -in $root	-row 5 -column 2 
	grid $base.frame#3 -in $root	-row 3 -column 2  \
		-sticky ew
	grid $base.frame#4 -in $root	-row 4 -column 2  \
		-pady 8 \
		-sticky ew
	grid $base.ilog_title_l -in $root	-row 2 -column 2  \
		-ipadx 10 \
		-ipady 7 \
		-pady 3
	grid $base.ilog_configure_mb -in $base.frame#3	-row 1 -column 1  \
		-ipady 1 \
		-sticky w
	grid $base.label#10 -in $base.frame#3	-row 1 -column 3  \
		-sticky e
	grid $base.ilog_camphost_l -in $base.frame#3	-row 1 -column 4  \
		-padx 8
	grid $base.label#2 -in $base.ilog_vars_f	-row 1 -column 1 
	grid $base.label#17 -in $base.ilog_vars_f	-row 1 -column 2 
	grid $base.ilog_name_l -in $base.ilog_vars_f	-row 1 -column 4  \
		-sticky w
	grid $base.ilog_poll_l -in $base.ilog_vars_f	-row 1 -column 5  \
		-sticky w

	# Resize behavior management

	grid rowconfigure $base.frame#3 1 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $base.frame#3 1 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $base.frame#3 2 -weight 0 -minsize 20 -pad 0
	grid columnconfigure $base.frame#3 3 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $base.frame#3 4 -weight 0 -minsize 2 -pad 0

	grid rowconfigure $root 1 -weight 1 -minsize 10 -pad 0
	grid rowconfigure $root 2 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 3 -weight 0 -minsize 5 -pad 0
	grid rowconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 5 -weight 0 -minsize 26 -pad 0
	grid rowconfigure $root 6 -weight 1 -minsize 16 -pad 0
	grid columnconfigure $root 1 -weight 1 -minsize 15 -pad 0
	grid columnconfigure $root 2 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $root 3 -weight 1 -minsize 17 -pad 0

	grid rowconfigure $base.ilog_vars_f 1 -weight 0 -minsize 35 -pad 0
	grid rowconfigure $base.ilog_vars_f 2 -weight 0 -minsize 8 -pad 0
	grid columnconfigure $base.ilog_vars_f 1 -weight 0 -minsize 10 -pad 0
	grid columnconfigure $base.ilog_vars_f 2 -weight 0 -minsize 7 -pad 0
	grid columnconfigure $base.ilog_vars_f 3 -weight 0 -minsize 8 -pad 0
	grid columnconfigure $base.ilog_vars_f 4 -weight 0 -minsize 22 -pad 0
	grid columnconfigure $base.ilog_vars_f 5 -weight 0 -minsize 30 -pad 0
# additional interface code

# end additional interface code

}


# Allow interface to be run "stand-alone" for testing

catch {
    if [info exists embed_args] {
	# we are running in the plugin
	ilogged_ui .
    } else {
	# we are running in stand-alone mode
	if {$argv0 == [info script]} {
	    wm title . "Testing ilogged_ui"
	    ilogged_ui .
	}
    }
}
