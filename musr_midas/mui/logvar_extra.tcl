#   logvar_extra.tcl
#   Procedures for showing and controlling logging of Camp variables

#   $Log: logvar_extra.tcl,v $
#   Revision 1.45  2016/09/06 22:12:50  asnd
#   Improve responsiveness with graph updates
#
#   Revision 1.44  2015/10/01 06:36:54  asnd
#   Proper time-cutoff fore restoration
#
#   Revision 1.43  2015/09/30 03:12:27  asnd
#   Log method <expt> added
#
#   Revision 1.42  2015/09/28 00:38:58  asnd
#   Select varname style; show selection variables; save and reload past variable history
#
#   Revision 1.41  2015/04/18 00:02:15  asnd
#   Remove some debug messages
#
#   Revision 1.40  2015/03/20 00:47:01  suz
#   changes by Donald. This version part of package for new VMIC frontends
#
#   Revision 1.39  2013/10/30 06:01:50  asnd
#   Extra warning and better error message filtering
#
#   Revision 1.38  2009/10/19 13:10:35  asnd
#   Retain graph data longer
#
#   Revision 1.37  2008/10/30 05:15:05  asnd
#   Hardcopy resolution
#
#   Revision 1.36  2008/08/23 20:47:28  asnd
#   Use finer and variable rounding of digits when setting vertical axis limits.
#
#   Revision 1.35  2008/04/15 01:36:10  asnd
#   Various small adjustments
#
#   Revision 1.34  2007/11/21 23:46:09  asnd
#   Don't update while rebooting Camp.
#
#   Revision 1.33  2007/10/03 04:05:00  asnd
#   debug campvarlog; handle missing camp_cmd better
#
#   Revision 1.32  2007/10/03 02:39:01  asnd
#   Support bnmr-logger; Camp access uses emissary (affects event timing!)
#
#   Revision 1.31  2006/11/22 21:20:49  asnd
#   More frequent graphing
#
#   Revision 1.30  2006/06/29 07:29:57  asnd
#   Use safer p_win_name (from mui_utils.tcl) for dialogs
#
#   Revision 1.29  2006/05/26 17:58:45  asnd
#   Non-stipled grab-handles
#
#   Revision 1.28  2006/05/26 16:42:25  asnd
#   Accumulated changes.  Notably y-axis locking and multiple plots on an axis.
#
#   Revision 1.27  2005/07/13 04:02:04  asnd
#   Extend radiomenu for dynamic lists; use it for automatic header selection.
#
#   Revision 1.26  2005/06/22 15:31:29  asnd
#   Selection for recording uncertainty in automatic headers
#
#   Revision 1.25  2004/11/10 06:16:57  asnd
#   Use weighted Camp statistics calculated by mdarc (through odb)
#
#   Revision 1.24  2004/10/29 06:44:36  asnd
#   Fix old bug with menu checkboxes getting out of sync
#
#   Revision 1.23  2004/10/29 06:03:53  asnd
#   Handle (mostly ignore) logged non-numeric Camp variables.
#   Add more comments and change a couple of variable names.
#
#   Revision 1.22  2004/10/05 01:02:58  asnd
#   Longer stripchart recording
#
#   Revision 1.21  2004/08/05 02:25:21  asnd
#   Better handling when no Camp server
#
#   Revision 1.20  2004/05/13 02:46:57  asnd
#   Avoid possible integer division in statistics calculation
#
#   Revision 1.19  2004/04/27 11:45:43  asnd
#   Ensure first run marker exists
#
#   Revision 1.18  2004/04/10 07:00:07  asnd
#   Add auto-run control
#
#   Revision 1.17  2004/02/20 23:08:06  asnd
#   Single musr Midas experiment.  Handle year changes.
#
#   Revision 1.16  2003/11/18 11:12:27  asnd
#   Implement zooming into parts of the Camp Stripchart plot.
#
#   Revision 1.15  2003/10/15 09:28:28  asnd
#   Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#   Revision 1.14  2003/10/01 10:12:07  asnd
#   Enable Camp-var logging
#
#   Revision 1.13  2003/09/19 06:23:27  asnd
#   ImuSR and many other revisions.
#
#   Revision 1.11  2002/11/02 10:41:54  asnd
#   Make graph handle changes to logged-var lists; plus other small changes.
#
#   Revision 1.10  2002/10/30 09:35:54  asnd
#   Eliminate duplicate markers.  Hide run markers after kill.
#
#   Revision 1.9  2002/10/30 08:19:08  asnd
#   Longer graph memory; graph's right margin; no sunken buttons;
#
#   Revision 1.8  2002/10/26 08:03:19  asnd
#   Restore history
#
#   Revision 1.7  2002/10/26 07:12:06  asnd
#   Speed up build of CONFIGURE menu by doing more processing in Camp server
#
#   Revision 1.6  2002/10/26 04:19:09  asnd
#   Fix disappearing Configure menu.
#
#   Revision 1.5  2002/10/12 09:24:27  asnd
#   Improve stripchart plot
#
#   Revision 1.4  2002/10/01 05:50:19  asnd
#   Improve recording of acquisition for graph
#
#   Revision 1.3  2002/09/21 04:44:41  asnd
#   Plotting Camp variables
#
#   Revision 1.2  2002/06/18 06:24:07  asnd
#   Changes today
#
#   Revision 1.1  2002/06/12 01:26:39  asnd
#   Starting versions of Mui files.
#

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

catch { package require BLT }

global logwin clogv logvar_help_text

# Global Variable Initializations

# variables for table of statistics:
set clogv(all_lvnames) {}     ;# List of all logged variables reported by Camp
set clogv(lvnames) {}         ;# List of logged numeric (or selection) variables (only these are in table)
set clogv(lvtitles) {}        ;# List of titles corresponding to the names
set clogv(lvtypes) {}         ;# List of variable types corresponding to the names
set clogv(numRows) 0          ;# Number of rows in table of logged variables

set clogv(auto_shown_at) 0    ;# Remember time when auto-header panel was last made visible
set clogv(confirmed_at) 0     ;# Remember time when user confirmed un-logging
set clogv(confirm_stop) 2     ;# like run(confirm_stop)
set clogv(loggedVars) {}      ;# For auto-headers menu: list of logged numeric variables, plus "none"
set clogv(cvarstyle) "Path"   ;# How to display camp variable names

# variables for the stripchart:
set clogv(Graph_Minutes) 50   ;# Requested time-span of stripchart graph
set clogv(Graph_To_Sec) [clock seconds] ;# requested end of stripchart graph
set clogv(PrevGraphUp) 0      ;# Time when graph was previously updated (set to 0 to force update asap)
set clogv(GraphUpID) ""

# variables for the recovery file
set clogv(recoveryfile) [file join $env(HOME) .loggedcampvars]
set clogv(rf:interval) 60

# Axis variables for the stripchart, clogv(<axis>.<ident>), where <axis> is y or y2
# and <ident> is one of:
#   Variables: Lists of plotted variables
#   Colors:    List of their colors
#   Indices:   List of "similar_color" indices of these colors
#   Color0:    The base color for each axis (corresp index 0)
#   Began:     Sequence number of beginning plots (to know which axis is older)
#   Elem:      Highest defined "element" (dataset) number for axis (named like y.0, y.1)
#   Lolim:     Lower limit (for fixed scales)
#   Hilim:     Upper limit (for fixed scales)
#   Lock:      Boolean indication for fixed scaling
# These are assigned initial values in logvar_initialize 

# variables for acquisition markers on graph
set clogv(start_sec) 0        ;# Record run start-time known at previous graph update, to detect changes
set clogv(run_number) 0       ;# Record run number during previous graph update
set clogv(Acq_Marks) [list]   ;# List of tags and times for all acquisition-related markers on stripchart
set clogv(acqOffMarker) {}    ;# Tag for final acq-off marker (grey rectangle) on stripchart
set lv_acq_record [list]      ;# Remembers all acquisition transitions, for display (recorded in odb_musr)

# variables for the cascade menu to select logged variables
set clogv(insnames) {}        ;# List of instrument names when menu-tree of variables was constructed
set camplogvars(/) {}         ;# Array of all loggable variables and paths to them, for cascade menus
set logvar_help_text {}       ;# Text for help-box at bottom

# This bunch is used for the stand-alone Camp logger, indicated by boolean clogv(camp_only)
set clogv(varlog_interval) 60
set clogv(previous_log) 0
set clogv(logged_lvnames) {}
set clogv(filehandle) {}
set clogv(filename) {}

if { ![info exists clogv(camp_only)] } {
    set clogv(camp_only) [expr { [info commands midas] == "" }]
}

if { ![info exists subserv(rebooting)] } {
    set subserv(rebooting) 0
}

set midas(camplogging) 1

proc logvar_initialize { } {
#   Draw the logged-variables window.
    global logwin clogv logvar_help_text run midas

#   mui_camp_cmd will (re)connect to the odb-specified camp host.
#   If it fails, and if we aren't set up to change it, then quit.
    if { !$clogv(camp_only) && [logvar_catch {mui_camp_cmd sysGetInsNames}] } {
        return 
    }

    set lw [td_win_name $logwin]
    if { [ string length $logwin ] } {
        # We are in a real application, where the window is not main
        if { [winfo exists $lw] } {
            wm deiconify $lw
            raise $lw
        } else { # No window: Create it and initialize for no graph
            toplevel $lw
            logvar_ui $logwin
            set clogv(numRows) 0
            grid remove $logwin.lv_graph_f $logwin.lv_graph_sep_c
            $logwin.lv_time_scale set 20
            set clogv(Graph_Minutes) 30
            foreach ax {y y2} {
                array set clogv [list $ax.Variables {} \
                                     $ax.Colors {} $ax.Indices {} $ax.Began 0 \
                                     $ax.Lolim "" $ax.Hilim "" $ax.Lock 0 ]
            }
            array set clogv [list y.Color0 $PREF::revbgcol y2.Color0 $PREF::brightcol]
            array set clogv [list Acq_Marks [list] acqOffMarker {} start_sec 0]
        }
    }

    set clogv(PrevGraphUp) 0
    set clogv(insnames) {}

    wm title $lw "Logged Camp Variables"
    bind $lw <Alt-q> "$logwin.lv_quit_b invoke ; break"
    bind $lw <Alt-u> "$logwin.lv_update_b invoke"

#   Binding to apply time-scale (for graph)
    bind $logwin.lv_time_scale <<ApplyScale>> { 
        logvar_delay_update 200 logvar_full_update
    }

    set clogv(useauto) 0
    if { $clogv(camp_only) } {
        puts "Set up for stand-alone logger"
        set gridinfo [grid info $logwin.lv_autohead_b]
        grid remove  $logwin.lv_autohead_b  $logwin.lv_configure_b
        radiomenu $logwin.lv_fontsize_b -values {20 18 16 14 12 10} -variable newfontsize \
            -text "Aa" -command {mui_font_size configure $newfontsize} 
        eval grid $logwin.lv_fontsize_b $gridinfo
        activateEntry $logwin.lv_camphost_e -width 8
        bind $lw <Alt-c> "$logwin.lv_configure_mb invoke"
        #   Bindings for Camp_host entry fields (for stand-alone logger only)
        # puts "Set up camp host bindings"
        bind $logwin.lv_camphost_e <FocusOut> logvar_apply_camphost
        bind $logwin.lv_camphost_e <Return> logvar_apply_camphost
        logvar_bind_help lv_camphost_e "Set the Camp host name (Enter to apply)"
    } else {
        deactivateEntry $logwin.lv_camphost_e -width 0

#       Only apply bindings to auto-header widgets if they exist.
#       (They don't in stand-alone logger and during ImuSR runs.)
#       This ImuSR variant will disappear soon, and is not used for bnmr.
        if { $midas(musr) && $run(ImuSR) } {
            puts "Set up for ImuSR logged var page..."
            grid remove $logwin.lv_autohead_b $logwin.lv_configure_mb
            grid $logwin.lv_configure_b -column 1
            lower $logwin.lv_configure_b $logwin.lv_update_b
            bind $lw <Alt-c> "$logwin.lv_configure_b invoke"
        } else {
            # The pop-up menus for automatic run headers, placed so the active
            # selection overlaps the button.
            if { ! $midas(musr) } { # bnmr
                puts "Set up for bNMR logged var page"
                set gridinfo [grid info $logwin.lv_autohead_b]
                grid remove  $logwin.lv_autohead_b  $logwin.lv_configure_b
                radiomenu $logwin.lv_fontsize_b -values {20 18 16 14 12 10} -variable newfontsize \
                    -text "Aa" -command {mui_font_size configure $newfontsize} 
                eval grid $logwin.lv_fontsize_b $gridinfo
                #grid remove $logwin.lv_autohead_b
                bind . <Destroy> {bnmr_destroy %W}
            } else {
                puts "Set up for TDmuSR logged var page"
                set clogv(useauto) 1
                bind $lw <Alt-a> "$logwin.lv_autohead_b invoke"
                foreach which {autoT autoB} {
                    radiomenu $logwin.lv_${which}_b -direction flush \
                        -variable run(${which}var) -listvar clogv(loggedVars) \
                        -command logvar_auto_apply
                }
                logvar_bind_help lv_autoT_b "Camp Variable to use for Temperature in run header"
                logvar_bind_help lv_autoB_b "Camp Variable to use for Field in run header"
                logvar_bind_help lv_autohead_b "Use Camp variables for automatic Run Headers"
            }
            grid remove $logwin.lv_configure_b
            bind $lw <Alt-c> "$logwin.lv_configure_mb invoke"

        }
        set clogv(auto_shown_at) 0
        logvar_auto_hide
    }

    radiomenu $logwin.lv_nameselect_b -direction below -variable clogv(cvarstyle) \
        -values {"Path" "Title" "Both path and title"} -command {logvar_delay_update 10 logvar_request_update}
    logvar_bind_help lv_nameselect_b "Select how to identify Camp variables"

    if { [string length $clogv(filehandle)] } {
        logvar_show_logfile
    } else {
        logvar_hide_logfile
    }

#   It is too confusing to have the "apply" function bound to events in
#   an entry window.  Instead, disable text entry, and use only the button.

    deactivateEntry $logwin.lv_file_e -width 0
#    bind $logwin.lv_file_e <Return> logvar_apply_logfile
#    bind $logwin.lv_file_e <FocusOut> logvar_apply_logfile
    bind $lw <Alt-l> "logvar_show_logfile ; $logwin.lv_get_logfile_b invoke"
#    logvar_bind_help lv_file_e "File name for logging values"
    logvar_bind_help lv_get_logfile_b "Select file to log values"
    logvar_bind_help lv_interval_e "Set the file-logging interval (seconds)"

    logvar_bind_help lv_quit_b "Close the logged-variables window"
    logvar_bind_help lv_update_b "Update display of variables and their values"
    logvar_bind_help lv_configure_mb "Select logged variables"
    logvar_bind_help lv_configure_b "Select logged variables"
    logvar_bind_help lv_printer_b "Print the graph (Postscript)"
    logvar_bind_help lv_time_scale "Set time range for the plot"
    set logvar_help_text {}

    $logwin.lv_configure_mb configure -state disabled -direction flush 

    logvar_catch logvar_update

    logvar_delay_update 400 logvar_request_update
}

#   Make a menu showing all loggable camp variables as checkboxes (checked
#   if being logged appropriately).  logvar_make_menu does not pop-up message
#   boxes, but it may return an error.  Building the menu is slow due to many
#   interactions with the Camp server, so we try to retain old information
#   about the Camp variable hierarchy, using the array names of camplogvars().
#   For numeric variables, camplogvars(path) is the -variable for the corresponding
#   checkbox menu item.  For instrument or struct variables, camplogvars(path/)
#   gives a list of *numeric or path* variables on that path.  (Thus, 
#   $camplogvars(/) holds the list of instrument names, with "/" appended
#   to each; these names are the same as the labels for the menu entries.)

proc logvar_make_menu { menub } {
    global clogv midas
    if { ![ winfo exists $menub ] } { return }
    if { $midas(camp_host) == "" || $midas(camp_host) == "none" } { 
        set names [list]
        $menub configure -state disabled
        $menub.menu delete 0 last
        set clogv(insnames) $names
        return
    }
    if { [ catch { mui_camp_cmd {sysGetInsNames} } names ] } {
        #puts "Failed to access Camp: $names"
        return 
    }

    #puts "logvar_make_menu $menub ------------------------\nnames: $names"
    if { ! [string equal $clogv(insnames) $names] } {
        # The instrument list has changed, so rebuild menu.
        # Start by disabling menubutton and rewriting top menu of instruments.
        $menub configure -state disabled
        $menub.menu delete 0 last
        set clogv(insnames) $names

        foreach i $names {
            if { ! [winfo exists $menub.menu.m$i] } {
                menu $menub.menu.m$i -tearoff 0
            }
            # the following line causes menus to stay up, facilitating multiple 
            # selections.  Do we want that (non-standard) behavior?
   #        bind $menub.menu.m$i <ButtonRelease> \
   #                "$menub.menu.m$i invoke active ; break "

            $menub.menu add cascade -label "${i}/" -menu $menub.menu.m$i
        }
        # Next, we clean old crud out of camplogvars() (avoid "memory leak")
        global camplogvars
        foreach i $camplogvars(/) {
            if { [lsearch -exact $names $i] < 0 } {
                #puts "Destroying $menub.menu.m$i  ([winfo exists $menub.menu.m$i])"
                destroy $menub.menu.m$i
                foreach n [array names camplogvars "/${i}/*"] {
                    #puts "Unsetting camplogvars($n)"
                    unset camplogvars($n)
                }
            }
        }
        set camplogvars(/) $names
        # Last, initiate building each sub-menu.  This can be slow so we use
        # [after] to allow Tk proceesing to occur (with menubutton still disabled)
        if { [string length $names] } {
            after 2 after idle logvar_make_submenu $menub $names
        }
    } else {
        #puts "No changes"
        $menub configure -state normal
    }
}

proc logvar_make_submenu { menub args } {
    global clogv camplogvars
    if { ![ winfo exists $menub ] } { return }

    if { [llength $args] } {
        set i [lindex $args 0]
        #puts "Make submenu for $i"
        logvar_make_entry $menub.menu {} $i
        after 2 after idle [lreplace $args 0 0 logvar_make_submenu $menub ]
    } else {
        # Before enabling button, ensure logged vars are labelled properly;
        # they might have changed since we built the remembered camplogvars.
        #puts "Done menus -- enable $menub"
        foreach v $clogv(all_lvnames) {
            set camplogvars($v) 1
        }
        $menub configure -state normal
    }
}

#   Add sub-menu to menu $men giving loggable Camp variables and sub-paths
#   on the Camp path $path/$dir.  If there are no such variables, then do
#   not make the (empty) sub-menu.  logvar_make_entry calls itself recursively.
#
#   Numeric Camp variable properties, from camp.h:
#
#   Attribute val      var type     val
#   -------------      ----------------
#    show      1       none           0
#    set       2       numeric      256
#    read      4       int          273 = 16 + 256 + 1
#    poll      8       float        289 = 32 + 256 + 1
#    alarm    16       selection    512
#    log      32       string      1024 
#    alert    64       array       2048
#    is_set  128       structure   4096
#                      instrument  8192
#                      link       16384

proc logvar_make_entry { men path dir } {
    global camplogvars midas
    if { ![ winfo exists $men ] } { return 0 }
    set any 0
    #puts "logvar_make_entry  "
    if { [winfo exists $men.m$dir] } {
        $men.m$dir delete 0 last
    } else {
        menu $men.m$dir -tearoff 0
        # For persistent-style menus:
#       bind $men.m$dir <ButtonRelease> "$men.m$dir invoke active ; break "
    }

    set pathdir ${path}/${dir}
    if { [info exists camplogvars(${pathdir}/)] } {
        # Know structure already; avoid camp server.  Loop over var names on path
        #puts "  Using records for ${pathdir}/"
        foreach var $camplogvars(${pathdir}/) {
            set fullvar ${pathdir}/${var}
            if { [string match */ $var] } { # var type is path or instrument
                #puts "    Entry $var is path or instrument"
                incr any [ logvar_make_entry $men.m$dir ${pathdir} [ string trimright $var / ] ]
            } else { # var is loggable
                #puts "    Entry $var is loggable"
                set any 1
                set camplogvars($fullvar) 0
                $men.m$dir add checkbutton -label $var -variable camplogvars($fullvar) \
                    -command [list logvar_menu_cmd $fullvar]
            }
        }
    } elseif { $midas(camp_host) != "none" && $midas(camp_host) != "" } {
        # Get variables from Camp
        set camplogvars(${pathdir}/) [list]
        # get list of doublets for variables on path, taking only sub-paths or loggable vars
        # Doublets are: variable; type (Type&12288 | Attributes&32)
        set stat [catch {mui_camp_cmd "set l {};\
            foreach v \[sysGetVarsOnPath $pathdir\] {\
            set i \[expr \[varGetVarType ${pathdir}/\$v\]&12288 | \[varGetAttributes ${pathdir}/\$v\]&32\];\
            if {\$i} {lappend l \$v \$i} };return \$l" } plist ]
        if { $stat || ![ winfo exists $men ] } { return 0 }
        # Loop over that list of interesting variables
        foreach {var type} $plist {
            set fullvar ${pathdir}/${var}
            #puts "Var $fullvar type $type"
            if { ![string is integer -strict $type] } { 
                #puts "Failed integer test!!!"
                break 
            }
            if { $type & 12288 } { # var type is path or instrument
                if { [ logvar_make_entry $men.m$dir ${pathdir} $var ] } {
                    incr any
                    lappend camplogvars(${pathdir}/) ${var}/
                }
            } elseif { $type & 32 } { # var is loggable
                # Initialize the value as "not being logged".  If it is logged,
		# the entry will be set to 1 later using clogv(all_lvnames)
                set any 1
                lappend camplogvars(${pathdir}/) ${var}
                set camplogvars($fullvar) 0
                #puts "adding checkbutton $var with command: logvar_menu_cmd $fullvar"
                $men.m$dir add checkbutton -label "$var" -variable camplogvars($fullvar) \
                    -command [list logvar_menu_cmd $fullvar]
            }
        }
    }

    if { [string length $path] } {
        if { $any } {
            # For persistent menus:
            # bind $men.m$dir <ButtonRelease> "$men.m$dir invoke active ; break "
            $men add cascade -label "${dir}/" -menu $men.m$dir
        } else {
            destroy $men.m$dir
        }
    }
    return [expr {$any > 0}]
}

#   logvar_menu_cmd is invoked when the user selects (or de-selects) a
#   Camp variable from the menu.

proc logvar_menu_cmd { var } {
    global clogv camplogvars run midas
    # puts "logvar_menu_cmd: Add/subtract logged variable $var for exper $run(expertype)"
    #  BUG WORKAROUND: (if needed) Allow plotting variables while ImuSR prevents logging
    #  switch -glob TD 
    if { $clogv(camp_only) } { # Pure Camp stand-alone logger: use clogv(selectedvars)
        set i [lsearch -exact $clogv(select_vars) $var]
        #puts "camplogvars($var) says: $camplogvars($var)"
        if { $camplogvars($var) } { # add to list
            if { $i < 0 } {
                set i [llength $clogv(select_vars)]
                lappend clogv(select_vars) $var
            }
        } else { # Remove from list
            if { $i >= 0 } {
                set clogv(select_vars) [lreplace $clogv(select_vars) $i $i]
            }
        }
    } elseif { $midas(musr) && $run(ImuSR) } {
        # I-muSR experiment; use odb via midef(logged_vars) 
        global midef
        set i [lsearch -exact $midef(logged_vars) $var]
        #puts "camplogvars($var) says: $camplogvars($var)"
        if { $camplogvars($var) } { # add to list
            if { $i < 0 } {
                set i [llength $midef(logged_vars)]
                lappend midef(logged_vars) $var
                lappend midef(logged_poll) 5
            }
            imusr_check_polling $var $i
        } else {
            if { $i >= 0 } {
                set midef(logged_vars) [lreplace $midef(logged_vars) $i $i]
                set midef(logged_poll) [lreplace $midef(logged_poll) $i $i]
            }
        }
        set midef(num_logged) [llength $midef(logged_vars)]
        idef_declare_changed
        idef_propagate
    } else { # TD-muSR or any bnmr: Use list of logged vars given by Camp.
        logvar_catch {
            if { $camplogvars($var) } {
                mui_camp_cmd "varDoSet $var -l on -l_act log_mdarc"
            } else {
                mui_camp_cmd "varDoSet $var -l off"
            }
        }
    }
    logvar_delay_update 20 logvar_full_update
}

#   logvar_update updates the record of logged variables, but not the
#   configuration menu.  It can return with error code, so should be
#   caught when used.  It is used here and in td_update.
#   If used when the logged-variable window is not built, then the
#   values are still added to the history, and logged to the log file.

proc logvar_update { } {
    global logwin clogv camplogvars run midef wtstat midas subserv

    #puts "Start logvar_update"
    if { $subserv(rebooting) } {
        return -code error "Camp host not available yet"
    }

    after cancel $clogv(GraphUpID)
    set clogv(GraphUpID) ""
    set clogv(entry_camp_host) $midas(camp_host)

    # Get the list of logged variables
    if { $clogv(camp_only) } {
        set all $clogv(select_vars)
    } elseif { $run(ImuSR) && [info exists midef(logged_vars)] && $midas(musr) } {
        set all $midef(logged_vars)
    } else {
        if { [string match -nocase "I*" $run(expertype)] } {
            set methods [list log_i_musr log_mdarc_ti log_mdarc $midas(expt)]
        } else { 
            set methods [list log_td_musr log_mdarc_td log_mdarc $midas(expt)]
        }
        if { [ catch { mui_camp_cmd "sysGetLoggedVars $methods" } all ] } {
            return -code error "Failed to access Camp: $all"
        }
    }
    # Determine the weighted statistics
    odb_get_wt_stat
    # When logged vars change, record full list of vars, then delist any non-numeric 
    # or non-existent variables
    if { ! [string equal $all $clogv(all_lvnames) ] } {
        # Check for variables not logged anymore, to update menu checkboxes
        foreach lvar $clogv(all_lvnames) {
            if { [lsearch -exact $all $lvar] == -1 } {
                set camplogvars($lvar) 0
            }
        }
        set errors ""
        set lvnames [list]
        set lvtypes [list]
        foreach lvar $all { # Now do each separately for error filtering
            if { [catch {mui_camp_cmd "varGetVarType $lvar"} type] } {
                append errors "$type "
            } else {
                set camplogvars($lvar) 1
                if { $type & (256|512) } {# filter list to include numerics (or selections) only
                    lappend lvnames $lvar
                    lappend lvtypes $type
                }
            }
        }
        # Update memory for next cycle
        set clogv(all_lvnames) $all
        set clogv(lvnames) $lvnames
        set clogv(lvtypes) $lvtypes
        # Request graph update asap
        set clogv(PrevGraphUp) 0
    } else {
        set lvnames $clogv(lvnames)
        set lvtypes $clogv(lvtypes)
    }
    set numlv [llength $lvnames]
    set clogv(loggedVars) [linsert $lvnames 0 none]
    set lvtime [clock seconds]
    set values [list $lvtime]
    set cvals [list]

    # Now perform operations on logged-variables page
    if { [winfo exists [td_win_name $logwin]] } {

        # Do full update of displayed values
	# Delete rows if there are too many
        while { $clogv(numRows) > $numlv } {
            set row [expr $clogv(numRows) + 1]
            if { [winfo exists $logwin.label$row] } {
                logvar_delete_row $row
            }
            incr clogv(numRows) -1
        }
        set clogv(numRows) $numlv
        # Add rows as necessary
        set row 1
        foreach lvar $lvnames {
            incr row
            if { ! [winfo exists $logwin.label$row] } {
                logvar_make_row $row
            }
            set clogv(var$row) $lvar
        }
        # Update readings from Camp
        set row 1
        foreach lvar $lvnames type $lvtypes {
            incr row
            if { $type & 256 } {# Numeric, with statistics
               set pars [mui_camp_cmd "list \[varGetVal $lvar\] \[varNumGetNum $lvar\] \[varNumGetSum $lvar\] \
\[varNumGetSumSquares $lvar\] \[varNumGetSumOffset $lvar\] \[varNumGetUnits $lvar\] \[varGetTitle $lvar\]" ]
                #       Val Num Sum SumSquares SumOffset Units title
                if { [catch {set val [td_short_float [lindex $pars 0]]}] || [llength $pars] != 7 } {
                    return -code error "Failed to read Camp variable $lvar"
                }
                set clogv(units$row) [lindex $pars 5]
                set clogv(num$row) [set num [lindex $pars 1]]
		set tit [lindex $pars 6]
                if { $run(in_progress) && !$run(ImuSR) && $wtstat(enabled) && ![catch {
                    set i [lsearch $wtstat(variables) "[string range $lvar 0 29]*"]
                } ] && $i >= 0 && $run(hist_total) > 0 } {
                    set clogv(mean$row) [td_short_float [lindex $wtstat(mean) $i]]
                    set clogv(stdd$row) [td_short_float [lindex $wtstat(stddev) $i]]
                } else {
                    if { $num < 2 } {
                        set clogv(mean$row) $val
                        set clogv(stdd$row) 0.0
                    } else {
                        set sum [expr { double([lindex $pars 2]) }]
                        set sumsquares [lindex $pars 3]
                        set sumoffset [lindex $pars 4]
                        set clogv(mean$row) [td_short_float [expr { $sumoffset + ( $sum / $num ) } ] ]
                        set clogv(stdd$row) [td_short_float [expr { \
                            sqrt( abs( ($sumsquares - ($sum * $sum / $num) ) / ( $num - 1.0 ) )) } ] ]
                    }
                }
            } elseif { $type & 512 } {# Selection var
                set pars  [mui_camp_cmd "list \[varGetVal $lvar\] \[varSelGetValLabel $lvar\] \[varGetTitle $lvar\]"]
                if { [llength $pars] != 3 } {
                    return -code error "Failed to read Camp variable $lvar"
                }
                set val [lindex $pars 0]
		set tit [lindex $pars 2]
                set clogv(units$row) [lindex $pars 1]
                set clogv(num$row) " "
                set clogv(mean$row) [string range [lindex $pars 1] 0 12]
                set clogv(stdd$row) " "
            } else {# impossible
		return -code error "Improper logged Camp variable $lvar"
	    }
            set clogv(val$row) $val
            append values "  " $val
            lappend cvals $lvar $val

	    switch -glob $clogv(cvarstyle) {
		Title* { set clogv(label$row) $tit }
		Both* { set clogv(label$row) "$lvar   ($tit)" }
		default { set clogv(label$row) $lvar }
	    }

            # track which are being plotted.
	    catch {
            lassign [logvar_axis_lookup $lvar] axi idx
            if { $idx < 0 } {# not plotted
                $logwin.graph$row configure -bg "$PREF::revfgcol" \
		      -activebackground "$PREF::revfgcol" -relief raised
            } else {# var is plotted
                set col [lindex $clogv($axi.Colors) $idx]
                $logwin.graph$row configure -activebackground $col -bg $col
            }
	    }
        }
        logvar_auto_hide
    } else {
        # No window, so just record values (maybe... silently abort on errors)
        if { $numlv < 1 || $lvtime < $clogv(PrevGraphUp) + 30 } { return }
        # OK, Update $cvals list of values; use a single camp_cmd for speed.
        set cc {list}
        foreach lvar $lvnames {
            append cc " \[varGetVal $lvar\]"
        }
        if { [catch {set res [mui_camp_cmd $cc]}] } { return }
        foreach lvar $lvnames val $res {
            if { [string length $val] == 0 } {
                return
            } elseif { [string is double $val] && ![string is integer $val] } {
                set val [td_short_float $val]
            }
            lappend cvals $lvar $val
            append values "  " $val
        }
    }
    #
    # Now save to log file
    #
    set intval 60
    catch { 
        set intval [expr { round( $clogv(varlog_interval) < 1 ? 1 : $clogv(varlog_interval) ) }]
    }
    if { [string length $clogv(filehandle)] } {
        if { ! [string equal $lvnames $clogv(logged_lvnames)] } {
            puts $clogv(filehandle) "!   time   [join $lvnames {   }]"
        }
        if { $clogv(previous_log) + $intval <= $lvtime } {
            if { [string length $clogv(filehandle)] && [llength $lvnames] } {
                catch {
                    puts $clogv(filehandle) $values
                    flush $clogv(filehandle)
                }
            }
            set clogv(previous_log) $lvtime
        }
    }
    #  Log data in a file to be used for re-starting a musr gui. Skip it if we are "camp only"
    #  and have chosen our own private list of variables.
    if { [llength $lvnames] && !$clogv(camp_only) } {
        if { [file exists $clogv(recoveryfile)] } {
            set prevrflog [file mtime $clogv(recoveryfile)]
        } else {
            set prevrflog 0
        }
        set samehead [string equal $lvnames $clogv(logged_lvnames)]
        if { $prevrflog + $clogv(rf:interval) <= $lvtime || ! $samehead } {
            #puts "Try to update $clogv(recoveryfile)"
            set rfvars $clogv(logged_lvnames)
            # Might need to check last header in file, but that's too slow to do every time. 
            # Instead, check only when we are about to write a new header.
            if { ! $samehead } {
                if { [catch {
                    set resp [exec /bin/grep -F "!   time " $clogv(recoveryfile) | /usr/bin/tail -n 1]
                    set rfvars [lrange $resp 2 end]
                    #puts "Previous vars in file $clogv(recoveryfile) are $rfvars"
                } mmm] } { puts "error: $mmm\n>>$::errorCode\n>>$::errorInfo" }
                set samehead [string equal $lvnames $rfvars]
            }
            if { ![catch { open $clogv(recoveryfile) a } rfh] } {
                if { ! $samehead } {
                    puts "New loggedvar titles"
                    catch { puts $rfh "!   time   $lvnames" }
                }
                catch { puts $rfh $values }
                catch { close $rfh }
            }
        }
    }

    set clogv(logged_lvnames) $lvnames

    # Now update stripchart records, if not too close to previous point.
    # Choose minimum interval between points based on:
    # A Quarter the log-file interval, constrained to the range 15 to 45 seconds,
    # but shorter than 15 seconds when the graph has a short time range.
    set gtr [expr {$clogv(Graph_Minutes)*60.0}]
    set intval [bounded 0.25*$intval 15 45]
    set intval [expr { $intval>$gtr/15.0 ? $gtr/15.0 : $intval }]
    # If we are scheduled for a new point, then append to list, and record time of update
    # and set time-scale to go to "now"
    if { [llength $cvals] && $lvtime >= $clogv(PrevGraphUp) + $intval } {
        set clogv(PrevGraphUp) $lvtime
        set clogv(Graph_To_Sec) $lvtime
        foreach {lvar v} $cvals {
            lappend clogv(T:$lvar) $lvtime
            lappend clogv(V:$lvar) $v
        }
        if { [winfo exists $logwin.lv_graph_sc] } {
            logvar_display_graph
        }
    } elseif { $numlv == 0 && [winfo ismapped $logwin.lv_graph_f] } {
	logvar_display_graph
    }
    return {}
}

#   This one does an update, and *always* updates the graph (if it exists)
proc logvar_full_update { } {
    set ::clogv(PrevGraphUp) 0 
    logvar_catch { logvar_update }
}

#   logvar_request_update forces update of the graph *and* the menus
proc logvar_request_update { } {
    global logwin idefwin
    logvar_full_update
    if {[catch { logvar_make_menu $::logwin.lv_configure_mb } msg]} {
        puts "logvar_make_menu $::logwin.lv_configure_mb error: $msg"
    }
    if { [info exists idefwin] } {
        catch { logvar_make_menu $::idefwin.tabs.log.ilog_configure_mb }
    }
}

# schedule any of the above after a delay
proc logvar_delay_update { delay args } {
    global clogv
    after cancel $clogv(GraphUpID)
    set clogv(GraphUpID) [after $delay $args]
}

#   logvar_make_row: make a row for a camp variable and values.
#   For some reason, SpecTcl uses grid rows/columns starting at 1
#   instead of 0.  Rows are numbered 2,3,...

proc logvar_make_row { row } {
    global logwin run clogv midas

    # Create buttons
    [button $logwin.graph$row] configure \
            -activebackground "$PREF::revfgcol" -bg "$PREF::revfgcol" -borderwidth 1 \
            -highlightthickness 1 -padx 1 -pady 1 -takefocus 1 \
            -state normal -relief raised -text {} -image graph_icon \
            -command [list logvar_toggle_graph $row]
    logvar_bind_help graph$row "Display graph of \$clogv(var$row) (drag to link multiple graphs)"
    group_drag_bind lvgraphs $logwin.graph$row $row

    [button $logwin.del$row] configure \
            -activebackground "$PREF::revfgcol" -bg "$PREF::revfgcol" -borderwidth 1 \
            -highlightthickness 1 -padx 1 -pady 1 -takefocus 1 \
            -state normal -relief raised -text {} -image delete_x \
            -command [list logvar_nolog $row]
    logvar_bind_help del$row "Stop logging variable \$clogv(var$row)"

    # Adjust tab-traversal order for buttons
    if { $row < 3 } {
        raise $logwin.graph$row $logwin.lv_printer_b
    } else {
        raise $logwin.graph$row $logwin.del[expr {$row-1}]
    }
    raise $logwin.del$row $logwin.graph$row

    # create labels to display values
    foreach c [list label val num mean stdd] {
        [label $logwin.${c}$row] configure -textvariable clogv(${c}$row) -padx 5
    }

    if { [llength [info commands blt::stripchart]] } {
        grid $logwin.graph$row -in $logwin.lv_vars_f -row $row -column 1 -ipadx 1 -ipady 1 -sticky nsew
    }

    if { $clogv(camp_only) || ! ( $midas(musr) && $run(ImuSR) ) } {
        grid $logwin.del$row -in $logwin.lv_vars_f -row $row -column 2 -ipadx 1 -ipady 1 -sticky nsew
    }

    grid x x x $logwin.label$row $logwin.val$row $logwin.num$row $logwin.mean$row $logwin.stdd$row \
        -in $logwin.lv_vars_f -row $row -sticky w
}

#  logvar_delete_row: Delete a row from the table of Camp variables.
proc logvar_delete_row { row } {
    global logwin
    group_drag_unbind lvgraphs $logwin.graph$row
    destroy $logwin.graph$row $logwin.del$row \
            $logwin.label$row $logwin.val$row $logwin.num$row $logwin.mean$row $logwin.stdd$row
}

#   Stop logging (and stop displaying) a Camp variable (indicated by row number in tabular display)
proc logvar_nolog { row } {
    global run logwin clogv logvar_help_text camplogvars
    if { $clogv(confirm_stop)>=2 && $clogv(camp_only)==0 && [clock seconds]-$clogv(confirmed_at)>1400 } {
        beep
        if { [timed_messageBox -timeout 15000 -parent [p_win_name $logwin] -icon question -type okcancel -default ok \
                  -message "This action will also stop logging variable $clogv(var$row) with the experiment data.\nIs that OK?" \
                  -title "Confirm stop logging" ] == "cancel" } {
            return
        }
        set clogv(confirmed_at) [clock seconds]
    }
    logvar_catch {
        # turn off row, and schedule full update
        set camplogvars($clogv(var$row)) 0
        logvar_menu_cmd $clogv(var$row)
        set logvar_help_text {}
        # experiment with this???  logvar_delay_update 50 logvar_full_update
    }
}

#   logvar_catch catches camp errors and pops up a message box.
#   It returns boolean 1 for (caught) error and 0 for success.

proc logvar_catch { script } {
    if { [catch { uplevel 1 $script } msg] } {
        beep
        timed_messageBox -timeout 30000 -type ok -icon error -title "Bad Camp" \
                -message "Camp access error for ${script}:\n$msg"
        return 1
    }
    return 0
}

#proc logvar_catch { script } {
#    uplevel 1 $script
#    return 0
#}

# Procedure to immediately apply changes in the automatic-header settings.
proc logvar_auto_apply { } {
    global clogv
    set clogv(auto_shown_at) [clock seconds]
    td_odb odb_set_auto_head "change run header information"
    auto_heads_logging
    after 900 { td_background_update }
}

# Ensure auto-header vars are also logged (executed from just above, from
# start-run in td_extra.tcl and imusr_extra.tcl, and titles_OK.
proc auto_heads_logging { } {
    global run midas clogv
    if { !([string length $midas(camp_host)] && [string compare "none" $midas(camp_host)]) } {
        return
    }
    foreach which {autoT autoB} {
        if { [string match "/?*" $run(${which}var)] && \
             [lsearch -exact $clogv(all_lvnames) $run(${which}var)] < 0 } {
            catch { mui_camp_cmd "varDoSet $run(${which}var) -l on -l_act log_mdarc" }
        }
    }
}

# Make the automatic-run-headers disappear if they have not been touched
# within the last minute. The global variable clogv(auto_shown_at) records
# the time when the controls were displayed or last used, or 0 when they
# are hidden.
proc logvar_auto_hide { } {
    global logwin clogv 
    if { $clogv(auto_shown_at) + 60 < [clock seconds] } {
        grid remove $logwin.lv_autohead_f $logwin.lv_auto_sep
        set clogv(auto_shown_at) 0
    }
}

# Toggle showing and hiding the automatic-run-headers panel. This is bound to the
# "Auto Headers" button.
proc logvar_auto_show { } {
    global logwin clogv
    if { $clogv(auto_shown_at) } {
        set clogv(auto_shown_at) 0
        logvar_auto_hide
    } else {
        set clogv(auto_shown_at) [clock seconds]
        grid $logwin.lv_autohead_f $logwin.lv_auto_sep
    }
}


#  THE HELP LINE:
#
#  Many widgets have a binding for mouse-entry and mouse-exit where some
#  help text is displayed while the cursor is on that widget.

proc logvar_bind_help { w h } {
    global logwin
    bind $logwin.$w <Enter> "set logvar_help_text \"$h\""
    bind $logwin.$w <FocusIn> "set logvar_help_text \"$h\""
    bind $logwin.$w <Leave> "set logvar_help_text {}"
    bind $logwin.$w <FocusOut> "set logvar_help_text {}"
}


image create bitmap graph_icon -background {} -foreground "$PREF::fgcol" \
        -file [file join $muisrc graph_data.xbm]

#  logvar_toggle_graph
#  This is the procedure for clicking the "graph" button beside each logged
#  variable -- it toggles whether that variable is graphed on the stripchart.
#  The plotted variables form two groups -- the ones plotted on the left
#  axis and the ones on the right axis.  When an inactive graph button is
#  clicked, it becomes a group of one, displacing the older plotted group if
#  two groups are already plotted.  When an active graph button is clicked,
#  that variable is removed grom the graph. If it was the last member of its
#  group, the group vanishes. If the vanishing group was plotted on the left
#  axis, the right-axis-group moves to the left. If there is no other plotted
#  variable then the graph disappears.
#
#  To get more than one variable in a group, use drag-and-drop (see ????).
#
#  Rows are numbered starting at 2.

proc logvar_toggle_graph { row } {
    global logwin clogv

    set var $clogv(var$row)
    #puts "Toggle graphing of var $var.  Previous plotting: [logvar_axis_lookup $var]"
    lassign [logvar_axis_lookup $var] axi idx
    if { [string equal $axi ""] } { # Variable is not being graphed.  Add it
        # Choose axis to plot this variable on; drop oldest set if necessary
        set axi [expr { $clogv(y2.Began) < $clogv(y.Began) ? "y2" : "y" }]
        #puts "Start graphing it.  Coose axis $clogv(y2.Began) < $clogv(y.Began) -> $axi"
        while { [llength $clogv($axi.Variables)] > 0 } {
            logvar_ungraph_var $axi 0
        }
        # Begin graphing this variable on this axis
        logvar_graph_var $var $axi $row
    } else { # Variable is already being graphed: Remove it.
        logvar_ungraph_var $axi $idx $row
    }
    set clogv(Graph_To_Sec) [clock seconds]
    logvar_display_graph
}

#   Start graphing a variable, indicated by var, axis, and row number
proc logvar_graph_var { var axi row } {
    global clogv logwin
    # Note sequence for changes to axis plotting
    set clogv($axi.Began) [expr \
          { 1 + ($clogv(y.Began) > $clogv(y2.Began) ? $clogv(y.Began) : $clogv(y2.Began) )}]
    # Choose a color for this variable, using the lowest unused iteration for "similar" colors
    # pick first number where sorted list of color indices deviates from list index
    set ic 0
    foreach i [lsort -integer $clogv($axi.Indices)] {
        if { $ic != $i } {
            break
        }
        incr ic
    }
    # Choose the $ic'th color similar to Color0; 0.25 is a parameter for the degree of difference
    set col [similar_color $clogv($axi.Color0) $ic 0.25]
    # Add info to variables
    lappend clogv($axi.Variables) $var
    lappend clogv($axi.Colors) $col
    lappend clogv($axi.Indices) $ic
    # Set color of graph button
    $logwin.graph$row configure -activebackground $col -bg $col
    # the graph itself will track the changes just made, via logvar_display_graph
    return
}

#   Stop graphing a variable, indicated by the axis name and index
proc logvar_ungraph_var { axi idx {row 0} } {
    global clogv logwin
    # First a parameter validity check, though it shouldn't be necessary:
    if { ![info exists clogv($axi.Variables)] || $idx >= [llength $clogv($axi.Variables)] } {
	puts "Invalid parameters for logvar_ungraph_var { $axi $idx }"
        return
    }
    # Use row, if given, or look up which row
    if { $row < 1 } {
	set var [lindex $clogv($axi.Variables) $idx]
	set row [expr {[lsearch -exact $clogv(lvnames) $var] + 2}]
    }
    # remove notation and display of color on table of values
    if { $row > 1 } {
	#puts "Reconfigure row $row"
        catch { $logwin.graph$row configure \
            -activebackground "$PREF::revfgcol" -bg "$PREF::revfgcol" -relief raised
	}
    }
    # remove entry from the list of variables plotted on this axis
    set clogv($axi.Variables) [lreplace $clogv($axi.Variables) $idx $idx]
    set clogv($axi.Colors) [lreplace $clogv($axi.Colors) $idx $idx]
    set clogv($axi.Indices) [lreplace $clogv($axi.Indices) $idx $idx]
    # Knock down sequencer when no variables left for this axis
    if { [llength $clogv($axi.Variables)] < 1 } {
        set clogv($axi.Began) 0
    }
    #puts "After removal, remaining vars: $clogv($axi.Variables); began: $clogv($axi.Began)"
    # the graph itself will track the changes just made.
}

# Procedure to link the plotting of two variables -- plot them on the same axis with
# similar colors.  The two variables are identified by their row numbers.  This is the
# procedure for drag-n-drop between graph buttons.
#
proc logvar_link_graphs { fromrow torow } {
    global clogv
    lassign [logvar_axis_lookup $clogv(var$fromrow)] axi1 i1
    lassign [logvar_axis_lookup $clogv(var$torow)] axi2 i2
    if { $i1 < 0 && $i2 < 0 } {
        # Neither is being plotted already.  Begin plotting 1
        logvar_toggle_graph $fromrow
        lassign [logvar_axis_lookup $clogv(var$fromrow)] axi1 i1
    }
    # Now at least one of them must be graphed.
    # Check if both are already plotted on the same axis (then do nothing)
    if { [string equal $axi1 $axi2] } { return }
    # If "from" is not graphed then swap from<->to
    set row $torow
    if { $i1 < 0 } {
        set axi1 $axi2
        set i1 $i2
        set axi2 ""
        set i2 -1
        set row $fromrow
    }
    # Now "from", at least, is being plotted.  Is "to" already plotted (on the other axis)?
    if { $i2 >= 0 } { 
        # It is; so un-graph it
        logvar_ungraph_var $axi2 $i2
    }
    # Now plot "to" on same axis as "from"
    logvar_graph_var $clogv(var$row) $axi1 $row
    #
    set clogv(Graph_To_Sec) [clock seconds]
    logvar_display_graph
}

group_drag_bind_proc lvgraphs logvar_link_graphs

#  Procedure to tell on which axis a Camp variable is being plotted, and where it sits
#  in the list of vars plotted on that axis.  The input parameter is the camp variable name.
#  Returns a list of {axis index}.  If the variable is not being plotted, return {"" -1}.
#  (Note: result conveniently assigned to two variables using foreach).
#
proc logvar_axis_lookup { var } {
    global clogv
    foreach a {y y2} {
        set i [lsearch -exact $clogv($a.Variables) $var]
        if { $i >= 0 } { return [list $a $i] }
    }
    return [list "" -1]
}



#  Display the camp stripchart graph.

proc logvar_display_graph { } {
    global run clogv logwin

    set now [clock seconds]
    set ny [llength $clogv(y.Variables)]
    set ny2 [llength $clogv(y2.Variables)]
    #puts "logvar_display_graph  $ny  $ny2"

    # If variables plotted only on "y2" axis, swap y <--> y2
    if { $ny == 0 && $ny2 > 0 } {
        foreach v [list Indices Began Color0 Colors Variables Lolim Hilim Lock] {
            swap_vars clogv(y.$v) clogv(y2.$v)
        }
        set ny $ny2
        set ny2 0
    }

    if { $ny + $ny2 == 0 } {
        # puts "No curves: remove whole graph panel, if it is visible"
        if { [winfo ismapped $logwin.lv_graph_f] } {
            grid remove $logwin.lv_graph_f $logwin.lv_graph_sep_c
        }
    } elseif { [llength [info commands blt::stripchart]] } {
        # There is/are curve(s), and the stripchart command is available: Display graph panel
        if { ! [winfo ismapped $logwin.lv_graph_f] } {
            grid $logwin.lv_graph_f $logwin.lv_graph_sep_c
        }
        # If no stripchart, create it:
        if { ! [winfo exists $logwin.lv_graph_sc] } {
            blt::stripchart $logwin.lv_graph_sc -width 640 -height 300 -plotpadx 1 -plotpady 1
            grid $logwin.lv_graph_sc -in $logwin.lv_graph_f -row 1 -rowspan 4 -column 1 -columnspan 4 -sticky nsew
            lower $logwin.lv_graph_sc $logwin.lv_time_scale
            $logwin.lv_graph_sc legend configure -hide yes
	    array set clogv {y.Elem -1 y2.Elem -1}
            $logwin.lv_graph_sc axis configure x -command logvar_format_time -title Time
            logvar_bind_help lv_graph_sc "Stripchart. Drag rectangle to zoom; click to restore"
	    # Set bindings for zoom function
	    bind $logwin.lv_graph_sc <ButtonPress-1> { lv_BeginZoom %W %x %y }
	    bind $logwin.lv_graph_sc <ButtonRelease-1> { logvar_full_update }
            # Create axis scale-locking and -adjustment widgets
            foreach ax {y2 y} dir {e w} {
                logvar_bind_help lv_lock${ax}axis_cb "Indicator for locking vertical axis. Click to reverse lock/unlock"
                raise $logwin.lv_lock${ax}axis_cb $logwin.lv_graph_sc
                lv_make_yscale_knob $ax Hilim 1 $dir
                lv_make_yscale_knob $ax Lolim 3 $dir
            }
            set clogv($ax.Locked) 0
            # Load old acquisition records as markers
            set clogv(Acq_Marks) {}
            set clogv(acqOffMarker) {}
            set clogv(runNumberMarker) {}
            set clogv(runStartMarker) {}
            eval [join $::lv_acq_record "\n"]
        }
        # Configure x (time) axis, whose units are seconds-of-the-epoch:
        set inc 60
        set s [expr {$clogv(Graph_Minutes)*0.3}]
        foreach i [list 1 2 3 5 10 20 30 60 120] {
            if { $i < $s } { set inc [expr {60*$i}] }
        }
        set tmax [expr { ($clogv(Graph_To_Sec)/$inc + 1)*$inc }]
        set ninc [expr { round(($clogv(Graph_Minutes)*60+.99)/$inc) }]
        set tmin [expr { $tmax - $ninc*$inc }]
        $logwin.lv_graph_sc axis configure x -min $tmin -max $tmax -stepsize $inc
        # Configure each y axis and its data set:
        if { [llength $clogv(y2.Variables)] < 1 } {
            #puts "No y2 variables -- hide axis"
            $logwin.lv_graph_sc axis configure y2 -hide 1
            $logwin.lv_graph_sc configure -rightmargin 8
            set clogv(y2.Lock) 0
            lower $logwin.lv_locky2axis_cb $logwin.lv_graph_sc
            $logwin.lv_locky2axis_cb configure -state disabled
        } else {
            #puts "y2 vars are: $clogv(y2.Variables)"
            $logwin.lv_graph_sc configure -rightmargin 0
            raise $logwin.lv_locky2axis_cb $logwin.lv_lockyaxis_cb
            $logwin.lv_locky2axis_cb configure -state normal
        }
	foreach axi {y y2} {
	    set title ""
	    set i 0
	    foreach var $clogv($axi.Variables) col $clogv($axi.Colors) {
		# If graphed variable disappeared, then de-list it and re-display graph
		if { [lsearch -exact $clogv(lvnames) $var] < 0 } {
		    logvar_ungraph_var $axi $i
		    after 1 after idle logvar_display_graph
		    return
		}
                #puts "Update Graph of var $var"
		# stripchart does poorly clipping data to displayed ranges, so prepare lists myself.
		lassign [lsearch_cover_range clogv(T:$var) $tmin $tmax] i0 i1
		if { $i > $clogv($axi.Elem) } {
		    $logwin.lv_graph_sc element create $axi.$i -label {} -linewidth 2 -pixels 0 -symbol {}
		    set clogv($axi.Elem) $i
		}
		$logwin.lv_graph_sc element configure $axi.$i -hide 0 -color $col -mapy $axi \
                    -xdata [lrange $clogv(T:$var) $i0 $i1] -ydata [lrange $clogv(V:$var) $i0 $i1]

		set t [join [lrange $clogv($axi.Variables) 0 $i] " "]
		if { [string length $t] < 55 } {
		    set title $t
		}
		incr i
	    }
	    if { $i > 0 } {
		$logwin.lv_graph_sc axis configure $axi -hide 0 \
                    -title $title -titlecolor $clogv($axi.Color0) \
                    -loose 1 \
                    -min $clogv($axi.Lolim) -max $clogv($axi.Hilim)
		# possible additions: -color $col  -loose [expr {!$clogv($axi.Lock)}] 
	    } 
            if { $clogv($axi.Lock) } {# show adjustment handles
                raise $logwin.lv_${axi}Hilim_b $logwin.lv_graph_sc
                raise $logwin.lv_${axi}Lolim_b $logwin.lv_graph_sc
            } else {# hide adjustment handles
                set clogv($axi.Lolim) {}
                set clogv($axi.Hilim) {}
                lower $logwin.lv_${axi}Hilim_b $logwin.lv_graph_sc
                lower $logwin.lv_${axi}Lolim_b $logwin.lv_graph_sc
            }
	    while { $i <= $clogv($axi.Elem) } {
		$logwin.lv_graph_sc element delete $axi.$clogv($axi.Elem)
		incr clogv($axi.Elem) -1
	    }
        }
        # Check for and handle changes of run -- given by start_sec changing
        # (The run number will change when changing acq between real/test)
        # (Don't look for run numbers in the stand-alone Camp logger.)
        if { !$clogv(camp_only) } {
            if { $clogv(start_sec) != $run(start_sec) } { # Run started
                # puts "start_sec $run(start_sec) > $clogv(start_sec) means run started"
                # Ensure markers line up at true begin time:
                set now $run(start_sec)
                # Create new markers -- line and text -- at run-begin-time.
                lv_run_started $run(number) $run(start_sec)
            } elseif { $run(in_progress) } { # No begin/end/kill, but maybe run number changed (real/test)
                if { [string length $clogv(runNumberMarker)] } {
		    # Apply run-number marker, but only if changed. Bug in blt *requires* this optimization!
		    set rnt ""
		    catch { set rnt [lindex [$logwin.lv_graph_sc marker configure $clogv(runNumberMarker) -text] end] }
		    if { [string compare $run(number) $rnt] } {
			$logwin.lv_graph_sc marker configure $clogv(runNumberMarker) -text $run(number)
		    }
                } else {
                    lv_run_started $run(number) $run(start_sec)
                }
            } elseif { $run(number) == $clogv(run_number) - 1 } { # Run killed (does not catch all kills)
                #puts "Run number $run(number) == $clogv(run_number) - 1 means run killed"
                if { [string length $clogv(runStartMarker)] } {
                    # Do not hide marker lines for killed runs
		    #puts "Hide run number marker for killed run, leave line marker"
                    #$logwin.lv_graph_sc marker configure $clogv(runStartMarker) -hide 1
                    $logwin.lv_graph_sc marker configure $clogv(runNumberMarker) -hide 1
                }
            }
            # Check for and perform changes to run-status markers:
            if { $run(state) == 3 } {# Acquisition is ON
                if { [ string length $clogv(acqOffMarker) ] } {
                    # If acq-off marker is active: 
                    set coords [$logwin.lv_graph_sc marker cget $clogv(acqOffMarker) -coords]
                    $logwin.lv_graph_sc marker configure $clogv(acqOffMarker) \
                            -coords [lreplace $coords 4 7 $now +Inf $now -Inf]
                    set clogv(acqOffMarker) {}
                }
            } else { # Acquisition is off or paused
                if { ! [ string length $clogv(acqOffMarker) ] } {
                    # If there is no acq-off marker active:
                    # create it, with start point "now" (when paused) or last stop (when stopped)
                    set tao [expr {$run(in_progress) ? $now : $run(stop_sec)}]
                    set clogv(acqOffMarker) [ $logwin.lv_graph_sc marker create polygon \
                            -fill "#dddddd" -under true \
                            -coords [list $tao -Inf $tao +Inf +Inf +Inf +Inf -Inf] ]
                    lappend clogv(Acq_Marks) $clogv(acqOffMarker) $tao
                }
            }
            set clogv(start_sec) $run(start_sec)
            set clogv(run_number) $run(number)
        }
    }
}

# Create new markers -- line and text -- at run-begin-time.
# Ignore ancient history (graphTimeLimit is in minutes)
proc lv_run_started { num tim } {
    if { [clock seconds] - $tim < 60*$PREF::graphTimeLimit } {
        global logwin clogv
        set clogv(runStartMarker) [ $logwin.lv_graph_sc marker create line \
                -linewidth 1 -coords [list $tim -Inf $tim +Inf] ]
        set clogv(runNumberMarker) [ $logwin.lv_graph_sc marker create text \
                -anchor nw -text $num -background {} -coords [list $tim +Inf] ]
        # add them to list of markers
        lappend clogv(Acq_Marks) $clogv(runStartMarker) $tim
        lappend clogv(Acq_Marks) $clogv(runNumberMarker) $tim
    }
}

# Create marker for a period of acquisition-off.  Ignore markers that are
# too narrow (11 sec) or too old (graphTimeLimit)
proc lv_acq_off { from to } {
    if { $to - $from > 11 && [clock seconds] - $to < 60*$PREF::graphTimeLimit } {
        global logwin clogv
        set mk [ $logwin.lv_graph_sc marker create polygon \
                -fill "#dddddd" -under true \
                -coords [list $from -Inf $from +Inf $to +Inf $to -Inf] ]
        lappend clogv(Acq_Marks) $mk $from
    }
}



proc logvar_format_time { w t } {
    return [clock format $t -format {%H:%M}]
}

#   Drag-handles to adjust y-axis scale on stripchart
proc lv_make_yscale_knob { ax tag row dir } {
    global logwin
    array set words {Hilim "upper limit" Lolim "lower limit"}
    set win $logwin.lv_${ax}${tag}_b
    [button $win] configure \
        -activebackground $PREF::bgcol -bg $PREF::bgcol -borderwidth 1 \
        -highlightthickness 0 -padx 4 -pady 2 -takefocus 0 -command {} \
        -relief flat -text {} -image grab_handle
    logvar_bind_help lv_${ax}${tag}_b "Drag up or down to adjust $words($tag) for this y axis"
    grid $win -in $logwin.lv_graph_f -row $row -column 1 -columnspan 4 -sticky $dir
    declare_drag $win [list apply_drag_graph_limit $ax $tag] [list drag_graph_limit $ax $tag]
    lower $win $logwin.lv_graph_sc
}

proc apply_drag_graph_limit { ax tag x0 y0 x1 y1 } {
    global clogv
    set clogv($ax.$tag) [drag_graph_limit $ax $tag $x0 $y0 $x1 $y1]
    logvar_full_update
}

#  Calculate a y-axis limit based on the dragged position of a grab-handle.
#  The conversion is linear between existing limits, quadratic beyond the
#  the current limit value, and constant when it bumps into the opposite limit.
#  the x/y parameters refer to the drag operation only.
proc drag_graph_limit { ax tag x0 y0 x1 y1 } {
    global clogv logwin
    set vdiff [expr { $clogv($ax.Hilim) - $clogv($ax.Lolim) }]
    set vave [expr { 0.5 * ( abs($clogv($ax.Hilim)) + abs($clogv($ax.Lolim)) ) }]
    set vadj [expr { 0.15*$vave > $vdiff ? 0.15*$vave : $vdiff }]
    set pdiff [expr { [winfo rooty $logwin.lv_${ax}Hilim_b] - [winfo rooty $logwin.lv_${ax}Lolim_b] }]
    # two-pixels "epsilon", expressed in y-axis units:
    set eps [expr { abs( 2.0 * $vdiff/$pdiff ) }]
    switch -- $tag {
        Hilim {
            set v [expr {$clogv($ax.Hilim) + $vdiff/$pdiff * ($y1 - $y0) }]
            if { $v > $clogv($ax.Hilim) } {
                set v [expr { $v + $vadj/$pdiff * ( ($y1-$y0) - ($y1-$y0)*($y1-$y0)/300 ) }]
            }
            set v [expr {$v < $clogv($ax.Lolim)+$eps ? $clogv($ax.Lolim)+$eps : $v} ]
            set sig [expr {log10(abs(($v+$clogv($ax.Lolim))/($v-$clogv($ax.Lolim))))+1.5}]
        }
        Lolim {
            set v [expr {$clogv($ax.Lolim) + $vdiff/$pdiff * ($y1 - $y0) }]
            if { $v < $clogv($ax.Lolim) } {
                set v [expr { $v + $vadj/$pdiff * ( ($y1-$y0) + ($y1-$y0)*($y1-$y0)/300 ) }]
            }
            set v [expr {$v > $clogv($ax.Hilim)-$eps ? $clogv($ax.Hilim)-$eps : $v} ]
            set sig [expr {log10(abs(($v+$clogv($ax.Hilim))/($v-$clogv($ax.Hilim))))+1.5}]
        }
    }
    #puts "$tag $v  $sig [sigfig $v $sig]  2.5  [sigfig $v 2.5]"
    set sig [expr { 2.5>$sig ? 2.5 : $sig }]
    return [sigfig $v $sig]
}


#  Apply fixed- or auto-scaling for a graph axis.  State is given by the 
#  corresponding clogv(y.Lock) and clogv(y2.Lock) variables.

proc lv_apply_axis_lock { ax } {
    global logwin clogv
    if { ![winfo exists $logwin.lv_graph_sc] } { return }
    if { $clogv($ax.Lock) } {
        # Axis scale locked -- initialize limits and show adjustment handles
        lassign [$logwin.lv_graph_sc axis limits $ax] clogv($ax.Lolim) clogv($ax.Hilim)
    } else {
        # Axis scale unlocked -- clear limits and hide adjustment handles
        set clogv($ax.Lolim) {}
        set clogv($ax.Hilim) {}
    }
    logvar_full_update
}


# Set the time-range of plot based on sliding scale (non-linear)
#
proc logvar_set_plottime { tim_code } {
    global clogv
    # mxc = maximum tim_code (configured value on scale widget)
    set mxc [$::logwin.lv_time_scale cget -to]
    # mxt = maximum time to display, in minutes.
    set mxt $PREF::graphTimeLimit
    set r [expr {$tim_code/$mxc}]
    set clogv(Graph_To_Sec) [clock seconds]
    set clogv(Graph_Minutes) [expr { round( ($mxt-$mxc-1)*$r*$r*$r + $tim_code) + 1 }]
    if { $clogv(Graph_Minutes) >= 60 } {
        set clogv(Graph_T_lab) [format "%d min (%.1f h)" $clogv(Graph_Minutes) \
				    [expr $clogv(Graph_Minutes)/60.0]]
    } else {
        set clogv(Graph_T_lab) "$clogv(Graph_Minutes) minutes"
    }
}



#   Procedures for zooming into the plot (applies to time-scale only, in
#   this first version, because I don't want to deal with the dual y-axes.)

#   Begin the zoom process by scheduling the start to occur after a short 
#   delay so that we don't get accidental zooming if the user accidentally
#   "drags" the mouse slightly while making a regular "click".

proc lv_BeginZoom { W x y } {
    bind $W <ButtonRelease-1> { 
	after cancel $::ZoomStartToken(%W)
	# Do an immediate graph update:
	logvar_full_update
    }
    set ::ZoomStartToken($W) [ after $PREF::clickms [list lv_ActivateZoom $W $x $y] ]
}

#   When the delay expires, display the rectangle for the zoom area, and
#   enable the zoom action for when the button is released.

proc lv_ActivateZoom { W x y } {
    if { [winfo exists $W] } {
	set x0 [$W axis invtransform x $x]
	set y0 [$W axis invtransform y $y]
	bind $W <ButtonRelease-1> [list lv_FinishZoom $W $x0 $y0 %x %y]
	bind $W <B1-Motion> [list lv_DisplayZoom $W $x0 $y0 %x %y]
	# Freeze graph for one minute before returning to normal
	set clogv(PrevGraphUp) [expr { [clock seconds] + 60 }]
    }
}

#   Display or update the rectangular zoom selection.
proc lv_DisplayZoom { W x0 y0 x y } {
    set x1 [$W axis invtransform x $x]
    set y1 [$W axis invtransform y $y]
#   if { $x0 == $x1 || $y0 == $y1 } { puts "Box too small" ; return }
    if { ![ info exists ::ZoomMarkerID($W) ] } {
	set ::ZoomMarkerID($W) [$W marker create line -linewidth 1 ]
	# Perhaps add   -dashes {2 1}
	#puts "Created line marker $::ZoomMarkerID($W) anchored at ${x0} ${y0}"
    }
    $W marker configure $::ZoomMarkerID($W) -hide 0 \
	-coords [list $x0 $y0 $x0 $y1 $x1 $y1 $x1 $y0 $x0 $y0]
}

#   Zoom selection complete; apply it. 
proc lv_FinishZoom { W x0 y0 x y } {
    global clogv
    if { [winfo exists $W] } {
	set x1 [$W axis invtransform x $x]
	set y1 [$W axis invtransform y $y]
	#puts "Zoom Box is $x0,$y0 - $x1,$y1"
	if { [ info exists ::ZoomMarkerID($W) ] } {
	    $W marker delete $::ZoomMarkerID($W)
	    unset ::ZoomMarkerID($W)
	}
	bind $W <ButtonRelease-1> { logvar_full_update }
	bind $W <B1-Motion> {}
	if { $x1 == $x0 } { return }
        # We will set the graph range only for the next re-plot, and restore afterwards
        after 100 after idle [list set clogv(Graph_Minutes) $clogv(Graph_Minutes)]
        # Apply time range
	set clogv(Graph_Minutes) [ bounded [expr { round( ( abs($x1-$x0)/60.0 ) ) } ] 1 $PREF::graphTimeLimit ]
	set clogv(Graph_To_Sec) [expr { int( $x1 > $x0 ? $x1 : $x0 ) - 10 }]
        #puts "Zoom in to plot $clogv(Graph_Minutes) minutes up to [logvar_format_time {} $clogv(Graph_To_Sec)]"
	# Hold zoomed graph for at least one minute before updating
	set clogv(PrevGraphUp) [expr { [clock seconds] + 60 }]
	logvar_display_graph
    }
}


#  Periodically purge excessive data saved for the graph.  We will thin out
#  the old points, retaining half of them, rather than throwing them all away.
#  This allows a plot to extend over a longer time-range, but with coarser 
#  sampling.  To reduce the performance hit, we only trim one variable per
#  iteration.

set clogv(too_old)    15000
set clogv(rf:maxbyte) 400000
set clogv(rf:maxline) 5000
set clogv(rf:retainl) 3000
set clogv(gc:maxrec)  4500
set clogv(gc:retainp) 3000
set clogv(gc:msdelay) 300000

proc logvar_garbage_collect { } {
    global logwin clogv lv_acq_record
    # execute every 5 minutes
    after $clogv(gc:msdelay) logvar_garbage_collect
    # puts "logvar_garbage_collect"
    # Trim or expunge variable log lists
    set now [clock seconds]
    set longest 0
    foreach tag [array names clogv "T:*"] {
        set var [string range $tag 2 end]
        if { [lsearch -exact $clogv(lvnames) $var] < 0 } {
            #puts "Camp Variable $var is not logged any more "
            if { [lindex $clogv(T:$var) end] + $clogv(too_old) < $now } {
                #puts "(...not for a long time)"
                unset clogv(T:$var) clogv(V:$var)
            }
        } elseif { [set l [llength $clogv($tag)]] > $longest } {
            set longvar $var
            set longest $l
        }
    }
    # Prune long lists, retaining $clogv(gc:retainp)
    if { $longest > $clogv(gc:maxrec) } {
        #puts "garbage_collect: prune records for $longvar"
        prune_list clogv(T:$longvar) $clogv(gc:retainp)
        prune_list clogv(V:$longvar) $clogv(gc:retainp)
    }

    # Trim old graph markers
    if { [winfo exists $logwin.lv_graph_sc] } {
        set l [llength $clogv(Acq_Marks)]
        if { $l > 300 } {
            # remove stale markers, keeping recent 200
            incr l -200
            foreach {m t} [lrange $clogv(Acq_Marks) 0 $l] {
                if { [$logwin.lv_graph_sc marker exists $m] } {
                    $logwin.lv_graph_sc marker delete $m
                }
            }
            set clogv(Acq_Marks) [lrange $clogv(Acq_Marks) [incr l] end]
        }
    }
    # Trim old acquisition state records, retain 200
    set l [llength $lv_acq_record]
    if { $l > 350 } {
        incr l -200
        set lv_acq_record [lrange $lv_acq_record $l end]
    }
    # Monitor size of recovery file, and trim
    #puts "$clogv(recoveryfile) [file exists $clogv(recoveryfile)] [file size $clogv(recoveryfile)] >? $clogv(rf:maxbyte)"
    if { [file exists $clogv(recoveryfile)] && [file size $clogv(recoveryfile)] > $clogv(rf:maxbyte) } {
        global env
        catch {
            set lines [lindex [exec wc -l $clogv(recoveryfile)] 0]
            if { $lines > $clogv(rf:maxline) } {
                puts "File $clogv(recoveryfile) needs trimming"
                set headers [split [exec fgrep -n "!   time " $clogv(recoveryfile)] "\n"]
                exec tail -n $clogv(rf:retainl) $clogv(recoveryfile) > $clogv(recoveryfile)[pid]
                set lost [expr {$lines-$clogv(rf:retainl)}]
                # get the final header in the discarded lines
                set head "!   time   $clogv(lvnames)"
                foreach h $headers {
                    if { [scan $h {%d:! %s} lll ttt] == 2 && $ttt == "time" && $lll <= $lost } {
                        set head [string range $h [string first "!" $h] end]
                    }
                }
                # insert that header at top of retained records
                exec sed -i "1i $head" $clogv(recoveryfile)[pid]
                file rename -force $clogv(recoveryfile) $clogv(recoveryfile)[clock format $now -format "%y%m%d-%H"]
                file rename -force $clogv(recoveryfile)[pid] $clogv(recoveryfile)
            }
        }
    }
}

after $clogv(gc:msdelay) logvar_garbage_collect

#   Generate a printout of the graph (plus the table of values)

proc logvar_prompt_print { } {
    global logwin
    print_dialog -parent [p_win_name $logwin] -timeout 50000 -file camplog.ps \
            -savecommand  {logvar_hardcopy s} \
            -printcommand {logvar_hardcopy p}
}

# Hardcopy of the graph using blt features, now not used because it crashes.
# See below.

if 0 { proc logvar_hardcopy { tag queue file } {
    global logwin lpr_mess lpr_stat
    set lpr_stat ""
    set lpr_mess ""
    if { $tag == "p" || $file == "" } {
        set file "/tmp/camplog.ps"
    }
    set stat [ catch {
        $logwin.lv_graph_sc postscript output $file -decorations 0 -maxpect 1
        if { $tag == "p" } {
            if { [string length $queue] } {
                blt::bgexec lpr_stat -error lpr_mess lpr -P$queue -r $file
            } else {
                blt::bgexec lpr_stat -error lpr_mess lpr -r $file
            }
        } } rv ]
    if { $stat } {
        array set ps [list p printing s saving]
        beep
        timed_messageBox -timeout 30000 -type ok -icon error -title {Print error} \
                -parent [p_win_name $logwin] -message "Error $ps($tag) the plot: $rv\n$lpr_mess"
    }
}
}

#  New logvar_hardcopy using separate executable "import" (part of 
#  imagemagick).  Use it because the BLT postscript generator causes
#  segfaults (sometimes).
#
#  Raise window, allow display to update, then ...

proc logvar_hardcopy { tag queue file } {
    global logwin lpr_mess lpr_stat
    # Ensure window is uncovered, but hide any drag icons
    raise [td_win_name $logwin]
    lower $logwin.lv_yHilim_b $logwin.lv_graph_sc
    lower $logwin.lv_yLolim_b $logwin.lv_graph_sc
    lower $logwin.lv_y2Hilim_b $logwin.lv_graph_sc
    lower $logwin.lv_y2Lolim_b $logwin.lv_graph_sc
    # Perform snapshot after display updates
    after 10 after idle [list logvar_get_hardcopy $tag $queue $file]
}

# ... then take snapshot of rectangular region with containing graph and 
# table of Camp variables (executing import).  Perhaps print the file.

proc logvar_get_hardcopy { tag queue file } {
    global logwin lpr_mess lpr_stat
    set lw [td_win_name $logwin]
    set lpr_stat ""
    set lpr_mess ""
    if { $tag == "p" || $file == "" } {
        set file "/tmp/camplog.ps"
    }
    set stat [ catch {
        set id [winfo id $lw]
        scan [winfo geometry $logwin.lv_graph_f] {%dx%d%d%d} gw gh gx gy
        scan [winfo geometry $logwin.lv_vars_f] {%dx%d%d%d} vw vh vx vy
        set gh [expr {$vy-$gy+$vh}]
        set geom [format {%dx%d%+d%+d} $gw $gh $gx $gy]
        blt::bgexec lpr_stat -error lpr_mess import -window $id -crop $geom -density 100 PNG:$file
        if { $tag == "p" } {
            if { [string length $queue] } {
                blt::bgexec lpr_stat -error lpr_mess lpr -P$queue -r $file
            } else {
                blt::bgexec lpr_stat -error lpr_mess lpr -r $file
            }
        } } rv ]

    # Now restore proper appearance
    lv_apply_axis_lock y
    lv_apply_axis_lock y2

    # Handle error conditions
    if { $stat } {
        array set ps [list p printing s saving]
        beep
        timed_messageBox -timeout 30000 -type ok -icon error -title {Print error} \
                -parent [p_win_name $logwin] -message "Error $ps($tag) the plot: $rv\n$lpr_mess"
    }
}

# Apply the manual setting of the Camp host name.
#
proc logvar_apply_camphost { } {
    uplevel \#0 {
        puts "Change camp host from $midas(camp_host) to $clogv(entry_camp_host)"
        set midas(camp_host) $clogv(entry_camp_host)
        set midas(PrevGraphUp) 0
        restart_camp_subserver
        logvar_request_update
    }
} 

# The following procedures deal with the entry of the log file name.  What a mess!
# I shouldn't have made simultaneous popup and text-entry selection methods, and
# I should have had an "apply" or "set" button rather than using "Return" and
# "Focusout".  I will deactivate the entry, so users just use the popup to set
# the logfile.  Later, we can simplify the following routines.

proc logvar_get_logfile { } {
    global clogv logwin
    # temporarily disable <FocusOut> binding so it doesn't fire for the dialog
#    bind $logwin.lv_file_e <FocusOut> {}

    set dflt .log
    set types {{"Log files" {.log}} {"Data files" {.dat}}}
    if { [string length $clogv(filename)] } {
        set dflt [file extension $clogv(filename)]
        if { [ string compare $dflt .log ] && [ string compare $dflt .dat ] } {
            set types [linsert $types 0 [list "$dflt files" $dflt]]
        }
    }
    set clogv(entry_filename) [ tk_getSaveFile -title "Select log file" \
            -defaultextension $dflt -filetypes $types \
            -initialdir [file dirname $clogv(entry_filename)] \
            -initialfile [file tail $clogv(entry_filename)] \
            -parent [p_win_name $logwin] ]
    logvar_set_logfile
}

proc logvar_apply_logfile { } {
    global clogv logwin

    # Don't do anything if we are already logging in that file
    if { [string equal $clogv(entry_filename) $clogv(filename)] } {
        return
    }
    # temporarily disable <FocusOut> binding so it doesn't fire for the message box.
    #    bind $logwin.lv_file_e <FocusOut> {}
    # after 900 { bind $logwin.lv_file_e <FocusOut> logvar_apply_logfile }
    if { [string length $clogv(entry_filename)] && [file exists $clogv(entry_filename)] } {
        beep
        switch [ timed_messageBox -parent [p_win_name $logwin] -timeout 30000 \
                -type yesno -default no -icon question -title "Replace existing file?" \
                -message "File $clogv(entry_filename) already exists.\nDo you want to overwrite it?" ] {
            yes { logvar_set_logfile }
        }
    } else {
        logvar_set_logfile
    }
}

proc logvar_set_logfile { } {
    global clogv logwin env

    # Restore the <FocusOut> binding
    # after 900 { bind $logwin.lv_file_e <FocusOut> logvar_apply_logfile }

    if { $clogv(entry_filename) == "" } {
        if { [string length $clogv(filehandle)] } {
            beep
            switch [ timed_messageBox -parent [p_win_name $logwin] -timeout 30000 \
                    -type yesno -default no -icon question -title "Stop logging?" \
                    -message "Stop logging to file $clogv(filename)?" ] {
                no { set clogv(entry_filename) $clogv(filename) ; return }
            }
            close $clogv(filehandle)
            set clogv(filehandle) {}
        }
    } else {
        if { [string length $clogv(filehandle)] } {
            close $clogv(filehandle)
            set clogv(filehandle) {}
        }
        # Trim verbose home directory path
        #puts "Matching $env(HOME) to $clogv(entry_filename):  [string match $env(HOME)* $clogv(entry_filename)]"
        if { [string match "$env(HOME)*" $clogv(entry_filename)] } {
            # regsub will not work for odd values of HOME
            regsub "^$env(HOME)" $clogv(entry_filename) ~ clogv(entry_filename)
        } else {
            set cd [pwd]
            cd $env(HOME)
            set home [pwd]
            cd $cd
            #puts "Matching $home to $clogv(entry_filename):  [string match $home* $clogv(entry_filename)]"
            if { [string match "$home*" $clogv(entry_filename)] } {
                # regsub will not work for odd values of HOME
                regsub "^$home" $clogv(entry_filename) ~ clogv(entry_filename)
            }
        }

        if { [file exists $clogv(entry_filename)] } {
            if { [catch {file delete $clogv(entry_filename)}] } {
                beep
                timed_messageBox -parent [p_win_name $logwin] -timeout 30000 \
                        -type ok -icon error -title "Failed replace" \
                        -message "Failed to replace file $clogv(entry_filename)"
                return
            }
        }
        if { [catch {open $clogv(entry_filename) w} clogv(filehandle)] } {
            beep
            set clogv(filehandle) {}
            timed_messageBox -parent [p_win_name $logwin] -timeout 30000 \
                    -type ok -icon error -title "Failed open" \
                    -message "Failed to open log file $clogv(entry_filename)"
            return
        }
    }
    # If we opened a file, display; else if we closed a file, hide.
    if { [string length $clogv(entry_filename)] } {
        logvar_show_logfile
        # Ensure column headings are in new file
        set clogv(logged_lvnames) ""
        set clogv(previous_log) 0
    } elseif { [string length $clogv(filename)] } {
        logvar_hide_logfile
    }

    set clogv(filename) $clogv(entry_filename)
}

#  Hide the widgets for file-logging, leave only a button to call them up.
#  Make the remainder be centered.
proc logvar_hide_logfile { } {
    global logwin
    grid remove $logwin.lv_get_logfile_b $logwin.lv_file_e $logwin.lv_refresh_l $logwin.lv_interval_e
    grid $logwin.lv_dolog_b -in $logwin.lv_file_f
    grid configure $logwin.lv_file_f -sticky {}
}

#  Show the widgets for file-logging, hide the button to call them up.
#  Fill the window sideways.
proc logvar_show_logfile { } {
    global logwin
    grid $logwin.lv_get_logfile_b $logwin.lv_file_e $logwin.lv_refresh_l \
	$logwin.lv_interval_e -in $logwin.lv_file_f
    grid remove $logwin.lv_dolog_b
    grid configure $logwin.lv_file_f -sticky ew
}

#  Read in saved file to pre-load stripchart data.

proc logvar_restore_log { } {
    global clogv
    set now [clock seconds]
    #puts "logvar_restore_log -- Recover logged vars from file $clogv(recoveryfile)"
    if { ![file exists $clogv(recoveryfile)] || [file mtime $clogv(recoveryfile)] < $now - $clogv(too_old) } {
        return
    }
    set fh [open $clogv(recoveryfile) r]
    set tlim [expr {$now - 60*$PREF::graphTimeLimit}]
    puts "Recover logged vars since $tlim ([clock format $tlim])"
    set vars {}
    set n 0
    set n0 0
    set n1 0
    while { ![eof $fh] } {
	incr n0
        set n [gets $fh line]
        if { $n < 0 } { break }
        if { [string match {!   time*} $line] } {
            set vars [lrange $line 2 end]
	    #puts "varlist $vars"
        } elseif { [llength $line] == [llength $vars] + 1 } {
            set t [lindex $line 0]
            if { $t > $tlim } {
		incr n1
                foreach lvar $vars v [lrange $line 1 end] {
                    lappend clogv(T:$lvar) $t
                    lappend clogv(V:$lvar) $v
                }
            }
        }
    }
    puts "Restored $n1 entries ($n0 lines read)"
    close $fh
}

#  Start-up and initialization code:
if { $clogv(camp_only) } {

    set run(expertype) "Independent"
    set run(ImuSR) 0

    # Initializations.  Use clogv(select_vars) for list of variables
    set run(start_sec) 0
    set clogv(select_vars) [list]
}

if { $logwin == "" } {
    # stand-alone testing, so start immediately!
    puts "Stand-alone logvar testing"
    set run(autoBvar) {none}      ;# Camp variable path used for automatic Field header, or "none"
    set run(autoTvar) {none}      ;# Camp variable path used for automatic Temperature header, or "none"
    set run(autoBerror) 1         ;# Boolean: include error with automatic Field header
    set run(autoTerror) 1         ;# Boolean: include error with automatic Temperature header
    set run(start_sec) 0
    set run(expertype) "TD-\u00b5SR"
    set run(ImuSR) 0
    set run(number) 1
    set run(in_progress) 0
    set wtstat(enabled) 0
    set run(state) 0
    logvar_initialize
}
