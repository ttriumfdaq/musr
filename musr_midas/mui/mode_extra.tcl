# Additions to and procedures for mode.ui.tcl
# $Log: mode_extra.tcl,v $
# Revision 1.28  2016/09/06 22:13:45  asnd
# Improve tracking of unexpected changes
#
# Revision 1.27  2015/09/29 01:24:13  asnd
# Prevent consumption of all of maxbins, avoid buffer overflow in fev680
#
# Revision 1.26  2015/03/20 00:47:01  suz
# changes by Donald. This version part of package for new VMIC frontends
#
# Revision 1.25  2013/10/30 05:52:25  asnd
# Button to set T2bin enabled when run in progress
#
# Revision 1.24  2006/12/07 02:51:06  asnd
# Add a warning for 2-byte bins. (Also brace some expressions.)
#
# Revision 1.23  2006/06/29 07:29:57  asnd
# Use safer p_win_name (from mui_utils.tcl) for dialogs
#
# Revision 1.22  2006/05/26 17:05:31  asnd
# Accumulated changes over winter shutdown
#
# Revision 1.21  2005/06/22 15:31:52  asnd
# update comment
#
# Revision 1.20  2004/04/10 07:00:07  asnd
# Add auto-run control
#
# Revision 1.19  2003/10/29 15:26:12  asnd
# Allow cancel of loading old run mode
#
# Revision 1.18  2003/10/29 15:17:11  asnd
# Better alignment; no pulser control yet
#
# Revision 1.17  2003/10/15 09:28:28  asnd
# Add "yield" operation for shutdown; changes to client checks; other small changes.
#
# Revision 1.16  2003/09/26 00:39:03  asnd
# Recent gui changes (sweep details, ggl pulser...)
#
# Revision 1.15  2003/09/19 06:23:27  asnd
# ImuSR and many other revisions.
#
# Revision 1.14  2003/05/14 02:38:40  asnd
# Fix odb_get_runinfo misspelling; use [file join] instead of explicit "/".
#
# Revision 1.13  2003/03/14 02:40:22  asnd
# Change Gate generator code calculation.
#
# Revision 1.12  2002/11/14 00:08:49  asnd
# Column-copy of bin-range parameters ( |, shift-up/down)
#
# Revision 1.11  2002/11/07 00:04:42  asnd
# Update Load-mode menu after changes to list of modes
#
# Revision 1.10  2002/11/06 23:48:05  asnd
# Update mode menu after deletions
#
# Revision 1.9  2002/10/04 02:52:18  asnd
# Let mode-change notice when run has ended
#
# Revision 1.8  2002/09/21 04:39:22  asnd
# Loading modes from previous runs
#
# Revision 1.7  2002/08/24 08:21:20  asnd
# fix rename mode checks
#
# Revision 1.6  2002/07/19 11:49:45  asnd
# Fix several mode/rig window interactions
#
# Revision 1.5  2002/07/16 06:00:53  asnd
# Changes up to Jul 15
#
# Revision 1.4  2002/06/19 08:55:36  asnd
# Small esthetic gui changes
#
# Revision 1.2  2002/06/15 01:05:48  asnd
# Ensure rig exists
#
# Revision 1.1  2002/06/12 01:26:39  asnd
# Starting versions of Mui files.
#

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

# modewin is the window name for the show/change mode window
# mode contains the midas mode-related parameters
# mmode mirrors the mode array but also contains unapplied changes
# amode contains ancillary vars used for the mode gui functioning

global modewin mode mmode amode

# Fire up the mode window. When used for real, mode_initialize is invoked by the
# [mode] or [show mode] buttons, or when changing the rig (or when a change in the
# rig is detected).  When testing the gui it can be invoked at the end of this 
# source file.

proc mode_initialize { } {
    global rig mmode mode modewin text_Hist_r

    set mw [td_win_name $modewin]

    if { [ string length $modewin ] } {
        # We are in a real application, where the mode window is not main, and
        # mode_initialize is invoked by [mode] buttons
        if { [winfo exists $modewin.m_OK_b] } {
            # if window exists, make it visible
            wm deiconify $mw
            raise $mw
        } else {
            # window is not built yet. 
            toplevel $modewin
            mode_ui $modewin
        }
    }

    # undo a bad spectcl binding.
    bind $mw <Destroy> {}

    # add our own destroy binding, but not to a parent!
    bind $modewin.m_OK_b <Destroy> { set mmode(changing) 0 ; set mode(changing) 0 }

    # initialize a label to neither normal value
    set text_Hist_r "."

    # Even with some duplicate entries, it is simpler to bind most help strings directly
    # rather than putting them in an array.  Help text for dynamic table of histogram 
    # parameters are in the array modehelpstrings.
    mode_bind_help m_change_b "Change the current mode settings (Alt-C)"
    mode_bind_help m_apply_b  "Apply (and save) the current mode settings (Alt-A)"
    mode_bind_help m_saveas_b "Write a new mode with current parameters; leave old behind (Alt-S)"
    mode_bind_help m_rename_b "Write a new mode with current parameters; delete old (Alt-M)"
    mode_bind_help m_delete_b "Delete some other saved mode (Alt-D)"
    mode_bind_help m_load_mb  "Load a previously saved set of mode parameters (Alt-L)"
    mode_bind_help m_TDCg_add_e  "Extra time added to TDC gate (beyond histogram length)"
    mode_bind_help m_PUg_add_e   "Extra time added to pile-up gate (beyond TDC gate)"
    mode_bind_help m_hislen_b_e  "Histogram length, in bins (multiple of 256)"
    mode_bind_help m_hislen_us_e "Histogram length, in microseconds"
    mode_bind_help m_binsize_mb  "TDC resolution; histogram time-per-bin"

    wm title $mw "Mode Settings"
    bind $mw <Alt-c> "$modewin.m_change_b invoke ; break"
    bind $mw <Alt-r> "$modewin.m_revert_b invoke ; break"
    bind $mw <Alt-a> "$modewin.m_apply_b invoke ; break"
    bind $mw <Alt-s> "$modewin.m_saveas_b invoke ; break"
    bind $mw <Alt-m> "$modewin.m_rename_b invoke ; break"
#    bind $mw <Alt-p> "$modewin.m_pulser_b invoke ; break"
    bind $mw <Alt-d> "$modewin.m_delete_b invoke ; break"
    bind $mw <Alt-q> "$modewin.m_cancel_b invoke ; break"
    bind $mw <Alt-o> "$modewin.m_OK_b invoke ; break"
    bind $mw <Alt-k> "$modewin.m_OK_b invoke ; break"
#   Menu buttons get alt- binding automatically:
#   bind $mw <Alt-l> "$modewin.m_load_mb invoke ; break"

#   Declare widget rows for counters in current rig (using the lesser of num_counters
#   and the length of the counter list.

    set mmode(num_counters) $rig(num_counters)

    for { set ic 0 } { $ic < $cfg::numCounter } { incr ic } {
        if { $ic < $mmode(num_counters) } {
            set counter [lindex $rig(counter_names) $ic]
        } else {
            set counter ""
        }

        if { [ string compare $counter "" ] } {
            if { ! [winfo exists $modewin.m_c${ic}_name_c] } {
                mode_make_counter_row $counter $ic
            }
        } else {
            if { [winfo exists $modewin.m_c${ic}_name_c] } {
                mode_destroy_counter_row $counter $ic
            }
        }
    }
    set mmode(changing) 0
    mode_revert_change
}


#   Activate widgets to allow changing mode parameters.  The widget layout (using SpecTcl)
#   should be careful so that activating widgets does not cause a change of size, because
#   that "redraw" is visually annoying (and too slow for this complex window; its OK for
#   the rig window).

proc mode_begin_change {} {
    global mmode modewin modehelpstrings
    global run mrig
    global duallabel
#
    if { $mrig(changing) } {
        beep
        switch [ timed_messageBox -parent [p_win_name $modewin] -timeout 30000 \
                -type yesnocancel -default cancel -icon question -title "Really change mode?" \
                -message "The rig is being changed!\nApply rig changes (yes/no)\nor cancel mode change?" ] {
            yes { rig_apply_change ; rig_end_change }
            no { }
            cancel { return }
        }
    }

    set mmode(changing) 1
    set mmode(run_in_progress) $run(in_progress)
    $modewin.m_change_b configure -state disabled
    $modewin.m_apply_b configure -state normal
    $modewin.m_revert_b configure -text "Revert"
    mode_bind_help m_revert_b "Un-do the changes (Alt-R)"
    mode_bind_help m_cancel_b "Close mode window (abandoning changes) (Alt-Q)"
    mode_bind_help m_OK_b "Close mode window (applying changes) (Alt-O, Alt-K)"
    mode_display_counters
    showButton $modewin.m_applen_b
    mode_bind_help m_applen_b "Make histogram good-bin ranges fit histogram length"
    #  ??? until I decide to use it:
    hideButton $modewin.m_pulser_b -disabledforeground $::PREF::bgcol
    if { $run(in_progress) } {
        set mmode(onlybins) "Run in progress.  Only the bin ranges may be changed"
    } else {
        set mmode(onlybins) ""
        showButton $modewin.m_binsize_mb
        showButton $modewin.m_dualspec_mb
        $modewin.m_hislen_s configure -state normal -fg $PREF::fgcol -troughcolor $PREF::entrybg \
                -highlightcolor $PREF::brightcol -sliderlength 20 -borderwidth 2 -width $PREF::slidewidth
        mode_bind_help m_hislen_s $modehelpstrings(histlenus)
#        showButton $modewin.m_pulser_b
#        mode_bind_help m_pulser_b "Set GGL pulser output (Alt-P)"
        set l [list m_hislen_b_e m_hislen_us_e]
        if { $mmode(dual_enabled) } {
            lappend l m_samptitle_e m_samptag_e m_reftitle_e m_reftag_e
            set duallabel "Dual names / tags"
        }
        foreach e $l {
            activateEntry $modewin.$e -fg $PREF::fgcol
        }
        bind $modewin.m_hislen_us_e <FocusIn> { set prevnumericv $mmode(hlen_us) }
        bind $modewin.m_hislen_us_e <KeyRelease> \
                        { keep_numeric mmode(hlen_us); mode_histlenus e $mmode(hlen_us) }
        bind $modewin.m_hislen_us_e <Control-u> { set mmode(hlen_us) {} }
        bind $modewin.m_hislen_us_e <<ApplyEntry>> { mode_adjust_ranges 1 1.0 }
        bind $modewin.m_hislen_b_e <FocusIn> { set prevnumericv $mmode(hlen) }
        bind $modewin.m_hislen_b_e <KeyRelease> { keep_numeric mmode(hlen); mode_histlenb }
        bind $modewin.m_hislen_b_e <Control-u> { set mmode(hlen) {} }
        bind $modewin.m_hislen_b_e <<ApplyEntry>> { mode_adjust_ranges 1 1.0 }
    }
#   We are able to change gate lengths while running!
    activateEntry $modewin.m_TDCg_add_e
    activateEntry $modewin.m_PUg_add_e
    bind $modewin.m_TDCg_add_e <FocusIn> { set prevnumericv $mmode(TDCgate_add) }
    bind $modewin.m_TDCg_add_e <Control-u> { set mmode(TDCgate_add) {} }
    bind $modewin.m_TDCg_add_e <KeyRelease> { keep_numeric mmode(TDCgate_add); mode_show_gates }
    bind $modewin.m_TDCg_add_e <<ApplyEntry>> { mode_adjust_ranges 0 1.0 }
    bind $modewin.m_PUg_add_e <FocusIn> { set prevnumericv $mmode(PUgate_add) }
    bind $modewin.m_PUg_add_e <KeyRelease> { keep_numeric mmode(PUgate_add); mode_show_gates }
    bind $modewin.m_PUg_add_e <Control-u> { set mmode(PUgate_add) {} }
    bind $modewin.m_PUg_add_e <<ApplyEntry>> { mode_adjust_ranges 0 1.0 }

#   Change activation order for apply-ranges button to get focus after the hist-length
#   slider.  (Note the effect of raise and lower on focus-order is counter-intuitive!)
    raise $modewin.m_applen_b $modewin.m_hislen_s

}

#   Deactivate widgets.  Invoked when mode window is (re)displayed and, of course, at
#   the end of some changes.  See notes about constant sizes, above.

proc mode_end_change {} {
    global modewin mmode duallabel
    set mmode(onlybins) ""
    set mmode(changing) 0
    $modewin.m_change_b configure -state normal 
    $modewin.m_apply_b  configure -state disabled
    $modewin.m_revert_b configure -text "Refresh"
    mode_bind_help m_revert_b "Refresh displayed mode parameters (Alt-R)"
    mode_bind_help m_cancel_b "Close mode window (Alt-Q)"
    mode_bind_help m_OK_b "Close mode window (Alt-O, Alt-K)"
    # The only way to hide the histogram-length slider's trough is to set the borderwidth to zero, so 
    # we increase the (trough) width to compensate (4 borders * 2 pixels), keeping the same total size.
    $modewin.m_hislen_s configure -state disabled -fg $PREF::bgcol -troughcolor $PREF::bgcol \
            -highlightcolor $PREF::bgcol -sliderlength 0 -borderwidth 0 -width [expr {$PREF::slidewidth+8}]
    mode_bind_help m_hislen_s ""
    hideButton $modewin.m_applen_b
    mode_bind_help m_applen_b ""
    # Thoroughly hide until it is integrated into acq
    hideButton $modewin.m_pulser_b -disabledforeground $::PREF::bgcol
    mode_bind_help m_pulser_b ""
    hideButton $modewin.m_binsize_mb
    hideButton $modewin.m_dualspec_mb
    foreach e [list m_hislen_b_e m_hislen_us_e m_TDCg_add_e m_PUg_add_e] {
        deactivateEntry $modewin.$e
    }
    if { $mmode(dual_enabled) } {
        foreach e [list m_samptitle_e m_samptag_e m_reftitle_e m_reftag_e] {
            # deactivate but display samp/ref titles in reverse-colors. Use the highlight border
            # to expand the background area.
            deactivateEntry $modewin.$e -fg $PREF::revfgcol -bg $PREF::revbgcol \
                -highlightcolor $PREF::revbgcol -highlightbackground $PREF::revbgcol
#
#            $modewin.$e configure -state disabled -cursor {} -relief flat \
#                    -fg $PREF::revfgcol -bg $PREF::revbgcol \
#                   -highlightcolor $PREF::revbgcol -highlightbackground $PREF::revbgcol
        }
    }
    set duallabel " "
    mode_display_counters
}


#   Re-assert existing mode parameters, discarding on-screen changes.
#   It is used by the [Revert]/[Refresh] button, by mode_initialize, by mode_load,
#   and by mode_apply_change (when not confirmed).  The optional "partial" flag 
#   indicates whether to revert only those parameters that must remain constant
#   during a run. 
#
proc mode_revert_change { {partial 0} } {
    global modewin mmode rig mode
#   remember if we are changing mode
    set changing $mmode(changing)

#   Get the odb mode pars.
    if { ![ td_odb odb_get_mode "get mode parameters"] } { return }
    set mode(num_counters) $rig(num_counters)
    if { $partial } {
        foreach item {dual_titles dual_enabled dual_suffs resolution hlen bytesbin 
            numsel selcounters numhist binsize binsize_label hlen_us hlen_label} {
            set mmode($item) $mode($item)
        }
    } else {
        array set mmode [array get mode]
    }
#   foreach v [array names mmode] { puts "mode($v) = $mmode($v)" }

#   Check if selected counters are sensible
    set msg ""
    if { $mmode(numsel) < 1 || [llength $mmode(selcounters)] < $mmode(numsel) } {
        set msg {No counters were selected!  Force selection of all.}
    } else {
        set mmode(selcounters) [lrange $mmode(selcounters) 0 [expr $mmode(numsel) - 1] ]
        foreach counter $mmode(selcounters) {
            if { [lsearch -exact $rig(counter_names) $counter] == -1 } {
                puts "counters = $rig(counter_names)"
                puts "selected = $mmode(selcounters)"
                puts "mis-match on $counter"
                set msg {Name mismatch for selected counters. Force selection of all.}
                break
            }
        }
    }
    if { [string length $msg] } {
        set mmode(selcounters) $rig(counter_names)
        set mmode(numsel) $rig(num_counters)
        mode_show_values 1
        beep
        timed_messageBox -timeout 30000 -type ok -icon warning -message $msg \
                 -parent [p_win_name $modewin] -title {Bad counter selection}
    } else {
        mode_show_values $changing
    }
}

#   mode_update: update the mode display.
#   If no change is in progress, the displayed parameters are refreshed from the
#   odb's values, and redisplayed.  Thisis like mode_revert_change but without the 
#   validity-checking, the popup error messages, and getting the odb parameters.
#   If there is a limited change in progress (as if a run is in progress) but the
#   run has ended, then convert to full change mode.
#   mode_update is used (in the background) by td_all_update, therefore, it checks 
#   that the mode display is active before updating it.
#   
proc mode_update { } {
    global mmode rig run mode modewin

    if { [set changing $mmode(changing)] } { 
        if { !$run(in_progress) && $mmode(run_in_progress) && [winfo exists $modewin.m_OK_b]} {
            set mmode(run_in_progress) $run(in_progress)
            mode_begin_change; mode_display_dual
        }
    } else {
        array set mmode [array get mode]
        set mmode(num_counters) $rig(num_counters)
        if { [winfo exists $modewin.m_OK_b] } {
            mode_show_values 0
        }
    }
}

#   After getting, and perhaps correcting, the mode parameters, update their display.
#   The boolean parameter $changing indicates whether we want to change.  This proc
#   is used by mode_revert_change and mode_updte, which differ in how they treat error
#   conditions.

proc mode_show_values { changing } {
    global rig mmode amode 

#   Assemble working array from lists of values.
    set ic -1
    set ih 0
    foreach counter $rig(counter_names) {
        incr ic
#       get corresponding mode counter index -- index of counter $ic as it is listed by the mode:
        set mic [lsearch -exact $mmode(selcounters) $counter]
        if { $mic >= 0 } {
            set amode(c${ic}_sel) 1
            catch {
                foreach b [list tzero fgbin lgbin fback lback] {
                    set amode(${b}_c${ic}s) [lindex $mmode($b) $mic]
                    set amode(${b}_c${ic}r) [lindex $mmode($b) [expr $mmode(numsel) + $mic] ]
                }
            }
            incr ih
        } else {
            set amode(c${ic}_sel) 0
        }
    }
#   Sample/reference names
    catch {
        set mmode(samptitle) [lindex $mmode(dual_titles) 0]
        set mmode(samptag) [lindex $mmode(dual_suffs) 0]
        set mmode(reftitle) [lindex $mmode(dual_titles) 1]
        set mmode(reftag) [lindex $mmode(dual_suffs) 1]
        set mmode(dualspec) [lindex "DISABLED ENABLED" $mmode(dual_enabled)]
    }
#   Num hists
    set mmode(numhist) [expr { $mmode(dual_enabled) ? 2*$mmode(numsel) : $mmode(numsel) } ]

#   gate lengths
#    set mmode(TDCgate_add) $mmode(default_gate_add)
    catch {expr {$mmode(TDCgate_code)*0.01} } mmode(TDCgate_add)
#    set mmode(PUgate_add) $mmode(default_gate_add)
    catch {expr {$mmode(PUgate_code)*0.01} } mmode(PUgate_add)

#   Hist length
    mode_histlenb

    mode_make_menu

#   Bin-size button/menu.  Setting binsize to zero prevents adjustment of last-good-bin.
    set mmode(binsize) 0.0
    catch { mode_set_binsize $mmode(resolution) }
#
#   Propagate these values to some others, and adjust display
    if { $changing } {
        mode_begin_change 
        mode_display_dual
    } else {
        mode_display_dual
        mode_end_change
    }
}

#   Apply mode changes.  
#   The "cmd" parameter indicates what to do afterwards, if successfully applied.
#
proc mode_apply_change { cmd } {
    global modewin mmode run
#   First, validate values
    mode_adjust_ranges 0 1.0
    if { $mmode(numsel) < 1 } {
        beep
        timed_messageBox -timeout 30000 -type ok -icon error \
                -title "Invalid mode" -parent [p_win_name $modewin] \
                -message "Invalid settings:\nNo counters selected!"
        return
    }
#   check odb to see if a run has been started while changing.
    catch { odb_get_runinfo }
    if { $run(in_progress) && ! $mmode(run_in_progress) } {
        beep
        if { "ok" == [ timed_messageBox -parent [p_win_name $modewin] -timeout 30000 \
                -type okcancel -default ok -icon question \
                -message "A run has been started!\nApply just bin-range changes?" \
                -title "Confirm apply" ] } {
            mode_do_apply_change
        }
        # Do readback of real parameters to re-synchronize:
        mode_revert_change
        mode_end_change
        mode_begin_change
    } else {
        mode_do_apply_change
    }
    eval $cmd
}

proc mode_do_apply_change {} {
    global mode amode mmode rig modewin

#   First, save backup copies

    mode_make_backup

#   Assemble lists of values from array.  This could have been done
#   more easily inside mode_display_counters, but that gets used a
#   lot, and I don't want to slow it down.
    foreach b [list selcounters tzero fgbin lgbin fback lback] {
        set mmode($b) ""
    }
    set ic -1
    foreach counter $rig(counter_names) {
        incr ic
        if { $amode(c${ic}_sel) } {
            lappend mmode(selcounters) $counter
            foreach b [list tzero fgbin lgbin fback lback] {
                lappend mmode($b) $amode(${b}_c${ic}s)
    }   }   }
#   now do the reference histograms
    if { $mmode(dual_enabled) } {
        set ic -1
        foreach counter $rig(counter_names) {
            incr ic
            if { $amode(c${ic}_sel) } {
                foreach b [list tzero fgbin lgbin fback lback] {
                    lappend mmode($b) $amode(${b}_c${ic}r)
    }   }   }   }
    set mmode(selcounters) [lrange [lappend mmode(selcounters) . . . . . . . . ] 0 7]
    foreach b [list tzero fgbin lgbin fback lback] {
        set mmode($b) [lrange [lappend mmode($b) \
                -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1] 0 15]
    }
    mode_set_bytesbin
    set mmode(Dgate_code) [bounded [expr {round($mmode(Data_gate)/0.01)}] 100 2047] 
    set mmode(TDCgate_code) [bounded [expr {round($mmode(TDCgate_add)/0.01)}] 2 127] 
    set mmode(PUgate_code) [bounded [expr {round($mmode(PUgate_add)/0.01)}] 2 127] 
    set mmode(dual_titles) [list $mmode(samptitle) $mmode(reftitle)]
    set mmode(dual_suffs) [list $mmode(samptag) $mmode(reftag)]
#
#   copy modified mode settings to mode
#
    array set mode [array get mmode]
 
#   If current rig has never been saved, there will be no place to save the
#   mode, so save rig first.
 
    if { ! [ file exists [file join $rig(rigpath) $rig(rigname) $rig(rigname).rig] ] } {
        td_odb odb_set_rig "save rig"
    }

    td_odb odb_set_mode "save mode"
}

proc mode_close { apply } {
    global modewin mmode mode
    if { $mmode(changing) } {
        if { $apply } {
            mode_apply_change { mode_end_change }
        } else {
            mode_end_change
        }
    } 
    if { ! $mmode(changing) } {
        destroy [td_win_name $modewin]
        mode_update
    }
}

#        beep
#        set confirm [ timed_messageBox -timeout 30000 \
#                -parent [p_win_name $modewin] -title "Confirm close" \
#                -type okcancel -default ok -icon question \
#                -message "Mode is being changed!  Apply changes?" ]
#        if { $confirm == "ok" } {
#            mode_apply_change mode_end_change
#        }
#    }
#    switch $confirm {
#        ok { destroy [td_win_name $modewin] }
#        default {}
#    }
#}

#   mode_make_menu builds the menu of loadable modes.  It is invoked when 
#   the mode pars are read from odb, and when some local action changes the 
#   list of available modes.  (The mode window may not be active; in which 
#   case, do nothing.)

proc mode_make_menu { } {
    global rig run mode mmode modewin

    if { ![winfo exists $modewin.m_load_mb] } { return }
    if { $run(in_progress) } {
        $modewin.m_load_mb configure -state disabled
        return
    }

    $modewin.m_load_mb configure -state normal

    $modewin.m_load_mb.m delete 0 last
    $modewin.m_load_mb.m configure -tearoff 0

    $modewin.m_load_mb.m add command -label "Old run" -command "mode_load_old_run"

    foreach n $mode(backups) {
        set f [file join $rig(rigpath) $rig(rigname) $n]
        if { [file exists $f] } {
            $modewin.m_load_mb.m add command -label $n -command "mode_load_file $f"
        }
    }

    $modewin.m_load_mb.m add separator

    foreach f [lsort [glob -nocomplain [file join $rig(rigpath) $rig(rigname) *.mode] ] ] {
        $modewin.m_load_mb.m add command -command "mode_load_file $f" \
            -label [file rootname [file tail $f] ]
    }
}

set restomess {}
proc mode_load_old_run { } {
    global rig midas muisrc modewin

    set fname [ tk_getOpenFile -defaultextension ".odb" -filetypes {{"ODB files" {.odb} }} \
            -initialdir $midas(data_dir) -title "Select run" ]

    if { $fname == "" } { return }

    set af [ after 1500 { set ::perlstat timeout } ]
    set ::restomess {}
    if { [ catch {
        blt::bgexec ::perlstat -error ::restomess $muisrc/extract_branch.pl \
                $fname $rig(rigpath)/$rig(rigname)/temp_mode.odb \
                /Equipment/MUSR_TD_acq/Settings/mode
    } rc ] } {
        after cancel $af
        beep
        if { [string length $::restomess] == 0 } {
            set ::restomess $rc
        }
        after idle [list timed_messageBox -parent [p_win_name $modewin] -timeout 30000 \
                -type ok -icon error -title "Restore Failed" \
                -message "Failure restoring mode:\n$::restomess" ]
    } else {
        after cancel $af
        mode_load_file [file join $rig(rigpath) $rig(rigname) temp_mode.odb]
    }
}

proc mode_make_backup { } {
    global mmode mode rig

    if { ! [ file exists [file join $rig(rigpath) $rig(rigname) $mode(modename).mode] ] } {
        return
    }

    set i [expr $mode(num_back) - 1]
    set ffrom [file join $rig(rigpath) $rig(rigname) [lindex $mode(backups) $i]]
    while { $i > 0 } {
        set fto $ffrom
        incr i -1
        set ffrom [file join $rig(rigpath) $rig(rigname) [lindex $mode(backups) $i]]
        if { [file exists $ffrom] } {
            file rename -force $ffrom $fto
        }
    }
    file copy -force [file join $rig(rigpath) $rig(rigname) $mode(modename).mode] $ffrom
    mode_make_menu
}

#   Load a mode file.  This is typically called from the mode [Load] menu,
#   but may be invoked after loading a rig, possibly without the mode window defined.
   
proc mode_load_file { f } {
    global mmode run modewin

    if { ![td_odb odb_get_runinfo "get run state"] } { return }
    if { $run(in_progress) } {
        beep
        timed_messageBox -parent [p_win_name $modewin] -timeout 30000 \
                -type ok -icon error -title "Run in progress!" \
                -message "Loading mode not allowed while a run is in progress!"
        # Update load menu because it was obviously out of synch.
        mode_make_menu
        return
    }
    #  Perhaps put mode_make_backup here, but I think not.

    # puts "Load new mode: [file rootname [file tail $f] ] from file $f"

    if { ![td_odb "odb_load_file \"$f\"" "load mode file"] } { return }
    # then read back that mode
    if { [winfo exists [td_win_name $modewin]] } { 
        mode_revert_change
    } else {
        mode_initialize
    }
}

proc mode_save_as { } {
    set modes [mode_list_modes "" ]
    mode_do_save_as $modes "Enter a name for the new mode:"
    mode_make_menu
}

#   Get a new mode name and save mode.  Use list $modes
#   and text $blurb to prompt.  Return the new mode name, or null.

proc mode_do_save_as { modes blurb } {
    global mmode rig modewin

    set newmode [ popup_selector "Choose new mode" $blurb \
         $modes "" single false 90 ]

    # sanitize name to allow only sensible characters
    set newmode [ file tail $newmode ]
    set newmode [ join [split $newmode { `~!@#$%^&*()-+={}[];:'",<>?/} ] {} ]

    if { [string length $newmode] == 0 } { return "" }

    # Check if new mode already exists
    if { [ lsearch -exact $modes $newmode ] < 0 } {
        lappend modes $newmode
    } else {
        beep
        switch [ timed_messageBox -timeout 30000 -parent [p_win_name $modewin] \
                -type okcancel -icon warning -title "Replace mode?" \
                -message "Mode $newmode already exists!\nOver-write OK?" ] {
            ok { }
            default { return "" }
        }
    }

    set mmode(modename) $newmode
    #  do save mode:
    mode_do_apply_change
    return $newmode
}

proc mode_rename { } {
    global mmode rig

    set oldmode $mmode(modename)

#   Prompt with list of other modes:
    set modes [mode_list_modes $oldmode ]

    set newmode [ mode_do_save_as $modes "Enter the new name for mode $oldmode:" ]
    if { $newmode == "" || $oldmode == "" } { return }

#   delete old mode
    file delete -- [file join $rig(rigpath) $rig(rigname) $oldmode.mode]

#   update mode list inload menu
    mode_make_menu
}

#   Delete mode(s).  Make a list of all mode names, excluding current.
#   Ask user to select one.  Check the selection (because non-existant
#   mode may have been typed in selection box).

proc mode_delete { } {
    global mmode mode rig

    set oldmode $mmode(modename)

    # Prompt with list of other modes:
    set modes [mode_list_modes $oldmode ]

    # Ask user for selections.
    set delmode [ popup_selector \
            "Choose mode" "Select modes to delete:" $modes "" multiple true 180 ]

    # Delete each indicated mode, but only if it is on valid-selection list!
    # Also, explicitly check again for matches to the current mode (in case mode
    # name has changed under us)
    foreach m $delmode {
        if { [lsearch -exact $modes $m ] >= 0 && [string compare $m $mode(modename)] } {
            file delete -- [file join $rig(rigpath) $rig(rigname) $m.mode]
        }
    }
    # Rebuild menu of available modes
    mode_make_menu
}


#   procedure for selection of dual spectra ENABLED/DISABLED.
#   Note that this is written presuming it can only be invoked 
#   when the mode screen is in the "change" mode.

proc mode_activate_dual {} {
    global mmode amode rig
    #puts "mode_activate_dual"
    switch -- $mmode(dualspec) {
        ENABLED {
            set mmode(dual_enabled) 1
            set ic -1
            foreach counter $rig(counter_names) {
                incr ic
                if { $amode(c${ic}_sel) } {
                    mode_cp_samp2ref $ic 
                }
            }
        }
        default {
            set mmode(dual_enabled) 0
	}
    }
    mode_display_dual
    mode_display_counters
}

proc mode_display_dual {} {
    global mmode amode modewin duallabel rig modehelpstrings
    global text_Hist_r text_Tzero_r text_First_r text_Last_r
    if { $mmode(dual_enabled) } {
	set duallabel "Dual names / tags"
	if { $text_Hist_r ne "Hist" } {
            $modewin.m_stubsamp_f configure -bg $PREF::revbgcol
            $modewin.m_stubref_f configure -bg $PREF::revbgcol
            $modewin.m_gbstubr_l configure -bg $PREF::revbgcol -text "Good Bins"
            $modewin.m_bkstubr_l configure -bg $PREF::revbgcol -text "Background"
            $modewin.m_srsep_l configure -padx 12
            set text_Hist_r Hist
            set text_Tzero_r Tzero
            set text_First_r First
            set text_Last_r Last
            foreach sr [list samp ref] {
                # activate samp/ref title entries
                activateEntry $modewin.m_${sr}title_e -width 12 -fg $PREF::fgcol -bg $PREF::entrybg \
                    -highlightbackground $PREF::revbgcol -highlightcolor $PREF::brightcol
                activateEntry $modewin.m_${sr}tag_e   -width 4  -fg $PREF::fgcol -bg $PREF::entrybg \
                    -highlightbackground $PREF::revbgcol -highlightcolor $PREF::brightcol
                mode_bind_help m_${sr}title_e $modehelpstrings($sr)
                mode_bind_help m_stub${sr}_f $modehelpstrings($sr)
                mode_bind_help m_${sr}tag_e $modehelpstrings(${sr}tag)
            }
	}
    } else {
	set duallabel ""
	if { $text_Hist_r ne "" } {
            $modewin.m_stubsamp_f configure -bg $PREF::bgcol
            $modewin.m_stubref_f configure -bg $PREF::bgcol
            $modewin.m_gbstubr_l configure -bg $PREF::bgcol -text ""
            $modewin.m_bkstubr_l configure -bg $PREF::bgcol -text ""
            $modewin.m_srsep_l configure -padx 0
            set text_Hist_r {}
            set text_Tzero_r {}
            set text_First_r {}
            set text_Last_r {}
            foreach e [list m_samptitle_e m_samptag_e m_reftitle_e m_reftag_e] {
                # disable and hide samp/ref titles
                deactivateEntry $modewin.$e -width 1 -fg $PREF::bgcol -bg $PREF::bgcol \
                    -highlightcolor $PREF::bgcol -highlightbackground $PREF::bgcol 
                mode_bind_help $e ""
            }
            mode_bind_help m_stubsamp_f ""
            mode_bind_help m_stubref_f ""
        }
    }
}

proc mode_display_counters {} {
    global modewin run rig mmode amode modehelpstrings
#
    if { $mmode(changing) && ! $mmode(run_in_progress) } {
        set changeflag "normal"
    } else {
        set changeflag "disabled"
    }
#   set display of all counters; do sample set first
    set ih 0
    set ic -1
    foreach counter $rig(counter_names) {
        incr ic
        set sel $amode(c${ic}_sel)
        $modewin.m_c${ic}_name_c configure -state $changeflag
        if { $sel } {
#       show the range values for selected counters
            incr ih
            set amode(ih_c${ic}s) $ih
            mode_show_range_boxes $modewin $ic s
            mode_bind_help m_c${ic}_name_c \
                    "$modehelpstrings(selcounter)$modehelpstrings($changeflag)"
        } else {
#       hide the range values for unselected counters
            set amode(ih_c${ic}s) {}
            mode_hide_range_boxes $modewin $ic s
            mode_bind_help m_c${ic}_name_c \
                    "$modehelpstrings(deselcounter)$modehelpstrings($changeflag)"
        }
        if { $sel && $mmode(changing) } {
#       Set sunken entry boxes and visible buttons for selected counters when changing.
            $modewin.m_c${ic}_arrdn configure -state normal -image arrow_down -bg $PREF::revfgcol \
                        -relief raised -bd 1 -command "mode_cp_rest $ic"
            mode_bind_help m_c${ic}_arrdn $modehelpstrings(arrdn)
            $modewin.m_c${ic}_handle configure -state normal -image grab_handle -bg $PREF::revfgcol -relief raised -bd 1
            mode_bind_help m_c${ic}_handle $modehelpstrings(handle)
            group_drag_bind "counters" "$modewin.m_c${ic}_handle" \
                [list amode(tzero_c${ic}s) amode(fgbin_c${ic}s) amode(lgbin_c${ic}s) amode(fback_c${ic}s) amode(lback_c${ic}s) \
                      amode(tzero_c${ic}r) amode(fgbin_c${ic}r) amode(lgbin_c${ic}r) amode(fback_c${ic}r) amode(lback_c${ic}r) ] \
                [list mode_disp_drag $ic]
        } else {
#       Set flat entry boxes and hidden buttons when not changing, or for de-selected counters
            $modewin.m_c${ic}_arrdn configure -state disabled -image {} -bg $PREF::bgcol -relief flat -bd 0
            mode_bind_help m_c${ic}_arrdn {}
            $modewin.m_c${ic}_handle configure -state disabled -image {} -bg $PREF::bgcol -relief flat -bd 0
            mode_bind_help m_c${ic}_handle {}
            group_drag_unbind "counters" "$modewin.m_c${ic}_handle"
        }
    }
    set mmode(numsel) $ih
#   now do the reference histograms
    set ic -1
    foreach counter $rig(counter_names) {
        incr ic
        set sel $amode(c${ic}_sel)
        if { $sel && $mmode(dual_enabled) } {
            incr ih
            set amode(ih_c${ic}r) $ih
            mode_show_range_boxes $modewin $ic r
        } else {
            set amode(ih_c${ic}r) {}
            mode_hide_range_boxes $modewin $ic r
        }
        if { $sel && $mmode(dual_enabled) && $mmode(changing) } {
            $modewin.m_c${ic}_arrrt configure -state normal -image arrow_right \
                    -command "mode_cp_samp2ref $ic" -bg $PREF::revfgcol -relief raised -bd 1 
            mode_bind_help m_c${ic}_arrrt $modehelpstrings(arrrt)
        } else {
            $modewin.m_c${ic}_arrrt configure -state disabled -image {} \
                    -command {} -bg $PREF::bgcol -relief flat -bd 0
            mode_bind_help m_c${ic}_arrrt {}
        }
    }
    set mmode(numhist) $ih
    mode_set_bytesbin
}

proc mode_show_range_boxes { w ic sr } {
#   If we are changing mode, show bin ranges as sunken entry boxes;  otherwise simulate a label
    global mmode amode modehelpstrings
    if { $mmode(changing) } {
        foreach box [list tzero fgbin lgbin fback lback] {
            activateEntry $w.m_c${ic}${sr}_${box}_e -width 7 -textvariable amode(${box}_c${ic}${sr})
            bind $w.m_c${ic}${sr}_${box}_e <FocusIn> \
                  "set prevnumericv {}; mode_valid_bins ${box}_c${ic}${sr}; $w.m_c${ic}${sr}_${box}_e icursor end"
            bind $w.m_c${ic}${sr}_${box}_e <KeyRelease> "mode_valid_bins ${box}_c${ic}${sr}"
            bind $w.m_c${ic}${sr}_${box}_e <Control-u> "set amode(${box}_c${ic}${sr}) {}; set prevnumericv {}"
            bind $w.m_c${ic}${sr}_${box}_e <<Delimiter>> "mode_ranges_navigate $box $ic $sr \%W \%K ; break"
            bind $w.m_c${ic}${sr}_${box}_e <<CopyVertical>> "mode_cp_single $box $ic $sr \%K ; break"

            mode_bind_help m_c${ic}${sr}_${box}_e "$modehelpstrings($box) $modehelpstrings(keys)"
        }
    } else {
        foreach box [list tzero fgbin lgbin fback lback] {
            deactivateEntry $w.m_c${ic}${sr}_${box}_e -width 7 -textvariable amode(${box}_c${ic}${sr})
            mode_bind_help m_c${ic}${sr}_${box}_e "$modehelpstrings($box)"
        }
    }   
}

#   Hide a row of bin-range boxes (same whether changing or not)

proc mode_hide_range_boxes { w ic sr } {
    global mmode amode
    foreach box [list tzero fgbin lgbin fback lback] {
        set amode(${box}_c${ic}${sr}) ""
        deactivateEntry $w.m_c${ic}${sr}_${box}_e -width 0
        mode_bind_help m_c${ic}${sr}_${box}_e ""
    }
}

#   Set the bin ranges of counter $nc by copying from the last previous selected 
#   counter, if any, or the next selected if any.

proc mode_set_def_bins { nc } {
    global mmode
    set ic [mode_prev_sel $nc]
    if { $ic == $nc } {
        set ic [mode_next_sel $nc]
    }
    if { $ic != $nc } {
        mode_cp_c2c $ic $nc
    }
}

#   return the number of the selected counter previous to number $nc,
#   or $nc if none.

proc mode_prev_sel { nc } {
    global amode
    set ic $nc
    while { $ic > 0 } {
        incr ic -1
        if { $amode(c${ic}_sel) } {
            return $ic
        }
    }
    return $nc
}

#   return the number of the selected counter following number $nc,
#   or $nc if none.

proc mode_next_sel { nc } {
    global mmode amode
    set ic $nc
    while { $ic < $mmode(num_counters) - 1 } {
        incr ic
        if { $amode(c${ic}_sel) } {
            return $ic
        }
    }
    return $nc
}


#   Sanitize all the histogram length fields/variables, then apply
#   them as limits to the last-good-bins, and perhaps others, for every
#   histogram.  
#   The boolean parameter lench indicates a histogram length change;
#   when it is true, the last good bin is usually set to the (new) 
#   histogram length.
#   The factor $fact indicates a bin-size change, used for re-scaling
#   bin ranges of histograms from previous values.

proc mode_adjust_ranges { lench fact } {
    global mmode amode rig
#   Apply limits and checks to ranges:  hardware gate-extenders
#   give delays 0.01*<7_bit_code>, for range 0.02 to 1.27
    set u $mmode(default_gate_add)
    scan $mmode(TDCgate_add) {%f} u
    set mmode(TDCgate_add) [bounded $u 0.02 1.27]
    set u $mmode(default_gate_add)
    scan $mmode(PUgate_add) {%f} u
    set mmode(PUgate_add) [bounded $u 0.02 1.27]
    if { [scan $mmode(hlen_us) {%f} u] == 0} { mode_histlenb }
    if { [scan $mmode(hlen_us) {%f} u] == 0} { beep ; return }
    mode_histlenus s [bounded $u 0.0 20.0]
#   Apply the limits to histograms.
    set minlgb [expr { $lench? int(0.90 * $mmode(hlen)) : 5 }]
#
    foreach sr [list s r] {
        set ic -1
        foreach c $rig(counter_names) {
            incr ic
            if { $amode(c${ic}_sel) } {
                mode_adjeach lgbin_c${ic}${sr} $fact 2 $mmode(hlen)
                if { $amode(lgbin_c${ic}${sr}) < $minlgb } {
                    set amode(lgbin_c${ic}${sr}) $mmode(hlen)
                }
                set lb [expr {$amode(lgbin_c${ic}${sr}) - 1}]
                mode_adjeach tzero_c${ic}${sr} $fact 1 $lb
                mode_adjeach fgbin_c${ic}${sr} $fact 1 $lb
                mode_adjeach fback_c${ic}${sr} $fact 0 $lb
                mode_adjeach lback_c${ic}${sr} $fact $amode(fback_c${ic}${sr}) $lb
            }
        }
        if { !$mmode(dual_enabled) } { return }
    }
}

#   Adjust one histogram range parameter ($var) to be rescaled by $fact, and
#   be in the range $fb to $lb.
proc mode_adjeach { var fact fb lb } {
    global mmode amode
    set v $amode($var)
    if { "$v" == "" } { set v 0 }
    if { [catch {expr {round($fact * $v)}} v ] } { set v 0 }
    set amode($var) [bounded $v $fb $lb ]
}

#   Copy all histogram range parameters from counter $n to counter $m
proc mode_cp_c2c { n m } {
    global mmode amode
    foreach box [list tzero fgbin lgbin fback lback] {
        set amode(${box}_c${m}s) $amode(${box}_c${n}s)
        set amode(${box}_c${m}r) $amode(${box}_c${n}r)
    }
}

#   Copy all histogram range parameters from counter $n to all following
#   selected counters (used by down-arrow green button widget)
proc mode_cp_rest { n } {
    global mmode amode
    set ic $n
    set z [expr {$mmode(num_counters) - 1} ]
    while { $ic < $z } {
        incr ic
        if { $amode(c${ic}_sel) } {
            mode_cp_c2c $n $ic
        }
    }
}

#   Copy "sample" histogram range parameters to the "reference" equivalents
proc mode_cp_samp2ref { n } {
    global mmode amode
    foreach box [list tzero fgbin lgbin fback lback] {
        set amode(${box}_c${n}r) $amode(${box}_c${n}s)
    }
}

#   Validate histogram bin-range parameter $amode($var), ensuring an
#   integer number (or blank) that is less than the histogram length.
#   If it fails, restore previous value. 

proc mode_valid_bins { var } {
    global mmode amode prevnumericv
    if { "$amode($var)" != "" } {
        if { [scan $amode($var) {%d%c} i c] != 1 } { set i 9.e9 }
        if { $i > $mmode(hlen) } {
            set amode($var) $prevnumericv
            beep
        }
    }
    set prevnumericv $amode($var)
}

#   Special key functions used in the grid of bin-range entries.  These are
#   all bound to the pseudo-event <<Delimiter>>
#
#   Cursor Up/Down:    move focus in grid  (Left/Right move within entry box)
#   Shift-Down:        copy this entry to all entries below
#   Return:            move to beginning of next row
#   Quote (") = Ditto: copy entry from row above
#   space, comma:      move to next entry box (or next widget)
#
#   Note the systematic naming of mode variables:  amode(${box}_c${ic}${sr})
#   and the entry widgets: $modewin.m_c${ic}${sr}_${box}_e
#
proc mode_ranges_navigate { box ic sr W K } {
    global mmode amode modewin
    # Validate numeric value, and prevent octal forms with leading zeros:
    mode_valid_bins ${box}_c${ic}${sr}
    scan $amode(${box}_c${ic}${sr}) { %d} amode(${box}_c${ic}${sr})
    # Determine new focus window
    switch -- $K {
        Up {
            set next $modewin.m_c[mode_prev_sel $ic]${sr}_${box}_e
        }
        Down {
            set next $modewin.m_c[mode_next_sel $ic]${sr}_${box}_e
        }
        Return {
            set next $modewin.m_c[mode_next_sel $ic]s_tzero_e
        }
        quotedbl { ;# ditto
            set amode(${box}_c${ic}${sr}) $amode(${box}_c[mode_prev_sel $ic]${sr})
            set next $W
        }
        default {
            set next [tk_focusNext $W]
        }
    }
    focus $next
    catch { $next icursor end }
}

#   Copy an entry cell to all cells below (above).  Bound to <<CopyVertical>>

proc mode_cp_single { box n sr direct } {
    global mmode amode

#   Validate numeric value, and prevent octal forms with leading zeros:
    mode_valid_bins ${box}_c${n}${sr}
    scan $amode(${box}_c${n}${sr}) { %d} val

    set first 0
    set last $mmode(num_counters)
    switch -- $direct {
        Up { set last $n }
        Down { set first $n }
        bar { }
    }
    for { set ic $first } { $ic < $last } { incr ic } {
        if { $amode(c${ic}_sel) } {
            set amode(${box}_c${ic}${sr}) $val
        }
    }
}


#   The histogram length parameter is controlled by three equivalent widgets,
#   which must be kept in synchrony.  The working (varying) copies of the mode
#   variables are in array mmode(), and the entries are:
#      hlen     -  entry-box length in bins (primary internal variable)
#      hlen_us  -  entry-box hist length, in microseconds
#      slide_us -  slide-bar hist length, in microseconds
#   In addition, there are gate lengths which are the sum the histogram length
#   plus an added delay:
#      TDC_gate = hlen_us + TDCgate_add
#      PU_gate  = TDCgate + PUgate_add

#   Propagate hist-length setting $val (given in microseconds) to all five
#   widgets.  The source is indicated by $type: e for entry box, and s for
#   slider.  This is fairly complicated because we keep the number of bins
#   properly rounded and bounded, and we do not want the slider changing
#   the number of bins just to round the histogram length to a multiple
#   of 50 ns.
#
#   This system of histlenus and histlenb could probably be simplified,
#   especially now that I'm setting the resolution of the slidebar, but
#   they are invoked from many places, so we must be very careful making
#   any changes.

proc mode_histlenus { type val } {
    global mmode
    if { [catch { expr 0.0+$val } v] } { return }
    if { [scan $val {%g} v] != 1 } { return }
#   get maximum number of bins per histogram 
    if { [catch {
        set mb $mmode(maxbins)
        set mb [expr {$mb / $mmode(numhist)} ] 
      } ] } { return }

    set mb [next_pow2 [ expr {($mb / 2) + 1} ] ]
    if { $mb > ($mmode(maxbins)-128)/$mmode(numhist) } {# Overhead in event 
        incr mb -256
    }

#   calculate properly-rounded previous number of bins:
    if { [catch {expr {round( [ bounded $mmode(hlen) 256 $mb ] / 256 ) * 256} } pb] } {
        set pb 256
    }
    set mmode(hlen) $pb
#   calc number of bins implied by hist length:
    if { [catch { bounded [expr int($v*1000.0/(256*$mmode(binsize)))*256] 256 $mb } b] } {
        return
    }
#   When slider changes length by less than 50 ns, do not propagate!
    if { "$type" == "e" || abs( $b - $pb ) * $mmode(binsize) >= 50. } { 
        set mmode(hlen) $b
    }
    set us [expr {$mmode(hlen) * $mmode(binsize) / 1000.0}]
    set mmode(slide_us) $us
    if { "$type" == "s" } { set mmode(hlen_us) [format "%.4f" $us ] }

    mode_show_gates
}

#   Propagate hist-length-in-bins from (global) $mmode(hlen) to all three
#   widgets, and to last-good-bin for all hists. (scan prevents interpretation
#   of leading-zeros as octal!)

proc mode_histlenb {} {
    global mmode
    if { [scan $mmode(hlen) {%d} b] != 1 } { return }
    if { [catch { expr $mmode(binsize)*$b/1000.0 } u] } { return }
    set mmode(slide_us) $u
    set mmode(hlen_us) [format "%.4f" $u ]
    mode_show_gates
}

proc mode_show_gates {} {
    global mmode
    set tga $mmode(TDCgate_add)
    if { "$tga" == "" } { set tga 0.0 }
    set pga $mmode(PUgate_add)
    if { "$pga" == "" } { set pga 0.0 }
    catch {
        set mmode(Data_gate) [bounded $mmode(hlen_us) 1.0 20.0]
        set mmode(TDC_gate) [expr $mmode(Data_gate) + $tga ]
        set mmode(PU_gate) [expr $mmode(TDC_gate) + $pga ]
    }
    mode_set_bytesbin
}

global prevnumericv
set prevnumericv 0

#   Ensure that variable named by $var stays numeric.  This is to be invoked
#   after each character is added to the string value (so is bound to KeyRelease).
#   If non-numeric, the value is restored to the "previous" value $prevnumericv,
#   which is itself updated.  The actual test for valid numeric is if either the
#   raw value or the value with "0" can be used as a number; this allows pre-numeric
#   strings such as "", "-", and "1.0e".

proc keep_numeric { var } {
    upvar $var v
    global prevnumericv 
#    if { [ catch { expr 0.1 + $prevnumericv } ] } {
#        set prevnumericv 0
#    }
    if { [ catch { expr 0.1 + $v } ] } {
        if { [ catch { expr 0.1 + ${v}0 } ] } {
#            puts "$v is not a number, and neither is ${v}0, so use $prevnumericv."
            if { [ catch { expr 0.1 + $prevnumericv } ] } {
                if { [ catch { expr 0.1 + ${prevnumericv}0 } ] } {
                    # previous "numeric" is no good, erase it.
                    set prevnumericv {}
                }
            }
            set v $prevnumericv
            beep
        }
    }
    set prevnumericv $v
}

#   Primarily, Set the bin-size (TDC resolution).
#   sets resolution, binsize, and binsize_label based on a resolution 
#   code, then adjust the good-bin ranges to be valid.
#   This is activated when the gui changes the bin size, and under the
#   influence of mode_revert_change (initialization, refresh, revert,
#   load mode)
proc mode_set_binsize { code } {
    global mmode modewin
    set prevbs 0.0
    catch { set prevbs $mmode(binsize) }
    set mmode(resolution) $code
    set div [ expr round(pow(2,26.0-$code)) ]
    set mmode(binsize) [ expr 50.0/$div ]
    if { $code > 18 } {
        set mmode(binsize_label) " $mmode(binsize)  ns  "
    } else {
        set mmode(binsize_label) " [expr {1000.0 * $mmode(binsize)}]  ps  "
    }
    set res [bounded [expr 0.256 * $mmode(binsize)] 0.05 15.0]
    $modewin.m_hislen_s configure -resolution $res -to [expr {15.0 + 0.5*$res}]
    mode_histlenus s $mmode(hlen_us)
    catch {
        if { $prevbs > 0.0 } {
            mode_adjust_ranges 0 [expr $prevbs / $mmode(binsize) ] 
        } else {
            mode_adjust_ranges 0 1.0
        }
    }
}

# Proc to calculate the number of bytes per bin.  Sets a warning message too.
# Called by mode_do_apply_change, mode_show_gates, mode_display_counters
proc mode_set_bytesbin { } {
    global mmode
    set mmode(bytesbin) [ expr { $mmode(maxbins) < 2 * $mmode(numhist) * [next_pow2 $mmode(hlen)] ? 2 : 4} ]
    if { $mmode(bytesbin) == 2 } {
        set mmode(binsizewarn) "2 bytes per bin"
    } else {
        set mmode(binsizewarn) ""
    }
}

#  Display some bin numbers when dragging to copy bin range values
proc mode_disp_drag { ic args } {
    global amode
    return "  $amode(tzero_c${ic}s),  $amode(fgbin_c${ic}s),  $amode(lgbin_c${ic}s) ..."
}


#  THE HELP LINE:
#
#  Many widgets have a binding for mouse-entry and mouse-exit where some
#  help text is displayed while the cursor is on that widget.
#
#  Due to the other <FocusIn> bindings on the entry boxes, we don't
#  update the help display on focus.

proc mode_bind_help { w h } {
    global modewin
    bind $modewin.$w <Enter> "set mode_help_text \"$h\""
    bind $modewin.$w <Leave> "set mode_help_text {}"
}

global modehelpstrings
set modehelpstrings(handle) "Drag onto other handles to copy settings"
set modehelpstrings(arrdn)  "Click to copy bin setting to following counters"
set modehelpstrings(arrrt)  "Click to copy sample to reference"
set modehelpstrings(selcounter)   "Selected counter"
set modehelpstrings(deselcounter) "Deselected counter"
set modehelpstrings(normal)   "; Click to change selection"
set modehelpstrings(disabled) ""
set modehelpstrings(samp)     "Identifiers for `sample' histograms"
set modehelpstrings(samptag)  "Histogram-name suffix for `sample' histograms"
set modehelpstrings(ref)      "Identifiers for `reference' histograms"
set modehelpstrings(reftag)   "Histogram-name suffix for `reference' histograms"
set modehelpstrings(histlenus) "Histogram length, in microseconds"
set modehelpstrings(tzero) "Bin where time=0"
set modehelpstrings(fgbin) "First good data bin"
set modehelpstrings(lgbin) "Last good data bin"
set modehelpstrings(fback) "First background bin (or 0 for automatic)"
set modehelpstrings(lback) "Last background bin (or 0 for automatic)"
set modehelpstrings(keys)  "(special keys comma,space,tab,return,up,down,quote,bar,shift-up/down)"

proc mode_make_counter_row { name n } {
    global modewin
    checkbutton $modewin.m_c${n}_name_c \
            -command "mode_set_def_bins $n ; mode_display_counters" \
            -anchor w -takefocus 0 \
            -disabledforeground $PREF::fgcol -activebackground $PREF::entrybg -state disabled \
            -justify left -textvariable rig(c${n}_name) -variable amode(c${n}_sel)
    label $modewin.m_c${n}_TDCin \
            -textvariable rig(c${n}_TDC_in) -anchor e -width 2 -padx 4
    label $modewin.m_c${n}_ORout \
	    -textvariable rig(c${n}_OR_out) -anchor e -width 2 -padx 4
    foreach b [list arrdn handle arrrt] {
        button $modewin.m_c${n}_$b \
                -activebackground $PREF::revfgcol -bg $PREF::bgcol -borderwidth 1 \
                -highlightthickness 0 -padx 1 -pady 1 -takefocus 0 \
                -image {} -text {} -state disabled -relief flat -bd 0
    }
    foreach sr [list s r] {
        label $modewin.m_c${n}${sr}_Hnum \
                -anchor e -textvariable amode(ih_c${n}${sr}) -width 2
        foreach b [list tzero fgbin lgbin fback lback] {
            entry $modewin.m_c${n}${sr}_${b}_e \
                    -cursor {} -highlightcolor $PREF::brightcol -justify right \
                    -textvariable amode(${b}_c${n}${sr}) -width 7 -relief flat
            # not avaliable:    -validate all -invalidcommand beep -vcmd { validbinnum %P }
        }
    }
    set gridrow [expr $n + 4]
    grid $modewin.m_c${n}_name_c -in $modewin.bins_f -row $gridrow -column 1 -sticky ew
    grid $modewin.m_c${n}_TDCin -in $modewin.bins_f -row $gridrow -column 2 -sticky w
    grid $modewin.m_c${n}_ORout -in $modewin.bins_f -row $gridrow -column 3 -sticky w
    grid $modewin.m_c${n}_arrdn -in $modewin.bins_f -row $gridrow -column 4 -ipadx 1 -ipady 1 -pady 0 -sticky nesw
    grid $modewin.m_c${n}_handle -in $modewin.bins_f -row $gridrow -column 5 -ipadx 1 -ipady 1 -pady 0 -sticky nesw
    grid $modewin.m_c${n}s_Hnum -in $modewin.bins_f -row $gridrow -column 6 
    grid $modewin.m_c${n}s_tzero_e -in $modewin.bins_f -row $gridrow -column 7 -sticky w
    grid $modewin.m_c${n}s_fgbin_e -in $modewin.bins_f -row $gridrow -column 8 -sticky w
    grid $modewin.m_c${n}s_lgbin_e -in $modewin.bins_f -row $gridrow -column 9 -sticky w
    grid $modewin.m_c${n}s_fback_e -in $modewin.bins_f -row $gridrow -column 11 -sticky w
    grid $modewin.m_c${n}s_lback_e -in $modewin.bins_f -row $gridrow -column 12 -sticky w
    grid $modewin.m_c${n}_arrrt -in $modewin.bins_f -row $gridrow -column 13 -ipadx 1 -ipady 1 -padx 5 -pady 0 -sticky nsew
    grid $modewin.m_c${n}r_Hnum -in $modewin.bins_f -row $gridrow -column 14
    grid $modewin.m_c${n}r_tzero_e -in $modewin.bins_f -row $gridrow -column 15 -sticky w
    grid $modewin.m_c${n}r_fgbin_e -in $modewin.bins_f -row $gridrow -column 16 -sticky w
    grid $modewin.m_c${n}r_lgbin_e -in $modewin.bins_f -row $gridrow -column 17 -sticky w
    grid $modewin.m_c${n}r_fback_e -in $modewin.bins_f -row $gridrow -column 19 -sticky w
    grid $modewin.m_c${n}r_lback_e -in $modewin.bins_f -row $gridrow -column 20 -sticky w
}


proc mode_destroy_counter_row { name n } {
    global modewin

    destroy $modewin.m_c${n}_name_c
    destroy $modewin.m_c${n}_TDCin
    destroy $modewin.m_c${n}_ORout
    foreach b [list arrdn handle arrrt] {
        destroy $modewin.m_c${n}_$b 
    }
    foreach sr [list s r] {
        destroy $modewin.m_c${n}${sr}_Hnum
        foreach b [list tzero fgbin lgbin fback lback] {
            destroy $modewin.m_c${n}${sr}_${b}_e 
        }
    }
}


if { $modewin == "" } {
    # stand-alone testing, so start immediately!
    odb_get_general
    odb_get_rig

    for {set ic 0} {$ic < $cfg::numCounter} {incr ic} {
        if {$ic < $rig(num_counters)} {
            set rig(c${ic}_num)  [expr $ic + 1]
            set rig(c${ic}_name) [lindex $rig(counter_names) $ic]
            set rig(c${ic}_Orig) [lindex $rig(counter_names) $ic]
            set rig(c${ic}_TDC_in) [lindex $rig(TDC_in) $ic]
            set rig(c${ic}_OR_out) [lindex $rig(OR_out) $ic]
        } else {
            set rig(c${ic}_name) {}
            set rig(c${ic}_Orig) {}
            set rig(c${ic}_TDC_in) {}
            set rig(c${ic}_OR_out) {}
            set rig(c${ic}_num) {}
        }
    }


    mode_initialize
    
}

#  Default inits so something available before get odb:

set mmode(maxbins) 100000
set mmode(run_in_progress) 0
set mrig(changing) 0

