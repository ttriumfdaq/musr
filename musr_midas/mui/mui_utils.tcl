#  Various start-up operations for musr user interface.  This file is
#  loaded at the beginning of the main procedure, or by an individual
#  page when executed as stand-alone.  This facilitates testing.
#  The procedures defined here vary from being very general utilities
#  to some that are very specific to the muSR "mui".
#
#   $Log: mui_utils.tcl,v $
#   Revision 1.39  2016/09/06 22:10:53  asnd
#   Add more beamline control to autoruns, and fix some bugs
#
#   Revision 1.38  2015/09/28 00:45:33  asnd
#   No fancy sounds played (locally) for a remote connection
#
#   Revision 1.37  2015/04/17 23:59:42  asnd
#   Small changes to notification sounds, and make them the default (system bell not working)
#
#   Revision 1.36  2015/03/20 00:35:17  suz
#   changes by Donald for sounds. This version part of package for new VMIC frontends
#
#   Revision 1.35  2008/11/19 06:22:46  asnd
#   Strip spurious quoted from titles
#
#   Revision 1.34  2008/04/15 01:36:10  asnd
#   Various small adjustments
#
#   Revision 1.33  2007/11/21 23:45:05  asnd
#   Changes leading up to bnmr support (coexistence now)
#
#   Revision 1.32  2007/10/03 02:29:00  asnd
#   Camp emissary sub-server; configentry disables validation for readonly
#
#   Revision 1.31  2006/11/22 20:58:29  asnd
#   Update timed messagebox for Tk8.4
#
#   Revision 1.30  2006/09/22 07:11:24  asnd
#   More of the same.
#
#   Revision 1.29  2006/09/22 05:38:23  asnd
#   Handle Tk 8.4 (yet more corrections, groan)
#
#   Revision 1.28  2006/09/22 05:04:16  asnd
#   Handle Tk 8.4 (yet more, sigh, clumsy)
#
#   Revision 1.27  2006/09/22 03:45:19  asnd
#   Handle Tk 8.4 (more).
#
#   Revision 1.26  2006/09/22 03:13:58  asnd
#   Alterations for Tk 8.4
#
#   Revision 1.25  2006/07/08 01:27:22  asnd
#   Configurable mu character for window titles, to handle bug with versions in Sci Linux.
#
#   Revision 1.24  2006/05/26 16:53:33  asnd
#   Accumulated changes.  Fix Camp-server reconnects to avoid locking up. Various others.
#
#   Revision 1.23  2005/07/13 04:02:05  asnd
#   Extend radiomenu for dynamic lists; use it for automatic header selection.
#
#   Revision 1.22  2005/06/22 15:32:13  asnd
#   more prefs, messagebox timeout change, error bars
#
#   Revision 1.21  2004/08/05 02:22:14  asnd
#   new parent-win-name function
#
#   Revision 1.20  2004/05/18 01:16:05  asnd
#   Add hostname matching function
#
#   Revision 1.19  2004/05/18 00:56:01  asnd
#   Error message in timed_Messagebox
#
#   Revision 1.18  2004/05/09 07:14:35  asnd
#   Preserve literal Temperature, Field entry, if possible
#
#   Revision 1.17  2004/04/27 11:50:23  asnd
#   Audible alert when run ends asynchronously, or gets enough counts
#
#   Revision 1.16  2004/04/15 09:15:59  asnd
#   Adjust font of ImuSR run log, and make the column heads responsive to window resize
#
#   Revision 1.15  2004/04/10 07:00:07  asnd
#   Add auto-run control
#
#   Revision 1.14  2004/03/11 05:19:49  asnd
#   Initial work on autoruns + some cosmetics
#
#   Revision 1.13  2004/02/20 23:08:06  asnd
#   Single musr Midas experiment.  Handle year changes.
#
#   Revision 1.12  2003/11/24 05:43:26  asnd
#   Handle year changes.
#
#   Revision 1.11  2003/11/18 11:12:27  asnd
#   Implement zooming into parts of the Camp Stripchart plot.
#
#   Revision 1.10  2003/10/29 14:54:52  asnd
#   Proper hiding vs disabling of button.
#
#   Revision 1.9  2003/10/15 09:28:28  asnd
#   Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#   Revision 1.8  2003/09/19 06:23:27  asnd
#   ImuSR and many other revisions.
#
#   Revision 1.6  2003/03/11 03:16:12  asnd
#   Add to gui appearances preferences
#
#   Revision 1.5  2002/11/14 00:08:49  asnd
#   Column-copy of bin-range parameters ( |, shift-up/down)
#
#   Revision 1.4  2002/10/12 09:24:27  asnd
#   Improve stripchart plot
#
#   Revision 1.3  2002/09/21 04:45:42  asnd
#   Various recent updates
#
#   Revision 1.2  2002/07/19 11:47:38  asnd
#   Global ^U deletion in entry-boxes
#
#   Revision 1.1  2002/06/12 01:26:39  asnd
#   Starting versions of Mui files.
#

#  Source directory.

set muisrc [file dirname [info script]]
#puts "mui_utils in $muisrc"
#  Midas odb interaction, or fake it.

if { [info commands midas] == "" } {
    if { [info exists clogv(camp_only)] && $clogv(camp_only) } {
        # Do we want to do anything for stand-alone?
    } else { # testing DAQ, without midas
	if { [file exists odb_general.fake] } {
	    puts "Testing mui components using $::argv0 instead of mtcl"
	    source [file join $muisrc fake_odb.tcl]
	    set testING 1
	    set midas(check_clients) 0
	} else {
	    puts "Failure trying to run mui component without mtcl!"
	    puts "Running [info script] within $::argv0 under [info nameofexecutable]."
	    puts "ENV:"
	    parray env
	    exit 1
	}
    }
} else {
    if { ![info exists midas(musr)] || $midas(musr) } {
        source [file join $muisrc odb_musr.tcl]
    } else {
        source [file join $muisrc odb_bnmr.tcl]
    }
    set testING 0
    set midas(check_clients) 60
}

#   Midas experiment parameters
source [file join $muisrc mumid_cfg.tcl]

#   utility functions. 
#   bounded: delimit a value (x) between limits (lo, hi)
#   If use expr "..." then args can be expressions without [expr ]
proc bounded { x lo hi } {
    expr "(($x)<($lo)) ? ($lo) : ( (($x)>($hi)) ? ($hi) : ($x) )"
}

#   Pick the power of 2 equal or greater than the given value.  128 is the minimum possible.
#   (Corresponds to histogram allocation.)
proc next_pow2 { val } {
    set p2 128
    while { $p2 < $val } { set p2 [ expr { 2*$p2 } ] }
    return $p2
}


# Convert a delta-time spec to seconds.
# The time spec can be in many formats; all of the following are equal:
#    1:30,   5400 s,   90 min,   1.5h,   01:30:00,   90
# Units are any strings that begin with "s", "m", "h", or "d"
# ("m" for minutes, not months, milliseconds, or millennia).
# The default unit applied to a bare number may be specified by 
# an optional second argument, which defaults to "s" if omitted.
# 
proc deltaTime { spec {defaultu "s"} } {
    if { [string first ":" $spec] >= 0 } {
        return [expr { [clock scan $spec] - [clock scan 0:00:00] }]
    } else {
        set mult 0.0
        if { [scan "$spec $defaultu" { %f %1s} t u] == 2 } {
            switch -- $u {
                s { set mult 1. }
                m { set mult 60. }
                h { set mult 3600. }
                d { set mult 86400. }
            }
        }
        if { $mult > 0.0 } {
            return [expr { round($t*$mult) }]
        } else {
            return -code error "unable to convert delta-time string \"$spec\""
        }
    }
}

#   Dumb Little Routine to give plural "s" based on number
#   (nothing fancy, and no error checking!)

proc plural { num } {
    if { $num == 1 } {
        return ""
    } else {
        return "s"
    }
}

#   How to print a big number (integer).
#
#   Note: this is not broken into groups of three digits!  If that
#         is desired, though, this is the place to do it (see commify).

set tcl_precision 15

proc td_big_number { val } {
    if { [scan $val {%g} val] < 1 } { return 0 }
    if { abs($val) < 2.1e9 } { 
        return [expr { round( $val ) } ]
    } else {
        return [ string trim [format {%14g} $val] ]
    }
}

# commify --
#   puts commas into a decimal number
# Arguments:
#   num         number in acceptable decimal format
#   sep         separator char (defaults to English format ",")
# Returns:
#   number with commas in the appropriate place
# (Credit Jeff Hobbs's bag of utilities.)

# proc commify {num {sep ,}} {
#     while {[regsub {^([-+]?\d+)(\d\d\d)} $num "\\1$sep\\2" num]} {}
#     return $num
# }

#   And now we make long floating-point numbers shorter
#   (If number is given as integer, leave it that way.)
proc td_short_float { a } {
    global tcl_precision
    if { [string is integer $a] } { return $a }
    if { $a == 0.0 } { return 0.0 }
    if { abs($a) >= 100000.0 || abs($a) < 0.01 } {
        return [format %.5g $a]
    } else {
        return [format %.7g $a]
    }
}

#   Here we round floating point numbers to "n" digits.  The number of
#   digits can be a non-integer!  Think log-scale.  
#
proc sigfig { val sig } {
    set scale 1.0
    set logsig [expr {pow(10.0,$sig)}]
    set sv [expr {abs($val)}]
    while { abs($val)*$scale < $logsig } { set scale [expr {10.0*$scale}] }
    while { abs($val)*$scale > $logsig } { set scale [expr {0.1*$scale}] }
    return [expr { round($val*$scale)/$scale }]
}

#   Find the last element of an *ordered* *numeric* list that is below 
#   some number.  The list is passed as a variable name, to avoid extra
#   long lists.  It is optimized for the usual case of the "good" data
#   all near the end (thus, no foreach).

proc lsearch_last_below { lvar v } {
    upvar 1 $lvar l
    set e [expr { [llength $l] - 1 } ]
    set s -1
    for { set i $e } { $i > 0 } { incr i $s } {
        if { [lindex $l $i] < $v } { break }
    }
    return $i
}

#   Find the range of element indices for values within the range (a,b),
#   but expanded by 1, if possible.  The list is passed as a variable name,
#   to avoid extra copying of long lists.  It is optimized for the usual 
#   case of the "good" data all near the end.  Returns a list of two
#   numbers.

proc lsearch_cover_range { lvar a b } {
    upvar 1 $lvar l
    set ll [expr {[llength $l] - 1}]
    if { $a > $b } { return [list $ll $ll] }

    for { set j $ll } { $j > 0 } { incr j -1 } {
        if { [lindex $l $j] < $b } { break }
    }
    if { $j < $ll } { incr j }
    for { set i $j } { $i > 0 } { incr i -1 } {
        if { [lindex $l $i] < $a } { break }
    }
    return [list $i $j]
}

#  lassign, as found in TclX and also in Tcl 8.5+
#  lassign {list of values} var var var ...
#  (DA: I personally dislike this syntax, but will use it because THEY have chosen it
#  for the official Tcl command.)
if {[llength [info commands lassign]] == 0} {
    proc lassign {values args} {
        uplevel 1 [list foreach $args $values break]
        lrange $values [llength $args] end
    }
}

#   swap values of two variables:   swap_vars var_name other_name
#   (There is probably a cleaner way to do this!)
proc swap_vars { v1 v2 } {
    uplevel 1 "foreach \{\{$v1\} \{$v2\}\} \[list \$\{$v2\} \$\{$v1\}\] break"
}
#     uplevel 1 "eval set \{$v1\} \\\{\$\{$v2\}\\\} \\\; set \{$v2\} \\\{\$\{$v1\}\\\}"
        


#   Shorten a list, retaining the specified number of entries.  Do so by
#   thinning out the old entries by half, as far as possible.  If the 
#   list is more than twice as long as it should be, then the whole list 
#   is thinned, and the "oldest" (lowest index) entries are all discarded.
#   The final length need be only roughly $targetlen.
#
#   This is written in a clunky style, optimized for speed, and is not meant 
#   for short lists.
#
#   skip = number of totally discarded entries from beginning
#   n1 = number of entries that will be thinned (by half)
#   n2 = number of entries (from end) that will be retained.
#   i1 = index of point at end of the n1 block (start of next block)
#   i2 = index of point at end of the copy-16 block 

proc prune_list { thelist targetlen } {
    upvar $thelist oldlist

    set oldlen [llength $oldlist]

    if { $targetlen > $oldlen - 16 } { return }

    # choose to discard early entries if list is too long to handle.
    set skip [expr { (2 * $targetlen < $oldlen) ? ($oldlen - 2 * $targetlen) : 0 }]
 
    # Choose n1, n2, and skip;  ensure n1 is a multiple of 32 (for execution efficiency)
    set n1 [expr { 32 * int(($oldlen-$skip-$targetlen)/16) } ]
    set i1 [expr { $skip + $n1 - 1 } ]
    set n2 [expr { $oldlen - $n1 - $skip } ]
    set i2 [expr { $i1 + 16 * int($n2/16) } ]

#    puts "For old $oldlen   target $targetlen ..."
#    puts "skip $skip   n1 $n1   n2 $n2   actual final len [expr {$n1/2 + $n2}]"

    set newlist [list]
    # Copy thinned part of list, grabbing chunks of 32 at a time (keeping 16)
#    puts "Thinned range $skip $i1 for [llength [lrange $oldlist $skip $i1]] elements"
    foreach { A a B b C c D d E e F f G g H h I i J j K k L l M m N n O o P p } [lrange $oldlist $skip $i1] {
        lappend newlist $a $b $c $d $e $f $g $h $i $j $k $l $m $n $o $p 
    }
#    puts "New has partial length [llength $newlist]"

    # Copy un-thinned part of list in chunks of 16
#    puts "First part of un-thinned range [expr $i1+1] $i2 for [llength [lrange $oldlist [expr $i1+1] $i2]] elements"
    foreach { a b c d e f g h i j k l m n o p } [lrange $oldlist [incr i1] $i2] {
        lappend newlist $a $b $c $d $e $f $g $h $i $j $k $l $m $n $o $p
    }
#    puts "New has partial length [llength $newlist]"

    # Copy whatever un-thinned elements remain (fewer than 16)
#    puts "Final range [expr $i2+1] ...  for [llength [lrange $oldlist [expr $i2+1] end] ] elements"
    foreach { a } [lrange $oldlist [incr i2] end] {
        lappend newlist $a
    }
#    puts "New has final length [llength $newlist]"
#   The double [foreach] loops used to copy the un-thinned entries are much faster than
#   eval lappend x [lrange $oldlist [incr i1] end]
        
    set oldlist $newlist
}

# Strip quotes from string
proc unquoted { str } {
    if { [string match {"*"} $str] } {
        return [string range $str 1 end-1]
    }
    return $str
}

###################################
#
#   hostmatch  --  compare two strings and return boolean true if they 
#   seem to indicate the same internet host name

proc hostmatch { ha hb } {
    if { [string first "." $ha] < 1 } {
	regsub {[^.]+} [info hostname] $ha ha
    }
    if { [string first "." $hb] < 1 } {
	regsub {[^.]+} [info hostname] $hb hb
    }
    return [string equal -nocase $ha $hb]
}

###################################
#
#   mui_convert_temperature  and  mui_convert_field
#
#   These take a string representation of a temperature or field, and convert 
#   to normal units if possible (K or G).
#
#   Parameter: input string
#   Return:    For conversion: value K
#              Otherwise: the raw input value
#   Do accept some funny values like "zf" or "RT", converting to numbers.
#   Scan the number as a string so as to preserve significant digits
#   (but the exact representation is, of course, changed when units
#   conversion is performed).
#
#   The input strings might be value/error combinations, but no attempt
#   is made to perform units conversion for them.  
#
#   Scanning numbers with %[0-9.eE+-] is an attempt to preserve the exact
#   input string when no conversion is needed.
proc mui_convert_temperature { in } {
    set in [string trim $in]
    if { [string match "/?*" $in] } { return $in }
    if { [string match -nocase {rt} $in] } { return "295 K" }
    set u @
    set d1 ""
    set d2 ""
    set p ""
    set E -99999.
    scan $in { %[~<>=@ ]} p
    set v [string trim $in "~<>=@ "]
    set n [ scan $v { %[0-9.eE+-] %[(] %[0-9.eE+-] %[)] %s} T d1 E d2 u ]
    if { $n < 4 } {
        set n [ scan $v { %g%[ +/-]%g %s} T d1 E u ]
    }
    if { $n < 3 } { # no error bar
        set n [ scan $v { %[0-9.eE+-] %s} T u ]
    }
    if { $n < 1 } { # not a number
        return $in
    }
    switch -- $u {
        C { set T [ expr {273.15 + $T} ] }
        F {
            set T [ expr {273.15 + (($T-32.0)*5./9.)} ]
            set E [ expr {$E*5./9.} ]
        }
        mK -
        m {
            set T [ expr {$T / 1000.0} ]
            set E [ expr {$T / 1000.0} ]
        }
        @ {
            if { $E <= 0.0 } {
                return "$in K"
            }
        }
        K { 
            if { $E <= 0.0 } {
                return $in
            }
        }
        default { return $in }

    }
    # To get here, we must have done a conversion
    # Set these to force format style of any error bar
    set d1 (
    set d2 )
    if { $E < 0.0 } { return "$p$T K" }
    if { $E < 0.008 && $T < 1.0 } { return [format {%s%.4f%s%.4f%s K} $p $T $d1 $E $d2] }
    if { $E < 0.07 && $T < 10.0 } { return [format {%s%.3f%s%.3f%s K} $p $T $d1 $E $d2] }
    if { $E < 0.7 } { return [format {%s%.2f%s%.2f%s K} $p $T $d1 $E $d2] }
    if { $E < 7.0 } { return [format {%s%.1f%s%.1f%s K} $p $T $d1 $E $d2] }
    return [format {%s%.0f%s%.0f%s K} $p $T $d1 $E $d2]
}

proc mui_convert_field { in } {
    set in [string trim $in]
    if { [string match "/?*" $in] } { return $in }
    if { [string match -nocase {zf*} $in] || [string match -nocase {zero*} $in] } {
        return "0.0 G"
    }
    set u @
    set d1 ""
    set d2 ""
    set p ""
    set E -99999.
    scan $in { %[~<>=@ ]} p
    set v [string trim $in "~<>=@ "]
    set n [ scan $v { %[0-9.eE+-] %[[(] %[0-9.eE+-] %[])] %s} B d1 E d2 u ]
    if { $n < 4 } {
        set n [ scan $v { %g%[ +/-]%g %s} B d1 E u ]
    }
    if { $n < 3 } { # no error bar
        set n [ scan $v { %[0-9.eE+-] %s} B u ]
    }
    if { $n < 1 } { # not a number
        return $v
    }
    set mul 1.0
    switch -- $u {
        T { set mul 10000.0 }
        mT { set mul 10.0 }
        k -
        kG -
        kOe { set mul 1000.0 }
        mG -
        m { set mul 0.001 }
        @ {
            if { $E <= 0.0 } {
                return "$in G"
            }
        }
        Oe { }
        G {
            if { $E <= 0.0 } {
                return $in
            }
        }
        default { return $in }
    }
    # To get here, we must have done a conversion
    # Set these to force any error bar into format xxx(yyy)
    set d1 (
    set d2 )
    if { $mul == 1.0 } {
        if { $E < 0.0 } { return "$p$B G" }
        return "$p$B$d1$E$d2 G"
    }
    set B [expr {$B*$mul}]
    set E [expr {$E*$mul}]
    if { $E < 0.0 } { return "$p$B G" }
    if { $E < 0.008 } { return [format {%s%.4f%s%.4f%s G} $p $B $d1 $E $d2] }
    if { $E < 0.07 } { return [format {%s%.3f%s%.3f%s G} $p $B $d1 $E $d2] }
    if { $E < 0.7 } { return [format {%s%.2f%s%.2f%s G} $p $B $d1 $E $d2] }
    if { $E < 7.0 } { return [format {%s%.1f%s%.1f%s G} $p $B $d1 $E $d2] }
    return [format {%s%.0f%s%.0f%s G} $p $B $d1 $E $d2]
}

#   Return list of available modes, excluding the one specified.

proc mode_list_modes { except } {
    global rig
    set modes [list]
    foreach f [glob -nocomplain [file join $rig(rigpath) $rig(rigname) *.mode]] {
        set m [file rootname [file tail $f] ]
        if { [string compare $m $except] } {
            lappend modes $m
        }
    }
    return [lsort $modes]
}

#   Epics command wrapper function. cmd can be caget or caput, executed locally or
#   over ssh, or a script or other command (like ls) executed remotely.

# Initialization for epics communication, defaults followed by specifics.
array set epics [list \
   host sbp1       user $env(USER) local 0  retries 0  maxtries 2 \
   cmd_status {}   cmd_errors {}   tuning_status {}   busy 0 queue {} lock 0 \
]

# Make settings that differ from these defaults
catch {
    switch -glob -- $env(USER) {
        bn*r { # Changes to epics permissions means we have no local access for bnmr
            set epics(host) "lx$env(USER)"
            set epics(local) 0
        }
        m20* {
	    set epics(user) "m20"
	}
    }
}

proc mui_epics_cmd { cmd } {
    global midas epics
    #puts "mui_epics_cmd {$cmd}"
    set local 0
    if { [string match {ca[pg][ue]t *} $cmd] } {
        if { $epics(local) } {
            set timeout 2000
            set local 1
        } else {
            set timeout 6000
        }
    } elseif { [string match {*restore_tune*} $cmd] } {
        # 2 minutes alotted time because there might be argon and slits settings
        set timeout 120000
    } else {
        set timeout 10000
    }
    set aft [after $timeout "puts {After $timeout ms halted epics command $cmd};set epics(cmd_status) {timeout 0 666 timeout}" ]
    set epics(cmd_output) "Failure in Epics command"
    set epics(cmd_errors) ""
    set epics(lock) 1
    puts "Epics command: $cmd"
    if { $local } {
        catch { eval blt::bgexec epics(cmd_status) -output epics(cmd_output) -error epics(cmd_errors) $cmd }
    } else {
        catch { blt::bgexec epics(cmd_status) -output epics(cmd_output) -error epics(cmd_errors) \
	    ssh -x -o PasswordAuthentication=no $epics(user)@$epics(host) $cmd }
    }
    after cancel $aft
    #puts "status: $epics(cmd_status)"
    #puts "output: $epics(cmd_output)"
    #puts "errors: $epics(cmd_errors)"
    set epics(lock) 0
    if { [lindex $epics(cmd_status) 2] > 0 } {
        if { [string trim $epics(cmd_errors)] eq "" } {
            set epics(cmd_errors) $epics(cmd_output)
        }
        if { [string trim $epics(cmd_errors)] eq "" } {
            set epics(cmd_errors) "Failed Epics command '$cmd' status $epics(cmd_status)"
        }
        return -code error $epics(cmd_errors)
    }
    return $epics(cmd_output)
}


#   Camp command wrapper function.  This version relays the commands through
#   an emissary process, a camp subserver, which is defined in camp_emissary.tcl
#   and camp_subserver.tcl.
#
#   Before doing command, checks if the requested camp server has changed, and reconnects.

if { [llength [info proc restart_camp_subserver]] == 0 } {
    source [file join $muisrc camp_emissary.tcl]
}

set midas(camp_host) {}

proc mui_camp_cmd { cmd } {
    global midas subserv

    # Check if camp host name changed; then reconnect subserver (which might only start or
    # stop subserver, depending on circunstances).
    if { ![info exists subserv(host)] || ! [string equal $subserv(host) $midas(camp_host)] } {
        # Camp host changed, so reconnect.
        restart_camp_subserver
    }

    # If Camp host is real, then do camp command
    if { [string length $midas(camp_host)] && ! [string equal "none" $midas(camp_host)] } {
	# Now execute the camp command
	if { [catch {subserver_camp_cmd $cmd} return] } {
            puts "Camp command '$cmd' gave error '$return'"
	    return -code error $return
	} else {
	    return $return
	}
    } else {
        return -code error "There is no Camp server"
    }
}

set mui_utils_loaded 1


####################################################################################
#
#  All Tk definitions follow.  Skip the rest of the file if Tk is not loaded.
#
####################################################################################


if { [llength [info commands toplevel] ] == 0 } {
    return
}



#   Another Camp command wrapper function, wraps around preceding mui_camp_cmd.
#   Catches errors from mui_camp_cmd and prompts for retry with a messagebox.
proc retried_camp_cmd { cmd args } {
    if { [llength $args] } {
        set purpose $args
    } else {
        set purpose "execute Camp command:\n$cmd"
    }
    while { 1 } {
        if { [catch { mui_camp_cmd $cmd } ans ] } {
            beep
            switch  [timed_messageBox -timeout 30000 -type retrycancel -default cancel \
                    -message "Failed to $purpose\nResponse was:\n$ans" \
                    -icon error -title "Failed Camp Command"] {
                cancel {
                    return -code error $ans
                }
            }
        } else {
            return $ans
        }
    }
}

#   My drag-and-drop functions and a popup selection window.

source [file join $muisrc da_dragndrop.tcl]
source [file join $muisrc similar_color.tcl]
source [file join $muisrc popsel.tcl]
source [file join $muisrc print_dial.tcl]

proc bells { {number 1 } } {
    if { !$::PREF::enableBeep } { return }
    if { $::PREF::enableBeep == 2 && [info exists ::PREF::bellsounds] && [llength $::PREF::bellsounds] > 1 } {
	set number [bounded $number 1 [llength $::PREF::bellsounds]]
	set file [lindex $::PREF::bellsounds [expr {$number-1}]]
	if { [file exists [file join $::muisrc $file]] } {
	    set file [file join $::muisrc $file]
	}
	if { [file exists $file] } {
	    switch -- [file extension $file] {
		.ogg { 
		    catch { exec ogg123 $file >& /dev/null & }
                    return
		}
		.wav {
		    catch { exec aplay $file >& /dev/null & }
                    return
		}
		default {
		    #puts "Don't know how to play $file"
		}
	    }
        }
	dumb_beeps $number
    }
}

proc dumb_beeps { {number 1} } {
    bell
    if { $number > 1 } {
	after 400 dumb_beeps [incr number -1]
    }
}

proc beep { } {
    bells 1
}

#  Force all toplevels to have my application class. (This facilitates window icons.)
if { [llength [info commands musr_toplevel] ] == 0 } {
    rename toplevel musr_toplevel
    proc toplevel { args } {
        if { [lsearch -exact $args "-class"] < 0 } {
            lappend args -class [winfo class .]
        }
        uplevel 1 [linsert $args 0 musr_toplevel]
    }
} 


#   Validation procedure for numeric entry boxes that have a -textvariable.
#   (See man pages for problems mixing validation with -textvariable!!)
#   Use:
#   Parameters:  type min max act old new win typeval howval var helpvar
#      type:    "f" for float/double; "d" for decimal integer; "i" for generic int
#      min,max: allowed range of valid values
#      act:     action that caused change (%d)
#      old:     previous value (%s)
#      new:     proposed new value (%P)
#      win:     widget name (%W)
#      typeval: type of validation (%v)
#      howval:  which validation type triggered this change (%V)
#      var:     the (global) -textvariable 
#      helpvar: the (global) variable displaying help information ({} for none)
#   Entry widgets should be defined or configured like, e.g.,
#     -validate all \
#     -validatecommand {validateNumber f -88 1.0e8 %d %s %P %W %v %V thevariable thehelp}
#
#   This validation must usually accept *partial* numbers, so the proposed number
#   will pass if it scans as a number of the right type, or if appending "0" gives
#   a valid number.  The exception is when the validation is triggered by "focusout",
#   in which case the proposed number must be complete and valid.
#
#   When an invalid number is seen, the old value is restored (by the Tk
#   validation system), or if the old value was invalid, the entry is
#   cleared.  An alert is sounded by calling invalid_entry_alert, which flashes a
#   bright color in the entry widget and sounds the bell.  For "focusout" events, 
#   it holds the bright color longer because the user is probably not looking at
#   the entry.  You can redefine invalid_entry_alert to behave differently.
#
#   The test for range validity (between min and max) is only performed when all of
#   the new value, min, and max are well-formed numbers.  The range test does not *fail*
#   on invalid numbers, it is skipped (effectively passing that test). The range test
#   is also passed when a (partial) new value is out of range, but is closer than the
#   old value. (For "focusout" validation, the number must always be in range.)
#
#   As you may have noticed, "focusout" selects a differnt, stricter, validation.
#   If you want to perform strict validation at other times (say, when an "apply"
#   button is pressed) you can directly invoke validateNumber; e.g, to check entry
#   .win.dow with textvariable var:
#   validateNumber d 0 100 -1 $var $var .win.dow all focusout var thehelp
#
#   The test uses [scan] rather than [string is] because that is more convenient, 
#   especially for numbers with leading zeros. Specifically, decimal integers ("d")
#   with leading zeros are not treated as octal, but are silently converted to regular
#   decimal.  To allow variable-radix entry (decimal, octal, hex) use the "i" type.
#
#   Note that the value of the textvariable will normally be the old entry value
#   while validating, except when setting the textvariable changed the entry.
#   If the previous value was invalid, or if the new value was forced bad by a direct
#   setting of the textvariable, then Tcl/Tk will disable validation.  We recover
#   by forcing a blank value and re-enabling validation.
#
#    validateNumber   f,d  1   2  %d  %s  %P  %W  %v      %V     var help
proc validateNumber {type min max act old new win typeval howval var helpvar} {

    #puts "Validate $type var $var because $howval changing \"$old\" to \"$new\" in $win."

    set return 0

    if { [string equal $howval "focusout"] } {
        # Do strict final validation
        set time 2500
        if { [scan $new "%${type}%c" x c] == 1 } { # well-formed number; test within range
            #puts "Test val $x in range $min $max"
            set return [validate_in_range $x $min $max]
        }
    } else {
        # Do relaxed partial validation
        set time 70
        if { $act == 0 } {
            #puts "always allow deletion!"
            return 1
        }
        # scan for well-formed number:
        if { [scan $new "%${type}%c" x c] == 1 } {
            #puts "Test val $x in range $min $max"
            # Check if the old value was valid:
            set o 0
            scan $old "%${type} %c" o c
            set return [validate_in_range $x $min $max $o]

            # Watch out for octal integers, and silently correct them
            #puts "Test $return == 1 && [string equal {d} $type] && ![string is integer $new]"
            if { $return == 1 && [string equal {d} $type] && $new != $x } {
                #puts "Repair octal number"
                after idle [list after 0 [list set $var $x]]
                after idle [list after 0 [list $win configure -validate $typeval]]
            }
        } elseif { [scan "${new}0" "%${type}%c" x c] == 1 } {
            #puts "good partial value"
            set return 1
        } else {
            #puts "ill-formed new value; reject"
            set return 0
            if { [scan "${old}0" "%${type}%c" y c] != 1 && [scan $old "%${type}%c" o c] != 1 } {
                #puts "ill-formed old value; clear"
                set howval "forced"
            }
        }
    }
    # Now we have determined whether to accept or reject.  Here we take action.
    if { $return == 0 } {
        #puts "Rejected $type $new"
        invalid_entry_alert $win $time
        if { [string length $helpvar] } {
            switch -- $type {
                d { set type {a decimal integer}}
                i { set type {an integer}}
                f { set type {a real number}}
                default { set type {a number}} 
            }
            uplevel \#0 [list set $helpvar "Enter $type between $min and $max."]
        }
        if { [string equal $howval "forced"] } { # perform recovery
            after idle [list after 0 [list set $var {}]]
            after idle [list after 0 [list $win configure -validate $typeval]]
        }
    }
    # Clear the help var for focus-out (???)
    if { [string length $helpvar] && [string equal $howval "focusout"] } {
        uplevel \#0 [list set $helpvar {}]
    }
    return $return
}

proc validate_in_range { x min max {old {}} } {
    # if change from $old->$x takes you further out of range, then reject.
    # $x is the value to be checked (must be valid)
    # $min, $max give the allowed range: non-numbers cause the range test to be skipped 
    # $old is the (optional) old value;
    set useold [string length $old]

    set mn ""
    if { [scan $min {%f} mn] == 1 } {
        if { $x < $mn && ( !$useold || $mn - $x > $mn - $old ) } {
            return 0
        }
    }
    set mx ""
    if { [scan $max {%f} mx] == 1 } {
        if { $x > $mx && ( !$useold || $x - $mx > $old - $mx ) } {
            return 0
        }
    }
    return 1
}


#  Alert the user of a bad entry by ringing the bell and flashing a bright background
#  color.  The optional "time" argument tells how many milliseconds to flash.

proc invalid_entry_alert { win {time 70}} {
    catch {
        set bg [$win cget -bg]
        if { ![string equal $bg $::PREF::brightcol] } {
            beep
            if { [string length $bg] } {
                $win configure -bg $::PREF::brightcol
                after $time [list $win configure -bg $bg]
            }
        }
    }
}

#  A popup menu of values, one of which is selected (like radiobuttons).
#
#  The option -values { ... } gives the list of menu values (used as both the
#  value and the label).  Alternaltvely, the option -listvar var gives a 
#  (global) variable name which will specify the list when popped up.
#  The option -direction (above|below|align) says where to pop up the menu;
#  the default, align, tries to place the currently-selected entry so it
#  is centered on the menu button.
#  The options -indicatoron, -command, and -variable are applied to the 
#  menu items.  Other options are applied to the menu button.
# 
#  To be a proper megawidget, it should have resource-database lookup for
#  the defaults, but instead (for now) the defaults are hard-coded.  It
#  should also create a special [.win] command for configuration, but
#  all you get is the regular button widget command.
#
#  For example:
#  radiomenu .rmdwarf -textvariable dwarf -variable dwarf \
#    -values {Doc Happy Dopey Sleepy Bashful Sneezy Grumpy Gimli}
#

proc radiomenu { win args } {
    # Grab args for the radio list; other options will be applied to 
    # the button.  No testing is done for valid option list.
    # puts "Radiomenu $win $args"
    set variable radioMenuVariable
    set values {}
    set listvar {}
    set command {}
    set direction flush
    set indicatoron 1
    foreach v {direction variable values listvar command indicatoron} {
	set i [lsearch -exact $args -$v]
	if {$i >= 0} {
	    set $v [lindex $args [expr {$i+1}]]
	    set args [lreplace $args $i [incr i]]
	}
    }
    if { [lsearch -exact {above below flush align} $direction] < 0 } {
        return -code error "Invalid -direction \"$direction\". Must be one of above below flush (align)"
    }
    # Make the button
    if { [winfo exists $win] && [string equal "Button" [winfo class $win]] } {
        # puts "OK, button $win already exists -- use it."
    } else { # create button or generate proper error
        button $win
    }
    eval [list $win configure -command \
              [list radiomenu_popup $win $direction $variable $listvar $indicatoron $command] \
             ] $args

    # Make the menu
    if { [winfo exists $win.menu] && [string equal "Menu" [winfo class $win.menu]] } {
        # puts "OK, menu $win.menu already exists -- use it."
    } else { # create menu or generate proper error
        menu $win.menu
    }
    $win.menu configure -tearoff 0
    radiomenu_build_menu $win $variable $values $indicatoron $command

    # Mouse button Bindings so the regular button will act like a menubutton.
    # I am not using menubuttons because I'm using a popup menu, with active entry
    # aligned with the button. 
    bind $win <Button-1> "%W invoke ; break"
    bind $win <ButtonRelease-1> "break"
    # Binding so after-selection processing doesn't wait forever if user dismisses
    # the menu without selecting.  Otherwise, if a selection is finally made after
    # several non-selections, the GUI would perform several "updates" consecutively.
    bind $win.menu <Unmap> "catch { set $variable \${$variable} }"
}

proc radiomenu_build_menu { win variable values indicatoron command } {
    $win.menu delete 0 last
    foreach v $values {
	$win.menu add radiobutton -variable $variable -value $v -label $v \
                -indicatoron $indicatoron -command $command
    }
}

proc radiomenu_popup { win direction variable listvar indicatoron command} {
    upvar \#0 $variable locvar

    if { [string length $listvar] } {
        # The list variable is given, so (re)build menu now
        upvar \#0 $listvar values
        radiomenu_build_menu $win $variable $values $indicatoron $command
    } else {
        # else static menu, so collect the menu items. (I could look-up the value
        # directly, but only if no values are "none" or "active" or "last".)
        set lasti [$win.menu index last]
        set values [list]
        for {set i 0} {$i <= $lasti} {incr i} {
            lappend values [$win.menu entrycget $i -value]
        }
    }

    if { [llength $values] == 0 } {
        return
    }

    # Prevent immediate selection due to ButtonRelease event from "click"
    bind $win.menu <ButtonRelease> {break}
    after $PREF::clickms [list bind $win.menu <ButtonRelease> {}]

    # Find index that is already selected
    set acti 0
    if { [info exists locvar] } {
        set acti [lsearch -exact $values $locvar]
    }
    if { $acti < 0 } { set acti 0 }
    $win.menu activate $acti

    # Pop up menu, above, below, or aligned with button
    switch -- $direction {
        above { # Put the menu above the menu button
            set xpos [winfo rootx $win]
            set ypos [expr { [winfo rooty $win] - [winfo reqheight $win.menu] }]
            tk_popup $win.menu $xpos $ypos
        }
        below { # Put the menu below the menu button
            set xpos [winfo rootx $win]
            set ypos [expr { [winfo rooty $win] + [winfo height $win] }]
            tk_popup $win.menu $xpos $ypos
        }
        default { # center the selected item on the middle of the button
            set xpos [expr { [winfo rootx $win] + [winfo width $win]/2  }]
            set ypos [expr { [winfo rooty $win] + [winfo height $win]/2 }]
            tk_popup $win.menu $xpos $ypos $acti
        }
    }
    return
}


#   A variation of tk_messageBox that takes a -timeout milliseconds option.
#   If there is no response by the timeout, it returns the default value,
#   if any was specified, or "timeout" in the absence of a default.
#   This fails sometimes because of "recursive event loops" -- A messagebox
#   will not return until all other message boxes invoked at the same time
#   have returned; and the return values get mixed up.  That's a bug in
#   Tcl and messagebox.  There is a global flag "tmb_active", which can be
#   used to avoid making a messagebox for minor messages when another box
#   is active.  timed_messageBox will itself wait for up to 10s for a previous
#   box to terminate before clobbering it.

set tmb_active 0
proc timed_messageBox { args } {
    global tmb_to tmb_active
    #puts "Timed message box: $args"
    if { [llength $args]%2 } {
        error "Odd number of arguments ([llength $args]) for timed_messageBox: $args"
    }
   array set opts $args
   if { $tmb_active } { set tmb_active 10 }
   while { $tmb_active } {
      blt::bgexec xx sleep 1s
      incr tmb_active -1
   }
   set tmb_active 1
   if { [info exists opts(-timeout)] } {
      set p ""
      if { [info exists opts(-parent)] } {
         set p $opts(-parent)
      }
      if { [string compare $p "."] == 0 } {
         set p {}
      }
      # Specific for tk8.3, 8.4:
      set win "${p}.__tk__messagebox"

      set tmb_to 0
      if { [info exists opts(-default)] } {
          set d $opts(-default)
      } else {
          set d "timeout"
      }
      set aid [after $opts(-timeout) [list tmb_timeout $win $d ] ]
      #puts "Set timeout in $opts(-timeout) for $win $d (id $aid)"
      unset opts(-timeout)
      set tmb_ret [eval tk_messageBox [array get opts]]
      #puts "tk_messageBox returns:  $tmb_ret"
      after cancel $aid
      if { $tmb_to } { 
          set tmb_ret timeout
      }
      set tmb_to 0
   } else {
      set tmb_ret [eval tk_messageBox $args]
   }
   set tmb_active 0
   return $tmb_ret
}

proc tmb_timeout { win default } {
    global tmb_to
    #beep
    #puts "Timeout $win $default"
    if { [string equal $default "timeout"] } {
        set tmb_to 1
    }
    if { [winfo exists $win] } {
	if { [array exists ::tk::Priv ] } {
	    set ::tk::Priv(button) $default
	} else {
	    set ::tkPriv(button) $default
	}
    }
}

proc centerWinonWin { wc wp } {
    wm withdraw $wc
    update idletasks
    catch {
        scan [winfo geometry $wp] {%dx%d+%d+%d} pw ph px py
        set x [expr {$px + $pw/2 - [winfo reqwidth $wc]/2 }]
        set y [expr {$py + $ph/2 - [winfo reqheight $wc]/2 }]
        wm geometry $wc +$x+$y
    }
    wm deiconify $wc
}

# Progress bar modified from DK Fellows version.

option add *Progress.borderWidth  1      widgetDefault
option add *Progress.relief       sunken widgetDefault

namespace eval muiprogress {
    namespace export Progress SetProgress

    proc Progress {w args} {
	uplevel 1 [list frame $w -class Progress] $args

        set varname [namespace current]::progressLabel($w)

        set undoneForeground $::PREF::fgcol
        set undoneBackground $::PREF::lightbg
        set doneForeground   $::PREF::revfgcol
        set doneBackground   $::PREF::revbgcol

	frame $w.l -borderwidth 0 -background $undoneBackground
	label $w.l.l -textvariable $varname -borderwidth 0 \
		-foreground $undoneForeground -background $undoneBackground
	$w.l configure -height [expr {int([winfo reqheight $w.l.l]+2)}]
	frame $w.l.fill -background $doneBackground
	label $w.l.fill.l -textvariable $varname -borderwidth 0 \
		-foreground $doneForeground -background $doneBackground

	bind $w.l <Configure> [namespace code [list ProgressConf $w "%w"]]

	pack $w.l -fill both -expand 1
	place $w.l.l -relx 0.5 -rely 0.5 -anchor center
	place $w.l.fill -x 0 -y 0 -relheight 1 -relwidth 0
	place $w.l.fill.l -x 0 -rely 0.5 -anchor center

	SetProgress $w 0
	return $w
    }

    proc ProgressConf {w width} {
	place conf $w.l.fill.l -x [expr {int($width/2)}]
    }

    proc SetProgress {win value {range 100} {text {}}} {
	set relwidth [expr {double($value)/double($range)}]

	variable progressLabel
        if { [string length $text] == 0} {
            set text "[expr {int(100.0*$relwidth)}]%"
        }
	place conf $win.l.fill -relwidth $relwidth
	set progressLabel($win) $text
    }
}

namespace import muiprogress::Progress muiprogress::SetProgress


#   ODB operation wrapper function. Returns 1 on success.
#   On failure pops up a message box (30 sec timeout) with default "retry".
#   After the first try, the default is "cancel"
#   

proc td_odb { odb_func text } {
    set try 1
    set dflt retry
    while { $try } {
        if { [set try [catch { eval $odb_func} errmsg]] } {
            beep
            set response [ timed_messageBox -timeout 30000 \
                    -type retrycancel -default $dflt -icon warning \
                    -message "Failed to $text: $errmsg" -title "Failure" ]
            set try [expr {"$response" == "retry"} ]
            if { ! $try } { return 0 }
            set dflt cancel
        }
    }
    return 1
}

#   Handle main window names and dependent window names; needed for
#   specTcl testing of rig mode etc pages.

proc td_win_name { w } {
    if { $w == "" } { return . } else { return $w }
}

#   Like previous, but safer to use for the "-parent" qualifier on
#   transient dialogs.

proc p_win_name { w } {
    if { [string length $w] && [winfo exists $w] } {
        return $w
    } else {
        return .
    }
}

#   Hide or display buttons. Extra arguments are extra options to also 
#   configure.  Note that buttons should have their -disabledforeground 
#   color set to give the kind of hiding desired:  Use the fgcolor for
#   making buttons look like labels, or use the bgcolor to make them 
#   disappear completely.  If the button is sometimes disabled in the 
#   regular fashion then you should specify the proper -disabledbackground 
#   in $args.

proc showButton { b args } {
    eval [list $b configure -state normal -relief raised -takefocus 1\
            -highlightcolor $::PREF::brightcol -bg $::PREF::buttonbg] $args
}
proc hideButton { b args } {
    catch { # temporarily, defeat (rather than use) Tcl 8.4.7 feature
        $b configure -disabledbackground {} 
        $b configure -disabledforeground {}
    }
    eval [list $b configure -state disabled -relief flat -takefocus 0\
            -highlightcolor $::PREF::bgcol -bg $::PREF::bgcol] $args
}

#   Activate or deactivate text entries.  Inactive entries appear
#   like ordinary labels.   Extra arguments are extra options to
#   also configure.  configEntry will activate or deactivate an
#   entry based on the "state" parameter (disabled/normal), but
#   it gives a shaded foreground for disabled entries.
#
#   Tk versions 8.4+, entries have states normal, disabled, and 
#   readonly, where the previous disabled behaviour is closest to
#   the newer readonly behaviour.  I might have wanted to use the 
#   new options -disabledbackground and -disabledforeground, but the 
#   new "disabled" behavior prevents text selection out of the entry!
#   I need "readonly". 

proc activateEntry { e args } {
    if { [string length [$e cget -validatecommand]] } {
        set args "-validate all $args"
    }
    eval [list $e configure -state normal -takefocus 1 -cursor xterm -relief sunken \
              -bg "$PREF::entrybg" -highlightcolor "$PREF::brightcol"] $args
}

# Decide whether to use "disabled" or "readonly"
if { [package vcompare [info patchlevel] 8.4] < 0 } {
    # no "readonly" state
    proc deactivateEntry { e args } {
        eval [list $e configure -state disabled -takefocus 0 -cursor {} -relief flat \
                  -bg "$PREF::bgcol" -highlightcolor "$PREF::bgcol" -validate none] $args
    }
} else {
    proc deactivateEntry { e args } {
        eval [list $e configure -state readonly -takefocus 0 -cursor {} -relief flat \
                  -bg "$PREF::bgcol" -readonlybackground {} -highlightcolor "$PREF::bgcol" -validate none] $args
    }
}

proc configEntry { e state args } {
    switch -- $state {
        disabled { eval deactivateEntry $e -fg $::PREF::disabledfg $args }
        default  { eval activateEntry $e -fg $::PREF::fgcol $args }
    }
}

# For associating a list "list_var" with a bank of entry widgets.  
# Declare widgets like:
# for {set i 0} {$i < [llength $list_var]} {incr i} {
#     entry .e$i -validate key -validatecommand [list intolist list_var $i %P]
# }
proc intolist { list index val } {
#   uplevel \#0 "lset $list $index \{$val\}"
    uplevel \#0 "set $list \[lreplace \$$list $index $index \{$val\}\]"
    return 1
}

#   Fix problem with menu items being selected accidentally when clicking on
#   a menu button.  This blocks use of the menu's binding of ButtonRelease for
#   the first 300 ms (longer than that is interpreted as a valid selection).
#   This solution assumes that bindtags has not been used to put the class
#   bindings ahead of the individual widget bindings, and that the individual
#   menus have no binding for ButtonRelease-1 (any such would be removed). 
#   It also assumes that the menu is the only child of the menubutton.  Each
#   of these assumptions could be checked or handled if this procedure were
#   used as a universal tool.

bind Menubutton <Button-1> {+
   bind [winfo children %W] <ButtonRelease-1> {break}
   after $PREF::clickms { bind [winfo children %W] <ButtonRelease-1> {}}
}

#   Change behavior of entry widgets, so Control-u deletes text (shell-style)

bind Entry <Control-u> { %W delete 0 end }


#   Space key normally activates buttons and menus, but I want <Return> 
#   and <Down> to activate menus, and <Return> to activate buttons (in
#   addition to the space).  Generating a <space> event works better
#   than "%W invoke" because with the space the button-press is 
#   animated.  This can cause problems when the button destroys the
#   window.

event add <<Menu_Key>> <Return> <Down> <Up>
bind Button <Return> {event generate %W <space>}
bind Menubutton <<Menu_Key>> {event generate %W <space>}

#   Composite events used by different widgets
event add <<ApplyEntry>> <Return> <FocusOut>
event add <<Delimiter>> <comma> <space> <Return> <Up> <Down> <quotedbl>
event add <<CopyVertical>> <Shift-Up> <Shift-Down> <bar>
event add <<ApplyScale>> <Return> <FocusOut> <ButtonRelease>

#   Load images used for little button widgets.
image create photo grab_handle -file [file join $muisrc knob.gif]
image create photo arrow_down -file [file join $muisrc green_arrow_down12.gif]
image create photo arrow_right -file [file join $muisrc green_arrow_right12.gif]
image create photo delete_x -file [file join $muisrc redx12.gif]
image create photo small_printer -file [file join $muisrc small-printer-icon.gif]
image create photo small_folder  -file [file join $muisrc small-folder-icon.gif]
image create photo locked_icon  -file [file join $muisrc lock-closed.gif]
image create photo unlocked_icon  -file [file join $muisrc lock-open.gif]

switch -exact -- $env(USER) {
"m20d" - "bnqr" - "asnd" {
  namespace eval PREF {
    variable bgcol      "#fffef7"
    variable buttonbg   "#fff4e8"
    variable lightbg    "white"
    variable dimbgcol   "#fff0df"
    variable entrybg    "#f2d4b4"
    variable fgcol      "#000000"
    variable disabledfg "#aaaaaa"
    variable brightcol  "#d00000"
    variable brightalt  "#ff4488"
    variable revfgcol   "#ffffff"
    variable revbgcol   "#009000"
    variable revbgalt   "#006633"
    variable selectcol  "#446699"
  } }
default {
  namespace eval PREF {
    variable bgcol      "#d9d9d9"
    variable buttonbg   "#d9d9d9"
    variable entrybg    "#ececec"
    variable lightbg    "#ececec"
    variable dimbgcol   "#bbbbbb"
    variable fgcol      "#000000"
    variable disabledfg "#a3a3a3"
    variable brightcol  "#d90000"
    variable brightalt  "#ff4488"
    variable revfgcol   "#ffffff"
    variable revbgcol   "#0000a0"
    variable revbgalt   "#4040ff"
    variable selectcol  "#446699"
} } }

namespace eval PREF {
    variable fontsize   14
    variable titlesize  22
    variable monosize   12
    variable currfontsize $fontsize
    variable slidewidth 10
    variable textWidthMax [expr {5 + 9*(3+8)}]
    variable clickms 400
    variable menudirect below
    variable menutearoff 1

    variable ImusrUpdatePeriod 800
    variable graphTimeLimit 1800
    variable memoryLapse 120

    if { [info exists ::env(REMOTEHOST)] && ![string equal [info hostname] $::env(REMOTEHOST)] } {
	variable enableBeep 1
    } else {
	variable enableBeep 2
    }
    variable bellsounds [list drip.wav clink.wav notify.wav cuckoo.wav]

    variable muchar "\u00b5"
    # Handle conflict between Tk 8.4 and KDE for unicode window titles
    if { [string match "8.4*" [info patchlevel]] } {
        set muchar "mu"
    }
}

#   Create or adjust named fonts.
proc mui_font_size { what size } {
    set tsize [expr { -round((1.0*$size*$PREF::titlesize)/$PREF::fontsize) }]
    set msize [expr { -round((1.0*$size*$PREF::monosize)/$PREF::fontsize) }]
    set size -$size
    switch -glob -- $what {
        create {
            font $what muMainFont   -family helvetica -size $size -weight bold -slant roman
            font $what muEntryFont  -family helvetica -size $size -weight bold -slant roman
            font $what muTitleFont  -family {times new roman} -size $tsize -weight bold -slant roman
            if { [string first {times new roman} [string tolower [font actual muTitleFont]]] < 0  &&
                 [string first {nimbus roman} [string tolower [font actual muTitleFont]]] < 0 } {
                font configure muTitleFont  -family {Liberation Serif} -size $tsize -weight bold -slant roman
                if { [string first {liberation serif} [string tolower [font actual muTitleFont]]] < 0 } {
                    font configure muTitleFont  -family times -size $tsize -weight bold -slant roman
                }
            }
            font $what muMonoFont   -family fixed -size $msize -weight bold
            if { [string first {fixed} [font actual muMonoFont]] < 0 } {
                font configure muMonoFont -family Courier -size $msize -weight bold
            }
        }
        conf* {
            font $what muMainFont -size $size
            font $what muEntryFont -size $size
            font $what muTitleFont -size $tsize
            font $what muMonoFont -size $msize
        }
    }
}
set newfontsize $PREF::fontsize
mui_font_size create $PREF::fontsize

option add *font         muMainFont
option add *Entry.font   muEntryFont
catch {
    ::ttk::style configure . -font muMainFont
    ::ttk::style configure TEntry -font muEntryFont
}

tk_setPalette $PREF::bgcol

option add            *background  $PREF::bgcol
option add *Menu.activeBackground  $PREF::buttonbg
option add *Menubutton.background  $PREF::buttonbg
option add     *Button.background  $PREF::buttonbg
option add      *Entry.background  $PREF::entrybg
option add            *foreground  $PREF::fgcol
option add    *disabledForeground  $PREF::disabledfg
option add        *highlightColor  $PREF::brightcol

#option add *Listbox.background $PREF::lightbg
#option add *Listbox.selectBorderWidth 0
#option add *Listbox.selectForeground $PREF::revfgcol
#option add *Listbox.selectBackground $PREF::selectcol

option add *Entry.background $PREF::entrybg
option add *Entry.foreground $PREF::fgcol
option add *Entry.selectBorderWidth 0
option add *Entry.selectForeground $PREF::revfgcol
option add *Entry.selectBackground $PREF::selectcol
option add *Entry.readonlyBackground $PREF::bgcol

option add *Text.background $PREF::lightbg
option add *Text.selectBorderWidth 0
option add *Text.selectForeground $PREF::revfgcol
option add *Text.selectBackground $PREF::selectcol

#option add *Menu.background $PREF::lightbg
#option add *Menu.activeBackground $PREF::selectcol
#option add *Menu.activeForeground $PREF::revfgcol
#option add *Menu.activeBorderWidth 0
#option add *Menu.highlightThickness 0
#option add *Menu.borderWidth 2

# Minimal adjustment to ttk style
namespace eval PREF { catch {
    set activebg [option get . *.activeBackground {}]
    ::ttk::style map TButton \
        -background [list disabled $bgcol active $activebg {} $buttonbg] \
        -foreground [list disabled $disabledfg {} $fgcol]
    ::ttk::style map TEntry \
        -background [list disabled $bgcol active $activebg {} $entrybg] \
        -foreground [list disabled $disabledfg {} $fgcol]
    ::ttk::style map TNotebook.Tab \
        -background [list disabled $bgcol active $activebg selected $bgcol {} $buttonbg] \
        -foreground [list disabled $disabledfg {} $fgcol] \
        -expand {{} {2}}
    ::ttk::style map . -background [list disabled $bgcol active $activebg {} $bgcol] \
        -foreground [list disabled $disabledfg {} $fgcol] \
        -darkcolor [list disabled $disabledfg {} $fgcol]

} }

#  Assign colors and fonts and ... ?
#  Not really implemented yet.

# Reject crazy display resolution pixels-per-point
catch {
    if { [tk scaling] > 1.5 } {
        puts "Force resolution to 90dpi (was [expr {[tk scaling]*72}])"
        tk scaling 1.25 ;# 1.25 ppp = 90 dpi
    }
}
