#  mumid_cfg.tcl
#  Midas uSR acquisition system parameters.
#  $Log: mumid_cfg.tcl,v $
#  Revision 1.6  2015/03/20 00:34:27  suz
#  changes by Donald. This version part of package for new VMIC frontends
#
#  Revision 1.5  2004/08/05 02:21:35  asnd
#  Change default editor
#
#  Revision 1.4  2004/04/10 07:00:07  asnd
#  Add auto-run control
#
#  Revision 1.3  2003/10/15 09:28:28  asnd
#  Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#  Revision 1.2  2002/07/16 06:00:53  asnd
#  Changes up to Jul 15
#
#  Revision 1.1  2002/06/12 01:26:39  asnd
#  Starting versions of Mui files.
#

namespace eval cfg {
    variable minTDC 0
    variable maxTDC 7
    variable minOR 0
    variable maxOR 15
    variable minScal 0
    variable maxScal 15
    variable numScaler 10
    variable numCounter 8
    variable lenCtName 14
    variable lenScName 16
#   And 1 less:
    variable endCtName 13
    variable endScName 15
    if { [info exists ::env(MIDAS_EXPT_NAME)] } {
        variable expt $::env(MIDAS_EXPT_NAME)
    } else {
        variable expt "musr"
    }
}

array set cfg::editorChoices {
    emacs emacs 
    nedit nedit 
    kedit kedit
    pico {xterm -fn 9x15 -fb 9x15bold -e pico}
    vi {xterm -fn 9x15 -fb 9x15bold -e vi}
    {} nedit
}
# emacs is too slow to load for it to be the default

# Default values for some user-interface behavior

global mode midas
set midas(refresh_ms) 8000
set midas(max_bg_err) 5

set mode(num_back) 3
set mode(default_gate_add) 0.4
set mode(backups) \
    [list Previous_mode Next_previous Third_previous Fourth_previous Fifth_previous]

