#! /bin/sh
# the next line restarts using mtcl \
exec $MUSR_DIR/musr_midas/midas_tcl/mtcl "$0" "$@"

#   TRIUMF �SR RUN CONTROL - both TD and TI
#   $Log: musrrc.tcl,v $
#   Revision 1.14  2015/09/28 00:44:07  asnd
#   Restore camp variable log from file
#
#   Revision 1.13  2015/03/20 00:33:18  suz
#   changes by Donald. This version part of package for new VMIC frontends
#
#   Revision 1.12  2013/10/30 05:54:05  asnd
#   Changes for updating target counts
#
#   Revision 1.11  2008/04/15 01:36:10  asnd
#   Various small adjustments
#
#   Revision 1.10  2007/11/21 23:42:29  asnd
#   Revise startup order and client shutdown
#
#   Revision 1.9  2005/07/01 03:48:57  asnd
#   Better error message when run without DISPLAY.
#
#   Revision 1.8  2005/06/22 15:34:51  asnd
#   add change_titles
#
#   Revision 1.7  2004/04/10 07:00:07  asnd
#   Add auto-run control
#
#   Revision 1.6  2003/10/01 09:56:12  asnd
#   Minor changes (use "other" instead of "field" for field sweeps)
#
#   Revision 1.5  2003/09/26 00:39:03  asnd
#   Recent gui changes (sweep details, ggl pulser...)
#
#   Revision 1.4  2003/09/19 06:23:27  asnd
#   ImuSR and many other revisions.
#
#   Revision 1.3  2002/07/23 09:41:00  asnd
#   Add HMVW utilities window
#
#   Revision 1.2  2002/06/24 22:27:19  asnd
#   original
#
#   Revision 1.1  2002/06/12 01:26:39  asnd
#   Starting versions of Mui files.
#

#  Source directory.

#puts "Start musrrc"

global muisrc 

set muisrc [file dirname [info script]]

if { [catch {package require Tk} mes] } {
    if { [info exists env(DISPLAY)] } {
        puts stderr "Failed to connect with graphics display \"$env(DISPLAY)\"."
    } else {
        puts stderr "This is a graphical application, but no graphics display is available."
    }
    puts stderr $mes
    exit
}


#cd $muisrc

#if { !  [file exists $muisrc/mui_utils.tcl] && \
#        [file exists mui_utils.tcl] } {
#    set muisrc "."
#}

set tcl_precision 15

source [file join $muisrc mui_utils.tcl]

# source [file join $muisrc td.ui.tcl]
source [file join $muisrc mui.ui.tcl]
source [file join $muisrc td_extra.tcl]
source [file join $muisrc imusr_extra.tcl]
source [file join $muisrc status.ui.tcl]
source [file join $muisrc status_extra.tcl]

#puts "Sourced some files"

# Temporary no-ops
proc logvar_update { } { puts "Suppressed logvar_update" }
proc idef_do_update { {time -1} } { puts "Suppressed idef_update" }
proc rig_update { } { puts "Suppressed rig_update" }

#puts "begin ui"
# td_ui .
mui_ui .
#puts "Returned from ui"
#mui_display_tdmusr

# After initial screen, load the rest of sources and perform update
after 400 after idle {
    source [file join $muisrc rig.ui.tcl]
    source [file join $muisrc rig_extra.tcl]
    source [file join $muisrc mode.ui.tcl]
    source [file join $muisrc mode_extra.tcl]
    source [file join $muisrc expert.ui.tcl]
    source [file join $muisrc expert_extra.tcl]
    source [file join $muisrc logvar.ui.tcl]
    source [file join $muisrc logvar_extra.tcl]
    source [file join $muisrc iquick.ui.tcl]
    source [file join $muisrc imusr_setup.ui.tcl]
    source [file join $muisrc isetup_extra.tcl]
    source [file join $muisrc hmutil.ui.tcl]
    source [file join $muisrc hmutil_extra.tcl]
    source [file join $muisrc camp_inter.ui.tcl]
    source [file join $muisrc camp_inter_extra.tcl]
    source [file join $muisrc pulser.ui.tcl]
    source [file join $muisrc pulser_extra.tcl]
    source [file join $muisrc details.ui.tcl]
    source [file join $muisrc details_extra.tcl]
    source [file join $muisrc autor.ui.tcl]
    source [file join $muisrc autor_extra.tcl]
    source [file join $muisrc change_titles.tcl]
    # Perform updates, including camp and rig this time
    catch logvar_restore_log
    catch td_background_update
    
}
