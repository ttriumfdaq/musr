#  MuSR odb interaction.
#  Some routines copy areas of the odb into array variables
#  (midas run rstat mode rig), and others serve as simple wrappers
#  around single midas-tcl "midas" commands.  The purpose is to 
#  have complete separation between the tk-gui functions and the
#  odb interaction.  (Nevertheless, some variables, derived from 
#  ODB parameters and used only for gui labels, are set here;
#  e.g., run(state_label) and run(last_file).)
#
#  $Log: odb_musr.tcl,v $
#  Revision 1.38  2016/09/06 22:10:53  asnd
#  Add more beamline control to autoruns, and fix some bugs
#
#  Revision 1.37  2015/09/28 00:43:01  asnd
#  odb_init_acqrecord changed to use new init_acq_record (read_midas_log.tcl) to load history
#
#  Revision 1.36  2015/09/26 06:32:30  asnd
#  update logfile dir; init acq record; reapply gates; hits counters changed; imusr tolerance
#
#  Revision 1.35  2015/04/17 23:57:36  asnd
#  Use default Midas timeout, but mtcl should NOT run a watchdog! (Causes erroneous exit).
#
#  Revision 1.34  2015/03/20 00:31:09  suz
#  changes by Donald for VMIC frontends
#
#  Revision 1.33  2013/10/30 05:53:19  asnd
#  Changes for updating target counts
#
#  Revision 1.32  2008/05/15 04:35:22  asnd
#  Avoid rounding nonsensical-low time limits
#
#  Revision 1.31  2008/04/30 01:56:24  asnd
#  Convert to bnmr-style /autorun parameters in odb
#
#  Revision 1.30  2008/04/15 01:36:10  asnd
#  Various small adjustments
#
#  Revision 1.29  2007/11/21 23:45:05  asnd
#  Changes leading up to bnmr support (coexistence now)
#
#  Revision 1.28  2007/10/03 02:26:45  asnd
#  Conversion of modulation value; add autorun-hist-number
#
#  Revision 1.27  2006/11/21 22:43:39  asnd
#  When starting real runs, force purge+archive enabled
#
#  Revision 1.26  2006/09/22 03:44:19  asnd
#  Change times displayed between runs; Improve initialization of acq-on record.
#
#  Revision 1.25  2006/05/26 16:59:52  asnd
#  Accumulated changes over winter shutdown.
#
#  Revision 1.24  2005/06/22 15:23:46  asnd
#  Automatic headers revised
#
#  Revision 1.23  2004/11/11 03:17:53  asnd
#  No failure when missing weighted averages
#
#  Revision 1.22  2004/11/10 13:04:45  asnd
#  Enjoy Suzannah's improvements to mdarc when rebooting; and add "abrupt stop" run control.
#
#  Revision 1.21  2004/11/10 06:16:57  asnd
#  Use weighted Camp statistics calculated by mdarc (through odb)
#
#  Revision 1.20  2004/11/07 08:27:12  asnd
#  Allow FE reboot during unattended autoruns & deal with run termination.
#
#  Revision 1.19  2004/10/29 06:52:03  asnd
#  Minor touch-ups to behaviour on autorun control page
#
#  Revision 1.18  2004/06/19 04:15:02  asnd
#  Report true totals rate (counts per second) for status page
#
#  Revision 1.17  2004/04/27 11:50:23  asnd
#  Audible alert when run ends asynchronously, or gets enough counts
#
#  Revision 1.16  2004/04/21 05:15:13  asnd
#  Watch out for divide-by-zero
#
#  Revision 1.15  2004/04/15 02:59:44  asnd
#  Enable ImuSR "end run after..." features
#
#  Revision 1.14  2004/04/10 07:00:07  asnd
#  Add auto-run control
#
#  Revision 1.13  2004/02/20 23:08:06  asnd
#  Single musr Midas experiment.  Handle year changes.
#
#  Revision 1.12  2003/11/24 05:43:26  asnd
#  Handle year changes.
#
#  Revision 1.11  2003/11/04 20:26:30  asnd
#  Retry in odb_reconnect.
#
#  Revision 1.10  2003/10/15 09:28:28  asnd
#  Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#  Revision 1.9  2003/09/19 06:23:27  asnd
#  ImuSR and many other revisions.
#
#  Revision 1.7  2002/10/01 05:50:19  asnd
#  Improve recording of acquisition for graph
#
#  Revision 1.6  2002/08/23 09:05:55  asnd
#  archive data on write; unstick bad transition-in-progress;
#  de-parent because of failed timouts on message boxes.
#
#  Revision 1.5  2002/07/27 09:28:04  asnd
#  Start implementation on hit-rate diagnostics.
#
#  Revision 1.4  2002/07/26 06:21:28  asnd
#  First working version for rebooting HMVW computer
#
#  Revision 1.3  2002/07/23 09:41:00  asnd
#  Add HMVW utilities window
#
#  Revision 1.2  2002/06/24 22:27:19  asnd
#  original
#
#  Revision 1.1  2002/06/12 01:26:39  asnd
#  Starting versions of Mui files.
#

proc odb_connect { } {
    global midas
    puts "connect to midas using: connect_experiment {$midas(host)} {$midas(expt)} {$midas(application)}"
    midas connect_experiment $midas(host) $midas(expt) $midas(application)
    midas get_experiment_database
    midas set_value "/Programs/$midas(application)/Watchdog timeout" 60000
    set midas(connected) 1
    set midas(musr) [expr { ! [string match "b*" $midas(expt)]} ]
    puts "Established connection to experiment $midas(expt)"
}

proc odb_disconnect { } {
    global midas
    puts "disconnect $midas(application) from midas"
#   after cancel $midas(heart_id)
    set midas(connected) 0
    midas disconnect_experiment
}

proc odb_reconnect { } {
    global midas
    if { ! $midas(connected) } { odb_connect }
    if { $midas(connected) } {
        if { [ catch {set x [midas get_value "/Programs/$midas(application)/Watchdog timeout" ] } err ] } {
            puts "Error getting sample odb param: $err\n...Retry..."
            after 100
            if { [ catch {set x [midas get_value "/Programs/$midas(application)/Watchdog timeout" ] } err ] } {
                puts "Error on retry: $err\nDisconnect and reconnect" 
                odb_disconnect
                after 400
                odb_connect
            }
        }
    }
    if { [midas yield] == "SHUTDOWN" } {
        odb_disconnect
        exit
    }
    if { ! $midas(connected) } { return -code error "Could not connect to odb" }
}

#   Actively change experiment type to $exp (nice textual name) $mexp (midas version 
#   of type; was once the experiment name).  The midas experiment name is not paseed,
#   but is $midas(expt).
#
#   As of early 2004, there is only one musr experiment in Midas ("musr"), 
#   so we do not connect to experiment "$mexp"; but $mexp is still necessary
#   for controlling the symbolic link for booting the PPC.  We rely on a 
#   subsequent check on client health to start up anything not running (and
#   restart the HM frontend). 
#
#   By 2014, experiments using vmic frontend are named according to the beamline,
#   (or user) and there is no symlink controlling what frontend gets started 
#   (there is no booting of the vmic either).

proc odb_set_exper_type { exp mexp } {
    global midas env run

    #puts "odb_set_exper_type {$exp} {$mexp} "
    odb_reconnect

    set run(expertype) [midas get_value "/Experiment/Edit on start/musr type"]

    if { [string equal $midas(expt) "musr"] } {# PPC, so set symlink for reboot
        set ppc_link "none"
        catch {
            set ppc_link [file readlink [file join $env(HOME) online ppc_startup.cmd]]
        }
        if { ![string match *startup.$mexp $ppc_link] } {
            catch { file delete [file join $env(HOME) online ppc_startup.cmd] }
            #puts "Declare link: [file join $env(MUSR_DIR) musr_midas frontend $midas(beamline) startup/startup.$mexp] [file join $env(HOME) online ppc_startup.cmd]"
            file link -symbolic [file join $env(HOME) online ppc_startup.cmd] \
                [file join $env(MUSR_DIR) musr_midas frontend $midas(beamline) startup startup.$mexp]
        }
    } else {
        #puts "vmic"
    }
    midas set_value "/Experiment/Edit on start/musr type" $exp
#
#   Do not connect to a different Midas experiment now (see note above).
#
#   odb_disconnect
#   set midas(expt) $mexp
#   odb_connect
#   midas set_value "/Experiment/Edit on start/musr type" $exp

    odb_get_general

#   Perform frontend restart (if necessary) by an immediate client check
    set midas(clt_checked_at) 0
    #puts "finished odb_set_exper_type"

    return ""
}

proc odb_set_general { } {
    global midas

    odb_reconnect

    set base "/Equipment/MUSR_TD_acq/mdarc"
    midas set_value "$base/camp/camp hostname"  $midas(camp_host)
    midas set_value "$base/num_versions_before_purge"  $midas(keep_versions)
    midas set_value "$base/archived_data_directory"  $midas(archive_to)
    midas set_value "$base/save_interval(sec)"  $midas(save_sec)
    midas set_value "$base/saved_data_directory"  $midas(data_dir)
    midas set_value "$base/enable mdarc logging"  $midas(mdarc_enabled)

    set dd [midas get_value "/Logger/Data dir"]
    if { ![string equal $dd $midas(data_dir)] } {
        midas set_value "/Logger/Data dir"  $midas(data_dir)
    }

    return ""
}


#   Included in the general information is the mode name and rig name -- so
#   we don't have to get the whole rig/mode to display the names.

proc odb_get_general { } {
    global midas rig mode run env
#
    odb_reconnect
#
    set base "/Experiment/Edit on start"
    set run(expertype) [midas get_value "$base/musr type" ]
#
    set base "/Equipment/MUSR_TD_acq/v680"
    set midas(hm_host) [midas get_value "$base/hostname" ]
#
    set base "/Equipment/MUSR_TD_acq/mdarc"
    set midas(camp_host) [midas get_value "$base/camp/camp hostname" ]
    set midas(keep_versions) [midas get_value "$base/num_versions_before_purge" ]
    set midas(archive_to) [midas get_value "$base/archived_data_directory" ]
    set midas(archive_cmd) [midas get_value "$base/archiver task" ]
    set midas(save_sec) [midas get_value "$base/save_interval(sec)" ]
    set midas(scriptpath) [midas get_value "$base/perlscript path" ]
    set midas(data_dir) [midas get_value "$base/saved_data_directory" ]
    set midas(mdarc_enabled) [midas get_value "$base/enable mdarc logging"]
#
    set base "/Equipment/MUSR_TD_acq/Settings"
    set midas(beamline) [midas get_value "$base/beamline" ]
    set rig(rigname) [midas get_value "$base/rig/current rig"]
    set mode(modename) [midas get_value "$base/mode/current mode" ]
#
    set base "/Logger"
    set midas(log_file) [file join [midas get_value "$base/Data dir"] \
			 [midas get_value "$base/Message file"]]

    set run(ImuSR) [string match -nocase {I*} $run(expertype)]

    return ""
}


#   odb_set_runinfo only sets the titles parameters!  Other parameters dealing
#   with the run state (and run number) are read-only.  They are changed by 
#   performing specific actions, like odb_begin_run, odb_set_run_type... 

proc odb_set_runinfo { } {
    global run midas

    odb_reconnect

    set base "/Experiment/Edit on start"
#   Don't set this without meaning to:
#   midas set_value "$base/musr type" $run(expertype)
#   Use odb_set_run_type instead!
    midas set_value "$base/run_title"  $run(runtitle) 
    midas set_value "$base/sample"  $run(sample)
    midas set_value "$base/temperature"  $run(temperature) 
    midas set_value "$base/field"  $run(field) 
    midas set_value "$base/orientation"  $run(orientation) 
    midas set_value "$base/experimenter"  $run(operator) 
    midas set_value "$base/experiment number"  $run(experiment) 

    set run(ImuSR) [string match -nocase {I*} $run(expertype)]
    if { $run(ImuSR) } {
        midas set_value "$base/imusr comment1" $run(comment1)
        midas set_value "$base/imusr comment2" $run(comment2)
        midas set_value "$base/imusr comment3" $run(comment3)
        midas set_value "$base/imusr subtitle" $run(subtitle)
    }

    odb_set_auto_head

    return ""
}

# Applying automatic run-header information may be done separately from odb_set_runinfo:
proc odb_set_auto_head { } {
    global run midas

    odb_reconnect
    set base "/Equipment/MUSR_TD_acq/mdarc/camp"
#
    midas set_value "$base/temperature variable" $run(autoTvar)
    midas set_value "$base/record temperature error" $run(autoTerror)
    midas set_value "$base/field variable" $run(autoBvar)
    midas set_value "$base/record field error" $run(autoBerror)

    return ""
}

proc odb_get_runinfo { } {
    global run midas lv_acq_record

    odb_reconnect
#
    set prev_number $run(number)
    set prev_state $run(state)
    set prev_start $run(start_sec)
#
    set base "/Runinfo"
    set run(state) [midas get_value "$base/State" ]
    set run(online) [midas get_value "$base/Online Mode" ]
    set run(number) [midas get_value "$base/Run number" ]
    set run(transition) [midas get_value "$base/Transition in progress" ]
    set run(req_trans) [midas get_value "$base/Requested transition" ]
    set run(start_time) [midas get_value "$base/Start time" ]
    set run(start_sec) [midas get_value "$base/Start time binary" ]
    set run(stop_time) [midas get_value "$base/Stop time" ]
    set run(stop_sec) [midas get_value "$base/Stop time binary" ]
#
    set run(in_progress) [expr {$run(state) > 1} ]
    set run(paused) [expr {$run(state) == 2} ]

#   When we see the run is active, we force not-"starting" 

    if { $run(state) == 3 && $run(transition) == 0 } { 
	set run(starting) 0
    }

#   run(starting) has values: 0 = not starting, 1 while Midas starting run, 2 while
#   prompting user for ImuSR sweep range, 3 while ramping ImuSR sweep device.
#   The startup sequence for imusr gives run_starting 0-2-3-1-0; corresponding with
#   state_label:  Stopped - Preparing - Initializing - Starting - Running
#   This is NOT the sequence I want!!!  If we can get the fe to start in paused
#   state, then: Stopped - Preparing - Starting - Initializing - Running.
#   ...see saved_imusr_extra.tcl and examine all uses of run(starting).
#
#   Ordinarily, $run(starting)==0 and the state label comes from $run(state).

    set run(state_label) [lindex [list \
			[lindex [list bad Stopped Paused Running] $run(state)] \
			Starting Preparing Initializing ] $run(starting) ]

#   puts "odb_get_runinfo $run(starting) $run(state) $run(transition) $run(state_label)"
#
    set base "/Equipment/MUSR_TD_acq/mdarc"
    set run(runtype) [midas get_value "$base/run type" ]
    set run(last_save) [midas get_value "$base/time_of_last_save" ]
    set run(last_file) [midas get_value "$base/last_saved_filename" ]
    if { [ scan [ file extension $run(last_file) ] {.msr_v%d} v ] == 1 } {
        set run(last_file) "[file rootname $run(last_file)].msr (v${v})"
    }
#
    # Check for and record changes of run -- given by start_sec changing.
    # (The run number will change when changing acq between real/test)
    set now [clock seconds]
    if { $prev_start != $run(start_sec) } { # Run began
        # Ensure acq tags show true begin time:
        set now $run(start_sec)
        if { [string compare $prev_number $run(number)] && [llength $run(start_record)]} {
            lappend lv_acq_record $run(start_record)
        }
        # Make autoruns control panel keep track of counts (because the counts get reset
        # by server at start of run), even if values are being edited and are not applied.
        # Fail silently when autoruns not configured.
	catch {
	    global mauto autor
	    set autor(counts) [midas get_value "/autorun/target counts"]
	    set mauto(counts) $autor(counts)
	    set autor(counthist) [midas get_value "/autorun/count hidtogram"]
	    set mauto(counthist) $autor(counthist)
	}
    }
    if { $run(in_progress) } {
        set run(ssat_text) "Saved:"
        set elmin [expr { int( ($now-$run(start_sec))/60 ) }] 
        if { $elmin > 0 && $elmin < 5555 } {
            set run(ssat_time) "$run(last_save)   ($elmin min)"
        } else {
            set run(ssat_time) $run(last_save)
        }
        set run(start_record) [list lv_run_started $run(number) $run(start_sec)]
        set run(next_number) $run(number)
    } else {
        set run(ssat_text) "Stopped:"
        set run(ssat_time) $run(stop_time)
        if { [string length $run(start_record)]} {
            lappend lv_acq_record $run(start_record)
            set run(start_record) {}
        }
        set run(next_number) [expr {$run(number) + 1}]
    }
    # Record changes to acq-status markers:
    if { $run(state) == 3 && $prev_state != 3 } {# Acquisition just turned ON
        if { [ string length $run(acqoff_record) ] } {
            # If acq-off marker is active:
            #puts "lv_acq_record [list lv_acq_off $run(acqoff_record) $now]"
            lappend lv_acq_record [list lv_acq_off $run(acqoff_record) $now]
            set run(acqoff_record) {}
        }
    } elseif { $run(state) != 3 && $prev_state == 3 } {# Acquisition just turned OFF
        set run(acqoff_record) $now
    }

    set run(test_mode) [expr [string compare $run(runtype) real] ]
#
    set base "/Experiment/Edit on start"
    set run(runtitle) [midas get_value "$base/run_title" ]
    set run(sample) [midas get_value "$base/sample" ]
    set run(temperature) [midas get_value "$base/temperature" ]
    set run(field) [midas get_value "$base/field" ]
    set run(orientation) [midas get_value "$base/orientation" ]
    set run(operator) [midas get_value "$base/experimenter" ]
    set run(experiment) [midas get_value "$base/experiment number" ]

    set run(expertype) [midas get_value "$base/musr type" ]
    switch -glob -- $run(expertype) {
         [iI]* {
            set run(expertype) "I-�SR"
            set run(ImuSR) 1
            set run(comment1) [midas get_value "$base/imusr comment1" ]
            set run(comment2) [midas get_value "$base/imusr comment2" ]
            set run(comment3) [midas get_value "$base/imusr comment3" ]
            set run(subtitle) [midas get_value "$base/imusr subtitle" ]
        }
        default {
            set run(expertype) "TD-�SR"
            set run(ImuSR) 0
            set base "/Equipment/MUSR_TD_acq/mdarc/histograms"
            set run(hist_total) [midas get_value "$base/output/total saved" ]
            # round to integer if possible
            catch { set run(hist_total) [expr {round($run(hist_total))}] }
        }
    }
    set base "/Equipment/MUSR_TD_acq/mdarc/camp"
    set run(autoTvar) [midas get_value "$base/temperature variable"]
    set run(autoTerror) [midas get_value "$base/record temperature error"]
    set run(autoBvar) [midas get_value "$base/field variable"]
    set run(autoBerror) [midas get_value "$base/record field error"]

    return ""
}


#   Some portion of odb_get_runinfo plus setting up minimal initial acq record.
#
proc odb_init_acqrecord { } {
    #puts "odb_init_acqrecord"
    global run midas lv_acq_record

    odb_reconnect
#
    set base "/Runinfo"
    set run(state) [midas get_value "$base/State" ]
    set run(number) [midas get_value "$base/Run number" ]
    set run(start_sec) [midas get_value "$base/Start time binary" ]
    set run(stop_sec) [midas get_value "$base/Stop time binary" ]
#
    set run(in_progress) [expr {$run(state) > 1} ]
    set run(paused) [expr {$run(state) == 2} ]

    set acqtext [exec tail -n 2000 $midas(log_file) | grep -e {\] Run \#[0-9]* st} ]

    set on 0
    set started 0
    set stopped 0
    set rnum 0
    set earliest [expr {[clock seconds]-3600*48}]
    set run(acqoff_record) $earliest

    foreach line [split $acqtext "\n"] {
        if { [regexp {^(.*) \[\w*\] Run \#(\d+) (st\w*ed)} $line -- time num state] } {
            if { [string equal $state "started"] } {
                set started [clock scan $time]
                set rnum $num
                set run(start_record) [list lv_run_started $rnum $started]
                set on 1
            } elseif { $on && [string equal $state "stopped"] } {
                set stopped [clock scan $time]
                if { $stopped > $earliest } {
                    lappend lv_acq_record [list lv_acq_off $run(acqoff_record) $started]
                    lappend lv_acq_record $run(start_record)
                }
                set on 0
                set run(acqoff_record) $stopped
            }
        }
    }
    set upto [clock seconds]
    if { $run(in_progress) } {
        set run(start_record) [list lv_run_started $run(number) $run(start_sec)]
        lappend lv_acq_record $run(start_record)
        #if { !$run(paused) } { 
        #    set upto $run(start_sec)
        #    lappend lv_acq_record [list lv_acq_off 0 $upto]
        #}
    }
    #puts "Initialized acq record $lv_acq_record"
    # Soon, collect past acquisition records, and update current
    after 2222 "catch {source [file join $::muisrc read_midas_log.tcl]; init_acq_record; odb_get_runinfo}"
}

proc odb_set_mode { } {
    global mode rig run

    odb_reconnect
#
    set base "/Runinfo"
    set run(state) [midas get_value "$base/State" ]
    set run(in_progress) [expr {$run(state) > 1} ]
#
    set base "/Equipment/MUSR_TD_acq/Settings/mode"
    midas set_value "$base/current mode" $mode(modename)
    if { ! $run(in_progress) } {
        midas set_value "$base/dual spectra mode/enabled" $mode(dual_enabled)
        midas set_data_array "$base/dual spectra mode/title"   $mode(dual_titles)
        midas set_data_array "$base/dual spectra mode/suffix"  $mode(dual_suffs)
        midas set_value "$base/histograms/TDC resolution code" $mode(resolution)
        midas set_value "$base/histograms/num bins"      $mode(hlen)
        midas set_value "$base/histograms/bytes per bin" $mode(bytesbin)
        midas set_value "$base/histograms/num selected counters" $mode(numsel)
        midas set_data_array "$base/histograms/select counters to histogram" $mode(selcounters)
    }
    midas set_data_array "$base/histograms/bin zero" $mode(tzero)
    midas set_data_array "$base/histograms/first good bin" $mode(fgbin)
    midas set_data_array "$base/histograms/last good bin"  $mode(lgbin)
    midas set_data_array "$base/histograms/first background bin" $mode(fback)
    midas set_data_array "$base/histograms/last background bin"  $mode(lback)
    midas set_value "$base/gates/data gate code"   $mode(Dgate_code)
    midas set_value "$base/gates/TDC gate code"    $mode(TDCgate_code)
    midas set_value "$base/gates/pileup gate code" $mode(PUgate_code)

    midas save $base [file join $rig(rigpath) $rig(rigname) $mode(modename).mode]

    set base "/Equipment/MUSR_TD_acq/v680"
    midas set_value "$base/delta"   $mode(Dgate_code)
    midas set_value "$base/delta1"  $mode(TDCgate_code)
    midas set_value "$base/delta2"  $mode(PUgate_code)

    return ""
}


proc odb_reapply_gates { } {
    set base "/Equipment/MUSR_TD_acq/v680"
    set Dgate_code  [midas get_value "$base/delta"]
    set TDCgate_code  [midas get_value "$base/delta1"]
    set PUgate_code  [midas get_value "$base/delta2"]
    midas set_value "$base/delta"   $Dgate_code
    midas set_value "$base/delta1"  $TDCgate_code
    midas set_value "$base/delta2"  $PUgate_code
}


proc odb_get_mode { } {
    global mode midas

    odb_reconnect
#
    set base "/Equipment/MUSR_TD_acq/Settings/mode"
    set mode(modename)    [midas get_value "$base/current mode" ]
    set mode(dual_enabled) [midas get_value "$base/dual spectra mode/enabled" ]
    set mode(dual_titles) [midas get_data "$base/dual spectra mode/title" ]
    set mode(dual_suffs) [midas get_data  "$base/dual spectra mode/suffix" ]
    set mode(resolution) [midas get_value "$base/histograms/TDC resolution code" ]
    set mode(hlen)       [midas get_value "$base/histograms/num bins" ]
    set mode(bytesbin)   [midas get_value "$base/histograms/bytes per bin" ]
    set mode(numsel)     [midas get_value "$base/histograms/num selected counters" ]
    set mode(selcounters) [midas get_data "$base/histograms/select counters to histogram" ]
    set mode(tzero) [midas get_data "$base/histograms/bin zero" ]
    set mode(fgbin) [midas get_data "$base/histograms/first good bin" ]
    set mode(lgbin) [midas get_data "$base/histograms/last good bin" ]
    set mode(fback) [midas get_data "$base/histograms/first background bin" ]
    set mode(lback) [midas get_data "$base/histograms/last background bin" ]
    set mode(Dgate_code)   [midas get_value "$base/gates/data gate code" ]
    set mode(TDCgate_code) [midas get_value "$base/gates/TDC gate code"  ]
    set mode(PUgate_code)  [midas get_value "$base/gates/pileup gate code" ]

    set base "/Equipment/MUSR_TD_acq/v680"
    set mb 1024
    scan [midas get_value "$base/maxbyte" ] {%d} mb
    set mode(maxbins) [ expr {$mb/2} ]
#
#   Others based on the above:
    catch {
        set mode(numhist) [ expr { $mode(numsel) + $mode(numsel) * $mode(dual_enabled) } ]
        set div [ expr round(pow(2,26.0-$mode(resolution))) ]
        set mode(binsize) [ expr 50.0/$div ]
        if { $mode(resolution) > 18 } {
            set mode(binsize_label) " $mode(binsize)  ns  "
        } else {
            set mode(binsize_label) " [expr 1000.0 * $mode(binsize)]  ps  "
        }
        set mode(hlen_us) [format {%.3f} [expr $mode(binsize)*$mode(hlen)/1000.0 ] ]
        set mode(hlen_label) "$mode(hlen) bins  ($mode(hlen_us) �s)"
    }
#
    return ""
}

proc odb_set_rig { } {
    global rig midas

    odb_reconnect
#
    set base "/Equipment/MUSR_TD_acq/Settings/rig"
    midas set_value "$base/current rig" $rig(rigname)
#
#   scalers
    midas set_value "$base/scalers/enabled" $rig(enable_scalers)
    midas set_value "$base/scalers/num inputs" $rig(num_scalers)
    midas set_data_array "$base/scalers/titles" $rig(scaler_names)
    midas set_data_array "$base/scalers/inputs" $rig(scaler_in)
#
#   counters
    midas set_value "$base/counters/number defined" $rig(num_counters)
    midas set_data_array "$base/counters/names" $rig(counter_names)
    midas set_data_array "$base/counters/TDC input" $rig(TDC_in)
#
#   output register
    midas set_value "$base/output_register/enabled" $rig(enable_OR)
    midas set_data_array "$base/output_register/outputs in use" $rig(OR_out)
#
    if { ! [file exists "$rig(rigpath)/$rig(rigname)"] } {
        file mkdir "$rig(rigpath)/$rig(rigname)"
    }
    midas save $base [file join $rig(rigpath) $rig(rigname) $rig(rigname).rig]

    return 1
}

proc odb_get_rig { } {
    global rig midas

    odb_reconnect
#
    set base "/Equipment/MUSR_TD_acq/Settings"
    set rig(rigpath) [midas get_value "$base/rig path"]

    set base "/Equipment/MUSR_TD_acq/Settings/rig"
    set rig(rigname) [midas get_value "$base/current rig"]
#
#   scalers
    set rig(enable_scalers) [midas get_value "$base/scalers/enabled"]
    set rig(num_scalers)   [midas get_value "$base/scalers/num inputs"]
    set rig(scaler_names) [midas get_data "$base/scalers/titles"]
    set rig(scaler_in)   [midas get_data "$base/scalers/inputs"]
    if { ! $rig(enable_scalers) } { set rig(num_scalers) 0 }
    set rig(scaler_names) [lrange $rig(scaler_names) 0 [expr $rig(num_scalers) - 1 ] ]
#
#   counters
    set rig(num_counters) 0
    scan [midas get_value "$base/counters/number defined"] {%d} rig(num_counters)
    set rig(counter_names) [midas get_data "$base/counters/names"]
    set rig(TDC_in) [midas get_data "$base/counters/TDC input"]
    set rig(counter_names) [lrange $rig(counter_names) 0 [expr $rig(num_counters) - 1 ] ]
#
#   output register
    set rig(enable_OR) [midas get_value "$base/output_register/enabled"]
    set rig(OR_out) [midas get_data "$base/output_register/outputs in use"]
#
#   Look for inconsistencies:
    if { $rig(enable_scalers) == 0} { set rig(num_scalers) 0 }
    if { $rig(num_scalers) == 0} { set rig(enable_scalers) 0 }
    set rig(rigpath) [string trimright $rig(rigpath) "/"]
#
    return ""
}

proc get_midas_int { path } {
    set i [midas get_value $path]
    catch { set i [expr { round($i) }] }
    return $i
}

proc get_midas_float { path } {
    set a [midas get_value $path]
    catch { set a [expr { 0.0+$a }] }
    return $a
}

proc get_midas_String { path } {
    set s [string tolower [midas get_value $path]]
    catch {
        set s [string replace $s 0 0 [string toupper [string range $s 0 0]]]
    }
    return $s
}

proc odb_get_idef { } {
    global idef midas

    odb_reconnect

    #puts "odb_get_idef"
    set base "/Equipment/MUSR_I_acq/settings"
    set idef(setup_name) [midas get_value "$base/config name"]
    set idef(setup_path) [midas get_value "$base/config path"]

    set base "/Equipment/MUSR_I_acq/settings/hardware/gate generator"
    set idef(preset_counts)   [midas get_value "$base/down counter preset"]
    set idef(ggl_dac_range)   [get_midas_int "$base/DAC range"]
    set idef(pulser_enable)   [midas get_value "$base/Pulser Enable"]
    set idef(pulser_ontime)   [get_midas_int "$base/Pulser High (counts)"]
    set idef(pulser_offtime)  [get_midas_int "$base/Pulser Low (counts)"]

    set base "/Equipment/MUSR_I_acq/settings/hardware/sweep device"
    # ??? set idef(sweep_device)      [midas get_value "$base/name"]
    set idef(sweep_ins_name)    [midas get_value "$base/camp instrument name"]  ;# Renamed!!  Was "camp path"
    # ???                       [midas get_value "$base/GPIB port or rs232 portname"]
    set idef(sweep_ins_type)    [midas get_value "$base/instrument type"]
    set idef(sweep_camp_var)    [midas get_value "$base/camp scan path"]
    set div         [get_midas_int "$base/setting divide by"]
    set idef(divide_by)         $div
    set idef(init_script)       [midas get_value "$base/initialization script"]
    set idef(tol_test)          [midas get_value "$base/test ready script"]
    #### OMIT:                  [midas get_value "$base/script directory"]
    set idef(max)               [expr { round($div * [midas get_value "$base/maximum"]) }]
    set idef(min)               [expr { round($div * [midas get_value "$base/minimum"]) }]
    set idef(scan_title)        [midas get_value "$base/scan title"]
    set idef(sweep_units)       [midas get_value "$base/scan var units"]

    set base "/Equipment/MUSR_I_acq/settings/input"
    set idef(sweep_device)      [midas get_value "$base/sweep device"]
    set idef(sweep_from)        [expr { round($div * [midas get_value "$base/sweep start"]) }]
    set idef(sweep_to)          [expr { round($div * [midas get_value "$base/sweep stop"]) }]
    set idef(sweep_incr)        [expr { round($div * [midas get_value "$base/sweep step"]) }]
    set idef(sweep_settle)      [get_midas_int "$base/step settling time (ms)"]
    set idef(num_sweeps)        [get_midas_int "$base/num sweeps"]
    set idef(final_point)       [get_midas_int "$base/num points"]
    set mod_suffix              [midas get_data  "$base/inner toggle suffix"]
    set idef(mod1_suffix1)      [lindex $mod_suffix 0]
    set idef(mod1_suffix2)      [lindex $mod_suffix 1]
    set idef(mod_type_1)        [get_midas_String "$base/inner toggle type"]
    set idef(mod_num_1)         [get_midas_int "$base/num inner toggle cycles"]
    set idef(mod_settle_1)      [get_midas_int "$base/inner toggle settling time (ms)"]
    set idef(mod_value_1)       [expr { round($div * [midas get_value "$base/inner toggle value"]) }]
    set mod_suffix              [midas get_data  "$base/outer toggle suffix"]
    set idef(mod2_suffix1)      [lindex $mod_suffix 0]
    set idef(mod2_suffix2)      [lindex $mod_suffix 1]
    set idef(mod_type_2)        [get_midas_String "$base/outer toggle type"]
    set idef(mod_num_2)         [get_midas_int "$base/num outer toggle cycles"]
    set idef(mod_settle_2)      [get_midas_int "$base/outer toggle settling time (ms)"]
    set idef(mod_value_2)       [expr { round($div * [midas get_value "$base/outer toggle value"]) }]
    set idef(num_presets)       [get_midas_int "$base/num presets"]
    set idef(toler_enable)      [midas get_value "$base/tolerance check"]
    set idef(tolerance)         [get_midas_float "$base/tolerance (%)"]
    set idef(toler_delay)       [get_midas_float "$base/out-of-tolerance delay (s)"]
    set idef(toggle_always)     [midas get_value "$base/keep toggling out-of-tol"]
    set idef(renorm_at_begin)   [midas get_value "$base/normalize at begin run"]
    set idef(constant_time)     [midas get_value "$base/constant time"]
    set idef(mod_fast_enable)   [midas get_value "$base/fast modulation"]
    set mod_suffix              [midas get_data  "$base/fast mod suffix"]
    set idef(mod_fast_suffix1)  [lindex $mod_suffix 0]
    set idef(mod_fast_suffix2)  [lindex $mod_suffix 1]
    set idef(norm_e)            [midas get_value "$base/normalize on e rate"]
    set idef(norm_timeout)      [midas get_value "$base/normalization timeout (s)"]

    set base "/Equipment/MUSR_I_acq/settings/imdarc"
    set idef(log_file)          [midas get_value "$base/log file"]
    set idef(num_hist)          [midas get_value "$base/histograms/number defined"]
    set idef(hist_titles)       [lrange [midas get_data  "$base/histograms/titles"] \
            0 [expr {$idef(num_hist)-1}] ]

    set base "/Equipment/Camp/Settings"
    set idef(num_logged)        [get_midas_int "$base/n_var_logged"]
    if { $midas(camp_host) == "" || $midas(camp_host) == "none" } {
        set idef(num_logged) 0
    }
    set n  [expr {$idef(num_logged)-1}]
    set idef(logged_vars)       [lrange [midas get_data  "$base/var_path"] 0 $n ]
    set idef(logged_poll)       [lrange [midas get_data  "$base/polling_interval"] 0 $n ]
 
    return ""
}

proc odb_set_idef { } {
    global idef

    odb_reconnect

    #puts "odb_set_idef"

    set base "/Equipment/MUSR_I_acq/settings/hardware/gate generator"
    midas set_value "$base/down counter preset" $idef(preset_counts)

    set base "/Equipment/MUSR_I_acq/settings/hardware/sweep device"
    # ???    midas set_value "$base/name" $idef(sweep_device)
    midas set_value "$base/camp instrument name"  $idef(sweep_ins_name)   ;# Renamed!!  Was "camp path"
    # ???                       midas set_value "$base/GPIB port or rs232 portname"
    midas set_value "$base/instrument type" $idef(sweep_ins_type)
    midas set_value "$base/camp scan path" $idef(sweep_camp_var)
    if { ![string is integer -strict $idef(divide_by)] || $idef(divide_by)<=0 } { 
	set idef(divide_by) 1
    }
    midas set_value "$base/setting divide by" $idef(divide_by)
    set div [expr { double($idef(divide_by)) }]
    midas set_value "$base/initialization script" $idef(init_script)
    midas set_value "$base/test ready script" $idef(tol_test)
    #### OMIT:                  midas set_value "$base/script directory"
    midas set_value "$base/maximum" [expr {0.0+$idef(max)/$div}]
    midas set_value "$base/minimum" [expr {0.0+$idef(min)/$div}]
    midas set_value "$base/scan title" $idef(scan_title)
    midas set_value "$base/scan var units" $idef(sweep_units)

    set base "/Equipment/MUSR_I_acq/settings/input"
    midas set_value "$base/sweep device" $idef(sweep_device)
    midas set_value "$base/sweep start" [expr {0.0+$idef(sweep_from)/$div}]
    midas set_value "$base/sweep stop" [expr {0.0+$idef(sweep_to)/$div}]
    midas set_value "$base/sweep step" [expr {0.0+$idef(sweep_incr)/$div}]
    midas set_value "$base/step settling time (ms)" $idef(sweep_settle)
    midas set_value "$base/num sweeps" $idef(num_sweeps)
    midas set_value "$base/num points" $idef(final_point)
    midas set_data_array "$base/inner toggle suffix" [list $idef(mod1_suffix1) $idef(mod1_suffix2)]
    midas set_value "$base/inner toggle type" $idef(mod_type_1)
    midas set_value "$base/num inner toggle cycles" $idef(mod_num_1)
    midas set_value "$base/inner toggle settling time (ms)" $idef(mod_settle_1)
    midas set_value "$base/inner toggle value" [expr {0.0+$idef(mod_value_1)/$div}]
    midas set_data_array "$base/outer toggle suffix" [list $idef(mod2_suffix1) $idef(mod2_suffix2)]
    midas set_value "$base/outer toggle type" $idef(mod_type_2)
    midas set_value "$base/num outer toggle cycles" $idef(mod_num_2)
    midas set_value "$base/outer toggle settling time (ms)" $idef(mod_settle_2)
    midas set_value "$base/outer toggle value" [expr {0.0+$idef(mod_value_2)/$div}]
    midas set_value "$base/num presets" $idef(num_presets)
    midas set_value "$base/tolerance check" $idef(toler_enable)
    midas set_value "$base/tolerance (%)" $idef(tolerance)
    midas set_value "$base/out-of-tolerance delay (s)" $idef(toler_delay)

    # A hack because femusr fails to get back in tolerance if doing out-of-tolerance toggles
    # This should be checked to see that recovery does work with Hard modulation
    if { $idef(toler_enable) && $idef(mod_type_1)!="Hard" && $idef(mod_type_2)!="Hard" } {
        set idef(toggle_always) 0
    }
    midas set_value "$base/keep toggling out-of-tol" $idef(toggle_always)

    midas set_value "$base/constant time" $idef(constant_time)
    midas set_value "$base/fast modulation" $idef(mod_fast_enable)
    midas set_data_array "$base/fast mod suffix" [list $idef(mod_fast_suffix1) $idef(mod_fast_suffix2)]
    midas set_value "$base/normalize on e rate" $idef(norm_e)
    midas set_value "$base/normalization timeout (s)" $idef(norm_timeout)
    midas set_value "$base/normalize at begin run" $idef(renorm_at_begin)

    set base "/Equipment/MUSR_I_acq/settings/imdarc"
    midas set_value "$base/log file" $idef(log_file)

    set base "/Equipment/MUSR_I_acq/settings/imdarc/histograms"
    midas set_value "$base/number defined" $idef(num_hist)
    midas set_data_array "$base/titles" $idef(hist_titles)

    set base "/Equipment/Camp/Settings"
    midas set_value "$base/n_var_logged" $idef(num_logged)
    midas set_data_array "$base/var_path" $idef(logged_vars)
    midas set_data_array "$base/polling_interval" $idef(logged_poll)

    set base "/Equipment/MUSR_I_acq/settings"
    midas set_value "$base/config name" $idef(setup_name)
    midas set_value "$base/config path" $idef(setup_path)

    midas save $base [file join $idef(setup_path) $idef(setup_name).idef]

    return ""
}

#  Get scaler readings and histogram totals.  There is no "set".

proc odb_get_stat { } {
    global run rstat rig mode midas

    odb_reconnect
#
    if { $run(ImuSR) } {
	global idef
	# Some parameters are not in the "Variables" section
	set base "/Equipment/MUSR_I_acq/settings/input"
	set idef(num_sweeps) [get_midas_int "$base/num sweeps"]
	set dev [midas get_value "$base/sweep device"]
	if { [string match -nocase Camp $dev] } {
	    set base "/Equipment/MUSR_I_acq/Settings/hardware/sweep device"
	    set dev [midas get_value "$base/camp instrument name"]
	}
	set rstat(sweep_device) $dev

        set base "/Equipment/info_odb/Variables"
	set div $idef(divide_by)
        # If divide_by has changed since the previous run, details displayed for
        # the previous run's sweeps will be bogus.
	set from [expr { round($div * [midas get_value "$base/sweep start"]) }]
	set to [expr { round($div * [midas get_value "$base/sweep stop"]) }]
	set incr [expr { round($div * [midas get_value "$base/sweep step"]) }]
        if { $incr == 0 } { set incr 1 } 
        set rstat(ptspersweep) [expr {1+round( abs(0.0+$to-$from)/($incr+0.0) )}] 
	set rstat(sweep_range_text) "$from to $to step $incr"
        set rstat(sweep_num) [get_midas_int "$base/current sweep num"]
        set rstat(curr_sweep_setting) [expr { round($div * [midas get_value "$base/current sweep setting"] ) }]
        set rstat(actual_setting) [get_midas_float "$base/actual sweep setting"]
        set rstat(curr_point) [get_midas_int "$base/current data point"]
        set rstat(curr_preset) [get_midas_int "$base/current preset"]
        set rstat(curr_inner_toggle) [get_midas_int "$base/current inner toggle"]
        set rstat(curr_outer_toggle) [get_midas_int "$base/current outer toggle"]
        set rstat(total) [get_midas_int "$base/total data points"]
        set rstat(norm_time) [get_midas_int "$base/normalization time (ms)"]
        set rstat(norm_ticks) [get_midas_int "$base/normalization clock ticks"]
        set ntot [get_midas_int "$base/normalization total rate"]
        set rstat(norm_rate) [expr { round(1000*($ntot/($rstat(norm_time)+0.01)))}]
        set rstat(curr_ticks) [get_midas_int "$base/last clock ticks"]
	set rstat(current_rate) [expr { round($rstat(norm_rate) * (double($rstat(norm_ticks))/($rstat(curr_ticks)+0.01))) }]
        #puts "Calc rate: ntime $rstat(norm_time), nticks $rstat(norm_ticks), tot $ntot, cticks $rstat(curr_ticks), crate $rstat(current_rate)"
        set rstat(tolerance) [get_midas_float "$base/tolerance (%)"]
        set rstat(in_tolerance) [midas get_value "$base/in tolerance"]
	set rstat(run_time) [get_midas_float "$base/total run time (s)"]
	set rstat(num_polls) [get_midas_int "$base/no. times polling loop called"]
        set rstat(renormalize) [midas get_value "/Equipment/MUSR_I_Acq/Settings/input/renormalize"]
        #puts "R $rstat(renormalize), P $rstat(num_polls), IT $rstat(in_tolerance), NR $rstat(norm_rate), CR $rstat(current_rate)"
    } else {
        set base "/Equipment/MUSR_TD_acq/mdarc/histograms"
        set run(hist_total) [midas get_value "$base/output/total saved" ]
        # round to integer if possible
        catch { set run(hist_total) [expr {round($run(hist_total))}] }
        set rstat(htotals) [midas get_data "$base/output/histogram totals" ]
        set rstat(nhist) [midas get_value "$base/number defined" ]
        set rstat(hnames) [midas get_data "$base/titles" ]
        #
        set base "/Equipment/MUSR_TD_acq/Settings/rig/scalers"
        set rig(enable_scalers) [midas get_value "$base/enabled"]
        
        if { $rig(enable_scalers) } {
            set rig(num_scalers)   [midas get_value "$base/num inputs"]
            set rig(scaler_names) [midas get_data "$base/titles"]            
            set rstat(srates) [midas get_data "/Equipment/Rscal/Variables/RATE" ]
            set rstat(stots)  [midas get_data "/Equipment/Scaler/Variables/SCLR" ]
        } else {
            set rig(num_scalers) 0
        }
        #
        set v6pars [midas get_data "/Equipment/Diag/Variables/DIAG" ]
        catch {
            set rstat(hit) [lindex $v6pars 3]
            set rstat(inv) [lindex $v6pars 5]
            set rstat(ovf) [lindex $v6pars 7]
            set rstat(mis) [lindex $v6pars 9]
            set rstat(hhits) [lrange $v6pars 11 end]
        }
        #
        set rstat(hist_total) [td_big_number $run(hist_total)]
    }
#
    return ""
}


#  Set enable/disable of weighted Camp averages,
proc odb_set_wt_stat { } {
    global wtstat

    odb_reconnect

    midas set_value "/Equipment/MUSR_TD_acq/mdarc/camp/enable weighted averaging" $wtstat(enabled)
}

#  Get weighted Camp averages, as calculated by mdarc
proc odb_get_wt_stat { } {
    global wtstat

    odb_reconnect

    set n 0
    if { [catch {
        set wtstat(enabled) [midas get_value \
                "/Equipment/MUSR_TD_acq/mdarc/camp/enable weighted averaging"]
    } ] } {
        set wtstat(enabled) 0
    }
    if { $wtstat(enabled) && ![catch {
        set n [midas get_value "/averages/number"]
    } ] && $n > 0 } {
        set wtstat(number) $n
        incr n -1
        set wtstat(variables) [lrange [midas get_data "/averages/variable"] 0 $n]
        set wtstat(mean)    [lrange [midas get_data "/averages/mean"] 0 $n]
        set wtstat(stddev) [lrange [midas get_data "/averages/stddev"] 0 $n]
    } else {
        set wtstat(number) 0
        set wtstat(variables) [list]
        set wtstat(mean)    [list]
        set wtstat(stddev) [list]
    }
    return
}

# We have two routines for setting autorun parameters -- one for the clients and
# one for the autorun server.  They are different because certain parameters are 
# only changed by one or the other.

# This is the client's (gui's) way to set autorun parameters:

proc odb_set_autor_cli { } {
    global autor

    odb_reconnect
#
    set autor(enabled) [lsearch {Disabled Enabled} $autor(oper)]
    set autor(pausing) [lsearch {Disabled Enabled} $autor(pausE)]
    set base "/autorun"
    midas set_value "$base/enable" $autor(enabled)

    #   Only apply state if it is "reload"; otherwise it is a read-only parameter
    if { $autor(enabled) && [string equal $autor(state) "reload" ] } {
        # states are:  disabled idle acquiring paused ending stopped setting changing starting reload
        midas set_value "$base/state" 9
    }
    midas set_value "$base/plan file" [string range $autor(filename) 0 62]
    midas set_value "$base/refresh seconds" $autor(refresh_period)
    midas set_value "$base/target counts" $autor(counts)
    midas set_value "$base/count histogram" $autor(counthist)
    if { $autor(elapsed) > 60 } {
	midas set_value "$base/time limit (minutes)" [format %.1f [expr {$autor(elapsed)/60.0}]]
    } else {
	midas set_value "$base/time limit (minutes)" [format %.2f [expr {$autor(elapsed)/60.0}]]
    }
    midas set_value "$base/enable pausing" $autor(pausing)
    set autor(altered_at) 0
}

# This is the client's (gui's) way to set just the autorun counts parameters:

proc odb_set_autor_counts { } {
    global autor

    odb_reconnect
#
    set base "/autorun"
    midas set_value "$base/target counts" $autor(counts)
    midas set_value "$base/count histogram" $autor(counthist)
}

# This is the autorun server's way of setting autorun parameters:
#
proc odb_set_autor_srv { } {
    global autor

    odb_reconnect
#
    set base "/autorun"
    midas set_value "$base/enable" $autor(enabled)
    set s [lsearch -exact [list \
            disabled idle acquiring paused ending stopped setting changing starting reload] \
            $autor(state)]
    if { $s < 0 } {
        puts "Autorun state `$autor(state)' is unknown!!!!!!!"
        set s 0
    }
    if { !$autor(enabled) } {
        set s 0
    }
    midas set_value "$base/state" $s
#   midas set_value "$base/state label" [lindex [list \
#            disabled idle acquiring paused ending stopped setting changing starting reload] \
#            $s]
    midas set_value "$base/message" [string range $autor(message) 0 127]
    midas set_value "$base/target counts" $autor(counts)
    midas set_value "$base/count histogram" $autor(counthist)
    if { $autor(elapsed) > 60 } {
	midas set_value "$base/time limit (minutes)" [format %.1f [expr {$autor(elapsed)/60.0}]]
    } else {
	midas set_value "$base/time limit (minutes)" [format %.2f [expr {$autor(elapsed)/60.0}]]
    }
}

proc odb_get_autor { } {
    global autor

    odb_reconnect
#
    set base "/autorun"
    set autor(enabled) [get_midas_int "$base/enable"]
    set s [get_midas_int "$base/state"]
    if { $s < 0 || $s > 9 } { set s 0 }
    set autor(state) [lindex [list \
            disabled idle acquiring paused ending stopped setting changing starting reload] \
            $s]
    set autor(message) [midas get_value "$base/message"]
    set autor(filename) [midas get_value "$base/plan file"]
    set autor(refresh_period) [midas get_value "$base/refresh seconds"]
    set autor(counts) [midas get_value "$base/target counts"]
    set autor(counthist) 0
    catch { set autor(counthist) [midas get_value "$base/count histogram"] }
    set autor(timelimit) [midas get_value "$base/time limit (minutes)"]
    set autor(elapsed) [expr { round(60.0*$autor(timelimit)) }]
    set autor(pausing) [midas get_value "$base/enable pausing"]
    catch {
        set autor(oper) [lindex {Disabled Enabled} $autor(enabled)]
        set autor(pausE) [lindex {Disabled Enabled} $autor(pausing)]
    }
}


#
#  Tell midas to load file into odb

proc odb_load_file { f } {

    odb_reconnect

    midas load $f

    return ""
}

proc odb_begin_run { } {
    global run midas 

    odb_get_general
    odb_get_runinfo

    if { $run(in_progress) } { 
        return -code error "A run is already in progress"
    }

    midas set_data "/Equipment/Diag/Variables/DIAG" 0 15 0

    set pa [midas get_value "/Equipment/MUSR_TD_acq/mdarc/endrun_purge_and_archive"]
    if { ! ( $run(test_mode) || $pa ) } {
	midas set_value /Equipment/MUSR_TD_acq/mdarc/endrun_purge_and_archive 1
	midas msg 2 musrrc "Enable end-run purge and archive when starting a real run."
    }

#   Tell Midas to determine the run number

    odb_get_runnum

# Again, try to start run asynchronously; seems to work this time.
# Another way of running asynchronously is to open the odb program as a pipe
# to read from.  With that method we could read and parse the output, and
# schedule the reading with [fileevent] rather than [after].  But best is
# to modify mtcl so "midas start" allows the Tk event loop to run.

    set run(starting) 1
    set run(bg_start_out) {}
    set run(state_label) "Starting"
    after 10 [list blt::bgexec run(bg_start_stat) -output run(bg_start_out) \
		  odbedit -h $midas(host) -e $midas(expt) -c "start now" &]

#    midas set_debug 1
#    midas start now
#    midas set_debug 0

    set base "/Equipment/MUSR_TD_acq/mdarc"
    midas set_value "$base/time_of_last_save" {}
    midas set_value "$base/last_saved_filename" {}

    return ""
}

proc odb_get_runnum { } {
    uplevel \#0 {
        #puts "midas get_RunNum $run(runtype) $midas(expt) $midas(beamline) $midas(scriptpath)"
        midas get_RunNum $run(runtype) $midas(expt) $midas(beamline) $midas(scriptpath)
        odb_get_runinfo
    }
}


proc odb_cancel_transition { } {
    global run

    odb_reconnect
#
    set base "/Runinfo"
    set run(transition) 0
    midas set_value "$base/Transition in progress" $run(transition)
}


proc odb_end_run { } {
    odb_reconnect
    global run midas
    set run(did_stop_at) [expr {[clock seconds] + 10}]
    #set code [catch {midas stop now} result]
    #puts "Command 'midas stop now' gave result $code: $result"

    set code [catch {exec odbedit -h $midas(host) -e $midas(expt) -c "stop"} result]
    #puts "Command 'odbedit stop' gave result $code: $result"

    return ""
}

#   Trying to kill a run too soon causes errors, so try to kill
#   asynchronously when run is fully started.  The "midas kill"
#   operation is done in a separate shell because it can freeze 
#   the acquisition.

proc odb_kill_run { } {
    global run midas

    odb_get_runinfo

    if { !$run(transition) && !$run(in_progress) } {
        return -code error "No run to kill"
    }

    set run(did_stop_at) [expr {[clock seconds] + 10}]
    set run(state_label) "Killing"
    after 100 "asynch_kill_run 15"

    return ""
}

#  If run is still being started, killing it would fail.  Wait until
#  run is fully started before killing.

proc asynch_kill_run { iter } {
    global run midas

    odb_get_runinfo

    set run(did_stop_at) [expr {[clock seconds] + 10}]
    incr iter -1
    if { $iter <= 0 || $run(transition) == 0 } {
        if { $run(transition) } { odb_cancel_transition }
        if { $run(in_progress) } {
            if { !$run(paused) } { catch { midas pause suppress } }
            if { [catch {midas kill $midas(expt) $run(number) $midas(beamline) $midas(scriptpath)} ] } {
                midas stop
            }
        }
    } else {
        after 1000 "asynch_kill_run $iter"
    }
}

#   Here we end a run without final save, and without cleaning up anything.
#   Used when things are broken, or when experimenters poisoned their data,
#   but can use a previous saved version.

proc odb_abrupt_stop { } {
    global run midas
    odb_reconnect
    midas set_value "/Equipment/MUSR_TD_acq/mdarc/suppress_save_temporarily" 1
    set pa [midas get_value "/Equipment/MUSR_TD_acq/mdarc/endrun_purge_and_archive"]
    set err [catch {
        midas set_value /Equipment/MUSR_TD_acq/mdarc/endrun_purge_and_archive 0
        midas stop now
    } msg ]
    midas set_value /Equipment/MUSR_TD_acq/mdarc/endrun_purge_and_archive $pa  
    if { $err } {
        return -code error $msg
    }
}

proc odb_pause_run { } {
    global run midas

    odb_reconnect
#   Tests are disabled -- use the error return from midas.
#    odb_get_runinfo
#    if { $run(in_progress) } {
#        if { $run(paused) } { 
#           return -code error "Run is already paused"
#        } else {
            midas pause 
#        }
#    } else {
#       return -code error "No run is in progress"
#    }

    return ""
}

proc odb_resume_run { } {
    global run midas

    odb_reconnect 
#   No tests -- use the error return from midas.
#    odb_get_runinfo
#    if { $run(in_progress) } {
#        if { $run(paused) } { 
#            midas resume
    exec odb -c resume &
#        } else {
#           return -code error "Run is not paused"
#        }
#    } else {
#       return -code error "No run is in progress"
#    }

    return ""
}

proc odb_save_data { } {
    global run

#   Since there is no error return from midas hot_save, we will perform some 
#   explicit tests.  However, it is good that this is silently ignored if the
#   data was just saved recently!
    odb_get_runinfo
    if { $run(in_progress) } {
        if { $run(paused) } { 
            return -code error "Run is paused - there is no new data"
        } else {
            midas hot_save
        }
    } else {
        return -code error "No run is in progress"
    }
    return ""
}

#   change the run type: parameter "real" or "test"

proc odb_set_run_type { type } {
    global run midas 

    if { [lsearch -exact [list real test] $type] < 0 } {
        return -code error "Run type \"$type\" not recognized; should be real or test."
    }

    odb_get_general
    odb_get_runinfo

#   A paused run needs to be resumed in order change its type.  Is there a
#   way to disable events during that time?

    if { [string compare $run(runtype) $type] } {

        if { $run(in_progress) } {
            after 2000 "midas hot_save ; odb_finish_run_type {$run(last_file)} {$run(paused)}"
            if { $run(paused) } {
                midas resume
            }
            midas toggle $midas(expt) $midas(beamline) $midas(scriptpath)
        } else {
            midas get_RunNum $type $midas(expt) $midas(beamline) $midas(scriptpath)
        }
    }
    after 1000 odb_get_general
    return ""
}


#   Wait for run to be saved under a new file name. then perhaps, pause it.
proc odb_finish_run_type { last_file paused } {
    global run midas 

    set finished 0
    catch {
        odb_get_runinfo
        if { [string compare $last_file $run(last_file)] } {
            set finished 1
            if { $paused } {
                midas pause
            }
        }
    }
    if { !$finished } {
        after 2000 "odb_finish_run_type {$last_file} {$paused}"
    }
}

proc odb_renormalize { } {
    odb_reconnect
    midas set_value "/Equipment/MUSR_I_Acq/Settings/input/renormalize" 1
}

proc odb_zero_hists { } {
    odb_reconnect
    midas set_value "/Equipment/MUSR_TD_acq/v680/commands/zero run" 1
}

proc odb_zero_ref_hists { } {
    odb_reconnect
    midas set_value "/Equipment/MUSR_TD_acq/v680/commands/zero some" 65280
}

proc odb_zero_one_hist { hist } {
    global rig mode 

    odb_get_rig
    odb_get_mode

    if { $hist > $mode(numsel) } {
        # reference hist
        set add 8
        set ctr [expr $hist - $mode(numsel)]
    } else {
        # sample hist
        set add 0
        set ctr $hist
    }
    set chan [expr { [lindex $rig(TDC_in) $ctr] + $add } ]
    set bits [ expr { round( pow(2.0,$chan) ) } ]
    midas set_value "/Equipment/MUSR_TD_acq/v680/commands/zero some" $bits
}

proc odb_zero_scalers { } {
    odb_reconnect
    midas set_value "/Equipment/MUSR_TD_acq/v680/commands/zero scaler" 1
}

proc odb_hmdisplay { yn } {
    odb_reconnect
    midas set_value "/Equipment/MUSR_TD_acq/v680/display" $yn
}

proc odb_tdc_clear { } {
    odb_reconnect
    midas set_value "/Equipment/MUSR_TD_acq/v680/commands/clear clock hang" 1
}

set run(starting) 0
set run(did_stop_at) [clock seconds]
array set autor {
    enabled 0
    oper Disabled
    pausing 0
    pausE Disabled
    altered_at 0
}

