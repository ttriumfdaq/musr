#   popsel.tcl      Mar 26 2002     Donald Arseneau
#
#   A tcl/tk pop-up window to select a value or values from a
#   multi-column listbox.  Value can also be entered in an entry
#   widget.  The intent is to make something that looks a lot
#   like tk_OpenFile, but taking a specified list of values,
#   and without the directory navigation.  Maybe I should have
#   used a plain listbox.
#
#   The implementation is a row of listboxes to make a 4-column
#   listing of values.  That may be a poor approach, but I just
#   did it that way rather than figuring out which method would
#   be the best.  (Other possibilities:  A canvas with names
#   arranged nicely, and with bindings; A grid of (flat) radio
#   buttons / check boxes....)
#
#   Usage:
#   popup_selector window_title text_blurb \
#                  list_of_names default_name \
#                  multiple_or_single prefer_old timeout
#
#   Returns the selection (or a list of selections when "multiple"
#   is specified) which is also the contents of the entry box.
#   OR, if user says [Cancel] or time expires, then return a null
#   string.
#
#   $Log: popsel.tcl,v $
#   Revision 1.3  2015/09/28 00:40:59  asnd
#   Double-click selection
#
#   Revision 1.2  2006/05/26 16:59:52  asnd
#   Accumulated changes over winter shutdown.
#
#   Revision 1.1  2002/06/12 01:26:39  asnd
#   Starting versions of Mui files.
#
#

event add <<ListBoxSelect>> <ButtonRelease> <KeyRelease>

proc popup_selector { title blurb names dflt multi prefer_old timeout } {

#   Get unique identifier for this instance
    set id 0
    while { [ winfo exists .popsel$id ] } {
        incr id
    }
    global popselres$id

    set popselres$id $dflt

    set popw [toplevel .popsel$id]

#   Use "single" mode by default; "multiple" only when specified
    set multi [string tolower $multi]
    if { ![string equal "multiple" $multi] } {
        set multi "single"
    }

#   determine column widths
    set numn [llength $names]
    set maxl 6
    foreach n $names {
        set l [string length $n]
        if { $l > $maxl } { set maxl $l }
    }
    incr maxl

#   determine number of rows; at least 5.
#   There are 4 columns; determine how many are filled.
    set rows 5
    set cols 4
    if { $rows * $cols < $numn } {
        set rows [ expr { ($numn + $cols - 1) / $cols } ]
        set nfilled $cols
    } else {
        set nfilled [ expr { ($numn + $rows - 1) / $rows } ]
    }

#   Insert code to line-break the label text when it is long.

#   Declare most widgets
    frame $popw.menframe -borderwidth 2 -relief sunken
    frame $popw.buttonframe
    label $popw.blurb -anchor w -justify left -padx 0 -pady 4 -text $blurb
    entry $popw.entry -background "#ececec" -textvariable popselres$id -width 30 \
            -highlightcolor "#d90000"
    button $popw.ok -highlightcolor "#d90000" -text OK -width 6 \
            -command "popsel_ok $id"
    button $popw.clear -highlightcolor "#d90000" -text Clear -width 6 \
            -command "popsel_clear $id"
    button $popw.cancel -highlightcolor "#d90000" -text Cancel -width 6 \
            -command "popsel_clear $id ; popsel_ok $id"

#   try to print the text label (blurb) in a bigger font
    catch {
        array set fops [font actual [ $popw.blurb cget -font] ]
        set fsiz [expr {round( 1.2*$fops(-size) + 0.5 )} ]
        $popw.blurb configure -font "$fops(-family) $fsiz $fops(-weight)"
    }

#   Declare list boxes
    foreach c [list 1 2 3 4] {
        listbox $popw.lb$c -height $rows -width $maxl -selectmode $multi \
                -exportselection false -takefocus 1 \
                -relief flat -borderwidth 0 -highlightthickness 0 \
                -selectbackground "#cc99ff" -selectborderwidth 0
        if { $c > $nfilled } {
            $popw.lb$c configure -width 4 -takefocus 0
        }
        if { ![string equal $multi multiple] } {
            bind $popw.lb$c <Double-Button-1> [list after 5 "popsel_ok $id"]
        }
    }

#   Fill in list boxes
    set c 1
    set r 1
    foreach n $names {
        $popw.lb$c insert end $n
        incr r
        if { $r > $rows } {
            set r 1
            incr c
        }
    }

#   Grid packing.  (I used spectcl for layout, so this is verbose)
    grid $popw.menframe -in $popw -row 3 -column 2  -padx 1 -sticky ew
    grid $popw.buttonframe -in $popw -row 5 -column 2 -sticky ew
    grid $popw.blurb -in $popw -row 2 -column 2 -sticky ew
    grid $popw.lb1 -in $popw.menframe -row 1 -column 1 -padx 4 -pady 4 -sticky nesw
    grid $popw.lb2 -in $popw.menframe -row 1 -column 2 -padx 4 -pady 4 -sticky nesw
    grid $popw.lb3 -in $popw.menframe -row 1 -column 3 -padx 4 -pady 4 -sticky nesw
    grid $popw.lb4 -in $popw.menframe -row 1 -column 4 -padx 4 -pady 4 -sticky nesw
    grid $popw.entry -in $popw -row 4 -column 2 -pady 8 -sticky ew
    grid $popw.ok -in $popw.buttonframe -row 1 -column 1 
    grid $popw.clear -in $popw.buttonframe -row 1 -column 2 
    grid $popw.cancel -in $popw.buttonframe -row 1 -column 3 
    
    #   Resize behavior management; more verbose
    grid rowconfigure $popw.buttonframe 1 -weight 0 -minsize 30 -pad 0
    grid columnconfigure $popw.buttonframe 1 -weight 0 -minsize 30 -pad 0
    grid columnconfigure $popw.buttonframe 2 -weight 1 -minsize 30 -pad 0
    grid columnconfigure $popw.buttonframe 3 -weight 0 -minsize 30 -pad 0
    
    grid rowconfigure $popw 1 -weight 0 -minsize 15 -pad 0
    grid rowconfigure $popw 2 -weight 0 -minsize 2 -pad 0
    grid rowconfigure $popw 3 -weight 0 -minsize 30 -pad 0
    grid rowconfigure $popw 4 -weight 0 -minsize 30 -pad 0
    grid rowconfigure $popw 5 -weight 0 -minsize 30 -pad 0
    grid rowconfigure $popw 6 -weight 0 -minsize 15 -pad 0
    grid columnconfigure $popw 1 -weight 0 -minsize 15 -pad 0
    grid columnconfigure $popw 2 -weight 0 -minsize 204 -pad 0
    grid columnconfigure $popw 3 -weight 0 -minsize 16 -pad 0
    
    grid rowconfigure $popw.menframe 1 -weight 0 -minsize 30 -pad 0
    grid columnconfigure $popw.menframe 1 -weight 1 -minsize 30 -pad 0
    grid columnconfigure $popw.menframe 2 -weight 1 -minsize 30 -pad 0
    grid columnconfigure $popw.menframe 3 -weight 1 -minsize 30 -pad 0
    grid columnconfigure $popw.menframe 4 -weight 1 -minsize 30 -pad 0

#   Window title
    tkwait visibility $popw
    wm title $popw $title

#   Bind keys
#   Selecting from lists: apply selection; arrow keys navigate.
    foreach c [list 1 2 3 4] {
        bind $popw.lb$c <<ListBoxSelect>> "popsel_change $id $multi $c"
        bind $popw.lb$c <Return> "%W selection set active; popsel_change $id $multi $c ; focus $popw.ok"
        bind $popw.lb$c <Right> "popsel_nav_lb $id $c $nfilled 1"
        bind $popw.lb$c <Left>  "popsel_nav_lb $id $c $nfilled -1"
    }
#   Tab in entry box: 
#   Completion when boolean $prefer_old is true, indicating that entry is 
#   expected from the list; then if complete, focus to [OK] button.
#   Otherwise, default binding (focus to [OK])
    if { $prefer_old } {
        bind $popw.entry <Tab> "popsel_complete $id [list $names]; break"
    }
    bind $popw.entry <Return> [list focus $popw.ok]
    bind $popw.entry <Down> [list focus $popw.ok]
    bind $popw.entry <Up> [list focus $popw.lb1]

    bind $popw.ok <Return> "{$popw.ok} invoke ; break"
    bind $popw.cancel <Return> "{$popw.cancel} invoke ; break"
    bind $popw.clear <Return> "{$popw.clear} invoke ; break"

    bind $popw.ok <Up> [list focus $popw.entry]
    bind $popw.cancel <Up> [list focus $popw.entry]
    bind $popw.clear <Up> [list focus $popw.entry]

    bind $popw.ok <Down> [list focus $popw.lb1]
    bind $popw.cancel <Down> [list focus $popw.lb1]
    bind $popw.clear <Down> [list focus $popw.lb1]

    bind $popw.ok <Right> [list focus $popw.clear]
    bind $popw.cancel <Right> [list focus $popw.lb1]
    bind $popw.clear <Right> [list focus $popw.cancel]

    bind $popw.ok <Left> [list focus $popw.entry]
    bind $popw.cancel <Left> [list focus $popw.clear]
    bind $popw.clear <Left> [list focus $popw.ok]

#   Set timeout; default is never (0)
    set t 0.0
    scan $timeout {%f} t
    set ms [expr int( $t * 1000.0 )]
    if { $ms > 100 } {
        set cancelid [after $ms "popsel_clear $id; popsel_ok $id"]
    } else {
        set cancelid popsel_none
    }

#   Put initial keyboard focus in text-entry box
    focus $popw.entry
    $popw.entry icursor end

#   Wait for user selection
    tkwait window $popw

#   clean up and return selection value
    after cancel $cancelid
    set n [ set popselres$id ]
    unset popselres$id
    return $n
}

proc popsel_clear { id } {
    global popselres$id

    set popselres$id {}
    catch {
        .popsel$id.lb1 selection clear 0 end
        .popsel$id.lb2 selection clear 0 end
        .popsel$id.lb3 selection clear 0 end
        .popsel$id.lb4 selection clear 0 end
        set popselres$id {}
    }
}

#   Tab-completion for entry box.  This is only for a single value
#   of course, not a list of selected values!
proc popsel_complete { id names } {
    global popselres$id

    set val [ set popselres$id ]
    set nm 0
    set mat $val
    set lv [expr [string length $val ] - 1]
    foreach name $names {
        if { ![string compare $val [string range $name 0 $lv] ] } {
            if { $nm } {
                # not unique completion; shorten to shared match
                while { ! [string match "$mat*" $name ] } {
                    if { [incr lm -1] < 0 } {
                        set mat ""
                        break 
                    }
                    set mat [string range $mat 0 $lm]
                }
            } else {
                # first (perhaps unique) completion
                set mat $name
                set lm [expr [string length $mat ] - 1]
            }
            incr nm
        }
    }
    if { $nm == 1 } {
        focus .popsel$id.ok
    } else {
        beep
    }
    set popselres$id $mat
    .popsel$id.entry icursor end
    return
}



#   Destroying the popup window triggers return of the value!
proc popsel_ok { id } {
    destroy .popsel$id
}

#   This is invoked whever the user changes the listbox selections
#   (button-release, ...)
proc popsel_change { id m w } {
    global popselres$id

    set n {}
    switch $m {
        multiple {
            foreach c [list 1 2 3 4] {
                foreach i [ .popsel$id.lb$c curselection ] {
                    lappend n [ .popsel$id.lb$c get $i ]
                }
            }
        }
        single {
            foreach c [list 1 2 3 4] {
                if { $c == $w } {
                    set i [ .popsel$id.lb$c curselection ]
                    if { [string length $i] } {
                        set n [ .popsel$id.lb$c get $i ]
                    }
                } else {
                    .popsel$id.lb$c selection clear 0 end
                }
            }
        }
    }
    set popselres$id $n
    .popsel$id.entry icursor end
}

proc popsel_nav_lb { id c max ch } {
    set n [expr $c + $ch ]
    if { $n < 1 } { set n 1 }
    if { $n > $max } { set n $max }
    set a [ .popsel$id.lb$c index active ]
    focus .popsel$id.lb$n 
    .popsel$id.lb$n activate $a
}


