#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
if { [catch { set mui_utils_loaded }] } {
    source "mui_utils.tcl"
}
# interface generated by SpecTcl version 1.2 from /home/asnd/SpecTcl/mui/presets.ui
#   root     is the parent window for this user interface

proc presets_ui {root args} {

	# this treats "." as a special case

	if {$root == "."} {
	    set base ""
	} else {
	    set base $root
	}
    
	frame $base.frame#1 \
		-borderwidth 1 \
		-height 2 \
		-relief sunken

	frame $base.frame#2 \
		-borderwidth 1 \
		-height 2 \
		-relief sunken

	label $base.preset_head_l \
		-borderwidth 3 \
		-text {Preset Counts Configuration}
	catch {
		$base.preset_head_l configure \
			-font muTitleFont
	}

	label $base.preset_const_l \
		-justify left \
		-text {Preset scaler will count data
events or clock ticks}

	label $base.preset_source_l \
		-justify left \
		-text {Preset scaler will count muons
or total positrons}

	radiobutton $base.preset_const_counts_rb \
		-command idef_changed \
		-text {Constant Counts} \
		-value 0 \
		-variable midef(constant_time)

	radiobutton $base.preset_norm_e_rb \
		-command idef_changed \
		-text {Normalize on total e} \
		-value 1 \
		-variable midef(norm_e)

	radiobutton $base.preset_const_time_rb \
		-command idef_changed \
		-text {Constant Time} \
		-value 1 \
		-variable midef(constant_time)

	radiobutton $base.preset_norm_mu_rb \
		-command idef_changed \
		-text {Normalize on �} \
		-value 0 \
		-variable midef(norm_e)

	label $base.preset_num_l \
		-text {Number of presets: }

	entry $base.preset_num_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(num_presets) \
		-validate all \
		-validatecommand "valmidefNum d 1 100 %d %s %P $base.preset_num_e %v %V num_presets" \
		-width 10

	label $base.preset_set_l \
		-text {Preset counts:}

	entry $base.preset_set_e \
		-background $PREF::entrybg \
		-cursor {} \
		-takefocus 1 \
		-textvariable midef(preset_counts) \
		-validate all \
		-validatecommand "valmidefNum f 0 1.0e9 %d %s %P $base.preset_set_e %v %V preset_counts" \
		-width 10


	# Add contents to menus

	# Geometry management

	grid $base.frame#1 -in $root	-row 2 -column 2  \
		-columnspan 5 \
		-pady 10 \
		-sticky ew
	grid $base.frame#2 -in $root	-row 6 -column 2  \
		-columnspan 5 \
		-pady 10 \
		-sticky ew
	grid $base.preset_head_l -in $root	-row 1 -column 2  \
		-columnspan 5 \
		-ipadx 10 \
		-ipady 7 \
		-pady 3 \
		-sticky ew
	grid $base.preset_const_l -in $root	-row 3 -column 2  \
		-columnspan 2 \
		-pady 3 \
		-sticky w
	grid $base.preset_source_l -in $root	-row 3 -column 5  \
		-columnspan 2 \
		-pady 3 \
		-sticky w
	grid $base.preset_const_counts_rb -in $root	-row 4 -column 2  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_norm_e_rb -in $root	-row 4 -column 5  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_const_time_rb -in $root	-row 5 -column 2  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_norm_mu_rb -in $root	-row 5 -column 5  \
		-columnspan 2 \
		-sticky w
	grid $base.preset_num_l -in $root	-row 7 -column 3  \
		-pady 2 \
		-sticky w
	grid $base.preset_num_e -in $root	-row 7 -column 5  \
		-pady 2 \
		-sticky ew
	grid $base.preset_set_l -in $root	-row 8 -column 3  \
		-pady 2 \
		-sticky w
	grid $base.preset_set_e -in $root	-row 8 -column 5  \
		-pady 2 \
		-sticky ew

	# Resize behavior management

	grid rowconfigure $root 1 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 2 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 3 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 5 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 6 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 7 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 8 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 9 -weight 0 -minsize 44 -pad 0
	grid columnconfigure $root 1 -weight 0 -minsize 12 -pad 0
	grid columnconfigure $root 2 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 3 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $root 4 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $root 5 -weight 0 -minsize 22 -pad 0
	grid columnconfigure $root 6 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 7 -weight 0 -minsize 12 -pad 0
# additional interface code

# end additional interface code

}


# Allow interface to be run "stand-alone" for testing

catch {
    if [info exists embed_args] {
	# we are running in the plugin
	presets_ui .
    } else {
	# we are running in stand-alone mode
	if {$argv0 == [info script]} {
	    wm title . "Testing presets_ui"
	    presets_ui .
	}
    }
}
