# print_dial.tcl
# Procedures for the (small) pop-up print dialog, for selecting
# the print queue or saving to a file.
#
# Options    Default values
# -text      "Print or Save to File"
# -parent    ""
# -timeout   ""  (none)
# -file      <Previous file name> or ""
# -queue     <Previous queue name> or $env(PRINTER) or ""
# -savecommand   -- no default --
# -printcommand  -- no default --
#
# Warning: only minimal checking if args are valid!
# 
# Note that print_dialog opens the dialog window and returns immediately,
# but uses the -savecommand or -printcommand call-back when finished
# (but not cancelled).  For convenience, both the queue name and file
# name are appended to each callback command; i.e., -printcommand foo
# means foo was defined thus: "proc foo { queue file } { ..."
#
# The window layout was originally done by specTcl -- see printpopup.ui
#
#   $Log: print_dial.tcl,v $
#   Revision 1.1  2002/09/21 04:44:41  asnd
#   Plotting Camp variables
#
#
proc print_dialog { args } {
    global env
    # Get options values
    set printQ {}
    if { [info exists env(PRINTER)] } {
        set printQ $env(PRINTER)
    }
    array set opts [list -text {Print or Save to File} \
            -parent {} -timeout {} -file {} -queue $printQ ]
    array set opts $print_dialog::previous
    array set opts $args
    #
    set pdnum [incr print_dialog::number]
    if { [string length $opts(-parent)] && [winfo exists $opts(-parent)] } {
        set pdwin [toplevel $opts(-parent).printDial$pdnum]
        wm transient $pdwin $opts(-parent)
    } else {
        set pdwin [toplevel .printDial$pdnum]
        wm transient $pdwin
    }

    wm title $pdwin $opts(-text)
    if { ![catch {expr {10+$opts(-timeout)}} ] } {
        after $opts(-timeout) [list destroy $pdwin]
    }

    label $pdwin.title -text $opts(-text)

    label $pdwin.queue_label -text {Print Queue:}
    entry $pdwin.queue_name_e -cursor {} -width 25
    $pdwin.queue_name_e insert 0 $opts(-queue)
    button $pdwin.print_button \
            -image small_printer -padx 2 -pady 2 \
            -command [list print_dialog::action $pdwin $opts(-printcommand)]

    label $pdwin.file_label -text {File Name:}
    entry $pdwin.file_name_e -cursor {} -width 25
    $pdwin.file_name_e insert 0 $opts(-file)
    button $pdwin.save_button \
            -image small_folder -padx 2 -pady 2 \
            -command [list print_dialog::action $pdwin $opts(-savecommand)]

    button $pdwin.cancel_button \
            -padx 2 -pady 2 -text Cancel -underline 0 \
            -command "after idle [list destroy $pdwin]"

    grid $pdwin.title -in $pdwin -row 2 -column 2 -columnspan 3 -pady 8
    grid $pdwin.queue_label -in $pdwin -row 3 -column 2 -sticky w
    grid $pdwin.queue_name_e -in $pdwin -row 3 -column 3 
    grid $pdwin.print_button -in $pdwin -row 3 -column 4 -ipadx 2 -ipady 2 \
            -padx 1 -pady 1 -sticky nesw
    grid $pdwin.file_label -in $pdwin -row 4 -column 2 -sticky w
    grid $pdwin.file_name_e -in $pdwin -row 4 -column 3 
    grid $pdwin.save_button -in $pdwin -row 4 -column 4 -ipadx 2 -ipady 2 \
            -padx 1 -pady 1 -sticky nesw
    grid $pdwin.cancel_button -in $pdwin -row 5 -column 2 -columnspan 3 \
            -ipadx 2 -ipady 2 -padx 1 -pady 8

    grid rowconfigure $pdwin 1 -weight 0 -minsize 6 -pad 0
    grid rowconfigure $pdwin 6 -weight 0 -minsize 6 -pad 0
    grid columnconfigure $pdwin 1 -weight 0 -minsize 12 -pad 0
    grid columnconfigure $pdwin 5 -weight 0 -minsize 12 -pad 0
#
    bind $pdwin <Alt-c> "after idle [list destroy $pdwin]"
    bind $pdwin.queue_name_e <Return> [list $pdwin.print_button invoke]
    bind $pdwin.file_name_e <Return> [list $pdwin.save_button invoke]

    centerWinonWin $pdwin [winfo parent $pdwin]
}

namespace eval print_dialog {
    variable previous {} 
    variable number 0
    proc action {win cmd} {
        if { ![winfo exists $win] } { return }
        set p [list]
        set f [$win.file_name_e get]
        if { [string length $f] } {
            set p [list -file $f]
        }
        set q [$win.queue_name_e get]
        if { [string length $q] } {
            lappend p -queue $q
        }
        set print_dialog::previous $p
        eval [lappend cmd $q $f]
        after idle [list destroy $win]
    }
}
