#   pulser_extra.tcl
#   Ancilliary procedures for the GGL pulser settings.

#   $Log: pulser_extra.tcl,v $
#   Revision 1.2  2006/05/26 16:59:52  asnd
#   Accumulated changes over winter shutdown.
#
#   Revision 1.1  2003/09/19 06:23:27  asnd
#   ImuSR and many other revisions.
#

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
    set puwin ""
}

proc pulser_initialize { } {

    global puwin mpulser idef

    set puw [td_win_name $puwin]
    if { [ string length $puwin ] } {
        # We are in a real application, where the setup window is not main
        if { [winfo exists $puwin.pu_OK_b] } {
            # if window exists, make it visible
            wm deiconify $puw
            raise $puw
        } else {
            # window is not built yet.  Build main window
            toplevel $puw
            pulser_ui $puwin
        }
    }

    wm title $puw "Pulser Set"

    pu_bind_help pu_apply_b  "Apply changed settings"
    pu_bind_help pu_cancel_b "Close window; abandon changes"
    pu_bind_help pu_OK_b  "Apply changes and close window"
    pu_bind_help pu_freq_e  "Frequency of pulses"
    pu_bind_help pu_duty_e  {Percentage of time output is "on"}
    pu_bind_help pu_ontime_e  "Duration of each pulse"
    pu_bind_help pu_offtime_e  "Duration of space between pulses"

#   Unlike with menu-buttons, regular buttons with -underline still need to 
#   have an alt-key binding added explicitly.
    bind $puw <Alt-a> "$puwin.pu_apply_b invoke ; break"
    bind $puw <Alt-q> "$puwin.pu_cancel_b invoke ; break"
    bind $puw <Alt-o> "$puwin.pu_OK_b invoke ; break"

    # Un-do a bad spectcl binding
    bind $puw <Destroy> {}

    # Make Bindings to synchronize values
    bind $puwin.pu_freq_e <FocusOut> {after 1 {pu_propagate freq}}
    bind $puwin.pu_duty_e <FocusOut> {after 1 {pu_propagate freq}}
    bind $puwin.pu_ontime_e <FocusOut> {after 1 {pu_propagate time}}
    bind $puwin.pu_offtime_e <FocusOut> {after 1 {pu_propagate time}}

    bind $puwin.pu_freq_e <Key-Return> {after 1 {pu_propagate freq}}
    bind $puwin.pu_duty_e <Key-Return> {after 1 {pu_propagate freq}}
    bind $puwin.pu_ontime_e <Key-Return> {after 1 {pu_propagate time}}
    bind $puwin.pu_offtime_e <Key-Return> {after 1 {pu_propagate time}}

#    bind $puwin.pu_freq_e <FocusOut> {puts "Out of %W"}
#    bind $puwin.pu_duty_e <FocusOut> {puts "Out of %W"}
#    bind $puwin.pu_ontime_e <FocusOut> {puts "Out of %W"}
#    bind $puwin.pu_offtime_e <FocusOut> {puts "Out of %W"}

    catch {
        odb_get_idef
        set mpulser(ontime) [expr {$idef(pulser_ontime)*0.0001}]
        set mpulser(offtime) [expr {$idef(pulser_offtime)*0.0001}]
        set mpulser(enable) $idef(pulser_enable)
        pu_propagate time
    }
}

#   Propagate entry values to others.  
proc pu_propagate { what } {
    global puwin mpulser
    # puts "Propagating $what"
    switch -- $what {
        freq {
            set freq [bounded $mpulser(freq) 0.77 2500.0]
            set duty [bounded $mpulser(duty) 0.01 99.9999]
            set ontime [expr {(1.0/$freq)*($duty/100.0)}]
            set offtime [expr {(1.0/$freq)-$ontime}]
        }
        time {
            set ontime [bounded $mpulser(ontime) 0.0002 6.55]
            set offtime [bounded $mpulser(offtime) 0.0002 6.55]
        }
    }
    set mpulser(ontime) [expr {[ format %.4f $ontime ]}]
    set mpulser(offtime) [expr {[ format %.4f $offtime ]}]
    set mpulser(freq) [expr {[ format %.4f [expr { 1.0 / ($ontime + $offtime) }] ]}]
    set mpulser(duty) [expr {[ format %.4f [expr { 100.0 * $ontime / ($ontime + $offtime) }] ]}]
}

proc pu_apply {  } {
    global puwin mpulser idef

    pu_propagate time
    set idef(pulser_ontime) [expr {round($mpulser(ontime)*10000.0)}]
    set idef(pulser_offtime) [expr {round($mpulser(offtime)*10000.0)}]
    set idef(pulser_enable) $mpulser(enable)
    odb_set_idef
}



#  THE HELP LINE:
proc pu_bind_help { w h } {
    global puwin
    bind $puwin.$w <Enter> [list set pulser_help_text "$h"]
    bind $puwin.$w <FocusIn> [list set pulser_help_text "$h"]
    bind $puwin.$w <Leave> [list set pulser_help_text {}]
    bind $puwin.$w <FocusOut> [list set pulser_help_text {}]
}

#   Apply any changes in the pulser set window, and close it.
proc pu_OK { } {
    pu_apply
    pu_cancel
}

#   Quit the pulser set window, abandoning any changes
proc pu_cancel { } {
    global puwin
    destroy [td_win_name $puwin]
}


if { $puwin == "" } {
    # Testing, so provide some values
    set idef(pulser_enable) 1
    set idef(pulser_ontime) 0.1
    set idef(pulser_ontime) 0.25
    # stand-alone testing, so start immediately!
    pulser_initialize
}

