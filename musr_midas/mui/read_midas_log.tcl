# Read the midas.log file to reconstruct acquisition record.
# This is done in two possible ways:
# 1) all-at-once -- done at start-up to populate an acquisition record
#    for the stripchart, held in global variable lv_acq_record.
# 2) continuous -- As a replacement for connecting to midas, track 
#    updates to midas.log to fill in several run() variables (only
#    enough for campvarlog).  (NOT IMPLEMENTED!)
#
#  $Log: read_midas_log.tcl,v $
#  Revision 1.2  2015/09/28 00:33:19  asnd
#  Debugging
#
#  Revision 1.1  2015/09/26 06:23:14  asnd
#  New file for collecting past acquisition information from midas.log.
#
#
# State variables:
# state: 0 unknown, 1 stopped, 2 run starting, 3 run acquiring, 4 run paused
# rnum:  run number
# began: time run began
# acqoffat: time acquisition paused/stopped
# startrec: start-of-run record (to be put in lv_acq_record at end)

proc init_acq_record { } {
    global midas 
    #puts "init_acq_record"
    set now [clock seconds]
    set state 0
    set acqoffat 0
    set rnum -1
    set began 0
    set startrec {}
    if { ![file exists $midas(log_file)] } { return }
    set bytes [file size $midas(log_file)]
    if { [catch {set fd [open $midas(log_file) r] }] } { return }
    # Because I don't trap all errors:
    after 5000 [list catch "close $fd"]
    if { $bytes > 250000 } {
        seek $fd -200000 end
    }
    # Now, do I want to read at once or use gets for line-by-line???
    # Try gets with some preliminary seeks
    set lv_acq_record {}
    set nln 0

    # Seek to a decent starting position
    while { ![eof $fd] } {
        if { [gets $fd line] < 33 } { continue }
        if { [catch {set time [clock scan [string range $line 0 24]] }] } { continue }
        if { $now - $time < 88000 || [tell $fd] > $bytes-10000 } {
            if { $now - $time < 40000 || [tell $fd] > 1000 } {
                seek $fd -1000 current
            }
            break
        }
        if { [catch {
            incr suck
            seek $fd 3000 current
            gets $fd
        }] } { 
            close $fd
            return
        }
    }

    # Read log line-by-line parsing
    while { ![eof $fd] } {
        if { [gets $fd line] < 33 } { continue }
        if { [catch {set time [clock scan [string range $line 0 24]] }] } { continue }
	incr nln

        # Look for information in line
        set i [expr {[string first "\]" $line]+2}]
        if { $i < 25 } { continue }
        set msg [string range $line $i end]
        
        # Is it more efficient to switch with glob or to use if-else on [scan] and [string match] ?
        #puts $msg
        switch -glob -- $msg {
            "Run #*" {
                set n [scan $msg "Run #%d %s %s" r s ss]
                if { $n > 1 } {
		    set rnum $r
                    if { $s eq "started" } {# Not sure acquisition is on yet
                        #puts "--- started $r @$time"
                        if { [llength $startrec] } {
                            lappend lv_acq_record $startrec
                        }
                        set began $time
                        set state 2
                        set startrec [list lv_run_started $rnum $began]
                    } elseif { $s eq "stopped" } {
                        #puts "--- stopped $r with state $state @$time"
                        if { $state == 3 || $acqoffat == 0} {
                            set acqoffat $time
                        }
                        set state 1
                    } elseif { $s eq "start" && $n == 3 && $ss eq "aborted" } {# kill
                        #puts "--- aborted $r @$time"
                        if { $acqoffat == 0 } {
                            set acqoffat $began
                        }
                        set startrec {}
                        set state 1
                    } elseif { $s eq "paused" } {
                        #puts "--- paused $r @$time"
                        if { $acqoffat == 0 } {
                            set acqoffat $time
                        }
                        set state 4
                    } elseif { $s eq "resumed" && $state != 1 } {
                        #puts "--- resumed $r @$time"
                        if { $acqoffat > 0 } {
                            lappend lv_acq_record [list lv_acq_off $acqoffat $time]
                        }
                        set acqoffat 0
                        set state 3
                    }
                }
            }
            "Successfully killed run *" {
                #puts "--- Successfully killed @$time"
                if { $acqoffat == 0 } {
                    set acqoffat $began
                }
                set startrec {}
                set state 1
            }
            "* hot_toggle:  Run number has been toggled*" {
		set i [string first {Run number has been toggled} $msg]
		if { [scan [string range $msg $i end] {Run number has been toggled from %d to %d} r1 r2] == 2 } {
                    #puts "--- hot toggle $r1 to $r2 @$time"
		    set rnum $r2
		    set startrec [list lv_run_started $rnum $began]
		}
            }
            "INFO: Hold flag has *" {
                if { [string match "*Hold flag has been set*" $msg] } {
                    #puts "--- hold flag set @$time"
                    if { $acqoffat == 0 } {
                        set acqoffat $time
                    }
                    set state 4
                } elseif { [string match "*Hold flag has been cleared*" $msg] && $state != 1 } {
                    #puts "--- hold flag cleared @$time"
                    if { $acqoffat > 0 } {
                        lappend lv_acq_record [list lv_acq_off $acqoffat $time]
                    }
                    set acqoffat 0
                    set state 3
                }
            }
            "Run is on hold. No" {
                if { $state > 1 } {
                    if { $acqoffat == 0 } {
                        set acqoffat [expr {$state==2 ? $began : $time}]
                    }
                    set state 4
                }
            }
            "Run is resumed*" -
            "user resumes run*" -
	    "Success - all headers sent, resuming run*" {
                #puts "--- resumed @$time"
                if { $acqoffat > 0 } {
                    lappend lv_acq_record [list lv_acq_off $acqoffat $time]
                }
                set acqoffat 0
                set state 3
            }
            "* data saved in file*" {
                if { $state == 2 } {# data saved when uncertain of acq status => acquiring
                    if { $acqoffat > 0 } {
                        lappend lv_acq_record [list lv_acq_off $acqoffat $began]
                    }
                    set acqoffat 0
                    set state 3
                }
            }
        }
    }
    catch { close $fd }

    #puts "Processed $nln lines from $midas(log_file)."
    #puts [join $lv_acq_record "\n"]
    set ::lv_acq_record $lv_acq_record

    # Not necessary because use odb_get_runinfo: put final state into relevant run() variables
    global run
    if { [llength $startrec] } {
    	# CONFLICT WITH PROPER set run(start_record) $startrec
        #puts "Discard $startrec"
    }
    #set run(number) $rnum
    #set run(start_sec) $began
    #set run(start_time) [clock format $began]
    #set run(in_progress) [expr {$state > 2}]
    #set run(paused) [expr {$state == 4}]
    #set run(starting) [expr {$state == 2}]
    #set run(state) [lindex {0 1 2 3 2} $state]
}


# Default initialization:
if { (![info exists midas(log_file)]) } {
    switch -glob -- $::env(USER) {
        {bn*r} {
            if { [file exists "/isdaq/data1/$::env(USER)/dlog/[clock format [clock seconds] -format {%Y}]/midas.log"] } {
                set midas(log_file) "/isdaq/data1/$::env(USER)/dlog/[clock format [clock seconds] -format {%Y}]/midas.log"
            }
        }
        {m*} {
            if { [file exists "/data/$::env(USER)/[clock format [clock seconds] -format {%Y}]/midas.log"] } {
                set midas(log_file) "/data/$::env(USER)/[clock format [clock seconds] -format {%Y}]/midas.log"
            }
        }
    }
}

