#   readplan.tcl  procedures for reading an autorun plan.
#
#   $Log: readplan.tcl,v $
#   Revision 1.24  2016/09/06 22:10:53  asnd
#   Add more beamline control to autoruns, and fix some bugs
#
#   Revision 1.23  2015/09/28 00:32:19  asnd
#   Add "finally" for end-of-plan settings.
#
#   Revision 1.22  2015/04/17 23:54:54  asnd
#   Call to epicsset was missing a parameter
#
#   Revision 1.21  2015/03/20 00:29:15  suz
#   changes by Donald. This version part of package for new VMIC frontends
#
#   Revision 1.20  2013/10/30 05:54:52  asnd
#   Recent updates for autoruns
#
#   Revision 1.19  2009/09/10 04:25:22  asnd
#   Cache results of camp_cmd when reading plan
#
#   Revision 1.18  2008/11/19 06:22:46  asnd
#   Strip spurious quoted from titles
#
#   Revision 1.17  2008/11/13 04:37:34  asnd
#   Minor message
#
#   Revision 1.16  2008/05/15 04:34:32  asnd
#   Allow expressions in Camp settings
#
#   Revision 1.15  2008/05/13 23:32:38  asnd
#   Fix some glitches in autorun.
#
#   Revision 1.14  2008/04/30 01:56:24  asnd
#   Convert to bnmr-style /autorun parameters in odb
#
#   Revision 1.13  2008/04/15 01:36:10  asnd
#   Various small adjustments
#
#   Revision 1.12  2007/10/03 02:34:27  asnd
#   Changes to support bNMR
#
#   Revision 1.11  2006/11/23 02:57:33  asnd
#   maxwait parsing error fix; default to 60 min.  Change error messaging
#
#   Revision 1.10  2006/11/22 21:47:15  asnd
#   Add email notifier
#
#   Revision 1.9  2006/09/22 04:16:02  asnd
#   Minor change to epis set
#
#   Revision 1.8  2006/09/22 03:03:08  asnd
#   Add kludged epics-setting ability, and conditional blocks of Camp settings
#
#   Revision 1.7  2006/05/26 16:58:02  asnd
#   Accumulated changes.  Some new autorun features. Better synching of variables and gui.
#
#   Revision 1.6  2005/06/22 15:39:14  asnd
#   plan extra/other; change titles checking
#
#   Revision 1.5  2004/11/07 08:19:13  asnd
#   Addition of when and after to plan vocabulary.
#
#   Revision 1.4  2004/08/18 03:29:25  asnd
#   Clean up autorun interactions, incl using temporary plan file for editing.
#
#   Revision 1.3  2004/04/15 03:06:03  asnd
#   Add CVS log tag
#
#

################################
# read_plan  -- Read plan file
#
# This proc should be invoked in catch, because it returns an
# error when the file is not found.
# 
# Parameter: 
# planfile    the name of the file giving the run plan
# 
# Returns:
#   A list of command lines, constituting the plan, or an
#   error on failure.

namespace eval ::plan {}
global midas env 
if { ![info exists midas(expt)] } {
    if { [info exists env(MIDAS_EXPT_NAME)] } {
        set midas(expt) $env(MIDAS_EXPT_NAME)
    } elseif { [info exists env(BEAMLINE)] } {
        set midas(expt) $env(BEAMLINE)
    } elseif { [info exists env(USER)] } {
        set midas(expt) $env(USER)
    } else {
        set midas(expt) musr
    }
}

proc read_plan { planfile } {

    set fh [open $planfile r]
    set input [read $fh]
    close $fh

    # Lines that may have been broken with \ at end are NOT
    # recombined now, because that loses track of the line numbers
    # desired for error reporting.
    #regsub -all {((^|[^\\])(\\\\)*)\\ *\n\s*} $input {\1} input

    return [split $input "\n"]
}

################################
# parse_plan - Parse run plan.
#
# This proc should be invoked in catch, because the parse_error
# function causes an error return.
# 
# Parameters: 
# commands    A list of commands (as returned by read_plan)
# initial     (optional) the current (if in progress) or next (between runs) run number;
#             Runs before this in the plan are ignored.
# 
# Returns:
#   A success or failure message (return with code error on failure).
#   Parsed Plan actions saved in data structures in ::plan namespace.
#
# Globals/externals: 
#   ::plan::parse_error_action  -  indicates whether to stop ("error") or "continue" on errors
#   ::plan::parse_error_count   -  number of errors
#   ::plan::parse_error_messages - all error messages
#   ::plan::requirements($runnum)- list of requirements (each requirement is a list)
#   ::plan::odbset($runnum)     -  list of odb settings, pairwise var, val
#   ::plan::campset($runnum)    -  list of camp commands, with delays (delay, command)
#   ::plan::epicsset($runnum)   -  list of epics settings, with delays (delay, command)
#   ::plan::config($runnum)     -  list of configuration settings
#   ::plan::titles($runnum)     -  list of header info
#   ::plan::ccmd_cache()        -  cache of responses to camp commands
#   ::plan::epicsvars()         -  [array names] gives list of epics vars
#   ::plan::epicsfiles()        -  [array names] gives list of epics files

proc parse_plan { commands { initial -99 } } {

    array unset ::plan::ccmd_cache
    array unset ::plan::epicsvars
    array unset ::plan::epicsfiles

    set ::plan::parse_error_count 0
    set ::plan::parse_error_messages ""
    set ::plan::parse_stale_runs 0

    if { [info exists ::midas(expt)] } {
        set ::plan::expt $::midas(expt)
    } elseif { [info exists ::env(MIDAS_EXPT_NAME)] } {
        set ::plan::expt $::env(MIDAS_EXPT_NAME)
    } elseif { [info exists ::env(USER)] } {
        set ::plan::expt$::env(USER)
    } else {
        set ::plan::expt musr
    }

    set numruns 0

    set mtype ""       ;# current musr type ("" means unknown)
    set havetype 0     ;# indicates if we set a musrtype for this run
    set beginning 1    ;# indicates if we are before the first run
    set skipping 1     ;# indicates if we are skipping (discarding) commands/runs now
    set collecting 0   ;# indicates if we are collecting a block of camp commands
    set havestats 0    ;# indicates if we have one of the stats commands for this run
    set lnum 0         ;# input line number
    set prefx ""       ;# previuos line that had ended with \

# BAD!!    set runnum [expr {$initial-1}]    ;# current (previous) run number
    set runnum -99
    #puts "Parse plan beginning with initial run $initial"

    foreach v [list titles requirements campset odbset epicsset config] {
        set $v {}
        catch {unset ::plan::$v}
    }

    foreach line $commands {
        incr lnum
	set line [string trim $line]
        # insert existing prefix portion of line
        if { [string length $prefx] } {
            set line "$prefx $line"
            set prefx ""
        }
        if { [string match {[\#%!;]*} $line] } {
            # "Comment line - ignore it"
            continue
        }
        # detect continuation (line ends with odd number of backslashes)
        if { [regexp {\\+$} $line bstail] && [string length $bstail]%2 == 1 } {
            set prefx [string range $line 0 end-1]
            continue
        }
        if { [scan $line "%s" cmd] < 1 } {
            # "Blank line - ignore it"
            continue
        }

        set cmd [string trimright $cmd {:}]
 
        # simplified command string
        set scmd [string tolower [string map { "_" ""} $cmd]]

        if { $beginning  && ! [string equal $scmd "run"] } {
            parse_error "Command \"$cmd\" occurs before any \"Run\" declaration" $lnum $line
        }

        # Special handling when collecting a block of setting-related commands, after "when".
        if { $collecting } {
            switch -- $scmd {
                \} -
                enddo {
                    lappend requirements [lreplace $reqlist 6 6 $actions]
                    set actions [list]
                    set collecting 0
                }
                epicsset -
                setepics {
                    if { [catch { parse_epics_set $line } ret ] } {
                        parse_error $ret $lnum $line
                    } else {
                        lappend actions 0 $ret
                    }
                }
                moveslits - restoretune - loadtune -
                tunebeam - autotune - multiplettune -
                savetune {
                    if { [catch { parse_epics_script $line } ret ] } {
                        parse_error $ret $lnum $line
                    } else {
                        # ret value is a list: default_delay, command
                        lappend actions 0 [lindex $ret 1]
                    }
                }
                campcmd -
                setcamp -
                campset {
                    if { [catch { parse_camp $line } ret ] } {
                        parse_error $ret $lnum $line
                    } else {
                        lappend actions 0 $ret
                    }
                }
                after {
                    if { [catch { parse_after $line } ret ] } {
                        parse_error "Invalid After: $ret" $lnum $line
                    } else {
                        set actions [concat $actions $ret]
                    }
                }
                default {
                    parse_error "Illegal command \"$cmd\" found in Camp \"do\" block" $lnum $line
                }
            }
            
            continue
            # ... next input line (foreach)
        }

        # Else, this line is a regular command.

        switch -- $scmd {
            finally -
            next -
            run {
                if { [scan $line "%s %d" cmd rn] < 2 } {
                    if { $scmd eq "finally" } {
                        set rn [expr {$runnum + 1}]
                    } elseif { !$beginning && \
                             [scan $line "%s %s %c" cmd next xtra] == 2 && \
                             ( "$scmd $next" eq "run next" || "$scmd $next" eq "next run" ) } {
                        set rn [expr {$runnum + 1}]
                    } else {
                        parse_error "Missing or invalid run number" $lnum $line
                    }
                }
		#puts "$line ... run $rn after $runnum"
                # Note: rn variable is retained for use in following "repeat" case.
                # Copy out the previous run's parameters.  See identical block of
                # code at end.

                if { !$skipping } { # Add everything about previous run to plan database
                    # Allow missing statistics since adding "counts" to odb.
                    #if { !$havestats } {
                        # parse_error "Run number $runnum has no statistics requested" \
                        #        [expr {$lnum - 1}] ""
                    #}
                    foreach v [list titles requirements campset odbset epicsset config] {
                        set ::plan::${v}($runnum) [set $v]
                    }
                    incr numruns
                } elseif { $runnum > 0 } {
		    #puts "Discarding plans for run $runnum."
		}

                # Handle errors of mis-sequenced runs.
                # runnum = previous run, or something negative
                # If negative, then accept any new number.
                if { $rn != $runnum + 1 && $runnum >= 0 } {
		    if { $rn < $initial } {
			# Mis-sequenced run before initial run, so silently ignore misnumbering
		    } else {
			set msg "Run number out of order: $rn follows $runnum"
                        set runnum $rn
                        parse_error $msg $lnum $line
		    }
		}
                set runnum $rn
		if { $runnum < $initial } {
		    puts "Skipping plan for run $runnum < $initial "
		} else {
		    set skipping 0 
		}
                set beginning 0
                set havestats 0
                set havetype 0
                foreach v [list requirements campset odbset epicsset] {
                    set $v [list]
                }
                set titles {}; # [dict filter $titles key title] ;# To preserve meta-title
                # set initial config with run number and retain email and max_wait
                set config [concat [list number $rn] [dict filter $config key {[em][ma][ax][i_][lw]*}]]
                if { $scmd eq "finally" } {
                    lappend requirements [list "finalize" . . . . . {}]
                }
            }
            repeat {
                if { $beginning } {
                    parse_error "Repeat used before any run in plan" $lnum $line
                }
                if { [scan $line "%s %s" cmd repeatcount] != 2 } {
                    parse_error "Missing repeat count (need number or \"...\")" $lnum $line
                }
                if { [string equal $repeatcount "..."] } {
                    set repeatcount 500
                } elseif { ! [string is integer -strict $repeatcount] || $repeatcount < 0 } {
                    parse_error "Invalid repeat count $repeatcount (need number or \"...\")" $lnum $line
                }

                # Copy out the previous run's parameters several times.  See similar block of
                # code at end.
                # Note: variable rn set by the "run" command.

                for {set r 0} {$r < $repeatcount} {incr r} {
                    if { !$skipping } {
                        foreach v [list titles requirements campset odbset epicsset config] {
                            set ::plan::${v}($rn) [set $v]
                        }
                        incr numruns
                    }
                    incr rn
                    # runnum = previous run, or one less than the initial.
                    # rn = number for the run we are about to handle
                    if { $rn >= $runnum + 1 } {
                        set skipping 0
                        set runnum $rn
                    }
                    foreach v [list campset odbset epicsset] {
                        set $v [list]
                    }
                    set config [lreplace $config 0 1 number $rn]
                }
            }
            email {
                if { [scan $line "%*s %\[^\n]" spec] < 1 } {
                    parse_error "Missing email address(es)" $lnum $line
                }
                # Parse comma-separated string into a list of trimmed strings
                set al [list]
                foreach a [split $spec ,] {
                    if { [string first "@" $a] < 1 } {
                        parse_error "Invalid email address" $lnum $a
                    }
                    lappend al [string trim $a]
                }
                lappend config email $al
            }
            counts {
                parse_check_mtype mtype havetype TD-muSR $lnum $line
                set hn -1
                set un ""
                if { [scan $line {%s %f} cmd val] < 2 } {
                    parse_error "Invalid counts request" $lnum $line
                } elseif { [scan "$line * ." {%s %f %[Mm] %d * %c %c} cmd val un hn c d] == 5 } {
                    lappend config counts [expr { round($val*1.0e6) }] counthist $hn
                } elseif { [scan "$line * ." {%s %f %d * %c %c} cmd val hn c d] == 4 } {
                    lappend config counts [expr { round($val) }] counthist $hn
                } elseif { [scan "$line * ." {%s %f %[Mm] * %c %c} cmd val un c d] == 4 } {
                    lappend config counts [expr { round($val*1.0e6) }] counthist 0
                } elseif { [scan $line {%s %f %s} cmd val un] == 2 } {
                    lappend config counts [expr { round($val) }] counthist 0
                } else {
                    parse_error "Invalid counts units or hist number" $lnum $line
                }
                set havestats 1
            }
	    timelimit -
            elapsed {
                if { [scan $line "%*s %\[^\n]" spec] < 1 } {
                    parse_error "Missing elapsed time" $lnum $line
                }
                if { [catch { deltaTime $spec "m" } val] } {
                    parse_error "Invalid elapsed time value" $lnum $line
                }
                lappend config elapsed $val
                set havestats 1
            }
            maxwait -
            waitmax {
                if { [scan $line "%*s %\[^\n]" spec] < 1 } {
                    parse_error "Missing elapsed time spec" $lnum $line
                }
                if { [catch { deltaTime $spec "m" } val] } {
                    parse_error "Invalid elapsed time value" $lnum $line
                }
                lappend config max_wait $val
            }
	    scans -
            sweeps {
                parse_check_mtype mtype havetype I-muSR $lnum $line
                if { [scan $line "%s %d" cmd val] < 2 } {
                    parse_error "Invalid number of sweeps requested" $lnum $line
                }
                lappend config sweeps $val
                set havestats 1
            }
            cycles {
                parse_check_mtype mtype havetype TD-muSR $lnum $line
                if { [scan $line "%s %d" cmd val] < 2 } {
                    parse_error "Invalid number of cycles requested" $lnum $line
                }
                lappend config cycles $val
                set havestats 1
            }
            musrtype {
                if { $havetype } {
                    parse_error \
                        "Improperly positioned muSRtype command (must immediately follow Run)" \
			$lnum $line
                }
                if { [scan $line "%s %s" cmd typ] < 2 || \
			 ![string match {[ITit]*} $typ] } {
                    parse_error "Invalid muSR type (must be I-muSR or TD-muSR)" \
			$lnum $line
                }
                if { [string match {[Ii]*} $typ] } {
                    set mtype "I-muSR"
                    
                } else {
                    set mtype "TD-muSR"
                }
                lappend config musrtype $mtype
                set havetype 1
            }
            sweeprange {
                parse_check_mtype mtype havetype I-muSR $lnum $line
                if { [scan $line "%*s %\[^\n]" spec] < 1 } {
                    parse_error "Blank sweep range" $lnum $line
                }
                set range [ join [split $spec {,:;tobyTOBY} ] { } ]
                if { [scan $range " %f %f %f" from to step] < 3 } {
                    if { [string first "-" $spec] } {
                        parse_error "Invalid sweep range (perhaps an illegal hyphen)" \
			    $lnum $line
                    } else {
                        parse_error "Invalid sweep range (need three numbers)" \
			    $lnum $line
                    }
                }
                lappend config sweeprange [list $from $to $step]
            }
            tolerance {
                parse_check_mtype mtype havetype I-muSR $lnum $line
                if { [scan $line "%s %f" cmd val] < 2 || $val < 0.0 } {
                    parse_error "Invalid % tolerance" $lnum $line
                }
                lappend config tolerance $val
            }
            sample -
            orientation -
            operator {
                if { [scan $line "%*s %\[^\n]" val] < 1 } {
                    parse_error "Invalid $cmd command" $lnum $line
                }
                lappend titles $scmd [unquoted $val]
            }
            comment1 -
            comment2 -
            other {
                parse_check_mtype mtype havetype I-muSR $lnum $line
                if { [scan $line "%*s %\[^\n]" val] < 1 } {
                    parse_error "Invalid $cmd command" $lnum $line
                }
                lappend titles $scmd [unquoted $val]
            }
            experiment {
                if { [scan $line "%s %d" cmd val] < 2 || $val < 0 } {
                    parse_error "Invalid experiment number" $lnum $line
                }
                lappend titles experiment [unquoted $val]
            }
            field {
                if { [scan $line "%*s %\[^\n]" spec] < 1 } {
                    parse_error "Missing Field value or variable" $lnum $line
                }
                set spec [string trim [unquoted $spec]]
                if { [scan $spec " %g %s" B u] == 1 } {# Apply default unit G
                    append spec " G"
                }
                if { [scan [mui_convert_field $spec] %g B] < 1 && ![string match "/?*" $spec] } {
                    parse_error "Invalid Field specification '$spec'" $lnum $line
                }
                lappend titles field $spec
            }
	    temperature {
                if { [scan $line "%*s %\[^\n]" spec] < 1 } {
                    parse_error "Missing Temperature value or variable" $lnum $line
                }
                set spec [string trim [unquoted $spec]]
                if { [scan $spec " %g %s" T u] == 1 } {# Apply default unit K
                    append spec " K"
                }
                if { [scan [mui_convert_temperature $spec] %g T ] < 1 && ![string match "/?*" $spec] } {
                    parse_error "Invalid Temperature specification '$spec'" $lnum $line
                }
                lappend titles temperature $spec
	    }
            title {
                if { [string match "I*" $mtype] } {
                    parse_error \
                        "Illegal title for I-muSR run (the title will constructed automatically)" \
                        $lnum $line
                }
                if { [scan $line "%*s %\[^\n]" val] < 1 } {
                    parse_error "Invalid run title specification" $lnum $line
                }
                lappend titles title [unquoted $val]
            }
            mode {
		global rig
                parse_check_mtype mtype havetype TD-muSR $lnum $line
                if { [scan $line "%*s %\[^\n]" val] < 1 } {
                    parse_error "Invalid Mode command" $lnum $line
                }
                if { [catch { 
                    odb_get_rig
                    set f [file join $rig(rigpath) $rig(rigname) $val.mode]
                } ee ] } {
                    parse_error "Invalid Mode request ($ee)" $lnum $line
                }
                if { ![file exists $f] } {
                    parse_error "Invalid mode $val: Rig $rig(rigname) has no such mode ($f)" \
			$lnum $line
                }
                lappend config mode $val
            }
            setup {
                parse_check_mtype mtype havetype I-muSR $lnum $line
                if { [scan $line "%*s %s" spec] < 1 } {
                    parse_error "Invalid ImuSR Setup specification" $lnum $line
                }
                if { [catch { 
                    odb_get_idef
                    set f [file join $idef(setup_path) $spec.idef]
                }] } {
                    parse_error "Invalid Setup request" $lnum $line
                }
                if { ![file exists $f] } {
                    parse_error "Invalid setup: There is no I-�SR Setup named $spec" \
			$lnum $line
                }
                lappend config setup $spec
            }
	    setodb -
	    odbset {
		set varval [eval parse_varval $line]
		if { [llength $varval] != 2 } {
		    parse_error "Invalid odbset command" $lnum $line
		}
		lassign $varval var val
		if { [catch {midas get_value $var}] && [catch {midas get_data $var}] } {
		    parse_error "Unknown odb variable $var" $lnum $line
		}
		lappend odbset $var $val
	    }
            campcmd -
	    setcamp -
	    campset {
		if { [catch { parse_camp $line } ret ] } {
                    parse_error $ret $lnum $line
                } else {
                    lappend campset 0 $ret
                }
	    }
	    setepics -
	    epicsset {
                if { [catch { parse_epics_set $line } ret ] } {
                    parse_error $ret $lnum $line
                } else {
                    lappend epicsset 0 $ret
                }
	    }
            moveslits - restoretune - loadtune -
            tunebeam - autotune - multiplettune -
            savetune {
                if { [catch { parse_epics_script $line } ret ] } {
                    parse_error $ret $lnum $line
                } else {
                    # ret value is a list: default_delay, command
                    eval lappend epicsset $ret
                }
	    }
            after {
                if { [catch { parse_after $line } ret ] } {
                    parse_error "Invalid After: $ret" $lnum $line
                }
                # ret value is a list: delay, command
                eval lappend campset $ret
            }
	    require {
		if { [catch { eval parse_require $line } ret ] } {
                    #puts "Error code [catch { eval parse_require $line}]\nReturns: $ret"
		    parse_error "Invalid Require: $ret" $lnum $line
		}
                lappend requirements $ret
            }
	    when {
                # puts "when command: $line"
                # check for a null action, and replace with the command "null"
                if { [regexp -nocase \
                        {^when:?\s+(.*?):\s+$} \
                        $line -> req ] == 1 } { # null action specified.
                    append line " null"
                }
                # allow omission of colon and space for braced do-blocks
                if { [regexp -nocase \
                        {^when:?\s+(.*?):?\s*(\{|do)\s*$} \
                        $line -> req seccmd] } {
                    # puts "matched do-block case"
                    set params ""
                } elseif { [regexp -nocase \
                        {^when:?\s+(.*?):\s+(\w+):?\s+(.*)$} \
                        $line -> req seccmd params] < 1 } {
		    parse_error "Invalid When: bad syntax or missing arguments" $lnum $line
                }
		if { [catch { eval parse_require require: $req } reqlist ] } {
		    parse_error "Invalid When requirement: $reqlist" $lnum $line
		}
                set seccmd [string map { "_" "" } [string tolower $seccmd]]
                switch -- $seccmd {
                    \{ -
                    do {
                        # Start collecting a block of setting actions
                        set actions [list]
                        set collecting 1
                        set colline $line
                        set collnum $lnum
                        continue
                        # to next line of input file (foreach)
                    }
                    after {
                        if { [catch { parse_after "$seccmd $params" } ret ] } {
                            parse_error "Invalid When After: $ret" $lnum $line
                        }
                        set action $ret
                    }
                    null {
                        set action [list 0 {}]
                    }
                    epicsset -
                    setepics {
                        if { [catch { parse_epics_set "$seccmd $params" } ret ] } {
                            parse_error $ret $lnum $line
                        } else {
                            set action [list 0 $ret]
                        }
                    }
                    moveslits - restoretune - loadtune -
                    tunebeam - autotune - multiplettune -
                    savetune {
                        if { [catch { parse_epics_script "$seccmd $params" } ret ] } {
                            parse_error $ret $lnum $line
                        } else { # omit default delay from epics scripts:
                            set action [lreplace $ret 0 0 0]
                        }
                    }
                    campcmd - setcamp - campset {
                        if { [catch { parse_camp "$seccmd $params" } ret ] } {
                            parse_error $ret $lnum $line
                        } else {
                            set action [list 0 $ret]
                        }
                    }
		    default {
			parse_error "Invalid When: unsupported secondary command $seccmd" $lnum $line
		    }	
                }
                lappend requirements [lreplace $reqlist 6 6 $action]
            }
	    default {
                parse_error "Unrecognized command \"$cmd\"" $lnum $line
            }
	}

        # end foreach looping over input lines:
    }

    if { $collecting } {
        catch { parse_error "File ended inside a \"when\" block" $collnum $colline }
    }

    # Copy out the values for the last run (see identical block for run command)

    while { !$skipping } {
        # Need a useless loop, so "continue" from parse_error will work
        set skipping 1
        if { !$havestats } {
            #   parse_error "Run number $runnum has no statistics requested" [expr {$lnum - 1}] ""
        }
        foreach v [list titles requirements campset odbset epicsset config] {
            set ::plan::${v}($runnum) [set $v]
        }
    }

    # Perform some final overall checks 

    # Check existence of all epics variables
    set err [check_epics_vars]
    if { $err ne "" } {
        parse_error $err
    }

    # Check existence of all epics files (only complain; no error)
    set err [check_epics_files]
    if { $err ne "" } {
        parse_error $err
    }

    array unset ::plan::ccmd_cache
    array unset ::plan::epicsvars
    array unset ::plan::epicsfiles

    #puts "Error action is: $::plan::parse_error_action"
    if { $::plan::parse_error_count == 1} {
        return -code error "Run plan failed: $::plan::parse_error_messages"
    } elseif { $::plan::parse_error_count > 4 } {
        return -code error "Run plan failed with $::plan::parse_error_count errors"
    } elseif { $::plan::parse_error_count } {
        return -code error "Run plan failed with $::plan::parse_error_count errors: $::plan::parse_error_messages"
    } elseif { $::plan::parse_stale_runs } {
        set ::plan::parse_error_count 1
        set ::plan::parse_error_messages "Plan includes very old runs that should be cleaned out."
        return -code continue "Run plan succeeds with warning:\n$::plan::parse_error_messages"
    } else {
        return "Plan succeeds for $numruns run[plural $numruns]"
    }
}

# Collect arguments for commands with var and val parameters.  Return
# a list of two strings. 

proc parse_varval { cmd {var {}} args } {
    return [list $var [join $args " "]]
}


# Collect and parse the arguments for the Epicsset command.
# (This is collected in a function because it is used also by After and
# When commands.) Support simplified settings of "on" and "off" as well
# as partial/simplified PV names.

proc parse_epics_set { line } {
    global epics
    #puts "parse_epics_set for line {$line}"
    set varval [eval parse_varval $line]
    if { [llength $varval] != 2 } {
        return -code error "Invalid epicsset command \"$line\""
    }
    lassign $varval var val
    set var [string toupper $var]
    set op [string tolower $val]
    set iop [lsearch -exact {on off reset} $op]
    if { $iop < 0 } {
        set op "set"
    } elseif { $epics(${op}cmd) eq "CMD" } {
        set val [lindex {On Off Reset} $iop]
    } else {
        set val 1
    }
    set avar [resolve_epics_alias $var $op]
    if { ![string match {?*:?*:?*} $avar] } {
        set err "Invalid epics channel name $var"
        if { $avar ne $var } {
            append err " ($avar)"
        }
        return -code error $err
    }
    # delay real verification until the end. Now just make a note of this var.
    set ::plan::epicsvars($avar) {}
    return "caput $avar $val"
}

# Parse commands that involve a script on the epics host computer.
# Return a list of:  default delay ms, command to run remotely.
# (All such commands should explicitly invoke the scripting language,
# so beginning with "bash" or "tcsh" or "perl", and are recognized 
# by this pattern elsewhere [autoruns.tcl].)

proc parse_epics_script { line } {
    #puts "parse_epics_script for line {$line}"
    set cmd "epics script"
    set extra ""
    set n [scan $line "%s %s %\[^\n\]" cmd tune extra]
    if { $n < 2 } {
        return -code error "Invalid $cmd command"
    }
    set cmd [string tolower [string map { "_" ""} $cmd]]
    regsub {\.(\d|snap)$} [file tail $tune] {} tune
    switch -- $cmd {
        moveslits -
        restoretune -
        loadtune {
            if { [string match -nocase {*slits*} $cmd] } {
                append extra " slitsRestore"
            }
            regsub {\.(\d|snap)$} [file tail $tune] {} tune
            incr ::plan::epicsfiles(/$::epics(user)/tune/$tune.snap)
            return [list 0 "perl ~/bin/restore_tune_fancy.pl $tune $extra"]
        }
        savetune {
            return [list 5000 "tcsh -e ~/bin/save_tune $tune"]
            # tune will be saved in 5 seconds, to allow for possible settings
        }
        tunebeam -
        autotune -
        multiplettune {
            set n [scan $line "%s %s %s %c" cmd script tune extra]
            if { $n < 2 || $n > 3 } {
                return -code error "Invalid autotune command"
            }
            incr ::plan::epicsfiles(~/autotune/multiplet/scr/$script)
            if { $n == 2 } {# just tune
                return [list 5000 "bash -e ~/autotune/multiplet/scr/$script"]
            } else {# save tune after tuning
                regsub {\.(\d|snap)$} [file tail $tune] {} tune
                #  do tuning delayed, so other settings can settle first
                return [list 6000 "bash -e ~/autotune/multiplet/scr/$script && tcsh -e ~/bin/save_tune $tune"]
            }
        }
    }
    return -code error "Error with epics script command $cmd"
}

# Check existence of all requested epics variables (do not abort until all missing vars are reported)
# returns null for success, err message on error
proc check_epics_vars { } {
    set err ""
    if { [array exists ::plan::epicsvars] } {
        set evarlist [array names ::plan::epicsvars]
        set request "caget [join $evarlist { }]"
        catch {mui_epics_cmd $request} valstat
        set missing {}
        foreach line [split $valstat "\n"] {
            if { [string match $line "Channel connect timed out*"] } {
                set err "Epics $line\n"
            } elseif { [scan "$line\n" "%s %\[^\n\]" var val] == 2 } {
                if { [info exists ::plan::epicsvars($var)] } {
                    set ::plan::epicsvars($var) [string trim $val]
                    if { [string equal [string trim $val] "*** not connected"] } {
                        lappend missing $var
                    } else {
			#puts "Epics variable $var checks out (value $val)"
		    }
                } else {
                    puts "Unexpected epics return \"$line\""
                }
            } else {
                puts "Unusable epics return line \"$line\""
            }
        }
        if { [llength $missing] } {
            set err "Could not find Epics variable[plural [llength $missing]]:\n[join $missing {  }]"
        }
    }
    return $err
}

# Check existence of all epics files
proc check_epics_files { } {
    global epics
    set err ""
    if { [array exists ::plan::epicsfiles] \
             && [info exists epics(user)] && [info exists epics(host)]} {
        set efilelist [array names ::plan::epicsfiles]
        set request "ls [join $efilelist { }]"
        if { [catch { exec ssh -x -o PasswordAuthentication=no $epics(user)@$epics(host) $request } valstat] } {
            puts "Error finding one or more epics files in the list:\n$efilelist"
            # separate errors from successfully listed files to avoid confusion
            set valstat [join [regexp -all -inline -line {^ls\:.+$} $valstat] "\n"]
            set err "Could not find requested epics file(s):\n$valstat"
        } else {
            #puts "Listing of epics files:\n$valstat"
        }
    }
    return $err
}

# Collect and parse the arguments for the various camp-setting commands.
# (This is collected in a function because it is used also by After and
# When commands.)  The line has already been verified to begin with a 
# valid command.

proc parse_camp { line } {
    #puts "Parse_camp for line {$line}"
    if { [regexp -nocase {^camp_?cmd:?\s+(.*)$} $line -> val] } {
        regsub -all {<(/[^ >]+)>} $val {[varGetVal \1]} val
        return $val
    }
    set varval [eval parse_varval $line]
    if { [llength $varval] != 2 } {
        return -code error "Invalid Campset command"
    }
    lassign $varval var val
    #puts "Separated var {$var} and val {$val}"
    #
    # Get variable type and settability
    if { [catch {
        set ts [pp_cached_camp_cmd "list \[varGetVarType $var\] \[varGetStatus $var\]"]
        lassign $ts type stat
    } msg] || ![string is integer -strict $type] || ![string is integer -strict $stat]} {
        return -code error "Failed to detect Camp variable $var ($msg)"
    }
    if { ($stat & 2) == 0 } {
        return -code error "Camp variable $var is not settable"
    }
    # Substitute for </camp/var> notation
    set nsub [regsub -all {<(/[^ >]+)>} $val {[varGetVal \1]} val]
    #
    if { $type & 256 } { # 256 == CAMP_VAR_TYPE_NUMERIC
	if { [string is double -strict $val] } {
	    #puts "Numeric $var has numeric value $val"
	    return "varSet $var -v $val"
	}
        # Camp server is too forgiving with [expr {60.0.}], so only evaluate
	# remotely when vars are involved.
	if { $nsub > 0 } {
	    set code [catch {pp_cached_camp_cmd "expr {$val}"} setp]
	} else {
	    set code [catch {expr {$val}} setp]
	}
	if { $code } {
	    puts "Error evaluating numeric expression $val: $setp"
	    return -code error "Error evaluating numeric expression $val: $setp"
	} elseif { ![string is double -strict $setp] } {
	    puts "Bad value for numeric variable: $setp"
	    return -code error "Bad value for numeric variable: $setp"
	}
	#puts  "Numeric $var has numeric expression $val"
	return "varSet $var -v \[expr {$val}\]"
    }
    # non-numeric variable, accept any string
    #puts "Non-numeric $var setting \"$val\""
    return "varSet $var -v \"$val\""
}

# Collect and parse the arguments for the After command.  The syntax is:
# After[:] <elapsed_time>: <camp setting command>
# If successful, return the list: <delay> <camp_command>
#
# The parsing is especially tricky because the elapsed time can contain 
# colon characters!  Therefore, the elapsed time spec is terminated by
# a colon plus a valid subsidiary command (any camp settings).

proc parse_after { line } {
    if { [regexp -nocase \
            {after:?\s+(.+):\s+(camp_?cmd|set_?camp|camp_?set|set_?epics|epics_?set|move_?slits|restore_?tune|load_?tune|tune_?beam|auto_?tune|multiplet_?tune|save_?tune):?\s+(.*)$} \
            $line -> dtime cmd params] < 1 } {
        return -code error "bad syntax or unsupported secondary command for After"
    }
    if { [catch { deltaTime $dtime "s" } dtval] } {
        return -code error "bad elapsed time"
    }
    if { [string match "*epics*" $cmd] } {
        if { [catch { parse_epics_set "$cmd $params" } ret ] } {
            return -code error $ret
        }
    } else {
        if { [catch { parse_camp "$cmd $params" } ret ] } {
            return -code error $ret
        }
    }
    return [list [expr {$dtval*1000}] $ret]
}


# Collect and validate the parameters for the Require: command.
# On success, returns a list of values: 
#    Var isString atValue equalVariable withinError forTime
# On failure, returns an error message.
# The equalVariable can hold various information. It is the Camp
# or Epics variable name in "...equal /camp/var"; it is the variable 
# type of the primary Camp variable in "...is string"; it is the relation
# implied by "above" or "below" (> or <).
# In further processing, the list may have more values appended:
#    Var isString atValue equalVar withinErr forTime actions log target


proc parse_require { cmd args } {
    if { [llength $args] < 2 } {
        return -code error "Too few of arguments"
    }
    # puts "Parse requirements for $cmd $args"
    # The default time is 1 minute; the default error bar
    # is not zero because of floating-point roundoff
    set is {}
    set at {}
    set equal {}
    set within 0.00001
    set for 1
    set command {}

    set cev [lindex $args 0]  ;# Camp or Epics variable name
    if { [string match "/?*/?*" $cev] } {
        set campvar 1
        set epicsvar 0
    } elseif { [regexp {^[-A-Z0-9_]+(:[-A-Z0-9._]+)*$} $cev] &&
               [string match "?*:?*:?*" [set ev [resolve_epics_alias $cev read]]] } {
        set campvar 0
        set epicsvar 1
        set ::plan::epicsvars($ev) {}
        set cev $ev
    } else {
        return -code error "Invalid variable name \"$cev\""
    }

    set sub [string tolower [lindex $args 1]] ;# sub-command

    # A hack to allow the tempting but nonstandard syntax "require var equal var" (omit "stable")
    if { [string equal $sub "equal"] } {
        set sub "stable"
        set args [linsert $args 1 "stable"]
    }

    switch -- $sub {
        is {
            # Variable is compared with an explicit string; use quotes if it contains spaces
            if { [llength $args] != 3 } {
                return -code error "Invalid \"is\" parameter"
            }

            set is [lindex $args 2]

            if { $campvar } {
                # ensure that variable exists and is a string
                if { [catch {pp_cached_camp_cmd "varGetVarType $cev"} att] } {
                    return -code error "Camp variable \"$cev\" not found"
                }
                if { !($att & (512|1024)) } {
                    return -code error "Camp variable \"$cev\" invalid (need string or selection var)"
                }
                # save the appropriate command to use for retrieving the string value
                if { $att & 512 } {
                    set equal "varSelGetValLabel"
                } else {
                    set equal "varGetVal"
                }
            } else { # epics var; delay checking type and existance
                set equal "caget -t"
            }
        }
        stable {
            if { $campvar } {
                # Camp variable will be compared numerically
                if { [catch {pp_cached_camp_cmd "varNumGetNum $cev"} msg] } {
                    return -code error "Camp variable \"$cev\" incorrect (need numeric) ($msg)"
                }
            }
            set key [string tolower [lindex $args 2]]
            if { [lsearch -exact [list at equal within for] $key] < 0 } {
                return -code error "Unrecognized keyword \"$key\""
            }
            #
            # Now loop over keys and values.  This is complicated because
            # we allow a space in the time-range argument ("2 min") and
            # don't want to require quotes.
            # NOT:     foreach { key val } [lrange $args 2 end] 
            # Instead, we grab text in whatever chunks are between keywords.
            #
            set params [join [lrange $args 2 end] " "]
            foreach key [list at equal within for] {
                set n [regexp -nocase -all "$key\\s(.*?)( at | equal | within | for |\$)" $params -> val]
                if { $n > 1 } {
                    return -code error "Multiple \"$key\" keywords"
                }
                if { $n < 1 } {
                    continue
                }
                switch $key {
                    at {
                        if { [scan "$val Z" " %f %s" num stop] < 2 || $stop ne "Z" } {
                            return -code error "Bad \"at\" value: \"$val\""
                        }
                        set at $num
                    }
                    equal {
                        if { [string match "/?*" $val] } {
                            if { [catch {pp_cached_camp_cmd "varNumGetNum $val"}] } {
                                return -code error "Camp variable \"$val\" incorrect"
                            }
                        } elseif { [regexp {^[-A-Z0-9_]+(:[-A-Z0-9._]+)+$} $val] } {
                            set ::plan::epicsvars($val) {}
                        } else {
                            return -code error "Invalid comparison variable name \"$val\""
                        }
                        set equal $val
                    }
                    within {
                        if { [scan "$val Z" " %f %s" num stop] < 2 || $stop ne "Z" } {
                            return -code error "Bad \"within\" error value: \"$val\""
                        }
                        set within [expr {abs($num)}]
                    }
                    for {
                        if { [catch { deltaTime $val "s" } sec] } {
                            return -code error "Bad elapsed time \"$val\""
                        }
                        set for [expr { $sec < 1 ? 1 : round($sec) }]
                    }
                }
            }
            if { [string length $at] && [string length $equal] } {
                return -code error "Conflicting \"at\" and \"equal\" keywords"
            }
        }
        above -
        below {
            # Camp variable value will be compared numerically
            if { $campvar } {
                if { [catch {pp_cached_camp_cmd "varNumGetNum $cev"} ] } {
                    return -code error "Camp variable \"$cev\" incorrect (need numeric)"
                }
            }
            if { [string equal $sub "above"] } {
                set equal ">"
            } else {
                set equal "<"
            }
            set params [join [lrange $args 2 end] " "]
            set key {}
            set n [scan $params "%f %s %\[^\n\]" at key dt]
            if { $n < 1 } {
                return -code error "Bad \"$sub\" value: \"[lindex $args 2]\""
            }
            switch $key {
                "for" {
                    if { [catch { deltaTime $dt "s" } sec] || $sec < 1 } {
                        return -code error "Bad elapsed time \"$dt\""
                    }
                    set for [expr {round($sec)}]
                }
                "" {
                }
                default {
                    return -code error "Unknown qualifier for $sub: $key"
                }
            }
        }
        default {
            return -code error "Missing \"stable\", \"is\", \"above\" or \"below\" keyword"
        }
    }
        
    # Append final list item for the "action(s)" (secondary command(s) of "When")
    return [list $cev $is $at $equal $within $for {}]
}

namespace eval ::plan {
    variable parse_error_action continue
    variable parse_error_count 0
    variable parse_error_messages ""
}

##############################
# parse_error - a utility function to issue a parsing error message.
# It is general, in that it does not use the GUI, but it does 
# write the error to the text screen, appends it to the global 
# message block, and returns the message.  It also returns the
# error code indicated by  ::plan::parse_error_action, which 
# is "error" for stopping after the first error, or "continue"
# or "ok" for reporting all errors; or it could be "return"
# potentially.
#
# Params: {base message} {source line number} {source line}
# Globals: increments ::plan::parse_error_count
#          appends to ::plan::parse_error_messages
#          uses ::plan::parse_error_action
# 
proc parse_error { msg {line ""} {context ""} } {
    incr ::plan::parse_error_count
    if { $line ne "" } {
        append msg "\nin line ${line}"
        if { $context ne "" } {
            append msg ": \"$context\""
        }
    }
    puts "parse_error: $msg"
    append ::plan::parse_error_messages "\n" $msg "\n"
    return -code $::plan::parse_error_action $msg
}

###############################
# parse_check_mtype - a utility function to check for consistent 
#               experiment types within a run.
#
# parameters:   mvar  - variable name of current musrType
#                       (variable contains "" for unknown)
#               havevar - variable name for boolean which indicates
#                       if we set a musrtype for this run
#               type -  type implied/required now.
#               line -  the source line number (for error)
#               context - the source line text (for error)
#
# returns:      on conflicting types, return an error in the
#               same way as parse_error does.

proc parse_check_mtype {mvar havevar type line context} {
    upvar $mvar mtype $havevar have

    # For the interim, accept all type-dependent commands for bn*r.  Later, will want type to
    # be "1" or "2".
    if { ! [string equal -nocase $::plan::expt musr] } {
        return "*"
    }
    # puts "Check_mtype: $mvar $mtype, $havevar $have, type $type, line $line, context $context"
    if { $have && ![string equal $mtype ""] } {
        if { [string compare $type $mtype] } {
            incr ::plan::parse_error_count
            set errm "Conflicting �SR experiment type $type requested on line ${line}:\n$context"
            puts $errm
            append ::plan::parse_error_messages "\n$errm\n"
            return -code $::plan::parse_error_action $errm
        }
    } else {
        set mtype $type
        set have 1
    }        
    return $type
}

#  Cache results of camp commands to get large speed-up when checking a long
#  plan with a lot of repetition.  The cache entries are lists of return code and
#  return value, indexed by the literal camp command.  Commands containing ")"
#  are not cached.

proc pp_cached_camp_cmd { ccmd } {
    if { [string first ")" $ccmd] == -1 && [info exists ::plan::ccmd_cache($ccmd)] } {
        return -code [lindex $::plan::ccmd_cache($ccmd) 0] [lindex $::plan::ccmd_cache($ccmd) 1]
    } else {
        set code [catch {mui_camp_cmd $ccmd} res]
	#puts "mui_camp_cmd {$ccmd} returns $code: $res"
        set ::plan::ccmd_cache($ccmd) [list $code $res]
        return -code $code $res
    }
}

set epics(prefixlist) {M20 M15 M9 M9B M9A BNMR BNQR ILE2 ILE2A1 ITE ITW}

switch $midas(expt) {
    m20d {
        #  short-name  PV-head  Setting-PV-tail  Reading-PV-tail
        set epics(aliases) {
            EXPT M20:EXPT CUR RDCUR
            XSLIT1 M20:SM1:XSLIT1D WIDSP MOTION.K
            SM1:XSLIT1 M20:SM1:XSLIT1D WIDSP MOTION.K
            XSLIT1D M20:SM1:XSLIT1D WIDSP MOTION.K
            SM1:XSLIT1D M20:SM1:XSLIT1D WIDSP MOTION.K
            XSLIT1C M20:SM1:XSLIT1C WIDSP MOTION.K
            SM1:XSLIT1C M20:SM1:XSLIT1C WIDSP MOTION.K
            XSLIT0 M20:S2:XSLIT0 WIDSP MOTION.K
            S2:XSLIT0 M20:S2:XSLIT0 WIDSP MOTION.K
            VSLIT1 M20:S2:VSLIT1 WIDSP MOTION.K
            S2:VSLIT1 M20:S2:VSLIT1 WIDSP MOTION.K
            VSLIT M20:S2:VSLIT1 WIDSP MOTION.K
            S2:VSLIT M20:S2:VSLIT1 WIDSP MOTION.K
            S1:HVPOS M20:S1:HVPOS VOL RDVOL
            S2:HVPOS M20:S2:HVPOS VOL RDVOL
            S1:HVNEG M20:S1:HVNEG VOL RDVOL
            S2:HVNEG M20:S2:HVNEG VOL RDVOL
        }
        set epics(prefixdflt) M20
        set epics(resetcmd) RST
        set epics(oncmd) DRVON
        set epics(offcmd) DRVOFF
    }
    m20c {
        set epics(aliases) {
            EXPT M20:EXPT CUR RDCUR
            XSLIT1D M20:SM1:XSLIT1D WIDSP MOTION.K
            SM1:XSLIT1D M20:SM1:XSLIT1D WIDSP MOTION.K
            XSLIT1 M20:SM1:XSLIT1C WIDSP MOTION.K
            SM1:XSLIT1 M20:SM1:XSLIT1C WIDSP MOTION.K
            XSLIT1C M20:SM1:XSLIT1C WIDSP MOTION.K
            SM1:XSLIT1C M20:SM1:XSLIT1C WIDSP MOTION.K
            XSLIT0 M20:S2:XSLIT0 WIDSP MOTION.K
            S2:XSLIT0 M20:S2:XSLIT0 WIDSP MOTION.K
            VSLIT1 M20:S2:VSLIT1 WIDSP MOTION.K
            S2:VSLIT1 M20:S2:VSLIT1 WIDSP MOTION.K
            VSLIT M20:S2:VSLIT1 WIDSP MOTION.K
            S2:VSLIT M20:S2:VSLIT1 WIDSP MOTION.K
            S1:HVPOS M20:S1:HVPOS VOL RDVOL
            S2:HVPOS M20:S2:HVPOS VOL RDVOL
            S1:HVNEG M20:S1:HVNEG VOL RDVOL
            S2:HVNEG M20:S2:HVNEG VOL RDVOL
        }
        set epics(prefixdflt) M20
        set epics(resetcmd) RST
        set epics(oncmd) DRVON
        set epics(offcmd) DRVOFF
    }
    m15 {
        set epics(aliases) {
            EXP3 M15:EXP3 SC_CUR RDCUR
            EXP M15:EXP3 SC_CUR RDCUR
            EXPT M15:EXP3 SC_CUR RDCUR
            SL1 M15:SL1 WIDTH RDWIDTH
            SL2 M15:SL2 WIDTH RDWIDTH
            SL3 M15:SL3 WIDTH RDWIDTH
            SL4 M15:SL4P WIDTH RDWIDTH
            SL4P M15:SL4P WIDTH RDWIDTH
            SL5 M15:SL5 WIDTH RDWIDTH
            SL6 M15:SL6 WIDTH RDWIDTH
        }
        set epics(prefixdflt) M15
        set epics(resetcmd) CMD
        set epics(oncmd) CMD
        set epics(offcmd) CMD
    }
    bnmr {
        set epics(aliases) {
            HVBIAS:POS BNMR:HVBIAS:POS VOL RDVOL
            HVBIAS:NEG BNMR:HVBIAS:NEG VOL RDVOL
            BIAS:POS BNMR:HVBIAS:POS VOL RDVOL
            BIAS:NEG BNMR:HVBIAS:NEG VOL RDVOL
            BIASTUBE ILE2:BIASTUBE VOL RDVOL
            RBCELLBIAS ILE2:BIAS15 VOL RDVOL
            BIAS15 ILE2:BIAS15 VOL RDVOL
        }
        set epics(prefixdflt) BNMR
        set epics(resetcmd) RST
        set epics(oncmd) DRVON
        set epics(offcmd) DRVOFF
    }
    bnqr {
        set epics(aliases) {
            HH3 ILE2A1:HH3 CUR RDCUR
            HH ILE2A1:HH3 CUR RDCUR
            HVBIAS BNQR:HVBIAS VOL RDVOL
            POLSW ILE2:POLSW2 {} {}
            POLSW2 ILE2:POLSW2 {} {} 
            BIAS ILE2:BIAS15 VOL RDVOL
            BIAS15 ILE2:BIAS15 VOL RDVOL
        }
        set epics(prefixdflt) ILE2
        set epics(resetcmd) RST
        set epics(oncmd) DRVON
        set epics(offcmd) DRVOFF
    }
}

proc resolve_epics_alias { name {action set} } {
    global epics
    set name [string toupper $name]
    set action [string tolower $action]
    foreach {a b s r} $epics(aliases) {
        if { $name eq $a || $name eq $b } {
            switch $action {
                "set" { return "$b:$s" }
                "read" { return "$b:$r" }
                "on" { return "$b:$epics(oncmd)" }
                "off" { return "$b:$epics(offcmd)" }
                "reset" { return "$b:$epics(resetcmd)" }
            }
        }
    }
    # No alias matched. name might be the full PV name already, but let's
    # see if it looks like one before accepting it.
    # TODO: delay applying the default prefix until we find the variable does not exist. ????
    set names [split $name ":"]
    if { [lsearch -exact $epics(prefixlist) [lindex $names 0]] < 0 \
             && [llength $names] < 3 } {
        set name "$epics(prefixdflt):$name"
    }
    return $name
}

#################################################################################
#
#                            T E S T I N G
#
#################################################################################
#


if { [string equal [file nativename $argv0] [file nativename [info script]]] } {
    puts "!!!!!!!!!!!!!!!!!!!! TEST PLAN LOADER !!!!!!!!!!!!!!!!!"
    proc midas { args } {
        return 1
    }

    source mui_utils.tcl
    
    proc mui_camp_cmd { args } {
        return 2
    }


    set ::plan::parse_error_action continue
    
    set e [catch {parse_plan [read_plan plan.dat] 1233 } msg]
    
    if { $e } {
        puts "Parsing returns with error status"
        puts $msg
        puts $::plan::parse_error_messages
    } else {
        puts "No errors!"
        puts $msg
    }
}


