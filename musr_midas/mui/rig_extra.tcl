#  Rig procedures to go with rig.ui.tcl
#  $Log: rig_extra.tcl,v $
#  Revision 1.18  2008/11/19 06:20:39  asnd
#  Small patch for if user deletes rig window
#
#  Revision 1.17  2006/09/22 06:46:00  asnd
#  Bugfix in "New" rig.
#
#  Revision 1.16  2006/07/08 01:27:22  asnd
#  Configurable mu character for window titles, to handle bug with versions in Sci Linux.
#
#  Revision 1.15  2006/06/29 07:29:57  asnd
#  Use safer p_win_name (from mui_utils.tcl) for dialogs
#
#  Revision 1.14  2006/05/26 17:14:01  asnd
#  Accumulated changes over winter shutdown.
#
#  Revision 1.13  2004/02/20 23:08:06  asnd
#  Single musr Midas experiment.  Handle year changes.
#
#  Revision 1.12  2003/09/19 06:23:27  asnd
#  ImuSR and many other revisions.
#
#  Revision 1.11  2003/05/14 02:38:40  asnd
#  Fix odb_get_runinfo misspelling; use [file join] instead of explicit "/".
#
#  Revision 1.10  2003/03/11 03:20:16  asnd
#  Use BLT tabset for rig's counter/scaler pages
#
#  Revision 1.8  2002/10/04 02:53:06  asnd
#  Fix bug of unknown "selcounters"
#
#  Revision 1.7  2002/09/21 04:45:42  asnd
#  Various recent updates
#
#  Revision 1.6  2002/07/23 10:08:08  asnd
#  Fix recent test for if rig's counters changed
#
#  Revision 1.5  2002/07/19 11:49:45  asnd
#  Fix several mode/rig window interactions
#
#  Revision 1.4  2002/07/16 06:00:53  asnd
#  Changes up to Jul 15
#
#  Revision 1.3  2002/07/03 03:02:01  asnd
#  *** empty log message ***
#
#  Revision 1.2  2002/06/18 06:24:07  asnd
#  Changes today
#
#  Revision 1.1  2002/06/12 01:26:39  asnd
#  Starting versions of Mui files.
#

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

global rigwin

group_drag_bind_proc rig_c_move rig_move_counter
group_drag_bind_proc rig_s_move rig_move_scaler

proc rig_initialize { } {
    # Fire up the rig window. When used for real, rig_initialize is invoked by the
    # [rig] or [show rig] buttons, or when changing the rig.  When testing the gui
    # it can be invoked at the end of this source file.
    global rig rigwin mrig

    set rw [td_win_name $rigwin] 
    if { [ string length $rigwin ] } {
        # We are in a real application, where the rig window is not main, and
        # rig_initialize is invoked by [rig] buttons
        if { [winfo exists $rigwin.r_OK_b] } {
            # if window exists, make it visible
            wm deiconify $rw
            raise $rw
        } else {
            # window is not built yet. 
            toplevel $rigwin
#            ::blt::tabset $rigwin.tabs -samewidth true -relief flat -takefocus 1 -selectpad 3 -gap 2 -tearoff 0
            rig_ui $rigwin
        }
    }

    # misc initializations
    set mrig(chnames) 0

    # bind most help strings
    rig_bind_help tabs "Select counters or scalers"
    rig_bind_help tabs.r_counters_f "Counter definitions"
    rig_bind_help tabs.r_scalers_f "Scaler definitions"
    rig_bind_help r_change_b "Change the current rig settings (Alt-C)"
    rig_bind_help r_backup_b "Make a backup copy of the current rig (Alt-B)"
    rig_bind_help r_load_b   "Load a previously saved rig (Alt-L)"
    rig_bind_help r_rename_b "Rename the current rig (Alt-M)"
    rig_bind_help r_new_b    "Create a new rig (Alt-N)"
    rig_bind_help r_delete_b "Delete some other saved rig (Alt-D)"
    rig_bind_help r_apply_b  "Approve rig parameters (finish changes) (Alt-A)"

#   Unlike with menu-buttons, regular buttons with -underline still need to 
#   have an alt-key binding added explicitly.
    wm title $rw "${PREF::muchar}SR Rig Settings"
    bind $rw <Alt-l> "$rigwin.r_load_b invoke ; break"
    bind $rw <Alt-m> "$rigwin.r_rename_b invoke ; break"
    bind $rw <Alt-b> "$rigwin.r_backup_b invoke ; break"
    bind $rw <Alt-n> "$rigwin.r_new_b invoke ; break"
    bind $rw <Alt-d> "$rigwin.r_delete_b invoke ; break"
    bind $rw <Alt-c> "$rigwin.r_change_b invoke ; break"
    bind $rw <Alt-r> "$rigwin.r_revert_b invoke ; break"
    bind $rw <Alt-a> "$rigwin.r_apply_b invoke ; break"
    bind $rw <Alt-q> "$rigwin.r_cancel_b invoke ; break"
    bind $rw <Alt-o> "$rigwin.r_OK_b invoke ; break"

    # Un-do a bad spectcl binding
    bind $rw <Destroy> {}

    # add our own destroy binding, but not to a parent!
    bind $rigwin.r_OK_b <Destroy> { set mrig(changing) 0 ; set rig(changing) 0 }

    catch { odb_get_general ; odb_get_runinfo }
    rig_revert_change

    bind $rigwin.r_numcount_e <FocusOut> rig_apply_numcount
    bind $rigwin.r_numcount_e <<Delimiter>> rig_apply_numcount
    bind $rigwin.r_numscal_e <FocusOut> rig_apply_numscal
    bind $rigwin.r_numscal_e <<Delimiter>> rig_apply_numscal
}

proc rig_begin_change {} {
#   Activate widgets to allow changing rig parameters
    global mrig rigwin run mmode
#
    catch { odb_get_runinfo }
    set mrig(run_in_progress) $run(in_progress)
    if { $run(in_progress) } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
                -type ok -icon warning -title "Fail rig" \
                -message "A run is in progress!  Rig parameters cannot be changed"
        return
    }
    if { $mmode(changing) } {
        beep
        switch [ timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
                -type okcancel -default ok -icon warning -title "Lose mode changes" \
                -message "The mode is being edited!\nMode changes must be discarded to modify rig." ] {
            ok { mode_revert_change ; mode_end_change }
            default { return }
        }
    }
#
    set mrig(changing) 1
    $rigwin.r_change_b configure -state disabled
    $rigwin.r_apply_b configure -state normal
    $rigwin.r_backup_b configure -state disabled
    $rigwin.r_revert_b configure -text "Revert"
    rig_bind_help r_revert_b "Forget the changes in progress; restore previous (Alt-R)"
    rig_bind_help r_cancel_b "Close rig window (rejecting any changes) (Alt-Q)"
    rig_bind_help r_OK_b     "Approve rig parameters and close window (Alt-O)"
#
#   Display variable components (counters or scalers):
    rig_display_var
}

proc rig_end_change {} {
#   Deactivate widgets.  Invoked when rig window is (re)displayed and, of course, at
#   the end of some changes.
    global rigwin mrig run
    set mrig(changing) 0
    set mrig(chnames) 0
    if { ! [winfo exists $rigwin.r_change_b] } { return }
    if { ! $run(in_progress) } {
        $rigwin.r_change_b configure -state normal
    }
    $rigwin.r_apply_b configure -state disabled
    $rigwin.r_backup_b configure -state normal
    $rigwin.r_revert_b configure -text "Refresh"
    rig_bind_help r_revert_b "Refresh displayed rig parameters (Alt-R)"
    rig_bind_help r_cancel_b "Close rig window (Alt-Q)"
    rig_bind_help r_OK_b "Close rig window (Alt-O)"
#
#   Display variable components (counters or scalers):
    rig_display_var
}

#   Re-assert existing rig parameters, discarding on-screen changes.
#   It is used by the [Revert]/[Refresh] button, by rig_initialize, by rig_load,
#   by rig_apply_change (when not confirmed) and rig_close ([Cancel] button and
#   [OK] button when run in progress)
#
proc rig_revert_change { } {
    global rigwin mrig rig

#   Get the odb rig pars.
    if { [ td_odb odb_get_rig "get rig" ] == 0 } { return }

#   remember if we are changing rig, and signal full update (changing=0)
    set changing $mrig(changing)
    set mrig(changing) 0

    rig_update

    set mrig(changing) $changing
    set mrig(ori_numc) $rig(num_counters)
    set mrig(numc) $rig(num_counters)
    set mrig(nums) $rig(num_scalers)

#   Propagate these values to some others, and adjust display
    if { $changing } {
        rig_begin_change
    } else {
        rig_end_change
    }
}


#   rig_update should be invoked after calls to odb_get_rig.  It is used
#   by rig_revert_change and after rig_apply_change
proc rig_update { } {
    global mrig rig mmode run rigwin modewin

    set changing $mrig(changing)

#   Assemble working array from lists of values. (Need array so we can
#   associate particular elements with entry boxes.)
#
#   Counters:
    for {set ic 0} {$ic < $cfg::numCounter} {incr ic} {
        if {$ic < $rig(num_counters)} {
            set rig(c${ic}_num)  [expr $ic + 1]
            set rig(c${ic}_name) [lindex $rig(counter_names) $ic]
            set rig(c${ic}_Orig) [lindex $rig(counter_names) $ic]
            set rig(c${ic}_TDC_in) [lindex $rig(TDC_in) $ic]
            set rig(c${ic}_OR_out) [lindex $rig(OR_out) $ic]
        } else {
            set rig(c${ic}_name) {}
            set rig(c${ic}_Orig) {}
            set rig(c${ic}_TDC_in) {}
            set rig(c${ic}_OR_out) {}
            set rig(c${ic}_num) {}
        }
    }
#   Scalers:
    for {set is 0} {$is < $cfg::numScaler} {incr is} {
        if {$is < $rig(num_scalers)} {
            set rig(s${is}_num) [expr $is + 1]
            set rig(s${is}_name) [lindex $rig(scaler_names) $is]
            set rig(s${is}_input) [lindex $rig(scaler_in) $is]
        } else {
            set rig(s${is}_name) {}
            set rig(s${is}_input) {}
            set rig(s${is}_num) {}
        }
    }

#   perhaps update mode display:

    if { [winfo exists $modewin.m_modename_l] && ! $mmode(changing) \
            && $rig(num_counters) != $mmode(num_counters) && ! $changing } {
        # puts "rig_update forces mode_initialize"
        mode_initialize
    }

#   update rig display only when it already exists,

    if { [winfo exists $rigwin.r_load_b] } {
        if { $run(in_progress) } {
            $rigwin.r_change_b configure -state disabled
            $rigwin.r_load_b configure -state disabled
        } else {
            if { ! $mrig(changing) } {
                $rigwin.r_change_b configure -state normal
            }
            $rigwin.r_load_b configure -state normal
        }
        if { ! $changing } {
            array set mrig [array get rig]
            set mrig(changing) $changing
            rig_display_var
        }
    }
}

#  Apply changes to rig parameters.  Returns 1 on success, 0 on failure.
#  May be called when not changing!  If so, then test and return validity.

proc rig_apply_change {} {
    global rigwin rig mrig run mode muisrc
#   First, validate values
#
    if { [rig_test_invalid "[rig_proc_counters]" "Bad counter parameters"] } { return 0 }
    if { [rig_test_invalid "[rig_proc_scalers]" "Bad scaler parameters"] } { return 0 }
#
    if { ! $mrig(changing) } { return 1 }
#   check odb to see if a run has been started while changing.
    catch { odb_get_runinfo }
    if { $run(in_progress) } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
                -type ok -icon error -title "Cancel apply" \
                -message "A run is in progress!\nChanges to rig do not apply."
        # Do readback of real parameters to re-synchronize:
        rig_revert_change
        rig_end_change
        return 0
    }

#   Check if any changes were made to the counter parameters.

    set nmapc 0
    set ctr_change [expr { $mrig(num_counters) != $rig(num_counters) } ]
    if { !$ctr_change } {
        set n [expr {$mrig(num_counters) - 1}]
        set ctr_change [expr { [string compare [lrange $rig(OR_out) 0 $n] $mrig(OR_out)] \
         || [string compare [lrange $rig(TDC_in) 0 $n] $mrig(TDC_in)] \
         || [string compare [lrange $rig(counter_names) 0 $n] $mrig(counter_names)] } ]
    }

    if { $ctr_change } {
#       They changed, so we want to apply changes to the saved modes

        if { $mrig(chnames) } {
            #  edit mode files to update counter names
            odb_get_mode
            set plcmd "exec $muisrc/edit_modes.pl $mrig(rigpath)/$mrig(rigname)"
            set cmap() ""
            for {set ic 0} {$ic < $mrig(num_counters)} {incr ic} {
                if { [string compare $mrig(c${ic}_Orig) ""] && \
                        [string compare $mrig(c${ic}_name) ""] } {
                    append plcmd " $mrig(c${ic}_Orig) $mrig(c${ic}_name)"
                    incr nmapc
                }
                set cmap($mrig(c${ic}_Orig)) $mrig(c${ic}_name)
            }
            set sels [list]
            foreach selc $mode(selcounters) {
                if { [info exists cmap($selc)] } {
                    lappend sels $cmap($selc)
                } else {
                    lappend sels ?
                }
            }
            set mode(selcounters) $sels
            odb_set_mode
        }
    }

    set mrig(chnames) 0

#   Do apply change (mrig->rig)

    array set rig [array get mrig]
    set rig(enable_scalers) [ expr $rig(num_scalers) > 0 ]

    td_odb odb_set_rig "set rig parameters"

    rig_end_change

#   DO NOT: rig_update
    
    if { $ctr_change } {
        # counters changed, so bring up mode, with full sanity checking, and then 
        # begin changing it.
        mode_initialize
        mode_begin_change 
    } else {
        mode_update
    }

    if { $nmapc } {
        after 100 $plcmd
    }

    return 1
}

proc rig_OK { } {
    global mrig
    if { ![rig_apply_change] } { return }
    rig_close
}

proc rig_test_invalid { specific reason } {
    global rigwin
    if { [string length $specific] } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
                -type ok -icon error -title "Invalid rig" \
                -message "Invalid Rig definition:\n${reason} ($specific)"
        return 1
    }
    return 0
}

#   gather counter parameters into lists; 
#   return null string on success, or reason for failure
proc rig_proc_counters {} {
    global mrig
    if { $mrig(num_counters) < 1 } { return "no counters" }
#
    set mrig(counter_names) {}
    set mrig(TDC_in) {}
    set mrig(OR_out) {}
#
    for {set ic 0} {$ic < $mrig(num_counters)} {incr ic} {
        if { [ catch {
            # truncate name; no blank counter names
            if { [set c [string range $mrig(c${ic}_name) 0 $cfg::endCtName]] == "" } { return "blank name" }
            # no duplicate counter names:
            if { [lsearch $mrig(counter_names) $c] >= 0 } { return "duplicate name" }
            lappend mrig(counter_names) $c
#
            # valid TDC input number
            set t -9
            if { [scan $mrig(c${ic}_TDC_in) {%d} t] == 0 } { return "invalid TDC input number" }
            if { $t < $cfg::minTDC || $t > $cfg::maxTDC } { return "invalid TDC input number" }
            # no duplicate TDC inputs:
            if { [lsearch $mrig(TDC_in) $t] >= 0 } { return "duplicate TDC input" }
            lappend mrig(TDC_in) $t
#
            # valid number for O.R. output
            set r -9
            if { [scan $mrig(c${ic}_OR_out) {%d} r] == 0 } { return "invalid OR output number" }
            if { $r < $cfg::minOR || $r > $cfg::maxOR } { return "invalid OR output number" }
            # ALLOW DUPLICATE O.R. CHANNELS!
            lappend mrig(OR_out) $r
          } errmsg ] } {
            return "$errmsg"
        }
    }
    return ""
}

#   gather scaler parameters into lists; 
#   return null string on success, or reason for failure
proc rig_proc_scalers {} {
    global mrig
#
    set mrig(scaler_names) {}
    set mrig(scaler_in) {}
    if { $mrig(num_scalers) == 0 } { return "" }
    if { $mrig(num_scalers) < 0 || $mrig(num_scalers) > $cfg::numScaler } { return "bad number of scalers" }
#
    for {set is 0} {$is < $mrig(num_scalers)} {incr is} {
        if { [ catch {
            # truncate name; no blank scaler names
            if { [set s [string range $mrig(s${is}_name) 0 $cfg::endScName]] == "" } { return "blank name" }
            # no duplicate scaler names:
            if { [lsearch $mrig(scaler_names) $s] >= 0 } { return "duplicate name" }
            lappend mrig(scaler_names) $s
#
            # valid scaler input number
            set i -9
            if { [scan $mrig(s${is}_input) {%d} i] == 0 } { return "invalid input number" }
            if { $i < $cfg::minScal || $i > $cfg::maxScal } { return "invalid input number" }
            # no duplicate scaler inputs:
            if { [lsearch $mrig(scaler_in) $i] >= 0 } { return "duplicate input number"  }
            lappend mrig(scaler_in) $i
          } errmsg ] } {
            return "$errmsg"
        }
    }
    return ""
}

proc rig_close { } {
    global rigwin mrig run
    set confirm ok
    if { $mrig(changing) } {
        if { $run(in_progress) } {
            rig_revert_change
            rig_end_change
        } else {
            beep
            set confirm [ timed_messageBox -timeout 30000 \
                    -parent [p_win_name $rigwin] -title "Confirm close" \
                    -type okcancel -default ok -icon question \
                    -message "Rig is being changed!  Apply changes?" ]
            if { $confirm == "ok" } {
                catch {
                    rig_end_change
                    rig_apply_change
                }
            }
        }
    }
#  Note debugging puts...
    switch $confirm {
        ok {
            if { $rigwin == "" } { 
                puts "pretend window disappeared!" 
            } else { 
                destroy [td_win_name $rigwin]
            } 
        }
    }
}


proc rig_list_others { skiprig } {
    global mrig
    set rigs [list]
    foreach r [lsort [glob -nocomplain [file join $mrig(rigpath) *]] ] {
        set r [file tail $r]
        if { [string compare $r $skiprig] } {
            lappend rigs $r
        }
    }
    return $rigs
}

proc rig_load { } {
    global mrig rig rigwin run

    if { ![td_odb odb_get_runinfo "get run state"] } { return }
    if { $run(in_progress) } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
                -type ok -icon error -title "Run in progress!" \
                -message "A rig cannot be loaded\nwhile a run is in progress!"
        # Update load button because obviously it was out of synch.
        td_background_update
        catch { rig_update }
        return
    }
    set prevrig $mrig(rigname)
    set rigs [ rig_list_others $prevrig ]
    set newrig [ popup_selector \
        "Choose rig" "Choose rig to load:" \
        $rigs $mrig(rigname) single true 90 ]
    # puts "Selected rig is ($newrig)."
    if { [ string equal $newrig "" ] } { # User [Cancel] or timeout
        return
    }
    if { [ lsearch -exact $rigs $newrig ] < 0 } { 
	beep
        timed_messageBox -timeout 20000 -parent [p_win_name $rigwin] \
                -type ok -icon error -title "Unknown rig" \
                -message "No such rig: \"$newrig\""
	return 
    }

    set rig(rigname) $newrig

#   now what to do about the obsolete mode parameters?
#   Ask user to load a mode from current rig.  
    set themode ""
    if { [ string compare $newrig $prevrig] } {
        set modes [mode_list_modes ""]
        if { [llength $modes] } {
            set themode [ popup_selector \
                    "Choose mode" "Choose mode to load:" \
                    $modes "" single true 90 ]
        }
    }

#   puts "Loading rig $rig(rigpath)/$newrig/$newrig.rig"
    if { ! [td_odb "odb_load_file \{[file join $rig(rigpath) $newrig $newrig.rig]\}" "load rig file"] } {
        return
    }


    if { [string length $themode] } {
        if { [ lsearch -exact $modes $themode ] >= 0 } {
            set f [file join $rig(rigpath) $newrig $themode.mode]
            td_odb "odb_load_file \"$f\"" "load mode file"
        }
    }

    rig_revert_change
    set mrig(rigname) $newrig

#   If there is no new mode selected, then try keeping the mode 
#   from the previous rig.  If user typed in a  non-existent mode
#   name, then change mode's name to that.  If the rig is being edited,
#   then delay getting the mode.
    if { [ string compare $newrig $prevrig] && ! $mrig(changing) } {
        mode_initialize
        if { [ string compare $::mmode(modename) $themode] } {
            mode_begin_change
        }
    }

    if { [string length $themode] } {
        set ::mmode(modename) $themode
        set ::mode(modename) $themode
    }
}

#   Make Backup copy of current rig under a different name.
#   Also performed for [rename] and [new].  All of these copy
#   (or move) the rig files to the new location and edit the 
#   rig name in the file, rather than getting midas to make
#   the new versions.  Then [rename] and [new] get midas to 
#   load the new rig defs from the files.  Maybe it would be
#   better to change rig name and have midas do the saving.

proc rig_make_backup { oper blurb } {
    global mrig rigwin

    # save current values
    if { ![rig_apply_change] } { return }

    # Verify valid starting rig before proceeding
    if { ! [file exists [file join $mrig(rigpath) $mrig(rigname) $mrig(rigname).rig] ] } {
        beep
        timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
           -type ok -icon error -title "Bad Rig" \
           -message "Rig $mrig(rigname) has no data file!\nReport this to Data Acquisition."
        return ""
    }

    # Ask for new rig name (displaying list of existing rigs)
    set rigs [ rig_list_others $mrig(rigname) ]
    set newrig [ popup_selector "Choose new rig" $blurb $rigs "" single false 90 ]

    # fix up name to allow only sensible characters
    set newrig [ file tail $newrig ]
    set newrig [ join [split $newrig { `~!@#$%^&*()-+={}[];:'",<>?/} ] {} ]

    # return nil if select current name
    if { [string length $newrig] == 0 ||
            [string equal $newrig $mrig(rigname)] } {
        return ""
    }

    # Check if new rig already exists
    if { [lsearch -exact $rigs $newrig ] >= 0 } {
        beep
        switch [ timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
                -type okcancel -default cancel -icon warning -title "Replace rig?" \
                -message "Rig $newrig already exists!\nOver-write OK?" ] {
            ok { exec rm -r "$mrig(rigpath)/$newrig" }
            default { return "" }
        }
    }

    file $oper [file join $mrig(rigpath) $mrig(rigname)] [file join $mrig(rigpath) $newrig]
    file rename [file join $mrig(rigpath) $newrig $mrig(rigname).rig] \
            [file join $mrig(rigpath) $newrig $newrig.rig]

    set context {current rig = STRING : \[32\]}
    exec perl -pi -e "s/$context $mrig(rigname)/$context $newrig/g" \
                "$mrig(rigpath)/$newrig/$newrig.rig"

    return $newrig
}


proc rig_backup { } {
    rig_make_backup copy "Enter new name for backup copy:"
}

proc rig_rename { } {
    global mrig

    if { [set newrig [ rig_make_backup rename "Enter new name for this rig:" ]] == "" } { return }

    # tell midas to load the newly-saved rig
    odb_load_file [file join $mrig(rigpath) $newrig $newrig.rig]

    # and read back from midas
    rig_revert_change
    # for debugging:
    set mrig(rigname) $newrig
}

#   rig_new behaves somewhat differently from making a backup.  It doesn't worry
#   much about the existence of the current rig.  When the rig is changing, it
#   switches to the new rig and applies the changes, on the assumption the user
#   has already made the desired modifications.  When not changing, it copies the
#   current rig, switches to it, and begins changing on the assumtion that the 
#   user wants a new rig that is different from the current rig.  Much of the code
#   is copied from rig_make_backup, but rearranged.

proc rig_new { } {
    global rig mrig rigwin

    # Ask for new rig name (displaying list of existing rigs)
    set rigs [ rig_list_others $mrig(rigname) ]
    set newrig [ popup_selector "Choose new rig" "Enter name for new rig:" $rigs "" single false 90 ]

    # fix up name to allow only sensible characters
    set newrig [ file tail $newrig ]
    set newrig [ join [split $newrig { `~!@$%^&*()-+={}[];:',<>?/\#""} ] {} ]

    # return nil if Cancel selection
    if { [string length $newrig] == 0 } {
        return ""
    }

    # Check if new rig already exists, and is not the current rig
    if { [string compare $newrig $mrig(rigname)] && [lsearch -exact $rigs $newrig ] >= 0 } {
        beep
        switch [ timed_messageBox -timeout 30000 -parent [p_win_name $rigwin] \
                -type okcancel -default cancel -icon warning -title "Replace rig?" \
                -message "Rig $newrig already exists!\nOver-write OK?" ] {
            ok { exec rm -r "$mrig(rigpath)/$newrig" }
            default { return "" }
        }
    }

    # Verify valid starting rig, and if so, copy files
    if { [file exists [file join $mrig(rigpath) $mrig(rigname) $mrig(rigname).rig] ] } {
        file copy [file join $mrig(rigpath) $mrig(rigname)] [file join $mrig(rigpath) $newrig]
        file rename [file join $mrig(rigpath) $newrig $mrig(rigname).rig] \
            [file join $mrig(rigpath) $newrig $newrig.rig]
    }

    # It seems most natural to apply and end changes when already changing ( 
    if { $mrig(changing) } {
        set mrig(rigname) $newrig
        rig_apply_change
    } else {
        td_odb odb_get_rig "update rig parameters"
        set rig(rigname) $newrig
        td_odb odb_set_rig "set rig parameters"
        rig_begin_change
    }
    return
}

#   Delete rig(s).  Make a list of all rig names, excluding current
#   rig.  Ask user to select one.  Check the selection (because may
#   have been typed in selection box).

proc rig_delete { } {
    global mrig

    # Make list of rigs to select from (excluding current rig)
    set rigs [ rig_list_others $mrig(rigname) ]

    # Ask user for selection.
    set newrig [ popup_selector \
            "Choose rig" "Select rigs to delete:" $rigs "" multiple true 180 ]

    # Delete each selected rig, if it is on selection list!
    foreach r $newrig {
        if { [lsearch -exact $rigs $r ] >= 0 } {
            file delete -force -- $mrig(rigpath)/$r
        }
    }
}

#   Display variable components: counters or scalers.
#   Used by: rig_begin_change, rig_end_change, rig_update, delete (X) buttons, 
#            rig_apply_numcount, rig_apply_numscal, tab selection command.
proc rig_display_var { } {
    global rigwin run rig mrig
#
    set rroot $rigwin
    if { $rroot == "" } { set rroot "." }
#
#   These two aren't really rig parameters, and must not be altered
#   by status updates, so equate mrig and rig.
    set rig(changing) $mrig(changing)

    set changing $mrig(changing)
    if { $changing } {
        set sta "normal"
    } else {
        set sta "disabled"
    }
#   Counters:
#   Can skip updating counters if scalers are displayed
#    if { [winfo ismapped $rigwin.tabs.r_counters_f] || ![winfo ismapped $rigwin.tabs.r_scalers_f] } {
        rig_conf_entry $rigwin.r_numcount_e $changing
        if { $changing } {
            showButton $rigwin.r_countadd_b
            showButton $rigwin.r_chcnames_b
            rig_bind_help r_countadd_b "Add another counter"
            rig_bind_help r_chcnames_b "Allow counter names to be edited (affects modes)"
        } else {
            hideButton $rigwin.r_countadd_b
            hideButton $rigwin.r_chcnames_b
            rig_bind_help r_countadd_b ""
            rig_bind_help r_chcnames_b ""
        }
        if { $changing && $mrig(chnames) } {
            set namch 1
            set namwid 16
        } else {
            set namch 0
            # use width based on actual names:
            set namwid 0
        }
        
        for {set i 0} {$i < $mrig(num_counters)} {incr i} {
            if { ! [winfo exists $rigwin.r_c${i}_name_e] } {
                rig_make_counter_row $i
            }
            rig_conf_entry $rigwin.r_c${i}_name_e $namch -width $namwid
            rig_conf_entry $rigwin.r_c${i}_TDC_in_e $changing
            rig_conf_entry $rigwin.r_c${i}_OR_out_e $changing
            if { $changing } {
                $rigwin.r_c${i}_del_b configure -state normal -image delete_x -bg "$::PREF::revfgcol" \
                        -highlightcolor "$::PREF::brightcol" -relief raised -command "rig_del_counter $i ; rig_display_var"
                $rigwin.r_c${i}_handle_b configure -state normal -image grab_handle -bg "$::PREF::revfgcol" \
                        -highlightcolor "$::PREF::brightcol" -relief raised
                rig_bind_help r_c${i}_del_b "Delete counter entry"
                rig_bind_help r_c${i}_handle_b "Drag to rearrange order"
                group_drag_bind rig_c_move $rigwin.r_c${i}_handle_b $i [list rig_disp_drag c $i]
            } else {
                $rigwin.r_c${i}_del_b configure -state disabled -image {} -bg "$::PREF::bgcol" \
                        -highlightcolor "$::PREF::bgcol" -relief flat
                $rigwin.r_c${i}_handle_b configure -state disabled -image {} -bg "$::PREF::bgcol" \
                        -highlightcolor "$::PREF::bgcol" -relief flat
                rig_bind_help r_c${i}_del_b ""
                rig_bind_help r_c${i}_handle_b ""
                group_drag_unbind rig_c_move $rigwin.r_c${i}_handle_b 
            }
        }
        for {set i $mrig(num_counters)} {$i < 8} {incr i} {
            if { [winfo exists $rigwin.r_c${i}_name_e] } {
                rig_destroy_counter_row $i
            }
        }
#    }
        
#   Scalers:
#   (There are no restrictions on changing scaler names)
#   Can skip updating scalers if counters are displayed
#    if { [winfo ismapped $rigwin.tabs.r_counters_f] || ![winfo ismapped $rigwin.tabs.r_scalers_f] } {
        rig_conf_entry $rigwin.r_numscal_e $changing
        if { $changing } {
            showButton $rigwin.r_scaladd_b
            rig_bind_help r_scaladd_b "Add another scaler"
            set namwid 16
        } else {
            hideButton $rigwin.r_scaladd_b
            rig_bind_help r_scaladd_b ""
            # use width based on actual names:
            set namwid 0
        }
        
        for {set i 0} {$i < $mrig(num_scalers)} {incr i} {
            if { ! [winfo exists $rigwin.r_s${i}_name_e] } {
                rig_make_scaler_row $i
            }
            rig_conf_entry $rigwin.r_s${i}_name_e $changing -width $namwid
            rig_conf_entry $rigwin.r_s${i}_input_e $changing
            if { $changing } {
                $rigwin.r_s${i}_del_b configure -state normal -image delete_x -bg "$::PREF::revfgcol" \
                        -highlightcolor "$::PREF::brightcol" -relief raised -command "rig_del_scaler $i ; rig_display_var"
                $rigwin.r_s${i}_handle_b configure -state normal -image grab_handle -bg "$::PREF::revfgcol" \
                        -highlightcolor "$::PREF::brightcol" -relief raised
                rig_bind_help r_s${i}_del_b "Delete scaler entry"
                rig_bind_help r_s${i}_handle_b "Drag to rearrange order"
                group_drag_bind rig_s_move $rigwin.r_s${i}_handle_b $i [list rig_disp_drag s $i]
            } else {
                $rigwin.r_s${i}_del_b configure -state disabled -image {} -bg "$::PREF::bgcol" \
                        -highlightcolor "$::PREF::bgcol" -relief flat
                $rigwin.r_s${i}_handle_b configure -state disabled -image {} -bg "$::PREF::bgcol" \
                        -highlightcolor "$::PREF::bgcol" -relief flat
                rig_bind_help r_s${i}_del_b ""
                rig_bind_help r_s${i}_handle_b ""
                group_drag_unbind rig_s_move $rigwin.r_s${i}_handle_b
            }
        }
        for {set i $mrig(num_scalers)} {$i < $cfg::numScaler} {incr i} {
            if { [winfo exists $rigwin.r_s${i}_name_e] } {
                rig_destroy_scaler_row $i
            }
        }
#    }
}

proc rig_conf_entry { win sta args } {
    if { $sta } {
        eval activateEntry $win $args
    } else {
        eval deactivateEntry $win $args
    }
}

#proc rig_menus_build { } {
#    global mrig rigwin
#
#    $rigwin.r_load_mb.m configure -tearoff 0
#    $rigwin.r_load_mb.m delete 0 last
#
#    foreach f [lsort [glob -nocomplain $mrig(rigpath)/*] ] {
#        set n [file tail $f]
#        $rigwin.r_load_mb.m add command -label $n -command "do_load_rig $n"
#    }
#}

#proc do_load_rig { name } {
#    global mrig
#    puts "Loading rig $name"
#    set mrig(rigname) $name
#}


# The following four procedures need to have added tracing of the
# identities of the counters, to map starting counters -> changed
# counters (resp scalers) when we make the modes get automatically
# modified to suit the rig.

proc rig_add_counter { } {
    global mrig

    set ic $mrig(num_counters)
    if { $ic >= $cfg::numCounter } { return }
    set mrig(chnames) 1
    set mrig(c${ic}_name) ""
    set mrig(c${ic}_Orig) ""
    set mrig(c${ic}_TDC_in) ""
    set mrig(c${ic}_OR_out) ""
    set mrig(c${ic}_num) [ incr mrig(num_counters) ]
    set mrig(numc) $mrig(num_counters)
}

proc rig_del_counter { ic } {
    global mrig

    while { $ic < $mrig(num_counters) - 1 } {
        set i $ic
        incr ic
        set mrig(c${i}_name) $mrig(c${ic}_name)
        set mrig(c${i}_Orig) $mrig(c${ic}_Orig)
        set mrig(c${i}_TDC_in) $mrig(c${ic}_TDC_in)
        set mrig(c${i}_OR_out) $mrig(c${ic}_OR_out)
        set mrig(c${i}_num) $ic
    }
    incr mrig(num_counters) -1
    set mrig(numc) $mrig(num_counters)
}


proc rig_move_counter { from to } {
    global mrig
    set nc $mrig(num_counters)
    if { $from < 0 || $from >= $nc || $to < 0 || $to >= $nc || $from == $to } { return }
    set name  $mrig(c${from}_name)
    set orig  $mrig(c${from}_Orig)
    set tdcin $mrig(c${from}_TDC_in)
    set orout $mrig(c${from}_OR_out)
    set step [expr { ($to - $from) / abs($to - $from) } ]
    set ic $from
    while {$ic != $to} {
        set i $ic
        incr ic $step
        set mrig(c${i}_name) $mrig(c${ic}_name)
        set mrig(c${i}_Orig) $mrig(c${ic}_Orig)
        set mrig(c${i}_TDC_in) $mrig(c${ic}_TDC_in)
        set mrig(c${i}_OR_out) $mrig(c${ic}_OR_out)
    }
    set mrig(c${to}_name) $name
    set mrig(c${to}_Orig) $orig
    set mrig(c${to}_TDC_in) $tdcin
    set mrig(c${to}_OR_out) $orout
}

proc rig_apply_numcount { } {
    global mrig

    set n $mrig(num_counters)
    scan $mrig(numc) {%d} n
    set n [bounded $n 0 $cfg::numCounter]
    while { $n > $mrig(num_counters) } { 
        rig_add_counter
    }
    while { $n < $mrig(num_counters) } {
        rig_del_counter [expr $mrig(num_counters) - 1]
    }
    set mrig(numc) $mrig(num_counters)
    rig_display_var
}

proc rig_add_scaler { } {
    global mrig

    set is $mrig(num_scalers)
    if { $is >= $cfg::numScaler } { return }
    set mrig(s${is}_name) ""
    set mrig(s${is}_input) ""
    set mrig(s${is}_num) [ incr mrig(num_scalers) ]
    set mrig(nums) $mrig(num_scalers)
}

proc rig_del_scaler { is } {
    global mrig

    incr mrig(num_scalers) -1
    set mrig(nums) $mrig(num_scalers)
    while { $is < $mrig(num_scalers) } {
        set i $is
        incr is
        set mrig(s${i}_name) $mrig(s${is}_name)
        set mrig(s${i}_input) $mrig(s${is}_input)
        set mrig(s${i}_num) $is
    }
}

proc rig_move_scaler { from to } {
    global mrig
    set ns $mrig(num_scalers)
    if { $from < 0 || $from >= $ns || $to < 0 || $to >= $ns || $from == $to } { return }
    set name  $mrig(s${from}_name)
    set inp $mrig(s${from}_input)
    set step [expr { ($to - $from) / abs($to - $from) } ]
    set is $from
    while {$is != $to} {
        set i $is
        incr is $step
        set mrig(s${i}_name) $mrig(s${is}_name)
        set mrig(s${i}_input) $mrig(s${is}_input)
    }
    set mrig(s${to}_name) $name
    set mrig(s${to}_input) $inp
}

#   Display object name while dragging to rearrange order:
proc rig_disp_drag { type index args } {
    global mrig
    return "  $mrig(${type}${index}_name)"
}

proc rig_apply_numscal { } {
    global mrig

    set n $mrig(num_scalers)
    scan $mrig(nums) {%d} n
    set n [bounded $n 0 $cfg::numScaler]
    while { $n > $mrig(num_scalers) } { 
        rig_add_scaler
    }
    while { $n < $mrig(num_scalers) } {
        rig_del_scaler [expr $mrig(num_scalers) - 1]
    }
    set mrig(nums) $mrig(num_scalers)
    rig_display_var
}


proc rig_entries_navigate { box i sc W K } {
#   Special key functions used in the grid of bin-range entries.  These are
#   all bound to the psuedo-event <<Delimiter>>
#
#   Cursor Up/Down:    move focus in grid  (Left/Right move within entry box)
#   Return:            move to next (like Tab)
#   Quote (") = Ditto: copy entry from row above
#   space, comma:      move to next entry box (or next widget)
#
    global mrig rigwin
    switch -- $sc {
        s { set mx $mrig(num_scalers) }
        c { set mx $mrig(num_counters) }
    }
    switch -- $K {
        Up {
            set next $rigwin.r_${sc}[bounded [expr {$i - 1}] 0 $mx]_${box}_e
        }
        Down {
            set next $rigwin.r_${sc}[bounded [expr {$i + 1}] 0 $mx]_${box}_e
        }
        Return {
#           some extra checking here
            set next [tk_focusNext $W]
        }
        quotedbl {
            set mrig(${sc}${i}_${box}) $mrig(${sc}[bounded [expr {$i - 1}] 0 $mx]_${box})
            set next $W
        }
        default {
            set next [tk_focusNext $W]
        }
    }
#   Validate numeric value, and prevent octal forms with leading zeros:
    if { [string compare $box "name"] } {
        scan $mrig(${sc}${i}_${box}) { %d} mrig(${sc}${i}_${box})
    }
    focus $next
    catch { $next icursor end }
}


#  THE HELP LINE:
#
#  Many widgets have a binding for mouse-entry and mouse-exit where some
#  help text is displayed while the cursor is on that widget.

proc rig_bind_help { w h } {
    global rigwin
    bind $rigwin.$w <Enter> "set rig_help_text \"$h\""
    bind $rigwin.$w <FocusIn> "set rig_help_text \"$h\""
    bind $rigwin.$w <Leave> "set rig_help_text {}"
    bind $rigwin.$w <FocusOut> "set rig_help_text {}"
}


proc rig_make_counter_row { i } {
    global rigwin

    foreach b [list del handle] {
        [button $rigwin.r_c${i}_${b}_b] configure \
                -activebackground "$::PREF::revfgcol" -bg "$::PREF::bgcol" -borderwidth 1 \
                -highlightthickness 0 -padx 1 -pady 1 -takefocus 0 \
                -image {} -text {} -state disabled -relief flat 
    }
    [label $rigwin.r_c${i}_num_l] configure -textvariable mrig(c${i}_num)
    [entry $rigwin.r_c${i}_name_e] configure \
            -textvariable mrig(c${i}_name) -width 12
    [entry $rigwin.r_c${i}_TDC_in_e] configure \
            -textvariable mrig(c${i}_TDC_in) -justify right -width 3
    [entry $rigwin.r_c${i}_OR_out_e] configure \
            -textvariable mrig(c${i}_OR_out) -justify right -width 3
    set gridrow [expr $i + 4]
    grid $rigwin.r_c${i}_del_b    -in $rigwin.tabs.r_counters_f -row $gridrow -column 2 -ipadx 1 -ipady 1 -sticky nesw
    grid $rigwin.r_c${i}_handle_b -in $rigwin.tabs.r_counters_f -row $gridrow -column 3 -ipadx 1 -ipady 1 -sticky nesw
    grid $rigwin.r_c${i}_num_l    -in $rigwin.tabs.r_counters_f -row $gridrow -column 4
    grid $rigwin.r_c${i}_name_e   -in $rigwin.tabs.r_counters_f -row $gridrow -column 5 -sticky w
    grid $rigwin.r_c${i}_TDC_in_e -in $rigwin.tabs.r_counters_f -row $gridrow -column 6 -padx 1 -sticky w
    grid $rigwin.r_c${i}_OR_out_e -in $rigwin.tabs.r_counters_f -row $gridrow -column 7 -padx 8 -sticky w
    bind $rigwin.r_c${i}_name_e   <<Delimiter>> "rig_entries_navigate name $i c \%W \%K ; break"
    bind $rigwin.r_c${i}_TDC_in_e <<Delimiter>> "rig_entries_navigate TDC_in $i c \%W \%K ; break"
    bind $rigwin.r_c${i}_OR_out_e <<Delimiter>> "rig_entries_navigate OR_out $i c \%W \%K ; break"
    if { $i > 0 } {
        raise $rigwin.r_c${i}_name_e $rigwin.r_c[expr {$i - 1}]_OR_out_e
    } else {
        raise $rigwin.r_c${i}_name_e $rigwin.r_numcount_e
    }
    raise $rigwin.r_c${i}_TDC_in_e $rigwin.r_c${i}_name_e
    raise $rigwin.r_c${i}_OR_out_e $rigwin.r_c${i}_TDC_in_e
}


proc rig_destroy_counter_row { i } {
    global rigwin

    destroy $rigwin.r_c${i}_del_b
    destroy $rigwin.r_c${i}_handle_b
    destroy $rigwin.r_c${i}_num_l
    destroy $rigwin.r_c${i}_name_e
    destroy $rigwin.r_c${i}_TDC_in_e
    destroy $rigwin.r_c${i}_OR_out_e
}

proc rig_make_scaler_row { i } {
    global rigwin

    foreach b [list del handle] {
        [button $rigwin.r_s${i}_${b}_b] configure \
                -activebackground "$::PREF::revfgcol" -bg "$::PREF::bgcol" -borderwidth 1 \
                -highlightthickness 0 -padx 1 -pady 1 -takefocus 0 \
                -image {} -text {} -state disabled -relief flat 
    }
    [label $rigwin.r_s${i}_num_l] configure -textvariable mrig(s${i}_num)
    [entry $rigwin.r_s${i}_name_e] configure \
            -textvariable mrig(s${i}_name) -width 12
    [entry $rigwin.r_s${i}_input_e] configure \
            -justify right -textvariable mrig(s${i}_input) -width 2
    set gridrow [expr $i + 4]
    grid $rigwin.r_s${i}_del_b    -in $rigwin.tabs.r_scalers_f -row $gridrow -column 2 -ipadx 1 -ipady 1 -sticky nesw
    grid $rigwin.r_s${i}_handle_b -in $rigwin.tabs.r_scalers_f -row $gridrow -column 3 -ipadx 1 -ipady 1 -sticky nesw
    grid $rigwin.r_s${i}_num_l    -in $rigwin.tabs.r_scalers_f -row $gridrow -column 4 
    grid $rigwin.r_s${i}_name_e   -in $rigwin.tabs.r_scalers_f -row $gridrow -column 5 -sticky w
    grid $rigwin.r_s${i}_input_e  -in $rigwin.tabs.r_scalers_f -row $gridrow -column 6 -padx 4 -sticky w
    bind $rigwin.r_s${i}_name_e  <<Delimiter>> "rig_entries_navigate name $i s \%W \%K ; break"
    bind $rigwin.r_s${i}_input_e <<Delimiter>> "rig_entries_navigate input $i s \%W \%K ; break"
    if { $i > 0 } {
        raise $rigwin.r_s${i}_name_e $rigwin.r_s[expr {$i - 1}]_input_e
    } else {
        raise $rigwin.r_s${i}_name_e $rigwin.r_numscal_e
    }
    raise $rigwin.r_s${i}_input_e $rigwin.r_s${i}_name_e
}

proc rig_destroy_scaler_row { i } {
    global rigwin

    destroy $rigwin.r_s${i}_del_b
    destroy $rigwin.r_s${i}_handle_b
    destroy $rigwin.r_s${i}_num_l
    destroy $rigwin.r_s${i}_name_e
    destroy $rigwin.r_s${i}_input_e
}

#  Using BLT's tabset widget is hairy because SpecTcl won't set
#  up tabsets.  We define the scaler- and counter-properties frames
#  with SpecTcl, but then modify the setup using the perl "postproc"
#  script; see comments there.  After the BLT tabset is created,
#  we use the following rig_setup_tabs to put the counter and scaler
#  frames on tabs.  rig_setup_tabs is invoked in rig_ui; see Edit Code
#  in SpecTcl.

proc rig_setup_tabs { root base } {
    $base.tabs insert 0 counters -text Counters
    $base.tabs insert 1 scalers -text Scalers

    $base.tabs tab configure counters -window $base.tabs.r_counters_f \
            -fill both -ipadx 8 -ipady 4 \
            -background $::PREF::dimbgcol -selectbackground $::PREF::bgcol \
            -command { rig_display_var }

    $base.tabs tab configure scalers -window $base.tabs.r_scalers_f \
            -fill both -ipadx 8 -ipady 4 \
            -background $::PREF::dimbgcol -selectbackground $::PREF::bgcol \
            -command { rig_display_var }

    grid $base.tabs -in $root -row 2 -column 1 -padx 0 -pady 1 -sticky ew
}


set mmode(num_counters) 0

if { $rigwin == "" } {
    # stand-alone testing, so start immediately!
    set rig(changing) 0
    set mrig(changing) 0
    set rig(show) counters
    set modewin ".xx"

    rig_initialize
}
