# similar_color.tcl -- Some color manipulation procedures, and in particular
# one to choose a cluster of colors similar to one specified.
#
# $Log: similar_color.tcl,v $
# Revision 1.6  2015/09/28 00:31:16  asnd
# Tweak plot colors
#
# Revision 1.5  2008/04/30 01:56:24  asnd
# Convert to bnmr-style /autorun parameters in odb
#
# Revision 1.4  2006/11/22 21:17:55  asnd
# Avoid excessively dark/light (indistinguishable) colors
#
# Revision 1.3  2006/06/04 15:23:49  asnd
# Remove some debugging messages
#
# Revision 1.2  2006/05/26 17:21:52  asnd
# Accumulated changes. Notably y-axis locking and multiple plots on an axis.
#
#
# rgb2hsv
# The procedure below converts an RGB value to HSV.  It takes red, green,
# and blue components (0-65535) as arguments, and returns a list containing
# HSV components (floating-point, 0-1) as result.  The code here is derived
# from the code on page 615 of "Fundamentals of Interactive Computer Graphics"
# by Foley and Van Dam by way of the tk demos and wiki.

proc rgb2hsv {red green blue} {
    foreach {min mid max} [lsort -real [list $red $green $blue]] {break}
    set range [expr {double($max-$min)}]
    set sat [expr { $max > 0 ? $range/$max : 0.0 }]

    if {$sat == 0} {
        set hue 0
    } else {
        set rc [expr {($max - $red)/$range}]
        set gc [expr {($max - $green)/$range}]
        set bc [expr {($max - $blue)/$range}]

        set hue [expr { fmod(1.0 + ($red==$max ? $bc-$gc : ( $green==$max ? 2+$rc-$bc : 4+$gc-$rc ) ) / 6.0 , 1.0) }]
    }
    return [list $hue $sat [expr {$max/65535.0}]]
}

# hsv2rgb
# Takes hue, saturation, and value components (floating-point, 0-1.0) as 
# arguments; return a list containing RGB components (integers, 0-65535) 
# as result.

proc hsv2rgb {h s v} {
    set h6 [expr {($h-floor($h))*6}]
    set r [expr { $h6 <= 3 ? 2-$h6 : $h6-4}]
    set g [expr { $h6 <= 2 ? $h6 : ($h6 <= 5 ? 4-$h6 : $h6-6) }]
    set b [expr { $h6 <= 1 ? -$h6 : ($h6 <= 4 ? $h6-2 : 6-$h6) }]

    set r [expr {$r < 0.0 ? 0.0 : $r > 1.0 ? 1.0 : double($r)}]
    set g [expr {$g < 0.0 ? 0.0 : $g > 1.0 ? 1.0 : double($g)}]
    set b [expr {$b < 0.0 ? 0.0 : $b > 1.0 ? 1.0 : double($b)}]

    set r [expr {round(65535*(($r-1)*$s+1)*$v)}]
    set g [expr {round(65535*(($g-1)*$s+1)*$v)}]
    set b [expr {round(65535*(($b-1)*$s+1)*$v)}]
    return [list $r $g $b]
 }


# rgb2dec --
#
#   Turns #rgb into 3 elem list of decimal vals.
#
# Arguments:
#   cv	 The #rgb hex of the color to translate, or a known color name
# Results:
#   List of three decimal numbers, corresponding to #RRRRGGGGBBBB color
#   each in range 0 - 65534(?)
#
proc rgb2dec { cv } {
    set c [string tolower $cv]
    if {[regexp -nocase {^\#([0-9a-f])([0-9a-f])([0-9a-f])$} $c x r g b]} {
	# make #9fc => #99ffcc
        set c "\#$r$r$g$g$b$b"
    }
    if {[regexp -nocase {^\#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$} $c x r g b]} {
	# make #9f8e7d => #9f9f8e8e7d7d
        set c "\#$r$r$g$g$b$b"
    }
    if {[scan $c " \#%4x%4x%4x %c" r g b x] == 3} {
        return [list $r $g $b]
    }
    if {[catch {winfo rgb . $c} rgb]} {
        return -code error "bad color value \"$cv\""
    }
    return $rgb
}

# dec2rgb --
#
#   Takes a color name or decimal triplet and returns a #RRRRGGGGBBBB color.
#
# Arguments:
#   r		red dec value, the full color spec as a name or list of
#               three decimal numbers.  The decimal numbers go up to 65535.
#   g		green dec value, or omitted
#   b		blue dec value, or omitted
# Results:
#   Returns a #RRRRGGGGBBBB color
#
proc dec2rgb { r {g {}} {b {}}} {
    if {[string length $g] == 0} {
        if { [llength $r] == 3 } {
	    foreach {r g b} $r {break}
	} else {
	    foreach {r g b} [winfo rgb . $r] {break}
        }
    }
    set max 65535
    return [format "#%.4X%.4X%.4X" \
	    [expr {($r>$max)?$max:(($r<0)?0:$r)}] \
	    [expr {($g>$max)?$max:(($g<0)?0:$g)}] \
	    [expr {($b>$max)?$max:(($b<0)?0:$b)}]]
}

# similar_color color index ?basis?
# 
# Given a starting color, return one of a number of similar but distinct 
# colors (selected by index).  The optional basis tells how similar to
# make the colors; it should be a fractional number, and defaults to 0.25.
# 
# The color is converted to hsv form, and the hue may be shifted
# by a small amount (the scaling depends on the hue, see below) and
# the saturation and value are shifted by more, but always together.
#
# Rather than doing a direct calculation of the $index-th color, we
# go though the list of *feasible* similar colors, skipping illegal
# values.
# 
# This is programmed as a bunch of ad-hoc adjustments based on appearance
# and personal feel.  I might someday write a version that maps hsv space
# into a conical solid, and uses a Sobol' sequence to choose values from
# within it.

proc similar_color { color index {basis 0.25} } {
    #puts "Find color similar to $color for index $index"
    foreach {r g b} [rgb2dec $color] { break }
    if { $b > $r+$g } {#puts " lighten blues"
        set b [expr { 65534 - round(0.7*(65535-$b))}]
        set r [expr { 65534 - round(0.8*(65535-$r))}]
        set g [expr { 65534 - round(0.8*(65535-$g))}]
    }
    if { $index == 0 } {
        #puts "Return [dec2rgb $r $g $b]"
        return [dec2rgb $r $g $b]
    }
    foreach {h s v} [rgb2hsv $r $g $b] { break }

    set notgrey [expr {$s>0.01}]

    #puts "similar_color $color $index :  rgb $r $g $b  :  hsv $h $s $v"
    set svfacs [list 0.0 1.0 0.5 1.5 0.75 1.25 0.25 1.75 1.125 0.325 0.875 1.125 0.625]
    set svfnum [llength $svfacs]

    # Don't shift hue as much as saturation/value
    set hf 0.36

    if { $notgrey } {
        # Possible to adjust saturation differently than value (brightness)
	set sf 1.3
    } else {
        # Unsaturated (grey) values will only give grey-scale similar colors
	set sf 0.0
    }
    set unsat $notgrey

    # Move away from extremes of saturation and dark
    if { $notgrey } {
	set s [expr {(2.0*$s+0.5)/3.0}]
    }
    set v [expr {(3.0*$v+1.0)/4.0}]

    set i 0
    for {set tot 0} {$tot < $svfnum} {incr tot} {
        for {set hi 0} {$hi <= $tot*$notgrey} {incr hi} {
            set svi [expr {$tot-$hi}]
            set svf [lindex $svfacs $svi]
            foreach shi [list $hi -$hi] {
                foreach ssvf [list $svf -$svf] {
                    #puts "Try $i $tot $hi $svi - $shi $ssvf"

                    set sn [expr {$s+$sf*$ssvf*$basis}]
                    # generate white or black for out-of-range saturation only once:
                    if { $sn > 1.0 && $unsat } { set sn 1.0 ; set unsat 0 }
                    if { $sn < 0.0 && $unsat } { set sn 0.0 ; set unsat 0 }
                    if { $sn < 0.07 && $shi < 0.0 } { 
                        #puts "Skip because shifting hue when unsaturated."
                        continue
                    }

                    # Visual appearance varies too little for hue values near pure
                    # r, g, or b (0.0,.333,.666), and too quickly in between.  But the
                    # effect is much more pronounced for saturated colors than for
                    # pastels.  So scale hue values non-linearly -- by adding a
                    # three-hump sunusoidal component which contributes in proportion
                    # to the saturation value.
                    set nonlin [expr { 1.0 + 0.2*$sn*cos(3*6.28318530*$h) - 0.07*$sn*cos(6.2831853*$h) }]
                    #puts "nonlin: $nonlin <- { 1.0 + 0.2*$sn*cos(3*6.28318530*$h) - 0.1*$sn*cos(6.2831853*$h) }" 
                    set hn [expr { fmod(10.0+$h+$shi*$hf*$basis*$nonlin, 1.0) }]
                    #puts "hn: $hn diff [expr {$shi*$hf*$basis*$nonlin}]={$shi*$hf*$basis*$nonlin}"
                    set vn [expr {$v+$ssvf*$basis}]
                    #puts "set vn (expr {$v+$ssvf*$basis}) = $vn"
                    if { $vn > 1.0 && $vn < 1.0+0.5*$basis*$notgrey } { set vn 1.0 }
                    #if { $vn < 0.0 && $vn > -0.5*$basis*$notgrey } { set vn 0.0 }
                    if { $vn > 1.0 || $vn < 0.33 || $sn > 1.0 || $sn < 0.0 } {
                        #puts "Reject h s v $hn $sn $vn for value"
                        continue
                    }
                    if { [incr i] > $index } {
                        #puts "Choose for $index $tot $hi $svi - $shi $ssvf - ($hn, $sn, $vn)   [dec2rgb [hsv2rgb $hn $sn $vn]]"
                        return [dec2rgb [hsv2rgb $hn $sn $vn]]
                    }
                    if { $ssvf == 0 } { break }
                }
                if { $shi == 0.0 } { break }
            }
        }
    }
    return black

}

if { ![info exists mui_utils_loaded] } {
    # Running stand-alone -- testing
    #console show
    frame .f

    label .l -text "Enter color:"
    entry .e -textvar col -width 12
    entry .d -textvar bas -width 4
    set col "\#d04000"
    set bas 0.1

    button .b -text "OK" -command show_similar
    pack .l .e .d .b -in .f -side left
    pack .f
    pack [label .cref -text "ref"] -fill x

    for {set i 0} {$i < 6} {incr i} {
        pack [label .c$i -text " "] -fill x
    }

    proc show_similar { } {
        global col bas
        .cref configure -bg $col
        for { set i 0 } { $i < 6 } { incr i } {
            set c [similar_color $col $i $bas]
            .c$i configure -bg $c
        }
        #puts "-------------------------------------------------------------------------------------------------"
    }
}


    
