#  $Log: status_extra.tcl,v $
#  Revision 1.17  2015/04/17 23:52:56  asnd
#  Squash unexpected blanking of target counts when editing
#
#  Revision 1.16  2015/03/20 00:20:17  suz
#  changes by Donald. This version part of package for new VMIC frontends
#
#  Revision 1.15  2013/10/30 05:24:06  asnd
#  Allow entry of target counts right on status page
#
#  Revision 1.14  2008/08/23 20:43:08  asnd
#  Enable single-histogram counts on progress bar
#
#  Revision 1.13  2007/10/03 02:43:25  asnd
#  Fix spelling typo
#
#  Revision 1.12  2006/09/22 03:08:40  asnd
#  Change which times are displayed between runs
#
#  Revision 1.11  2006/07/08 01:27:22  asnd
#  Configurable mu character for window titles, to handle bug with versions in Sci Linux.
#
#  Revision 1.10  2006/05/26 16:59:53  asnd
#  Accumulated changes over winter shutdown.
#
#  Revision 1.9  2005/06/22 15:36:14  asnd
#  Ignore initial out-of-tolerance
#
#  Revision 1.8  2004/11/11 03:13:16  asnd
#  Fix up total counts display (allows formatted numbers)
#
#  Revision 1.7  2004/04/27 11:50:23  asnd
#  Audible alert when run ends asynchronously, or gets enough counts
#
#  Revision 1.6  2004/04/21 05:16:49  asnd
#  Fix progress bar number-of-points
#
#  Revision 1.5  2004/04/15 09:15:59  asnd
#  Adjust font of ImuSR run log, and make the column heads responsive to window resize
#
#  Revision 1.4  2004/04/10 07:00:07  asnd
#  Add auto-run control
#
#  Revision 1.3  2003/10/15 09:28:28  asnd
#  Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#  Revision 1.2  2003/09/19 06:23:27  asnd
#  ImuSR and many other revisions.
#
#  Revision 1.1  2002/06/12 01:26:39  asnd
#  Starting versions of Mui files.
#

if { [catch { set mui_utils_loaded }] } {
    source [file join [file dirname [info script]] mui_utils.tcl]
}

global statwin

# Fire up the run status window. When used for real, runstat_initialize is invoked by
# menus.  When testing the gui it can be invoked at the end of this source file.

proc runstat_initialize { } {
    global statwin midas mode run rstat statpb

    set sw [td_win_name $statwin]
    if { [ string length $statwin ] } {
        # We are in a real application, where the window is not main
        if { [winfo exists $statwin.rs_wintitle_l] } {
            # window exists.  Make it visible
            wm deiconify $sw
            raise $sw
        } else {
            # window is not built yet. 
            toplevel $statwin
            status_ui $statwin
        }
    }

    wm title $sw "${PREF::muchar}SR Run Status"
    bind $sw <Alt-c> "$statwin.rs_close_b invoke; break"
    bind $sw <Alt-u> "$statwin.rs_update_b invoke; break"

    set rstat(nhw) 0
    set rstat(nsw) 0
    # rstat(toler_memory) records out-of-tolerance history of point:
    # -1 = start of run (find tolerance), 0 = in tolerance, 1 = out of tolerance 
    set rstat(toler_memory) 0 
    # rstat(toler_time) records when we annoyed user about being out of tolerance.
    set rstat(toler_time) [clock seconds]

    $statwin.rs_htitles_l configure -font muMonoFont -wraplength 0
    bind $statwin.rs_htitles_l <Configure> {runstat_text_headings}
    $statwin.rs_text configure -font muMonoFont
    $statwin.rs_text tag configure highlight -foreground $::PREF::brightcol
    # Text widget doesn't work properly when fully disabled, so disable editing
    # by trapping keyboard and mouse-button events.  Do use mouse click events
    # to cancel any text selection.
    bind $statwin.rs_text <Key> {break} 
    bind $statwin.rs_text <Button> {catch {%W tag remove sel sel.first sel.last} ; break}
    # Make filename entry be disabled and look like a label (do it here so it works
    # in multiple Tcl versions.)
    deactivateEntry $statwin.rs_fname_l
    # And binding to close logfile when scroll-text destroyed (use title as surrogate)
    bind $statwin.rs_wintitle_l <Destroy> { runstat_stop_imusr_log }
    grid remove $statwin.rs_ames_l $statwin.rs_arun_mess_l
    # Add progress bar widget hiding label/button
    if { ![winfo exists $statwin.rs_progress_bar] } {
        grid [Progress $statwin.rs_progress_bar] -column 6 -row 6 -columnspan 5 -padx 2 -sticky nsew
    } else {
        grid $statwin.rs_progress_bar
    }

    # There is no single megawidget, so make binding for all components
    foreach w [info commands $statwin.rs_progress_bar*] {
        catch { bind $w <1> runstat_ask_progress }
    }
    # Create entry field on status page for changing target counts
    if { ![winfo exists $statwin.rs_progress_ask] } {
	#puts "Create entry field on status page for changing target counts"
        frame $statwin.rs_progress_ask -borderwidth 0 -padx 0 -pady 0
        label $statwin.rs_progress_ask.l1 -text "Counts:"
        entry $statwin.rs_progress_ask.e -takefocus 0 -highlightthickness 0 -background $PREF::entrybg -textvariable ::statpb(scts)
        label $statwin.rs_progress_ask.l2 -text "  Hist:"
        entry $statwin.rs_progress_ask.h -takefocus 0 -width 3 -highlightthickness 0 -background $PREF::entrybg -textvariable ::statpb(shst)
        label $statwin.rs_progress_ask.l3 -text "  "
        button $statwin.rs_progress_ask.ok -takefocus 0 -padx 3 -pady 0 -highlightthickness 0 -text "\u2714" -width 2 -command {runstat_set_progress 1}
        button $statwin.rs_progress_ask.x -takefocus 0 -padx 5 -pady 0 -highlightthickness 0 -text "\u2716" -width 2 -command {runstat_set_progress 0}
        pack $statwin.rs_progress_ask.l1 $statwin.rs_progress_ask.e $statwin.rs_progress_ask.l2 $statwin.rs_progress_ask.h \
            $statwin.rs_progress_ask.l3 $statwin.rs_progress_ask.ok $statwin.rs_progress_ask.x \
            -side left -fill y
        grid $statwin.rs_progress_ask -column 6 -row 6 -columnspan 5 -padx 0 -pady 0 -sticky nsew
        grid remove $statwin.rs_progress_ask
    }
    catch { td_all_update }
    after idle after 100 { td_all_update }
    #puts "[winfo reqheight $statwin.rs_progress_bar]"
    #puts "[winfo reqheight $statwin.rs_progress_ask]"
    #puts "[winfo reqheight $statwin.rs_progress_ask.l1], [winfo reqheight $statwin.rs_progress_ask.e], [winfo reqheight $statwin.rs_progress_ask.x]."
}

# Update the display of run-status variables (histogram and scaler).
# This is invoked by td_all_update.

proc runstat_update { } {
    global run rstat autor statwin

    after cancel $rstat(after_key)
    set rstat(after_key) {}

    if { $autor(enabled) == 0 } {
        grid remove $statwin.rs_ames_l $statwin.rs_arun_mess_l
    } elseif { $autor(state) != "idle" } {
        grid $statwin.rs_ames_l $statwin.rs_arun_mess_l
    }

    if { $run(ImuSR) } {

        global idef
        grid remove $statwin.rs_TDmusr_f
        grid $statwin.rs_Imusr_f
        grid remove $statwin.rs_set_progress_b

        # Perform rapid status updates when ImuSR run is in progress and status is displayed
        if { [winfo exists $statwin.rs_wintitle_l] } {

            # adjust progress bar
            if { $run(in_progress) } {
                if { $idef(num_sweeps) > 0 } {
                    set totpt [expr {$idef(num_sweeps)*$rstat(ptspersweep)}]
                    grid $statwin.rs_progress_bar
                    SetProgress $statwin.rs_progress_bar $rstat(total) $totpt \
                            [format {%d of %d points} $rstat(total) $totpt]
                } else {
                    # Unlimited sweeps: Progress bar is *removed*, revealing
                    # the plain label "(no target specified)"
                    grid remove $statwin.rs_progress_bar
                }
            } else {
                grid $statwin.rs_progress_bar
                SetProgress $statwin.rs_progress_bar 100 100 "finished $rstat(total) points"
            }

            # adjust labels
            if { $idef(num_sweeps) > 0 } {
                set rstat(sweep_req_text) "(of  $idef(num_sweeps)  requested)"
            } else {
                set rstat(sweep_req_text) {}
            }
            if { $run(state) < 3 } {
                $statwin.rs_toler_status_l configure -text {} -fg $::PREF::fgcol
            } elseif { $rstat(in_tolerance) } {
                $statwin.rs_toler_status_l configure -text OK -fg $::PREF::fgcol
            } elseif { $idef(toler_enable) } {
                # Display out-of-tolerance information:
                # Display bright "OUT OF TOLERANCE"
                # Ring bell at most once per point and once per minute
                #    OR once every 4 minutes during protracted out-of-tolerance
                # Remember any out-of-tolerance to display data point highlighted,
                # except the first out-of-tolerance period of a run.
                $statwin.rs_toler_status_l configure -text {OUT OF TOLERANCE} -fg $::PREF::brightcol
                if { $rstat(toler_memory) > -1 } {
                    if { $rstat(toler_time) < [clock seconds] - 60 - 180*$rstat(toler_memory) } {
                        beep
                        set rstat(toler_time) [clock seconds]
                    }
                    set rstat(toler_memory) 1
                }
            } else {
                $statwin.rs_toler_status_l configure -text "" -fg $::PREF::fgcol
            }
            set stat [runstat_update_scroll]
            if { $run(in_progress) && $stat } {
                # puts "Repeat scroll update after $::PREF::ImusrUpdatePeriod"
                set rstat(after_key) [after $::PREF::ImusrUpdatePeriod { 
                    catch { odb_get_stat ; runstat_update }
                }]
            }
        }

    } else { # TD muSR

	global rig

        grid $statwin.rs_TDmusr_f
        grid remove $statwin.rs_Imusr_f

        if { [string length $rstat(log_handle)] } { runstat_stop_imusr_log }

        # adjust progress bar
        if { $autor(counts) > 0 } {
            grid remove $statwin.rs_set_progress_b
            if { $run(in_progress) } {
                if { $autor(counthist) > 0  && $autor(counthist) <= $rstat(nhist) } {
                    set tot [expr {round([lindex $rstat(htotals) [expr {$autor(counthist)-1}]])}]
                    set ctstr [format {%d of %d counts in hist %d} $tot $autor(counts) $autor(counthist) ]
                } else {
                    set tot [expr {round($run(hist_total))}]
                    set ctstr [format {%d of %d counts} $tot $autor(counts) ]
                }
                # ring bells (beeps) when progress bar moves over the top
                if { $tot > $rstat(previous_total) && $tot >= $autor(counts) } {
                    bells 3
                }
                grid $statwin.rs_progress_bar
                SetProgress $statwin.rs_progress_bar $tot $autor(counts) $ctstr                    
                set rstat(previous_total) $tot
            } else {
                grid $statwin.rs_progress_bar
                if { $autor(counthist) > 0  && $autor(counthist) <= $rstat(nhist) } {
                    set tot [expr {round([lindex $rstat(htotals) [expr {$autor(counthist)-1}]])}]
                    SetProgress $statwin.rs_progress_bar 100 100 \
                        [format "finished %d counts in hist %d" $tot $autor(counthist)]
                } else {
                    SetProgress $statwin.rs_progress_bar 100 100 "finished $rstat(hist_total) counts"
                }
                set rstat(previous_total) 0
            }
        } else {
            # No counts specified:  Progress is *removed*, revealing the plain label 
            # "(no target specified)" and button to set progress target
            grid remove $statwin.rs_progress_bar
            SetProgress $statwin.rs_progress_bar 0 100 ""
	    grid $statwin.rs_set_progress_b
        }

#       Activate/deactivate widgets if need more or less than last time
        if { $rstat(nhist) != $rstat(nhw) } {
            runstat_change_hist
        }
        if { $rig(num_scalers) != $rstat(nsw) } {
            runstat_change_scal
        }
#       Put lists into hash-arrays so they are displayed
        set i 0
        while { $i < $rstat(nhist) } {
            set rstat(h${i}name) [lindex $rstat(hnames) $i]
            set rstat(h${i}tot) [td_big_number [lindex $rstat(htotals) $i]]
            set rstat(h${i}num) [incr i]
        }
        set i 0
        catch {
            while { $i < $rig(num_scalers) } {
                set rstat(s${i}name) [lindex $rig(scaler_names) $i]
                set rstat(s${i}tot) [td_big_number [lindex $rstat(stots) $i]]
                catch { 
                    set rstat(s${i}rate) [lindex $rstat(srates) $i]
                    set rstat(s${i}rate) [expr { round($rstat(s${i}rate)) }]
                }
                set rstat(s${i}num) [incr i]
            }
        }
    }
}

#  Update number of displayed histogram contents:

proc runstat_change_hist { } {
    global statwin rstat

    set nh [bounded $rstat(nhist) 0 16]
    for { set i 0 } { $i < $nh } { incr i } {
        if { ![winfo exists $statwin.rs_h${i}name_l] } {
            label $statwin.rs_h${i}num_l -anchor w -textvariable rstat(h${i}num) -width 9
            label $statwin.rs_h${i}name_l -anchor w -textvariable rstat(h${i}name)
            label $statwin.rs_h${i}tot_l -anchor w -textvariable rstat(h${i}tot)
        }
    }
    for { set i $nh } { $i < 16 } { incr i } {
        if { [winfo exists $statwin.rs_h${i}name_l] } {
            catch {
                destroy $statwin.rs_h${i}num_l
                destroy $statwin.rs_h${i}name_l
                destroy $statwin.rs_h${i}tot_l
            }
        }
    }
    for { set i 0 } { $i < $nh } { incr i } {
        if { $i < 8 } {
            set r 1
            set c [expr {$i + 1}]
        } else {
            set r 4
            set c [expr {$i - 7}]
        }
        grid $statwin.rs_h${i}num_l -in $statwin.rs_hists_f \
                -row $r -column $c -padx 4 -sticky w
        grid $statwin.rs_h${i}name_l -in $statwin.rs_hists_f \
                -row [incr r] -column $c -padx 4 -sticky w
        grid $statwin.rs_h${i}tot_l -in $statwin.rs_hists_f \
                -row [incr r] -column $c -padx 4 -sticky w
    }
    set rstat(nhw) $nh
}

#  Update number of displayed scalers:

proc runstat_change_scal { } {
    global statwin rstat rig

    set ns [bounded $rig(num_scalers) 0 10]
    for { set i 0 } { $i < $ns } { incr i } {
        if { ![winfo exists $statwin.rs_s${i}name_l] } {
            label $statwin.rs_s${i}name_l -anchor w -textvariable rstat(s${i}name)
            label $statwin.rs_s${i}tot_l  -anchor w -textvariable rstat(s${i}tot)
            label $statwin.rs_s${i}rate_l -anchor w -textvariable rstat(s${i}rate)
        }
    }
    for { set i $ns } { $i < 10 } { incr i } {
        if { [winfo exists $statwin.rs_s${i}name_l] } {
            catch {
                destroy $statwin.rs_s${i}name_l
                destroy $statwin.rs_s${i}tot_l
                destroy $statwin.rs_s${i}rate_l
            }
        }
    }
    for { set i 0 } { $i < $ns } { incr i } {
        set r 1
        set c [expr {$i + 2}]
        grid $statwin.rs_s${i}name_l -in $statwin.rs_scals_f \
                -row $r -column $c -padx 4 -sticky e
        grid $statwin.rs_s${i}tot_l  -in $statwin.rs_scals_f \
                -row [incr r] -column $c -padx 4 -sticky e
        grid $statwin.rs_s${i}rate_l -in $statwin.rs_scals_f \
                -row [incr r] -column $c -padx 4 -sticky e
    }
    set rstat(nsw) $ns
}

proc runstat_update_now { } {
    td_odb td_all_update "update parameters"
}

proc runstat_close { } {
    global statwin
    runstat_stop_imusr_log
    destroy [td_win_name $statwin]
}

proc runstat_ask_progress { } {
    global run statwin statpb autor
    if { $run(ImuSR) } { return }
    # Use different local variables for counts than on the autoruns page
    #puts "runstat_ask_progress when scts, autor(counts) = {$statpb(scts)}, {$autor(counts)}"
    set statpb(scts) $autor(counts)
    if { $autor(counts)%1000000 == 0 && $autor(counts) > 1 } {
        set statpb(scts) "[expr {$autor(counts)/1000000}]M"
    } elseif { $autor(counts)%100000 == 0 && $autor(counts) > 1 } {
        set statpb(scts) "[expr {$autor(counts)/1000000.0}]M"
    } else {
        set statpb(scts) $autor(counts)
    }
    set statpb(shst) [expr {$autor(counthist)>0 ? $autor(counthist) : ""}]
    bind $statwin.rs_progress_ask.e <Key-Return> {runstat_set_progress 1}
    bind $statwin.rs_progress_ask.e <Key-Escape> {runstat_set_progress 0}
    bind $statwin.rs_progress_ask.h <Key-Return> {runstat_set_progress 1}
    bind $statwin.rs_progress_ask.h <Key-Escape> {runstat_set_progress 0}
    $statwin.rs_progress_ask.e configure -takefocus 1
    $statwin.rs_progress_ask.h configure -takefocus 1
    focus $statwin.rs_progress_ask.e
    grid $statwin.rs_progress_ask
    raise $statwin.rs_progress_ask
}

proc runstat_set_progress { flag } {
    global statwin statpb mauto autor
    if { $flag } { # apply
	#puts "runstat_set_progress "
        set c 0 
        set n [scan $statpb(scts) " %f %s" c u]
        if { $n == 2 && ($u == "M" || $u == "m") } {
            set c [expr {1000000*$c}]
        }
        if { $c > 0 && $c < 500 } { 
            set c [expr {$c*1000000}]
        }
        set mauto(counts) [expr { round($c) }]
        set mauto(counthist) 0
        scan $statpb(shst) %d mauto(counthist)
        set autor(counts) $mauto(counts)
        set autor(counthist) $mauto(counthist)
        catch odb_set_autor_counts
    }
    catch {
        $statwin.rs_progress_ask.e configure -takefocus 0
        $statwin.rs_progress_ask.h configure -takefocus 0
        grid remove $statwin.rs_progress_ask
        runstat_update
    }
}

# Here we update the (scrolling) text window showing the ImuSR data points.
# The points are read from a plain text log file, identified in the ODB, as
# reproduced in $idef(log_file). The file we have open and are reading is
# $rstat(log_file).
# Returns a success code: 
#   0 Failure to access the log file
#   1 See log file, but no new data
#   2 Got new data

set idef(log_file) {}
set rstat(log_file) {}
set rstat(log_handle) {}
set rstat(scrolltim) 0 
set rstat(scrollsiz) 0

proc runstat_update_scroll { } {
    global statwin rstat idef run

    set code 1
    set siz 0
    set tim 0

    # puts "Update scroll: $idef(log_file), $rstat(log_file), $rstat(log_handle)."  
    set len -1
    set line ""

    # If there is a file opened ...
    if { [string length $rstat(log_handle)] } {
        # If file name changed, we will close it (below)
        if { [string compare $idef(log_file) $rstat(log_file)] } {
            #puts "file name changed, we will close it (below)"
            set rstat(log_file) {}
        }

        # If we have the correct file ready, try to read from it.
        if { [string length $rstat(log_file)] } {
            set siz [file size $rstat(log_file)]
            set tim [file mtime $rstat(log_file)]
            if { [ catch { gets $rstat(log_handle) line } len ] } {
                #puts "Error during read. Must close $rstat(log_file) (below)"
                #puts "Message is: $len"
                set rstat(log_file) {}
            } elseif { $len < 0 } {
                #puts "No new data: $len:  $line"
                # No new data in file.  Check if file has contracted, or if 
                # modifications are not being seen.  If so, we will close it (below)
                if { $tim > $rstat(scrolltim) + 30 || $siz < $rstat(scrollsiz) } {
                    set rstat(log_file) {}
                } else { # nothing wrong with file, we just read it all
                    return $code
                }
            } else { # Got a new line from file.
                # ??? Need to work on good formatting of numbers.  What about the list 
                # being too long?  Should it wrap or truncate or stay?  It will wrap
                # if it goes beyond the maximum width.  For now, assume the line is well
                # formatted already.
                #puts "Read a line from the file. toler memory = $rstat(toler_memory)"
                if { $rstat(toler_memory) > 0 } {
                    $statwin.rs_text insert end "\n$line" highlight
                } else {
                    $statwin.rs_text insert end "\n$line"
                }
                set rstat(toler_memory) 0
                $statwin.rs_text see end
                set rstat(scrolltim) $tim
                set rstat(scrollsiz) $siz
                set code 2
            }
        }

        # Now close file, if signalled by a null name
        if { [string length $rstat(log_file)] == 0 } {
            catch { close $rstat(log_handle) }
            set rstat(log_handle) {}
        }
    }

    # if we have a file name, but no file open ...
    if { [string length $rstat(log_handle)] == 0 && [string length $idef(log_file)] > 0 } {
        # Clear the text.
        # Some versions of Tcl/Tk are very slow deleting all the text in a large
        # text widget using the normal [$statwin.rs_text delete 1.0 end].  Here we will
        # delete in smaller chunks.
        set nl [lindex [split [$statwin.rs_text index end] .] 0]
        while {$nl >= 150} {
            $statwin.rs_text delete 1.0 100.end
            incr nl -100
        }
        $statwin.rs_text delete 1.0 end
        $statwin.rs_text configure -wrap none -width 60
        #
        #puts "we have a file name, but no file open ..."
        # Attempt to open file
        if { [catch { set rstat(log_handle) [open $idef(log_file) r] }] } {
            puts "Failed to open log file $idef(log_file)"
            set rstat(log_file) {}
        }
        if { [string length $rstat(log_handle)] } { # opened anew.
            #puts "Successfully opened log file $idef(log_file)"
            set rstat(log_file) $idef(log_file)
            # Update column headings
            runstat_text_headings
            # Read any existing lines all at once
            set lines [read -nonewline $rstat(log_handle)]
            # If no points yet, set toler_memory to flag the beginning of a run
            if { [llength $lines] } {
                set rstat(toler_memory) 0
            } else {
                set rstat(toler_memory) -1
            }
            # Adjust width of text area according to first line read, if any,
            # or calculated based on the number of hists.  This simple procedure
            # REQUIRES a monospace font.  Handling a variable font requires
            # [font measure], and careful placement of each number to maintain
            # columns.
            set len [string first "\n" $lines]
            if { $len < 0 } {
                set len [string length $lines]
            }
            if { $len < 5 + 9*[llength $idef(hist_titles)] } { # No full line yet.
                set len [expr {5 + 9*[llength $idef(hist_titles)]}]
            }
            if { $len > $::PREF::textWidthMax } {
                $statwin.rs_text configure -width $::PREF::textWidthMax -wrap word
            } else {
                $statwin.rs_text configure -width $len -wrap none
            }
            # Display the text lines (if any)
            $statwin.rs_text insert 1.0 $lines
            $statwin.rs_text see end
            # Record file status for comparison next time
            set rstat(scrolltim) [file mtime $rstat(log_file)]
            set rstat(scrollsiz) [file size $rstat(log_file)]
            set code 2
        }
    }

    # If there is no log file, exit with failure code
    if { [string length $rstat(log_handle)] == 0 } {
        #puts "No log file -- exit"
        set rstat(log_file) ""
        return 0
    }
    return $code
}

proc runstat_text_headings { } {
    global idef statwin rstat
    # puts " update label above scrolling text"
    # Set the wrapping width of the column heads
    catch {
        odb_get_idef 
        # for resizes, use the resized-size for wrapping
        set ww [expr { [winfo width $statwin.rs_htitles_l] - 9 }]
        if { $ww < 100 } { # for initialization when there's no width yet, use the preference value
            set pitch [expr { 0.1*[font measure muMonoFont {1234567890}] }]
            set ww [expr { int( $::PREF::textWidthMax*$pitch + 0.9 ) }]
        }
        $statwin.rs_htitles_l configure -wraplength $ww
    }
    set heads " num "
    foreach h $idef(hist_titles) {
        append heads [format {%8.8s } $h]
    }
    set rstat(text_headings) $heads
}

proc runstat_stop_imusr_log { } {
    global rstat
    after cancel $rstat(after_key)
    set rstat(after_key) {}
    if { [string length $rstat(log_handle)] } { close $rstat(log_handle) }
    set rstat(log_file) {}
    set rstat(log_handle) {} 
}

set rstat(after_key) ""
set rstat(previous_total) 0


set tcl_precision 15

if { [catch {set statwin}] } {
    set statwin ""
}

if { $statwin == "" } {
    # stand-alone testing, so start immediately!
    if { [info proc status_ui] == "" } {
	uplevel \#0 source [file join [file dirname [info script]] status.ui.tcl]
    }
    runstat_initialize
    set idef(hist_titles) "A B C D"
}


