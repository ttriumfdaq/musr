#! /bin/sh
# the next line restarts using wish \
exec wish "$0" "$@"
if { [catch { set mui_utils_loaded }] } {
    source "mui_utils.tcl"
}
# interface generated by SpecTcl version 1.2 from /home/asnd/SpecTcl/mui/sweeps.ui
#   root     is the parent window for this user interface

proc sweeps_ui {root args} {

	# this treats "." as a special case

	if {$root == "."} {
	    set base ""
	} else {
	    set base $root
	}
    
	frame $base.frame#1

	frame $base.frame#2 \
		-borderwidth 2 \
		-height 2 \
		-relief sunken

	frame $base.frame#3 \
		-borderwidth 2 \
		-height 2 \
		-relief sunken

	label $base.sw_head_l \
		-borderwidth 3 \
		-text {Sweeps Configuration}
	catch {
		$base.sw_head_l configure \
			-font muTitleFont
	}

	label $base.sw_sweep_dev_l \
		-text {Sweep device:}

	menubutton $base.sw_sweep_dev_mb \
		-highlightcolor $PREF::brightcol \
		-highlightthickness 1 \
		-justify left \
		-menu "$base.sw_sweep_dev_mb.menu" \
		-pady 4 \
		-relief raised \
		-takefocus 1 \
		-textvariable midef(sweep_device_label)

	button $base.sw_range_inter_b \
		-padx 4 \
		-pady 3 \
		-takefocus 1 \
		-text Int/range

	button $base.sw_details_b \
		-command idef_set_details \
		-padx 4 \
		-pady 3 \
		-takefocus 1 \
		-text Details

	label $base.sw_htitle_l \
		-text {Label for scan values:}

	entry $base.sw_htitle_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(scan_title) \
		-width 10

	label $base.sw_min_l \
		-text {Set Limits:          Min:}

	entry $base.sw_min_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(min) \
		-validate all \
		-validatecommand "valmidefNum f {} {} %d %s %P $base.sw_min_e %v %V midef(min)" \
		-width 10

	label $base.sw_max_l \
		-text { Max:}

	entry $base.sw_max_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(max) \
		-validate all \
		-validatecommand "valmidefNum f {} {} %d %s %P $base.sw_max_e %v %V midef(max)" \
		-width 10

	label $base.sw_settle_l \
		-text {Step settle time:}

	entry $base.sw_settle_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_settle) \
		-validate all \
		-validatecommand "valmidefNum d 0 100000 %d %s %P $base.sw_settle_e %v %V midef(sweep_settle)" \
		-width 10

	label $base.sw_ms_l \
		-text ms

	label $base.sw_number_l \
		-text {Number of sweeps:}

	entry $base.sw_num_sweeps_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(num_sweeps) \
		-validate all \
		-validatecommand "valmidefNum d 0 1000 %d %s %P $base.sw_num_sweeps_e %v %V midef(num_sweeps)" \
		-width 10

	label $base.sw_continuous_l \
		-text { 0 for continuous}

	label $base.sw_from_l \
		-text {Sweep from:}

	entry $base.sw_from_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_from) \
		-validate all \
		-validatecommand "valmidefNum f \$midef(min) \$midef(max) %d %s %P $base.sw_from_e %v %V midef(sweep_from)" \
		-width 15

	label $base.sw_from_un_l \
		-padx 3 \
		-textvariable midef(sweep_units)

	label $base.sw_to_l \
		-text {Sweep to:}

	entry $base.sw_to_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_to) \
		-validate all \
		-validatecommand "valmidefNum f \$midef(min) \$midef(max) %d %s %P $base.sw_to_e %v %V midef(sweep_to)" \
		-width 15

	label $base.sw_to_un_l \
		-padx 3 \
		-textvariable midef(sweep_units)

	label $base.sw_step_l \
		-text {Sweep step:}

	entry $base.sw_step_e \
		-background $PREF::entrybg \
		-cursor {} \
		-textvariable midef(sweep_incr) \
		-validate all \
		-validatecommand "valmidefNum f \$midef(min) \$midef(max) %d %s %P $base.sw_step_e %v %V midef(sweep_incr)" \
		-width 15

	label $base.sw_step_un_l \
		-padx 3 \
		-textvariable midef(sweep_units)

	menu $base.sw_sweep_dev_mb.menu \
		-tearoff 0


	# Add contents to menus

	$base.sw_sweep_dev_mb.menu add command\
		-command {idef_sweep_device DAC {}}\
		-label {DAC       }
	$base.sw_sweep_dev_mb.menu add separator

	# Geometry management

	grid $base.frame#1 -in $root	-row 2 -column 3  \
		-columnspan 3 \
		-sticky ew
	grid $base.frame#2 -in $root	-row 3 -column 2  \
		-columnspan 4 \
		-pady 8 \
		-sticky ew
	grid $base.frame#3 -in $root	-row 7 -column 2  \
		-columnspan 4 \
		-pady 8 \
		-sticky ew
	grid $base.sw_head_l -in $root	-row 1 -column 2  \
		-columnspan 4 \
		-ipadx 10 \
		-ipady 7 \
		-pady 3 \
		-sticky ew
	grid $base.sw_sweep_dev_l -in $root	-row 2 -column 2  \
		-pady 2 \
		-sticky w
	grid $base.sw_sweep_dev_mb -in $base.frame#1	-row 1 -column 1  \
		-sticky ew
	grid $base.sw_range_inter_b -in $base.frame#1	-row 1 -column 2  \
		-sticky e
	grid $base.sw_details_b -in $base.frame#1	-row 1 -column 3  \
		-sticky e
	grid $base.sw_htitle_l -in $root	-row 4 -column 2  \
		-sticky w
	grid $base.sw_htitle_e -in $root	-row 4 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_min_l -in $root	-row 5 -column 2  \
		-sticky w
	grid $base.sw_min_e -in $root	-row 5 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_max_l -in $root	-row 5 -column 4  \
		-sticky w
	grid $base.sw_max_e -in $root	-row 5 -column 5  \
		-pady 2 \
		-sticky ew
	grid $base.sw_settle_l -in $root	-row 6 -column 2  \
		-sticky w
	grid $base.sw_settle_e -in $root	-row 6 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_ms_l -in $root	-row 6 -column 4  \
		-sticky w
	grid $base.sw_number_l -in $root	-row 8 -column 2  \
		-sticky w
	grid $base.sw_num_sweeps_e -in $root	-row 8 -column 3  \
		-pady 2 \
		-sticky ew
	grid $base.sw_continuous_l -in $root	-row 8 -column 4  \
		-columnspan 2 \
		-sticky w
	grid $base.sw_from_l -in $root	-row 9 -column 2  \
		-sticky w
	grid $base.sw_from_e -in $root	-row 9 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.sw_from_un_l -in $root	-row 9 -column 5  \
		-sticky w
	grid $base.sw_to_l -in $root	-row 10 -column 2  \
		-sticky w
	grid $base.sw_to_e -in $root	-row 10 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.sw_to_un_l -in $root	-row 10 -column 5  \
		-sticky w
	grid $base.sw_step_l -in $root	-row 11 -column 2  \
		-sticky w
	grid $base.sw_step_e -in $root	-row 11 -column 3  \
		-columnspan 2 \
		-pady 2 \
		-sticky ew
	grid $base.sw_step_un_l -in $root	-row 11 -column 5  \
		-sticky w

	# Resize behavior management

	grid rowconfigure $root 1 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 2 -weight 0 -minsize 30 -pad 0
	grid rowconfigure $root 3 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 4 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 5 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 6 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 7 -weight 0 -minsize 2 -pad 0
	grid rowconfigure $root 8 -weight 0 -minsize 9 -pad 0
	grid rowconfigure $root 9 -weight 0 -minsize 8 -pad 0
	grid rowconfigure $root 10 -weight 0 -minsize 8 -pad 0
	grid rowconfigure $root 11 -weight 0 -minsize 10 -pad 0
	grid rowconfigure $root 12 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $root 1 -weight 0 -minsize 16 -pad 0
	grid columnconfigure $root 2 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $root 3 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 4 -weight 0 -minsize 30 -pad 0
	grid columnconfigure $root 5 -weight 1 -minsize 30 -pad 0
	grid columnconfigure $root 6 -weight 0 -minsize 16 -pad 0

	grid rowconfigure $base.frame#1 1 -weight 0 -minsize 2 -pad 0
	grid columnconfigure $base.frame#1 1 -weight 1 -minsize 34 -pad 0
	grid columnconfigure $base.frame#1 2 -weight 1 -minsize 85 -pad 0
	grid columnconfigure $base.frame#1 3 -weight 0 -minsize 2 -pad 0
# additional interface code

# end additional interface code

}


# Allow interface to be run "stand-alone" for testing

catch {
    if [info exists embed_args] {
	# we are running in the plugin
	sweeps_ui .
    } else {
	# we are running in stand-alone mode
	if {$argv0 == [info script]} {
	    wm title . "Testing sweeps_ui"
	    sweeps_ui .
	}
    }
}
