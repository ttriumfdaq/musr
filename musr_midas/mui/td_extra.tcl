#   td_extra.tcl
#   Procedures for the main td window.

#   $Log: td_extra.tcl,v $
#   Revision 1.63  2016/09/29 04:27:37  asnd
#   Autorun started under screen
#
#   Revision 1.62  2016/09/06 22:14:56  asnd
#   Add logging to screen sessions
#
#   Revision 1.61  2015/09/29 01:16:29  asnd
#   Check hist len when starting run, to avoid buffer overflow in fev680
#
#   Revision 1.60  2015/09/28 00:27:53  asnd
#   Fix persistent cancel button
#
#   Revision 1.59  2015/09/26 06:29:43  asnd
#   Different hits checking; xterm color for camp; CAEN console; logfile dir yearly update; reapply_gates
#
#   Revision 1.58  2015/05/18 21:24:11  asnd
#   Proper backgrounding of tasks.
#
#   Revision 1.57  2015/05/18 21:18:35  asnd
#   M20 beamline control for M20C&D
#
#   Revision 1.56  2015/05/10 04:22:50  asnd
#   Start other clients in screen sessions.
#
#   Revision 1.55  2015/05/02 01:19:47  asnd
#   Add experiments for mserver start
#
#   Revision 1.54  2015/04/18 03:32:24  asnd
#   Improve monitoring of frontend startup.
#
#   Revision 1.53  2015/04/17 23:51:23  asnd
#   Tiny change
#
#   Revision 1.52  2015/03/20 00:17:21  suz
#   changes by Donald to run the new VMIC frontends
#
#   Revision 1.51  2013/10/30 05:30:19  asnd
#   Refinements to validations and prompting; beamline control changed
#
#   Revision 1.50  2008/10/05 07:31:35  asnd
#   Add CAEN HV in "extras" menu
#
#   Revision 1.47  2007/11/21 23:43:29  asnd
#   Revise startup order and client shutdown
#
#   Revision 1.46  2007/10/03 02:41:56  asnd
#   Better font for Camp's ncurses; other small changes.
#
#   Revision 1.45  2006/11/22 21:06:18  asnd
#   Do initial management of imusr labels
#
#   Revision 1.44  2006/09/22 03:00:16  asnd
#   Menu changes
#
#   Revision 1.43  2006/07/08 01:33:18  asnd
#   Configurable mu character for window title
#
#   Revision 1.42  2006/05/26 17:14:01  asnd
#   Accumulated changes over winter shutdown.
#
#   Revision 1.41  2005/07/13 04:02:05  asnd
#   Extend radiomenu for dynamic lists; use it for automatic header selection.
#
#   Revision 1.40  2005/07/02 05:16:42  asnd
#   Change display of some parameters in autoruns window
#
#   Revision 1.39  2005/06/22 15:35:45  asnd
#   menus, change_titles, prompting, more error reporting,
#
#   Revision 1.38  2004/11/10 13:04:45  asnd
#   Enjoy Suzannah's improvements to mdarc when rebooting; and add "abrupt stop" run control.
#
#   Revision 1.37  2004/11/07 08:21:53  asnd
#   Rewrite client checks; watch for multiple tasks.  Allow FE reboot during
#   unattended autoruns & deal with run termination.
#
#   Revision 1.36  2004/10/29 06:54:51  asnd
#   Periodically check for no counts in histograms.  Trap errors on write data
#
#   Revision 1.35  2004/05/18 01:34:35  asnd
#   Use new [midas scl] command to get client list; use [hostmatch] to match host names.
#
#   Revision 1.34  2004/04/27 11:50:23  asnd
#   Audible alert when run ends asynchronously, or gets enough counts
#
#   Revision 1.33  2004/04/21 05:14:29  asnd
#   Prevent double-reboot when change experiment
#
#   Revision 1.32  2004/04/15 02:55:28  asnd
#   Alter check for client re-start
#
#   Revision 1.31  2004/04/10 07:00:07  asnd
#   Add auto-run control
#
#   Revision 1.30  2004/03/11 05:20:32  asnd
#   Initial work on autoruns
#
#   Revision 1.29  2004/02/20 23:08:06  asnd
#   Single musr Midas experiment.  Handle year changes.
#
#   Revision 1.28  2003/11/24 05:43:26  asnd
#   Handle year changes.
#
#   Revision 1.27  2003/11/16 05:32:00  asnd
#   Initialize "renormalize" and "write" buttons as hidden/disabled.
#
#   Revision 1.26  2003/11/04 16:02:24  asnd
#   Tearoff run menu labelling
#
#   Revision 1.25  2003/10/30 13:08:45  asnd
#   Delete log-window when change experiments
#
#   Revision 1.24  2003/10/29 14:54:52  asnd
#   Proper hiding vs disabling of button.
#
#   Revision 1.23  2003/10/16 07:37:58  asnd
#   Fix erasure of auto-headers
#
#   Revision 1.22  2003/10/16 03:27:28  asnd
#   Initialise titles variables
#
#   Revision 1.21  2003/10/15 09:28:28  asnd
#   Add "yield" operation for shutdown; changes to client checks; other small changes.
#
#   Revision 1.20  2003/10/01 09:56:12  asnd
#   Minor changes (use "other" instead of "field" for field sweeps)
#
#   Revision 1.19  2003/09/26 00:39:03  asnd
#   Recent gui changes (sweep details, ggl pulser...)
#
#   Revision 1.18  2003/09/19 06:23:27  asnd
#   ImuSR and many other revisions.
#
#   Revision 1.16  2002/11/16 01:50:31  asnd
#   Fix hits check.
#
#   Revision 1.15  2002/11/14 00:07:32  asnd
#   Refresh HM active display after run start/stop.
#
#   Revision 1.14  2002/10/30 08:10:18  asnd
#   Quick plot update after acq change
#
#   Revision 1.13  2002/10/01 05:49:37  asnd
#   Improve checking hits for invalid/overflow
#
#   Revision 1.12  2002/09/25 04:42:11  asnd
#   Check musr_config client
#
#   Revision 1.11  2002/09/21 04:45:42  asnd
#   Various recent updates
#
#   Revision 1.10  2002/08/23 09:05:55  asnd
#   archive data on write; unstick bad transition-in-progress;
#   de-parent because of failed timouts on message boxes.
#
#   Revision 1.9  2002/07/27 09:28:04  asnd
#   Start implementation on hit-rate diagnostics.
#
#   Revision 1.8  2002/07/27 06:31:58  asnd
#   Periodic check on midas clients
#
#   Revision 1.7  2002/07/24 23:43:44  asnd
#   Add beamline name to main title
#
#   Revision 1.6  2002/07/23 23:48:23  asnd
#   Archiver name change mdarc_musr -> mdarc
#
#   Revision 1.4  2002/07/19 12:15:16  asnd
#   Accept kG (for real -- remember trim)
#
#   Revision 1.3  2002/07/19 11:41:55  asnd
#   zero camp at begin run; handle kG units for field
#
#   Revision 1.2  2002/06/15 01:05:24  asnd
#   Alter mdarc tests
#
#   Revision 1.1  2002/06/12 01:26:39  asnd
#   Starting versions of Mui files.
#

global muisrc
set muisrc [file dirname [info script]]

if { [catch { set mui_utils_loaded }] } {
    source [file join $muisrc mui_utils.tcl]
}


proc td_initialize { } {
#   Draw the main window.

    global tdwin modewin midas run

    bind . <Destroy> {td_destroy %W}

    td_odb odb_get_general "get Midas ODB"
    td_odb odb_get_runinfo "get run parameters"

    set tw [td_win_name $tdwin]
    set t "${PREF::muchar}SR Run Control"
    if { [string length $midas(beamline)] } {
        append t " on $midas(beamline)"
    }
    .td_wintitle_l configure -text $t
    wm title $tw $t

    if { $run(ImuSR) } {
        mui_display_imusr
    } else {
        mui_display_tdmusr
    }
    td_make_menus

    td_bind_help td_run_mb "Run control menu (Alt-R)"
    td_bind_help td_show_mb "Menu: Display various things (Alt-S)"
    td_bind_help td_change_mb "Menu: Change various things (Alt-C)"
    td_bind_help td_extras_mb "Menu: Extra utilities/parameters (Alt-E)"
    td_bind_help td_exit_b "Quit this run-control interface program (Alt-Q)"

    bind $tw <Alt-q> "$tdwin.td_exit_b invoke ; break"
    bind $tw <Alt-o> "$tdwin.td_tit_ok_b invoke ; break"
    bind $tw <Alt-v> "$tdwin.td_tit_rest_b invoke"
    bind $tw <Alt-w> "$tdwin.td_write_b invoke"
    bind $tw <Alt-n> "$tdwin.td_renorm_b invoke"

    titles_end_change
    hideButton $tdwin.td_write_b -disabledforeground $::PREF::bgcol
    hideButton $tdwin.td_renorm_b -disabledforeground $::PREF::bgcol
    td_bind_help td_write_b ""
    td_bind_help td_renorm_b ""

    # start the repeating handler that calls "midas yield" function
    after 5000 mui_heartbeat

    # Begin with a quick update.  Check Midas clients soon after
    set midas(clt_checked_at) 0
    update
    catch {
        odb_init_acqrecord
        td_all_update
        td_make_menus
        titles_end_change
    }
    # Second initial update moved to musrrc.tcl
}

# Run-control functions

proc td_run { command } {
    global tdwin run midas mmode mrig rtitles rigwin

    if { [mui_check_transition] } { return }

    switch -- $command {
        begin {
            if { $mrig(changing) } {
                beep
                switch [ timed_messageBox -timeout 30000 \
                        -type okcancel -default ok -icon question \
                        -message "Rig is being changed!\nBegin run anyway?" \
                        -title "Begin run anyway?" ] {
                    cancel { return }
                    default { }
                }
            }
            while { $mmode(changing) } {
                beep
                switch [ timed_messageBox -timeout 30000 \
			-type yesnocancel -default yes -icon question \
                        -message "Mode is being changed!\nApply changes (yes/no) or cancel run?" \
                        -title "Begin Run - mode" ] {
                    yes {# Confirm mode settings
                        mode_apply_change mode_end_change
                    }
                    no {# Leave mode changing
                        break
                    }
                    cancel {# Cancel run begin
                        return
                    }
                }
            }

            catch td_check_hlen

            if { [mui_check_clients] } {
                puts "Client check delays run begin"
                after 1000 {td_run begin}
                return
            }

            if { $run(confirm_stop) >= 3 && $run(test_mode) } {
                beep
                if { [timed_messageBox -timeout 25000 -icon question -type yesno -default yes \
                          -message "Begin a test run, where data is not archived?" -title "Confirm test" ] == "no" } {
                    return
                }
            }

            mui_check_year

            if { !$rtitles(changing) } { titles_begin_change }
            td_zero camp
            td_odb odb_begin_run "start run"
            after 100 td_monitor_start [clock seconds] 20 1000
            td_act_cancelb show
        }
        end {
            if { $run(confirm_stop) >= 3 } {
                beep
                if { [timed_messageBox -timeout 15000 -icon question -type okcancel -default ok \
                          -message "OK to end run?" -title "Confirm end" ] == "cancel" } {
                    return
                }
            }
            if { $rtitles(changing) } {
                set ans "yes"
                if { $run(confirm_stop) >=1 } {
                    beep
                    set ans [ timed_messageBox -timeout 30000 -icon question \
                        -type yesnocancel -default yes \
                        -message "Run titles are still being edited!\nApply titles before ending run?" \
                        -title "Confirm Titles" ]
                }
                switch $ans {
                    no {# Reject titles, but end run
                        titles_begin_change
                        titles_OK
                    }
                    cancel {# Cancel ending of run
                        return
                    }
                    default {# Confirm titles,
                        titles_OK
                    }
                }
                # The end-run perl scripts already perform the run archival; no need here.
            }
            td_odb odb_end_run "end run"
        }
        kill {
            if { $run(confirm_stop) >= 1 && \
                     $run(in_progress) && !$run(test_mode) && \
                     [clock seconds]-$run(start_sec) > 200 && \
                     $run(hist_total) > 99999 
             } then {
                beep
                if { [timed_messageBox -timeout 30000 -type yesno -default no -icon question \
                          -message "Really kill run $run(number)?\nAll data will be lost!" \
                          -title "Confirm Kill" ] == "no" } {
                    return
                }
            }
            titles_OK
            td_odb odb_kill_run "kill run"
        }
        quickstop {
            if { $run(confirm_stop) >= 2 } {
                beep
                if { [timed_messageBox -timeout 30000 -type yesno -default no -icon question \
                          -message "Really stop run $run(number),\nwithout finalizing or archiving?" \
                          -title "Confirm Quick Stop" ] == "no" } {
                    return
                }
            }
            titles_OK
            td_odb odb_abrupt_stop "stop run"
        }

        write {
            mui_write_data 0 30 500 $run(last_file)
        }
        pause {
            odb_pause_run
        }
        continue {
            odb_resume_run
        }
    }
    td_all_update
    td_make_menus
    after 2000 { td_background_update }
    after 6000 { set clogv(PrevGraphUp) 0 ; td_background_update }
    after 15000 {
        set clogv(PrevGraphUp) 0
        td_background_update
        catch { odb_hmdisplay 0 ; set hmutil(display_active) }
    }
}

#   Do repeated updates while run is changing state
proc td_monitor_start { from for every } {
    global run
    catch { odb_get_runinfo }

    set now [clock seconds]
    if { $run(in_progress) && !$run(transition) } {
        set run(starting) 0
        after 3000 {
            td_all_update
            td_make_menus
        }
	return
    }
    if { $now < $from + $for && $run(bg_start_out) == "" } {
	after $every [list td_monitor_start $from $for $every]
    } else {
	set run(starting) 0
	if { $run(transition) || !$run(in_progress) } {
	    # Timeout on begin run
	    mui_handle_start_error { after 0 td_run begin }
	}
	td_all_update
	td_make_menus
    }
}

# Handle failure to start run.  Normally just an error message, but one/some
# errors may be fixed up.  If an error can be fixed up, then the run will
# be restarted using the "$restart" script parameter.

proc mui_handle_start_error { restart } {
    global run
    set run(starting) 0
    # stop bgexec-uting odbedit, if it hasn't terminated, by setting status (see blt::bgexec)
    set run(bg_start_stat) ""
    set out $run(bg_start_out)
    # Remove cancel button
    td_act_cancelb hide
    # Handle specific type(s) of failure specially:
    if { [regexp {highest run no. \((\d+)\) is not the most recent \((\d+)\)} $out -> highest latest] } {
	mui_filetime_error $highest $latest $restart
    # add elseif's here for other handlers of specific errors
    } else { # The usual error report:
        beep
	timed_messageBox -timeout 40000 -type ok -icon error \
	    -message "There was a problem starting the run\n(still in transition).\n$out" \
	    -title "Failed run start"
    }
    set run(starting) 0
}

proc mui_filetime_error { highest latest restart } {
    global midas
    set hf [file join $midas(data_dir) [format %.6d.msr $highest]]
    puts "Run number $latest appears to be more recent than run $highest."
    puts "[file mtime $lf] vs [file mtime $hf]"
    beep
    switch [timed_messageBox -timeout 44000 -type yesno -default yes -icon warning \
		-message "Run number $latest appears to be more recent than run $highest.
Was run $highest really the last previous run?" \
		-title "Runs out of order" ] {
	yes {
	    td_odb [list file mtime $hf [clock seconds]] "update (\"touch\") file $hf"
	    after idle $restart
	}
    }
}

proc td_testmode { val } {
    global run

    if { $run(confirm_stop) >= 3 && $val == "test" } {
        beep
        if { [timed_messageBox -timeout 25000 -icon question -type yesno -default yes \
                  -message "Switch to test mode, where data is not archived?" -title "Confirm test" ] == "no" } {
            return
        }
    }
    td_odb "odb_set_run_type $val" "set $val run type"

    td_all_update
    td_make_menus
    # Force update of any Camp plot
    after 2000 { set clogv(PrevGraphUp) 0 ; td_background_update }
    after 8000 { set clogv(PrevGraphUp) 0 ; td_background_update }
}

# Check if a midas transition is in progress, and query user for what to do.
# It is a bit muddled, but...
#   abort:  abandons current action
#   retry:  retries current action (in case transition has completed)
#   ignore: "ignore" previous transition action by clearing the
#           transition flag, and then continuing with our current action.
# Without user intervention, will do one retry, then ignore.
# Returns boolean for "transition in progress"

proc mui_check_transition { } {
    global run

    set def retry
    while 1 {
        catch { odb_get_runinfo }
        if { $run(transition) } {
	    beep
            switch [ timed_messageBox -timeout 7000 \
			 -type abortretryignore -default $def -icon warning \
			 -message "A transition is already in progress.\nWaiting to clear..." \
			 -title "Transition is in progress" ] {
                abort {
                    return 1
                }
                retry { 
                    set def ignore
                    continue
                }
                ignore {
                    catch {
                        odb_get_runinfo 
                        if { $run(transition) } {
                            odb_cancel_transition
        }   }   }   }   }
        return 0
    }
}

# last-second check of histogram lengths before starting run. Ensure hist lengths fit
# within allocated space, with some headroom. 
proc td_check_hlen { } {
    global mode
    odb_get_mode
    set bytesbin [expr { $mode(maxbins) < 2 * $mode(numhist) * [next_pow2 $mode(hlen)] ? 2 : 4}]
    if { ($mode(hlen)+256) * $bytesbin * $mode(numhist) > 2 * $mode(maxbins) } {
        set nhl [expr {256 * int((2 * $mode(maxbins) - 128)/($bytesbin * $mode(numhist))/256 ) }]
        puts "Reduced hist length from $mode(hlen) to $nhl ($mode(numhist) hists, $bytesbin bytes per bin)."
        set mode(hlen) $nhl
        odb_set_mode
    }
}

#  mui_write_data  --  Attempt to write (save) data file and archive it
#  to the permanent database.  This should be attached to the "write data"
#  button and menu items.  It will call itself repeatedly (counted by 
#  $iter, up to $max, every $delay ms) monitoring when the new file is 
#  written (when the saved filename changes from $file).
#
#  When a run is paused, it cannot be re-saved, but we will perform
#  the archival anyway.
#
proc mui_write_data { iter max delay file } {
    global run tdwin
    #puts "mui_write_data $iter $max $delay $file : $run(last_file)" 
    catch { odb_get_runinfo }
    if { $iter == 0 } {
        if { [winfo exists $tdwin.td_write_b] } {
            $tdwin.td_write_b configure -state disabled
        }
        if { $run(in_progress) && $run(paused) && [string length $run(last_file)] && !$run(test_mode) } {
            after 1000 td_archive_run
            beep
            timed_messageBox -timeout 15000 \
                    -type ok -default ok -icon info \
                    -message "Run is paused, so archiving last saved version" \
                    -title "Paused save"
            if { [winfo exists $tdwin.td_write_b] } {
                $tdwin.td_write_b configure -state normal
            }
            return
        }
        if { ![td_odb odb_save_data "save data file"] } {
	    # If we had a problem writing data, quit watching
            if { [winfo exists $tdwin.td_write_b] } {
                $tdwin.td_write_b configure -state normal
            }
            return
	}
    }

    # Watch until new file is written, or window closed, or timed out.
    if { [winfo exists $tdwin.td_write_b] } { 
        if { [string equal $file $run(last_file)] } {
            incr iter
            if { $iter < $max } {
                after $delay [list mui_write_data $iter $max $delay $file]
            } else {
                # File did not appear!  Restore write button
                $tdwin.td_write_b configure -state normal
                # Look for error message
                set sec [ expr { round($max*$delay*0.001)+10 }]
                catch {recent_midas_messages 1 $sec 20 mdarc} msg
                # Prompt for action
                beep
                switch [ timed_messageBox -timeout 20000 \
                        -type retrycancel -default cancel -icon warning \
                        -message "Data file not saved yet!\n$msg" \
                        -title "Write Failure" ] {
                    retry {
                        mui_write_data 0 $max $delay $run(last_file)
                    }
                }
                # Force an immediate check that mdarc is functioning
                set midas(clt_checked_at) 0
            }
        } else {
            $tdwin.td_write_b configure -state normal
            if { ! $run(test_mode) } {
                after 1000 td_archive_run
            }
        }
    }
}


proc td_make_menus { } {
    global run

#   remember run state so background updates may skip menu-building
    set run(menustate) "$run(state) $run(in_progress) $run(test_mode) $run(expertype) $run(paused) $::mode(dual_enabled) $::rig(num_scalers)"

    if { $run(ImuSR) } {
	imusr_make_run_menu
    } else {
	td_make_run_menu
    }
    mui_make_change_menu
    mui_make_show_menu
    td_make_extras_menu
    td_show_titles

}

# For the dynamic main menus, there are two possibilities for appearance:
#
# 1) The most obvious, and the original intention: to list all possibilities
#    in the menu, but toggle the state of entries between "normal" and "disabled".
#    The menus get very long then, and it is jarring to see wildly inappropriate
#    menu entries, even if they are greyed-out.
# 2) Only have active entries.  Instead of configuring the entries as status 
#    changes, we'll rebuild the menu with only the desired entries.
#
# For each of these, there are three possibilities for implementing the dynamic
# behaviour.
#
# A) If menu-button widgets had a "-command" option we could build or configure 
#    the menu as it is activated.  Actually, that feature CAN be implemented using
#    bind .mb <Activate> {build menu}
#    or 
#    .mb.menu -postcommand {build menu}
#    I hadn't known this at first though, and I still worry about delays in
#    building the menu. More importantly, menus wouldn't update while posted
#    (in use or torn off)
# B) Instead, we'll alter the menus whenever the run-state changes, either
#    through our action, or noticed from the odb.
# C) Or we can make a regular button whose command builds the menu and then
#    brings up an "independent" pop-up menu at the button location, e.g.,
#    tk_popup .td_run_mb.menu [winfo rootx .td_run_mb] [winfo rooty .td_run_mb]
#    This is pretty much like (A).
#
# The implementation here is 2B.
#
# We also handle activation/deactivation of the "Write Data" button here
# (It is really a short-cut to the menu item).

proc td_make_run_menu { } {
    global tdwin run mode rig

    # Set window title for any torn-off run menus:
    if { $PREF::menutearoff } { run_tearoff_title }

    # Enable/disable the "Write Data" button as appropriate
    set writeb_shown [string match raised [$tdwin.td_write_b cget -relief]]
    if { $writeb_shown != $run(in_progress) } {
        if { $run(in_progress) } {
            showButton $tdwin.td_write_b -disabledforeground $::PREF::disabledfg
            td_bind_help td_write_b "Write data to disk, and archive (Alt-W)"
        } else {
            hideButton $tdwin.td_write_b -disabledforeground $::PREF::bgcol
            td_bind_help td_write_b ""
        }
    }

    # Completely hide a disabled "renormalize" button
    if { [string compare "disabled" [$tdwin.td_renorm_b cget -state]] } {
        hideButton $tdwin.td_renorm_b
        td_bind_help td_renorm_b ""
    }

    # Rebuild run menu
    $tdwin.td_run_mb.menu delete 0 last
    $tdwin.td_run_mb configure -direction $PREF::menudirect

    if { ! $run(in_progress) } {

        $tdwin.td_run_mb.menu add command -label "Begin run" \
            -command {td_run begin} -underline 0 -state active

    } else {

        $tdwin.td_run_mb.menu add command -label "Write data" \
            -command {td_run write} -underline 0
        $tdwin.td_run_mb.menu add separator

        $tdwin.td_run_mb.menu add command -label "End run" \
            -command {td_run end} -underline 0 -state active
        if { $run(paused) } {
            $tdwin.td_run_mb.menu add command -label "Continue" \
                -command {td_run continue} -underline 0
        } else {
            $tdwin.td_run_mb.menu add command -label "Pause run" \
                -command {td_run pause} -underline 0
        }

        $tdwin.td_run_mb.menu add command -label "Kill run" \
            -command {td_run kill} -underline 0

        $tdwin.td_run_mb.menu add command -label "Abrupt Stop" \
            -command {td_run quickstop} -underline 7

        $tdwin.td_run_mb.menu add cascade -label "Zero ..." \
            -menu $tdwin.td_run_mb.menu.zero -underline 0
        $tdwin.td_run_mb.menu.zero delete 0 last
        $tdwin.td_run_mb.menu.zero add command -label "all data" \
            -command {td_zero all} 
        $tdwin.td_run_mb.menu.zero add command -label "histograms" \
            -command {td_zero hists}
        if { $mode(dual_enabled) } {
            $tdwin.td_run_mb.menu.zero add command -label "reference hists" \
                -command {td_zero reference}
        }
        if { $rig(num_scalers) > 0 } {
            $tdwin.td_run_mb.menu.zero add command -label "scalers" \
                -command {td_zero scalers}
        }
        $tdwin.td_run_mb.menu.zero add command -label "Camp statistics" \
            -command {td_zero camp}
    }

    $tdwin.td_run_mb.menu add separator

    $tdwin.td_run_mb.menu add command -label "Modify Titles" \
        -command titles_begin_change -underline 0

#    $tdwin.td_run_mb.menu add command -label "Logbook" \
#        -command {} -underline 0 -state disabled

    $tdwin.td_run_mb.menu add separator

    if { $run(test_mode) } {
        $tdwin.td_run_mb.menu add command -label "Acquire Real data" \
            -command {td_testmode real} -underline 0
    } else {
        $tdwin.td_run_mb.menu add command -label "Acquire Test data" \
            -command {td_testmode test} -underline 0
    }
}

proc mui_make_change_menu { } {
    global tdwin run

    $tdwin.td_change_mb.menu delete 0 last
    $tdwin.td_change_mb.menu configure -tearoff $PREF::menutearoff
    $tdwin.td_change_mb configure -direction $PREF::menudirect

    if { $run(test_mode) } {
        $tdwin.td_change_mb.menu add command -label "Acq type to Real" \
                -command {td_testmode real} -underline 0 -state active
    } else {
        $tdwin.td_change_mb.menu add command -label "Acq type to Test" \
                -command {td_testmode test} -underline 0 -state active
    }
    $tdwin.td_change_mb.menu add command -label "Titles" \
            -command titles_begin_change -underline 0

    if { $run(ImuSR) } {
	$tdwin.td_change_mb.menu add command -label "Sweep range" \
	    -command {iq_initialize sweep} -underline 0
	$tdwin.td_change_mb.menu add command -label "Rate tolerance" \
	    -command {iq_initialize toler} -underline 0
    } else {
	$tdwin.td_change_mb.menu add command -label "Mode" \
	    -command {mode_initialize ; mode_begin_change} -underline 0
    }

    if { $run(in_progress) } {
    } else {
        if { $run(ImuSR) } {
            $tdwin.td_change_mb.menu add command -label "Modulation" \
                    -command {iq_initialize mod} -underline 0
            $tdwin.td_change_mb.menu add command -label "Preset counts" \
                    -command {iq_initialize preset} -underline 0
            $tdwin.td_change_mb.menu add separator
            $tdwin.td_change_mb.menu add command -label "Define everything" \
                    -command {isetup_initialize} -underline 0
            $tdwin.td_change_mb.menu add separator
            $tdwin.td_change_mb.menu add command -label "Experiment type to TD-\u00b5SR" \
                    -command {mui_set_experiment TD-\u00b5SR} -underline 0
        } else {
            $tdwin.td_change_mb.menu add command -label "Rig (hardware config)" \
                    -command {rig_initialize ; rig_begin_change} -underline 0
            $tdwin.td_change_mb.menu add separator
            $tdwin.td_change_mb.menu add command -label "Experiment type to I-\u00b5SR" \
                    -command {mui_set_experiment I-\u00b5SR} -underline 0
        }
    }
    $tdwin.td_change_mb.menu add separator
    $tdwin.td_change_mb.menu add command -label "Old run titles" \
            -command {change_old_titles} -underline 0
}

proc mui_make_show_menu { } {
    global tdwin run

    $tdwin.td_show_mb.menu delete 0 last
    $tdwin.td_show_mb.menu configure -tearoff $PREF::menutearoff
    $tdwin.td_show_mb configure -direction $PREF::menudirect

    $tdwin.td_show_mb.menu add command -label "Status" \
            -command {runstat_initialize} -underline 0 -state active

    if { $run(ImuSR) } {

        $tdwin.td_show_mb.menu add command -label "I-\u00b5SR setup" \
                -command {isetup_initialize} -underline 0

    } else {

        $tdwin.td_show_mb.menu add command -label "Mode" \
                -command {mode_initialize} -underline 0

        $tdwin.td_show_mb.menu add command -label "Rig (hardware)" \
                -command {rig_initialize} -underline 0
    }

    $tdwin.td_show_mb.menu add command -label "Logged variables" \
            -command {logvar_initialize} -underline 0

    $tdwin.td_show_mb.menu add command -label "Midas messages" \
            -command {show_midas_log} -underline 2

    $tdwin.td_show_mb.menu add command -label "Front-end console" \
            -command {hm_cons_grab} -underline 0
}

proc td_make_extras_menu { } {
    global tdwin run

    $tdwin.td_extras_mb.menu delete 0 last
    $tdwin.td_extras_mb.menu configure -tearoff $PREF::menutearoff
    $tdwin.td_extras_mb configure -direction $PREF::menudirect

    $tdwin.td_extras_mb.menu add command -label "Auto-Run" \
            -command {} -underline 0 -command { autor_initialize }

    $tdwin.td_extras_mb.menu add command -label "Camp" \
            -underline 0 -command { td_camp_cui }

    $tdwin.td_extras_mb.menu add command -label "Beamline control" \
            -underline 0 -command mui_beamline_control

    $tdwin.td_extras_mb.menu add cascade -label "CAEN HV" \
            -underline 6 -menu $tdwin.td_extras_mb.menu.hv

    $tdwin.td_extras_mb.menu.hv delete 0 last
    $tdwin.td_extras_mb.menu.hv add command -label "Save" -command { hvutil_gui save }
    $tdwin.td_extras_mb.menu.hv add command -label "Restore" -command { hvutil_gui restore }
    $tdwin.td_extras_mb.menu.hv add command -label "Console" -command { hvutil_console }

    $tdwin.td_extras_mb.menu add command -label "Expert set-up" \
            -underline 1 -command {tdx_initialize} 

    $tdwin.td_extras_mb.menu add command -label "VME HM utility" \
            -underline 5 -command {hmu_initialize}

    $tdwin.td_extras_mb.menu add command -label "Change old run titles" \
            -underline 7 -command {change_old_titles}
}

proc td_zero { what } {
    global run mode
    #if { ! $run(in_progress) } { return }

    if { $run(confirm_stop) >= 2 && ( $what == "all" || $what == "hists" ) && \
             $run(in_progress) && !$run(test_mode) && \
             [clock seconds]-$run(start_sec) > 200 && \
             $run(hist_total) > 99999 } {
        beep
        if { [timed_messageBox -timeout 30000 \
                  -type yesno -default yes -icon question -title "Zero run?" \
                  -message "Really forget all data in current run?" ] == "no" } {
            return
        }
    }

    switch -- $what {
        all {
            catch {
                odb_zero_hists
                odb_zero_scalers
                mui_camp_cmd { foreach v [sysGetLoggedVars] {varDoSet $v -z} }
            }
        }
        hists {
            odb_zero_hists
        }
        reference {
            if { $mode(dual_enabled) } {
                for { set i $mode(numsel) } { $i < $mode(numhist) } { incr i } {
                    odb_zero_one_hist $i
                }
            }
        }
        scalers {
            odb_zero_scalers
        }
        camp {
            catch {
                mui_camp_cmd { foreach v [sysGetLoggedVars] {varDoSet $v -z} }
            }
        }
    }
    td_all_update
    after 5000 { catch odb_save_data }
    after 8000 { td_background_update }
}

proc td_exit { } {
    global run

    if { $run(confirm_stop) > 0 } {
        beep
        if { "ok" != [timed_messageBox -timeout 30000 \
                          -type okcancel -default ok -icon question -title "Quit OK?" \
                          -message "Really quit \u00b5SR run control interface?" ] } {
            return
        }
    }
    destroy .
    # Note that experiment disconnects automatically; see td_destroy, next
}

proc td_destroy { win } {
    if { [string equal $win "."] } {
	# preempt connecting to Midas and Camp
	set ::midas(camp_host) ""
	proc odb_reconnect { } { return -code error "Disconnected" }
        odb_disconnect
        foreach id [after info] { after cancel $id } 
        kill_midas_log
    }
}


proc mui_cancelrun { } {
    global run
    catch { odb_get_runinfo }
    if { $run(ImuSR) } {
        if { $run(in_progress) || $run(starting) } {
            imusr_run kill
        }
        iquick_cancel
    } else {
        td_run kill
    }
}

proc td_archive_run { } {
    global run midas bgarc_status bgarc_output env
    set bgarc_output "Retried"
    set acmd [concat blt::bgexec bgarc_status -output bgarc_output $midas(archive_cmd) \
                  [list [file rootname $run(last_file)].msr $midas(archive_to)]]
    set stat [ catch $acmd msg ]
    if { $stat } { 
        beep
        timed_messageBox -timeout 30000 -type ok -icon error \
                -message "Failed to archive run!\n$msg\n$bgarc_output" \
                -title "Failed archive"
    }
}

#  Provide special window title for a torn-off run menu (reporting the
#  current run number)

proc run_tearoff_cmd { m to } {
    global run
    #puts "Menu $m is torn off, forming window $to"
    lappend run(tearoffs) $to
    run_tearoff_title
}

set run(tearoffs) [list]

proc run_tearoff_title { } {
    global run tdwin

    if { $run(in_progress) } {
	set title "Run $run(number)"
    } else {
	set title "Run Menu"
    }

    if { $PREF::menutearoff } {
        $tdwin.td_run_mb.menu configure -title $title -tearoff 1 -tearoffcommand run_tearoff_cmd
    } else {
        $tdwin.td_run_mb.menu configure -title $title -tearoff 0
    }

    set i 0
    #puts "Set title \"$title\" for run menu and window(s) $run(tearoffs)"
    foreach w $run(tearoffs) {
	if { [winfo exists $w] } { # tearoff is present.  Set its title
	    incr i
	    $w configure -title $title
            wm title $w $title
	} else { # tearoff is gone, remove it from list
	    set run(tearoffs) [lreplace $run(tearoffs) $i $i]
	}
    }
}

#  Validate a run title (header) entry, specified by $b. Invoked by the [OK]
#  button, and by FocusOut from each title entry.
#
#  Temperature and Field are stored as strings, so allow arbitrary values.
#  If the user types "/..." it signals automatic field or temperature entries
#  to be taken from a Camp variable.
#  Attempt automatic units conversion to give K or G units. Return the success
#  code.  If $rtitles(validate)==2, then apply converted value.
#
#  Experiment must be an integer (but accepts [MmSsEe]integer)
#
#  I could truncate too-long strings here, but odb-interface does that anyway.
#
#  Returns Boolean status code: 1 for OK, 0 for "bad".
#
proc title_validate { b } {
    global tdwin rtitles run idef
    set v8 [expr {[string match -nocase $rtitles(noV4exp) $rtitles(experiment)]? 0 : $rtitles(validate)}]
    set v [string trim $rtitles($b)]
    if { $v == "" } { return 1 }
    switch -- $b {
        temperature {
            if { $run(ImuSR) } {
                # Set "error"-return code so no error for funny T during T scan
                idef_init_sweep_kind
                set rcode [string match -nocase T $idef(sweep_kind)]
            } else {
                set rcode 0
                if { [string match "/*" $v] } {
                    set rtitles(autoTvar) $v
                    return 1
                }
            }
            set rtitles(autoTvar) none
            set cv [mui_convert_temperature $v]
	    #puts "mui_convert_temperature $v gives $cv"
            if { [regexp {^[~<>=@]*\s*([0-9.eE+-]+)[ (_+/-]*[0-9.eE+-]*[ )]*K$} $cv -> T] \
                     && [string is double -strict $T] } {# valid (converted) temperature
                if { $v8==2 || ($v8==1 && [string is double -strict $v]) } {
                    # Use converted number if requested, or if input was a bare number 
                    set rtitles($b) $cv
		}
	    } else {# not converted means failure (but maybe OK)
		return $rcode
            }
          }
        field {
            if { $run(ImuSR) } {
                # Set "error"-return code so no error for funny B during B scan
                idef_init_sweep_kind
                set rcode [string match -nocase B $idef(sweep_kind)]
            } else {
                set rcode 0
                if { [string match "/*" $v] } {
                    set rtitles(autoBvar) $v
                    return 1
                }
            }
            set rtitles(autoBvar) none
            set cv [mui_convert_field $v]
	    # puts "mui_convert_field $v gives $cv"
            if { [regexp {^[~<>=@]*\s*([0-9.eE+-]+)[ (_+/-]*[0-9.eE+-]*[ )]*G$} $cv -> B] \
                     && [string is double -strict $B] } {# valid (converted) field
                if { $v8==2 || ($v8==1 && [string is double -strict $v]) } {
                    # Use converted number if requested, or if input was a bare number 
                    set rtitles($b) $cv
		}
	    } else {# not converted means failure (but maybe OK)
		return $rcode
            }
          }
        experiment { # Always perform conversion to integer because odb value is integer
            set v [string trimleft $v "eEsSmM"]
            return [expr {[scan $v {%d} rtitles(experiment) ] == 1 } ] 
          }
    }
    if { $run(ImuSR) } {
        imusr_set_runtitle
    }
    return 1
}


proc titles_begin_change { } {
#   Activate widgets to allow changing run titles.
    global tdwin run rtitles
#
    if { ![ td_odb odb_get_runinfo "get run information" ] } { return }
#
    showButton $tdwin.td_tit_rest_b
    td_bind_help td_tit_rest_b "Undo the changes to titles (alt-v)"
    showButton $tdwin.td_tit_ok_b
    td_bind_help td_tit_ok_b "Confirm run titles (alt-o)"
    foreach b $rtitles(list) {
        set rtitles($b) $run($b)
        if { [winfo exists $tdwin.td_${b}_e] } { # Skip some fields -- don't display their entry 
            activateEntry $tdwin.td_${b}_e -justify l
            bind $tdwin.td_${b}_e <Return> {focus [tk_focusNext %W] ; break}
            bind $tdwin.td_${b}_e <FocusOut> [list title_validate $b]
        }
    }

    if { $run(ImuSR) } {
        idef_TBO_labels
        deactivateEntry $tdwin.td_runtitle_e
        imusr_set_runtitle
    } else {
        set rtitles(autoTvar) $run(autoTvar)
        if { [string match "/*" $rtitles(autoTvar)] } {
            set rtitles(temperature) $rtitles(autoTvar)
        }
        set rtitles(autoBvar) $run(autoBvar)
        if { [string match "/*" $rtitles(autoBvar)] } {
            set rtitles(field) $rtitles(autoBvar)
        }
    }
    set rtitles(changing) 1
}

#   Deactivate title-entry widgets on main page.  Invoked by [Titles OK] button.
proc titles_end_change { } {
    global tdwin run rtitles
#
    set run(autoTvar) none
    if { [string match "/*" $rtitles(temperature)] } {
        if { ![catch { mui_camp_cmd "varNumGetNum $rtitles(temperature)" } ] } {
            set run(autoTvar) $rtitles(temperature)
        }
        set rtitles(temperature) $run(temperature)
    }
    set run(autoBvar) none
    if { [string match "/*" $rtitles(field)] } {
        if { ![catch { mui_camp_cmd "varNumGetNum $rtitles(field)" } ] } {
            set run(autoBvar) $rtitles(field)
        }
        set rtitles(field) $run(field)
    }

    foreach b $rtitles(list) {
        set run($b) $rtitles($b)
    }
#
#   Do save; Handle error conditions; skip rest on failure
    if { $rtitles(changing) } {
        if { ![ td_odb odb_set_runinfo "set run titles" ] } { return }
    }
#
    hideButton $tdwin.td_tit_rest_b
    td_bind_help td_tit_rest_b ""
    hideButton $tdwin.td_tit_ok_b 
    td_bind_help td_tit_ok_b ""
    td_act_cancelb hide
    foreach b $rtitles(list) {
        if { [winfo exists $tdwin.td_${b}_e] } {
            deactivateEntry $tdwin.td_${b}_e
            bind $tdwin.td_${b}_e <Return> {}
            bind $tdwin.td_${b}_e <FocusOut> ""
        }
    }
    if { $run(ImuSR) } {
        imusr_set_runtitle
    }
    set rtitles(changing) 0
}

# Checking of titles is controlled by two variables.
# rtitles(validate) 0 for none, 1 for checks, 2 for auto-conversion
# rtitles(noV4exp) "no validation for exp" gives the experiment number 
# that previously cancelled validation (cancellation remains in effect 
# until exp changes). The main routine is title_validate, defined in 
# mui_utils.tcl
proc titles_OK {} {
    global tdwin run rtitles
    set badfields ""
    if { [string match -nocase $rtitles(noV4exp) $rtitles(experiment)] } { # suppress validation
        set v8 0
    } else {
        set v8 $rtitles(validate)
        set rtitles(noV4exp) "-----"
    }
    foreach b $rtitles(list) {
        if { [title_validate $b] == 0 } {
            append badfields $b " "
        }
    }
    if { $v8 && [string length $badfields] } {
        beep
        switch [ timed_messageBox -timeout 30000 \
                -type yesnocancel -default yes -icon error \
                -message "Possible error in titles:\n$badfields\nReally apply (yes/no), or cancel all such checking?" \
                -title "Error in titles" ] {
            yes { }
            no { titles_begin_change }
            cancel { set rtitles(noV4exp) $rtitles(experiment) }
        }
    }
    titles_end_change
}

proc td_act_cancelb { {action show} } {
    global tdwin
    if { [string equal $action show] } {
        showButton $tdwin.td_cancelrun_b
        td_bind_help td_cancelrun_b "Cancel (kill) new run"
    } else {
        hideButton $tdwin.td_cancelrun_b
        td_bind_help td_cancelrun_b ""
    }
}

proc td_camp_cui { } {
    global midas
    if { [string length $midas(camp_host) ] && \
            [string compare "none" $midas(camp_host)] } {
        exec xterm +bdc +mb +sb -sl 0 -rw +t -pc -geometry 80x25 \
                -fn  "-misc-fixed-medium-r-normal--14-130-75-75-c-70-iso10646-1" \
                -fb  "-misc-fixed-bold-r-normal--14-130-75-75-c-70-iso10646-1" \
                -bg "#eeeeee" -fg "#222222" \
                -xrm "XTerm*colorBDMode: true" -xrm "XTerm*colorBD: #0000ee" \
                -xrm "XTerm*colorBLMode: true" -xrm "XTerm*colorBL: #d00000" \
                -e camp_cui -node $midas(camp_host) &
    } else {
        beep
        timed_messageBox -timeout 30000 -type ok -default ok -icon warning \
                -message "There is no Camp host defined" -title "No Camp"
    }
}

#  THE HELP LINE:
#
#  Many widgets have a binding for mouse-entry and mouse-exit where some
#  help text is displayed while the cursor is on that widget.

proc td_bind_help { w h } {
    global tdwin
    bind $tdwin.$w <Enter> "set td_help_text \"$h\""
    bind $tdwin.$w <FocusIn> "set td_help_text \"$h\""
    bind $tdwin.$w <Leave> "set td_help_text {}"
    bind $tdwin.$w <FocusOut> "set td_help_text {}"
}

proc show_midas_log { } {
    global midas
    kill_midas_log
    set midas(showmess_pid) [ exec \
      xterm +bdc -geometry 132x18 -rightbar \
        -fn  "-misc-fixed-medium-r-normal--14-130-75-75-c-70-iso10646-1" \
        -fb  "-misc-fixed-bold-r-normal--14-130-75-75-c-70-iso10646-1" \
        -xrm "XTerm*internalBorder: 4" -title "Midas Messages" \
        -fg $PREF::fgcol -bg $PREF::bgcol \
        -e tail -n 1500 -f $midas(log_file) \& ]
}

proc kill_midas_log { } {
    global midas
    if { [string length $midas(showmess_pid)] } {
        catch { exec kill -9 $midas(showmess_pid) }
        set midas(showmess_pid) ""
    }
}

#   Display messages extracted from midas.log.
# Parameters:
#   num    Display (at most) this many messages
#   sec    Choose the latest, within this many seconds
#   nread  Look at only this many lines at the end of midas.log
#   client Choose messages from this client (or *)
# Returns:
#   Text of selected messages.
#   (or error)

proc recent_midas_messages { num sec nread client } {
    global midas
    if {[catch {exec tail -n $nread $midas(log_file)} log]} {
        puts "error"
        return $log
    }
    set ret [list]
    set now [clock seconds]
    foreach line [split $log "\n"] {
        if { [regexp "^(.*?)\\\[$client\\\]\\s*(.*)\$" $line -- date msg] } {
            if { $now - [clock scan $date] < $sec } {
                if { [string compare [lindex $ret end] $msg] } {
                    lappend ret $msg
                }
            }
        }
    }
    return [join [lrange $ret end-[incr num -1] end] "\n"]
}



#   A "heartbeat" porcedure to re-connect to ODB when disconnected, 
#   and also to respond to (shutdown) commands from Midas.
proc mui_heartbeat { } {
    after 2000 mui_heartbeat
    catch { odb_reconnect } 
}

proc td_background_update { } {
    global midas run tdwin

    if { [catch { td_all_update } mes] } {
        # Occasionally report errors, but not when a message is there already.
        puts "td_background_update caught error $midas(num_bg_err): $mes"
        if { [incr midas(num_bg_err)] > $midas(max_bg_err) && !$::tmb_active} {
            set midas(num_bg_err) 0
            beep
            after 10 [list timed_messageBox -timeout 20000 \
                        -type ok -icon error -title {Midas update error} \
                        -message "Failed to get status update: $mes" ]
        }
        return
    } else {
	# Two errors after many successes will trigger error
	if { $midas(num_bg_err) < $midas(max_bg_err) - 2 } { incr midas(num_bg_err) }
    }

#   Check that the musr-midas clients are alive, but only check rarely: 
    if { $midas(check_clients) > 0 && \
            [clock seconds] - $midas(clt_checked_at) > $midas(check_clients) } {
        mui_check_clients
    } else {
        if { $run(state) == 3 && !$run(ImuSR) } {
            td_check_hits
        }
    }
}

proc td_all_update { } {
    global tdwin modewin statwin midas run rtitles midef

#   cancel any other pending background update
    if { [string compare $midas(refresh_id) ""] } {
        after cancel $midas(refresh_id)
    }

#   initialise error traps
    set errm ""
    set nerr 0

    #puts "update all"
#   do updates
    if { [ catch { odb_get_general } mes ] } { 
        append errm "\n$mes"
        incr nerr
        #puts "$nerr $mes"
    }
#   $rtitles(ImuSR) indicates what type of run is being displayed, as of the
#   last update.  $run(ImuSR) indicates what type is current.  A funny value
#   of rtitles(ImuSR) triggers "re"display at start.
    if { $rtitles(ImuSR) != $run(ImuSR) } {
	if { $run(ImuSR) } {
	    mui_display_imusr
	} else {
	    mui_display_tdmusr
	}
        # Cancel any titles-editing and display new experiment's titles
        set rtitles(changing) 0
        titles_end_change
        # Declare that the proper titles are displayed:
	set rtitles(ImuSR) $run(ImuSR)
    }
    if { [ catch { odb_get_runinfo } mes ] } {
        append errm "\n(runinfo) $mes"
        incr nerr
        #puts "$nerr $mes"
    }
#   Give an audible warning when the run ends without being commanded
    if { $run(did_stop_at) > 0 && $run(stop_sec) > $run(did_stop_at) } {
        #puts "run(stop_sec) > run(did_stop_at): $run(stop_sec) > $run(did_stop_at)"
        bells 3
        set run(did_stop_at) $run(stop_sec)
    }
#   Hide cancel button when run stops or saves data
    if { (!$run(starting)) && ( (!$run(in_progress)) || ($run(last_save)> $run(start_sec)) ) } {
        td_act_cancelb hide
    }
#
    if { [ catch { odb_get_autor } mes ] } { 
        append errm "\n(autor) $mes"
        incr nerr
        #puts "$nerr $mes"
    } else {
        global autor
        # optionally update displayed values, depending on whether user is editing them (recently)
        # ($memorylapse is the expected attention span of the user, in seconds :-)
        if { [clock seconds] > $autor(entry_at) + $PREF::memoryLapse } {
            if { [ catch { autor_revert } mes ] } { 
                append errm "\n(revert) $mes"
                incr nerr
            }
        }
    }

    set statw [ winfo exists $statwin.rs_wintitle_l ]

    if { $run(ImuSR) } {
        if { [ catch { idef_do_update } mes ] } {
            append errm "\n(idef) $mes"
            incr nerr
            #puts "$nerr $mes"
        }

    } else { # TD musr

        if { [ catch { odb_get_rig ; rig_update } mes ] } { 
            append errm "\n(rig) $mes"
            incr nerr
            #puts "$nerr $mes"
        }
        if { [ winfo exists $modewin.m_OK_b ] || $statw } {
            if { [ catch { odb_get_mode ; mode_update } mes ] } { 
                append errm "\n(mode) $mes"
                incr nerr
                puts "$nerr $mes"
            }
        }
    }

    if { $statw } {
        if { [ catch { odb_get_stat ; runstat_update } mes ] } { 
            append errm "\n(stat) $mes"
            incr nerr
            #puts "$nerr $mes"
        }
    }
    if { [string length $midas(camp_host) ] && ![string equal "none" $midas(camp_host)] } {
        if { [ catch { logvar_update } mes ] } {
            append errm "\n(Camp) $mes"
            #incr nerr
        } else {
            if { [ catch {
                if { $run(ImuSR) } {
                    logvar_make_menu $::idefwin.tabs.log.ilog_configure_mb
                } else {
                    logvar_make_menu $::logwin.lv_configure_mb
                }
            } mes ] } {
                append errm "\n(Camp) $mes"
                #incr nerr
            }
        }
    }
#   If run state changed, rebuild main menus
    if { [ catch {
        set state "$run(state) $run(in_progress) $run(test_mode) $run(expertype) $run(paused) $::mode(dual_enabled) $::rig(num_scalers)"
        if { [string compare $run(menustate) $state] } {
            td_make_menus
        } else {
            td_show_titles
        }
    } mes ] } { 
            append errm "\n$mes"
            incr nerr
            #puts "$nerr $mes"
    }
#   Reschedule the background updates
    set midas(refresh_id) [ after $midas(refresh_ms) { td_background_update } ]
#   Do error return
    if { $nerr } {
        set run(state_label) "incommunicado"
        if { $nerr > 1 } {
            return -code error "$nerr errors:$errm"
        } else {
            return -code error $errm
        }
    }
    return
}

proc td_show_titles { } {
    global run rtitles
    if { !$rtitles(changing) } {
        foreach b $rtitles(list) {
            set rtitles($b) $run($b)
        }
    }
}

proc mui_display_imusr { } {
    global tdwin rigwin modewin logwin idef
    # puts "Display I-muSR windows"
    grid $tdwin.mui_scan_f
    grid $tdwin.mui_comment_f
    grid remove $tdwin.mui_rig_f
    grid remove $tdwin.mui_mode_f
    destroy [td_win_name $rigwin]
    destroy [td_win_name $modewin]
    destroy [td_win_name $logwin]
    kill_midas_log
    catch { # at start-up some things may not be defined yet
	$tdwin.mui_rigis_l configure -text "Setup: "
	$tdwin.mui_modeis_l configure -text "Device: "
	$tdwin.mui_rigname_l configure -textvariable idef(setup_name)
	$tdwin.mui_modename_l configure -textvariable idef(sweep_ins_name)
        idef_TBO_labels
    }
}

proc mui_display_tdmusr { } {
    global tdwin idefwin iqwin rtitles logwin
    # puts "Display TD-muSR windows"
    grid remove $tdwin.mui_scan_f
    grid remove $tdwin.mui_comment_f
    grid $tdwin.mui_rig_f
    grid $tdwin.mui_mode_f
    destroy [td_win_name $idefwin]
    destroy [td_win_name $iqwin]
    destroy [td_win_name $logwin]
    kill_midas_log
    set rtitles(Field_lab) "Field:"
    set rtitles(Temp_lab) "Temperature:"
    catch {
	$tdwin.mui_rigis_l configure -text "Rig is"
	$tdwin.mui_rigname_l configure -textvariable rig(rigname)
	$tdwin.mui_modeis_l configure -text "Mode is"
	$tdwin.mui_modename_l configure -textvariable mode(modename)
    }
}

# Here we handle the user's instruction to change the experiment type
proc mui_set_experiment { exp } {
    global midas run env

    # puts "mui_set_experiment $exp"
    # Normalize the strings used for each experiment.  Set exten as the 
    # file extension for the FE boot file.
    switch -glob -- $exp {
	I* {
            set im 1
	    set exp "I-\u00b5SR"
            set exten "imusr"
	}
	default {
            set im 0
	    set exp "TD-\u00b5SR"
            set exten "tdmusr"
	}
    }

    # Check if selected experiment is different from current (yes, odb_get_general 
    # updates run(ImuSR) boolean), then perform switch. 

    set midas(clt_checked_at) [expr {[clock seconds] + 50}]
    set midas(clt_defer) $midas(clt_checked_at)

    td_odb odb_get_general "get Midas ODB"
    if { $im != $run(ImuSR) } {
        if { $run(confirm_stop) >= 1 } {
            beep
            if { [ timed_messageBox -timeout 30000 \
                -type okcancel -default cancel -icon question \
                -message "Please confirm switching experiment to ${exp}." \
                -title "Confirm switch!" ] != "ok" } { return }
        }
	td_odb [list odb_set_exper_type $exp $exten] "switch experiment type"
        # Set a default run title for new experiment (either type)
        # This is not good, but other things are bad too.
        after 2000 {
            set run(runtitle) "$run(sample) $run(temperature) $run(field)"
            catch odb_set_runinfo
        }
    }
    set midas(start_automatic) yes
    after 10000 {set midas(start_automatic) no}
    set midas(clt_checked_at) 0
    set midas(clt_defer) 0
    mui_check_clients
    td_background_update
}

# Get a list of Midas clients, listing only those running on the Midas main
# host or the frontend.  The variable midas(clt_defer) is a semaphore used 
# (here and elsewhere) to signal that checking is in progress and prevent 
# multiple simultaneous checks.  It is set to a time value, so it will "expire"
# by itself, and it should be set to 0 when finished checking.

proc mui_get_midas_clients { } {
    global midas env

    #puts "Get list of Midas clients at [clock format [clock seconds]]"
    set midas(clt_defer) [expr {[clock seconds] + 40}]
    set midas(clt_stat) {0 0 666 pre}
    set midas(odb_output) ""
    # First, tell Midas to clean up its client list, then get the client list
    set af [ after 1500 { set midas(clt_stat) {timeout 0 666 timeout}} ]
    set midas(odb_output) "Timed out"
    set cl "Failure in odb (cleanup)"
    catch { blt::bgexec midas(clt_stat) -output midas(odb_output)\
            odb -h $midas(host) -e $midas(expt) -c cleanup }
    after cancel $af
    set ok 0
    catch { set ok [expr { [lindex $midas(clt_stat) 2] == 0 } ] }
    if { $ok && [catch { midas scl } cl ] } {
        set ok 0
    }

    # check: No errors, and client list ($cl) has even number of elements, and I (musrrc) am listed
    if { $ok && ([llength $cl] & 1) == 0 && \
            [lsearch -glob $cl "$midas(application)*"] >= 0 } {
        #puts "Valid client list"
        set midas(clt_check_failed) 0
    } else {# Failed to get proper client list
        # Don't complain on first failure because there can be random failures
        # when midas inserts messages asynchronously
        incr midas(clt_check_failed)
        puts "==================================================================="
        puts "Client check failed!  midas(clt_check_failed)=$midas(clt_check_failed)  ok=$ok "
        puts "llength = [llength $cl], [expr {([llength $cl] & 1) == 0}]"
        puts "Failed to check on Midas clients:\n$cl\nSomething might be seriously wrong!"
        puts "I am $midas(application), at [lsearch -glob $cl {$midas(application)*}]"
        if { $midas(clt_check_failed) > 1 } {#
            beep
            timed_messageBox -timeout 20000 \
                    -type ok -default ok -icon warning -title {Fail clients check} \
                    -message "Failed to check on Midas clients:\n$cl\nSomething might be seriously wrong!"
        }
        set cl [list]
    }
    #puts "Client list: $cl"
    return $cl
}

# Check for needed clients, and (re)start the missing ones.
# Return 0 for all clients alive already, 1 for incomplete check, 9 for FE restart 
# initiated, else the number of restarted clients.
#
proc mui_check_clients { } {
    global midas tdwin run autor env nstart

    set nstart 0

    # Avoid duplicate streams of client checks. clt_stat is null when not checking
    if { $midas(clt_defer) > [clock seconds] } {
        puts "Postpone client check because one is in progress"
        return 1
    }

    set midas(clt_defer) [expr {[clock seconds]+40}]

    mui_check_and_start_mserver

    # Get list of midas clients
    set clist [mui_get_midas_clients]

    # Quit now, but retry soon after, if failed to get client list.
    if { $midas(clt_check_failed) } { 
        set midas(clt_defer) 0
        return 1
    }

    set midas(clt_checked_at) [clock seconds]

    if { $run(ImuSR) } {
        if { [mui_check_a_client clmat msg femusr $clist {} 0] } {
            mui_query_restart femusr imusr
            # return from here because Imusr-mdarc won't work until fe is running
            # The reboot/restart (if done) will trigger another client check.
            return 9
        } else {
            set midas(failed_FE_checks) 0
        }

        if { [mui_check_a_client clmat msg mdarc $clist mdarc 1] } {
            # must stop the run before restarting Imdarc, which is a pain!
            mui_restart_client $clmat $msg mdarc mdarc {
                if { $run(in_progress) } { 
                    switch -- [mui_choose_end_or_kill] {
                        end { catch odb_end_run }
                        kill { catch odb_kill_run }
                    }
                }
                catch {exec xterm -geometry 112x35 -fn 7x13 -fg black -bg \#f0f0c0 \
                   -e /usr/bin/screen -L -t "$midas(expt)-mdarc" -S $midas(expt)_mdarc \
                      $env(MUSR_DIR)/mdarc/bin/mdarc -e $midas(expt) &}
                incr nstart
            }
        }
        if { [mui_check_a_client clmat msg musr_config $clist musr_config 1] } {
            mui_restart_client $clmat $msg musr_config musr_config {
                exec /usr/bin/screen -d -m -L -t "$midas(expt)-musr_config" -S $midas(expt)_musr_config \
		    $env(MUSR_DIR)/musr_midas/musr_config/bin/musr_config -e $midas(expt) &
                incr nstart
            }
        }
        if { [mui_check_a_client clmat msg mheader $clist mheader 1] } {
            mui_restart_client $clmat $msg mheader mheader {
                exec /usr/bin/screen -d -m -L -t "$midas(expt)-mheader" -S $midas(expt)_mheader \
		    $env(MUSR_DIR)/mdarc/bin/mheader -e $midas(expt) &
                incr nstart
            }
        }

    } else { # TD

        if { [mui_check_a_client clmat msg fev680 $clist {} 0] } {
            if { [incr midas(failed_FE_checks)] > 5 } {
                if { $autor(enabled) } {
                    set midas(start_automatic) "yes"
                }
            }
            mui_query_restart fev680 tdmusr
            return 9
        } else {
            set midas(failed_FE_checks) 0
        }
        if { [mui_check_a_client clmat msg musr_config $clist musr_config 1] } {
            mui_restart_client $clmat $msg musr_config musr_config {
                exec /usr/bin/screen -d -m -L -t "$midas(expt)-musr_config" -S $midas(expt)_musr_config \
		    $env(MUSR_DIR)/musr_midas/musr_config/bin/musr_config -e $midas(expt) &
                incr nstart
            }
        }
        # specify no program name because bnmr uses separate mdarc with same name
        if { [mui_check_a_client clmat msg mdarc $clist {} 1] } {
            mui_restart_client $clmat $msg mdarc mdarc {
                catch {exec xterm -geometry 112x35 -fn 7x13 -fg black -bg \#f0f0c0 \
                   -e /usr/bin/screen -L -t "$midas(expt)-mdarc" -S $midas(expt)_mdarc \
                      $env(MUSR_DIR)/mdarc/bin/mdarc -e $midas(expt) &}
                incr nstart
            }
        }
        if { (!$midas(mdarc_enabled)) && $run(runtype) == "real" } {
            set midas(mdarc_enabled) 1
            odb_set_general
        }
    }
    if { $autor(enabled) } {
        if { [mui_check_a_client clmat msg autorun $clist {} 1] } {
            mui_restart_client $clmat $msg autorun {} {
                exec /usr/bin/screen -d -m -L -t "$midas(expt)-autorun" -S $midas(expt)_autorun \
		    $env(MUSR_DIR)/musr_midas/midas_tcl/mtcl $::muisrc/autoruns.tcl &
                incr nstart
            }
        }
    }

    set midas(clt_defer) [expr { [clock seconds] + ($nstart ? 5 : 1) }]

    return $nstart

}

# mserver starts on particular port for some experiments (and not at all for old "musr")
# Add experiment names as needed.
# Returns 0 if no action needed, 1 for successful restart, or error on failure

proc mui_check_and_start_mserver { } {
    global midas
#
    array set ports {m15 7071 m20c 7071 m20d 7071 m9a 7071 m9b 7071 dev 7072}
#
    if { [info exists ports($midas(expt))] } {# This expt needs mserver started
        if { ![regexp -line  " mserver .*\-p $ports($midas(expt))" [exec ps -ef]] } {
            puts "Starting mserver for experiment $midas(expt) on port $ports($midas(expt))"
            exec mserver -p $ports($midas(expt)) -D
            after 100
            return 1
        }
    }
    return 0
}

# Check if a particular client is active.
# Parameters:  cvar     variable name, in which to return the list of clients
#              client   client name to check
#              clist    list of (client, host) pairs given by scl
#              program  program name of client, or null string.
#              maxnum   maximum number of clients (or 0 for don't care)
# Return a code value:
#   0: OK
#   1: No client found (or no task)
#   2: Too many clients (or tasks) found
# Also return in variable $cvar a list of the actual midas client names.
#
# If the program string is supplied (not null) then the list of tasks (ps -A)
# is checked to ensure that it has the same number of said tasks as there were
# midas clients.  For programs used by multiple clients (mtcl for musrrc and 
# autorun) or programs that run on a different computer (fev680, femusr), one
# must supply a null string instead of the program name.
#
proc mui_check_a_client {cvar msgvar client clist program maxnum} {
    global midas env
    upvar 1 $cvar matches
    upvar 1 $msgvar msg
    #puts "Check client $client $program from list:\n  $clist"
    set midas(clt_defer) [expr {[clock seconds] + 20}]
    # Get list of matching midas clients
    set matches [list]
    foreach {c h} $clist {
        if { [scan "${c}0" "${client}%d" i] == 1 } {
            lappend matches $c
        }
    }
    #puts "Check Client $client matches: $matches"
    set nc [llength $matches]
    if { $nc < 1 } { 
        set msg "is gone"
        puts "Client $client $msg"
        return 1
    }
    if { $nc > $maxnum && $maxnum > 0 } {
        set msg "has too many instances ($nc vs $maxnum)"
        puts "Client $client $msg"
        return 2
    }
    # Check how many instances of the program are running
    set nt $nc
    if { [string length $program] } {
        set nt 0
        catch { scan [exec ps -A | grep -c -- " $program\$"] { %d} nt }
        if { $nt > $nc } {
            set msg "has too many copies ($nt vs $nc)"
            puts "Client $client $msg"
            return 2
        }
        if { $nt < 1 } {
            set msg "is not running"
            puts "Client $client $msg"
            return 1
        }
        #  regexp -line -- "^ *(\\d+) +$program\$" \
        #        [exec ps --User $env(USER) --sort=start_time --format pid,comm] \
        #        line pid
    }
    return 0
}


#  Re-start a client process by evaluating $code
#  Parameters: clmat:  list of matching midas clients to stop
#              msg:    message for prompt (reason)
#              clname: client name for people reading
#              clprog: client program (usually same) to look for restart
#              code:   Tcl script code to restart client
proc mui_restart_client { clmat msg clname clprog code } {
    global tdwin midas
    set c $midas(start_automatic)
    if { ! [string equal $c yes] } {
        set midas(clt_defer) [expr {[clock seconds] + 60}]
        beep
        set c [ timed_messageBox -timeout 20000 \
                -type yesno -default yes -icon warning \
                -message "Client $clname ${msg}!\nRestart it?" \
                -title "Restart ${clname}?" ]
    }
    if { [string equal $c yes] } {
        midas msg 2 musrrc "Restart client $clname because it ${msg}"
        mui_maintain_autorun
        puts "Stop clients $clmat"
        foreach client $clmat {
            shutdown_midas_client $client
        }
        if { [string length $clprog] } {
            catch {
                exec killall $clprog
                exec killall $clprog
            }
        }
        puts "Start program $clname"
        uplevel 1 $code
        if { [string length $clprog] } {
            after 20000 [list mui_after_restart_client $clprog]
        }
    }
}

# Shutdown a client.  If the client name ends in *, shut down all instances
# of that client.

proc shutdown_midas_client { client } {
    global midas
    puts "Shutdown midas client $client"
    if { [string match {*\*} $client] } {
        set client [string trimright $client "* "]
        set clist [mui_get_midas_clients]
        set slist [list]
        foreach {c h} $clist {
            if { [scan "${c}0" "${client}%d" i] == 1 } {
                lappend slist $c
            }
        }
        puts "Clients Matching $client\* are: $slist"
    } else {
        set slist [list $client]
    }
    set fail 0
    foreach c $slist {
        set af [ after 4000 { set midas(clt_stat) {timeout 666 666 timeout} } ]
        catch { blt::bgexec midas(clt_stat) \
                    odb -h $midas(host) -e $midas(expt) -c "shutdown $c" }
        after cancel $af
        puts "After shutdown $client, status is [lindex $midas(clt_stat) 2]"
        if { [lindex $midas(clt_stat) 2] } {
            puts "Failed to shutdown client $client.  Try to kill it instead"
            incr fail
        }
    }
    if { $fail } {
        catch {
            exec killall -9 $client
            exec odbedit -h $midas(host) -e $midas(expt) -c cleanup
        }
    }
}


proc mui_after_restart_client { program } {
    set foundClient 0
    catch { scan [exec ps -A | grep -c -- " ${program}\$" ] { %d} foundClient }
    if { $foundClient } {
        set midas(clt_checked_at) 0
    } else {
        beep
        timed_messageBox -timeout 30000 \
                -type ok -default ok -icon error -title "Failed $program" \
                -message "Failed to start ${program}!\nData acquisition is broken.\nTry kill-all and start-all or get help."

    }
}

# Interact with the user to initiate a restart of the frontend client.
# On the old vxworks system, this means a full reboot of the ppc, but
# on the new Linux frontend we restart the individual process.
# $client is the Midas client name for the frontend program, $exptyp is 
# "tdmusr" or "imusr", and is the file extension for the vxworks bootup 
# script, when booting.
#
proc mui_query_restart { client exptyp } {
    global midas run

    set midas(clt_defer) [expr {[clock seconds] + 50}]
    set c $midas(start_automatic)
    if { ! [string equal $c yes] } {
        set mes "The front-end processor appears to be gone!\nRestart the frontend process?"
        if { $run(in_progress) } {
            append mes "\n(Run will end.)"
        }
        set midas(clt_defer) [expr {[clock seconds] + 50}]
        beep
        set c [ timed_messageBox -timeout 30000 \
            -type yesno -default no -icon warning \
            -message $mes -title "Restart Front-end?" ]
    }
    set midas(clt_defer) [expr {[clock seconds] + 60}]
    if { [string equal $c yes] } {
        # I want to temporarily disable autoruns so it doesn't complain during restart
        mui_maintain_autorun disable
        # We would like to stop run here, but mdarc handles that very badly when the frontend
        # is crashed; Therefore, I will manage the data files myself.
        mui_end_and_clean
        # Clean up by shutting down Midas client
        #set af [ after 6000 { set midas(clt_stat) {timeout 666 666 timeout} } ]
        #catch { blt::bgexec midas(clt_stat) \
        #        odbedit -h $midas(host) -e $midas(expt) -c "shutdown $client" }
        #after cancel $af
        # Make sure we start the right experiment type
        odb_set_exper_type $run(expertype) $exptyp
        hm_do_restart
    }
}

# Proc to end the run (abruptly; possibly killing it) and clean it up manually

proc mui_end_and_clean { } {
    global run
    if { $run(in_progress) } {
        switch [mui_choose_end_or_kill] {
            kill {
                odb_kill_run
            }
            end {
                odb_abrupt_stop
            }
        }
        after 10000 [list mui_cleanup_for_restart [mui_choose_end_or_kill] $run(number)]
    }
}


#  Proc to choose whether to end run, if one is in progress, or kill it,
#  when it is going to stop anyway due to midas client failures.
#  It returns:
#  ""      when there is no run in progress
#  "kill"  when a run is in progress and has no events, or it has less than
#          half the desired events during an autorun.
#  "end"   otherwise
#
#  *SIGH*  dealing with these hassles makes for terrible programs!

proc mui_choose_end_or_kill { } {
    global run autor rstat

    if { $run(in_progress) } {
        catch { odb_get_stat }
        if { $run(ImuSR) } {
            global idef
            catch { odb_get_idef }
            if { $autor(enabled) && 2*$rstat(sweep_num) <= $idef(num_sweeps) } {
                # puts "Kill ImuSR run because sweep num = $rstat(sweep_num), desided = $idef(num_sweeps)"
                return "kill"
            } elseif { $rstat(curr_point) < 2 } {
                # puts "Kill ImuSR run because point num = $rstat(curr_point)"
                return "kill"
            } else {
                # puts "End ImuSR run with sweep num = $rstat(sweep_num)"
                return "end"
            }
        } else {
            if { $autor(enabled) && $autor(counts) > 0 && 2*$run(hist_total) <= $autor(counts) } {
                # puts "Kill TDmuSR run because counts = $run(hist_total), desired = $autor(counts)"
                return "kill"
            } elseif { $run(hist_total) < 2 } {
                # puts "Kill TDmuSR run because counts = $run(hist_total)"
                return "kill"
            } else {
                # puts "End TDmuSR run with counts = $run(hist_total)"
                return "end"
            }
        }
    }
}

#  On frontend restart, the run is ended or killed, but might not be ended cleanly, depending
#  on how badly the acquisition was broken.  Here we clean it up, depending on whether
#  we wanted to keep it or not.
#
proc mui_cleanup_for_restart { how number } {
    global midas env
    puts "Cleanup for restart similar to `${how}' for run $number"
    switch -- $how {
        end {
            # We ended run, so make sure it got purged and archived
            catch { exec $env(MUSR_DIR)/mdarc/bin/cleanup -r $number >&/dev/null & }
        }
        kill {
            # We killed run, so make sure files got deleted
            set f [file join $midas(data_dir) [format %06d $number] ]
            if { [file exists $f.msr] } {
                #puts "Remove files $f.msr etc"
                catch { 
                    exec /bin/cp $f.msr $f.sav
                    foreach file [glob $f.msr $f.msr_v* $f.odb] {
                        file delete $file
                    }
                }
            } else {
                # puts "No files saved for $f.msr"
            }
        }
    }
}

# mui_reenable_autorun: enable autoruns if they are not running; force reload of plan.
#
proc mui_reenable_autorun { } {
    global autor
    catch { odb_get_autor }
    # puts "Re-enable autorun at [clock format [clock seconds]]"
    if { !$autor(enabled) || $autor(state) == "idle" || $autor(state) == "disabled" } {
        set autor(state) "reload"
    }
    set autor(enabled) 1
    catch { odb_set_autor_cli }
}

# mui_maintain_autorun ? disable ?
# Procedure to maintain autorun (on/off) through some drastic action (like rebooting
# frontend and/or killing clients).  It gets the current autorun state and schedules
# a restoration (mui_reenable_autorun) in two minutes.  If the word "disable" is given
# as a parameter, autoruns are explicitly disabled now; otherwise they are left running
# and may or may not fail during the upcoming "drastic action".
#
proc mui_maintain_autorun { args } {
    global autor
    # puts "Execute mui_maintain_autorun at [clock format [clock seconds]]"
    catch { odb_get_autor }
    if { $autor(enabled) } {
        if { [llength $args] && [string equal [lindex $args 0] "disable"] } {
            # puts "Temporarily disable autoruns"
            set autor(enabled) 0
            catch { odb_set_autor_cli }
        }
        # puts "Plan on resuming autoruns at [clock format [expr [clock seconds]+120]]"
        after 120000 { mui_reenable_autorun }
    }
}

#  Check if the "hits" diagnostics or histogram totals indicate any problems.
proc td_check_hits {} {
    global rstat run tdwin mode

    if { [clock seconds] - $rstat(hits_checked_at) < 60 } { return }

    if { [catch { odb_get_stat } ] } { return }
    set rstat(hits_checked_at) [clock seconds]

    set hit $rstat(hit)
    set inv $rstat(inv)
    set ovf $rstat(ovf)
    lassign $rstat(previous_hits) phit pinv povf

    if { $hit + $inv + $ovf > $phit + $pinv + $povf + 500 } {
        # puts "Check hits when $hit + $inv + $ovf > $phit + $pinv + $povf + 500"
        # (Don't use incr because it barfs on --)
	set rstat(previous_hits) [list $hit $inv $ovf]
	set hit [expr {$hit-$phit}]
	set inv [expr {$inv-$pinv}]
	set ovf [expr {$ovf-$povf}]
	# Check if > 1% of hits are on an invalid channel
	set p [format {%.1f} [expr {100.0*$inv/($hit+$inv+1.0)}] ]
	if { $p > 1.0 } {
            odb_reapply_gates
            # prevent re-checking while prompting
            set rstat(hits_checked_at) [expr {[clock seconds]+200}]
	    beep
	    switch [timed_messageBox -timeout 60000 \
		    -type okcancel -default ok -icon warning \
		    -message "$p\% of events are on a bad channel.\nCheck the TDC inputs!\n(Cancel prevents further checks.)" \
		    -title "Bad events!"] {
		cancel {# Cancel further checks for ten hours
		    set rstat(hits_checked_at) [expr {[clock seconds] + 36000}]
		    return
		}
	    }
        }
	# Check if > 3% of hits are outside the good time range
        # (Note ovf does not count bin overflows but out-of-time-range hits!)
	set p [format {%.1f} [expr {100.0*$ovf/($hit+$ovf+1.0)}] ]
	#puts "%overtime = 100.0*$ovf/($hit+$ovf+1.0)= $p" 
	if { $p > 3.0 } {
            odb_reapply_gates
            # prevent re-checking while prompting
            set rstat(hits_checked_at) [expr {[clock seconds]+200}]
	    beep
	    switch [timed_messageBox -timeout 60000 \
                    -type okcancel -default ok -icon warning \
                    -message "$p\% of events are outside the histogram range.\nYou may want to adjust the gate lengths!\n(Cancel prevents further checks.)" \
                    -title "Bad events!"] {
                cancel {# Cancel further checks for ten hours
                    set rstat(hits_checked_at) [expr {[clock seconds] + 36000}]
                    return
                }
            }
	}
    }
    set rstat(previous_hits) [list $rstat(hit) $rstat(inv) $rstat(ovf)]

    # Now check the hit rates on individual histograms
    # Warn if any of them are getting a very small share of the total counts.
    set badh [list]
    set thr 0
    foreach {ht hr} $rstat(hhits) { incr thr $hr }
    if { $thr > 50 } {
	set i 0
	foreach {ht hr} $rstat(hhits) {
	    #puts "H[expr {$i+1}] $hr * $rstat(nhist) / $thr = [expr {double($hr) * $rstat(nhist) / double($thr)}]"
	    if { double($hr) * $rstat(nhist) / double($thr) < 0.15 } {
		set j [expr {$i+1}]
		puts "****** Hist $j ([lindex $rstat(hnames) $i]) low counts (hits)"
		lappend badh "$j ([lindex $rstat(hnames) $i])"
	    }
	    incr i
	}
    }

    # Now maybe check the recent additions to histograms
    # Warn if any of them are getting a very small share of the total counts.
    # I want a diagnostic for bin-count overflows!
    catch { odb_get_mode }
    set newtot [expr {$run(hist_total) - $rstat(previous_cts_total)}]
    if { ![string equal $rstat(prev_save_file_name) $run(last_file)] && $newtot < 500 } {
        # Few good counts when TM is high may be due to bad GGL gate length
        # (It may also be due to bad IO register output, but we do nothing yet.)
        catch { if { [lindex $rstat(srates) 0] + [lindex $rstat(srates) 1] > 500 }  {
            odb_reapply_gates
        }  }
    }
    if { $newtot > 10000 && $mode(bytesbin) > 16 && [llength $badh] == 0 } {
	for { set i 0 } { $i < $rstat(nhist) } { incr i } {
	    set j [expr {$i+1}]
	    #puts "Hist $j new counts: [expr { double([lindex $rstat(htotals) $i] - [lindex $rstat(previous_hist_cts) $i])}] = ([lindex $rstat(htotals) $i] - [lindex $rstat(previous_hist_cts) $i])"
	    if { double([lindex $rstat(htotals) $i] - [lindex $rstat(previous_hist_cts) $i]) * $rstat(nhist) / double($newtot) < 0.15 } {
		puts "****** Hist $j ([lindex $rstat(hnames) $i]) bad counts [expr { double([lindex $rstat(htotals) $i] - [lindex $rstat(previous_hist_cts) $i]) * $rstat(nhist) / double($newtot)}] < 0.15"
		lappend badh "$j ([lindex $rstat(hnames) $i])"
	    }
	}
	# Remember current totals for next time we test
	set rstat(previous_cts_total) $run(hist_total)
	set rstat(previous_hist_cts) $rstat(htotals)
    }

    # If there are any low counts, give warning
    if { [llength $badh] > 1 } {
        set mes "Histograms [join $badh {, }] have very low counts."
    } elseif { [llength $badh] > 0 } {
        set mes "Histogram [join $badh {}] has very low counts."
    } else {
        set mes ""
    }
    # Do not annoy by repeating the same message
    if { [llength $badh] > 0 && [string compare $mes $rstat(counts_message)] } {
        # prevent re-checking while prompting
        set rstat(hits_checked_at) [expr {[clock seconds]+200}]
        beep
        switch [timed_messageBox -timeout 60000 \
                    -type okcancel -default ok -icon warning \
                    -message "$mes You may want to check the connections and delay box switches.\n(Cancel prevents further checks.)" \
                    -title "Low counts!"] {
             cancel {# Cancel further checks for ten hours
                 set rstat(hits_checked_at) [expr {[clock seconds] + 36000}]
                 return
             }
        }
    } elseif { $newtot < 0 } {# zeroed hists
	set rstat(previous_cts_total) $run(hist_total)
	set rstat(previous_hist_cts) $rstat(htotals)
    }
    # remember for comparison next time
    set rstat(counts_message) $mes
    set rstat(prev_save_file_name) $run(last_file)
    set rstat(hits_checked_at) [clock seconds]
}

# Compare the current year with the saved-data directory name, and if
# it looks like last year, prompt to change it.  Create new directory
# if necessary.
#
# Return 1 on failure, 0 otherwise (like catch).  (The daq does not
# do anything about the error, because it is better to keep taking data
# into the wrong directory than to stop taking data.)

proc mui_check_year { } {
    global midas
    set year [clock format [clock seconds] -format {%Y}]
    set ddir [file split $midas(data_dir)]
    # puts "Perform year check on $midas(yearcheck), $midas(data_dir), $year"
    if { $midas(yearcheck) && [lsearch -exact $ddir $year] < 0 } {
        set elem [lsearch -exact $ddir [expr {$year - 1}] ]
        if { $elem >= 0 } {
            set newdir [eval file join [lreplace $ddir $elem $elem $year]]
            beep
            switch [ timed_messageBox -timeout 60000 \
                        -type yesno -default yes -icon question \
                        -title "Happy New Year" -message \
"The year is now $year, but the 
data directory is still $midas(data_dir).
Shall we change to $newdir now?" ] {
                yes {
                    if { [file exists $newdir] } {
                        if { ! [file isdirectory $newdir] } {
                            beep
                            timed_messageBox -timeout 30000 \
                                    -type ok -default ok -icon error \
                                    -title "Directory failure" -message \
"Proposed data directory $newdir already exists but is not a directory!
Get expert help."
                            return 1
                        }
                    } else {
                        if { [catch { file mkdir $newdir } msg] } {
                            beep
                            timed_messageBox -timeout 30000 \
                                  -type ok -default ok -icon error \
                                  -title "Directory failure" -message \
"Failed to create new data directory $newdir:
$msg
Data directory remains as $midas(data_dir)."
                            return 1
                        }
                    }
                    set midas(data_dir) $newdir
                    td_odb odb_set_general "Change data directory"
                    #puts "Changed data directory to $newdir"
                }
            }
        }
    }
}

proc hvutil_gui { action } {
    switch -- $action {
        save {
            set fname [tk_getSaveFile -defaultextension ".hv" -title "Select HV $action file" \
                           -initialdir $::env(HOME)/hv]
        }
        restore {
            set fname [tk_getOpenFile -defaultextension ".hv" -title "Select HV $action file" \
                           -initialdir $::env(HOME)/hv]
        }
    }
    if { [catch {exec hvutil $action $fname} msg] } {
        timed_messageBox -timeout 20000 -type ok -default ok -icon error \
            -title "CAEN HV $action failure" -message $msg
    }
}

proc hvutil_console { } {
    global midas
    switch  -glob -- $midas(beamline) {
        m20* { set hvhost "m20hv01" }
        default { set hvhost $midas(beamline)hv01 }
    }
    exec xterm +mb +sb -sl 0 -rw +t -geometry 82x25 -fn -misc-fixed-medium-r-normal--15-140-75-75-c-90-iso10646-1 \
        -title "CAEN HV Control" -xrm "XTerm*backarrowKey: true" -e telnet $hvhost 1527 &
}

proc mui_beamline_control { } {
    global midas env
    switch -glob -- $midas(beamline) {
        bn*r { set cmd "?????" }
        m20* { set cmd "ssh -X -nf -o PasswordAuthentication=no m20@sbp1 m20 2>@1 &" }
        default { set cmd "ssh -X -nf -o PasswordAuthentication=no $midas(beamline)@sbp1 $midas(beamline) 2>@1 &" }
    }
    #puts "command: $cmd"
    set code [catch "exec $cmd" results]
    #puts "ssh gives code $code, result $results"
    # delayed check if still active, because errors don't come
    after 800 [list mui_beamline_control_check $code $results]
}

# ensure that ssh process finished cleanly
proc mui_beamline_control_check {code results} {
    if { $code == 0 } {# claims OK
	set pid [lindex $results 0]
	if { [catch { exec ps $pid } psres] } {
	    set code 0
	} else {
	    set code [string match "*<defunct>*" $psres]
	}
    }
    if { $code } {
        timed_messageBox -timeout 20000 \
            -type ok -default ok -icon error \
            -message "Beamline control proves inaccessible from here." \
            -title "No Epics" 
    }
}

global midas

set midas(clt_check_failed) 0
set midas(start_automatic) no
set midas(refresh_id) ""
set midas(showmess_pid) ""

global tdwin modewin rigwin tdxwin statwin logwin iqwin idefwin

# set the window names
if { [catch set tdwin] } { set tdwin {} }
set modewin $tdwin.mode
set rigwin $tdwin.rig
set tdxwin $tdwin.exp
set statwin $tdwin.stat
set logwin $tdwin.logv
set hmuwin $tdwin.hmu
set iqwin  $tdwin.iq
set idefwin $tdwin.idef
set ciswin  $tdwin.cis
set puwin $tdwin.pu
set detwin $tdwin.det
set autorwin $tdwin.ar
set chtitwin $tdwin.ct

# set some default parameters (for use before odb is read)
set rig(num_scalers) 0
set mode(dual_enabled) 0
set run(state) 1
set run(starting) 0
set run(test_mode) -1
set run(in_progress) 0
set run(paused) 0
set run(menustate) {}
set run(starting) 0
set run(number) {}
set run(start_sec) 0
set run(start_record) {}
set run(acqoff_record) {}

set run(sweep_from) 0
set run(sweep_to) 100
set run(sweep_incr) 1
set run(comment1) ""
set run(comment2) ""
set run(comment3) ""

set rstat(inv) 0
set rstat(ovf) 0
set rstat(hits_checked_at) 0
set rstat(previous_cts_total) 9e9
set rstat(previous_hits) [list 9e9 9e9 9e9]
set rstat(prev_save_file_name) ""
set rstat(counts_message) ""

set wtstat(enabled) 0
set run(autoTvar) {none}
set run(autoBvar) {none}
set rtitles(autoTvar) {none}
set rtitles(autoBvar) {none}
set rtitles(Field_lab) "Field:"
set rtitles(Temp_lab) "Temperature:"
set rtitles(ImuSR) -9 ; # A number neither 0 nor 1, so initial display always reconfigures
set rtitles(changing) 0
set rtitles(validate) 1
set rtitles(noV4exp) "XYZt"

# other initializations
set mrig(changing) 0
set mmode(changing) 0
set midas(connected) 0
set midas(expt) $cfg::expt
set midas(application) musrrc
set midas(num_bg_err) 0
set midas(beamline) {}
set midas(yearcheck) 1
set midas(clt_stat) {}
set midas(clt_defer) 0
set midas(data_dir) .
set midas(camplogging) 1
set midas(failed_FE_checks) 0
set run(bg_start_out) {}
set run(did_stop_at) 0
set autor(enabled) 0

# Only the muSR gui invokes this file, so we must be musr
set midas(musr) 1

# midas(host) is normally null, meaning to use localhost without going through the
# network (or even mserver), but may be set otherwise if environment variables 
# point to a different host.

set midas(host) ""
if { [info exists env(MIDAS_SERVER_HOST)] } {
    set midas(host) $env(MIDAS_SERVER_HOST)
}

# Preference for nagging:
set run(confirm_stop) 2

global rtitles


#  List of run-title fields.   These are look-up tags in the arrays run() and
#  rtitles() (the latter being the working, in-transition values), and ALSO 
#  parts of the entry-widget names, e.g., "$tdwin.td_operator_e"
#
set rtitles(list) [list runtitle sample temperature field orientation operator experiment \
        comment1 comment2 comment3 subtitle]
# Initialize titles to null
foreach b $rtitles(list) { 
    set run($b) {}
    set rtitles($b) {}
}


# Get name of ostensible camp server that we connected to.
set midas(camp_host) [info hostname]
if { [info exists env(CAMP_HOST)] } {
    if { [string length $env(CAMP_HOST)] } {
        set midas(camp_host) $env(CAMP_HOST)
    }
}

if { [catch { set mui_utils_loaded }] } {
    source [file join $muisrc mui_utils.tcl]
}
