# Routines to send camp_cmd through a different "subserver" process, and to restart
# that process if it does not respond.  Communication is through a bi-directional pipe. 
# All this is because the real camp server is hanging up in ways that freeze some
# clients; their rpc calls never time out.
#
#  $Log: camp_emissary.tcl,v $
#  Revision 1.9  2016/09/06 22:11:36  asnd
#  Minor Cleanup
#
#  Revision 1.8  2015/04/18 00:07:37  asnd
#  Make camp timeouts longer, because some actual commands are slow
#
#  Revision 1.7  2015/03/20 00:56:56  suz
#  changes by Donald, and some regressions! This version part of package for new VMIC frontends
#
#  Revision 1.6 Misc. changes.
#  Revision 1.5 Small error message change
#  Revision 1.4 Check for no camp host name
#  Revision 1.3 Change rebooting, locking, and remove debug noise
#  Revision 1.2 debug campvarlog; handle missing camp_cmd better
#  Revision 1.1 Camp access using emissary sub-server

package require BLT

# New requests will be turned away if the queue of pending requests exceeds this
set subserv(maxpending) 5

# Each request times out after $subserv(timeoutms) ms (15 sec)
set subserv(timeoutms) 15000

# The server is declared dead when there are at least $subserv(errorlimit)
# consecutive errors spanning at least $subserv(totdeadtime) seconds.  
# totdeadtime should be greater than the time it takes to reboot the camp
# server, to avoid the trap of perpetual rebooting.
#
set subserv(errorlimit) 4
set subserv(totdeadtime) 50

set subserv(locktime) 0
set subserv(rebooting) 0
set subserv(starting) 0

# Start the "subserver"
#
proc start_camp_subserver { } {
    global muisrc subserv env midas
    if { $midas(camp_host) == "" || $midas(camp_host) == "none" } { return }
    puts "start_camp_subserver for camp host $midas(camp_host)"
    if { $subserv(starting) } {
        # Already starting (supposedly). Wait for it to start, or time-out.
        set aaa [after 10000 { set subserv(starting) 0 }]
        # Harmful vwait!!!
        vwait subserv(starting)
        after cancel $aaa
        if { [info exists subserv(fh)] } {# It started already
            set subserv(starting) 0
            return
        }
    }
    set subserv(starting) 1
    set env(CAMP_HOST) $midas(camp_host)
    set subserv(host) $midas(camp_host)
    if { [info exists env(CAMP_DIR)] && [file executable "$env(CAMP_DIR)/tclsh_camp"] } {
        set campprg "$env(CAMP_DIR)/tclsh_camp"
    } elseif { [string first "/camp/" $env(PATH)] } {
        set campprg "tclsh_camp"
    } elseif { [info exists env(MUSR_DIR)] && [file executable "$env(MUSR_DIR)/camp/linux/tclsh_camp"] } {
        set campprg "$env(MUSR_DIR)/camp/linux/tclsh_camp"
    } elseif { [file executable ~musrdaq/musr/camp/linux/tclsh_camp] } {
        set campprg "~musrdaq/musr/camp/linux/tclsh_camp"
    } else {
        # No tclsh_camp detected, so hope it is resolvable
        set campprg "tclsh_camp"
    }
    if { [catch { set subserv(fh) [open "|$campprg [file join $muisrc verbose_subserver.tcl]" RDWR] } stat] } {
        puts "Failed to start Camp sub-server; $stat"
        return -code error "Failed to start Camp sub-server; $stat"
    }
    set subserv(pid) [pid $subserv(fh)]
    array set subserv { reqid 0 queuelen 0 consecerr 0 lost_time 0 starting 0 }
    array unset subserv mb*
    fconfigure $subserv(fh) -buffering line -blocking 0
    fileevent $subserv(fh) readable [list read_camp_subserver $subserv(fh)]
}

# Stop the camp subserver, if it is running.
#
proc stop_camp_subserver { } {
    global subserv
    puts "stop_camp_subserver "
    if { ![info exists subserv(pid)] } { 
        if { [info exists subserv(fh)] } {
            catch { set subserv(pid) [pid $subserv(fh)] }
        } else {
            return
        }
    }
    # Ensure that subserver does get stopped, even if it is hung.
    # (This is incautious, and we could use [exec ps...] to check before killing.)
    if { [info exists subserv(pid)] } { 
        after 1500 [list catch [list exec kill $subserv(pid)]]
        after 4000 [list catch [list exec kill -9 $subserv(pid)]]
        after 4100 {# trigger vwait
            set subserv(pid) 0
            unset subserv(pid)
        }
    }
    # Close pipeline, which is usually enough to terminate subserver
    if { [info exists subserv(fh)] } {
        set fh $subserv(fh)
        unset subserv(fh)
        # answer all pending requests with a "server shutdown" error
        foreach mb [array names subserv mb*] {
            set subserv($mb) [list 1 "server shutdown"]
        }
        catch { close $fh }
    }
    vwait subserv(pid)
    puts "Done stopping"
}

# Read a result, and place it in the proper mailbox.

proc read_camp_subserver { fh } {
    global subserv
    if { ![info exists subserv(fh)] } { return }
    puts "subserv pipe is readable?  [info exists subserv(fh)] && ![eof $subserv(fh)]"
    if { [catch {gets $fh buf} ret] } {# Error on read
         puts "error reading Camp pipe: $ret"
         return
    } elseif { $ret == 0 } {# no error but zero chars
        if { [eof $fh] } {# EOF => pipe was closed => program exited
            puts "Camp connection exited (eof on pipe)"
            # something more
        }
        return
    }
    set ll 0
    catch { set ll [llength $buf] }
    if { $ll != 3 } {
        if { $ll != 0 } {puts "Got bad camp readback: |$buf|"}
        return
    }
    foreach {id code result} $buf break
    if { [info exists subserv(mb$id)] } {
        puts "put result |$buf| in mb$id"
        set subserv(mb$id) [list $code $result]
    }
    # else, discard result -- caller probably timed out
}

# Send a command; set up mailbox for receiving response
#
proc subserver_camp_cmd { cmd } {
    global subserv
    # Reject request if subserver not opened
    if { ! [info exists subserv(fh)] } {
        puts "Not connected to Camp now."
        return -code error "Camp server not available right now"
        # (we could decide to open new connection when disconnected, but 
        # that would be very messy for prolonged downtimes, like during
        # a Camp reboot.)
    } 
    # reject request if queue is too long
    if { $subserv(queuelen) >= $subserv(maxpending) } {
        puts "Too many pending Camp commands ($subserv(queuelen) >= $subserv(maxpending))."
        return -code error "Too many pending Camp commands"
    }
    incr subserv(queuelen)
    set id [incr subserv(reqid)]
    set reqtime [clock seconds]
    set subserv(mb$id) {}
    set afterid [after $subserv(timeoutms) [list set subserv(mb$id) [list 1 "timeout from Camp doing {$cmd}"]]]
    puts "Send request $id: |$cmd|"
    puts $subserv(fh) [list $id $cmd]
    puts "Await response $id"
    # Harmful vwait here!!!
    vwait subserv(mb$id)
    after cancel $afterid
    if { ! [info exists subserv(mb$id)] } {
        puts "Got NO response $id"
        return -code error "Mailbox for this camp command was prematurely closed!"
    }
    foreach {code result} $subserv(mb$id) break
    unset subserv(mb$id)
    puts "Got response $id: $code, |$result|"
    incr subserv(queuelen) -1
    if { $subserv(queuelen)>0 } { puts "subserv queue len $subserv(queuelen): [array names subserv mb*]" }
    if { $code } { # error of some kind
        if { $subserv(lost_time) == 0 } {
            set subserv(lost_time) $reqtime
        }
        incr subserv(consecerr)
        puts "stat $code, consec err $subserv(consecerr), dead since [clock format $subserv(lost_time)]"
        if { $subserv(consecerr) > $subserv(errorlimit) && \
                 [clock seconds] - $subserv(lost_time) > $subserv(totdeadtime) } {
            set subserv(lost_time) 0
            set subserv(consecerr) 0
            restart_camp_subserver
        }
    } else {
        set subserv(lost_time) 0
        set subserv(consecerr) 0
        puts "Success"
    }
    return -code $code $result
}

proc restart_camp_subserver { } {
    global subserv midas
    puts "Restart camp subserver !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    stop_camp_subserver
    if { $midas(camp_host) == "" || $midas(camp_host) == "none" } {
        set subserv(host) $midas(camp_host)
        puts "Don't start anything because there is no server defined"
        return
    }
    if { $subserv(locktime) >= [clock seconds] } {
        # If we are in the middle of a camp reboot, then just fail here to
        # get out of the way.  (vwait would prevent the reboot from proceeding
        # because of recursive event loops.)
        if { $subserv(rebooting) } {
            return -code error "Camp host not available yet"
        }
        # We must be presently checking the state of the camp server. 
        # Merge stream with that instance of check_camp_server.
    }
    if { $midas(clt_defer) > [clock seconds] } {
        puts "Postpone Camp check because clients check is in progress"
        return -code error "Busy restarting other clients"
    }
    if { [check_camp_server] != 0 } {
        if { [info exists subserv(fh)] } {
            puts "subserver restarted while we were checking server"
            return
        }
        puts "reboot real camp server OH NOOOOOOOOOOO!!!!!!!!!!"
        set subserv(rebooting) 1
        set midas(clt_defer) [expr {[clock seconds] + 60}]
        if { [llength [info command timed_messageBox]] } {
            puts "Query user to reboot Camp server"
            mui_camp_reboot
        } else {
            puts "Silently reboot Camp server"
            campserv_do_reboot
        }
        set midas(clt_defer) 0
    } else {
        puts "Server checks out fine"
    }
    start_camp_subserver
    set subserv(rebooting) 0
    puts "Finished restart_camp_subsever"
}

proc check_camp_server { } {
    global midas subserv env
    puts  "check_camp_server $midas(camp_host)"
    if { $subserv(locktime) > [clock seconds] } { 
        puts "Wait for alternate check to complete"
        vwait subserv(ex_stat)
        puts "Alternate check gave $subserv(ex_stat)"
    } else {
        puts "Lock check"
        set subserv(locktime) [expr {[clock seconds] + 6}]
        puts "exec camp_cmd to test"
        if { [info exists env(CAMP_DIR)] && [file executable "$env(CAMP_DIR)/camp_cmd"] } {
            set campcmd "$env(CAMP_DIR)/camp_cmd"
        } elseif { [string first "/camp/" $env(PATH)] } {
            set campcmd "camp_cmd"
        } elseif { [info exists env(MUSR_DIR)] && [file executable "$env(MUSR_DIR)/camp/linux/camp_cmd"] } {
            set campcmd "$env(MUSR_DIR)/camp/linux/camp_cmd"
        } elseif { [file executable ~musrdaq/musr/camp/linux/camp_cmd] } {
            set campcmd "~musrdaq/musr/camp/linux/camp_cmd"
        } else {
            # No camp_cmd detected, so hope it is resolvable
            set campcmd "camp_cmd"
        }
        set subserv(ex_stat) {error 0 1 error}
        set subserv(ex_output) "Timed out"
        set af [ after $subserv(timeoutms) { set subserv(ex_stat) {timeout 0 1 timeout}} ]
	puts "bgexec $campcmd -node $midas(camp_host) sysGetInsNames"
        catch { blt::bgexec subserv(ex_stat) -output subserv(ex_output) \
                    $campcmd -node $midas(camp_host) sysGetInsNames }
        after cancel $af
        puts "Done exec: $subserv(ex_stat)"
        set subserv(locktime) 0
    }
    return [lindex $subserv(ex_stat) 2]
}

# Interact with the user and initiate a reboot of the camp server.
#
proc mui_camp_reboot { } {
    global midas run subserv

    # NO!  Use locktime only for check_camp_server!
    # set subserv(locktime) [expr {[clock seconds] + 50}]
    set mes "The Camp server is not responding!\nReboot the $midas(camp_host) computer?"
    set default yes
    if { $run(in_progress) && $run(ImuSR) } {
        append mes "\n(Run will end.)"
        set default no
    }
    beep
    set c [ timed_messageBox -timeout 30000 \
                -type yesno -default $default -icon warning \
                -message $mes -title "Restart Camp Server?" ]
    #set subserv(locktime) [expr {[clock seconds] + 50}]
    if { [string equal $c yes] } {
        campserv_do_reboot
    } else {
        #set subserv(locktime) 0
        set subserv(rebooting) 0
        set midas(clt_defer) 0
    }
}


proc campserv_do_reboot { } {
    global midas hmutil hmuwin subserv
    puts "campserv_do_reboot"
    campserv_cleanup_daq
    set hmutil(message) {}
    hm_popup_mes "Attempt to re-boot $midas(camp_host)..."
    #
    #   Try giving the reboot command via telnet
    #
    hm_popup_mes "\nSend \"reboot\" via Telnet..."
    if { ! [ catch { hm_telnet_cmd $midas(camp_host) "reboot" } status ] } {
        hm_popup_mes "\nThe $midas(camp_host) computer should be rebooting now.\nPlease wait until it finishes."
    } elseif { [string first "System engaged" $status] >= 0  || [string first "shell is locked" $status] >= 0 } {
        hm_popup_mes "\nCamp system console is locked. It may be rebooting."
        hm_popup_mes "\nIf Camp does not come back,\nyou may need to push the Reset button\non $midas(camp_host)"
    } else {
        hm_popup_mes "\nFailed: $status."
    }
    after 2000 {campserv_watch_reboot [clock seconds] 0}
    # Block until server is restarted 
    vwait subserv(consecerr)
}

#  Watch for completion of reboot, remembering how long we've been watching.
proc campserv_watch_reboot { start previously } {
    puts "campserv_watch_reboot ***************************************** "
    global subserv midas
    if { [check_camp_server] == 0 } { # Restored!
        hm_popup_mes "\nCamp server is restored."
        after 5000 { catch { destroy .hmupopup } }
        set subserv(consecerr) 0
        set subserv(rebooting) 0
        # plan to check Midas clients soon
        set midas(clt_defer) 0
        set midas(clt_checked_at) 0
        return
    }
    set elapsed [expr {[clock seconds]-$start}]
    # Check for some milestones
    if { $elapsed >= 200 && $previously < 200 } {
        # After a long wait, go for it again.
        mui_camp_reboot
    } else {
        if { $elapsed >= 55 && $previously < 55 } {
            hm_popup_mes "\nCamp server has not booted.\nPush the Reset button on $midas(camp_host)\n(in the VME crate)"
        }
        after 5000 [list campserv_watch_reboot $start $elapsed]
    }
}

#
proc campserv_cleanup_daq { } {
    global midas run
    # Only do anything if we know how
    if { [llength [info proc mui_end_and_clean]] && $midas(musr) } {
        if { $run(ImuSR) } { # rebooting Camp can disrupt several clients
            puts "Shutdown any imusr run and stop mheader client"
            mui_shutdown_client mheader*
            mui_end_and_clean
        }
        puts "shut down mdarc in case it is frozen talking to Camp"
        mui_shutdown_client mdarc*
    }
    set midas(clt_defer) 0
    set midas(clt_checked_at) 0
}

# Provide defaults
set midas(clt_defer) 0
set midas(clt_checked_at) 0
