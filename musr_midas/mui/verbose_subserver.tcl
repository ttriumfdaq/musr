#!/usr/bin/tclsh
#! really needs tclsh_camp!
# This is an intermediary between the mui (or campvarlog) client and the
# camp server; a "subserver".  It reads camp commands from stdin, executes
# them using the built in camp_cmd, and writes the results to stdout.
# It normally runs in a pipeline rather than interactively.
#
# This may seem rather pointless, but the camp server has been crashing
# in a strange way that locks up clients (the pending rpcs never time out!)
# and I have not been able to find the problem.  This subserver will bear 
# the brunt of such lock-ups, leaving the mui in a functional state able to
# restart both the main camp server and this subserver.
#
# If and when the camp server crashes get fixed, this subserver can be
# dropped.
#
# Each request comes with a numeric identity code, so the result can remain
# associated with the request. 

# Local testing:

set debf [open camp_subserver.log a]
fconfigure $debf -buffering line

puts $debf "Now in tclsh_camp for server"
puts $debf "Start"
puts $debf "Camp host: $env(CAMP_HOST)"
catch {camp_cmd sysGetInsNames} inst
puts $debf "Instruments connected are: $inst"

proc main { } {
    global debf
    while 1 {
        puts $debf "Wait for command."
        set n [gets stdin buf]
        puts $debf "read $n char: |$buf|"
        if { $n < 0 } { exit }
        set ll 0
        catch { set ll [llength $buf] }
        if { $ll != 2 } { 
            puts $debf "Ignore that improper command!"
            continue
        }
        foreach { id cmd } $buf {}
        puts $debf "$id $cmd ..."
        set code [catch {camp_cmd $cmd} resp]
        # Because we read line-by-line, concatenate multiline camp responses.
        # (Currently, they are left that way, but we could choose a more distinctive
        # separator string than "|" and have the client map back to newlines.)
        set resp [string map {"\n" "|"} $resp]
        puts $debf "$id $code $resp"
        puts [list $id $code $resp]
    }
}

main


