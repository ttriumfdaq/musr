/********************************************************************\
  Name:        musr_config.c
  Created by:   Suzannah Daviel

  Contents: check/calculate input parameter values needed by the user interface.
            copy settings written to odb by mui (in rig/mode areas)
            to the mdarc/histogram area in odb needed by mdarc.

  Combined version for I and TD-musr 
           based on imusr_config version 1.6 which was based on musr_config.c 1.8

   $Log: musr_config.c,v $
   Revision 1.19  2015/03/20 01:05:43  suz
   VMIC version

   Revision 1.18  2006/09/22 08:08:08  asnd
   Remove some limitations and bugs regarding dual spectra names

   Revision 1.17  2004/10/19 17:38:29  suz
   change to cm_register_transition for Midas 1.9.5

   Revision 1.16  2004/09/29 18:32:52  suz
   get rid of a warning when compiling

   Revision 1.15  2004/04/08 18:18:53  suz
   change a comment

   Revision 1.14  2004/02/09 20:59:30  suz
   version for combined TD and IMUSR. Based on imusr_config 1.6


   imusr_config based on musr_config.c 1.8
 \********************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h> // fabs

/* midas includes */
#include "midas.h"
#include "msystem.h" //MAX_STRING_LENGTH
//#include "ybos.h" // MAX_FILE_PATH

/* darc includes */

#include "experim.h" /* odb structure */

#define LEN_NODENAME 127 // defined in camp.h (needed by darc_odb.h)
#include "darc_odb.h" // needed for MAX_HIS,MAX_CNTR,HIS_SIZE, MAX_NSCALS

// imusr includes
#include "imusr_inputs.h" /* IO reg and Scaler fixed inputs */
#include "imusr_scaler_sum.h" /* offsets into scaler_sum array */
#include "musr_common_subs.h" /* function prototypes */

// prototypes

// common to IMUSR and TDMUSR
INT tr_prestart(INT rn, char *error);
INT tr_poststop(INT run_number, char *error);
INT musr_write_v680(void);
INT musr_create_rec(void);
INT musr_get_rec(char *rig);
INT get_musr_type(void);

// TD musr only
INT TD_musr_validate_params(void);
INT TD_musr_select_inputs(BOOL dual_spectra_mode);
INT TD_musr_sort_and_index(void);
INT TD_musr_convert(BOOL dual_spectra_mode);
INT TD_musr_check_bins(DWORD nbin, INT shift, INT nhisto, INT bytes_per_bin);
INT TD_musr_write_odb(void);
INT TD_musr_write_mdarc(void);
INT TD_musr_diag(void);
INT TD_musr_write_scaler();
void TD_musr_check_gates(void);

// I_MUSR only
INT I_musr_validate_params(void);
INT I_musr_write_odb(void);
INT I_musr_write_imdarc(void);
INT I_musr_check_toggle(void);
INT I_musr_write_scaler(void);
INT I_musr_validate_sweep_values(void);
INT I_musr_get_limits (char *pinner, char *pouter, float start,float stop, float innertogval, float outertogval,float *pmin, float*pmax);
float smin(float a, float b);
float smax(float a, float b);
// end of prototypes

MUSR_TD_ACQ_SETTINGS tdmusr;
MUSR_TD_ACQ_MDARC fmdarc;
MUSR_TD_ACQ_V680 fv680;
MUSR_I_ACQ_SETTINGS imusr;
SCALER_SETTINGS fscal;



BOOL debug;
BOOL I_MUSR=FALSE; /* TRUE if I_MUSR is running */
BOOL TD_MUSR=FALSE; /* TRUE if TD_MUSR is running */

HNDLE hDB=0, hMdarc=0, hTS=0, hOut=0, hV680=0, hScal=0, hIS=0;

char td_eqp_name[] = "MUSR_TD_acq"; /* TD_MUSR area - used also for IMUSR */
char j_eqp_name[] = "MUSR_I_acq"; /* I_MUSR area - used ONLY for IMUSR */ 

DWORD  time_save;

/* globals for his_analyze & its subroutines: */

INT tot_cnt, ncnt, nhis;
INT selTdcInputs[MAX_HIS]; // array to contain selected TDC inputs
INT ioChan[MAX_CNTR];    // array to contain selected IO reg channels
INT HM_index[MAX_HIS]; // index into frontend histogram data array  
INT HM_sorted[MAX_HIS];    // array to contain sorted selected TDC inputs
char hist_titles[MAX_HIS][11]; // array to contain histogram titles
INT bitpat_tdc, bitpat_ioreg;
BOOL dual_spec;
BOOL ioreg;
INT  single_loop; // single_loop = 1 if in single sweep mode
BOOL no_update; // set if single_loop and running
BOOL error;

/* globals for IMUSR sweep params */
float  gbl_sweep_start ;
float   gbl_sweep_stop ;
float  gbl_sweep_step ;
float  gbl_sweep_direction ;
  char gbl_inner_toggle_type[5];
  char gbl_outer_toggle_type[5];


#define MAX_SUFF 4

INT TD_musr_validate_params (void)
{
  /*
            TD - MUSR :
	    ===========
Counter order gives desired histogram order. Frontend stores histograms in
TDC input order, so index is needed into data array to select correct
histogram data.

Also dual spectra mode DOUBLES the no. of histograms generated.
Normal mode : 1 counter -> 1 histogram
Dual spectra mode: 1 counter -> 2 histograms
     
TD_musr_validate_params does the following:
  *  Checks no. bins  is < maximum available in midas buffer (or HM) 

  *  Accesses TD musr area of ODB to find INPUT parameters:
     
        a. list of DEFINED  counters, their TDC input no., their I/O reg output
           no.
        b. List of SELECTED counters for histogramming
        c. Number of DEFINED and SELECTED counters
        d. Status of Dual spectra mode (enabled / disabled)
        e. Status of IO reg (enabled/disabled)


   * Checks for duplicate counters in DEFINED and SELECTED counter arrays
      
   * Fills array selTdcInputs (selected TDC inputs) in counter (or
      histogram) order

   * Fills array ioChan (selected I/O channels) in counter order

   * Sorts counter inputs into order as for frontend histogram data array
   
   * Generates INDEX into frontend histogram data array

   * if dual spectra mode, doubles histogram numbers and adjusts INDEX array
   
   * Generates histogram names

   * Generates TDC input bit pattern, O_R bit pattern, O_R mask if necessary

  *  Checks resoln code, bytes/bin and no. bins  is < maximum available in frontend 

   * Updates v680 area (includes copying ggl gate lengths from mode area to v680 area)

   * Updates mdarc area

   * Validates scalers and updates /equipment/scalers/settings

   */

  INT i,j,k;
  INT status;
  INT max, shift, his_length;
  
  // print something out
  if(debug)printf("TD_musr_validate_params: starting ... current rig is %s\n",tdmusr.rig.current_rig);


/* clear out all arrays - avoid any confusion in odb by clearing unused values */
  for (j=0;  j < MAX_HIS; j++)
  {
    sprintf(hist_titles[j],"");
    HM_index[j] = HM_sorted[j] = selTdcInputs[j] = -1;  // use -1 as 0 is a
    // valid value
  }
  for (j=0;  j < MAX_CNTR; j++)  // MAX_CNTR array size
    ioChan[j] = -1;       
  
  dual_spec = tdmusr.mode.dual_spectra_mode.enabled;
  tot_cnt = tdmusr.rig.counters.number_defined; 
  ncnt = tdmusr.mode.histograms.num_selected_counters;
  ioreg =  tdmusr.rig.output_register.enabled;

  if (debug)
  {
    printf("TD_musr_validate_params: no. defined counters %d, no. selected counters = %d\n",tot_cnt,ncnt);
    if(ioreg)
      printf("TD_musr_validate_params: IO register is enabled\n");
    else
      printf("TD_musr_validate_params: IO register is disabled\n");
  }

  /* check validity of parameters */
  if(tot_cnt <= 0 || ncnt <= 0 )
  {
    cm_msg(MERROR,"TD_musr_validate_params","Invalid number of counters defined (%d) or selected (%d)",
           tot_cnt,ncnt);  
    return (DB_INVALID_PARAM);
  }
  if (ncnt > tot_cnt)
  {
    cm_msg(MERROR,"TD_musr_validate_params","No. of selected counters (%d) > number defined (%d)",ncnt,tot_cnt);
    return (DB_INVALID_PARAM);
  }

  nhis = ncnt;  // default

  // print the defined values to start with
  printf(   "\nDEFINED:  TdcInput ioChan Counter Name\n");
  for (i=0; i< tdmusr.rig.counters.number_defined; i ++)
    printf ("            %2d       %2d    %s\n", 
	    tdmusr.rig.counters.tdc_input[i],
	    tdmusr.rig.output_register.outputs_in_use[i],
	    tdmusr.rig.counters.names[i]);


  status =  TD_musr_select_inputs(dual_spec);
  if (status != DB_SUCCESS)
    return(status);
/*
  sort the tdc input array & fill HM_index array
*/

  status = TD_musr_sort_and_index();
  if (status != DB_SUCCESS)
    return(status);  
/* write the information back into the odb
   ncntr,nhis,hist_titles,selTdcInputs,HM_sorted,HM_index
   
*/
  
  status = TD_musr_convert(dual_spec);  
  if (status != DB_SUCCESS)
    return(status);   

  printf ("\n HM_sorted     HM_index    selected     histogram\n");
  printf (  "                           TDC input    title\n");
  
  for (i=0; i< nhis; i++)
  {
    printf("  %2d             %2d           %2d         %- 10s\n",
           HM_sorted[i],HM_index[i], selTdcInputs[i], hist_titles[i]) ;
  }

/*
  Check validity of v680 parameters

*/
  
// check resolution code

  if ( tdmusr.mode.histograms.tdc_resolution_code <  16 ||
       tdmusr.mode.histograms.tdc_resolution_code > 28) 
  {
    cm_msg(MERROR,"TD_musr_validate_params","Invalid tdc resolution code (%d);16-28 are valid",
           tdmusr.mode.histograms.tdc_resolution_code);
    return (DB_INVALID_PARAM);
  }
  shift = tdmusr.mode.histograms.tdc_resolution_code - 16;

// check bytes/bin
  if( tdmusr.mode.histograms.bytes_per_bin != 4 &&
      tdmusr.mode.histograms.bytes_per_bin != 2)
  {
    cm_msg(MERROR,"TD_musr_validate_params",
           "Invalid bytes per bin for histograms (%d); 2 or 4 are valid",
           tdmusr.mode.histograms.bytes_per_bin);
    return (DB_INVALID_PARAM);
  }
  
/* check validity of the number of bins requested
   global nhis = num histos, determined by TD_musr_convert
*/
    
  if(debug)printf(
     "\nCalling TD_musr_check_bins with %d bins, shift = %d nhis = %d,bytes/bin = %d\n",
     tdmusr.mode.histograms.num_bins, shift, nhis,
     tdmusr.mode.histograms.bytes_per_bin);
  
  /* max = 0 if check works, otherwise TD_musr_check_bins returns max. bins permitted (per histo) */
  max  = TD_musr_check_bins(tdmusr.mode.histograms.num_bins, shift, nhis,
                         tdmusr.mode.histograms.bytes_per_bin);
  if(max > 0 )
    {
    cm_msg(MINFO,"TD_musr_validate_params","Histogram length (%d) exceeds maximum value (%d) for %d histograms",
	   tdmusr.mode.histograms.num_bins,max,nhis);
    if ( tdmusr.mode.histograms.bytes_per_bin == 4)
      cm_msg(MINFO,"TD_musr_validate_params","For more bins, set 2 bytes/bin or reduce no. of histograms ");
    else
      cm_msg(MINFO,"TD_musr_validate_params","For more bins, reduce the number of histograms");
    return(DB_INVALID_PARAM);
    }

  
  /* fill  tdmusr.output.selected_histograms.num_bins */
  tdmusr.output.selected_histograms.num_bins = tdmusr.mode.histograms.num_bins ; /*  nbins */
  printf ("TD_musr_validate_params: tdmusr.output.selected_histograms.num_bins=%u\n",
	  tdmusr.output.selected_histograms.num_bins);
  
  
  /*
    check the values for the GGL gates  
  */
  TD_musr_check_gates();

  
  /* write the information back into the odb
     ncntr,nhis,num bins,hist_titles,selTdcInputs,HM_sorted,HM_index
  */
  status = TD_musr_write_odb();
  if (status != DB_SUCCESS)
    return(status);   
  
  
  
  status = musr_write_v680();
  if (status != DB_SUCCESS)
    return(status);   

  status = TD_musr_write_mdarc();
  if (status != DB_SUCCESS)
    return(status);   

  status = TD_musr_write_scaler();

  status = TD_musr_diag();
  if(status != DB_SUCCESS) // not a fatal error (labelling of diagnostic channels)
    printf("TD_musr_diag returns failure status (%d)\n",status);
  return (DB_SUCCESS);
}


/* --------------------------------------------------------------------*/

INT TD_musr_check_bins(DWORD  nbins, INT shift, INT nhis, INT bytes_per_bin)
{
  /* Check that bins requested are less than maximum available in the VMIC

     
     The maximum in the VMIC is limited by the setting of midas event buffer size
     
    Input parameters :
    nbin    no. of bins requested
    shift   shift parameter (tdc_res - 16)
    nhis    number of histograms requested
    bytes_per_bin    4 or 2 bytes/bin

    Returns:
    0        success, number of bins requested is < maximum available
    max      maximum bins available per histogram
  */
  
  DWORD     total_byte, total_bin, memory_size;
  INT       i; 
  DWORD     max,max_bytes;
  
  max_bytes = fv680.maxbyte;  // maximum bytes available 
  
  if(debug) printf(
    "\nTD_musr_check_bins starting with %u bins, %d shift, %d bytes/bin, %d channels and max_bytes = %u\n",
         nbins,shift,bytes_per_bin,nhis,max_bytes);
  
  total_bin    = nbins * nhis;
  total_byte   =  total_bin*bytes_per_bin;
  if(debug)
  {
    printf("TD_musr_check_bins: requested %u bins (%d histos) or %u bytes (%d bytes/bin)\n",
           total_bin,nhis,bytes_per_bin,total_byte);
    printf("                 max_bytes = 0x%x  or %u\n",max_bytes,max_bytes);
  }
  if(total_byte > max_bytes)
  {
    printf("TD_musr_check_bins: bytes requested (%u) > maximum available (%u) \n",
           total_byte,max_bytes);
    max = max_bytes/bytes_per_bin; // max bins
    max = max / nhis;    // max bins per histogram
    printf("    Max bins allowed :0x%x %u\n",max,max);
    return (max);
  }

//  printf("success\n");
  return(0); // success
}


/*------------------------------------------------------------------*/ 

void TD_musr_check_gates(void) 
     
/*-------------------------------------------------------------------*/
     
     /* check the GGL gate values are reasonable */
{    
  if(tdmusr.mode.gates.data_gate_code < 2)
    {
      tdmusr.mode.gates.data_gate_code=2; /* minimum value is 2 */
      cm_msg(MINFO,"TD_musr_check_gates","data gate code must be >= 2; adjusted to %d",
	     tdmusr.mode.gates.data_gate_code); 
    }
  if(tdmusr.mode.gates.data_gate_code > 0x7FF)
    {
      tdmusr.mode.gates.data_gate_code=0x7FF; /* maximum value */
      cm_msg(MINFO,"TD_musr_check_gates","data gate code must be < 2047; adjusted to %d",
	     tdmusr.mode.gates.data_gate_code);
    }
  
  
  if(tdmusr.mode.gates.tdc_gate_code < 2)
    {
      tdmusr.mode.gates.tdc_gate_code=2; /* minimum value is 2 */
      cm_msg(MINFO,"TD_musr_check_gates","tdc gate code must be >= 2; adjusted to %d",
	     tdmusr.mode.gates.tdc_gate_code);    
    }
  
  if(tdmusr.mode.gates.tdc_gate_code > 0x7F)
    {
      tdmusr.mode.gates.tdc_gate_code=0x7F; /* maximum value */
      cm_msg(MINFO,"TD_musr_check_gates","tdc gate code must be < 127; adjusted to %d",
	     tdmusr.mode.gates.tdc_gate_code);
    }
  
  
  if(tdmusr.mode.gates.pileup_gate_code < 2)
    {
      tdmusr.mode.gates.pileup_gate_code=2; /* minimum value is 2 */
      cm_msg(MINFO,"TD_musr_check_gates","pileup gate code must be >=2; adjusted to %d",
	     tdmusr.mode.gates.pileup_gate_code);
    }
  
  if(tdmusr.mode.gates.pileup_gate_code > 0x7F)
    {
      tdmusr.mode.gates.pileup_gate_code=0x7F; /* maximum value */
      cm_msg(MINFO,"TD_musr_check_gates","pileup gate code must be < 127; adjusted to %d",
	     tdmusr.mode.gates.pileup_gate_code);
    } 
  return;
}


INT TD_musr_select_inputs(BOOL dual_spectra_mode)
{
  /* Inputs:  dual_spectra_mode
   */
  
  INT status;
  INT i,j,k,max;
  BOOL found_it;
  char selc[128],clist[128];
/*
    finds selected counters in defined counter array

    fills selTdcInputs and ioChan arrays, and hist_titles array with selected counter names 
*/
  // assign maximum number of characters for histogram name

  if(debug)printf("TD_musr_select_inputs: starting\n");
  max=HIS_SIZE-1;
  if(dual_spectra_mode)
    max = max - MAX_SUFF;  // room needed for 2-char suffixes


  // initialize bit patterns and mask
  bitpat_tdc=bitpat_ioreg=0;
  
  for (j=0; j<ncnt; j++)  // for each selected counter
  {
    found_it = FALSE;
    sprintf(selc,"%s",tdmusr.mode.histograms.select_counters_to_histogram[j]);
    if(debug) printf("TD_musr_select_inputs: selected counter no. %d :  %s\n",j,selc);
    if(strlen(selc)==0)
      {
        cm_msg(MERROR,"TD_musr_select_inputs","Empty string detected in selected counter list");
        return (DB_INVALID_PARAM); 
      }
    /* check for duplicated string in selected counter array */
    for (k=j+1; k<ncnt; k++)
    {
      if(debug)printf("k=%d, comparing %s to counter %s\n",k,selc,tdmusr.mode.histograms.select_counters_to_histogram[k]);
      if (strcmp(selc,tdmusr.mode.histograms.select_counters_to_histogram[k]) ==0)
      {
        cm_msg(MERROR,"TD_musr_select_inputs","duplicate counter name \"%s\" in selected counter array",selc);
        return(DB_INVALID_PARAM);
      }
    }
    
    for(i=0; i<tot_cnt; i++) // search defined counter array for this counter
    {
      sprintf(clist,"%s",tdmusr.rig.counters.names[i] );
      //printf("defined counter no. %d :  %s\n",i,clist);
      if (strncmp (selc,clist,max) == 0)  // counternames must be unique to max chars
      {
        // check for duplicated string in defined counter array
        if(found_it)
        {
          cm_msg(MERROR,"TD_musr_select_inputs","Duplicate counter name \"%s\" in defined counter array",selc);
          return(DB_INVALID_PARAM);
        } 
        found_it = TRUE;
        if ( strlen (selc) > max)
        {
          cm_msg(MINFO,"TD_musr_select_inputs",
                 "Warning - selected counter name \"%s\" will be truncated to %d chars as histo name",selc,max);
        }
        if(debug)
        {
          printf ("found selected counter \"%s\" in counter array (index=%d)\n",selc,i);
          if (ioreg)
            printf ("tdc_input no = %d and io_reg_input = %d for this counter\n",
                    tdmusr.rig.counters.tdc_input[i],
                    tdmusr.rig.output_register.outputs_in_use[i]);
          else
            printf ("tdc_input no = %d  for this counter\n",
                    tdmusr.rig.counters.tdc_input[i]);
        }          
        
        selTdcInputs[j]= tdmusr.rig.counters.tdc_input[i];

// check that ioregister channel number is valid 0-9 are valid, 15 reserved for
	// dual spectra mode   10-14 for IMUSR
	
//             (actually labelled 1-16 on front panel but we are using 0-15 by software )
        if(ioreg)
        {
          if( tdmusr.rig.output_register.outputs_in_use[i] >= 0 &&
              tdmusr.rig.output_register.outputs_in_use[i] < 10)
            ioChan[j]   =  tdmusr.rig.output_register.outputs_in_use[i];
          else
          {
            cm_msg(MERROR,"TD_musr_select_inputs",
		   "invalid IO reg channel (%d); 0-9 are valid, 11-15 reserved",
                   tdmusr.rig.output_register.outputs_in_use[i] );
            return(DB_INVALID_PARAM);
          }
        }
        
        sprintf(hist_titles[j],"%s",selc); // use counter title as hist title
        // calculate bit patterns
        bitpat_tdc = bitpat_tdc     | ( 1 << selTdcInputs[j] );
        if(ioreg)
	  {
	    INT kk;
	    /* check for duplicate input  */
	    kk=( bitpat_ioreg & (1<< ioChan[j])  );
	    if(kk > 0)
	      {  
		cm_msg(MERROR,"TD_musr_select_inputs",
		       "Duplicate IO register input[%d] = %d; bitpat=0x%x ",
		       j, ioChan[j],bitpat_ioreg);
		return(DB_INVALID_PARAM);
	      }
	    bitpat_ioreg = bitpat_ioreg | ( 1 << ioChan[j] );
	  }

	
        //break; /* found the counter but continue to check in case of duplicate string */
      }
      //      printf(" did not find selected counter %s, i=%d\n",selc,i);
    } // end of defined counter array
    if(debug)printf(" end of searching defined counter array, with  selected counter %s i=%d,j=%d\n",selc,i,j);
    if (!found_it)
    {
      cm_msg(MERROR,"TD_musr_select_inputs",
             "Could not find selected counter \"%s\" in defined counter array (i=%d,j=%d)",selc,i,j);
      return (DB_INVALID_PARAM);
    }
  } // next selected counter

  
//  if(debug)
  {
    printf(  "\nSELECTED: selTdcInputs   ioChan  counter name\n");
    for (i=0; i<j; i++)
      if(ioreg)
        printf("              %2d           %2d     %- 10s\n",
	       selTdcInputs[i],ioChan[i],hist_titles[i]);
      else
        printf("              %2d        disabled   %- 10s \n",
	       selTdcInputs[i],hist_titles[i]);
  }
  printf("Bitpatterns   tdc:0x%x   ioreg:0x%x  \n",bitpat_tdc,bitpat_ioreg);
  return (DB_SUCCESS);
  
}

INT TD_musr_sort_and_index(void)
{
  /*
    Sort selected tdc inputs into ascending order into array HM_sorted
    Histogram data is stored in HM in ascending
    order of tdc inputs.

    Fill array HM_index (in counter order) with index into array HM_sorted
    This array (HM_index) will be used by the frontend to send the data out in
    counter order.
    
  */
    
  INT status;
  INT i,j,k;
  BOOL found_it;

  if(debug)printf("TD_musr_sort_and_index starting\n");
  
  k=0; // index into HM_sorted and HM_index arrays
  for (i=0; i<MAX_CNTR; i++)
  {   // loop through inputs available (0-7)
    found_it=FALSE;
    for (j=0; j<ncnt; j++)
    {
      if (selTdcInputs[j] == i )
      {
        if(found_it)
        {
          cm_msg(MINFO,"TD_musr_sort_and_index","Duplicate TDC Input found (%d) in selected TDC input list",i);
          return(DB_INVALID_PARAM);
        } 
        found_it = TRUE;
        if(debug)printf(" found input i=%d at index %d of selected TdcInput array\n",i,j);
        HM_sorted[k] = i;
        k++;
      }
    }
  }

  /* now find index in HM_sorted array to the histogram data for each counter */
  for (i=0; i<ncnt; i++)
  {
    if(debug)printf("looking for TDC Input %d at index %d in HM_sorted array\n",selTdcInputs[i],i);
    for( j=0; j<ncnt; j++)
    {
      if (selTdcInputs[i] == HM_sorted[j] )
      {
        if(debug) printf ("found input %d in HM_sorted array at sorted index %d\n",selTdcInputs[i],j);
        HM_index[i] = j;  // this is index into HM_sorted; histo data sent out
        // by frontend in sorted tdc input order
        
        break;
      }
    }
  }
  // if (debug)
  {
    printf ("\nHM_sorted     HM_index    selected     counter\n");
    printf (  "                          TDC inputs   name\n"); 
    for (i=0; i< ncnt; i++)
    {
      printf( "  %2d             %2d          %2d         %- 10s\n",
              HM_sorted[i],HM_index[i],  selTdcInputs[i], hist_titles[i]  );
    }
  }
  return (DB_SUCCESS);
}

INT TD_musr_convert(BOOL dual_spectra_mode)
{
/*
  Convert counter arrays to histogram arrays 

  For DUAL SPECTRA mode:
    nhis = 2 * ncnt
    fix up histogram titles from counter names and suffixes
    double up selTdcInputs, HM_sorted, HM_index arrays 
    double up bitpat_tdc
    turn on bit 15 in ioreg
    
  For NORMAL mode:
    nhis = ncnt
    checks title length only
    
      ( maximum histogram title length = 10 )
  
 Input: dual_spectra_mode

*/
  INT status;
  INT i,j,len,maxl;
  char string[20],smp[10],ref[10];

  
  if( !dual_spectra_mode)
  {
/* normal mode */
    printf(" TD_musr_convert: dual spectra mode is not enabled\n");

    nhis = ncnt;   // one histogram per counter
    for (i=0; i<ncnt; i++)
    {
      sprintf(string,"%s",hist_titles[i]);
      len=strlen(string);
      if(len > 10) string[10]='\0'; // terminate string
    }
    
  }
  else
  {
    /* dual spectra mode */
    
    nhis = ncnt *2 ;  // double the histograms for dual spectra mode
    bitpat_tdc   = bitpat_tdc | ( bitpat_tdc << 8 );
    if(ioreg)
    {
      bitpat_ioreg = bitpat_ioreg | 0x8000 ; // turn on ioreg bit 15 for dual spec mode
      printf("Dual Spec bitpatterns for tdc:0x%x; ioreg:0x%x\n",
             bitpat_tdc,bitpat_ioreg);
    }
    else
      printf("Dual Spec bitpattern for tdc:0x%x \n",bitpat_tdc);
    
    // get suffixes for histo titles
    sprintf(smp,"%s",  tdmusr.mode.dual_spectra_mode.suffix[0]);
    sprintf(ref,"%s",  tdmusr.mode.dual_spectra_mode.suffix[1]);
    
    //if(debug)
      printf("TD_musr_convert: dual spectra mode is enabled with suffixes: %s and %s \n",smp,ref);
    
// check validity of suffix strings
    len = strlen (smp);
    if(debug) printf("length of smp is %d\n",len);
    if (len <= 0 ) strcpy(smp,"_s");
    if (len > MAX_SUFF ) smp[MAX_SUFF+1]='\0';
    len = strlen (ref);
    if(debug) printf("length of ref is %d\n",len);
    if (len <= 0 ) strcpy(ref,"_r");
    if (len > MAX_SUFF ) ref[MAX_SUFF+1]='\0';
    
    if(strncmp(smp,ref,MAX_SUFF)==0)
    {
      strcpy(smp,"_s");
      strcpy(ref,"_r");
    }
    if (  (strcmp (smp, tdmusr.mode.dual_spectra_mode.suffix[0]) != 0) ||
          (strcmp (ref, tdmusr.mode.dual_spectra_mode.suffix[1]) != 0) )
      cm_msg(MINFO,"TD_musr_convert",
             "invalid dual-spectra suffix values(\"%s\" and \"%s\") have been corrected to \"%s\" and \"%s\"",
            tdmusr.mode.dual_spectra_mode.suffix[0], tdmusr.mode.dual_spectra_mode.suffix[1], smp,ref);             
    maxl = HIS_SIZE - 1 - (strlen(smp) > strlen(ref) ? strlen(smp) : strlen(ref));
    for (i=0; i<ncnt; i++)
    {
      j=i+ncnt;
      /* add the "virtual" tdc inputs to the array ("virtual" input = 8 + "real" input)  */
      selTdcInputs[j] = selTdcInputs[i] + 8 ;
      HM_index[j] = HM_index[i] + ncnt;  // add "virtual" input
      HM_sorted[j]  = HM_sorted[i] + 8;  // add "virtual" input
      
      sprintf(string,"%s",hist_titles[i]);
      len=strlen(string);
      if(len > maxl) string[maxl+1]='\0'; // terminate string
      sprintf(hist_titles[i],"%s%s", string, smp);
      sprintf(hist_titles[j],"%s%s", string, ref);

      if(debug)
      {
        printf ("counter title: %s; sample histogram title: %s; index=%d\n",string,hist_titles[i],i);
        printf ("counter title: %s; ref    histogram title: %s; index=%d\n",string,hist_titles[j],j);
      }
    }
  }
  return (DB_SUCCESS);
}



/* ----------------------------------------------------------------------------------------*/

INT TD_musr_write_scaler()
     
/* ----------------------------------------------------------------------------------------*/
{
  
  /* calculate and write scaler information into /Equipment/scaler/settings area in odb
     
  
     note: the key for musr area  /Equipment/musr_td_acq/Settings is hTS
     hTS is a global 

     The (global) key for the scaler settings area /Equipment/scaler/settings is hScal
     
      set_record is used to write the scaler settings area.

      Checks that the scaler titles are unique, truncates to 32 characters.
      Checks there are no duplicate scaler channels, 0-15 are valid, ascending
      order
      ( ch 10-15 used for IMUSR - issues a warning if these are specified  )

  */
  char str[128];
  INT i,j,k;
  INT status,size;
  INT previous_input;
  /* clear the scaler values in odb */
  HNDLE hTmp=0;
  KEY   hKScaler;
  double clear_scaler[MAX_NSCALS];

  // maximum scaler title length :32
  
  /* assign the values */
  printf("\n");
  fscal.bit_pattern=0;
  if(debug)printf("TD_musr_write_scaler: starting\n");
  
  fscal.enabled        = tdmusr.rig.scalers.enabled;

  if (tdmusr.rig.scalers.enabled)
    {
      fscal.num_inputs     = tdmusr.rig.scalers.num_inputs;
      if (  fscal.num_inputs >  MAX_NSCALS)
	{
	  cm_msg(MINFO,"TD_musr_write_scaler","invalid no. of scalers (%d) is > maximum (%d)",
		 fscal.num_inputs,MAX_NSCALS);
	  return (DB_INVALID_PARAM);
	}
    
      for(j=0; j < fscal.num_inputs; j++)  // update titles
	{
	  if(strlen ( tdmusr.rig.scalers.titles[j])==0)
	    {
	      cm_msg(MERROR,"TD_musr_write_scaler","Scaler title for scaler %d is blank",j);
	      return(DB_INVALID_PARAM);
	    }
	  strncpy(fscal.titles[j], tdmusr.rig.scalers.titles[j],32);
	  fscal.titles[j][31]='\0';
	  
	  /* check scaler inputs values */
	  if (tdmusr.rig.scalers.inputs[j] < 0 || tdmusr.rig.scalers.inputs[j] > 15)
	    {
	      cm_msg(MERROR,"TD_musr_write_scaler","Invalid scaler input[%d] = %d; (0-15 are valid) ",
		     j, tdmusr.rig.scalers.inputs[j]);
	      return(DB_INVALID_PARAM);
	    }
	  if (tdmusr.rig.scalers.inputs[j] > 9)
	      cm_msg(MINFO,"TD_musr_write_scaler","Warning: selected Scaler input %d for TD-Musr; this input is reserved for I_musr",
		     tdmusr.rig.scalers.inputs[j]);
	  

	  // printf("input[%d]=%d, previous bitpat=0x%x\n",j,tdmusr.rig.scalers.inputs[j],fscal.bit_pattern);
	  
	  /* check for duplicate  */
	  k=( fscal.bit_pattern & (1<<( tdmusr.rig.scalers.inputs[j]))  );
	  if(debug)
	    printf("bitpat=0x%x, about to set value in bitpat: 0x%x,  k=0x%d\n", 
		   fscal.bit_pattern, (1<< (tdmusr.rig.scalers.inputs[j]) ),k);
	  if(k > 0)
	    {  
	      //if(debug)printf("k is true; duplicate detected \n");
	      cm_msg(MERROR,"TD_musr_write_scaler","Duplicate scaler input[%d] = %d; bitpat=0x%x ",
		     j, tdmusr.rig.scalers.inputs[j],fscal.bit_pattern);
	      return(DB_INVALID_PARAM);
	    }
	  

	  fscal.bit_pattern = fscal.bit_pattern | ( 1 <<  (tdmusr.rig.scalers.inputs[j]) );
	  fscal.inputs[j] = tdmusr.rig.scalers.inputs[j]; // copy input # to scaler area
	} // end of for loop on enabled channels

      for(j = fscal.num_inputs ; j < MAX_NSCALS; j++) 
	{
	  strcpy(fscal.titles[j], ""); // any unused array elements will be set blank
	  fscal.inputs[j] = -1 ; // -1 is not a valid input for the scaler
	}

      for(j=0; j < fscal.num_inputs; j++)  // check the titles are unique
	{ 
	  for(k=j+1; k < fscal.num_inputs; k++) 
	    {
	      //if(debug)printf("j=%d, comparing scaler[%d]=%s to scaler[%d]=%s\n",j,j,fscal.titles[j],k,fscal.titles[k]);
	      if (strcmp(fscal.titles[j],fscal.titles[k]) ==0)
		{
		  cm_msg(MERROR,"TD_musr_write_scaler","duplicate scaler name \"%s\" in scaler array",fscal.titles[k]);
		  return(DB_INVALID_PARAM);
		}
	    }
	}
    }
  else
    {   //       scaler is disabled
      fscal.bit_pattern = 0;
      fscal.num_inputs = 0;
      for(j = 0 ; j < MAX_NSCALS; j++)
	{
	  strcpy(fscal.titles[j], ""); // any unused array elements will be set blank
	  fscal.inputs[j] = -1 ; // -1 is not a valid input for the scaler
	}
    }  
  printf("TD_musr_write_scaler: updating /equipment/scaler/settings with the values:\n");
  printf  ("enabled           = %d\n",fscal.enabled);
  printf  ("number of inputs  = %d\n",fscal.num_inputs);
  printf  ("bitpattern  = 0x%x\n",fscal.bit_pattern);
  if(fscal.enabled)
    {
      printf  ("Scaler:\n");
      printf(  "Index Input Title\n");    
      for (j=0; j < fscal.num_inputs ; j++)
	printf(" %2d    %2d   %-s\n",j, fscal.inputs[j], fscal.titles[j]);
    }
  else
    printf("Scaler is DISABLED \n");
  /* Update scalers/settings in ODB */
  size = sizeof(fscal);
  status = db_set_record(hDB, hScal, &fscal, size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"TD_musr_write_scal","Failed to update scaler record (size=%d) (%d)",size,status);  
    write_message1(status,"TD_musr_write_scal");
  }

  /* Check for single sweep mode with run going .. if so
     DO NOT clear old scaler values
  */  
  if(single_loop)
  {
    if (no_update)
    {
      printf("\n *** TD_musr_write_scal: Single sweep mode - run is in progress\n");
      printf(" ***       scaler counts will not be cleared\n\n");
      return(status);
    }
    else
    {
      printf("TD_musr_write_scal: Single sweep mode - run is NOT in progress\n");
      printf("            so scaler counts will be cleared \n");
    }
  }

  /*
    Clear the old Scaler values for MUI
  */

  for (j=0; j<MAX_NSCALS; j++)
    clear_scaler[j]=0.0;
  
 /* find the key for scaler/Variables/SCLR area */
  sprintf(str,"/Equipment/Scaler/Variables/SCLR"); 
  status = db_find_key(hDB, 0, str, &hTmp);
  if (status != DB_SUCCESS)
    {
      printf("TD_musr_write_scaler: could not find key for %s (%d)\n",str,status);
      printf("              could not clear previous  values\n");
      return(status);
    }
  
  status = db_get_key(hDB, hTmp, &hKScaler);
  if (status != DB_SUCCESS)
  {
    printf("TD_musr_write_scaler: could not get key for %s (%d)\n",str,status);
    printf("              could not clear previous  values\n");
    return(status);
  }

  j =  hKScaler.num_values; /* find out total no. of scalers */
  printf("TD_musr_write_scaler: number of defined scalers in %s is %d\n",str,j);
  /* clear the array  */ 
  status = db_set_data(hDB, hTmp, &clear_scaler,  j * sizeof(double), 
		       j, TID_DOUBLE);
  if(status != DB_SUCCESS)
  {
    printf("TD_musr_write_scaler: failure with set_data to  %s (%d)\n",str,status);
    printf("              could not clear previous  values\n");
    return(status);   
  }

  /*  the rates should clear themselves as equipment runs all the time   */
    
    return(status);
}

/* ----------------------------------------------------------------------------------------*/

INT TD_musr_write_mdarc()

/* ----------------------------------------------------------------------------------------*/
{
  
  /* write calculated data  into mdarc area in odb
     
 *******************************************************************************
 ***                                                                        ****
 ***     NOTE : call this subroutine AFTER musr_write_odb                   ****
 ***         because it needs to write values calculated by musr_write_odb  ****
 ***                                                                        ****
 *******************************************************************************
     
     note: the key for musr area  /Equipment/musr_td_acq/Settings is hMusrSet
     hMusrSet is a global

     The key for the mdarc area   /Equipment/musr_td_acq/mdarc is hMdarc
     
     as we are accessing several times 
     /Equipment/musr_td_acq/Settings/output/selected histograms
     we will use the key hOut

     We will use set_record to write the mdarc area
      (in single sweep mode, only if run is NOT in progress)
  */
  HNDLE  hTmp;
  char str[128];
  INT i,j;
  INT status,size;
  INT time_save;
/* assign the values */

  printf("\nTD_musr_write_mdarc: starting\n");
     
   /* mdarc histogram area */
  fmdarc.histograms.number_defined  = tdmusr.output.selected_histograms.num_histograms;
  fmdarc.histograms.resolution_code = tdmusr.mode.histograms.tdc_resolution_code;
  fmdarc.histograms.num_bins        = tdmusr.output.selected_histograms.num_bins;
  
  for (j=0; j <tdmusr.output.selected_histograms.num_histograms  ; j ++)
  {
    /* check last_good_bin < num bins  for mdarc (musr_update.pl) */
    if(tdmusr.mode.histograms.last_good_bin [j] > tdmusr.mode.histograms.num_bins)
    {
      if(debug) printf("TD_musr_write_mdarc: last_good_bin [%d] is out of range; setting it to %d\n",
		       j, tdmusr.mode.histograms.num_bins );
      tdmusr.mode.histograms.last_good_bin [j] =  tdmusr.mode.histograms.num_bins;
    }
    
    
    fmdarc.histograms.bin_zero             [j] = tdmusr.mode.histograms.bin_zero [j] ;
    fmdarc.histograms.first_good_bin       [j] = tdmusr.mode.histograms.first_good_bin  [j] ;
    fmdarc.histograms.first_background_bin [j] = tdmusr.mode.histograms.first_background_bin  [j] ;
    fmdarc.histograms.last_background_bin  [j] = tdmusr.mode.histograms.last_background_bin  [j] ;
    fmdarc.histograms.last_good_bin	   [j] = tdmusr.mode.histograms.last_good_bin [j] ;
  }
  
  //printf("\nnum histos    : %d\n",tdmusr.output.selected_histograms.num_histograms  );

  /* avoid confusion by setting unused values to -1 

    also clear unused values of the mode area  so musr_update.pl (called by bnmr_darc) doesn't
     keep updating mdarc area for unused values 
  */
  for (j=tdmusr.output.selected_histograms.num_histograms; j < MAX_HIS; j++)
  {                  // any unused array elements will be set to -1
    //printf("Setting element %d of bin limit histos to -1\n",j);
    fmdarc.histograms.bin_zero             [j] = tdmusr.mode.histograms.bin_zero             [j] = -1 ;
    fmdarc.histograms.first_good_bin       [j] = tdmusr.mode.histograms.first_good_bin       [j] = -1 ;
    fmdarc.histograms.first_background_bin [j] = tdmusr.mode.histograms.first_background_bin [j] = -1 ;
    fmdarc.histograms.last_background_bin  [j] = tdmusr.mode.histograms.last_background_bin  [j] = -1 ;
    fmdarc.histograms.last_good_bin	   [j] = tdmusr.mode.histograms.last_good_bin        [j] = -1 ;
  }
  //     Find the key for musr/mode/histograms area
  sprintf(str,"/Equipment/%s/Settings/mode/histograms",td_eqp_name); 
  status = db_find_key(hDB, 0, str, &hTmp); 
  if (status == DB_SUCCESS)
  {
    /* rewrite the whole arrays for the bin parameters to mode area */ 
    status = db_set_value(hDB,hTmp, "bin zero",&tdmusr.mode.histograms.bin_zero,  MAX_HIS * sizeof(INT), 
                          MAX_HIS, TID_INT);
    if(status != DB_SUCCESS)
      printf("TD_musr_write_mdarc: could not update \"%s/bin zero\" (%d)\n",str,status);

    
    status = db_set_value(hDB,hTmp, "first good bin",&tdmusr.mode.histograms.first_good_bin,  MAX_HIS * sizeof(INT), 
                          MAX_HIS, TID_INT);
    if(status != DB_SUCCESS)
      printf("TD_musr_write_mdarc: could not update \"%s/first good bin\" (%d)\n",str,status);

    status = db_set_value(hDB,hTmp, "last good bin",&tdmusr.mode.histograms.last_good_bin,  MAX_HIS * sizeof(INT), 
                          MAX_HIS, TID_INT);
    if(status != DB_SUCCESS)
      printf("TD_musr_write_mdarc: could not update \"%s/last good bin\" (%d)\n",str,status);

    status = db_set_value(hDB,hTmp, "first background bin",
                          &tdmusr.mode.histograms.first_background_bin,
                          MAX_HIS * sizeof(INT), MAX_HIS, TID_INT);
    if(status != DB_SUCCESS)
      printf("TD_musr_write_mdarc: could not update \"%s/first background bin\" (%d)\n",str,status);

    status = db_set_value(hDB,hTmp, "last background bin",
                          &tdmusr.mode.histograms.last_background_bin,
                          MAX_HIS * sizeof(INT), MAX_HIS, TID_INT);
    if(status != DB_SUCCESS)
      printf("TD_musr_write_mdarc: could not update \"%s/last background bin\" (%d)\n",str,status);
    
  }
  
  for(j=0; j < MAX_HIS; j++)  // any unused array elements are already initialized to blank
    sprintf(fmdarc.histograms.titles[j],"%s", tdmusr.output.selected_histograms.histogram_titles[j]);

  /* print the values */
  if(debug)
  {
    printf("Values written to mdarc odb structure: \n");
    printf("Camp hostname: %s\n",fmdarc.camp.camp_hostname);
    printf("saved data directory: %s\n", fmdarc.saved_data_directory);
    printf("archived  data directory : %s\n",fmdarc.archived_data_directory  );
    printf("# versions before purge : %d\n",  fmdarc.num_versions_before_purge );
    printf("saved interval (s) : %d\n",  fmdarc.save_interval_sec_ );
    
    printf("\nnum histos    : %d\n", fmdarc.histograms.number_defined  );
    printf("num bins        : %d\n",  fmdarc.histograms.num_bins  );
    printf("resolution code : %d\n",  fmdarc.histograms.resolution_code );
    
    printf("\n bin_0 1st_good last_good 1st_bkg last_bkg  title\n"); 
    for (j=0; j< MAX_HIS; j++)
    {
      printf("    %2d    %2d       %2d     %2d       %2d    %- 10s\n",
             fmdarc.histograms.bin_zero             [j],
             fmdarc.histograms.first_good_bin       [j],
             fmdarc.histograms.first_background_bin [j],
             fmdarc.histograms.last_background_bin  [j],
             fmdarc.histograms.last_good_bin	   [j],
             fmdarc.histograms.titles[j] );
    } 
    
  }

  /* copy across parameters to mdarc/histograms/musr area */
  strcpy(fmdarc.histograms.musr.beamline, tdmusr.beamline);
  strcpy(fmdarc.histograms.musr.current_rig, tdmusr.rig.current_rig);
  strcpy(fmdarc.histograms.musr.current_mode, tdmusr.mode.current_mode);

  printf("beamline      :  %s\n",fmdarc.histograms.musr.beamline);
  printf("current rig   :  %s\n",fmdarc.histograms.musr.current_rig);
  printf("current mode  :  %s\n",fmdarc.histograms.musr.current_mode);
  

  /* if single sweep mode, and run is in progess
     DO NOT update mdarc area as it toggles the run number due to the hotlinks
  */
  if(single_loop)
  {
    if (no_update )
    {
      printf("\n *** TD_musr_write_mdarc: Single sweep mode - run is in progress\n");
      printf(" ***   so Mdarc record NOT ACTUALLY WRITTEN (avoid triggering hotlinks)\n");
      return(status);
    }
    else
    {
      printf("TD_musr_write_mdarc: Single sweep mode - run is NOT in progress\n");
      printf("            so Mdarc record will be written \n");
    }
  }
  
  /* clear the histogram totals */
  for (j=0;  j < MAX_HIS; j++)
    fmdarc.histograms.output.histogram_totals[j]=0;    
  fmdarc.histograms.output.total_saved=0;
  /* clear the last saved filename */
  sprintf(fmdarc.last_saved_filename,"");
  printf("TD_musr_write_mdarc: histogram totals and last saved filename have been cleared\n");
  
  /* Update mdarc settings in ODB */  size = sizeof(fmdarc);
  status = db_set_record(hDB, hMdarc, &fmdarc, size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"TD_musr_write_mdarc","Failed to update mdarc record (size=%d) (%d)",size,status);  
    write_message1(status,"TD_musr_write_mdarc");
  }
  
  return(status);
}






// ------------------------------------------------------------------------
INT musr_write_v680()
{
  /* write calculated data  into v680 area in odb 
     
     (values checked in TD_musr_validate_params)
     
 ***************************************************************
 ***    
 ***     NOTE : call this subroutine AFTER TD_musr_write_odb  ****
 ***
 ***************************************************************
      

      note: the key for v680 area  /Equipment/musr_td_acq/v680/params is hV680
     hV680 is a global 

   */
  HNDLE  hTmp;
  char str[128];
  INT j;
  INT status,size;
  
  /* assign the values */
  
  printf("\nmusr_write_v680: starting\n");
  
  sprintf(str,"/Equipment/%s/v680",td_eqp_name); 
  
  if(TD_MUSR)
    {
      fv680.delta               = tdmusr.mode.gates.data_gate_code;
      fv680.delta1              = tdmusr.mode.gates.tdc_gate_code;
      fv680.delta2              = tdmusr.mode.gates.pileup_gate_code;
      
      fv680.number_of_bins      = tdmusr.output.selected_histograms.num_bins;
      fv680.bytes_per_bin       = tdmusr.mode.histograms.bytes_per_bin;
      fv680.shift               = tdmusr.mode.histograms.tdc_resolution_code - 16;
      fv680.counter_bit_pattern =
	tdmusr.output.selected_histograms.tdc_inputs_bit_pattern;
      fv680.ioreg_bit_pattern   =
	tdmusr.output.selected_histograms.ioreg_bit_pattern;
      
      
      /* frontend needs index_into_hm for sending histograms out in correct order
	 and may need TDC_inputs */
      for(j=0; j < MAX_HIS; j++)  // any unused array elements will be set blank
	{        // any unused array elements are already initialized
	  fv680.histograms.index_into_hm  [j] =  tdmusr.output.selected_histograms.hm_index[j];
	  fv680.histograms.tdc_inputs     [j] =  tdmusr.output.selected_histograms.selected_tdc_inputs[j];
	}
      
      if (debug)
	{
	  printf("Writing the following parameters to v680 area:\n");
	  printf("  number of bins :   %d\n", fv680.number_of_bins);
	  printf("  shift          :   %d\n", fv680.shift);
	  printf("  bytes/bin      :   %d\n", fv680.bytes_per_bin);
	  printf("  bit pattern    : 0x%x (%d)\n", fv680.counter_bit_pattern,fv680.counter_bit_pattern);
	  printf("  data gate      : 0x%x (%d)counts \n",fv680.delta,fv680.delta);
	  printf("  TDC gate       : 0x%x (%d)counts \n",fv680.delta1,fv680.delta1);
	  printf("  pileup gate       : 0x%x (%d)counts \n",fv680.delta2,fv680.delta2);
	  printf(" - also fills histograms/index_into_hm and TDC_inputs arrays\n");
	}
      /*
	Update v680 area in ODB 
	* * * don't use set_record because of the hotlinks in the frontend !  * * *
	*/
      

      /*  number of bins */
      size = sizeof( fv680.number_of_bins);
      status = db_set_value(hDB, hV680, "number of bins", &fv680.number_of_bins, size,1, TID_DWORD);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_write_v680","cannot update key %s/number of bins (%d)",str,status);
	  write_message1(status,"musr_write_v680");
	  return status;
	}
      
      /* shift */
      size = sizeof( fv680.shift);
      status = db_set_value(hDB, hV680, "shift", &fv680.shift, size,1, TID_INT);
      if (status != DB_SUCCESS)
	{
      cm_msg(MERROR,"musr_write_v680","cannot update key %s/shift (%d)",str,status);
      write_message1(status,"musr_write_v680");
      return status;
	}
      
      
      /* counter bit pattern */
      size = sizeof( fv680.counter_bit_pattern);
      status = db_set_value(hDB, hV680, "counter bit pattern", &fv680.counter_bit_pattern, size,1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_write_v680","cannot update key %s/counter bit pattern (%d)",str,status);
	  write_message1(status,"musr_write_v680");
	  return status;
	}
      /* ioreg bit pattern */
      size = sizeof( fv680.ioreg_bit_pattern);
      status = db_set_value(hDB, hV680, "ioreg bit pattern", &fv680.ioreg_bit_pattern, size,1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_write_v680","cannot update key %s/ioreg bit pattern (%d)",str,status);
	  write_message1(status,"musr_write_v680");
	  return status;
	}
      
      /* data gate (delta) */
      size = sizeof( fv680.delta);
      status = db_set_value(hDB, hV680, "delta", &fv680.delta, size,1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_write_v680","cannot update key %s/delta (%d)",str,status);
	  write_message1(status,"musr_write_v680");
	  return status;
	}
      /* TDC gate code (delta1) */
      size = sizeof( fv680.delta1);
      status = db_set_value(hDB, hV680, "delta1", &fv680.delta1, size,1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_write_v680","cannot update key %s/delta1 (%d)",str,status);
	  write_message1(status,"musr_write_v680");
	  return status;
	}
      
      /* Pileup gate code (delta2) */
      size = sizeof( fv680.delta2);
      status = db_set_value(hDB, hV680, "delta2", &fv680.delta2, size,1, TID_INT);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_write_v680","cannot update key %s/delta2 (%d)",str,status);
	  write_message1(status,"musr_write_v680");
	  return status;
	}
      


      //     Find a key for v680/histograms area
      sprintf(str,"/Equipment/%s/v680/histograms",td_eqp_name); 
      status = db_find_key(hDB, 0, str, &hTmp);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_write_v680","cannot find key %s (%d)",td_eqp_name,str,status);
	  write_message1(status,"musr_write_v680");
	  return status;
	}
      
      /* write the TDC index array to v680/histograms */ 
      status = db_set_value(hDB,hTmp, "index into HM",&fv680.histograms.index_into_hm ,  MAX_HIS * sizeof(INT), 
			    MAX_HIS, TID_INT);
      if(status != DB_SUCCESS)
	{
	  printf("musr_write_v680: could not update \"%s/index into HM\" (%d)\n",str,status);
	  return status;
	}
      /* write the TDC input array to v680/histograms */ 
      status = db_set_value(hDB,hTmp, "TDC inputs",&fv680.histograms.tdc_inputs ,  MAX_HIS * sizeof(INT), 
			    MAX_HIS, TID_INT);
    if(status != DB_SUCCESS)
      {
	printf("musr_write_v680: could not update \"%s/TDC inputs\" (%d)\n",str,status);
	return status;
      } 
    
    }  // end of TD_MUSR
  
  else     // I_MUSR - nothing else is relevent
    fv680.bytes_per_bin       = 4;
  
  
  /* bytes per bin */
  size = sizeof( fv680.bytes_per_bin);
  status = db_set_value(hDB, hV680, "bytes per bin", &fv680.bytes_per_bin, size,1, TID_INT);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"musr_write_v680","cannot update key %s/bytes per bin (%d)",td_eqp_name,str,status);
      write_message1(status,"musr_write_v680");
      return status;
    }
  
  return (status);
}


// ------------------------------------------------------------------------
INT TD_musr_write_odb()
//-------------------------------------------------------------------------
{
  
  /* write calculated data  into odb area
         "/Equipment/musr_td_acq/Settings/output/selected histograms"
     

         note: the key for musr area  /Equipment/musr_td_acq/Settings is hTS
     hTS is a global (presently in mdarc.h
     
     as we are accessing several times 
     /Equipment/musr_td_acq/Settings/output/selected histograms
     we will get a (global) key to this area (hOut)
     
  */
  HNDLE  hTmp;
  char str[128];
  INT i;
  INT status,size;
  
  
  //if (debug)
  {
    printf("TD_musr_write_odb: Writing ncnt  = %d to the odb \n", ncnt);
    printf("                        nhis  = %d  \n", nhis);
    printf("                        nbins = %u  \n", 
           tdmusr.output.selected_histograms.num_bins ); // filled by TD_musr_validate_params
    printf("              TDC bitpattern = 0x%x (%d)  \n", bitpat_tdc,bitpat_tdc);
    printf("           IO reg bitpattern = 0x%x  (%d) \n", bitpat_ioreg,bitpat_ioreg);
  }
   
   tdmusr.output.selected_histograms.num_counters = ncnt;  // no. of counters
   tdmusr.output.selected_histograms.num_histograms = nhis;// no. of histograms
   tdmusr.output.selected_histograms.tdc_inputs_bit_pattern = bitpat_tdc;  // TDC bit pattern
   //  IO reg bit pattern (if ioreg is disabled it will write 0)
   tdmusr.output.selected_histograms.ioreg_bit_pattern = bitpat_ioreg;
   // write IO reg mask (if ioreg is disabled it will write 0x1FF)
   
   // arrays   write all MAX_HIS values so unused ones are set to -1
   for (i=0; i<MAX_HIS; i++)
   {
     sprintf( tdmusr.output.selected_histograms.histogram_titles[i],"%s",hist_titles[i]); // histogram titles
     
     tdmusr.output.selected_histograms.selected_tdc_inputs[i] = selTdcInputs[i];// selected tdc input array
     tdmusr.output.selected_histograms.sorted_tdc_inputs  [i] = HM_sorted[i];  // sorted tdc input array
     tdmusr.output.selected_histograms.hm_index           [i] = HM_index [i]; //  HM_index
     /*                               (array of index values into the HM data stored in the  frontend) */
   }
   
   
   /* print the values */
   if(debug)
   {
     printf("\nValue in odb structure tdmusr.output.selected_histograms:\n");
     printf("hm_sorted  hm_index  tdc_input  title\n"); 
     for (i=0; i< nhis; i++)
     {
       printf("  %2d             %2d           %2d         %- 10s\n",
              tdmusr.output.selected_histograms.sorted_tdc_inputs  [i],
              tdmusr.output.selected_histograms.hm_index [i],
              tdmusr.output.selected_histograms.selected_tdc_inputs[i],
              tdmusr.output.selected_histograms.histogram_titles[i]);
     }
     
   }        
   
   
   /*
     Update selected histogram area in ODB
   */
   
   // get the key to this area
   sprintf(str,"/Equipment/%s/Settings/output/selected histograms",td_eqp_name);
   status=db_find_key(hDB, 0, str, &hOut);
   if(status != DB_SUCCESS) 
   {
     cm_msg(MERROR,"TD_musr_write_odb","error from find key for %s, status(%d)",str,status);
     write_message1(status,"TD_musr_write_odb");
     return (status);
   }
   
   size = sizeof(tdmusr.output.selected_histograms);
   status = db_set_record(hDB, hOut, &tdmusr.output.selected_histograms, size, 0);
   if (status != DB_SUCCESS)
   {                          
     cm_msg(MINFO,"TD_musr_write_odb","Failed to update record %s (size=%d) (%d)",str,size,status);  
     write_message1(status,"TD_musr_write_odb");
     return (status);
   }
   return(status);
}
  
/*------------------------------------------------------------------*/
INT tr_prestart(INT run_number, char *error)
{
  time_t timbuf;
  char timbuf_ascii[30];
  DWORD elapsed; 
  INT status,size;
  char str[132];
  char rig[20];
  INT run_state;
  char client_str[128];
  BOOL flag;

  strcpy(error,"tr_prestart starting");

  I_MUSR=TD_MUSR=FALSE;
  
    /* check for  single sweep mode, and run is in progess.
         -> set no_update
    */
   no_update = FALSE; // default is false i.e. update if not in single sweep mode
   if(single_loop)
     {    
       /* get the run state to see if run is going */
       size = sizeof(run_state);
       status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
       if (status != DB_SUCCESS)
	 {
	   cm_msg(MERROR,"tr_prestart","key not found /Runinfo/State (%d)",status);
	   write_message1(status,"tr_prestart");
	   cm_msg(MERROR,"tr_prestart",
		  "Mdarc record NOT WRITTEN (in single sweep mode)");
	   return(status);
	 }
       if (run_state != STATE_STOPPED )
	 {
	   if(debug)printf("tr_prestart: Single sweep mode -  run IS in progress\n");
	   no_update = TRUE;
	 }
       else
	 {
	   if(debug)printf("tr_prestart: Single sweep mode - run is NOT in progress\n");
	   no_update = FALSE;
	 }
       printf("tr_prestart: single loop mode, not setting client flag false\n");
     } // end of test on single sweep mode
   else
     {    // except on single loop mode....
       /* first set the client flag to FALSE in case of failure */
       printf("tr_prestart: setting client flag for musr_config to false\n"); 
       sprintf(client_str,"/equipment/%s/client flags/musr_config",td_eqp_name );
       flag = FALSE;
       status = db_set_value(hDB, 0, client_str, &flag, sizeof(flag), 1, TID_BOOL);
       if (status != DB_SUCCESS)
	 {
	   cm_msg(MERROR, "tr_prestart", "cannot clear status flag at path \"%s\" (%d) ",client_str,status);
	   return status;
	 } 
     }

   /* get settings all records */
   status = musr_get_rec(rig); // use rig as param for now
  if (status != DB_SUCCESS)
  {
    printf("tr_prestart: failure after musr_get_rec. CHECK ODB FOR MESSAGES \n");
    return(status);
  }

  status = get_musr_type();
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR, "tr_prestart", "cannot determine MUSR run type (I or TD)");
    return DB_NO_ACCESS;
  }

  
  if(TD_MUSR)
  {
    cm_msg(MINFO, "tr_prestart", "MUSR run type is Time Differential (TD)");
    status=TD_musr_validate_params();
  }
  else if (I_MUSR)
  {
    cm_msg(MINFO, "tr_prestart", "MUSR run type is Integral (I)");
    status=I_musr_validate_params();
  }
  
  if (status != DB_SUCCESS)
  {
    printf("tr_prestart: failure after musr_validate_params - MUSR input parameters may be invalid\n");
    printf("          CHECK ODB FOR MESSAGES \n");
    return(status);
  }

  /* get the present time */
  time(&timbuf);
  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) );
  timbuf_ascii[24] = '\0';
  printf ("Present time:  %s or (binary) %d \n",timbuf_ascii,timbuf);   

  /* write this time to odb in v680 area */
  sprintf(str,"/Equipment/%s/v680/histograms",td_eqp_name);
  fv680.histograms.binary_time = (DWORD) timbuf;
  size = sizeof( fv680.histograms.binary_time);
  status = db_set_value(hDB, hV680, "histograms/binary time",
                        &fv680.histograms.binary_time, size,1, TID_DWORD);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_prestart","cannot update key \"%s/binary time\" (%d)",td_eqp_name,str,status);
    write_message1(status,"tr_prestart");
    return status;
  }

  sprintf( fv680.histograms.validated_at_time,"%s",timbuf_ascii) ;
  printf("validated at time: %s\n", fv680.histograms.validated_at_time);
  size = sizeof( fv680.histograms.validated_at_time );
  status = db_set_value(hDB, hV680, "histograms/validated at time",
                        &fv680.histograms.validated_at_time , size,1, TID_STRING);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"tr_prestart","cannot update key %s/validated at time (%d)",
           str,status);
    write_message1(status,"tr_prestart");
    return status;
  }
  

  if(!single_loop)
    {
      printf("tr_start: setting client flag for musr_config true (success)\n"); 
      /* Set the client flag to TRUE (success) */
      flag = TRUE;
      size = sizeof(flag);
      status = db_set_value(hDB, 0, client_str, &flag, size, 1, TID_BOOL);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "prestart", "cannot set client status flag at path \"%s\" (%d) ",client_str,status);
	  return status;
	}
    }
  else
    printf("tr_prestart: single loop mode, NOT setting client flag for musr_config true (success)\n"); 


  return status;
}



/*------------------------------------------------------------------*/

INT musr_get_rec(char *rig)
{
  /* retrieve the ODB structures again at each BOR  (called from tr_prestart)
   */
  INT size, status,j;
  char str[128];
  
  
  
  /* get the record for mdarc area (we already found the key in main) */
  size = sizeof(fmdarc);
  status = db_get_record (hDB, hMdarc, &fmdarc, &size, 0);/* get the whole record for mdarc */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_get_rec","Failed to retrieve the record for mdarc  (%d) size=%d ",status,size);
    write_message1(status,"musr_get_rec");
    return(status); /* error */
  }
  else
    if(debug)printf("Got the record for mdarc\n");
  
  /* get the record for musr settings area (we already found the key in main) */
  size = sizeof(tdmusr);
  status = db_get_record (hDB, hTS , &tdmusr, &size, 0);/* get the whole record for musr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_get_rec","Failed to retrieve musr Settings record  (%d) size=%d ",status,size);
    write_message1(status,"musr_get_rec");
    return(status); /* error */
  }
  else
    if(debug)printf("Got the record for musr/settings \n");


   /* get the record for v680 area (we already found the key in main) */
  size = sizeof(fv680);
  status = db_get_record (hDB, hV680, &fv680, &size, 0);/* get the whole record for musr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_get_rec","Failed to retrieve v680 record  (%d)",status);
    write_message1(status,"musr_get_rec");
    return(status);
  }

  
    /* get the record for imusr area
       (we already found the key in musr_create_rec) */ 

  size = sizeof(imusr);
  status = db_get_record (hDB, hIS, &imusr, &size, 0);/* get the whole record for imusr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_get_rec","Failed to retrieve imusr record  (%d)",status);
    write_message1(status,"musr_get_rec");
    return(status);
  }

  /* get record for scaler area */
  
  size = sizeof(fscal);
  status = db_get_record (hDB, hScal, &fscal, &size, 0);/* get the whole record*/
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_get_rec","Failed to retrieve scaler record  (%d)",status);
    write_message1(status,"musr_get_rec");
    return(status);
  }
 


  return status;
}

/*------------------------------------------------------------------*/

INT musr_create_rec(void)
{
  
  MUSR_TD_ACQ_MDARC_STR(musr_td_acq_mdarc_str); /* for mdarc (from experim.h) */
  MUSR_TD_ACQ_SETTINGS_STR(musr_td_acq_settings_str);
  MUSR_TD_ACQ_V680_STR(musr_td_acq_v680_str);
  SCALER_SETTINGS_STR(scaler_settings_str);
  MUSR_I_ACQ_SETTINGS_STR (musr_i_acq_settings_str);
  
  char str[128];
  INT size,struct_size;  
  INT status, stat1;
  INT run_state;

  if(debug)
    printf("musr_create_rec is starting...\n");

  /* get the run state to see if run is going */
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/State", &run_state, &size, TID_INT, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_create_rec","key not found /Runinfo/State (%d)",status);
    write_message1(status,"musr_create_rec");
    return(status);
  }



  
//  M U S R   S E T T I N G S
  
  /* find the key for TD - Musr area */
  sprintf(str,"/Equipment/%s/Settings",td_eqp_name); 
  status = db_find_key(hDB, 0, str, &hTS);
  if (status != DB_SUCCESS)
    {
      hTS=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for td-musr settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"musr_create_rec","Failure creating TD Musr settings area (%d)",status);
          write_message1(status,"musr_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hTS has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hTS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "musr_create_rec", "error during get_record_size (%d) for TD Musr/settings record",status);
          write_message1(status,"musr_create_rec");
	  return status;
	}
      struct_size =   sizeof(MUSR_TD_ACQ_SETTINGS);
      
      if(debug)
	printf("musr_create_rec:Info - size of TD_musr Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("musr_create_rec:Info - creating TD musr record due to size mismatch\n");
        cm_msg(MINFO,"musr_create_rec","creating record (TD_musr Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"musr_create_rec","Could not create TD musr settings record (%d)",status);
          write_message1(status,"musr_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"musr_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hTS  */
  
  status = db_find_key(hDB, 0, str, &hTS);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "musr_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"musr_create_rec");
    return (status);
  }

  size = sizeof(tdmusr);
  status = db_get_record (hDB, hTS, &tdmusr, &size, 0);/* get the whole record for musr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"musr_create_rec");
    return(status);
  }

  // print something out from TD musr record area
  // printf("current rig is %s\n",tdmusr.rig.current_rig);



//  I - MUSR
    /* find the key for I-musr area */
  sprintf(str,"/Equipment/%s/Settings",j_eqp_name); 
  status = db_find_key(hDB, 0, str, &hIS);
  if (status != DB_SUCCESS)
    {
      hIS=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for I-musr settings area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(musr_i_acq_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"musr_create_rec","Failure creating I_musr settings area (%d)",status);
          write_message1(status,"musr_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hIS has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hIS, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "musr_create_rec", "error during get_record_size (%d) for imusr/settings record",status);
          write_message1(status,"musr_create_rec");
	  return status;
	}
      struct_size =   sizeof(MUSR_I_ACQ_SETTINGS);
      
      if(debug)
	printf("musr_create_rec:Info - size of IMUSR Settings saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        printf("musr_create_rec:Info - creating IMUSR record due to size mismatch\n");
        cm_msg(MINFO,"musr_create_rec","creating record (I_musr Settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
       /* create record */
        status = db_create_record(hDB, 0, str , strcomb(musr_i_acq_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"musr_create_rec","Could not create imusr settings record (%d)",status);
          write_message1(status,"musr_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"musr_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hIS  */
  
  status = db_find_key(hDB, 0, str, &hIS);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "musr_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"musr_create_rec");
    return (status);
  }

  size = sizeof(imusr);
  status = db_get_record (hDB, hIS, &imusr, &size, 0);/* get the whole record for imusr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"musr_create_rec");
    return(status);
  }

  // print something out from imusr record area
   if(debug)
     printf("musr_create_rec: current sweep device  is %s\n",imusr.input.sweep_device);


  
//        V 6 8 0        A R E A


  
  /* find the key for v680 area */
  sprintf(str,"/Equipment/%s/v680",td_eqp_name); 
  status = db_find_key(hDB, 0, str, &hV680);
  if (status != DB_SUCCESS)
    {
      hV680=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for V680  area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_v680_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"musr_create_rec","Failure creating musr v680 area (%d)",status);
          write_message1(status,"musr_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hV680 has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hV680, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "musr_create_rec", "error during get_record_size (%d) for V680 record",status);
          write_message1(status,"musr_create_rec");
	  return status;
	}
      struct_size =   sizeof(MUSR_TD_ACQ_V680);
      if(debug)
	printf("musr_create_rec:Info - size of V680 saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"musr_create_rec","creating record (musr V680); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_v680_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"musr_create_rec","Could not create musr v680 record (%d)",status);
          write_message1(status,"musr_create_rec");

          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"musr_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hV680  */
  
  status = db_find_key(hDB, 0, str, &hV680);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "musr_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"musr_create_rec");
    return (status);
  }

  size = sizeof(fv680);
  status = db_get_record (hDB, hV680, &fv680, &size, 0);/* get the record v680 */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"musr_create_rec");
    return(status);
  }
  
  
//        M D A R C  


/* get the key hMdarc  */
  sprintf(str,"/Equipment/%s/mdarc",td_eqp_name); 
  status = db_find_key(hDB, 0, str, &hMdarc);
  if (status != DB_SUCCESS)
    {
      if(debug) printf("musr_create_rec: Failed to find the key %s ",str);
      
      /* Create record for mdarc area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_mdarc_str));
      if (status != DB_SUCCESS)
	{
	  if(debug)printf("musr_create_rec: Failure creating mdarc odb record. Checking if mdarc client is running...\n");
	  stat1 = cm_exist("mdarc",FALSE) ;
	  if(stat1 == CM_SUCCESS)	  /* mdarc is already running */
	    {
	      /* how come we can't find the mdarc area if mdarc is running? This shouldn't happen */
	      cm_msg(MINFO,"musr_create_rec","Data archiver mdarc is running; mdarc record could not be created");
	    }
	  else
	    cm_msg(MINFO,"musr_create_rec","Could not create record for %s  (%d); (mdarc not running)", str,status);
        }
      else
	if(debug) printf("Success from create record for %s\n",str);
      /* try again to get the key hMdarc  */
      status = db_find_key(hDB, 0, str, &hMdarc);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"musr_create_rec","Failed to get the key %s ",str);
          write_message1(status,"musr_create_rec");
	  return status;
	}
    }    
  else  /* key mdarc has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hMdarc, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "musr_create_rec", "error during get_record_size (%d) for mdarc",status);
          write_message1(status,"musr_create_rec");
	  return status;
	}
      if(debug)
      printf("Info: size of mdarc saved structure: %d, size of mdarc record: %d\n", sizeof(MUSR_TD_ACQ_MDARC) ,size);
      if (sizeof(MUSR_TD_ACQ_MDARC) != size)
      {
	cm_msg(MINFO,"musr_create_rec",
               "creating record (mdarc); mismatch between size of structure (%d) & record size (%d)",
               sizeof(MUSR_TD_ACQ_MDARC) ,size);
	/* create record */
	status = db_create_record(hDB, 0, str , strcomb(musr_td_acq_mdarc_str));
	if (status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"musr_create_rec","Could not create mdarc record (%d)\n",status);
            write_message1(status,"musr_create_rec");
	    return status;
	  }
	else
	  if (debug)printf("Success from create record for %s\n",str);
      }
    }

 //        S C A L E R


  
  /* find the key for scaler Settings area */
  sprintf(str,"/Equipment/Scaler/Settings"); 
  status = db_find_key(hDB, 0, str, &hScal);
  if (status != DB_SUCCESS)
    {
      hScal=0;
      if(debug) printf(": Failed to find the key %s\n ",str);
      
      /* Create record for Scaler Settings  area */     
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(scaler_settings_str));
      if (status != DB_SUCCESS)
      {
	  cm_msg(MERROR,"musr_create_rec","Failure creating record for %s (%d)",str,status);
          write_message1(status,"musr_create_rec");
      }
      else
	if(debug) printf("Success from create record for %s\n",str);
    }    
  else  /* key hScal has been found */
    {
      /* check that the record size is as expected */
      status = db_get_record_size(hDB, hScal, 0, &size);
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR, "musr_create_rec", "error during get_record_size (%d) for scaler record",status);
          write_message1(status,"musr_create_rec");
	  return status;
	}
      struct_size =   sizeof(SCALER_SETTINGS);
      if(debug)
      printf("musr_create_rec:Info - size of SCALER saved structure: %d, size of ODB record: %d\n",
	     struct_size ,size);
      if (struct_size  != size)    
      {
        cm_msg(MINFO,"musr_create_rec","creating record (scaler/settings); mismatch between size of structure (%d) & record size (%d)",
               struct_size ,size);
        /* create record */
        status = db_create_record(hDB, 0, str , strcomb(scaler_settings_str));
        if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"musr_create_rec","Could not create %s record (%d)\n",str,status);
          write_message1(status,"musr_create_rec");
          if (run_state == STATE_RUNNING )
            cm_msg(MINFO,"musr_create_rec","May be due to open records");
          return status;
        }
        else
          if (debug)printf("Success from create record for %s\n",str);
      }
    }  
  
  /* now try again to get the key hScal  */
  
  status = db_find_key(hDB, 0, str, &hScal);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR, "musr_create_rec", "key %s not found (%d)", str,status);
    write_message1(status,"musr_create_rec");
    return (status);
  }

  size = sizeof(fscal);
  status = db_get_record (hDB, hScal, &fscal, &size, 0);/* get the whole record for musr */
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"musr_create_rec","Failed to retrieve %s record  (%d)",str,status);
    write_message1(status,"musr_create_rec");
    return(status);
  }

  return (status);
}

/*------------------------------------------------------------------*/
INT TD_musr_diag(void)
{
  /* Name the diagnostic channels (hit counters) by 
     truncating or expanding variable length array /equipment/diag/Settings/Names
     and by rewriting labels if necessary
  */
  char str[128];
  HNDLE hTmp;
  KEY hKey;
  INT i,j,k,size,status;
  char name[32];


  sprintf(str,"/Equipment/diag/Settings/Names");
  /* find the key */
  status = db_find_key(hDB, 0, str, &hTmp);
  if (status != DB_SUCCESS)
  {
    printf("TD_musr_diag: could not find key for %s (%d)\n",str,status);
    cm_msg(MERROR,"TD_musr_diag","could not find key for %s (%d)",str,status);
    return(status); 
  }
  
  status = db_get_key(hDB, hTmp, &hKey);
  if (status != DB_SUCCESS)
  {
    printf("TD_musr_diag: could not get key for %s (%d)\n",str,status);
    cm_msg(MERROR,"TD_musr_diag","could not get key for %s (%d)",str,status);
    return (status);
  }
  j =  hKey.num_values; /* find out total no. values */
  if(debug)
    printf("TD_musr_diag: number of diagnostic channels in %s is currently %d (nhis=%d)\n",str,j,nhis);
  
  if(j == nhis+4)
  {
    if(debug)
      printf("TD_musr_diag: Number of defined histograms (%d) matches diagnostic (%d + 4) ; no action\n",j,nhis); 
    return (status);
  }
  
  
  /* resize the array */
  i = nhis + 4; // 4 hit counters + no. of defined histograms
  if(debug)printf("TD_musr_diag: Resizing histogram to %d\n",i);
  status = db_set_num_values(hDB, hTmp, i);
  if(status != DB_SUCCESS)
  {
    printf("TD_musr_diag: failed to resize diagnostic array to %d (%d)\n",j,status);
    cm_msg(MERROR,"TD_musr_diag","failed to resize diagnostic array to %d (%d)",j,status);
    return(status);
  }

  if(i > j)
  {
    if(debug)printf("TD_musr_diag: labelling the extra counters...\n");
    /* label the extra hit counters */
    for (k=j; k<i; k++)
    {
      sprintf(name,"Hit[%d]",(k-4));
      size=sizeof(name);
      if(debug)printf("TD_musr_diag: Relabelling array index %d to \"%s\"\n",k,name);
      status = db_set_data_index(hDB,hTmp,name,size,k,TID_STRING);
      if(status != DB_SUCCESS)
      {
	printf("TD_musr_diag: failed trying to set array element %d to %s (%d)\n",k,name,status);
	cm_msg(MERROR,"TD_musr_diag","failed trying to set array element %d to %s (%d)",
	       k,name,status);
	return(status);
      }
    }
  }
  return (status);  
}

 /*--  find whether running I or TD MUSR ----------------------------------------*/
INT get_musr_type(void)
{
  char musr_type[10];
  char str[128];
  int j,len;
  INT size,status;
  
  I_MUSR=TD_MUSR=FALSE; /* global flags */

  if(debug)printf("get_musr_type starting\n");
  
  sprintf(str,"/experiment/edit on start/musr type");
  size = sizeof(musr_type);
  status = db_get_value(hDB, 0, str,
                        &musr_type , &size, TID_STRING, FALSE);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"get_musr_type","cannot access key %s (%d)",
           str,status);
    write_message1(status,"running_IMUSR");
    return status;
  }

  len=strlen(musr_type);
  for(j=0; j<len; j++)
    musr_type[j] = toupper (musr_type[j]); /* convert to upper case */

  if(debug)printf("len:%d, string %s\n",len,musr_type);

  if (strchr(musr_type,'I'))
    I_MUSR=TRUE;
  if (strchr(musr_type,'T') )
    TD_MUSR=TRUE;
  
  if ( (I_MUSR == FALSE && TD_MUSR == FALSE) ||
       (I_MUSR == TRUE  && TD_MUSR == TRUE) )
  {
    cm_msg(MERROR,"get_musr_type","Bad value of key %s (%s). Must contain \"I\" or \"TD\" ",
           str,musr_type);

    return(DB_INVALID_PARAM);
  }
  return status;
}


/*  ----------------------------------------------------------------------------------------------------------------*/
INT I_musr_validate_params (void)
/*    --------------------------------------------------------------------*/
{
/*   I - MUSR

   To do:
     CHANGES to TD-MUSR:
     Add checks to TD-MUSR so it can't use IMUSR reserved Scaler and IOReg
     Channels

     
     For IMUSR   v680 area IS used for check_musr_config and validated time.

     - we don't need ioreg_bit_pattern since bit pattern is fixed for
     IMUSR and bits are turned on one by one

     IOREG bits and fixed scalers are in include files
     
  
     scalers : num auxiliary inputs and aux titles listed in /hardware/scaler
	If num aux scalers > 0 update /equipment/scaler/settings

	Check that scaler inputs correspond to fixed values in include file
	
     checks if toggle_type is soft or ref that num toggles > 0
               if num toggles =0 sets toggle_type to NONE

	fills    hardware/campPD depending on sweep device
	writes input/camp paths to log etc to /equipment/camp/settings - or
	maybe the info goes straight there

	validate the sweep start and stop against the maximum values supplied (or known) for
	the sweep device
	
	Update mdarc area for number of histograms (= number of scalers enabled)
	
*/
  INT i,j,k;
  INT status;
  INT max, shift, his_length;
    float sweep_max,sweep_min;
  
  // print something out
  if(debug)printf("I_musr_validate_params: starting ... current PD is %s\n",imusr.input.sweep_device);
  
  /* IMUSR : check the toggle states makes sense */
  status = I_musr_check_toggle();
  if (status != DB_SUCCESS)
  {
    if(debug)
      printf("I_musr_validate_params: bad status after I_musr_check_toggle (%d)\n",status);
    return(status);
  }

  status = I_musr_validate_sweep_values();

  
/* IMUSR : the no. of scaler channels defines the no. of histograms */
  status = I_musr_write_scaler();
  if (status != DB_SUCCESS)
  {
    if(debug)
      printf("I_musr_validate_params: bad status after I_musr_write_scaler (%d)\n",status);
    return(status);
  }
  if(status != SUCCESS)
  {
    cm_msg(MERROR,"begin_of_run","error return from validate_sweep_values (%d)", status);
    return FE_ERR_HW;
  }
  

  /* get the limits of the sweep according to toggle type (adding in the toggle_value if necessary) */
  printf("begin_of_run: calling I_musr_get_limits with inner:%s outer:%s start:%f stop:%f intogval:%f outtogval:%f \n",
	 gbl_inner_toggle_type, gbl_outer_toggle_type, 
	 gbl_sweep_start, gbl_sweep_stop,
	 imusr.input.inner_toggle_value,imusr.input.outer_toggle_value);
  
  if(I_musr_get_limits (gbl_inner_toggle_type, gbl_outer_toggle_type, 
		 gbl_sweep_start, gbl_sweep_stop,
		 imusr.input.inner_toggle_value,imusr.input.outer_toggle_value,
		 &sweep_min,&sweep_max)== SUCCESS)
    printf(" BOR: after I_musr_get_limits, sweep_max=%f,sweep_min=%f\n",sweep_max,sweep_min);
  else
    {
      cm_msg(MERROR,"I_musr_validate_params","error return from I_musr_get_limits; cannot check sweep device parameters");
      return FE_ERR_HW;
    }
  

  if(debug)printf(" TO BE ADDED : check on sweep device (range etc.) and CAMP equipment checks \n"); 
  /* camp */


  
  
  /* IMUSR : writes only  bytes/bin */
  status = musr_write_v680();
  if (status != DB_SUCCESS)
  {
    if(debug)
      printf("I_musr_validate_params: bad status after musr_write_v680 (%d)\n",status);
    return(status);   
  }
  /* write values to imdarc area - doesn't need to
     write anything to mdarc area at present */
  status = I_musr_write_imdarc();
  if (status != DB_SUCCESS)
  {
    if(debug)
      printf("I_musr_validate_params: bad status after I_musr_write_imdarc (%d)\n",status);
    return(status);   
  }
  
  return DB_SUCCESS;
}


/* ----------------------------------------------------------------------------------------*/

INT I_musr_write_imdarc()

/* ----------------------------------------------------------------------------------------*/
{
  INT nc,size,status;
  HNDLE  hTmp;
  char str[128];
  INT num_datawords;

  /* I_MUSR does not use mdarc histogram area (apart from beamline) since
     currently  for both toggles no. histograms > MAX_HIS
     
     an imdarc area has been defined for the banknames etc
     
     
     Total No. "virtual" scaler channels  =
     no. "virtual" scaler channels for fixed scaler inputs (8+2)
     + 8 if FAST MOD is enabled

     If one toggle type is "none" many of these "virtual" channels will be empty.
     Imdarc should determine what to save depending on the toggle type.
     
  */    

  /* nc = no. "virtual" scaler channels sent in the bank */
  nc =  SCALER_SUM_CHANNELS ; //(2+8)
  if (imusr.input.fast_modulation)
    nc +=  FM_SUM_CHANNELS; //8
  
  /* add 2 for the data point counter and the sweep value (1st 2 words in bank) */
  num_datawords = 2 + nc; // no. words in the bank
  if(debug)
    printf("Number of virtual scaler channels: %d, no. words in sum_scaler array: %d\n",
	   nc-2,num_datawords);


  /* now calculate how many data words will actually be sent out in the data bank 
     (depends on toggle types) */

    //imusr.imdarc.num_datawords_in_bank
  /*  Note:  outer toggle must also be NONE if inner toggle is NONE */
  if ( strcmp("NONE",gbl_inner_toggle_type) == 0)
    imusr.imdarc.num_datawords_in_bank=2;
  else
    {
      if ( strcmp("NONE",gbl_outer_toggle_type) == 0)
	imusr.imdarc.num_datawords_in_bank=4;
      else
	imusr.imdarc.num_datawords_in_bank=8;
    }
  if(imusr.input.fast_modulation)
    imusr.imdarc.num_datawords_in_bank=imusr.imdarc.num_datawords_in_bank*2;

  imusr.imdarc.num_datawords_in_bank+=4; /* add in data point counter, sweep value, clock, total_rate */
  
  if(debug)
    printf("Number words in data bank = %d, inner_toggle=%s outer_toggle=%s fast_mod=%d\n"
	   ,imusr.imdarc.num_datawords_in_bank,gbl_inner_toggle_type,gbl_outer_toggle_type,imusr.input.fast_modulation);



  if(single_loop)
  {
    if (no_update)
    {
      printf("\n *** I_musr_write_imdarc: Single sweep mode - run is in progress\n");
      printf(" ***     so imdarc area will not be updated \n\n");
      return(SUCCESS);
    }
    else
    {
      printf("I_musr_write_imdarc: Single sweep mode - run is NOT in progress\n");
      printf("            so imdarc area will be updated\n");
    }
  }

 
  //     Find the key for imdarc area
  sprintf(str,"/Equipment/%s/Settings/imdarc",j_eqp_name); 
  
  size = sizeof(imusr.imdarc.num_datawords_in_bank);
  status = db_set_value( hDB, hIS, "imdarc/num datawords in bank", &imusr.imdarc.num_datawords_in_bank,  size, 1, TID_INT);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"I_musr_write_imdarc","could not update \"%s/num datawords in bank\" (%d)\n",str,status);
    write_message1(status,"musr_write_v680");
    return status;   
  }

 
  
  return status;
  
 }

/*------------------------------------------------------------------------*/

INT  I_musr_check_toggle(void)
  
/*---------------------------------------------------------------------*/
{
  /* check the toggle types make sense */
  

  INT num_outer_toggles,num_inner_toggles;
  INT j,len;
  INT size,status;
  
  /* get the number of toggles */
  num_inner_toggles = imusr.input.num_inner_toggle_cycles;
  num_outer_toggles = imusr.input.num_outer_toggle_cycles; 
  
  /* and the toggle types */
  sprintf( gbl_inner_toggle_type,"%s",imusr.input.inner_toggle_type);
  len=strlen( gbl_inner_toggle_type);
  for(j=0; j<len; j++)
    gbl_inner_toggle_type[j] = toupper ( gbl_inner_toggle_type[j]); /* convert to upper case */
  
  
  sprintf( gbl_outer_toggle_type,"%s",imusr.input.outer_toggle_type);
  len=strlen( gbl_outer_toggle_type);
  for(j=0; j<len; j++)
    gbl_outer_toggle_type[j] = toupper ( gbl_outer_toggle_type[j]); /* convert to upper case */    

  if(debug)
    {
      printf("toggle types inner:%s outer: %s\n",gbl_inner_toggle_type,gbl_outer_toggle_type);
      printf("num toggles  inner:%d outer: %d\n",
	     num_inner_toggles, num_outer_toggles);
    }
  /* Check toggle type is NONE, HARD, SOFT, or REF */

  if ( strcmp("NONE", gbl_inner_toggle_type) != 0 &&
       strcmp("HARD", gbl_inner_toggle_type) != 0 &&
       strcmp("REF", gbl_inner_toggle_type) != 0 &&
       strcmp("SOFT", gbl_inner_toggle_type) != 0 )
  {
    cm_msg(MERROR,"I_musr_check_toggle",
	   "Invalid Inner toggle type \"%s\". Must be one of \"HARD\" \"SOFT\" \"REF\" or \"NONE\" ",gbl_inner_toggle_type);
    return(DB_INVALID_PARAM);
  }
  if ( strcmp("NONE", gbl_outer_toggle_type) != 0 &&
       strcmp("HARD", gbl_outer_toggle_type) != 0 &&
       strcmp("REF", gbl_outer_toggle_type) != 0 &&
       strcmp("SOFT", gbl_outer_toggle_type) != 0 )
  {
    cm_msg(MERROR,"I_musr_check_toggle",
	   "Invalid Outer toggle type \"%s\". Must be one of \"HARD\" \"SOFT\"  \"REF\" or \"NONE\" ",gbl_outer_toggle_type);
    return(DB_INVALID_PARAM);
  }

  if ( strcmp("NONE", gbl_inner_toggle_type) == 0)
  {
    if ( strcmp("NONE", gbl_outer_toggle_type) != 0)
      {
	cm_msg(MERROR,"I_musr_check_toggle",
	       "If Inner toggle type is \"%s\" then outer toggle type \"%s\" must also be \"NONE\" ",
	       gbl_inner_toggle_type,gbl_outer_toggle_type);
	return(DB_INVALID_PARAM);
      }
  }

      
/*
  I N N E R     T O G G L E
*/
  
  /* If no. toggles is <= 0,  toggle_type must be NONE */
  if(  num_inner_toggles <= 0)
  {
    if ( strcmp("NONE", gbl_inner_toggle_type) != 0)
    {
     sprintf( gbl_inner_toggle_type,"NONE");
      if(single_loop)
      {
	if (no_update)
	{
	  printf("\n *** I_musr_check_toggle: Single sweep mode - run is in progress\n");
	  printf(" ***       Inner toggle mode will NOT be changed to NONE\n\n");
	}
	else
	{
	  printf("I_musr_check_toggle: Single sweep mode - run is NOT in progress\n");
	  printf("            so  Inner toggle mode WILL be changed to NONE\n");
	}
      }// end of if single loop
      
      if (!no_update)
      {
	  cm_msg(MINFO,"I_musr_check_toggle","Inner toggle mode changed to \"NONE\" since num toggles <= 0");
 	  /* write to odb  */
	  sprintf( imusr.input.inner_toggle_type,"NONE");      
	  size = sizeof (imusr.input.inner_toggle_type);
	  status=db_set_value (hDB, hIS, "input/inner toggle type", &imusr.input.inner_toggle_type, size, 1, TID_STRING);
	  if (status != DB_SUCCESS)
	  {
	      cm_msg(MERROR,"I_musr_check_toggle","could not write value=%s to \"input/inner_toggle_type\" (%d) ",
		     imusr.input.inner_toggle_type,status);
	      return status;
	  }
      }
      
    }
  }
  
  /* If toggle_type = NONE, then  no. toggles must be 0  */
  if (strcmp("NONE", gbl_inner_toggle_type) == 0)
  {
      if( num_inner_toggles != 0)
      {
	  num_inner_toggles = 0; /* set no. toggles to 0 - no toggling */
	  
	  if(single_loop)
	  {
	      if (no_update)
	      {
		  printf("\n *** I_musr_check_toggle: Single sweep mode - run is in progress\n");
		  printf(" ***     so  number of inner toggle will NOT be set to 0 as toggle type is NONE\n\n");
	      }
	      else
	      {
		  printf("I_musr_check_toggle: Single sweep mode - run is NOT in progress\n");
		  printf("            so number of Inner toggles WILL be changed to 0\n");
	      }
	  }  // single toggle mode

	  if(!no_update)
	  {
	      cm_msg(MINFO,"I_musr_check_toggle","Number of Inner toggle cycles set to 0 since toggle type is \"NONE\" ");
	      
	      /* write also to odb  */
	      imusr.input.num_inner_toggle_cycles = 0;
	      size = sizeof ( imusr.input.num_inner_toggle_cycles);
	      status=db_set_value (hDB, hIS, "input/num inner toggle cycles", &imusr.input.num_inner_toggle_cycles, size, 1, TID_INT);
	      if (status != DB_SUCCESS)
	      {
		  cm_msg(MERROR,"I_musr_check_toggle","could not write value=%d to \"input/num inner toggle cycles\" (%d) ",
			 imusr.input.num_inner_toggle_cycles,status);
		  return FE_ERR_ODB;
	      }
	  } // update
	  
      }
  } 
/*
  O U T E R     T O G G L E
*/
  
  /* If no. toggles is <= 0,  toggle_type must be NONE */
  if(  num_outer_toggles <= 0)
  {
      if ( strcmp("NONE", gbl_outer_toggle_type) != 0)
      {
	  sprintf( gbl_outer_toggle_type,"NONE");
	  if(single_loop)
	  {
	      if (no_update)
	      {
		  printf("\n *** I_musr_check_toggle: Single sweep mode - run is in progress\n");
		  printf(" ***       Outer toggle mode will NOT be changed to NONE\n\n");
	      }
	      else
	      {
		  printf("I_musr_check_toggle: Single sweep mode - run is NOT in progress\n");
		  printf("            so  Outer toggle mode WILL be changed to NONE\n");
	      }
	  } // end of single loop

	  if(!no_update)
	  {
	      cm_msg(MINFO,"I_musr_check_toggle","Outer toggle mode changed to \"NONE\" since num toggles <= 0");
	      /* write also to odb  */
	      sprintf( imusr.input.outer_toggle_type,"NONE");      
	      size = sizeof (imusr.input.outer_toggle_type);
	      status=db_set_value (hDB, hIS, "input/outer toggle type", &imusr.input.outer_toggle_type, size, 1, TID_STRING);
	      if (status != DB_SUCCESS)
	      {
		  cm_msg(MERROR,"I_musr_check_toggle","could not write value=%s to \"input/outer_toggle_type\" (%d) ",
			 imusr.input.outer_toggle_type,status);
		  return FE_ERR_ODB;
	      }
	  } // update
     
      }
  }
  
  /* If toggle_type = NONE, then  no. toggles must be 0  */
  if (strcmp("NONE", gbl_outer_toggle_type) == 0)
  {
      if( num_outer_toggles != 0)
      {
	  num_outer_toggles = 0; /* set no. toggles to 0 - no toggling */
	  if(single_loop)
	  {
	      if (no_update)
	      {
		  printf("\n *** I_musr_check_toggle: Single sweep mode - run is in progress\n");
		  printf(" ***     so  number of outer toggle will NOT be set to 0 as toggle type is NONE\n\n");
	      }
	      else
	      {
		  printf("I_musr_check_toggle: Single sweep mode - run is NOT in progress\n");
		  printf("            so number of Outer toggles WILL be set to 0\n");
	      }
	  }   // end of if single loop
	  if(!no_update)
	  {
	      cm_msg(MINFO,"I_musr_check_toggle","Number of Outer toggle cycles set to 0 since toggle type is \"NONE\" ");
	      
	      /* write also to odb  */
	      imusr.input.num_outer_toggle_cycles = 0;
	      size = sizeof ( imusr.input.num_outer_toggle_cycles);
	      status=db_set_value (hDB, hIS, "input/num outer toggle cycles", &imusr.input.num_outer_toggle_cycles, size, 1, TID_INT);
	      if (status != DB_SUCCESS)
	      {
		  cm_msg(MERROR,"I_musr_check_toggle","could not write value=%d to \"input/num outer toggle cycles\" (%d) ",
			 imusr.input.num_outer_toggle_cycles,status);
		  return FE_ERR_ODB;
	      }
	  } // update
	  
      }
      
      // Recheck in case toggle types have been changed by above checks  
      if ( strcmp("NONE", gbl_inner_toggle_type) == 0)
      {
	  if ( strcmp("NONE", gbl_outer_toggle_type) != 0)
	  {
	      cm_msg(MERROR,"I_musr_check_toggle",
		     "If Inner toggle type is \"%s\" then outer toggle type \"%s\" must also be \"NONE\" ",
		     gbl_inner_toggle_type,gbl_outer_toggle_type);
	      return(DB_INVALID_PARAM);
	  }
      }
      
  } 

  return SUCCESS;      
}

/* ----------------------------------------------------------------------------------------*/

INT I_musr_write_scaler()
     
/* ----------------------------------------------------------------------------------------*/
{
  
  /* write scaler information into /Equipment/scaler/settings area in odb
     for IMUSR

     The fixed scaler inputs and titles are written each time in case they got
     changed by running TD-Musr

     If fast modulation  is enabled, later may let user select the title. For
     now they are fixed.
          
     The (global) key for the scaler settings area /Equipment/scaler/settings is hScal
     
      set_record is used to write the scaler settings area.

 

  */
  char str[128];
  INT i,j,k;
  INT status,size;
  INT previous_input;
  /* clear the scaler values in odb */
  HNDLE hTmp=0;
  KEY   hKScaler;
  double clear_scaler[MAX_NSCALS];

/*   Fixed scaler titles imusr_sc_titles are defined in imusr_scaler_sum.h  */  
  // maximum scaler title length :32
  
  /* assign the values */
  printf("\n");
  
  if(debug)
    printf("I_musr_write_scaler: starting\n");
  
  fscal.enabled        = TRUE; // got to be enabled
  fscal.num_inputs =  SC_NUM_FIXED_INPUTS;

    /* Fixed scalers from imusr_inputs.h */
  fscal.inputs[0] = SC_CLOCK;        // 15  
  fscal.inputs[1] = SC_TOTAL_RATE;   // 14
  fscal.inputs[2] = SC_BACK_FM_NEG;  // 13
  fscal.inputs[3] = SC_FRONT_FM_NEG; // 12
  fscal.inputs[4] = 0 ;
  fscal.inputs[5] = 0 ;

  /* check for fast modulation */
  if(imusr.input.fast_modulation)
  {
    fscal.num_inputs +=  SC_MAX_FM_INPUTS; // we have fast mod scalers as well
    if(debug)printf("fast modulation is ENABLED; no. scaler inputs = %d\n",fscal.num_inputs);
    fscal.inputs[4] = SC_BACK_FM_POS ; // 11
    fscal.inputs[5] = SC_FRONT_FM_POS; // 10
  }   
  else
    if(debug)printf("fast modulation is DISABLED; no. scaler inputs = %d\n",fscal.num_inputs);
  /* write fixed scaler titles each time ( SC_NUM_FIXED_INPUTS=4 fixed ) */
  for(j=0; j < fscal.num_inputs ; j++)
  {
    strncpy(fscal.titles[j], imusr_sc_titles[j],32);
    fscal.titles[j][31]='\0';
  }
  
  // this shouldn't happen
  if (  fscal.num_inputs >  MAX_NSCALS)
  {
    cm_msg(MINFO,"I_musr_write_scaler","invalid no. of scalers (%d) is > maximum (%d)",
	   fscal.num_inputs,MAX_NSCALS);
    return (DB_INVALID_PARAM);
  }
  
  fscal.bit_pattern=0;
  for(j=0; j < fscal.num_inputs; j++)
  {
    
    /* not necessary to check for duplicates ... calculate bit pattern */
    fscal.bit_pattern = fscal.bit_pattern | ( 1 <<  (fscal.inputs[j]) );
  } // end of for loop on enabled channels
  
  
  for (j = fscal.num_inputs ; j < MAX_NSCALS; j++) 
  {
    strcpy(fscal.titles[j], ""); // any unused array elements will be set blank
    fscal.inputs[j] = -1 ; // -1 is not a valid input for the scaler
  }
  
  for(j=0; j < fscal.num_inputs; j++)  // check the titles are unique
  { 
    for(k=j+1; k < fscal.num_inputs; k++) 
    {
      //if(debug)printf("j=%d, comparing scaler[%d]=%s to scaler[%d]=%s\n",j,j,fscal.titles[j],k,fscal.titles[k]);
      if (strcmp(fscal.titles[j],fscal.titles[k]) ==0)
      {
	cm_msg(MERROR,"I_musr_write_scaler","duplicate scaler name \"%s\" in scaler array",fscal.titles[k]);
	return(DB_INVALID_PARAM);
      }
    }
  }

  if(debug)
  {
    printf("I_musr_write_scaler: updating /equipment/scaler/settings with the values:\n");
    printf  ("enabled           = %d\n",fscal.enabled);
    printf  ("number of inputs  = %d\n",fscal.num_inputs);
    printf  ("bitpattern  = 0x%x\n",fscal.bit_pattern);
 
  /* IMUSR : scaler is always enabled */
    printf  ("Scaler:\n");
    printf(  "Index Input Title\n");    
    for (j=0; j < fscal.num_inputs ; j++)
      printf(" %2d    %2d   %-s\n",j, fscal.inputs[j], fscal.titles[j]);
  }
  /* Update scalers/settings in ODB */
  size = sizeof(fscal);
  status = db_set_record(hDB, hScal, &fscal, size, 0);
  if (status != DB_SUCCESS)
  {
    cm_msg(MERROR,"I_musr_write_scal","Failed to update scaler record (size=%d) (%d)",size,status);  
    write_message1(status,"I_musr_write_scal");
  }
  
  /* Check for single sweep mode with run going .. if so
     DO NOT clear old scaler values
  */  
  if(single_loop)
  {
    if (no_update)
    {
      printf("\n *** I_musr_write_scal: Single sweep mode - run is in progress\n");
      printf(" ***       scaler counts will not be cleared\n\n");
      return(status);
    }
    else
    {
	printf("I_musr_write_scal: Single sweep mode - run is NOT in progress\n");
	printf("            so scaler counts will be cleared \n");
    }
  }
  
  /*
    Clear the old Scaler values in case TD was running last
  */
  
  for (j=0; j<MAX_NSCALS; j++)
      clear_scaler[j]=0.0;
  
  /* find the key for scaler/Variables/SCLR area */
  sprintf(str,"/Equipment/Scaler/Variables/SCLR"); 
  status = db_find_key(hDB, 0, str, &hTmp);
  if (status != DB_SUCCESS)
  {
      printf("I_musr_write_scaler: could not find key for %s (%d)\n",str,status);
      printf("              TD-musr scaler values are not present\n");
      /*      return(SUCCESS);  non-fatal error - TD may not have been running with this
	      odb */
      return status;
  }
  
  status = db_get_key(hDB, hTmp, &hKScaler);
  if (status != DB_SUCCESS)
  {
    printf("I_musr_write_scaler: could not get key for %s (%d)\n",str,status);
    printf("              could not clear previous  values\n");
    return(status);
  }

  j =  hKScaler.num_values; /* find out total no. of scalers */
  if(debug)
    printf("I_musr_write_scaler: number of defined scalers in %s is %d\n",str,j);
  /* clear the array  */ 
  status = db_set_data(hDB, hTmp, &clear_scaler,  j * sizeof(double), 
		       j, TID_DOUBLE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"I_musr_write_scaler","failure with set_data to  %s (%d)\n",str,status);
    printf("I_musr_write_scaler: failure with set_data to  %s (%d)\n",str,status);
    printf("              could not clear previous  values\n");
    return(status); 
  }
  
  return(status);
}


/*-------------------------------------------------------------------------------------------------------*/

INT I_musr_validate_sweep_values(void)
/*-------------------------------------------------------------------------------------------------------*/
{
  /* get the sweep values and check they are reasonable */
  
  printf("validate_sweep_values: sweep start=%f stop=%f step=%f \n",
	 imusr.input.sweep_start,imusr.input.sweep_stop,imusr.input.sweep_step);
  
  
  gbl_sweep_start = imusr.input.sweep_start;
  gbl_sweep_stop  = imusr.input.sweep_stop;
  gbl_sweep_step  = imusr.input.sweep_step; /* is.input.sweep_step is hotlinked */
  gbl_sweep_direction = 1;
  
  
  if( gbl_sweep_step == 0 )
  {
    cm_msg(MERROR,"validate_sweep_values","Invalid sweep increment value: %f",
	   gbl_sweep_step);
    return FE_ERR_HW;
  }
  
  if( ((gbl_sweep_step > 0) && (gbl_sweep_stop < gbl_sweep_start)) ||
      ((gbl_sweep_step < 0) && (gbl_sweep_start < gbl_sweep_stop)) )
    gbl_sweep_direction *= -1;
  
  
  /* make sure we have at least one step */
  if ( ( fabs(gbl_sweep_stop - gbl_sweep_start)/ fabs(gbl_sweep_step) )  < 1)
  {
    cm_msg(MERROR,"validate_sweep_values","invalid sweep step size (%f) ; must have at least one step",
	   gbl_sweep_step);
    return FE_ERR_HW;
  }



  return(SUCCESS);
}
/*------------------------------------------------------------------------------------------*/

INT I_musr_get_limits (char *pinner, char *pouter, float start,float stop, float innertogval, float outertogval,float *pmin, float*pmax)
{
  
  float iref,oref;
  int IREF,OREF;
  float xstart,xstop, it_val,ot_val;
  float min,max;
 
  if(debug)
    printf("I_musr_get_limits: inner:%s outer:%s start:%f stop:%f intogval:%f outtogval:%f \n",
	   pinner, pouter, start, stop,  innertogval, outertogval);
  
    
  IREF=OREF=0;
  it_val=iref=0;
  ot_val=oref=0;
  
  
  if (strcmp (pinner,"REF")==0 )
    {
      if(debug)
	printf("I_musr_get_limits: inner is REF toggle\n");
      IREF=1;
      iref=innertogval;
    }
  else if (strcmp (pinner,"SOFT")==0 )
    {
      if(debug)
	printf("I_musr_get_limits: inner is SOFT toggle\n");
      it_val=innertogval;
    }
  else if( (strcmp (pinner,"HARD")==0 ) || (strcmp (pinner,"NONE")==0))
    {
      if(debug)
	printf("I_musr_get_limits: inner toggle (%s) is HARD or NONE\n",pinner);
    }
  else
    {
      printf("I_musr_get_limits: illegal inner toggle type %s\n",pinner);
      return(-1);
    }
  

  if (strcmp (pouter,"REF")==0 )
    {
      if(debug)
	printf("I_musr_get_limits: outer is REF toggle\n");
      OREF=1;
      oref=outertogval;
    }
  else if (strcmp (pouter,"SOFT")==0 )
    {
      if(debug)
	printf("I_musr_get_limits: outer is SOFT toggle\n");
      ot_val=outertogval;
    }
  else if( (strcmp (pouter,"HARD")==0 ) || (strcmp (pouter,"NONE")==0))
    {
      //if(debug)
      printf("I_musr_get_limits: outer toggle (%s) is HARD or NONE\n",pouter);
    }
  else
    {
      printf("I_musr_get_limits: illegal outer toggle type %s\n",pouter);
      return(-1);
    }
  
  /* use soft toggle values */
  
  xstart = fabs(start) + fabs(it_val) + fabs(ot_val);
  xstop = fabs (stop) + fabs(it_val) + fabs(ot_val);
  
  if(start < 0) 
    xstart = xstart * -1;
  if(stop < 0) 
    xstop = xstop * -1;
  
  if(debug)
    printf("I_musr_get_limits: soft : start=%f, stop=%f, it_val=%f,ot_val=%f,  xstart=%f, xstop=%f\n",
	 start,stop,it_val,ot_val,xstart,xstop);
  min = smin(xstart,xstop);
  max = smax(xstart,xstop);
  
  printf("I_musr_get_limits: Min is %f, max is %f\n",min,max);
  
  /* Now check for ref */
  if(IREF)
    {
      if(debug)
	printf("I_musr_get_limits: REF:  inner_tog_val=%f\n",iref);
      
      min = smin(min,iref);
      max = smax(max,iref);
    }
  if(OREF)
    {
      if(debug)
	printf("I_musr_get_limits: REF:  outer_tog_val=%f\n",oref);
      
      min = smin(min,oref);
      max = smax(max,oref);
    }
  
  if(debug)
    printf("I_musr_get_limits:  Returning with Min= %f, max =%f\n",min,max);
  *pmin=min;
  *pmax=max;
  return(SUCCESS);
}



/*------------------------------------------------------------------------------------------*/


float smin(float a, float b)
{
  if ( a < b)
    return a;
  else
    return b;
}

/*------------------------------------------------------------------------------------------*/

float smax(float a, float b)
{
  if ( a > b)
    return a;
  else
    return b;
}




/*------------------------------------------------------------------*/

/* Main program - based on rf_config */

/*------------------------------------------------------------------*/
int main(unsigned int argc,char **argv)
{
  INT    status, stat1; 
  DWORD  j, i;
  HNDLE  hKey;
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  char   info[128];
  INT    fHandle;
  INT    msg, ch;
  BOOL   daemon;
  INT    run_state,size;
  daemon = FALSE;

  single_loop = 0;
  /* for Midas 1.9.3 */
  cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);

/* for earlier Midas */
  //cm_get_environment (host_name, expt_name);

  /* get parameters */
  /* parse command line parameters */
  for (i=1 ; i<argc ; i++)
  {
    if (argv[i][0] == '-' && argv[i][1] == 'd')
      debug = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 'D')
      daemon = TRUE;
    else if (argv[i][0] == '-' && argv[i][1] == 's')
      single_loop = 1;
    else if (argv[i][0] == '-')
    {
      if (i+1 >= argc || argv[i+1][0] == '-')
	goto usage;
      if (strncmp(argv[i],"-e",2) == 0)
	strcpy(expt_name, argv[++i]);
      else if (strncmp(argv[i],"-h",2)==0)
	strcpy(host_name, argv[++i]);
    }
    else
    {
   usage:
      printf("usage: musr_config -d (debug) -s (single loop) \n");
      printf("             [-h Hostname] [-e Experiment] [-D] \n\n");
      return 0;
    }
  }
  
  if (daemon)
    {
    printf("Becoming a daemon...\n");
    ss_daemon_init(FALSE);
    }
  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "musr_config", 0);
  if (status != CM_SUCCESS)
    return 1;
  
#ifdef _DEBUG
  cm_set_watchdog_params(TRUE, 0);
#endif
  
  /* turn off message display, turn on message logging */
  cm_set_msg_print(MT_ALL, 0, NULL);
  
  /* register transition callbacks */
  /* prestart before frontend start (500) */
  if (cm_register_transition(TR_START, tr_prestart, 400) != CM_SUCCESS )
  {
      cm_msg(MERROR, "main", "cannot register transition");
      goto error;
    }

  /* connect to the database */
  cm_get_experiment_database(&hDB, &hKey);

  if(!single_loop)
  {
      status=cm_exist("musr_config",FALSE); 
      //printf("status after cm_exist for musr_config = %d (0x%x)\n",status,status);
      if(status == CM_SUCCESS)
      {
          cm_msg(MERROR, "main","Detected another copy of musr_config already running");
          printf("main: detected another copy of musr_config already running\n");
          goto exit;
      }
  }

  
  status=musr_create_rec();
  if (status != DB_SUCCESS) goto error;

  // single sweep mode
  if (single_loop)    
    {         /* single sweep */
      status = tr_prestart(10, info);   // call prestart
      if (status)
	{
	  printf("Thank you\n");
	  goto exit; /* force exit */
	}
      else
	goto error; 
    }
  else
    printf("musr_config: ready\n");
  
  
  /* initialize ss_getchar() */
  ss_getchar(0);
  do
    {
      while (ss_kbhit())
	{
	  ch = ss_getchar(0);
      if (ch == -1)
	ch = getchar();
      if (ch == 'R')
	ss_clear_screen();
      if ((char) ch == '!')
	break;
    }
    msg = cm_yield(1000);
  } while (msg != RPC_SHUTDOWN && msg != SS_ABORT && ch != '!');
  

  printf("Thank you\n");
  goto exit;
 
 error:
  printf("\n Error detected. Check odb messages for details\n");
  
 exit:


  /* reset terminal */
  ss_getchar(TRUE);
  
  cm_disconnect_experiment();
  return 1;
}

