/*
 *	$Id: tip850Lib.c,v 1.2 2000/12/22 21:20:41 David.Morris Exp $
 * 
 *	$Revision: 1.2 $
 *
 *	Purpose: This module contains the implementation of the TEWS Datenteknik TIP-850
 *	DAC/ADC board driver for VxWorks
 *
 *	$Log: tip850Lib.c,v $
 *	Revision 1.2  2000/12/22 21:20:41  David.Morris
 *	Added CVS tags. Cleaned up braces through code
 *	
 *
 */

#include "vxWorks.h"
#include "taskLib.h"
#include "semLib.h"
#include "wdLib.h"
#include "iv.h"
#include "stdio.h"
#include "tip850Lib.h"

TIP850_BOARD tip850Board[TIP850_MAX_BOARD];

SEM_ID sidTip850ADCComplete;

/*
tip850Init(int iPosition, short sVector)
Purpose:	This routine initializes the data structures associated with the 
					ADC and DAC channels on the TIP850 Industry Pack module. It attaches the interrupt routine
		used to indicate the end of an ADC collection. It spawns the ADC 
		scanning task which collects data for all registered ADC channel in the
		module.
Inputs:	 iPosition - integer identifying the Industry Pack position on the MVME162
					sVector - short integer identifying the unique interrupt vector for this board.
Precond:	A TIP850 must be installed in the proper position on the MVME162. An access fault will
					occur if no board is installed or the wrong position is given.
Outputs:	STATUS - the result code indicates if the initialization was successful. TIP850_ERROR is
					returned for any error. TIP850_OK is returned on success.
Postcond: The IP will be initialized, the registry will be created and the ADC scan task will start.
*/

STATUS tip850Init(char cPosition, short sVector) {
	static char bFirstTime = TRUE;
	int iIndex;
	unsigned char szBuffer[16];
	int tidTip850Scan;
	char * pcIPProm;

 /* First time through initialize positions */

	if (bFirstTime) {
		for (iIndex = 0; iIndex < TIP850_MAX_BOARD; iIndex ++) {
			tip850Board[iIndex].bRegistered = FALSE;
		}

		bFirstTime = FALSE;
	}

	/* Determine location of IP specified in cPosition */

	cPosition -= 'A';
	if (cPosition > 3) {
		cPosition -= 0x20;
	}

	/* Is input data valid? Is this board already initialized? */

	if ((cPosition < 0) || (cPosition > 3) || (tip850Board[cPosition].bRegistered)) {
		return TIP850_ERROR;
	}

	/*
	 *	Check ID PROM data
	 */
	pcIPProm = (char *) MVME162_IPBaseAddress + (cPosition * 0x100) + 0x81;
	for( iIndex = 0; iIndex < 7; iIndex++ ) {
		szBuffer[iIndex] = *pcIPProm;
		pcIPProm += 2;
	}
	if( szBuffer[0] != 'I' || szBuffer[1] != 'P' || szBuffer[2] != 'A' ||
			szBuffer[3] != 'C' || szBuffer[4] != 0xB3 || szBuffer[5] != 0x11 ) {
		printf( "Invalid ID PROM data: '%c%c%c%c' Manufacturer:'0x%X' Model:'0x%X' Revision:'0x%X'\n", 
		szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
		szBuffer[4], szBuffer[5], szBuffer[6] );
		return(TIP850_ERROR);
	}	else {
		printf( "ID PROM data: '%c%c%c%c' Manufacturer '0x%X' Model '0x%X' Revision '0x%X'\n", 
		szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
		szBuffer[4], szBuffer[5], szBuffer[6] );
	}

	/* Register the position. Set all addresses */

	tip850Board[cPosition].iPosition = cPosition;

	tip850Board[cPosition].psADC_CSR =		 (short *) (MVME162_IPBaseAddress + cPosition * 0x100);
	tip850Board[cPosition].psADC_CON =		 tip850Board[cPosition].psADC_CSR + 0x01;
	tip850Board[cPosition].psADC_DAT =		 tip850Board[cPosition].psADC_CSR + 0x02;
	tip850Board[cPosition].psDAC_DAT[0] =	tip850Board[cPosition].psADC_CSR + 0x03;
	tip850Board[cPosition].psDAC_DAT[1] =	tip850Board[cPosition].psADC_CSR + 0x04;
	tip850Board[cPosition].psDAC_DAT[2] =	tip850Board[cPosition].psADC_CSR + 0x05;
	tip850Board[cPosition].psDAC_DAT[3] =	tip850Board[cPosition].psADC_CSR + 0x06;
	tip850Board[cPosition].psDAC_LOA =		 tip850Board[cPosition].psADC_CSR + 0x07;
	tip850Board[cPosition].psVector =			tip850Board[cPosition].psADC_CSR + 0x20;
	tip850Board[cPosition].psIDAddress =	 tip850Board[cPosition].psADC_CSR + 0x40;

	*tip850Board[cPosition].psVector = sVector;
 
	/* Initialize all channels on board */

	for (iIndex = 0; iIndex < TIP850_MAX_ADC; iIndex ++) {
		tip850Board[cPosition].ADC[iIndex].Board = & tip850Board[cPosition];
		tip850Board[cPosition].ADC[iIndex].iType = TIP850_ADC;
		tip850Board[cPosition].ADC[iIndex].bRegistered = FALSE;
		tip850Board[cPosition].ADC[iIndex].iChannel = iIndex;
	}
	
	for (iIndex = 0; iIndex < TIP850_MAX_DAC; iIndex ++) {
		tip850Board[cPosition].DAC[iIndex].Board = & tip850Board[cPosition];
		tip850Board[cPosition].DAC[iIndex].iType = TIP850_DAC;
		tip850Board[cPosition].DAC[iIndex].bRegistered = FALSE;
		tip850Board[cPosition].DAC[iIndex].iChannel = iIndex;
	}

	/* Set up general operation for this IP position
		 
		 General Control Registers look like:
				x_ERR	 0	x_RT1	 x_RT0	 x_WIDTH1	 x_WIDTH0	 0	 x_MEN

		 x_ERR: 1 = Assert IP's Error* signal.
		 x_RT1-x_RT0: Recovery Timer Delay 0, 2, 4, 8 useconds.
		 x_WIDTH1-x_WIDTH0: 00=32bits, 01=8bits, 10=16bits.
		 x_MEN: 1=Enable Memory Accesses to the IP

					 0 0 0 0	0 0 0 0	= 0x00	(non memory enabled, I/O only)
	*/

	*((unsigned char *) (MVME162_IPIC_ADDRESS + 0x18 + cPosition)) = 0x00;

	/*
		connect the TIP850 IP board interrupt:
	*/

	sidTip850ADCComplete = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
	if (ERROR == intConnect (INUM_TO_IVEC (sVector), tip850IntADC, 0)) {
		return TIP850_ERROR;
	}

	/*
		now enable interrupts in IPIC to drive selected IP board

		interrupt register looks like:
		 PLTY			 EL		 INT		IEN		ICLR		 IL2		 IL1			IL0
				PLTY: 1 = Rising Edge/High Level causes interrupts
	EL: 1 = Edge Sensitive
	INT: (READ ONLY) 1 = Interrupt being generated
	IEN: 1 = Interrupts enabled
	ICLR: 1 = Clear edge interrupt.	No meaning for level-ints.
	IL2-IL0: Level at which IP should interrupt.

		For interrupting IP's, this should generally be:
				 0 0 0 1	0 1 0 0 = 0x14	(interrupt low-level on #3)
		For non-interrupting IP's, this should be:
				 0 0 0 0	0 0 0 0 = 0x00


		 As suggested by GreenSpring Application Brief AN#93008
		"Using Industry Packs on the Motorola MVME162"
	*/

	/*
	*((unsigned char *) (MVME162_IPIC_ADDRESS + 0x10 + cPosition * 2)) = 0x15 - cPosition;
	*((unsigned char *) (MVME162_IPIC_ADDRESS + 0x11 + cPosition * 2)) = 0x15 - cPosition;

	*/

	/* Spawn a scanning task for this TIP850 module */

	tidTip850Scan = taskSpawn("tipScan", 100, VX_FP_TASK, 20000, (FUNCPTR) tip850Scan,
					(int) (& tip850Board[cPosition]), 0, 0, 0, 0, 0, 0, 0, 0, 0);

	tip850Board[cPosition].bRegistered = TRUE;

	return TIP850_OK;
}


/*
tip850PhysDevCreate(int iPosition, int iType, int iChannel)
Purpose:	This routine registers the requested ADC or DAC channel for the
					board. It validates the input data for bounds, and tests for a channel already
		registered.
Inputs:	 iPosition - integer identifying the Industry Pack position on the MVME162
					iType - integer identifying if the channel is an ADC or a DAC
		iChannel - integer identifying the ADC/DAC channel on the Industry Pack
Precond:	the Industry Pack must be initialized with tip850Init.
Outputs:	TIP850_DEV - a pointer to the device structure for the registered channel. NULL
					is returned if the channel is already in use or the input data was invalid.
Postcond: The requested channel will be registered and initialized. ADC channels will
					be non-averaging
*/

TIP850_DEV * tip850PhysDevCreate(int iPosition, int iType, int iChannel) {
	int iCount;
	double dTemp;

	/* Check the type and that this channel is not already registered */

	switch (iType) {
		case TIP850_ADC:
			if ((iChannel < 0) || (iChannel >= TIP850_MAX_ADC) ||
					tip850Board[iPosition].ADC[iChannel].bRegistered) {
				return NULL;
			}
			break;

		case TIP850_DAC:
			if ((iChannel < 0) || (iChannel >= TIP850_MAX_DAC) ||
					tip850Board[iPosition].DAC[iChannel].bRegistered) {
				return NULL;
			}
			break;

		default:
			printf("Invalid TIP850 type\n");
			return NULL;
	}

	/* Register channel	*/
 
	if (iType == TIP850_ADC) {
		tip850Board[iPosition].ADC[iChannel].bRegistered = TRUE;
		tip850Board[iPosition].ADC[iChannel].iChannel = iChannel;
		tip850Board[iPosition].ADC[iChannel].iGain = 0;
		tip850Board[iPosition].ADC[iChannel].dSlope = 1.0;
		tip850Board[iPosition].ADC[iChannel].dOffset = 0.0;
		tip850Board[iPosition].ADC[iChannel].iAverage = 1;
		tip850Board[iPosition].ADC[iChannel].iBin = 0;

		for (iCount = 0; iCount < TIP850_ADC_BINS; iCount ++) {
			tip850Board[iPosition].ADC[iChannel].sLastData[iCount] = 0;
		}

		return	& tip850Board[iPosition].ADC[iChannel];
	}	else {
		tip850Board[iPosition].DAC[iChannel].bRegistered = TRUE;
		tip850Board[iPosition].DAC[iChannel].iChannel = iChannel;
		tip850Board[iPosition].DAC[iChannel].dSlope = 1.0;
		tip850Board[iPosition].DAC[iChannel].dOffset = 0.0;
		tip850Board[iPosition].ADC[iChannel].sLastData[0] = 0;

		/* Set DAC to 0.0 v */
		
		dTemp = 0.0;
		tip850Ioctl(&tip850Board[iPosition].DAC[iChannel], TIP850_WRITE, &dTemp);

		return & tip850Board[iPosition].DAC[iChannel];
	}
}


/*
tip850CreateAll(int iPosition)
Purpose:	Creates all the adc and dac channels on the Tip850 board. Use when the program
					needs all channels made or cannot selectively create them
Inputs:	 iPosition - integer identifying the Industry Pack position on the MVME162
Precond:	The Industry Pack must be initialized with tip850Init. 
Outputs:	None
Postcond: All channels are created using tip850PhysDevCreate.
*/

STATUS tip850CreateAll(char cPosition) {
	int iChannel;

	/* Determine location of IP specified in cPosition */

	cPosition -= 'A';
	if (cPosition > 3) {
		cPosition -= 0x20;
	}

	/* Is input data valid? Is this board already initialized? */

	if ((cPosition < 0) || (cPosition > 3)) {
		return TIP850_ERROR;
	}

	/* Make all DAC channels */

	for (iChannel = 0; iChannel < TIP850_MAX_DAC; iChannel ++) {
		tip850PhysDevCreate(cPosition, TIP850_DAC, iChannel);
	}

	/* Make all ADC channels */

	for (iChannel = 0; iChannel < TIP850_MAX_ADC; iChannel ++) {
		tip850PhysDevCreate(cPosition, TIP850_ADC, iChannel);
	}

	return TIP850_OK;
}


/*
tip850GetChannel(int iPosition, int iType, int iChannel)
Purpose:	Gets a pointer to the device structure for the specified adc or dac channel.
					Use this routine when the channel pointer cannot be specified
Inputs:	 iPosition - integer identifying the Industry Pack position on the MVME162
					iType - integer identifying if the channel is an ADC or a DAC
		iChannel - integer identifying the ADC/DAC channel on the Industry Pack
Precond:	the Industry Pack must be initialized with tip850Init. All the channels should be
					created before the use to avoid returning a NULL pointer.
Outputs:	TIP850_DEV - a pointer to the device structure for the registered channel. NULL
					is returned if the input data was invalid or the channel is not registered.
Postcond: None
*/

TIP850_DEV * tip850GetChannel(int iPosition, int iType, int iChannel) {
	if ((iType == TIP850_ADC) && (tip850Board[iPosition].ADC[iChannel].bRegistered == TRUE)) {
		return & tip850Board[iPosition].ADC[iChannel];
	} else if ((iType == TIP850_DAC) && (tip850Board[iPosition].DAC[iChannel].bRegistered == TRUE)) {
		return & tip850Board[iPosition].DAC[iChannel];
	}	else {
		return NULL;
	}
}


/*
tip850Ioctl(TIP850_DEV * tip850Device, int iFunction, double * dValue)
Purpose:	Used to access channels to change parameters and read data
Inputs:	 tipDevice - TIP850_DEV pointer to registered channel.
					iFunction - integer identifying which function to perform
		dValue - double pointer for storing return value
Precond:	The device must be created.
Outputs:	STATUS - indicates the result of the function. The value will depend on
					the function called.
Postcond: The contents of the passed double pointer may be changed by the routine
*/

STATUS tip850Ioctl(TIP850_DEV * tipDevice, int iFunction, double * dValue) {
	int iTemp;
	double dTemp;

	switch (iFunction) {
		case TIP850_GAIN: /* Write the new gain value */
			iTemp = (int) * dValue;
			tipDevice->iGain = iTemp;
			break;

		case TIP850_SLOPE: /* Write the new slope value */
			tipDevice->dSlope = * dValue;
			break;

		case TIP850_OFFSET: /* Write the new offset value */
			tipDevice->dOffset = * dValue;
			break;

		case TIP850_AVERAGE: /* Write the new average value */
			iTemp = (int) * dValue;

			if ((iTemp < 1) || (iTemp > TIP850_ADC_BINS)) {
				return TIP850_ERROR;
			}

			tipDevice->iAverage = (char) iTemp;
			break;

		case TIP850_WRITE: /* Write a new value to the DAC channel */
			if (tipDevice->iType != TIP850_DAC) {
				return TIP850_ERROR;
			}

			/* Write data to last location and to output port. Protect task until DAC update */

			taskLock();

			tipDevice->sLastData[0] = (short) ((* dValue - tipDevice->dOffset) / tipDevice->dSlope);
			* tipDevice->Board->psDAC_DAT[tipDevice->iChannel] = tipDevice->sLastData[0];

			/* Write to DAC LOAD port */

			* tipDevice->Board->psDAC_LOA = TRUE;

			taskUnlock();

			break;

		case TIP850_READ: /* Read DAC last write or current ADC value with averaging */
			if (tipDevice->iType == TIP850_DAC) {
				* dValue = (double) tipDevice->sLastData[0] * tipDevice->dSlope + tipDevice->dOffset;
			} else {
				dTemp = 0.0;
				for (iTemp = 0; iTemp < tipDevice->iAverage; iTemp ++) {
					dTemp += (double) tipDevice->sLastData[iTemp];
				}
				dTemp /= (double) tipDevice->iAverage;
				*dValue = dTemp;
			}
			break;

		case TIP850_READ_RAW: /* Read binary DAC or first ADC value - no average */
			* dValue = (double) tipDevice->sLastData[0];
			break;

	}
	return TIP850_OK;
}

/*
tip850Show()
Purpose: Shows all the created DAC and ADC channels in the system
Inputs:	None
Precond: None
Outputs: All registered channels are displayed
Postcond:None
*/

void tip850Show() {
	int iBoard;
	int iChannel;
	int iIndex;

	for (iBoard = 0; iBoard < TIP850_MAX_BOARD; iBoard ++) {
		if (tip850Board[iBoard].bRegistered) {
			for (iChannel = 0; iChannel < TIP850_MAX_ADC; iChannel ++) {
				if (tip850Board[iBoard].ADC[iChannel].bRegistered) {
					printf("IP %c %s Channel %d Gain %d Slope %lf Offset %lf Avg %d Data ",
								 (char) iBoard + 'A', 
								 tip850Board[iBoard].ADC[iChannel].iType == TIP850_ADC ? "ADC" : "DAC",
								 tip850Board[iBoard].ADC[iChannel].iChannel,
								 tip850Board[iBoard].ADC[iChannel].iGain,
								 tip850Board[iBoard].ADC[iChannel].dSlope,
								 tip850Board[iBoard].ADC[iChannel].dOffset,
								 tip850Board[iBoard].ADC[iChannel].iAverage);
				}
			}
			for (iIndex = 0; iIndex < tip850Board[iBoard].ADC[iChannel].iAverage; iIndex++) {
				printf("%d	", tip850Board[iBoard].ADC[iChannel].sLastData[iIndex]);
			}
		}
		printf("\n");
	}

	for (iChannel = 0; iChannel < TIP850_MAX_DAC; iChannel ++) {
		if (tip850Board[iBoard].DAC[iChannel].bRegistered) {
			printf("IP %c %s Channel %d Data %d Slope %lf Offset %lf\n",
						 (char) iBoard + 'A', 
						 tip850Board[iBoard].DAC[iChannel].iType == TIP850_ADC ? "ADC" : "DAC",
						 tip850Board[iBoard].DAC[iChannel].iChannel,
						 tip850Board[iBoard].DAC[iChannel].sLastData[0],
						 tip850Board[iBoard].DAC[iChannel].dSlope,
						 tip850Board[iBoard].DAC[iChannel].dOffset);
		}
	}
}


/*
tip850Scan(TIP850_BOARD * tip850Board)
Purpose:	This task loops through all the created ADC channels and collects data
					for them. It triggers the collection of data for the current channel.
		The ADC complete signal generates an interrupt, which will give the 
		semaphore taken after the conversion is started. The data for each
		channel is stored in the LastData array. If there is no end of collection
		interrupt, the task will ignore data and try again.
Inputs:	 tip850Board - TIP850_BOARD pointer identifying which board is to be scanned. Each
					board would have its own scan task.
Precond:	The IP must be successfully registered.
Outputs:	None
Postcond: The task will run forever.
*/

void tip850Scan(TIP850_BOARD * tip850Board) {
	short iChannel;
	static short iDifferential = 0x1;
	static short iGainSettle = 0x1;
	static short iInterrupt = 0x1;
	short sCmdStatus;
	STATUS sResult = OK;
	short iCount;

	char cIGP;
	char cICR0;
	char cICR1;
	char cVector;
	char cPosition = 3;

	/* wait for ints to get set up */

	taskDelay(120);

	printf("Starting Tip850 Scan Task\n");

	while (1) {
		for (iChannel = 0; iChannel < TIP850_MAX_ADC; iChannel ++) {
			if (tip850Board->ADC[iChannel].bRegistered) {
				iCount = 1;
				do {					 /* Try five times to get conversion with interrupt working */
					sCmdStatus = iChannel | 
					(iDifferential << 4) | 
					(tip850Board->ADC[iChannel].iGain << 5) |
					(iGainSettle << 7) |
					(iInterrupt << 8);

					/* Write command to set up collection */
		
					* (tip850Board->psADC_CSR) = sCmdStatus;

					/* Dummy write to start conversion */

					/*		* (tip850Board->psADC_CON) = 0xff; /* dummy write to trigger ADC */
		
					/* wait for interrupt to give semaphore, timeout after 1 clock tick */

					taskDelay(1);

					/*		sResult = semTake(sidTip850ADCComplete, 2); */

					iCount --;
				} while ((sResult == ERROR) && (iCount > 0));


				sCmdStatus = *(tip850Board->psADC_CSR);

				if (sCmdStatus & 0xc000) {
					sResult = ERROR;
				}

				if (sResult == ERROR)	{
					printf("Ch %d Cmd %x Stat %x ", iChannel, sCmdStatus, *(tip850Board->psADC_CSR));

					printf("Vector %x ",	*(tip850Board->psVector));

					cIGP = *((unsigned char *) (MVME162_IPIC_ADDRESS + 0x18 + cPosition));
					printf("IGP %x	", cIGP);

					cICR0 = *((unsigned char *) (MVME162_IPIC_ADDRESS + 0x10 + cPosition * 2));
					cICR1 = *((unsigned char *) (MVME162_IPIC_ADDRESS + 0x11 + cPosition * 2));
					printf("ICR0 %x ICR1 %x	", cICR0, cICR1);

					printf("IntLoclLev %d	", intLockLevelGet());

					printf("IntCount %d\n", intCount());
				}	else {	/* Get data and store for current channel */

					tip850Board->ADC[iChannel].sLastData[tip850Board->ADC[iChannel].iBin] = * (tip850Board->psADC_DAT);

					/*		printf("Chan %d Data %d\n", iChannel,	* (tip850Board->psADC_DAT));*/

					/* Increment bin counter for averaged ADC channels */

					tip850Board->ADC[iChannel].iBin ++;
					if (tip850Board->ADC[iChannel].iBin >= tip850Board->ADC[iChannel].iAverage) {
						tip850Board->ADC[iChannel].iBin = 0;
					}
				}
			}
		}
	}
}


/*
tip850IntADC()
Purpose:	This interrupt function gives the semaphore that is blocking the scan task.
Inputs:	 None
Precond:	The semaphore must be created empty. The interrupt system must be running.
Outputs:	None
Postcond: The semaphore is given and the scan task will proceed.
*/

void tip850IntADC() {
	semGive(sidTip850ADCComplete);
}


void tip850Test(int iPosition, double dValue) {
	TIP850_DEV * pDevice;
	int iChannel;

	for (iChannel = 0; iChannel < 4; iChannel++) {
		pDevice = tip850GetChannel(iPosition, TIP850_DAC, iChannel);
		if (pDevice == NULL) {
			return;
		}
		tip850Ioctl(pDevice, TIP850_WRITE, &dValue);
	}
}
