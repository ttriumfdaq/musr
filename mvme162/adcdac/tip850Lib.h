/*
 *  $Id: tip850Lib.h,v 1.2 2000/12/22 21:20:41 David.Morris Exp $
 * 
 *  $Revision: 1.2 $
 *                                                                                     
 *  Purpose: This module contains the definition of the TEWS Datenteknik TIP-850
 *  DAC/ADC board driver for VxWorks
 *
 *  $Log: tip850Lib.h,v $
 *  Revision 1.2  2000/12/22 21:20:41  David.Morris
 *  Added CVS tags. Cleaned up braces through code
 *
 *
 */

#ifndef TIP850_H
#define TIP850_H

#define TIP850_MAX_BOARD 4
#define TIP850_MAX_ADC 8
#define TIP850_MAX_DAC 4
#define TIP850_ADC_BUSY 0xC000
#define TIP850_ADC_BINS 16

#define MVME162_IPBaseAddress 0xfff58000
#define MVME162_IPIC_ADDRESS  0xfffbc000

enum
{
  TIP850_OK,
  TIP850_ERROR,
  TIP850_GAIN,
  TIP850_SLOPE,
  TIP850_OFFSET,
  TIP850_WRITE,
  TIP850_READ,
  TIP850_READ_RAW,
  TIP850_AVERAGE,
  TIP850_ADC,
  TIP850_DAC
};

typedef struct tagTIP850_DEV
{
  struct tagTIP850_BOARD * Board;
  char iChannel;
  char bRegistered;
  char iType;
  char iGain;

  char iAverage;
  char iBin;

  double dSlope;
  double dOffset;
  short sLastData[TIP850_ADC_BINS];
} TIP850_DEV;

typedef struct tagTIP850_BOARD
{
  char bRegistered;
  char iPosition;

  short * psADC_CSR;
  short * psADC_CON;
  short * psADC_DAT;
  short * psDAC_DAT[4];
  short * psDAC_LOA;
  short * psVector;
  short * psIDAddress;

  struct tagTIP850_DEV ADC[TIP850_MAX_ADC];
  struct tagTIP850_DEV DAC[TIP850_MAX_DAC];
} TIP850_BOARD;

#endif

STATUS tip850Init(char cPosition, short sVector);
STATUS tip850CreateAll(char cPosition);
TIP850_DEV * tip850PhysDevCreate(int iBoard, int iType, int iChannel);
TIP850_DEV * tip850GetChannel(int iPosition, int iType, int iChannel);
STATUS tip850Ioctl(TIP850_DEV * tip850Device, int iFunction, double * dValue);
void tip850Show();
void tip850Scan(TIP850_BOARD * tip850Board);
void tip850IntADC();


