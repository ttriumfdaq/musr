#include <rpc/types.h>  // needed for cross-compile for vxworks under linux
#include <time.h>
#ifdef VMS
#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_ 1
#include <sys/time.h>
#endif /* !_SYS_TIME_H_ */
#endif /* VMS */

struct CA_BCNA {
	long b;
	long c;
	long n;
	long a;
};
typedef struct CA_BCNA CA_BCNA;
bool_t xdr_CA_BCNA();


struct CA_FXD {
	long f;
	long ext;
	long dat;
};
typedef struct CA_FXD CA_FXD;
bool_t xdr_CA_FXD();


struct CA_DQ {
	long dat;
	char q;
};
typedef struct CA_DQ CA_DQ;
bool_t xdr_CA_DQ();


struct CA_XL {
	long ext;
	char l;
};
typedef struct CA_XL CA_XL;
bool_t xdr_CA_XL();


struct CA_LL {
	long lam;
	char l;
};
typedef struct CA_LL CA_LL;
bool_t xdr_CA_LL();


struct CA_CB {
	long count;
	long tally;
	long lam_ident;
	long channel;
};
typedef struct CA_CB CA_CB;
bool_t xdr_CA_CB();


struct CFBLOCK_ARG {
	long f;
	long ext;
	struct {
		u_int pData_len;
		long *pData_val;
	} pData;
	CA_CB cb;
};
typedef struct CFBLOCK_ARG CFBLOCK_ARG;
bool_t xdr_CFBLOCK_ARG();


struct CFBLOCK_RES {
	struct {
		u_int pData_len;
		long *pData_val;
	} pData;
	CA_CB cb;
};
typedef struct CFBLOCK_RES CFBLOCK_RES;
bool_t xdr_CFBLOCK_RES();


struct CSBLOCK_ARG {
	long f;
	long ext;
	struct {
		u_int pData_len;
		short *pData_val;
	} pData;
	CA_CB cb;
};
typedef struct CSBLOCK_ARG CSBLOCK_ARG;
bool_t xdr_CSBLOCK_ARG();


struct CSBLOCK_RES {
	struct {
		u_int pData_len;
		short *pData_val;
	} pData;
	CA_CB cb;
};
typedef struct CSBLOCK_RES CSBLOCK_RES;
bool_t xdr_CSBLOCK_RES();


struct CFLIST_ARG {
	struct {
		u_int fa_len;
		long *fa_val;
	} fa;
	struct {
		u_int exta_len;
		long *exta_val;
	} exta;
	struct {
		u_int intc_len;
		long *intc_val;
	} intc;
	CA_CB cb;
};
typedef struct CFLIST_ARG CFLIST_ARG;
bool_t xdr_CFLIST_ARG();


struct CFLIST_RES {
	struct {
		u_int intc_len;
		long *intc_val;
	} intc;
	struct {
		u_int qa_len;
		long *qa_val;
	} qa;
	CA_CB cb;
};
typedef struct CFLIST_RES CFLIST_RES;
bool_t xdr_CFLIST_RES();


struct CSLIST_ARG {
	struct {
		u_int fa_len;
		long *fa_val;
	} fa;
	struct {
		u_int exta_len;
		long *exta_val;
	} exta;
	struct {
		u_int intt_len;
		short *intt_val;
	} intt;
	CA_CB cb;
};
typedef struct CSLIST_ARG CSLIST_ARG;
bool_t xdr_CSLIST_ARG();


struct CSLIST_RES {
	struct {
		u_int intt_len;
		short *intt_val;
	} intt;
	struct {
		u_int qa_len;
		long *qa_val;
	} qa;
	CA_CB cb;
};
typedef struct CSLIST_RES CSLIST_RES;
bool_t xdr_CSLIST_RES();


#define CASRVR ((u_long)0x2c0da009)
#define CAVERS ((u_long)1)
#define CA_RPCCDREG ((u_long)1)
extern long *ca_rpccdreg_1();
#define CA_RPCCFSA ((u_long)2)
extern CA_DQ *ca_rpccfsa_1();
#define CA_RPCCCCZ ((u_long)3)
extern void *ca_rpccccz_1();
#define CA_RPCCCCC ((u_long)4)
extern void *ca_rpccccc_1();
#define CA_RPCCCCI ((u_long)5)
extern void *ca_rpcccci_1();
#define CA_RPCCTCI ((u_long)6)
extern char *ca_rpcctci_1();
#define CA_RPCCCCD ((u_long)7)
extern void *ca_rpccccd_1();
#define CA_RPCCTCD ((u_long)8)
extern char *ca_rpcctcd_1();
#define CA_RPCCTGL ((u_long)9)
extern char *ca_rpcctgl_1();
#define CA_RPCCCINIT ((u_long)15)
extern void *ca_rpcccinit_1();
#define CA_RPCCTSTAT ((u_long)16)
extern long *ca_rpcctstat_1();
#define CA_RPCCFMAD ((u_long)17)
extern CFBLOCK_RES *ca_rpccfmad_1();
#define CA_RPCCFUBC ((u_long)18)
extern CFBLOCK_RES *ca_rpccfubc_1();
#define CA_RPCCFUBR ((u_long)19)
extern CFBLOCK_RES *ca_rpccfubr_1();
#define CA_RPCCSSA ((u_long)20)
extern CA_DQ *ca_rpccssa_1();
#define CA_RPCCSMAD ((u_long)21)
extern CSBLOCK_RES *ca_rpccsmad_1();
#define CA_RPCCSUBC ((u_long)22)
extern CSBLOCK_RES *ca_rpccsubc_1();
#define CA_RPCCSUBR ((u_long)23)
extern CSBLOCK_RES *ca_rpccsubr_1();
#define CA_RPCCFGA ((u_long)24)
extern CFLIST_RES *ca_rpccfga_1();
#define CA_RPCCSGA ((u_long)25)
extern CSLIST_RES *ca_rpccsga_1();

