
%#include <time.h>
%#ifdef VMS
%#ifndef _SYS_TIME_H_
%#define _SYS_TIME_H_ 1
%#include <sys/time.h>
%#endif /* !_SYS_TIME_H_ */
%#endif /* VMS */

struct CA_BCNA {
    long b;
    long c;
    long n;
    long a;
};
struct CA_FXD {
    long f;
    long ext;
    long dat;
};
struct CA_DQ {
    long dat;
    char q;
};
struct CA_XL {
    long ext;
    char l;
};
struct CA_LL {
    long lam;
    char l;
};
struct CA_CB {
    long count;
    long tally;
    long lam_ident;
    long channel;
};
struct CFBLOCK_ARG {
    long f;
    long ext;
    long pData<>;
    CA_CB cb;
};
struct CFBLOCK_RES {
    long pData<>;
    CA_CB cb;
};
struct CSBLOCK_ARG {
    long f;
    long ext;
    short pData<>;
    CA_CB cb;
};
struct CSBLOCK_RES {
    short pData<>;
    CA_CB cb;
};
struct CFLIST_ARG {
    long fa<>;
    long exta<>;
    long intc<>;
    CA_CB cb;
};
struct CFLIST_RES {
    long intc<>;
    long qa<>;
    CA_CB cb;
};
struct CSLIST_ARG {
    long fa<>;
    long exta<>;
    short intt<>;
    CA_CB cb;
};
struct CSLIST_RES {
    short intt<>;
    long qa<>;
    CA_CB cb;
};

program CASRVR {
  version CAVERS {
    long  CA_RPCCDREG( CA_BCNA )=1;
    CA_DQ CA_RPCCFSA( CA_FXD )=2;
    void  CA_RPCCCCZ( long )=3;
    void  CA_RPCCCCC( long )=4;
    void  CA_RPCCCCI( CA_XL )=5;
    char  CA_RPCCTCI( long )=6;
    void  CA_RPCCCCD( CA_XL )=7;
    char  CA_RPCCTCD( long )=8;
    char  CA_RPCCTGL( long )=9;
/*
    int CA_RPCCDLAM(CA_BCNA)=10;
    int CA_RPCCCLM(CA_LL)=11;
    int CA_RPCCCLC(long)=12;
    int CA_RPCCTLM(long)=13;
    int CA_RPCCCLWT(long)=14;
*/
    void  CA_RPCCCINIT( long )=15;
    long CA_RPCCTSTAT( void )=16;
    CFBLOCK_RES CA_RPCCFMAD( CFBLOCK_ARG )=17;
    CFBLOCK_RES CA_RPCCFUBC( CFBLOCK_ARG )=18;
    CFBLOCK_RES CA_RPCCFUBR( CFBLOCK_ARG )=19;
    CA_DQ       CA_RPCCSSA(  CA_FXD )=20;
    CSBLOCK_RES CA_RPCCSMAD( CSBLOCK_ARG )=21;
    CSBLOCK_RES CA_RPCCSUBC( CSBLOCK_ARG )=22;
    CSBLOCK_RES CA_RPCCSUBR( CSBLOCK_ARG )=23;
    CFLIST_RES  CA_RPCCFGA( CFLIST_ARG )=24;
    CSLIST_RES  CA_RPCCSGA( CSLIST_ARG )=25;
  } = 1;
} = 0x2c0da009;