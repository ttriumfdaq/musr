
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <rpc/rpc.h>
#include "camacRpc.h"
#include "camacLib.h"

CLIENT *caHandle;
char caServer[128];
int caOutput = 1;

#define CAMAC_CONNECT_TIMEOUT 1

#define _doRPC( proc ) \
  while( proc == NULL ) \
  { \
    time_t rpct; \
    time(&rpct); \
    fprintf( stderr, "CAMAC RPC failure at time %s\n", ctime(&rpct) ); \
    caclose(); \
    while( !caopen( caServer ) ) {caOutput=0;sleep( CAMAC_CONNECT_TIMEOUT );}\
    caOutput = 1; \
    time(&rpct); \
    fprintf( stderr, "CAMAC RPC reconnected at time %s\n", ctime(&rpct) ); \
  }

#ifdef VMS

/*
 *  Access to CAMIF common block
 */
#include "cfortran.h"
typedef struct {
    short ierr;
    short ierr1;
} ERRCOM_DEF;
#define Errcom COMMON_BLOCK( ERRCOM, errcom )
COMMON_BLOCK_DEF( ERRCOM_DEF, Errcom );
ERRCOM_DEF Errcom;  /* set aside storage */

/*
 *  These two routines are meant solely for
 *  compatibility with Dave Maden's 
 *  CAMIF package.
 */
#include <descrip.h>
#include <lnmdef.h>
int
camif( long* dummy )
{
  int status;
  char targethost[128];
  unsigned short ret_len;
  struct {
    unsigned short eqv_len;
    unsigned short item_code;
    int eqv_address;
    int ret_len_address;
    int terminator;
  } itemlist = { 128, LNM$_STRING, targethost, &ret_len, 0 };
  $DESCRIPTOR( table, "LNM$DCL_LOGICAL" );
  $DESCRIPTOR( lognam, "CAMAC_RPC_HOST" );

  /*
   *  Get the hostname
   */
  status = sys$trnlnm( 0, &table, &lognam, 0, &itemlist );
  if( !( status & 0x1 ) ) return( 0 );

  /*
   *  Null-terminate
   */
  targethost[ret_len] = '\0';

  if( !caopen( targethost ) ) 
    {
      Errcom.ierr = 4;    /* status of call - error */
      Errcom.ierr1 = 10;  /* type of OS/hardware */
      return( 0 );
    }

  Errcom.ierr = 0;    /* status of call */
  Errcom.ierr1 = 10;  /* type of OS/hardware */
  return( 1 );
}

int
ctrap( void )
{
  return( 1 );
}
#endif /* VMS */

int
caopen( char* server )
{
  struct timeval caTime = { 25, 0 };  /* standard 25 sec timeout */

  caHandle = clnt_create( server, CASRVR, CAVERS, "tcp" );
  if( caHandle == NULL ) 
  {
    if( caOutput ) clnt_pcreateerror( server );
    return( 0 );
  }

  strcpy( caServer, server );
  clnt_control( caHandle, CLSET_TIMEOUT, (char*)&caTime );

  return( 1 );
}

int
caclose( void )
{
  if( caHandle != NULL )
    {
      clnt_destroy( caHandle );
      caHandle = NULL;
    }
  return( 1 );
}

int
ccinit( long* pBranch )
{
  _doRPC( ca_rpcccinit_1( pBranch, caHandle ) )
  return( 1 );
}

int
ctstat( long* pStatus )
{
  _doRPC( ca_rpcctstat_1( pStatus, caHandle ) )
  return( 1 );
}

int 
cccz( long* pext )
{
  _doRPC( ca_rpccccz_1( (long*)pext, caHandle ) )
  return( 1 );
}

int 
cccc( long* pext )
{
  _doRPC( ca_rpccccc_1( (long*)pext, caHandle ) )
  return( 1 );
}

int 
ccci( long* pext, char* pl )
{
  CA_XL arg;
 
  bzero( &arg, sizeof( arg ) );

  bcopy( pext, &arg.ext, sizeof( long ) );
  arg.l = *pl;

  _doRPC( ca_rpcccci_1( &arg, caHandle ) )

  return( 1 );
}

int 
ctci( long* pext, char* pl )
{
  char *r;

  _doRPC( ( r = ca_rpcctci_1( (long*)pext, caHandle ) ) )

  *pl = r ? *r : 0;		/* return 0 on any error */
  if( r == NULL ) return( 0 );

  return( 1 );
}

int 
cccd( long* pext, char* pl )
{
  CA_XL arg;

  bzero( &arg, sizeof( arg ) );

  bcopy( pext, &arg.ext, sizeof( long ) );
  arg.l = *pl;

  _doRPC( ca_rpccccd_1( &arg, caHandle ) )

  return( 1 );
}

int 
ctcd( long* pext, char* pl )
{
  char *r;
  _doRPC( ( r = ca_rpcctcd_1( (long*)pext, caHandle ) ) )
  *pl = r ? *r : 0;
  if( r == NULL ) return( 0 );
  return( 1 );
}

int 
ctgl( long* pext, char* pl )
{
  char *r;
  _doRPC( ( r = ca_rpcctgl_1( (long*)pext, caHandle ) ) )
  *pl = r ? *r : 0;
  if( r == NULL ) return( 0 );
  return( 1 );
}

int 
cdreg( long* pext, long* pb, long* pc, long* pn, long* pa )
{
  CA_BCNA arg;
  long* r;

  bzero( &arg, sizeof( arg ) );

  arg.b = *pb;
  arg.c = *pc;
  arg.n = *pn;
  arg.a = *pa;
  _doRPC( ( r = ca_rpccdreg_1( &arg, caHandle ) ) )
  if( r == NULL )
    {
      bzero( pext, sizeof( long ) );
      return( 0 );
    }
  bcopy( r, pext, sizeof( long ) );
  return( 1 );
}

int 
cfsa( long* pF, long* pext, long* pdata, char* pq )
{
  CA_FXD arg;
  CA_DQ *r;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pext, &arg.ext, sizeof( long ) );

  if( ( *pF >= 16 ) && ( *pF <= 23 ) ) arg.dat = *pdata;

  _doRPC( ( r = ca_rpccfsa_1( &arg, caHandle ) ) )

  if( *pF < 8 ) *pdata = r->dat;
  *pq = r->q;

  return( 1 );
}

int
cfmad( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
  CFBLOCK_RES* pRes;
  CFBLOCK_ARG arg;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pExt, &arg.ext, sizeof( long ) );
  if( ( *pF >= 16 ) && ( *pF <= 23 ) )
    {
      arg.pData.pData_len = pCb->count;
      arg.pData.pData_val = pData;
    }
  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccfmad_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );

  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  if( *pF < 8 )
    {
      bcopy( pRes->pData.pData_val, pData, pRes->pData.pData_len*sizeof( long ) );
    }
  xdr_free( (xdrproc_t)xdr_CFBLOCK_RES, (char *)pRes );

  return( 1 );
}

int
cfubc( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
  CFBLOCK_RES* pRes;
  CFBLOCK_ARG arg;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pExt, &arg.ext, sizeof( long ) );
  if( ( *pF >= 16 ) && ( *pF <= 23 ) )
    {
      arg.pData.pData_len = pCb->count;
      arg.pData.pData_val = pData;
    }
  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccfubc_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );

  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  if( *pF < 8 )
    {
      bcopy( pRes->pData.pData_val, pData, pRes->pData.pData_len*sizeof( long ) );
    }
  xdr_free( (xdrproc_t)xdr_CFBLOCK_RES, (char *)pRes );

  return( 1 );
}

int
cfubr( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
  CFBLOCK_RES* pRes;
  CFBLOCK_ARG arg;
  int i;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pExt, &arg.ext, sizeof( long ) );
  if( ( *pF >= 16 ) && ( *pF <= 23 ) )
    {
      arg.pData.pData_len = pCb->count;
      arg.pData.pData_val = pData;
    }
  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccfubr_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );
  
  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  if( *pF < 8 )
    {
      bcopy( pRes->pData.pData_val, pData, pRes->pData.pData_len*sizeof( long ) );
    }
  xdr_free( (xdrproc_t)xdr_CFBLOCK_RES, (char *)pRes );

  return( 1 );
}

int 
cssa( long* pF, long* pext, short* pdata, char* pq )
{
  CA_FXD arg;
  CA_DQ *r;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pext, &arg.ext, sizeof( long ) );

  if( ( *pF >= 16 ) && ( *pF <= 23 ) ) arg.dat = *pdata;

  _doRPC( ( r = ca_rpccssa_1( &arg, caHandle ) ) )
  
  if( *pF < 8 ) *pdata = r->dat;
  *pq = r->q;

  return( 1 );
}

int
csmad( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
  CSBLOCK_RES* pRes;
  CSBLOCK_ARG arg;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pExt, &arg.ext, sizeof( long ) );
  if( ( *pF >= 16 ) && ( *pF <= 23 ) )
    {
      arg.pData.pData_len = pCb->count;
      arg.pData.pData_val = pData;
    }
  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccsmad_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );
  
  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  if( *pF < 8 )
    {
      bcopy( pRes->pData.pData_val, pData, pRes->pData.pData_len*sizeof( short ) );
    }
  xdr_free( (xdrproc_t)xdr_CSBLOCK_RES, (char *)pRes );

  return( 1 );
}

int
csubc( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
  CSBLOCK_RES* pRes;
  CSBLOCK_ARG arg;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pExt, &arg.ext, sizeof( long ) );
  if( ( *pF >= 16 ) && ( *pF <= 23 ) )
    {
      arg.pData.pData_len = pCb->count;
      arg.pData.pData_val = pData;
    }
  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccsubc_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );
  
  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  if( *pF < 8 )
    {
      bcopy( pRes->pData.pData_val, pData, pRes->pData.pData_len*sizeof( short ) );
    }
  xdr_free( (xdrproc_t)xdr_CSBLOCK_RES, (char *)pRes );

  return( 1 );
}

int
csubr( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
  CSBLOCK_RES* pRes;
  CSBLOCK_ARG arg;

  bzero( &arg, sizeof( arg ) );

  arg.f = *pF;
  bcopy( pExt, &arg.ext, sizeof( long ) );
  if( ( *pF >= 16 ) && ( *pF <= 23 ) )
    {
      arg.pData.pData_len = pCb->count;
      arg.pData.pData_val = pData;
    }
  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccsubr_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );
  
  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  if( *pF < 8 )
    {
      bcopy( pRes->pData.pData_val, pData, pRes->pData.pData_len*sizeof( short ) );
    }
  xdr_free( (xdrproc_t)xdr_CSBLOCK_RES, (char *)pRes );

  return( 1 );
}

int
cfga( long* fa, long* exta, long* intc, long* qa, CAMAC_CB* pCb )
{
  CFLIST_ARG arg;
  CFLIST_RES* pRes;
  int count;

  bzero( &arg, sizeof( arg ) );

  count = pCb->count;

  arg.fa.fa_val = fa;
  arg.fa.fa_len = count;

  arg.exta.exta_val = exta;
  arg.exta.exta_len = count;

  arg.intc.intc_val = intc;
  arg.intc.intc_len = count;

  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccfga_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );
  
  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  bcopy( pRes->intc.intc_val, intc, count*sizeof( long ) );
  bcopy( pRes->qa.qa_val, qa, count*sizeof( short ) );
  xdr_free( (xdrproc_t)xdr_CFLIST_RES, (char *)pRes );

  return( 1 );
}

int
csga( long* fa, long* exta, short* intt, long* qa, CAMAC_CB* pCb )
{
  CSLIST_ARG arg;
  CSLIST_RES* pRes;
  int count;

  bzero( &arg, sizeof( arg ) );

  count = pCb->count;

  arg.fa.fa_val = fa;
  arg.fa.fa_len = count;

  arg.exta.exta_val = exta;
  arg.exta.exta_len = count;

  arg.intt.intt_val = intt;
  arg.intt.intt_len = count;

  bcopy( pCb, &arg.cb, sizeof( CAMAC_CB ) );

  _doRPC( ( pRes = ca_rpccsga_1( &arg, caHandle ) ) )
  if( pRes == NULL ) return( 0 );

  bcopy( &pRes->cb, pCb, sizeof( CAMAC_CB ) );
  bcopy( pRes->intt.intt_val, intt, count*sizeof( long ) );
  bcopy( pRes->qa.qa_val, qa, count*sizeof( short ) );
  xdr_free( (xdrproc_t)xdr_CSLIST_RES, (char *)pRes );

  return( 1 );
}

