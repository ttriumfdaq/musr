
#include <stdio.h>
#include <rpc/rpc.h>
#include "camacRpc.h"
#include "camacLib.h"
#include "vxWorks.h"

void rpcTaskInit();

void 
localServerInit( void )
{
  rpcTaskInit();
}

void*
ca_rpcccinit_1( long* pBranch )
{
  ccinit( pBranch );
}

long*
ca_rpcctstat_1( void )
{
  static long status;
  ctstat( &status );
  return( &status );
}

void*
ca_rpccccz_1( long* ext )
{
  cccz( ext );
}

void*
ca_rpccccc_1( long* ext )
{
  cccc( ext );
}

void*
ca_rpcccci_1( CA_XL* arg )
{
  ccci( &arg->ext, &arg->l );
}

char*
ca_rpcctci_1( long* ext )
{
  static char l;
  ctci( ext , &l );
  return( &l );
}

void*
ca_rpccccd_1( CA_XL* arg )
{
  cccd( &arg->ext, &arg->l );
}

char*
ca_rpcctcd_1( long* ext )
{
  static char l;
  ctcd( ext , &l );
  return( &l );
}

char*
ca_rpcctgl_1( long* ext )
{
  static char l;
  ctgl( ext , &l );
  return( &l );
}

long*
ca_rpccdreg_1( CA_BCNA* arg )
{
  static long res;

  bzero( &res, sizeof( res ) );

  cdreg( &res, &arg->b, &arg->c, &arg->n, &arg->a );

  return( &res );
}

CA_DQ*
ca_rpccfsa_1( CA_FXD *arg )
{
  static CA_DQ o;

  cfsa( &arg->f, &arg->ext, &(arg->dat), &o.q );
  o.dat=arg->dat;

  return( &o );
}

CFBLOCK_RES*
ca_rpccfmad_1( CFBLOCK_ARG* arg )
{
  static CFBLOCK_RES res = { 0, 0 };
  long* pData;

  if( res.pData.pData_val != NULL ) free( res.pData.pData_val );
  bzero( &res, sizeof( res ) );

  if( arg->f < 8 )
    {
      res.pData.pData_val = (long*)malloc( arg->cb.count*sizeof( long ) );
      pData = res.pData.pData_val;
    }
  else
    {
      pData = arg->pData.pData_val;
    }
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );
  cfmad( &arg->f, &arg->ext, pData, (CAMAC_CB*)&res.cb );
  if( arg->f < 8 )
    {
      res.pData.pData_len = res.cb.tally;
    }
  return( &res );
}

CFBLOCK_RES*
ca_rpccfubc_1( CFBLOCK_ARG* arg )
{
  static CFBLOCK_RES res = { 0, 0 };
  long* pData;

  if( res.pData.pData_val != NULL ) free( res.pData.pData_val );
  bzero( &res, sizeof( res ) );

  if( arg->f < 8 )
    {
      res.pData.pData_val = (long*)malloc( arg->cb.count*sizeof( long ) );
      pData = res.pData.pData_val;
    }
  else
    {
      pData = arg->pData.pData_val;
    }
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );
  cfubc( &arg->f, &arg->ext, pData, (CAMAC_CB*)&res.cb );
  if( arg->f < 8 )
    {
      res.pData.pData_len = res.cb.tally;
    }
  return( &res );
}

CFBLOCK_RES*
ca_rpccfubr_1( CFBLOCK_ARG* arg )
{
  static CFBLOCK_RES res = { 0, 0 };
  long* pData;

  if( res.pData.pData_val != NULL ) free( res.pData.pData_val );
  bzero( &res, sizeof( res ) );

  if( arg->f < 8 )
    {
      res.pData.pData_val = (long*)malloc( arg->cb.count*sizeof( long ) );
      pData = res.pData.pData_val;
    }
  else
    {
      pData = arg->pData.pData_val;
    }
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );
  cfubr( &arg->f, &arg->ext, pData, (CAMAC_CB*)&res.cb );
  if( arg->f < 8 )
    {
      res.pData.pData_len = res.cb.tally;
    }
  return( &res );
}

CA_DQ *
ca_rpccssa_1( CA_FXD *arg )
{
  static CA_DQ o;
  short data = (short)arg->dat;

  cssa( &arg->f, &arg->ext, &data, &o.q );
  o.dat = data;

  return( &o );
}

CSBLOCK_RES*
ca_rpccsmad_1( CSBLOCK_ARG* arg )
{
  static CSBLOCK_RES res = { 0, 0 };
  short* pData;

  if( res.pData.pData_val != NULL ) free( res.pData.pData_val );
  bzero( &res, sizeof( res ) );

  if( arg->f < 8 )
    {
      res.pData.pData_val = (short*)malloc( arg->cb.count*sizeof( short ) );
      pData = res.pData.pData_val;
    }
  else
    {
      pData = arg->pData.pData_val;
    }
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );
  csmad( &arg->f, &arg->ext, pData, (CAMAC_CB*)&res.cb );
  if( arg->f < 8 )
    {
      res.pData.pData_len = res.cb.tally;
    }
  return( &res );
}

CSBLOCK_RES*
ca_rpccsubc_1( CSBLOCK_ARG* arg )
{
  static CSBLOCK_RES res = { 0, 0 };
  short* pData;

  if( res.pData.pData_val != NULL ) free( res.pData.pData_val );
  bzero( &res, sizeof( res ) );

  if( arg->f < 8 )
    {
      res.pData.pData_val = (short*)malloc( arg->cb.count*sizeof( short ) );
      pData = res.pData.pData_val;
    }
  else
    {
      pData = arg->pData.pData_val;
    }
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );
  csubc( &arg->f, &arg->ext, pData, (CAMAC_CB*)&res.cb );
  if( arg->f < 8 )
    {
      res.pData.pData_len = res.cb.tally;
    }
  return( &res );
}

CSBLOCK_RES*
ca_rpccsubr_1( CSBLOCK_ARG* arg )
{
  static CSBLOCK_RES res = { 0, 0 };
  short* pData;

  if( res.pData.pData_val != NULL ) free( res.pData.pData_val );
  bzero( &res, sizeof( res ) );

  if( arg->f < 8 )
    {
      res.pData.pData_val = (short*)malloc( arg->cb.count*sizeof( short ) );
      pData = res.pData.pData_val;
    }
  else
    {
      pData = arg->pData.pData_val;
    }
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );

  csubr( &arg->f, &arg->ext, pData, (CAMAC_CB*)&res.cb );

  if( arg->f < 8 )
    {
      res.pData.pData_len = res.cb.tally;
    }
  else
    {
      res.pData.pData_len = 0;
    }
  return( &res );
}

CFLIST_RES*
ca_rpccfga_1( CFLIST_ARG* arg )
{
  static CFLIST_RES res = { 0, 0, 0 };
  int count;
  int i;
  int dataCount;
  char q;

  if( res.qa.qa_val != NULL ) free( res.qa.qa_val );

  bzero( &res, sizeof( res ) );

  count = arg->cb.count;
  res.intc.intc_val = arg->intc.intc_val;
  res.intc.intc_len = count;
  res.qa.qa_val = (long*)malloc( count*sizeof( long ) );
  res.qa.qa_len = count;
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );

  for( i = 0, dataCount = 0; i < count; i++ )
    {
      cfsa( &arg->fa.fa_val[i], &arg->exta.exta_val[i], 
            &res.intc.intc_val[dataCount], &q );
      res.qa.qa_val[i] = q;

      if( !( arg->fa.fa_val[i] & 0x8 ) ) dataCount++;
    }

  res.cb.tally = count;

  return( &res );
}

CSLIST_RES*
ca_rpccsga_1( CSLIST_ARG* arg )
{
  static CSLIST_RES res = { 0, 0, 0 };
  int count;
  int i;
  int dataCount;
  char q;

  if( res.qa.qa_val != NULL ) free( res.qa.qa_val );

  bzero( &res, sizeof( res ) );

  count = arg->cb.count;
  res.intt.intt_val = arg->intt.intt_val;
  res.intt.intt_len = count;
  res.qa.qa_val = (long*)malloc( count*sizeof( long ) );
  res.qa.qa_len = count;
  bcopy( &arg->cb, &res.cb, sizeof( CA_CB ) );

  for( i = 0, dataCount = 0; i < count; i++ )
    {
      cssa( &arg->fa.fa_val[i], &arg->exta.exta_val[i], 
            &res.intt.intt_val[dataCount], &q );
      res.qa.qa_val[i] = q;

      if( !( arg->fa.fa_val[i] & 0x8 ) ) dataCount++;
    }

  res.cb.tally = count;

  return( &res );
}

