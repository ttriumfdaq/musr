#include <rpc/rpc.h>
#include "camacRpc.h"
#include <time.h>
#ifdef VMS
#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_ 1
#include <sys/time.h>
#endif /* !_SYS_TIME_H_ */
#endif /* VMS */

/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25, 0 };

long *
ca_rpccdreg_1(argp, clnt)
	CA_BCNA *argp;
	CLIENT *clnt;
{
	static long res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCDREG, xdr_CA_BCNA, argp, xdr_long, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CA_DQ *
ca_rpccfsa_1(argp, clnt)
	CA_FXD *argp;
	CLIENT *clnt;
{
	static CA_DQ res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCFSA, xdr_CA_FXD, argp, xdr_CA_DQ, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


void *
ca_rpccccz_1(argp, clnt)
	long *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCCCZ, xdr_long, argp, xdr_void, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&res);
}


void *
ca_rpccccc_1(argp, clnt)
	long *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCCCC, xdr_long, argp, xdr_void, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&res);
}


void *
ca_rpcccci_1(argp, clnt)
	CA_XL *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCCCI, xdr_CA_XL, argp, xdr_void, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&res);
}


char *
ca_rpcctci_1(argp, clnt)
	long *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCTCI, xdr_long, argp, xdr_char, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


void *
ca_rpccccd_1(argp, clnt)
	CA_XL *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCCCD, xdr_CA_XL, argp, xdr_void, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&res);
}


char *
ca_rpcctcd_1(argp, clnt)
	long *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCTCD, xdr_long, argp, xdr_char, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


char *
ca_rpcctgl_1(argp, clnt)
	long *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCTGL, xdr_long, argp, xdr_char, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


void *
ca_rpcccinit_1(argp, clnt)
	long *argp;
	CLIENT *clnt;
{
	static char res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCCINIT, xdr_long, argp, xdr_void, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&res);
}


long *
ca_rpcctstat_1(argp, clnt)
	void *argp;
	CLIENT *clnt;
{
	static long res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCTSTAT, xdr_void, argp, xdr_long, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CFBLOCK_RES *
ca_rpccfmad_1(argp, clnt)
	CFBLOCK_ARG *argp;
	CLIENT *clnt;
{
	static CFBLOCK_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCFMAD, xdr_CFBLOCK_ARG, argp, xdr_CFBLOCK_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CFBLOCK_RES *
ca_rpccfubc_1(argp, clnt)
	CFBLOCK_ARG *argp;
	CLIENT *clnt;
{
	static CFBLOCK_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCFUBC, xdr_CFBLOCK_ARG, argp, xdr_CFBLOCK_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CFBLOCK_RES *
ca_rpccfubr_1(argp, clnt)
	CFBLOCK_ARG *argp;
	CLIENT *clnt;
{
	static CFBLOCK_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCFUBR, xdr_CFBLOCK_ARG, argp, xdr_CFBLOCK_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CA_DQ *
ca_rpccssa_1(argp, clnt)
	CA_FXD *argp;
	CLIENT *clnt;
{
	static CA_DQ res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCSSA, xdr_CA_FXD, argp, xdr_CA_DQ, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CSBLOCK_RES *
ca_rpccsmad_1(argp, clnt)
	CSBLOCK_ARG *argp;
	CLIENT *clnt;
{
	static CSBLOCK_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCSMAD, xdr_CSBLOCK_ARG, argp, xdr_CSBLOCK_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CSBLOCK_RES *
ca_rpccsubc_1(argp, clnt)
	CSBLOCK_ARG *argp;
	CLIENT *clnt;
{
	static CSBLOCK_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCSUBC, xdr_CSBLOCK_ARG, argp, xdr_CSBLOCK_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CSBLOCK_RES *
ca_rpccsubr_1(argp, clnt)
	CSBLOCK_ARG *argp;
	CLIENT *clnt;
{
	static CSBLOCK_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCSUBR, xdr_CSBLOCK_ARG, argp, xdr_CSBLOCK_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CFLIST_RES *
ca_rpccfga_1(argp, clnt)
	CFLIST_ARG *argp;
	CLIENT *clnt;
{
	static CFLIST_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCFGA, xdr_CFLIST_ARG, argp, xdr_CFLIST_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}


CSLIST_RES *
ca_rpccsga_1(argp, clnt)
	CSLIST_ARG *argp;
	CLIENT *clnt;
{
	static CSLIST_RES res;

	bzero((char *)&res, sizeof(res));
	if (clnt_call(clnt, CA_RPCCSGA, xdr_CSLIST_ARG, argp, xdr_CSLIST_RES, &res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&res);
}

