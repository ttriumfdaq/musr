/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include <memory.h> /* for memset */
#include "camacRpc.h"
#include <time.h>
#ifdef VMS
#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_ 1
#include <sys/time.h>
#endif /* !_SYS_TIME_H_ */
#endif /* VMS */

/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25, 0 };

long *
ca_rpccdreg_1(CA_BCNA *argp, CLIENT *clnt)
{
	static long clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCDREG, (xdrproc_t)xdr_CA_BCNA, (caddr_t)argp, (xdrproc_t)xdr_long, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CA_DQ *
ca_rpccfsa_1(CA_FXD *argp, CLIENT *clnt)
{
	static CA_DQ clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCFSA, (xdrproc_t)xdr_CA_FXD, (caddr_t)argp, (xdrproc_t)xdr_CA_DQ, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

void *
ca_rpccccz_1(long *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCCCZ, (xdrproc_t)xdr_long, (caddr_t)argp, (xdrproc_t)xdr_void, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&clnt_res);
}

void *
ca_rpccccc_1(long *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCCCC, (xdrproc_t)xdr_long, (caddr_t)argp, (xdrproc_t)xdr_void, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&clnt_res);
}

void *
ca_rpcccci_1(CA_XL *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCCCI, (xdrproc_t)xdr_CA_XL, (caddr_t)argp, (xdrproc_t)xdr_void, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&clnt_res);
}

char *
ca_rpcctci_1(long *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCTCI, (xdrproc_t)xdr_long, (caddr_t)argp, (xdrproc_t)xdr_char, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

void *
ca_rpccccd_1(CA_XL *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCCCD, (xdrproc_t)xdr_CA_XL, (caddr_t)argp, (xdrproc_t)xdr_void, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&clnt_res);
}

char *
ca_rpcctcd_1(long *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCTCD, (xdrproc_t)xdr_long, (caddr_t)argp, (xdrproc_t)xdr_char, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

char *
ca_rpcctgl_1(long *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCTGL, (xdrproc_t)xdr_long, (caddr_t)argp, (xdrproc_t)xdr_char, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

void *
ca_rpcccinit_1(long *argp, CLIENT *clnt)
{
	static char clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCCINIT, (xdrproc_t)xdr_long, (caddr_t)argp, (xdrproc_t)xdr_void, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return ((void *)&clnt_res);
}

long *
ca_rpcctstat_1(void *argp, CLIENT *clnt)
{
	static long clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCTSTAT, (xdrproc_t)xdr_void, (caddr_t)argp, (xdrproc_t)xdr_long, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CFBLOCK_RES *
ca_rpccfmad_1(CFBLOCK_ARG *argp, CLIENT *clnt)
{
	static CFBLOCK_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCFMAD, (xdrproc_t)xdr_CFBLOCK_ARG, (caddr_t)argp, (xdrproc_t)xdr_CFBLOCK_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CFBLOCK_RES *
ca_rpccfubc_1(CFBLOCK_ARG *argp, CLIENT *clnt)
{
	static CFBLOCK_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCFUBC, (xdrproc_t)xdr_CFBLOCK_ARG, (caddr_t)argp, (xdrproc_t)xdr_CFBLOCK_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CFBLOCK_RES *
ca_rpccfubr_1(CFBLOCK_ARG *argp, CLIENT *clnt)
{
	static CFBLOCK_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCFUBR, (xdrproc_t)xdr_CFBLOCK_ARG, (caddr_t)argp, (xdrproc_t)xdr_CFBLOCK_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CA_DQ *
ca_rpccssa_1(CA_FXD *argp, CLIENT *clnt)
{
	static CA_DQ clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCSSA, (xdrproc_t)xdr_CA_FXD, (caddr_t)argp, (xdrproc_t)xdr_CA_DQ, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CSBLOCK_RES *
ca_rpccsmad_1(CSBLOCK_ARG *argp, CLIENT *clnt)
{
	static CSBLOCK_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCSMAD, (xdrproc_t)xdr_CSBLOCK_ARG, (caddr_t)argp, (xdrproc_t)xdr_CSBLOCK_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CSBLOCK_RES *
ca_rpccsubc_1(CSBLOCK_ARG *argp, CLIENT *clnt)
{
	static CSBLOCK_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCSUBC, (xdrproc_t)xdr_CSBLOCK_ARG, (caddr_t)argp, (xdrproc_t)xdr_CSBLOCK_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CSBLOCK_RES *
ca_rpccsubr_1(CSBLOCK_ARG *argp, CLIENT *clnt)
{
	static CSBLOCK_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCSUBR, (xdrproc_t)xdr_CSBLOCK_ARG, (caddr_t)argp, (xdrproc_t)xdr_CSBLOCK_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CFLIST_RES *
ca_rpccfga_1(CFLIST_ARG *argp, CLIENT *clnt)
{
	static CFLIST_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCFGA, (xdrproc_t)xdr_CFLIST_ARG, (caddr_t)argp, (xdrproc_t)xdr_CFLIST_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

CSLIST_RES *
ca_rpccsga_1(CSLIST_ARG *argp, CLIENT *clnt)
{
	static CSLIST_RES clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call(clnt, CA_RPCCSGA, (xdrproc_t)xdr_CSLIST_ARG, (caddr_t)argp, (xdrproc_t)xdr_CSLIST_RES, (caddr_t)&clnt_res, TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}
