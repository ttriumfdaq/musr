#include <stdio.h>
#include <rpc/rpc.h>
#include "camacRpc.h"
#include <time.h>
#ifdef VMS
#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_ 1
#include <sys/time.h>
#endif /* !_SYS_TIME_H_ */
#endif /* VMS */

static void casrvr_1();

camacRpcServer()
{
	SVCXPRT *transp;
	(void)localServerInit();

	(void)pmap_unset(CASRVR, CAVERS);

	transp = svctcp_create(RPC_ANYSOCK, 0, 0);
	if (transp == NULL) {
		(void)fprintf(stderr, "cannot create tcp service.\n");
		exit(1);
	}
	if (!svc_register(transp, CASRVR, CAVERS, casrvr_1, IPPROTO_TCP)) {
		(void)fprintf(stderr, "unable to register (CASRVR, CAVERS, tcp).\n");
		exit(1);
	}
	svc_run();
	(void)fprintf(stderr, "svc_run returned\n");
	exit(1);
}

static void
casrvr_1(rqstp, transp)
	struct svc_req *rqstp;
	SVCXPRT *transp;
{
	union {
		CA_BCNA ca_rpccdreg_1_arg;
		CA_FXD ca_rpccfsa_1_arg;
		long ca_rpccccz_1_arg;
		long ca_rpccccc_1_arg;
		CA_XL ca_rpcccci_1_arg;
		long ca_rpcctci_1_arg;
		CA_XL ca_rpccccd_1_arg;
		long ca_rpcctcd_1_arg;
		long ca_rpcctgl_1_arg;
		long ca_rpcccinit_1_arg;
		CFBLOCK_ARG ca_rpccfmad_1_arg;
		CFBLOCK_ARG ca_rpccfubc_1_arg;
		CFBLOCK_ARG ca_rpccfubr_1_arg;
		CA_FXD ca_rpccssa_1_arg;
		CSBLOCK_ARG ca_rpccsmad_1_arg;
		CSBLOCK_ARG ca_rpccsubc_1_arg;
		CSBLOCK_ARG ca_rpccsubr_1_arg;
		CFLIST_ARG ca_rpccfga_1_arg;
		CSLIST_ARG ca_rpccsga_1_arg;
	} argument;
	char *result;
	bool_t (*xdr_argument)(), (*xdr_result)();
	char *(*local)();

	switch (rqstp->rq_proc) {
	case NULLPROC:
		(void)svc_sendreply(transp, xdr_void, (char *)NULL);
		return;

	case CA_RPCCDREG:
		xdr_argument = xdr_CA_BCNA;
		xdr_result = xdr_long;
		local = (char *(*)()) ca_rpccdreg_1;
		break;

	case CA_RPCCFSA:
		xdr_argument = xdr_CA_FXD;
		xdr_result = xdr_CA_DQ;
		local = (char *(*)()) ca_rpccfsa_1;
		break;

	case CA_RPCCCCZ:
		xdr_argument = xdr_long;
		xdr_result = xdr_void;
		local = (char *(*)()) ca_rpccccz_1;
		break;

	case CA_RPCCCCC:
		xdr_argument = xdr_long;
		xdr_result = xdr_void;
		local = (char *(*)()) ca_rpccccc_1;
		break;

	case CA_RPCCCCI:
		xdr_argument = xdr_CA_XL;
		xdr_result = xdr_void;
		local = (char *(*)()) ca_rpcccci_1;
		break;

	case CA_RPCCTCI:
		xdr_argument = xdr_long;
		xdr_result = xdr_char;
		local = (char *(*)()) ca_rpcctci_1;
		break;

	case CA_RPCCCCD:
		xdr_argument = xdr_CA_XL;
		xdr_result = xdr_void;
		local = (char *(*)()) ca_rpccccd_1;
		break;

	case CA_RPCCTCD:
		xdr_argument = xdr_long;
		xdr_result = xdr_char;
		local = (char *(*)()) ca_rpcctcd_1;
		break;

	case CA_RPCCTGL:
		xdr_argument = xdr_long;
		xdr_result = xdr_char;
		local = (char *(*)()) ca_rpcctgl_1;
		break;

	case CA_RPCCCINIT:
		xdr_argument = xdr_long;
		xdr_result = xdr_void;
		local = (char *(*)()) ca_rpcccinit_1;
		break;

	case CA_RPCCTSTAT:
		xdr_argument = xdr_void;
		xdr_result = xdr_long;
		local = (char *(*)()) ca_rpcctstat_1;
		break;

	case CA_RPCCFMAD:
		xdr_argument = xdr_CFBLOCK_ARG;
		xdr_result = xdr_CFBLOCK_RES;
		local = (char *(*)()) ca_rpccfmad_1;
		break;

	case CA_RPCCFUBC:
		xdr_argument = xdr_CFBLOCK_ARG;
		xdr_result = xdr_CFBLOCK_RES;
		local = (char *(*)()) ca_rpccfubc_1;
		break;

	case CA_RPCCFUBR:
		xdr_argument = xdr_CFBLOCK_ARG;
		xdr_result = xdr_CFBLOCK_RES;
		local = (char *(*)()) ca_rpccfubr_1;
		break;

	case CA_RPCCSSA:
		xdr_argument = xdr_CA_FXD;
		xdr_result = xdr_CA_DQ;
		local = (char *(*)()) ca_rpccssa_1;
		break;

	case CA_RPCCSMAD:
		xdr_argument = xdr_CSBLOCK_ARG;
		xdr_result = xdr_CSBLOCK_RES;
		local = (char *(*)()) ca_rpccsmad_1;
		break;

	case CA_RPCCSUBC:
		xdr_argument = xdr_CSBLOCK_ARG;
		xdr_result = xdr_CSBLOCK_RES;
		local = (char *(*)()) ca_rpccsubc_1;
		break;

	case CA_RPCCSUBR:
		xdr_argument = xdr_CSBLOCK_ARG;
		xdr_result = xdr_CSBLOCK_RES;
		local = (char *(*)()) ca_rpccsubr_1;
		break;

	case CA_RPCCFGA:
		xdr_argument = xdr_CFLIST_ARG;
		xdr_result = xdr_CFLIST_RES;
		local = (char *(*)()) ca_rpccfga_1;
		break;

	case CA_RPCCSGA:
		xdr_argument = xdr_CSLIST_ARG;
		xdr_result = xdr_CSLIST_RES;
		local = (char *(*)()) ca_rpccsga_1;
		break;

	default:
		svcerr_noproc(transp);
		return;
	}
	bzero((char *)&argument, sizeof(argument));
	if (!svc_getargs(transp, xdr_argument, &argument)) {
		svcerr_decode(transp);
		return;
	}
	result = (*local)(&argument, rqstp);
	if (result != NULL && !svc_sendreply(transp, xdr_result, result)) {
		svcerr_systemerr(transp);
	}
	if (!svc_freeargs(transp, xdr_argument, &argument)) {
		(void)fprintf(stderr, "unable to free arguments\n");
		exit(1);
	}
}

