#include <rpc/rpc.h>
#include "camacRpc.h"
#include <time.h>
#ifdef VMS
#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_ 1
#include <sys/time.h>
#endif /* !_SYS_TIME_H_ */
#endif /* VMS */


#if defined(__i386__) || defined(VAX) || defined(vax) || defined(__alpha) || defined(mips) || defined(__mips)
#define IM_LITTLE_ENDIAN
#else
#define IM_BIG_ENDIAN
#endif /* __i386__ || VAX || vax || __alpha || mips || __mips */


#define my_swap_long(p1,p2)              \
        ((char*)p2)[0] = ((char*)p1)[3]; \
        ((char*)p2)[1] = ((char*)p1)[2]; \
        ((char*)p2)[2] = ((char*)p1)[1]; \
        ((char*)p2)[3] = ((char*)p1)[0];

#define swap_long_array(a,s) { \
  int i; u_long tmpl; u_long* pl = (u_long*)(a); \
  for( i = 0; i < (s); i++, pl++ ) { tmpl = *pl; my_swap_long( &tmpl, pl ) } }

#define my_swap_short(p1,p2)             \
        ((char*)p2)[0] = ((char*)p1)[1]; \
        ((char*)p2)[1] = ((char*)p1)[0];

#define swap_short_array(a,s) { \
  int i; u_short tmps; u_short* ps = (u_short*)(a); \
  for( i = 0; i < (s); i++, ps++ ) { tmps = *ps; my_swap_short( &tmps, ps ) } }


bool_t
my_xdr_array( XDR* xdrs, char** arrp, u_int* sizep, 
	      u_int maxsize, u_int elsize, xdrproc_t elproc)
{
  int sizeInBytes;

  if( xdrs->x_op == XDR_FREE )
    {
      if( *arrp != NULL )
	{
	  free( *arrp );
	  *arrp = NULL;
	}
      return( TRUE );
    }

  if( !xdr_u_int( xdrs, sizep ) ) return( FALSE );
  
  if( *sizep > maxsize ) return( FALSE );
  
  sizeInBytes = elsize*(*sizep);
  
  if( xdrs->x_op == XDR_DECODE )
    {
      *arrp = (char*)malloc( sizeInBytes );
    }
  
#ifdef IM_LITTLE_ENDIAN
  if( xdrs->x_op == XDR_ENCODE )
    {
      if( elsize == 4 )
	{
	  swap_long_array( *arrp, *sizep )
        }
      else if( elsize == 2 )
	{
	  swap_short_array( *arrp, *sizep )
        }
    }
#endif
  
  if( !xdr_opaque( xdrs, *arrp, sizeInBytes ) )
    {
      return( FALSE );
    }
  
#ifdef IM_LITTLE_ENDIAN
  if( xdrs->x_op == XDR_DECODE )
    {
      if( elsize == 4 )
	{
	  swap_long_array( *arrp, *sizep )
        }
      else if( elsize == 2 )
	{
	  swap_short_array( *arrp, *sizep )
        }
    }
#endif
  
  return (TRUE);
}





bool_t
xdr_CA_BCNA(xdrs, objp)
	XDR *xdrs;
	CA_BCNA *objp;
{
	if (!xdr_long(xdrs, &objp->b)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->c)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->n)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->a)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_FXD(xdrs, objp)
	XDR *xdrs;
	CA_FXD *objp;
{
	if (!xdr_long(xdrs, &objp->f)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->ext)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->dat)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_DQ(xdrs, objp)
	XDR *xdrs;
	CA_DQ *objp;
{
	if (!xdr_long(xdrs, &objp->dat)) {
		return (FALSE);
	}
	if (!xdr_char(xdrs, &objp->q)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_XL(xdrs, objp)
	XDR *xdrs;
	CA_XL *objp;
{
	if (!xdr_long(xdrs, &objp->ext)) {
		return (FALSE);
	}
	if (!xdr_char(xdrs, &objp->l)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_LL(xdrs, objp)
	XDR *xdrs;
	CA_LL *objp;
{
	if (!xdr_long(xdrs, &objp->lam)) {
		return (FALSE);
	}
	if (!xdr_char(xdrs, &objp->l)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_CB(xdrs, objp)
	XDR *xdrs;
	CA_CB *objp;
{
	if (!xdr_long(xdrs, &objp->count)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->tally)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->lam_ident)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->channel)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CFBLOCK_ARG(xdrs, objp)
	XDR *xdrs;
	CFBLOCK_ARG *objp;
{
	if (!xdr_long(xdrs, &objp->f)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->ext)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->pData.pData_val, (u_int *)&objp->pData.pData_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CFBLOCK_RES(xdrs, objp)
	XDR *xdrs;
	CFBLOCK_RES *objp;
{
	if (!my_xdr_array(xdrs, (char **)&objp->pData.pData_val, (u_int *)&objp->pData.pData_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CSBLOCK_ARG(xdrs, objp)
	XDR *xdrs;
	CSBLOCK_ARG *objp;
{
	if (!xdr_long(xdrs, &objp->f)) {
		return (FALSE);
	}
	if (!xdr_long(xdrs, &objp->ext)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->pData.pData_val, (u_int *)&objp->pData.pData_len, ~0, sizeof(short), (xdrproc_t)xdr_short)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CSBLOCK_RES(xdrs, objp)
	XDR *xdrs;
	CSBLOCK_RES *objp;
{
	if (!my_xdr_array(xdrs, (char **)&objp->pData.pData_val, (u_int *)&objp->pData.pData_len, ~0, sizeof(short), (xdrproc_t)xdr_short)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CFLIST_ARG(xdrs, objp)
	XDR *xdrs;
	CFLIST_ARG *objp;
{
	if (!my_xdr_array(xdrs, (char **)&objp->fa.fa_val, (u_int *)&objp->fa.fa_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->exta.exta_val, (u_int *)&objp->exta.exta_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->intc.intc_val, (u_int *)&objp->intc.intc_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CFLIST_RES(xdrs, objp)
	XDR *xdrs;
	CFLIST_RES *objp;
{
	if (!my_xdr_array(xdrs, (char **)&objp->intc.intc_val, (u_int *)&objp->intc.intc_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->qa.qa_val, (u_int *)&objp->qa.qa_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CSLIST_ARG(xdrs, objp)
	XDR *xdrs;
	CSLIST_ARG *objp;
{
	if (!my_xdr_array(xdrs, (char **)&objp->fa.fa_val, (u_int *)&objp->fa.fa_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->exta.exta_val, (u_int *)&objp->exta.exta_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->intt.intt_val, (u_int *)&objp->intt.intt_len, ~0, sizeof(short), (xdrproc_t)xdr_short)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CSLIST_RES(xdrs, objp)
	XDR *xdrs;
	CSLIST_RES *objp;
{
	if (!my_xdr_array(xdrs, (char **)&objp->intt.intt_val, (u_int *)&objp->intt.intt_len, ~0, sizeof(short), (xdrproc_t)xdr_short)) {
		return (FALSE);
	}
	if (!my_xdr_array(xdrs, (char **)&objp->qa.qa_val, (u_int *)&objp->qa.qa_len, ~0, sizeof(long), (xdrproc_t)xdr_long)) {
		return (FALSE);
	}
	if (!xdr_CA_CB(xdrs, &objp->cb)) {
		return (FALSE);
	}
	return (TRUE);
}


