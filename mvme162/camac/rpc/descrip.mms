
CC =		cc/decc/standard=vaxc
CFLAGS =	/def=(DONT_DECLARE_MALLOC)
FAST_CAMAC = 	-DCAMAC_BUILD_INLINE -DCAMAC_BUILD_NOIPL -DCAMAC_BUILD_NOSTATUS

all :  libcamac_rpc.olb
    @ continue

cnaf.exe : cnaf.obj libcamac_rpc.olb
	$(LINK) $(LINKFLAGS) cnaf.obj, libcamac_rpc/lib, rpc_multinet/opt

cnaf.obj : cnaf.c
	$(CC) $(CFLAGS) cnaf.c

libcamac_rpc.olb : camac$r$pc_clnt.obj camac$r$pc$c$lient.obj camac$r$pc_xdr_clnt.obj
    $(LIBR)/create $* camac$r$pc_clnt.obj,camac$r$pc$c$lient.obj,camac$r$pc_xdr_clnt.obj

camac$r$pc_clnt.obj : camac$r$pc_clnt.c
	$(CC) $(CFLAGS) camac$r$pc_clnt.c

camac$r$pc$c$lient.obj : camac$r$pc$c$lient.c
	$(CC) $(CFLAGS) camac$r$pc$c$lient.c

camac$r$pc_xdr_clnt.obj : camac$r$pc_xdrmod.c
	$(CC) $(CFLAGS) /obj=camac$r$pc_xdr_clnt.obj camac$r$pc_xdrmod.c 

camac$r$pc_clnt.c : camac$r$pc.x

clean :
    delete/noconf *.obj;*,*.olb;*,*.exe;*

