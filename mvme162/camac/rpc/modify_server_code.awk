#-----------------------------------------------------------------------------
#  Copyright (c) 1991,1992 Southeastern Universities Research Association,
#                          Continuous Electron Beam Accelerator Facility
# 
#  This software was developed under a United States Government license
#  described in the NOTICE file included as part of this distribution.
# 
#  CEBAF Data Acquisition Group, 12000 Jefferson Ave., Newport News, VA 23606
#  Email: coda@cebaf.gov  Tel: (804) 249-7101  Fax: (804) 249-7363
# -----------------------------------------------------------------------------
#  
#  Description:
#  ------------
# Awk script to insert a call to a user written routine into the main
# routine of the server task.  This routine can contain any special
# initializations for VxWorks machines or for Slow Controls servers.
# 	
# 	
#  Author:  Chip Watson, CEBAF Data Acquisition Group
#	    Steve Wood, Hall C
# 
#  Revision History:
#    $Log: modify_server_code.awk,v $
#    Revision 1.1.1.1  1995/11/25 01:24:46  ted
#    import from decu18
#
# Revision 1.1  1992/06/05  20:28:55  watson
# Initial revision
#
#

BEGIN {count=0}

{print}

/.*SVCXPRT \*transp;/ { if (count==0){print "	(void)localServerInit();"; count=1 }}
