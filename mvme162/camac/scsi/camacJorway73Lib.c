/*-----------------------------------------------------------------------------
 * Copyright (c) 1993 TRIUMF Cylotron Facility
 *
 * TRIUMF Electronics Grp, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A5
 * Email: waters@sundae.triumf.ca Tel: (604) 222-1047 Fax: (604) 222-1074
 *-----------------------------------------------------------------------------
 * 
 * Description:
 *
 *	Driver for Jorway Model 73 SCSI CAMAC Crate Controller.
 *
 *   An implementation of the CAMAC standard routines in C,
 *   using C argument passing conventions.
 *
 *	Adapted from functions written by David Morris (TRIUMF)
 * 
 * Author:	Graham Waters, TRIUMF Electronics Group
 * File:        camacLib_scsi.c
 * Created:     December 1994.
 * $Revision: 1.1.1.1 $
 * last edit: $Date: 1995/12/16 08:54:18 $ $Author: ted $
 *
 * log file:  $Source: /usr/local/cvsroot/musr/mvme162/camac/scsi/camacJorway73Lib.c,v $
 *-------------------------------------------------------------------------**
 *
 * Revision History:
 *   22-Oct-1995  TW  All parameters now passed by reference.  
 *                    Some variable declaration changes.
 *                    Added ESONE routines: cfmad, cfubc, cfubr
 *                                          csmad, csubc, csubr
 *   03-Nov-1995  TW  Added cccz,cccc,ccci,cccd for comprehensiveness
 */

#include "vxWorks.h"
#include "scsiLib.h"
#include "ioLib.h"
#include "taskLib.h"
#include "memLib.h"
#include "stdio.h"

#include "camacLib.h"

/*
 *  SCSI command block masks
 */
#define SCB1_MASK          0xC0    /* vendor-unique mask */
#define SCB2_16BIT_MASK    0x00    /* 16 bit data */
#define SCB2_24BIT_MASK    0x20    /* full 24 bit data */
#define SCB2_SINGLE_MASK   0x00    /* single cycle */
#define SCB2_ADDRSCAN_MASK 0x40    /* single cycle */
#define SCB2_QSTOP_MASK    0x80    /* single cycle */
#define SCB2_QREPEAT_MASK  0xC0    /* single cycle */

#define CAMACSTATUS_Q(s)  (!(s&0x2))
#define CAMACSTATUS_X(s)  (!(s&0x1))

SCSI_PHYS_DEV* pScsiCamacDev[8];  /* storage for physical device info */
SEM_ID semCamacAction;            /* mutual exclusion semaphore */
char lastStatusByte = 0;          /* status byte of last command */


int
rstcam( long Crate )
{
    if( scsiPhysDevIdGet( pSysScsiCtrl, Crate, 0 ) == (SCSI_PHYS_DEV *)NULL )
    {
  	  /*-----------------------------------------*/
 	 /* create a SCSI physical device structure */
	/*-----------------------------------------*/

	pScsiCamacDev[Crate] = scsiPhysDevCreate( pSysScsiCtrl, Crate, 0, 6,
					SCSI_DEV_DIR_ACCESS, FALSE, 1, 16 );
	if( pScsiCamacDev[Crate] == (SCSI_PHYS_DEV *)NULL )
	{
            printf("Failed to create SCSI device\n");
            return( 0 );
    	}
    }
    return( 1 );
}


/*
 *  ccinit() - Initialize branch
 *
 *     Simulated by issuing crate initialize for
 *     each crate found on the bus
 */
int
ccinit( long* pBranch )
{
  long ext;
  long c, n, a;

  n = a = 0;  /* ignored */
  for( c = 0; c < 8; c++ )
    {
      ext = -1;
      cdreg( &ext, pBranch, &c, &n, &a );
      /*
       *  Do only if cdreg successful
       */
      if( ext != -1 )
	{
	  cccz( &ext );
	}
    }

  return( 1 );
}


/*
 *  ctstat() - Status of last command
 */
int
ctstat( long* pStatus )
{
  *pStatus = 0;
  if( !CAMACSTATUS_Q( lastStatusByte ) ) *pStatus |= 1;
  if( !CAMACSTATUS_X( lastStatusByte ) ) *pStatus |= 2;

  return( 1 );
}


/*
 *  cccz() - Generate Dataway Initialize
 */
int
cccz( long* pExt )
{
  CAMAC_EXT ext;
  long f;
  long dummy = 0;
  char q = 0;
  
  bcopy( pExt, &ext, sizeof( long ) );
  ext.n = 28;
  ext.a = 8;
  f = 26;
  
  return( cfsa( &f, (long*)&ext, &dummy, &q ) );
}


/*
 *  cccc() - Generate Dataway Clear
 */
int
cccc( long* pExt )
{
    CAMAC_EXT ext;
    long f;
    long dummy = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 28;
    ext.a = 9;
    f = 26;

    return( cfsa( &f, (long*)&ext, &dummy, &q ) );
}


/*
 *  ccci() - Set or Clear Dataway Inihibit
 */
int
ccci( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    long dummy = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 9;

    if( *pL ) f = 26;  /* Set Dataway Inhibit */
    else      f = 24;  /* Clear Dataway Inhibit */

    return( cfsa( &f, (long*)&ext, &dummy, &q ) );
}


/*
 *  cccd() - Enable or Disable Crate Demand
 */
int
cccd( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    long dummy = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 10;

    if( *pL ) f = 26;  /* Enable Crate Demand */
    else      f = 24;  /* Disable Crate Demand */

    return( cfsa( &f, (long*)&ext, &dummy, &q ) );
}


/*
 *  ctcd() - Test Crate Demand enabled
 */
int
ctcd( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    long data = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 10;
    f = 27;

    if( cfsa( &f, (long*)&ext, &data, &q ) == 0 ) return( 0 );

    /* I think this is correct? */
    *pL = q;

  return( 1 );
}


/*
 *  ctci() - Test Crate Inhibit
 */
int
ctci( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    long data = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 9;
    f = 27;

    if( cfsa( &f, (long*)&ext, &data, &q ) == 0 ) return( 0 );

    /* I think this is correct? */
    *pL = q;

  return( 1 );
}


/*
 *  ctgl() - Test Crate Demand present
 */
int
ctgl( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    long data = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 11;
    f = 27;

    if( cfsa( &f, (long*)&ext, &data, &q ) == 0 ) return( 0 );

    /* I think this is correct? */
    *pL = q;

  return( 1 );
}


/*
 *  cdreg()  - pack CAMAC B, C, N, A into 32 bits
 *
 *  Synopsis:
 *    int cdreg (ext,b,c,n,a)
 *    long* pExt -- OUTPUT: CAMAC packed address
 *    long *pB --------- INPUT:  CAMAC branch 0-7
 *    long *pC --------- INPUT:  CAMAC crate 0-7
 *    long *pN --------- INPUT:  CAMAC slot
 *    long *pA --------- INPUT:  CAMAC sub-address
 *
 *  Description:
 *    Packs CAMAC BCNA into a 32 bit integer.
 *
 */
int
cdreg( long* pExt, long* pB, long* pC, long* pN, long* pA )
{
    long C = *pC;
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

    if( scsiPhysDevIdGet( pSysScsiCtrl, C , 0 ) == (SCSI_PHYS_DEV*)NULL )
    {
        semCamacAction = semBCreate (SEM_Q_FIFO, SEM_FULL);

	  /*-----------------------------------------*/
	 /* create a SCSI physical device structure */
	/*-----------------------------------------*/

        pScsiCamacDev[C] = scsiPhysDevCreate( pSysScsiCtrl, C, 0, 6,
                                              SCSI_DEV_DIR_ACCESS, 
                                              FALSE, 1, 16 );
  	if( pScsiCamacDev[C] == (SCSI_PHYS_DEV *)NULL )
  	{
    	    printf("Failed to create SCSI device\n");
	    return( 0 );
    	}
    }

    ext.b = *pB;
    ext.c = *pC;
    ext.n = *pN;
    ext.a = *pA;

  return( 1 );
}


/*
 *  cfsa()  -  Execute 24-bit camac function
 *
 *  Synopsis:
 *    int cfsa( pF, pExt, pData, pQ )
 *    long* pF --------- INPUT: CAMAC function
 *    long* pExt -- INPUT: CAMAC packed address
 *    long* pData ------ INPUT/OUTPUT: write or read to from CAMAC
 *    char* pQ --------- OUTPUT -- true if q was present
 *
 *  Description:
 *    Execute a single long camac read/write
 *
 */
int 
cfsa( long* pF, long* pExt, long* pData, char* pQ )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    char Temp[4];
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_SINGLE_MASK | SCB2_24BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;

	
       /*------------------------------------------------------------------*/
      /* If the camac cycle is a write,				          */
     /* shuffle the 3 bytes into the proper order for the SCSI transfer  */
    /*------------------------------------------------------------------*/

    if( *pF > 15 )
    {
    	memcpy( Temp, (char*)pData, 4 );
    	Temp[0] = Temp[1];
	Temp[1] = Temp[2];
	Temp[2] = Temp[3];
	Temp[3] = 0;
    }

      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 3;
    scsiXaction.dataAddress   = (char*)Temp;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 3;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;

    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    /* 
     *  After a read, shuffle the 3 bytes 
     *  into a long word for return 
     */
    if( *pF < 8 )
    {
        Temp[3] = Temp[2];
    	Temp[2] = Temp[1];
    	Temp[1] = Temp[0];
    	Temp[0] = 0;
    	memcpy( (char*)pData, Temp, 4 );
    }

    *pQ = CAMACSTATUS_Q( scsiXaction.statusByte );

  return( 1 );
}


/*
 *  cfmad()  -  Execute Address Scan mode block transfer
 *
 *  Synopsis:
 *	int cfmad( pF, pExtb, pData, pCb )
 *	long* pF --------- INPUT: CAMAC function
 *	long* pExtb - INPUT: CAMAC packed address array
 *	long* pData ------ INPUT/OUTPUT: write or read to from CAMAC
 *	CAMAC_CB* pQ ----- INPUT/OUTPUT: control block
 *
 *  Description:
 *	Execute a 24-bit Q-Stop block mode camac read/write
 *
 *  Note:
 *      The Jorway73 does not ask for the last address
 *      in address scan mode.  Only the count in the control
 *      block is used.  So the "ext" for the last address
 *      is ignored here.
 */
int 
cfmad( long* pF, long* pExtb, long* pData, CAMAC_CB* pCb )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    long transferLength;
    char* pTempData;
    int i;
    char* pT;
    long* pD;
    CAMAC_EXT ext;

    bcopy( pExtb, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_ADDRSCAN_MASK | SCB2_24BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;
    transferLength = 32768 - pCb->count;
    scsiCommandBlock[3] = ( transferLength >> 8 ) & 0xFF;
    scsiCommandBlock[4] = transferLength & 0xFF;

    pTempData = (char*)malloc( ( 3 * pCb->count ) + 10 );
    if( pTempData == NULL )
    {
        return( 0 );
    }

       /*------------------------------------------------------------------*/
      /* If the camac cycle is a write,					  */
     /* shuffle the 3 bytes into the proper order for the SCSI transfer	 */
    /*------------------------------------------------------------------*/

    if( (*pF > 15) && (*pF < 24) )
    {
        pD = pData;                     /* pointer to data source */
        pT = pTempData;                 /* pointer to temporary allocated buffer */
        for( i = 0; i < pCb->count; i++ )
        {
            bcopy( ((char*)pD) + 1, pT, 3 );
            pT += 3;
            pD ++;
        }
    }

      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 5;
    scsiXaction.dataAddress   = (char*)pTempData;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 3 * pCb->count;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;


    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    /* 
     *  After a read, shuffle the 3 bytes 
     *  into a long word for return 
     */
    if( *pF < 8 )
    {
        pD = pData;                   /* pointer to data destination */
        pT = pTempData;               /* pointer to temporary allocated buffer */
        for( i = 0; i < pCb->count; i++ )
        {
            *pD = 0;
            bcopy( pT, ((char *)pD) + 1, 3 );
            pT += 3;
            pD ++;
        }
    }

    free( pTempData );

    /*
     *  What about pCb->tally ?
     */

  return( 1 );
}


/*
 *  cfubc()  -  Execute Q-Stop mode block transfer
 *
 *  Synopsis:
 *	int cfubc( pF, pExt, pData, pCb )
 *	long* pF --------- INPUT: CAMAC function
 *	long* pExt -- INPUT: CAMAC packed address
 *	long* pData ------ INPUT/OUTPUT: write or read to from CAMAC
 *	CAMAC_CB* pQ ----- INPUT/OUTPUT: control block
 *
 *  Description:
 *	Execute a 24-bit Q-Stop block mode camac read/write
 *
 */
int 
cfubc( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    long transferLength;
    char* pTempData;
    int i;
    char* pT;
    long* pD;
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_QSTOP_MASK | SCB2_24BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;
    transferLength = 32768 - pCb->count;
    scsiCommandBlock[3] = ( transferLength >> 8 ) & 0xFF;
    scsiCommandBlock[4] = transferLength & 0xFF;

    pTempData = (char*)malloc( ( 3 * pCb->count ) + 10 );
    if( pTempData == NULL )
    {
        return( 0 );
    }

       /*------------------------------------------------------------------*/
      /* If the camac cycle is a write,					  */
     /* shuffle the 3 bytes into the proper order for the SCSI transfer	 */
    /*------------------------------------------------------------------*/

    if( (*pF > 15) && (*pF < 24) )
    {
        pD = pData;                     /* pointer to data source */
        pT = pTempData;                 /* pointer to temporary allocated buffer */
        for( i = 0; i < pCb->count; i++ )
        {
            bcopy( ((char*)pD) + 1, pT, 3 );
            pT += 3;
            pD ++;
        }
    }

      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 5;
    scsiXaction.dataAddress   = (char*)pTempData;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 3 * pCb->count;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;


    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    /* 
     *  After a read, shuffle the 3 bytes 
     *  into a long word for return 
     */
    if( *pF < 8 )
    {
        pD = pData;                   /* pointer to data destination */
        pT = pTempData;               /* pointer to temporary allocated buffer */
        for( i = 0; i < pCb->count; i++ )
        {
            *pD = 0;
            bcopy( pT, ((char *)pD) + 1, 3 );
            pT += 3;
            pD ++;
        }
    }

    free( pTempData );

    /*
     *  What about pCb->tally ?
     */

  return( 1 );
}


/*
 *  cfubr()  -  Execute Q-Repeat mode block transfer
 *
 *  Synopsis:
 *	int cfubc( pF, pExt, pData, pCb )
 *	long* pF --------- INPUT: CAMAC function
 *	long* pExt -- INPUT: CAMAC packed address
 *	long* pData ------ INPUT/OUTPUT: write or read to from CAMAC
 *	CAMAC_CB* pQ ----- INPUT/OUTPUT: control block
 *
 *  Description:
 *	Execute a 24-bit Q-Repeat block mode camac read/write
 *
 */
int 
cfubr( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    long transferLength;
    char* pTempData;
    int i;
    char* pT;
    long* pD;
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_QREPEAT_MASK | SCB2_24BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;
    transferLength = 32768 - pCb->count;
    bcopy( &transferLength, &scsiCommandBlock[3], 2 );


    pTempData = (char*)malloc( ( 3 * pCb->count ) + 10 );
    if( pTempData == NULL )
    {
        return( 0 );
    }

       /*------------------------------------------------------------------*/
      /* If the camac cycle is a write,					  */
     /* shuffle the 3 bytes into the proper order for the SCSI transfer	 */
    /*------------------------------------------------------------------*/

    if( (*pF > 15) && (*pF < 24) )
    {
        pD = pData;                     /* pointer to data source */
        pT = pTempData;                 /* pointer to temporary allocated buffer */
        for( i = 0; i < pCb->count; i++ )
        {
            bcopy( ((char*)pD) + 1, pT, 3 );
            pT += 3;
            pD ++;
        }
    }

      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 5;
    scsiXaction.dataAddress   = (char*)pTempData;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 3 * pCb->count;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;


    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    /* 
     *  After a read, shuffle the 3 bytes 
     *  into a long word for return 
     */
    if( *pF < 8 )
    {
        pD = pData;                   /* pointer to data destination */
        pT = pTempData;               /* pointer to temporary allocated buffer */
        for( i = 0; i < pCb->count; i++ )
        {
            *pD = 0;
            bcopy( pT, ((char *)pD) + 1, 3 );
            pT += 3;
            pD ++;
        }
    }

    free( pTempData );

    /*
     *  What about pCb->tally ?
     */

  return( 1 );
}


/*
 *  cssa()  -  Execute 16-bit camac function 
 *
 *  Synopsis:
 *	int cssa( pF, pExt, pData, pQ )
 *	long* pF --------- INPUT: CAMAC function
 *	long* pExt ------- INPUT: CAMAC packed address
 *	short* pData ----- INPUT/OUTPUT: write or read to from CAMAC
 *	char* pQ --------- OUTPUT -- true if q was present
 *
 *  Description:
 *	Execute a single short camac read/write
 *
 */
int 
cssa( long* pF, long* pExt, short* pData, char* pQ )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_SINGLE_MASK | SCB2_16BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;
	
      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 3;
    scsiXaction.dataAddress   = (char*)pData;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 2;  /* 2 bytes */
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;

    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    *pQ = CAMACSTATUS_Q( scsiXaction.statusByte );

  return( 1 );
}


/*
 *  csmad()  -  Execute Address Scan mode block transfer
 *
 *  Synopsis:
 *	int csmad( pF, pExtb, pData, pCb )
 *	long* pF --------- INPUT: CAMAC function
 *	long* pExtb - INPUT: CAMAC packed address
 *	short* pData ----- INPUT/OUTPUT: write or read to from CAMAC
 *	CAMAC_CB* pQ ----- INPUT/OUTPUT: control block
 *
 *  Description:
 *	Execute a 16-bit Q-Stop block mode camac read/write
 *
 *  Note:
 *      The Jorway73 does not ask for the last address
 *      in address scan mode.  Only the count in the control
 *      block is used.  So the "ext" for the last address
 *      is ignored here.
 */
int 
csmad( long* pF, long* pExtb, short* pData, CAMAC_CB* pCb )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    long transferLength;
    CAMAC_EXT ext;

    bcopy( pExtb, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_ADDRSCAN_MASK | SCB2_16BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;
    transferLength = 32768 - pCb->count;
    bcopy( &transferLength, &scsiCommandBlock[3], 2 );


      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 5;
    scsiXaction.dataAddress   = (char*)pData;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 2 * pCb->count;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;


    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    /*
     *  What about pCb->tally ?
     */

  return( 1 );
}


/*
 *  csubc()  -  Execute Q-Stop mode block transfer
 *
 *  Synopsis:
 *	int csubc( pF, pExt, pData, pCb )
 *	long* pF --------- INPUT: CAMAC function
 *	long* pExt -- INPUT: CAMAC packed address
 *	short* pData ----- INPUT/OUTPUT: write or read to from CAMAC
 *	CAMAC_CB* pQ ----- INPUT/OUTPUT: control block
 *
 *  Description:
 *	Execute a 16-bit Q-Stop block mode camac read/write
 *
 */
int 
csubc( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    long transferLength;
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_QSTOP_MASK | SCB2_16BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;
    transferLength = 32768 - pCb->count;
    bcopy( &transferLength, &scsiCommandBlock[3], 2 );


      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 5;
    scsiXaction.dataAddress   = (char*)pData;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 2 * pCb->count;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;


    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    /*
     *  What about pCb->tally ?
     */

  return( 1 );
}


/*
 *  csubr()  -  Execute Q-Repeat mode block transfer
 *
 *  Synopsis:
 *	int csubr( pF, pExt, pData, pCb )
 *	long* pF --------- INPUT: CAMAC function
 *	long* pExt -- INPUT: CAMAC packed address
 *	short* pData ----- INPUT/OUTPUT: write or read to from CAMAC
 *	CAMAC_CB* pQ ----- INPUT/OUTPUT: control block
 *
 *  Description:
 *	Execute a 16-bit Q-Repeat block mode camac read/write
 *
 */
int 
csubr( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    long transferLength;
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

      /*------------------------------------------*/
     /* fill in fields of SCSI_COMMAND structure */	
    /*------------------------------------------*/

    scsiCommandBlock[0] = SCB1_MASK | *pF;
    scsiCommandBlock[1] = SCB2_QREPEAT_MASK | SCB2_16BIT_MASK | ext.n;
    scsiCommandBlock[2] = ext.a;
    transferLength = 32768 - pCb->count;
    bcopy( &transferLength, &scsiCommandBlock[3], 2 );


      /*----------------------------------------------*/
     /* fill in fields of SCSI_TRANSACTION structure */
    /*----------------------------------------------*/

    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 5;
    scsiXaction.dataAddress   = (char*)pData;
    scsiXaction.dataDirection = *pF > 15 ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 2 * pCb->count;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = 1000;


    semTake( semCamacAction, WAIT_FOREVER );	/* IS it my turn ? */

    scsiIoctl( pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int)&scsiXaction );

    semGive( semCamacAction );

    lastStatusByte = scsiXaction.statusByte;

    /*
     *  What about pCb->tally ?
     */

  return( 1 );
}
