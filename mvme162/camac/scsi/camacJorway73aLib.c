/*-----------------------------------------------------------------------------
 * Copyright (c) 1995 TRIUMF Cyclotron Facility
 *
 * TRIUMF, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A3
 * Email: whidden@triumf.ca  Voice: (604)222-7306  Fax: (604)222-1074
 *-----------------------------------------------------------------------------
 * 
 * Description:
 *
 *	Driver for Jorway Model 73A SCSI CAMAC Crate Controller.
 *
 *   An implementation of the CAMAC standard routines in C,
 *   using C argument passing conventions.
 *
 *	Adapted from functions written at TRIUMF 
 *      by David Morris and Graham Waters for the Jorway Model 73
 * 
 * Author:      Ted Whidden, TRIUMF Data Acquisition Group
 * File:        camacJorway73aLib.c
 * Created:     November 1995
 *-----------------------------------------------------------------------------
 *
 * Revision History:
 *
 *  26-Apr-1996  TW  Change rstcam to initScsiCamacDev.  Call initScsiCamacDev
 *                   for every CAMAC cycle, no longer does cccz.
 *                   Semaphore for each crate - not just one.
 *
 * Returns:
 *   All routines return 1 on success and 0 on failure.
 *
 */

#include "vxWorks.h"
/*  #define INCLUDE_SCSI2  /*  New Dec.14,2000  tests  */
#include "scsiLib.h"
#include "ioLib.h"
#include "taskLib.h"
#include "memLib.h"
#include "stdio.h"

#include "camacLib.h"

/*
 *  SCSI command block masks
 */
#define SCB_16BIT_MASK     0x00    /* 16 bit data */
#define SCB_24BIT_MASK     0x20    /* full 24 bit data */
#define SCB_SINGLE_MASK    0x00    /* Single Word */
#define SCB_ADDRSCAN_MASK  0x40    /* Address Scan  */
#define SCB_QSTOP_MASK     0x80    /* Q-Stop */
#define SCB_QREPEAT_MASK   0xC0    /* Q-Repeat */

#define OPCODE_TESTUNITREADY 0x00
#define OPCODE_NONDATA     0x01
#define OPCODE_SHORTDATA   0x01
#define OPCODE_LONGDATA    0x21

#define CAMAC_SCSI_TIMEOUT    SCSI_TIMEOUT_5SEC
#define CAMAC_SEM_TIMEOUT     600   /* 10 s */

#define REQUEST_SENSE_LEN  18      /* size of REQUEST_SENSE buffer in bytes */
#define NUM_BLOCKS         0
#define BLOCK_SIZE         0

SCSI_PHYS_DEV* pScsiCamacDevLast = NULL;         /* used to implement ctstat */
SCSI_PHYS_DEV* pScsiCamacDev[8];         /* storage for physical device info */
SEM_ID semCamacAction[8];                      /* mutual exclusion semaphore */

/*
 *  Use globals to keep track of Inhibit Enabled and Demand Enabled
 *  since it seems that the 73A does not implement the
 *  controller functions CTCI and CTCD.
 *  They are set by CCCI and CCCD, which are initialized using
 *  CCCZ, CTSTAT and CDREG.
 */
char scsiCamacInhibit[8] = {0,0,0,0,0,0,0,0};
char scsiCamacDemand[8]  = {0,0,0,0,0,0,0,0};

static int CamacNonData( long* pF, long* pExt, char* pQ );
static int CamacShortData( long* pF, long* pExt, caddr_t pData, int mask, long count, long* pTally, char* pQ );
static int CamacLongData( long* pF, long* pExt, caddr_t pData, int mask, long count, long* pTally, char* pQ );


/*
 *  TW  14-Dec-2000  Testing.  Consider dropping priority during CAMAC
 *                   calls if they're found to block VxWorks tasks.
 *                   See camp_if_gpib_mvip300.c for example.
 */
static int
priority_down( int down_amount )
{
  int tid, iPriority, iNewPriority;

  tid = taskIdSelf();

  taskPriorityGet( tid, &iPriority );

  iNewPriority = iPriority + down_amount;
  if( iNewPriority > 255 ) iNewPriority = 255;

  taskPrioritySet( tid, iNewPriority );

  return( iPriority );
}

static void
priority_set( int iPriority )
{
  taskPrioritySet( taskIdSelf(), iPriority );
}


/*
 *  CamacNonData()  -  Like cfsa, but handles status byte for non-data
 */
static int 
CamacNonData( long* pF, long* pExt, char* pQ )
{
    STATUS status;
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

    if( scsiPhysDevIdGet( pSysScsiCtrl, ext.c, 0 ) == (SCSI_PHYS_DEV *)NULL )
    {
      if( ( status = initScsiCamacDev( ext.c ) ) != 1 ) return( status );
    }

    pScsiCamacDevLast = pScsiCamacDev[ext.c];

    /*
     *  Build up the Command Block
     */
    scsiCommandBlock[0] = OPCODE_NONDATA;
    scsiCommandBlock[1] = *pF;
    scsiCommandBlock[2] = ext.n;
    scsiCommandBlock[3] = ext.a;
    scsiCommandBlock[4] = 0;
    scsiCommandBlock[5] = 0;
	
    /*
     *  Build up the Transaction Block
     */
    bzero( &scsiXaction, sizeof( SCSI_TRANSACTION ) );
    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 6;
    scsiXaction.dataAddress   = 0;
    scsiXaction.dataDirection = ( *pF > 15 ) ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = 0;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = CAMAC_SCSI_TIMEOUT;

    /*    semTake( semCamacAction[ext.c], CAMAC_SEM_TIMEOUT );   /* is it my turn? */
    semTake( semCamacAction[ext.c], WAIT_FOREVER );   /* is it my turn? */

    status = scsiIoctl( pScsiCamacDev[ext.c], 
		        FIOSCSICOMMAND, 
		        (int)&scsiXaction );

    semGive( semCamacAction[ext.c] );

    /*
     *  statusByte == 0 means GOOD, Q=0
     *  statusByte == 2 means CHECK CONDITION (error)
     *  statusByte == 4 means GOOD, Q=1
     *  This is a fudge, should really make
     *  Request Sense call on CHECK CONDITION
     */
    *pQ = ( scsiXaction.statusByte == 4 );

    return( status == OK );
}


/*
 *  CamacShortData()  -  data transfers under 256 bytes
 */
static int
CamacShortData( long* pF, long* pExt, caddr_t pData, 
                int mask, long count, long* pTally, char* pQ )
{
    STATUS status;
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    int is24bit;
    long transferLength;
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

    if( scsiPhysDevIdGet( pSysScsiCtrl, ext.c, 0 ) == (SCSI_PHYS_DEV *)NULL )
    {
      if( ( status = initScsiCamacDev( ext.c ) ) != 1 ) return( status );
    }

    pScsiCamacDevLast = pScsiCamacDev[ext.c];

    is24bit = ( mask & SCB_24BIT_MASK );

    if( *pF & 0x8 )  transferLength = 0;
    else if( is24bit ) transferLength = 4*count;
    else             transferLength = 2*count;

    if( transferLength > 255 ) return( 0 );

    /*
     *  Build up the Command Block
     */
    scsiCommandBlock[0] = OPCODE_SHORTDATA;
    scsiCommandBlock[1] = *pF;
    scsiCommandBlock[2] = ( mask | ext.n );
    scsiCommandBlock[3] = ext.a;
    scsiCommandBlock[4] = (char)transferLength;
    scsiCommandBlock[5] = 0;

    /*
     *  Build up the Transaction Block
     */
    bzero( &scsiXaction, sizeof( SCSI_TRANSACTION ) );
    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 6;
    scsiXaction.dataAddress   = (UINT8*)pData;
    scsiXaction.dataDirection = ( *pF > 15 ) ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = scsiCommandBlock[4];
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = CAMAC_SCSI_TIMEOUT;

    /*    semTake( semCamacAction[ext.c], CAMAC_SEM_TIMEOUT );   /* is it my turn? */
    semTake( semCamacAction[ext.c], WAIT_FOREVER );   /* is it my turn? */

#ifdef DEBUG
    printf( "doing scsiIoctl (%d) ...", scsiTestUnitRdy( pScsiCamacDev[ext.c] ) );
#endif /* DEBUG */

    status = scsiIoctl( pScsiCamacDev[ext.c], 
		        FIOSCSICOMMAND, 
		        (int)&scsiXaction );

#ifdef DEBUG
    printf( "done, status %d\n", status );
#endif /* DEBUG */

    semGive( semCamacAction[ext.c] );

    /*
     *  Overload meaning of tally
     */
    *pTally = ( scsiXaction.statusByte == 0 ) ? count : 0;

    /*
     *  statusByte == 0 means GOOD, Q=1
     *  statusByte == 2 means CHECK CONDITION (Q or X = 0)
     *  This is a fudge, should really make
     *  Request Sense call on CHECK CONDITION
     */
    *pQ = ( scsiXaction.statusByte == 0 );

    return( status == OK );
}


/*
 *  CamacLongData()  -  data transfers of 256+ bytes
 */
static int
CamacLongData( long* pF, long* pExt, caddr_t pData, 
	       int mask, long count, long* pTally, char* pQ )
{
    STATUS status;
    SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
    SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
    int is24bit;
    long transferLength;
    CAMAC_EXT ext;

    bcopy( pExt, &ext, sizeof( long ) );

    if( scsiPhysDevIdGet( pSysScsiCtrl, ext.c, 0 ) == (SCSI_PHYS_DEV *)NULL )
    {
      if( ( status = initScsiCamacDev( ext.c ) ) != 1 ) return( status );
    }

    pScsiCamacDevLast = pScsiCamacDev[ext.c];

    is24bit = ( mask & SCB_24BIT_MASK );

    if( *pF & 0x8 )  transferLength = 0;
    else if( is24bit ) transferLength = 4*count;
    else             transferLength = 2*count;

    /*
     *  Build up the Command Block
     */
    scsiCommandBlock[0] = OPCODE_LONGDATA;
    scsiCommandBlock[1] = 0;
    scsiCommandBlock[2] = *pF;
    scsiCommandBlock[3] = mask | ext.n;
    scsiCommandBlock[4] = ext.a;
    scsiCommandBlock[5] = 0;
    scsiCommandBlock[6] = ( transferLength >> 16 ) & 0xFF;
    scsiCommandBlock[7] = ( transferLength >> 8 ) & 0xFF;
    scsiCommandBlock[8] = transferLength & 0xFF;
    scsiCommandBlock[9] = 0;
	
    /*
     *  Build up the Transaction Block
     */
    bzero( &scsiXaction, sizeof( SCSI_TRANSACTION ) );
    scsiXaction.cmdAddress    = scsiCommandBlock;
    scsiXaction.cmdLength     = 10;
    scsiXaction.dataAddress   = (UINT8*)pData;
    scsiXaction.dataDirection = ( *pF > 15 ) ? O_WRONLY : O_RDONLY;
    scsiXaction.dataLength    = transferLength;
    scsiXaction.addLengthByte = -1;
    scsiXaction.cmdTimeout    = CAMAC_SCSI_TIMEOUT;

    /*    semTake( semCamacAction[ext.c], CAMAC_SEM_TIMEOUT );   /* is it my turn? */
    semTake( semCamacAction[ext.c], WAIT_FOREVER );   /* is it my turn? */

    status = scsiIoctl( pScsiCamacDev[ext.c], 
                        FIOSCSICOMMAND, 
                        (int)&scsiXaction );

    semGive( semCamacAction[ext.c] );

    /*
     *  Overload meaning of tally
     */
    *pTally = ( scsiXaction.statusByte == 0 ) ? count : 0;

    /*
     *  statusByte == 0 means GOOD, Q=1
     *  statusByte == 2 means CHECK CONDITION (Q or X = 0)
     *  This is a fudge, should really make
     *  Request Sense call on CHECK CONDITION
     */
    *pQ = ( scsiXaction.statusByte == 0 );

#ifdef DEBUG
    printf( "status=%d scsiStatus=%d tally=%d q=%d\n", 
            status, scsiXaction.statusByte, *pTally, *pQ );
#endif /* DEBUG */

    return( status == OK );
}


/*
 *  initScsiCamacDev()
 */
int
initScsiCamacDev( long Crate )
{
    int i;
    CAMAC_EXT ext;
/*  SCSI library 2 only
    SCSI_OPTIONS scsi_options;
*/

    if( scsiPhysDevIdGet( pSysScsiCtrl, Crate, 0 ) == (SCSI_PHYS_DEV*)NULL )
    {
        semCamacAction[Crate] = semBCreate( SEM_Q_FIFO, SEM_FULL );

	/*
	 *  Create a SCSI physical device structure
         *
	 *  Note that if the reqSenseLength is 0, one or more
         *  REQUEST_SENSE commands are used to determine this value.
         *  If devType is NONE, an INQUIRY is done.
         *  I don't think numBlocks or blockSize matter, since
         *  it isn't a block device.
	 */

#ifdef DEBUG
        printf("calling scsiPhyDevCreate - %p\n", pSysScsiCtrl);
#endif /* DEBUG */

        pScsiCamacDev[Crate] = scsiPhysDevCreate( pSysScsiCtrl, 
                                        Crate, 0,      /* ID, LUN */
				        REQUEST_SENSE_LEN, /* reqSenseLength */
					NONE,      /* devType */
					FALSE,     /* removable */
					NUM_BLOCKS,         /* numBlocks */
					BLOCK_SIZE );       /* blockSize */
#ifdef DEBUG
        printf("pScsiCamacDev[Crate] is %p\n", pScsiCamacDev[Crate]);
#endif /* DEBUG */

  	if( pScsiCamacDev[Crate] == (SCSI_PHYS_DEV *)NULL )
  	{
    	    printf("Failed to create SCSI device\n");
	    return( 0 );
    	}
/*      scsi 2 only  */
/*
	scsi_options.selTimeOut = 10000;
	scsiTargetOptionsSet( pSysScsiCtrl,
			     Crate,
			     &scsi_options,
			     SCSI_SET_OPT_TIMEOUT );
*/
    }

    /*
     *  The Jorway 73A manual recommends sending
     *  TEST UNIT READY until status is GOOD.
     *  So, try 10 times.
     */
    for( i = 0; i < 10; i++ )
      {
	if( scsiTestUnitRdy( pScsiCamacDev[Crate] ) == OK ) break;
      }
    if( i == 10 )
      {
	printf( "The Jorway 73A is not ready, check connections\n" );
	return( 0 );
      }

/* scsiShow(pSysScsiCtrl);  */
    
    return( 1 );
}


/*
 *  ccinit() - Initialize branch
 *
 *     Simulated by issuing crate initialize for
 *     each crate found on the bus
 */
int
ccinit( long* pBranch )
{
  long ext;
  long c, n, a;

  n = a = 0;  /* ignored */
  for( c = 0; c < 8; c++ )
    {
      ext = -1;
      if( cdreg( &ext, pBranch, &c, &n, &a ) )
	{
	  cccz( &ext );
	}
    }
  return( 1 );
}


/*
 *  ctstat() - Status of last command
 *
 *    Warning:  this is implemented by keeping
 *    a global variable pScsiPhysDevLast.  There
 *    is no mechanism to ensure that you're really
 *    getting the status from the correct crate, or
 *    that your last command was the last command to
 *    the crate.
 *
 *    Note that there is no way to get a status of
 *    No Q and No X - it's either or.
 *
 *    Possibly change error in physDev pointer and
 *    error in SCSI call to success status??
 */
int
ctstat( long* pStatus )
{
  char senseData[REQUEST_SENSE_LEN];
  int senseKey;
  int asc;

  if( pScsiCamacDevLast == NULL )
    {
      *pStatus = 4;  /* General error */
      return;
    }

  /* 
   *  Do Request Sense 
   */
  if( scsiReqSense( pScsiCamacDevLast, 
                    senseData, 
                    REQUEST_SENSE_LEN ) == ERROR )
    {
      *pStatus = 4;  /* General error */
      return( 0 );
    }

  senseKey = senseData[2];
  asc = senseData[12];

  if( senseKey == 0 )
    {
      *pStatus = 0;  /* Success */
    }
  else
    {
      if( asc == 0x44 )
	{
	  *pStatus = 2;  /* No X */
	}
      else if( asc == 0x80 )
	{
	  *pStatus = 1;  /* No Q */
	}
      else
	{
	  *pStatus = 4;  /* General error */
	}
    }

  return( 1 );
}


/*
 *  cccz() - Generate Dataway Initialize
 */
int
cccz( long* pExt )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;
    char l;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 28;
    ext.a = 8;
    f = 26;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    l = 1;
    ccci( pExt, &l );
    l = 0;
    cccd( pExt, &l );

    return( 1 );
}


/*
 *  cccc() - Generate Dataway Clear
 */
int
cccc( long* pExt )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 28;
    ext.a = 9;
    f = 26;

    return( CamacNonData( &f, (long*)&ext, &q ) );
}


/*
 *  ccci() - Set or Clear Dataway Inihibit
 */
int
ccci( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 9;

    if( *pL ) f = 26;  /* Set Dataway Inhibit */
    else      f = 24;  /* Clear Dataway Inhibit */

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    scsiCamacInhibit[ext.c] = *pL;

    return( 1 );
}


/*
 *  ctci() - Test Crate Inhibit
 */
int
ctci( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
/*
    ext.n = 30;
    ext.a = 9;
    f = 27;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    *pL = q;
*/
    *pL = scsiCamacInhibit[ext.c];

    return( 1 );
}


/*
 *  cccd() - Enable or Disable Crate Demand
 */
int
cccd( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 10;

    if( *pL ) f = 26;  /* Enable Crate Demand */
    else      f = 24;  /* Disable Crate Demand */

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    scsiCamacDemand[ext.c] = *pL;

    return( 1 );
}


/*
 *  ctcd() - Test Crate Demand enabled
 */
int
ctcd( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
/*
    ext.n = 30;
    ext.a = 10;
    f = 27;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    *pL = q;
*/
    *pL = scsiCamacDemand[ext.c];

    return( 1 );
}


/*
 *  ctgl() - Test Crate Demand present
 */
int
ctgl( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    long data = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 11;
    f = 27;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    /* I think this is correct? */
    *pL = q;

    return( 1 );
}


/*
 *  cdreg()  - pack CAMAC B, C, N, A into 32 bits
 *
 *  Returns:
 *    0 on failure
 *    1 on success
 */
int
cdreg( long* pExt, long* pB, long* pC, long* pN, long* pA )
{
    CAMAC_EXT ext;
    int status;

#ifdef DEBUG
    printf("calling cdreg for %d %d %d\n", *pC, *pN, *pA);
#endif /* DEBUG */

    if( ( status = initScsiCamacDev( *pC ) ) != 1 ) return( status );
	
    ext.b = *pB;
    ext.c = *pC;
    ext.n = *pN;
    ext.a = *pA;

    bcopy( &ext, pExt, sizeof( long ) );

#ifdef DEBUG
    printf("cdreg completed\n");
#endif /* DEBUG */

    return( 1 );
}


/*
 *  cfsa()  -  Execute 24-bit camac function
 *
 */
int 
cfsa( long* pF, long* pExt, long* pData, char* pQ )
{
    long dummy;

    if( *pF & 0x8 )
      {
	return( CamacNonData( pF, pExt, pQ ) );
      }
    else
      {
	return( CamacShortData( pF, pExt, (caddr_t)pData, 
				   SCB_SINGLE_MASK | SCB_24BIT_MASK,
				   1, &dummy, pQ ) );
      }
}


/*
 *  cfmad()  -  Execute Address Scan mode block transfer
 *
 *  Note:
 *      The Jorway73 does not ask for the last address
 *      in address scan mode.  Only the count in the control
 *      block is used.  So the "ext" for the last address
 *      is ignored here.
 */
int 
cfmad( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    char q;

    if( pCb->count < 64 )
    {
        return( CamacShortData( pF, pExt, (caddr_t)pData,
                        SCB_ADDRSCAN_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
    else
    {
        return( CamacLongData(  pF, pExt, (caddr_t)pData,
                        SCB_ADDRSCAN_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
}


/*
 *  cfubc()  -  Execute Q-Stop mode block transfer
 *
 */
int 
cfubc( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    char q;

    if( pCb->count < 64 )
    {
        return( CamacShortData( pF, pExt, (caddr_t)pData,
                        SCB_QSTOP_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
    else
    {
        return( CamacLongData(  pF, pExt, (caddr_t)pData,
                        SCB_QSTOP_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
}


/*
 *  cfubr()  -  Execute Q-Repeat mode block transfer
 *
 */
int 
cfubr( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    char q;

    if( pCb->count < 64 )
    {
        return( CamacShortData( pF, pExt, (caddr_t)pData,
                        SCB_QREPEAT_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
    else
    {
        return( CamacLongData(  pF, pExt, (caddr_t)pData,
                        SCB_QREPEAT_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
}


/*
 *  cssa()  -  Execute 16-bit camac function 
 *
 */
int 
cssa( long* pF, long* pExt, short* pData, char* pQ )
{
    long dummy;

    if( *pF & 0x8 )
      {
	return( CamacNonData( pF, pExt, pQ ) );
      }
    else
      {
	return( CamacShortData( pF, pExt, (caddr_t)pData, 
				   SCB_SINGLE_MASK | SCB_16BIT_MASK,
				   1, &dummy, pQ ) );
	
      }
}


/*
 *  csmad()  -  Execute Address Scan mode block transfer
 */
int 
csmad( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    char q;

    if( pCb->count < 128 )
    {
        return( CamacShortData( pF, pExt, (caddr_t)pData,
                        SCB_ADDRSCAN_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
    else
    {
        return( CamacLongData(  pF, pExt, (caddr_t)pData,
                        SCB_ADDRSCAN_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
}


/*
 *  csubc()  -  Execute Q-Stop mode block transfer
 */
int 
csubc( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    char q;

    if( pCb->count < 128 )
    {
        return( CamacShortData( pF, pExt, (caddr_t)pData,
                        SCB_QSTOP_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
    else
    {
        return( CamacLongData(  pF, pExt, (caddr_t)pData,
                        SCB_QSTOP_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
}


/*
 *  csubr()  -  Execute Q-Repeat mode block transfer
 */
int 
csubr( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    char q;

    if( pCb->count < 128 )
    {
        return( CamacShortData( pF, pExt, (caddr_t)pData,
                        SCB_QREPEAT_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
    else
    {
        return( CamacLongData(  pF, pExt, (caddr_t)pData,
                        SCB_QREPEAT_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
    }
}



