/* structures and defines for standard esone camac subroutines */

typedef struct {
    char b;
    char c;
    char n;
    char a;
} CAMAC_EXT;

typedef struct {
    long count;
    long tally;
    long lam_ident;
    long channel;
} CAMAC_CB;


#ifdef __cplusplus
extern "C" {
#endif

#if defined(__STDC__) || defined(__cplusplus)

 int ccinit( long* pB );
 int ctstat( long* pStatus );
 int cccz( long* pExt );
 int cccc( long* pExt );
 int ccci( long* pExt, char* pL );
 int cccd( long* pExt, char* pL );
 int ctcd( long* pExt, char* pL );
 int ctci( long* pExt, char* pL );
 int ctgl( long* pExt, char* pL );
 int cdreg( long* pExt, long* pB, long* pC, long* pN, long* pA );
 int cfsa(  long* pF, long* pExt, long*  pData, char* pQ );   /* 204 usec */
 int cfmad( long* pF, long* pExtb, long*  pData, CAMAC_CB* pCb );
 int cfubc( long* pF, long* pExt, long*  pData, CAMAC_CB* pCb );
 int cfubr( long* pF, long* pExt, long*  pData, CAMAC_CB* pCb );
 int cssa(  long* pF, long* pExt, short* pData, char* pQ );
 int csmad( long* pF, long* pExtb, short*  pData, CAMAC_CB* pCb );
 int csubc( long* pF, long* pExt, short* pData, CAMAC_CB* pCb );
 int csubr( long* pF, long* pExt, short* pData, CAMAC_CB* pCb );
 int cfga( long* fa, long* exta, long* intc, long* qa, CAMAC_CB* pCb );
 int csga( long* fa, long* exta, short* intt, long* qa, CAMAC_CB* pCb );

#else	/* __STDC__ */

 int ccinit();
 int ctstat();
 int cccz();
 int cccc();
 int ccci();
 int cccd();
 int ctcd();
 int ctci();
 int ctgl();
 int cdreg();
 int cfsa();
 int cfmad();
 int cfubc();
 int cfubr();
 int cssa();
 int csmad();
 int csubc();
 int csubr();
 int cfga();
 int csga();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif


