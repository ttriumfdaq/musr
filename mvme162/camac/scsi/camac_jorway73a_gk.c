/*-----------------------------------------------------------------------------
 * Copyright (c) 1995 TRIUMF Cyclotron Facility
 *
 * TRIUMF, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A3
 * Email: whidden@triumf.ca  Voice: (604)222-7306  Fax: (604)222-1074
 *-----------------------------------------------------------------------------
 * 
 * Description:
 *
 *	Driver for Jorway Model 73A SCSI CAMAC Crate Controller.
 *
 *   An implementation of the CAMAC standard routines in C,
 *   using C argument passing conventions.
 *
 *	Adapted from functions written at TRIUMF 
 *      by David Morris and Graham Waters for the Jorway Model 73
 * 
 * Author:      Ted Whidden, TRIUMF Data Acquisition Group
 * File:        camacJorway73aLib.c
 * Created:     November 1995
 *-----------------------------------------------------------------------------
 *
 * Revision History:
 *
 */

#include <stdio.h>
#include "camacLib.h"

typedef char* caddr_t;

/*
 *  SCSI command block masks
 */
#define SCB_16BIT_MASK     0x00    /* 16 bit data */
#define SCB_24BIT_MASK     0x20    /* full 24 bit data */
#define SCB_SINGLE_MASK    0x00    /* Single Word */
#define SCB_ADDRSCAN_MASK  0x40    /* Address Scan  */
#define SCB_QSTOP_MASK     0x80    /* Q-Stop */
#define SCB_QREPEAT_MASK   0xC0    /* Q-Repeat */

#define OPCODE_TESTUNITREADY 0x00
#define OPCODE_NONDATA     0x01
#define OPCODE_SHORTDATA   0x01
#define OPCODE_LONGDATA    0x21

#define CAMAC_SCSI_TIMEOUT    10000000 /* 10 s */
#define CAMAC_SEM_TIMEOUT     600   /* 10 s */

#define REQUEST_SENSE_LEN  18      /* size of REQUEST_SENSE buffer in bytes */
#define NUM_BLOCKS         0
#define BLOCK_SIZE         0

long scsiCamacLast = 0; /* the last crate accessed */

/*
 *  Use globals to keep track of Inhibit Enabled and Demand Enabled
 *  since it seems that the 73A does not implement the
 *  controller functions CTCI and CTCD.
 *  They are set by CCCI and CCCD, which are initialized using
 *  CCCZ, CTSTAT and CDREG.
 */
char scsiCamacInhibit[8];
char scsiCamacDemand[8];

static int CamacNonData( long* pF, long* pExt, char* pQ );
static int CamacData( long* pF, long* pExt, caddr_t pData, int mask, long count, long* pTally, char* pQ );


/*
 *  The following two are for compatibility with
 *  the MODAS CAMIF library
 */
/*
 *  Access to CAMIF common block
 */
#include "cfortran.h"
typedef struct {
  short ierr;
  short ierr1;
} ERRCOM_DEF;
#define Errcom COMMON_BLOCK( ERRCOM, errcom )
COMMON_BLOCK_DEF( ERRCOM_DEF, Errcom );
ERRCOM_DEF Errcom; /* set aside storage */

int
camif( long* dummy )
{
  Errcom.ierr = 0;    /* status of call */
  Errcom.ierr1 = 11;  /* type of OS/hardware */

  return( 1 );
}

int
ctrap( void ) 
{
  return( 1 );
}

int
get_chan( int C, long* pChan )
{
    int status;
    long gk_device_desc[2];
    char gk_device[8];

    if( C == 0 )
      {
	strcpy( gk_device, "GKA0" );
      }
    else
      {
        sprintf( gk_device, "GKA%1d00", C );
      }

    gk_device_desc[0] = strlen( gk_device );
    gk_device_desc[1] = gk_device;

    status = sys$assign( gk_device_desc, pChan, 0, 0 );
    if( !(status&1) ) 
      {
	printf("Failed to create SCSI device\n");
	return( 0 );
      }

    return( 1 );
}

int
free_chan( long chan )
{
    sys$dassgn( chan );
}
	
int
scsiTestUnitRdy( long C )
{
    long chan;
    int status;

    if( !get_chan( C, &chan ) ) return( 0 );
    status = test_ready( chan );
    free_chan( chan );
    return( !status );
}

int
scsiReqSense( int C, char* buf, int buf_len )
{
    long chan;
    int status;

    if( !get_chan( C, &chan ) ) return( 0 );
    status = request_sense( chan, buf, buf_len );
    free_chan( chan );
    return( !status );
}

/*
 *  CamacNonData()  -  Like cfsa, but handles status byte for non-data
 */
static int 
CamacNonData( long* pF, long* pExt, char* pQ )
{
    int status;
    CAMAC_EXT ext;
    char N, A, F;
    long chan;
    char statusByte;

    bcopy( pExt, &ext, sizeof( long ) );

    scsiCamacLast = ext.c;
    N = ext.n;
    A = ext.a;
    F = *pF;

    if( !get_chan( ext.c, &chan ) ) return( 0 );

    status = camacxfr( chan, N, A, F, 0, 0, 0, NULL );

    free( chan );

    statusByte = ( status >> 24 );

    /*
     *  qio error or gk error
     */
    if( (statusByte&0x80) || (statusByte&0x40) ) return( 0 );

    /*
     *  statusByte == 0 means GOOD, Q=0
     *  statusByte == 2 means CHECK CONDITION (error)
     *  statusByte == 4 means GOOD, Q=1
     *  This is a fudge, should really make
     *  Request Sense call on CHECK CONDITION
     */
    *pQ = ( statusByte == 4 );

    return( 1 );
}


/*
 *  CamacData()  -  data transfers under 256 bytes
 */
static int
CamacData( long* pF, long* pExt, caddr_t pData, 
                int mask, long count, long* pTally, char* pQ )
{
    int status;
    CAMAC_EXT ext;
    char N, A, F;
    long chan;
    char statusByte;
    char xfrlen;
    char mode;
    char word_len;

    bcopy( pExt, &ext, sizeof( long ) );

    scsiCamacLast = ext.c;
    N = ext.n;
    A = ext.a;
    F = *pF;

    if( !get_chan( ext.c, &chan ) ) return( 0 );

    word_len = ( mask & SCB_24BIT_MASK );
    xfrlen = count;
    mode = ( ( mask >> 6 ) & 0x3 );
    status = camacxfr( chan, N, A, F, word_len, xfrlen, mode, pData );

    free( chan );

    statusByte = ( status >> 24 );

    /*
     *  Overload meaning of tally
     */
    *pTally = ( status & 0x00FFFFFF );

    /*
     *  qio error or gk error
     */
    if( (statusByte&0x80) || (statusByte&0x40) ) return( 0 );

    /*
     *  statusByte == 0 means GOOD, Q=1
     *  statusByte == 2 means CHECK CONDITION (Q or X = 0)
     *  This is a fudge, should really make
     *  Request Sense call on CHECK CONDITION
     */
    *pQ = ( statusByte == 0 );

    return( 1 );
}


/*
 *  ccinit() - Initialize branch
 *
 *     Simulated by issuing crate initialize for
 *     each crate found on the bus
 */
int
ccinit( long* pBranch )
{
  long ext;
  long c, n, a;

  n = a = 0;  /* ignored */
  for( c = 0; c < 8; c++ )
    {
      ext = -1;
      cdreg( &ext, pBranch, &c, &n, &a );
      /*
       *  Do only if cdreg successful
       */
      if( ext != -1 )
	{
	  cccz( &ext );
	}
    }
  return( 1 );
}


/*
 *  ctstat() - Status of last command
 *
 *    Warning:  this is implemented by keeping
 *    a global variable pScsiPhysDevLast.  There
 *    is no mechanism to ensure that you're really
 *    getting the status from the correct crate, or
 *    that your last command was the last command to
 *    the crate.
 *
 *    Note that there is no way to get a status of
 *    No Q and No X - it's either or.
 *
 *    Possibly change error in physDev pointer and
 *    error in SCSI call to success status??
 */
int
ctstat( long* pStatus )
{
  char senseData[REQUEST_SENSE_LEN];
  int senseKey;
  int asc;

  if( scsiCamacLast == 0 )
    {
      *pStatus = 4;  /* General error */
      return;
    }

  /* 
   *  Do Request Sense 
   */
  if( !scsiReqSense( scsiCamacLast, 
                    senseData, 
                    REQUEST_SENSE_LEN ) )
    {
      *pStatus = 4;  /* General error */
      return( 0 );
    }

  senseKey = senseData[2];
  asc = senseData[12];

  if( senseKey == 0 )
    {
      *pStatus = 0;  /* Success */
    }
  else
    {
      if( asc == 0x44 )
	{
	  *pStatus = 2;  /* No X */
	}
      else if( asc == 0x80 )
	{
	  *pStatus = 1;  /* No Q */
	}
      else
	{
	  *pStatus = 4;  /* General error */
	}
    }

  return( 1 );
}


/*
 *  cccz() - Generate Dataway Initialize
 */
int
cccz( long* pExt )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;
    char l;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 28;
    ext.a = 8;
    f = 26;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    l = 1;
    ccci( pExt, &l );
    l = 0;
    cccd( pExt, &l );

    return( 1 );
}


/*
 *  cccc() - Generate Dataway Clear
 */
int
cccc( long* pExt )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 28;
    ext.a = 9;
    f = 26;

    return( CamacNonData( &f, (long*)&ext, &q ) );
}


/*
 *  ccci() - Set or Clear Dataway Inihibit
 */
int
ccci( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 9;

    if( *pL ) f = 26;  /* Set Dataway Inhibit */
    else      f = 24;  /* Clear Dataway Inhibit */

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    scsiCamacInhibit[ext.c] = *pL;

    return( 1 );
}


/*
 *  ctci() - Test Crate Inhibit
 */
int
ctci( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
/*
    ext.n = 30;
    ext.a = 9;
    f = 27;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    *pL = q;
*/
    *pL = scsiCamacInhibit[ext.c];
}


/*
 *  cccd() - Enable or Disable Crate Demand
 */
int
cccd( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 10;

    if( *pL ) f = 26;  /* Enable Crate Demand */
    else      f = 24;  /* Disable Crate Demand */

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    scsiCamacDemand[ext.c] = *pL;

    return( 1 );
}


/*
 *  ctcd() - Test Crate Demand enabled
 */
int
ctcd( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
/*
    ext.n = 30;
    ext.a = 10;
    f = 27;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    *pL = q;
*/
    *pL = scsiCamacDemand[ext.c];
}


/*
 *  ctgl() - Test Crate Demand present
 */
int
ctgl( long* pExt, char* pL )
{
    CAMAC_EXT ext;
    long f;
    long data = 0;
    char q = 0;

    bcopy( pExt, &ext, sizeof( long ) );
    ext.n = 30;
    ext.a = 11;
    f = 27;

    if( CamacNonData( &f, (long*)&ext, &q ) == 0 ) return( 0 );

    /* I think this is correct? */
    *pL = q;
}


/*
 *  cdreg()  - pack CAMAC B, C, N, A into 32 bits
 *
 */
int
cdreg( long* pExt, long* pB, long* pC, long* pN, long* pA )
{
    int status;
    long C = *pC;
    int i;
    CAMAC_EXT ext;
    int isNew = FALSE;

    /*
     *  The Jorway 73A manual recommends sending
     *  TEST UNIT READY until status is GOOD.
     *  So, try 10 times.
     */
    for( i = 0; i < 10; i++ )
      {
	if( scsiTestUnitRdy( C ) ) break;
	isNew = TRUE;  /* tested unready once, so generate cccz */
      }
    if( i == 10 )
      {
	printf( "ERROR: the Jorway 73A is not ready, check connections\n" );
	return( 0 );
      }
    
    ext.b = *pB;
    ext.c = *pC;
    ext.n = *pN;
    ext.a = *pA;

    if( isNew )
      {
        /*
	 *  Generate dataway initialize
	 */
	cccz( (long*)&ext );
      }

    bcopy( &ext, pExt, sizeof( long ) );

    return( 1 );
}


/*
 *  cfsa()  -  Execute 24-bit camac function
 *
 */
int 
cfsa( long* pF, long* pExt, long* pData, char* pQ )
{
    long dummy;

    if( *pF & 0x8 )
      {
	return( CamacNonData( pF, pExt, pQ ) );
      }
    else
      {
	return( CamacData( pF, pExt, (caddr_t)pData, 
				   SCB_SINGLE_MASK | SCB_24BIT_MASK,
				   1, &dummy, pQ ) );
      }
}


/*
 *  cfmad()  -  Execute Address Scan mode block transfer
 *
 *  Note:
 *      The Jorway73 does not ask for the last address
 *      in address scan mode.  Only the count in the control
 *      block is used.  So the "ext" for the last address
 *      is ignored here.
 */
int 
cfmad( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    char q;

        return( CamacData( pF, pExt, (caddr_t)pData,
                        SCB_ADDRSCAN_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
}


/*
 *  cfubc()  -  Execute Q-Stop mode block transfer
 *
 */
int 
cfubc( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    char q;

        return( CamacData( pF, pExt, (caddr_t)pData,
                        SCB_QSTOP_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
}


/*
 *  cfubr()  -  Execute Q-Repeat mode block transfer
 *
 */
int 
cfubr( long* pF, long* pExt, long* pData, CAMAC_CB* pCb )
{
    char q;

        return( CamacData( pF, pExt, (caddr_t)pData,
                        SCB_QREPEAT_MASK | SCB_24BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
}


/*
 *  cssa()  -  Execute 16-bit camac function 
 *
 */
int 
cssa( long* pF, long* pExt, short* pData, char* pQ )
{
    long dummy;

    if( *pF & 0x8 )
      {
	return( CamacNonData( pF, pExt, pQ ) );
      }
    else
      {
	return( CamacData( pF, pExt, (caddr_t)pData, 
				   SCB_SINGLE_MASK | SCB_16BIT_MASK,
				   1, &dummy, pQ ) );
	
      }
}


/*
 *  csmad()  -  Execute Address Scan mode block transfer
 */
int 
csmad( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    char q;

        return( CamacData( pF, pExt, (caddr_t)pData,
                        SCB_ADDRSCAN_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
}


/*
 *  csubc()  -  Execute Q-Stop mode block transfer
 */
int 
csubc( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    char q;

        return( CamacData( pF, pExt, (caddr_t)pData,
                        SCB_QSTOP_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
}


/*
 *  csubr()  -  Execute Q-Repeat mode block transfer
 */
int 
csubr( long* pF, long* pExt, short* pData, CAMAC_CB* pCb )
{
    char q;

        return( CamacData( pF, pExt, (caddr_t)pData,
                        SCB_QREPEAT_MASK | SCB_16BIT_MASK,
                        pCb->count, &pCb->tally, &q ) );
}


int
cfga( long* fa, long* exta, long* intc, long* qa, CAMAC_CB* pCb )
{
  int count;
  int i;
  char q;

  count = pCb->count;

  for( i = 0; i < count; i++ )
    {
      cfsa( &fa[i], &exta[i], &intc[i], &q );
      qa[i] = q;
    }

  pCb->tally = count;

  return( 1 );
}


int
csga( long* fa, long* exta, short* intt, long* qa, CAMAC_CB* pCb )
{
  int count;
  int i;
  char q;

  count = pCb->count;

  for( i = 0; i < count; i++ )
    {
      cssa( &fa[i], &exta[i], &intt[i], &q );
      qa[i] = q;
    }

  pCb->tally = count;

  return( 1 );
}


