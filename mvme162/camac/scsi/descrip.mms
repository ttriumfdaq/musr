
CC =		cc/decc/standard=vaxc
CFLAGS =	/def=(DONT_DECLARE_MALLOC)

all :  libcamac_scsi.olb
    @ continue

cnaf.exe : cnaf.obj libcamac_scsi.olb
	$(LINK) $(LINKFLAGS) cnaf.obj, libcamac_scsi/lib, rpc_multinet/opt

cnaf.obj : cnaf.c
	$(CC) $(CFLAGS) cnaf.c

libcamac_scsi.olb : camac_jorway73a_gk.obj jorway73a_gk.obj
    $(LIBR)/create $* camac_jorway73a_gk.obj, jorway73a_gk.obj

