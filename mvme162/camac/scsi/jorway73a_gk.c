
/*
This C language routine operates the Jorway Model 73A SCSI Bus CAMAC
Crate Controller using 6 and 10 byte commands (microcode revision 2.0.)
It does so by calling the VAX/VMS Generic Class (GK)  driver. 
The user must have DIAGNOSE and PHY_IO privileges. This driver is
described in DEC manual Part Number AA-PAJ2A-TE. During system startup
autoconfiguration, the Model 73A responds correctly, but does not identify
itself as a device known to VMS. Therefore, the GK driver is not loaded.
The generic class driver should be loaded by an explicit SYSGEN CONNECT
as follows:

$ RUN SYS$SYSTEM:SYSGEN
SYSGEN> CONNECT GKpd0u /NOADAPTER

NOTE - VMS version 6. et seq use SYSMAN to connect - see DEC documentation

GK is the device mnemonic for the generic SCSI class driver (GKDRIVER);
p represents the SCSI port ID ( A or B ); d is the CAMAC Controller ID
set on the front panel thumbwheel switch; 0 signifies the digit 0; and u
represents the logical unit number. Since the Model 73 has only one lun,
this parameter should be 0.

Before accessing the CAMAC hardware, A channel must be reserved using
the VMS system service call $ASSIGN.

A CAMAC transfer is invoked by calling the subroutine with the following
parameters ( from left to right ):

gk_chan		the channel returned by $assign
station		the CAMAC station
subaddress	the CAMAC address
function	the CAMAC function
word_len	0 for 16 bit CAMAC transfers, else 1
xfrlen		number of CAMAC words to transfer
mode		block transfer mode. refer to Model 73 manual.
			0 = Single Word
			1 = Address Scan
			2 = Q-Stop
			3 = Q-Repeat
data_buffer	address of data buffer to be used
		it must hold all bytes to be transferred
		if word_len = 0, 2 * xfrlen bytes
		if word_len = 1, 4 * xfrlen, since a null byte is sent
The routine returns a 32-bit  integer to the caller containing the
status as follows:

                -----------------------------------
                |YZ000QC0|  transfer count        |
                -----------------------------------
where:
	Y is 1 for a QIO Call error.
	Z is 1 for an error returned by the gk driver
	Q is the CAMAC Q response for non-data command
	C is 1 for Check Condition Status

To call this function from FORTRAN, include a statement

		INTEGER CAMACXFR

in the calling routine, and pass all but the last argument by value, i.e.

	ISTAT = CAMACXFR (%VAL(ICHAN),%VAL(N),...%VAL(MODE),IBUFF)

This code is a modification of code written for the original Model 73
by Rex Hubbard, Jorway Corp (516) 997-8120 and W.T.Meyer, Ames Laboratory,
Ames IA 50010 (515) 294-4047.
------------------------------------------------------------------------*/

#include <stdio.h>

#define OPCODE	0
#define	FLAGS	1
#define	COMMAND_ADDRESS 2
#define COMMAND_LENGTH 3
#define DATA_ADDRESS 4
#define DATA_LENGTH 5
#define PAD_LENGTH 6
#define PHASE_TIMEOUT 7
#define DISCONNECT_TIMEOUT 8

#define GK_EFN 1

globalvalue	IO$_DIAGNOSE;

unsigned gk_desc[15],gk_iosb[2];
char	command[10];

int
camacxfr(gk_chan,station,subaddress,function,word_len,xfrlen,mode,buffer)
char station,subaddress,function,word_len,mode;
char *buffer;
int xfrlen, gk_chan;
{
	int i,status, byte_len;
	byte_len  =  xfrlen * 2 * ( word_len + 1 );  /* to bytes	*/
	gk_desc[OPCODE] = 1;
	gk_desc[FLAGS] = function & 0x10 ? 0 : 1;
	gk_desc[COMMAND_ADDRESS] = &command[0];
	gk_desc[COMMAND_LENGTH] = byte_len < 256? 6 : 10;
	gk_desc[DATA_ADDRESS] =  function & 0x8 ? 0 : buffer;
	gk_desc[DATA_LENGTH] = byte_len;
	gk_desc[PAD_LENGTH] = 0;
	gk_desc[PHASE_TIMEOUT] = 0;		/* use defaults 4 sec */
	gk_desc[DISCONNECT_TIMEOUT] = 0;        /* ditto */
	for ( i = 9; i < 15; i++ )
		gk_desc[i] = 0;

	if ( byte_len < 256 ) {
	    command[0] = 0x01;
	    command[1] = function & 0x1F;
	    command[2] = (mode<<1 | (word_len & 0x1))<<5 | ( station & 0x1F);
	    command[3] = subaddress & 0xF;
	    command[4] = byte_len;
	    command[5] = 0;
	}
	else {
	    command[0] = 0x21;
	    command[1] = 0;
	    command[2] = function & 0x1F;
	    command[3] = (mode<<1 | (word_len & 0x1))<<5 | ( station & 0x1F);
	    command[4] = subaddress & 0xF;
	    command[5] = 0;
	    command[6] = byte_len >> 16;
	    command[7] = byte_len >> 8;
	    command[8] = byte_len & 0xFF;
	    command[9] = 0;
	}
	status = sys$qiow ( GK_EFN, gk_chan, IO$_DIAGNOSE, gk_iosb, 0, 0,
				gk_desc, 60, 0, 0, 0, 0 );

	if ( !( status & 1 )) 
		return ( 0x80000000 );
	if ( !(gk_iosb[0] & 1 ))
		return ( 0x40000000 + (gk_iosb[0] & 0xFFFF));
	else
		return ( (gk_iosb[0] >> 16) ) |
			 (( gk_iosb[1] & 0xff) << 16)
			| ( gk_iosb[1] & 0xff000000 );
}


/* Function to send SCSI TEST UNIT READY command to mdl 73A, using the GK
driver. This should be done after power up, after a SCSI reset, or after a
manual Z, before any CAMAC commands are attempted. If not, the first CAMAC
command issued will fail, since the model 73A will have UNIT ATTENTION asserted
in accordance with the SCSI Standard. (Subsequent CAMAC commands will not be
affected).
        The first TEST UNIT READY command itself will return CHECK
CONDITION status, but will remove the UNIT ATTENTION condition. A second TEST
UNIT READY (or any other command) should return GOOD status. The function
returns status as described above, with transfer count = 0. The first status
word returned should be 2000000 ( hex ), and then 0.
NEXT LINES DEFINED BY AK*/

int
test_ready( int gk_chan )
{
        int i,status;

        gk_desc[OPCODE] = 1;
        gk_desc[FLAGS] = 0;
        gk_desc[COMMAND_ADDRESS] = command;
        gk_desc[COMMAND_LENGTH] = 6;
        for ( i=4; i<15; i++ )
                gk_desc[i] = 0;
        for ( i=0; i<6; i++ )
                command[i] = 0;
        status = sys$qiow ( GK_EFN, gk_chan, IO$_DIAGNOSE, gk_iosb,0,0,
                gk_desc, 60,0,0,0,0);
        if ( !(status & 1) )
                return ( 0x80000000 );          /* qio call error       */
        if ( !( gk_iosb[0] & 1 ) )
                return ( 0x40000000 );          /* io error              */
        return ( gk_iosb[1] );
}


int
request_sense( int gk_chan, char* buf, int buf_len )
{
	int i,status;

	bzero( gk_desc, sizeof( gk_desc ) );

	gk_desc[OPCODE] = 1;
	gk_desc[FLAGS] = 1;  /* ?? 1 is for READ */
	gk_desc[COMMAND_ADDRESS] = command;
	gk_desc[COMMAND_LENGTH] = 6;
	gk_desc[DATA_ADDRESS] =  buf;
	gk_desc[DATA_LENGTH] = buf_len;
	gk_desc[PAD_LENGTH] = 0;
	gk_desc[PHASE_TIMEOUT] = 0;		/* use defaults 4 sec */
	gk_desc[DISCONNECT_TIMEOUT] = 0;        /* ditto */

	command[0] = 0x3; /* opcode */
	command[1] = 0;
	command[2] = 0;
	command[3] = 0;
	command[4] = buf_len;  /* allocation length */
	command[5] = 0;

	status = sys$qiow ( GK_EFN, gk_chan, IO$_DIAGNOSE, gk_iosb, 0, 0,
				gk_desc, 60, 0, 0, 0, 0 );
	if ( !( status & 1 )) 
		return ( 0x80000000 );
	if ( !(gk_iosb[0] & 1 ))
		return ( 0x40000000 + (gk_iosb[0] & 0xFFFF));
	else
		return ( (gk_iosb[0] >> 16) ) |
			 (( gk_iosb[1] & 0xff) << 16)
			| ( gk_iosb[1] & 0xff000000 );
}

