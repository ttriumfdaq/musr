
/* Functions (all return void):
 *
 * cdreg(int *ext,int b,int c,int n,int a)   define external address 
 * cssa(int f,int ext,int *dat,int *q)       short (16) single action 
 * cfsa(int f,int ext,int *dat,int *q)       long (24) single action 
 * cccz(int ext)			 crate initialize 
 * cccc(int ext)			 crate clear 
 * ccci(int ext,int l)			 set/reset crate inhibit 
 * ctci(int ext,int *l)			 test crate inhibit 
 * cccd(int ext,int l)			 ena/disable crate demand 
 * ctcd(int ext,int *l)			 test crate demand enabled
 * ccgl(int lam,int l)                   ena/disable graded lam ( NON STANDARD )
 * ctgl(intext,int *l)                   test graded lam
 * cdlam(int *lam,int b,int c,int n,int a,int inta[])  define lam
 * cclc(int lam)                         clear lam (dataless function)
 * cclm(int lam,int *l)                  alternative lam clears
 * ctlm(int lam,int *l)                  test lam
 * ccinit(int b)                         initionlize branch 
 * cclnk(int lam,void *())               link routine to lam
 * cculk(int lam,void *())               unlink routine from lam
 * ccrgl(int lam)                        re-enable graded lam(@end of isr)
 * cclwt(int lam)                        lam wait
 * ctstat(int *istat)			 test status of previous operation
 */


#include "vxWorks.h"
#include "intLib.h"
#include "taskLib.h"
#include "vme.h"
int INIT = 0;
int INTERRUPTS = 0;
int LAM_REG;
int ISR_ADDR;
int LAM_NUM;
int badadr;
int q,x;
/* THE BASIC ADDRESS FOR COMPUTING ADDRESS  */

#define    cbd_bas_vadr     0x00800000
#define    cbd_bas_ladr     0xf0800000

#define    am_reset         0        /* clear all IRR and all IMR bits */
#define    am_clirr         0x40     /* clear all IRR  */
#define    am_clirrb        0x48     /* clear IRR bit (0,1,2) */
#define    am_clirrmrb      0x18     /* clear IRR and IMR bit (0,1,2) */
#define    am_clisrb        0x78     /* clear ISR bit (0,1,2)  */
#define    am_load04        0x80     /* load mode bits 0-4 */
#define    am_load56        0xa1     /* load mode bits 5,6 set bit 7 */
#define    am_rsmem         0xe0     /* load bits 3,4 into byte count reg. and
                                        select response memory(bits 0,1,2) */ 
#define    am_sirr          0x58     /* set IRR bits (0,1,2)  */
#define    am_stimrb        0x38     /* set single imr bit   */
#define    am_prio          6
#define    am_vect          1
#define    am_irm           2
#define    am_gint          3
#define    am_ireq          4
#define    am_md_ini        1<<am_ireq
/*======================================================================
*
**FILE: camacLib.c
**AUTHOR(S): FU, SIAHONG
**DATE CREATED: 16-JUL-1992 16:12:45.51
**     **NOTES:
*
*======================================================================
*/
/* Get_int_info*/
void GET_INT_INFO(init,ints,lams,addr,num)
	int *init;
	int *ints;
	int *lams;
	int *addr;
	int *num;
	{
		*init = INIT;
		*ints = INTERRUPTS;
		*lams = LAM_REG;
		*addr = ISR_ADDR;
		*num  = LAM_NUM;
		return;
	}

/*
**NAME: cdreg
**PURPOSE:
*  CAMAC standard subroutine,declares CAMAC address. 
*/

void cdreg(ext,b,c,n,a)
int b,c,n,a;
int *ext;
{
STATUS sysBusToLocalAdrs();
int cbd_ladr;
/* 	camac packed ext=0, if user enter illegal b, c, n, a */

if (0>b||b>7||0>c||c>7||n==0||n==25||n==27||n==31||0>a||a>15)
    {
    	*ext=0;
    }
   
/* CALCULATE CAMAC PACKED ADDRESS */

else
    {	
        sysBusToLocalAdrs(VME_AM_STD_USR_DATA,cbd_bas_vadr,&cbd_ladr);
	*ext=cbd_ladr|b<<19|c<<16|n<<11|a<<7; 
    }
	
    return;

}


/*=============================================================================
**NAME: cssa.c
**PURPOSE:CMAC standard subrutine -- perform single action, 24 bits
*/

void cssa(f,ext,dat,q)
      int     ext;            /* INPUT -- CAMAC packed address */
      int     f;              /* INPUT -- CAMAC function code */
      unsigned  long *dat;    /* INPUT-OUTPUT -- Write to or read form CAMAC */
      unsigned  long *q;      /* OUTPUT -- true if q was present */
{
    register short  *reg1;   
    int    vme_adr;           /* CAMAC VME address */
    int    cbd_csr;           /* cbd control status register address */
    int    lockKey;
    int    istat; 
    void   ctstat();
    lockKey = intLock();
/* calculate CAMAC 16 bits operation VME address */
   vme_adr=ext+(f<<2)+2;

/* bad address badadr=1 when enter illegal b,c,n,a. */
  
   if (ext == 0 )
      {
         badadr=1;
      }

/* badadr=0, when b,c,n,a are correct. */
 
   else 
      {
          badadr=0;

/* execute command function or read function -- 16 bits operation. */

          if (16 > f || f > 23)
             {
                 reg1 = (short *)vme_adr;
                 *dat = *reg1;
             }
         
/* execute 16 bits write operation. */
	 
           else 
	      {
		  reg1 = (short *)vme_adr;
		  *reg1 = *dat;
 	      }
       }

       cbd_csr=(cbd_bas_ladr|29<<11)+2;     /* c=0,n=29,a=0,f=0 */	
/*       *q=*((unsigned short *)cbd_csr)>>15; */
       ctstat(&istat);
         *q=istat;
       intUnlock(lockKey);
       return;	
} 
  

/*============================================================================
**NAME: cfsa.c
**PURPOSE:CMAC standard subrutine -- perform single action, 24 bits
*/
void cfsa(f,ext,dat,q)
      int      ext;           /* INPUT -- CAMAC packed address */
      int      f;             /* INPUT -- CAMAC function code */
      unsigned long *dat;     /* INPUT-OUTPUT -- Write to or read form CAMAC */
      unsigned long *q;       /* OUTPUT -- true if Q was present */
{
/* n=29 read cbd register, n=30 and n=28 implement camac command */

if (((((ext>>11)&0x1f)==29)&&(f!=10))||(((ext>>11)&0x1f)==28)||(((ext>>11)&0x1f)==30))
   {
       cssa(f,ext,dat,q);
   }
else
{
    register int   *reg1;   
    int      vme_adr;         /* CAMAC VME address */
    int      cbd_csr;         /* CBD control status register address */
    int      istat;
    void     ctstat();
    int      lockKey;
    lockKey = intLock();
/* calculate CAMAC 24 bits operation VME address */
  
      vme_adr=ext+(f<<2);
/* bad address badadr=1 when enter illegal b,c,n,a.  */
  
    if (ext == 0 )
      {
         badadr=1;
      }

/* badadr=0, when b,c,n,a are correct. */
 
    else 
      {
         badadr=0;

/* execute command function or read function -- 16 bits operation. */
          
	if (16 > f || f > 23)
             {
                 reg1 = (int *)vme_adr;
                 *dat = *reg1;
              }
         
/* execute 16 bits write operation.  */
	 

        else 
	      {
		  reg1 = (int *)vme_adr;
		  *reg1 = *dat;
 	      }
        }
	
	cbd_csr=(cbd_bas_ladr|29<<11)+2;    /* c=0,n=29,a=0,f=0  */
     /*   *q=*((unsigned short *)cbd_csr)>>15; */
	ctstat(&istat);
	*q=istat;
	intUnlock(lockKey);
        return;	
   } 
}


/*===========================================================================
**NAME: ctstat
**PURPOSE: test status of proceding action
*/


void ctstat(istat)
               
   int *istat;             /* OUTPUT -- Q,X status of proceding CAMAC action */

{
     short    y;
     register short   *reg1;
     int      cbd_csr;
/* read control status register */
     cbd_csr =(cbd_bas_ladr|29<<11)+2;
        reg1 = (short *)cbd_csr;
           y = *reg1;


/* status=4 when enter illegal b,c,n,a    */

        if (badadr == 1) 
                 {
                       *istat = 4;
                 }	
       
/* test CBD control status register q, x bit  */

         else 
                 {
                      if ((y & 0x8000) && (y & 0x4000))
                         {
                                 *istat = 0 ; 
                         }
                  
                      else if ( !(y & 0x8000) && (y & 0x4000))
                         {
                                 *istat = 1 ;
                         }
                    
                      else if ((y & 0x8000) && !(y & 0x4000))
                         {
          	                 *istat = 2 ;
                         }

                      else 
                         {
                                 *istat = 3 ;
                         }
                }   
                return;
}


/*============================================================================
** name cccz.c 
** Purpose  crate initialize   
*/
 
void cccz(ext)
     int  ext;      /* INPUT--CAMAC external address  */
{
	short    temp;
	register short  *reg1;
	int      cam_gen_z;
       
        int     lockKey;
	lockKey = intLock();

	cam_gen_z=(ext|(26<<2)|2);
        reg1 = (short *)cam_gen_z;
	temp = *reg1;

	intUnlock(lockKey);
}

/*============================================================================
** Name cccc.c
**purpose  crate cleare
*/
void cccc(ext)
     int  ext;      /* INPUT--CAMAC external address */
{
	short    temp;
        register short *reg1;
	int      cam_gen_c;

        int      lockKey;
	lockKey = intLock();

	cam_gen_c=(ext|(26<<2)|2);
	reg1 = (short *)cam_gen_c;
        temp = *reg1;

	intUnlock(lockKey);
}

/*=============================================================================
** Name ccci.c
** Purpose  set/reset crate inhibit
*/
void ccci(ext,l)
     int        ext;      /* INPUT--CAMAC external address */  
     int        l;        /* INPUT--l=1 set dataway inhibit ,l=0
                             clear dataway inhibit   */ 
{
     short    temp;
     register short *reg1;
     int      cam_ed_i;

     int      lockKey;
     lockKey = intLock();   
     if (l)
	cam_ed_i=(ext|(26<<2)|2); /* c=1,n=30,a=9,f=26 --clear I*/ 
     else
	cam_ed_i=(ext|(24<<2)|2); /* c=1,n=30,a=9,f=24 --set I*/
     reg1 = (short *)cam_ed_i;
     temp = *reg1;  
     intUnlock(lockKey);
}
 

/*=============================================================================
** Name ctci.c
** Purpose  Test Crate Dataway Inhibit
*/

void ctci(ext,l)
     int        ext;        /* INPUT--CAMAC external address */
     int        *l;         /* OUTPUT-- l=1 if inhibit(I) is set
                                        l=0 if inhibit(I) is clear */
{
     short    temp;
     register short *reg1;
     int      cam_tst_i;
     int      cbd_csr;

     int      lockKey;
     lockKey = intLock();
   
     cam_tst_i=(ext|(27<<2)|2); /* c=1,n=30,a=9,f=27 */ 
     reg1 = (short *)cam_tst_i;
     temp = *reg1;
     
     cbd_csr=(cbd_bas_ladr|(29<<11)|2); /* c=0,n=29,a=0,f=0 */
     intUnlock(lockKey);
     *l=(*(short *)cbd_csr >> 15)&1;       /* if cbd csr q=1 l=1, or l=0 */
}
/*=============================================================================
** Name cccd.c
** Purpose  set/reset crate Demand E/D 
*/

void cccd(ext,l)
     int        ext;             /* INPUT--CAMAC external address */
     int        l;               /* INPUT--l=1 set camac demand 
                                           l=0 reset camac demand */
{
     int dat;
     int   *temp;
     register short *reg1;
     int      cam_ed_bd;

     int      lockKey;
     lockKey = intLock();   
     temp = &dat;
     
     if (l)   
  {
     cam_ed_bd=(ext+(26<<2)+2);   /* c=1,n=30,a=10,f=26 */
  printf("enabling BD, VME addr is %x\n",cam_ed_bd);
  }
     else
  {
  cam_ed_bd=(ext+(24<<2)+2);   /* c=1,n=30,a=10,f=24 */
  printf("disabling BD, VME addr is %x\n",cam_ed_bd);
  }

     reg1 = (short *)cam_ed_bd;
     printf("reg1 contains %x\n",reg1);
     *temp = *reg1;  
     intUnlock(lockKey);
}
 

/*=============================================================================
** Name ctcd.c
** Purpose  Test Crate Demand Enabled
*/

void ctcd(ext,l)
     int        ext;            /* INPUT--CAMAC external address */
     int        *l;             /* OUTPUT-- l=1 if E/D Flip Flop is set  
                                            l=0 if E/D Flip Flop is cleared */
{
     short    temp;
     register short *reg1;
     int      cbd_csr;
     int      cam_tst_bd;

     int      lockKey;
     lockKey = intLock();   

     cam_tst_bd=(ext|(27<<2)|2);     /* c=1,n=30,a=10,f=27 */
 
     reg1 = (short *)cam_tst_bd;
     temp = *reg1;
   
     cbd_csr=(cbd_bas_ladr|(29<<11)|2); 
     *l= (*(short *)cbd_csr >> 15)&1;
     intUnlock(lockKey);
}


/*=============================================================================
** Name cdlam.c
** Purpose  define lam
*/

void cdlam(ext,b,c,n,a,inta)
	int *ext;
	int b,c,n,a;
	int inta[2];
{
	/* ignore inta[] */
	cdreg(ext,b,c,n,a);
	return;
}

/*=============================================================================
** Name ccgl.c
** Purpose  ena/disable graded lam (NON STANDARD) */

void ccgl(lam,l)
     int        lam;  /* INPUT -- Lam identifier */
     int        l;    /* INPUT -- l=1 enable graded lam
                                  l=0 disable graded lam */
{
     char     temp;
     register char *reg1;
     register short *reg2;
     int      cbd_csr;
     int      gl,b;
     int      cbd_icr_lam;
     int      lockKey;
     lockKey = intLock();   
     /* read cbd csr */
 
     cbd_csr = (cbd_bas_ladr|29<<11|2);
     reg2 = (short *)cbd_csr;
     temp = *reg2;                                     
     gl = ((lam>>11)&31-1);             /* gl=0--23 (internal interrupt) */ 
     if (gl>23)        /* external interrupt: INT2=1 gl=24, INT4=1 gl=25  */
	{
		if (l==1)            /* enable single external interrupt */     
		{
			if (gl==24)
			{	
        		temp = temp & ~(0x08);      /* clear MIT2  */
			}
			if (gl==25)
			{
			temp = temp & ~(0x04);     /* clear MIT4  */
			}
		}
                else
		{
			if (gl==24)
			{
			temp = temp | 0x08;        /* set MIT2  */
			}
			if (gl==25)
			{
			temp = temp | 0x40;        /* set MIT4  */
			}
		}
	}
     else           /* for internal interrupt gl=0--23  */
	{
	b = (lam>>19)&7;
	cbd_icr_lam = (cbd_bas_ladr|b<<19|29<<11|5<<2|3);
	reg1 = (char *)cbd_icr_lam;    
	while (gl > 7)
	    {
		cbd_icr_lam += 2;        /* 7< gl <=15 use INT_CONT.2 */
	                                 /* 15< gl <= 23 use INT_CONT.3 */	
		gl -= 8;
            }
		*reg1 = am_clisrb | gl;	   /* clear ISR bits  */	
	if (l==1)                         /* enable single internal interrupt */
		{
		*reg1 = am_clirrmrb | gl;  /* clear IRR and IMR bits (0,1,2) */ 
/*		*reg1 = am_sirr | gl;        set IRR bits  */
		}
	else
		{
		*reg1 = am_stimrb | gl;    /* set IMR bits (0,1,2)  */
		}
     } 
     intUnlock(lockKey);
}
 

/*=============================================================================
** Name ctgl.c
** Purpose  Test graded lam (Test Demand Line)
*/

void ctgl(ext,l)
     int        ext;        /* INPUT -- CAMAC external address */
     int        *l;         /* OUTPUT-- l=1 if graded lam exist 
                                        l=0 if no graded lam */
{
     short    temp;
     register short *reg1;
     int      cbd_csr;
     int      cam_tst_dl;

     int      lockKey;
     lockKey = intLock();   
     
     cam_tst_dl=(ext|(27<<2)|2);     /* c=1,n=30,a=11,f=27 */
 
     reg1 = (short *)cam_tst_dl;
     temp = *reg1;
   
     cbd_csr=(cbd_bas_ladr|(29<<11)|2); 
     *l=(*(short *)cbd_csr >> 15)&1;
     intUnlock(lockKey);
}


/*=============================================================================
** Name cclm.c
** Purpose alternative lam clears  
*/
     
void cclm(lam,l)
     int lam;                    /* INPUT -- lam identifier   */
     int l;                      /* INPUT -- l=1 enable lam   
                                             l=0 disable lam  */
{
  int temp1,temp2;

  if (l) {
    cssa(26,lam,&temp1,&temp2);    /* enable lam output    */ 
  } else {
    cssa(24,lam,&temp1,&temp2);    /* disable lam output   */
  }
}


/*=============================================================================
** Name cclc.c
** Purpose clear lam 
*/

void cclc(lam)
     int lam;              /* INPUT -- Lam identifier    */
{
  int temp1,temp2;
  cssa(10,lam,&temp1,&temp2);
}

/*=============================================================================
** Name ctlm.c
** Purpose test lam
*/

void ctlm(lam,l)
     int lam;               /* INPUT -- Lam identifier    */ 
     int *l;                /* OUTPT -- l=1 if lam set
                                        l=0 if lam clear  */ 
{
  int temp1,temp2;
  cssa(8,lam,&temp1,&temp2);
  *l=temp2;
}

/*=============================================================================
**
*/

VOIDFUNCPTR lamisrs[25][8];
    int  taskidlam[25][8];        /* c<8, gl<25  */

 
/*=============================================================================
** Name lamIsr.c
** Purpose read graded lam
*/

void lamIsr()
{
int    data;
int ii,jj,pattern;       /* ii--c, jj--n */
	 /* read LAM pattern--read gl */
int  cbd_rd_gl;
        register int *reg1;
	INTERRUPTS = INTERRUPTS + 1;
	ii=1; 
        data=0xaabb;
	cbd_rd_gl = (cbd_bas_ladr|29<<11|10<<2);  /*  n=29, f=10, */ 
        reg1 = (int *)cbd_rd_gl;
        pattern = *reg1;
	LAM_REG = pattern;

	for (jj=0;jj<25;jj++)     
	if ((pattern&(1<<jj))&&(lamisrs[jj][ii]!=0))
	{
		ISR_ADDR=(int)lamisrs[jj][ii];
		LAM_NUM=jj;
		(* lamisrs[jj][ii])();
		taskResume(taskidlam[jj][ii]);
	}  
          
}


/*=============================================================================
** Name ccinit.c
** Purpose intialize branch (clear IRR, IMR,  load interrupt mode, clear MLAM,
** give BZ .
*/

void ccinit(b)
       int  b;
{
     int      cbd_csr_r;        /* cbd control status reg. read address */
     int      cbd_icr_lam;      /* cbd interrupt control reg. address */
     int      cbd_ivr_lam;      /* cbd interrupt data reg. address */ 
     int      i,j;
     int      cbd_bz;
     int      temp;
     int      xvect=200;
     int      status;
     register char  *reg1;
     register char  *reg2;
     register short *reg3;
     INIT = 1;
     cbd_csr_r=(cbd_bas_ladr|b<<19|29<<11)|2;      /* n=29, c=0,f=0 */
 status=intConnect((VOIDFUNCPTR *) (xvect<<2), (VOIDFUNCPTR) lamIsr,0);  

     for (i=0;i<3;i++)
     {
	cbd_icr_lam = (cbd_bas_ladr|b<<19|29<<11|(i+5)<<2|3); /* byte address */
	cbd_ivr_lam = (cbd_bas_ladr|b<<19|29<<11|(i+1)<<2|3); /* byte address */
             reg1 = (char *)cbd_icr_lam;
             reg2 = (char *)cbd_ivr_lam;
        *reg1 = am_reset;                 /* isue reset command   */ 
	*reg1 = (am_load04 + am_md_ini);  /* M4=1, M0,M1,M2,M3=0 */ 
	*reg1 = am_clirr;                 /* clear all IRR bits   */
	*reg1 = am_load56;                /* M7=1, M5,M6=0  chips armed */
	for (j=0;j<8;j++)
		{
		    *reg1 = (am_rsmem+j);  /* increment response mem level */
		    *reg2 = (xvect) ;    /* increment vector   */          
		} 
        
      }

      /* clear mlam of cbs csr    */
      reg3 = (short *)cbd_csr_r;
      temp = *reg1;
      temp = temp & ~(0x10);                
      *reg3 = temp;

	/* immplement BZ */
      cbd_bz = (cbd_bas_ladr|b<<19|29<<11|9<<2|2);
      reg3 = (short *)cbd_bz;
      *reg3 = temp;

}

/*============================================================================
** name cclnk.c
** purpose  link routine to lam
*/
 
void cclnk(lam,label)
	int lam;
	VOIDFUNCPTR label;
{
    
	lamisrs[((lam>>11)&31)-1][(lam>>16)&7] = label;
	taskidlam[((lam>>11)&31)-1][(lam>>16)&7] = taskIdSelf(); 
	printf("lamisrs[%d][%d]=%x\n",(lam>>11&31)-1,(lam>>16)&7,label);
}


/*============================================================================
** name cculk.c
** purpose  unlink routine from lam
*/
 
void cculk(lam,label)
	int lam;
	VOIDFUNCPTR label;
{
	lamisrs[((lam>>11)&31)-1][(lam>>16)&7] = 0;

}

/*============================================================================
** name ccrgl.c
** purpose  re-enable graded lam (@end of isr)
*/
 
void ccrgl(lam)
	int lam;

{
     short    temp;
     register short *reg1;
     int      cbd_csr;
     int      gl,b;
     int      cbd_icr_lam;
     /* read cbd csr */
 
     cbd_csr = (cbd_bas_ladr|29<<11|2);
     reg1 = (short *)cbd_csr;
     temp = *reg1;                                     
     gl = (((lam>>11)-1)&31);
     if (gl>23)        /* IT2=1 gl=24, IT4=1 gl=25  */
	{
		if (gl==24)
		temp = temp & ~(0x08);         /* clear MIT2 */ 
		if (gl==25)
        	temp = temp & ~(0x04);         /* clear MIT4 */
		*reg1 = temp;
	}
     else
	{
	b = (lam>>19)&7;
	cbd_icr_lam = (cbd_bas_ladr|b<<19|29<<11|5<<2|2);
	reg1 = (short *)cbd_icr_lam;
	while (gl > 7)
	    {
		cbd_icr_lam += 4;
		gl -= 8;
            }
	*reg1 = am_clisrb | gl;		  /* clear ISR bits */
        *reg1 = am_clirrmrb | gl;         /* clear IRR and IMR bits */
        } 
}
 


/*============================================================================
** name cclwt.c
** purpose  link routine to lam
*/
 
void cclwt(lam)
	int lam;
{
       taskSuspend(0);
       return;
}

