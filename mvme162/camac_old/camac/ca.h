/*-----------------------------------------------------------------------------
 * Copyright (c) 1991,1992 Southeastern Universities Research Association,
 *                         Continuous Electron Beam Accelerator Facility
 *
 * This software was developed under a United States Government license
 * described in the NOTICE file included as part of this distribution.
 *
 * CEBAF Data Acquisition Group, 12000 Jefferson Ave., Newport News, VA 23606
 * Email: coda@cebaf.gov  Tel: (804) 249-7101  Fax: (804) 249-7363
 *-----------------------------------------------------------------------------
 * 
 * Description:
 *	Function prototypes for CAMAC standard routines
 *	
 * Author:  Chip Watson, CEBAF Data Acquisition Group
 *
 * Revision History:
 *   $Log: ca.h,v $
 *   Revision 1.1.1.1  1995/10/22 02:30:13  ted
 *   import from decu18
 *
 *	  Revision 1.1  1992/06/05  19:25:13  watson
 *	  Initial revision
 *
 * 			Added ccgl prototype 03-jun-92 watson
 */

void caopen(char *server,int *success);
void cdreg(int *ext,int b,int c,int n,int a);
void cfsa(int f,int ext,int *data,int *q);
void cssa(int f,int ext,int *data,int *q);
void cccz(int ext);
void cccc(int ext);
void ccci(int ext,int l);
void ctci(int ext,int *l);
void cccd(int ext,int l);
void ctcd(int ext,int *l);
void ccgl(int ext,int l);
void ctgl(int ext,int *l);
void cdlam(int *lam,int b,int c,int n,int a,int *inta);
void cclm(int lam,int l);
void cclc(int lam);
void ctlm(int lam,int *l);
void cclwt(int lam);
void ccinit(int b);
void ctstat(int *istat);
