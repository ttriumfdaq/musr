#include <rpc/rpc.h>
#include "caRpc.h"


bool_t
xdr_CA_BCNA(xdrs, objp)
	XDR *xdrs;
	CA_BCNA *objp;
{
	if (!xdr_int(xdrs, &objp->b)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->c)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->n)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->a)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_FXD(xdrs, objp)
	XDR *xdrs;
	CA_FXD *objp;
{
	if (!xdr_int(xdrs, &objp->f)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->ext)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->dat)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_DQ(xdrs, objp)
	XDR *xdrs;
	CA_DQ *objp;
{
	if (!xdr_int(xdrs, &objp->dat)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->q)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_XL(xdrs, objp)
	XDR *xdrs;
	CA_XL *objp;
{
	if (!xdr_int(xdrs, &objp->ext)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->l)) {
		return (FALSE);
	}
	return (TRUE);
}




bool_t
xdr_CA_LL(xdrs, objp)
	XDR *xdrs;
	CA_LL *objp;
{
	if (!xdr_int(xdrs, &objp->lam)) {
		return (FALSE);
	}
	if (!xdr_int(xdrs, &objp->l)) {
		return (FALSE);
	}
	return (TRUE);
}


