/*
 *	test.c
 *
 * Test bed for CAMAC RPC calls (client program)
 *
 * Author: Chip Watson
 *
 * 11/15/91 cw 5 working calls
 */

main(argc,argv)
     int argc;
     char *argv[];
{
  int ext,b,c,n,a,f,d,q;
  int stat;

  if (argc!=2) {
    printf("catest server\n");
    return;
  }

  caopen(argv[1],&stat);
  if (stat==1)
    printf("connection established to %s\n",argv[1]);
  else {
    printf("failed to establish connection to %s\n",argv[1]);
    return;
  }

  b=0;
  c=1;
  n=3;
  a=0;
  cdreg(&ext,b,c,n,a);
  printf("ext %x b %d c %d n %d a %d\n",ext,b,c,n,a);
  if (ext==0) {
    printf("cdreg call failed, exiting...\n");
    return;
  }

  f=16;
  d=0x123456;
  cfsa(f,ext,&d,&q);
  printf("f %d d %x q %d\n",f,d,q);

  f=0;
  d=0;
  q=0;
  cfsa(f,ext,&d,&q);
  printf("f %d d %x q %d\n",f,d,q);

}
