/* get_int.c get the value of the ISR address and the LAM # */
get_int()
	{
		int init;
		int ints;
		int reg;
		int addr;
		int num;
		GET_INT_INFO(&init,&ints,&reg,&addr,&num);
		printf("The CCINIT flag is %d\n",init);
		printf("# of times lamIsr has been called is %d\n",ints);
		printf("The contents of the LAM register is %x\n",reg);
		printf("Address of int service code is %x\n",addr);
		printf("The LAM number is %d\n",num);
	}
