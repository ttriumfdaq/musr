/* main_int.c   a example program to test intrrupt routines   */

typedef void (*VOIDFUNCPTR) ();
typedef unsigned long (*FUNCPTR) ();
void  trigger();
VOIDFUNCPTR onTrigger = trigger;
int   q, x, istat;
int   lam, ext,l;
main_int()
{
int *dumstat;
int inta[2]; 
int eventn;
int lockKey;

	lockKey = intLock();
	cdreg(&ext,0,1,30,10);
	printf("enb BD addr is %x\n",ext);
	cccd(ext,1);          /* enable camac BD */
	cdlam(&lam,0,1,23,0,inta);       /* define lam */
	cclnk(lam,(VOIDFUNCPTR)trigger); /* link lam to trigger() */
	printf("Addr of TRIGGER is %x\n",(VOIDFUNCPTR)trigger);

	
	cdlam(&lam,0,1,22,0);
	cclm(lam,1);          /* enable lam */

	cdreg(&ext,0,1,22,2);
	eventn=5;
	cfsa(16,ext,&eventn,&q);    /* write event number--set lam */
	intUnlock(lockKey);
}

void trigger()
{
int  *dumstat;
	cdreg(&ext,0,0,22,0);
	cfsa(25,ext,&dumstat,&q);     
	
return;
}

