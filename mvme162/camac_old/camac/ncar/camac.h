/* camac.h - camac access library header file */

#ifndef INCcamac_h
#define INCcamac_h

/*
written by;

D'Anne Thompson
National Optical Astronomy Observatories
P.O. Box 26732
Tucson, AZ 85726
(602) 325-9335
dat@noao.edu  {...}!uunet!noao.edu!dat
*/


/*
modification history
--------------------
01c 29may89,dat - cleaned up for exporting.
01b 17feb89,dat - converted to vxWorks
01a xxoct88,dat - original (intermetrics)
*/

#include "lstLib.h"	/* linked lam lists */
#include "semLib.h"	/* lam semaphores */
#include "ces8210.h"	/* camac status bit definitions */

/*
This header file defines the interface to both the 'camacLib' and
the 'camacESONE' modules.
*/


/**************************************************************
*
* camacLib interface definitions
*
* This section describes the interface to the camacLib module.
*
*/

typedef USHORT CAM_STAT;	/* camac status word */
typedef ULONG CAM_REF;		/* camac address pointer */

IMPORT CAM_REF	camacBuildRef();/* (b, c, n, a, f, size) */
IMPORT STATUS	camacIn();	/* (cam_ref, pData, pStatus) */
IMPORT STATUS	camacOut();	/* (cam_ref, data, pStatus) */
IMPORT STATUS	camacTest();	/* (cam_ref, pStatus) */
IMPORT STATUS	camacProbe();	/* (cam_ref) */

/* Macro to check for good status - Must have either X or Q */
#define CAM_STAT_OK(status) ((status)&(CAM_X_BIT|CAM_Q_BIT))


/******************************************************************
*
* camacESONE module interface
*
*/

/*
 * The lam handler is called as follows:
 *    (pLam->handler) (pLam->arg, pLam)
 * the handler gets the optional argument, and a pointer to
 * the lam structure.
 * The handler returns a BOOL value. It returns TRUE if it
 * successfully handled the LAM condition and reset the LAM.
 * It returns FALSE if it didn't.  The master interrupt handler
 * will attempt to clear the LAM with camacCLM (lam) if the
 * user handler returns FALSE.
 *
 * ESONE LAM handling routines.
 *
 * intArray[] is REQUIRED.
 * intArray[0] is the Graded Lam #,  (0 --> use 'n' from bcna)
 * intArray[1] is the optional mailbox address. (use NULL for no mailbox)
 */
typedef struct {
	NODE	node;
	CAM_REF	bcna;	 /* f=0, size=LONG */
	short	bitnum;	 /* 0--> sub_address lam */
	short	bglam;
	SEM_ID	sem;	 /* given upon LAM occurrence */
	FUNCPTR	handler; /* 1=success, 0=failure */
	int	arg;
} LAM_DATA;
typedef LAM_DATA	*CAM_LAM;
/* macros for scanning lam lists */
#define	lamNext(x)	((CAM_LAM)lstNext((NODE *)(x)))
#define	lamFirst(x)	((CAM_LAM)lstFirst(x))


/* IMPORT CAM_REF camacDREG ();	/* (b,c,n,a) */
IMPORT STATUS	camacSSA ();	/* (f, reg, pShort, pStatus) */
IMPORT STATUS	camacFSA ();	/* (f, reg, pLong, pStatus) */
IMPORT STATUS	camacCCC ();	/* (reg) */
IMPORT STATUS	camacCCZ ();	/* (reg) */
IMPORT STATUS	camacECI ();	/* (reg, flag) */
IMPORT STATUS	camacECD ();	/* (reg, flag) */
IMPORT BOOL	camacTCI ();	/* (reg) */
IMPORT BOOL	camacTCD ();	/* (reg) */
IMPORT BOOL	camacTGL ();	/* (reg) */
IMPORT CAM_LAM	camacDLAM ();	/* (b, c, n, m, intArray) */
IMPORT STATUS	camacCLC ();	/* (lam) */
IMPORT STATUS	camacELM ();	/* (lam, flag) */
IMPORT STATUS	camacLNK ();	/* (lam, pFunc, arg) */
IMPORT BOOL	camacTLM ();	/* (lam) */

/* camacDREG is really just a macro to camacBuildRef() */
#define camacDREG(b,c,n,a) camacBuildRef(b,c,n,a,0,CAM_LONG)

#endif /* !INCcamac_h */
