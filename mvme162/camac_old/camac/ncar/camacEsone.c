/* camacEsone.c  - ESONE library of CAMAC functions */

/*
written by;

D'Anne Thompson
National Optical Astronomy Observatories
P.O. Box 26732
Tucson, AZ 85726
(602) 325-9335
dat@noao.edu  {...}!uunet!noao.edu!dat
*/


/*
modification history
--------------------
01d 30may89,dat - changed mailboxes for semaphores.
01c 29may89,dat - fixed up for exporting
01b 04apr89,dat - fixed camacELM, so it re-enables crate Controller
		with each enable operation.
01a 28oct88 dat	written
*/

/*
This module provides the functionality of the ESONE "Subroutines
For Camac" DOE/EV-0016.  Block transfer and scan functions are
not yet implemented.  The calling sequences have been modified
as little as possible to be consistent with a C language
interface.  The original specification is for a FORTRAN interface.

The ESONE standard function names have all been prepended with
the module name camac, dropping the initial C character.
In the case of functions that take a boolean flag to enable or
disable some function, the second character of C has been
changed to E to more clearly define the function.
e.g. Function CCLM becomes camacELM (camac enable lam).

The initialization routine camacEsoneInit must be called prior
to using any other functions within this module.

PROBLEMS:
1) Taking a crate off-line for some work and then returning it to the system
causes many problems.
The current solution is to do a branch initialize
which disables lams throughout the branch.
Perhaps lam enabled status should be kept in the lam structure
so that we could scan through the list and re-activate lams that
were previously enabled.

2) Block transfer and scan functions are not yet implemented.
*/

#include "vxWorks.h"
#include "iv68k.h"	/* interrupt routines */
#include "vmeConfig.h"	/* for 'CAM_MAX_BRANCH' */
#include "camac.h"	/* General camac interface definitions */
#undef camacDREG	/* we will define a non-macro version of CDREG */
#include "am9519.h"	/* vector generators on board ces8210 */


IMPORT char *	malloc ();
IMPORT		free ();


/* External References (Other than #includes) */
IMPORT FUNCPTR	intHandlerCreate ();
IMPORT STATUS	intConnect ();
IMPORT VOID	intVecSet ();
IMPORT CES8210  *camacBaseAddr;  /* local bus address */

/* Locally Defined Globals.  */
VOID	camacEsoneInit ();	/* () */
CAM_REF camacDREG ();		/* (b, c, n, a) */
STATUS	camacSSA ();		/* (f, reg, pShort, pStatus) */
STATUS	camacFSA ();		/* (f, reg, pLong, pStatus) */
STATUS	camacCCC ();		/* (reg) */
STATUS	camacCCZ ();		/* (reg) */
STATUS	camacECI ();		/* (reg, flag) */
STATUS	camacECD ();		/* (reg, flag) */
BOOL	camacTCI ();		/* (reg) */
BOOL	camacTCD ();		/* (reg) */
BOOL	camacTGL ();		/* (reg) */
CAM_LAM	camacDLAM ();		/* (b, c, n, m, intArray) */
STATUS	camacLNK ();		/* (lam, pFunc, arg) */
STATUS	camacCLC ();		/* (lam) */
STATUS	camacELM ();		/* (lam, flag) */
BOOL	camacTLM ();		/* (lam) */

/* Local Symbols. (Forward References) */
LOCAL STATUS	camacBrInit ();		/* (brNum, vecNum) */
LOCAL VOID	camacInterrupt();	/* common interrupt handler */
LOCAL STATUS	camacEGLAM();		/* (bglam, flag) */
LOCAL BOOL	br_init [CAM_MAX_BRANCH] = {0};
LOCAL int 	br_vec [CAM_MAX_BRANCH] = {0};
LOCAL LAM_DATA lamIt2 = 	/* LAM structure for IT2 */
	{ {0,0}, 25, 0, 0, 0, 0};
LOCAL LAM_DATA lamIt4 =	/* lam structure for IT4 */
	{ {0,0}, 26, 0, 0, 0, 0};
LOCAL	LIST	listIt2;
LOCAL	LIST	listIt4;
LOCAL	LIST	lamList[CAM_MAX_BRANCH][24];


/*************************************************************
*
* camacEsoneInit - initialize all branch controllers
*
* This function must be called prior to any use of the esone
* standard routines.  This will initialize the interrupt controllers
* on the driver boards and will allow generation of lam interrupts.
*/

VOID camacEsoneInit ()

    {
    camacBrInit (0,CAMAC_BR0_INUM);
#if CAM_MAX_BRANCH > 1
    camacBrInit (1,CAMAC_BR1_INUM);
#if CAM_MAX_BRANCH > 2
    camacBrInit (2,CAMAC_BR2_INUM);
#if CAM_MAX_BRANCH > 3
    camacBrInit (3,CAMAC_BR3_INUM);
#if CAM_MAX_BRANCH > 4
    camacBrInit (4,CAMAC_BR4_INUM);
#if CAM_MAX_BRANCH > 5
    camacBrInit (5,CAMAC_BR5_INUM);
#if CAM_MAX_BRANCH > 6
    camacBrInit (6,CAMAC_BR6_INUM);
#if CAM_MAX_BRANCH > 7
    camacBrInit (7,CAMAC_BR7_INUM);
#endif
#endif
#endif
#endif
#endif
#endif
#endif
    }

/*************************************************************
*
* camacBrInit - initialize a single branch controller
*
* This local routine is used to initialize a single branch driver.
* It will initialize all 3 am_9519 interrupt vector generators.
* It will also send a branch Z signal to reset all crates on
* that branch.  A previously initialized branch will just receive
* a Z signal and enabling of Glam scanning.
*
* Vectors are assigned in priority order.
* The first vector is associated with GL1, the highest priority graded lam
* The IT2 and IT4 vectors are assigned in hardware, and are
* common to all branches.
*
* RETURNS:
* Returns ERROR for invalid arguments, or an attempt to redefine
* the interrupt vector for a previously initialized controller.
* ERROR will also be returned if the controller is not present.
* Malloc failures can also cause an ERROR return.
*/

LOCAL STATUS camacBrInit (brNum, vectorNum)
    int	brNum;	   /* branch number 0-7 */
    int vectorNum; /* base vector (24 total) */

    {
    LOCAL FUNCPTR	handlerIt2 = NULL;
    LOCAL FUNCPTR	handlerIt4 = NULL;
    int			dummy;
    CES8210		*pBoard;

    if (brNum >= CAM_MAX_BRANCH)
	{
	/*HELP: set error code BAD_PARAMETERS*/
	return ERROR;
	}

    pBoard = camacBaseAddr + brNum;

    if (vxMemProbe ((char *)&(pBoard->BCSR), READ,
		    sizeof(pBoard->BCSR), (char *)&dummy) == ERROR)
	return ERROR;

    if (br_init [brNum] == TRUE)
	{
	if (vectorNum != br_vec [brNum])
	    {
	    /* Attempt to redefine with different vector set */
	    return ERROR;
	    }
	pBoard->BZ   = 0;
	pBoard->BITF = 0;
	pBoard->BCSR &= ~(CAM_MTO|CAM_MLAM);
	return OK;
	}

    if (handlerIt2 == NULL)
	{
	lstInit (&listIt2);
	lstAdd (&listIt2, &(lamIt2.node));
	handlerIt2 = intHandlerCreate (camacInterrupt, (int)(&listIt2));
	if (handlerIt2 == NULL)
	    return ERROR;
	intVecSet ((FUNCPTR *)INUM_TO_IVEC (CAMAC_IT2_INUM), handlerIt2);
	}

    if (handlerIt4 == NULL)
	{
	lstInit (&listIt4);
	lstAdd (&listIt4, &(lamIt4.node));
	handlerIt4 = intHandlerCreate (camacInterrupt, (int)(&listIt4));
	if (handlerIt4 == NULL)
	    return ERROR;
	intVecSet ((FUNCPTR *)INUM_TO_IVEC (CAMAC_IT4_INUM), handlerIt4);
	}

    /*
     * Init the three interrupt vector generators.  The vectors
     * are assigned in priority order.  The first vector is
     * associated with GL1, the highest priority Glam.  The
     * IT2 and IT4 vectors are assigned in hardware, and are
     * common to all branches.
     */
    {
	LIST 		*pList = &(lamList[brNum][0]);
	FAST short	*pDataReg = &(pBoard->BD1);
	FAST short	*pCmdReg = &(pBoard->BC1);
	FAST int	i,j;
	int		tempVec = vectorNum;

	for (i=0; i<3; i++)
	    {
	    *pCmdReg = AM_RESET;
	    *pCmdReg = AM_LOAD04 | AM_04INIT;
	    *pCmdReg = AM_CLIRR;
	    *pCmdReg = AM_LOAD56 | AM_56INIT;
	    for (j=0; j<8; j++)
		{
		lstInit (pList);
		if (intConnect ((FUNCPTR *)INUM_TO_IVEC(tempVec),
			    camacInterrupt, (int)(pList++)) == ERROR)
		    return ERROR;
		*pCmdReg = AM_WRMEM | AM_8BIT | j;
		*pDataReg = tempVec++ ;
		}
	    pCmdReg += 2;
	    pDataReg += 2;
	    }
    }

    /*
     * Branch successfully initialized.  Set the br_init flag and
     * then send a branch Z command to reset all crates.
     * Enable branch Glam scanning, The driver will scan for Glam
     * every 10 micro-seconds while the BD signal is active.
     * Specific Glam interrupts are enabled/disabled by the camacEGLAM()
     * routine.
     */
    br_init [brNum] = TRUE;
    br_vec [brNum] = vectorNum;
    pBoard->BZ   = 0; /* NECESSARY ?? */
    pBoard->BITF = 0;
    pBoard->BCSR &= ~(CAM_MTO|CAM_MLAM);

    return OK;
}

/*************************************************************
*
* camacEGLAM - enable/disable graded lam level (non-Esone)
*
* Enable/Disable individual Graded lam from an encoded bglam value.
* This sets/clears the interrupt mask bits in the appropriate
* vector generator modules.
* (For IT2 and IT4 it set/clears mask bits in the csr register
* for branch #0).
*
* The branch number and lam number are encoded together as an int.
* Shift the branch number left 5 bits and 'or' in the lam number
* to encode.  Lam numbers are one based, with special codes for
* IT2 and IT4. IT2 is lam number 25, while IT4 is lam number 26.
*
* RETURNS:
* Returns ERROR if the referenced branch is not initialized.
*/

LOCAL STATUS camacEGLAM (bglam, flag)
    int	 bglam; /* encoded branch & lam number */
    BOOL flag;	/* TRUE-> enable, FALSE->disable */

    {
    FAST int		glam;
    FAST CES8210 *	pBoard;
    int			brNum = bglam/32;

    glam = (bglam%32) - 1;	/* 0 - 25 */

    if (glam > 23)
	{
	/* Set/Clr Special interrupts. IT2 and IT4. (Branch 0) */
	if (br_init [0] == FALSE)
	    return ERROR;
	pBoard = camacBaseAddr;
	if (flag == TRUE)
		pBoard->BCSR  &= (glam==24 ? ~CAM_MIT2 : ~CAM_MIT4) ;
	else
		pBoard->BCSR  |= (glam==24 ? CAM_MIT2 : CAM_MIT4) ;
	}
    else
	{
	/*
	 * Always clear the ISR flag.  To enable interrupts
	 * clear both the IMR and IRR bits.  To disable, just
	 * set the IMR bit.
	 */
	FAST short *	pCmdReg;

	if (br_init [brNum] == FALSE)
	    return ERROR;

	pBoard = camacBaseAddr + brNum;

	pCmdReg = &(pBoard->BC1);
	while (glam > 7)
	    {
	    pCmdReg += 2;
	    glam -= 8;
	    }
	*pCmdReg = AM_CLISRB | glam ;
	*pCmdReg = ((flag == TRUE ? AM_CLIMRIRRB : AM_STIMRB) | glam) ;
	}

    return OK;
    }


/*************************************************************
*
* camacInterrupt - camac graded lam interrupt service routine
*
* Each Graded lam vectors to this routine with
* a pointer to the linked list of lams on the stack.  This routine
* searches the lam list for an active lam and executes the associated
* routine.  The lam handler routine should return TRUE if the routine 
* was successful in servicing (and clearing) the lam.  If the routine was
* not successful, then we must assume the lam (interrupt) is still
* asserted.  This routine will then try to disable the lam using
* camacCLM().  If camacCLM() is unsuccessful, then the only graceful way
* to exit is to report a major error and disable the complete graded lam
* for that branch.
*/

LOCAL VOID camacInterrupt (pList)
    LIST *	pList; /* A pointer to the first lam structure */

    {
    BOOL	temp;
    BOOL	retval = FALSE;
    FAST CAM_LAM pLam = lamFirst (pList);
    int		bglam;

    if (pLam == 0)
	{
	/*ERROR: No defined lam for this interrupt */
	/*ERROR: Major system fault, don't even know what bglam this is. */
	return;
	}

    bglam = pLam->bglam;

    /* Search through the list for any and all active lam. */
    while (pLam != NULL)
	{
	if (camacTLM (pLam) == TRUE)
	    {
	    /* We have an active lam. */
	    temp = FALSE;
	    if ((pLam->handler) != 0)
		temp = (pLam->handler)(pLam->arg, pLam);

	    /*
	     * If handler didn't do it, then we must try to clear the
	     * lam condition, if that fails try to disable it.
	     */
	    if (temp == FALSE)
		{
		if (camacCLC (pLam) == OK
		 || camacELM (pLam,0) == OK
		 )
			retval = TRUE;
		}
	    else
		retval = TRUE;

	    /* signal semaphore upon completion of handler routines. */
	    if (pLam->sem)
		semGive (pLam->sem);
	    }
	pLam = lamNext (pLam);
	}

    /*
     * We were unable to process any lam attached to this Glam.
     * We must therefore step back and PUNT.  Lets report a 
     * major system error and then disable this complete BGlam.
     */
    if (retval == FALSE) 
	{
	/***ERROR: Unsuccessful Camac Interrupt ***/
	(void) camacEGLAM (bglam, 0);
	}
    else
	(void) camacEGLAM (bglam, 1); /* Is this necessary ? */

    return;
    }


/************************************************************
*
* camacDREG - define a camac register (CDREG)
*
* This routine creates an ESONE standard register reference
* and returns it to the caller.  The register reference
* includes branch number, crate number, slot number, and module
* sub-address. It does not include function code or size
* information.
*
* RETURNS:
* The ESONE standard reference is returned in the form of a CAM_REF.
* (See camacBuildRef).  NULL is returned for invalid arguments.
*
* SEE ALSO
* camacBuildRef
*/

CAM_REF camacDREG (b, c, n, a)
    int b; /* branch */
    int c; /* crate */
    int n; /* slot */
    int a; /* sub-address */

    {
    /* use F0 and CAM_LONG to finish out the reference */
    return camacBuildRef (b, c, n, a, 0, CAM_LONG);
    }

/*********************************************************
*
* camacSSA - camac short single action (CSSA)
*
* Execute a short (16 bit) single action to a camac register.
*
* The transaction status is stored at the location given by
* pStatus. If pStatus is zero or NULL, then the
* camac status word is discarded.
*
* RETURNS:
* Returns ERROR if the camac cycle fails (no-X and no-Q).
*/

STATUS	camacSSA (f, reg, pShort, pStatus)
    int	     f;	       /* function code, 0-31 */
    CAM_REF  reg;      /* register reference */
    short    *pShort;  /* data pointer */
    CAM_STAT *pStatus; /* result status pointer */

    {
    STATUS result;

    reg &= CAM_BCNA_MASK;
    reg |= (f << CAM_F_SHIFT) | CAM_SHORT;

    if (f & 8)
	result = camacTest (reg, pStatus);
    else if (f < 16)
	result = camacIn (reg , (char *)pShort, pStatus);
    else
	result = camacOut (reg, (ULONG)*pShort, pStatus);

    return result;
    }


/*********************************************************
*
* camacFSA - camac full (24 bit) single action (CFSA)
*
* Execute a full (24-bit) single action to/from a camac register.
*
* The transaction status is stored at the location given by
* pStatus. If pStatus is zero or NULL, then the
* camac status word is discarded.
*
* RETURNS:
* Returns ERROR if the camac cycle fails (no-X and no-Q).
*/

STATUS	camacFSA (f, reg, pLong, pStatus)
    int	     f;		/* function code */
    CAM_REF  reg;	/* register reference */
    long     *pLong;	/* data pointer */
    CAM_STAT *pStatus;	/* pointer to status result */

    {
    STATUS result;

    reg &= CAM_BCNA_MASK;
    reg |= (f << CAM_F_SHIFT) ;

    if (f & 8) 
	result = camacTest (reg, pStatus);

    else if (f < 16)
	result = camacIn (reg, (char *)pLong, pStatus);

    else
	result = camacOut (reg, (ULONG)*pLong, pStatus);
    
    return result;
    }


/**************************************************************
*
* camacCCC - generate a dataway clear cycle (CCCC)
*
* Causes a dataway clear cycle to be generated by the crate controller.
* The camRef argument may be any valid register reference within
* the crate.  Slot number and sub-address are masked off by the
* routine.
*
* Type A-1 and A-2 parallel highway crate controllers respond
* to N28A8F26 by generating a dataway clear cycle.
*
* RETURNS:
* Returns the status code from the underlying camacTest operation.
* See camacTest.
*/

STATUS	camacCCC (camRef)
    FAST CAM_REF camRef; /* any valid reference within crate */

    {
    camRef &= CAM_BC_MASK;
    camRef |= (28 << CAM_N_SHIFT) + (8 << CAM_A_SHIFT) + (26 << CAM_F_SHIFT);
    return (camacTest (camRef, (CAM_STAT *)NULL));
    }


/**************************************************************
*
* camacCCZ - generate a dataway initialize cycle (CCCZ)
*
* Causes a dataway initialize (Z) cycle to be generated by the crate controller.
* The camRef argument may be any valid register reference within
* the crate.  Slot number and sub-address are masked off by the
* routine.
*
* Type A-1 and A-2 parallel highway crate controllers respond
* to N28A9F26 by generating a dataway initialize cycle.
*
* RETURNS:
* Returns the status code from the underlying camacTest operation.
* See camacTest.
*/

STATUS	camacCCZ (camRef)
    CAM_REF camRef; /* any valid reference within crate */

    {
    camRef &= CAM_BC_MASK;
    camRef |= (28 << CAM_N_SHIFT) + (9 << CAM_A_SHIFT) + (26 << CAM_F_SHIFT);
    return (camacTest (camRef, (CAM_STAT *)NULL));
    }


/***********************************************************
*
* camacECI - assert/release crate controller inhibit (CCCI)
*
* Controls the crate controller's output to the dataway inhibit line.
*
* The dataway inhibit line is a wire-or'd open-collector line
* common to all slot positions.  Any module can drive the line
* active by pulling down on it (low true).  At dataway
* initialize time, regular modules must release the line, but
* the crate controller will assert it.
*
* The camacECI function controls only the crate controller's
* assertion of the inhibit line.  All modules within the crate
* must release the inhibit line for the line to go inactive (high).
*
* Type A-1 and A-2 crate controllers respond to N30A9F24 by
* asserting the inhibit line.  An N30A9F26 command will cause
* them to release the inhibit line.
*
* RETURNS:
* The return code indicates success/failure of the actual camac cycle.
* See camacTest.
*/

STATUS	camacECI (camRef, flag)
    CAM_REF camRef; /* any valid crate address */
    BOOL    flag;   /* enable/disable code */

    {
    camRef &= CAM_BC_MASK;
    camRef |= (30 << CAM_N_SHIFT) + (9 << CAM_A_SHIFT)
		+ (flag ? (26 << CAM_F_SHIFT) : (24 << CAM_F_SHIFT));
    return (camacTest (camRef, (CAM_STAT *)NULL));
    }


/**********************************************************
*
* camacTCI - test crate inhibit line (CTCI)
*
* Returns the current status of the dataway inhibit line.
* Inhibit status is determined by accessing N30A9F27 through
* the type-A crate controller.  The controller will respond with Q
* equal to the current state of the inhibit line.
*
* RETURNS:
* Returns the state of the dataway inhibit line.
* FALSE is returned upon any error conditions.
*/

BOOL	camacTCI (camRef)
    CAM_REF camRef; /* any reference into crate */

    {
    CAM_STAT status = 0;

    camRef &= CAM_BC_MASK;
    camRef |= (30 << CAM_N_SHIFT) + (9 << CAM_A_SHIFT) + (27 << CAM_F_SHIFT);
    camacTest (camRef, &status);
    return (status & CAM_Q_BIT ? TRUE : FALSE);
    }


/*********************************************************
*
* camacECD - enable/disable crate controller demands (CCCD)
*
* Enable/disable the crate controller's ability to generate
* a branch demand (graded lam).
* Type-A controller access at N30A10F26 will enable demands.
* Access at N30A10F24 will disable demand generation.
*
* RETURNS:
* Returns the camac cycle success/fail code.
* See camacTest.
*/

STATUS	camacECD (camRef, flag)
    CAM_REF camRef; /* any reference within the crate */
    BOOL    flag;   /* enable/disable flag */

    {
    camRef &= CAM_BC_MASK;
    camRef |= (30 << CAM_N_SHIFT) + (10 << CAM_A_SHIFT)
		+ (flag ? (26 << CAM_F_SHIFT) : (24 << CAM_F_SHIFT));
    return (camacTest (camRef, (CAM_STAT *)NULL));
    }


/**************************************************************
*
* camacTCD - test crate controller demand enable (CTCD)
*
* Returns the crate controller's demand enable status.
* Demand enable status is determined by accessing N30A10F27 through
* the type-A crate controller.  The controller will respond with Q
* equal to the demand enable state.
*
* RETURNS:
* Returns TRUE if crate demands are enabled.
*/

BOOL	camacTCD (camRef)
    CAM_REF camRef; /* any reference within the crate */

    {
    CAM_STAT status = 0;

    camRef &= CAM_BC_MASK;
    camRef |= (30<<CAM_N_SHIFT) + (10 << CAM_A_SHIFT) + (27 << CAM_F_SHIFT);

    camacTest(camRef, &status);

    return (status & CAM_Q_BIT ? TRUE : FALSE);
    }

/*************************************************************
*
* camacTGL - test crate graded lam status (CTGL)
*
* Returns TRUE if the crate controller is receiving a graded lam
* signal from any module within the crate.
* This is determined by accessing N30A11F27 through
* the type-A crate controller.  The controller will respond with Q
* equal to the graded lam signal state.
*
* The returned status does not depend upon crate demand enable.
*
* RETURNS:
* Returns TRUE if a graded lam is present within the crate.
*/

BOOL	camacTGL (camRef)
    CAM_REF camRef; /* any reference into the crate */

    {
    CAM_STAT status = 0;

    camRef &= CAM_BC_MASK;
    camRef |= (30 << CAM_N_SHIFT) + (11 << CAM_A_SHIFT) + (27 << CAM_F_SHIFT);

    camacTest(camRef, &status);

    return (status & CAM_Q_BIT ? TRUE : FALSE);
    }

/************************************************************
*
* camacDLAM - define a camac lam (look-at-me)
*
* Creates a camac lam data structure.  The user only receives a pointer
* to the structure.  If the lam has been previously defined, the
* new definition overwrites the old definition.
*
* The module lam code argument identifies the lam as either a sub-address lam
* or as a bit-address lam.  Sub-address lam codes are in the range from
* zero to 15.  Bit-address lam codes are in the range -1 to -24.
*
* A sub-address lam is manipulated using F codes F8, F10, F24 and F26
* at the sub-address referenced.  A bit-address lam is controlled
* by bit positions of the group 2 registers at sub addresses A12, A13
* and A14. (A12 is the lam source, A13 the mask register, and A14 is
* the masked lam sources).
*
* Like the FORTRAN version, argument inta[] exists to pass implementation
* specific data to the module.  In this implementation, inta[0] represents
* the graded lam number.  A graded lam value of zero signifies that
* the graded lam number is identical to the module slot number 'n'.
* Inta[1] is the address of an optional semaphore.  If non-NULL, the
* semaphore will be signalled with a semGive with each lam occurrence.
*
* RETURNS:
* Returns a CAM_LAM item (pointer) if successful, returns NULL upon error.
*/

CAM_LAM camacDLAM (b, c, n, m, inta)
    int	b; 	/* branch number (0-7) */
    int	c; 	/* crate number (1-7) */
    int	n; 	/* slot number (1-23) */
    int	m; 	/* module lam code */
    int inta[]; /* graded lam number and optional sempahore */

    {
#define GLAM inta[0]	/* Graded lam, for this lam, MUST BE PRESENT*/
#define SEMAPHORE inta[1]/* Semaphore to signal upon lam occurrence */
    LAM_DATA	*pLam;
    CAM_REF	bcna;
    int		bitnum;

    if (br_init[b] != TRUE)
	    return (CAM_LAM) NULL;

    if (GLAM == 0)
	GLAM = n;	/* Default is slot number 1-23 */

    if ((GLAM == 25) || (GLAM == 26))
	{
	/*
	 * IT2 or IT4 These pseudo-lams are predefined.  The exit
	 * routine will update the semaphore and handler routine
	 * items.
	 */
	pLam = (GLAM==25 ? &lamIt2 : &lamIt4);
	goto exit;
	}
    else
	{
	/* Step 1, Validate the input parameters. */
	if (b<0 || b>=CAM_MAX_BRANCH || c<1 || c>7 || n<1 || n>23
	 || m<-32 || m>15 || GLAM<1 || GLAM>24)
	    {
	    /***ERROR: "Invalid parameters" ***/
	    return (CAM_LAM) NULL;
	    }
	pLam = lamFirst (&(lamList[b][ GLAM - 1])) ;
	}
    
    /*
     * generate BCNA for the lam.
     * Bit position lam are recorded with A=0.
     * generate flags for the lam.
     */
    bcna = camacDREG (b, c, n, (m<0 ? 0 : m));
    bitnum = (m<0 ? -m : 0);

    /*
     * Search the lam list for a match to BCNA.
     * The list is ordered by bcna as the major key, with bitnum as
     * the minor key.
     */
    while (pLam && (pLam->bcna < bcna))
	pLam = lamNext (pLam);

    /*
     * Continue the search on the minor key (only for bit position
     * lams).
     */
    if (pLam && bitnum)
	while (pLam && (bcna==pLam->bcna) && (bitnum > pLam->bitnum))
	    pLam = lamNext (pLam);

    /*
     * Insert a completely new lam structure ?
     */
    if (!pLam || (bcna != pLam->bcna) || (bitnum != pLam->bitnum))
	{
	CAM_LAM	pNew = (CAM_LAM) malloc (sizeof(LAM_DATA));

	if (pNew == NULL)
	    return (CAM_LAM) NULL;

	pNew->bcna = bcna;
	pNew->bitnum = bitnum;
	pNew->bglam = b*32 + GLAM;

	lstInsert (&(lamList[b][GLAM-1]),
		(pLam ? lstPrevious((NODE *)pLam) : (NODE *)NULL),
		(NODE *)pNew);

	pLam = pNew;
	}

    /* Enable the crate controller for this lam */
    camacECD (pLam->bcna, 1);

    /*
     * Before returning, we must insure that the GLAM is enabled.
     * Update the parameter fields in old lam structures.
     */
exit:
    pLam->sem = (SEM_ID) SEMAPHORE;
    pLam->handler = (FUNCPTR) NULL;
    pLam->arg = 0;
    if (camacEGLAM (pLam->bglam, 1) == ERROR)
	return (CAM_LAM) NULL;

    return (pLam);
    }


/*************************************************************
*
* camacLNK - link a function to a lam (CLNK)
*
* This routine associates a C-callable function with a lam signal
* (interrupt).
*
* When enabled and active a lam (as defined by camacDLAM) generates
* an interrupt.  The camacLNK routine will connect the user
* specified function with the processing of that interrupt.
*
* The function will be invoked, in interrupt context, with
* two arguments.  The first argument is 'arg', the procedure
* argument specified in the camacLNK function.  The CAM_LAM
* data structure pointer is the second argument.
*
* The  user procedure must return a boolean result indicating that
* the lam has been properly serviced (TRUE).  A FALSE return
* value indicates that the lam was not serviced and is still active.
* Under that situation the system code will attempt to disable the
* lam from generating further interrupts.
*
* .CS
*      BOOL function (arg, pLam)
*          int arg;
*          CAM_LAM pLam;
*
*      Returns TRUE if lam was properly serviced, FALSE otherwise.
* .CE
*
* NOTES:
* Be aware that the user procedure is executed in interrupt context.  All
* of the warnings about restricted system calls apply.
*
* RETURNS:
* The camacLNK function normally returns OK.
* ERROR is returned for invalid arguments.
*/

STATUS	camacLNK (pLam, proc, arg)
    CAM_LAM pLam; 	/* lam pointer */
    BOOL    (*proc)();	/* procedure pointer*/
    long    arg;  	/* procedure argument */

    {
#define VALID_LAM 1 /*** HELP: Need better validation ***/
    if (VALID_LAM)
	{
	pLam->handler = proc;
	pLam->arg = arg;
	return OK;
	}
    else
	return ERROR;
#undef	VALID_LAM
    }


/*******************************************************
*
* camacTLM - test lam status (CTLM)
*
* Test if a lam is active and enabled.
*
* Sub address lams are tested using F8 at the correct sub-address.
* Bit-address lams are tested by examining the proper bit
* of register A14 (F1A14).
*
* RETURNS:
* Returns TRUE if the lam is active and enabled.
*/

BOOL	camacTLM (pLam)
    CAM_LAM	pLam;

    {
    FAST CAM_REF	addr;

    addr = pLam->bcna;

    if (pLam->bitnum)
	{
	/*
	 * Bit position type lam.  Test by examining bit bitnum
	 * in the group 2 register at A14.
	 */
	FAST int mask;
	long	bits;

	mask = 1 << (pLam->bitnum - 1);
	addr += 14 << CAM_A_SHIFT + 1 << CAM_F_SHIFT;

	if (camacIn (addr, (char *)&bits, (CAM_STAT *)NULL) == ERROR)
	    return FALSE;

	return ((bits & mask) ? TRUE : FALSE);
	}
    else
	{
	CAM_STAT status = 0;
	/*
	 * Sub address type lam.  Use F8 to test lam status.
	 * The lam status is returned as Q.
	 */
	camacTest (addr + (8 << CAM_F_SHIFT), &status);
	return (status & CAM_Q_BIT) ? TRUE : FALSE;
	}
    }


/**************************************************************
*
* camacCLC - clear lam condition (CCLC)
*
* Clears a lam condition if possible.  Sub-address lams are cleared
* using F10.  Bit-address lams are cleared by selectively clearing
* the proper bit of register A12 (F23A12).
*
* RETURNS:
* The status code of the underlying action cycle is returned as
* the result of this function.  See camacOut and camacTest.
*/

STATUS	camacCLC (pLam)
    CAM_LAM	pLam;

    {
    FAST CAM_REF	addr;
    FAST STATUS	retval;

    addr = pLam->bcna;
    if (pLam->bitnum)
	{
	/*
	 * Bit position type lam.  Clear by resetting
	 * bit bitnum of the group 2 register at address 12.
	 * (A=12, F=23, CAM_LONG)
	 */
	FAST int mask;

	mask = 1 << (pLam->bitnum - 1);
	retval = camacOut (addr + (12 << CAM_A_SHIFT) + (23 << CAM_F_SHIFT),
			(ULONG)mask, (CAM_STAT *)NULL);
    	}
    else
	{
	/*
	 * Sub address type lam.  Use F10 to clear lam status.
	 */
	retval = camacTest (addr + (10 << CAM_F_SHIFT), (CAM_STAT *)NULL);
	}

    return (retval);
    }


/*****************************************************************
*
* camacELM - enable/disable a lam (CCLM)
*
* Enable or disable lam.
* Sub-address lams are enabled with F26 and disabled with F24.
*
* Bit-address lams are enabled by setting the bit in the enable
* register A13 (F19A13).
* They are disabled by selectively clearing the bit (F23A13).
*
* RETURNS:
* The return code is that of the underlying camac cycle.
* See camacTest and camacOut.
*/

STATUS	camacELM (pLam, flag)
    CAM_LAM pLam; /* lam pointer */
    BOOL    flag; /* enable/disable flag */

    {
    FAST CAM_REF	addr;
    FAST STATUS	retval;

    addr = pLam->bcna;
    if (pLam->bitnum)
	{
	/***
	Bit position type lam.  Update by setting/clearing
	bit 'bitnum of the group 2 register at address 13.
	F19A13 to set lam enable bit.  F23A13 to clear lam enable bit.
	***/
	FAST int mask;
	mask = 1 << (pLam->bitnum - 1);
	addr |= (13 << CAM_A_SHIFT) +
		(flag ? (19 << CAM_F_SHIFT) : (23 << CAM_F_SHIFT));
	retval = camacOut (addr, (ULONG)mask, (CAM_STAT *)NULL);
	}
    else
	{
	/*
	 * Sub address type lam.  Use F26 to enable, F24 to disable.
	 */
	addr |= (flag ? (26 << CAM_F_SHIFT) : (24 << CAM_F_SHIFT));
	retval = camacTest (addr, (CAM_STAT *)NULL);
	}

    /* re-enable the crate controller demand */
    if (flag && retval == OK)
	{
	return (camacECD (addr,flag));
	}

    return (retval);
    }
SHAR_EOF
#	End of shell archive
exit 0
