/* camacLib.c - camac Library */

/*
written by;

D'Anne Thompson
National Optical Astronomy Observatories
P.O. Box 26732
Tucson, AZ 85726
(602) 325-9335
dat@noao.edu  {...}!uunet!noao.edu!dat
*/

/*
modification history
--------------------
01b 20jan89,dat updated to use tcpLib.h
0a0 27oct88 dat	written
*/

/*
This module represents the basic simple interface to the CES-CBD 8210
camac driver.  Only primitive access methods are implemented here.
See module camacEsone for formalized access routines.

CAMACLIBINIT
The camacLibInit () function must be called prior to any attempted
camac accesses.  It is necessary to intercept the bus error exception
vector so that accesses to non-existant branches/crates don't crash
the system.

ROM IMAGES:
Because of self-modifying code for the bus error exception
intercept code, the intercept handling routine must be in RAM
at execution time.  The remaining parts of the text segment may
reside in and be executed from ROM.

CONTROLLER CONFUSION:
The camac branch driver is based upon a bit-slice controller.  It
has been known to get confused by illegal access attempts such
as trying to read a register using a write function code (F16).
The controller will straighten itself out usually after the next
valid transaction.  Be careful when using the primitive routines
camacIn, camacOut, and camacTest.  The esone standard interface
is much safer.
*/

#ifndef ASMLANGUAGE

#include "vxWorks.h"
#include "iv68k.h"	/* interrupt routines */
#include "vmeConfig.h"  /* vme addresses */
#include "camac.h"	/* camac routines */


IMPORT FUNCPTR	intVecGet ();
IMPORT 		intVecSet ();
IMPORT int	camacIntercept();
IMPORT short	camacPatch[];

CES8210 	*camacBaseAddr=NULL;
CES8210 	*camacTopAddr=NULL;

#define F16 (16<<CAM_F_SHIFT)
#define F8 (8<<CAM_F_SHIFT)

/************************************************************
*
* camacLibInit - initialize the camac library
*
* Inserts the bus_error intercept code into the the bus_error
* vector.  This routine also converts the VME address of the
* branch #0 controller to the local cpu address space.  All
* camac references are made relative to that address.
*
* WARNING:
* The vector intercept code uses a self modifying code sequence
* to patch the jump instruction back to the original bus error
* handler.  There is no other way to get back to the original
* interrupt handler without using up either a data or address register.
* The 68020 could use memory indirect addressing mode, but the code
* would then be incompatible with 68000 and 68010 processors.
*
* BUGS:
* The externel interrupt levels 2 and 4 are never enabled.  The
* Heurikon HK-V2F cpu needs some special interrupt handling
* routines to enable these levels.  When they are available the
* external interrupts can be enabled.
*/

STATUS  camacLibInit ()

    {
    FUNCPTR	oldVector;

    if (sysBusToLocalAdrs (CAMAC_ADDR_SPACE, (char *)CAMAC_BR0_ADDR,
		(char **)&camacBaseAddr) == ERROR )
	{
	return ERROR;
	}
    
    camacTopAddr = camacBaseAddr + (CAM_MAX_BRANCH);

    oldVector = intVecGet (IV_BUS_ERROR);
    if (oldVector != camacIntercept)
	{
	/* SELF-MODIFYING CODE */
	camacPatch[0] = 0x4ef9;
	camacPatch[1] = (ULONG)oldVector >> 16;
	camacPatch[2] = (ULONG)oldVector;

	intVecSet (IV_BUS_ERROR, camacIntercept);
	}

/*HELP: sysIntEnable (2); */
    sysIntEnable (3);
/*HELP: sysIntEnable (4); */

    return OK;
    }


/*******************************************************
*
* camacBuildRef - generate a camac reference
*
* Given the geographical and local addressing parameters
* for a camac operation, this routine returns the local
* cpu address for that operation.
*
* Crate #0 is permitted since the branch driver control and
* status registers live in the space allocated to crate #0.
* Similarly, slot numbers (n) 26 thru 31 are permitted because
* crate controller special functions respond to those addresses.
*
* NOTES:
* In normal usage most references are built with both 'a' and 'f'
* set to zero.  The user will 'or' in the desired codes when
* making the actual function call.  The following example shows
* this usage.
*
* .CS
* .nf
* #define A2   (2 << CAM_A_SHIFT)
* #define A0   (0)
* #define F16	(16 << CAM_F_SHIFT)
* #define F2	(2 << CAM_F_SHIFT)
*
*      /* ioModule = branch 0, crate 2, slot 14 *
*      ioModule = camacBuildRef (0,2,14,0,0,CAM_LONG);
*
*      camacIn (ioModule|A2|F2, &newData, NULL);
*      camacOut (ioModule|A0|F16, newCommand, NULL);
* .fi
* .CE
*/

CAM_REF	camacBuildRef (b, c, n, a, f, sz)
    int b;   /* branch number */
    int c;   /* crate number */
    int n;   /* slot number */
    int a;   /* sub-address */
    int f;   /* function code */
    int sz;  /* size code: ~0 -> short, 0 -> long */

    {
    if (b < 0 || b >= CAM_MAX_BRANCH
     || c < 0 || c > 7
     || n < 1 || n > 32
     || a < 0 || a > 15
     || f < 0 || f > 31
	)
	return NULL;
    else
	return  (CAM_REF) (
		(char *)camacBaseAddr
		+ (b << CAM_B_SHIFT)
		+ (c << CAM_C_SHIFT)
		+ (n << CAM_N_SHIFT)
		+ (a << CAM_A_SHIFT)
		+ (f << CAM_F_SHIFT)
		+ (sz ? CAM_SHORT : CAM_LONG) );
    }

/*******************************************************
*
* camacIn - perform a camac input function
*
* Perform a read data operation on a camac register (function
* codes 0 through 7).
*
* If the camac operation reference was defined as being 'long'
* then the data pointer must point to a long integer location.
* If it was defined as short, then it must point to
* a short integer location.
*
* The transaction status is stored at the location given by
* pStatus. If pStatus is zero or NULL, then the
* camac status word is discarded.
*
* WARNING:
* The camac driver microcode can be confused by attempting
* a read cycle with a camac reference that does not represent
* a valid read function code, 0 through 7.
*
* RETURNS:
* Returns ERROR if the camac cycle fails (no-X and no-Q).
*/

STATUS	camacIn (camRef, pData, pStatus)
    CAM_REF  camRef;   /* camac operation reference */
    char     *pData;   /* data pointer, camRef determines size */
    CAM_STAT *pStatus; /* status result pointer */
    
    {
    int oldLevel;
    CAM_STAT status;
    /* Integer to pointer conversion */
    FAST USHORT *pCamac = (USHORT *)(camRef);


    oldLevel = intLock ();
    if ((camRef & CAM_SHORT) == CAM_SHORT)
	*(USHORT *)pData = *pCamac;
    else 
	{
	FAST ULONG temp = *pCamac++ << 16;
	*(ULONG *)pData = temp | *pCamac;
	}

    /* transaction status */
    status = *(USHORT *)(((ULONG)pCamac&CAM_B_MASK)|CAM_CSR);

    intUnlock (oldLevel);

    if (pStatus != NULL)
	*pStatus = status;

    return CAM_STAT_OK(status) ? OK : ERROR;
    }


/*******************************************************
*
* camacOut - perform a camac output function
*
* Output data to a camac register (function codes 16 thru 23).
*
* The transaction status is stored at the location given by
* pStatus. If pStatus is zero or NULL, then the
* camac status word is discarded.
*
* WARNING:
* The camac driver microcode can be confused by attempting
* a write cycle with a camac reference that does not represent
* a valid write function code, 16 through 23.
*
* RETURNS:
* Returns ERROR if the camac cycle fails (no-X and no-Q).
*/

STATUS	camacOut (camRef, data, pStatus)
    CAM_REF  camRef;   /* camac operation reference */
    ULONG    data;     /* data to be written */
    CAM_STAT *pStatus; /* status result pointer */
    
    {
    int		oldLevel;
    CAM_STAT	status;
    FAST short	*pCamac = (short *)(camRef);

    oldLevel = intLock ();

    if ((camRef & CAM_SHORT) == CAM_SHORT)
	*pCamac = data;
    else 
	{
	*pCamac++ = data >> 16;
	*pCamac   = data;
	}

    status = *(short *)(((ULONG)pCamac & CAM_B_MASK) | CAM_CSR);

    intUnlock (oldLevel);

    if (pStatus)
	*pStatus = status;

    return CAM_STAT_OK(status) ? OK : ERROR ;
    }


/*******************************************************
*
* camacTest - perform a camac test function
*
* Perform a non-data camac test function (function codes 8 thru 15
* and 24 thru 31).
*
* The transaction status is stored at the location given by pStatus.
* If pStatus is zero or NULL, the status word is discarded.
*
* NOTE:
* The return code is the success/error code for the
* camac cycle itself, not just the Q bit response.
* You must use the status result pointer to obtain
* the status word if you want to examine just the Q bit.
*
* WARNING:
* The camac driver microcode can be confused by attempting
* a test cycle with a camac reference that does not represent
* a valid test function code, 8 through 15 and 24 through 31.
*
* RETURNS:
* Returns ERROR if the camac cycle does not return X or Q status.
*/

STATUS  camacTest (camRef, pStatus)
    CAM_REF  camRef;   /* camac operation reference */
    CAM_STAT *pStatus; /* result status pointer */
    
    {
    int oldLevel;
    CAM_STAT status;

    /* Integer to pointer conversion */
    FAST short *pCamac = (short *)(camRef|CAM_SHORT);

    oldLevel = intLock ();
    status = *pCamac;
    status = *(short *)(((ULONG)pCamac & CAM_B_MASK) | CAM_CSR);
    intUnlock (oldLevel);
    if (pStatus)
	*pStatus = status;
    return CAM_STAT_OK(status) ? OK : ERROR ;
    }

/*******************************************************
*
* camacProbe - test for presence of module
*
* Tries to determine if there is a module present at a
* given camac address.  For write operations and test operations
* this is determined by the presence of either X or Q status
* following an access cycle.  For a read operation, a non-zero
* data response will also yield a present indication.
*
* RETURNS:
* Returns OK if branch, crate, and module are all present.
* Returns ERROR if absent or off-line.
*
* WARNING:
* There is a possibility of a false absent indication for home-built
* write-only modules that give neither X nor Q.  It is just not possible
* to detect the presence/absence of such a poorly designed module.
* (You guessed it, NOAO has some of these).
*/

STATUS	camacProbe (camRef)
    CAM_REF camRef; /* camac address/operation code */
    
    {
    STATUS temp;
    if (camRef & F8)
	{
	/* SUCCESS if either X or Q is returned */
	temp = camacTest (camRef, (CAM_STAT *)NULL);
	}
    else
	{
	if (camRef & F16)
	    /* SUCCESS if either X or Q is returned */
	    temp = camacOut (camRef, (ULONG)0, (CAM_STAT *)NULL);
	else
	    {
	    /* SUCCESS if X, or Q, or data is nonZero */
	    long dummy = 0;
	    temp = camacIn (camRef, (char *)&dummy, (CAM_STAT *)NULL);
	    /* If we get non-zero data, then it MUST BE OK */
	    if (dummy != 0)
		temp =  OK;
	    }
	}

    return (temp);
    }

#endif /* !ASMLANGAUGE */

#ifdef ASMLANGUAGE

#include "ces8210.h"

/**************************************************************
*
* camacIntercept - intercept bus error exceptions
*
* Intercept bus error exceptions, in order to fix up references
* to non existant camac branches or crates.
* 
* If the user references into a non existant branch, two bus errors
* will occur.  The first on the module reference, and the second
* upon trying to reference the branch status register.  This routine
* intercepts both errors returning a zero for the data reference and
* a timeout indication from the non existant branch driver module.
* 
* If the user references a non-existant crate on a good branch, then
* the branch driver will abort the data reference with a bus error,
* which will be intercepted. The user will get a timeout directly
* from the branch status register.
*/

/* LOCAL   camacIntercept () */

	.globl	_camacIntercept
	.globl	_camacPatch
	.globl	_camacBaseAddr	/* branch 0 address */
	.globl	_camacTopAddr	/* last camac address */

/* EXCEPTION FRAME INFORMATION (with d0 on stack) */
#define	SSW		0xa + 4	/* Stack Offset to Special Status Word */
#define	SSW_RW_BIT	0x6	/* bit 6, byte 1 */
#define	SSW_DF_BIT	0x0	/* bit 0, byte 0 */

#define	FRAME_TYPE	0x6 + 4	/* Stack offset to the Frame Id Nibble */
#define	SHORT_FRAME	0xa0	/* Compare the whole byte, lower nibble */
#define  LONG_FRAME	0xb0	/* has to be zero anyway. */

#define	FAULT_ADDR	0x10 + 4/* Stack offset to the fault address */
#define	DIB		0x2c	/* Stack offset to Data Input Buffer */

	.text
	.even
_camacIntercept:
	movel	d0,sp@-		/* save one register */
	cmpb	#SHORT_FRAME,sp@(FRAME_TYPE)	
	beq	cam_1
	cmpb	#LONG_FRAME,sp@(FRAME_TYPE)
	beq	cam_1
	bra	not_camac		/* not a bus error frame */

cam_1:
	btst	#SSW_DF_BIT,sp@(SSW)
	beq	not_camac		/* not a data fault */

	movel	sp@(FAULT_ADDR), d0
	cmpl	_camacTopAddr,d0
	bcc	not_camac		/* bcc = bhis */
	cmpl	_camacBaseAddr,d0
	bcs	not_camac		/* bcs = blo */

	btst	#SSW_RW_BIT,sp@(SSW+1)
	beq	fixed			/* ignore a faulted camac write cycle */

	movel	sp@(FAULT_ADDR),sp@-
	andl	#CAM_CNAF_MASK,sp@
	cmpl	#CAM_CSR,sp@+	
	bne	cam_2			/* not a CSR address fault */

	movel	#CAM_TO,sp@(DIB)	/* return CSR = Time_Out */
	bra	fixed

cam_2:
	clrl	sp@(DIB)		/* All other faulted reads return 0 */

fixed:
	bclr	#SSW_DF_BIT,sp@(SSW)	/* don't rerun the instruction */
	movel	sp@+,d0
	rte

not_camac:
	movel	sp@+,d0
_camacPatch:
/* Patched Locations SELF_MODIFYING_CODE */
	jmp	0:l	/*  patched to point to orig handler */
	.word	0
	.word	0
#endif /*ASMLANGUAGE*/
