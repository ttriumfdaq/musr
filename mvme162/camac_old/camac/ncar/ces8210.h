/* ces8210.h - header file for CES-CBD 8210 Camac Branch Highway Driver */

#ifndef INCces8210_h
#define INCces8210_h

/*
written by;

D'Anne Thompson
National Optical Astronomy Observatories
P.O. Box 26732
Tucson, AZ 85726
(602) 325-9335
dat@noao.edu  {...}!uunet!noao.edu!dat
*/


/*
modification history
--------------------
28may89,dat - fixed up for export.
*/

/*
Because of our Herikon cpu's limitations we must limit ourselves to
16 bit VMEbus transactions.  Define the Branch Highway device as a
simple array of shorts.  We will use #define's to identify special
registers within the structure.
*/

/*
Standard Board setup:
Interrupt Level 2 vector, set to IV_CAMAC_IT2 (See vmeConfig.h)
Interrupt Level 4 vector, set to IV_CAMAC_IT4 (See vmeConfig.h)
Time Out Selection (J3) = 8 microsec = in,in,in,out,in
Level Selects = all TTL  j4=j5=B(left), j6=out, j7=j8=in
*/

#ifndef ASMLANGUAGE
typedef struct {
	short	reg[8*32*16*32*2];	/* 262,144 shorts = 512KB */
} CES8210;
#endif

#define	BCSR	reg[29697]	/* CR0, N29, A0, F0, (halfword) */
#define BD1	reg[29699]
#define BD2	reg[29701]
#define BD3	reg[29703]
#define BITF	reg[29705]	/* CR0, N29, A0, F4 */
#define BC1	reg[29707]
#define BC2	reg[29709]
#define BC3	reg[29711]
#define BCAR	reg[29713]
#define BTB	reg[29715]	/* CR0, N29, A0, F9 *read* */
#define BZ	reg[29715]	/* CR0, N29, A0, F9 *write* */
#define BGL_U	reg[29716]	/* upper 8 bits, read FIRST */
#define BGL_L	reg[29717]	/* lower 16 bits, read SECOND */

#define CAM_Q_BIT 0x8000	/* 'Q' */
#define CAM_X_BIT 0x4000	/* 'X' */
#define CAM_TO	0x2000	/* Time out */
#define CAM_BD	0x1000	/* Branch Demand Pending */
#define CAM_MNOX	0x0800	/* Mask the NO-X bus error, default = 1 */
#define CAM_MTO	0x0020	/* Mask Timeout, default = 1 */
#define CAM_MLAM	0x0010	/* Mask all LAMS, default = 1 */
#define CAM_MIT2	0x0008	/* Mask external interrupt #2 */
#define CAM_MIT4	0x0004	/* Mask external interrupt #4 */
#define CAM_IT2	0x0002	/* State of external interrupt #2 */
#define CAM_IT4	0x0001	/* State of external interrupt #4 */

#define CAM_F_SHIFT	(2)
#define CAM_A_SHIFT	(7)
#define CAM_N_SHIFT	(11)
#define CAM_C_SHIFT	(16)
#define CAM_B_SHIFT	(19)

/* mask out cnaf, leaving ptr to branch driver */
#define CAM_B_MASK	(0xfff80000)
#define CAM_CNAF_MASK	(0x0007ffff)

/* low order address bits pointing to CSR of a branch driver */
#define CAM_CSR		(0x0000e802)

/* mask off b and c, leaving only n,a, and f */
#define CAM_NAF_MASK	(0x0000fffc)

/* mask off n,a,f leaving ptr to b,c */
#define CAM_BC_MASK	(~CAM_NAF_MASK)

/* mask out f, reference is changed to have F = 0, size = LONG */
#define CAM_BCNA_MASK	(0xffffff80)

/* size codes even short word address is start of a 24 bit cycle,
 * odd word address is start of a single 16 bit cycle. */
#define CAM_LONG	(0)
#define CAM_SHORT	(2)

#endif
