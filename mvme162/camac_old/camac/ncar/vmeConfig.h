/* vmeConfig.h - VME bus configuration for 'aries' */

#ifndef INCvmeConfig_h
#define INCvmeConfig_h

/*
written by;

D'Anne Thompson
National Optical Astronomy Observatories
P.O. Box 26732
Tucson, AZ 85726
(602) 325-9335
dat@noao.edu  {...}!uunet!noao.edu!dat
*/


/*
modification history
--------------------
01b 01jun89,dat - updated for archive exporting
01a 03mar89,dat - converted to vxWorks
*/

#include "vme.h"	/* address modifier codes */

/******************************************************************
*
* Camac branch Drivers,	CES-CBD8210 (A24D16)
*
* VMEA24 (0x800000 thru 0x9fffff)
* Maximum of 4 branches ( #0 thru #3 )
* Base addresses are all relative to CAMAC_BR0_ADDR, which cannot be
* changed.
*/

/* number of active branches */
#define CAM_MAX_BRANCH	(4)

/* vme address of first branch *FIXED* */
#define CAMAC_BR0_ADDR	(0x800000)

/* vme address space code (from $VW/h/vme.h) */
#define CAMAC_ADDR_SPACE (VME_AM_STD_SUP_DATA)

/* interrupt vector for it2, SAME JUMPER SETTINGS FOR ALL BRANCHES */
#define CAMAC_IT2_INUM	(193)	/* IRQ2 */

/* interrupt vector for it4, SAME FOR ALL BRANCHES */
#define CAMAC_IT4_INUM	(194)	/* IRQ4 */

/* interrupt vectors (changeable) for individual branches */
#define CAMAC_BR0_INUM	(144)	/* IRQ3, 144 thru 167 */
#define CAMAC_BR1_INUM	(168)	/* IRQ3, 168 thru 191 */
#define CAMAC_BR2_INUM	(196)	/* IRQ3, 196 thru 219 */
#define CAMAC_BR3_INUM	(220)	/* IRQ3, 220 thru 243 */

#endif /* !INC vmeConfig_h */
