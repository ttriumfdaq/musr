/*
 * THIS IS MV167 TRIGGER TEST WITH C CODE 
 */

/* #include </h0/DEFS/stdio.h> */
#include "vxWorks.h"
#include "taskLib.h"
#include "intLib.h"

/*	M B D   E V E N T  P R O C E S S I N G   E M U L A T O R */



/*	C A M A C   A D D R E S S				*/
#define		cbd 	0xf0800000
#define		brnch	0x00080000
#define		crt 	0x00010000
#define		slot	0x00000800
#define		suba	0x00000080
#define		func	0x00000004
#define		bit16	0x00000002
#define		bit24	0x00000000

/*	C A M A C   F U N C T I O N S				*/
#define		read16		1
#define		read24		2
#define		write16		3
#define		write24		4
#define		end_list	0


/*		E V E N T   P R O C E S S I N G   L I S T S	*/
/*			a list consists of;			*/
/*			# of times this FCNA is repeated	*/
/*				(zero is end of list)		*/
/*			the function 				*/
/*			encoded FCNAs for the function		*/
/*			.					*/
/*			.					*/
/*			.					*/
/*	 							*/
	int	evt0_li[]	=	{end_list};
	int	evt1_li[]	=	{end_list};
	int	evt2_li[]	=	{end_list};
	int	evt3_li[]	=	{end_list};
	int	evt4_li[1]	=	{end_list};
	int	evt5_li[1]	=	{
/*		4,
		read16,
		cbd+(brnch*0)+(crt*1)+(slot*1)+(suba*1)+(func*0)+bit16,
		cbd+(brnch*0)+(crt*1)+(slot*1)+(suba*1)+(func*0)+bit16,
		cbd+(brnch*0)+(crt*1)+(slot*1)+(suba*1)+(func*0)+bit16,
		cbd+(brnch*0)+(crt*1)+(slot*1)+(suba*1)+(func*0)+bit16,
		2,
		read24,
		cbd+(brnch*0)+(crt*1)+(slot*1)+(suba*1)+(func*0)+bit24,
		cbd+(brnch*0)+(crt*1)+(slot*1)+(suba*1)+(func*0)+bit24, */
		end_list    }; 
	int	evt6_li[]	=	{end_list}; 

/*		E V E N T   A D D R E S S   T A B L E		*/
	int *evt_tab[31] = {
		evt0_li,
		evt1_li,
		evt2_li,
		evt3_li,
		evt4_li,
		evt5_li,
		evt6_li	};

/*		H A R D W A R E   A D D R E S S E S	*/
	int	enb_bd 	=cbd+(brnch*0)+(crt*1)+(slot*30)+(suba*10)
			+(func*26)+bit16;
	int	loa_msk	=cbd+(brnch*0)+(crt*1)+(slot*22)+(suba*2)
			+(func*16)+bit24;
	int	clrmlam	=cbd+(brnch*0)+(crt*0)+(slot*29)+(suba*0)
			+(func*0)+bit16;
	int	enblams	=cbd+(brnch*0)+(crt*1)+(slot*22)+(suba*0)
			+(func*26)+bit24;
	int	tst_lam	=cbd+(brnch*0)+(crt*0)+(slot*29)+(suba*0)
			+(func*0)+bit16;
	int	read_gl	=cbd+(brnch*0)+(crt*0)+(slot*29)+(suba*0)
			+(func*10)+bit24;
	int	rd_evno	=cbd+(brnch*0)+(crt*1)+(slot*22)+(suba*0)
			+(func*0)+bit24;
	int	clrtrig	=cbd+(brnch*0)+(crt*1)+(slot*22)+(suba*1)
			+(func*16)+bit24;
	int	clr_lam	=cbd+(brnch*0)+(crt*1)+(slot*22)+(suba*0)
			+(func*10)+bit16;

/*		D A T A   B U F F E R			*/
	short	int	buffer[10000];

trig()	/* emulate MBD event handling */
{
	short	int	evt_no	=	5;
	short	int	mlammsk	=	0xffffffef;
	short	int	lamset	=	0x1000;   /* if lam set BD=1 */
	int	gl23		=	0x400000;
	short	int	x;	/* scratch variable		*/
	int	y;		/* scratch variable			*/
	short	int	event;	/* event number from trigger module*/
	int	*table;		/* pointer to the table for an event	*/
	register	short	int	*reg1;
	register	short	int	*reg2;
	register	long	int	*reg3;
	int             i, loops;

        int     lockKey;
	lockKey = intLock();

 printf("1.	enable branch demands\n");
	reg1 = (short int *)enb_bd;
	x = *reg1;

 printf("2.	write event number to trigger module\n");
	reg3 = (long int *)loa_msk;
	*reg3 = evt_no;

 printf("3.	clear MLAM bit in CBD\n");
	reg1 = (short int *)clrmlam;
	*reg1 = *reg1 & mlammsk;
	
 printf("4.	enable lams in event trigger module	\n"); 
	reg3 = (long int *)enblams;
	x = *reg3;

 printf("5.	*****	loop forever	*****	\n"); 
	while(1) 
/* printf("enter loops \n");
scanf("%d",&loops);
for (i=0; i<loops; i++); */ 

	{

/*  printf("6.	waiting for	LAM			\n"); */ 
		reg1 = (short int *) tst_lam;
		while(!(lamset & *reg1 ));      
		reg3 = (long int *) read_gl;
		if(!(gl23 & *reg3))
		{
			reg1 = (short int *) clr_lam;
			x = *reg1;
		}
		else
		{
			reg3 = (long int *) rd_evno;
			event = *reg3;
			if (0 > event || 31 < event)
			{
				reg1 = (short int *) clr_lam;
				x = *reg1;
			}
			else
			{
 /* printf("7.	enter event process\n"); */ 
				table = evt_tab[event];

				while( (*table) != 0)
				{
				switch(*(table+1)) 
				{
					case	0:
							break;
					case	1:
					/*		printf("CAMAC 16 bit read\n"); */ 
							x=read_16(table,buffer);
							break;
					case	2:
/*							printf("go on read_24 subroutine\n"); */
                                                        x=read_24(table,buffer);
							break;
					case	3:
							break;
					case	4:
							break;
					case	5:
							break;
					case	6:
							break;
					case	7:
							break;
					case	8:
							break;
					case	9:
							break;
					default:
							break;
				}
				x = (*table);
				table = table + x + 2;
				}
			reg3 = (long int *) clrtrig;
			*reg3 = event;
			reg1 = (short int *) clr_lam;
			x = *reg1;
			}

		}

	}
	intUnlock(lockKey);
printf("the operation is ended!\n");		
}

read_16(list,buffer)
long	int	list[];
short	int	buffer[];
{
	register short int *reg1;
	int	a;
	int	i	=	2;
	int	j	=	0;
	int	k	=	0;
		i = 2;
		j = 0;
		k = 0;
		for (a = list[k]; a != 0; a--)
		{
			reg1 = (short int *) list[k+i++];
			buffer[j++] = *reg1;
		}
		k = k + list[k] + 2;
	return(0);
}
read_24(list,buffer)
long	int	list[];
long	int	buffer[];
{
	register long int *reg1;
	int	a;
	int	i	=	2;
	int	j	=	0;
	int	k	=	0;
		i = 2;
		j = 0;
		k = 0;
		for (a = list[k]; a != 0; a--)
		{
			reg1 = (long int *) list[k+i++];
			buffer[j++] = *reg1;
		}
		k = k + list[k] + 2;
	return(0);
}
