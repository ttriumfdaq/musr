/*-----------------------------------------------------------------------------
** Copyright (c) 1993 TRIUMF Cylotron Facility
**
** TRIUMF Electronics Grp, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A5
** Email: waters@sundae.triumf.ca Tel: (604) 222-1047 Fax: (604) 222-1074
**-----------------------------------------------------------------------------
** 
** Description:
**
**	Driver for CES 8210 CAMAC Branch Driver.
** 
**   An implementation of the CAMAC standard routines in C,
**   using C argument passing conventions.
**
**	Based on work carried out at lampf by:
**	Tom Kozlowski and Siahong fu
**      
** 
** Author:	Graham Waters, TRIUMF Electronics Group
** File:        camacLib.c
** Created:     June 1993.
** $Revision: 1.1.1.1 $
** last edit: $Date: 1995/10/22 02:30:19 $ $Author: ted $
**
** log file:  $Source: /usr/local/cvsroot/musr/mvme162/camac_old/cescam/camacLib_8210.c,v $
**-------------------------------------------------------------------------**
 *
 * Revision History:
 *   $Log: camacLib_8210.c,v $
 *   Revision 1.1.1.1  1995/10/22 02:30:19  ted
 *   import from decu18
 *
 * Revision 1.2  93/08/13  11:19:50  waters
 * Front panel interrupts only, no lams.
 * 
 * Revision 1.1  93/07/07  11:30:11  waters
 * Initial revision
 * 
 * 
 * 
 * 
 * 
 * 
 *
 *
*/
/* Functions (all return void):
 *
 * cdreg(int *ext,int b,int c,int n,int a)   define external address 
 * cssa(int f,int ext,int *dat,int *q)       short (16) single action 
 * cfsa(int f,int ext,int *dat,int *q)       long (24) single action 
 * cccz(int ext)			 crate initialize 
 * cccc(int ext)			 crate clear 
 * ccci(int ext,int l)			 set/reset crate inhibit 
 * ctci(int ext,int *l)			 test crate inhibit 
 * cccd(int ext,int l)			 ena/disable crate demand 
 * ctcd(int ext,int *l)			 test crate demand enabled
 * ccgl(int lam,int l)                   ena/disable graded lam ( NON STANDARD )
 * ctgl(intext,int *l)                   test graded lam
 * cdlam(int *lam,int b,int c,int n,int a,int inta[])  define lam
 * cclc(int lam)                         clear lam (dataless function)
 * cclm(int lam,int *l)                  alternative lam clears
 * ctlm(int lam,int *l)                  test lam
 * ccinit(int b)                         initialize branch 
 * ccexlnk(int level,void *())		 link routine to external interrupt
 * cclnk(int lam,void *())               link routine to lam
 * cculk(int lam,void *())               unlink routine from lam
 * ccrgl(int lam)                        re-enable graded lam(@end of isr)
 * cclwt(int lam)                        lam wait
 * ctstat(int *istat)			 test status of previous operation
 */

void extIsr();


typedef unsigned long   uint_32;
typedef unsigned short   uint_16;

#include "vxWorks.h"
#include "intLib.h"
#include "taskLib.h"
#include "vme.h"
#include "ces8210.h"

int INIT = 0;
int INTERRUPTS = 0;
int LAM_REG;
int ISR_ADDR;
int LAM_NUM;
int badadr;
int q,x;
int DEBUG = 0;
int POLLING = 0;
int mit24;			/* mask for external interrupts */


void int2Isr();

setDiag(diag)
int diag;
{
	DEBUG = diag;
}
setpolling(pol)
int pol;
{
	POLLING = pol;
}


/*-------------------------------------------------------------------------*/
/* CDREG: Pack camac B,C,N,A into long integer                             */
/*-------------------------------------------------------------------------*/
/*
synopsis
	void cdreg (ext,b,c,n,a)
	uint_32 * ext ----- OUTPUT: CAMAC packed address
	uint_32 b --------- INPUT:  CAMAC branch 0-7
	uint_32 c --------- INPUT:  CAMAC crate 0-7
	uint_32 n --------- INPUT:  CAMAC slot
	uint_32 a --------- INPUT:  CAMAC sub-address

Description
	Packs camac b,c,n,a init a long integer

*/

void
cdreg (ext,b,c,n,a)
uint_32 *ext;
uint_32 b,c,n,a;
{
 	STATUS sysBusToLocalAdrs();
	int cbd_ladr;


	   /*------------------------------------------------------*/
	  /* camac packed ext=0, if user enter illegal b, c, n, a */
	 /* allow n25 and n26 for external interrupt def'n       */ 
	/*------------------------------------------------------*/

	if (0>b||b>7||0>c||c>7||n==0||n==24||n==27||n==31||0>a||a>15)
	{
    		*ext=0;
		badadr = 1;
	}

	  /*--------------------------------*/
	 /* CALCULATE CAMAC PACKED ADDRESS */
	/*--------------------------------*/

	else
	{	
        	sysBusToLocalAdrs(VME_AM_STD_USR_DATA,cbd_bas_vadr,&cbd_ladr);
		*ext=cbd_ladr|b<<19|c<<16|n<<11|a<<7;
		badadr = 0;
	}
}

/*-------------------------------------------------------------------------*/
/* CSSA: Execute camac function f, address EXT, with data (short)VALUE     */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cssa (f,ext,data,q)
	uint_32 ext ----- INPUT: CAMAC packed address
	uint_32 f --------- INPUT:  CAMAC function
	uint_32 *data ----- INPUT/OUTPUT: write or read to from CAMAC
	uint_32 *q -------- OUTPUT -- true if q was present

Description
	Execute a single short camac read/write

--------------------------------------------------------------------------*/

void
cssa (f,ext,data,q)
uint_32 ext;		/* INPUT -- CAMAC packed address */
uint_32 f;			/* INPUT -- CAMAC function code */
uint_32 *data;		/* INPUT-OUTPUT -- Write to or read form CAMAC */
uint_32 *q;		 	/* OUTPUT -- true if q was present */
{
    uint_16 *vme_addr, value;
    uint_16 csr, temp;
	uint_32 addr;
	uint_32 cbd_csr_r;
	int      lockKey;
 	lockKey = intLock();

	  /*-----------------------------------------------*/
	 /* calculate CAMAC 16 bits operation VME address */
	/*-----------------------------------------------*/

        addr = ext | ( (uint_32 )f << 2 );
	addr = addr | 2;
	vme_addr = (uint_16*)addr;

	  /*-----------------------------------------------------------------*/
	 /* execute command function or read function -- 16 bits operation. */
	/*-----------------------------------------------------------------*/
         
        if (f < 16)
		*data = *vme_addr;
        else if (f<24)
		*vme_addr =  (uint_16)*data; 
        else
		temp = *vme_addr;
	  
	  /*----------------------------------------------*/
	 /* read the Control & Status Registe, extract q */
	/*----------------------------------------------*/
 
	cbd_csr_r= CSR;			/* address: camac control/status reg: c=0,n=29,a=0,f=0*/
	csr = *(uint_16 *)cbd_csr_r;
	csr = csr & 0x8000;
	if (csr == 0)
		*q = 0;
	else 
		*q = 1;
	
	intUnlock(lockKey);
        return;	
}
/*-------------------------------------------------------------------------*/
/* CFSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfsa (f,ext,data,q)
	uint_32 ext ----- INPUT: CAMAC packed address
	uint_32 f --------- INPUT:  CAMAC function
	uint_32 *data ----- INPUT/OUTPUT: write or read to from CAMAC
	uint_32 *q -------- OUTPUT -- true if q was present

Description
	Execute a single long camac read/write

--------------------------------------------------------------------------*/


void
cfsa (f,ext,data,q)
uint_32 ext;
uint_32 *data,f, *q;
{
	uint_32 *vme_addr, value;
	uint_32 addr, value_32, temp;
	uint_16 csr;
	uint_32 cbd_csr_r;



	int      lockKey;

 	lockKey = intLock();

	  /*-----------------------------------------------*/
	 /* calculate CAMAC 24 bits operation VME address */
	/*-----------------------------------------------*/

        addr = ext | ( (uint_32 )f << 2 ); 
	vme_addr = (uint_32*)addr;

	  /*-----------------------------------------------------------------*/
	 /* execute command function or read function -- 24 bits operation. */
	/*-----------------------------------------------------------------*/
         
        if (f < 16)
		*data = *(uint_32*)vme_addr;
        else if (f < 24)
		*vme_addr =  *data; 
	else 
		temp = *vme_addr;
	  /*-----------------------------------------------*/
	 /* read the Control & Status Register, extract q */
	/*-----------------------------------------------*/

	cbd_csr_r= CSR;			/* address: camac control/status reg */
	csr = *(uint_16 *)cbd_csr_r;
	csr = csr & 0x8000;
	if (csr == 0)
		*q = 0;
	else 
		*q = 1;
	
	intUnlock(lockKey);
        return;
	
}



/*===========================================================================
**NAME: ctstat
**PURPOSE: test status of proceding action
*/


void ctstat(istat)
               
int *istat;             /* OUTPUT -- Q,X status of proceding CAMAC action */

{
	uint_32 cbd_csr_r;
	uint_16 csr;

	cbd_csr_r= CSR;			/* address: camac control/status reg */
	csr = *(uint_16 *)cbd_csr_r;

	/* status=4 when enter illegal b,c,n,a    */

       if (badadr == 1) 
		*istat = 4;
	else
	{
 		/* test CBD control status register q, x bit  */

 		csr = csr & 0xc000;
		switch(csr)
		{
			case 0xc000:
  				*istat = 0 ; 	/* x and q response */ 
				break;
			case  0x4000:   
                         	*istat = 1 ;	/* noq and x*/
				break;
			case	0x8000:
				*istat = 2 ;	/* q and nox */
				break;
			case 0x0000:
				*istat = 3 ;	/* nox and no q */
				break;
		}
	}
}


/*============================================================================
** name cccz.c 
** Purpose  crate initialize   
*/
 
void cccz(ext)
     int  ext;      /* INPUT--CAMAC external address  */
{
	short    temp;
	register short  *reg1;
	int      cam_gen_z;
       
        int     lockKey;
        lockKey = intLock();

	cam_gen_z=(ext|(26<<2)|2);
        reg1 = (short *)cam_gen_z;
	temp = *reg1;

	intUnlock(lockKey);
}

/*============================================================================
** Name cccc.c
**purpose  crate cleare
*/
void cccc(ext)
     int  ext;      /* INPUT--CAMAC external address */
{
	short    temp;
        register short *reg1;
	int      cam_gen_c;

        int      lockKey;
          lockKey = intLock();

	cam_gen_c=(ext|(26<<2)|2);
	reg1 = (short *)cam_gen_c;
        temp = *reg1;

	intUnlock(lockKey);
}

/*=============================================================================
** Name ccci.c
** Purpose  set/reset crate inhibit
*/
void ccci(ext,l)
     int        ext;      /* INPUT--CAMAC external address */  


     int        l;        /* INPUT--l=1 set dataway inhibit ,l=0
                             clear dataway inhibit   */ 
{
     short    temp;
     register short *reg1;
     int      cam_ed_i, slot;

     int      lockKey;
     lockKey = intLock();
     slot = (ext>>11)&31; 
     if (POLLING) printf ("\nccgl: slot = %d",slot);
     switch(slot)
     {
     	case 25:		/* external interrupt: INT2  */
		if (l)
		{
			disableInt(2);
			if (POLLING) printf ("\nccci: mask INT2");
		}
		else
		{
			enableInt(2);
			if (POLLING) printf ("\nccci: unmask INT2");
		}
		break;
	case 26:		/* external interrupt: INT4  */
		if (l)
		{
			disableInt(4);
			if (POLLING) printf ("\nccci: mask INT4");
		}
		else
		{
			enableInt(4);
			if (POLLING) printf ("\nccci: unmask INT4");
		}
		break;
	default:
    		if (l)
			cam_ed_i=(ext|(26<<2)|2); /* c=1,n=30,a=9,f=26 --clear I*/ 
     		else
			cam_ed_i=(ext|(24<<2)|2); /* c=1,n=30,a=9,f=24 --set I*/
     		reg1 = (short *)cam_ed_i;
     		temp = *reg1;
		break;
       } 
       intUnlock(lockKey);
}
 

/*=============================================================================
** Name ctci.c
** Purpose  Test Crate Dataway Inhibit
*/

void ctci(ext,l)
     int        ext;        /* INPUT--CAMAC external address */
     int        *l;         /* OUTPUT-- l=1 if inhibit(I) is set
                                        l=0 if inhibit(I) is clear */
{
     short    temp;
     register short *reg1;
     int      cam_tst_i;
     int      cbd_csr;

     int      lockKey;
     lockKey = intLock();
   
     cam_tst_i=(ext|(27<<2)|2); /* c=1,n=30,a=9,f=27 */ 
     reg1 = (short *)cam_tst_i;
     temp = *reg1;
     
     cbd_csr=(cbd_bas_ladr|(29<<11)|2); /* c=0,n=29,a=0,f=0 */
     intUnlock(lockKey);
     *l=(*(short *)cbd_csr >> 15)&1;       /* if cbd csr q=1 l=1, or l=0 */
}
/*=========================    ====================================================
** Name cccd.c
** Purpose  set/reset crate Demand E/D 
*/

void cccd(ext,l)
     int        ext;             /* INPUT--CAMAC external address */
     int        l;               /* INPUT--l=1 set camac demand 
                                           l=0 reset camac demand */
{
     int dat;
     int   *temp;
     register short *reg1;
     int      cam_ed_bd;

     int      lockKey;
     lockKey = intLock();   
     temp = &dat;
     
     if (l)   
  {
     cam_ed_bd=(ext+(26<<2)+2);   /* c=1,n=30,a=10,f=26 */
   }
     else
  {
  cam_ed_bd=(ext+(24<<2)+2);   /* c=1,n=30,a=10,f=24 */
  }

     reg1 = (short *)cam_ed_bd;
     *temp = *reg1;  
     intUnlock(lockKey);
}
 

/*=============================================================================
** Name ctcd.c
** Purpose  Test Crate Demand Enabled
*/

void ctcd(ext,l)
     int        ext;            /* INPUT--CAMAC external address */
     int        *l;             /* OUTPUT-- l=1 if E/D Flip Flop is set  
                                            l=0 if E/D Flip Flop is cleared */
{
     short    temp;
     register short *reg1;
     int      cbd_csr;
     int      cam_tst_bd;

     int      lockKey;
     lockKey = intLock();   

     cam_tst_bd=(ext|(27<<2)|2);     /* c=1,n=30,a=10,f=27 */
 
     reg1 = (short *)cam_tst_bd;
     temp = *reg1;
   
     cbd_csr=(cbd_bas_ladr|(29<<11)|2); 
     *l= (*(short *)cbd_csr >> 15)&1;
     intUnlock(lockKey);
}


/*=============================================================================
** Name cdlam.c
** Purpose  define lam
*/

void cdlam(ext,b,c,n,a,inta)
	int *ext;
	int b,c,n,a;
	int inta[2];
{
	/* ignore inta[] */
	if (POLLING) printf ("\ncdlam: n = %d",n);
 	cdreg(ext,b,c,n,a);
	return;
}

/*=============================================================================
** Name ccgl.c
** Purpose  ena/disable graded lam (NON STANDARD) */

void ccgl(lam,l)
     int        lam;  /* INPUT -- Lam identifier */
     int        l;    /* INPUT -- l=1 enable graded lam
                                  l=0 disable graded lam */
{
     char     temp;
     register char *reg1;
     register short *reg2;
     int      cbd_csr;
     int      slot,b;
     int      cbd_icr_lam;
     int      lockKey;

      lockKey = intLock();   
     /* read cbd csr */
 
      cbd_csr = (cbd_bas_ladr|29<<11|2);
     reg2 = (short *)cbd_csr;
     temp = *reg2;                                     
     slot = (lam>>11)&31;
     if (POLLING) printf ("\nccgl: slot = %d",slot);
     if (slot>24)        /* external interrupt: INT2=1 slot=25, INT4=1 slot=26  */
	{
		if (l==1)            /* enable single external interrupt */     
		{
			if (slot==25)
			{
				if (POLLING) printf ("\nccgl: enable int 2");	
        			enableInt(2);	/* unmask 8210 external interrupt 2 */
				sysIntEnable(2);/* enable bus interrupt level 2 */
			}
			if (slot==26)
			{
				if (POLLING) printf ("\nccgl: enable int 4");
				enableInt(4);/* unmask 8210 external interrupt 4 */
				sysIntEnable(4);/* enable bus interrupt level 4 */
			}
		}
                else
		{
			if (slot==25)
			{
				if (POLLING) printf ("\nccgl: disable int 2");	
				disableInt(2);/* mask 8210 external interrupt 2 */
				sysIntDisable(2);/* disable bus interrupt level 2 */
			}
			if (slot==26)
			{
				if (POLLING) printf ("\nccgl: disable int 4");	
				disableInt(4);	/* mask 8210 external interrupt 2 */
				sysIntDisable(4);/* disable bus interrupt level 2 */
			}
		}
	}
     else           /* for internal interrupt gl=0--23  */
	{
	b = (lam>>19)&7;
	cbd_icr_lam = (cbd_bas_ladr|b<<19|29<<11|5<<2|3);
	reg1 = (char *)cbd_icr_lam;    
	while (slot > 7)
	    {
		cbd_icr_lam += 2;        /* 7< slot <=15 use INT_CONT.2 */
	                                 /* 15< slot <= 23 use INT_CONT.3 */	
		slot -= 8;
            }
		*reg1 = am_clisrb | slot;	   /* clear ISR bits  */	
	if (l==1)                         /* enable single internal interrupt */
		{
		*reg1 = am_clirrmrb | slot;  /* clear IRR and IMR bits (0,1,2) */ 
/*		*reg1 = am_sirr | slot;        set IRR bits  */
		}
	else
		{
		*reg1 = am_stimrb | slot;    /* set IMR bits (0,1,2)  */
		}
     } 
     intUnlock(lockKey);

}
 

/*=============================================================================
** Name ctgl.c
** Purpose  Test graded lam (Test Demand Line)
*/

void ctgl(ext,l)
     int        ext;        /* INPUT -- CAMAC external address */
     int        *l;         /* OUTPUT-- l=1 if graded lam exist 
                                        l=0 if no graded lam */
{
     short    temp;
     register short *reg1;
     int      cbd_csr;
     int      cam_tst_dl;

     int      lockKey;
     lockKey = intLock();   
     
     cam_tst_dl=(ext|(27<<2)|2);     /* c=1,n=30,a=11,f=27 */
 
     reg1 = (short *)cam_tst_dl;
     temp = *reg1;
   
     cbd_csr=(cbd_bas_ladr|(29<<11)|2); 
     *l=(*(short *)cbd_csr >> 15)&1;
     intUnlock(lockKey);
}


/*=============================================================================
** Name cclm.c
** Purpose alternative lam clears  
*/
     
void cclm(lam,l)
     int lam;                    /* INPUT -- lam identifier   */
     int l;                      /* INPUT -- l=1 enable lam   
                                             l=0 disable lam  */
{
  int temp1,temp2;

  if (l) {
    cssa(26,lam,&temp1,&temp2);    /* enable lam output    */ 
  } else {
    cssa(24,lam,&temp1,&temp2);    /* disable lam output   */
  }
}


/*=============================================================================
** Name cclc.c
** Purpose clear lam 
*/

void cclc(lam)
     int lam;              /* INPUT -- Lam identifier    */
{
  int temp1,temp2;
  cssa(10,lam,&temp1,&temp2);
}

/*=============================================================================
** Name ctlm.c
** Purpose test lam
*/

void ctlm(lam,l)
     int lam;               /* INPUT -- Lam identifier    */ 
     int *l;                /* OUTPT -- l=1 if lam set
                                        l=0 if lam clear  */ 
{
  int temp1,temp2, slot;
  uint_16 csr, ifr;
  uint_32 cbd_ifr, ext, data, q;

	slot = (lam>>11)&31; 
	/* printf ("\nctlm: slot = %d",slot); */
	csr = *C_SRC;
	data = (uint_32)csr;
	cdreg (&ext,0,1,22,0);
	cssa  (16,ext,&data,&q);	


	if (slot>24)        /* external interrupt: INT2=1 slot=24, INT4=1 slot=25  */
	{
		*l = 0;
		csr = *C_SRC;
       		csr = csr & 0x0003;
		if(slot == 25)
		{
 			if (csr == 0x0001)
			{
				if (POLLING) printf ("\nextIsr: interrupt level 2");
				*l = 1;
			}
		}
		if (slot == 26)
		{
			if (csr == 0x0001)
			{
				if (POLLING) printf ("\nextIsr: interrupt level 4");
				*l = 1;
			}
		}
		clrInt();
	}
	else
	{
		cssa(0,lam,&temp1,&temp2);   /* should be f8 for real test lam status */
		*l=temp2;
	}


/* reading this switch register will fool coda into thinking there is an event */
/*
  cdreg(&ext,0,1,18,0);
  cssa(0,ext,&temp1,&temp2);
  *l=temp2;
*/
}

/*=============================================================================
**
*/

VOIDFUNCPTR lamisrs[25][8];
    int  taskidlam[25][8];        /* c<8, gl<25  */
VOIDFUNCPTR extisrs;		/* pointer to front panel interrupt handler */
int taskid;			/* address of calling routine */
 
/*=============================================================================
** Name lamIsr.c
** Purpose read graded lam
*/

void lamIsr()
{
int    data;
int ii,jj,pattern;       /* ii--c, jj--n */
	 /* read LAM pattern--read gl */
int  cbd_rd_gl;
        register int *reg1;
	INTERRUPTS = INTERRUPTS + 1;
	ii=1; 
        data=0xaabb;
	cbd_rd_gl = (cbd_bas_ladr|29<<11|10<<2);  /*  n=29, f=10, */ 
        reg1 = (int *)cbd_rd_gl;
        pattern = *reg1;
	LAM_REG = pattern;

	for (jj=0;jj<25;jj++)     
	if ((pattern&(1<<jj))&&(lamisrs[jj][ii]!=0))
	{
		ISR_ADDR=(int)lamisrs[jj][ii];
		LAM_NUM=jj;
		(* lamisrs[jj][ii])();
		taskResume(taskidlam[jj][ii]);
	}
          
}

/*=============================================================================
** Name ccinit.c
** Purpose intialize branch (clear IRR, IMR,  load interrupt mode, clear MLAM,
** give BZ .

NOTE: Initialization for lam interrupt not yet implemented

*/

void ccinit(int b)
{
     uint_16 ifr;
     uint_32 cbd_ifr;

     int      cbd_csr_r;        /* cbd control status reg. read address */
     int      cbd_icr_lam;      /* cbd interrupt control reg. address */
     int      cbd_ivr_lam;      /* cbd interrupt data reg. address */ 
     int      i,j;
     int      cbd_bz;
     int      temp;
     int      xvect=200;
     int      status;
     register char  *reg1;
     register char  *reg2;
     register short *reg3;

	INIT = 1;
	cbd_csr_r=(cbd_bas_ladr|b<<19|29<<11)|2;      /* n=29, c=0,f=0 */



	clrInt();		/* clear any pending front panel interrupts */
	disableInt(2);		/* mask 8210 external interrupt 2 */
	sysIntDisable(2);	/* disable bus interrupt level 2 */
	disableInt(4);		/* mask 8210 external interrupt 2 */
	sysIntDisable(4);	/* disable bus interrupt level 2 */

	status=intConnect((VOIDFUNCPTR *) (85<<2),    (VOIDFUNCPTR) extIsr,0);
	status=intConnect((VOIDFUNCPTR *) (170<<2),   (VOIDFUNCPTR) extIsr,0);
	status=intConnect((VOIDFUNCPTR *) (xvect<<2), (VOIDFUNCPTR) lamIsr,0);

 }

/*============================================================================
** name cclnk.c
** purpose  link routine to lam		for CODA link to trigger
*/
 
void cclnk(int lam,VOIDFUNCPTR label)
{
	int slot;
     slot = (lam>>11)&31;         /* lams = slot 1-24, external interrupts == slot 25,26 */ 
     printf ("\ncclnk: slot = %d",slot);
	switch (slot)					/* external interrupt: INT2=1 slot=25, INT4=1 slot=26  */
	{
		case 25:
				printf ("\nconnect to extisr INT2");
				extisrs = label;	/* init interrupt handler pointer */
				break;
		case 26:
				printf ("\nconnect to extisr INT4");
				extisrs = label;	/* init interrupt handler pointer */
				break;
		default:
				printf ("\nconnect to lamisr");
				lamisrs[((lam>>11)&31)-1][(lam>>16)&7] = label;
				taskidlam[((lam>>11)&31)-1][(lam>>16)&7] = taskIdSelf();
				mit24 = 0;	/* flag for LAM interrupts */
				break;
	}
}

/*============================================================================
** name cculk.c
** purpose  unlink routine from lam
*/
 
void cculk(lam,label)
	int lam;
	VOIDFUNCPTR label;
{
	lamisrs[((lam>>11)&31)-1][(lam>>16)&7] = 0;
}

/*============================================================================
** name ccrgl.c
** purpose  re-enable graded lam (end of isr)
*/
 
void ccrgl(lam)
	int lam;

{
     short    temp;
     register short *reg1;
     int      cbd_csr;
     int      slot,b;
     int      cbd_icr_lam;
     /* read cbd csr */
 
     cbd_csr = (cbd_bas_ladr|29<<11|2);
     reg1 = (short *)cbd_csr;
     temp = *reg1; 
     slot = (lam>>11)&31;
     if (slot>24)        /* IT2=1 slot=25, IT4=1 slot=26  */
	{
		if (slot==25)
		temp = temp & ~(0x08);         /* clear MIT2 */ 
		if (slot==26)
        	temp = temp & ~(0x04);         /* clear MIT4 */
		*reg1 = temp;
	}
     else
	{
	b = (lam>>19)&7;
	cbd_icr_lam = (cbd_bas_ladr|b<<19|29<<11|5<<2|2);
	reg1 = (short *)cbd_icr_lam;
	while (slot > 7)
	    {
		cbd_icr_lam += 4;
		slot -= 8;
            }
	*reg1 = am_clisrb | slot;		  /* clear ISR bits */
        *reg1 = am_clirrmrb | slot;         /* clear IRR and IMR bits */
        } 
}
 


/*============================================================================
** name cclwt.c
** purpose  link routine to lam
*/
 
void cclwt(lam)
	int lam;
{
       taskSuspend(0);
       return;
}


/*********************** Multiple Action Routines ***************************/

void cfga(int f[],int exta[],int intc[],int qa[],int cb[])
{ int i;
  for (i=0;i<cb[0];i++) {
    cfsa(f[i],exta[i],&(intc[i]),&(qa[i]));
  }
  cb[1] = cb[0];
}

/*-------------------------------------------------------------------------*/
/* CFMAD: Execute Address Scan     		  			   */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfmad (int f,int extb[],int intc[],int cb[])
	uint_32 extb ----- INPUT: extb[0] = start addr; extb[1] final addr
	uint_32 f -------- INPUT:  CAMAC function
	uint_32 intc ----- INPUT/OUTPUT: camac data array
	uint_32 cb ------- INPUT: cb[0] = repeat count
				  cb[1] = tally
				  cb[2] = LAM identification
				  cb[3] = channel identification

Description
	Execute function f at successive addresses

--------------------------------------------------------------------------*/

void cfmad(int f,int extb[],int intc[],int cb[])
{
	int ext,final_ext,count;
	ext = extb[0];
	final_ext = extb[1];
	count = cb[0];
	do
	{
		cfsa (f,ext,intc,&q);
		if (q == 0)
		{
			ext = ext & ~(15<<7); /* set subaddress to zero */
			ext = ext + (1<<11);  /* select next slot */
		}
		else
		{
			ext = ext + (1<<7);	/* increment address */
			++cb[1];		/* increment tally count */
			++intc;			/* next data array */
			--count;
		}
		if (ext > final_ext) count = 0; /* force exit */
	}
	while (count);
}
/*-------------------------------------------------------------------------*/
/* CFUBC: Execute Controller-Synchronized Block Transfer     		  			   */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfubc (int f,int ext,int intc[],int cb[])
	uint_32 ext ------ INPUT: CAMAC address
	uint_32 f -------- INPUT: CAMAC function
	uint_32 intc ----- INPUT/OUTPUT: camac data array
	uint_32 cb ------- INPUT: cb[0] = repeat count
				  cb[1] = tally
				  cb[2] = LAM identification
				  cb[3] = channel identification

Description
	Emulate a block transfer.
		stop mode: terminate on q=0 or on repeat count
		stop on word: not implemented

--------------------------------------------------------------------------*/


void cfubc(int f,int ext,int intc[],int cb[])
{
	int count;

	count = cb[0];
	do
	{
		cfsa (f,ext,intc,&q);
		if (q == 0)
		{
			count == 0;	/* stop on no q */
		}
		else
		{
			++cb[1];		/* increment tally count */
			++intc;			/* next data array */
			--count;
		}
	}
	while (count);
}

/*-------------------------------------------------------------------------*/
/* CFUBR: Execute Repeat Mode Block Transfer     		  			   */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfmad (int f,int ext,int intc[],int cb[])
	uint_32 ext ------ INPUT: CAMAC address
	uint_32 f -------- INPUT: CAMAC function
	uint_32 intc ----- INPUT/OUTPUT: camac data array
	uint_32 cb ------- INPUT: cb[0] = repeat count
				  cb[1] = tally
				  cb[2] = LAM identification
				  cb[3] = channel identification

Description
	Emulate a block transfer.
		terminate on repeat count
		stop on word: not implemented

--------------------------------------------------------------------------*/


void cfubr(int f,int ext,int intc[],int cb[])
{
	int count;

	count = cb[0];
	do
	{
		do
		{
		   cfsa (f,ext,intc,&q);
		}
		while (q == 0);
		++cb[1];		/* increment tally count */
		++intc;			/* next data array */
		--count;
	}
	while (count);
}

/*

void cfubl(int f,int ext,int intc[],int cb[])
{ int i,q;
  for (i=0;i<cb[0];i++) {
    CLPOLL(cb[2]);
    CFSA(f,ext,&(intc[i]),&q);
    if (!q) {
      i--;
      break;
    }
  }
  cb[1]=i+1;
}
*/

/*********** non standard functions from TRIUMF VDACS/TWOTRAN ***************/


/*-------------------------------------------------------------------------*/
/* CFSCAN : Execute Q-scan block transfer on successive sub-addresses                                 */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfscan (int b, int c, int n, int a, int f, int intc[], int r)
	uint_32 b ------ INPUT: CAMAC branch
	uint_32 c ------ INPUT: CAMAC crate
	uint_32 n ------ INPUT: CAMAC slot
	uint_32 a ------ INPUT: CAMAC subaddress
	uint_32 f -------- INPUT: CAMAC function
	uint_32 intc ----- INPUT/OUTPUT: camac data array
	uint_32 r ------- INPUT: repeat count

Description
	Read successive subaddresses unitl repeat count or no q
--------------------------------------------------------------------------*/


void cfscan(int b, int c, int n, int a, int f, int intc[], int r)
{
	int count, ext;

	count = r;
	cdreg(&ext,b,c,n,a);
	do
	{
		cfsa (f,ext,intc,&q);
		if (q == 0)
		{
			count == 0;	/* stop on no q */
		}
		else
		{
			ext = ext + (1<<7);	/* next sub-address */
			++intc;			/* next data array */
			--count;
		}
	}
	while (count);
}

/*-------------------------------------------------------------------------*/
/* CFQSTOP : Execute Q-scan block transfer on same subaddress              */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfqstop (int b, int c, int n, int a, int f, int intc[], int r)
	uint_32 b ------ INPUT: CAMAC branch
	uint_32 c ------ INPUT: CAMAC crate
	uint_32 n ------ INPUT: CAMAC slot
	uint_32 a ------ INPUT: CAMAC subaddress
	uint_32 f -------- INPUT: CAMAC function
	uint_32 intc ----- INPUT/OUTPUT: camac data array
	uint_32 r ------- INPUT: repeat count

Description
	Read successive subaddresses unitl repeat count or no q
--------------------------------------------------------------------------*/


void cfqstop(int b, int c, int n, int a, int f, int intc[], int r)
{
	int count, ext;

	count = r;
	cdreg(&ext,b,c,n,a);
	do
	{
		cfsa (f,ext,intc,&q);
		if (q == 0)
		{
			count == 0;	/* stop on no q */
		}
		else
		{
			++intc;			/* next data array */
			--count;
		}
	}
	while (count);
}


/*****************************************************************************/
/*             Functions for CES8210 Front panel interrupts                  */
/*****************************************************************************/
/*=============================================================================
** Name extIsr.c
** Purpose: front panel interrupt IT2 handler
*/

void extIsr()
{
	uint_16 csr, ifr;
	uint_32 cbd_ifr, ext, data, q;


	(* extisrs)();		/* go to trigger routine */
	clrInt();		/* clear external interrupt */

/* 
	csr = *C_SRC;
       csr = csr & 0x0003;
       if (csr == 0x0001)
		printf ("\nextIsr: interrupt level 2");
	else if (csr == 0x0001)
		printf ("\nextIsr: interrupt level 4");
	printf (": done");
*/     
	taskResume(taskid);
}



disableInt(intnum)
int intnum;
{
	uint_32 cbd_csr_r, cbd_ifr;
	uint_16 csr;
	cbd_csr_r= CSR;			/* address: camac control/status reg */
	csr = *(uint_16 *)cbd_csr_r;	/* read csr */
	if (intnum == 2)
		csr = csr | MIT2;
	else if (intnum == 4)
		csr = csr | MIT4;
	*(uint_16 *)cbd_csr_r = csr;
}
enableInt(intnum)
int intnum;
{
	uint_32 cbd_csr_r, cbd_ifr;
	uint_16 csr;
	cbd_csr_r= CSR;			/* address: camac control/status reg */
	csr = *(uint_16 *)cbd_csr_r;	/* read csr */
	if (intnum == 2)
		csr = csr & ~MIT2;
	else if (intnum == 4)
		csr = csr & ~MIT4;
	*(uint_16 *)cbd_csr_r = csr;
}

clrInt()
{
	uint_32 cbd_ifr;
	uint_16 ifr;

	cbd_ifr = IFR;			/* address: interrupt flag register */
	ifr = 0;
	*(uint_16 *)cbd_ifr = ifr;
}
