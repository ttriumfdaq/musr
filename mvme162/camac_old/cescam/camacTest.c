typedef unsigned long   uint_32;
typedef unsigned short   uint_16;

#include "vxWorks.h"
#include "intLib.h"
#include "taskLib.h"
#include "vme.h"
#include "ces8210.h"

#define	   ram_bas	    (char *)0xfffe102b
#define	   slave_config_reg (char *)0xfffe2007

extern int DEBUG;
extern int POLLING;
extern VOIDFUNCPTR extisrs;	/* pointer to front panel interrupt handler */
extern int badadr;

void softHandler2();
void softHandler4();
void wait();

showcsr()
{
	uint_32 cbd_csr_r;
	uint_16 csr;
	uint_32 vmeAddr, cbd_ifr, ext, data, q;
 	STATUS sysBusToLocalAdrs(), status;

	vmeAddr = camac_csr | 0x02;
/*	csr = *(uint_16 *)vmeAddr; */
	csr = *CAMAC_CSR;
	printf ("csr = %x\n",csr);
}

showcstat()
{
	int istat;
	ctstat(&istat);
	printf ("CTSTAT: %d ",istat);
	switch(istat)
	{

		case 0:	printf ("q(1) x(1)\n");
				break;
		case 1:	printf ("q(0) x(1)\n");
				break;

		case 2:	printf ("q(1) x(0)\n");
				break;

		case 3:	printf ("q(0) x(0)\n");
				break;
		case 4:	printf ("bad address\n");
				break;

	}
}


readc()
{
	camac(1,4,0,0);
}

softInt(intnum)
int intnum;
{
	uint_32 cbd_ifr;
	uint_16 ifr;

	cbd_ifr = IFR;			/* address: interrupt flag register */
	if (intnum == 2)
		ifr = IT2;
	if (intnum == 4)
		ifr = IT1;
	*(uint_16 *)cbd_ifr = ifr;
}

void setup()
{
	uint_32 triglam, status;
	clrInt();
	status = intConnect( (VOIDFUNCPTR *)(85<<2), (VOIDFUNCPTR)softHandler2,0);
	enableInt(2);		/* unmask 8210 external interrupt 2 */
	sysIntEnable(2);	/* enable bus interrupt level 2 */
	printf ("\nsetup for external interrupt 2\n");	
}


cesInit()
{
	int status;
	uint_32 xvect=170;
	FUNCPTR extIsr;

	sysIntEnable(4);	/* enable bus interrupt level 4 */
	sysIntEnable(2);	/* enable bus interrupt level 2 */
	enableInt(2);		/* unmask 8210 external interrupt 2 */
	enableInt(4);		/* unmask 8210 external interrupt 4 */
/*
	status = intConnect( (VOIDFUNCPTR *)(170<<2), (VOIDFUNCPTR)softHandler2,0);
	status = intConnect( (VOIDFUNCPTR *)(85<<2), (VOIDFUNCPTR)softHandler4,0);
*/
}



void softHandler2()
{
        uint_16 stat_134, control_134, switch_status,last_status;
        uint_32 dsp, swr, csr_addr, ext, q, csr, *vme_addr;
	uint_32 data;
        int   b,c,n,a,f;

	b = 0;
	c = 1;
	n = 18;
	a = 0;
	f = 0;



	cdreg(&ext,b,c,n,a);
	cdreg(&dsp,b,c,20,a);
	data = 0x55;
	cfsa (f,ext,&data,&q);
	if (DEBUG) printf ("\nmytest: data = %x q = %d\n",data,q);
	clrInt();
}

void softHandler4()
{
        uint_16 stat_134, control_134, switch_status,last_status;
        uint_32 dsp, swr, ext, q, camac_status, *camac_p;
        int   b,c,n,a, i, count=1;

	b = 0;
	c = 1;
	n = 20;
	a = 0;

	cdreg(&ext,b,c,n,a);
	for (i = 0; i<= count; ++i)
	{
		camac_status = 1;
        	for (n = 1; n <= 23; ++n)
        	{
                	camac_status = camac_status << 1;
			cfsa (16,ext,&camac_status,q);
                	wait(10000);
		}
         
		for (n = 1; n <= 23; ++n)
		{
                	camac_status = camac_status >> 1;
			cfsa (16,ext,&camac_status,q);
                	wait(10000);
		}
		wait(10000);

	}
	clrInt();
}



mytest()
{
        uint_16 stat_134, control_134, switch_status,last_status;
        uint_32 dsp, swr, csr_addr, ext, q, csr, *vme_addr;
	uint_32 data;
        int   b,c,n,a,f;
	int extb[2],intc[10],cb[4], i;

	b = 0;
	c = 1;
	n = 18;
	a = 0;
	f = 0;

	cdreg (&extb[0],b,c,n,a);
	cdreg (&extb[1],b,c,n,4);
	cb[0] = 10;
	cb[1] = 0;
	cfubc(f,extb[0],intc,cb);
	for (i=0;i<=cb[0];++i)
		printf ("\narray[%d] = %x",i,intc[i]);
	printf ("  tally = %d\n",cb[1]);
	
}


camac(c,n,a,f)
int c,n,a,f;
{
        uint_16 stat_134, control_134, switch_status;
        uint_32 dsp, swr, csr_addr, ext, q, csr, *vme_addr;
        int   b, last_status;
		uint_32 data;
	b = 0;
	
	if (f >= 16)
	{
		printf ("\nEnter data: ");
		scanf ("%x",&data);
	}

	cdreg(&ext,b,c,n,a);
	if (badadr)
	{
		printf ("\nerror: bad address, b%d, c%d, n%d, a%d\n",b,c,n,a);
		return;
	}
	cfsa (f,ext,&data,&q);
	printf ("CAMAC: b(%d),c(%d),n(%d),a(%d) f(%d) data(%x)  q(%d)\n",
				b,c,n,a,f,data,q);

}




scamac(c,n,a,f)
int c,n,a,f;
{
        uint_16 stat_134, control_134, switch_status;
        uint_32 dsp, swr, csr_addr, ext, q, csr, *vme_addr;
        int   b, last_status;
		uint_32 data;
	b = 0;
	
	if (f >= 16)
	{
		printf ("\nEnter data: ");
		scanf ("%x",&data);
	}

	cdreg(&ext,b,c,n,a);
	if (badadr)
	{
		printf ("\nerror: bad address, b%d, c%d, n%d, a%d\n",b,c,n,a);
		return;
	}
	cssa (f,ext,&data,&q);
	printf ("SCAMAC: b(%d),c(%d),n(%d),a(%d) f(%d) data(%x)  q(%d)\n",
				b,c,n,a,f,data,q);

}


bounce_forever(delay)
unsigned int delay;
{
	for (;;)
	{
		bounce(delay);
	}
}


/* bounce a single bit up and down a dataway display */


bounce(delay)
unsigned int delay;
{
		unsigned int count=0;
        uint_16 stat_134, control_134, switch_status,last_status;
        uint_32 dsp, swr, ext, q, camac_status, *camac_p;
        static int   b,c,n,a, i;

	if (delay == 0)
		delay = 100000;

	b = 0;
	c = 1;
	n = 20;
	a = 0;

	cdreg(&ext,b,c,n,a);
	for (i = 0; i<= count; ++i)
	{
		camac_status = 1;
        for (n = 1; n <= 23; ++n)
        {
            camac_status = camac_status << 1;
			/* logMsg("Begin cfsa"); */
			cfsa (16,ext,&camac_status,&q);
			/* logMsg("cfsa complete\n"); */
            wait(delay);
		}
         
		for (n = 1; n <= 23; ++n)
		{
            camac_status = camac_status >> 1;
			cfsa (16,ext,&camac_status,&q);
            wait(delay);
		}
	}
}

int cmem_value;

cmem()

{
	uint_32 ext, q;
	int   b, c, n, a, f;
	uint_32 data;

	b = 0; c=1;n=15;a=0;f=16;
	
	cdreg(&ext,b,c,n,a);
	cfsa (f,ext,&cmem_value,&q);
	cmem_value++;
}
	


void wait(int count)
{
        int i;
        for (i = 0; i <= count; ++i)
                i = i;
}



/* These are the successfully test camacLib functions */

myz()
{
	uint_32 ext;
	cdreg(&ext,0,1,28,8);
	cccz(ext);
}

myc()
{
	uint_32 ext;
	cdreg(&ext,0,1,28,9);
	cccc(ext);
}

myi(l)
int l;
{
	uint_32 ext;
	cdreg(&ext,0,1,30,9);
	ccci(ext,l);
}

testi()
{
	int l;
	uint_32 ext, data=0, q;
	cdreg(&ext,0,1,30,9);
	/*
	cssa(27,ext,&data,&q);
	printf ("Inhibit = %d\n",q);
	*/
	ctci(ext,&l);
	printf ("\nInhibit = %d\n",l);

}


