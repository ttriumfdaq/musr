/*-----------------------------------------------------------------------------
 * Copyright (c) 1991,1992 Southeastern Universities Research Association,
 *                         Continuous Electron Beam Accelerator Facility
 *
 * This software was developed under a United States Government license
 * described in the NOTICE file included as part of this distribution.
 *
 * CEBAF Data Acquisition Group, 12000 Jefferson Ave., Newport News, VA 23606
 * Email: coda@cebaf.gov  Tel: (804) 249-7101  Fax: (804) 249-7363
 *-----------------------------------------------------------------------------
 * 
 * Description:
 *   An implementation of the CAMAC standard routines in C,
 *   using C argument passing conventions.
 *	
 *	
 * Author:  Graham Heyes, Chip Watson, CEBAF Data Acquisition Group
 *
 * Revision History:
 *   $Log: cebafLib.c,v $
 *   Revision 1.1.1.1  1995/10/22 02:30:17  ted
 *   import from decu18
 *
 *	  Revision 1.5  1993/05/13  14:53:29  watson
 *	  add sysIntEnable(2) to ccinit
 *
 *	  Revision 1.4  1993/05/05  15:42:59  heyes
 *	  fix cclnk, cculnk
 *
 *	  Revision 1.3  1992/12/08  18:53:41  watson
 *	  moved most code into macro file ks2917.h; added block I/O routines
 *
 *	  Revision 1.2  1992/07/21  18:08:32  heyes
 *	  fixed Q truth value in ctlm
 *
 *	  Revision 1.1  1992/06/05  20:25:36  watson
 *	  Initial revision
 *
 * 03-sep-91 cw  modified some arguments to pass by value, not reference.
 * 06-sep-91 cw  clear high 8 bits on reads
 * 11-nov-91 cw  modified to be non-interruptable while performing I/O so
 *               that hardware can be shared by 2 tasks and/or interrupts
 * 03-jun-92 cw  added code for ctcd
 */

/* Functions (all return void):
 *
 * cdreg(int *ext,int b,int c,int n,int a)   define external address 
 * cssa(int f,int ext,short *dat,int *q)     short (16) single action 
 * cfsa(int f,int ext,int *dat,int *q)       long (24) single action 
 * cccz(int ext)			 crate initialize 
 * cccc(int ext)			 crate clear 
 * ccci(int ext,int l)			 set/reset crate inhibit 
 * ctci(int ext,int *l)			 test crate inhibit 
 * cccd(int ext,int l)			 ena/disable crate demand 
 * ctcd(int ext,int *l)			 test crate demand enabled
 * ccgl(int lam,int l)			 ena/disaable graded lam (NON STANDARD)
 * ctgl(int ext,int *l)			 test graded lam
 * cdlam(int *lam,int b,int c,int n,int a,int inta[]) 	 define lam 
 * cclc(int lam)			 clear lam (dataless function)
 * cclm(int lam,int l)			 alternative lam clears 
 * ctlm(int lam,int *l)			 test lam 
 * ccinit(int b)			 initialize branch 
 * cclnk(int lam,void *())		 link routine to lam 
 * cculk(int lam,void *())		 unlink routine from lam 
 * ccrgl(int lam)			 re-enable graded lam (@end of isr)
 * cclwt(int lam)			 lam wait 
 * ctstat(int *istat)			 test status of previous operation 
 */

#include "vxWorks.h"
#include "intLib.h"
#include "taskLib.h"
#include "ks2917.h"
#include "vme.h"
#include "sigLib.h"

int camacStatus;
int camacErrcnt;
short cdreg_pattern[MAX_CDREG_PATTERN];
VOIDFUNCPTR lamisrs[25][8];
int taskidlam[25][8];

void lamIsr();
void camacBusError();

void ccinit(int b)
{
  int i,status;
  unsigned short data;
  struct sigvec sigbus;
  camacStatus = camacErrcnt = 0;
  if (b==0) {			/* only one branch implemented */
/*  This doesn't work right yet, so leave it off
    sigbus.sv_handler = camacBusError;
    sigbus.sv_mask = 0;
    sigbus.sv_flags = 0;
    if (sigvec(SIGSEGV,&sigbus,0)!=OK)
      printf("Failed to initialize bus error handler for CAMAC!");
*/
    status = vxMemProbe(&ks2917_csr,READ,2,&data);
    if (status==OK) {
      ks2917_csr = ks2917_reset;
      ks2917_cmar = 0;		/* preload halt's everywhere */
      for (i=0;i<8192;i++) ks2917_cmr = ks2917_halt;
      for (i=0;i<MAX_CDREG_PATTERN;i++) cdreg_pattern[i]=0;
      ks2917_sccr = ks2917_sccr_abrt; /* abort any left over DMA */
      ks2917_amr = VME_AM_EXT_USR_DATA |0x40; /* DMA address modifier | LWORD* */

      intConnect((VOIDFUNCPTR *) (KS2917_LAM_VEC<<2), (VOIDFUNCPTR) lamIsr,0);
      ks2917_ivr_lam = KS2917_LAM_VEC;	/* int vector */
      ks2917_icr_lam = 0x1a;	/* ena ints, auto-clr, level 2 */
      sysIntEnable(2);		/* enable bus interrupts on level 2 */
      camacStatus = 1;
    }
  }
}

void camacBusError(int sig,int code,SIGCONTEXT *sigContext)
{
  camacErrcnt++;
}

void cdreg(int *ext,int b,int c,int n,int a)
{ CDREG(ext,b,c,n,a); }

void cdreg_1(int *ext,int b,int c,int n,int a,int i,int match)
{
  int temp;
  if (!(camacStatus) || (i==(MAX_CDREG_PATTERN-1)) ||
      (b) || ((c)&(0xfffffff8)) || ((n)&(0xffffffe0)) || ((a)&(0xfffffff0)) )
    *ext = -1;
  else {
    cdreg_pattern[i] = match ;
    temp = ((n)<<9) | ((a)<<5); 
    *ext = ((18*i) << 16) | temp; 
    CAMAC_RAISE_IPL; 
    ks2917_cmar = 18*i; /* jump to address in list memory */ 
    ks2917_cmr = (c)<<8 | ks2917_ws24; /* single word I/O */
    ks2917_cmr = temp; 
    ks2917_cmr = ks2917_halt; 
    ks2917_cmr = (c)<<8 | ks2917_tm_block | ks2917_qm_scan; /* q scan */ 
    ks2917_cmr = temp; 
    ks2917_cmr = -1; /* LS transfer length */ 
    ks2917_cmr = -1; /* MS transfer length */ 
    ks2917_cmr = ks2917_halt; 
    ks2917_cmr = (c)<<8 | ks2917_tm_block | ks2917_qm_stop; /* q stop */ 
    ks2917_cmr = temp; 
    ks2917_cmr = -1; /* LS transfer length */ 
    ks2917_cmr = -1; /* MS transfer length */
    ks2917_cmr = ks2917_halt;
    ks2917_cmr = (c)<<8 | ks2917_tm_block | ks2917_qm_repeat;/* q repeat */
    ks2917_cmr = temp;
    ks2917_cmr = -1; /* LS transfer length */
    ks2917_cmr = -1; /* MS transfer length */
    ks2917_cmr = ks2917_halt;
    CAMAC_LOWER_IPL;
  }
}

void cfsa(int f,int ext,unsigned long *dat,unsigned long *q)
{ CFSA(f,ext,dat,q); }

void cccz(int ext)
{ CCCZ(ext); }

void cccc(int ext)
{ CCCC(ext); }

void ccci(int ext,int l)
{ CCCI(ext,l); }

void ctci(int ext,int *l)
{ CTCI(ext,l); }

void cccd(int ext,int l)
{ CCCD(ext,l); }

void ctcd(int ext,int *l)
{ CTCD(ext,l); }

void ccgl(int lam,int l)
{ CCGL(lam,l); }
  
void ctgl(int ext,int *l)
{ CTGL(ext,l); }

void cdlam(int *ext,int b,int c,int n,int a,int inta[2])
{ CDLAM(ext,b,c,n,a,inta); }
     
void cclm(int lam,int l)
{ CCLM(lam,l); }

void cclc(int lam)
{ CCLC(lam); }

void ctlm(int lam,int *l)
{ CTLM(lam,l); }

void lamIsr()
{
  int ii,jj,pattern;
  for (ii=0;ii<8;ii++)		/* loop over crates */
    if (ks2917_srr & (1<<ii)) {
      ks2917_cmar = 0;
      ks2917_cmr = ks2917_ws24 + (ii<<8); /* read LAM pattern */
      ks2917_cmr = ((30<<9) |(12<<5)| 1 ); /* N=30 A=12 F=1 */
      ks2917_cmr = ks2917_halt;
      ks2917_cmar = 0;
      ks2917_csr =  ks2917_go;
      KS2917_WHEN_RDY(pattern = ks2917_dlr + (ks2917_dhr<<16););
      ks2917_wait_done;
      
      ks2917_cmar = 0;
      ks2917_cmr = ks2917_ws24 + (ii<<8); /* read LAM mask */
      ks2917_cmr = ((30<<9) |(13<<5)| 1 ); /* N=30 A=12 F=1 */
      ks2917_cmr = ks2917_halt;
      ks2917_cmar = 0;
      ks2917_csr =  ks2917_go;
      KS2917_WHEN_RDY(pattern &= (ks2917_dlr + (ks2917_dhr<<16)););
      ks2917_wait_done;
      for (jj=0;jj<25;jj++)	/* loop over slots */
	if ((pattern&(1<<jj))&&(lamisrs[jj][ii]!=0)) {
	  (* lamisrs[jj][ii])();
	  taskResume(taskidlam[jj][ii]);
	}
    }
}

void cclnk(int lam,VOIDFUNCPTR label)
{
  int crate,slot;
  ks2917_cmar = *((short *)&(lam));
  crate = (ks2917_cmr>>8) & 7;
  slot = ((lam>>9) & 31) - 1;
  lamisrs[slot][crate] = label;
  taskidlam[slot][crate] = taskIdSelf();
}

void cculk(int lam,VOIDFUNCPTR label)
{
  int crate,slot;
  ks2917_cmar = *((short *)&(lam));
  crate = (ks2917_cmr>>8) & 7;
  slot = ((lam>>9) & 31) - 1;
  lamisrs[slot][crate] = 0;
}
  
void ccrgl(int lam)
{ CCRGL(lam); }

void cclwt(int lam)
{ taskSuspend(0); }

void ctstat(int *istat)
{ CTSTAT(istat); }

/*********************** Multiple Action Routines ***************************/

void cfga(int f[],int exta[],int intc[],int qa[],int cb[])
{ int i;
  for (i=0;i<cb[0];i++) {
    CFSA(f[i],exta[i],&(intc[i]),&(qa[i]));
  }
  cb[1] = cb[0];
}

void cfmad(int f,int extb[],int intc[],int cb[])
{ CFBLOCK(f,extb,intc,cb,KS2917_OFFSET_QSCAN); }

void cfubc(int f,int ext,int intc[],int cb[])
{ CFBLOCK(f,&ext,intc,cb,KS2917_OFFSET_QSTOP); }

void cfubr(int f,int ext,int intc[],int cb[])
{ CFBLOCK(f,&ext,intc,cb,KS2917_OFFSET_QREPEAT); }

void cfubl(int f,int ext,int intc[],int cb[])
{ int i,q;
  for (i=0;i<cb[0];i++) {
    CLPOLL(cb[2]);
    CFSA(f,ext,&(intc[i]),&q);
    if (!q) {
      i--;
      break;
    }
  }
  cb[1]=i+1;
}

/************************ Short Data-Word Transfers ************************/

void cssa(int f,int ext,unsigned short *dat,unsigned long *q)
{ CSSA(f,ext,dat,q); }

void csga(int f[],int exta[],unsigned short intc[],int qa[],int cb[])
{ int i;
  for (i=0;i<cb[0];i++) {
    CSSA(f[i],exta[i],&(intc[i]),&(qa[i]));
  }
  cb[1] = cb[0];
}

void csmad(int f,int extb[],unsigned short intc[],int cb[])
{ CSBLOCK(f,extb,intc,cb,KS2917_OFFSET_QSCAN); }

void csubc(int f,int ext,unsigned short intc[],int cb[])
{ CSBLOCK(f,&ext,intc,cb,KS2917_OFFSET_QSTOP); }

void csubr(int f,int ext,unsigned short intc[],int cb[])
{ CSBLOCK(f,&ext,intc,cb,KS2917_OFFSET_QREPEAT); }

void csubl(int f,int ext,unsigned short intc[],int cb[])
{ int i,q;
  for (i=0;i<cb[0];i++) {
    CLPOLL(cb[2]);
    CSSA(f,ext,&(intc[i]),&q);
    if (!q) {
      i--;
      break;
    }
  }
  cb[1]=i+1;
}


/********************** Diagnostics *************************************/

ksdump()
{
  int status;
  unsigned short data;
  printf(" KS2917 interface -----------------------------------------\n");
  status = vxMemProbe(&ks2917_cser,READ,2,&data);
  if (status==OK) {
    printf("   cser: %8x",data);
    if (data & ks2917_cser_coc) printf(" COC");
    if (data & ks2917_cser_ndt) printf(" NDT");
    if (data & ks2917_cser_err) printf(" ERR");
    if (data & ks2917_cser_act) printf(" ACT");
    if ((data & ks2917_cser_code)==ks2917_cser_berr) printf(" BERR");
    if ((data & ks2917_cser_code)==ks2917_cser_abrt) printf(" SOFT_ABRT");
    printf("\n");
  } else {
    printf("bus error accessing cser\n");
    return;
  }

  status = vxMemProbe(&ks2917_csr,READ,2,&data);
  if (status==OK) {
    printf("    csr: %8x",data);
    if (data & ks2917_go) printf(" GO");
    if (data & ks2917_write) printf(" WRITE");
    if (data & ks2917_dma) printf(" DMA");
    if (data & ks2917_done) printf(" DONE");
    if (data & ks2917_rdy) printf(" RDY");
    if (data & ks2917_timeo) printf(" TIMEO");
    if (data & ks2917_error) printf(" ERROR");
    printf("\n");
  } else {
    printf("bus error accessing cser\n");
    return;
  }

  printf("    mtc: %8x\n",ks2917_mtc);
  printf("  machi: %8x\n",ks2917_machi);
  printf("  maclo: %8x\n",ks2917_maclo);
  printf("    cwc: %8x\n",ks2917_cwc);
  printf("   cmar: %8x\n",ks2917_cmar);
}
