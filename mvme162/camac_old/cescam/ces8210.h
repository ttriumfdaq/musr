/* Internal Register Definitions */
/* ces 8210 Internal Register Definitions */


#define BASE		             0x00800000	 /* camac base address */
#define A32D16		   	     0xf0000002  	/* A32D16 camac base address */

#define C_SRC   (uint_32 *)(BASE   | 0x0080e800) /* camac control and status reg */
#define CSR	    A32D16 | 0x0080e800  /* camac control and status reg */
#define IFR     A32D16 | 0x0080e810  /* interrupt flag register */
#define ICR1_C  A32D16 | 0x0080e814  /* interrupt control reg 1: Control */
#define ICR1_D  A32D16 | 0x0080e804  /* interrupt control reg 1: data */
#define ICR2_C  A32D16 | 0x0080e818  /* interrupt control reg 2: Control */
#define ICR2_D  A32D16 | 0x0080e808  /* interrupt control reg 2: data */
#define ICR3_C  A32D16 | 0x0080e81c  /* interrupt control reg 3: Control */
#define ICR3_D  A32D16 | 0x0080e80c  /* interrupt control reg 3: data */
#define CAR	A32D16 | 0x0080e820  /* Crate Address Register */
#define BTB	A32D16 | 0x0080e824  /* on-line status (R) */
#define BZ	A32D16 | 0x0080e824  /* reset camac branch BZ (W)
#define GL	A32D16 | 0x0080e828  /* GL Register */


/* bit definitions */

/* Control and Status Register */

#define IT4	0x0001		/* front panel int IT4 status */
#define IT2	0x0002		/* front panel int IT2 status */
#define MIT4	0x0004		/* IT4 mask 0 = enabled */
#define MIT2	0x0008		/* IT2 mask 0 = enabled */

/* Interrupt Flag Register */

#define IT1	0x0001		/* set/reset external interrupt 1 */
#define IT2	0x0002		/* set/reset external interrupt 2 */

/* THE BASIC ADDRESS FOR COMPUTING ADDRESS  */

#define    cbd_bas_vadr     0x00800000
#define    cbd_bas_ladr     0xf0800000
#define	   camac_csr		0x0080e800
#define    CAMAC_CSR		(uint_16 *)0x0080e802
#define    am_reset         0        /* clear all IRR and all IMR bits */
#define    am_clirr         0x40     /* clear all IRR  */
#define    am_clirrb        0x48     /* clear IRR bit (0,1,2) */
#define    am_clirrmrb      0x18     /* clear IRR and IMR bit (0,1,2) */
#define    am_clisrb        0x78     /* clear ISR bit (0,1,2)  */
#define    am_load04        0x80     /* load mode bits 0-4 */
#define    am_load56        0xa1     /* load mode bits 5,6 set bit 7 */
#define    am_rsmem         0xe0     /* load bits 3,4 into byte count reg. and
                                        select response memory(bits 0,1,2) */ 
#define    am_sirr          0x58     /* set IRR bits (0,1,2)  */
#define    am_stimrb        0x38     /* set single imr bit   */
#define    am_prio          6
#define    am_vect          1
#define    am_irm           2
#define    am_gint          3
#define    am_ireq          4
#define    am_md_ini        1<<am_ireq

