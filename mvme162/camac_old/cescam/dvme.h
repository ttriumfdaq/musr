/* File: dvme.h	*/

/* Include file for vme/g++ applications */

/* Define Message Buffers */

static short x = 0;
#define NOMSG ((char*)&x)
#define INIT_PORT 2
#define BSIZE 48
#define RESET_ADDR (int *)0xFFFDC000

struct msg
{
	uint_16 size;
	uint_16 filler;
	char buf[BSIZE];
	uint_32 addr;
	uint_32 data;
};

struct CHILDMSG
{
	uint_16 size;
	uint_32  dat;
};


/*---------------------------------------------------------------------------*/
/*                      DEFINE CLASS DVME_750                                */
/*---------------------------------------------------------------------------*/

class dvme_750 {
	uint_16 upper_sw;
	uint_16 lower_sw;
	uint_16 sw_status;
	uint_16* USLED_REG_750  = (uint_16 *)0xFE1003;
	uint_16* LSLED_REG_750  = (uint_16 *)0xFE1001;
	uint_16* UPPER_SW	= (uint_16 *)0xFE100F;
 	uint_16* LOWER_SW	= (uint_16 *)0xFE100D;
	int* RESET           = (int *)0xFE1401;
public:
	void upper_led_only();
	void lower_led_only();
	uint_16 switches();
 	void reset(); 
};

/* --------------------  Define Member Functions -----------------------------*/


void dvme_750::upper_led_only()
{
	*(unsigned char *)USLED_REG_750 = 0x7F;              /* upper led on */
        *(unsigned char *)LSLED_REG_750 = 0xFF;  
}

void dvme_750::lower_led_only()
{
        *(unsigned char *)USLED_REG_750 = 0xFF;              /* upper led off */
        *(unsigned char *)LSLED_REG_750 = 0x7F;  
}	 
uint_16 dvme_750::switches()
{
	upper_sw = *UPPER_SW;
	lower_sw = *LOWER_SW;
	sw_status = lower_sw | (upper_sw << 1);
	return(sw_status);
}


void dvme_750::reset()
{
	*(int *)RESET = 0xffffffff;
}


/*---------------------------------------------------------------------------*/
/*                      DEFINE CLASS MESSENGER                               */
/*---------------------------------------------------------------------------*/

class messenger {
	struct msg request;
	int port;
	uint_32 id, portset, sigset, pmask;
	TaskId taskid;
	int status;
public:
	messenger(uint_32,uint_32);
	messenger();
	void        initialize();
	uint_32     get_command();
	uint_32     send(char*, char*, uint_32, uint_32);
	char*       receive(uint_32*, uint_32* );
	struct msg* receive(void);
	void        reply(int);
};
/* --------------------  Define Member Functions -----------------------------*/

messenger::messenger(uint_32 p, uint_32 s)		/* constructer */
{
	request.size = sizeof(request);
	portset = p ? p : 1 << INIT_PORT;
	sigset = s  ? s : NOSIGS;
}

messenger::messenger()				/* constructer with defaults */
{
	request.size = sizeof(request);
	portset = 1 << INIT_PORT;
	sigset = NOSIGS;
	_log_gossip("PROC04: Device Constructer Active");
	_printf ("\nDevice created");
}

uint_32 messenger::get_command()	/* receive command over message port */
{
	id = u_receive((char *)&request,portset,sigset,&port,10);
	return(id);
}

	/* send command over message port */

uint_32 messenger::send(char* dst,char* msg, uint_32 data, uint_32 addr)
{
	request.data = data;
	request.addr = addr;
	strcpy(request.buf,msg);
	request.size = sizeof(request);
	status = _lookup_name(dst,&taskid);
	id = u_send((char *)&request, (char *)&request, taskid, port, FOREVER);
	return(id);
}


char *	messenger::receive(uint_32 *data, uint_32 *addr )	/* read data */
{
	*data = request.data;
	*addr = request.addr;
	*addr = request.addr;
	return(request.buf);
}

struct msg* messenger::receive(void)    /*  data part of message */
{
		
	return(&request);
}

void messenger::reply(int return_id)			/* reply to message */
{
	_reply((char *)&request,return_id);	/* return data */
}

