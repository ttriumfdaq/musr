typedef unsigned long   uint_32;
typedef unsigned short   uint_16;

#include "vxWorks.h"
#include "intLib.h"
#include "taskLib.h"
#include "vme.h"


#define	   ram_bas	    (char *)0xfffe102b
#define	   slave_config_reg (char *)0xfffe2007

readRBA(base)
char *base;
{
	*base = *ram_bas;
	printf ("\nram base_addr_reg = %x\n",*base);
}

writeRBA(base)
char base;
{
	*ram_bas = base;
}

readAddr(addr)
int *addr;
{
	printf ("\naddr %x = %x\n",addr,*addr);
}

writeAddr(addr,data)
int *addr, data;
{
	*addr = data;
}

enableDRAM()
{
	*slave_config_reg = 0x80;
}


/* bounce a single bit up and down a dataway display */

fTop()
{
	uint_32 *addr = 0, value;
	int finish = 1;
	do
	{
		value = *addr;
		++addr;

	}
	while (finish);

}


camac(c,n,a,f)
int c,n,a,f;
{
        uint_16 stat_134, control_134, switch_status,last_status;
        uint_32 dsp, swr, csr_addr, ext, q, data, csr, *vme_addr;
        int   b;
	b = 0;
/*
	c = 1;
	n = 18;
	a = 0;
	f = 0;
*/


	cdreg(&ext,b,c,n,a);
	data = 0x55;
	cfsa (f,ext,&data,q);
	/* vme_addr = (uint_32 *)0x819002; */
	data = *vme_addr;
/*	csr=(cbd_bas_ladr|29<<11)+2;  */  /* c=0,n=29,a=0,f=0  */
	printf ("CAMAC: addr = %x, data = %x,status = %xn",ext,data,csr);

	/* cccz(ext); */
	
}

lcam()
{
        uint_16 stat_134, control_134, switch_status,last_status;
        uint_32 dsp, swr, ext, q, data, *vme_addr, i=1;
        int   b,c,n,a,f;

	b = 7;
	c = 1;
	n = 1;
	a = 0;
	f = 0;

	cdreg(&ext,b,c,n,a);
	vme_addr =  (uint_32 *)(ext + ((uint_32 )f << 2));
	do
	{
		printf ("\ncamac: vme_address = %lx",vme_addr);
		*vme_addr = (uint_32)vme_addr;
		vme_addr = vme_addr + 2;
		++i;
	}
	while (i > 0);
 

}

void
bounce_forever(msgp)
struct smallmsg *msgp;
{
        uint_16 stat_134, control_134, switch_status,last_status;
        uint_32 dsp, swr, ext, q, camac_status, *camac_p;
        int   b,c,n,a;

	b = 7;
	c = 1;
	n = 22;
	a = 0;

	cdreg(&ext,b,c,n,a);
	for (; ;)
	{
		camac_status = 1;
        	for (n = 1; n <= 23; ++n)
        	{
                	camac_status = camac_status << 1;
			cfsa (16,ext,&camac_status,q);
                	c_delay();
		}
         
		for (n = 1; n <= 23; ++n)
		{
                	camac_status = camac_status >> 1;
			cfsa (16,ext,&camac_status,q);
                	c_delay();
		}
		c_delay();

	}
}



