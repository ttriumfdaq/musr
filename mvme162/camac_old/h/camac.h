/* Camac.h */

/* Include File for Camac Functions - Argument Checking */

#ifndef NARGS
extern void cdreg(UINT32 *, UINT32, UINT32, UINT32, UINT32);
extern void cfsa(UINT32, UINT32, UINT32 *, int *);
extern void cfsax(int, long int, long int *, int *, int *);
extern void cycle();
extern void cssa(UINT32, UINT32, UINT32 *, int *);
extern void cgo(int, long int, int *);
extern void rd2401(int, long int, long int *, int *);
extern void wr2401(int, long int, long int *, int *);
extern void cccz(long int);
extern void cccc(long int);
extern void ccci(long int, int);
extern void ctstat(int *);
extern void rstcam(int);

#else
extern void cdreg();
extern void cfsa();
extern void cfsax();
extern void cycle();
extern void cssa();
extern void cgo();
extern void rd2401();
extern void wr2401();
extern void cccz();
extern void cccc();
extern void ccci();
extern void ctstat();
extern void rstcam();
#endif

/* common functions etc */

#define b0 0
#define b1 1
#define c0 0
#define c1 1

#define n0 0
#define n1 1
#define n2 2
#define n3 3
#define n4 4
#define n5 5
#define n6 6
#define n7 7
#define n8 8
#define n9 9
#define n10 10
#define n11 11
#define n12 12
#define n13 13
#define n14 14
#define n15 15
#define n16 16
#define n17 17
#define n18 18
#define n19 19
#define n20 20
#define n21 21
#define n22 22
#define n23 23
#define n24 24

#define a0 0
#define a1 1
#define a2 2
#define a3 3
#define a4 4
#define a5 5
#define a6 6
#define a7 7
#define a8 8
#define a9 9
#define a10 10
#define a11 11
#define a12 12
#define a13 13
#define a14 14
#define a15 15

#define f0   0
#define f1   1
#define f2   2
#define f3   0
#define f4   0
#define f5   0
#define f6   0
#define f7   0
#define f8   0
#define f9   0
#define f10  0
#define f11  0
#define f12  0
#define f13  0
#define f14  0
#define f15  0
#define f16 16
#define f17 17
#define f25 25
#define f26 26
#define f27 27
#define f28 28
#define f29 29
#define f30 30
