/* structures and defines for standard esone camac subroutines */

#define B0 0
#define B1 1

#define C1 1
#define C2 2

#define N1 1
#define N2 2
#define N3 3
#define N4 4
#define N5 5
#define N6 6
#define N7 7
#define N8 8

#define F0 0
#define F1 1
#define F2 2
#define F16 16
#define F17 17


typedef struct {
   UINT8 b;
   UINT8 c;
   UINT8 n;
   UINT8 a;
} CAMAC_BCNA;

typedef struct {
    UINT16 count;
    UINT16 tally;
    UINT16 lam_ident;
    UINT16 channel;
} CAMAC_CB;


#ifdef __cplusplus
extern "C" {
#endif

#if defined(__STDC__) || defined(__cplusplus)

extern void cdreg( UINT32* ext, UINT32 b, UINT32 c, UINT32 n, UINT32 a );
extern void cfsa( UINT32 f, UINT32 ext, UINT32* data, UINT8* q );
extern void cfubc( UINT32 f, UINT32 ext, UINT32* data, CAMAC_CB cb );
extern void cfubr( UINT32 f, UINT32 ext, UINT32* data, CAMAC_CB cb );
extern void cssa( UINT32 f, UINT32 ext, UINT16* data, UINT8* q );
extern void csubc( UINT32 f, UINT32 ext, UINT16* data, CAMAC_CB cb );
extern void csubr( UINT32 f, UINT32 ext, UINT16* data, CAMAC_CB cb );

#else	/* __STDC__ */

extern void cdreg();
extern void cfsa();
extern void cfubc();
extern void cfubr();
extern void cssa();
extern void csubc();
extern void csubr();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

