/* structures and defines for standard esone camac subroutines */

#define B0 0
#define B1 1

#define C1 1
#define C2 2

#define N1 1
#define N2 2
#define N3 3
#define N4 4
#define N5 5
#define N6 6
#define N7 7
#define N8 8


#define F0 0
#define F1 1
#define F2 2
#define F16 16
#define F17 17


#ifdef __cplusplus
extern "C" {
#endif


#if defined(__STDC__) || defined(__cplusplus)

void cdreg(UINT32 *ext,UINT32 b,UINT32 c,UINT32 n,UINT32 a);
void cfsa(UINT32 f,UINT32 ext,UINT32 *camacData,UINT8 *q);
void cssa(UINT32 f,UINT32 ext,UINT16 *camacData,UINT8 *q);





#else	/* __STDC__ */

extern cdreg();

#endif	/* __STDC__ */



#ifdef __cplusplus
}
#endif

