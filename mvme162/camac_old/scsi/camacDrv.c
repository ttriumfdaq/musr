/*-----------------------------------------------------------------------------
** Copyright (c) 1993 TRIUMF Cylotron Facility
**
** TRIUMF Electronics Grp, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A5
** Email: waters@sundae.triumf.ca Tel: (604) 222-1047 Fax: (604) 222-1074
**-----------------------------------------------------------------------------
** 
** Description:
**
**	Driver for scsi camac.
** 
** 
** Author:	David Morris, TRIUMF Electronics Group
** File:        camacDrv.c
** Created:     December 1994.
** $Revision: 1.1.1.1 $
** last edit: $Date: 1995/10/22 02:30:26 $ $Author: ted $
**
** log file:  $Source: /usr/local/cvsroot/musr/mvme162/camac_old/scsi/camacDrv.c,v $
**-------------------------------------------------------------------------**
 *
 * Revision History:
 * 
 * 
 * 
 * 
 * 
 *
 *
*/

#include "vxWorks.h"
#include "scsiLib.h"
#include "ioLib.h"
#include "taskLib.h"
#include "memLib.h"
#include "stdio.h"

SCSI_PHYS_DEV * pScsiCamacDev[8];		/* storage for physical device info */

/* #define DEBUG */


/*-------------------------------------------------------------------------*/
/* Initialize the SCSI driver based on the camac crates used by the system */
/* This routine enables access to a SCSI device and must be invoked ones.  */
/*-------------------------------------------------------------------------*/

STATUS camacInit(UINT8 Crate)
{
  if (Crate > 7)
  {
    return (ERROR);
  } 
  pScsiCamacDev[Crate] = scsiPhysDevCreate(pSysScsiCtrl, Crate, 0, 6, SCSI_DEV_DIR_ACCESS, FALSE, 1, 16);
  if (pScsiCamacDev[Crate] == (SCSI_PHYS_DEV *) NULL)
  {
    printf("Failed to create SCSI device\n");
    return (ERROR);
  }
  return (OK);
}

/*------------------------------------*/
/* Delete a physical device structure */
/*------------------------------------*/

STATUS camacDelete(UINT8 Crate)
{
  if (scsiPhysDevDelete(pScsiCamacDev[Crate]) == ERROR)
  {
   printf("Failed to delete SCSI device\n");
  }
  pScsiCamacDev[Crate] = (SCSI_PHYS_DEV *) NULL;
  return (OK);
}

STATUS camacCycle(UINT8 C, UINT8 N, UINT8 A, UINT8 F, long * camacData, UINT8 * camacStatus)
{
  SCSI_COMMAND scsiCommandBlock;
  SCSI_TRANSACTION scsiXaction;
  UINT8 Temp[4];
  
  scsiCommandBlock[0] = 0xc0 | F;
  scsiCommandBlock[1] = 0x20 | N;
  scsiCommandBlock[2] = A;

/*
  If the camac cycle is a write, shuffle the 3 bytes into the proper order for
  the SCSI transfer
*/
  if (F > 15)
  {
    memcpy(Temp, camacData, sizeof(long));
    Temp[0] = Temp[1];
    Temp[1] = Temp[2];
    Temp[2] = Temp[3];
    Temp[3] = 0;
  }

  scsiXaction.cmdAddress    = scsiCommandBlock;
  scsiXaction.cmdLength     = 3;
  scsiXaction.dataAddress   = Temp;
  scsiXaction.dataDirection = F > 15 ? O_WRONLY : O_RDONLY;
  scsiXaction.dataLength    = 3;
  scsiXaction.addLengthByte = -1;
  scsiXaction.cmdTimeout    = 1000;

  taskLock();
  scsiIoctl(pScsiCamacDev[C], FIOSCSICOMMAND, (int) &scsiXaction);
  taskUnlock();

/*
  After a read, shuffle the 3 bytes into a long word for return
*/
  if (F < 16)
  {
    Temp[3] = Temp[2];
    Temp[2] = Temp[1];
    Temp[1] = Temp[0];
    Temp[0] = 0;
    memcpy(camacData, Temp, sizeof(long));
  }

  *camacStatus = scsiXaction.statusByte;

  return (OK);
}    

STATUS camacBlock(UINT8 C, UINT8 N, UINT8 A, UINT8 F, long * camacBlock, long sizeBlock, UINT8 * camacStatus)
{
/*
  This routine performs a fast block transfer from or to CAMAC over the SCSI bus
  The calling program passes CNAF a pointer to a data buffer the size of the buffer and
  a pointer to a status byte.

  *** NOTE *** 
    The data buffer must be declared as STATIC or else malloc'd. If this is not done
    the internal calloc of the temporary SCSI buffer may overwrite the data buffer

  *** NOTE ***
    camacInit must be run before this routine is called to define a handle to
    the SCSI device attached to the specified CAMAC crate
           ie  for CAMAC Crate 1 call camacInit(1) 
    This assigns a global variable for the whole system so it only is done once
*/
  SCSI_COMMAND scsiCommandBlock;
  SCSI_TRANSACTION scsiXaction;
  UINT8 * tempBlock;
  UINT8 * pTempBlock;
  long * pCamacBlock;
  long i;
  long tempCamac;

/*
  Set up command block with NAF and the size of the data transfer block. See Jorway Model 73
  users manual for further detail
*/
  scsiCommandBlock[0] = 0xc0 | F;
  scsiCommandBlock[1] = 0xE0 | N;
  scsiCommandBlock[2] = A;
  scsiCommandBlock[3] = (32768 - sizeBlock) >> 8;
  scsiCommandBlock[4] = (32768 - sizeBlock) & 0x00ff;

#ifdef DEBUG
  printf("In camacBlock transfer routine\n");
  for (i = 0; i < 5; i++)
  {
    printf("scsiCommandBlock[%d] = %X\n", i, scsiCommandBlock[i]);
  }
#endif

/*
  Alloc a block of memory to hold 3 byte SCSI data
*/
  tempBlock = (UINT8 *) calloc(sizeBlock * 3 + 10, sizeof(UINT8));
  if (tempBlock == (UINT8 *) NULL)
  {
    printf("Error calloc'ing in camacBlock\n");
  }

/*
  If a CAMAC write, then we must shift the long data to the left by one byte and
  copy it into the temp SCSI data buffer
*/
  if (F > 15 && F < 24)
  {
    pCamacBlock = camacBlock;
    pTempBlock = tempBlock;

    for (i = 0; i < sizeBlock; i++)
    {
      tempCamac = (*pCamacBlock) << 8;
      memcpy(pTempBlock, &tempCamac, sizeof(UINT8) * 3);
      pTempBlock += 3;
      pCamacBlock ++;
    }
  }

/*
  Set up the SCSI transaction block. This tells the SCSI driver how much data
  to pass and what command bytes to passs
*/
  scsiXaction.cmdAddress    = scsiCommandBlock;
  scsiXaction.cmdLength     = 5;
  scsiXaction.dataAddress   = tempBlock;
  scsiXaction.dataDirection = F > 15 ? O_WRONLY : O_RDONLY;
  scsiXaction.dataLength    = sizeBlock * 3;
  scsiXaction.addLengthByte = -1;
  scsiXaction.cmdTimeout    = 1000;

/*
  Do SCSI cycle(s). Note global variable pScsiCamacDev[C]. There are 8 possible
  SCSI devices allowed on the bus so in theory 8 camac crates. Note that the
  controller is one of the devices
*/
  scsiIoctl(pScsiCamacDev[C], FIOSCSICOMMAND, (int) &scsiXaction);

/*
  Translate the 3 byte SCSI temporary buffer into long words if a read
*/
  if (F < 8)
  {
    pCamacBlock = camacBlock;
    pTempBlock = tempBlock;
    tempCamac = 0;
    for (i = 0; i < sizeBlock; i++)
    {
      memcpy(((UINT8 *) pCamacBlock) + 1, pTempBlock, sizeof(UINT8) * 3);
      pTempBlock += 3;
      pCamacBlock ++;
    }
  }

  free(tempBlock);
  *camacStatus = scsiXaction.statusByte;
  return (OK);
}    

int
QTest(STATUS Status)
{
/*
  This routine simply tests the camac status passed for Q. It returns
  FALSE if everything is OK and TRUE if Q is missing
*/

  if (Status & 0x02)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

int
XTest(STATUS Status)
{
/*
  This routine simply tests the camac status passed for X. It returns
  FALSE if everything is OK and TRUE if X is missing
*/

  if (Status & 0x01)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}







