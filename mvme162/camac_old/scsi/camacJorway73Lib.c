/*-----------------------------------------------------------------------------
** Copyright (c) 1993 TRIUMF Cylotron Facility
**
** TRIUMF Electronics Grp, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A5
** Email: waters@sundae.triumf.ca Tel: (604) 222-1047 Fax: (604) 222-1074
**-----------------------------------------------------------------------------
** 
** Description:
**
**	Driver for Jorway Model 73 SCSI CAMAC Crate Controller.
**
**   An implementation of the CAMAC standard routines in C,
**   using C argument passing conventions.
**
**	Adapted from functions written by David Morris (TRIUMF)
** 
** Author:	Graham Waters, TRIUMF Electronics Group
** File:        camacLib_scsi.c
** Created:     December 1994.
** $Revision: 1.1.1.1 $
** last edit: $Date: 1995/10/23 03:38:21 $ $Author: ted $
**
** log file:  $Source: /usr/local/cvsroot/musr/mvme162/camac_old/scsi/camacJorway73Lib.c,v $
**-------------------------------------------------------------------------**
 *
 * Revision History:
 *   22-Oct-1995  TW  More ESONE routines (c
 * 
 * 
 * 
 * 
 *
 *
*/
/* Functions (all return void):
 *
 * cdreg( int *ext, int b, int c, int n, int a)   define external address 
 * cfsa( int f, int ext, int *dat, int *q)       long (24) single action 
 * cfubc( int f, int ext, 
 * cssa( int f, int ext, int *dat, int *q)       short (16) single action 
 */

#include "vxWorks.h"
#include "scsiLib.h"
#include "ioLib.h"
#include "taskLib.h"
#include "memLib.h"
#include "stdio.h"


SCSI_PHYS_DEV * pScsiCamacDev[8];		/* storage for physical device info */
SEM_ID semCamacAction;					/*mutual exclusion semaphore */

#define NO_DEBUG

typedef struct {
   UINT8 b;
   UINT8 c;
   UINT8 n;
   UINT8 a;
} BCNA;

typedef struct {
    UINT16 count;
    
} CB;

/* define function prototypes */

void cdreg(BCNA *ext,UINT32 b,UINT32 c,UINT32 n,UINT32 a);
void cfsa (UINT32 f,BCNA ext,UINT32 *camacData,UINT8 *q);		/* 204 usec */
void cssa (UINT32 f,BCNA ext,UINT16 *camacData,UINT8 *q);
STATUS cfmad(UINT32 F, BCNA ext,UINT32 * cdata, long sizeBlock, UINT8 * camacStatus);/* 268 + (blkcnt * 8) usec */


rstcam(UINT32 Crate)
{
	if ( scsiPhysDevIdGet(pSysScsiCtrl,Crate,0) == (SCSI_PHYS_DEV *) NULL )
	{
	  	  /*-----------------------------------------*/
	 	 /* create a SCSI physical device structure */
		/*-----------------------------------------*/

		pScsiCamacDev[Crate] = scsiPhysDevCreate(pSysScsiCtrl, Crate, 0, 6,
								SCSI_DEV_DIR_ACCESS, FALSE, 1, 16);
  		if (pScsiCamacDev[Crate] == (SCSI_PHYS_DEV *) NULL)
  		{
    		printf("Failed to create SCSI device\n");
			return;
    	}
	}

}


/*-------------------------------------------------------------------------*/
/* CDREG: Pack camac B,C,N,A into long integer                             */
/*-------------------------------------------------------------------------*/
/*
synopsis
	void cdreg (ext,b,c,n,a)
	uint_32 * ext ----- OUTPUT: CAMAC packed address
	uint_32 b --------- INPUT:  CAMAC branch 0-7
	uint_32 c --------- INPUT:  CAMAC crate 0-7
	uint_32 n --------- INPUT:  CAMAC slot
	uint_32 a --------- INPUT:  CAMAC sub-address

Description
	Packs camac b,c,n,a init a long integer

*/

void
cdreg (ext,b,Crate,n,a)
BCNA *ext;
UINT32 b,Crate,n,a;
{
	if ( scsiPhysDevIdGet(pSysScsiCtrl,Crate,0) == (SCSI_PHYS_DEV *) NULL )
	{
		semCamacAction = semBCreate (SEM_Q_FIFO, SEM_FULL);

	  	  /*-----------------------------------------*/
	 	 /* create a SCSI physical device structure */
		/*-----------------------------------------*/

		pScsiCamacDev[Crate] = scsiPhysDevCreate(pSysScsiCtrl, Crate, 0, 6,
								SCSI_DEV_DIR_ACCESS, FALSE, 1, 16);
  		if (pScsiCamacDev[Crate] == (SCSI_PHYS_DEV *) NULL)
  		{
    		printf("Failed to create SCSI device\n");
			return;
    	}
	}


	ext->b = b;
	ext->c = Crate;
	ext->n = n;
	ext->a = a;

}

/*-------------------------------------------------------------------------*/
/* CFSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfsa (f,ext,data,q)
	UINT32 ext ------- INPUT: CAMAC packed address
	UINT32 f --------- INPUT: CAMAC function
	uINT32 *data ----- INPUT/OUTPUT: write or read to from CAMAC
	UINT8 *q --------- OUTPUT -- true if q was present

Description
	Execute a single long camac read/write

--------------------------------------------------------------------------*/


void cfsa(UINT32 f,BCNA ext,UINT32 *camacData,UINT8 *q)
{

	SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
	SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
	UINT8 Temp[4], camacStatus;
#ifdef DEBUG
	printf ("begin CFSA: f(%d), b(%d) c(%d) n(%d) a(%d) = %x\n",f,ext.b,ext.c,ext.n,ext.a,*camacData);
#endif


	semTake(semCamacAction, WAIT_FOREVER);	/* IS it my turn ? */

	  /*------------------------------------------*/
	 /* fill in fields of SCSI_COMMAND structure */	
	/*------------------------------------------*/

	scsiCommandBlock[0] = 0xc0 | f;
	scsiCommandBlock[1] = 0x20 | ext.n;	/* include 3 byte transfer flag */
	scsiCommandBlock[2] = ext.a;

	
	   /*------------------------------------------------------------------*/
	  /* If the camac cycle is a write,									  */
	 /* shuffle the 3 bytes into the proper order for the SCSI transfer	 */
	/*------------------------------------------------------------------*/

	if (f > 15)
	{
    	memcpy(Temp, (UINT8*)camacData, sizeof(UINT32));
    	Temp[0] = Temp[1];
		Temp[1] = Temp[2];
		Temp[2] = Temp[3];
		Temp[3] = 0;
	}

	  /*----------------------------------------------*/
	 /* fill in fields of SCSI_TRANSACTION structure */
	/*----------------------------------------------*/

	scsiXaction.cmdAddress    = scsiCommandBlock;
	scsiXaction.cmdLength     = 3;
	scsiXaction.dataAddress   = Temp;
	scsiXaction.dataDirection = f > 15 ? O_WRONLY : O_RDONLY;
	scsiXaction.dataLength    = 3;
	scsiXaction.addLengthByte = -1;
	scsiXaction.cmdTimeout    = 1000;

	taskLock();
#ifdef DEBUG
	printf ("scsiIoctl: Crate = %d\n",ext.c);
#endif
	scsiIoctl(pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int) &scsiXaction);
	taskUnlock();


	/* After a read, shuffle the 3 bytes into a long word for return */

	if (f < 16)
	{
		Temp[3] = Temp[2];
    	Temp[2] = Temp[1];
    	Temp[1] = Temp[0];
    	Temp[0] = 0;
    	memcpy(camacData, Temp, sizeof(long));
  }

	camacStatus = scsiXaction.statusByte;
	*q = ~(camacStatus >> 1) & 0x01;
#ifdef DEBUG	
	printf ("done CFSA: f(%d), b(%d) c(%d) n(%d) a(%d) = %x\n",f,ext.b,ext.c,ext.n,ext.a,*camacData);
#endif	


	semGive(semCamacAction);
}
/*-------------------------------------------------------------------------*/
/* CSSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfsa (f,ext,data,q)
	UINT32 ext ------- INPUT: CAMAC packed address
	UINT32 f --------- INPUT: CAMAC function
	uINT32 *data ----- INPUT/OUTPUT: write or read to from CAMAC
	UINT8 *q --------- OUTPUT -- true if q was present

Description
	Execute a single long camac read/write

--------------------------------------------------------------------------*/


void cssa(UINT32 f,BCNA ext,UINT16 *camacData,UINT8 *q)
{

	SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
	SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
	UINT8 Temp[4], camacStatus;

	semTake(semCamacAction, WAIT_FOREVER);	/* IS it my turn ? */
	
	  /*------------------------------------------*/
	 /* fill in fields of SCSI_COMMAND structure */	
	/*------------------------------------------*/

	scsiCommandBlock[0] = 0xc0 | f;
	scsiCommandBlock[1] = ext.n;
	scsiCommandBlock[2] = ext.a;

	
	   /*------------------------------------------------------------------*/
	  /* If the camac cycle is a write,									  */
	 /* shuffle the 3 bytes into the proper order for the SCSI transfer	 */
	/*------------------------------------------------------------------*/

	if (f > 15)
	{
    	memcpy(Temp, (UINT8*)camacData, sizeof(UINT32));
    	Temp[0] = Temp[1];
		Temp[1] = Temp[2];
		Temp[2] = Temp[3];
		Temp[3] = 0;
	}

	  /*----------------------------------------------*/
	 /* fill in fields of SCSI_TRANSACTION structure */
	/*----------------------------------------------*/

	scsiXaction.cmdAddress    = scsiCommandBlock;
	scsiXaction.cmdLength     = 3;
	scsiXaction.dataAddress   = Temp;
	scsiXaction.dataDirection = f > 15 ? O_WRONLY : O_RDONLY;
	scsiXaction.dataLength    = 3;
	scsiXaction.addLengthByte = -1;
	scsiXaction.cmdTimeout    = 1000;

	taskLock();
	scsiIoctl(pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int) &scsiXaction);
	taskUnlock();


	/* After a read, shuffle the 3 bytes into a long word for return */

	if (f < 16)
	{
		Temp[3] = Temp[2];
    	Temp[2] = Temp[1];
    	Temp[1] = Temp[0];
    	Temp[0] = 0;
    	memcpy(camacData, Temp, sizeof(long));
  }

	camacStatus = scsiXaction.statusByte;
	*q = ~(camacStatus >> 1) & 0x01;

	semGive(semCamacAction);
}

/*-------------------------------------------------------------------------*/
/* CFMAD: Execute function f in address scan modes (inc sub-address        */   		  			  /*-------------------------------------------------------------------------*/

/*
synopsis
	void cfmad (int f,int ext,int intc[],int cb[])
	uint_32 f ----------- INPUT: CAMAC function
	uint_32 ext[2] ------ INPUT: [1]first address, [2]last address
	uint_32 intc -------- INPUT/OUTPUT: camac data array
	uint_32 cb ---------- INPUT: cb[0] = repeat count
				                 cb[1] = tally
				                 cb[2] = LAM identification
				                 cb[3] = channel identification

Description
		Execute a block transfer.
		stop mode: terminate on q=0 or on repeat count

--------------------------------------------------------------------------*/


int  cfmad_defunct(UINT32 f,BCNA extb[],UINT32 intc[],UINT32 cb[])
{
	SCSI_COMMAND scsiCommandBlock;	/* SCSI command byte array    */
	SCSI_TRANSACTION scsiXaction;	/* info on a SCSI transaction */
  	UINT8 * tempBlock;				/* pointer to block transfer buffer */
  	UINT8 * pTempBlock;				/*   "      "   "      "        "   */

  	UINT32 * pCamacBlock;
  	UINT32 i;
	UINT32 tempCamac;				/* temporary storage for re-ordered data */
	UINT8 camacStatus;

	int status;
	
	semTake(semCamacAction, WAIT_FOREVER);	/* IS it my turn ? */

	  /*------------------------------------------*/
	 /* fill in fields of SCSI_COMMAND structure */	
	/*------------------------------------------*/

  	scsiCommandBlock[0] = 0xc0 | f;
  	scsiCommandBlock[1] = 0xE0 | extb[0].n;	/* include Address Scan mode bits */
  	scsiCommandBlock[2] = extb[0].a;
  	scsiCommandBlock[3] = (32768 - cb[0]) >> 8;
  	scsiCommandBlock[4] = (32768 - cb[0]) & 0x00ff;

	  /*---------------------------------------------*/
	 /* allocate sufficient memory for the transfer */
	/*---------------------------------------------*/

	tempBlock = (UINT8 *) calloc(cb[0] * 3 + 10, sizeof(UINT8));
  	if (tempBlock == (UINT8 *) NULL)
  	{
    	printf("Error calloc'ing in camacBlock\n");
  	}

	   /*------------------------------------------------------------------*/
	  /* If the camac cycle is a write,									  */
	 /* shuffle the 3 bytes into the proper order for the SCSI transfer	 */
	/*------------------------------------------------------------------*/

  if (f > 15 && f < 24)
  {
    pCamacBlock = intc;				/* pointer to data source */
    pTempBlock = tempBlock;		/* pointer to temporary allocated buffer */
    for (i = 0; i < cb[0]; i++)
    {
      tempCamac = (*pCamacBlock) << 8;
      memcpy(pTempBlock, &tempCamac, sizeof(UINT8) * 3);
      pTempBlock += 3;
      pCamacBlock ++;
    }
  }
	  /*----------------------------------------------*/
	 /* fill in fields of SCSI_TRANSACTION structure */
	/*----------------------------------------------*/

	scsiXaction.cmdAddress    = scsiCommandBlock;
	scsiXaction.cmdLength     = 5;
	scsiXaction.dataAddress   = tempBlock;
	scsiXaction.dataDirection = f > 15 ? O_WRONLY : O_RDONLY;
	scsiXaction.dataLength    = cb[0] * 3;
	scsiXaction.addLengthByte = -1;
	scsiXaction.cmdTimeout    = 1000;

	  /*-----------------------------------------------------*/
	 /* disable all other tasks and do the scsi transaction */
	/*-----------------------------------------------------*/

  	/* taskLock(); */
  	status = scsiIoctl(pScsiCamacDev[extb[0].c], FIOSCSICOMMAND, (int) &scsiXaction);
	/* printf ("cfmad: scsi status = %d\n",status); */
  	/* taskUnlock(); */

	 /*---------------------------------------------------------------*/
	 /* After a read, shuffle the 3 bytes into a long word for return */
	/*---------------------------------------------------------------*/

  if (f < 8)
  {
    pCamacBlock = intc;					/* pointer to data destination */
    pTempBlock = tempBlock;	/* pointer to temporary allocated buffer */
    tempCamac = 0;
    for (i = 0; i < cb[0]; i++)
    {
      memcpy(((UINT8 *) pCamacBlock) + 1, pTempBlock, sizeof(UINT8) * 3);
	  *pCamacBlock &= 0x00ffffff;
      pTempBlock += 3;
      pCamacBlock ++;
    }
  }

  free(tempBlock);				/* de-allocated tempory storage */
  semGive(semCamacAction);
	return(status);
 
}



/*-------------------------------------------------------------------------*/
/* CFUBC: Execute Controller-Synchronized Block Transfer     		  			   */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfubc (int f,int ext,int intc[],int cb[])
	uint_32 ext ------ INPUT: CAMAC address
	uint_32 f -------- INPUT: CAMAC function
	uint_32 intc ----- INPUT/OUTPUT: camac data array
	uint_32 cb ------- INPUT: cb[0] = repeat count
				  cb[1] = tally
				  cb[2] = LAM identification
				  cb[3] = channel identification

Description
		Execute a block transfer.
		stop mode: terminate on q=0 or on repeat count
		stop on word: not implemented

--------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
/* CFUBR: Execute Repeat Mode Block Transfer     		  			       */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfmad (int f,int ext,int intc[],int cb[])
	uint_32 ext ------ INPUT: CAMAC address
	uint_32 f -------- INPUT: CAMAC function
	uint_32 intc ----- INPUT/OUTPUT: camac data array
	uint_32 cb ------- INPUT: cb[0] = repeat count
				  cb[1] = tally
				  cb[2] = LAM identification
				  cb[3] = channel identification

Description
	Emulate a block transfer.
		terminate on repeat count
		stop on word: not implemented

--------------------------------------------------------------------------*/



STATUS cfmad(UINT32 F, BCNA ext,UINT32 * camacBlock, long sizeBlock, UINT8 * camacStatus)
{
/*
  This routine performs a fast block transfer from or to CAMAC over the SCSI bus
  The calling program passes CNAF a pointer to a data buffer the size of the buffer and
  a pointer to a status byte.

  *** NOTE *** 
    The data buffer must be declared as STATIC or else malloc'd. If this is not done
    the internal calloc of the temporary SCSI buffer may overwrite the data buffer

  *** NOTE ***
    camacInit must be run before this routine is called to define a handle to
    the SCSI device attached to the specified CAMAC crate
           ie  for CAMAC Crate 1 call camacInit(1) 
    This assigns a global variable for the whole system so it only is done once
*/
  SCSI_COMMAND scsiCommandBlock;
  SCSI_TRANSACTION scsiXaction;
  UINT8 * tempBlock;
  UINT8 * pTempBlock;
  UINT32 * pCamacBlock;
  long i;
  long tempCamac;

/*
  Set up command block with NAF and the size of the data transfer block. See Jorway Model 73
  users manual for further detail
*/
  scsiCommandBlock[0] = 0xc0 | F;
  scsiCommandBlock[1] = 0x60 | ext.n;
  scsiCommandBlock[2] = ext.a;
  scsiCommandBlock[3] = (32768 - sizeBlock) >> 8;
  scsiCommandBlock[4] = (32768 - sizeBlock) & 0x00ff;

#ifdef DEBUG
  printf("In camacBlock transfer routine\n");
  for (i = 0; i < 5; i++)
  {
    printf("scsiCommandBlock[%d] = %X\n", i, scsiCommandBlock[i]);
  }
#endif

/*
  Alloc a block of memory to hold 3 byte SCSI data
*/
  tempBlock = (UINT8 *) calloc(sizeBlock * 3 + 10, sizeof(UINT8));
  if (tempBlock == (UINT8 *) NULL)
  {
    printf("Error calloc'ing in camacBlock\n");
  }

/*
  If a CAMAC write, then we must shift the long data to the left by one byte and
  copy it into the temp SCSI data buffer
*/
  if (F > 15 && F < 24)
  {
    pCamacBlock = camacBlock;
    pTempBlock = tempBlock;

    for (i = 0; i < sizeBlock; i++)
    {
      tempCamac = (*pCamacBlock) << 8;
      memcpy(pTempBlock, &tempCamac, sizeof(UINT8) * 3);
      pTempBlock += 3;
      pCamacBlock ++;
    }
  }

/*
  Set up the SCSI transaction block. This tells the SCSI driver how much data
  to pass and what command bytes to passs
*/
  scsiXaction.cmdAddress    = scsiCommandBlock;
  scsiXaction.cmdLength     = 5;
  scsiXaction.dataAddress   = tempBlock;
  scsiXaction.dataDirection = F > 15 ? O_WRONLY : O_RDONLY;
  scsiXaction.dataLength    = sizeBlock * 3;
  scsiXaction.addLengthByte = -1;
  scsiXaction.cmdTimeout    = 1000;

/*
  Do SCSI cycle(s). Note global variable pScsiCamacDev[C]. There are 8 possible
  SCSI devices allowed on the bus so in theory 8 camac crates. Note that the
  controller is one of the devices
*/
  scsiIoctl(pScsiCamacDev[ext.c], FIOSCSICOMMAND, (int) &scsiXaction);

/*
  Translate the 3 byte SCSI temporary buffer into long words if a read
*/
  if (F < 8)
  {
    pCamacBlock = camacBlock;
    pTempBlock = tempBlock;
    tempCamac = 0;
    for (i = 0; i < sizeBlock; i++)
    {
      memcpy(((UINT8 *) pCamacBlock) + 1, pTempBlock, sizeof(UINT8) * 3);
      pTempBlock += 3;
      pCamacBlock ++;
    }
  }

  free(tempBlock);
  *camacStatus = scsiXaction.statusByte;
  return (OK);
}    



