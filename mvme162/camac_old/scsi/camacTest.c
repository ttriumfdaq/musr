/*-------------------------------------------------------------------------**
** file: bounce.c                                                          **
**-------------------------------------------------------------------------**
** project: VME real time tasks                                            **
** module(s):  tics_server                                                 **
** version:    1.0                                                         **
**                                                                         **
** 
** Author:	Graham Waters, TRIUMF Electronics Group                    **
** created:     28-Jan-93                                                  ** 
**-------------------------------------------------------------------------**
**
** Description:
** 
**	CAMAC dataway display bouncing ball 
**	 
**
*--------------------------------------------------------------------------*/

#include "vxWorks.h"
#include "esone_camac.h"

#define SWR_SLOT 5
#define DSP_SLOT 8


void bounce();
void real_bounce();
int  wr0694();


void cdemo()
{

	int err;
	UINT8 q;

	UINT32 n_dsp, n_reg;
	
	UINT32 swr_bcna, dsp_bcna;
	UINT32 cdata, i;
	

	cdreg(&swr_bcna,     0,1,SWR_SLOT,0);
	cdreg(&dsp_bcna,     0,1,DSP_SLOT,0);

	cfsa (0,swr_bcna,&cdata,&q);		 /* read switch reg */
	printf ("\nCDEMO: SWITCH REG =  %x q = %d\n",cdata,q);

	cfsa (16,dsp_bcna,&cdata,&q);		 /* echo to display */
	printf ("camac = %x\n",cdata);

}

void bounce()
{

	long i, counter=0;
   for(; ;)
   {

      real_bounce(DSP_SLOT);            /* cycle through bounce under gravity */

      /* big delay before starting another bounce cycle */
      
      {      for (counter = 1; counter <= 100000; ++counter)

           i = i;
      }


   }
}

/*------------------------------------------------------------------------*/
/* bounce cycle on display module                                         */
/*------------------------------------------------------------------------*/

simple_bounce()
{
  	int n, f, wait, port, value, top;
   	UINT32 cdata;
	UINT32 ext;
	UINT8 q;
	int n_slot;

	n_slot = 8;

	cdreg(&ext,0,1,DSP_SLOT,0);		/* define B0,C0,N12,a0  */
	top = 24;                  /* ball goes right to the top first time */
	f = 16;
	wait = 1;

	/* going up */

	for (n = 0; n <= top; ++n)
	{

		cdata = 1 << n;
		cfsa (f,ext,&cdata,&q);
       		delay(wait*1000);
      	}
      
      	/* falling */
   
      	for (n = top; n >= 0; --n)
      	{
		cdata = 1 << n;
		cfsa (f,ext,&cdata,&q);
         	delay(wait*1000);
   	}
}




/*------------------------------------------------------------------------*/
/* bounce cycle on display module                                         */
/*------------------------------------------------------------------------*/
void
real_bounce(n_slot)
int n_slot;
{
  	int n, f, wait, port, value, top;
   	UINT32 cdata;
	UINT32 ext;
	UINT8 q;

	cdreg(&ext,0,1,n_slot,0);		/* define B0,C0,N12,a0  */
	top = 24;                  /* ball goes right to the top first time */
	f = 16;
	wait = top;
	do 
	{   
		/* rising */
   
		wait = 24/top;
      
		for (n = 0; n <= top; ++n)
		{

			cdata = 1 << n;
			cfsa (f,ext,&cdata,&q);
       			delay(wait*2000);
         		wait = wait+1;
      		}
      
      		/* falling */
   
      		for (n = top; n >= 0; --n)
      		{
			cdata = 1 << n;
			cfsa (f,ext,&cdata,&q);
         		delay(wait*2000);
         		wait = wait-1;
      		}
      		top = top -1;              /* don't bounce so high next time */
   	}
   	while (top != 0);
}


delay(count)
int count;
{
	int i;
	for (i = 0; i <= count; ++i)
		i = i;
}


void time_cfsa()
{
	unsigned long t1,t2,diff;
	unsigned long count = 10000;
	double period;
  	int f, wait, port, value, top;
   	UINT32 cdata ,n;
	UINT32 ext;
	UINT8 q;
	int n_slot;

	n_slot = 8;
	cdata = 0x8f0f0f;

	printf ("\nTiming CAMAC cycle CFSA:");

	cdreg(&ext,0,1,DSP_SLOT,0);		/* define B0,C0,N12,a0  */
	f = 16;

	/* 1 tic = 16ms */

	cdata = n;
    t1 = tickGet();
	for (n=1;n<=count;++n)
	{
		cfsa (f,ext,&cdata,&q);
	}
    t2 = tickGet();
    diff = t2 - t1;
	period =  diff * 16.0 / count;
    printf ("%f ms\n",period);
}

void time_cssa()
{
	unsigned long t1,t2,diff;
	unsigned long count = 10000;
	double period;
  	int n, f, wait, port, value, top;
   	UINT16 cdata;
	UINT32 ext;
	UINT8 q;
	int n_slot;

	n_slot = 8;
	cdata = 0x8f0f;

	printf ("\nTiming CAMAC cycle CFSA:");

	cdreg(&ext,0,1,n_slot,0);		/* define B0,C0,N12,a0  */
	f = 16;

	/* 1 tic = 16ms */

	cdata = n;
    t1 = tickGet();
	for (n=1;n<=count;++n)
	{
		cssa (f,ext,&cdata,&q);
	}
    t2 = tickGet();
    diff = t2 - t1;
	period =  diff * 16.0 / count;
    printf ("%f ms\n",period);
}

void time_cfmad(int blkCount)
{
	unsigned long t1,t2,diff;
	unsigned long count = 10000;
	int i, status;
	double transferTime, period;
 	UINT32 a, f, extb, cdata[128], cb;
	UINT8 cstat;

	printf ("\nTiming CAMAC cycle CFMAD:");
	f = 0;
	a = 0;
	cdreg(&extb,0,1,5,a);		/* starting address */
	cb = blkCount;					/* block count */

	/* 1 tic = 16ms */

    t1 = tickGet();
	for (i=1;i<=count;++i)
	{
		status = cfmad(f,extb,cdata,cb,&cstat);		/* block read */
	}
    t2 = tickGet();
    diff = t2 - t1;
	transferTime =  diff * 16.0 / count;
	period =  transferTime / blkCount;
    printf ("\ndone: %f ms for %d words = %f ms per word, status = %d\n",
										transferTime,blkCount,period,status);

}

/*------------------------------------------------------------------*/
/*																	*/
/* WR0694:	write to camac memory module type 0694					*/
/* input:															*/
/* 			n: -------- camac slot of 0694	     					*/
/*			word: -----	0694 word number							*/
/*			cdata: ---- value to be written							*/
/*------------------------------------------------------------------*/


int  wr0694(int n,int word,UINT32 cdata)
{

	UINT8 q;

	UINT32 a,f;

	UINT32 ext;


	f = ( (word >> 4) & 0x0f) + 16;
	a = word & 0x0f;

	cdreg(&ext,0,1,n,a);		/* define camac address  */
	cfsa (f,ext,&cdata,&q);		/* write to 0694 */
	return(q);
}
/*------------------------------------------------------------------*/
/*																	*/
/* RD0694:	Read camac memory module type 0694	       				*/
/* input:															*/
/* 			n: -------- camac slot of 0694	     					*/
/*			word: -----	0694 word number							*/
/*			cdata: ---- value to be written							*/
/*------------------------------------------------------------------*/


int  rd0694(int n,int word,UINT32 *cdata)
{

	UINT8 q;

	UINT32 a,f;

	UINT32 ext;


	f = ( (word >> 4) & 0x0f) + 0;
	a = word & 0x0f;

	cdreg(&ext,0,1,n,a);		/* define camac address  */
	cfsa (f,ext,cdata,&q);		/* read 0694 */
	return(q);
}

int blkrd0694()
{
	UINT32 a, f, extb, cdata[128], cb;
	UINT8 q,cstat;
	int i;

	f = 0;
	a = 0;
	cdreg(&extb,0,1,15,a);		/* starting address */
	cb = 8;					/* block count */
	cfmad(f,extb,cdata,cb,&cstat);		/* block read */
	for (i = 0; i < cb; ++i)
		printf ("%x ",cdata[i]);
	printf ("\n");


}

/* ------------- 0694/AIDC Reflective Memory Tasks ------------- */


/*----------------------------------------------------*/
/*                                                    */
/* TESTRCV:	write to the aidc via the camac 0694/sdlc */
/*                                                    */
/*----------------------------------------------------*/

void testrcv(int delay)
{
	static UINT32 cdata1, cdata2[128];
	int i, status,limit;
	static int n;

	cdata1 = 0;
	n = 16;
	limit = 63;
	for (i = 0; i<= 127; ++i)
		cdata2[i] = i;

	wr0694(n,0,0x0694);
	for(;;)
	{
		taskDelay(delay);
		for (i =0; i <= limit; ++i)
		{
			wr0694(n,i,cdata1+i);
		}
		cdata1++;
	}
}

/*-------------------------------*/
/* srComp: send/receiver compare */
/*-------------------------------*/

void srComp(int delay)
{
	UINT32 a, f, extb, cdata[128], cb;
	static UINT32 cdata1, cdata2[128];
	int i, status,limit;
	static int n;
	UINT8 cstat;

	printf("srComp: test 0694/AIDC sdlc link\n");

	cdata1 = 0;
	n = 15;
	f = 0;
	a = 0;
	cdreg(&extb,0,1,n,a);				/* starting address */


	limit = 63;
	for (i = 0; i<= limit; ++i)			/* load output buffers */
	cdata2[i] = i;

	for(;;)
	{
		/* taskDelay(delay); */

		for (i = 0; i<= limit; ++i)			/* incremend output buffer data */
		cdata2[i] = cdata2[i] + 1;


		 /*----------------------------------------------*/
		 /* do a block write to 1st half of 0694 memory */
		/*---------------------------------------------*/

		for (f = 0; f <= 3; ++f)
		{
			cb = 16;								/* block count */
			cfmad((f+16),extb,&cdata2[f*16],cb,&cstat);	/* block write */
		}
		taskDelay(10);				/* wait for vme to echo received data */

		  /*-------------------------------------*/
		 /* do a block read of all 0694 memory */
		/*------------------------------------*/

		for (f = 0; f <= 7; ++f)
		{
			cb = 16;							/* block count */
			cfmad(f,extb,&cdata2[f*16],cb,&cstat);		/* block read */
		}

		 /*------------------------------------------*/
		 /* compare 1st and 2nd half of camac memory */
		/*------------------------------------------*/

		for (i =0; i <= limit; ++i)
		{
			if (cdata2[i] != cdata2[i+limit+1])
			{
				printf ("ERROR: loc %d, expected %x, received %x\n",
						i+limit+1,cdata2[i],cdata2[i+limit+1]);
			}

		}
		
	}


}

show0694(int word)
{
	UINT32 cdata;
	rd0694(15,word,&cdata);
	printf("0694: loc %d = %x\n",word,cdata);
}

ctest()
{
	UINT8 c,n,a,f, status, camacStatus;
	static UINT32 cData[100], sizeBlock;
	UINT32 ext;

	c = 1;
	n = 5;
	a = 0;
	f = 0;
	sizeBlock = 4;

	cdreg(&ext,0,1,5,0);		/* define camac address  */

	status =  cfmad(f, ext, cData, sizeBlock, &camacStatus);

	printf ("%x, %x,%x, %x\n",cData[0],cData[1],cData[2],cData[3]);

}

