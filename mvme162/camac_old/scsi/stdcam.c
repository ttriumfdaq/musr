/*----------------------------------------------------------------------**
*  file:        stdcam.c                                                **
*-----------------------------------------------------------------------**
*  project:     BL1 Meson production targets T1T2                       **
*  module(s):   main                                                    **
*                                                                       **
*  version:     1.0                                                     **
*  $Revision: 1.1.1.1 $
*  created:      28-Jan-92 G.Waters                                    **
*  last edit: $Date: 1995/10/22 02:30:26 $ $Author: ted $
*-----------------------------------------------------------------------**
* 
*  description: 
*  Camac Functions for STD/SCSI/CAMAC interface                  

** history:
** $Log: stdcam.c,v $
** Revision 1.1.1.1  1995/10/22 02:30:26  ted
** import from decu18
**
 * 
 *    Rev 1.1   02 Oct 1992 16:33:12   G.Waters
 * read/write routines for 0694 camac memory
 * 
 *    Rev 1.0   06 Apr 1992 16:17:32   G.Waters
 * Initial revision.
 * 
 *
 *
 *
*/


#define NARGS 1

#define SCSI_BASE  0xFE80
#define SCSI_CDR  SCSI_BASE+0x0      /*  R  Current SCSI Data */
#define SCSI_ODR  SCSI_BASE+0x0      /*  W  Output Data */
#define SCSI_ICR  SCSI_BASE+0x1      /* R/W Initiator Command */
#define SCSI_MR   SCSI_BASE+0x2      /* R/W Mode */
#define SCSI_TCR  SCSI_BASE+0x3      /* R/W Target Command */
#define SCSI_CBS  SCSI_BASE+0x4      /*  R  Current SCSI Bus Status */
#define SCSI_SER  SCSI_BASE+0x4      /*  W  Select Enable */
#define SCSI_BSR  SCSI_BASE+0x5      /*  R  Bus and Status */
#define SCSI_DSR  SCSI_BASE+0x5      /*  W  Start DMA Send */
#define SCSI_IDR  SCSI_BASE+0x6      /*  R  Input Data */
#define SCSI_DTRR SCSI_BASE+0x6      /*  W  Start DMA Target Receive */
#define SCSI_RPIR SCSI_BASE+0x7      /*  R  Reset Parity/Interrupt */
#define SCSI_DIRR SCSI_BASE+0x7      /*  W  Start DMA Initiator Receive */
#define SCSI_9    SCSI_BASE+0x8      /*     vector generator */
#define SCSI_SW   SCSI_BASE+0x9      /*  R  dip switch port */


/*
#define ENABLE_INT  _enable();
#define DISABLE_INT _disable();
*/
#define ENABLE_INT  
#define DISABLE_INT 



#include "c:\lang\mc\include\camac.h"
#include "c:\lang\mc\include\types.h"
#include "v53.h"

void outmem();
void scsi_select();
void  scsi_send();
uchar scsi_receive();
void display_scsi();

union camac
{
   char c[4];
   uint_32 l;
   uint_16 i;
};

struct bcna
{
   uchar b;
   uchar c;
   uchar n;
   uchar a;
};


int_16 c_status;
uint_16 ocw1;

/*-------------------------------------------------------------------------*/
/* CDREG: Pack camac B,C,N,A into long integer EXT                         */
/*-------------------------------------------------------------------------*/


void cdreg(ext,b,c,n,a)
struct bcna *ext;
int b,c,n,a;
{
   ext->b = b;
   ext->c = c;
   ext->n = n;
   ext->a = a;
}

/*-------------------------------------------------------------------------*/
/* CFSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/


void cfsa(f,ext,value,q)
int f,*q;
long int *value;
struct bcna ext;
{
   union camac data;
   uchar phase, scsi_data;

   if (ext.n >= 31) return;         /* dummy call for debugging */

   scsi_select(ext.c+1);                 /* select crate */

   phase = 0x02;                    /* command phase: send f,n,a */
   outp(SCSI_TCR,phase);
   scsi_send(f+0xc0);
   scsi_send(ext.n | 0x20);         /* include 3 byte transfer flag */
   scsi_send(ext.a);
   
   switch (f)
   {
      case 16:
      case 17: phase = 0x00;                    /* data out phase */
               outp(SCSI_TCR,phase);
               data.l = *value;
               scsi_send(data.c[2]);            /* write data */
               scsi_send(data.c[1]);
               scsi_send(data.c[0]);
               break;
      case 0:
      case 1:
      case 2:  phase = 0x01;                    /* data in phase */
               outp(SCSI_TCR,phase);
               data.c[2] = scsi_receive();      /* read data */
               data.c[1] = scsi_receive();
               data.c[0] = scsi_receive();
               data.c[3] = 0;
               *value = data.l;
               break;

   }
   phase = 0x03;
   outp(SCSI_TCR,phase);            /* status phase */
   c_status = scsi_receive();      /* read x,q */
   if ( (c_status & 0x02) == 0)
      *q = 1;
   else *q = 0;
   phase = 0x07;
   outp(SCSI_TCR,data);             /* message phase */
   scsi_data = scsi_receive();
}





/*-------------------------------------------------------------------------*/
/* CSSA: Execute camac function f, address EXT, with data (int)VALUE      */
/*-------------------------------------------------------------------------*/


void cssa(f,ext,value,q)
int f,*q;
uint_16 *value;
struct bcna ext;
{
   union camac data;
   uchar phase, scsi_data;

   if (ext.n >= 31) return;         /* dummy call for debugging */

   scsi_select(ext.c+1);                 /* select crate */

   phase = 0x02;                    /* command phase: send f,n,a */
   outp(SCSI_TCR,phase);
   scsi_send(f+0xc0);
   scsi_send(ext.n);
   scsi_send(ext.a);

   switch (f)
   {
      case 16:
      case 17: phase = 0x00;                    /* data out phase */
               outp(SCSI_TCR,phase);
               data.i = *value;
               scsi_send(data.c[1]);            /* write data */
               scsi_send(data.c[0]);
               break;
      case 0:
      case 1:
      case 2:  phase = 0x01;                    /* data in phase */
               outp(SCSI_TCR,phase);
               data.c[1] = scsi_receive();      /* read data */
               data.c[0] = scsi_receive();
               *value = data.i;
               break;

   }
   phase = 0x03;
   outp(SCSI_TCR,phase);            /* status phase */
   c_status = scsi_receive();      /* read x,q */
   if ( (c_status & 0x02) == 0)
      *q = 1;
   else *q = 0;
   phase = 0x07;
   outp(SCSI_TCR,data);             /* message phase */
   scsi_data = scsi_receive();
}



/*-------------------------------------------------------------------------*/
/* CFUBC: Control-Synchronized Block Transfer (24 bit words)               */
/*                                                                         */
/* read/write camac in q-stop mode (same address)                          */
/*                                                                         */
/*                                                                         */
/* f    = function code                                                    */
/* ext  = external address                                                 */
/* intc = camac data array                                                 */
/* cb   = repeat count                                                     */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/


void cfubc(f,ext,intc,cb,iword)
int iword;
int f;
struct bcna ext;
uint_32 *intc;
uint_16 cb;
{
   union camac data;
   uchar phase, scsi_data;
   int i;

   if (ext.n >= 31) return;         /* dummy call for debugging */
   
   scsi_select(ext.c+1);            /* select crate */

   phase = 0x02;                    /* command phase: send f,n,a */
   outp(SCSI_TCR,phase);
   scsi_send(f+0xc0);
   scsi_send(ext.n | 0xa0);         /* 24 bit word, q-stop mode */
   scsi_send(ext.a);
   data.i = 32768 - cb;
   scsi_send(data.c[1]);             /* send word count */
   scsi_send(data.c[0]);
   
   switch (f)
   {
      case 16:
      case 17: phase = 0x00;                    /* data out phase */
               outp(SCSI_TCR,phase);
               for (i = 1; i <= cb; ++i)
               {
                  data.l = *intc;
                  /* printf ("\nOUTMEM: (%o) = %lx",iword+i-1,data.l); */
                  scsi_send(data.c[2]);            /* write data */
                  scsi_send(data.c[1]);
                  scsi_send(data.c[0]);
                  /* pause(" "); */
                  intc++;
               }
               break;
      case 0:
      case 1:
      case 2:  phase = 0x01;                    /* data in phase */
               outp(SCSI_TCR,phase);
               for (i = 1; i <= cb; ++i)
               {
                  data.c[2] = scsi_receive();      /* read data */
                  data.c[1] = scsi_receive();
                  data.c[0] = scsi_receive();
                  data.c[3] = 0;
                  *intc = data.l;
                  intc++;
               }
               break;

   }
   phase = 0x03;
   outp(SCSI_TCR,phase);            /* status phase */
   c_status = scsi_receive();      /* read x,q */
   phase = 0x07;
   outp(SCSI_TCR,data);             /* message phase */
   scsi_data = scsi_receive();
}



/*-------------------------------------------------------------------------*/
/* CSUBC: Control-Synchronized Block Transfer (16 bit words)               */
/*                                                                         */
/* read/write camac in q-stop mode (same address)                          */
/*                                                                         */
/*                                                                         */
/* f    = function code                                                    */
/* ext  = external address                                                 */
/* intc = camac data array                                                 */
/* cb   = repeat count                                                     */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void csubc(f,ext,intc,cb)
int f;
struct bcna ext;
uint_16 *intc;
uint_16 cb;
{
   union camac data;
   uchar phase, scsi_data;
   int i;

   if (ext.n >= 31) return;         /* dummy call for debugging */
   
   scsi_select(ext.c+1);            /* select crate */

   phase = 0x02;                    /* command phase: send f,n,a */
   outp(SCSI_TCR,phase);
   scsi_send(f+0xc0);
   scsi_send(ext.n | 0x80);         /* 16 bit word, q-stop mode */
   scsi_send(ext.a);
   data.i = 32768 - cb;
   scsi_send(data.c[1]);             /* send word count */
   scsi_send(data.c[0]);
   
   switch (f)
   {
      case 16:
      case 17: phase = 0x00;                    /* data out phase */
               outp(SCSI_TCR,phase);
              for (i = 1; i <= cb; ++i)
               {
                  data.i = *intc;
                  scsi_send(data.c[1]);         /* write data */
                  scsi_send(data.c[0]);
                  intc++;
               }
               break;
      case 0:
      case 1:
      case 2:  phase = 0x01;                    /* data in phase */
               outp(SCSI_TCR,phase);
               for (i = 1; i <= cb; ++i)
               {
                  data.c[1] = scsi_receive();      /* read data */
                  data.c[0] = scsi_receive();
                  *intc = data.i;
                  intc++;
               }
               break;

   }
   phase = 0x03;
   outp(SCSI_TCR,phase);            /* status phase */
   c_status = scsi_receive();      /* read x,q */
   phase = 0x07;
   outp(SCSI_TCR,data);             /* message phase */
   scsi_data = scsi_receive();
}



/*-------------------------------------------------------------------------*/
/* CFMAD: Address Scan Block Transfer (24 bit words)                       */
/*                                                                         */
/* read/write camac in address scan mode (inc sub-address)                 */
/*                                                                         */
/*                                                                         */
/* f    = function code                                                    */
/* ext  = external address                                                 */
/* intc = camac data array                                                 */
/* cb   = repeat count                                                     */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/


void cfmad(f,ext,intc,cb)
int f;
struct bcna ext;
long int *intc;
uint_16 cb;
{
   union camac data;
   uchar phase, scsi_data;
   int i;

   if (ext.n >= 31) return;         /* dummy call for debugging */
   
   scsi_select(ext.c+1);            /* select crate */

   phase = 0x02;                    /* command phase: send f,n,a */
   outp(SCSI_TCR,phase);
   scsi_send(f+0xc0);
   scsi_send(ext.n | 0x60);         /* 24 bit word, address-scan mode */
   scsi_send(ext.a);
   data.i = 32768 - cb;
   scsi_send(data.c[1]);             /* send word count */
   scsi_send(data.c[0]);
   
   switch (f)
   {
      case 16:
      case 17: phase = 0x00;                    /* data out phase */
               outp(SCSI_TCR,phase);
              for (i = 1; i <= cb; ++i)
               {
                  data.l = *intc;
                  scsi_send(data.c[2]);            /* write data */
                  scsi_send(data.c[1]);
                  scsi_send(data.c[0]);
                  intc++;
               }
               break;
      case 0:
      case 1:
      case 2:  phase = 0x01;                    /* data in phase */
               outp(SCSI_TCR,phase);
               for (i = 1; i <= cb; ++i)
               {
                  data.c[2] = scsi_receive();      /* read data */
                  data.c[1] = scsi_receive();
                  data.c[0] = scsi_receive();
                  data.c[3] = 0;
                  *intc = data.l;
                  intc++;
               }
               break;

   }
   phase = 0x03;
   outp(SCSI_TCR,phase);            /* status phase */
   c_status = scsi_receive();      /* read x,q */
   phase = 0x07;
   outp(SCSI_TCR,data);             /* message phase */
   scsi_data = scsi_receive();
}



/*-------------------------------------------------------------------------*/
/* CSMAD: Address Scan Block Transfer (16 bit words)                       */
/*                                                                         */
/* read/write camac in address scan mode (inc sub-address)                 */
/*                                                                         */
/*                                                                         */
/* f    = function code                                                    */
/* ext  = external address                                                 */
/* intc = camac data array                                                 */
/* cb   = repeat count                                                     */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/


void csmad(f,ext,intc,cb)
int f;
struct bcna ext;
uint_16 *intc;
uint_16 cb;
{
   union camac data;
   uchar phase, scsi_data;
   int i;

   if (ext.n >= 31) return;         /* dummy call for debugging */
   
   scsi_select(ext.c+1);            /* select crate */

   phase = 0x02;                    /* command phase: send f,n,a */
   outp(SCSI_TCR,phase);
   scsi_send(f+0xc0);
   scsi_send(ext.n | 0x40);         /* 16 bit word, address-scan mode */
   scsi_send(ext.a);
   data.i = 32768 - cb;
   scsi_send(data.c[1]);             /* send word count */
   scsi_send(data.c[0]);
   
   switch (f)
   {
      case 16:
      case 17: phase = 0x00;                    /* data out phase */
               outp(SCSI_TCR,phase);
              for (i = 1; i <= cb; ++i)
               {
                  data.i = *intc;
                  scsi_send(data.c[1]);            /* write data */
                  scsi_send(data.c[0]);
                  intc++;
               }
               break;
      case 0:
      case 1:
      case 2:  phase = 0x01;                    /* data in phase */
               outp(SCSI_TCR,phase);
               for (i = 1; i <= cb; ++i)
               {
                  data.c[1] = scsi_receive();      /* read data */
                  data.c[0] = scsi_receive();
                  *intc = data.i;
                  intc++;
               }
               break;

   }
   phase = 0x03;
   outp(SCSI_TCR,phase);            /* status phase */
   c_status = scsi_receive();      /* read x,q */
   phase = 0x07;
   outp(SCSI_TCR,data);              /* message phase */
   scsi_data = scsi_receive();
}






/*-------------------------------------------------------------------------*/
/* OUTMEM: Write a block of data to a camac memory module                  */
/*                                                                         */
/* iword = camac memory word number                                        */
/* ext   = camac module address                                            */
/* intc  = address of data block (array of 32 bit words)                   */
/* count = number of 24 bit words to transfer                              */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void outmem(iword,ext,intc,count)
int iword;
struct bcna ext;
uint_32 *intc;
int count;
{
   int f, q;

   if (ext.n >= 31) return;         /* dummy call for debugging */
   ext.a = a0;
   cssa (f16,ext,&iword,&q);        /* program start address in camac memory */
   ext.a = a1;                      /* switch to data write address */
   cfubc(f16,ext,intc,count,iword); /* and perform a block transfer */   
}


/*-------------------------------------------------------------------------*/
/* WR0694: Write a block of data to 0694 camac memory                      */
/*                                                                         */
/* iword = starting word address                                           */
/* ext   = external address                                                */
/* intc  = camac data array                                                */
/* cb    = repeat count                                                    */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void wr0694(iword,ext,array,cb)
uint_16 iword;
struct bcna ext;
long int *array;
uint_16 cb;
{
   uchar phase, scsi_data;
   union camac data;
   int i, a, f, next_word, words_left, count;


   if (ext.n >= 31) return;         /* dummy call for debugging */

   next_word = iword;
   words_left = cb;
   do
   {

      
      /* calculate a and f from iword */
      
      a = next_word & 0xf;
      f = ((next_word >> 4) + f16) & 0x1f;
      
      if (words_left <= 16)
         count = words_left;
      else
         count = 16 - a;               /* this number of writes to a15 */
       
      scsi_select(ext.c+1);            /* select crate */
      phase = 0x02;                    /* command phase: send f,n,a */
      outp(SCSI_TCR,phase);
      scsi_send(f+0xc0);
      scsi_send(ext.n | 0x60);         /* 24 bit word, address-scan mode */
      scsi_send(a);
      data.i = 32768 - count;
      scsi_send(data.c[1]);             /* send word count */
      scsi_send(data.c[0]);
      
      
      phase = 0x00;                    /* data out phase */
      outp(SCSI_TCR,phase);
      for (i = 1; i <= count; ++i)
      {
         data.l = *array;
         scsi_send(data.c[2]);         /* write data */
         scsi_send(data.c[1]);
         scsi_send(data.c[0]);
         array++;
      }
      phase = 0x03;
      outp(SCSI_TCR,phase);            /* status phase */
      c_status = scsi_receive();      /* read x,q */
      phase = 0x07;
      outp(SCSI_TCR,data);             /* message phase */
      scsi_data = scsi_receive();


      /* read next block if there is one */
      
      next_word = next_word + count;
      words_left = words_left - count;

   }
   while (words_left != 0);

}

/*-------------------------------------------------------------------------*/
/* RD0694: Write a block of data to 0694 camac memory                      */
/*                                                                         */
/* iword = starting word address                                           */
/* ext   = external address                                                */
/* intc  = camac data array                                                */
/* cb    = repeat count                                                    */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void rd0694(iword,ext,array,cb)
uint_16 iword;
struct bcna ext;
long int *array;
uint_16 cb;
{
   uchar phase, scsi_data;
   union camac data;
   int i, a, f, next_word, words_left, count;


   if (ext.n >= 31) return;         /* dummy call for debugging */

   next_word = iword;
   words_left = cb;
   do
   {

      
      /* calculate a and f from iword */
      
      a = next_word & 0xf;
      f = (next_word >> 4) & 0x1f;
      
      if (words_left <= 16)
         count = words_left;
      else
         count = 16 - a;               /* this number of reads to a15 */
       
      scsi_select(ext.c+1);            /* select crate */
      phase = 0x02;                    /* command phase: send f,n,a */
      outp(SCSI_TCR,phase);
      scsi_send(f+0xc0);
      scsi_send(ext.n | 0x60);         /* 24 bit word, address-scan mode */
      scsi_send(a);
      data.i = 32768 - count;
      scsi_send(data.c[1]);             /* send word count */
      scsi_send(data.c[0]);
      
      
      phase = 0x01;                    /* data in phase */
      outp(SCSI_TCR,phase);
      for (i = 1; i <= count; ++i)
      {
         data.c[2] = scsi_receive();      /* read data */
         data.c[1] = scsi_receive();
         data.c[0] = scsi_receive();
         data.c[3] = 0;
         *array = data.l;
         array++;
      }
      phase = 0x03;
      outp(SCSI_TCR,phase);            /* status phase */
      c_status = scsi_receive();      /* read x,q */
      phase = 0x07;
      outp(SCSI_TCR,data);             /* message phase */
      scsi_data = scsi_receive();


      /* read next block if there is one */
      
      next_word = next_word + count;
      words_left = words_left - count;

   }
   while (words_left != 0);

}




/*-------------------------------------------------------------------------*/
/* CGO: Execute a single dataless CAMAC action, function f, address EXT    */
/*-------------------------------------------------------------------------*/


void cgo(f,ext,q)
int f,*q;
struct bcna ext;
{
   union camac data;
   uchar phase, scsi_data;

   if (ext.n >= 31) return;         /* dummy call for debugging */
   switch (f)
   {
      case 16:
      case 17: *q = 0;
               return;
      case 0:
      case 1:
      case 2:  *q = 0;
               return;

   }
   scsi_select(ext.c+1);            /* select crate */

   phase = 0x02;                    /* command phase: send f,n,a */
   outp(SCSI_TCR,phase);
   scsi_send(f+0xc0);
   scsi_send(ext.n);
   scsi_send(ext.a);
   phase = 0x03;
   outp(SCSI_TCR,phase);            /* status phase */
   c_status = scsi_receive();      /* read x,q */
   if ( (c_status & 0x02) == 0)
      *q = 1;
   else *q = 0;
   phase = 0x07;
   outp(SCSI_TCR,data);             /* message phase */
   scsi_data = scsi_receive();
}


/*-------------------------------------------------------------------------*/
/* CTSTAT: Test status of last camac cyle                                  */
/*-------------------------------------------------------------------------*/


void ctstat(status)
int *status;
{

   uchar qx;
   
   qx = c_status & 0x03;
   switch(qx)
   {
      case 0:  *status = 0;     /* q = 1, x = 1 */
               break;
      case 1:  *status = 2;     /* q = 1, x = 0 */
               break;
      case 2:  *status = 1;     /* q = 0, x = 1 */
               break;
      case 3:  *status = 3;     /* q = 0, x = 0 */
               break;
   }
}

/*-------------------------------------------------------------------------*/
/* SCSI_SELECT: Select scsi target_id                                      */
/*-------------------------------------------------------------------------*/
void scsi_select(scsi_id)
uchar scsi_id;
{
   uchar icr, data;


   data = 0x00;
   outp(SCSI_TCR,data);             /* data_out phase */
   outp(SCSI_ODR,scsi_id);          /* write ID bit to output reg */
   icr = 0x05;
   outp(SCSI_ICR,icr);              /* assert SEL and enable output */
   do 
   {
      data = inp(0x1f9);            /* waiting for REQ/ */
   }
   while (data == 0);
   icr = icr & ~0x04;
   outp(SCSI_ICR,icr);              /* reset SEL output enabled */
}


/*-------------------------------------------------------------------------*/
/* SCSI_SEND: send a data byte to scsi target                              */
/*-------------------------------------------------------------------------*/
void scsi_send(data)
uchar data;
{

   uchar status,icr;

   icr = 0;

   do                         /* waiting for REQ/ from target */ 
   {
      status = inp(0x1f9);
   }
   while (status == 0);
   outp(SCSI_ODR,data);       /* write data to output reg */
   icr |= 0x01;
   outp(SCSI_ICR,icr);        /* Assert data bus write */
   icr |=0x11;
   outp(SCSI_ICR,icr);        /* assert ACK */
   do
      status = inp(0x1f9);    /* waiting for REQ/ inactive */
   while (status != 0);
   icr = icr & ~0x10;
   outp(SCSI_ICR,icr);        /* reset ACK */
}

/*-------------------------------------------------------------------------*/
/* SCSI_RECEIVE: read a data byte from scsi target                         */
/*-------------------------------------------------------------------------*/
uchar scsi_receive()
{

   uchar status,icr, data;

   icr = 0;
   outp(SCSI_ICR,icr);        /* data in phase */
   do 
      status = inp(0x1f9);    /* waiting for REQ */
   while (status == 0);
   data = inp(SCSI_CDR)       /* input data byte */;
   icr |=0x10;
   outp(SCSI_ICR,icr);        /* assert ACK */
   do 
      status = inp(0x1f9);    /* waiting for REQ/ inactive */
   while (status != 0);
   icr = icr & ~0x10;
   outp(SCSI_ICR,icr);        /* reset ACK */
   return(data);
}

void display_scsi()
{
   int i;
   uint_16 scsi_r[10];

   scsi_r[5] = inp(SCSI_BASE+5);

   printf ("\nscsi status %.4x  ",scsi_r[5]);
}





