/*-----------------------------------------------------------------------------
** Copyright (c) 1993 TRIUMF Cylotron Facility
**
** TRIUMF Electronics Grp, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A5
** Email: waters@sundae.triumf.ca Tel: (604) 222-1047 Fax: (604) 222-1074
**-----------------------------------------------------------------------------
** 
** Description:
**
**	Serial CAMAC driver for TRIMAC RS232CC.
** 
**   An implementation of the CAMAC standard routines in C,
**   using C argument passing conventions.
**      
** 
** Author:	Graham Waters, TRIUMF Electronics Group
** File:        camacLib.c
** Created:     June 1993.
** $Revision: 1.1.1.1 $
** last edit: $Date: 1995/10/22 02:30:21 $ $Author: ted $
**
** log file:  $Source: /usr/local/cvsroot/musr/mvme162/camac_old/sercam/camacLib.c,v $
**-------------------------------------------------------------------------**/
/*
 * Revision History:
 *   $Log: camacLib.c,v $
 *   Revision 1.1.1.1  1995/10/22 02:30:21  ted
 *   import from decu18
 *
 * Revision 1.1  93/07/07  11:35:29  waters
 * Initial revision
 * 
 * 
 * 
 *
 *
*/

/* Functions (all return void):
 *
 * cdreg(int *ext,int b,int c,int n,int a)   define external address 
 * cssa(int f,int ext,int *dat,int *q)       short (16) single action 
 * cfsa(int f,int ext,int *dat,int *q)       long (24) single action 
 * cccz(int ext)			 crate initialize 
 * cccc(int ext)			 crate clear 
 * ccci(int ext,int l)			 set/reset crate inhibit 
 * ctci(int ext,int *l)			 test crate inhibit 
 * cccd(int ext,int l)			 ena/disable crate demand 
 * ctcd(int ext,int *l)			 test crate demand enabled
 * ccgl(int lam,int l)			 ena/disaable graded lam (NON STANDARD)
 * ctgl(int ext,int *l)			 test graded lam
 * cdlam(int *lam,int b,int c,int n,int a,int inta[]) 	 define lam 
 * cclc(int lam)			 clear lam (dataless function)
 * cclm(int lam,int l)			 alternative lam clears 
 * ctlm(int lam,int *l)			 test lam 
 * ccinit(int b)			 initialize branch 
 * cclnk(int lam,void *())		 link routine to lam 
 * cculk(int lam,void *())		 unlink routine from lam 
 * ccrgl(int lam)			 re-enable graded lam (@end of isr)
 * cclwt(int lam)			 lam wait 
 * ctstat(int *istat)			 test status of previous operation 
 */


/* #include "target.h" */	/* SPARC, ULTRIX, VXWORKS */

#ifdef VXWORKS

#include "vxWorks.h"
#include "intLib.h"
#include "selectLib.h"
#include "ctype.h"
#include "ioLib.h"
#include "iosLib.h"
#include "taskLib.h"
#include "msgQLib.h"
#include "types.h"
#include "socket.h"
#include "in.h"

#else

#include	<stdio.h>
#include	<sys/types.h>
#include	<sgtty.h>
#include	<sys/ioctl.h>
#include	<signal.h>
#include	<sys/time.h>
#include	<fcntl.h>
#include	<varargs.h>

#endif 

int rstcam();
void cdreg();
void cfsa();
void bounce();
void real_bounce();

union camac
{
   char c[4];
   u_long l;
   u_int i[2];
};

struct bcna
{
   u_char b;
   u_char c;
   u_char n;
   u_char a;
};


#define BELL	7
#define CR	0x0D

/*---------------------------------------------------------------------------*/
/* 			DEFINE port names	*/

#ifdef SUN_SPARC
char		*port_name = "/dev/ttya";

struct sgttyb	oldport;
struct sgttyb	new;

#endif 
#ifdef DEC_MIPSEL
char		*port_name = "/dev/tty00";

struct sgttyb	oldport;
struct sgttyb	new;

#endif
#ifdef VXWORKS
	#ifdef CPU_332
		char		*port_name = "/tyCo/0";
	#endif

	#ifdef AIDC
		char		*port_name = "/mpc/0";
	#else
		char		*port_name = "/tyCo/1";
	#endif
#endif

/*---------------------------------------------------------------------------*/

int diag = 0;
int diag2 = 0;

char		*command_file;
int		port_fd;
int		con_fd = 0;
int		mflag = 0;
int		tandem = 1;
struct timeval	poll;

char		*logfile;
/* FILE		*logfp; */

setDiag(flag)
int flag;
{
	diag = flag;

}

setDiag2(flag)
int flag;
{
	diag = flag;

}


/*-------------------------------------------------------------------------*/
/* READ_PORT: Wait for character on specified port                         */
/*-------------------------------------------------------------------------*/

int read_port(port_fd,c)
int port_fd;
char *c;
{

	int portbit, readfds, err;
	struct timeval timeout;

	timeout.tv_sec = 4;		/* set timeout to 4 second */
	timeout.tv_usec = 0;

	/* now wait for characters */

	portbit = (1 << port_fd);

	readfds = portbit;

	err = select(32, &readfds, 0, 0, &timeout);
	if  (err <= 0)
	{
		perror(" select");
		return(err);
	}
	read(port_fd,c,1);
	return(err);
}

/*-------------------------------------------------------------------------*/
/* RSTCAM: Init serial io port for camac                                   */
/*-------------------------------------------------------------------------*/


int
rstcam()
{
	char c;
	int err;

	if ((port_fd = open(port_name, O_RDWR, 0)) < 0)	/* open port r/w */
	{
		perror(port_name);
		exit(1);
	}

#ifdef	VXWORKS
	(void) ioctl (port_fd, FIOBAUDRATE, 9600);
	(void) ioctl (port_fd, FIOSETOPTIONS, OPT_RAW);

#else
	ioctl(port_fd,TIOCSETP,&oldport);	/* set parameters */
	new = oldport;
	new.sg_flags |= RAW;
	if (tandem)
		new.sg_flags |= TANDEM;
	new.sg_flags &= ~(ECHO|CRMOD);
	new.sg_ispeed = B9600;			/* 9600 baud */
	new.sg_ospeed = B9600;
	stty(port_fd, &new);
#endif
	c = CR;
	printf ("\nrstcam: sending sync character ---- ");
	write(port_fd, &c, 1);			/* send sync chracter */
	err = read_port(port_fd,&c);		/* wait for reply */
	if (err <= 0)
	{
		printf (" error reading port %s\n",port_name);
		 return(err);
	}
	if (c != CR)
	{
		printf ("wrong sync char\n");
		return(-1);		/* not sync character */
	}
	printf ("ok\n");
	return(err);
}



/*-------------------------------------------------------------------------*/
/* CDREG: Pack camac B,C,N,A into long integer EXT                         */
/*-------------------------------------------------------------------------*/


void cdreg(ext,b,c,n,a)
struct bcna *ext;
int b,c,n,a;
{
   ext->b = b;
   ext->c = c;
   ext->n = n;
   ext->a = a;
}


/*-------------------------------------------------------------------------*/
/* CCSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/


void cssa(f,ext,data,q)
unsigned int f,*q;
unsigned int *data;
struct bcna ext;
{
	int i, err;
	char f_code;
	char c, outbuf[4], byte1, byte2, byte3;
	union camac cdata;
#ifdef	VXWORKS
	int lockKey;
 	lockKey = intLock();
#endif
	cdata.i[1] = *data;

	*q = 1;

	f_code = f;
	c = 'F';			/* fast mode - no handshake */

	ext.c = 0;		/* always crate 0 */

/*
	if (f != 0) 
	{
		if (f != 16)
		{
			if (diag) printf ("   (F not valid)");
			return;
		}
	}
*/

	/*-------------------------*/
	/* send NAF to output port */
	/*-------------------------*/

	sprintf (outbuf,"%c%c%c%c",c,ext.n,ext.a,f_code);
	write(port_fd,outbuf,4);
		err = read_port(port_fd,&c);	/* echo'd sync character */
		err = read_port(port_fd,&c);	/* wait for  NAF checksum */
		if (err <= 0)
        	if (diag) printf ("\nCSSA: timeout on CNAF");

	/*---------------------------*/
	/* checking camac cycle type */
	/*---------------------------*/

	if ((f_code >=16) & (f_code <= 24))				/* camac write requested */
	{

		/*--------------------------*/
		/* send DATA to output port */
		/*--------------------------*/

	#ifdef DEC_MIPSEL
		sprintf (outbuf,"%c%c%c%c",cdata.c[0],cdata.c[1],cdata.c[2]);
	#endif
	#ifdef SUN_SPARC
		sprintf (outbuf,"%c%c%c%c",cdata.c[3],cdata.c[2],cdata.c[1]);
	#endif
	#ifdef	VXWORKS
		sprintf (outbuf,"%c%c%c%c",cdata.c[3],cdata.c[2],cdata.c[1]);
	#endif
		write(port_fd,outbuf,3);
			err = read_port(port_fd,&c);	/* wait for  W1,W2,W3 checksum */
			if (err <= 0)
				if (diag) printf ("\nCSSA: timeout on DATA");
	}

	c = 'T';
	write(port_fd, &c, 1);		/* send the Terminator */
		err = read_port(port_fd,&c);	/* wait for echo'd Terminator */
	err = read_port(port_fd,&c);	/* wait for camac status word */
	if ( (c & 0x10) == 0x10)
		*q = 1;
	else
		*q = 0;

	/*-------------------------------*/
	/* checking for camac read cycle */
	/*-------------------------------*/

	if ((f_code >= 0) & (f_code <= 15))
	{

#ifdef SUN_SPARC 
		for (i = 3; i >=1; --i)
			err = read_port(port_fd,&cdata.c[i]);

#endif

#ifdef DEC_MIPSEL
		for (i = 0; i <=2; i++)
			err = read_port(port_fd,&cdata.c[i]);
#endif
#ifdef VXWORKS
		for (i = 3; i <=1; --i)
			err = read_port(port_fd,&cdata.c[i]);
#endif
		*data = cdata.i[1];		/* return data to caller */
		err = read_port(port_fd,&c);	/* wait for  STAT, R1,R2,R3 checksum */
		if (err <= 0)
			if (diag) printf ("\nCSSA: timeout on DATA");
		
	}
	/* if (cdata.i[1] & 0x01 == 0x01) */
		if (diag) printf ("\ncssa: c%d n%d a%d f%d,%lx",ext.c,ext.n,ext.a,f,*data);

#ifdef	VXWORKS
  	intUnlock(lockKey);
#endif

}


/*-------------------------------------------------------------------------*/
/* CFSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/


void cfsa(f,ext,data,q)
unsigned int f,*q;
long int *data;
struct bcna ext;
{
	int i, err;
	char f_code;
	char c, outbuf[4], byte1, byte2, byte3;
	union camac cdata;
#ifdef	VXWORKS
  	int lockKey;
  	lockKey = intLock();
#endif

	cdata.l = *data;

	*q = 1;

	f_code = f;
	c = 'F';			/* fast mode - no handshake */

	ext.c = 0;		/* always crate 0 */
/*
	if (f != 0) 
	{
		if (f != 16)
		{
			if (diag) printf ("   (F not valid)");
			return;
		}
	}
*/


	/*-------------------------*/
	/* send NAF to output port */
	/*-------------------------*/

	sprintf (outbuf,"%c%c%c%c",c,ext.n,ext.a,f_code);
	write(port_fd,outbuf,4);
		err = read_port(port_fd,&c);	/* echo'd sync character */
		err = read_port(port_fd,&c);	/* wait for  NAF checksum */
		if (err <= 0)
                        if (diag) printf ("\nCFSA: timeout on CNAF");

	/*---------------------------*/
	/* checking camac cycle type */
	/*---------------------------*/

	if ((f_code >=16) & (f_code <= 24))				/* camac write requested */
	{

		/*--------------------------*/
		/* send DATA to output port */
		/*--------------------------*/

#ifdef DEC_MIPSEL
		sprintf (outbuf,"%c%c%c%c",cdata.c[0],cdata.c[1],cdata.c[2]);
#endif
#ifdef SUN_SPARC
		sprintf (outbuf,"%c%c%c%c",cdata.c[3],cdata.c[2],cdata.c[1]);
#endif
#ifdef	VXWORKS
		sprintf (outbuf,"%c%c%c%c",cdata.c[3],cdata.c[2],cdata.c[1]);
#endif


		write(port_fd,outbuf,3);
			err = read_port(port_fd,&c);	/* wait for  W1,W2,W3 checksum */
			if (err <= 0)
				if (diag) printf ("\nCFSA: timeout on DATA");
	}

	c = 'T';
	write(port_fd, &c, 1);				/* send the Terminator */
		err = read_port(port_fd,&c);	/* wait for echo'd Terminator */
	if (diag) printf ("cfsa: echo'd Terminator = %c (%x)\n",c,c);
	err = read_port(port_fd,&c);		/* wait for camac status word */
	if (diag) printf ("cfsa: camac status = %x\n",c);
	if ( (c & 0x10) == 0x10)
		*q = 1;
	else
		*q = 0;

	/*-------------------------------*/
	/* checking for camac read cycle */
	/*-------------------------------*/

	if ((f_code >= 0) & (f_code <= 15))
	{

	if (diag) printf ("cfsa: f code < 0 - 15 >\n");
#ifdef SUN_SPARC 
		for (i = 3; i >=1; --i)
			err = read_port(port_fd,&cdata.c[i]);

#endif

#ifdef DEC_MIPSEL
		for (i = 0; i <=2; i++)
			err = read_port(port_fd,&cdata.c[i]);
#endif
#ifdef VXWORKS
		for (i = 3; i <=1; --i)
		{
			err = read_port(port_fd,&cdata.c[i]);
			if (diag) printf ("cfsa: cdata[%d] = %x\n",i,cdata.c[i]);
		}
#endif
		*data = cdata.l;				/* return data to caller */
		err = read_port(port_fd,&c);	/* wait for  STAT, R1,R2,R3 checksum */
		if (err <= 0)
			if (diag) printf ("\nCFSA: timeout on DATA");
		
	}
	if (diag) printf ("\ncfsa: c%d n%d a%d f%d data = %lx",ext.c,ext.n,ext.a,f,*data);
#ifdef	VXWORKS
  	intUnlock(lockKey);
#endif
}


#ifdef VXWORKS

void __stderr()
{
	printf ("stub to keep CaSrvr happy\n");	
}

void cccz(ext)
     int ext;
{
 if (diag2) printf ("\ncccz: ext %x",ext);
}

void cccc(ext)
     int ext;
{
	if (diag2) printf ("\ncccc: ext %x",ext);
}

void ccci(ext,l)
     int ext;
     int l;
{
	if (diag2) printf ("\nccci: ext %x l %d",ext,l);
}

void ctci(ext,l)
     int ext;
     int *l;
{
 if (diag2) printf ("\nctci: ext %x %d",ext,l); 
}

void cccd(ext,l)
     int ext;
     int l;
{
 if (diag2) printf ("\ncccd: ext %x %d",ext,l);
}

void ctcd(ext,l)
     int ext,*l;
{
 if (diag2) printf ("\nctcd: ext %x %d",ext,l);
}

void ccgl(lam,l)
     int lam;
     int l;
{
 if (diag2) printf ("\nccgl: ext %x %d",lam,l);
}
  
void ctgl(ext,l)
     int ext;
     int *l;
{
 if (diag2) printf ("\nctgl: ext %x %d",ext,l);
}

void cdlam(ext,b,c,n,a,inta)
     int *ext;
     int b,c,n,a;
     int inta[2];
{
  /* ignore inta[] */
 if (diag2) printf ("\ncdlam: cdreg (ext %d &d %d %d)",b,c,n,a);
  cdreg(ext,b,c,n,a);
}
     
void cclm(lam,l)
     int lam;
     int l;
{
  int temp1,temp2;

  if (l) {
    if (diag2) printf("\ncclm: cssa(24,lam,&temp1,&temp2)");
  } else {
    if (diag2) printf("\ncclm: cssa(26,lam,&temp1,&temp2)");
  }
}

void cclc(lam)
     int lam;
{
  int temp1,temp2;
  if (diag2) printf("\ncclc: cssa(10,lam,&temp1,&temp2)");
}

void ctlm(lam,l)
     int lam;
     int *l;
{
  int temp1,temp2;
  if (diag2) printf("\nctlm: cssa(8,lam,&temp1,&temp2)");;
  *l = 1; /* temp2; */
}

VOIDFUNCPTR lamisrs[25][8];

int taskidlam[25][8];

void lamIsr()
{
	if (diag2) printf ("\nlamIsr:");
}

void ccinit(b)
     int b;
{
	rstcam();
	if (diag2) printf ("\nccinit:");
}

void cclnk(lam,label)
     int lam;
     VOIDFUNCPTR label;
{
  if (diag2) printf("\ncclnk");
  lamisrs[((lam>>9) & 31)-1][lam>>24] = label;
  taskidlam[((lam>>9) & 31)-1][lam>>24] = taskIdSelf();
}

void cculk(lam,label)
     int lam;
     VOIDFUNCPTR label;
{
  if (diag2) printf("\ncculk:");
  lamisrs[((lam>>9) & 31)-1][lam>>24] = 0;
}
  
void ccrgl(lam)
     int lam;
{
  if (diag2) printf("\nccrg:");
}

void cclwt(lam)
     int lam;
{
  if (diag2) printf("\ncclwt: taskSuspend");
  taskSuspend(0);
}

void ctstat(istat)
     int *istat;
{
  if (diag2) printf ("\ncstat");
}

#endif
