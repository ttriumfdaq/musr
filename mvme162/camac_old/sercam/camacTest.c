/*-------------------------------------------------------------------------**
** file: bounce.c                                                          **
**-------------------------------------------------------------------------**
** project: VME real time tasks                                            **
** module(s):  tics_server                                                 **
** version:    1.0                                                         **
**                                                                         **
** 
** Author:	Graham Waters, TRIUMF Electronics Group                    **
** created:     28-Jan-93                                                  ** 
**-------------------------------------------------------------------------**
**
** Description:
** 
**	CAMAC dataway display bouncing ball 
**	 
**
*--------------------------------------------------------------------------*/

#include "vxWorks.h"

union camac
{
   char c[4];
   unsigned long l;
   unsigned int i[2];
};

struct bcna
{
   unsigned char b;
   unsigned char c;
   unsigned char n;
   unsigned char a;
};

void bounce();
void real_bounce();
void wr0694();


void cdemo()
{

	int err, q;

	int b,c,n_dsp, n_reg,a,f;
	
	static struct bcna display, sw_reg;
	long int cdata, i;
	

	f = 0;
	n_reg = 12;			/* sw_reg in this slot */
	n_dsp = 20;			/* dataway display in this slot */
	a =0;
	cdreg(&display,0,0,n_dsp,a);	/* define dataway display address  */
	cdreg(&sw_reg,0,0,n_reg,a);		/* define camac switch reg address */

	cfsa (f,sw_reg,&cdata,&q);		/* read switch reg */
	printf ("\nCDEMO: SWITCH REG =  %x q = %d\n",cdata,q);

	f = 16;
	/* cfsa (f,display,&cdata,&q); */		/* echo to display */

	
	/* simple_bounce(n_dsp); */


}


void bounce()
{

	long i, counter=0;
   for(; ;)
   {

      real_bounce();                   /* cycle through bounce under gravity */

      /* big delay before starting another bounce cycle */
      
      {      for (counter = 1; counter <= 100000; ++counter)

           i = i;
      }


   }
}

/*------------------------------------------------------------------------*/
/* bounce cycle on std bus display module                                 */
/*------------------------------------------------------------------------*/

simple_bounce()
{
  	int n, f, q, wait, port, value, top;
   	long int cdata;
	struct bcna ext;
	int n_slot;

	n_slot = 20;

	cdreg(&ext,0,0,n_slot,0);		/* define B0,C0,N12,a0  */
	top = 24;                  /* ball goes right to the top first time */
	f = 16;
	wait = 1;

	/* going up */

	for (n = 0; n <= top; ++n)
	{

		cdata = 1 << n;
		cfsa (f,ext,&cdata,&q);
       		delay(wait*1000);
      	}
      
      	/* falling */
   
      	for (n = top; n >= 0; --n)
      	{
		cdata = 1 << n;
		cfsa (f,ext,&cdata,&q);
         	delay(wait*1000);
   	}
}




/*------------------------------------------------------------------------*/
/* bounce cycle on std bus display module                                 */
/*------------------------------------------------------------------------*/
void
real_bounce(n_slot)
int n_slot;
{
  	int n, f, q, wait, port, value, top;
   	long int cdata;
	struct bcna ext;

	cdreg(&ext,0,0,n_slot,0);		/* define B0,C0,N12,a0  */
	top = 24;                  /* ball goes right to the top first time */
	f = 16;
	wait = top;
	do 
	{   
		/* rising */
   
		wait = 24/top;
      
		for (n = 0; n <= top; ++n)
		{

			cdata = 1 << n;
			cfsa (f,ext,&cdata,&q);
       			delay(wait*1000);
         		wait = wait+1;
      		}
      
      		/* falling */
   
      		for (n = top; n >= 0; --n)
      		{
			cdata = 1 << n;
			cfsa (f,ext,&cdata,&q);
         		delay(wait*1000);
         		wait = wait-1;
      		}
      		top = top -1;              /* don't bounce so high next time */
   	}
   	while (top != 0);
}


delay(count)
int count;
{
	int i;
	for (i = 0; i <= count; ++i)
		i = i;
}


void time()
{
	unsigned long t1,t2,diff, period;
	unsigned long count = 100;
  	int n, f, q, wait, port, value, top;
   	long int cdata;
	struct bcna ext;
	int n_slot;

	n_slot = 20;

	printf ("\nTiming CAMAC cycle CFSA:");

	cdreg(&ext,0,0,n_slot,0);		/* define B0,C0,N12,a0  */
	f = 16;

	/* 1 tic = 16ms */

	cdata = n;
    t1 = tickGet();
	for (n=1;n<=count;++n)
	{
		cfsa (f,ext,&cdata,&q);
	}
    t2 = tickGet();
    diff = t2 - t1;
	period =( diff * 16)/count;
    printf (" %ld ms\n",period);
}
