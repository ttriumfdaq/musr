/*-----------------------------------------------------------------------------
** Copyright (c) 1993 TRIUMF Cylotron Facility
**
** TRIUMF Electronics Grp, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A5
** Email: waters@sundae.triumf.ca Tel: (604) 222-1047 Fax: (604) 222-1074
**-----------------------------------------------------------------------------
** 
** Description:
**
**	Serial CAMAC driver for TRIMAC RS232CC.
** 
**   An implementation of the CAMAC standard routines in C,
**   using C argument passing conventions.
**      
** 
** Author:	Graham Waters, TRIUMF Electronics Group
** File:        camacLib.c
** Created:     June 1993.
** $Revision: 1.1.1.1 $
** last edit: $Date: 1995/10/22 02:30:21 $ $Author: ted $
**
** log file:  $Source: /usr/local/cvsroot/musr/mvme162/camac_old/sercam/sabcam.c,v $
**-------------------------------------------------------------------------**/
/*
 * Revision History:
 *   $Log: sabcam.c,v $
 *   Revision 1.1.1.1  1995/10/22 02:30:21  ted
 *   import from decu18
 *
 * Revision 1.1  93/07/07  11:35:29  waters
 * Initial revision
 * 
 * 
 * 
 *
 *
*/

/* Functions (all return void):
 *
 * cdreg(int *ext,int b,int c,int n,int a)   define external address 
 * cssa(int f,int ext,int *dat,int *q)       short (16) single action 
 * cfsa(int f,int ext,int *dat,int *q)       long (24) single action 
 * cccz(int ext)			 crate initialize 
 * cccc(int ext)			 crate clear 
 * ccci(int ext,int l)			 set/reset crate inhibit 
 * ctci(int ext,int *l)			 test crate inhibit 
 * cccd(int ext,int l)			 ena/disable crate demand 
 * ctcd(int ext,int *l)			 test crate demand enabled
 * ccgl(int lam,int l)			 ena/disaable graded lam (NON STANDARD)
 * ctgl(int ext,int *l)			 test graded lam
 * cdlam(int *lam,int b,int c,int n,int a,int inta[]) 	 define lam 
 * cclc(int lam)			 clear lam (dataless function)
 * cclm(int lam,int l)			 alternative lam clears 
 * ctlm(int lam,int *l)			 test lam 
 * ccinit(int b)			 initialize branch 
 * cclnk(int lam,void *())		 link routine to lam 
 * cculk(int lam,void *())		 unlink routine from lam 
 * ccrgl(int lam)			 re-enable graded lam (@end of isr)
 * cclwt(int lam)			 lam wait 
 * ctstat(int *istat)			 test status of previous operation 
 */


/* #include "target.h" */	/* SPARC, ULTRIX, VXWORKS */

#ifdef VXWORKS

#include "vxWorks.h"
#include "intLib.h"
#include "selectLib.h"
#include "ctype.h"
#include "ioLib.h"
#include "iosLib.h"
#include "taskLib.h"
#include "msgQLib.h"
#include "types.h"
#include "socket.h"
#include "in.h"

#else

#include	<stdio.h>
#include	<sys/types.h>
#include	<sgtty.h>
#include	<sys/ioctl.h>
#include	<signal.h>
#include	<sys/time.h>
#include	<fcntl.h>
#include	<varargs.h>

#endif 

int rstcam();
void cdreg();
void cfsa();
void bounce();
void real_bounce();
int sabRead();
int sabWrite();


union camac
{
   char c[4];
   u_long l;
   u_int i[2];
};

struct bcna
{
   u_char b;
   u_char c;
   u_char n;
   u_char a;
};


#define BELL	7
#define CR	0x0D
/*---------------------------------------------------------------------------*/

int diag = 1;
int diag2 = 0;

char		*command_file;
int		port_fd;
int		con_fd = 0;
int		mflag = 0;
int		tandem = 1;
struct timeval	poll;

char		*logfile;
/* FILE		*logfp; */

setDiag(flag)
int flag;
{
	diag = flag;

}

setDiag2(flag)
int flag;
{
	diag = flag;

}


/*-------------------------------------------------------------------------*/
/* RSTCAM: Init serial io port for camac                                   */
/*-------------------------------------------------------------------------*/


int
rstcam()
{
	char c;
	int err;
	c = CR;

	mpcAsync();

	port_fd = 0;

	printf ("\nrstcam: sending sync character ---- ");
	err = sabWrite(port_fd, "should be CR\n", 1);			/* send sync chracter */
	err = sabRead(port_fd,&c);		/* wait for reply */
	if (err <= 0)
	{
		printf (" error reading port %d\n",port_fd);
		 return(err);
	}
	if (c != CR)
	{
		printf ("wrong sync char\n");
		return(-1);		/* not sync character */
	}
	printf ("ok\n");
	return(err);
}



/*-------------------------------------------------------------------------*/
/* CDREG: Pack camac B,C,N,A into long integer EXT                         */
/*-------------------------------------------------------------------------*/


void cdreg(ext,b,c,n,a)
struct bcna *ext;
int b,c,n,a;
{
   ext->b = b;
   ext->c = c;
   ext->n = n;
   ext->a = a;
}


/*-------------------------------------------------------------------------*/
/* CCSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/


void cssa(f,ext,data,q)
unsigned int f,*q;
unsigned int *data;
struct bcna ext;
{
	int i, err;
	char f_code;
	char c, outbuf[4], byte1, byte2, byte3;
	union camac cdata;
	int lockKey;
 	lockKey = intLock();
	cdata.i[1] = *data;

	*q = 1;

	f_code = f;
	c = 'F';			/* fast mode - no handshake */

	ext.c = 0;		/* always crate 0 */

/*
	if (f != 0) 
	{
		if (f != 16)
		{
			if (diag) printf ("   (F not valid)");
			return;
		}
	}
*/

	/*-------------------------*/
	/* send NAF to output port */
	/*-------------------------*/

	sprintf (outbuf,"%c%c%c%c",c,ext.n,ext.a,f_code);
	sabWrite(port_fd,outbuf,4);
		err = sabRead(port_fd,&c);	/* echo'd sync character */
		err = sabRead(port_fd,&c);	/* wait for  NAF checksum */
		if (err <= 0)
        	if (diag) printf ("\nCSSA: timeout on CNAF");

	/*---------------------------*/
	/* checking camac cycle type */
	/*---------------------------*/

	if ((f_code >=16) & (f_code <= 24))				/* camac write requested */
	{

		/*--------------------------*/
		/* send DATA to output port */
		/*--------------------------*/

		sprintf (outbuf,"%c%c%c%c",cdata.c[3],cdata.c[2],cdata.c[1]);
		sabWrite(port_fd,outbuf,3);
			err = sabRead(port_fd,&c);	/* wait for  W1,W2,W3 checksum */
			if (err <= 0)
				if (diag) printf ("\nCSSA: timeout on DATA");
	}

	c = 'T';
	sabWrite(port_fd, &c, 1);		/* send the Terminator */
		err = sabRead(port_fd,&c);	/* wait for echo'd Terminator */
	err = sabRead(port_fd,&c);	/* wait for camac status word */
	if ( (c & 0x10) == 0x10)
		*q = 1;
	else
		*q = 0;

	/*-------------------------------*/
	/* checking for camac read cycle */
	/*-------------------------------*/

	if ((f_code >= 0) & (f_code <= 15))
	{
		for (i = 3; i <=1; --i)
			err = sabRead(port_fd,&cdata.c[i]);
		*data = cdata.i[1];		/* return data to caller */
		err = sabRead(port_fd,&c);	/* wait for  STAT, R1,R2,R3 checksum */
		if (err <= 0)
			if (diag) printf ("\nCSSA: timeout on DATA");
		
	}
	/* if (cdata.i[1] & 0x01 == 0x01) */
		if (diag) printf ("\ncssa: c%d n%d a%d f%d,%lx",ext.c,ext.n,ext.a,f,*data);

  	intUnlock(lockKey);

}


/*-------------------------------------------------------------------------*/
/* CFSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/


void cfsa(f,ext,data,q)
unsigned int f,*q;
long int *data;
struct bcna ext;
{
	int i, err;
	char f_code;
	char c, outbuf[4], byte1, byte2, byte3;
	union camac cdata;
  	int lockKey;
  	lockKey = intLock();

	cdata.l = *data;

	*q = 1;

	f_code = f;
	c = 'F';			/* fast mode - no handshake */

	ext.c = 0;		/* always crate 0 */
/*
	if (f != 0) 
	{
		if (f != 16)
		{
			if (diag) printf ("   (F not valid)");
			return;
		}
	}
*/


	/*-------------------------*/
	/* send NAF to output port */
	/*-------------------------*/

	sprintf (outbuf,"%c%c%c%c",c,ext.n,ext.a,f_code);
	sabWrite(port_fd,outbuf,4);
		err = sabRead(port_fd,&c);	/* echo'd sync character */
		err = sabRead(port_fd,&c);	/* wait for  NAF checksum */
		if (err <= 0)
                        if (diag) printf ("\nCFSA: timeout on CNAF");

	/*---------------------------*/
	/* checking camac cycle type */
	/*---------------------------*/

	if ((f_code >=16) & (f_code <= 24))				/* camac write requested */
	{

		/*--------------------------*/
		/* send DATA to output port */
		/*--------------------------*/

		sprintf (outbuf,"%c%c%c%c",cdata.c[3],cdata.c[2],cdata.c[1]);


		sabWrite(port_fd,outbuf,3);
			err = sabRead(port_fd,&c);	/* wait for  W1,W2,W3 checksum */
			if (err <= 0)
				if (diag) printf ("\nCFSA: timeout on DATA");
	}

	c = 'T';
	sabWrite(port_fd, &c, 1);				/* send the Terminator */
		err = sabRead(port_fd,&c);	/* wait for echo'd Terminator */
	if (diag) printf ("cfsa: echo'd Terminator = %c (%x)\n",c,c);
	err = sabRead(port_fd,&c);		/* wait for camac status word */
	if (diag) printf ("cfsa: camac status = %x\n",c);
	if ( (c & 0x10) == 0x10)
		*q = 1;
	else
		*q = 0;

	/*-------------------------------*/
	/* checking for camac read cycle */
	/*-------------------------------*/

	if ((f_code >= 0) & (f_code <= 15))
	{

	if (diag) printf ("cfsa: f code < 0 - 15 >\n");
		for (i = 3; i <=1; --i)
		{
			err = sabRead(port_fd,&cdata.c[i]);
			if (diag) printf ("cfsa: cdata[%d] = %x\n",i,cdata.c[i]);
		}
		*data = cdata.l;				/* return data to caller */
		err = sabRead(port_fd,&c);	/* wait for  STAT, R1,R2,R3 checksum */
		if (err <= 0)
			if (diag) printf ("\nCFSA: timeout on DATA");
		
	}
	if (diag) printf ("\ncfsa: c%d n%d a%d f%d data = %lx",ext.c,ext.n,ext.a,f,*data);
  	intUnlock(lockKey);
}


/*-------------------------------------------------------------------------*/
/* READ_PORT: Wait for character on specified port                         */
/*-------------------------------------------------------------------------*/

int sabRead(port_fd,c)
int port_fd;
char *c;
{
	mpcIn(c);
	return(0);
}


/*-------------------------------------------------------------------------*/
/* WRITE_PORT:                                                             */
/*-------------------------------------------------------------------------*/
int sabWrite(port_fd,buf,count)
int port_fd;
char *buf;
int count;
{
	
	char outbuf[64];
	int i;

	for (i = 0; i < count; ++i)
		outbuf[i] = buf[i];
	outbuf[i] = 0;
	mpcOut(outbuf);
	return(0);
}



#ifdef VXWORKS

void __stderr()
{
	printf ("stub to keep CaSrvr happy\n");	
}

void cccz(ext)
     int ext;
{
 if (diag2) printf ("\ncccz: ext %x",ext);
}

void cccc(ext)
     int ext;
{
	if (diag2) printf ("\ncccc: ext %x",ext);
}

void ccci(ext,l)
     int ext;
     int l;
{
	if (diag2) printf ("\nccci: ext %x l %d",ext,l);
}

void ctci(ext,l)
     int ext;
     int *l;
{
 if (diag2) printf ("\nctci: ext %x %d",ext,l); 
}

void cccd(ext,l)
     int ext;
     int l;
{
 if (diag2) printf ("\ncccd: ext %x %d",ext,l);
}

void ctcd(ext,l)
     int ext,*l;
{
 if (diag2) printf ("\nctcd: ext %x %d",ext,l);
}

void ccgl(lam,l)
     int lam;
     int l;
{
 if (diag2) printf ("\nccgl: ext %x %d",lam,l);
}
  
void ctgl(ext,l)
     int ext;
     int *l;
{
 if (diag2) printf ("\nctgl: ext %x %d",ext,l);
}

void cdlam(ext,b,c,n,a,inta)
     int *ext;
     int b,c,n,a;
     int inta[2];
{
  /* ignore inta[] */
 if (diag2) printf ("\ncdlam: cdreg (ext %d &d %d %d)",b,c,n,a);
  cdreg(ext,b,c,n,a);
}
     
void cclm(lam,l)
     int lam;
     int l;
{
  int temp1,temp2;

  if (l) {
    if (diag2) printf("\ncclm: cssa(24,lam,&temp1,&temp2)");
  } else {
    if (diag2) printf("\ncclm: cssa(26,lam,&temp1,&temp2)");
  }
}

void cclc(lam)
     int lam;
{
  int temp1,temp2;
  if (diag2) printf("\ncclc: cssa(10,lam,&temp1,&temp2)");
}

void ctlm(lam,l)
     int lam;
     int *l;
{
  int temp1,temp2;
  if (diag2) printf("\nctlm: cssa(8,lam,&temp1,&temp2)");;
  *l = 1; /* temp2; */
}

VOIDFUNCPTR lamisrs[25][8];

int taskidlam[25][8];

void lamIsr()
{
	if (diag2) printf ("\nlamIsr:");
}

void ccinit(b)
     int b;
{
	rstcam();
	if (diag2) printf ("\nccinit:");
}

void cclnk(lam,label)
     int lam;
     VOIDFUNCPTR label;
{
  if (diag2) printf("\ncclnk");
  lamisrs[((lam>>9) & 31)-1][lam>>24] = label;
  taskidlam[((lam>>9) & 31)-1][lam>>24] = taskIdSelf();
}

void cculk(lam,label)
     int lam;
     VOIDFUNCPTR label;
{
  if (diag2) printf("\ncculk:");
  lamisrs[((lam>>9) & 31)-1][lam>>24] = 0;
}
  
void ccrgl(lam)
     int lam;
{
  if (diag2) printf("\nccrg:");
}

void cclwt(lam)
     int lam;
{
  if (diag2) printf("\ncclwt: taskSuspend");
  taskSuspend(0);
}

void ctstat(istat)
     int *istat;
{
  if (diag2) printf ("\ncstat");
}

#endif

