/*-----------------------------------------------------------------------------
** Copyright (c) 1993 TRIUMF Cylotron Facility
**
** TRIUMF Electronics Grp, 4004 Wesbrook Mall, Vancouver, B.C. Canada, V6T 2A5
** Email: waters@sundae.triumf.ca Tel: (604) 222-1047 Fax: (604) 222-1074
**-----------------------------------------------------------------------------
** 
** Description:
**
**	Dummy CAMAC subroutines.
**
**   An implementation of the CAMAC standard routines in C,
**   using C argument passing conventions.
**
 
** 
** Author:	Graham Waters, TRIUMF Electronics Group
** File:        camacLib_scsi.c
** Created:     December 1994.
** $Revision: 1.1.1.1 $
** last edit: $Date: 1995/10/22 02:30:29 $ $Author: ted $
**
** log file:  $Source: /usr/local/cvsroot/musr/mvme162/camac_old/stub/camacLib_stub.c,v $
**-------------------------------------------------------------------------**
 *
 * Revision History:
 * 
 * 
 * 
 * 
 * 
 *
 *
*/
/* Functions (all return void):
 *
 * cdreg(int *ext,int b,int c,int n,int a)   define external address 
 * cssa(int f,int ext,int *dat,int *q)       short (16) single action 
 * cfsa(int f,int ext,int *dat,int *q)       long (24) single action 
*/

#include "vxWorks.h"
#include "scsiLib.h"
#include "ioLib.h"
#include "taskLib.h"
#include "memLib.h"
#include "stdio.h"


SCSI_PHYS_DEV * pScsiCamacDev[8];		/* storage for physical device info */

/* #define DEBUG */


typedef struct
{
	UINT8 b;
 	UINT8 c;
 	UINT8 n;
 	UINT8 a;
}BCNA;


/* define function prototypes */

void cdreg(BCNA *ext,UINT32 b,UINT32 c,UINT32 n,UINT32 a);
void cfsa(UINT32 f,BCNA ext,UINT32 *camacData,UINT8 *q);
void cssa(UINT32 f,BCNA ext,UINT16 *camacData,UINT8 *q);

#define maxN 24
#define maxA 16

UINT32	cnaf[maxN][maxA];		/* array for dummy camac crate */


/*-------------------------------------------------------------------------*/
/* CDREG: Pack camac B,C,N,A into long integer                             */
/*-------------------------------------------------------------------------*/
/*
synopsis
	void cdreg (ext,b,c,n,a)
	uint_32 * ext ----- OUTPUT: CAMAC packed address
	uint_32 b --------- INPUT:  CAMAC branch 0-7
	uint_32 c --------- INPUT:  CAMAC crate 0-7
	uint_32 n --------- INPUT:  CAMAC slot
	uint_32 a --------- INPUT:  CAMAC sub-address

Description
	Packs camac b,c,n,a init a long integer

*/

void
cdreg (ext,b,Crate,n,a)
BCNA *ext;
UINT32 b,Crate,n,a;
{
	ext->b = b;
	ext->c = Crate;
	ext->n = n;
	ext->a = a;
}

/*-------------------------------------------------------------------------*/
/* CFSA: Execute camac function f, address EXT, with data (long)VALUE      */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cfsa (f,ext,data,q)
	UINT32 ext ------- INPUT: CAMAC packed address
	UINT32 f --------- INPUT: CAMAC function
	uINT32 *data ----- INPUT/OUTPUT: write or read to from CAMAC
	UINT8 *q --------- OUTPUT -- true if q was present

Description
	Execute a single long camac read/write

--------------------------------------------------------------------------*/


void cfsa(UINT32 f,BCNA ext,UINT32 *camacData,UINT8 *q)
{

	switch(f)
	{
		case 0:
		case 1:
		case 2:
				*camacData = cnaf[ext.n][ext.a];
				break;

		case 16:
		case 17:
				cnaf[ext.n][ext.a] = *camacData;
				break;
	}

	*q = TRUE;

}


/*-------------------------------------------------------------------------*/
/* CSSA: Execute camac function f, address EXT, with data (short)VALUE     */
/*-------------------------------------------------------------------------*/

/*
synopsis
	void cssa (f,ext,data,q)
Description
	Execute a single short camac read/write
	UINT32 ext ------- INPUT: CAMAC packed address
	UINT32 f --------- INPUT: CAMAC function
	uINT16 *data ----- INPUT/OUTPUT: write or read to from CAMAC
	UINT8 *q --------- OUTPUT -- true if q was present


--------------------------------------------------------------------------*/


void cssa(UINT32 f,BCNA ext,UINT16 *camacData,UINT8 *q)
{

	switch(f)
	{
		case 0:
		case 1:
		case 2:
				*camacData = (UINT16)cnaf[ext.n][ext.a];
				break;

		case 16:
		case 17:
				cnaf[ext.n][ext.a] = *camacData;
				break;
	}

	*q = TRUE;
}
