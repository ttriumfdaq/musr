typedef unsigned long   uint_32;
typedef unsigned short   uint_16;

#include "vxWorks.h"
#include "intLib.h"
#include "taskLib.h"
#include "vme.h"

extern int DEBUG;
extern int POLLING;

readVme(addr)
uint_32 addr;
{
	uint_32 *vme_addr, data;

	sysBusToLocalAdrs(VME_AM_STD_SUP_DATA,(char*)addr,&vme_addr);
	printf ("\n %x -> %x\n",addr,vme_addr);
	data = *vme_addr;
	printf ("\n vme address %x = %x\n",vme_addr,data);
}


testVme(limit,diag)
int limit, diag;
{
	uint_32 *vme_addr, addr, data, cp;
	uint_16 counter;
	static uint_32 i, ext,q;
    static int   b,c,n,a;

	b = 0;c = 1;n = 20;a = 0;
	cdreg(&ext,b,c,n,a);

	cp = 0xf07e0000;
	counter = *(uint_16*)cp;

	for (i = 0; i <= limit; ++i )
	{
		addr = (i*4) + 0xf07c0000;
		if (addr == 0xf07e0000) break;
		vme_addr = (uint_32*)addr;
		*vme_addr = addr;
		cfsa(16,ext,&i,&q);
	}
	for (i = 0; i <= limit; ++i )
	{
		addr = (i*4) + 0xf07c0000;
		if (addr == 0xf07e0000) break;
		vme_addr = (uint_32*)addr;
		data = *vme_addr;
		counter = *(uint_16*)cp;
		cfsa(16,ext,&i,&q);
		if (diag)
		{
			printf ("\nloc %x, expected %x, received %x",addr,addr,data);
			printf ("  address count = %d",counter);
		}
		if (data != addr)
		{
			printf ("\nvme1190: loc %x, expected %x, rcvd %x",addr,addr,data);
			printf ("  address count = %d",counter);
		}
	}
}
