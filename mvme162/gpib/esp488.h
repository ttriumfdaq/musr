/************************************************************************
 *
 * Enhanced Engineering Software Package for GPIB-1014 Interface
 * Version 1.0
 * Copyright (c) 1991 National Instruments Corporation
 * All rights reserved.
 *
 * Include file for esp488.c
 *
 ************************************************************************/

#include "ugpib.h"		/* User GPIB include file */


/*----- Editable Parameters --------------------------------------------*/

#define PAD	 0		/* Primary address of GPIB interface	*/
				/*  Range: 00H - 30H			*/
#define SAD	 0		/* Secondary address of GPIB interface	*/
				/*  Range: 60H - 7EH, or 0 if none	*/

#define IBBASE	 0xFFF58000	/* 16-bit I/O address of GPIB interface	*/
#define IBVEC	 0xF0		/* interrupt vector number		*/
#define IBIRQ	 2		/* interrupt request line (1 - 7)	*/
#define IBBRG	 3		/* bus request/grant line (1 - 3)	*/

#define DFLTTIMO T10s		/* Default timeout for I/O operations	*/
#define HZ	 60		/* System clock ticks per second	*/

#define DEBUG	 0		/* 0 = normal operation, 1 = DEBUG mode */
#define INTROP	 0		/* 0 = polled operation, 1 = interrupt	*/
#define SYSTIMO	 1		/* 0 = no system timeouts (use counter)	*/
				/* 1 = system timeouts supported	*/

/*----- END Editable Parameters ----------------------------------------*/


#define IBMAXIO  0xFFFE			/* Maximum cont. I/O transfer	*/


typedef char		    int8;	/* HW-independent data types...	*/
typedef unsigned char	    uint8;
typedef short		    int16;
typedef unsigned short	    uint16;
typedef long		    int32;
typedef unsigned long	    uint32;

typedef unsigned char	  * faddr_t;	/* far character pointer	*/


typedef struct ibregs {         /*  hardware registers       */
	uint8   f00,    cdor;   /*  byte out register        */
	uint8   f01,    imr1;   /*  interrupt mask reg. 1    */
	uint8   f02,    imr2;   /*  interrupt mask reg. 2    */
	uint8   f03,    spmr;   /*  serial poll mode reg.    */
	uint8   f04,    admr;   /*  address mode register    */
	uint8   f05,    auxmr;  /*  auxiliary mode register  */
	uint8   f06,    adr;    /*  address register 0/1     */
	uint8   f07,    eosr;   /*  end of string register   */
	uint8   f08,    lpad;   /*  primary address switch   */
	uint8   f09,    vector; /*  vector register          */
} ibregs_t;


/* Read-only register mnemonics corresponding to write-only registers */

#define dir	cdor		/* Data In Register */
#define isr1	imr1		/* Interrupt Status Register 1 */
#define isr2	imr2		/* Interrupt Status Register 2 */
#define spsr	spmr		/* Serial Poll Status Register */
#define adsr	admr		/* Address Status Register */
#define cptr	auxmr		/* Command Pass Through Register */
#define adr0	adr		/* Address 0 Register */
#define adr1	eosr		/* Address 1 Register */ 
#define gsr     eosr            /* GPIB status register paged in in place of eosr */
#define isr0    adr             /* status register 0 paged in in place of adr */

/*--------------------------------------------------------------*/
/* NAT7210 bits:             POSITION              NAT7210 reg  */
/*--------------------------------------------------------------*/

#define HR_EOS          (unsigned char)(1<<4)   /* ISR0         */
#define HR_DI           (unsigned char)(1<<0)	/* ISR1         */
#define HR_DO           (unsigned char)(1<<1)	/*  ,           */
#define HR_ERR          (unsigned char)(1<<2)	/*  ,           */
#define HR_DEC          (unsigned char)(1<<3)	/*  ,           */
#define HR_END          (unsigned char)(1<<4)	/*  ,           */
#define HR_DET          (unsigned char)(1<<5)	/*  ,           */
#define HR_APT          (unsigned char)(1<<6)	/*  ,           */
#define HR_CPT          (unsigned char)(1<<7)	/*  ,           */
#define HR_DIIE         (unsigned char)(1<<0)	/* IMR1         */
#define HR_DOIE         (unsigned char)(1<<1)	/*  ,           */
#define HR_ERRIE        (unsigned char)(1<<2)	/*  ,           */
#define HR_DECIE        (unsigned char)(1<<3)	/*  ,           */
#define HR_ENDIE        (unsigned char)(1<<4)	/*  ,           */
#define HR_DETIE        (unsigned char)(1<<5)	/*  ,           */
#define HR_APTIE        (unsigned char)(1<<6)	/*  ,           */
#define HR_CPTIE        (unsigned char)(1<<7)	/*  ,           */
#define HR_ADSC         (unsigned char)(1<<0)	/* ISR2         */
#define HR_REMC         (unsigned char)(1<<1)	/*  ,           */
#define HR_LOKC         (unsigned char)(1<<2)	/*  ,           */
#define HR_CO           (unsigned char)(1<<3)	/*  ,           */
#define HR_REM          (unsigned char)(1<<4)	/*  ,           */
#define HR_LOK          (unsigned char)(1<<5)	/*  ,           */
#define HR_SRQI         (unsigned char)(1<<6)	/*  ,           */
#define HR_INT          (unsigned char)(1<<7)	/*  ,           */
#define HR_ACIE         (unsigned char)(1<<0)	/* IMR2         */
#define HR_REMIE        (unsigned char)(1<<1)	/*  ,           */
#define HR_LOKIE        (unsigned char)(1<<2)	/*  ,           */
#define HR_COIE         (unsigned char)(1<<3)	/*  ,           */
#define HR_DMAI         (unsigned char)(1<<4)	/*  ,           */
#define HR_DMAO         (unsigned char)(1<<5)	/*  ,           */
#define HR_SRQIE        (unsigned char)(1<<6)	/*  ,           */
#define HR_PEND         (unsigned char)(1<<6)	/* SPSR         */
#define HR_RSV          (unsigned char)(1<<6)	/* SPMR         */
#define HR_MJMN         (unsigned char)(1<<0)	/* ADSR         */
#define HR_TA           (unsigned char)(1<<1)	/*  ,           */
#define HR_LA           (unsigned char)(1<<2)	/*  ,           */
#define HR_TPAS         (unsigned char)(1<<3)	/*  ,           */
#define HR_LPAS         (unsigned char)(1<<4)	/*  ,           */
#define HR_SPMS         (unsigned char)(1<<5)	/*  ,           */
#define HR_NATN         (unsigned char)(1<<6)	/*  ,           */
#define HR_CIC          (unsigned char)(1<<7)	/*  ,           */
#define HR_ADM0         (unsigned char)(1<<0)	/* ADMR         */
#define HR_ADM1         (unsigned char)(1<<1)	/*  ,           */
#define HR_TRM0         (unsigned char)(1<<4)	/*  ,           */
#define HR_TRM1         (unsigned char)(1<<5)	/*  ,           */
#define HR_LON          (unsigned char)(1<<6)	/*  ,           */
#define HR_TON          (unsigned char)(1<<7)	/*  ,           */
#define HR_DL           (unsigned char)(1<<5)	/* ADR          */
#define HR_DT           (unsigned char)(1<<6)	/*  ,           */
#define HR_ARS          (unsigned char)(1<<7)	/*  ,           */
#define HR_HLDA         (unsigned char)(1<<0)	/* auxra        */
#define HR_HLDE         (unsigned char)(1<<1)	/*  ,           */
#define HR_REOS         (unsigned char)(1<<2)	/*  ,           */
#define HR_XEOS         (unsigned char)(1<<3)	/*  ,           */
#define HR_BIN          (unsigned char)(1<<4)	/*  ,           */
#define HR_CPTE         (unsigned char)(1<<0)	/* auxrb        */
#define HR_SPEOI        (unsigned char)(1<<1)	/*  ,           */
#define HR_TRI          (unsigned char)(1<<2)	/*  ,           */
#define HR_INV          (unsigned char)(1<<3)	/*  ,           */
#define HR_ISS          (unsigned char)(1<<4)	/*  ,           */
#define HR_PPS          (unsigned char)(1<<3)	/* ppr          */
#define HR_PPU          (unsigned char)(1<<4)	/*  ,           */

#define HR_LCM (unsigned char)(HR_HLDE|HR_HLDA)	/* auxra listen continuous */

/*  GSR register bit positions - */
#define BR_REN		(unsigned char)(1<<0)	/* gsr          */
#define BR_IFC		(unsigned char)(1<<1)	/*  ,           */
#define BR_SRQ		(unsigned char)(1<<2)	/*  ,           */
#define BR_EOI		(unsigned char)(1<<3)	/*  ,           */
#define BR_NRFD		(unsigned char)(1<<4)	/*  ,           */
#define BR_NDAC		(unsigned char)(1<<5)	/*  ,           */
#define BR_DAV		(unsigned char)(1<<6)	/*  ,           */
#define BR_ATN		(unsigned char)(1<<7)	/*  ,           */


#define ICR		(unsigned char)0x20	/* AUXMR control masks for hidden regs */
#define PPR		(unsigned char)0x60
#define AUXRA		(unsigned char)0x80
#define AUXRB		(unsigned char)0xA0
#define AUXRE		(unsigned char)0xC0
#define AUXRF		(unsigned char)0xD0
#define AUXRG		(unsigned char)0x40
#define AUXRI		(unsigned char)0xE0

#define LOMASK		0x1F			/* mask to specify lower 5 bits */

/* 7210 Auxiliary Commands */
#define AUX_PON         (unsigned char)0x00	/* Immediate Execute pon                  */
#define AUX_CPPF        (unsigned char)0x01	/* Clear Parallel Poll Flag               */
#define AUX_CR          (unsigned char)0x02	/* Chip Reset                             */
#define AUX_FH          (unsigned char)0x03	/* Finish Handshake                       */
#define AUX_TRIG        (unsigned char)0x04	/* Trigger                                */
#define AUX_RTL         (unsigned char)0x05	/* Return to local                        */
#define AUX_SEOI        (unsigned char)0x06	/* Send EOI                               */
#define AUX_NVAL        (unsigned char)0x07	/* Non-Valid Secondary Command or Address */
#define AUX_RQC         (unsigned char)0x08	/* Request Control                        */
#define AUX_SPPF        (unsigned char)0x09	/* Set Parallel Poll Flag                 */
#define AUX_RLC         (unsigned char)0x0A	/* Release Control                        */
#define AUX_LUT         (unsigned char)0x0B	/* Untalk                                 */
#define AUX_LUL         (unsigned char)0x0C	/* Unlisten                               */
#define AUX_NBAF        (unsigned char)0x0E	/* New Byte Available False               */
#define AUX_VAL         (unsigned char)0x0F	/* Valid Secondary Command or Address     */
#define AUX_GTS         (unsigned char)0x10	/* Go To Standby                          */
#define AUX_TCA         (unsigned char)0x11	/* Take Control Asynchronously            */
#define AUX_TCS         (unsigned char)0x12	/* Take Control Synchronously             */
#define AUX_LTN         (unsigned char)0x13	/* Listen                                 */
#define AUX_DSC         (unsigned char)0x14	/* Disable System Control                 */
#define AUX_9914        (unsigned char)0x15	/* Switch to 9914 Mode                    */
#define AUX_CIFC        (unsigned char)0x16	/* Clear IFC                              */
#define AUX_CREN        (unsigned char)0x17	/* Clear REN                              */
#define AUX_REQT        (unsigned char)0x18	/* Issue reqt message                     */
#define AUX_REQF        (unsigned char)0x19	/* Issue reqf message                     */
#define AUX_TCSE        (unsigned char)0x1A	/* Take Control Synchronously on End      */
#define AUX_LTNC        (unsigned char)0x1B	/* Listen in Continuous Mode              */
#define AUX_LUN         (unsigned char)0x1C	/* Local Unlisten                         */
#define AUX_EPP         (unsigned char)0x1D	/* Execute Parallel Poll                  */
#define AUX_SIFC        (unsigned char)0x1E	/* Set IFC                                */
#define AUX_SREN        (unsigned char)0x1F	/* Set REN                                */
#define AUX_PAGE        (unsigned char)0x50	/* Page in Additional Registers           */
#define AUX_HLDI        (unsigned char)0x51	/* Holdoff Handshake Immediately          */

/*
  There are several other possible definitions here for clearing various bits and signals.
  Not implemented at this time
*/


/* Configuration Register 2 (cfg2) bits */
#define D_SC    (1<<0)          /* set system controller (SC) bit */
#define D_LMR   (1<<1)          /* set local master reset bit */
#define D_SPAC  (1<<2)          /* set supervisor only access to board */
#define D_SFL   (1<<3)         	/* clear SYSFAIL line */
#define D_32B   (1<<4)          /* enable 32-bit addressing (1014D only) */


/* PROGRAM STATUS codes (see int pgmstat) */

#define PS_ONLINE	(1 << 0)		/* GPIB interface is online */
#define PS_SYSRDY	(1 << 1)		/* System support functions are initialized */
#define PS_TIMINST	(1 << 2)		/* Watch dog timer is installed and running */
#define PS_HELD		(1 << 3)		/* Handshake holdoff in effect */
#define PS_NOINTS	(1 << 4)		/* Do NOT use interrupts for I/O and waits */
#define PS_NOEOI	(1 << 5)		/* Do NOT send EOI at the end of writes */
#define PS_SAC		(1 << 6)		/* GPIB interface is System Controller */


/* IBPOKE codes for undocumented, low-level control */

#define PK_CODE		0x7FFF0000		/* CODE portion of mask */
#define PK_VALUE	0x0000FFFF		/* VALUE portion of mask */

#define PK_DEBUG	0x00000000		/* Turn on debugging if VALUE non-zero */
#define PK_USEINTS	0x00010000		/* Turn on interrupts if VALUE non-zero */
#define PK_IFCDELAY	0x00020000		/* Adjust length of IFC delay */
#define PK_POLLTIMO	0x00030000		/* Adjust POLL timeout index */
#define PK_ENABMEM	0x00040000		/* Enable system memory for DMA xfers */


/* Interrupt support macros */

#define USEINTS		(INTROP && SYSTIMO)	/* interrupts require SYSTIMO */

#if USEINTS
#define WaitingFor(i,j)	{if (!(pgmstat & PS_NOINTS)) waitForInt(i,j);}
#else
#define WaitingFor(i,j)
#endif


/* Timeout support macros */

#if SYSTIMO
#define TimedOut()	(!noTimo)		/* for testing within loops... */
#define NotTimedOut()	(noTimo)

#define TMFAC		HZ			/* timing factor for TM() macro */

#else
#define TimedOut()	((noTimo > 0) && !(--noTimo))
#define NotTimedOut()	((noTimo < 0) || ((noTimo > 0) && --noTimo))

#define TMFAC		(HZ * 1000)		/* timing factor for TM() macro */
#endif						/* -- approximate and system dependent! */

#define INITTIMO	(-1)			/* initial timeout setting */
#define TM(f,n,d)	((((f)*(n))/(d))+1)	/* clock ticks or counts per timo value */

