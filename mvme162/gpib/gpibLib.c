/*
 *	$Id: gpibLib.c,v 1.2 2000/12/22 21:51:56 David.Morris Exp $
 * 
 *	$Revision: 1.2 $
 *                                                                                     
 *	This module contains all the routines for running the GPIB interface
 *
 *	The GPIB unit is an MVIP300 module mounted on the MVME162 board. It should be
 *	considered as a system device like SCSI or Ethernet, at a level higher
 *	than CAMAC which hangs off SCSI.
 *
 *	The GPIB system can only be used by one task at a time. A semaphore must
 *	be used to block access to the routines until the current user is finished
 *	with the interface.
 *
 *	Modification history:
 *
 *	15-Feb-1996  TW  Added check for proper IP ID PROM data
 *	16-Feb-1996  TW  Read and write routines return ERROR on error rather
 *                   than TRUE.  gpibStatus is global.
 *
 *	$Log: gpibLib.c,v $
 *	Revision 1.2  2000/12/22 21:51:56  David.Morris
 *	Added CVS tags
 *	
 *
 */

#include "stdio.h"
#include "string.h"
#include "vxWorks.h"
#include "semLib.h"
#include "memLib.h"
#include "taskLib.h"
#include "ugpib.h"

#define GPIBLIB_H
#include "gpibLib.h"

/* #define DEBUG */

STATUS
gpibInit(char cPosition, int iTimeout) 
{
  static int flag = 0;	/* First time flag */
  int i, j;
  char * pcIPBase;
  char * pcIPProm;
  unsigned char * pcIPMem;
  char * pcGPIBAddress;
  unsigned char szBuffer[16];

/* Determine location of IP specified in cPosition */

  cPosition -= 'A';
  if (cPosition > 3)
  {
    cPosition -= 0x20;
  }

  if ((cPosition > 3) || (cPosition < 0))
  {
    return (IP300_BAD_ADDRESS);
  }

/*
  Set up memory block address for IP location
*/
  pcIPMem = (cPosition * 0x800000) + (unsigned char *) IP_MEM_ADDR;

#ifdef DEBUG
  printf("Setting IP memory block address to %p\n", pcIPMem);
#endif

/* 
  identify address of specified IP location
*/
  pcIPBase = (cPosition * 0x100) + (char *) IP_BASE_ADRS;

  /*
   *  Check ID PROM data
   */
  pcIPProm = pcIPBase + 0x81;
  for (i = 0; i < 7; i++)
  {
    szBuffer[i] = *pcIPProm;
    pcIPProm += 2;
  }
  if( szBuffer[0] != 'I' || szBuffer[1] != 'P' || szBuffer[2] != 'A' || 
      szBuffer[3] != 'C' || szBuffer[4] != 0xf0 || szBuffer[5] != 0x14 )
    {
      printf( "Invalid ID PROM data: '%c%c%c%c' Manufacturer:'0x%X' Model:'0x%X' Revision:'0x%X'\n",
        szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
        szBuffer[4], szBuffer[5], szBuffer[6] );
      return( ERROR );
    }
  else
    {
      printf( "ID PROM data: '%c%c%c%c' Manufacturer:'0x%X' Model:'0x%X' Revision:'0x%X'\n",
        szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
        szBuffer[4], szBuffer[5], szBuffer[6] );
    }

#ifdef DEBUG
  pcGPIBAddress =  pcIPBase + 0x1;
  for (i = 0; i < 8; i++)
  {
    szBuffer[i] = *pcGPIBAddress;
    printf("GPIB Register %d at %p = 0x%2X\n", i, pcGPIBAddress, szBuffer[i]);
    pcGPIBAddress += 2;
  }
#endif

  *((unsigned char *) (IPIC_ADDRESS + 0x04 + (cPosition * 2))) = (((unsigned long) pcIPMem) >> 24) & 0xff;
  *((unsigned char *) (IPIC_ADDRESS + 0x05 + (cPosition * 2))) = (((unsigned long) pcIPMem) >> 16) & 0xff;

  /* 64k size */
  /*
   *  As originally written by Dave Morris
   */
  *((unsigned char *) (IPIC_ADDRESS + 0x0c + cPosition)) = 0x00;

  /* 128k size */
  /* 
   * As suggested by GreenSpring Application Brief AN#93008
   * "Using Industry Packs on the Motorola MVME162"
   */
/*
  *((unsigned char *) (IPIC_ADDRESS + 0x0c + cPosition)) = 0x7f;
*/

/*
  Set up IPIC general control register

  General Control Registers look like:
        x_ERR   0  x_RT1   x_RT0   x_WIDTH1   x_WIDTH0   0   x_MEN
	   x_ERR: 1 = Assert IP's Error* signal.
	   x_RT1-x_RT0: Recovery Timer Delay 0, 2, 4, 8 useconds.
	   x_WIDTH1-x_WIDTH0: 00=32bits, 01=8bits, 10=16bits.
	   x_MEN: 1=Enable Memory Accesses to the IP

  For "standard" IP modules, this should generally be:
           0 0 0 0  0 1 0 1  = 0x05  (enabled for byte-transfers)
*/
  *((unsigned char *) (IPIC_ADDRESS + 0x18 + cPosition)) = 0x05;

/*
  now enable serial interrupts in IPIC to drive selected IP board

  interrupt register looks like:
     PLTY       EL     INT    IEN    ICLR     IL2     IL1      IL0
        PLTY: 1 = Rising Edge/High Level causes interrupts
	EL: 1 = Edge Sensitive
	INT: (READ ONLY) 1 = Interrupt being generated
	IEN: 1 = Interrupts enabled
	ICLR: 1 = Clear edge interrupt.  No meaning for level-ints.
	IL2-IL0: Level at which IP should interrupt.

  For interrupting IP's, this should generally be:
         0 0 0 1  0 1 0 0 = 0x14  (interrupt low-level on #4)
  For non-interrupting IP's, this should be:
         0 0 0 0  0 0 0 0 = 0x00
*/
  *((unsigned char *) (IPIC_ADDRESS + 0x10 + cPosition * 2)) = 0x00;
  *((unsigned char *) (IPIC_ADDRESS + 0x11 + cPosition * 2)) = 0x00;

/*
  Read switches on MVIP300 and save in global for other routines
*/
  pcGPIBAddress = pcIPBase + 0x11;
  gpibAddress = 0x1f & *pcGPIBAddress;

#ifdef DEBUG
  printf("GPIB Address Switch = %d\n", gpibAddress);
#endif

  pcGPIBAddress = pcIPBase + 0x13;
  for (i = 0; i < 256; i++)
  {
    *pcGPIBAddress = (char) i;
    j = 0x00ff & *pcGPIBAddress;
    if (j != i)
    {
      printf("Error writing to MVIP300 Vector Register. Wrote %d read %d\n", i, j);
    }
  }

#ifdef DEBUG
  printf("Make semaphore for mutex\n");
#endif

/*
  Make a binary semaphore that can be used for mutual exclusion in accessing
  the GPIB interface routines
*/
  if (!flag)		/* If first time */
  {
    semGPIB = semBCreate(SEM_Q_FIFO, SEM_FULL);  /* Create binary semaphore */

    for (i = 0; i < 32; i++)
    {
      gpibDevices[i] = NULL;                      /* Init device array */
    }

    flag++;
  }


/*
  Call the ESP-488 routines to
  -  put the interface online
  -  assign address of DIP switch on interface as primary address
  -  clear the interface
  -  set the remote enable
*/

  ibonl(1);
  ibpad(gpibAddress);
  ibsic();
  ibsre(1);
  ibtmo(iTimeout);
       
  return (OK);
}

GPIB_PHYS_DEV *
gpibPhysDevCreate(int iPriBusId)
{
/*
  This routine allocates memory for a GPIB device. It initializes the
  space with the passed data and returns a pointer to the structure
  in memory
*/

/* Check primary address boundaries */  

  if (iPriBusId < 0 || iPriBusId > 31)
  {
#ifdef DEBUG
    printf("Error with Primary Address boundaries\n");
#endif
    gpibError = EARG;
    return (NULL);
  }


/* Check for this primary address already existing */

  if (gpibDevices[iPriBusId])
  {
#ifdef DEBUG
    printf("This Primary Address alread used\n");
#endif
    gpibError = EARG;
    return (NULL);
  }

/* Allocate memory for new GPIB device and initialize space */

  semTake(semGPIB, WAIT_FOREVER);		/* Is it my turn? */

  gpibDevices[iPriBusId] = (GPIB_PHYS_DEV *) calloc(1, sizeof(GPIB_PHYS_DEV));

  gpibDevices[iPriBusId]->iPriBusId = iPriBusId;

  semGive(semGPIB);				/* Done changing GPIB database */

#ifdef DEBUG
  printf("Primary Bus Id = %d gpibDevices[%d] = %p\n", iPriBusId, iPriBusId, gpibDevices[iPriBusId]);
#endif
 
  gpibError = OK;
  return (gpibDevices[iPriBusId]);
}

STATUS
gpibPhysDevDelete(GPIB_PHYS_DEV * gpibItem)
{
/*
  This routine removes a particular GPIB device from the system.
  It frees memory allocated by gpibPhysDevCreate.
*/
  int i;

  for (i = 0; i < 31; i++)
  {
    if (gpibDevices[i] == gpibItem)
    {
      semTake(semGPIB, WAIT_FOREVER);		/* Is it my turn? */

      free (gpibDevices[i]);
      gpibDevices[i] = NULL;

      semGive(semGPIB);				/* Done changing GPIB database */

      gpibError = OK;
      return (OK);
    }
  }
  gpibError = EARG;
  return (ERROR);
}

STATUS
gpibShow(void)
{
/*
  This routine goes through the list of GPIB devices and prints out the ones
  that are created
*/
  int i;

  printf(" primary\n");
  printf(" address\n");
  printf(" -------\n");

  for (i = 0; i < 31; i++)
  {
    if (gpibDevices[i])
    {
      printf("    %2d\n", 
             gpibDevices[i]->iPriBusId);
    }
  }
  return (OK);
}

STATUS
gpibTrigger(GPIB_PHYS_DEV * gpibItem)
{
/*
  This routine tiggers the gpib device
*/
  semTake(semGPIB, WAIT_FOREVER);		/* Is it my turn? */
  dvtrg(gpibItem->iPriBusId);
  semGive(semGPIB);				/* Done with GPIB */
  return (OK);
}

STATUS
gpibRead(GPIB_PHYS_DEV * gpibItem, char * pszString, int iLength)
{
/*
  This routine reads data from the specified device and places it into the buffer.
  The count of the number of characters received is also returned.
*/
  int tid;
  int iPriority;
  int iNewPriority;

  semTake(semGPIB, WAIT_FOREVER);		/* Is it my turn? */

/*
  Get the current priority and slow it down so that the GPIB driver does not block other tasks
*/
  tid = taskIdSelf();
  taskPriorityGet(tid, &iPriority);
  iNewPriority = iPriority + 50;
  if (iNewPriority > 255)
  {
    iNewPriority = 255;
  }
  taskPrioritySet(tid, iNewPriority);

/*
  this next line must be made more general later
*/
  ibeos(0x040d);
  gpibStatus = dvrd(gpibItem->iPriBusId, pszString, iLength);
  gpibError = iberr;

/*
  Return to old priority
*/
  taskPrioritySet(tid, iPriority);
  semGive(semGPIB);				/* Done reading from GPIB */

#ifdef DEBUG
  printf("After gpibRead status is %X\n", gpibStatus);
#endif

  if (gpibStatus & ERR)
  {
    return (ERROR);
  }
  else
  {
    return (OK);
  }
}

STATUS
gpibWrite(GPIB_PHYS_DEV * gpibItem, char * pszString)
{
/*
  This routine writes data to the specified device from the buffer.
  The count of the number of characters written is also passed.
*/
  int iLength;
  int tid;
  int iPriority;
  int iNewPriority;

  semTake(semGPIB, WAIT_FOREVER);		/* Is it my turn? */

/*
  Get the current priority and slow it down so that the GPIB driver does not block other tasks
*/
  tid = taskIdSelf();
  taskPriorityGet(tid, &iPriority);
  iNewPriority = iPriority + 50;
  if (iNewPriority > 255)
  {
    iNewPriority = 255;
  }
  taskPrioritySet(tid, iNewPriority);

  iLength = strlen(pszString);
  
#ifdef DEBUG
  printf("Writing %s to GPIB address %d length = %d \n", pszString, gpibItem->iPriBusId, iLength);
#endif

  gpibStatus = dvwrt(gpibItem->iPriBusId, pszString, iLength);
  gpibError = iberr;

/*
  If there is an addressing error on a write try the write again, it might work the second time
*/
  if (gpibStatus & ERR)
  {
    if (iberr == EADR)
    {
#ifdef DEBUG
  printf("Trying write a second time\n");
#endif
      gpibStatus = dvwrt(gpibItem->iPriBusId, pszString, iLength);
      gpibError = iberr;
    }
  }
      
#ifdef DEBUG
  printf("ibsta = %x  iberr = %x\n", ibsta, iberr);
#endif

/*
  Return to old priority
*/
  taskPrioritySet(tid, iPriority);
  semGive(semGPIB);				/* Done writing to GPIB */

  if (gpibStatus & ERR)
  {
    return (ERROR);
  }
  else
  {
    return (OK);
  }
}

STATUS
gpibIoctl(GPIB_PHYS_DEV * gpibItem, int function, long arg)
{
/*
  This routine provides device specific control for GPIB instruments

  The functions that are available are based on National Instruments NI-488M 
  GPIB functions. These are both high level device functions and low level board functions.
  
  There also the NI-488.2M routines which adhere to IEEE-488.2-1987 standard for
  GPIB interfaces. These routines have not been implemented at this time.
*/

  semTake(semGPIB, WAIT_FOREVER);		/* Is it my turn? */

  switch (function)
  {
    default:
    {
      semGive(semGPIB);				/* Finished with GPIB */
      gpibError = ECAP;
      return (ERROR);
    }
  }
  semGive(semGPIB);				/* Finished with GPIB */
  return (OK);
}

char
gpibReadSerialPoll(GPIB_PHYS_DEV * gpibItem)
{
/*
  This routine reads the serial poll byte from the specified device and returns
  it in the STATUS byte
*/
  char StatusByte;
  
  semTake(semGPIB, WAIT_FOREVER);		/* Is it my turn? */
  dvrsp(gpibItem->iPriBusId, &StatusByte);
  semGive(semGPIB);				/* Done with GPIB */

#ifdef DEBUG
  printf("Status byte from serial poll of address %d was %d\n", gpibItem->iPriBusId, StatusByte);
#endif
  
  return (StatusByte);
}


