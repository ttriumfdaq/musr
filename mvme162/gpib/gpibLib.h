/********************************** gpibLib.h *************************/
/*
 *  Modification history:
 *
 *  16-Feb-1996  TW  gpibStatus is global.
 *
 */

typedef struct tag_GPIB_PHYS_DEV
{
  int iPriBusId;
} GPIB_PHYS_DEV;

#ifdef GPIBLIB_H

  #define IP300_BAD_ADDRESS   (1)
  #define IPIC_ADDRESS        (0xfffbc000)     /* IPIC chip base address */
  #define IP_BASE_ADRS        (0xfff58000)     /* Industry Packs base address */
  #define IP_MEM_ADDR         (0xc0000000)     /* IP 8M memory address block */

  SEM_ID semGPIB;

  GPIB_PHYS_DEV * gpibDevices[32];
  int gpibError;
  int gpibStatus;
  char gpibAddress;  /* Set with switches on MVIP300 */

#else

  extern GPIB_PHYS_DEV * gpibDevices[];
  extern int gpibError;
  extern int gpibStatus;

#endif

STATUS gpibInit(char cPosition, int iTimeout);
GPIB_PHYS_DEV * gpibPhysDevCreate(int iPriBusId);
STATUS gpibPhysDevDelete(GPIB_PHYS_DEV * gpibItem);
STATUS gpibShow(void);
STATUS gpibTrigger(GPIB_PHYS_DEV * gpibItem);
STATUS gpibRead(GPIB_PHYS_DEV * gpibItem, char * pszString, int iLength);
STATUS gpibWrite(GPIB_PHYS_DEV * gpibItem, char * pszString);
STATUS gpibIoctl(GPIB_PHYS_DEV * gpibItem, int function, long arg);
char gpibReadSerialPoll(GPIB_PHYS_DEV * gpibItem);
