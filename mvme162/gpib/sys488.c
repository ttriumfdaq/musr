/************************************************************************
 *
 * Enhanced Engineering Software Package for GPIB-1014 Interface
 * System Support Functions for VxWorks
 * Version 1.0
 * Copyright (c) 1990,1991 National Instruments Corporation
 * All rights reserved.
 *
 * NOTE: This file is included and compiled within esp488.c.
 *       It is NOT compiled separately.
 *
 ************************************************************************/

#include "vxWorks.h"
/*
#include "68k/iv.h"
*/
#include "iv.h"
#include "wdLib.h"
#include "semLib.h"
#include "sysLib.h"
#include "intLib.h"
#include "taskLib.h"

#if SYSTIMO
IBLCL WDOG_ID espwdid = 0;	/* watchdog timer ID for ESP routines */
#endif
#if USEINTS
IBLCL SEM_ID espsemid = 0;	/* semaphore ID for ESP interrupt support */
#endif


#if SYSTIMO
/*
 * Timer functions
 */
IBLCL ibtmintr()			/* Watchdog timeout routine */
{
#if DEBUG
	if (dbgMask & DBG_INTR)
		logMsg("TIMER INTERRUPT!\n");
#endif
	noTimo = 0;
#if USEINTS
	if (!(pgmstat & PS_NOINTS)) {
		semGive(espsemid);	/* wake up sleeping task */
	}
#endif
}
#endif


IBLCL startTimer(v)			/* Starts the timeout task  */
int v;					/* v = index into timeTable */
{
	DBGin(STARTTIMER);
	noTimo = INITTIMO;
#if USEINTS
	if (!(pgmstat & PS_NOINTS))
		semClear(espsemid);
#endif
	if (v > 0) {
		DBGprint(DBG_DATA, ("timo=%d  ", timeTable[v]));
#if SYSTIMO
		wdStart(espwdid, timeTable[v], ibtmintr, 0);
#else
		noTimo = timeTable[v];
#endif
		pgmstat |= PS_TIMINST;
	}
	DBGout();
}


IBLCL removeTimer()			/* Removes the timeout task */
{
	DBGin(REMOVETIMER);
	if (pgmstat & PS_TIMINST) {
#if SYSTIMO
		wdCancel(espwdid);
#else
		if (noTimo > 0)
			noTimo = INITTIMO;
#endif
		pgmstat &= ~PS_TIMINST;
	}
	DBGout();
}


#if USEINTS
/*
 * GPIB interrupt service routine -- fast and simple
 */
IBLCL ibintr()
{
#if DEBUG
	if (dbgMask & DBG_INTR)
		logMsg("GPIB INTERRUPT!\n");
#endif
	semGive(espsemid);		/* wake up sleeping task */
	sysBusIntAck(ibirq);		/* acknowledge interrupt on VXIcpu-030 */
					/* (null function on MV162) */
}


/*
 * Wait for GPIB or timer interrupt.  Semaphore will be posted by
 * either ibintr or ibtmintr.
 */
IBLCL waitForInt(imr1mask, imr2mask)
int imr1mask;
int imr2mask;
{
	DBGin(WAITFORINT);
	if (semClear(espsemid) == ERROR) {
	/*
	 *	If semaphore not already available, enable
	 *	requested interrupts and wait until it is...
	 */
		DBGprint(DBG_DATA, ("imr1mask=0x%x imr2mask=0x%x  ", imr1mask, imr2mask));
		if (imr1mask)
			GPIBout(imr1, imr1mask);
		if (imr2mask)
			GPIBout(imr2, imr2mask);
		semTake(espsemid, WAIT_FOREVER);
	}
	DBGout();
}
#endif


/*
 * VxWorks initialization functions
 */
IBLCL osInit()
{
	int	s;

	DBGin(OSINIT);
#if SYSTIMO
	if (!espwdid)
		espwdid = wdCreate();
	DBGprint(DBG_DATA, ("espwdid=0x%x  ",  espwdid));
	s = sysClkRateGet();
	DBGprint(DBG_DATA, ("ClkRate=%d  ", s));
	if (s != timeTable[0]) {
		DBGprint(DBG_BRANCH, ("adjusting timeTable  ", s));
		timeTable[ 0] = s;			/* (New TMFAC)  */
		timeTable[ 1] = TM(s,10,1000000L);	/*  1: T10us    */
		timeTable[ 2] = TM(s,30,1000000L);	/*  2: T30us    */
		timeTable[ 3] = TM(s,100,1000000L);	/*  3: T100us   */
		timeTable[ 4] = TM(s,300,1000000L);	/*  4: T300us   */
		timeTable[ 5] = TM(s,1,1000);		/*  5: T1ms     */
		timeTable[ 6] = TM(s,3,1000);		/*  6: T3ms     */
		timeTable[ 7] = TM(s,10,1000);		/*  7: T10ms    */
		timeTable[ 8] = TM(s,30,1000);		/*  8: T30ms    */
		timeTable[ 9] = TM(s,100,1000);		/*  9: T100ms   */
		timeTable[10] = TM(s,300,1000);		/* 10: T300ms   */
		timeTable[11] = TM(s,1,1);		/* 11: T1s      */
		timeTable[12] = TM(s,3,1);		/* 12: T3s      */
		timeTable[13] = TM(s,10,1);		/* 13: T10s     */
		timeTable[14] = TM(s,30,1);		/* 14: T30s     */
		timeTable[15] = TM(s,100,1);		/* 15: T100s    */
		timeTable[16] = TM(s,300,1);		/* 16: T300s    */
		timeTable[17] = TM(s,1000,1);		/* 17: T1000s   */
	}
#endif
#if USEINTS
	if (!espsemid)
		espsemid = semCreate();
	DBGprint(DBG_DATA, ("espsemid=0x%x  ", espsemid));
/*
 *	Install ISR for GPIB interrupts...
 */
        s = intConnect(INUM_TO_IVEC(ibvec), ibintr, 0);
	DBGprint(DBG_DATA, ("intConnect=%d  ", s));
/*
 *	Enable GPIB interrupt request line...
 *
 *	*LCSR_INT_MASK = *LCSR_INT_MASK | ((char) (1 << ibirq));
 *	*GP_CTL = *GP_CTL | ((char) 0x10);
 */
	sysIntEnable(ibirq);	/* performs function of above two lines */
#endif
	pgmstat |= PS_SYSRDY;
	DBGout();
}


IBLCL osReset()
{
	DBGin(OSRESET);
/*
 *	Disable GPIB interrupt request line...
 *
 *	*LCSR_INT_MASK = *LCSR_INT_MASK & ~((char) (1 << ibirq));
 */
	sysIntDisable(ibirq);	/* performs function of above line */
	pgmstat &= ~PS_SYSRDY;
	DBGout();
}




