/*
 *   $Id: te28Lib.c,v 1.3 2006/09/29 14:15:36 asnd Exp $
 * 
 *   $Revision: 1.3 $
 *                                                                                     
/**
 * \mainpage
 *
 * This file contains the implementation of a motor control system for running
 * DC motors with incremental encoders. The driver is used by CAMP applications
 * to run cryostat valve motor controllers and operates under the vxWorks RTOS.
 * The motor controller is a Technology-80 TE28 2 Channel DC Motor Controller Industry Pack.
 *
 * A simple console based tool te28() is provided for manual testing of the motor system.
 *
 * The W1 and W2 jumper locations on the TE28 board must be jumpered.
 *
 * Further information on the use of the module can be found in these publications:
 *                                                                                  
 * Technology-80                                                                  
 * - Model 28/29 Hardware Guide                                                   
 * - Model 28/29 Software Guide                                                   
 *                                                                                  
 * National Semiconductor                                                         
 * - LM628/LM629 Precision Motor Controller                                       
 * - LM628 Programming Guide Application Note AN-693
 * - LM628 User's Guide Application Note AN-706
 *                                                                                  
 */
/**
 * \file
 *
 * The te28Lib Motor Controller Driver module provides a user console for controlling
 * motors and testing equipment. The command to start the console is te28(). A ? or invalid
 * command character will display the help screen.
 * <code>
\verbatim
te28 Motor System interactive tool commands
i<d> - use IP position (A-D)
c<d> - use channel (0,1)
d - list info about devices
an - set acceleration to n
vn - set velocity to n
en - set encoder count to n
qn - set slope to n
xn - set position error to n
mn - move device absolute turns n
jn - jog to encoder count n
l(n) - read/(set polarity) limits
s - stop device
h - halt motion immediately
nn - set motor enable to n
r - reset device
p(n) - report/(set) position of device
bm n - bounce from m to n
* - quit
\endverbatim
 * </code>
 * 
 */
#include "vxWorks.h"
#include "taskLib.h"
#include "semLib.h"
#include "iv.h"
#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "math.h"
#include "te28Lib.h"
#include "te28v.h"

#include "te28_1.c" /* Low level driver routines */

/** The new documented version with all the bells and whistles */
#define VERSION "2.0"

static SEM_ID sidTe28Int;
static SEM_ID sidTe28Access;
static int bBounce = FALSE;
static TE28_BOARD te28Board[TE28_MAX_BOARD];

/**************************************
  Undocumented DEBUG function
***************************************/

#define DEBUG_IOCTL   0x001
#define DEBUG_SET     0x002
#define DEBUG_GET     0x004
#define DEBUG_MOVE    0x008

static WORD wLibDebug = 0;

void te28LibDebug(short sValue) {
	printf("te28 debug mask old 0x%x ", wLibDebug);
  wLibDebug = sValue;
	printf("new 0x%x\n", wLibDebug);
}


/**
 *   This routine initializes the driver software for the Technology-80      
 *   Model 28 Industry Pack. The low level driver is initialized and the board        
 *   registry is cleared the first time it is run. For each IP module installed in    
 *   the system, the base address is determined, the low level board code is          
 *   initialized, and the data space for the two axes on the board is cleared.        
 *   Interrupts are connected to the MVME162 IPIC controller and the interrupt vector 
 *   is loaded. Finally the board is registered.                                      
 *                                                                                    
 *   After calling, the low level driver will be initialized, and specific axes can now be 
 *   initialized.                                                                     
 *
 *   @param cPosition position of the Industry Pack on the MVME162 carrier CPU
 *   @param iVector the interrupt vector for this board.        
 *                                                                                    
 *   @return the result code indicates if the initialization was            
 *   successful. TE28_ERROR is returned for any error. TE28_OK is returned on         
 *   success.                                                                         
 *                                                                                    
 */
STATUS te28Init(char cPosition, int iVector) {
  static int bFirstTime = TRUE;
  short sResult;
  unsigned char * usIPBaseAddress;
  unsigned short * pusIPIDAddress;
  static int iNextChannel = 0;
  char * pcIPProm;
  int iIndex;
  unsigned char szBuffer[16];

  /* First time through initialize software                                             */
  /* driver                                                                             */

  if (bFirstTime) {
    printf("te28Lib Version %s %s \n", VERSION, __DATE__);
    
    sResult = te28InitSw();
    printf("te28 Low Level Driver Version %4X\n", sResult);

    for (iIndex = 0; iIndex < TE28_MAX_BOARD; iIndex ++)
      te28Board[iIndex].bRegistered = FALSE;

    bFirstTime = FALSE;
  }

  /*   Determine location of IP specified in     */
  /*   cPosition                                 */

  cPosition -= 'A';
  if (cPosition > 3) {
    cPosition -= 0x20;
  }

  /*   Is input data valid? Is this board        */
  /*   already initialized?                      */

  if ((cPosition < 0) || (cPosition > 3) || (te28Board[cPosition].bRegistered)) {
    return TE28_ERROR;
  }

  /*   Check ID PROM data                        */
  
  pcIPProm = (char *) MVME162_IPBaseAddress + (cPosition * 0x100) + 0x81;
  for (iIndex = 0; iIndex < 7; iIndex++) {
    szBuffer[iIndex] = *pcIPProm;
    pcIPProm += 2;
  }

  if (szBuffer[0] != 'I' || szBuffer[1] != 'P' || szBuffer[2] != 'A' ||
      szBuffer[3] != 'C' || szBuffer[4] != 0xC1 || szBuffer[5] != 0x1C) {
    printf("te28 - Invalid ID PROM data: '%c%c%c%c' Manufacturer:'0x%X' Model:'0x%X' Revision:'0x%X'\n", 
           szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
           szBuffer[4], szBuffer[5], szBuffer[6] );
    return TE28_ERROR;
  } else {
    printf("te28 - ID PROM data: '%c%c%c%c' Manufacturer '0x%X' Model '0x%X' Revision '0x%X'\n", 
           szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
           szBuffer[4], szBuffer[5], szBuffer[6]);
  }

  /*   Register the position. Set all addresses  */

  usIPBaseAddress = (unsigned char *) (MVME162_IPBaseAddress + cPosition * 0x100);
  pusIPIDAddress =  (unsigned short *) (MVME162_IPBaseAddress + cPosition * 0x100 + 0x80);

  /*   The library routine initializes both      */
  /*   channels in the te28InitPack call.        */

  sResult = te28InitPack(0, cPosition, usIPBaseAddress, iVector, pusIPIDAddress);

  /*   If the returned value is not 2 then an    */
  /*   error ocurred                             */

  if (sResult != 2)
    return TE28_ERROR;

  /*   Initialize each channel. Low level        */
  /*   driver assigns channel numbers. Parallel  */
  /*   assignment by iNextChannel                */

  for (iIndex = 0; iIndex < TE28_MAX_AXES; iIndex ++) {
    te28Board[cPosition].Axis[iIndex].iChannel = iNextChannel;
    te28Board[cPosition].Axis[iIndex].bRegistered = FALSE;

    te28ResetAxis(te28Board[cPosition].Axis[iIndex].iChannel);
    te28Home(te28Board[cPosition].Axis[iIndex].iChannel);
    te28StopMove(te28Board[cPosition].Axis[iIndex].iChannel);
    iNextChannel ++;
  }

  /*   Set up general operation for this IP      */
  /*   position                                  */
  /*   General Control Registers look like:
        x_ERR   0  x_RT1   x_RT0   x_WIDTH1   x_WIDTH0   0   x_MEN

     x_ERR: 1 = Assert IP's Error* signal.
     x_RT1-x_RT0: Recovery Timer Delay 0, 2, 4, 8 useconds.
     x_WIDTH1-x_WIDTH0: 00=32bits, 01=8bits, 10=16bits.
     x_MEN: 1=Enable Memory Accesses to the IP

        0 0 0 0  0 0 0 0  = 0x00  (non memory enabled, I/O only) */

  *((unsigned char *) (MVME162_IPIC_ADDRESS + 0x18 + cPosition)) = 0x00;

  /*   connect the TE28 IP board interrupt:      */

  sidTe28Int = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
  intConnect (INUM_TO_IVEC (iVector), te28Int, 0);
  intConnect (INUM_TO_IVEC (iVector), te28Int, 0);

  /*   now enable interrupts in IPIC to drive    */
  /*   selected IP board                         */

  /*  interrupt register looks like:
     PLTY       EL     INT    IEN    ICLR     IL2     IL1      IL0

    PLTY: 1 = Rising Edge/High Level causes interrupts
    EL: 1 = Edge Sensitive
    INT: (READ ONLY) 1 = Interrupt being generated
    IEN: 1 = Interrupts enabled
    ICLR: 1 = Clear edge interrupt.  No meaning for level-ints.
    IL2-IL0: Level at which IP should interrupt.

    For interrupting IP's, this should generally be:
         0 0 0 1  0 1 0 0 = 0x14  (interrupt low-level on #4)
    For non-interrupting IP's, this should be:
         0 0 0 0  0 0 0 0 = 0x00  */

  /*   As suggested by GreenSpring Application   */
  /*   Brief AN#93008 "Using Industry Packs on   */
  /*   the Motorola MVME162"                     */

  *((unsigned char *) (MVME162_IPIC_ADDRESS + 0x10 + cPosition * 2)) = 0x05 - cPosition;
  *((unsigned char *) (MVME162_IPIC_ADDRESS + 0x11 + cPosition * 2)) = 0x05 - cPosition;

  /*   create semaphore for exclusive access to  */
  /*   I/O registers. Most require two accesses  */
  /*   for control                               */

  sidTe28Access = semBCreate(SEM_Q_FIFO, SEM_FULL);

  /*   Finish by registering board               */

  te28Board[cPosition].bRegistered = TRUE;

  return TE28_OK;
}

/**
 *   Creates a handle to each new motor axis. Note that the low level driver assigns   
 *   channel numbers to both axes on the IP autonomously. This is bad design because 
 *   the user should be assigning channels. It would be OK if it only made one       
 *   assignment each time and returned the number, two is not normal.                
 *                                                                                   
 *   This needs work here since currently the code ignores more than one board and   
 *   hence can use channel 0 or 1 only                                               
 *                                                                                   
 *   The IP must be initialized with te28Init before this function is       
 *   called.                                                                         
 *                                                                                   
 *   @param cPosition the Industry Pack position on the MVME162 carrier CPU
 *   @param iChannel the channel on the Model 28                      
 *                                                                                   
 *   @return a TE28_DEV pointer to the data structure for the requested channel.
 *   NULL is returned if an error occurs or an input is invalid.                    
 */
TE28_DEV * te28PhysDevCreate(char cPosition, int iChannel) {
  /*   Determine location of IP specified in     */
  /*   cPosition                                 */

  cPosition -= 'A';
  if (cPosition > 3) {
    cPosition -= 0x20;
  }

  /*   Is the position registered, is this       */
  /*   channel valid and not already registered  */

  if ((cPosition < 0) || (cPosition > 3) ||
      (!te28Board[cPosition].bRegistered) ||
      (iChannel < 0 || iChannel >= TE28_MAX_AXES) ||
      (te28Board[cPosition].Axis[iChannel].bRegistered))
    return NULL;

  /*   register the axis for this board          */
  /*   position. Init struct members             */

  te28Board[cPosition].Axis[iChannel].iChannel = iChannel;
  te28Board[cPosition].Axis[iChannel].bRegistered = TRUE;
  te28Board[cPosition].Axis[iChannel].tidMove = 0;
  
  te28ResetAxis(iChannel);

  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_ENCODER, ENCODER_COUNTS);
  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_SLOPE, 1.0);
  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_OFFSET, 0.0);
  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_VELOCITY, 1.0);
  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_ACCELERATION, 2.0);

  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_LIMITS, 0);

  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_POS_ERR, 0x7fff);
  te28Set(&te28Board[cPosition].Axis[iChannel], TE28_POS_ERR_STAT, FALSE);
  
  te28Ioctl(&te28Board[cPosition].Axis[iChannel], TE28_FILTER_KP, 100);
  te28Ioctl(&te28Board[cPosition].Axis[iChannel], TE28_FILTER_KI, 0);
  te28Ioctl(&te28Board[cPosition].Axis[iChannel], TE28_FILTER_KD, 2000);
  te28Ioctl(&te28Board[cPosition].Axis[iChannel], TE28_FILTER_IL, 0);
  te28Ioctl(&te28Board[cPosition].Axis[iChannel], TE28_FILTER_SI, 0);

  
  return & (te28Board[cPosition].Axis[iChannel]);
}


/**
 *   Gets a pointer to the device structure for the specified motor channel.
 *   Use this routine when the channel pointer cannot be specified.                  
 *
 *   The Industry Pack must be initialized with te28Init. All the channels  
 *   should be created before the use to avoid returning a NULL pointer.             
 *                                                                                   
 *   @param iPosition integer identifying the Industry Pack position on the       
 *   MVME162                                                                         
 *                                                                                   
 *   @param iChannel the motor channel on the Industry Pack           
 *                                                                                   
 *   @return a TE28_DEV pointer to the device structure for the registered        
 *   channel. NULL is returned if the input data was invalid or the channel is not   
 *   registered.                                                                     
 */
TE28_DEV * te28GetChannel(int iPosition, int iChannel) {
  return (te28Board[iPosition].Axis[iChannel].bRegistered == TRUE) ? 
           &(te28Board[iPosition].Axis[iChannel]) : NULL;
}


/**
 * Shows all information about all motor channels
 */
void te28Show(void) {
  int iPosition;
  int iChannel;

  for (iPosition = 0; iPosition < TE28_MAX_BOARD; iPosition ++) {
    if (te28Board[iPosition].bRegistered) {
      for (iChannel = 0; iChannel < TE28_MAX_AXES; iChannel ++) {
        if (te28Board[iPosition].Axis[iChannel].bRegistered) {
          te28ShowDevice(te28GetChannel(iPosition, iChannel));
        }
      }
    }
  }
}

/**
 * Shows all information about the requested motor channel
 *
 * @param teDevice a TE28_DEV pointer to the channel of interest
 */
void te28ShowDevice(TE28_DEV * teDevice) {
  printf("\nChannel %d Encoder %d Slope %lf Offset %lf\n",
	 teDevice->iChannel,
	 teDevice->lEncoderCounts,
	 teDevice->dSlope,
	 teDevice->dOffset);

  printf("\t  Position %lf Accel turns/s/s %lf Vel turns/s %lf\n",
	 ((double) te28ActPos(teDevice->iChannel) / 
	      (teDevice->dSlope * teDevice->lEncoderCounts) + teDevice->dOffset),
	 teDevice->dAcceleration,
	 teDevice->dVelocity);

  printf("\t  Position steps %d Dest steps %d Accel raw %d Vel raw %d\n",
	 te28ActPos(teDevice->iChannel),
	 te28DesPos(teDevice->iChannel),
	 teDevice->lAcceleration,
	 teDevice->lVelocity);

  printf("\t  Position error %sset\n", teDevice->bPositionError ? "" : "not ");

  printf("\t  KP %d KI %d KD %d IL %d Si %d\n",
	 teDevice->lFilterKP,
	 teDevice->lFilterKI,
	 teDevice->lFilterKD,
	 teDevice->lFilterIL,
	 teDevice->lFilterSI);
}
  

/**
 *   Used to access low level driver.                                        
 *                                                                                    
 * Valid functions:
 *
 * - TE28_RESET_AXIS
 * - TE28_ABS_POS 
 * - TE28_ABS_VEL 
 * - TE28_ABS_ACC 
 * - TE28_FILTER_KP
 * - TE28_FILTER_KI
 * - TE28_FILTER_KD
 * - TE28_FILTER_IL
 * - TE28_FILTER_SI
 * - TE28_START   
 * - TE28_STOP_MOVE
 * - TE28_ABORT_MOVE
 * - TE28_COAST   
 * - TE28_HOME      
 * - TE28_DONE
 *
 *   @param teDevice a TE28_DEV pointer to the channel of interest
 *   @param iFunction function identifier to perform                        
 *   @param lValue value to be passed to the function                  
 *                                                                                    
 *   @return indicates the result of the function. The value will depend on 
 *   the low level function called.                                                   
 */
STATUS te28Ioctl(TE28_DEV * teDevice, int iFunction, long lValue) {
  STATUS sResult;

  if (wLibDebug & DEBUG_IOCTL) printf("te28Ioctl ");

  if (teDevice->bRegistered == FALSE) {
    printf("The device is not registered. Check IP location.\n");
    return TE28_ERROR;
  }

  semTake(sidTe28Access, WAIT_FOREVER);

  switch (iFunction) {
    case TE28_RESET_AXIS:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_RESET_AXIS");
      sResult = te28ResetAxis(teDevice->iChannel);
      break;

    case TE28_ABS_POS:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_ABS_POS %ld\n", lValue);
      sResult = te28AbsPos(teDevice->iChannel, lValue);
      break;

    case TE28_ABS_VEL:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_ABS_VEL %ld", lValue);
      sResult = te28AbsVel(teDevice->iChannel, lValue);
      teDevice->lVelocity = lValue;
      break;

    case TE28_ABS_ACC:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_ABS_ACC %ld", lValue);
      sResult = te28AbsAcc(teDevice->iChannel, lValue);
      teDevice->lAcceleration = lValue;
      break;

    case TE28_FILTER_KP:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_FILTER_KP %ld", lValue);
      sResult = te28FilterKP(teDevice->iChannel, lValue);
      teDevice->lFilterKP = lValue;
      break;

    case TE28_FILTER_KI:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_FILTER_KI %ld", lValue);
      sResult = te28FilterKI(teDevice->iChannel, lValue);
      teDevice->lFilterKI = lValue;
      break;

    case TE28_FILTER_KD:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_FILTER_KD %ld", lValue);
      sResult = te28FilterKD(teDevice->iChannel, lValue);
      teDevice->lFilterKD = lValue;
      break;

    case TE28_FILTER_IL:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_FILTER_IL %ld", lValue);
      sResult = te28FilterIL(teDevice->iChannel, lValue);
      teDevice->lFilterIL = lValue;
      break;

    case TE28_FILTER_SI:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_FILTER_SI %ld", lValue);
      sResult = te28FilterSI(teDevice->iChannel, lValue);
      teDevice->lFilterSI = lValue;
      break;

    case TE28_START:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_START");
      sResult = te28Start(teDevice->iChannel);
      break;

    case TE28_STOP_MOVE:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_STOP_MOVE");
      sResult = te28StopMove(teDevice->iChannel);
      break;

    case TE28_ABORT_MOVE:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_ABORT_MOVE");
      sResult = te28AbortMove(teDevice->iChannel);
      break;

    case TE28_COAST:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_COAST");
      sResult = te28Coast(teDevice->iChannel);
      break;

    case TE28_HOME:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_HOME");
      sResult = te28Home(teDevice->iChannel);
      break;

    case TE28_DONE:
      if (wLibDebug & DEBUG_IOCTL) printf("TE28_DONE");
      sResult = (te28Complete(teDevice->iChannel) == TE28ACTIVE ? TE28_OK : TE28_MOVING);
      break;
      
    default:
      printf("te28Ioctl - Unknown command requested - %d %ld\n", iFunction, lValue);
      break;
  }

  semGive(sidTe28Access);

  if (sResult != TE28_OK) {
    printf("Ioctl result code %d from function %d\n", sResult, iFunction);
  } 
  
  if (wLibDebug & DEBUG_IOCTL) printf("\n");
  
  return sResult;
}


/**
 *   Used to set values and move motor.
 *
 * Valid functions:
 *
 * - TE28_SLOPE tells the number of motor turns per external device turn. Thus
 * it is the gearbox <i>reduction</i> ratio. See also TE28_ENCODER.<br>
 * Format is <b>te28Set(teDevice, TE28_SLOPE, doubleValue)</b>.<br>
 * - TE28_OFFSET sets the offset of the drive mechanism in device rotations.<br>
 * Format is <b>te28Set(teDevice, TE28_OFFSET, doubleValue)</b>.<br>
 * - TE28_POS_ERR defines the position error value that will stop the motor
 * if the load was to jam or lock up. This value is the delta between the present
 * raw position and the desired raw position during a move which creates the drive
 * voltage to the motor. It is highly dependant on the velocity. Typical values for 
 * slow movement would be about 300 counts. Valid range is from 0 to 32767, although a 
 * value of zero would be nonsensical. A value of -1 disables the function.<br>
 * Format is <b>te28Set(teDevice, TE28_POS_ERR, longValue)</b>.<br>
 * - TE28_POS_ERR_STAT sets the status of the channel position error bit. This is typically
 * called before a move to ensure the bit is cleared. The bit is set of the position error 
 * value is exceeded. The bit is cleared automatically when read, so a channel bit duplicates
 * the value. Value should be TRUE or FALSE.<br>
 * Format is <b>te28Set(teDevice, TE28_POS_ERR_STAT, longValue)</b>.<br>
 * - TE28_ENCODER sets the number of encoder counts for one turn of the motor. Typically
 * the encoder disk hole count times 4 for proper conversion. The product ENCODER times
 * SLOPE gives the number of encoder ticks per external device rotation.<br>
 * Format is <b>te28Set(teDevice, TE28_ENCODER, longValue)</b>.<br>
 * - TE28_LIMITS sets the polarity of the limit switch inputs to the controller.<br>
 * Format is <b>te28Set(teDevice, TE28_LIMITS, unsignedshortPositiveValue, unsignedshortNegativeValue)</b>.<br>
 * Polarity value is:
 *  - 1 for active high (normal style)
 *  - 0 for active low
 * - TE28_VELOCITY sets the velocity of the motor in device-turns per second.<br>
 * Format is <b>te28Set(teDevice, TE28_VELOCITY, doubleValue)</b>.<br>
 * - TE28_ACCELERATION sets the acceleration of the motor in device-turns/second/second.<br>
 * Format is <b>te28Set(teDevice, TE28_ACCELERATION, doubleValue)</b>.<br>
 * - TE28_PARAMETERS applies the previously set parameters for the PID filters, 
 * velocity and acceleration to the motor controller.
 * - TE28_MOVE starts a move of the motor to the destination.<br>
 * Format is <b>te28Set(teDevice, TE28_MOVE, doubleValue)</b>.<br>
 * - TE28_DEST      
 *
 * @param teDevice a TE28_DEV pointer from te28PhysDevCreate                       
 * @param iFunction function identifier. Further parameters follow depending
 *        on the function
 *                                                                                    
 * @return indicates the result of the function. The value will depend on 
 *        the function called.                                                             
 */
STATUS te28Set(TE28_DEV * teDevice, int iFunction, ...) {
  va_list argptr;
  double dTemp;
  long lTemp;

  if (wLibDebug & DEBUG_SET) printf("te28Set ");

  if (teDevice->bRegistered == FALSE) {
    printf("The device is not registered. Check IP location.\n");
    return TE28_ERROR;
  }

  va_start(argptr, iFunction);
  switch (iFunction) {
    case TE28_ENABLE:
      lTemp = va_arg(argptr, long);
      if (wLibDebug & DEBUG_SET) printf("TE28_ENABLE %ld", lTemp);
      if (lTemp) {
        te28MotorOn(teDevice->iChannel);
      } else {
        te28MotorOff(teDevice->iChannel);
      }             
      break;
    
    case TE28_SLOPE:
      dTemp = va_arg(argptr, double);
      if (wLibDebug & DEBUG_SET) printf("TE28_SLOPE %f", dTemp);
      teDevice->dSlope = dTemp;
			te28Set(teDevice, TE28_VELOCITY, teDevice->dVelocity);
			te28Set(teDevice, TE28_ACCELERATION, teDevice->dAcceleration);
      break;

    case TE28_OFFSET:
      dTemp = va_arg(argptr, double);
      if (wLibDebug & DEBUG_SET) printf("TE28_OFFSET %f", dTemp);
      teDevice->dOffset = dTemp;
      break;

    case TE28_ENCODER:
      lTemp = va_arg(argptr, long);
      if (wLibDebug & DEBUG_SET) printf("TE28_ENCODER %ld", lTemp);
      teDevice->lEncoderCounts = lTemp;
			te28Set(teDevice, TE28_VELOCITY, teDevice->dVelocity);
			te28Set(teDevice, TE28_ACCELERATION, teDevice->dAcceleration);
      break;

    case TE28_POS_ERR:
      lTemp = va_arg(argptr, long);
      if (wLibDebug & DEBUG_SET) printf("TE28_POS_ERR %ld", lTemp);
      if (lTemp == -1) {
        teDevice->lPositionError = 0;
        te28SErrI(teDevice->iChannel, lTemp);
      } else if ((lTemp > 0) && (lTemp <= 32767)) {
        teDevice->lPositionError = lTemp;
        te28SErrS(teDevice->iChannel, lTemp);
      }
      break;

    case TE28_POS_ERR_STAT:
      lTemp = va_arg(argptr, long);
      if (wLibDebug & DEBUG_SET) printf("TE28_POS_ERR_STAT %ld", lTemp);
      teDevice->bPositionError = lTemp;
      break;

    case TE28_LIMITS: {
      lTemp = va_arg(argptr, long);
      if (wLibDebug & DEBUG_SET) printf("TE28_LIMITS %ld", lTemp);
      te28LimitPolarity(teDevice->iChannel, (unsigned short) lTemp, (unsigned short) lTemp);
      break;
    }
    case TE28_LIMITS_ENABLE:
      lTemp = va_arg(argptr, long);
      if (wLibDebug & DEBUG_SET) printf("TE28_LIMITS_ENABLE %ld", lTemp);
      if (lTemp) {
        te28LimitControlEnable(teDevice->iChannel);
      } else {
        te28LimitControlDisable(teDevice->iChannel);
      }             
      break;
    
    case TE28_VELOCITY:
      teDevice->dVelocity = va_arg(argptr, double);
      if (wLibDebug & DEBUG_SET) printf("TE28_VELOCITY %f ", teDevice->dVelocity);

      /* turns per second *
         Encoder counts *
         265 uSec per sample time *
         scale factor of 65536 */

      dTemp = teDevice->dVelocity * fabs(teDevice->dSlope) * (double) teDevice->lEncoderCounts * 0.000256 * 65536.0;
/*      dTemp = (teDevice->dVelocity / teDevice->dSlope) * (double) teDevice->lEncoderCounts * 0.000256; */

			if (dTemp > 900000.0) {
				dTemp = 900000.0;
				teDevice->dVelocity = dTemp / (fabs(teDevice->dSlope) * (double) teDevice->lEncoderCounts * 0.000256 * 65536.0);
				printf("Excessive velocity. Clipped at %lf\n", teDevice->dVelocity);
			}
			printf("Vel %lf dTemp = %lf\n", teDevice->dVelocity, dTemp);

      lTemp = (long) dTemp;
      if (lTemp < 1) {
        lTemp = 1;
      }
      te28Ioctl(teDevice, TE28_ABS_VEL, lTemp);
      break;

    case TE28_ACCELERATION:
      teDevice->dAcceleration = va_arg(argptr, double);
      if (wLibDebug & DEBUG_SET) printf("TE28_ACCELERATION %f ", teDevice->dAcceleration);

      /* turns per second per second *
         Encoder counts *
         265 uSec sec per sample time *
         265 uSec sec per sample time *
         scale factor of 65536 */

      dTemp = (teDevice->dAcceleration * fabs(teDevice->dSlope)) * (double) teDevice->lEncoderCounts * 0.000256 * 0.000265 * 65536.0;
/*      dTemp = (teDevice->dAcceleration / teDevice->dSlope) * (double) teDevice->lEncoderCounts * 0.000256 * 0.000265; */

      printf("Accel %lf dTemp = %lf\n", teDevice->dAcceleration, dTemp);

      lTemp = (long) dTemp;
      if (lTemp < 1) {
        lTemp = 1;
      }
      te28Ioctl(teDevice, TE28_ABS_ACC, lTemp);
      break;

    case TE28_PARAMETERS:
      if (wLibDebug & DEBUG_SET) printf("TE28_PARAMETERS\n");
    
      te28Ioctl(teDevice, TE28_FILTER_KP, teDevice->lFilterKP);
      te28Ioctl(teDevice, TE28_FILTER_KI, teDevice->lFilterKI);
      te28Ioctl(teDevice, TE28_FILTER_KD, teDevice->lFilterKD);
      te28Ioctl(teDevice, TE28_FILTER_IL, teDevice->lFilterIL);
      te28Ioctl(teDevice, TE28_FILTER_SI, teDevice->lFilterSI);
      te28Ioctl(teDevice, TE28_ABS_VEL, teDevice->lVelocity);
      te28Ioctl(teDevice, TE28_ABS_ACC, teDevice->lAcceleration);
      break;

    case TE28_DEST: /* move to the specified destination with current setup */
      lTemp = va_arg(argptr, long);
      if (wLibDebug & DEBUG_SET) printf("TE28_DEST %ld", lTemp);
      te28Show();     
      te28Filter(teDevice->iChannel, teDevice->lFilterKP, teDevice->lFilterKI, teDevice->lFilterKD, teDevice->lFilterIL, teDevice->lFilterSI);
      te28Profile(teDevice->iChannel, lTemp, teDevice->lVelocity, teDevice->lAcceleration);
      te28Start(teDevice->iChannel);
      break;
            
    case TE28_MOVE:
      /*   Is the motor already moving? Look at      */
      /*   te28Move task for this motor              */

      if (teDevice->tidMove != 0) {
        printf("Motor already moving in te28Set TE28_MOVE\n");
        return TE28_ERROR;
      }

      teDevice->dDestination = va_arg(argptr, double);
      if (wLibDebug & DEBUG_SET) printf("TE28_MOVE %f ", teDevice->dDestination);

/*      te28ShowDevice(teDevice); */
      
      teDevice->lPosition = te28ActPos(teDevice->iChannel);
      teDevice->lDestination = (long) ((teDevice->dDestination - teDevice->dOffset) * 
				       teDevice->dSlope * (double) teDevice->lEncoderCounts);

/*      teDevice->lDestination = (long) ((teDevice->dDestination - teDevice->dOffset) / teDevice->dSlope); */

/*
      printf("Move motor %d to dDest %f    lPosit %d lDest %d dVel %lf\n", 
              teDevice->iChannel, teDevice->dDestination, 
              teDevice->lPosition, teDevice->lDestination,
              teDevice->dVelocity);
*/
    
      lTemp = (long) (60.0 + 
		          (teDevice->dVelocity / teDevice->dAcceleration) * 2 * 60.0 +
							60.0 * fabs((double) teDevice->lDestination - (double) teDevice->lPosition) / 
                   (teDevice->dVelocity * fabs(teDevice->dSlope) * (double) teDevice->lEncoderCounts));

      printf("Move will take %ld ticks\n", lTemp);

      te28AbsPos(teDevice->iChannel, teDevice->lDestination);
      
      te28Start(teDevice->iChannel);

/*      te28ShowDevice(teDevice); */
            
      teDevice->tidMove = taskSpawn("teMove", 100, VX_FP_TASK, 20000, (FUNCPTR) te28Move,
            (int) teDevice, lTemp, (teDevice->lDestination > teDevice->lPosition), 0, 0, 0, 0, 0, 0, 0);

      break;
      
    default:
      printf("te28Set - Unknown command requested - %d\n", iFunction);
      break;
  }

  if (wLibDebug & DEBUG_SET) printf("\n");

  return TE28_OK;
}


/**
 *   Used to get various parameters relating to a motor                     
 *
 * Valid functions:
 *
 * - TE28_POSITION
 * - TE28_DESTINATION
 * - TE28_DONE
 * - TE28_POS_ERR returns the status of the controller position error bit. Controller
 * bit is reset after this function.
 * - TE28_POS_ERR_STAT returns the status of the channel position error bit.
 * - TE28_LIMITS returns the limit byte value. Bit positions are:
 *  - 0 - negative limit active
 *  - 1 - positive limit active
 *  - 2 - limit interrupt active
 *                                                                                   
 *   @param teDevice a TE28_DEV pointer from te28PhysDevCreate                      
 *   @param iFunction function identifier. Further parameters follow depending
 *          on the function
 *                                                                                   
 *   @return the result of the function. The value will depend on
 *   the function called.                                                            
 */
STATUS te28Get(TE28_DEV * teDevice, int iFunction, ...) {
  va_list argptr;
  long lTemp;
  double * dValue;
  STATUS sResult = TE28_OK;

  if (wLibDebug & DEBUG_GET) printf("te28Get ");

  semTake(sidTe28Access, WAIT_FOREVER);

  va_start(argptr, iFunction);
  switch (iFunction) {
    case TE28_POSITION:
      dValue = va_arg(argptr, double *);
      lTemp = te28ActPos(teDevice->iChannel);
      *dValue = ((double) lTemp) / (teDevice->dSlope * (double) teDevice->lEncoderCounts) + teDevice->dOffset;
      if (wLibDebug & DEBUG_GET) printf("TE28_POSITION %ld %f ", lTemp, *dValue);
      break;
    
    case TE28_DESTINATION:
      dValue = va_arg(argptr, double *);
      lTemp = te28DesPos(teDevice->iChannel);
      *dValue = ((double) lTemp) / (teDevice->dSlope * (double) teDevice->lEncoderCounts) + teDevice->dOffset;
      if (wLibDebug & DEBUG_GET) printf("TE28_DESTINATION %ld %f ", lTemp, *dValue);
      break;
    
    case TE28_DONE:
      sResult = (te28Complete(teDevice->iChannel) == TE28ACTIVE) ? TE28_OK : TE28_MOVING;
/*          &&
                 (teDevice->tidMove == 0)) ? TE28_OK : TE28_MOVING;
*/      
/*      printf("complete %d move %d result %d ", te28Complete(teDevice->iChannel), teDevice->tidMove, sResult);*/
      break;
      
    case TE28_POS_ERR:
      sResult = (te28PosErr(teDevice->iChannel) == TE28INACTIVE) ? TE28_OK : TE28_ERROR;
      break;
      
    case TE28_POS_ERR_STAT:
      sResult = teDevice->bPositionError ? TE28_ERROR : TE28_OK;
      break;
      
    case TE28_LIMITS:
      sResult = te28LimitInRead(teDevice->iChannel);
      break;
      
    default:
      printf("te28Get - Unknown command requested - %d ", iFunction);
        
  }

  semGive(sidTe28Access);

  if (wLibDebug & DEBUG_GET) printf("\n");

  return sResult;
}


/**
 *   This task is used to monitor a move request and ensure
 *   it completes within a reasonable time. The task is spawned 
 * when a motor is moved. The task starts timing  
 *   the move and after the time expires, tests to see if the move  
 *   has finished. If it has not then it posts an error message to  
 *   the console. If the move completes before the task times out,  
 *   then it clears the task ID and kills itself.                   
 *
 *   If the move completes before timeout, the task ends. 
 *   If the move has not completed the motor will be stopped and an 
 *   error printed on the console.                                  
 *                                                                  
 * @param teDevice a TE28_DEV pointer from te28PhysDevCreate      
 * @param lTime number of tick the move should take
 * @param lDirection TRUE for positive direction
 */
void te28Move(TE28_DEV * teDevice, long lTime, long lDirection) {
  STATUS sResult;
  int iCount;
  int iDelay;
  long lPosition;
  long lTemp;

  if (wLibDebug & DEBUG_MOVE) printf("te28Move ");

  iDelay = (int) lTime;

  /*   Loop for the delay time, testing status   */
  /*   and delay count                           */

  te28Set(teDevice, TE28_POS_ERR_STAT, FALSE);
  
  lPosition = te28ActPos(teDevice->iChannel);
  taskDelay(60);
  iCount = 0;
  do {
    sResult = te28LimitInRead(teDevice->iChannel);
    if (((lDirection) && (sResult & 0x02)) ||
      ((!lDirection) && (sResult & 0x01))) {
      printf("Limit hit dir %d lim %d\n", lDirection, sResult);
      te28AbortMove(teDevice->iChannel);
      break;
    }
        
    lTemp = te28ActPos(teDevice->iChannel);
    if (lTemp == lPosition) {/* should be moving but isn't */
/*      sResult = te28Ioctl(teDevice, TE28_RESET_AXIS, 0L); */
      sResult = te28Set(teDevice, TE28_PARAMETERS);
      printf("\nTask te28Move stopping. Encoder value did not change.\n");
      break;
    }

    lPosition = lTemp;
    iCount += 10;
    taskDelay(10);
    sResult = te28Get(teDevice, TE28_DONE);
  } while ((iCount < iDelay) && (sResult == TE28_MOVING));

  /*   Did timeout occur? Print error, stop      */
  /*   motor and exit                            */

  if (iCount >= iDelay) {
    printf("\nTask te28Move timed out. Motor stopped\n");
    sResult = te28Ioctl(teDevice, TE28_ABORT_MOVE, 0L);
  } else {
    printf("\nMove took %d ticks\n", iCount);
	}	
  /* was it a position error? */
  
  if (te28Get(teDevice, TE28_POS_ERR) == TE28_ERROR) { /* clears error flag here! */
    te28Set(teDevice, TE28_POS_ERR_STAT, TRUE);
    printf("\nTask te28Move excessive position error. Motor stopped\n");
  }
    
  /*   Move completed, clear task ID and exit    */

  teDevice->tidMove = 0;
  if (wLibDebug != 0x0) printf("\n");
}


/**
 *   This interrupt task handles any situation where the    
 *   TE28 generates an interrupt. This can include limit switch, GP 
 *   input, or motor board getting an error.                        
 */
void te28Int() {
  return;
}

/**
 * This task provides a bounce routine to exercise the motor, encoder and mechanism
 *
 * @param iDevice a TE28_DEV pointer to the device of interest
 * @param iVal1 a float cast as an integer for the first destination
 * @param iVal2 a float cast as an integer for the second destination
 */
void taskBounce(int iDevice, int iVal1, int iVal2) {
  char buff[32];
  double dTemp;
  float fVal1 = (float) iVal1;
  float fVal2 = (float) iVal2;
  int forward = 1;
  TE28_DEV * pDevice = (TE28_DEV *) iDevice;

  printf("Bouncing from %f to %f\n", fVal1, fVal2);
  
  while (bBounce) {
    printf("\nBouncing to %f ", (forward ? fVal2 : fVal1));
    te28Set(pDevice, TE28_MOVE, (double) (forward ? fVal2 : fVal1));
    do {
      taskDelay(60);
      te28Get(pDevice, TE28_POSITION, &dTemp);
      printf("position %f ", dTemp);
    } while (fabs(dTemp - (forward ? fVal2 : fVal1)) > 0.05);
    te28Ioctl(pDevice, TE28_STOP_MOVE, 0);
    taskDelay(120);

    forward = 1 - forward;
  }
}
    
/**
 * This is the console based user interface tool
 */
int te28(char *cmd) {
  char line[133];
  int nc, interactive;
  char *pc;
  double dTemp;
  double dTemp2;
  int iTemp;
  long lTemp;
  static TE28_DEV * pDevice = NULL;
  static char IPpos = 'A';
  static int chan = 0;

  interactive = (strlen(cmd) == 0);
  
  *line = '\0';
  while (*line != '*') {
    if (interactive) {
      printf("\nte28 Motor System Manual Control IP %c ch %d >", IPpos, chan);
      gets(line);
    } else {
      strcpy(line, cmd);
    }

    if (strlen(line)) {
      pc = line;
      while (*pc) {
        switch (*pc) {
          case '*':
            return 1;
            
          case 'i': {
            char c = toupper(*(pc + 1));
            if ((c >= 'A') && (c <= 'D')) {
              IPpos = c;
            }
            break;
          }
          case 'c':
            if ((*(pc + 1) == '0') || (*(pc + 1) == '1')) {
              chan = (*(pc + 1)) - '0';
            }
            break;
          
          case 'd':
            te28Show();
            break;
            
          case 'a':
            nc = sscanf(pc+1, "%lf", &dTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Set(pDevice, TE28_ACCELERATION, dTemp);
            }
            break;
          
          case 'v':
            nc = sscanf(pc+1, "%lf", &dTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Set(pDevice, TE28_VELOCITY, dTemp);
            }
            break;
          
          case 'e':
            nc = sscanf(pc+1, "%ld", &lTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Set(pDevice, TE28_ENCODER, lTemp);
            }
            break;
          
          case 'q':
            nc = sscanf(pc+1, "%lf", &dTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Set(pDevice, TE28_SLOPE, dTemp);
            }
            break;
          
          case 'o':
            nc = sscanf(pc+1, "%lf", &dTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Set(pDevice, TE28_OFFSET, dTemp);
            }
            break;
          
          case 'm':
            nc = sscanf(pc+1, "%lf", &dTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              if (te28Get(pDevice, TE28_DONE) == TE28_OK) {
                te28Ioctl(pDevice, TE28_ABORT_MOVE, 0);
              }
              te28Set(pDevice, TE28_MOVE, dTemp);
            }
            break;
          
          case 'j':
            nc = sscanf(pc+1, "%ld", &lTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Set(pDevice, TE28_DEST, lTemp);
            }
            break;
          
          case 's':
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Ioctl(pDevice, TE28_STOP_MOVE, 0);
            break;
            
          case 'k':
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Ioctl(pDevice, TE28_ABORT_MOVE, 0);
            break;
            
          case 'h':
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Ioctl(pDevice, TE28_HOME, 0);
            break;
            
          case 'r':
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Ioctl(pDevice, TE28_RESET_AXIS, 0);
            break;
            
          case 'p':
            nc = sscanf(pc+1, "%lf", &dTemp);
            if (nc == 1) {
							
						} else {
	            pDevice = te28GetChannel(IPpos - 'A', chan);
  	          te28Get(pDevice, TE28_POSITION, &dTemp);
						}
    	      printf("Position = %f\n", dTemp);
            break;
            
          case 'x':
            nc = sscanf(pc+1, "%ld", &lTemp);
            if (nc == 1) {
              pDevice = te28GetChannel(IPpos - 'A', chan);
              te28Set(pDevice, TE28_POS_ERR, lTemp);
            }
            break;

          case 'b':
            bBounce = !bBounce;
            if (bBounce) {
              float fTemp, fTemp2;
              nc = sscanf(pc+1, "%lf %lf", &dTemp, &dTemp2);
              fTemp = (float) dTemp;
              fTemp2 = (float) dTemp2;
              printf("Bounce %f %f\n", fTemp, fTemp2);
              taskSpawn("tBounc", 100, VX_FP_TASK, 20000, (FUNCPTR) taskBounce,
                  (int) pDevice, (int) fTemp, (int) fTemp2, 0, 0, 0, 0, 0, 0, 0);
            }
            break;
                        
          case 'l':
            nc = sscanf(pc+1, "%ld", &lTemp);
            pDevice = te28GetChannel(IPpos - 'A', chan);
            if (nc == 1) {
              te28Set(pDevice, TE28_LIMITS, lTemp, lTemp);
              te28Set(pDevice, TE28_LIMITS_ENABLE, 1);
            } else {
              STATUS result = te28Get(pDevice, TE28_LIMITS);
              printf("Limit info %d\n", result & 0x3);
            }
            break;
          
          case 'n':
            nc = sscanf(pc+1, "%ld", &lTemp);
            pDevice = te28GetChannel(IPpos - 'A', chan);
            if (nc == 1) {
              te28Set(pDevice, TE28_ENABLE, lTemp);
            }
            break;
          
          default:
            /** \ref userinterface  */
              
            printf("\nte28 Motor System interactive tool commands\n");
            printf("i<d> - use IP position (A-D)\n");
            printf("c<d> - use channel (0,1)\n");
            printf("d - list info about devices\n");
            printf("an - set acceleration to n\n");
            printf("vn - set velocity to n\n");
            printf("en - set encoder count to n\n");
            printf("qn - set slope to n\n");
	    printf("on - set offset to n\n");
            printf("xn - set position error to n\n");
            printf("mn - move device absolute turns n\n");
            printf("jn - jog to encoder count n\n");
            printf("l(n) - read/(set polarity) limits\n");
            printf("s - stop device\n");
            printf("k - kill motion immediately\n");
            printf("nn - set motor enable to n\n");
            printf("r - reset device\n");
            printf("h - home or set to zero position\n");
            printf("p - report position of device\n");
            printf("bm n - bounce from m to n\n");
            printf("* - quit\n");
            break;
        }
        *pc = 0x0;
      }
    }
    if (!interactive) {
      break;
    }
  }
  return(0);
}

/*
 * Test routine
 */
static te28Test() {
  
  
}
