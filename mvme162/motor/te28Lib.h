/*
 *   $Id: te28Lib.h,v 1.3 2006/09/29 14:15:36 asnd Exp $
 * 
 *   $Revision: 1.3 $
 *                                                                                     
 *   Purpose: This library provides an interface from VxWorks to the	
 *   Technology-80 Model 28 2 channel servo motor Industry Pack				
 *   module.																													
 *   																																	
 *   Version 1.1 2000-Sept dbm Added recovery from broken encoder			
 *   problem																													
 *
 *	$Log: te28Lib.h,v $
 *	Revision 1.3  2006/09/29 14:15:36  asnd
 *	Move "new" te28 libs to original motor dir, so into CVS
 *	
 *	Revision 1.2  2000/12/22 20:57:28  David.Morris
 *	Added header info to record CVS logs
 *	
 *
 */

#ifndef TE28_H
#define TE28_H

#define TE28LIB_VERSION "Version 2.0"

#define TE28_MAX_BOARD 4
#define TE28_MAX_AXES 2

#define MVME162_IPBaseAddress 0xfff58000
#define MVME162_IPIC_ADDRESS  0xfffbc000

#define ENCODER_COUNTS 2000

#define TE28_OK								0
#define TE28_RESET_AXIS				1
#define TE28_ABS_VEL					2
#define TE28_ABS_ACC					3
#define TE28_FILTER_KP				4
#define TE28_FILTER_KI				5
#define TE28_FILTER_KD				6
#define TE28_FILTER_IL				7
#define TE28_FILTER_SI				8
#define TE28_STOP_MOVE				9
#define TE28_ABORT_MOVE				10
#define TE28_SLOPE						11
#define TE28_OFFSET						12
#define TE28_MOVE							13	
#define TE28_POSITION					14
#define TE28_DONE							15
#define TE28_VELOCITY					16
#define TE28_ACCELERATION			17
#define TE28_ENCODER					18
#define TE28_PARAMETERS				19
#define TE28_MOVING						20
#define TE28_DEST 						21
#define TE28_DESTINATION			22
#define TE28_POS_ERR					23
#define TE28_POS_ERR_STAT			24
#define TE28_LIMITS						25
#define TE28_LIMITS_ENABLE		26
#define TE28_ENABLE						27
#define TE28_ERROR						28
#define TE28_ABS_POS					29
#define TE28_START						30
#define TE28_COAST						31
#define TE28_HOME							32


typedef struct tagTE28_DEV {
  char iChannel;
  char bRegistered;

	long lEncoderCounts;

  double dDestination;
  double dSlope;
  double dOffset;

  double dVelocity;
  double dAcceleration;

	long lVelocity;
	long lAcceleration;
	long lFilterKP;
	long lFilterKI;
	long lFilterKD;
	long lFilterIL;
	long lFilterSI;

  long lPosition;
  long lDestination;

	long lPositionError;
	long bPositionError;
	
  int tidMove;
} TE28_DEV;

typedef struct tagTE28_BOARD
{
  char bRegistered;

  struct tagTE28_DEV Axis[TE28_MAX_AXES];
} TE28_BOARD;

#endif

STATUS te28Init(char cPosition, int iVector);
TE28_DEV * te28PhysDevCreate(char cPosition, int iChannel);
TE28_DEV * te28GetChannel(int iPosition, int iChannel);
void te28Show(void);
void te28ShowChannel(int iPosition, int iChannel);
void te28ShowDevice(TE28_DEV * teDevice);
STATUS te28Ioctl(TE28_DEV * teDevice, int iFunction, long lArgument);
STATUS te28Set(TE28_DEV * teDevice, int iFunction, ...);
STATUS te28Get(TE28_DEV * teDevice, int iFunction, ...);
void te28Move(TE28_DEV * teDevice, long lTime, long lDirection);
void te28Int();

