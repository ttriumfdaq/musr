/****
*  This source code is of proprietary interest to Technology 80.  It is
*  supplied in confidence and the recipient, by accepting this material,
*  agrees that none of it shall be copied or reproduced, in whole or in part,
*  nor contents revealed in any manner or to any person except to meet the
*  purpose for which it was delivered.
****/

/****
*  NAME:         te28_1.C
*  COMPANY:      TECHNOLOGY 80 INC
*  REVISION:     5.02
*  LAST DATE:    11/15/94
*  DESCRIPTION:
*    SOURCE CODE FOR MODEL 28 DRIVER ROUTINES
****/

/*--------------------------------------------------------------------*/
/*
 *   $Id: te28_1.c,v 1.3 2006/09/29 14:15:36 asnd Exp $
 * 
 *   $Revision: 1.3 $
 *                                                                                     
 *   Revisions for VxWorks
 *
 *   Mar 24 1998 dbm Version 5.03
 * 
 *   - Added VXWORKS constant
 *
 *   - Added te28Debug, te28Display
 * 
 *   - Added error codes TE28BOARD_ERR, TE28AXIS_NOT_RESPOND,         
 *   TE28AXIS_OK to get rid of magic numbers
 *
 *  $Log: te28_1.c,v $
 *  Revision 1.3  2006/09/29 14:15:36  asnd
 *  Move "new" te28 libs to original motor dir, so into CVS
 *
 *  Revision 1.2  2000/12/22 20:57:28  David.Morris
 *  Added header info to record CVS logs
 *
 *
 */

#define TE28_VERSION 0x0503

/*******************************************************************
* One of the following four constants must be defined on the       *
* command line:                                                    *
*    DOS -   For applications running in an DOS (not Windows)      *
*            environment.                                          *
*    WIN   - For applications running the the Windows environment  *
*            (makes a DLL).                                        *
*                                                                  *
*    MOT68 - For applications running with a Motorola 680X0        *
*            processor (memory-mapped I/O, all pointers are        *
*            32 bits).                                             *
*                                                                  *
*    VXWORKS                                                       *
*          - For applications running under vxWorks RTOS           *
*            MOT68 defined automatically                           *
*                                                                  *
* If none of the four are defined, DOS is assumed.                 *
*                                                                  *
********************************************************************/

#ifdef __TURBOC__               /* defined by Borland C / Turbo C */
#include <dos.h>                /* for Turbo C, makes inp & outp intrinsic */
#endif /* __TURBOC__ */

#ifdef _MSC_VER                 /* defined by Microsoft C */
#include <conio.h>              /* for Microsoft C, inp & outp prototypes */
#pragma intrinsic(inp, outp)    /* For Microsoft C */
#endif /* _MSC_VER */

#ifndef MOT68
 #ifndef DOS
  #ifndef WIN
   #ifndef VXWORKS
    #define DOS
   #endif /* VXWORKS */
  #endif /* WIN */
 #endif /* DOS */
#endif /* MOT68 */

/**************************************/
/* WINDOWS:                           */
/**************************************/

#ifdef WIN
  #include <stdio.h>          /* for definition of NULL */
  #define STRICT
  typedef unsigned short ADDR;
  #include <windows.h>
  #include "te28w.h"
  #define InPort(x)           inp((x))
  #define OutPort(x, y)       (void)outp((x), (y))
  #define IntDisable()        _asm cli
  #define SaveFlags()         _asm pushf
  /* in protected mode, popf doesn't necessary affect interrupt flag */
  #define RestoreFlags() \
    _asm pop ax;         \
    if (_AX & 0x0200)    \
      _asm sti;
  /* register offsets from base address */
  #define OFFSET_FIRST_AXIS   0
  #define OFFSET_SECOND_AXIS  8
  #define OFFSET_VECTOR       0x10
#endif /* WIN */

/**************************************/
/* DOS:                               */
/**************************************/

#ifdef DOS
  typedef unsigned char BYTE;
  typedef unsigned short WORD;
  typedef unsigned long DWORD;
  typedef unsigned short ADDR;
  #include "te28.h"
  #define InPort(x)           inp((x))
  #define OutPort(x, y)       (void)outp((x), (y))
  #define IntDisable()        _asm cli
  #define SaveFlags()         _asm pushf
  #define RestoreFlags()      _asm popf
  /* register offsets from base address */
  #define OFFSET_FIRST_AXIS   0
  #define OFFSET_SECOND_AXIS  8
  #define OFFSET_VECTOR       0x10
#endif /* DOS */

/**************************************/
/* VXWORKS:                             */
/**************************************/

#ifdef VXWORKS
  #define MOT68
  #include "vxWorks.h"
  #include "stdio.h"
#endif /* VXWORKS */

/**************************************/
/* MOT68:                             */
/**************************************/

#ifdef MOT68
  #define far
  typedef unsigned char BYTE;
  typedef unsigned short WORD;
  typedef unsigned long DWORD;
  typedef BYTE *ADDR;
  #include "te28v.h"
  #define InPort(x)           (*(x))
  #define OutPort(x, y)       (*(x) = ((BYTE)(y)))
  #define IntDisable()
  #define SaveFlags()
  #define RestoreFlags()
  /* offsets from base pack address */
  #define OFFSET_FIRST_AXIS   1
  #define OFFSET_SECOND_AXIS  9
  #define OFFSET_VECTOR       0x11
#endif /* MOT68 */


/* offsets from base axis address */
#define LM628_CMD  0
#define LM628_DTA  2
#define CFG_ADR    4
#define CFG_DTA    6


/** LM628 status masks **/
#define BUSYMASK  0x01

typedef struct{
  ADDR aAddr;
  BYTE byIntMask;              /* LM628's interrupt mask */
  BYTE bySample;               /* LM628's filter sampling interval */
  WORD far *lpwIDProm;         /* ID PROM data */
} AXIS28_TYPE;

/* Global constants for driver */

static void ResetConfig(WORD wAxisNum);
static short NotBusy(WORD wAxisNum);
static AXIS28_TYPE aAxis28[MAX_AXES];
static WORD wTotalAxes = 0, wTotalPacks = 0;
static WORD awAxisNum[MAX_BOARDS][INTRPTS_PER_BOARD];
static WORD wDebug = 0;

/**************************************
  Undocumented DEBUG function
***************************************/

#define DEBUG_INIT    0x001
#define DEBUG_LOW     0x002
#define DEBUG_MOTION  0x004
#define DEBUG_FILTER  0x008
#define DEBUG_SERVO   0x010
#define DEBUG_TIME    0x020
#define DEBUG_ENCODER 0x040
#define DEBUG_LIMIT   0x080
#define DEBUG_INTS    0x100
#define DEBUG_OUTPUT  0x200
#define DEBUG_GENPURP 0x400
#define DEBUG_CONFIG  0x800

void te28Debug(short sValue)
{
  wDebug = sValue;
}

/**************************************/
/* Beginning of Code:                 */
/**************************************/
#ifdef WIN
static HTASK htaskInit = NULL;
int FAR PASCAL LibMain(HINSTANCE hInstance, WORD wDataSeg,
  WORD cbHeapSize, LPSTR lpCmdLine)
{
  WORD wAxisNum;
  htaskInit = NULL;
  wTotalAxes = 0;
  wTotalPacks = 0;
  return(1);
}

int CALLBACK WEP(int nParameter)
{
  return(1);
}
#endif /* WIN */


/**********************************************************************
  te28InitSw                                                          *
**********************************************************************/
short te28InitSw(void)
{
  if (wDebug & DEBUG_INIT)
    printf("te28InitSw ");

#ifdef WIN
  /* allow this routine to be called from only one task */
  if (htaskInit == NULL)
    /* record the initializing task */
    htaskInit = GetCurrentTask();
  else{
    if (htaskInit != GetCurrentTask())
      return(-2);
  }
#endif /* WIN */

  wTotalAxes = 0;
  wTotalPacks = 0;

  return(TE28_VERSION);
}

/**********************************************************************
  te28InitPack                                                        *
**********************************************************************/
/*--------------------------------------------------------------------*/
/*   Purpose: Initializes the specified Model 28 motor controller     */
/*                                                                    */
/*   Inputs: wBoard - board number - 0 to 7                           */
/*                                                                    */
/*   wSlot - IP module number on board - 0 to 3                       */
/*                                                                    */
/*   aPackAddr - Base address of IP module                            */
/*                                                                    */
/*   wVector - Interrupt vector supplied by IP                        */
/*                                                                    */
/*   lpwIDProm - pointer for ID Prom data                             */
/*                                                                    */
/*   Precond: VxWorks is booted. IP addrss space is Non-Cached.       */
/*   Check sysLib.c for memory space configuration.                   */
/*                                                                    */
/*   Outputs: short - the number of axes found for the specified      */
/*   pack. Should be 2. If axis not responding then                   */
/*   TE28AXIS_NOT_RESPOND. If invalid board, module, or axis number   */
/*   then TE28BOARD_ERR.                                              */
/*                                                                    */
/*   Postcond:All address and default values are assigned. Motor      */
/*   axes are in quiescent state.                                     */

/****
  Assuming te28RegisterDriver is called before teInitHw, te28InitPack
  will be called from teInitHw once for every model 28 pack on the
  carrier board.
****/

short te28InitPack(WORD wBoard, WORD wSlot, ADDR aPackAddr, WORD wVector, WORD far *lpwIDProm)
{
  WORD wAxisNum, wIntrptNum;
  short sRetCode;

  if (wDebug & DEBUG_INIT)
    printf("te28InitPack ");

#ifdef WIN
  /* allow only the task that called InitSw to use this routine */
  if (htaskInit != GetCurrentTask())
    return(-2);
#endif /* WIN */

  if ((wTotalPacks >= MAX_PACKS) || (wSlot >= PACKS_PER_BOARD) ||
    (wBoard >= MAX_BOARDS))
    return(TE28BOARD_ERR);

  sRetCode = AXES_PER_PACK;
  wAxisNum = wTotalAxes;
  wTotalAxes++;
  wIntrptNum = INTRPTS_PER_PACK * wSlot;

  /*   fill struct members with IP specific      */
  /*   data for first axis wAxisNum incremented  */
  /*   as each IP is added to system             */

  aAxis28[wAxisNum].byIntMask = 0;
  aAxis28[wAxisNum].bySample = 0;
  awAxisNum[wBoard][wIntrptNum] = wAxisNum;
  aAxis28[wAxisNum].aAddr= aPackAddr + OFFSET_FIRST_AXIS;
  aAxis28[wAxisNum].lpwIDProm = lpwIDProm;

  ResetConfig(wAxisNum);

  if (te28ResetAxis(wAxisNum))
    sRetCode = 0;
  te28AxisIntEnable(wAxisNum);

  /* Point at next axis */

  wAxisNum++;
  wTotalAxes++;
  wIntrptNum++;

  /*   fill struct members with IP specific      */
  /*   data for second axis wAxisNum             */
  /*   incremented as each IP is added to        */
  /*   system                                    */

  aAxis28[wAxisNum].byIntMask = 0;
  aAxis28[wAxisNum].bySample = 0;
  awAxisNum[wBoard][wIntrptNum] = wAxisNum;
  aAxis28[wAxisNum].aAddr = aPackAddr + OFFSET_SECOND_AXIS;
  aAxis28[wAxisNum].lpwIDProm = lpwIDProm;
  ResetConfig(wAxisNum);
  if (te28ResetAxis(wAxisNum))
    sRetCode = 0;
  te28AxisIntEnable(wAxisNum);

  /*   Write interrupt vector into register      */

  OutPort(aPackAddr + OFFSET_VECTOR, wVector);

  if (sRetCode < AXES_PER_PACK)
    wTotalAxes -= AXES_PER_PACK;
  else
    wTotalPacks++;
  return(sRetCode);
}

/***************************************************
  te28Display                                      *
***************************************************/
/*--------------------------------------------------------------------*/
/*   Purpose: Displays all motor axis information                     */
/*                                                                    */
/*   Inputs: None                                                     */
/*                                                                    */
/*   Precond: None                                                    */
/*                                                                    */
/*   Outputs: None                                                    */
/*                                                                    */
/*   Postcond: All details of each motor axis are displayed           */

void te28Display(void)
{
  WORD wAxisNum;
  WORD wRegNum;

  printf("te28Display - Axis information\n\n");

  for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum ++)
  {
    printf("Axis = %d Base Address = %p ID Address = %p\n", 
     wAxisNum, aAxis28[wAxisNum].aAddr, aAxis28[wAxisNum].lpwIDProm);
    printf("Limit info = %x\n", te28LimitInRead(wAxisNum));
    printf("Motor enable = %d\n", te28MotorRead(wAxisNum));
    printf("Actual position = %d\n", te28ActPos(wAxisNum));
    printf("Actual velocity = %d\n", te28ActVel(wAxisNum));
    for (wRegNum = 0; wRegNum < 16; wRegNum ++)
      printf("Register %d = %02x\n", wRegNum, te28RegRead(wAxisNum, wRegNum));
    printf("\n");
  }
}

/**********************************************************************
  te28IntrStart                                                       *
**********************************************************************/
short te28IntrStart(WORD wBoard, WORD wIntrptNum,
  WORD *pwPackNum, WORD *pwUnitNum, WORD *pwIntrType)
{
  WORD wAxisNum;
  short sInts;

  if (wDebug & DEBUG_INTS)
    printf("te28IntrStart ");

  wAxisNum = awAxisNum[wBoard][wIntrptNum];
  if (wAxisNum >= wTotalAxes)
    return(-1);
  sInts = te28RegRead(wAxisNum, 6) & te28RegRead(wAxisNum, 0);
  if (sInts & 8)
    *pwIntrType = TE28INTRPT_GPIN;
  else if (sInts & 4)
    *pwIntrType = TE28INTRPT_LIMIT;
  else if (sInts & 2)
    *pwIntrType = TE28INTRPT_AXIS;
  else
    *pwIntrType = 0;
  *pwUnitNum = wAxisNum;
  *pwPackNum = (wAxisNum >> 1);
  return(0);
}

/**********************************************************************
  te28IntrEnd                                                         *
**********************************************************************/
short te28IntrEnd()
{
  if (wDebug & DEBUG_INTS)
    printf("te28IntrEnd ");

  return(0);
}

/**********************************************************************
  ResetConfig                                                         *
**********************************************************************/
void ResetConfig(WORD wAxisNum)
{
  if (wDebug & DEBUG_CONFIG)
    printf("ResetConfig ");

  te28RegBitClr(wAxisNum, 0, 0);
  te28AxisIntDisable(wAxisNum);
  te28LimitIntDisable(wAxisNum);
  te28GPIntDisable(wAxisNum);
  te28EncNoSwap(wAxisNum);
  te28EncPolarity(wAxisNum, 0, 0, 0);
  te28MotorOff(wAxisNum);
  te28PWMPolarity(wAxisNum, 0, 0);
  te28LimitPolarity(wAxisNum, 0, 0);
  te28LimitControlDisable(wAxisNum);
  te28IPErrEnable(wAxisNum);
  te28GPIntPol(wAxisNum, 0);
  te28EncIndexDisable(wAxisNum);
  te28DacRelease(wAxisNum);
}

/**********************************************************************
  NotBusy                                                             *
**********************************************************************/
short NotBusy(WORD wAxisNum)
{
  WORD wLoopCount;
  ADDR aAxisAddr;

  if (wDebug & DEBUG_CONFIG)
    printf("NotBusy ");

  aAxisAddr = aAxis28[wAxisNum].aAddr + LM628_CMD;
  for (wLoopCount = 0xFFFF; wLoopCount; wLoopCount--){
    if ((InPort(aAxisAddr) & BUSYMASK) == 0)
      return(0);
  }
  return(-1);
}

/**********************************************************************
  te28WrServo                                                         *
**********************************************************************/
short te28WrServo(WORD wAxisNum, short sCommand, short sDataWords,
  WORD *pwData)
{
  ADDR aAxisAddr;

  if (wDebug & DEBUG_LOW)
    printf("te28WrServo ");

  if (wAxisNum == TE28ALL_AXES){
    SaveFlags();
    IntDisable();
    if ((char)sCommand != -1){
      for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
        aAxisAddr = aAxis28[wAxisNum].aAddr;
        aAxisAddr += LM628_CMD;
        if (NotBusy(wAxisNum)){
          RestoreFlags();
          return(TE28AXIS_TIMEOUT);
        }
        OutPort(aAxisAddr, sCommand);
      }
    }
    pwData += sDataWords;
    while(sDataWords){
      pwData--;
      for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
        aAxisAddr = aAxis28[wAxisNum].aAddr;
        aAxisAddr += LM628_DTA;
        if (NotBusy(wAxisNum)){
          RestoreFlags();
          return(TE28AXIS_TIMEOUT);
        }
        OutPort(aAxisAddr, *pwData >> 8);
        OutPort(aAxisAddr, *pwData & 0xFF);
      }
      sDataWords--;
    }
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    aAxisAddr = aAxis28[wAxisNum].aAddr;
    aAxisAddr += LM628_CMD;
    SaveFlags();
    IntDisable();
    if ((char)sCommand != -1){
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, sCommand);
    }
    aAxisAddr += (LM628_DTA - LM628_CMD);
    pwData += sDataWords;
    while(sDataWords){
      pwData--;
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, *pwData >> 8);
      OutPort(aAxisAddr, *pwData & 0xFF);
      sDataWords--;
    }
  }
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28WrCmd                                                           *
**********************************************************************/
short te28WrCmd(WORD wAxisNum, short sCommand)
{
  ADDR aAxisAddr;

  if (wDebug & DEBUG_LOW)
    printf("te28WrCmd ");

  if ((char)sCommand == -1)
    return(0);
  if (wAxisNum == TE28ALL_AXES){
    SaveFlags();
    IntDisable();
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
      aAxisAddr = aAxis28[wAxisNum].aAddr;
      aAxisAddr += LM628_CMD;
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, sCommand);
    }
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    aAxisAddr = aAxis28[wAxisNum].aAddr;
    aAxisAddr += LM628_CMD;
    SaveFlags();
    IntDisable();
    if (NotBusy(wAxisNum)){
      RestoreFlags();
      return(TE28AXIS_TIMEOUT);
    }
    OutPort(aAxisAddr, sCommand);
  }
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28WrWord                                                          *
**********************************************************************/
short te28WrWord(WORD wAxisNum, short sCommand, WORD wData)
{
  ADDR aAxisAddr;

  if (wDebug & DEBUG_LOW)
    printf("te28WrWord ");

  if (wAxisNum == TE28ALL_AXES){
    SaveFlags();
    IntDisable();
    if ((char)sCommand != -1){
      for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
        aAxisAddr = aAxis28[wAxisNum].aAddr;
        aAxisAddr += LM628_CMD;
        if (NotBusy(wAxisNum)){
          RestoreFlags();
          return(TE28AXIS_TIMEOUT);
        }
        OutPort(aAxisAddr, sCommand);
      }
    }
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
      aAxisAddr = aAxis28[wAxisNum].aAddr;
      aAxisAddr += LM628_DTA;
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, wData >> 8);
      OutPort(aAxisAddr, wData & 0xFF);
    }
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    aAxisAddr = aAxis28[wAxisNum].aAddr;
    aAxisAddr += LM628_CMD;
    SaveFlags();
    IntDisable();
    if ((char)sCommand != -1){
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, sCommand);
    }
    aAxisAddr += (LM628_DTA - LM628_CMD);
    if (NotBusy(wAxisNum)){
      RestoreFlags();
      return(TE28AXIS_TIMEOUT);
    }
    OutPort(aAxisAddr, wData >> 8);
    OutPort(aAxisAddr, wData & 0xFF);
  }
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28WrLong                                                          *
**********************************************************************/
short te28WrLong(WORD wAxisNum, short sCommand, long lData)
{
  WORD wHiWord, wLoWord;
  ADDR aAxisAddr;

  if (wDebug & DEBUG_LOW)
    printf("te28WrLong ");

  wHiWord = (WORD)(lData >> 16);
  wLoWord = (WORD)(lData & 0xFFFF);
  if (wAxisNum == TE28ALL_AXES){
    SaveFlags();
    IntDisable();
    if ((char)sCommand != -1){
      for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
        aAxisAddr = aAxis28[wAxisNum].aAddr;
        aAxisAddr += LM628_CMD;
        if (NotBusy(wAxisNum)){
          RestoreFlags();
          return(TE28AXIS_TIMEOUT);
        }
        OutPort(aAxisAddr, sCommand);
      }
    }
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
      aAxisAddr = aAxis28[wAxisNum].aAddr;
      aAxisAddr += LM628_DTA;
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, (BYTE)(wHiWord >> 8));
      OutPort(aAxisAddr, (BYTE)(wHiWord & 0xFF));
    }
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++){
      aAxisAddr = aAxis28[wAxisNum].aAddr;
      aAxisAddr += LM628_DTA;
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, (BYTE)(wLoWord >> 8));
      OutPort(aAxisAddr, (BYTE)(wLoWord & 0xFF));
    }
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    aAxisAddr = aAxis28[wAxisNum].aAddr;
    aAxisAddr += LM628_CMD;
    SaveFlags();
    IntDisable();
    if ((char)sCommand != -1){
      if (NotBusy(wAxisNum)){
        RestoreFlags();
        return(TE28AXIS_TIMEOUT);
      }
      OutPort(aAxisAddr, sCommand);
    }
    aAxisAddr += (LM628_DTA - LM628_CMD);
    if (NotBusy(wAxisNum)){
      RestoreFlags();
      return(TE28AXIS_TIMEOUT);
    }
    OutPort(aAxisAddr, (BYTE)(wHiWord >> 8));
    OutPort(aAxisAddr, (BYTE)(wHiWord & 0xFF));
    if (NotBusy(wAxisNum)){
      RestoreFlags();
      return(TE28AXIS_TIMEOUT);
    }
    OutPort(aAxisAddr, (BYTE)(wLoWord >> 8));
    OutPort(aAxisAddr, (BYTE)(wLoWord & 0xFF));
  }
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28RdWord                                                          *
**********************************************************************/
short te28RdWord(WORD wAxisNum, short sCommand, WORD *pwValue)
{
  ADDR aAxisAddr;

  if (wDebug & DEBUG_LOW)
    printf("te28RdWord ");

  *pwValue = 0x8000;    /* if error, max negative value */
  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  aAxisAddr = aAxis28[wAxisNum].aAddr;
  aAxisAddr += LM628_CMD;
  SaveFlags();
  IntDisable();
  if ((char)sCommand != -1){
    if (NotBusy(wAxisNum)){
      RestoreFlags();
      return(TE28AXIS_TIMEOUT);
    }
    OutPort(aAxisAddr, sCommand);
  }
  aAxisAddr += (LM628_DTA - LM628_CMD);
  if (NotBusy(wAxisNum)){
    RestoreFlags();
    return(TE28AXIS_TIMEOUT);
  }
  *pwValue = ((WORD)InPort(aAxisAddr) << 8);
  *pwValue |= ((WORD)InPort(aAxisAddr) & 0xFF);
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28RdLong                                                          *
**********************************************************************/
short te28RdLong(WORD wAxisNum, short sCommand, long *plValue)
{
  WORD wHiWord, wLoWord;
  ADDR aAxisAddr;

  if (wDebug & DEBUG_LOW)
    printf("te28RdLong ");

  *plValue = 0x80000000L;               /* if error, max negative value */
  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  aAxisAddr = aAxis28[wAxisNum].aAddr;
  aAxisAddr += LM628_CMD;
  SaveFlags();
  IntDisable();
  if ((char)sCommand != -1){
    if (NotBusy(wAxisNum)){
      RestoreFlags();
      return(TE28AXIS_TIMEOUT);
    }
    OutPort(aAxisAddr, sCommand);
  }
  aAxisAddr += (LM628_DTA - LM628_CMD);
  if (NotBusy(wAxisNum)){
    RestoreFlags();
    return(TE28AXIS_TIMEOUT);
  }
  wHiWord = (WORD)InPort(aAxisAddr) << 8;
  wHiWord |= (WORD)InPort(aAxisAddr) & 0xFF;
  if (NotBusy(wAxisNum)){
    RestoreFlags();
    return(TE28AXIS_TIMEOUT);
  }
  wLoWord = (WORD)InPort(aAxisAddr) << 8;
  wLoWord |= (WORD)InPort(aAxisAddr) & 0xFF;
  RestoreFlags();
  *plValue = ((long)wHiWord << 16) | (long)wLoWord;
  return(0);
}

/**********************************************************************
  te28RdStat                                                          *
**********************************************************************/
short te28RdStat(WORD wAxisNum)
{
  short sStatus;
  ADDR aAxisAddr;

  if (wDebug & DEBUG_LOW)
    printf("te28RdStat ");

  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  aAxisAddr = aAxis28[wAxisNum].aAddr;
  aAxisAddr += LM628_CMD;
  SaveFlags();
  IntDisable();
  if (NotBusy(wAxisNum)){
    RestoreFlags();
    return(TE28AXIS_TIMEOUT);
  }
  sStatus = (short)InPort(aAxisAddr) & 0xFF;
  if (wDebug & DEBUG_LOW)
    printf("%p 0x%x ", aAxisAddr, sStatus);
  RestoreFlags();
  return(sStatus);
}

/**********************************************************************
  te28RegRead                                                         *
**********************************************************************/
short te28RegRead(WORD wAxisNum, short sRegister)
{
  ADDR aAxisAddr;
  short sRetCode;

  if (wDebug & DEBUG_CONFIG)
    printf("te28RegRead ");

  aAxisAddr = aAxis28[wAxisNum].aAddr;
  SaveFlags();
  IntDisable();
  OutPort(aAxisAddr + CFG_ADR, sRegister);
  sRetCode = (short)InPort(aAxisAddr + CFG_DTA) & 0x0F;
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28RegWrite                                                        *
**********************************************************************/
short te28RegWrite(WORD wAxisNum, short sRegister, short sData)
{
  ADDR aAxisAddr;
  short sTemp;

  if (wDebug & DEBUG_CONFIG)
    printf("te28RegWrite ");

  aAxisAddr = aAxis28[wAxisNum].aAddr;
  SaveFlags();
  IntDisable();
  /* get current state of write enable bit */
  OutPort(aAxisAddr + CFG_ADR, 0);
  sTemp = (short)InPort(aAxisAddr + CFG_DTA) & 0x01;
  /* if write-protected, enable */
  if (!sTemp)
    OutPort(aAxisAddr + CFG_DTA, 0x04);
  /* write to the register */
  OutPort(aAxisAddr + CFG_ADR, sRegister);
  OutPort(aAxisAddr + CFG_DTA, sData);
  /* set write-protect back to its original state */
  if (!sTemp){
    OutPort(aAxisAddr + CFG_ADR, 0);
    OutPort(aAxisAddr + CFG_DTA, 0);
  }
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28RegBitSet                                                       *
**********************************************************************/
short te28RegBitSet(WORD wAxisNum, short sRegister, short sBit)
{
  ADDR aAxisAddr;
  short sTemp;

  if (wDebug & DEBUG_CONFIG)
    printf("te28RegBitSet ");

  aAxisAddr = aAxis28[wAxisNum].aAddr;
  SaveFlags();
  IntDisable();
  /* get current state of write enable bit */
  OutPort(aAxisAddr + CFG_ADR, 0);
  sTemp = (short)InPort(aAxisAddr + CFG_DTA) & 0x01;
  /* if write-protected, enable */
  if (!sTemp)
    OutPort(aAxisAddr + CFG_DTA, 0x04);
  /* set the given bit */
  OutPort(aAxisAddr + CFG_ADR, sRegister);
  OutPort(aAxisAddr + CFG_DTA, 0x04 | sBit);
  /* set write-protect back to its original state */
  if (!sTemp){
    OutPort(aAxisAddr + CFG_ADR, 0);
    OutPort(aAxisAddr + CFG_DTA, 0);
  }
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28RegBitClr                                                       *
**********************************************************************/
short te28RegBitClr(WORD wAxisNum, short sRegister, short sBit)
{
  ADDR aAxisAddr;
  short sTemp;

  if (wDebug & DEBUG_CONFIG)
    printf("te28RegBitClr ");

  aAxisAddr = aAxis28[wAxisNum].aAddr;
  SaveFlags();
  IntDisable();
  /* get current state of write enable bit */
  OutPort(aAxisAddr + CFG_ADR, 0);
  sTemp = (short)InPort(aAxisAddr + CFG_DTA) & 0x01;
  /* if write-protected, enable */
  if (!sTemp)
    OutPort(aAxisAddr + CFG_DTA, 0x04);
  /* clear the given bit */
  OutPort(aAxisAddr + CFG_ADR, sRegister);
  OutPort(aAxisAddr + CFG_DTA, sBit);
  /* set write-protect back to its original state */
  if (!sTemp){
    OutPort(aAxisAddr + CFG_ADR, 0);
    OutPort(aAxisAddr + CFG_DTA, 0);
  }
  RestoreFlags();
  return(0);
}

/**********************************************************************
  te28DacRead                                                         *
**********************************************************************/
short te28DacRead(WORD wAxisNum)
{
  short sTemp;

  if (wDebug & DEBUG_OUTPUT)
    printf("te28DacRead ");

  /* wait for the DacSync bit to go low */

  for (sTemp = 0; sTemp < 0x7FFF; sTemp++)
  {
    if (!(te28RegRead(wAxisNum, 8) & 0x04))
      break;
  }
  if (sTemp == 0x7FFF)
    return((short) TE28AXIS_TIMEOUT_WORD);

  /* wait for the DacSync bit to go high */

  for (sTemp = 0; sTemp < 0x7FFF; sTemp++)
  {
    if (te28RegRead(wAxisNum, 8) & 0x04)
      break;
  }
  if (sTemp == 0x7FFF)
    return((short) TE28AXIS_TIMEOUT_WORD);

  /* read the DAC registers */

  sTemp = te28RegRead(wAxisNum, 0x0F) << 8;
  sTemp |= te28RegRead(wAxisNum, 0x0E) << 4;
  sTemp |= te28RegRead(wAxisNum, 0x0D);
  
  /* sign-extend */
  
  if (sTemp & 0x800)
    sTemp |= 0xF000;
  return(sTemp);
}

/**********************************************************************
  te28DacRequest                                                      *
**********************************************************************/
short te28DacRequest(WORD wAxisNum)
{
  short sRetCode;
  WORD wTemp;

  if (wDebug & DEBUG_OUTPUT)
    printf("te28DacRequest ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28DacRequest(wAxisNum);
  }
  else{
    te28RegBitSet(wAxisNum, 8, 1);
    /* wait for the DacCntl bit to go high */
    sRetCode = TE28AXIS_TIMEOUT;
    for (wTemp = 0; wTemp < 0xFFFF; wTemp++){
      if (te28RegRead(wAxisNum, 8) & 0x01){
        sRetCode = 0;
        break;
      }
    }
  }
  return(sRetCode);
}

/**********************************************************************
  te28DacRelease                                                      *
**********************************************************************/
short te28DacRelease(WORD wAxisNum)
{
  short sRetCode;
  WORD wTemp;

  if (wDebug & DEBUG_OUTPUT)
    printf("te28DacRelease ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28DacRelease(wAxisNum);
  }
  else{
    te28RegBitClr(wAxisNum, 8, 1);
    /* wait for the DacCntl bit to go low */
    sRetCode = TE28AXIS_TIMEOUT;
    for (wTemp = 0; wTemp < 0xFFFF; wTemp++){
      if (!(te28RegRead(wAxisNum, 8) & 0x01)){
        sRetCode = 0;
        break;
      }
    }
  }
  return(sRetCode);
}

/**********************************************************************
  te28DacWrite                                                        *
**********************************************************************/
short te28DacWrite(WORD wAxisNum, short sData)
{

  if (wDebug & DEBUG_OUTPUT)
    printf("te28DacWrite ");

  te28RegWrite(wAxisNum, 0x0F, sData >> 8);
  te28RegWrite(wAxisNum, 0x0E, sData >> 4);
  te28RegWrite(wAxisNum, 0x0D, sData);
  return(0);
}

/**********************************************************************
  te28GPIn                                                            *
**********************************************************************/
short te28GPIn(WORD wAxisNum)
{
  if (wDebug & DEBUG_GENPURP)
    printf("te28GPIn ");

  return(te28RegRead(wAxisNum, 0x0A) & 0x01);
}

/**********************************************************************
  te28GPOut                                                           *
**********************************************************************/
short te28GPOut(WORD wAxisNum, short sOutVal)
{
  if (wDebug & DEBUG_GENPURP)
    printf("te28GPOut ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28GPOut(wAxisNum, sOutVal);
  }
  else{
    if (sOutVal)
      te28RegBitSet(wAxisNum, 8, 3);
    else
      te28RegBitClr(wAxisNum, 8, 3);
  }
  return(0);
}

/**********************************************************************
  te28GPIntPol                                                        *
**********************************************************************/
short te28GPIntPol(WORD wAxisNum, short sIntPol)
{
  if (wDebug & DEBUG_GENPURP)
    printf("te28GPIntPol ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28GPIntPol(wAxisNum, sIntPol);
  }
  else{
    if (sIntPol)
      te28RegBitSet(wAxisNum, 0x0A, 1);
    else
      te28RegBitClr(wAxisNum, 0x0A, 1);
  }
  return(0);
}

/**********************************************************************
  te28GPIntDisable                                                    *
**********************************************************************/
short te28GPIntDisable(WORD wAxisNum)
{
  if (wDebug & DEBUG_GENPURP)
    printf("te28GPIntDisable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28GPIntDisable(wAxisNum);
  }
  else
    te28RegBitClr(wAxisNum, 0, 3);
  return(0);
}

/**********************************************************************
  te28GPIntEnable                                                     *
**********************************************************************/
short te28GPIntEnable(WORD wAxisNum)
{
  if (wDebug & DEBUG_GENPURP)
    printf("te28GPIntEnable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28GPIntEnable(wAxisNum);
  }
  else
    te28RegBitSet(wAxisNum, 0, 3);
  return(0);
}

/**********************************************************************
  te28GPIntClr                                                        *
**********************************************************************/
short te28GPIntClr(WORD wAxisNum)
{
  if (wDebug & DEBUG_GENPURP)
    printf("te28GPIntClr ");

 if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28GPIntClr(wAxisNum);
  }
  else
    te28RegWrite(wAxisNum, 0x0B, 0);
  return(0);
}

/**********************************************************************
  te28GPIntRead                                                       *
**********************************************************************/
short te28GPIntRead(WORD wAxisNum)
{
  if (wDebug & DEBUG_GENPURP)
    printf("te28GPIntRead ");

  return((te28RegRead(wAxisNum, 6) >> 3) & 0x01);
}

/**********************************************************************
  te28IPErrEnable                                                     *
**********************************************************************/
short te28IPErrEnable(WORD wAxisNum)
{
  if (wDebug & DEBUG_OUTPUT)
    printf("te28IPErrEnable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28IPErrEnable(wAxisNum);
  }
  else
    te28RegBitClr(wAxisNum, 1, 3);
  return(0);
}

/**********************************************************************
  te28IPErrDisable                                                    *
**********************************************************************/
short te28IPErrDisable(WORD wAxisNum)
{
  if (wDebug & DEBUG_OUTPUT)
    printf("te28IPErrDisable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28IPErrDisable(wAxisNum);
  }
  else
    te28RegBitSet(wAxisNum, 1, 3);
  return(0);
}

/**********************************************************************
  te28EncPolarity                                                     *
*********************************************************************/
short te28EncPolarity(WORD wAxisNum, short sInvertPhaseA,
  short sInvertPhaseB, short sInvertIndex)
{
  if (wDebug & DEBUG_ENCODER)
    printf("te28EncPolarity ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28EncPolarity(wAxisNum, sInvertPhaseA, sInvertPhaseB, sInvertIndex);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    if (sInvertPhaseA)
      te28RegBitSet(wAxisNum, 4, 1);
    else
      te28RegBitClr(wAxisNum, 4, 1);
    if (sInvertPhaseB)
      te28RegBitSet(wAxisNum, 4, 0);
    else
      te28RegBitClr(wAxisNum, 4, 0);
    if (sInvertIndex)
      te28RegBitSet(wAxisNum, 4, 2);
    else
      te28RegBitClr(wAxisNum, 4, 2);
  }
  return(0);
}

/**********************************************************************
  te28EncSwap                                                         *
**********************************************************************/
short te28EncSwap(WORD wAxisNum)
{
  if (wDebug & DEBUG_ENCODER)
    printf("te28EncSwap ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28EncSwap(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitSet(wAxisNum, 4, 3);
  }
  return(0);
}

/**********************************************************************
  te28EncNoSwap                                                       *
**********************************************************************/
short te28EncNoSwap(WORD wAxisNum)
{
  if (wDebug & DEBUG_ENCODER)
    printf("te28EncNoSwap ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28EncNoSwap(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitClr(wAxisNum, 4, 3);
  }
  return(0);
}

/**********************************************************************
  te28EncIndexEnable                                                  *
**********************************************************************/
short te28EncIndexEnable(WORD wAxisNum)
{
  if (wDebug & DEBUG_ENCODER)
    printf("te28EncIndexEnable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28EncIndexEnable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitSet(wAxisNum, 3, 2);
  }
  return(0);
}

/**********************************************************************
  te28EncIndexDisable                                                 *
**********************************************************************/
short te28EncIndexDisable(WORD wAxisNum)
{
  if (wDebug & DEBUG_ENCODER)
    printf("te28EncIndexDisable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28EncIndexDisable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitClr(wAxisNum, 3, 2);
  }
  return(0);
}

/**********************************************************************
  te28EncInRead                                                       *
**********************************************************************/
short te28EncInRead(WORD wAxisNum)
{
  if (wDebug & DEBUG_ENCODER)
    printf("te28EncInRead ");

  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  return(te28RegRead(wAxisNum, 5));
}

/**********************************************************************
  te28EncOutRead                                                      *
**********************************************************************/
short te28EncOutRead(WORD wAxisNum)
{
  if (wDebug & DEBUG_ENCODER)
    printf("te28EncOutRead ");

  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  return(te28RegRead(wAxisNum, 0x0B));
}

/**********************************************************************
  te28LimitPolarity                                                   *
**********************************************************************/
short te28LimitPolarity(WORD wAxisNum,
  short sPosLimitPolarity, short sNegLimitPolarity)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitPolarity ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitPolarity(wAxisNum, sPosLimitPolarity, sNegLimitPolarity);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    if (sPosLimitPolarity)
      te28RegBitSet(wAxisNum, 2, 1);
    else
      te28RegBitClr(wAxisNum, 2, 1);
    if (sNegLimitPolarity)
      te28RegBitSet(wAxisNum, 2, 0);
    else
      te28RegBitClr(wAxisNum, 2, 0);
  }
  return(0);
}

/**********************************************************************
  te28LimitControlEnable                                              *
**********************************************************************/
short te28LimitControlEnable(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitControlEnable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitControlEnable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitSet(wAxisNum, 2, 2);
  }
  return(0);
}

/**********************************************************************
  te28LimitControlDisable                                             *
**********************************************************************/
short te28LimitControlDisable(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitControlDisable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitControlDisable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitClr(wAxisNum, 2, 2);
  }
  return(0);
}

/**********************************************************************
  te28LimitInRead                                                     *
**********************************************************************/
short te28LimitInRead(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitInRead ");

  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  return((te28RegRead(wAxisNum, 6) & 0x04) |
    (te28RegRead(wAxisNum, 7) & 0x03));
}

/**********************************************************************
  te28LimitIntRead                                                    *
**********************************************************************/
short te28LimitIntRead(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitIntRead ");

  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  return((te28RegRead(wAxisNum, 6) & 0x04) |
    (te28RegRead(wAxisNum, 3) & 0x03));
}

/**********************************************************************
  te28LimitIntClr                                                     *
**********************************************************************/
short te28LimitIntClr(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitIntClr ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitIntClr(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegWrite(wAxisNum, 6, 0);
  }
  return(0);
}

/**********************************************************************
  te28LimitNegIntClr                                                  *
**********************************************************************/
short te28LimitNegIntClr(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitNegIntClr ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitNegIntClr(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegWrite(wAxisNum, 5, 0);
  }
  return(0);
}

/**********************************************************************
  te28LimitPosIntClr                                                  *
**********************************************************************/
short te28LimitPosIntClr(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitPosIntClr ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitPosIntClr(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegWrite(wAxisNum, 7, 0);
  }
  return(0);
}

/**********************************************************************
  te28AxisIntDisable                                                  *
**********************************************************************/
short te28AxisIntDisable(WORD wAxisNum)
{
  if (wDebug & DEBUG_INTS)
    printf("te28AxisIntDisable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28AxisIntDisable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitClr(wAxisNum, 0, 1);
  }
  return(0);
}

/**********************************************************************
  te28AxisIntEnable                                                   *
**********************************************************************/
short te28AxisIntEnable(WORD wAxisNum)
{
  if (wDebug & DEBUG_INTS)
    printf("te28AxisIntEnable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28AxisIntEnable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitSet(wAxisNum, 0, 1);
  }
  return(0);
}

/**********************************************************************
  te28LimitIntDisable                                                 *
**********************************************************************/
short te28LimitIntDisable(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitIntDisable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitIntDisable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitClr(wAxisNum, 0, 2);
  }
  return(0);
}

/**********************************************************************
  te28LimitIntEnable                                                  *
**********************************************************************/
short te28LimitIntEnable(WORD wAxisNum)
{
  if (wDebug & DEBUG_LIMIT)
    printf("te28LimitIntEnable ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28LimitIntEnable(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitSet(wAxisNum, 0, 2);
  }
  return(0);
}

/**********************************************************************
  te28PollInt                                                         *
**********************************************************************/
short te28PollInt(WORD wAxisNum)
{
  if (wDebug & DEBUG_INTS)
    printf("te28PollInt ");

  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  return(te28RegRead(wAxisNum, 6) >> 1);
}

/**********************************************************************
  te28MotorOn                                                         *
**********************************************************************/
short te28MotorOn(WORD wAxisNum)
{
  if (wDebug & DEBUG_OUTPUT)
    printf("te28MotorOn ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28MotorOn(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitSet(wAxisNum, 1, 0);
  }
  return(0);
}

/**********************************************************************
  te28MotorOff                                                        *
**********************************************************************/
short te28MotorOff(WORD wAxisNum)
{
  if (wDebug & DEBUG_OUTPUT)
    printf("te28MotorOff ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28MotorOff(wAxisNum);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    te28RegBitClr(wAxisNum, 1, 0);
  }
  return(0);
}

/**********************************************************************
  te28MotorRead                                                       *
**********************************************************************/
short te28MotorRead(WORD wAxisNum)
{
  if (wDebug & DEBUG_OUTPUT)
    printf("te28MotorRead ");

  if (wAxisNum >= wTotalAxes)
    return(TE28AXIS_ERR);
  if (te28RegRead(wAxisNum, 7) & 0x04)
    return(TE28ACTIVE);
  return(TE28INACTIVE);
}

/**********************************************************************
  te28PWMPolarity                                                     *
**********************************************************************/
short te28PWMPolarity(WORD wAxisNum, short sInvertSign,
   short sInvertMagnitude)
{
  if (wDebug & DEBUG_OUTPUT)
    printf("te28PWNPolarity ");

  if (wAxisNum == TE28ALL_AXES){
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      te28PWMPolarity(wAxisNum, sInvertSign, sInvertMagnitude);
  }
  else{
    if (wAxisNum >= wTotalAxes)
      return(TE28AXIS_ERR);
    if (sInvertSign)
      te28RegBitSet(wAxisNum, 1, 1);
    else
      te28RegBitClr(wAxisNum, 1, 1);
    if (sInvertMagnitude)
      te28RegBitSet(wAxisNum, 1, 2);
    else
      te28RegBitClr(wAxisNum, 1, 2);
  }
  return(0);
}

/**********************************************************************
  te28AbsPos                                                          *
**********************************************************************/
short te28AbsPos(WORD wAxisNum, long lPosition)
{
  WORD awBuf[3];

  awBuf[0] = (WORD)(lPosition & 0xFFFF);
  awBuf[1] = (WORD)(lPosition >> 16);
  awBuf[2] = 0x0002;
  
  if (wDebug & DEBUG_MOTION)
    printf("te28AbsPos %ld ", lPosition);

  return(te28WrServo(wAxisNum, TE28LTRJ, 3, awBuf));
}

/**********************************************************************
  te28RelPos                                                          *
**********************************************************************/
short te28RelPos(WORD wAxisNum, long lPosition)
{
  WORD awBuf[3];
  if (wDebug & DEBUG_MOTION)
    printf("te28RelPos ");

  awBuf[0] = (WORD)(lPosition & 0xFFFF);
  awBuf[1] = (WORD)(lPosition >> 16);
  awBuf[2] = 0x0003;
  return(te28WrServo(wAxisNum, TE28LTRJ, 3, awBuf));
}

/**********************************************************************
  te28AbsVel                                                          *
**********************************************************************/
short te28AbsVel(WORD wAxisNum, long lVelocity)
{
  WORD awBuf[3];

  if (wDebug & DEBUG_MOTION)
    printf("te28AbsVel %d ", lVelocity);

  awBuf[0] = (WORD)(lVelocity & 0xFFFF);
  awBuf[1] = (WORD)(lVelocity >> 16);
  awBuf[2] = 0x0008;
  return(te28WrServo(wAxisNum, TE28LTRJ, 3, awBuf));
}

/**********************************************************************
  te28RelVel                                                          *
**********************************************************************/
short te28RelVel(WORD wAxisNum, long lVelocity)
{
  WORD awBuf[3];

  if (wDebug)
    printf("te28RelVel ");

  awBuf[0] = (WORD)(lVelocity & 0xFFFF);
  awBuf[1] = (WORD)(lVelocity >> 16);
  awBuf[2] = 0x000C;
  return(te28WrServo(wAxisNum, TE28LTRJ, 3, awBuf));
}

/**********************************************************************
  te28AbsAcc                                                          *
**********************************************************************/
short te28AbsAcc(WORD wAxisNum, long lAcceleration)
{
  WORD awBuf[3];

  if (wDebug & DEBUG_MOTION)
    printf("te28AbsAcc %d ", lAcceleration);

  awBuf[0] = (WORD)(lAcceleration & 0xFFFF);
  awBuf[1] = (WORD)(lAcceleration >> 16);
  awBuf[2] = 0x0020;
  return(te28WrServo(wAxisNum, TE28LTRJ, 3, awBuf));
}

/**********************************************************************
  te28RelAcc                                                          *
**********************************************************************/
short te28RelAcc(WORD wAxisNum, long lAcceleration)
{
  WORD awBuf[3];

  if (wDebug & DEBUG_MOTION)
    printf("te28RelAcc ");

  awBuf[0] = (WORD)(lAcceleration & 0xFFFF);
  awBuf[1] = (WORD)(lAcceleration >> 16);
  awBuf[2] = 0x0030;
  return(te28WrServo(wAxisNum, TE28LTRJ, 3, awBuf));
}

/**********************************************************************
  te28Profile                                                         *
**********************************************************************/
short te28Profile(WORD wAxisNum, long lPosition, long lVelocity, long lAcceleration)
{
  WORD awBuf[7];

  if (wDebug & DEBUG_MOTION)
    printf("te28Profile ");

  awBuf[0] = (WORD)(lPosition & 0xFFFF);
  awBuf[1] = (WORD)(lPosition >> 16);
  awBuf[2] = (WORD)(lVelocity & 0xFFFF);
  awBuf[3] = (WORD)(lVelocity >> 16);
  awBuf[4] = (WORD)(lAcceleration & 0xFFFF);
  awBuf[5] = (WORD)(lAcceleration >> 16);
  awBuf[6] = 0x002A;
  return(te28WrServo(wAxisNum, TE28LTRJ, 7, awBuf));
}

/**********************************************************************
  te28Start                                                           *
**********************************************************************/
short te28Start(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_MOTION) {
    printf("te28Start ");
  } 
  SaveFlags();
  IntDisable();
  /* reset trajectory complete interrupt, if active */
  sRetCode = te28RstInt(wAxisNum, TE28TC_INT);
  if (!sRetCode) {
    sRetCode = te28WrCmd(wAxisNum, TE28STT);
    if (wDebug & DEBUG_MOTION) {
      printf("%d ", sRetCode);
    }
  }
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28StopMove                                                        *
**********************************************************************/
short te28StopMove(WORD wAxisNum) 
{
  short sRetCode;

  SaveFlags();
  IntDisable();
  sRetCode = te28WrWord(wAxisNum, TE28LTRJ, 0x0400);
  if (wDebug & DEBUG_MOTION) {
    printf("te28StopMove %d ", sRetCode);
  }
  if (!sRetCode) {
    sRetCode = te28Start(wAxisNum);
  }
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28AbortMove                                                       *
**********************************************************************/
short te28AbortMove(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_MOTION)
    printf("te28AbortMove ");

  SaveFlags();
  IntDisable();
  sRetCode = te28WrWord(wAxisNum, TE28LTRJ, 0x0200);
  if (!sRetCode)
    sRetCode = te28Start(wAxisNum);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28Coast                                                           *
**********************************************************************/
short te28Coast(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_MOTION)
    printf("te28Coast ");

  SaveFlags();
  IntDisable();
  sRetCode = te28WrWord(wAxisNum, TE28LTRJ, 0x0100);
  if (!sRetCode)
    sRetCode = te28Start(wAxisNum);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28VelMode                                                         *
**********************************************************************/
short te28VelMode(WORD wAxisNum, long lVelocity)
{
  short sRetCode;
  long lNewVel;
  WORD awBuf[3];

  if (wDebug & DEBUG_MOTION)
    printf("te28VelMode ");

  if (lVelocity >= 0){
    awBuf[0] = (WORD)(lVelocity & 0xFFFF);
    awBuf[1] = (WORD)(lVelocity >> 16);
    awBuf[2] = 0x1808;          /* velocity mode, forward */
  }
  else{
    lNewVel = -lVelocity;
    awBuf[0] = (WORD)(lNewVel & 0xFFFF);
    awBuf[1] = (WORD)(lNewVel >> 16);
    awBuf[2] = 0x0808;          /* velocity mode, reverse */
  }
  SaveFlags();
  IntDisable();
  sRetCode = te28WrServo(wAxisNum, TE28LTRJ, 3, awBuf);
  if (!sRetCode)
    sRetCode = te28Start(wAxisNum);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28Home                                                            *
**********************************************************************/
short te28Home(WORD wAxisNum)
{
  if (wDebug & DEBUG_MOTION)
    printf("te28Home ");

  return(te28WrCmd(wAxisNum, TE28DFH));
}

/**********************************************************************
  te28ActPos                                                          *
**********************************************************************/
long te28ActPos(WORD wAxisNum)
{
  long lActPos;
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28ActPos ");

  sRetCode = te28RdLong(wAxisNum, TE28RDRP, &lActPos);
  if (sRetCode < 0){
    if (sRetCode == TE28AXIS_ERR)
      return(TE28AXIS_ERR_LONG);
    if (sRetCode == TE28AXIS_TIMEOUT)
      return(TE28AXIS_TIMEOUT_LONG);
  }
  if (wDebug & DEBUG_SERVO)
    printf("%d ", lActPos);
  return(lActPos);
}

/**********************************************************************
  te28DesPos                                                          *
**********************************************************************/
long te28DesPos(WORD wAxisNum)
{
  long lDesPos;
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28DesPos ");

  sRetCode = te28RdLong(wAxisNum, TE28RDDP, &lDesPos);
  if (sRetCode < 0){
    if (sRetCode == TE28AXIS_ERR)
      return(TE28AXIS_ERR_LONG);
    if (sRetCode == TE28AXIS_TIMEOUT)
      return(TE28AXIS_TIMEOUT_LONG);
  }
  return(lDesPos);
}

/**********************************************************************
  te28IndexPos                                                        *
**********************************************************************/
long te28IndexPos(WORD wAxisNum)
{
  long lIndexPos;
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28IndexPos ");

  sRetCode = te28RdLong(wAxisNum, TE28RDIP, &lIndexPos);
  if (sRetCode < 0){
    if (sRetCode == TE28AXIS_ERR)
      return(TE28AXIS_ERR_LONG);
    if (sRetCode == TE28AXIS_TIMEOUT)
      return(TE28AXIS_TIMEOUT_LONG);
  }
  return(lIndexPos);
}

/**********************************************************************
  te28ActVel                                                          *
**********************************************************************/
short te28ActVel(WORD wAxisNum)
{
  WORD wVel;
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28ActVel ");

  sRetCode = te28RdWord(wAxisNum, TE28RDRV, &wVel);
  if (sRetCode < 0)
  {
    if (sRetCode == TE28AXIS_ERR)
      return((short) TE28AXIS_ERR_WORD);

    if (sRetCode == TE28AXIS_TIMEOUT)
      return((short) TE28AXIS_TIMEOUT_WORD);
  }
  return(wVel);
}

/**********************************************************************
  te28DesVel                                                          *
**********************************************************************/
long te28DesVel(WORD wAxisNum)
{
  long lDesVel;
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28DesVel ");

  sRetCode = te28RdLong(wAxisNum, TE28RDDV, &lDesVel);
  if (sRetCode < 0){
    if (sRetCode == TE28AXIS_ERR)
      return(TE28AXIS_ERR_LONG);
    if (sRetCode == TE28AXIS_TIMEOUT)
      return(TE28AXIS_TIMEOUT_LONG);
  }
  return(lDesVel);
}

/**********************************************************************
  te28MaskInt                                                         *
**********************************************************************/
short te28MaskInt(WORD wAxisNum, short sNewMask)
{
  short sRetCode, sMask;

  if (wDebug & DEBUG_TIME)
    printf("te28MaskInt ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28MaskInt(wAxisNum, sNewMask);
  }
  else{
    sMask = ~sNewMask & aAxis28[wAxisNum].byIntMask & 0x7E;
    aAxis28[wAxisNum].byIntMask = (BYTE)sMask;
    sRetCode = te28WrWord(wAxisNum, TE28MSKI, sMask);
  }
  return(sRetCode);
}

/**********************************************************************
  te28UnmaskInt                                                       *
**********************************************************************/
short te28UnmaskInt(WORD wAxisNum, short sNewMask)
{
  short sRetCode, sMask;

  if (wDebug & DEBUG_TIME)
    printf("te28UnmaskInt ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28UnmaskInt(wAxisNum, sNewMask);
  }
  else{
    sMask = (sNewMask | aAxis28[wAxisNum].byIntMask) & 0x7E;
    aAxis28[wAxisNum].byIntMask = (BYTE)sMask;
    sRetCode = te28WrWord(wAxisNum, TE28MSKI, sMask);
  }
  return(sRetCode);
}

/**********************************************************************
  te28RstInt                                                          *
**********************************************************************/
short te28RstInt(WORD wAxisNum, short sIntType)
{
  short sRetCode;

  if (wDebug & DEBUG_TIME)
    printf("te28RstInt ");

  sRetCode = te28WrWord(wAxisNum, TE28RSTI, ~sIntType);
  /* wait for not busy to allow time for interrupt to clear */
  if (!sRetCode){
    if (NotBusy(wAxisNum))
      return(TE28AXIS_TIMEOUT);
  }
  return(sRetCode);
}

/**********************************************************************
  te28ReadInts                                                        *
**********************************************************************/
short te28ReadInts(WORD wAxisNum)
{
  short sMask, sRetCode;

  if (wDebug & DEBUG_TIME)
    printf("te28ReadInts ");

  SaveFlags();
  IntDisable();
  sMask = te28RdStat(wAxisNum);
  if (sMask >= 0){
    sMask &= aAxis28[wAxisNum].byIntMask;
    sRetCode = te28RstInt(wAxisNum, sMask);
    if (sRetCode){
      RestoreFlags();
      return(sRetCode);
    }
  }
  RestoreFlags();
  return(sMask);
}

/**********************************************************************
  te28Complete                                                        *
**********************************************************************/
short te28Complete(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28Complete ");

  SaveFlags();
  IntDisable();
  sRetCode = te28RdStat(wAxisNum);
  if (sRetCode >= 0){
    if (sRetCode & TE28TC_INT)
      sRetCode = TE28ACTIVE;
    else
      sRetCode = TE28INACTIVE;
  }
  RestoreFlags();
  if (wDebug & DEBUG_SERVO)
    printf("%d ", sRetCode);
  return(sRetCode);
}

/**********************************************************************
  te28BreakPt                                                         *
**********************************************************************/
short te28BreakPt(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28BreakPt ");

  SaveFlags();
  IntDisable();
  sRetCode = te28RdStat(wAxisNum);
  if (sRetCode >= 0){
    if (sRetCode & TE28BP_INT){
      te28RstInt(wAxisNum, TE28BP_INT);
      sRetCode = TE28ACTIVE;
    }
    else
      sRetCode = TE28INACTIVE;
  }
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28Index                                                           *
**********************************************************************/
short te28Index(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28Index ");

  SaveFlags();
  IntDisable();
  sRetCode = te28RdStat(wAxisNum);
  if (sRetCode >= 0){
    if (sRetCode & TE28IP_INT)
      sRetCode = TE28ACTIVE;
    else
      sRetCode = TE28INACTIVE;
  }
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28PosErr                                                          *
**********************************************************************/
short te28PosErr(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28PosErr ");

  SaveFlags();
  IntDisable();
  sRetCode = te28RdStat(wAxisNum);
  if (sRetCode >= 0){
    if (sRetCode & TE28PE_INT){
      te28RstInt(wAxisNum, TE28PE_INT);
      sRetCode = TE28ACTIVE;
    }
    else
      sRetCode = TE28INACTIVE;
  }
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28WrapAround                                                      *
**********************************************************************/
short te28WrapAround(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28WrapAround ");

  SaveFlags();
  IntDisable();
  sRetCode = te28RdStat(wAxisNum);
  if (sRetCode >= 0){
    if (sRetCode & TE28WA_INT){
      te28RstInt(wAxisNum, TE28WA_INT);
        sRetCode = TE28ACTIVE;
    }
    else
      sRetCode = TE28INACTIVE;
  }
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28CmdErr                                                          *
**********************************************************************/
short te28CmdErr(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_SERVO)
    printf("te28CmdErr ");

  SaveFlags();
  IntDisable();
  sRetCode = te28RdStat(wAxisNum);
  if (sRetCode >= 0){
    if (sRetCode & TE28CE_INT){
      te28RstInt(wAxisNum, TE28CE_INT);
      sRetCode = TE28ACTIVE;
    }
    else
     sRetCode = TE28INACTIVE;
  }
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28SBpAbs                                                          *
**********************************************************************/
short te28SBpAbs(WORD wAxisNum, long lBreakPoint)
{
  short sRetCode;

  if (wDebug & DEBUG_TIME)
    printf("te28SBpAbs ");

  SaveFlags();
  IntDisable();
  /* reset breakpoint interrupt, if active */
  sRetCode = te28RstInt(wAxisNum, TE28BP_INT);
  if (!sRetCode)
    sRetCode = te28WrLong(wAxisNum, TE28SBPA, lBreakPoint);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28SBpRel                                                          *
**********************************************************************/
short te28SBpRel(WORD wAxisNum, long lBreakPoint)
{
  short sRetCode;

  if (wDebug & DEBUG_TIME)
    printf("te28SBpRel ");

  SaveFlags();
  IntDisable();
  /* reset breakpoint interrupt, if active */
  sRetCode = te28RstInt(wAxisNum, TE28BP_INT);
  if (!sRetCode)
    sRetCode = te28WrLong(wAxisNum, TE28SBPR, lBreakPoint);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28SIndex                                                          *
**********************************************************************/
short te28SIndex(WORD wAxisNum)
{
  short sRetCode;

  if (wDebug & DEBUG_TIME)
    printf("te28SIndex ");

  SaveFlags();
  IntDisable();
  /* reset index pulse interrupt, if active */
  sRetCode = te28RstInt(wAxisNum, TE28IP_INT);
  if (!sRetCode)
    sRetCode = te28WrCmd(wAxisNum, TE28SIP);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28SErrI                                                           *
**********************************************************************/
short te28SErrI(WORD wAxisNum, WORD wErrorLimit)
{
  short sRetCode;

  if (wDebug & DEBUG_TIME)
    printf("te28SErrI ");

  SaveFlags();
  IntDisable();
  /* reset position error interrupt, if active */
  sRetCode = te28RstInt(wAxisNum, TE28PE_INT);
  if (!sRetCode)
    sRetCode = te28WrWord(wAxisNum, TE28LPEI, wErrorLimit);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28SerrS                                                           *
**********************************************************************/
short te28SErrS(WORD wAxisNum, WORD wErrorLimit)
{
  short sRetCode;

  if (wDebug & DEBUG_TIME)
    printf("te28SErrS ");

  SaveFlags();
  IntDisable();
  /* reset position error interrupt, if active */
  sRetCode = te28RstInt(wAxisNum, TE28PE_INT);
  if (!sRetCode)
    sRetCode = te28WrWord(wAxisNum, TE28LPES, wErrorLimit);
  RestoreFlags();
  return(sRetCode);
}

/**********************************************************************
  te28FilterKP                                                        *
**********************************************************************/
short te28FilterKP(WORD wAxisNum, WORD wKPValue)
{
  short sRetCode;
  WORD awBuf[2];

  if (wDebug & DEBUG_FILTER)
    printf("te28FilterKP ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28FilterKP(wAxisNum, wKPValue);
  }
  else{
    awBuf[0] = wKPValue;
    awBuf[1] = ((WORD)aAxis28[wAxisNum].bySample << 8) | 0x08;
    SaveFlags();
    IntDisable();
    sRetCode = te28WrServo(wAxisNum, TE28LFIL, 2, awBuf);
    if (!sRetCode)
      sRetCode = te28WrCmd(wAxisNum, TE28UDF);
    RestoreFlags();
  }
  return(sRetCode);
}

/**********************************************************************
  te28FilterKI                                                        *
**********************************************************************/
short te28FilterKI(WORD wAxisNum, WORD wKIValue)
{
  short sRetCode;
  WORD awBuf[2];

  if (wDebug & DEBUG_FILTER)
    printf("te28FilterKI ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28FilterKI(wAxisNum, wKIValue);
  }
  else{
    awBuf[0] = wKIValue;
    awBuf[1] = ((WORD)aAxis28[wAxisNum].bySample << 8) | 0x04;
    SaveFlags();
    IntDisable();
    sRetCode = te28WrServo(wAxisNum, TE28LFIL, 2, awBuf);
    if (!sRetCode)
      sRetCode = te28WrCmd(wAxisNum, TE28UDF);
    RestoreFlags();
  }
  return(sRetCode);
}

/**********************************************************************
  te28FilterKD                                                        *
**********************************************************************/
short te28FilterKD(WORD wAxisNum, WORD wKDValue)
{
  short sRetCode;
  WORD awBuf[2];

  if (wDebug & DEBUG_FILTER)
    printf("te28FilterKD ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28FilterKD(wAxisNum, wKDValue);
  }
  else{
    awBuf[0] = wKDValue;
    awBuf[1] = ((WORD)aAxis28[wAxisNum].bySample << 8) | 0x02;
    SaveFlags();
    IntDisable();
    sRetCode = te28WrServo(wAxisNum, TE28LFIL, 2, awBuf);
    if (!sRetCode)
      sRetCode = te28WrCmd(wAxisNum, TE28UDF);
    RestoreFlags();
  }
  return(sRetCode);
}

/**********************************************************************
  te28FilterIL                                                        *
**********************************************************************/
short te28FilterIL(WORD wAxisNum, WORD wILValue)
{
  short sRetCode;
  WORD awBuf[2];

  if (wDebug & DEBUG_FILTER)
    printf("te28FilterIL ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28FilterIL(wAxisNum, wILValue);
  }
  else{
    awBuf[0] = wILValue;
    awBuf[1] = ((WORD)aAxis28[wAxisNum].bySample << 8) | 0x01;
    SaveFlags();
    IntDisable();
    sRetCode = te28WrServo(wAxisNum, TE28LFIL, 2, awBuf);
    if (!sRetCode)
      sRetCode = te28WrCmd(wAxisNum, TE28UDF);
    RestoreFlags();
  }
  return(sRetCode);
}

/**********************************************************************
  te28FilterSI                                                        *
**********************************************************************/
short te28FilterSI(WORD wAxisNum, short sSIValue)
{
  short sRetCode;
  WORD wTemp;

  if (wDebug & DEBUG_FILTER)
    printf("te28FilterSI ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28FilterSI(wAxisNum, sSIValue);
  }
  else{
    wTemp = (sSIValue << 8) + 0;    /* only load SI */
    SaveFlags();
    IntDisable();
    aAxis28[wAxisNum].bySample = (BYTE)sSIValue;
    sRetCode = te28WrWord(wAxisNum, TE28LFIL, wTemp);
    if (!sRetCode)
      sRetCode = te28WrCmd(wAxisNum, TE28UDF);
    RestoreFlags();
  }
  return(sRetCode);
}

/**********************************************************************
  te28Filter                                                          *
**********************************************************************/
short te28Filter(WORD wAxisNum, WORD wKPValue, WORD wKIValue,
  WORD wKDValue, WORD wILValue, short sSIValue)
{
  WORD awBuf[5];
  short sRetCode;

  if (wDebug & DEBUG_FILTER)
    printf("te28Filter ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28Filter(wAxisNum, wKPValue, wKIValue, wKDValue,
        wILValue, sSIValue);
  }
  else{
    awBuf[0] = wILValue;
    awBuf[1] = wKDValue;
    awBuf[2] = wKIValue;
    awBuf[3] = wKPValue;
    awBuf[4] = (sSIValue << 8) | 0x0F; /* load all */
    SaveFlags();
    IntDisable();
    aAxis28[wAxisNum].bySample = (BYTE)sSIValue;
    sRetCode = te28WrServo(wAxisNum, TE28LFIL, 5, awBuf);
    if (!sRetCode)
      sRetCode = te28WrCmd(wAxisNum, TE28UDF);
    RestoreFlags();
  }
  return(sRetCode);
}

/**********************************************************************
  te28FilterISum                                                      *
**********************************************************************/
short te28FilterISum(WORD wAxisNum)
{
  short sRetCode;
  WORD wSum;

  if (wDebug & DEBUG_FILTER)
    printf("te28FilterISum ");

  sRetCode = te28RdWord(wAxisNum, TE28RDSUM, &wSum);
  if (sRetCode < 0)
  {
    if (sRetCode == TE28AXIS_ERR)
      return((short) TE28AXIS_ERR_WORD);

    if (sRetCode == TE28AXIS_TIMEOUT)
      return((short) TE28AXIS_TIMEOUT_WORD);
  }
  return((short) wSum);
}

/**********************************************************************
  te28RdSignals                                                       *
**********************************************************************/
WORD te28RdSignals(WORD wAxisNum)
{
  short sRetCode;
  WORD wSignals;

  if (wDebug & DEBUG_LOW)
    printf("te28RdSignals ");

  sRetCode = te28RdWord(wAxisNum, TE28RDSIGS, &wSignals);
  if (sRetCode < 0){
    if (sRetCode == TE28AXIS_ERR)
      return(TE28AXIS_ERR_WORD);
    if (sRetCode == TE28AXIS_TIMEOUT)
      return(TE28AXIS_TIMEOUT_WORD);
  }
  return(wSignals);
}

/**********************************************************************
  te28ResetAxis                                                       *
**********************************************************************/
short te28ResetAxis(WORD wAxisNum)
{
  short sRetCode, sTemp;
  WORD wLoopCount;

  if (wDebug & DEBUG_INIT)
    printf("te28ResetAxis ");

  if (wAxisNum == TE28ALL_AXES){
    sRetCode = 0;
    for (wAxisNum = 0; wAxisNum < wTotalAxes; wAxisNum++)
      sRetCode |= te28ResetAxis(wAxisNum);
  }
  else{
    SaveFlags();
    IntDisable();

    /* save state of the axis interrupt enable */

    sTemp = te28RegRead(wAxisNum, 0) & 0x02;
    te28AxisIntDisable(wAxisNum);
    while(1){
      sRetCode = te28WrCmd(wAxisNum, TE28RST);
      if (sRetCode)
        break;

      /* wait for reset to be complete (status non-zero) */

      for (wLoopCount = 0xFFFF; wLoopCount; wLoopCount--){
        sRetCode = te28RdStat(wAxisNum);
        if (sRetCode)
          break;
      }
      if (sRetCode < 0)
        break;
      if (!wLoopCount){
        sRetCode = TE28AXIS_TIMEOUT;
        break;
      }
      sRetCode = te28WrWord(wAxisNum, TE28MSKI, 0);
      if (sRetCode)
        break;

      /* set up DAC output to 12 bits, if necessary */

      if ((aAxis28[wAxisNum].lpwIDProm[5] & 0xFF) == 28)
        sRetCode = te28WrCmd(wAxisNum, TE28PORT12);

      /* set breakpoint so breakpoint interrupt will clear */

      sRetCode = te28SBpAbs(wAxisNum, 0x3FFFFFFFL);
      if (sRetCode)
        break;
      sRetCode = te28RstInt(wAxisNum, 0xFF);
      if (sRetCode)
        break;
      aAxis28[wAxisNum].bySample = 0;
      aAxis28[wAxisNum].byIntMask = 0;
      sRetCode = te28LimitIntClr(wAxisNum);
      if (sRetCode)
        break;
      sRetCode = te28MotorOn(wAxisNum);
      break;
    }
    if (sTemp)
      te28AxisIntEnable(wAxisNum);
    RestoreFlags();
  }
  return(sRetCode);
}
