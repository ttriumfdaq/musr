/****
*  te28v.h VME Header File
*  Model 28/29 Servo-Motor Controller Driver for the
*  Modular Motion System (MMS)
****/

/* system constants */
#define PACKS_PER_BOARD    4
#define INTRPTS_PER_PACK   2
#define INTRPTS_PER_BOARD  (PACKS_PER_BOARD * INTRPTS_PER_PACK)
#define AXES_PER_PACK      2

/* software maximums */
#define MAX_BOARDS  8
#define MAX_PACKS   (PACKS_PER_BOARD * MAX_BOARDS)
#define MAX_AXES    (MAX_PACKS * AXES_PER_PACK)


#define TE28PIN  (0xC100 | 28)
#define TE29PIN  (0xC100 | 29)

/** interrupt types **/
#define TE28INTRPT_AXIS   0x01
#define TE28INTRPT_LIMIT  0x02
#define TE28INTRPT_GPIN   0x04

/** return values **/
#define TE28INACTIVE   0
#define TE28ACTIVE    -1

/** error codes **/

#define TE28BOARD_ERR          -1

#define TE28AXIS_NOT_RESPOND   0 /* used only in te28InitPack */

#define TE28AXIS_OK            0
#define TE28AXIS_ERR           -3
#define TE28AXIS_TIMEOUT       -4
#define TE28AXIS_ERR_WORD      0x9000
#define TE28AXIS_TIMEOUT_WORD  0xA000
#define TE28AXIS_ERR_LONG      0x90000000L
#define TE28AXIS_TIMEOUT_LONG  0xA0000000L

/** axis definitions **/
#define TE28ALL_AXES  0xFFFF

/** LM628 axis controller commands **/
#define TE28RST     0x00
#define TE28PORT8   0x05
#define TE28PORT12  0x06
#define TE28DFH     0x02
#define TE28SIP     0x03
#define TE28LPEI    0x1B
#define TE28LPES    0x1A
#define TE28SBPA    0x20
#define TE28SBPR    0x21
#define TE28MSKI    0x1C
#define TE28RSTI    0x1D
#define TE28LFIL    0x1E
#define TE28UDF     0x04
#define TE28LTRJ    0x1F
#define TE28STT     0x01
#define TE28RDSIGS  0x0C
#define TE28RDIP    0x09
#define TE28RDDP    0x08
#define TE28RDRP    0x0A
#define TE28RDDV    0x07
#define TE28RDRV    0x0B
#define TE28RDSUM   0x0D

/** masks for the LM628 interrupt value **/
#define TE28BP_INT  0x40  /* breakpoint interrupt */
#define TE28PE_INT  0x20  /* position error interrupt */
#define TE28WA_INT  0x10  /* wrap-around interrupt */
#define TE28IP_INT  0x08  /* index pulse interrupt */
#define TE28TC_INT  0x04  /* terminal count interrupt */
#define TE28CE_INT  0x02  /* command error interrupt */

/** function prototypes **/
short te28AbortMove(unsigned short wAxisNum);
short te28AbsAcc(unsigned short wAxisNum, long lAcceleration);
short te28AbsPos(unsigned short wAxisNum, long lPosition);
short te28AbsVel(unsigned short wAxisNum, long lVelocity);
long te28ActPos(unsigned short wAxisNum);
short te28ActVel(unsigned short wAxisNum);
short te28AxisIntDisable(unsigned short wAxisNum);
short te28AxisIntEnable(unsigned short wAxisNum);
short te28BreakPt(unsigned short wAxisNum);
short te28CmdErr(unsigned short wAxisNum);
short te28Coast(unsigned short wAxisNum);
short te28Complete(unsigned short wAxisNum);
short te28DacRead(unsigned short wAxisNum);
short te28DacRelease(unsigned short wAxisNum);
short te28DacRequest(unsigned short wAxisNum);
short te28DacWrite(unsigned short wAxisNum, short sData);
long te28DesPos(unsigned short wAxisNum);
long te28DesVel(unsigned short wAxisNum);
short te28EncIndexDisable(unsigned short wAxisNum);
short te28EncIndexEnable(unsigned short wAxisNum);
short te28EncInRead(unsigned short wAxisNum);
short te28EncNoSwap(unsigned short wAxisNum);
short te28EncOutRead(unsigned short wAxisNum);
short te28EncPolarity(unsigned short wAxisNum, short sInvertPhaseA,
  short sInvertPhaseB, short sInvertIndex);
short te28EncSwap(unsigned short wAxisNum);
short te28Filter(unsigned short wAxisNum, unsigned short wKPValue,
  unsigned short wKIValue, unsigned short wKDValue, unsigned short wILValue,
  short sSIValue);
short te28FilterIL(unsigned short wAxisNum, unsigned short wILValue);
short te28FilterISum(unsigned short wAxisNum);
short te28FilterKD(unsigned short wAxisNum, unsigned short wKDValue);
short te28FilterKI(unsigned short wAxisNum, unsigned short wKIValue);
short te28FilterKP(unsigned short wAxisNum, unsigned short wKPValue);
short te28FilterSI(unsigned short wAxisNum, short sSIValue);
short te28GPIn(unsigned short wAxisNum);
short te28GPIntClr(unsigned short wAxisNum);
short te28GPIntDisable(unsigned short wAxisNum);
short te28GPIntEnable(unsigned short wAxisNum);
short te28GPIntPol(unsigned short wAxisNum, short sIntPol);
short te28GPIntRead(unsigned short wAxisNum);
short te28GPOut(unsigned short wAxisNum, short sOutVal);
short te28Home(unsigned short wAxisNum);
short te28Index(unsigned short wAxisNum);
long te28IndexPos(unsigned short wAxisNum);
short te28InitPack(unsigned short wBoard, unsigned short wSlot,
  unsigned char *pwPackAddr, unsigned short wVector,
  unsigned short *pwIDProm);
short te28InitSw(void);
short te28IntrStart(unsigned short wBoard, unsigned short wIntrptNum,
  unsigned short *pwPackNum, unsigned short *pwUnitNum,
  unsigned short *pwIntrType);
short te28IntrEnd(void);
short te28IPErrDisable(unsigned short wAxisNum);
short te28IPErrEnable(unsigned short wAxisNum);
short te28LimitControlDisable(unsigned short wAxisNum);
short te28LimitControlEnable(unsigned short wAxisNum);
short te28LimitInRead(unsigned short wAxisNum);
short te28LimitIntClr(unsigned short wAxisNum);
short te28LimitIntDisable(unsigned short wAxisNum);
short te28LimitIntEnable(unsigned short wAxisNum);
short te28LimitIntRead(unsigned short wAxisNum);
short te28LimitNegIntClr(unsigned short wAxisNum);
short te28LimitPolarity(unsigned short wAxisNum,
  short sPosLimitPolarity, short sNegLimitPolarity);
short te28LimitPosIntClr(unsigned short wAxisNum);
short te28MaskInt(unsigned short wAxisNum, short sNewMask);
short te28MotorOff(unsigned short wAxisNum);
short te28MotorOn(unsigned short wAxisNum);
short te28MotorRead(unsigned short wAxisNum);
short te28PollInt(unsigned short wAxisNum);
short te28PosErr(unsigned short wAxisNum);
short te28Profile(unsigned short wAxisNum, long lPosition,
  long lVelocity, long lAcceleration);
short te28PWMPolarity(unsigned short wAxisNum,
  short sInvertSign, short sInvertMagnitude);
short te28RdWord(unsigned short wAxisNum, short sCmd,
  unsigned short *pwValue);
short te28RdLong(unsigned short wAxisNum, short sCmd, long *plValue);
unsigned short te28RdSignals(unsigned short wAxisNum);
short te28RdStat(unsigned short wAxisNum);
short te28ReadInts(unsigned short wAxisNum);
short te28RegBitClr(unsigned short wAxisNum, short sRegister, short sBit);
short te28RegBitSet(unsigned short wAxisNum, short sRegister, short sBit);
short te28RegRead(unsigned short wAxisNum, short sRegister);
short te28RegWrite(unsigned short wAxisNum, short sRegister, short sData);
short te28RelAcc(unsigned short wAxisNum, long lAcceleration);
short te28RelPos(unsigned short wAxisNum, long lPosition);
short te28RelVel(unsigned short wAxisNum, long lVelocity);
short te28ResetAxis(unsigned short wAxisNum);
short te28RstInt(unsigned short wAxisNum, short sIntType);
short te28SBpAbs(unsigned short wAxisNum, long lBreakPoint);
short te28SBpRel(unsigned short wAxisNum, long lBreakPoint);
short te28SErrI(unsigned short wAxisNum, unsigned short wErrorLimit);
short te28SErrS(unsigned short wAxisNum, unsigned short wErrorLimit);
short te28SIndex(unsigned short wAxisNum);
short te28Start(unsigned short wAxisNum);
short te28StopMove(unsigned short wAxisNum);
short te28UnmaskInt(unsigned short wAxisNum, short sNewMask);
short te28VelMode(unsigned short wAxisNum, long lVelocity);
short te28WrCmd(unsigned short wAxisNum, short sCmd);
short te28WrWord(unsigned short wAxisNum, short sCmd,
  unsigned short wData);
short te28WrLong(unsigned short wAxisNum, short sCmd,long lData);
short te28WrServo(unsigned short wAxisNum, short sCmd,
  short sDataWords, unsigned short *pwData);
short te28WrapAround(unsigned short wAxisNum);
