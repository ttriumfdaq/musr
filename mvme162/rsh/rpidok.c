
#include <stdio.h>
#include <fcntl.h>
#include <vxWorks.h>
#include <remLib.h>

int
rpidok( char* host, int pid, const char* ros )
{
    char user[MAX_IDENTITY_LEN];
    char passwd[MAX_IDENTITY_LEN];
    int fd;
    char buf[512];
    int nbytes;
    int found;
    char cmd[64];
    char pidstr[16];
    char os[16];

    if( ros == NULL )
    {
        strcpy( os, "bsd" );
    }
    else
    {
        strcpy( os, ros );
    }

    remCurIdGet( user, passwd );

    if( strcmp( os, "bsd" ) == 0 )
    {
        sprintf( cmd, "ps %d", pid );
        if( pid <= 9999 )
        {
            sprintf( pidstr, " %d ", pid );
        }
        else
        {
 	    sprintf( pidstr, "%d ", pid );
        }
    }
    else if( strcmp( os, "vms" ) == 0 )
    {
        sprintf( cmd, "write sys$output f$getjpi(\"%X\",\"PID\")", pid );
        sprintf( pidstr, "%08X", pid );
    }

    fd = rcmd( host, 514, user, user, cmd, NULL );
    if( fd == ERROR )
    {
        fprintf( stderr, "error issuing rcmd\n" );
        return( 0 );
    }

    found = FALSE;
    buf[0] = '\0';
    while( ( nbytes = read( fd, buf, 512 ) ) != ERROR )
    {
	if( strstr( buf, pidstr ) != NULL ) 
	{
	    found = TRUE;
            break;
	}
        buf[0] = '\0';
        if( nbytes == 0 ) break;
    }

    close( fd );

    return( found );
}

