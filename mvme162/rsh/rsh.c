
#include <stdio.h>
#include <vxWorks.h>
#include <remLib.h>

int
rsh( char* host, char* cmd )
{
    char user[MAX_IDENTITY_LEN];
    char passwd[MAX_IDENTITY_LEN];
    int fd;
    char buf[512];
    int nbytes;
    int found;

    remCurIdGet( user, passwd );

    fd = rcmd( host, 514, user, user, cmd, NULL );
    if( fd == ERROR )
    {
      fprintf( stderr, "error issuing rcmd\n" );
    }

    found = FALSE;
    while( ( nbytes = read( fd, buf, 512 ) ) != ERROR )
    {
      puts( buf );
      if( nbytes == 0 ) break;
    }

    close( fd );
}

