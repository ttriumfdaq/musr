/*
 * vxRsh.h
 *
 * HISTORY:
 * 09/01/89 Benny Schnaider: Created.
 */

#define	VXRSH_PORT_NUM                  514
#define	VXRSH_PRIORITY                   90   
#define	VXRSH_STACK_SIZE              12000
#define	SHELL_STACK_SIZE              10000	
#define BUFF_LEN                        512
#define VXRSH_TO                         20 
#define VXRSH_VAR              "VXRSH_VALUE"  /* Environemt variable for */
                                              /* returns values from the */
                                              /* target. */
#define VXRSH_CMD_OPTIONS        "behl:rstw"  /* Command option for vxRsh */
#define FILE_NAME_LENGTH                100   /* For log files */

/*
 * Compile directives
 */
#ifdef  NEVER_DEF
#define VXRSH_DEBUG      /* For module debugging */
#define VXRSH_RESET      /* BSP support HW reset i.e., reset() */
#define VXRSH_VXWORK_4.X /* Define this if you can NOT link to execute */
#endif /* NEVER_DEF */

#define VXRSH_TEST       /* For model testing */
#define VXRSH_SP         /* Spawn each command and wait for new connections */

#ifndef OK
#define OK     0
#endif /* OK */
#ifndef ERROR
#define ERROR -1
#endif /* ERROR */
#ifndef LOCAL
#define LOCAL static
#endif /* LOCAL */
#ifndef STATUS
#define STATUS int
#endif /* STATUS */

/*
 * Return codes.
 * The return codes can be read by using the default retun value from the
 * Shell. If the value is OK (0) the actual value return from the target
 * can be read from shell VXRSH_VAR varaible.
 */
#define VXRSH_ERR_OK                 0   /* No errors */
#define VXRSH_ERR_SYNTAX             1   /* Illegal syntax */
#define VXRSH_ERR_OPEN               2   /* Can not establish connection */
#define VXRSH_ERR_IO                 3   /* Can not read or write */
#define VXRSH_ERR_INUSE              4   /* Traget is already in use. */
#define VXRSH_ERR_EXEC               5   /* Remote execution error */
#define VXRSH_ERR_ABORT              6   /* Operation aborted */
#define VXRSH_ERR_DECODE             7   /* Unable to decode return value */
#define VXRSH_ERR_TO                 8   /* Time out error */
#define VXRSH_ERR_CLIENT_CMD         9   /* Illegal client protocol command */
#define VXRSH_ERR_CLIENT_EXEC       10   /* Cleint execution error */
#define VXRSH_ERR_TARGET            20   /* Target returns ERROR (-1) */

#define VXRSH_SEARCH_PEER_LOW    4
#define VXRSH_SEARCH_PEER_HIGH  12

/*
 * Commands
 * This are primitive commands send from the server to the client at
 * the begining of the session.
 */
#define VXRSH_CMD_START_LOG   "SLG"     /* Start log file */
#define VXRSH_CMD_END_LOG     "ELG"     /* End log file */
#define VXRSH_CMD_HW_RESET    "HRS"     /* Hardware reset */
#define VXRSH_CMD_SW_RESET    "SRS"     /* Software reset */
#define VXRSH_CMD_SESSION     "SES"     /* vxRsh seesion - like rshd */
#define VXRSH_CMD_BYPASS      "BPS"     /* Bypass vxWorks shell */
#define VXRSH_CMD_WHO         "WHO"     /* Find who is locking the shell */

#define VXRSH_CMD_LEN            3      /* 3 char per command lexim */
#define VXRSH_MAX_CMD_ARG       11      /* Do not touch it unless you modify *
					/* the code */

/*
 * External routines
 */
extern vxRsh(/* hostName, command, protocolCommand */); /*
**********************************************************/
/* char *hostName;          */ /* Target name */
/* char *command;           */ /* VxWorks shell command */
/* char *protocolCommand;   */ /* e.g. VXRSH_CMD_SESSION */

extern void vxRshFilterLine(/* inBuff */); /*
*********************************************/
/* char *inBuff;   */ /* Input buffer */

extern vxRshd(); /*
*******************/

extern STATUS vxRshdInit(/* stackSize */); /*
*********************************************/
/* int stackSize; */  /* Stack size to be used for the server */

extern void vxRshdTest(); /*
****************************/

extern STATUS vxRshConnectWho(/* buffIn, rangeLow, rangeHigh */); /*
********************************************************************/
/*  char *buffIn;    */ /* Output buffer */
/*  int   rangeLow;  */ /* Lower limit for FD search */
/*  int   rangeHigh; */ /* Higher limit for FD search */
