/* vxRshd.c - vxRsh server source */

/*
modification history
 * vxRshd.c - mostly re-written by Graham Heyes from an original idea 
 *            taken from the VxWorks archives
 *
 * This module implements the server running on the target.
 *
 * ROUTINES:
 *  E vxRshd()               - The daemon - Must be spawned during boot.
 *  E vxRshdInit()           - Init routine.
 */

#include <vxWorks.h>
#include <socket.h>
#include <sysLib.h>
#include <in.h>
#include <stdioLib.h>
#include <symLib.h>
#include <sysSymTbl.h>
#include <a_out.h>
#include <ioLib.h>
#include <taskLib.h>
#include "vxRsh.h"
#include <ctype.h>
#include <sysLib.h>
#include <string.h>
/*
 * Forward declaration
 */
STATUS vxRshd();
static STATUS vxRshdBPS();
LOCAL STATUS vxRshdDecode();
LOCAL char usernames[40][40];
LOCAL int nusernames;

STATUS vxRshd()
{
  static int          sock = -1;	/* socket fd's */
  int		      snew;	        /* socket fd's - per connection */
  struct sockaddr_in  serverAddr;	/* server's address */
  struct sockaddr_in  clientAddr;	/* client's address */
  int		      clientLen;	/* length of clientAddr */
  FILE *user_file;
  char name[40];

 /*  strcpy(name,"~"); */
  strcpy(name,"/tools/.rhosts");
/*  strcat(name,sysBootParams.usr); 
  strcat(name,"/tools");
*/  
  
  /*
   * Check if module is initialized
   */
  if (sock != -1) {
    printf("vxRshd: Module is already initialized\n");
    return(ERROR);
  } /* init check */

  nusernames=0;
  if ((user_file = fopen(name,"r")) != NULL) {
    printf("vxRshd: Valid usernames in /tools/.rhosts : \n");
    while(fscanf(user_file, "%s",usernames[nusernames])>0) {
       printf("user %s\n",usernames[nusernames]);
      nusernames++;
    }
  }
  else  printf("vxRshd: can't open /tools/.rhosts - no valid usernames\n");
  fclose(user_file);

  /*
   * Zero out the sock_addr structures.
   * This MUST be done before the socket calls.
   */
  bzero ((char *)&serverAddr, sizeof (serverAddr));
  bzero ((char *)&clientAddr, sizeof (clientAddr));

  /*
   * Open the socket.
   * Use ARPA Internet address format and stream sockets.
   * Format described in "socket.h".
   */
  if ((sock = socket (AF_INET, SOCK_STREAM, 0)) == ERROR) {
    perror("vxRshd: Can not create a socket.\n");
    printErrno(errnoGet());
    return(ERROR);
  }
  
  /*
   * Set up our internet address, and bind it to enable connect.
   */
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port   = VXRSH_PORT_NUM;
  if (bind(sock, (SOCKADDR *)&serverAddr, sizeof (serverAddr)) == ERROR) {
    perror("vxRshd: bind failed\n");
    printErrno(errnoGet());
    close(sock);
    return(ERROR);
  }
  
  /*
   * Listen, for the client to connect to us.
   */

  if (listen (sock, 2) == ERROR) {
    perror("vxRshd: listen failed\n");
    printErrno(errnoGet());
    close (sock);
    return(ERROR);
  }
  
  /*
   * Receive new Clients
   */
  FOREVER {
    clientLen = sizeof (clientAddr);
    snew = accept (sock, (SOCKADDR *)&clientAddr, &clientLen);
    if (snew == ERROR) {
      perror("vxRshd: accept failed\n");
      printErrno(errnoGet());
      break;
    }
    
    /*
     * Decode the command
     */
    if (vxRshdDecode(snew, &clientAddr) == ERROR) {
      continue; 
    }
    close(snew);
  }
  
  close(sock);
  return(ERROR);
  
} /* vxRshdr() */


STATUS vxRshdInit(stackSize)
     int stackSize;   /* Stack size to be used for the server */
{
  int a1,a2,a3,a4,a5,a6,a7,a8,a9,a10;
  stackSize = (stackSize == 0) ? VXRSH_STACK_SIZE : stackSize;
  
  if (taskSpawn("vxRshd", VXRSH_PRIORITY, VX_STDIO, stackSize, vxRshd
		,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10 )
      == ERROR) 
    return(ERROR);
  else 
    return(OK);
  
} /* vxRshdInit() */

LOCAL void getstr(int s, char *buf, int cnt)
{
  char c;

  do {
    if (read(s, &c, 1) != 1) 
      exit(1);
    *buf++ = c;
    if (--cnt == 0) 
      exit(1);
  } while (c != 0);
}

/**************************************************************************
* vxRshdSession()
*
* This routine reads lines from the input stream. Each line is passed
* to execute(2). The global STDOUT is redirected to clntSock. This
* enables the peer to read the results back from the target.
*
* RETURN
* OK/ERROR. ERROR on socket I/O errors.
*
*/
LOCAL STATUS vxRshdSession(clntSock)
  int clntSock;  /* client socket */
{
  char command[BUFF_LEN];
  char buff[BUFF_LEN];
  int  oldStdOut, n, index;
  char c;
  STATUS st = OK;

  index = 0;
  while ((n = read(clntSock, &c, 1)) == 1) {
    if ((c == '\n') || (c == '\0')) {
      command[index] = '\0';
      (void)printf("vxRshd: executing: %s\n", command);
      if (execute(command) == ERROR) {
        (void)printf(buff, "\nRemote execution error in command %s\n", command);
        printErrno(errnoGet());
        (void)printf("value = -1 = 0xffffffff (server - %d)\n", VXRSH_ERR_EXEC);
	st = ERROR;
        break;
      } /* error while executing command */
      index = 0;
      if (c == '\0')
        break;
    } /* end of line was detected */
    else {
      /*
       * Ignore leading space blanks and tabs...
       */
      if ((index == 0) && ((c == ' ') || (c == '\0') || (c == '\t')))
        continue;
      command[index] = c;
      index++;
    } /* not end of line */
  } /* read from socket */
  
  if (n == -1) {
    perror("vxRshd: Can not read from server.\n");
    printErrno(errnoGet());
    st = ERROR;
  } /* if read error */

  /*
   * Release the shell
   */
  if (shellLock(FALSE) == FALSE) {
    perror("vxRshd: Can not release the shell\n");
    st = ERROR;
  }

  return(st);

} /* vxRshdSession() */



/* vxRshdCheck()
*
* Check if a new shell can be spawned, mainly, check that
* no one is using the shell (rlogin telent vxRshd).
* If the shell can be spawned:
*   - Lock the shell access
*   - Kill the existing shell (if any)
* If the shell is locked:
*   - Find the peer which is locking the shell.
*   - Send a message on the client socket consisting of the problem and the
*     IP address of the peer. If a peer was not found UNKNOWN is sent.
*
* RETURN
*  OK/ERROR
*
*/
LOCAL STATUS vxRshdCheck(clntSock)
  int clntSock; /* Client socket */
{
  int shellId;
  STATUS st;
  char buff[BUFF_LEN];
  char buff1[BUFF_LEN];

  if (shellLock(TRUE) == FALSE) {
    return(ERROR);
  } /* Can not lock the shell */

  if ((shellId = taskNameToId("shell")) == -1)
    return(OK);
  else
    if (taskDelete(shellId) == -1) {
      return(ERROR);
    } /* Can not delete the shell */

  return(OK);

} /* vxRshdCheck() */


/***************************************************************************
 * vxRshdBPS
 * Execute a command without using vxWorks shell - Bypass the shell.
 * The idea is to decode the line in the input line. For each string:
 *   If it is a number: convert it to binary
 *   If it is a name  : Look for it in the symbol table.
 *   If it is a string: Pass it as is (the address)
 * This is a "poor imitation" of the shell. It is designed to overcome
 * the fact the the shell is not reentrant.
 * Up to 10 arguments are parsed.
 *
 * RETURN
 * OK/ERROR
 * In addition it prints the return line just like the shell does.
 */
LOCAL STATUS vxRshdBPS(clientSock)
     int clientSock;   /* client socket */
{
  STATUS st = OK;
  char buff[BUFF_LEN + 1];
  char buff1[BUFF_LEN + 1];
  char *cmdArgStr[VXRSH_MAX_CMD_ARG];
  char *cmdArgVal[VXRSH_MAX_CMD_ARG];
  int   i, val, oldStdOut;
  int   stringc = 0;
  register char *s, *p;
  char *pAdrs;
  SYM_TYPE type;
  
  /*
   * Read the command line from the input
   */
  
  i = 0;
  while ((i < BUFF_LEN) && (read(clientSock, &buff[i], 1) == 1)) {
    if ((buff[i] == '\n') || (buff[i] == '\0')) {
      buff[i] = '\0';
      break;
    }
    i++;
  } /* Read input */

  buff[BUFF_LEN - 1] = NULL;
  
  /*
   * Split the line into strings ignore ','
   */

  s = buff;
  stringc = 0;

  while (*s != NULL) {

    stringc = 0;

    for (i = 0; i < VXRSH_MAX_CMD_ARG; i++) {
      cmdArgStr[i] = NULL;
      cmdArgVal[i] = NULL;
    } /* for cmdArg[] */
    
    while ((*s != NULL) && (*s != ';') && (stringc < VXRSH_MAX_CMD_ARG)) {
      
      /*
       * ignore white spaces and ','
       */
      while ((*s == ' ') || (*s == '\t') || (*s == ',')) {
	s++; 
      }
      
      /*
       * update the pointer of the substring in the array
     */
      cmdArgStr[stringc] = s;
      
      /* find the end of the current substring */
      while ((*s != ' ') && (*s != '\t') && (*s != NULL) &&(*s != ';') && (*s != ',')) {
	if (*s == '\"') {
	  s++;
	  while ((*s) && (*s != '\"')) {
	    if (*s == '\\') {
	      switch(*(s + 1)) {
	      case 'n': *s = '\n'; break;
	      case 't': *s = '\t'; break;
		defualt : *s = *(s + 1);
	      } /* switch(*s) */
	      p = s + 1;
	      while((*p) && ((p - buff) < BUFF_LEN)) {
		*p = *(p + 1);
		p++;
	      } /* Shift the rest of the string */
	    } /* if (*s == '\\') */
	    s++;
	  } /* While in string */
	  break;
	} /* A string */
	else {
	  s++;
	} /* Non string */
      } /* Continue reading */
      
      /*
       * If this is not the end of the string
       * terminate the substring with a NULL
       */
      if ((cmdArgStr[stringc][0] != NULL) && (strcmp(cmdArgStr[stringc], ","))) {
	stringc++;
	if (*s == ';') {
	  *s++ = NULL;
	  break;
	} else
	  *s++ = NULL;
      }
      
    }
    
    /* While more to tokens */
    
    /*
     * Parse each string
     */
    for (i = 0; ((st == OK) && (i < stringc)); i++) {
      /*
       * Check for symbol in the symbol table
       */
      (void)sprintf(buff1, "_%s", cmdArgStr[i]);
      if (symFindByName(sysSymTbl, cmdArgStr[i], &pAdrs, &type) == OK) {
	cmdArgVal[i] = pAdrs;
      }
      else if (symFindByName(sysSymTbl, buff1, &pAdrs, &type) == OK) {
	cmdArgVal[i] = pAdrs;
      }
      /*
       * Check for a string
       */
      else if ((cmdArgStr[i][0] == '\"') &&
	       (cmdArgStr[i][strlen((char *)cmdArgStr)] == '\"')) {
	cmdArgVal[i] = &cmdArgStr[i][1];
	cmdArgStr[i][strlen((char *) cmdArgStr[i])] = NULL;
      } /* Check for string */
      
      /*
       * Check for a number
       */
      else if (sscanf(cmdArgStr[i], "%i", &cmdArgVal[i])) {
      } /* Check for a number */
      
      /*
       * Error
       */
      else {
	(void)printf("undefined symbol: %s\n", cmdArgStr[i]);
	st = ERROR;
      } /* Error */
      
      /*
       * The first one MUST be a TEXT type
       */
      if ((i == 0) && (st == OK) && (type != (N_EXT | N_TEXT))) {
	(void)printf("undefined routine: %s\n", cmdArgStr[i]);
	st = ERROR;
      } /* Check all input strings */
    } /* Parse each parameter */
    
    /*
     * Trigger the call
     */
    printf ("Executing : ");
    for(i=0;i<stringc;i++)       
      printf("%s ",cmdArgStr[i]);
    printf("\n");

    if (st == OK) {
      val = (int)(((FUNCPTR)cmdArgVal[0])
		  (cmdArgVal[1], cmdArgVal[2], cmdArgVal[3],
		   cmdArgVal[4], cmdArgVal[5], cmdArgVal[6],
		   cmdArgVal[7], cmdArgVal[8], cmdArgVal[9],
		   cmdArgVal[10]));
    } /* Command trigger */
    
    /*
     * Set the return value
     */
    if (st == OK) {
      (void)printf("value = %d = 0x%x\n", val, val);
    } /* set the return value */
  }
  return(st);
  
} /* vxRshdBPS() */


/***************************************************************************
* vxRshdSES
* Execute a shell command(s) by calling execute(2) and redirecting
* STDOUT. Not that the shell must be unlocked during the entire session.
* Before exit restart the shell.
*
* RETURN
* ERROR if can not lock the shell.
*/
LOCAL STATUS vxRshdSES(clientSock)
  int clientSock;   /* client socket */
{
  STATUS st = OK;

  /*
   * Check if shell is locked...
   */
  if (vxRshdCheck(clientSock) == ERROR) {
    /* is shell in use */
    printf("Shell is locked, running minimal shell\n");
    vxRshdBPS (clientSock);
  } else {
    /*
     * Start the vxRsh session
     */
    
    st = vxRshdSession(clientSock);  
    /*
     * Restart the shell and return
     */
    shellInit(0, TRUE);
  }
  return(st);
  
} /* vxRshdSES() */

/**************************************************************************
* vxRshdDecode()
*
* Decode the command coming from the host.
*
* RETURN
* OK/ERROR. ERROR if:
*/
/*ARGSUSED*/
LOCAL STATUS vxRshdDecode(clntSock, clntAddr)
  int                 clntSock;   /* Client socket to read from */
  struct sockaddr_in *clntAddr;   /* Client address - for debug */
{
  char   username[BUFF_LEN];
  char   command[BUFF_LEN];
  char   buff[BUFF_LEN];
  STATUS st = ERROR;
  int    i,sock2,oursecport;
  char port[5];
  short theport;
  int std_out,std_err;
  char c=0;
  int found = FALSE;
  
  std_out = ioTaskStdGet(0, STD_OUT);
  std_err = ioTaskStdGet(0, STD_ERR);

  /*
   * Read the command from the stream
   */
  iam(sysBootParams.usr,0);

  command[0] = NULL;
  read(clntSock, port, 5);
  sscanf(port,"%d",&i);
  theport = i;
  
  oursecport = IPPORT_RESERVED -1;
  
  sock2 = rresvport(&oursecport);
  
  clntAddr->sin_port = htons(theport);
  if(connect(sock2,(struct sockaddr *) clntAddr, sizeof(struct sockaddr_in)) < 0) {
    perror ("connect failed : ");
    return (ERROR);
  }
  getstr(clntSock, username, BUFF_LEN);
  getstr(clntSock, command, BUFF_LEN);
  write(clntSock, &c, 1);
  for (i=0;i<nusernames;i++)
  {
/*    printf("usernames[%d] = %s - %s\n",i,usernames[i],username); */
    if (strcmp(username,usernames[i]) == 0)
    {
      found = TRUE;
      break;
    }
  }
  
  if (found) {
    ioTaskStdSet (0, STD_OUT, clntSock);    /* set std out for execute() */
    ioTaskStdSet (0, STD_ERR, clntSock);    /* set std out for execute() */
    vxRshdSES (clntSock);
    close (sock2);
    ioTaskStdSet (0, STD_OUT, std_out);
    ioTaskStdSet (0, STD_ERR, std_err);
    return(OK);
  } else {
    ioTaskStdSet (0, STD_OUT, clntSock);    /* set std out for execute() */
    printf(" Authentication failed\n");
    ioTaskStdSet (0, STD_OUT, std_out);  
    printf(" Authentication failed\n");
    return(ERROR);  
  }
}
