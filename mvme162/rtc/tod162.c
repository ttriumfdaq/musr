/*
 *	$Id: tod162.c,v 1.2 2000/12/22 21:57:48 David.Morris Exp $
 *
 *	$Revision: 1.2 $
 *
 */

/*
	Purpose: Motorola MVME162 specific time-of-day routines

revision history
----------------
1.1, 1 Jun 1998,dbm      Made Y2K compliant. Good to 2038 when time_t seconds
                       count overflows, but I'll be an old man then...

1.0, 22 Apr 94,dbm     Copied from tod133.c by Richard E. K. Neitzel 

	These routines are used to supply the hardware interface for the
	generic routines found in tod.c. They can be called directly if
	the overhead of an extra function call is too great.

	Details:

		The real time clock (RTC) on the MVME162 is the SGS-Thomson
        MK48T08. It consists of 8 8-bit registers, one
	for control and the rest for time keeping. The date and time 
	registers are in BCD format. Each unit, such as minutes, uses one
	byte, the most significant nibble for tens and the LS nibble for units.
        The Control register turns the clock updates on and off.

	The module is Y2K compliant but issues the year as a 2 digit value. The
	functions make the assumption that a year less than 98 must be greater than
	1999, and corrects accordingly.
*/

/*
 * $Log: tod162.c,v $
 * Revision 1.2  2000/12/22 21:57:48  David.Morris
 * Added CVS tags
 *
 *
 */

#include "vxWorks.h"
#include "timers.h"
#include "semLib.h"

#define TOD_CLOCK ((char *) 0xfffc1ff8)   /* Location of RTC base in Motorola MVME162 */

static char *todBase = TOD_CLOCK;         /* Base address of RTC registers */
static SEM_ID sidTod;			  /* Semaphore to control RTC access */
static char * todDays[7] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

/*
todInit
Purpose:  Initialize the onboard RTC. This simply creates a semaphore
	  to control access.
Inputs:   None
Precond:  None
Outputs:  None
Postcond: A sempahore is created for single task access to the library functions.
*/

void
todInit()
{
  static int flag = 0;	/* First time flag */
    
  if (!flag)		/* If first time */
  {
    sidTod = semBCreate(SEM_Q_FIFO, SEM_FULL);
    flag++;
  }
}


/*
todSet
Purpose:   Set the RTC.	This function tests all the time/date values for legality
           and if they are legal shuts off the clock, updates the registers and
	   then restarts the clock. The year must be four digits.
Inputs:    year - UINT value for year. Must be four digits
           month - UINT value for month (1 - 12)
	   date - UINT value for day of month (1 - 31)
	   day - UINT value for day of week (1 - 7, 1 is Mon)
	   hour - UINT value for hour (0 - 23)
	   minute - UINT value for minute (0 - 59)
	   second - UINT value for second (0 - 59)
Precond:   todInit must be successful
Outputs:   STATUS - OK on success else ERROR
Postcond:  If all data is valid, the RTC chip will be updated 
*/

STATUS 
todSet(UINT year, UINT month, UINT date, UINT day, UINT hour, UINT minute, UINT second)
{
    int stat = OK;
    char y_u, y, m_u, m, dt_u, dt, d_u, d, h_u, h, mn_u, mn, s_u, s;
    
    semTake(sidTod, WAIT_FOREVER);		/* Is it my turn? */
    
    if ((year < 1998) || (year > 2037))
      stat = ERROR;
    else
      {
	if (year > 1999) /* correct to two digits for clock chip */
	  year -= 2000;
	else
	  year -= 1900;

	  y_u = (char)(year % 10);
          y = ((((char)year - y_u) / 10) << 4) | y_u;
      }
    
    if (month > 12)
      stat = ERROR;
    else
      {
	  m_u = (char)(month % 10);
	  m = ((((char)month - m_u) / 10) << 4) | m_u;
      }

    switch (month)	/* Thirty days hath September....*/
      {
	case 4:
	case 6:
	case 9:
	case 11:
	  if (date > 30)
	    stat = ERROR;
	  break;
	  
	case 2:
	  if (date > 29)
	    stat = ERROR;
	  break;
	  
	default:
	  if (date > 31)
	    stat = ERROR;
	  break;
      }
    
    if ((day < 1 || day > 7))
      stat = ERROR;
    else
      {
	  d = (char)(day & 0x7);
      }
    
    if (hour > 23)
      stat = ERROR;
    else
      {
	  h_u = (char)(hour % 10);
	  h = ((((char)hour - h_u) / 10) << 4) | h_u;
      }
    
    if (minute > 59)
      stat = ERROR;
    else
      {
	  mn_u = (char)(minute % 10);
	  mn = ((((char)minute - mn_u) / 10) << 4 ) | mn_u;
      }
    
    if (second > 59)
      stat = ERROR;
    else
      {
	  s_u = (char)(second % 10);
	  s = ((((char)second - s_u) / 10) << 4) | s_u;
      }
    
    if (stat == OK)
      {
	  dt_u = (char)(date % 10);
	  dt = ((((char)date - dt_u) / 10) << 4) | dt_u;
	  
	  *(todBase) = 0x80;  /* stop updates for write */
	  
	  *(todBase + 1) = s;
	  *(todBase + 2) = mn;
	  *(todBase + 3) = h;
	  *(todBase + 4) = d;
	  *(todBase + 5) = dt;
	  *(todBase + 6) = m;
	  *(todBase + 7) = y;
	  
	  *(todBase) = 0x0;  /* resume updates */
      }
    
    semGive(sidTod);	/* Next! */
    
    return(stat);
}


/*
todGet
Purpose:   Read RTC. This function reads the RTC.
Inputs:    year - UINT pointer to value for year. Must be four digits
           month - UINT pointer to value for month (1 - 12)
	   date - UINT pointer to value for day of month (1 - 31)
	   day - UINT pointer to value for day of week (1 - 7, 1 is Mon)
	   hour - UINT pointer to value for hour (0 - 23)
	   minute - UINT pointer to value for minute (0 - 59)
	   second - UINT pointer to value for second (0 - 59)
Precond:   todInit must be successful
Outputs:   None
Postcond:  Date info is returned in the passed locations
*/

void
todGet(UINT * year, UINT * month, UINT * date, UINT * day, UINT * hour, UINT * minute, UINT * second)
{
  char s, mn, h, d, dt, m, y;
  
  semTake(sidTod, WAIT_FOREVER);	/* Is it my turn? */

  *(todBase) = 0x40;  /* Stop updates for read */

  s = *(todBase + 1);
  mn = *(todBase + 2);
  h = *(todBase + 3);
  d = *(todBase + 4);
  dt = *(todBase + 5);
  m = *(todBase + 6);
  y = *(todBase + 7);
    
  *(todBase) = 0x00;  /* resume updates */

/* Now we must rebuild real times from the units/tenths form. 
   Note that we must mask off never legal bits from the 
   registers, for despite claims, these may show up as set. */
    
  *second = (UINT)(((s & 0x70) >> 4) * 10 + (s & 0xf));
  *minute = (UINT)(((mn & 0x70) >> 4) * 10 + (mn & 0xf));
  *hour = (UINT)(((h & 0x30) >> 4) * 10 + (h & 0xf));
  *day = (UINT)(d & 0x7);
  *date = (UINT)(((dt & 0x30) >> 4) * 10 + (dt & 0xf));
  *month = (UINT)(((m & 0x10) >> 4) * 10 + (m & 0xf));

  *year = (UINT)(((y & 0xf0) >> 4) * 10 + (y & 0xf));
  if (*year < 98)
    *year += 2000;
  else
    *year += 1900;
    
  semGive(sidTod);	/* Next user! */

  return;
}


/*
todUpdate
Purpose:   This function reads the RTC chip and updates UNIX time for VxWorks. There
           is no time-zone calculation in this implementation
Inputs:    None
Precond:   todInit must be successful
Outputs:   None
Postcond:  UNIX time info is updated
*/

void
todUpdate()
{
  struct tm tmTime;
  struct timespec tsTime;
  time_t Time;  
  char Buffer[32];
  size_t stBuffer;

/*
  Get battery backed real time clock time
*/     
  todGet(&tmTime.tm_year, &tmTime.tm_mon, &tmTime.tm_mday, &tmTime.tm_wday, 
         &tmTime.tm_hour, &tmTime.tm_min, &tmTime.tm_sec);


  tmTime.tm_year -= 1900;

/*
  BBCLOCK has offset for month and day of week
*/
  tmTime.tm_isdst = 0;
  tmTime.tm_mon -= 1;
  tmTime.tm_wday -= 1;

  Time = mktime(&tmTime);
  tsTime.tv_sec = Time;
  tsTime.tv_nsec = 0;

/*
  Set time in VxWorks. Maintained by system interrrupt
*/
  clock_settime(CLOCK_REALTIME, &tsTime);

  return;
}


/*
date
Purpose:   This function implements the UNIX date command
Inputs:    None
Precond:   None
Outputs:   None
Postcond:  UNIX time info is displayed on the console
*/

void date()
{
  struct timespec tsTime;
  char szBuffer[32];
  size_t stBuffer = 32;

  clock_gettime(CLOCK_REALTIME, &tsTime);
  ctime_r(&tsTime.tv_sec, szBuffer, &stBuffer);
  printf("%s\n", szBuffer);
}


/*
todShow
Purpose:   This function displayes the RTC chip date and time
Inputs:    None
Precond:   None
Outputs:   None
Postcond:  RTC chip time info is displayed on the console
*/

void todShow()
{
    UINT year, month, date, day, hour, minute, second;

    todGet(&year, &month, &date, &day, &hour, &minute, &second);

    printf("BBRAM Clock set to %02u-%02u-%04d %02u:%02u:%02u %s\n", 
           month, date, year, hour, minute, second, todDays[day - 1]);
}
