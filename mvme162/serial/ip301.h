/***************************** ip301Serial.h *******************************/
/*
MODIFICATION HISTORY
ver  date      by      modification description
---------------------  --------------------------------
01   07/21/94  mdg     Creation Date


DESCRIPTION
-----------

Header file that contains the MEMORY decode addresses for the IP
modules on the mvme162.  These numbers cannot be changed without
also changing the MMU configuration information in ?/vw/config/mv162/sysLib.c

Also, each address must be at least 64k apart - farther if
Initialize_IPIC is allocating more than 64k for each IP module. 

**********************************************************************/

#define IP301_BAD_ADDRESS       0x1

#define IPIC_ADDRESS            0xfffbc000
#define BAUD_CLK_FREQ           3686400                 /* 3.6864 MHz baud rate crystal */

#define N_IP301_CHANNELS        2

#define	IP_BASE_ADRS	        (0xfff58000)		/* Industry Packs base address */
#define IP_MEM_ADDR             (0xc0000000)            /* IP 8 M memory address block */

#define	INT_VEC_IP301		0xA0
#define INT_VEC_IP301_A_WR	INT_VEC_IP301 + 0x08
#define	INT_VEC_IP301_A_EX	INT_VEC_IP301 + 0x0a
#define INT_VEC_IP301_A_RD	INT_VEC_IP301 + 0x0c
#define INT_VEC_IP301_A_SP	INT_VEC_IP301 + 0x0e

#define	INT_VEC_IP301_B_WR	INT_VEC_IP301 + 0x00
#define	INT_VEC_IP301_B_EX	INT_VEC_IP301 + 0x02
#define	INT_VEC_IP301_B_RD	INT_VEC_IP301 + 0x04
#define	INT_VEC_IP301_B_SP	INT_VEC_IP301 + 0x06

