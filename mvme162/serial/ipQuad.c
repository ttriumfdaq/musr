/*--------------------------------------------------------------------*/
/*   Name: ipQuad.c                                                   */
/*                                                                    */
/*   Purpose: Greenspring IP-Quad SCC2698B (Serial Communications     */
/*   Controller) tty driver                                           */

/* Copyright 1984-1992 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*--------------------------------------------------------------------*/
/*   modification history                                             */
/*                                                                    */
/*   08-Feb-96 TW Added FIOPARITY, FIODATABITS and missing baud       */
/*   rates.                                                           */
/*                                                                    */
/*   99-10-19 DBM Added FIOSTOPBITS and general tidying of code       */
/*   layout                                                           */

/*--------------------------------------------------------------------*/
/*   DESCRIPTION: This is the driver for the GreenSpring IP-Quad      */
/*   SCC2698B SCC (Serial Communications Controller). It uses the     */
/*   SCCs in asynchronous mode only.                                  */
/*                                                                    */
/*   USER-CALLABLE ROUTINES: Most of the routines in this driver are  */
/*   accessible only through the I/O system. Two routines, however,   */
/*   must be called directly: ipQuadDrv() to initialize the driver,   */
/*   and ipQuadDevCreate() to create devices.                         */
/*                                                                    */
/*   Before the driver can be used, it must be initialized by         */
/*   calling ipQuadDrv(). This routine should be called exactly       */
/*   once, before any reads, writes, or calls to ipQuadDevCreate().   */
/*   Normally, it is called by usrRoot() in usrConfig.c.              */
/*                                                                    */
/*   Before a terminal can be used, it must be created using          */
/*   ipQuadDevCreate(). Each port to be used should have exactly one  */
/*   device associated with it by calling this routine.               */
/*                                                                    */
/*   IOCTL FUNCTIONS: This driver responds to the same ioctl() codes  */
/*   as a normal tty driver; for more information, see the manual     */
/*   entry for tyLib. Available baud rates range from 50 to 38400.    */
/*                                                                    */
/*   SEE ALSO: tyLib                                                  */


#include "vxWorks.h"
#include "iv.h"
#include "ioLibx.h"  /* Add private ioctl functions, includes ioLib.h */
#include "iosLib.h"
#include "taskLib.h"
#include "tyLib.h"
#include "intLib.h"
#include "errnoLib.h"
#include "drv/serial/scc2698.h"
#include "ipQuad.h"

/* #define DEBUG */
/* #define DEBUG_STOP */

#define DEFAULT_BAUD	9600

LOCAL IP_QUAD_DEV ipQuadDv [N_IPQUAD_CHANNELS]; /* device descriptors */
LOCAL int ipQuadDrvNum;		/* driver number assigned to this driver */

/*   baudTable is a table of the available baud rates, and the        */
/*   values to write to the clock select register to get those rates  */

LOCAL BAUD baudTable[]	=	{
  { 75, RX_CLK_75 | TX_CLK_75},
  { 110, RX_CLK_110 | TX_CLK_110},
  { 150, RX_CLK_150 | TX_CLK_150},
  { 300, RX_CLK_300 | TX_CLK_300},
  { 600, RX_CLK_600 | TX_CLK_600},
  { 1200, RX_CLK_1200 | TX_CLK_1200},
  { 1800, RX_CLK_1800 | TX_CLK_1800},
  { 2000, RX_CLK_2000 | TX_CLK_2000},
  { 2400, RX_CLK_2400 | TX_CLK_2400},
  { 4800, RX_CLK_4800 | TX_CLK_4800},
  { 9600, RX_CLK_9600 | TX_CLK_9600},
  { 19200, RX_CLK_19200 | TX_CLK_19200},
  { 38400, RX_CLK_38400 | TX_CLK_38400},
};

/*   parityTable is a table of available parity settings, and the     */
/*   corresponding bit values for mode register 1                     */

LOCAL PARITY parityTable[] = {
   {'N', PARITY_NO},
   {'O', PARITY_ODD},
   {'E', PARITY_EVEN},
   {'M', PARITY_MARK},
   {'S', PARITY_SPACE}
};

/* forward declarations */

LOCAL void   ipQuadStartup ();
LOCAL int    ipQuadOpen ();
LOCAL STATUS ipQuadIoctl ();
LOCAL void   ipQuadHrdInit ();
LOCAL void   ipQuadInitChannel ();
LOCAL void   ipQuadResetChannel ();
LOCAL void   ipQuadInt ();


/*--------------------------------------------------------------------*/
/*   Name: ipQuadDrv                                                  */
/*                                                                    */
/*   Purpose: initialize the serial driver for the following          */
/*   Industry Packs:                                                  */
/*                                                                    */
/*   Greenspring IP-Quad                                              */
/*                                                                    */
/*   This routine initializes the serial driver, sets up interrupt    */
/*   vectors, and performs hardware initialization of the serial      */
/*   ports.                                                           */
/*                                                                    */
/*   This routine should be called exactly once, before any reads,    */
/*   writes, or calls to ipQuadDevCreate(). Normally, it is called    */
/*   by usrRoot() in usrConfig.c.                                     */
/*                                                                    */
/*   RETURNS: OK, or ERROR if the driver cannot be installed.         */
/*                                                                    */
/*   SEE ALSO: ipQuadDevCreate()                                      */

STATUS ipQuadDrv( char cPosition )
{
  char * pcIPBase;
  char * pcIPProm;
  unsigned char * pcIPMem;
  unsigned char szBuffer[16];
  int i;
  
  /*   check if driver already installed         */

  if (ipQuadDrvNum > 0)
  {
    return (OK);
  }
  
  /*   Determine location of IP specified in     */
  /*   cPosition                                 */

  cPosition -= 'A';
  if (cPosition > 3)
  {
    cPosition -= 0x20;
  }

  if ((cPosition > 3) || (cPosition < 0))
  {
    return (IPQUAD_BAD_ADDRESS);
  }
    
  /*   Identify address of specified IP          */
  /*   location                                  */

  pcIPBase = (cPosition * 0x100) + (char *) IP_BASE_ADRS;

  /*   Check ID PROM data                        */

  pcIPProm = pcIPBase + 0x81;
  for( i = 0; i < 7; i++ )
  {
    szBuffer[i] = *pcIPProm;
    pcIPProm += 2;
  }
  if( szBuffer[0] != 'I' || szBuffer[1] != 'P' || szBuffer[2] != 'A' ||
      szBuffer[3] != 'C' || szBuffer[4] != 0xF0 || szBuffer[5] != 0x37 )
    {
      printf( "Invalid ID PROM data: '%c%c%c%c' Manufacturer:'0x%X' Model:'0x%X' Revision:'0x%X'\n", 
        szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
        szBuffer[4], szBuffer[5], szBuffer[6] );
      return( ERROR );
    }
  else
    {
      printf( "ID PROM data: '%c%c%c%c' Manufacturer '0x%X' Model '0x%X' Revision '0x%X'\n", 
        szBuffer[0], szBuffer[1], szBuffer[2], szBuffer[3], 
        szBuffer[4], szBuffer[5], szBuffer[6] );
    }

  /*   Set up memory block address for IP        */
  /*   location                                  */

  pcIPMem = (cPosition * 0x800000) + (unsigned char *) IP_MEM_ADDR;
  
  *((unsigned char *) (IPIC_ADDRESS + 0x04 + (cPosition * 2))) = (((unsigned long) pcIPMem) >> 24) & 0xff;
  *((unsigned char *) (IPIC_ADDRESS + 0x05 + (cPosition * 2))) = (((unsigned long) pcIPMem) >> 16) & 0xff;

  /* 64k size */
/* tw
  *((unsigned char *) (IPIC_ADDRESS + 0x0c + cPosition)) = 0x00;
*/
  /* 128k size */
  /*
   *  As suggested by GreenSpring Application Brief AN#93008
   * "Using Industry Packs on the Motorola MVME162"
   */

  *((unsigned char *) (IPIC_ADDRESS + 0x0c + cPosition)) = 0x7f; 

  /*
   *  General Control Registers look like:
   *     x_ERR   0  x_RT1   x_RT0   x_WIDTH1   x_WIDTH0   0   x_MEN
   *	   x_ERR: 1 = Assert IP's Error* signal.
   *	   x_RT1-x_RT0: Recovery Timer Delay 0, 2, 4, 8 useconds.
   *	   x_WIDTH1-x_WIDTH0: 00=32bits, 01=8bits, 10=16bits.
   *	   x_MEN: 1=Enable Memory Accesses to the IP
   *
   *  For "standard" IP modules, this should generally be:
   *     0 0 0 0  0 1 0 1  = 0x05  (enabled for byte-transfers)
   */

  *((unsigned char *) (IPIC_ADDRESS + 0x18 + cPosition)) = 0x05;

  /*
   * Set interrupt vector register value
   */

  *pcIPMem = (unsigned char) IP_INT_VEC;

#ifdef DEBUG
  printf("IP-Quad Vector register = 0x%X\n", *pcIPMem);
#endif

/* tw  This works, but should be 0x05 as set above.
  *((unsigned char *) (IPIC_ADDRESS + 0x18 + cPosition)) = 0x0;
*/

  /*   setup serial device descriptors           */

  ipQuadDv[0].numChannels = N_IPQUAD_CHANNELS;

  for (i = 0; i < N_IPQUAD_CHANNELS; i++)
  {    
    ipQuadDv[i].created  = FALSE;
    ipQuadDv[i].num      = i;
    ipQuadDv[i].mra      = pcIPBase + 1;
    ipQuadDv[i].sr       = pcIPBase + 3;
    ipQuadDv[i].csr      = pcIPBase + 3;
    ipQuadDv[i].cra      = pcIPBase + 5;
    ipQuadDv[i].dr       = pcIPBase + 7;
    ipQuadDv[i].ipcr     = pcIPBase + 9;
    ipQuadDv[i].acr      = pcIPBase + 9;
    ipQuadDv[i].isr      = pcIPBase + 11;
    ipQuadDv[i].imr      = pcIPBase + 11;
    ipQuadDv[i].mrb      = pcIPBase + 17;
    ipQuadDv[i].crb      = pcIPBase + 21;
    ipQuadDv[i].ip       = pcIPBase + 27;
    ipQuadDv[i].opcr     = pcIPBase + 27;
    ipQuadDv[i].baudFreq = BAUD_CLK_FREQ;

    pcIPBase += 32;
  }

  /*   Connect serial interrupts There is only   */
  /*   one interrupt routine for all channels    */
  /*   since only one int vector. Interrupt      */
  /*   service routine polls the SCC2698 for     */
  /*   cause                                     */

  intConnect( INUM_TO_IVEC(IP_INT_VEC), ipQuadInt, (unsigned int)ipQuadDv );

  /*
   * Now enable serial interrupts in IPIC to drive selected IP board
   *
   * Interrupt register looks like:
   *   PLTY       EL     INT    IEN    ICLR     IL2     IL1      IL0
   *    PLTY: 1 = Rising Edge/High Level causes interrupts
   *	EL: 1 = Edge Sensitive
   *	INT: (READ ONLY) 1 = Interrupt being generated
   *	IEN: 1 = Interrupts enabled
   *	ICLR: 1 = Clear edge interrupt.  No meaning for level-ints.
   *	IL2-IL0: Level at which IP should interrupt.
   *
   *  For interrupting IP's, this should generally be:
   *         0 0 0 1  0 1 0 0 = 0x14  (interrupt low-level on #4)
   *  For non-interrupting IP's, this should be:
   *         0 0 0 0  0 0 0 0 = 0x00
   *
   */
/*
  *((unsigned char *) (IPIC_ADDRESS + 0x10 + cPosition * 2)) = 0x16;
  *((unsigned char *) (IPIC_ADDRESS + 0x11 + cPosition * 2)) = 0x16;
*/
  /*
   *  As suggested by GreenSpring Application Brief AN#93008
   * "Using Industry Packs on the Motorola MVME162"
   */

  *((unsigned char *) (IPIC_ADDRESS + 0x10 + cPosition * 2)) = 0x15-cPosition;
  *((unsigned char *) (IPIC_ADDRESS + 0x11 + cPosition * 2)) = 0x15-cPosition;

  ipQuadHrdInit();
  ipQuadDrvNum = iosDrvInstall( ipQuadOpen, (FUNCPTR) NULL, ipQuadOpen,
                               (FUNCPTR) NULL, tyRead, tyWrite, ipQuadIoctl );

  return( ipQuadDrvNum == ERROR ? ERROR : OK );
}


/*--------------------------------------------------------------------*/
/*   Name: ipQuadDevCreate                                            */
/*                                                                    */
/*   Purpose: create a device for an on-board serial port             */
/*                                                                    */
/*   This routine creates a device on a specified serial port. Each   */
/*   port to be used should have exactly one device associated with   */
/*   it by calling this routine.                                      */
/*                                                                    */
/*   For instance, to create the device "/tyCo/0", with buffer sizes  */
/*   of 512 bytes, the proper call would be:                          */
/*                                                                    */
/*   ipQuadDevCreate ("/tyCo/0", 0, 512, 512);                        */
/*                                                                    */
/*   RETURNS: OK, or ERROR if the driver is not installed, the        */
/*   channel is invalid, or the device already exists.                */
/*                                                                    */
/*   SEE ALSO: ipQuadDrv()                                            */

STATUS ipQuadDevCreate
(
  char *      name,           /* name to use for this device      */
  FAST int    channel,        /* physical channel for this device */
  int         rdBufSize,      /* read buffer size, in bytes       */
  int         wrtBufSize      /* write buffer size, in bytes      */
)
{
  FAST IP_QUAD_DEV *pipQuadDv;

  if (ipQuadDrvNum <= 0)
  {
  	errnoSet (S_ioLib_NO_DRIVER);
    return (ERROR);
  }

  /* if this doesn't represent a valid channel, don't do it */

  if (channel < 0 || channel >= ipQuadDv[0].numChannels)
    return (ERROR);

  pipQuadDv = &ipQuadDv[channel];

  /* if there is a device already on this channel, don't do it */

  if (pipQuadDv->created)
  	return (ERROR);

  /* initialize the ty descriptor */

  if (tyDevInit (&pipQuadDv->tyDev, rdBufSize, wrtBufSize, (FUNCPTR) ipQuadStartup) != OK)
    return (ERROR);

  /* initialize the channel hardware */

  ipQuadInitChannel (channel);

  /* mark the device as created, and add the device to the I/O system */

  pipQuadDv->created = TRUE;

  return (iosDevAdd (&pipQuadDv->tyDev.devHdr, name, ipQuadDrvNum));
}


/*--------------------------------------------------------------------*/
/*   ipQuadHrdInit - initialize the USART                             */

LOCAL void ipQuadHrdInit (void)
{
  FAST int   oldlevel;	/* current interrupt level mask */
  int        i;

  oldlevel = intLock ();	/* disable interrupts during init */

  for (i=0; i < ipQuadDv[0].numChannels; i++)
  {
    ipQuadResetChannel(i);	/* reset channel */
  }
  
  intUnlock (oldlevel);
}


/*--------------------------------------------------------------------*/
/*   ipQuadResetChannel - reset a single channel                      */

LOCAL void ipQuadResetChannel(int channel)
{
  volatile char *cra = ipQuadDv[channel].cra;        /* IPQUAD control reg adr */
  int delay;
  int zero = 0;

  *cra = RST_BRK_INT_CMD;
  *cra = RST_ERR_STS_CMD;
  *cra = RST_TX_CMD;
  *cra = RST_RX_CMD;
  *cra = RST_MR_PTR_CMD;
}


/*--------------------------------------------------------------------*/
/*   ipQuadInitChannel - initialize a single channel                  */

LOCAL void ipQuadInitChannel(int channel)
{
  FAST int          oldlevel;		/* current interrupt level mask */
  FAST IP_QUAD_DEV *pipQuadDv = &ipQuadDv[channel];
  
  oldlevel = intLock ();		/* disable interrupts during init */

  /*
   *  Disables interrupts for both channels (redundant?)
   */
  *pipQuadDv->imr = pipQuadDv->intMask = 0;

  /* Initialize registers */

  *pipQuadDv->cra = RST_TX_CMD;
  *pipQuadDv->cra = RST_RX_CMD;
  *pipQuadDv->cra = RST_BRK_INT_CMD;
  *pipQuadDv->cra = RST_ERR_STS_CMD;

  /* 
   * Set mode register.   By default no RTS/CTS.
   */
  *pipQuadDv->cra = RST_MR_PTR_CMD;
  *pipQuadDv->mra = PAR_MODE_NO | BITS_CHAR_8;   /* RX_RTS */
  *pipQuadDv->mra = STOP_BITS_1;                 /* CTS_ENABLE */

#ifdef DEBUG
  *pipQuadDv->cra = RST_MR_PTR_CMD;
  printf("mra1 = 0x%x ", (UINT8)*pipQuadDv->mra );
  printf("mra2 = 0x%x\n", (UINT8)*pipQuadDv->mra );
#endif /* DEBUG */

  *pipQuadDv->csr = RX_CLK_9600 | TX_CLK_9600;
  *pipQuadDv->acr = BRG_SELECT;
  
  *pipQuadDv->opcr = 0;

  /*
   *  Enable interrupts
   */
  *pipQuadDv->imr = pipQuadDv->intMask = RX_RDY_A_INT;
    
  /*
   *  Reset transmitter and receiver
   */
  *pipQuadDv->cra = RST_TX_CMD;
  *pipQuadDv->cra = RST_RX_CMD;

  /*
   *  Enable transmitter and receiver
   */
  *pipQuadDv->cra = TX_ENABLE | RX_ENABLE;

  /*
   *  Ready to receive
   */
  *pipQuadDv->cra = ASSERT_RTS;

  /*
   *  Set channel B
   */
  *pipQuadDv->crb = RST_MR_PTR_CMD;
  *pipQuadDv->mrb = PAR_MODE_NO | BITS_CHAR_8;
  *pipQuadDv->mrb = STOP_BITS_1;
  *pipQuadDv->crb = ASSERT_RTS;

#ifdef DEBUG
  *pipQuadDv->crb = RST_MR_PTR_CMD;
  printf("mrb1 = 0x%x ", (UINT8)*pipQuadDv->mrb );
  printf("mrb2 = 0x%x\n", (UINT8)*pipQuadDv->mrb );
#endif /* DEBUG */

  intUnlock (oldlevel);
}


/*--------------------------------------------------------------------*/
/*   ipQuadOpen - open file to USART                                  */

LOCAL int ipQuadOpen(IP_QUAD_DEV *pipQuadDv, char *name, int mode)
{
  return ((int) pipQuadDv);
}


/*--------------------------------------------------------------------*/
/*   ipQuadIoctl - special device control                             */
/*                                                                    */
/*   This routine handles FIOBAUDRATE, FIOPARITY FIODATABITS and      */
/*   FIOSTOPBITS requests and passes all others to tyIoctl().         */
/*                                                                    */
/*   RETURNS: OK, or ERROR if invalid baud rate, or whatever          */
/*   tyIoctl() returns.                                               */

LOCAL STATUS ipQuadIoctl(IP_QUAD_DEV *pipQuadDv, int request, int arg)
{
  FAST int     oldlevel;		/* current interrupt level mask */
  FAST int     baudConstant;
  FAST STATUS  status;
  volatile char *csr;			/* SCC control reg adr */
  int i;
  UINT8 oldValue;
  UINT8 old;

  switch (request)
  {
    case FIOBAUDRATE:
    {
    	status = ERROR;
    	for(i = 0; i < sizeof(baudTable)/sizeof(BAUD); i++)
      {
        if( baudTable[i].rate == arg )
        {
      		status = OK;	/* baud rate is valid */
    
#ifdef DEBUG
  printf( "Setting baud rate to %d\n", arg );
#endif /* DEBUG */
    
      		/* Disable transmitter and receiver */
      		*pipQuadDv->cra = TX_DISABLE | RX_DISABLE;
    
      		/* configure transmitter and receiver */
       		*pipQuadDv->csr = baudTable[i].csrVal;
    
      		/* Reset transmitter and receiver */
      		*pipQuadDv->cra = RST_TX_CMD;
      		*pipQuadDv->cra = RST_RX_CMD;
    
      		/* Enable transmitter and receiver */
      		*pipQuadDv->cra = TX_ENABLE | RX_ENABLE;
    
      		break;
        }
  	  }
      break;

    }
    case FIOPARITY:
    {
      status = ERROR;
    	arg = toupper( (char)arg );		/* Convert to upper case */
    	for( i = 0; i < sizeof(parityTable)/sizeof(PARITY); i++ )
      {
        if(parityTable[i].parity == (char) arg)		
        {
      		status = OK;
    
#ifdef DEBUG
  printf( "Setting parity to '%c'\n", arg );
#endif /* DEBUG */
    
      		/* Extract non parity bits from mode register 1 value */
      		*pipQuadDv->cra = RST_MR_PTR_CMD;
      		old = *pipQuadDv->mra;
      		oldValue = old & ~PARITY_BIT_MSK;
    
      		/*   Disable transmitter and receiver.         */

          *pipQuadDv->cra = TX_DISABLE | RX_DISABLE;

      		/* Set new parity */
      		*pipQuadDv->cra = RST_MR_PTR_CMD;
      		*pipQuadDv->mra = (UINT8)(oldValue | parityTable[i].parVal);
    
#ifdef DEBUG
  *pipQuadDv->cra = RST_MR_PTR_CMD;
  printf( "mra1 old = 0x%x, set = 0x%x, read = 0x%x\n",
          old, (UINT8)(oldValue | parityTable[i].parVal),
          (UINT8)*pipQuadDv->mra );
#endif /* DEBUG */
    
      		/* Reset transmitter and receiver */
          *pipQuadDv->cra = RST_TX_CMD;
          *pipQuadDv->cra = RST_RX_CMD;

      		/* Enable transmitter and receiver */
          *pipQuadDv->cra = TX_ENABLE | RX_ENABLE;

          /*   Do channel B                              */

          *pipQuadDv->crb = RST_MR_PTR_CMD;
          old = *pipQuadDv->mrb;
          oldValue = old & ~PARITY_BIT_MSK;
          *pipQuadDv->crb = RST_MR_PTR_CMD;
          *pipQuadDv->mrb = (UINT8)(oldValue | parityTable[i].parVal);
    
#ifdef DEBUG
  *pipQuadDv->crb = RST_MR_PTR_CMD;
  printf( "mrb1 old = 0x%x, set = 0x%x, read = 0x%x\n",
          old, (UINT8)(oldValue | parityTable[i].parVal),
          (UINT8)*pipQuadDv->mrb );
#endif /* DEBUG */

          break;
        }
  	  }
      break;
    }      
    case FIODATABITS:
    {
    	status = ERROR;
    	if( arg >= 5 && arg <= 8 )
      {
        status = OK;
      
#ifdef DEBUG
  printf( "Setting databits to %d\n", arg );
#endif /* DEBUG */
      
        /* Extract non data bits bits from mode register 1 value */
        *pipQuadDv->cra = RST_MR_PTR_CMD;
        old = *pipQuadDv->mra;
        oldValue = old & ~DATABITS_BIT_MSK;
      
        /*   Disable transmitter and receiver.         */

        *pipQuadDv->cra = TX_DISABLE | RX_DISABLE;

        /* Set new number of data bits value */
        *pipQuadDv->cra = RST_MR_PTR_CMD;
        *pipQuadDv->mra = (UINT8)(oldValue | (UINT8)(arg-5));

#ifdef DEBUG
  *pipQuadDv->cra = RST_MR_PTR_CMD;
  printf( "mra1 old = 0x%x, set = 0x%x, read = 0x%x\n",
          old, (UINT8)(oldValue | (UINT8)(arg-5)),
          (UINT8)*pipQuadDv->mra );
#endif /* DEBUG */

        /* Reset transmitter and receiver */
        *pipQuadDv->cra = RST_TX_CMD;
        *pipQuadDv->cra = RST_RX_CMD;

        /* Enable transmitter and receiver */
        *pipQuadDv->cra = TX_ENABLE | RX_ENABLE;

        /*   Do channel B                              */

        *pipQuadDv->crb = RST_MR_PTR_CMD;
        old = *pipQuadDv->mrb;
        oldValue = old & ~DATABITS_BIT_MSK;
        *pipQuadDv->crb = RST_MR_PTR_CMD;
        *pipQuadDv->mrb = (UINT8)(oldValue | (UINT8) (arg-5));
      
#ifdef DEBUG
  *pipQuadDv->crb = RST_MR_PTR_CMD;
  printf("mrb1 old = 0x%x, set = 0x%x, read = 0x%x\n",
          old, (UINT8)(oldValue | (UINT8)(arg-5)),
          (UINT8)*pipQuadDv->mrb );
#endif /* DEBUG */

        break;
      }
      break;
    }
    case FIOSTOPBITS:
    {
    	status = ERROR;
    	if( arg >= 1 && arg <= 2 )
      {
        status = OK;

        /* Access mode register 1 so auto advance to MR2 and grab data */

        *pipQuadDv->cra = RST_MR_PTR_CMD;
        old = *pipQuadDv->mra; /* read MR1 and point to MR2 */
        old = *pipQuadDv->mra; /* read MR2 */
        oldValue = old & ~STOPBITS_BIT_MSK;
      
        /*   Disable transmitter and receiver.         */

        *pipQuadDv->cra = TX_DISABLE | RX_DISABLE;

        /* Set new number of stop bits value */

        *pipQuadDv->cra = RST_MR_PTR_CMD;
        old = *pipQuadDv->mra; /* read MR1 and point to MR2 */
        *pipQuadDv->mra = (UINT8)(oldValue | (UINT8)(arg == 1 ? 0x7 : 0xf));

#ifdef DEBUG_STOP
  *pipQuadDv->cra = RST_MR_PTR_CMD;
  old = *pipQuadDv->mra;
  printf( "mra2 old = 0x%x, set = 0x%x, read = 0x%x\n",
          old, (UINT8)(oldValue | (UINT8)(arg-5)),
          (UINT8)*pipQuadDv->mra );
#endif /* DEBUG */

        /* Reset transmitter and receiver */

        *pipQuadDv->cra = RST_TX_CMD;
        *pipQuadDv->cra = RST_RX_CMD;

        /* Enable transmitter and receiver */

        *pipQuadDv->cra = TX_ENABLE | RX_ENABLE;
        
        /*   Do channel B                              */

        *pipQuadDv->crb = RST_MR_PTR_CMD;
        old = *pipQuadDv->mrb;
        old = *pipQuadDv->mrb;
        oldValue = old & ~STOPBITS_BIT_MSK;
        *pipQuadDv->crb = RST_MR_PTR_CMD;
        old = *pipQuadDv->mrb;
        *pipQuadDv->mrb = (UINT8)(oldValue | (UINT8)(arg == 1 ? 0x7 : 0xf));
      }
      break;
    }
    default:
    {
      status = tyIoctl (&pipQuadDv->tyDev, request, arg);
      break;
    }
  }
  return (status);
}

/*
 *  ipQuadStartup - transmitter startup routine
 *
 *  Call interrupt level character output routine.
 */
LOCAL void 
ipQuadStartup(IP_QUAD_DEV *pipQuadDv)
{
  char outChar;
  
  if( tyITx( &pipQuadDv->tyDev, &outChar ) == OK )
  {
    *pipQuadDv->dr = outChar;
    *pipQuadDv->imr = pipQuadDv->intMask |= TX_RDY_A_INT;
  }
}

/*
 * ipQuadInt - interrupt level processing
 *
 *  This routine handles interrupts from all SCC channels.
 *  Since there is only one interrupt vector for all channels we must look
 *  for the source of the interrupt in the SCC status registers
 */
void
ipQuadInt( IP_QUAD_DEV *pipQuadDv )
{
  FAST char        intStatus;
  char             outChar;
  char             inChar;
  int              i;
  char             ipcr;

/*
  We need to find out which channel interrupted.  Loop through the four
  Interrupt Status Registers and test the appropriate bits for true or false
  Remember that the only way we could get to this routine is by interrupts
  that are not masked by the IMR
*/  

#ifdef DEBUG
    printf( "Inside ipQuadInt\n" );
#endif /* DEBUG */

  for( i = 0; i < N_IPQUAD_CHANNELS; i++, pipQuadDv++ )
  {
    if( ( intStatus = *pipQuadDv->isr ) == 0 )
    {
      continue;
    }

    /*
     * If the device is not created then can't cause interrupt
     */        
    if( pipQuadDv->created == FALSE )
    {
/* tw      *pipQuadDv->imr = 0; */
      *pipQuadDv->imr = pipQuadDv->intMask &= ~(TX_RDY_A_INT | RX_RDY_A_INT);
      continue;
    }

#ifdef DEBUG
    printf( "Interrupt on channel %d, isr = 0x%x\n", i, intStatus );
#endif /* DEBUG */

    ipcr = *pipQuadDv->ip;
    
    if( intStatus & TX_RDY_A )
    {
      if( tyITx( &pipQuadDv->tyDev, &outChar ) == OK )
      {
        *pipQuadDv->dr = outChar;
#ifdef DEBUG
  printf( "Wrote character '%c' (0x%x)\n", outChar, outChar );
#endif /* DEBUG */
      }
      else
      {
        *pipQuadDv->imr = pipQuadDv->intMask &= ~TX_RDY_A_INT;
      }
    }

    /*
     *  For a read interrupt, read the whole buffer to empty it
     */
    while( *pipQuadDv->sr & RXRDY )
    {
      inChar = *pipQuadDv->dr;

#ifdef DEBUG
      printf( "Read character '%c' (0x%x)\n", inChar, inChar );
#endif /* DEBUG */

      tyIRd( &pipQuadDv->tyDev, inChar );
    }
  }
}  



/******

  Test program

****/

void
ipQuadTest(void)
{
  STATUS fdQuada, fdQuadb;
  STATUS fdQuadc, fdQuadd;
  STATUS iResult;
  long i, j;
  char szTemp1[64], szTemp2[64];
  char szTemp3[64], szTemp4[64];
    
  szTemp1[0] = 0x0;
  szTemp2[0] = 0x0;
  szTemp3[0] = 0x0;
  szTemp4[0] = 0x0;

  fdQuada = open("/tyCo/4", O_RDWR, 0);
  if (fdQuada == ERROR)
  {
    printf("Error opening serial port 4\n");
    exit(0);
  }
  iResult = ioctl(fdQuada, FIOBAUDRATE, 9600);
  if (iResult == ERROR)
  {
    printf("Error setting serial port 4 to 9600\n");
    exit(0);
  }

  fdQuadb = open("/tyCo/5", O_RDWR, 0);
  if (fdQuadb == ERROR)
  {
    printf("Error opening serial port 5\n");
    exit(0);
  }
  iResult = ioctl(fdQuadb, FIOBAUDRATE, 9600);
  if (iResult == ERROR)
  {
    printf("Error setting serial port 5 to 9600\n");
    exit(0);
  }
 
  fdQuadc = open("/tyCo/6", O_RDWR, 0);
  if (fdQuadc == ERROR)
  {
    printf("Error opening serial port 6\n");
    exit(0);
  }
  iResult = ioctl(fdQuadc, FIOBAUDRATE, 9600);
  if (iResult == ERROR)
  {
    printf("Error setting serial port 6 to 9600\n");
    exit(0);
  }
 
  fdQuadd = open("/tyCo/7", O_RDWR, 0);
  if (fdQuadd == ERROR)
  {
    printf("Error opening serial port 7\n");
    exit(0);
  }
  iResult = ioctl(fdQuadd, FIOBAUDRATE, 9600);
  if (iResult == ERROR)
  {
    printf("Error setting serial port 7 to 9600\n");
    exit(0);
  }
 
  printf("Starting writes and reads\n");
/*
  for (i = 0; i < 100; i++)
*/
  while (1)
  {
    iResult = write(fdQuada, "The Rain in Spain", 17);
    taskDelay(2);
    iResult = read(fdQuadb, szTemp1, 64);
    szTemp1[iResult] = 0x0;

    iResult = write(fdQuadb, "falls mainly on the plain", 25);
    taskDelay(2);
    iResult = read(fdQuada, szTemp2, 64);
    szTemp2[iResult] = 0x0;
/*
    iResult = write(fdQuadc, "A long long time ago", 20);
    taskDelay(2);
    iResult = read(fdQuadd, szTemp3, 64);
    szTemp3[iResult] = 0x0;

    iResult = write(fdQuadd, "in a galaxy far far away", 24);
    taskDelay(2);
    iResult = read(fdQuadc, szTemp4, 64);
    szTemp4[iResult] = 0x0;

*/
    printf("%s ", szTemp1);
    printf("%s\n", szTemp2);
/*    printf("%s %s\n", szTemp3, szTemp4);
*/
  }


  printf("Closing\n");
  
  close(fdQuada);
  close(fdQuadb);
  close(fdQuadc);
  close(fdQuadd);
}

  
