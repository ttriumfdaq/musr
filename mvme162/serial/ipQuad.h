/***************************** ipQuad.h *******************************/
/*
MODIFICATION HISTORY
ver  date      by      modification description
---------------------  --------------------------------
01   07/21/94  mdg     Creation Date
     08-Feb-96 TW      Added FIOPARITY, FIODATABITS and missing baud rates.


DESCRIPTION
-----------

Header file that contains the MEMORY decode addresses for the IP
modules on the mvme162.  These numbers cannot be changed without
also changing the MMU configuration information in ?/vw/config/mv162/sysLib.c

Also, each address must be at least 64k apart - farther if
Initialize_IPIC is allocating more than 64k for each IP module. 

**********************************************************************/

#define IPQUAD_BAD_ADDRESS       0x1

#define IPIC_ADDRESS            0xfffbc000
#define BAUD_CLK_FREQ           3686400                 /* 3.6864 MHz baud rate crystal */

#define N_IPQUAD_CHANNELS        4

#define	IP_BASE_ADRS	        (0xfff58000)		/* Industry Packs base address */
#define IP_MEM_ADDR             (0xc0000000)            /* IP memory address block */

#define	IP_INT_VEC              0x18

typedef struct {
  TY_DEV tyDev;
  BOOL   created;		/* true if device has already been created */
  char   num;
  volatile char  *mra;		/* mode register I/O address */
  volatile char  *sr;		/* status register I address */
  volatile char  *csr;		/* clock select register O address */
  volatile char  *cra;		/* control register O address */
  volatile char  *dr;		/* data port I/O address */
  volatile char  *ipcr;		/* Input port change register I/O address */
  volatile char  *acr;		/* auxilliary control register I/O address */
  volatile char  *isr;		/* interrupt status register I address */
  volatile char  *imr;		/* interrupt mask register O address */
  volatile char  *ctu;		/* counter/timer upper register I/O address */
  volatile char  *ctl;		/* counter/timer lower register I/O address */
  volatile char  *mrb;		/* mode register I/O address */
  volatile char  *crb;		/* control register 1 address */
  volatile char  *ip;		/* input port register I address */
  volatile char  *opcr;		/* output port config register O address */
  char   intMask;               /* interrupt mask register value */
  int    baudFreq;		/* baud clock frequency */
  char   numChannels;		/* number of channels to support */
} IP_QUAD_DEV;


/*   Bit values for various settings																		*/

#define PARITY_NO           PAR_MODE_NO
#define PARITY_ODD          (PAR_MODE_YES | PARITY_TYPE)
#define PARITY_EVEN         PAR_MODE_YES
#define PARITY_SPACE        PAR_MODE_FORCE
#define PARITY_MARK         (PAR_MODE_FORCE | PARITY_TYPE)
#define PARITY_BIT_MSK      0x1C

#define DATABITS_BIT_MSK	0x3

#define STOPBITS_BIT_MSK  0xf

typedef struct	{	/* BAUD */
	int rate;		/* a baud rate */
	UINT8 csrVal;	/* Corresponding value to write to clock select register */
} BAUD;

typedef struct	{	/* PARITY */
	char parity;	/* a parity represented by a character */
	UINT8 parVal;	/* Corresponding bit value in mode register 1 */
}PARITY;


