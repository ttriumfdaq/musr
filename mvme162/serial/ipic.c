I'm using the IP-Octal on an MVME162, so I may be of some help.....

I have a piece of code that initializes the IPIC for interrupts
and memory decoding.  That piece of code follows.

I have also shared my IP-Octal driver with several people in the
past, and would be happy to do so with you.  (It does not use
an 8530, so it probably is no good to you unless you plan on
switching modules..)


2 files follow - the first one is "ipic.h", the second one is "ipic.c".

Good luck



    _/_/_/_/  _/_/_/_/ _/_/_/_/_/      Mark Del Giorno   (del@rst.com)
   _/    _/  _/           _/           (410) 876-9200  Fax: 876-9470
  _/_/_/_/  _/_/_/_/     _/            Robotic Systems Technology
 _/   _/         _/     _/             1110 Business Parkway South
_/    _/  _/_/_/_/     _/              Westminster, MD 21157



/**********************************************************************
*  ipic.h    ipic.h    ipic.h    ipic.h    ipic.h
*  ipic.h    ipic.h    ipic.h    ipic.h    ipic.h
*  ipic.h    ipic.h    ipic.h    ipic.h    ipic.h
*  ipic.h    ipic.h    ipic.h    ipic.h    ipic.h

MODIFICATION HISTORY
ver  date      by      modification description
---------------------  --------------------------------
01   07/21/94  mdg     Creation Date


DESCRIPTION
-----------

Header file that contains the MEMORY decode addresses for the IP
modules on the mvme162.  These numbers cannot be changed without
also changing the MMU configuration information in ?/vw/config/mv162/sysLib.c

Also, each address must be at least 64k apart - farther if
Initialize_IPIC is allocating more than 64k for each IP module. 

**********************************************************************/

#ifndef IPIC_H
#define IPIC_H

#define IP_A_ADDRESS     0xffe80000
#define IP_B_ADDRESS     0xffe90000
#define IP_C_ADDRESS     0xffea0000
#define IP_D_ADDRESS     0xffeb0000

#endif


/**********************************************************************
 *  ipic.c    ipic.c    ipic.c    ipic.c    ipic.c
 *  ipic.c    ipic.c    ipic.c    ipic.c    ipic.c
 *  ipic.c    ipic.c    ipic.c    ipic.c    ipic.c
 *  ipic.c    ipic.c    ipic.c    ipic.c    ipic.c
 *
 MODIFICATION HISTORY
 ver  date      by      modification description
 ---------------------  --------------------------------
 01   03/08/95  mdg     Changed bit timing for IP-C to 8 usec delay.
 01   07/21/94  mdg     Creation Date

 
 DESCRIPTION
 -----------
 
 This is a single routine that initializes the IPIC chip on board the
 MVME 162.  All 4 industry pack addresses and interrupts are initialized
 at the same time.  It is only necessary to enable the chip if the
 ip module requires address space (rather than I/O space).

 Addresses of where the IP's are mapped must match those set up when
 the MMU is initialized (in ?/vw/config/mv162/sysLib.c).
 
 Each IP is set up for the minimum 64k space.  Change the code if
 this isn't what you need.

 USER CALLABLE ROUTINES
 ----------------------
 Initialize_IPIC();
 
************************************************************************/

#include "vxWorks.h"
#include "ipic.h"

#define IPIC_ADDRESS   0xfffbc000

#define ENABLE_A    FALSE
#define A_0_INTERRUPT_REG     0x15
#define A_1_INTERRUPT_REG     0x15

#define ENABLE_B    TRUE
#define B_0_INTERRUPT_REG     0x15
#define B_1_INTERRUPT_REG     0x15

#define ENABLE_C    TRUE
#define C_0_INTERRUPT_REG     0x15
#define C_1_INTERRUPT_REG     0x00

#define ENABLE_D    FALSE
#define D_0_INTERRUPT_REG     0x00
#define D_1_INTERRUPT_REG     0x00

#define UPPER(X) ((X >> 24 & 0xff))
#define LOWER(X) ((X >> 16 & 0xff))


void Initialize_IPIC()
{

  /* interrupt register looks like:
     PLTY       EL     INT    IEN    ICLR     IL2     IL1      IL0
        PLTY: 1 = Rising Edge/High Level causes interrupts
	EL: 1 = Edge Sensitive
	INT: (READ ONLY) 1 = Interrupt being generated
	IEN: 1 = Interrupts enabled
	ICLR: 1 = Clear edge interrupt.  No meaning for level-ints.
	IL2-IL0: Level at which IP should interrupt.

     For interrupting IP's, this should generally be:
         0 0 0 1  0 1 0 0 = 0x14  (interrupt low-level on #4)
     For non-interrupting IP's, this should be:
         0 0 0 0  0 0 0 0 = 0x00


     General Control Registers look like:
        x_ERR   0  x_RT1   x_RT0   x_WIDTH1   x_WIDTH0   0   x_MEN
	   x_ERR: 1 = Assert IP's Error* signal.
	   x_RT1-x_RT0: Recovery Timer Delay 0, 2, 4, 8 useconds.
	   x_WIDTH1-x_WIDTH0: 00=32bits, 01=8bits, 10=16bits.
	   x_MEN: 1=Enable Memory Accesses to the IP

     For "standard" IP modules, this should generally be:
           0 0 0 0  0 1 0 1  = 0x05  (enabled for byte-transfers)


	*/


  /* Industry Pack Slot A */
  *((unsigned char *) (IPIC_ADDRESS + 0x04)) = UPPER(IP_A_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x05)) = LOWER(IP_A_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x0c)) = 0x00;  /* 64k size */
  if (ENABLE_A)
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x18)) = 0x05;
      *((unsigned char *) (IPIC_ADDRESS + 0x10)) = A_0_INTERRUPT_REG;
      *((unsigned char *) (IPIC_ADDRESS + 0x11)) = A_1_INTERRUPT_REG;
    }
  else
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x18)) = 0x04;
      *((unsigned char *) (IPIC_ADDRESS + 0x10)) = 0x00;
      *((unsigned char *) (IPIC_ADDRESS + 0x11)) = 0x00;
    }


  /* Industry Pack Slot B */
  *((unsigned char *) (IPIC_ADDRESS + 0x06)) = UPPER(IP_B_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x07)) = LOWER(IP_B_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x0d)) = 0x00;  /* 64k size */
  if (ENABLE_B)
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x19)) = 0x05;
      *((unsigned char *) (IPIC_ADDRESS + 0x12)) = B_0_INTERRUPT_REG;
      *((unsigned char *) (IPIC_ADDRESS + 0x13)) = B_1_INTERRUPT_REG;
    }
  else
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x19)) = 0x04;
      *((unsigned char *) (IPIC_ADDRESS + 0x12)) = 0x00;
      *((unsigned char *) (IPIC_ADDRESS + 0x13)) = 0x00;
    }


  /* Industry Pack Slot C */
  *((unsigned char *) (IPIC_ADDRESS + 0x08)) = UPPER(IP_C_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x09)) = LOWER(IP_C_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x0e)) = 0x00;  /* 64k size */
  if (ENABLE_C)
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x1a)) = 0x35;
      *((unsigned char *) (IPIC_ADDRESS + 0x14)) = C_0_INTERRUPT_REG;
      *((unsigned char *) (IPIC_ADDRESS + 0x15)) = C_1_INTERRUPT_REG;
    }
  else
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x1a)) = 0x04;
      *((unsigned char *) (IPIC_ADDRESS + 0x14)) = 0x00;
      *((unsigned char *) (IPIC_ADDRESS + 0x15)) = 0x00;
    }


  /* Industry Pack Slot D */
  *((unsigned char *) (IPIC_ADDRESS + 0x0a)) = UPPER(IP_D_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x0b)) = LOWER(IP_D_ADDRESS);
  *((unsigned char *) (IPIC_ADDRESS + 0x0f)) = 0x00;  /* 64k size */
  if (ENABLE_D)
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x1b)) = 0x05;
      *((unsigned char *) (IPIC_ADDRESS + 0x16)) = D_0_INTERRUPT_REG;
      *((unsigned char *) (IPIC_ADDRESS + 0x17)) = D_1_INTERRUPT_REG;
    }
  else
    {
      *((unsigned char *) (IPIC_ADDRESS + 0x1b)) = 0x04;
      *((unsigned char *) (IPIC_ADDRESS + 0x16)) = 0x00;
      *((unsigned char *) (IPIC_ADDRESS + 0x17)) = 0x00;
    }

} /* end:Initialize_IPIC */

