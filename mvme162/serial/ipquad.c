/* ip301.c - MVIP301 Z8530 SCC (Serial Communications Controller) tty driver*/

/* Copyright 1984-1992 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01o,01dec92,jdi  NOMANUAL for tyCoInt(), tyCoIntRd(), tyCoIntEx(), tyCoIntWr(),
		 - SPR 1808.
01n,28oct92,caf  tweaked tyCoDevCreate() documentation.
01m,02sep92,ccc  change name from z8530Lib.c to z8530Serial.c
01l,18jul92,smb  Changed errno.h to errnoLib.h.
01k,05jun92,ccc  made board independant.  ansified.
		 renamed to z8530Lib.c.
01j,26may92,rrr  the tree shuffle
01i,21apr92,ccc  cleanup of ansi warnings.
01h,11dec91,jdi  more doc cleanup.
01g,04oct91,rrr  passed through the ansification filter
		  -changed VOID to void
		  -changed copyright notice
01f,13sep91,jdi  documentation cleanup.
01e,10aug90,dnw  added forward declarations of void functions.
01d,10jul90,gae  release 5.0 upgrade.
01e,08may90,kdl  changed baud rate to use BAUD_CLK_FREQ instead of SYS_CPU_FREQ.
01d,05mar90,dab  fixed interrupt handling in tyCoInt().
01c,20aug89,gae  changed iv68k.h to iv.h.
01b,09aug87,gae  clean up.
01a,26apr88,dfm  written, by modifying 01g of the iv24 version.
*/

/*
DESCRIPTION
This is the driver for the Z8530 SCC (Serial Communications Controller).
It uses the SCCs in asynchronous mode only.

USER-CALLABLE ROUTINES
Most of the routines in this driver are accessible only through the I/O
system.  Two routines, however, must be called directly: tyCoDrv() to
initialize the driver, and tyCoDevCreate() to create devices.

Before the driver can be used, it must be initialized by calling tyCoDrv().
This routine should be called exactly once, before any reads, writes, or
calls to tyCoDevCreate().  Normally, it is called by usrRoot() in usrConfig.c.

Before a terminal can be used, it must be created using tyCoDevCreate().
Each port to be used should have exactly one device associated with it by
calling this routine.

IOCTL FUNCTIONS
This driver responds to the same ioctl() codes as a normal tty driver; for
more information, see the manual entry for tyLib.  Available baud rates
range from 50 to 38400.

SEE ALSO
tyLib
*/

#include "vxWorks.h"
#include "iv.h"
#include "ioLib.h"
#include "iosLib.h"
#include "tyLib.h"
#include "intLib.h"
#include "errnoLib.h"
#include "drv/serial/z8530.h"
#include "ip301.h"

/* #define DEBUG */

#define DEFAULT_BAUD	9600

LOCAL TY_CO_DEV ip301Dv [N_IP301_CHANNELS]; /* device descriptors */
LOCAL int ip301DrvNum;		/* driver number assigned to this driver */

/* forward declarations */

LOCAL void   ip301Startup ();
LOCAL int    ip301Open ();
LOCAL STATUS ip301Ioctl ();
LOCAL void   ip301HrdInit ();
LOCAL void   ip301InitChannel ();
LOCAL void   ip301ResetChannel ();
LOCAL void   ip301IntWr ();
LOCAL void   ip301IntRd ();
LOCAL void   ip301IntEx ();

/*******************************************************************************
*
* ip301Drv - initialize the serial driver for the following Industry Packs
*
*            Motorola MVIP-301 
*            Greenspring IP-Serial 
*
* This routine initializes the serial driver, sets up interrupt vectors, and
* performs hardware initialization of the serial ports.
*
* This routine should be called exactly once, before any reads, writes, or
* calls to ip301DevCreate().  Normally, it is called by usrRoot() in
* usrConfig.c.
*
* RETURNS: OK, or ERROR if the driver cannot be installed.
*
* SEE ALSO: ip301DevCreate()
*/

STATUS ip301Drv (char cPosition)
{
  char * pcIPBase;
  char * pcIPProm;
  int i;
  char szBuffer[16];
  

/* check if driver already installed */

  if (ip301DrvNum > 0)
  {
    return (OK);
  }
  
/* Determine location of IP specified in cPosition */

  cPosition -= 'A';
  if (cPosition > 3)
  {
    cPosition -= 0x20;
  }

  if ((cPosition > 3) || (cPosition < 0))
  {
    return (IP301_BAD_ADDRESS);
  }
    
/* identify address of specified IP location */

  pcIPBase = (cPosition * 0x100) + (char *) IP_BASE_ADRS;

/*
  Set up memory block address for IP location
*/
#ifdef DEBUG
  printf("Setting MVIP301 memory block address to %p\n", (IP_MEM_ADDR + (cPosition * 0x10000)));
#endif

  *((unsigned char *) (IPIC_ADDRESS + 0x04 + (cPosition * 2))) = (IP_MEM_ADDR + (cPosition * 0x10000)) >> 24 & 0xff;
  *((unsigned char *) (IPIC_ADDRESS + 0x05 + (cPosition * 2))) = (IP_MEM_ADDR + (cPosition * 0x10000)) >> 16 & 0xff;
  *((unsigned char *) (IPIC_ADDRESS + 0x0c + cPosition)) = 0x00;  /* 64k size */

#ifdef DEBUG
  printf("Pointing to MVIP301 ID PROM data\n");

  pcIPProm = pcIPBase + 0x81;
  for (i = 0; i < 7; i++)
  {
    szBuffer[i] = *pcIPProm;
    printf("Data from IP Prom at %p = 0x%X\n", pcIPProm, szBuffer[i]);
    pcIPProm += 2;
  }
  printf("\n");
#endif

/* setup serial device descriptors */

  ip301Dv [0].numChannels = N_IP301_CHANNELS;

  ip301Dv [0].created = FALSE;
  ip301Dv [0].cr = pcIPBase + 5;
  ip301Dv [0].dr = pcIPBase + 7;
  ip301Dv [0].baudFreq = BAUD_CLK_FREQ;
  ip301Dv [0].intType = SCC_WR9_VIS;
  ip301Dv [0].intVec = INT_VEC_IP301;
  ip301Dv [0].clockModeWR11 = SCC_WR11_RTXC_XTAL | SCC_WR11_RX_BR_GEN | SCC_WR11_TX_BR_GEN |
                              SCC_WR11_TRXC_OI;
  ip301Dv [0].clockModeWR14 = SCC_WR14_BR_EN | SCC_WR14_SRC_BR;

  ip301Dv [1].created = FALSE;
  ip301Dv [1].cr = pcIPBase + 1;
  ip301Dv [1].dr = pcIPBase + 3;
  ip301Dv [1].baudFreq = BAUD_CLK_FREQ;
  ip301Dv [1].intType = SCC_WR9_VIS;
  ip301Dv [1].intVec = INT_VEC_IP301;
  ip301Dv [1].clockModeWR11 = SCC_WR11_RTXC_XTAL | SCC_WR11_RX_BR_GEN | SCC_WR11_TX_BR_GEN |
                              SCC_WR11_TRXC_OI;
  ip301Dv [1].clockModeWR14 = SCC_WR14_BR_EN | SCC_WR14_SRC_BR;

/*
   connect serial interrupts
   the MVIP301 IP board uses four interrupts for each channel:
          xxxx xx00 - Receive Exception Interrupt
          xxxx xx01 - Modem Signal Change Interrupt (not used)
          xxxx xx10 - Transmit Data Interrupt
          xxxx xx11 - Receive Data Interrupt
*/

  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_A_WR), ip301IntWr, 0);
  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_A_EX), ip301IntEx, 0);
  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_A_RD), ip301IntRd, 0);
  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_A_SP), ip301IntEx, 0);

  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_B_WR), ip301IntWr, 1);
  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_B_EX), ip301IntEx, 1);
  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_B_RD), ip301IntRd, 1);
  (void) intConnect (INUM_TO_IVEC (INT_VEC_IP301_B_SP), ip301IntEx, 1);

/*
  now enable serial interrupts in IPIC to drive selected IP board

  interrupt register looks like:
     PLTY       EL     INT    IEN    ICLR     IL2     IL1      IL0
        PLTY: 1 = Rising Edge/High Level causes interrupts
	EL: 1 = Edge Sensitive
	INT: (READ ONLY) 1 = Interrupt being generated
	IEN: 1 = Interrupts enabled
	ICLR: 1 = Clear edge interrupt.  No meaning for level-ints.
	IL2-IL0: Level at which IP should interrupt.

  For interrupting IP's, this should generally be:
         0 0 0 1  0 1 0 0 = 0x14  (interrupt low-level on #4)
  For non-interrupting IP's, this should be:
         0 0 0 0  0 0 0 0 = 0x00

*/
  *((unsigned char *) (IPIC_ADDRESS + 0x10 + cPosition * 2)) = 0x14;
  *((unsigned char *) (IPIC_ADDRESS + 0x11 + cPosition * 2)) = 0x14;

/*
  General Control Registers look like:
        x_ERR   0  x_RT1   x_RT0   x_WIDTH1   x_WIDTH0   0   x_MEN
	   x_ERR: 1 = Assert IP's Error* signal.
	   x_RT1-x_RT0: Recovery Timer Delay 0, 2, 4, 8 useconds.
	   x_WIDTH1-x_WIDTH0: 00=32bits, 01=8bits, 10=16bits.
	   x_MEN: 1=Enable Memory Accesses to the IP

  For "standard" IP modules, this should generally be:
           0 0 0 0  0 1 0 1  = 0x05  (enabled for byte-transfers)
*/
  *((unsigned char *) (IPIC_ADDRESS + 0x18 + cPosition)) = 0x05;

  ip301HrdInit ();
  ip301DrvNum = iosDrvInstall (ip301Open, (FUNCPTR) NULL, ip301Open,
                               (FUNCPTR) NULL, tyRead, tyWrite, ip301Ioctl);

  return (ip301DrvNum == ERROR ? ERROR : OK);
}

/*******************************************************************************
*
* ip301DevCreate - create a device for an on-board serial port
*
* This routine creates a device on a specified serial port.  Each port
* to be used should have exactly one device associated with it by calling
* this routine.
*
* For instance, to create the device "/tyCo/0", with buffer sizes of 512 bytes,
* the proper call would be:
* .CS
*     ip301DevCreate ("/tyCo/0", 0, 512, 512);
* .CE
*
* RETURNS: OK, or ERROR if the driver is not installed, the channel is
* invalid, or the device already exists.
*
* SEE ALSO: ip301Drv()
*/

STATUS ip301DevCreate
    (
    char *      name,           /* name to use for this device      */
    FAST int    channel,        /* physical channel for this device */
    int         rdBufSize,      /* read buffer size, in bytes       */
    int         wrtBufSize      /* write buffer size, in bytes      */
    )
    {
    FAST TY_CO_DEV *pip301Dv;

    if (ip301DrvNum <= 0)
	{
	errnoSet (S_ioLib_NO_DRIVER);
	return (ERROR);
	}

    /* if this doesn't represent a valid channel, don't do it */

    if (channel < 0 || channel >= ip301Dv [0].numChannels)
	return (ERROR);

    pip301Dv = &ip301Dv [channel];

    /* if there is a device already on this channel, don't do it */

    if (pip301Dv->created)
	return (ERROR);

    /* initialize the ty descriptor */

    if (tyDevInit (&pip301Dv->tyDev, rdBufSize, wrtBufSize,
		   (FUNCPTR) ip301Startup) != OK)
	{
	return (ERROR);
	}

    /* initialize the channel hardware */

    ip301InitChannel (channel);

    /* mark the device as created, and add the device to the I/O system */

    pip301Dv->created = TRUE;

    return (iosDevAdd (&pip301Dv->tyDev.devHdr, name, ip301DrvNum));
    }

/*******************************************************************************
*
* ip301HrdInit - initialize the USART
*/

LOCAL void ip301HrdInit (void)
{
  FAST int   oldlevel;	/* current interrupt level mask */
  int        ix;

  oldlevel = intLock ();	/* disable interrupts during init */

  for (ix=0; ix < ip301Dv [0].numChannels; ix++)
  {
    ip301ResetChannel (ix);	/* reset channel */
  }
  
  intUnlock (oldlevel);
}

/********************************************************************************
* ip301ResetChannel - reset a single channel
*/

LOCAL void ip301ResetChannel(int channel)
{
    volatile char *cr = ip301Dv [channel].cr;        /* IP301 control reg adr */
    int delay;
    int zero = 0;

    *cr = zero;		/* SCC_WR0_NULL_CODE sync the state machine */
    *cr = zero;		/* SCC_WR0_NULL_CODE sync the state machine */
    *cr = SCC_WR0_ERR_RST;
    *cr = SCC_WR0_RST_INT;

    *cr = SCC_WR0_SEL_WR9;      /* write register 9 - master int ctrl */

    *cr = ((channel % 2) == 0) ?
        SCC_WR9_CH_A_RST :      /* reset channel A */
        SCC_WR9_CH_B_RST;       /* reset channel B */

    for (delay = 0; delay < 1000; delay++)
	;     /* spin wheels for a moment */
}

/*******************************************************************************
*
* ip301InitChannel - initialize a single channel
*/

LOCAL void ip301InitChannel(int channel)
{
    FAST TY_CO_DEV *pip301Dv = &ip301Dv [channel];
    volatile char  *cr = pip301Dv->cr;	/* SCC control reg adr */
    FAST int   	    baudConstant;
    FAST int        oldlevel;		/* current interrupt level mask */
    int	            zero = 0;

    oldlevel = intLock ();		/* disable interrupts during init */

    /* initialize registers */

    *cr = SCC_WR0_SEL_WR4;              /* write reg 4 - misc parms and modes */
    *cr = SCC_WR4_1_STOP | SCC_WR4_16_CLOCK;

    *cr = SCC_WR0_SEL_WR1;
    *cr = SCC_WR1_INT_ALL_RX | SCC_WR1_TX_INT_EN;

    *cr = SCC_WR0_SEL_WR3;		/* write reg 3 - receive params */
    *cr = SCC_WR3_RX_8_BITS;

    *cr = SCC_WR0_SEL_WR5;		/* tx params */
    *cr = SCC_WR5_TX_8_BITS | SCC_WR5_DTR | SCC_WR5_RTS;

    *cr = SCC_WR0_SEL_WR10;		/* misc tx/rx control */
    *cr = zero;				/* clear sync, loop, poll */

    *cr = SCC_WR0_SEL_WR11;		/* clock mode */
    *cr = pip301Dv->clockModeWR11;

    *cr = SCC_WR0_SEL_WR15;		/* external/status interrupt cntrl */
    *cr = zero;

    /* Calculate the baud rate constant for the default baud rate
     * from the input clock frequency.  This assumes that the
     * divide-by-16 bit is set (done in WR4 above).
     */

    baudConstant = ((pip301Dv->baudFreq / 32) / DEFAULT_BAUD) - 2;

    *cr = SCC_WR0_SEL_WR12;		/* LSB of baud constant */
    *cr = (char) baudConstant;		/* write LSB */
    *cr = SCC_WR0_SEL_WR13;		/* MSB of baud constant */
    *cr = (char) (baudConstant >> 8);	/* write MSB */

    *cr = SCC_WR0_SEL_WR14;		/* misc control bits */
    *cr = pip301Dv->clockModeWR14;

    *cr = SCC_WR0_SEL_WR15;		/* external/status interrupt control */
    *cr = zero;

    *cr = SCC_WR0_RST_INT;		/* reset external interrupts */
    *cr = SCC_WR0_ERR_RST;		/* reset errors */

    *cr = SCC_WR0_SEL_WR3;		/* write reg 3 - receive params */
    *cr = SCC_WR3_RX_8_BITS | SCC_WR3_RX_EN;

    *cr = SCC_WR0_SEL_WR5;		/* tx params */
    *cr = SCC_WR5_TX_8_BITS | SCC_WR5_TX_EN | SCC_WR5_DTR | SCC_WR5_RTS;

    *cr = SCC_WR0_SEL_WR2;		/* interrupt vector */
    *cr = pip301Dv->intVec;

    *cr = SCC_WR0_SEL_WR1;		/* int and xfer mode */
    *cr = SCC_WR1_INT_ALL_RX | SCC_WR1_TX_INT_EN;

    *cr = SCC_WR0_SEL_WR9;		/* master interrupt control */
    *cr = SCC_WR9_MIE | pip301Dv->intType;	/* enable interrupts */

    *cr = SCC_WR0_RST_TX_CRC;
    *cr = SCC_WR0_RST_INT;
    *cr = SCC_WR0_RST_INT;

    *cr = zero;				/* reset SCC register counter */

    intUnlock (oldlevel);
}

/*******************************************************************************
*
* ip301Open - open file to USART
*/

LOCAL int ip301Open(TY_CO_DEV *pip301Dv, char *name,int mode)
{
  return ((int) pip301Dv);
}

/*******************************************************************************
*
* ip301Ioctl - special device control
*
* This routine handles FIOBAUDRATE requests and passes all others to tyIoctl().
*
* RETURNS: OK, or ERROR if invalid baud rate, or whatever tyIoctl() returns.
*/

LOCAL STATUS ip301Ioctl
    (
    TY_CO_DEV *pip301Dv,		/* device to control */
    int        request,		/* request code */
    int        arg		/* some argument */
    )
    {
    FAST int     oldlevel;		/* current interrupt level mask */
    FAST int     baudConstant;
    FAST STATUS  status;
    volatile char *cr;			/* SCC control reg adr */

    switch (request)
	{
	case FIOBAUDRATE:

	    if (arg < 50 || arg > 38400)
	        {
		status = ERROR;		/* baud rate out of range */
		break;
	        }

	    /* Calculate the baud rate constant for the new baud rate
	     * from the input clock frequency.  This assumes that the
	     * divide-by-16 bit is set in the Z8530 WR4 register (done
	     * in ip301InitChannel).
	     */

	    baudConstant = ((pip301Dv->baudFreq / 32) / arg) - 2;

	    cr = pip301Dv->cr;

	    /* disable interrupts during chip access */

	    oldlevel = intLock ();

	    *cr = SCC_WR0_SEL_WR12;	/* LSB of baud constant */
	    *cr = (char)baudConstant;	/* write LSB */
	    *cr = SCC_WR0_SEL_WR13;	/* MSB of baud constant */
	    *cr = (char)(baudConstant >> 8); /* write MSB */

	    intUnlock (oldlevel);

	    status = OK;
	    break;

	default:
	    status = tyIoctl (&pip301Dv->tyDev, request, arg);
	    break;
	}
    return (status);
    }

/*******************************************************************************
*
* ip301IntWr - interrupt level processing
*
* This routine handles write interrupts from the SCC.
*
* RETURNS: N/A
*
* NOMANUAL
*/

void
ip301IntWr(int channel)			/* interrupting serial channel */
{
  char            outChar;
  FAST TY_CO_DEV *pip301Dv = &ip301Dv [channel];
  volatile char  *cr = pip301Dv->cr;

  if (pip301Dv->created && tyITx (&pip301Dv->tyDev, &outChar) == OK)
  {
    *pip301Dv->dr = outChar;
  }
  else
  {
/*
 no more chars to xmit now.  reset the tx int, so the SCC does not keep interrupting.
*/
    *cr = SCC_WR0_RST_TX_INT;
  }
  *cr = SCC_WR0_RST_HI_IUS;	/* end the interrupt acknowledge */
}

/*****************************************************************************
*
* ip301IntRd - interrupt level input processing
*
* This routine handles read interrupts from the SCC
*
* RETURNS: N/A
*
* NOMANUAL
*/

void
ip301IntRd(int channel)			/* interrupting serial channel */
{
  FAST TY_CO_DEV *pip301Dv = &ip301Dv [channel];
  volatile char  *cr = pip301Dv->cr;
  char            inchar;

  inchar = *pip301Dv->dr;

  if (pip301Dv->created)
  {
    tyIRd (&pip301Dv->tyDev, inchar);
  }
  
  *cr = SCC_WR0_RST_HI_IUS;	/* reset the interrupt in the Z8530 */
}

/**********************************************************************
*
* ip301IntEx - miscellaneous interrupt processing
*
* This routine handles miscellaneous interrupts on the SCC
*
* RETURNS: N/A
*
* NOMANUAL
*/

void
ip301IntEx(int channel)			/* interrupting serial channel */
{
  FAST TY_CO_DEV *pip301Dv = &ip301Dv [channel];
  volatile char  *cr = pip301Dv->cr;

  *cr = SCC_WR0_ERR_RST;		/* reset errors */
  *cr = SCC_WR0_RST_HI_IUS;		/* reset the interrupt in the Z8530 */
}

/********************************************************************************
* ip301Int - interrupt level processing
*
* This routine handles interrupts from both of the SCCs.
* We determine from the parameter which SCC interrupted, then look at
* the code to find out which channel and what kind of interrupt.
*
* NOMANUAL
*/

void
ip301Int(int sccNum)
{
  volatile char   *cr;
  FAST char        intStatus;
  FAST TY_CO_DEV  *pip301Dv;
  char             outChar;

    /* We need to find out which channel interrupted.  We need to read
     * the B channel of the interrupting SCC to find out which channel
     * really interrupted.  Note that things are set up so that the A
     * channel is channel 0, even though on the chip it is the one with
     * the higher address
     */  

  pip301Dv = &ip301Dv [(sccNum * 2) + 1];
  cr = pip301Dv->cr;

  *cr = SCC_WR0_SEL_WR2;                      /* read reg 2 */
  intStatus = *cr;

  if ((intStatus & 0x08) != 0)
  {                               /* the A channel interrupted */
    --pip301Dv;
    cr = pip301Dv->cr;
  }

  switch (intStatus & 0x06)
  {
    case 0x00:                      /* Tx Buffer Empty */
      if (pip301Dv->created && (tyITx (&pip301Dv->tyDev, &outChar) == OK))
        *pip301Dv->dr = outChar;
      else
      {
                /* no more chars to xmit now.  reset the tx int,
                 * so the SCC doesn't keep interrupting us.
		 */
 
        *cr = SCC_WR0_RST_TX_INT;
      }
      break;
 
        case 0x04:                      /* RxChar Avail */
            if (pip301Dv->created)
                tyIRd (&pip301Dv->tyDev, *pip301Dv->dr);
            break;
 
        case 0x02:                      /* External Status Change */
            *cr = SCC_WR0_ERR_RST;
            outChar = *pip301Dv->dr;          /* throw away char */
            break;
 
        case 0x06:                      /* Special receive condition */
            *cr = SCC_WR0_ERR_RST;
            break;                      /* ignore */
 
        }
 
    *cr = SCC_WR0_RST_HI_IUS;   /* Reset the interrupt in the Z8530 */
    }

/*******************************************************************************
*
* ip301Startup - transmitter startup routine
*
* Call interrupt level character output routine.
*/

LOCAL void 
ip301Startup(TY_CO_DEV *pip301Dv) 		/* ty device to start up */
{
  char outChar;

  if (tyITx (&pip301Dv->tyDev, &outChar) == OK)
  {
    *pip301Dv->dr = outChar;
  }
}

/******

  Test program

****/

void
ip301Test(void)
{
  STATUS fd301a, fd301b;
  STATUS iResult;
  long i, j;
  char szTemp1[64], szTemp2[64];
    
  fd301a = open("/tyCo/2", O_RDWR, 0);
  if (fd301a == ERROR)
  {
    printf("Error opening serial port\n");
    exit(0);
  }
  iResult = ioctl(fd301a, FIOBAUDRATE, 9600);
  if (iResult == ERROR)
  {
    printf("Error setting serial port to 9600\n");
    exit(0);
  }

  fd301b = open("/tyCo/3", O_RDWR, 0);
  if (fd301b == ERROR)
  {
    printf("Error opening serial port\n");
    exit(0);
  }
  iResult = ioctl(fd301b, FIOBAUDRATE, 9600);
  if (iResult == ERROR)
  {
    printf("Error setting serial port to 9600\n");
    exit(0);
  }

  printf("Starting writes and reads\n");
  
  for (i = 0; i < 100; i++)
  {
    iResult = write(fd301a, "The Rain in Spain", 17);
    iResult = read(fd301b, szTemp1, 64);
    szTemp1[iResult] = 0x0;

    iResult = write(fd301b, "Falls mainly on the plain", 25);
    iResult = read(fd301a, szTemp2, 64);
    szTemp2[iResult] = 0x0;

    printf("%s %s\n", szTemp1, szTemp2);
  }
  
  close(fd301a);
  close(fd301b);
}

  
