/* mv162IndPackInit.c - Motorola 162 IndustryPack initialization
*
* Version: "@(#)mv162IndPackInit.c	1.1    23 May 1994 TSL"
*/

#pragma ident "@(#)mv162IndPackInit.c	1.1    23 May 1994 TSL"


/*
modification history
--------------------
940420,LT	written.
*/

/*
DESCRIPTION
This is an initialization routine for IndustryPack's on a MVME-162 CPU.
It searches the IP ID address space and try's to identify any pack's found.
If a suitable driver is available it is installed.

Note !
The #ifdef INDUSTRY_PACK_C_FILE entry in mv162/sysLib.c - sysHwInit() can't be
used to call mv162IndPackInit() since the I/O system isn't initialized at that
point. usrConfig.c - usrRoot() works better.
*/

#include "vxWorks.h"
#include "ioLib.h"
#include "taskLib.h"
#include "iv.h"
#include "intLib.h"
#include "stdio.h"
#include "vxLib.h"
#include "drv/serial/ipOctalSerial.h"
#include "mv162IndPackInit.h"



UINT8 *pIPIDBase[NUM_IP_SLOT] = {(UINT8 *) (IP_ID_AREA_BASE + 0*IP_SLOT_INC),
								 (UINT8 *) (IP_ID_AREA_BASE + 1*IP_SLOT_INC),
								 (UINT8 *) (IP_ID_AREA_BASE + 2*IP_SLOT_INC),
 								 (UINT8 *) (IP_ID_AREA_BASE + 3*IP_SLOT_INC)};

UINT8 *pIPIOBase[NUM_IP_SLOT] = {(UINT8 *) (IP_IO_AREA_BASE + 0*IP_SLOT_INC),
								 (UINT8 *) (IP_IO_AREA_BASE + 1*IP_SLOT_INC),
								 (UINT8 *) (IP_IO_AREA_BASE + 2*IP_SLOT_INC),
 								 (UINT8 *) (IP_IO_AREA_BASE + 3*IP_SLOT_INC)};

UINT8 *pIPIntCntrlReg[NUM_IP_SLOT] = {(UINT8 *) (IP_INT_CNTRL_BASE),
									  (UINT8 *) (IP_INT_CNTRL_BASE + 2),
									  (UINT8 *) (IP_INT_CNTRL_BASE + 4),
									  (UINT8 *) (IP_INT_CNTRL_BASE + 6)};

UINT16 *pIPMemBaseReg[NUM_IP_SLOT] = {(UINT16 *) (IP_MEM_BASE_BASE),
									  (UINT16 *) (IP_MEM_BASE_BASE + 2),
									  (UINT16 *) (IP_MEM_BASE_BASE + 4),
									  (UINT16 *) (IP_MEM_BASE_BASE + 6)};

UINT8 *pIPGenCntrlReg[NUM_IP_SLOT] = {(UINT8 *) (IP_GEN_CNTRL_REG_BASE),
									  (UINT8 *) (IP_GEN_CNTRL_REG_BASE + 1),
									  (UINT8 *) (IP_GEN_CNTRL_REG_BASE + 2),
									  (UINT8 *) (IP_GEN_CNTRL_REG_BASE + 3)};

UINT8 strId[4] = {'I', 'P', 'A', 'C'};


/*******************************************************************************
* mv162IndPackInit - Initialize mv162 IndustryPack modules.
*
* DESCRIPTION
* Searches the MVME162 IP module ID area for installed IP modules. When a 
* IP is found it's model number is compared to the "known" model numbers
* for identification. If a matching driver is available, that driver 
* is installed.
*
* RETURNS
* Number of IP modules found.
*/
int mv162IndPackInit(void)
{
	char devName[32];
	OCT_SER_DEV *pOctSerDv;
	int i, j, nIP;
	int intVecNum = IP_INT_VEC_START;
	int nSerChannels = 2;			/* Two channels fixed on mv162 CPU */ 
	unsigned int ipModel;
	UINT8 *pId;
	UINT8 byte;

	for(i=0, nIP = 0; i<NUM_IP_SLOT; i++)	{
		/*
		* Check if any IP module installed in this slot.
		*/
		pId = pIPIDBase[i] + 1;				/* Uses only odd addresses */
		for(j=0; j<sizeof(strId); j++, pId +=2)	{
			if(vxMemProbe((char *) pId, READ, 1,
						  (char *) &byte) == ERROR)
				break;
			if(byte != strId[j])
				break;
		}
		if(j < sizeof(strId))
			continue;				/* Didn't pass ID string test */

		nIP++;						/* IP-module found ! Try to identify it. */

		ipModel = *pId << 8;		/* Read manufacturer ID */
		pId += 2;
		ipModel |= *pId;			/* Read IP model number */

		switch(ipModel)	{			/* Check IP model  */

			case	MODEL_IP_OCTAL_SERIAL:
				printf("IP slot %d: Octal serial...", i);

				/*
				* Map in temp. memory page. Mem size needn't be set since
				* it should be 64K after reset.
				*/ 
				*pIPMemBaseReg[i] = (IP_TMP_MEM_PAGE >> 16);
				*pIPGenCntrlReg[i] = IP_MEM_WIDTH_BYTE | IP_MEM_ENABLE;

				pOctSerDv = octSerModuleInit((UINT8 *) IP_TMP_MEM_PAGE,
											 pIPIOBase[i], intVecNum++);

				/*
				* Int vector set. Now remove memory page again.	
				*/
				*pIPGenCntrlReg[i] = 0;

				if(octSerDrv() == ERROR)	{
					printf("Driver installation failed\n");
				}
				else	{
					for(j=0; j<N_CHANNELS; j++)	{
						sprintf(devName, "/tyCo/%d", nSerChannels++);
						if(octSerDevCreate(&pOctSerDv[j], devName,
										   512, 512) == ERROR)	{
							printf("Device creation failed\n");
							break;
						}
					}
				}
				/* Enable IPC int0 */
				*pIPIntCntrlReg[i] = IPC_INT_EN | IPC_INT_LEVEL_5;

				/* Enable IPC int1 */
				*(pIPIntCntrlReg[i] + 1) = IPC_INT_EN | IPC_INT_LEVEL_5;

				printf("Driver installed\n");
				break;

			case	MODEL_IP_DUAL_P_T:
				printf("IP slot %d: DUAL_P_T...", i);
				printf("No driver\n");
				break;

			case	MODEL_IP_DIGIT_48:
				printf("IP slot %d: DIGIT-48...", i);
				printf("No driver\n");
				break;

			default:
				printf("IP slot %d: Unknown module. Manufacturer code %x, model code 0x%x\n", i, ipModel >> 8, ipModel & 0xFF);
				break;
		}
	}
	return(nIP);
}
