/* mv162IndPackInit.h - Motorola 162 IndustryPack initialization header.
*
* Version: "@(#)mv162IndPackInit.h	1.1    23 May 1994 TSL"
*/

#pragma ident "@(#)mv162IndPackInit.h	1.1    23 May 1994 TSL"

/*
modification history
--------------------
940420,LT	written.
*/

#ifndef __INCmv162IndPackInith
#define __INCmv162IndPackInith

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE


#define NUM_IP_SLOT				4			/* Number of IP slots on MVME-162 */

#define IP_INT_VEC_START		0xB0		/* Start using interrupt vectors */
											/*  from this number */

#define IP_ID_AREA_BASE			0xfff58080	/* ID area base address */
#define IP_IO_AREA_BASE			0xfff58000	/* IO area base address */
#define IP_SLOT_INC				0x100		/* Address increment to next slot */

#define IP_INT_CNTRL_BASE		0xfffbc010	/* Interrupt control register */

#define IPC_INT_EN				0x10		/* IPC interrupt enable bit */
		
#define IPC_INT_LEVEL_1			0x01		/* IPC interrupt level codes */
#define IPC_INT_LEVEL_2			0x02
#define IPC_INT_LEVEL_3			0x03
#define IPC_INT_LEVEL_4			0x04
#define IPC_INT_LEVEL_5			0x05
#define IPC_INT_LEVEL_6			0x06
#define IPC_INT_LEVEL_7			0x07


#define IP_MEM_BASE_BASE		0xfffbc004	/* IP mem base reg. base address */

#define IP_GEN_CNTRL_REG_BASE	0xfffbc018	/* IP control reg. base address */

/*
* IP general control register codes
*/
#define IP_RT_0					0x00		/* Recovery time 0 uSec */
#define IP_RT_2					0x10		/* Recovery time 2 uSec */
#define IP_RT_4					0x20		/* Recovery time 4 uSec */
#define IP_RT_8					0x30		/* Recovery time 8 uSec */

#define IP_MEM_WIDTH_BYTE		0x04		/* IP memory width codes */	
#define IP_MEM_WIDTH_WORD		0x08
#define IP_MEM_WIDTH_LWORD		0x00

#define IP_MEM_ENABLE			0x01		/* Enable IP memory space */

/*
* The IP octal serial needs a temporary memory page to be map in during
* initialization since the interrupt vector must be written through 
* memory address space. 
*/
#define IP_TMP_MEM_PAGE			0xfffd0000	/* Temp. memory page base address */

/*
* GreenSpring IP model numbers
*/
#define MODEL_IP_OCTAL_SERIAL	0xF022	
#define MODEL_IP_DUAL_P_T		0xF023	
#define MODEL_IP_DIGIT_48		0xF024	

/*
* Function prototypes
*/
int mv162IndPackInit(void);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmv162IndPackInith */
