'\"
'\" $Header: /usr/local/cvsroot/musr/mvme162/tcl7.3/doc/tcp.n,v 1.1.1.1 1994/01/07 21:36:26 ted Exp $
'\" $Source: /usr/local/cvsroot/musr/mvme162/tcl7.3/doc/tcp.n,v $
'\" $Log: tcp.n,v $
'\" Revision 1.1.1.1  1994/01/07 21:36:26  ted
'\" import from decu18
'\"
# Revision 1.1  1994/01/07  21:36:26  vanandel
# : Modified Files:
# : 	README.vxworks tclMain.c tclTCP.c
# : ----------------------------------------------------------------------
# Prepared release notes.
# Allow simple commands to passed in as string arguments to tclMain().
# Fixed tclTCP.c to talk to Unix tclTCP1.1
# : Added Files:
# : 	tcp.n
# : ----------------------------------------------------------------------
# Documentation for tclTCP
#
'\" Revision 1.2  1992/03/12  18:39:24  kennykb
'\" Man page updated to reflect splitting off the simple event manager.
'\"
'\" Revision 1.1  1992/02/14  19:57:51  kennykb
'\" Initial revision
'\"
'\"
'\" Copyright 1990 Regents of the University of California
'\" Permission to use, copy, modify, and distribute this
'\" documentation for any purpose and without fee is hereby
'\" granted, provided that this notice appears in all copies.
'\" The University of California makes no representations about
'\" the suitability of this material for any purpose.  It is
'\" provided "as is" without express or implied warranty.
'\" 
.\" The definitions below are for supplemental macros used in Sprite
.\" manual entries.
.\"
.\" .HS name section [date [version]]
.\"	Replacement for .TH in other man pages.  See below for valid
.\"	section names.
.\"
.\" .AP type name in/out [indent]
.\"	Start paragraph describing an argument to a library procedure.
.\"	type is type of argument (int, etc.), in/out is either "in", "out",
.\"	or "in/out" to describe whether procedure reads or modifies arg,
.\"	and indent is equivalent to second arg of .IP (shouldn't ever be
.\"	needed;  use .AS below instead)
.\"
.\" .AS [type [name]]
.\"	Give maximum sizes of arguments for setting tab stops.  Type and
.\"	name are examples of largest possible arguments that will be passed
.\"	to .AP later.  If args are omitted, default tab stops are used.
.\"
.\" .BS
.\"	Start box enclosure.  From here until next .BE, everything will be
.\"	enclosed in one large box.
.\"
.\" .BE
.\"	End of box enclosure.
.\"
.\" .VS
.\"	Begin vertical sidebar, for use in marking newly-changed parts
.\"	of man pages.
.\"
.\" .VE
.\"	End of vertical sidebar.
.\"
.\" .DS
.\"	Begin an indented unfilled display.
.\"
.\" .DE
.\"	End of indented unfilled display.
.\"
'	# Heading for Sprite man pages
.de HS
.if '\\$2'cmds'       .TH \\$1 1 \\$3 \\$4
.if '\\$2'lib'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tcl'        .TH \\$1 3 \\$3 \\$4
.if '\\$2'tk'         .TH \\$1 3 \\$3 \\$4
.if t .wh -1.3i ^B
.nr ^l \\n(.l
.ad b
..
'	# Start an argument description
.de AP
.ie !"\\$4"" .TP \\$4
.el \{\
.   ie !"\\$2"" .TP \\n()Cu
.   el          .TP 15
.\}
.ie !"\\$3"" \{\
.ta \\n()Au \\n()Bu
\&\\$1	\\fI\\$2\\fP	(\\$3)
.\".b
.\}
.el \{\
.br
.ie !"\\$2"" \{\
\&\\$1	\\fI\\$2\\fP
.\}
.el \{\
\&\\fI\\$1\\fP
.\}
.\}
..
'	# define tabbing values for .AP
.de AS
.nr )A 10n
.if !"\\$1"" .nr )A \\w'\\$1'u+3n
.nr )B \\n()Au+15n
.\"
.if !"\\$2"" .nr )B \\w'\\$2'u+\\n()Au+3n
.nr )C \\n()Bu+\\w'(in/out)'u+2n
..
'	# BS - start boxed text
'	# ^y = starting y location
'	# ^b = 1
.de BS
.br
.mk ^y
.nr ^b 1u
.if n .nf
.if n .ti 0
.if n \l'\\n(.lu\(ul'
.if n .fi
..
'	# BE - end boxed text (draw box now)
.de BE
.nf
.ti 0
.mk ^t
.ie n \l'\\n(^lu\(ul'
.el \{\
.\"	Draw four-sided box normally, but don't draw top of
.\"	box if the box started on an earlier page.
.ie !\\n(^b-1 \{\
\h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.el \}\
\h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\l'|0u-1.5n\(ul'
.\}
.\}
.fi
.br
.nr ^b 0
..
'	# VS - start vertical sidebar
'	# ^Y = starting y location
'	# ^v = 1 (for troff;  for nroff this doesn't matter)
.de VS
.mk ^Y
.ie n 'mc \s12\(br\s0
.el .nr ^v 1u
..
'	# VE - end of vertical sidebar
.de VE
.ie n 'mc
.el \{\
.ev 2
.nf
.ti 0
.mk ^t
\h'|\\n(^lu+3n'\L'|\\n(^Yu-1v\(bv'\v'\\n(^tu+1v-\\n(^Yu'\h'-|\\n(^lu+3n'
.sp -1
.fi
.ev
.\}
.nr ^v 0
..
'	# Special macro to handle page bottom:  finish off current
'	# box/sidebar if in box/sidebar mode, then invoked standard
'	# page bottom macro.
.de ^B
.ev 2
'ti 0
'nf
.mk ^t
.if \\n(^b \{\
.\"	Draw three-sided box if this is the box's first page,
.\"	draw two sides but no top otherwise.
.ie !\\n(^b-1 \h'-1.5n'\L'|\\n(^yu-1v'\l'\\n(^lu+3n\(ul'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.el \h'-1.5n'\L'|\\n(^yu-1v'\h'\\n(^lu+3n'\L'\\n(^tu+1v-\\n(^yu'\h'|0u'\c
.\}
.if \\n(^v \{\
.nr ^x \\n(^tu+1v-\\n(^Yu
\kx\h'-\\nxu'\h'|\\n(^lu+3n'\ky\L'-\\n(^xu'\v'\\n(^xu'\h'|0u'\c
.\}
.bp
'fi
.ev
.if \\n(^b \{\
.mk ^y
.nr ^b 2
.\}
.if \\n(^v \{\
.mk ^Y
.\}
..
'	# DS - begin display
.de DS
.RS
.nf
.sp
..
'	# DE - end display
.de DE
.fi
.RE
.sp .5
..
.HS tcp cmds
.BS
'\" Note: do not modify the .SH NAME line immediately below!
.SH NAME
\fBtcp\fR \- TCP server code for Tcl interpreters
.SH SYNOPSIS
.TP
\fBtcp server\fI ?options?\fR
.TP
\fBtcp connect\fI hostname port\fR
.TP
\fBtcp mainloop\fR
.TP
\fBtcp poll\fR
.TP
\fBtcp wait\fR
.TP
\fBtcp login\fI client\fR
.TP
\fBtcp eval\fI client command\fR
.TP
\fBtcp client\fR
.TP
\fBtcp servers\fR
.BE

.PP
Copyright (C) 1992 General Electric. All rights reserved.

Permission to use, copy, modify, and distribute this
software and its documentation for any purpose and without
fee is hereby granted, provided that the above copyright
notice appear in all copies and that both that copyright
notice and this permission notice appear in supporting
documentation, and that the name of General Electric not be used in
advertising or publicity pertaining to distribution of the
software without specific, written prior permission.
General Electric makes no representations about the suitability of
this software for any purpose.  It is provided "as is"
without express or implied warranty.
.sp
This work was supported by the DARPA Initiative in Concurrent
Engineering (DICE) through DARPA Contract MDA972-88-C-0047.

.SH DESCRIPTION
.PP
The \fBtcp server\fP command allows a Tcl interpreter to function as a server
in a TCP/IP network.  The \fBtcp client\fP command allows another Tcl
interpreter, on the same network, to access the functions of the
server.

.SH "TCP SERVER"
The \fBtcp server\fP command is the first command to be executed to
establish a network of Tcl applications.  It is executed in an
interpreter that wants to accept requests from a client.  It has the
following general form:
.DS C
\fBtcp server\fR ?\fIoption value\fR?...
.DE
The options are:
.TP
\fB-port \fInumber\fR
Specifies that the server should listen at a particular IP port number
for connections.  The default is to accept whatever port number the
system assigned.
.TP
\fB-command \fIstring\fR
Specifies the prefix for a command to be executed whenever a
connection request is received.  This string will have appended to it
the ``client ID'' that identifies the client accepting the connection.
See ``Client Objects'' below for a description of the operations that
may be performed on the client.  The default command is \fBtcp
login\fP (\fIq.v.\fP).
.PP
Any of the above keywords may be abbreviated.
.PP
The \fBtcp server\fP command creates a new Tcl command to control the
operations of the server, and returns the name of the command.  The
server command has the following general form:
.DS C
\fIserverName option \fR?\fIarg arg ...\fR?
.DE
The following server commands are possible for these server objects:
.TP
\fIserverName \fBaccept\fR
Requests that the server accept a request for connection, and delays
until it does.  When a connection request is accepted, a client object
(see ``Client Objects'' below) is created to describe the client, and
the \fBdo\fP command is invoked on the client object.  \fIThis command
should not normally be used by an application.  It will normally be
invoked by \fBtcp poll\fI, \fBtcp mainloop\fI, or the application's
own event manager.\fR
.TP
\fIserverName \fBclients\fR
Returns a list of the clients currently active at the specified server.
.TP
\fIserverName \fBconfigure \fR?\fI-option\fR? ?\fIvalue\fR? ?\fI-option value ...\fR?
Query or modify the configuration options of the server.  If no
\fIoption\fR is specified, the command returns a list of the available
\fIoptions\fR.  If a single \fIoption\fR is specified with no
\fIvalue\fR, the command returns a string giving the current value of
the named \fIoption\fR.  If one or more \fIoption-value\fR pairs are
specified, the command modifies the specified server option(s) to have
the specified value(s); in this case, the command returns the name of
the server object.  The available options are the same as on the
\fBtcp server\fR command.
.TP
\fIserverName \fBstart\fR
Requests that the server start listening for connection requests.
This command returns an empty string immediately.  Connection requests
will be accepted as they are detected by \fBtcp mainloop\fR, \fBtcp
poll\fR, or the application's event handler.
.TP
\fIserverName \fBstop\fR
Requests that the server shut down.  Currently connected clients are
disconnected, currently pending requests are ignored, and the TCP
socket on which the server listens is closed.
.PP
Normally, a server will be created and initialized by a Tcl sequence
like:
.DS C
\fBset \fIserver \fR[\fBtcp server -port \fR2323\fB -command \fImylogin\fR]
.sp
$\fIserver \fBstart\fR
.sp
\fBtcp mainloop\fR # if the application doesn't have an event handler
.DE
The server will then be in control of the application, calling the
\fImylogin\fR Tcl procedure to initialize new clients and process
their requests.  See ``Login and Command Procedures'' below for
further discussion of the \fB-command\fR procedures.
.SH "TCP MAINLOOP, TCP POLL, and TCP WAIT"
.PP
The \fBtcp mainloop\fR procedure is intended for managing servers in
an application where no other event handling is provided nor required.
Once all servers associated with a given interpreter have been created
using \fBtcp server\fR, and the servers have been initialized with
\fIserverName\fB start\fR, a call to \fBtcp mainloop\fR will start to
poll the servers for connections, and once connections are
established, to poll the clients for requests.  The \fBtcp mainloop\fR
procedure will not return until all active clients have been
disconnected, and all active servers have been terminated with
\fIserverName\fB stop\fR.

Instead of using this main loop, an application may choose to
implement its own main loop in Tcl.  In this case, it will
periodically call the \fBtcp poll\fR procedure.  The \fBtcp poll\fR
procedure polls all servers and clients for activity, and executes all
pending requests.  It then returns to its caller the number of
requests that were processed.  A zero return indicates that no
requests were pending.

In order that the main loop have a way to await traffic, the \fBtcp
wait\fR procedure is provided.  A call to \fBtcp wait\fR delays
execution until a server or client has work to do.  It returns a count
of pending requests.  If all clients are disconnected, and all servers
have been shut down via \fIserverName \fBstop\fR, \fBtcp wait\fR
returns zero.

A sample main loop for an application would be:
.DS
.CW
while {[tcp wait] > 0} {
        tcp poll
	\fI...do other, application-specific event handling\fP
}
.DE

If an application uses another package (such as the Tcl-FrameMaker interface)
that uses the \fBsimpleEvent\fR library, its \fBmainloop\fR,
\fBpoll\fR, and \fBwait\fR functions may be used instead.  In other
words, \fBtcp poll\fR is synonymous with \fBfm poll\fR, and so on.
.SH "TCP SERVERS AND THE X WINDOW TOOLKITS"
.PP
.VS
If an application uses the Tk or Xt toolkits, there is no need for a
call to \fBtcp mainloop\fR, \fBtcp poll\fR, and \fBtcp wait\fR.
Instead, the server requests are processed automatically by the
toolkit's main event loop. A Tk application will terminate when all
windows are destroyed.  An Xt application will terminate upon request.
.VE

.VS
If a Tk or Xt application calls \fBtcp mainloop\fR, nothing will happen.  If
it calls \fBtcp poll\fR or \fBtcp wait\fR, the procedures will report
that no servers are known to the event manager.  This error message
is only slightly misleading: the servers are registered with the Tk or Xt
event manager, and not the one that processes the \fBtcp\fR requests.
.VE
.SH "TCP INFORMATIONAL COMMANDS"
.PP
The \fBtcp client\fR command returns the name of the client whose command
is currently being processed.  It gets an error if no client's command is
being processed.

The \fBtcp servers\fR command returns a list of all servers that have been
created in the current process.
.SH "CLIENT OBJECTS"
.PP
Whenever a server accepts a client's connection request, it creates a
new Tcl command, called the \fIclient object,\fR to control the
connection.  The command that it creates can be invoked with one of
the following forms:
.TP
\fIclientObject\fB close\fR
Requests that the connection to the requested client be closed.  If
the Tcl interpreter is currently processing the client's request, the
request processing will be finished and the result returned to the
client before the connection is shut down.
.TP
\fIclientObject\fB command \fR?\fIcommand\fR?
Changes the Tcl command that is used to process requests from the
client.  See ``Login and Command Procedures'' below for a description
of how this is used.
.TP
\fIclientObject\fB do \fR?\fIarg arg ...\fR?
Processes a request originating from a client.  \fIThis is the
internal procedure invoked by \fBtcp poll\fP, \fBtcp mainloop\fP, or
the application's main event loop to start a client request.  It
should never be invoked by the application directly.\fR
.TP
\fIclientObject\fB hostname\fR
Returns the name of the host on which the specified client is running.
.TP
\fIclientObject\fB server\fR
Returns the server object to which the client object originally connected.
.SH "LOGIN AND COMMAND PROCEDURES"
.PP
Managing the procedures that process commands originating from a
client is perhaps the most complex part of writing a TCP-based server.
The problem is maintaining the security of the system, while still
allowing useful work to be done.

When a client first connects, the server must first validate that the
client is authorized to use its services.  Following this, it may
present the entire suite of Tcl commands to the client, or may want to
present only a restricted subset of operations, perhaps with a non-Tcl
syntax.
The \fB-command\fR option to the
\fBtcp server\fR command and the \fBcommand\fR option to the
client object command address these requirements.

When a client first connects, the client object is created, and its
command is set to the string that was specified on the \fB-command\fR
option to the \fBtcp server\fR command.  The client's name is appended
to this string, and the resulting command is evaluated.  Thus, if the
server was created with \fBtcp login\fR as its command (the default),
a connection request will cause the interpreter to execute:
.DS
\fBtcp login \fIclientName\fR
.DE

This procedure is responsible for starting the login negotiation.  The
default \fBtcp login\fR command does the following:
.IP \(bu
It calls ``$\fIclientName \fBhostname\fR'' to get the name of the
client's machine.  It verifies that the host is the local host.
.IP \(bu
If the host is not the local host, it signals an error, and returns
the message, `Permission denied.'
.IP \(bu
If the host is the local host, it changes the client command to \fBtcp
eval\fR, and returns a greeting message.
.PP
Subsequent commands are processed through \fBtcp eval \fIclient
command \fR.  This procedure just calls \fBeval\fR on its second
argument, allowing the client full access to the Tcl interpreter.
.PP
This protocol is perhaps the simplest possible negotiation that
preserves any semblance of security.  An enhanced protocol might be
done in Tcl:
.IP 1.
Set the server's initial command to ``mylogin''.
.IP 2.
When the client connects, the interpreter executes ``mylogin
\fIclientName\fR''.  This procedure changes the client command to
``userPrompt'', and returns a login prompt.
.IP 3.
The client's first request should consist of a username.  The
``userPrompt'' procedure is called with the client name and user name.
It stores the user name away, and sets the client command to
``passwordPrompt.''  It then returns a message consisting of a
password prompt.
.IP 4.
The client's second request consists of a password.  The
``passwordPrompt procedure is called with the client name and
password.  It retrieves the saved username, and validates the
username/password combination.  If the combination is good, it sets
the client command to `tcp eval' as before, and returns a greeting
message.  If the combination is bad, it calls ``$\fIclientName\fB close\fR''
and returns an error message.
.SH "TCP CONNECT"
.PP
The \fBtcp connect\fR command takes the following form:
.DS
\fBtcp connect \fIhostName portNumber\fR
.DE
where \fIhostName\fR is the name of the host on which a server
(created by \fBtcp server\fR) is running, and \fIportNumber\fR is the
port number at which the server is listening.

If the connection request is successful, \fBtcp connect\fR returns a
``connection object'' that describes the connection.  This connection
object is installed as a Tcl command, and may be invoked with one of
the following forms:

.TP
\fIconnectionObject \fBsend \fIarg \fR?\fIarg...\fR?
Executes a remote procedure call on the connected server.  The
arguments are concatenated, and execute as a Tcl command in the remote
application; the result of the \fBsend\fR command is the result of the
remote command.

.TP
\fIconnectionObject \fBclose\fR
Closes a connection and destroys the associated connection object.
This command should be executed when the application no longer has
need of the server.

