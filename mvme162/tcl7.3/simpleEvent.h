/* simpleEvent.h --
 *
 * $Header: /usr/local/cvsroot/musr/mvme162/tcl7.3/simpleEvent.h,v 1.1.1.1 1992/03/04 18:24:21 ted Exp $
 * $Source: /usr/local/cvsroot/musr/mvme162/tcl7.3/simpleEvent.h,v $
 * $Log: simpleEvent.h,v $
 * Revision 1.1.1.1  1992/03/04 18:24:21  ted
 * import from decu18
 *
 * Revision 1.3  1992/03/04  18:24:21  kennykb
 * Added support for Xt event management
 *
 * Revision 1.2  1992/02/21  19:58:52  kennykb
 * Added declaration of simpleSelect, which was inadvertently omitted from the
 * initial version.
 *
 * Revision 1.1  1992/02/20  16:21:43  kennykb
 * Initial revision
 *
 *
 *	This file describes the interface to a trivially simple event
 * manager for Tcl programs that don't want to carry all the overhead of Tk.
 *
 *	It allows for file handlers and WhenIdles.  If you want timers
 * or X events, you're on your own.
 *
 */

#ifndef _SIMPLEEVENT
#define _SIMPLEEVENT 1

#ifndef _TCL
#include <tcl.h>
#endif /* not _TCL */

/* NOTE:  If an application is using the X toolkit intrinsics, it should
 * include <Intrinsic.h> prior to this file.
 */

/* A Simple_FileProc is invoked in response to a selected event on a file. */

typedef void Simple_FileProc _ANSI_ARGS_((ClientData clientData, int mask));

/* A Simple_IdleProc is invoked after all ready files have been handled. */

typedef void Simple_IdleProc _ANSI_ARGS_((ClientData clientData));

/* Flags provided to simpleCreateFileHandler */

#define SIMPLE_READABLE 1
#define SIMPLE_WRITABLE 2
#define SIMPLE_EXCEPTION 4
#define SIMPLE_DELETE 8
				/* SIMPLE_DELETE is used to mark a handler */
				/* for deletion.  The user shouldn't use it. */

/* Flags provided to simpleSelect */

#define SIMPLE_DO_EVENTS 1
				/* 1 - Do pending events */
				/* 0 - Poll only */
#define SIMPLE_WAIT 2
				/* 1 - Wait if no events are pending, */
				/*     handling WhenIdle calls first */
				/* 0 - Return immediately if no events are */
				/*     pending. */

/* If the X intrisics are not included, dummy up an application context
 * object class
 */

#ifndef _XtIntrinsic_h
typedef struct _XtAppStruct * XtAppContext;
#endif

/* Procedures provided to user code */

extern void simpleSetAppContext _ANSI_ARGS_((XtAppContext));

extern void simpleCreateFileHandler _ANSI_ARGS_((int fd,
						 int mask,
						 Simple_FileProc * proc,
						 ClientData clientData));

extern void simpleDeleteFileHandler _ANSI_ARGS_((int fd));

extern void simpleDoWhenIdle _ANSI_ARGS_((Simple_IdleProc * proc,
					  ClientData clientData));

extern void simpleCancelIdleCall _ANSI_ARGS_((Simple_IdleProc * proc,
					      ClientData clientData));

extern int simpleSelect _ANSI_ARGS_((int flags));

extern void simpleReportBackgroundError _ANSI_ARGS_((Tcl_Interp * interp));

#endif /* not _SIMPLEEVENT */
