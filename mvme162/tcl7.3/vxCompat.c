/******************************************************************************
*                                                                             *
* Copyright (c) 1991,							      *
* 	National Center for Atmospheric Research                              *
*                                                                             *
******************************************************************************/
    
#if !defined(lint) && !defined(VX)
static char rcsid[]=
"$Header: /usr/local/cvsroot/musr/mvme162/tcl7.3/vxCompat.c,v 1.1.1.1 1993/11/02 20:21:04 ted Exp $";
#endif
#include <vxWorks.h>

/*
 * compatability routines for Unix code
 */
#include "tclInt.h"
#include "tclUnix.h"


#ifdef NEED_ACCESS	/* now found in softclock/stubs.c */
/*
 * a very limited implementation of the Unix system call,
 * sufficient for use by TCL
 */
int
access(char *name, int mode)
{
    int fd;


    switch (mode)
    {
	case R_OK:
	    if ((fd = open(name, O_RDONLY, 0)) < 0) 
	    {
		return(-1);
	    }
	    (void) close(fd);
	    break;
	case W_OK:
	    if ((fd = open(name, O_WRONLY, 0)) < 0) 
	    {
		return(-1);
	    }
	    (void) close(fd);
	    break;
	default:
	    return(-1);
    }
    return(0);
}
#endif
#include <tickLib.h>
/*
 * provided time in ticks for timing purposes
 */

int times(struct tms *dummy)
{
    return(tickGet());
}

#ifdef NOTDEF
abort()
{
    logMsg ("abort called - task suspended\n");
    taskSuspend(0);
}
#endif
    
/******************************************************************************
* Revision history:                                                           *
* $Log: vxCompat.c,v $
* Revision 1.1.1.1  1993/11/02 20:21:04  ted
* import from decu18
*
 * Revision 1.1  1993/11/02  20:21:04  vanandel
 * Initial revision
 *
*                                                                             *
*********************************END OF RCS INFO******************************/


