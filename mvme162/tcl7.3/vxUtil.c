
/******************************************************************************
*                                                                             *
* Copyright (c) 1991,							      *
* 	National Center for Atmospheric Research                              *
*                                                                             *
******************************************************************************/
    
#if !defined(lint) && !defined(VX)
static char rcsid[]=
"$Header: /usr/local/cvsroot/musr/mvme162/tcl7.3/vxUtil.c,v 1.1.1.1 1995/01/31 20:39:11 ted Exp $";
#endif
#define MAX_SYM 200
#include <vxWorks.h>
#include <sysSymTbl.h>
#include <a_out.h>
#include <stdioLib.h>
#include <ioLib.h>
#include <shellLib.h>
#include <taskLib.h>

#include <logLib.h>



#include "tcl.h"

#define MAXLINE 200

extern void tt (char *cmd);
#define CHAR_EOF ''

/* NOTE - using WRS "execute()" seems to cause heap errors, but the rshell
 * provided by Real Time Innvations works just fine!
 *
 * #define USE_SHELL
 */

/* 
 * execute  command under vxWorks 
 * first argument must be a vxworks entry point
 * WARNING - because the shell isn't re-entrant, neither is this code!
 */
int
Tcl_VxExecCmd(ClientData *clientData, Tcl_Interp *interp, int argc, char *argv [])
{ 
    char *cmd = Tcl_Concat(argc-1, argv+1);
    int rc = TCL_OK;
    int bytes;
    int server, conn;
    char dummy;
    static char buf[MAXLINE+1];
    extern void doExecute();
    int status;

    static int pipeCreated = ERROR;
    int slave;
    int master;

#ifdef USE_SHELL
    if (shellLock(TRUE) == FALSE)
    {
	Tcl_AppendResult (interp, argv[0], 
	"ERROR: shell locked - can't execute\"", cmd, "\"", (char *) NULL);
	ckfree(cmd);
	return TCL_ERROR;
    }
#endif
    if (pipeCreated == ERROR) {
	pipeCreated = ptyDevCreate("/pty/0.", 512, 512);
	if (pipeCreated == ERROR) {
	    Tcl_AppendResult (interp, argv[0], 
		"ERROR: can't create pipe to capture output of \"", cmd, "\"", (char *) NULL);
	    ckfree(cmd);
	    return TCL_ERROR;
	}
    }
    if ((master = open("/pty/0.M", O_RDWR, 0)) == ERROR) {
	printErrno(0);
	Tcl_AppendResult (interp, argv[0], 
	    "ERROR: can't open master pipe to capture output of \"", cmd, "\"", (char *) NULL);
	ckfree(cmd);
	return TCL_ERROR;
    }

    if ((slave = open("/pty/0.S", O_RDWR, 0)) == ERROR) {
	printErrno(0);
	Tcl_AppendResult (interp, argv[0], 
	    "ERROR: can't open slave pipe to capture output of \"", cmd, "\"", (char *) NULL);
	close(master);
	ckfree(cmd);
	return TCL_ERROR;
    }


    /* start the task */
    doExecute(slave, cmd, master);

    /* read output of command, and return as our result */
    /* NOTE- if I read > 1 bytes, this task hangs on the last read,
     * even after our "child" has closed its end of the pty
     * reading 1 byte at a time is slow, but at least we can identify the
     * '^D' 
     */
    while ( (bytes = read(master, buf, 1)) > 0 )
    {
	buf[bytes] = '\0';	/* null terminate string */
	if (buf[0] == CHAR_EOF) break; 
	Tcl_AppendResult (interp, buf, (char *) NULL);
    }

    (void) close(master);
#ifdef USE_SHELL
    shellLock(FALSE);
#endif

    return rc;

}
#define MAXHOST 30
#ifdef USE_SHELL
extern int execute(char *);
#else
extern int rshellParseCommand(char *);
#endif

extern void executeAndClose(int portNum, char* cmd, int master);
int VxExecPri = 1;	/* patchable priority for created commands */

void
doExecute(int portNum, char *cmd, int master)
{
    extern int shellTaskPriority;
    extern int shellTaskId;
    extern int VxExecPri;
    int pri ;
#ifdef NOTDEF
    if (taskPriorityGet(shellTaskId, &pri) == ERROR)
    {
	pri = shellTaskPriority;
    }
#else
    pri = VxExecPri;	/* may want at higher priority than shell */
#endif


    if (taskSpawn( "exec", pri, VX_FP_TASK| VX_STDIO, 10000, 
	(FUNCPTR) executeAndClose, portNum, cmd, master) == ERROR)
    {
	logMsg("ERROR: %s: %d - taskSpawn failed!\n", __FILE__, __LINE__);
	taskSuspend(0);
    }
}

/* execute command, and close STD_OUT when complete,
 * so caller knows we are done
 * I also found it necessary to write CHAR_EOF on the output string,
 * so the parent task could know we're done.
 */
void executeAndClose(int portNum, char *cmd, int master)
{
    int rc = OK;
    int oldStdOut, oldStdErr;
    char dummy = CHAR_EOF;
    int bytesUnread;
    

  /*
   * Set IO redirection.
   */

    ioTaskStdSet (0, STD_OUT, portNum);    /* set std out for execute() */
    ioTaskStdSet (0, STD_ERR, portNum);    /* set std out for execute() */

#ifdef USE_SHELL
    rc = execute(cmd); 
#else
    rc = rshellParseCommand(cmd);
#endif
    ckfree(cmd);
    fflush(stderr);
    fflush(stdout);
    write(portNum, &dummy, 1);		/* indicate EOF */
    write(portNum, &dummy, 0);		/* indicate EOF */
    close(portNum);

#ifdef NOTDEF
    /* This is pretty gross - but I don't know how else to force 
     * our parent to stop reading ! 
     * (closing the slave side has no effect, neither does writing ^D)
     */

    rc = ioctl(master, FIONREAD, &bytesUnread);
    while ((rc != ERROR) && (bytesUnread > 0)){
	rc = ioctl(master, FIONREAD, &bytesUnread);
    }
    close(master);	
#endif
}

	/* ARGSUSED */
int
cmdVxTaskInfo(clientData, interp, argc, argv)
    ClientData clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    if (argc != 1) {
	Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
		"\"", (char *) NULL);
	return TCL_ERROR;
    }

    ti(0);

    return TCL_OK;
}
#ifdef NOTDEF

int
cmdVxExec(clientData, interp, argc, argv)
    ClientData clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    SYM_TYPE	type;
    int		(*pFn)();
    int		ret;
    char	buf[100];

    if (argc != 2) {
	Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
		"\" vxcmd", (char *) NULL);
	return TCL_ERROR;
    }

    /* We need _symbolname for lookup */
    strcpy(buf, "_");
    strcat(buf, argv[1]);

    /* Find the named symbol in the system symbol table (text symbols only) */
    if (symFindByNameAndType(sysSymTbl, buf, (char **)&pFn, &type,
			     N_TEXT, N_TEXT) == OK)
    {
	ret = pFn(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	printf("return value %d %#x\n", ret, ret);
    }
    else
    {
	perror("vx: symFindByNameAndType");
    }

    return TCL_OK;
}
#endif

int
cmdVxGetVar(clientData, interp, argc, argv)
    ClientData clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    SYM_TYPE	type;
    char	*value;
    int		(*pFn)();
    int		ret;
    char	buf[100];

    if (argc != 3) {
	Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
		"\" [float | long| double| short]  varName", (char *) NULL);
	return TCL_ERROR;
    }

    /* We need _symbolname for lookup */
    strcpy(buf, "_");
    strcat(buf, argv[2]);

    /* Find the named symbol in the system symbol table  */
    if (symFindByName(sysSymTbl, buf, (char **)&value, &type)== OK)
    {
	if (strcmp(argv[1], "long")==0) {
	    sprintf(buf, "%d", *(long *) value);
	} else if (strcmp(argv[1], "lhex")==0) {
	    sprintf(buf, "0x%x", *(long *) value);
	} else if  (strcmp(argv[1], "float")==0) {
	    sprintf(buf, "%f", *(float *) value);
	} else if  (strcmp(argv[1], "double")==0) {
	    sprintf(buf, "%f", *(double *) value);
	} else if  (strcmp(argv[1], "short")==0) {
	    sprintf(buf, "%d", *(short *) value);
	} else if  (strcmp(argv[1], "shex")==0) {
	    sprintf(buf, "%0x%x", *(short *) value);
	} else {
	    Tcl_AppendResult(interp, "couldn't understand: \"", argv[1],
		"\"", (char *) NULL);
	    return TCL_ERROR;
	}
	Tcl_AppendResult (interp, buf, (char *)NULL);
    }
    else
    {
	Tcl_AppendResult(interp, "couldn't find  \"", argv[2],
		"\"", (char *) NULL);
	perror("vx: symFindByName");
	return TCL_ERROR;
    }

    return TCL_OK;
}
int
cmdVxSetVar(clientData, interp, argc, argv)
    ClientData clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    SYM_TYPE	type;
    char	*value;
    int		(*pFn)();
    int		ret;
    char	buf[100];
    int		vi;
    double	vf;
    int 	status;

    if (argc != 4) {
	Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
		"\" [float | long| double| short]  varName value", (char *) NULL);
	return TCL_ERROR;
    }

    /* We need _symbolname for lookup */
    strcpy(buf, "_");
    strcat(buf, argv[2]);

    /* Find the named symbol in the system symbol table  */
    if (symFindByName(sysSymTbl, buf, (char **)&value, &type)== OK)
    {
	if (strcmp(argv[1], "long")==0) {
	    status = Tcl_GetInt(interp, argv[3], &vi);
	    if (status) return status;
	    *(long *) value = vi;
	} else if  (strcmp(argv[1], "float")==0) {
	    status = Tcl_GetDouble(interp, argv[3], &vf);
	    if (status) return status;
	    *(float *) value = vf;
	} else if  (strcmp(argv[1], "double")==0) {
	    status = Tcl_GetDouble(interp, argv[3], &vf);
	    if (status) return status;
	    *(double *) value = vf;
	} else if  (strcmp(argv[1], "short")==0) {
	    status = Tcl_GetInt(interp, argv[3], &vi);
	    if (status) return status;
	    *(short *) value = vi;
	} else {
	    Tcl_AppendResult(interp, "couldn't understand: \"", argv[1],
		"\"", (char *) NULL);
	    return TCL_ERROR;
	}
    }
    else
    {
	Tcl_AppendResult(interp, "couldn't find  \"", argv[2],
		"\"", (char *) NULL);
	perror("vx: symFindByName");
	return TCL_ERROR;
    }

    return TCL_OK;
}
int
cmdVxAddCmd(clientData, interp, argc, argv)
    ClientData clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    SYM_TYPE	type;
    int		(*pFn)();
    int		ret;
    char	buf[100];

    if (argc != 2) {
	Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
		"\" cmdname", (char *) NULL);
	return TCL_ERROR;
    }

    /* We need _symbolname for lookup */
    strcpy(buf, "_");
    strcat(buf, argv[1]);

    /* Find the named symbol in the system symbol table (text symbols only) */
    if (symFindByNameAndType(sysSymTbl, buf, (char **)&pFn, &type,
			     N_TEXT, N_TEXT) == OK)
    {
	Tcl_CreateCommand(interp, argv[1], pFn, (ClientData) 0,
	    (Tcl_CmdDeleteProc *) NULL);
	return TCL_OK;
    }
    else
    {
	Tcl_AppendResult(interp, "Couldn't find vxWorks symbol \"",
			 argv[1], "\".", NULL);
	return TCL_ERROR;
    }
}

	/* ARGSUSED */
int
cmdEcho(clientData, interp, argc, argv)
    ClientData *clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    int i;

    for (i = 1; ; i++) {
	if (argv[i] == NULL) {
	    if (i != argc) {
		echoError:
		sprintf(interp->result,
		    "argument list wasn't properly NULL-terminated in \"%s\" command",
		    argv[0]);
	    }
	    break;
	}
	if (i >= argc) {
	    goto echoError;
	}
	fputs(argv[i], stdout);
	if (i < (argc-1)) {
	    printf(" ");
	}
    }
    printf("\n");
    return TCL_OK;
}
void vxDemo(double f, int i, char *s)
{
    fprintf(stderr, "vxDemo called with f = %f, i = %d s = %s\n", f, i, s);
}

extern SEM_ID miscSem;

/* protect the following command from any other TCL interpreter
 *
 */

int
vxProtect(clientData, interp, argc, argv)
    ClientData *clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    char *cmdbuf;
    int rc;

    if (argc <=  1) {
        Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
                "<commands>", (char *) NULL);
	return TCL_ERROR;
    }
    cmdbuf =  Tcl_Merge(argc -1, &argv[1]);
    if (semTake(miscSem, WAIT_FOREVER) != OK) {
	fprintf(stderr, "couldn't take miscSem!\n");
	taskSuspend(0);
    }
    rc = Tcl_Eval(interp, cmdbuf);
    ckfree(cmdbuf);

    if (semGive(miscSem) != OK) {
	fprintf(stderr, "couldn't give miscSem!\n");
	taskSuspend(0);
    }
    
    return rc;

}

int
vxTCLDemo(clientData, interp, argc, argv)
    ClientData *clientData;
    Tcl_Interp *interp;
    int argc;
    char *argv[];
{
    int i;
    double d;
    char *s;

    if (argc != 4) {
	Tcl_AppendResult(interp, "wrong # args: should be \"", argv[0],
		"\" float int string", (char *) NULL);
	return TCL_ERROR;
    }
    if (Tcl_GetDouble(interp, argv[1], &d) != TCL_OK){
	return TCL_ERROR;
    }
    if (Tcl_GetInt(interp, argv[2], &i) != TCL_OK){
	return TCL_ERROR;
    }
    vxDemo(d, i, argv[3]);
    return TCL_OK;
}
/******************************************************************************
* Revision history:                                                           *
* $Log: vxUtil.c,v $
* Revision 1.1.1.1  1995/01/31 20:39:11  ted
* import from decu18
*
 * Revision 1.5  1995/01/31  20:39:11  vanandel
 * : Modified Files:
 * : 	README.vxworks RELEASE.vxworks simpleEvent.c tclTCP.c vxUtil.c
 * : ----------------------------------------------------------------------
 * new release notes
 * fixed memory leaks in tclTCP.c
 *
 * Revision 1.4  1994/01/06  18:16:27  vanandel
 * : 	README.vxworks RELEASE.vxworks tclBasic.c tclEnv.c tclUnixAZ.c
 * : 	vxUtil.c
 * : ----------------------------------------------------------------------
 * See change log in README.vxworks
 *
 * Revision 1.3  1993/11/02  20:21:04  vanandel
 * *** empty log message ***
 *
 * Revision 1.2  1993/10/14  14:52:18  vanandel
 * first working version of vxgetvar
 *
 * Revision 1.1  1993/10/12  15:28:14  vanandel
 * Initial revision
 *
 * Revision 1.4  1992/10/21  00:33:48  vanandel
 * command execution with results returned to TCL caller
 *
 * Revision 1.3  1992/10/20  21:40:55  vanandel
 * pass everything BUT the first argument to "execute"
 *
 * Revision 1.1  1992/10/20  20:31:07  vanandel
 * Initial revision
 *
*                                                                             *
*********************************END OF RCS INFO******************************/


