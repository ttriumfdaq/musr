# required by perlscripts
#
# Check that only one copy of the perlscript $name is running
# and that input parameters are supplied
#
# $Log: init_check.pl,v $
# Revision 1.9  2015/03/29 00:59:01  suz
# correct outfile path for musr experiments
#
# Revision 1.8  2015/03/25 23:31:23  suz
# use sub check_beamline. Updated for MUSR VMIC new beamlines
#
# Revision 1.7  2008/04/17 21:39:08  suz
# displays num optional params
#
# Revision 1.6  2005/09/26 17:50:00  suz
# change message to indicate too few or too many parameters supplied
#
# Revision 1.5  2004/11/08 20:58:24  suz
# make a print statement if(debug)
#
# Revision 1.4  2004/10/04 21:54:53  suz
# add a debug
#
# Revision 1.3  2004/06/10 18:27:15  suz
# check for valid beamline after check on expt
#
# Revision 1.2  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.1  2004/06/09 00:28:37  suz
# initial version - common code checks only one copy running
#
#
use strict;
our ($TRUE,$FALSE,$DIE,$CONT,$MERROR,$MINFO,$MTALK,$EXPERIMENT);
our ($inc_dir, $expt, $beamline ) ; # no other input parameters are needed here
our $len;
our $name ; # same as filename
our $parameter_msg;
our $nparam;  # no. of input parameters
our $outfile;
our ($status,$suppress);

my $process_id;
my $debug=$FALSE;
#
$nparam = $nparam + 0;  #make sure it's an integer
#
# Check the parameters:
#
#


# can't check if script is running already unless there's an experiment; can't msg either
unless ($expt) {    die  "$name:  FAILURE -  No experiment supplied \n"; }
# send msg only if $EXPERIMENT is defined
if ($expt) { $EXPERIMENT = $expt; }

# we must have a beamline or we can't check for second copy running
unless ($beamline) 
{
# we can print to screen. Can't print to file yet since outfile not open
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 

# msg as there's an experiment
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: no beamline is supplied") ; 
    die  "$name:  FAILURE -  No beamline supplied \n";
}

# check if another copy of this perlscript is running
#
#

$process_id = "unknown";
open PS, "ps -efw |";
while (<PS>) 
{
    if ($debug){ print "$_";}

    if (/ sh -c /)  #Ignore shell wrappers like "sh -c /path/to/script.pl"; only search for "/path/to/script.pl"
    {    
        next;
    }
    if (/[\s]([\d]+)[\s].*perl.*$name.pl.*$beamline/) # look for space +1 or more digits
                                          # followed by space etc
    {
	$process_id = $1;
	print "$expt on beamline \"$beamline\" is running  with PID $process_id\n" ;
	if ($$ == $process_id)
	{ 
	    if($debug){ print "$name: this is my process\n";}
	}
	else 
	{ 
	    # there is no file open yet
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: a copy of $name is already running with PID: $$") ;
	    unless($status) { print "$name: Failure from odb_cmd (msg)";} # no file yet
	    die  "$name:  FAILURE -   a copy of $name is already running with PID: $$\n";
	}
    }
}
close(PS) ;


$len += 1; # no. of supplied parameters

if ($debug) {print  "No. parameters supplied = $len\n";}
unless ($len >= $nparam)
{
# we can print to screen. Can't print to file yet since outfile not open
    print  "$name:   Supplied parameters: @ARGV\n";
    print  "Invoke this perl script $name with the parameters:\n";
    print  "   $parameter_msg\n"; 
    
# msg as there's an experiment
    if ($len < $nparam)
    {
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too few input parameters supplied " ) ;
    }
    else
    {
	($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Too many input parameters supplied " ) ;
    }
    
    unless($status) { print  "$name: Failure from odb_cmd (msg)";} #no file yet
    die  "$name:  FAILURE -  Incorrect number of parameters supplied ";
}
if ($len > $nparam)
{
    my $tmp = $len-$nparam;
    print  "$name: $nparam required parameters supplied, plus $tmp optional parameter(s)\n";
}

# check for valid experiment and beamline
# If more beamlines are added, add them to  @valid_beamlines array in sub check_beamline($)
# code numbers are  @valid_beamlines array indices
my $code = check_beamline($expt);   # code = 9 FAILURE  code < 3   BNMR/POL; otherwise MUSR experiment
if ($code >= 9)
{ 
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: unknown experiment $expt ") ;
    die  "$name:  FAILURE - unknown experiment \"$expt\" \n ";
}
elsif ($code < 3)
{
    #  print "valid BNMR-type expt $expt\n";  # BN[MQ}R/POL   path for outfile is different from MUSR-type
    $outfile=sprintf("%s/%s",$beamline,$outfile);
}
# for MUSR experiments, our $outfile contains the correct name.

# now open the output file since $beamline is defined
#$outfile=sprintf("%s/%s",$beamline,$outfile);
$status = $FALSE;
$status=open_output_file($name, $outfile, $process_id, $suppress);
if ($debug) { print_2 ($name,"init_check returning with status=$status",$CONT);}
#
# perl module must end with a 1
1; # return a good status
#
