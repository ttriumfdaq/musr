#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#
#    MUSR only - toggle v680 display
#
#   Normally invoked from display button
# 
# invoke this script with cmd
#                  include                        experiment  display  beamline
#                   path                                         flag
# musr_display.pl  /home/musrdaq/musr/perl             m20c       y      m20c      
#
# Toggles the display flag in odb (v680 area).
# v680 display is hot linked to ../v680/display so that the display on the
# ppc window  will be toggled between enable and disable
# 
#
# $Log: musr_display.pl,v $
# Revision 1.9  2015/03/25 23:52:07  suz
# added call to check_beamline. This version works for VMIC beamlines
#
# Revision 1.8  2004/06/10 18:13:36  suz
# require init_check; use our; last Rev message is bad
#
# Revision 1.7  2004/06/09 21:56:06  suz
# include init_check.pl; change use vars to our; Rev 1.6 message is erroneous
#
# Revision 1.6  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.5  2004/04/21 19:10:42  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.4  2004/04/08 17:47:01  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.3  2004/02/09 22:27:02  suz
# moved here from musr_mdarc
#
#
# Revision 1.2  2002/04/30 22:58:12  suz
# fix small bug
#
# Revision 1.1  2002/04/14 03:44:16  suz
# original: toggles v680 display
#

use strict;


##################### G L O B A L S ####################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir, $expt, $display_flag, $beamline ) = @ARGV;
our $name = "display"; # same as filename
our $len =  $#ARGV; # array length
our $nparam=4;
our $outfile = "display.txt";
our $parameter_msg = "include_path, experiment; display flag; beamline\n";
#######################################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($v680_path,$len) ;
my $debug=$FALSE;
my $eqp_name;
#########################################################

$|=1; # flush output buffers

# input parameters:


# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

#
# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; display flag = $display_flag\n";
#

# toggle display flag
if ($display_flag eq "y") { $display_flag = $FALSE;}
else { $display_flag = $TRUE ; }

#
#      determine equipment name from beamline
#
my $code = check_beamline($expt);   # code = 9 FAILURE  code < 3   BNMR/POL; otherwise MUSR experiment
if( $code < 3)
{
    
    # Not supported for BNMR experiment
    odb_cmd ( "msg","$MINFO","","$name", "INFO: routine not supported for BNMR experiments" ) ;
    print FOUT "$name: not supported for BNMR experiment";
    die "$name: not supported for BNMR experiment";
}

# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
$v680_path = "/Equipment/$eqp_name/v680/";

print FOUT "v680_path = $v680_path\n";

($status) = odb_cmd ( "set","$v680_path","display" ,"$display_flag") ;
unless($status)
  { 
    print FOUT "$name: Failure from odb_cmd (set); Error updating display flag.\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE updating display flag" ) ;
    die "$name: Failure updating display flag";
  }
print FOUT "INFO: Success - Display flag has been toggled \n";
odb_cmd ( "msg","$MINFO","","$name", "INFO: Display flag has been toggled " );
print "Success - Display flag has been toggled \n";

exit;

