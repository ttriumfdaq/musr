#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#
use warnings;
use strict;

#    MUSR beamlines only - monitor beam current
#    Run this as musrdaq on host midm15  (permissions set up on that host only)
#    Run as cron job during the beam period (except for testing)
#    Monitor files will be found under /data/mon/<year>/<beamline>/*.txt
#    Log file is under  /data/mon/<year>/
#
require "/home/musrdaq/musr/perl/perlmidas.pl";

# prototypes
sub get_filename_for_shift($);
sub read_epics_value($);
sub send_ssh_cmd($); # returns $err, $num, $sample
sub read_odb_data($); # returns $err, $num, $sample (replaces send_ssh_cmd)
sub send_command($);
sub write_line($$$$$);  # prototype with 5 arguments
sub add_new_exp($$$$);
sub add_year($);
sub get_digits($);

# input parameters   minimum BL1A proton current
#         if $noprint > 0, no output is printed
#   cron job should suppress printing otherwise email will be sent out
#   For cron job,  crontab -e    
#       e.g. monitor.pl  21.45 1
#
#   To stop cron job, comment out monitor.pl line
#
#   To try this file, with printing
#       e.g. monitor.pl 11.4
#
#   
#   For TESTING, 
#                            thresh noprint testing enab_m15 enab_m20 enab_m9b  skip_file
#      e.g. ./musr_monitor.pl 12.5     0       1        1         1      0         0
#        or ./musr_monitor.pl    0     0       1        1         1      0         0
# 
#      NOTE:
#      When testing is TRUE, setting threshold to 0 skips reading the EPICS value for beam current
#
# 
#
#         if $testing is true:
#            1.  line in file will have a "*" beside it (except for case 3 below)
#            2.  if thresh is set to 0, reading EPICS beam current BL1A will be skipped
#            3.  if $skip_file is true, suppresses any updating of shift files 
#
#         for real or testing,
#            1.  set enab_m15 to monitor m15
#            2.  set enab_m20 to monitor m20c and m20d
#            3.  set enab_m9 to monitor m9a and m9b
#            4.  to change expt numbers, change in the odb of each expt, e.g.
#                 odb -c 'set "/experiment/edit on start/experiment number" 1509'   
#            5.  to change sample, change in the odb of each expt, e.g.
#                 odb -c 'set "/experiment/edit on start/sample" XYZ123'
#            6.  a new line will be written in the monitor file if the expt number OR sample is changed
# 
## this address from Evgeniy for reading BL1A proton current
##setenv EPICS_CA_ADDR_LIST 142.90.140.222:9012
##setenv EPICS_CA_AUTO_LIST NO
## these are set up in the musrdaq .cshrc on midm15
  
our ($min_proton_current, $noprint, $testing,  $enab_m15, $enab_m20, $enab_m9, $skip_file) = @ARGV;# input parameters
my $len = $#ARGV +1; # array length
$skip_file += 0; #input parameter

our $DEBUG = 0;
our $MIDAS_EXPERIMENT;
our $MONITOR_PATH = "/data/mon/"; # current year and beamline will be added to this path
our $MONITOR_DIR; # this will be $MONITOR_PATH.  Year and Beamline will be added -> beamline_dir
my  $beamline_dir; # this will be $MONITOR_DIR with year and beamline added.
our $MONLOG_FH;
our $MIDAS_RN_PATH = "/Runinfo/Run number";
our $EXP_NUM_PATH = "/Experiment/Edit on start/experiment number";

my $EPICS_PROTON_CURRENT="BL1:XPRB:RDCUR";  # MUSR read proton current
my @beamlines; # array of enabled beamlines
my $EPICS_TEMP; # find something to read to stand in for proton current for testing
my $name="musr_monitor_try";
my $logfilename=$name;
my $read_proton_current=0;
my ($filename,$tempfile,$string);
my($done,$linenum);
# time
#my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
my ($dual_channel_mode,$manual_select);
my ($cmd,$status);
my $xm; # AM or PM
my($time_min);
# temp
my($test_bnmr_enabled, $test_bnqr_enabled, $bnmr,$bnqr, $bnmr_exp_num, $bnqr_exp_num);

# Required information to update the entry
my ($exp_num, $sample); # experiment number, sample
my $proton_uAmps; # current proton current

$min_proton_current +=0; # input parameter  
$noprint += 0; #input parameter
$testing += 0; #input parameter
$enab_m20 += 0; #input parameter 
$enab_m15 += 0; #input parameter
$enab_m9 += 0; #input parameter
my $ans;
my $err=0;

my $host= $ENV{'HOST'}; 
unless ($host=~/midm15/i)
{ die "$name: run this monitor programme on host midm15 only"};

# Make sure directory exists (no beamline or year yet)
if (-d $MONITOR_PATH) 
{ 
    unless ($noprint){ print "$name: found existing directory $MONITOR_PATH\n"};
}
else
{
    $ans =  `mkdir $MONITOR_PATH`;
    if( $!)
    {
	#print LOG   "$name: failure creating directory \"$MONITOR_PATH\"   : $!\n"; #log not open
	die  "$name: failure creating directory \"$MONITOR_PATH\"  : $!\n";
    } 
}

my  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(); # get present localtime to find year
$year+=1900; # year since 1900

$MONITOR_DIR = add_year($year); # returns full path with year. Creates directory if absent
my $logfile = $MONITOR_PATH.$logfilename.".log"; # logfile is now in /data/mon  (no year)

# open log file
if(-e $logfile)
{
    open LOG,">> $logfile" or die $!;
    unless($noprint){ print "Opened log file $logfile for append\n";}
}
else
{
    open LOG,"> $logfile" or die $!;
    unless($noprint){ print "Opened new log file $logfile for write\n";}
}
my $datestring =  localtime();
print LOG "\n\n -----------  $name starting at $datestring ---------------\n";
print LOG "Input parameters: min proton current=$min_proton_current; noprint=$noprint; testing=$testing; enable m15=$enab_m15, m20=$enab_m20, m9=$enab_m9; skip_file=$skip_file\n";


# build beamline array
if($enab_m15 == 1)
{ push @beamlines, "m15"; }
if($enab_m20 == 1)
{ 
    push @beamlines, "m20c"; 
    push @beamlines, "m20d"; 
}
if($enab_m9 == 1)
{ 
    push @beamlines, "m9a"; 
    push @beamlines, "m9b"; 
}
my $bl= $#beamlines +1;
unless($noprint) { print "beamlines array: @beamlines and length=$bl\n";}

unless ($bl > 0)
{
    print LOG "$name: no beamlines (m15/m20/m9) are enabled. Nothing to do\n";
    unless($noprint) {print "$name: no beamlines (m15/m20/m9) are enabled. Nothing to do\n";}
}


#print LOG "$name: testing email gets sent\n";
#print "$name: testing email gets sent\n";

MIDAS_env(); # setup for MIDAS BNMR 

###  TESTING only ####
if ($testing)
{
    unless($noprint) { print "***  Testing is TRUE  ***\n";}
    print LOG  "***  Testing is TRUE  ***\n";
}


unless ( $testing &&  ($min_proton_current==0))
{
# Check if the proton current is above threshold
    $read_proton_current = read_epics_value($EPICS_PROTON_CURRENT);
    if ($read_proton_current =~ /err/)
    {
	print LOG "$name: error from read_epics_value($EPICS_PROTON_CURRENT)\n";
	if ($testing)
	{
	    unless ($noprint){print "$name:  error from read_epics_value($EPICS_PROTON_CURRENT)... continuing as testing is TRUE\n";}
	    print LOG "        ... continuing as testing is TRUE. \n";
	    $read_proton_current = 0;
	}
	else
	{
	    die  "$name: error from read_epics_value($EPICS_PROTON_CURRENT). Cannot read proton current from EPICS\n"; 
	}
    }

    $read_proton_current +=0; # already checked that this is a number

    if($read_proton_current < $min_proton_current)
    {
	unless ($testing)
	{
	    unless($noprint){ print "$name: proton current is too low : $read_proton_current < $min_proton_current (uA). Nothing to do.\n"; }
	    print LOG  "$name: proton current is too low : $read_proton_current < $min_proton_current (uA), Nothing to do.\n";
	    exit; 
	}
    
	unless($noprint){ print "$name: proton current is too low : $read_proton_current < $min_proton_current (uA). Continuing as testing is TRUE\n"; }
	print LOG  "$name: proton current is too low : $read_proton_current < $min_proton_current (uA). Continuing as testing is TRUE\n";
	
    }
} # read proton current from epics
else
{
    	print LOG "$name: NOT reading proton current from epics (testing is true and threshold is set to 0) \n";
	unless ($noprint){ print "$name: NOT reading proton current from epics (testing is true and threshold is set to 0) \n";}
	$read_proton_current=0+0; # numerical
}


# do for each beamline...
my $beamline;

for $beamline (@beamlines)
{
    print LOG "\n $name:=====  Working on beamline $beamline... =====  \n";
    unless($noprint){  print "\n=====  Working on beamline $beamline... =====  \n";}

    $exp_num =0+0; # numerical

#Determine the experiment number
    
    # Use odbedit and mserver to get beamline's experiment number
    ($err, $exp_num, $sample) = read_odb_data($beamline);
    if ($err)
    {	
	print LOG "$name: error getting experiment number or sample for beamline $beamline using read_odb_data\n";
	unless($noprint){ print "$name: error getting experiment number or sample for beamline $beamline using read_odb_data\n"; }
        # mserver may not be running. 
        
        # Try again using ssh to get beamline's experiment number 
	print LOG "$name: now trying to get experiment number or sample for beamline $beamline using ssh\n"; 
	unless($noprint){ print "$name: now trying to get experiment number or sample for beamline $beamline using ssh\n"; }
        $err=0;
	($err, $exp_num, $sample) = send_ssh_cmd($beamline); 	    
	if ($err)
	{  # print (or sends a mail message) 
	    print  "$name: error getting expt number/sample for beamline $beamline  - see $logfile \n"; 
            print LOG "$name: Cannot get information from beamline $beamline. Assume $beamline not running \n";
          # continue with other beamlines
	    $exp_num = 9999; # assign number for this beamline not running
            $sample = "unknown";
	}
    }

    $exp_num += 0; # numerical (for perl)
    unless ($noprint){ print "\n experiment number for beamline $beamline is $exp_num and sample=$sample\n";}  # read_odb_data checked that response was numerical

    print LOG  "$name: after send_ssh_cmd, experiment number for beamline $beamline is $exp_num and sample=$sample \n";
    unless ($exp_num > 0)
    {
	print LOG  "$name: ERROR - illegal experiment number ($exp_num) for beamline $beamline\n";
	unless ($noprint){ print "$name:  ERROR -  illegal experiment number ($exp_num) for beamline $beamline\n";}
	next;   # next beamline
    }
    
    unless($exp_num < 9999)
    {
	print LOG  "$name: experiment number ($exp_num) indicates beamline $beamline is not running\n";
	unless ($noprint){ print "$name:  experiment number ($exp_num) indicates beamline $beamline is not running\n";}
	next; # next beamline
    }


    unless ($sample =~/\S/)
    { 
	print LOG  "$name: ERROR - illegal sample string (empty string) for beamline $beamline\n";
	unless ($noprint){ print "$name:  ERROR -  illegal sample string (empty string) for beamline $beamline\n";}
	next;   # next beamline
    }
    

    
    
# Assemble beamline_dir path and make sure it exists
    # if $beamline is m20c/d  beamline_path is m20
    # if $beamline is m9c/d   beamline_path is m9
    # if $beamline is m15     beamline_path is m15
    my $beamline_path;
    if   ($beamline =~ /^m20/){ $beamline_path="m20"; }
    elsif($beamline =~ /^m9/){ $beamline_path="m9"; }
    else { $beamline_path = $beamline;}
    
    $beamline_dir=$MONITOR_DIR.$beamline_path."/";
    # Make sure directory exists
    if (-d $beamline_dir) 
    { 
	unless ($noprint){ print "$name: found existing directory $beamline_dir\n"};
    }
    else
    {
	$ans =  `mkdir $beamline_dir`;
	if( $!)
	{
	    print LOG   "$name: failure creating directory \"$beamline_dir\"   : $!\n";
	    die  "$name: failure creating directory \"$beamline_dir\"  : $!\n";
	} 
    }
    
    if ($testing)
    { # only when testing
	if ($skip_file)
	{ print LOG  "$name: NOT writing information to shift file, skip_file is true (testing only)\n";
	  unless ($noprint){ print  "$name: NOT writing information to shift file, skip_file is true (testing only)\n";}
	  next;  # skip rest of loop
	}
    }
  
# Get the monitor filename
    
    $filename =  get_filename_for_shift($beamline_dir);     
    $tempfile =  $beamline_dir."tempfile.txt";
    unless($noprint){ print "filename=$filename and tempfile=$tempfile\n";}
    
    
    
#Look for an existing file for this shift
    if(-e $filename)
    {
	open FIN,"$filename" or die $!;  # open for reading
	unless($noprint){ print "$name: opening existing file $filename for reading\n";}
	print LOG "$name: opening existing file $filename for reading\n";
	
	open FOUT,"> $tempfile" or die $!;  # open tempfile for writing
	#print LOG "$name: opening temporary file $tempfile for writing\n";
	unless($noprint){ print "$name: opening temporary file $tempfile for writing\n";}
	
	
	$done=0; # clear flag 
	$linenum=1;
	unless($noprint){print "Looking for beamline=$beamline exp_num=$exp_num sample=$sample\n";}
	while (<FIN>)
	{ # while
	    unless($noprint)
	    { 
		print $linenum++;
		print ": $_";
	    }
	    if ( (/^#/) || ($done) )
	    {   
		print FOUT $_  ; # copy comments or rest of file 
	    }
	    else
	    { 	# Check whether this line is for current beamline, experiment number or sample 
		my @fields=split;
		unless($noprint){ print "Checking line from file: Beamline:$fields[0];Expt num:$fields[1]; Sample: $fields[2]; Minutes: $fields[3]; \n";}
		my $my_beamline=$fields[0];
		my $my_exp_num=$fields[1]+0; # convert to a number
		my $my_sample =$fields[2];
       
		unless($noprint){print "my_beamline=$my_beamline my_exp_num=$my_exp_num sample=$my_sample\n";}
		if(($my_beamline eq $beamline) &&  ($my_exp_num == $exp_num) && ($my_sample eq $sample))
		{
		    $time_min = $fields[3] + 1; # time in minutes; convert to  number, and increment
		    unless($noprint)
		    {
			my $l=$linenum-1; # linenum already incremented. Use $l in message.
			print "$name: updating line $l in file for beamline $beamline, experiment number $exp_num, sample $sample\n";
		    }
		    print LOG "$name: updating line $linenum in file for experiment number $exp_num and sample $sample\n";
		    write_line($beamline,$exp_num,$sample,$time_min,$datestring); # change line and write to file
		    $done=1; # done; copy rest of file unchanged
		}
		else
		{ 
		    print FOUT $_ ; # copy this line unchanged 
		}
	    }
	} # end of while 
	
	# Have we found (and dealt with) this exp_num/sample?
	unless($done)
	{ 
	    unless ($noprint){ print "Calling add_new_exp with beamline=$beamline, exp_num=$exp_num, sample=$sample to write a new line to the file\n";}
	    add_new_exp($beamline,$exp_num, $sample, $datestring); # add new experiment line(s)	# write a new line
	}
	close FIN;
	close FOUT;
    
    
	`rm $filename`; # remove the old file
	if( $!)
	{
	    print LOG   "$name: failure deleting old file $filename   : $!\n";
	    die  "$name: failure deleting old file $filename  : $!\n";
	} 
	
	`mv $tempfile $filename`; # replace with the temporary file
	if( $!)
	{
	    print LOG   "$name: failure renaming $tempfile to $filename   : $!\n";
	    die  "$name: failure renaming $tempfile to $filename : $!\n";
	} 
	
	unless($noprint){ print "$name:  renamed $tempfile to $filename\n"; }
	print LOG "$name: renamed $tempfile to $filename\n";
	
    } # end of existing file
    else
    { # new file
	open FOUT,"> $filename" or die $!;  # open for reading
	print LOG "$name: opening new file $filename\n";
	unless($noprint){ print "$name: opening new file $filename\n";}
	
	print FOUT "# MUSR Shift Monitor File first opened at $datestring\n";
	print FOUT "# Beamline  ExptNum  Sample            Time(minutes) 1A_Threshold(uA) Time line written         Testing\n";
	add_new_exp($beamline,$exp_num, $sample, $datestring); # add new experiment line(s)
	
	unless($noprint){ print  "$name: successfully written new monitor file $filename\n";}
	print LOG "$name: successfully written new monitor file $filename\n";
	close FOUT;
    }
} # for loop on beamlines
	     
# File format
# Beamline   ExptNum  Sample  Thresh(uA)  Minutes  Date

exit;

sub get_filename_for_shift($)
{
    # input parameter: $beamline
    # returns:         $filename
    my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
    my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
    my $filename;
    my ($ans,$now);
    my $name="get_filename_for_shift";

    my ($beamline_dir) = @_;

#
# Test program trytime.pl tests this routine
#
    

    $now=time();  # get present unix time 
    my  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($now); # get present localtime
    $year+=1900; # year since 1900

    # Determine if this is AM or PM shift  
    # AM shift starts at 8.30am finishes at 8.29pm the same day
    # PM shift starts at 8.30pm finishes at 8.29am the next day


    $hour +=0; # convert to number
    $min  +=0; # convert to number
    my $mtime="AM";  # default

    # Before 8.30am is the PM shift previous day
    if ( ($hour < 8) or ( ($hour == 8) && ($min < 30)))
    {  # PM shift previous day
	$mtime = "PM";
	my $yesterday = $now - 3600*24; # get date 24 hours ago (i.e. yesterday)
	
	my ($sec2,$min2,$hour2,$mday2,$mon2,$year2,$wday2,$yday2,$isdst2) = localtime($yesterday);
	$year2+=1900; # year since 1900
	

	#print "mday=$mday and mday2=$mday2\n";
	$filename= $beamline_dir.$mday2.$months[$mon2].$year2."_".$mtime.".txt";
	unless($noprint) { print "get_filename_for_shift: $mtime shift previous day;  filename is $filename\n";}
	print LOG "get_filename_for_shift: $mtime shift previous day;  filename is $filename\n";
	return $filename;
    }

    if ($hour > 20) { $mtime="PM";}  # PM same day
    if (($hour == 20) && ($min >= 30)) { $mtime="PM"; } # PM same day 


# assemble filename(s) for this shift
   
    $filename= $beamline_dir.$mday.$months[$mon].$year."_".$mtime.".txt";
    
    unless($noprint) { print "get_filename_for_shift:  $mtime shift same day;  Filename is $filename\n";}
    print LOG "get_filename_for_shift:  $mtime shift same day;   filename is $filename\n";
  
    return $filename;
}


sub add_year($)
{
    # parameter $year
    # adds $year to $MONITOR_PATH and returns full path ( $MONITOR_DIR )

    # make sure there is a directory for this year
    my ($year) = @_;
    my $dir = $MONITOR_PATH.$year."/";
    my $ans;

    if (-d $dir) 
    { 
	unless ($noprint){ print "found existing directory $dir\n"; }
	return $dir;
    }
    else
    {
	`mkdir $dir`;
	if( $!)
	{
	    print LOG   "add_year : failure creating new directory \"$dir\" for year $year   : $!\n";
	    die  "add_year :  failure creating new directory \"$dir\" for year $year  : $!\n";
	}
    }
    return $dir;
    
}

sub read_epics_value($)
{
    my ($epics_path) = @_; # get the parameters
    my ($cmd,$val,$result);
    $cmd = sprintf("caget %s", $epics_path);

    print LOG "read_epics_value: sending command: $cmd\n";
    unless($noprint){ print "read_epics_value: sending command: $cmd\n";}
    $val=`$cmd 2>&1`;  # redirect stderr to stdin

    unless ($noprint){ print "read_epics_value:  status=$? (0=success) and result=$val";}
    print LOG "read_epics_value:   status=$? (0=success) and result=$val ";
    unless( $?)
    {
	chomp $val;
	unless ($val =~/\S/)
	{ 
	    print "read_epics_value: command: $cmd\n     reply:$val\n";  ## crontab should send an email
            print "read_epics_value: error - reply is empty\n"; 
	    print LOG  "read_epics_value: returning error - reply is empty\n"; 
	    return ("err");
	}
	unless ($val =~/^$epics_path/)
	{ 
	    print "read_epics_value: command: $cmd\n     reply:$val\n"; ## crontab should send an email
	    print "read_epics_value: reply does not start with string $epics_path\n";
            print LOG  "read_epics_value: returning error, reply does not start with string $epics_path\n";
	    return ("err");
	}
        $result=$val; # remember $val for possible error message

	$val =~ s/$epics_path//; #remove epics path
#	print "read_epics_value: $val\n";


	$val =~ s/$epics_path//; #remove epics path
        $val =~ s/ *$//; #remove trailing spaces
        $val =~ s/^ *//; # and leading spaces

	my $temp = $val;
	$temp =~ s/\.//;  # temporarily remove decimal point (if present) to check for numerical reply
	if($temp =~ /\D/)
	{
	    print LOG "read_epics_value: error - expect numerical response from read_epics_value($EPICS_PROTON_CURRENT). Found a non-digit\n";
	    print "read_epics_value: expect numerical response from read_epics_value($EPICS_PROTON_CURRENT). Found a non-digit\n"; 
	    print "read_epics_value: command was: $cmd\n     reply:$result\n";  ## crontab should send an email
	    return  ("err");
	}   

	unless($noprint){ print  "read_epics_value: read $epics_path   returning \"$val\"\n";}
	print LOG "read_epics_value: read $epics_path   returning \"$val\"\n";
	return $val;
    }
    else
    {
	unless($noprint){ print  "read_epics_value: Failure after cmd:$cmd; status=$?\n";}
	print LOG  "read_epics_value: Failure after cmd:$cmd; status=$?\n";

        print "Musr_monitor cannot read EPICS value $epics_path; reply=$val ";  ## crontab should send an email
    }
    return ("err");
}

# send_ssh_cmd replaced by read_odb_data except when mserver is not running
sub read_odb_data($)
{
    my ($beamline) = @_;  # input parameter
    my ($expt_num, $sample); # return parameters
    my $name="read_odb_data";

  # read_odb_data  will do on appropriated midm* host (including localhost):  
    #    odb -h midm15:7071 -e m15  -c 'ls /Experiment/Edit on start/experiment number'
    #    odb -h midm15:7071 -e m15  -c 'ls /Experiment/Edit on start/sample'

    my $path="/Experiment/Edit on start/"; # subdir of required params
    my $sample_path=$path."sample";
    my $expt_num_path=$path."experiment number";
    my $port="7071"; # mserver port number for all musr hosts
    my $fail=1; 
    my $success=0;
    my $hostname= "mid".$beamline.":".$port; # hostname midm20c etc plus mserver port
    #print "hostname=$hostname\n";

    my $length;
#  First read the experiment number
    my $odbcmd='ls '.'"'.$expt_num_path.'"';
    #print "odbcmd=$odbcmd\n";	
    my $cmd= "odbedit -h $hostname -e $beamline  -c '$odbcmd'";
    #print "cmd=$cmd\n";

    $sample=""; # initialize
    $expt_num=0; # initialize

  unless($noprint){ print "read_odb_data: working on beamline \"$beamline\" \n";}
    print LOG "read_odb_data: working on beamline \"$beamline\" \n";

    my ($err, $val)= send_command($cmd); # checks reply
    if ($err)
    {
	print LOG "$name: error getting experiment number for beamline $beamline from odb\n";
	unless ($noprint){ print "$name: error getting experiment number for beamline $beamline from odb\n";}
        return ($fail);
    }
    
    my $pattern= '(experiment +number +)(\w+)';
    if($val=~ /$pattern/i)
    {
	#print "text matched; $1 and $2  \n";
	if($2=~/\D/)
	{ 
	    print LOG "$name: experiment number for beamline $beamline is not a number ($2)\n";
	    unless($noprint){ print "$name: experiment number for beamline $beamline is not a number ($2) \n";}
            return ($fail);
	}
	$expt_num=$2+0; # numerical  
	if($expt_num == 0)
	{
	    unless($noprint){ print  "$name: illegal response for $beamline experiment number ($expt_num)\n";}
	    print LOG "$name: illegal response for $beamline experiment number ($expt_num)\n";
	    return ($fail);
	} 
    }
    else
    {
	print LOG "$name: could not find experiment number for beamline $beamline\n";
	unless($noprint){ print  "$name: could not find experiment number for beamline $beamline \n";}
	return ($fail);
    }


# Now read the sample
    $sample="";
    $odbcmd='ls '.'"'.$sample_path.'"';
    $cmd= "odbedit -h $hostname -e $beamline  -c '$odbcmd'";
    #print "cmd=$cmd\n";
    ($err, $val)= send_command($cmd);
    if ($err)
    {	
	print LOG "$name: error getting Sample for beamline $beamline from odb\n";
	unless($noprint){ print "$name: error getting Sample for beamline $beamline from odb \n";}
        return ($err);
    }

    $pattern= '(Sample)\s+(.*)\s*$';
    if($val=~/$pattern/ig){
	#print "text matched; \"$1\" and \"$2\"  \n";
	$sample=$2;
    }
    else
    {
	print LOG "$name: could not find Sample for beamline $beamline\n";
	unless($noprint){ print  "$name: could not find Sample for beamline $beamline \n";}
	return ($fail);
    }

    unless ($sample =~/\S/) # non-blank character
    {
	unless($noprint){ print  "$name: $beamline sample has been left blank(\"$sample\")\n";}
	print LOG "$name: $beamline sample has been left blank (\"$sample\").\n";
	$sample="..blank..";

    } 
    else
    {
	$sample =~ s/\s*$//; #remove trailing spaces
	$sample =~ s/^\s*//; # and leading spaces
	$sample=~ s/\s+/_/g; # replace any other spaces with single underscores

	unless($noprint){ print "$name: now sample=$sample\n";}
	print LOG "$name:  now sample=$sample\n";
	$length = length($sample);
	if($length > 20)  # maximum length of sample to be written in the monitor file (format %20.20s)
	{ 
	    unless($noprint){print "$name: truncating $sample to 20 char \n";}
	    print LOG "$name: truncating $sample to 20 char \n";}
	
	$sample = substr($sample, 0, 20); # truncate string to 20 characters
	$length = length($sample);
    
    
	unless($noprint){print "sample=\"$sample\" (length=$length)\n";}
	print LOG "$name: sample=\"$sample\"  (length=$length)\n";
    }
    unless ($noprint){print "$name: returning success; expt number=$expt_num and sample=$sample\n";}
    return ($success, $expt_num, $sample);
}
 
    
sub send_command($)
{   # called by read_odb_data
    my ($cmd) = @_;  # input parameter
    my $name="send_command";
    my $fail=1;
    my $success=0;

    unless($noprint){ print "$name: sending command: \"$cmd\"\n";}
    print LOG "$name: sending command: \"$cmd\"\n";

    my $val=`$cmd`;
 		     
    unless( $!)
    {
        chomp $val;
	unless($noprint){print "$name: reply  val = \"$val\"\n";}
	print LOG "$name: reply val = \"$val\"\n";
	
	unless ($val =~/\S/) # non-blank character
	{ 
	    unless($noprint){ print "$name: reply is empty (command was \"$cmd\") \n";}
	    print LOG "$name: reply is empty (command was \"$cmd\") \n";
	    return ($fail);
	}
	
	if ($val =~/Cannot connect to remote host/)
	{
	    unless($noprint){ print "$name: Cannot connect to remote host; mserver may not be running \n";}
	    print LOG "$name:  Cannot connect to remote host; mserver may not be running  \n";
	    return ($fail);
	}

	if ($val =~/not found/)
	{
	    unless($noprint){ print "$name: reply \"$val\" contains \"not found\" \n";}
	    print LOG "$name: illegal reply $val=\"$val\" \n";
	    return ($fail);
	}
    
	return ($success,$val);
    }
}



# send_ssh_cmd replaced by read_odb_data
sub send_ssh_cmd($)
{
    my ($beamline) = @_;  # input parameter
    my ($err, $expt_num, $sample); # return parameters

    # check_exptnum script will do on appropriated midm* host:  
    #    odb -c ls /Experiment/Edit on start/experiment number
    #    odb -c ls /Experiment/Edit on start/sample
    # a key is supplied to access the beamline a/c
    
    my $hostname= "mid".$beamline;
    my $cmd= "ssh $beamline\@$hostname  /home/musrdaq/musr/musr_midas/bin/get_expt_info";
    my $num;
    my $le;
    $err=1;

    unless($noprint){ print "send_ssh_cmd: sending command: \"$cmd\"\n";}
    print LOG "send_ssh_cmd: sending command: \"$cmd\"\n";
    my $val=`$cmd`;
    unless( $!)
    {
	
	unless($noprint){print "val=$val\n";}
        print LOG "send_ssh_cmd: reply was val=$val\n";
    
	unless ($val =~/\S/) # non-blank character
	{ 
	    unless($noprint){ print "send_ssh_cmd: reply is empty (beamline=$beamline) \n";}
	    print LOG "send_ssh_cmd: reply is empty (beamline=$beamline) \n";
	    return ($err);
	}

	if ($val =~/not found/)
	{
	    unless($noprint){ print "send_ssh_cmd: reply \"$val\" contains \"not found\" (beamline=$beamline) \n";}
	    print LOG "send_ssh_cmd: illegal reply (beamline=$beamline) $val=\"$val\" \n";
	    return ($err);
	}

	my @fields = split /\s+/, $val;
	$le =$#fields; #last element
	unless($noprint){ print "fields: @fields  length $le\n"; }
	
	if( ($fields[0] =~/experiment/i) &&  ($fields[1] =~/number/i))
	{
	    $fields[2] =~ s/ *$//; #remove trailing spaces
	    $fields[2] =~ s/^ *//; # and leading spaces
	 
	    if($fields[2] =~ /\D/g)
	    {
		unless($noprint){ print "$name: expect numerical response for experiment number (\"$string\"). Found a non-digit after split (\"$fields[2]\")\n";} 
		print LOG "$name: expect numerical response for experiment number. Found a non-digit after split (\"$fields[2]\")\n"; 
		return ($err);
	    }
	    $num=$fields[2]+0; # numerical
	    #print "num=$num\n";
	    if($num == 0) # error from get_digits
	    {
		unless($noprint){ print  "send_ssh_cmd: illegal response for $beamline experiment number ($num)\n";}
		print LOG "send_ssh_cmd: illegal response for $beamline experiment number ($num)\n";
		return ($err);
	    } 
	}
	else
	{
	    unless($noprint)
	    { print  "send_ssh_cmd: illegal reply \"$val\". Could not find string \"experiment number\"  (beamline=$beamline) \n"; }
	    print  LOG "send_ssh_cmd: illegal reply \"$val\". Could not find string \"experiment number\"  (beamline=$beamline)\n";
	    return ($err);
	}

	if ($fields[3] =~ /sample/i)
	{
	    unless($noprint){print "sample i.e.  fields[4...$le]= @fields[4..$le]\n";}
            my @tmp= @fields[4..$le]; # there may be spaces within the string
       	    print LOG "send_ssh_cmd: tmp  = @tmp\n";
	    
	    $sample = join " ", @tmp; # first join with a blank to test for non-blank character
	    unless ($sample =~/\S/) # non-blank character
	    {
		unless($noprint){ print  "send_ssh_cmd: $beamline sample has been left blank(\"$sample\")\n";}
		print LOG "send_ssh_cmd: $beamline sample has been left blank (\"$sample\").\n";
		$sample="..blank..";
	    } 
	    $sample =~ s/ *$//; #remove trailing spaces
	    $sample =~ s/^ *//; # and leading spaces
            $sample=~ s/ /_/g;   # replace any others spaces with underscores

	    unless($noprint){ print "now sample=$sample\n";}
            print LOG "send_ssh_cmd:  now sample=$sample\n";
	    my $length = length($sample);
	    if($length > 20)  # maximum length of sample to be written in the monitor file (format %20.20s)
	    { 
		$sample = substr($sample, 0, 20); # truncate string to 20 characters
		$length = length($sample);
	    }
       
	    unless($noprint){print "after possible truncation at 20 char,  sample=\"$sample\" and  length=$length\n";}
	    print LOG "after possible truncation at 20 char,  sample=\"$sample\" and  length=$length\n";
	   
	}
	else
	{
	    unless($noprint){ print  "send_ssh_cmd: illegal reply \"$fields[3] $fields[4]\". Could not find string \"sample\"  (beamline=$beamline) \n"; }
	    print  LOG "send_ssh_cmd: illegal reply \"$fields[3] $fields[4]\". Could not find string \"sample\"  (beamline=$beamline)\n";
	    return ($err,0,"error");
	}
    }
    else
    {
	unless($noprint){ print  "send_ssh_cmd: Failure after cmd $cmd : $! (beamline=$beamline) \n";}
	print  LOG "send_ssh_cmd: Failure after cmd $cmd : $!  (beamline=$beamline)\n";
	return ($err);
    }
    return(0, $num, $sample); # success
}

   





sub write_line($$$$$)
{
    our $min_proton_current;
    our $testing;
    my $star="*";
    my $nostar="";
    my $name="write_line";
   
    # writes a line to the monitor file
    my ($beamline, $exp_num, $sample, $time_min, $datestring) = @_; # get the parameters
    unless($noprint){print "$name: starting with parameters beamline=$beamline expt num=$exp_num sample=$sample time(minutes)=$time_min and date=$datestring\n";}
      print  LOG "$name:  starting with parameters beamline=$beamline expt num=$exp_num sample=$sample time(minutes)=$time_min and date=$datestring\n";
    my $format="    %4.4s       %4.4d   %-20.20s   %4.4d      %6.3f          %s    %s\n";
   #                beamline    expnum  sample  time(minutes) thresh        date  testing(*)    
    unless($testing) 
    {
	printf FOUT ( $format, $beamline, $exp_num, $sample, $time_min, $min_proton_current,  $datestring, $nostar);
	unless($noprint){ 
            print "$name: writing line to file...\n";
	    printf  ( $format, $beamline, $exp_num, $sample, $time_min, $min_proton_current, $datestring,  $nostar);
	}
    }
    else
    {
	printf FOUT ( $format, $beamline, $exp_num, $sample,  $time_min, $min_proton_current, $datestring, $star);
	unless($noprint){ 
	    print "$name: writing line to file...\n";
	    printf  ( $format, $beamline, $exp_num, $sample,  $time_min, $min_proton_current, $datestring,  $nostar);
	}
    }
  
    return;
}

sub add_new_exp($$$$)
{   # add new experiment to the monitor file, i.e. add new line(s)
    
    my ($beamline,$exp_num,$sample,$datestring) = @_;
    my $name="add_new_exp";
    unless($noprint){ print "$name: starting with beamline $beamline, experiment number $exp_num sample $sample datestring $datestring\n";}
 print LOG "$name: starting with beamline $beamline, experiment number $exp_num sample $sample and datestring $datestring\n";
  
    if($exp_num > 0 && $exp_num < 9999)
    {  # add 1 minute for this experiment
	write_line($beamline,$exp_num,$sample,1,$datestring);
    }
	  
    return;
}

sub get_digits($)
{
    my $name="get_digits";
    my ($val)=@_;
    our $noprint;
    my @fields;
    my $string = $val; # original string

    if($val =~ /\D/) # any non-digit?
    {
	#print "$name: expect numerical response for experiment number ($val). Found a non-digit\n";
	# an ODB message may be appended to the response; appear to start with a <CR>
	
	@fields = split /\s+/, $val;
	$val=$fields[0];

	#print "after split, first element is \"$val\"\n";
	unless ($val =~/\S/) # non-blank character
	{ 
	    unless($noprint){ print "$name: expect numerical response for experiment number ($string). Found a non-digit\n"; }
	    print LOG "$name: expect numerical response for experiment number ($string). Found a non-digit\n";
	    return 0;
	}
  
	if($val =~ /\D/)
	{
	    unless($noprint){ print "$name: expect numerical response for experiment number (\"$string\"). Found a non-digit after split (\"$val\")\n";} 
	    print LOG "$name: expect numerical response for experiment number (\"$string\"). Found a non-digit after split (\"$val\")\n"; 
	    return 0;
	}
	
    }
    return $val;  
}
