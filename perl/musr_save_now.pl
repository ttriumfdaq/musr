#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d -w warnings
###  !/usr/bin/perl -d
#
#    MUSR only - save the histograms now
#
#   Normally invoked from "Save" button
# 
# invoke this script with cmd
#                   include_path                    experiment   beamline
# musr_save_now.pl  /home/musrdaq/musr/perl             m20c      m20c
#
# Writes to "save now" in mdarc's musr area in odb.
# "save now" is hot linked in mdarc.c so that the histograms will be
# saved immediately. It doesn't matter what is written to "save now".
#
# $Log: musr_save_now.pl,v $
# Revision 1.7  2015/03/26 00:17:10  suz
# change message to reflect VMIC New Beamlines
#
# Revision 1.6  2004/06/10 18:12:38  suz
# require init_check; use our; last Rev message is bad
#
# Revision 1.5  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.4  2004/04/21 19:10:29  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.3  2004/04/08 17:46:23  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.2  2004/02/10 22:32:06  suz
# add test on bnqr
#
# Revision 1.1  2002/04/14 03:48:17  suz
# original: causes mdarc to save histos now
#

use strict;


##################### G L O B A L S ####################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#########################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir, $expt, $beamline ) = @ARGV;  # input parameters
our $len =  $#ARGV; # array length
our $name = "musr_save_now";  # same as filename
our $nparam=3;
our $outfile = "musr_save_now.txt";
our $parameter_msg = "include_path; experiment; beamline\n";
############################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($mdarc_path) ;
my $debug=$FALSE;
my $eqp_name;
my $value = $TRUE;
#
$|=1; # flush output buffers


# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

# Output will be sent to file $outfile
# because this is for use with the browser and STDOUT and STDERR get set to null

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; beamline = $beamline\n";
#

# write to (hot-linked) save now
#
#      determine equipment name from beamline
#
if( $beamline =~  /bn[qm]r/i  )
  {
    # Not supported for BNMR/BNQR experiments
    odb_cmd ( "msg","$MINFO","","$name", "INFO: No support for BNMR/BNQR experiments" ) ;
    print FOUT "$name: no support for BNMR/BNQR experiment";
    die "$name: no support for BNMR/BNQR experiment";
  }

# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
$mdarc_path = "/Equipment/$eqp_name/mdarc/histograms/musr/";

print FOUT "mdarc_path = $mdarc_path\n";

($status) = odb_cmd ( "set","$mdarc_path","save now" ,"$value") ;
unless($status)
  { 
    print FOUT "$name: Failure from odb_cmd (set); Error updating $mdarc_path save now.\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE updating $mdarc_path save now flag" ) ;
    die "$name: Failure updating $mdarc_path save now";
  }
print FOUT "INFO: Success - save now flag has been touched \n";
odb_cmd ( "msg","$MINFO","","$name", "INFO: save now flag has been touched " );
print "Success - Save now flag has been touched \n";

exit;

