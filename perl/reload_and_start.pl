#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from "reload and start" button (Autorun) 
# 
# invoke this script with cmd
#                     include                    experiment      beamline 
#                     path                                             
# reload_and_start.pl /home/bnmr/online/mdarc/perl    bnmr           bnmr    
# 
#
# Sets flag in odb /autorun/state to 9
#   
#
# $Log: reload_and_start.pl,v $
# Revision 1.1  2008/04/19 05:05:31  suz
# original:custom script for autorun button
#
#
use strict;
######### G L O B A L S ##################

our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir, $expt, $beamline ) = @ARGV;
our $name = "reload_and_start"; # same as filename
our $len =  $#ARGV; # array length
our $nparam=3;
our $outfile = "reload_and_start.txt";
our $parameter_msg = "include_path, experiment; beamline\n";
#########################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($autorun_path, $eqp_name);
my $debug=$FALSE;
#########################################################

$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; beamline = $beamline\n";
#


unless ($beamline)
{
    print FOUT "FAILURE: beamline not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: beamline not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure: beamline not supplied \n"; } 
        die  "FAILURE:  beamline  not supplied \n";
}
#
#      determine equipment name from beamline
#
if( ($beamline =~ /bn[qm]r/i)  )
{
# BNMR/BNQR experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
    $autorun_path = "/autorun/";
}
else
{
# Not used by MUSR experiments
    print FOUT "$name: this routine for BNMR/BNQR only\n";
    odb_cmd ( "msg","$MINFO","","$name", " this routine for BNMR/BNQR only\n");
    die "$name: this routine for BNMR/BNQR only\n";  
} 


   
($status) = odb_cmd ( "set","$autorun_path","state" ,"9") ;
unless($status)
{ 
    print FOUT "$name: Failure from odb_cmd (set); Error setting autorun state flag.\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting state flag" ) ;
    die "$name: Failure setting autorun state flag";
}

print FOUT "INFO: Success - Autorun state flag has been set to 9 (reload)\n";
odb_cmd ( "msg","$MINFO","","$name", "INFO: Autorun state flag has been set to 9 (reload)" );
print "Success - Autorun state flag has been set to 9 (reload) \n";

exit;




