# Group of subroutines required by kill_script.pl & used by bnmr_darc.c
#
# Find the next real or test run number
#
# $Log: run_number_check.pl,v $
# Revision 1.19  2015/05/01 23:54:55  suz
# add beamlines M20C,M20D,M9A
#
# Revision 1.18  2005/06/10 17:47:13  suz
# minimum run numbers are incremented; add temp fix to allow old minimum values in data area until next year, when new minima will take effect
#
# Revision 1.17  2004/06/10 17:37:59  suz
# file list no longer written to outfile (except on debug); our; Rev1.16 message is bad
#
# Revision 1.16  2004/06/09 21:36:23  suz
# add check on beamline

# Revision 1.15  2004/03/30 22:53:35  suz
# add timing info using write_time
#
# Revision 1.14  2003/12/01 23:15:13  suz
# allow latest file to be of a lower run number
#
# Revision 1.13  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.12  2003/01/08 18:34:58  suz
# add bnqr
#
# Revision 1.11  2002/08/23 07:28:08  asnd
# Fix restriction to 1000 runs previously imposed by glob pattern.
#
# Revision 1.10  2002/07/10 18:58:25  suz
# improve message if highest rn is not the latest
#
# Revision 1.9  2001/12/07 22:06:45  suz
# give users a clearer message (message now > 1 line)
#
# Revision 1.8  2001/12/01 02:45:34  suz
# add a check for run number/date ordering
#
# Revision 1.7  2001/11/22 20:55:21  suz
# add check for  Run number=30499 & mlogger opens 030500.mid
#
# Revision 1.6  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.5  2001/09/14 19:26:58  suz
# add MUSR support; use strict; imsg now msg
#
# Revision 1.4  2001/04/30 19:58:49  suz
# Add support for midas logger
#
# Revision 1.3  2001/03/29 19:39:27  suz
# add support for imusr files, i.e. .mid file extension  a
#
# Revision 1.2  2001/03/01 18:55:58  suz
# Fix bug in number of saved files found
#
# Revision 1.1  2001/02/23 17:58:09  suz
# initial version
#
#
use strict;
# globals
our ( $TRUE, $FALSE, $FAILURE,  $SUCCESS) ;
our($DIE, $CONT);
# Globals defined in get_next_run_number.pl or archive.pl :
our($MAX_TEST, $MIN_TEST, $MAX_BNMR, $MIN_BNMR);
#other beamlines
our ($MIN_M20, $MAX_M20, $MIN_M15, $MAX_M15, $MIN_M9B, $MAX_M9B, $MIN_DEV, $MAX_DEV) ;
our ($MIN_M20D, $MAX_M20D, $MIN_M20C, $MAX_M20C, $MIN_M9A, $MAX_M9A );
our ($FOUND_HOLE, $CHECK_FOR_HOLES, $LIST_OF_HOLES); 
our ($MAX_BNQR, $MIN_BNQR);

sub run_number_check           
{
# input parameters:
#   $saved_dir  saved directory (string)
#   $run_type       test or real (string)
#   $check_for_holes  Set to 0 or 1 to check/not check for holes
#   $beamline   needed for run number ranges
#
# e.g.  run_number_check.pl /home/bnmr/dlog test 0 bnmr
#
# outputs:
#   $status        1 for SUCCESS, 0 for FAILURE
#   $next_run   if status is SUCCESS,   next run number (real or test depending on $type )
#               if status is FAILURE,      next_run_number = -1  (no message)
#               if status is FAILURE,      next_run_number = -2  (message in $message)

    my ($saved_dir, $run_type, $check_for_holes, $beamline)    = @_ ; # get the parameters

    my ($max_run, $min_run, $max_real, $min_real);
    my ($pattern, $mille_run, $message, $status);
    my $name = "run_number_check";
    my @files;
    my ($last_run);
    my ($len,$next_run_number,$nextname,$age,$most_recent_run);
    

    print FOUT " === $name ===\n";
    print FOUT "        starting with input parameters:\n";
    print FOUT "    Saved dir = $saved_dir; type = $run_type; \n";
    print FOUT "    Check for holes = $check_for_holes; beamline = $beamline\n";
    
    unless ($run_type) 
    {
        print FOUT "$name: FAILURE - Type is not defined. Supply REAL or TEST\n";
        return ($FAILURE, -1);
    }
    $CHECK_FOR_HOLES=0;
    if ($check_for_holes) { $CHECK_FOR_HOLES = 1; }
    
    unless ( $saved_dir )
    {
        print FOUT "$name: FAILURE - Empty string supplied for value of saved_dir\n";
        return ($FAILURE, -1);
    }   
    unless ( chdir ("$saved_dir"))
    {
        print FOUT "$name: FAILURE - cannot change directory to $saved_dir\n";
        return ($FAILURE, -1);
    }
    
# get the maximum and minimum run numbers for this beamline
    unless ($beamline) 
    {
        print FOUT "$name: FAILURE - no beamline has been supplied\n";
        return ($FAILURE, -1);
    }
        
    
    if  ( $beamline =~  (/bnmr/i) )
    {
        $max_real  = $MAX_BNMR + 0 ;      # integer
        $min_real  = $MIN_BNMR + 0 ;
        print FOUT "beamline is bnmr, max real run = $max_real, min_run = $min_real\n";
    }

    elsif (  $beamline =~  (/m20c/i) )
    {
        $max_real  = $MAX_M20C + 0 ;      # integer
        $min_real = $MIN_M20C + 0 ;            
    }

    elsif (  $beamline =~  (/m20d/i) )
    {
        $max_real  = $MAX_M20D + 0 ;      # integer
        $min_real = $MIN_M20D + 0 ;            
    }
    elsif (  $beamline =~  (/m20$/i) )
    {
        $max_real  = $MAX_M20 + 0 ;      # integer
        $min_real = $MIN_M20 + 0 ;            
    }
    elsif (  $beamline =~  (/m15/i) )
    {
        $max_real  = $MAX_M15 + 0 ;      # integer
        $min_real  = $MIN_M15 + 0 ;            
    }
    elsif (  $beamline =~  (/m9a/i) )
    {
        $max_real  = $MAX_M9A + 0 ;      # integer
        $min_real  = $MIN_M9A + 0 ;            
    }
    elsif (  $beamline =~  (/m9b/i) )
    {
        $max_real  = $MAX_M9B + 0 ;      # integer
        $min_real  = $MIN_M9B + 0 ;            
    }
    elsif (  $beamline =~  (/dev/i) )
    {
        $max_real  = $MAX_DEV + 0 ;      # integer
        $min_real = $MIN_DEV + 0 ;            
    }
    elsif (  $beamline =~  (/bnqr/i) )
    {
        $max_real  = $MAX_BNQR + 0 ;      # integer
        $min_real = $MIN_BNQR + 0 ;            
    }
    else 
    { 
        print FOUT "$name: beamline $beamline is not supported\n";
        return ($FAILURE, -1);
    }
    print FOUT "beamline is $beamline, max real run no = $max_real, min real run no = $min_real\n";

    if ( $run_type =~ /test/i)
    {      # for test  (ignores $beamline)
        $max_run = $MAX_TEST + 0 ; # integer
        $min_run = $MIN_TEST + 0 ; # integer        
    }
    elsif (  $run_type =~ /real/i)
    {     # for real    
        $max_run = $max_real ; # integer
        $min_run = $min_real ; # integer  
    }
    else
    {
        print FOUT "$name: FAILURE - Invalid run type $run_type supplied\n";
        return ($FAILURE, -1);
    }

    if ($max_run <= 0)
    {
        print FOUT "$name: FAILURE - Invalid value for maximum run number ($max_run)\n";
        return ($FAILURE, -1);
    }

## write the time to the file
    print FOUT "$name: CONT=$CONT, DIE=$DIE\n";
    write_time($name,$CONT);
    print FOUT "$name: calling validate_files\n";

# as a precaution (!) check for run files that are not of the correct format or are
# out of range for this beamline.
    ($status, $message) = validate_files($saved_dir, $beamline, $max_real, $min_real ); 
    write_time($name,$CONT);
    print FOUT "$name: returned from validate_files\n";

    unless ($status) 
    {
	print FOUT "$name: error returned by validate_files\n";
	return ($FAILURE, -2, $message);
    }
    

#   Search for range of run numbers having the first two digits valid for
#   real runs on this beamline (00 for m20 and m15, 01 for M9 and M13, 04 for bnmr)

    $mille_run = int( $min_run / 10000 ) ;
    $pattern = sprintf ("%02d*.msr* %02d*.mid",$mille_run,$mille_run);

    print FOUT  "$name:  search pattern for glob = $pattern\n";
    
    @files = glob($pattern); 
    $len = $#files; # array length  ( Note: no. of files found = len + 1 )
    #print FOUT "$name: INFO - no. of saved files found len = $len\n";

    if($len < 0)     
    {  
        print FOUT "$name: INFO - no saved files were found for $run_type runs in $saved_dir\n"; 
        return ($SUCCESS, $min_run); # return minumum run number
    }
    
    else 
    {
	write_time($name,$CONT);
	print FOUT "$name: calling check_files\n";

        # check files returns the highest run number and the most recent run number
        ($last_run, $most_recent_run )  = check_files($min_run, @files);

	write_time($name,$CONT);
	print FOUT "$name: returned from check_files\n";

    }
    $next_run_number = $last_run + 1 ;  # default case

#    TEST RUNS ONLY
    if ( $run_type =~ /test/i) 
    {
        if ($last_run  != $most_recent_run) 
        {         # highest run number was not of the most recent date
            print FOUT " INFO - Highest run number ($last_run) is not the most recent ($most_recent_run)\n";
	    # are we recycling run numbers or has someone done manual conversions/copies ?
	    if ($next_run_number > $max_run )
	    {
		$next_run_number = $most_recent_run + 1 ; # test runs are being recycled
	    } 
	    else
	    {
		print FOUT "INFO - assume human interferance.... ignoring most recent run & using run no. $next_run_number\n";
	    }
	}
        if ($next_run_number > $max_run )
        {
            print FOUT "$name: INFO - run numbers have reached the limit ($max_run) for $run_type runs\n";
            $next_run_number = $min_run;  # recycle run number for test runs only
        }
#      Delete any existing old TEST run files and .odb files for $next_run_number
        while ($nextname = <*$next_run_number.*>) 
        {
            if (( $nextname =~ /msr/i) ||  ( $nextname =~ /mid/i))  # for .msr* or .mid files check date
            { 
         
                unless (-l $nextname)  # check for symlink - ($age would be 0)
                {
                    $age = -M $nextname; 
                    print FOUT  "$name: About to delete old TEST saved file for next run number ( $nextname)  age $age days \n";
                    if ($age < 3) 
                    {
                        print FOUT "$name: Old test file $nextname is less than 3 days old. This is not reasonable.\n";
			print FOUT "   You may need to touch the file $last_run if a mid/mud conversion has been done by hand since last run\n";
#                       Test Run numbers won't have recycled in that time - most likely someone has redone some mid->msr conversion
#                       by hand - get the user to touch the file to be on the safe side
			$message = "ERROR - Cannot delete old file $nextname as it is unexpectedly recent;+Please check dates of saved files - you may need to touch the last valid run file";
                        return ($FAILURE, -2, $message);   # don't delete the files
                    }
                }
                else {  print FOUT  "$name: About to delete old TEST saved file (symlink) for next run number ( $nextname)\n"; }
            }
            else  {  print FOUT  "$name: About to delete old saved odb file for next run number ( $nextname)\n"; }

            unless (  unlink ($nextname))
            {
                print FOUT "$name: failure deleting old  saved file  $nextname\n";
                 return ($FAILURE, -1);
            }
            else { print FOUT "$name: Successfully deleted saved  file $nextname\n"; }
            
        }
    }

#   REAL RUNS
    else
    {
        if ($last_run  != $most_recent_run)      
        {    # highest run number was not of the most recent date
            print FOUT "$name: WARNING - Highest run number ($last_run) is not the most recent ($most_recent_run) for $run_type runs \n";
	    print FOUT "                 Assuming human intervention  & continuing using highest run number ";
            $message = "WARNING - File with highest run no. ($last_run) is not the most recent ($most_recent_run);assuming human intervention";
        }
        if ($next_run_number >= $max_run )
        {
            print FOUT "$name: FAILURE - run numbers have reached the maximum  ($max_run) for $beamline runs\n";
            $message = "FAILURE - run numbers have reached the maximum  ($max_run) for $beamline runs";
            return ($FAILURE, -2, $message); # return the maximum run number
         }
    }
    print FOUT "$name: INFO - Next run number for $run_type runs will be  $next_run_number\n"; 
    return ($SUCCESS, $next_run_number) ;
}

#############################################################################

    sub check_files( $$ )

##############################################################################
{
#  Check run files for any holes
#  Inputs
#     min run       minimum run (needed for check for holes)
#     @files        sorted array of run files
#     
### Note: if > 1 parameter,  array comes last in parameter list otherwise have 
#   to pass by reference 
###
#  
#  Outputs
#     $last_run             value of highest run number found among the saved files
#     $most_recent_run      value of most recent run number found among the saved files


    my  ($min_run, @files ) = @_ ; # get the parameter(s)
    my (@sorted_files,  $temp);
    my ($len, $first_run, $last_run);
    my $name = "check_files";
    my ($most_recent_run);
    my $debug=0;

    if ($debug){ print FOUT  " $name starting with min run=$min_run &  Array :  @files \n"; }
   
    
# Find first and last run numbers
    
    @sorted_files = sort {$a <=> $b } @files;
    if($debug)
    {
        print FOUT  "\n$name:  sorted files:\n";
        print FOUT  "@sorted_files\n";
    }
    $len = $#sorted_files; # array length   ( Note: no. of files found = len + 1 )
    
    
    $_ =  $sorted_files[$len];
    ($temp) = split /\./;
    $last_run = $temp+0 ; # make sure it's integer not string

    $_ =  $sorted_files[0];
    ($temp) = split /\./;
    $first_run = $temp +0 ; # make sure it's integer not string
    print FOUT  "$name: first & last saved runs are $first_run & $last_run\n";

    print FOUT "$name: CHECK_FOR_HOLES = $CHECK_FOR_HOLES\n";
    ($most_recent_run) = check_date (@sorted_files);
    if ($CHECK_FOR_HOLES)  
    {
        print FOUT "$name: calling check_for_holes\n";
        if ($first_run = $last_run ) { $first_run = $min_run;} # if only one run make sure there is no gap
        check_for_holes( $first_run, $last_run, @sorted_files );
    }
    if ($debug) 
     { print FOUT "$name: returning last run = $last_run, most recent run = $most_recent_run\n"; }
    return ($last_run, $most_recent_run) ;
    
}

sub check_date
{
# sorting files in array filenames  by creation date
#
# parameters:

#  input:   @filenames            list of sorted filenames
#  output:  $latest_run_number    most recent run number
#
#
    my (@filenames) = @_ ; # get the parameters

    my $name =  "check_date";
    my $debug = $FALSE;
    my $age;
    my $least_name = "no name"; # initialize filename
    my $least_age =  10000; # initialize age in days to a huge number
    my ($diff, $diffm);
    my @temp;
    my $rn;
# check for latest creation date
    print FOUT  "$name: Checking for most recent creation date\n";
    foreach (@filenames)
    {
        if (-l)
        {
            if ($debug) { print FOUT  "$name: file $_ is a symlink  - no creation information so skip it\n";}
            next;
        }
        $age = -M;
        if ($debug) { print FOUT  "$name: file $_:  modification age in days: $age\n"; }
        if ($least_age >= $age)  # if two have same creation date (possible if they were copied across)  
        {                        # since files are sorted will get the one with the largest run number
            if ($debug) { print FOUT "$name: INFO:found file $_ of age $age is younger than $least_name of age $least_age \n";}
	    
            $least_name = $_; 
            $least_age = $age;
        }
	else
	{
	    # check if there is some problem in run number /date ordering
	    # this will also catch .mid files older than .msr files but may be confused if conversion redone later 
	    $age = $age + 0;
	    $least_age = $least_age +0;
	    $diff = $age - $least_age;
	    #print FOUT "diff=$diff $least_name: $least_age $_:  $age \n"; # 0.0014 is about 2 minutes
	    $diffm  = $diff * 1440; # minutes

	 # remove this check that msr file is older than mid file as conversion is no longer done at end-of-run   
	   # if( $least_name=~/$rn/)
	   # { 
		#print FOUT "detected $rn in $least_name\n";
		#if( /mid/ && $least_name =~ /msr/i)
		#{
		#    print FOUT "$name: mid/msr files $_ and $least_name out of order by $diffm minutes\n";
		#}
	    #}
	    if ($diff > 0.0014)
	    {
		unless (/_v/i)
		{ print FOUT "$name: WARNING - file $_ is older than $least_name by $diffm minutes\n";}		
	    }
	}
    }
    if ($debug) { print FOUT  "$name: Most recent file is $least_name, age $least_age\n"; }
    $_ = $least_name;
    ($least_name) = split /\./;
    $least_name = $least_name +0 ; # make sure it's integer not string
    print FOUT "$name: returning most recent run number = $least_name\n"; 
    return($least_name);
 }



        



sub check_for_holes
{
# check for holes is a sequence of run files
#
# parameters:

#  input:   $first_run, $last_run,  @sorted_files        first and last runs, list of sorted filenames
#
#  output: prints list of holes into FOUT, stores them in $LIST_OF_HOLES
#          sets global $FOUND_HOLE to be TRUE.
#
    my ( $first_run, $last_run,  @sorted_files) = @_ ; # get the parameter
#
    my $name = "check_for_holes";
    my ($index, $run_num, $this_file, $missing);

    my $debug = $FALSE; 

    $FOUND_HOLE = $FALSE;
    $LIST_OF_HOLES="";

    print FOUT ("$name: starting with parameters first run = $first_run, last run = $last_run\n");
    print FOUT ("       sorted files: @sorted_files\n");
# Loop through the files looking for holes
    $index = 0;
    $run_num = $first_run;
    $this_file = 0;
    while ($this_file <= $run_num )
    {
        if($run_num > $last_run) {last;} 
        #open the next file
        $_= $sorted_files[$index];
        ($this_file) = split /\./;
        $this_file = $this_file +0 ; # make sure it's integer not string
        if($debug) { print FOUT "$name: inner while loop: index=$index, this_file = $this_file, run_num = $run_num\n";}
        if ($this_file > $run_num) 
        {
            if($this_file == $run_num +1)
            {
                print FOUT "$name: missing saved file for $run_num\n";
		$LIST_OF_HOLES="check informational output file for list of missing run files"; # multi-line output
                $run_num++;     # incr. run_num
		$FOUND_HOLE=$TRUE;
            }
            else
            {
                    $missing = $this_file-1;
                    print FOUT "$name: missing saved files for runs $run_num to $missing\n";
		    $LIST_OF_HOLES="missing saved files for runs $run_num to $missing";
                    $run_num = $this_file;
		    $FOUND_HOLE=$TRUE;
                } 
        }
        
        elsif ($this_file == $run_num)
        {
            # got run_num
            if ($debug) { print FOUT "$name: Matched run number $run_num\n";}            
            $index++; # next file
            $run_num++;     # incr. run_num
        }
        else  # must be a duplicate file
        {
            if ($debug) { print FOUT "$name:duplicate file this_file = $this_file\n";}
            $index++; # next file
        }
    }
    return ();
}

sub validate_files ($$$$)
{   
#   input parameter: 
#     $saved_dir
#     $beamline
#     $max_real max real run for this beamline
#     $min_real min real run for this beamline
#
#   returns
#     $status 
#     $message if $status = $FAILURE
#
#   Assumes we are already in $saved_dir, and $min_run and $max_run are integers
#
#
# Note: added + in $message makes get_next_run_number send to odb  as 2 lines
 
    my $name="validate_files";
    my $message=" ";
    my (@files,$pattern,$len,$temp);
    my $debug=$FALSE;

# TEMP for 2005
# minimum values of runs have been incremented to avoid confusion when run_no is set to 1 less
# than minimum. Therefore run numbers with previous minimum will be invalid after 2005. But for
# 2005 we must allow these existing runs.
    my ($min_real, $min_test);
#    my ($saved_dir, $beamline, $max_real, $min_real )  = @_; # parameter(s)
    my ($saved_dir, $beamline, $max_real, $min_real_ )  = @_; # parameter(s)
#end of TEMP
    print FOUT "$name: Starting with saved_dir=$saved_dir & beamline = $beamline \n";
    print FOUT "$name: Checking saved files against limits for $beamline:\n";

#TEMP
#    print FOUT "  Limits: Test (max=$MAX_TEST, min=$MIN_TEST); Real (max=$max_real, min=$min_real)\n";
   print FOUT "  Limits: Test (max=$MAX_TEST, min=$MIN_TEST); Real (max=$max_real, min=$min_real_)\n";
    print FOUT "TEMP - Allowed Min values changed in 2005. After 2005 remove this temp change....\n";
    print FOUT "TEMP - Setting min values one less to accept existing 2005 files\n";
    $min_real = $min_real_ -1;
    $min_test = $MIN_TEST -1;
#end of TEMP

    $pattern = "*.mid *.msr*"; # look for any midas files or msr files
    if ($debug) { print  "$name:  search pattern for glob = $pattern\n";}
    @files = glob($pattern); 
    $len = $#files; # array length  (no. files found = len+1
    if ($debug) { print "$name: len = $len; Number of files found = len+1\n";}
    if ($len == -1) 
    {
	print FOUT "$name: no run files found\n";
	return($SUCCESS);
    }

  if ($debug) { print "$name: files : @files\n";  }
  
    foreach (@files)
    {
	($temp)= split /\./; # split off extension
	if ($debug) { print "$name: after split: $temp\n";}
	if ( $temp =~  /\D/) 
	{ 
	    print "$name: found non-digits ($temp)\n";
	    print FOUT "$name: found non-digits ($temp)\n";
	    $message = "ERROR - invalid filename $_ in $saved_dir;+** Please DELETE or RENAME $_ in $saved_dir **";
	    return ($FAILURE,$message);
	}
	else 
	{  
	     if ($debug) { print "$name: found digits only ($temp)\n"}
	 }
	if ($temp =~ /^0/ )  
	{
	    if ($debug) { print "$name: found a run file starting with 0 ($_)\n";}
	    $temp = $temp + 0 ; #make sure it's an integer
	    
#           Check run number against the limits

#           Special case -  TEST FILES 
#           test file 030500.mid will be opened before we can stop it if last run number is 030499
#           send a message
	    if ( ( $temp >= $MAX_TEST+1 && $temp < $MIN_BNMR )) # illegal test run no. 30500-39999
	    {
		print "Illegal saved file $_  > maximum test run number $MAX_TEST)\n";
		$message= "Illegal saved file $_ (max test run number = $MAX_TEST).+** Please DELETE or RENAME $_ in $saved_dir & press TEST button**\n";
		return ($FAILURE,$message);
	    }
# TEMP
#	    unless  ( ($temp >= $MIN_TEST) && ($temp <= $MAX_TEST )) 
	    unless  ( ($temp >= $min_test) && ($temp <= $MAX_TEST ))
	    {
		   print FOUT "file for run $temp is not a test file\n";
		   print FOUT "temp=$temp; min_real=$min_real; max_real=$max_real\n";
		unless ( ( $temp >= $min_real)  && ( $temp <= $max_real))
		{
		       print FOUT "file for run $temp  IS NOT A REAL run either \n";
		    print "Run number of saved file $_ is out-of-range for this beamline ($beamline)\n";
		    $message= "Run no. of saved file $_ is out of range for this beamline ($beamline).+** Please DELETE or RENAME $_ in $saved_dir & press REAL button **\n";
		    
		    return ($FAILURE,$message);
		}
	    }
	} 
	else
	{
	    print "$name: WRONG FORMAT : run file found not beginning with 0 ($_)\n";
	    print FOUT "$name: WRONG FORMAT : run file found not beginning with 0 ($_)\n";
	    $message = "ERROR - invalid filename $_ in $saved_dir. +** Please DELETE or RENAME $_ in $saved_dir **";
	    return($FAILURE,$message);
	}
    }
    print FOUT "$name: All saved run files appear to have valid filenames\n";
    return ($SUCCESS);
}


sub check_for_msr
{
# Called from set_run_number.pl at the begin of a Type 1 run to make sure there is not already an existing
# .msr file that might get overwritten (or two runs with the same run number)
#
# look for presence of pre-existing  msr file for this run number
# input parameters:
#
#   $run_number
#   $saved_dir  saved directory (string)
    
    
#  returns $status  and 0 if no existing msr file found
# 
#         for this run number
    
    
    my    $run_number = shift;
    my    $saved_dir  = shift;
    
    my  $name = "msr_check";
    
    my ($nextname,$pattern,$len);
    my @files;

#    print "run_number=$run_number\n";
    unless ($run_number)
    {
        print  "$name: invalid run number\n";
	print FOUT  "$name: invalid run number\n";
	
        return ($FAILURE,0);
    }
    unless ($saved_dir)
    {
        print "$name: invalid saved directory\n";
	print FOUT "$name: invalid saved directory\n";

        return ($FAILURE,0);
    }
    unless ( chdir ("$saved_dir"))
    {
        print  "$name: FAILURE - cannot change directory to $saved_dir\n";
        print  FOUT "$name: FAILURE - cannot change directory to $saved_dir\n";
        return ($FAILURE,0);
    }
    $pattern =  sprintf ("%06d.msr*",$run_number,$run_number);
    @files=glob($pattern);
    $len = $#files; # array length  ( Note: no. of files found = len + 1 )
    $len=$len+1;
    #print FOUT "$name: INFO - no. of saved files found len = $len\n";
    if($len < 1)     
    {  
        print FOUT "$name: INFO - no saved files $pattern were found  in $saved_dir\n"; 
        print  "$name: INFO - no saved files $pattern were found  in $saved_dir\n"; 
        return ($SUCCESS, 0); 
    }
    print FOUT "matched $len files: @files \n";
    print  "matched $len files : @files \n"; 
    print "$name: Failure: saved file(s) @files already exist\n";
    print   FOUT "$name: Failure: saved file(s) @files already exist \n";  

   return ($SUCCESS,-1);
#    $nextname = <*$run_number.msr*>;
#    if ( -e $nextname  )
#    {
#	print "$name: Failure: saved file $nextname already exists\n";
#	print   FOUT "$name: Failure: saved file $nextname already exists \n";  
#	return ($SUCCESS,-1);

#    }
#    else
#    {

#        print   "$name:  Success: saved msr file does not already exist \n";
#        print   FOUT "$name:  Success: saved msr file does not already exist \n";
#    }

#    return ($SUCCESS,0);
    
}


# I don't think this is used anywhere but may come in useful....
sub saved_file
{
# look for presence of a saved file
# input parameters:
#
#   $run_number
#   $saved_dir  saved directory (string)

#  returns $status  
#  and  $success  if there is at least one saved file
#         for this run number

    my    $run_number = shift;
    my    $saved_dir  = shift;
   
    my  $name = "saved_file";
   
    my $nextname;

    unless ($run_number)
    {
        print FOUT "$name: invalid run number\n";
        return ($FAILURE,$FAILURE);
    }
    unless ($saved_dir)
    {
        print "$name: invalid saved directory\n";
        return ($FAILURE,$FAILURE);
    }
    unless ( chdir ("$saved_dir"))
    {
        print  "$name: FAILURE - cannot change directory to $saved_dir\n";
        return ($FAILURE, $FAILURE);
    }
    while ( ($nextname = <*$run_number.msr*>) || ($nextname= <*$run_number.mid >) ) #match .msr and .mid files 
    {
        print FOUT  "found a saved  file  $nextname \n";
        return ($SUCCESS, $SUCCESS);
    }
    return ($SUCCESS, $FAILURE);
    

}
 
# this 1 is needed at the end of the file so require returns a true value   
1;   
























