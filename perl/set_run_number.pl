# subroutines for odb access
#
####################################################################################
#
# Note: these subroutines are included by the main perl script with the command:
#
#                   require "set_run_number.pl";
#
# $Log: set_run_number.pl,v $
# Revision 1.15  2004/06/10 18:22:31  suz
# use our; last Rev message is bad
#
# Revision 1.14  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.13  2003/12/01 23:12:55  suz
# improve messages
#
# Revision 1.12  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.11  2002/06/07 20:52:36  suz
# modify for fixes to midas logger in midas 1.9
#
# Revision 1.10  2001/12/01 00:26:50  suz
# improved message after failed BOR
#
# Revision 1.9  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.8  2001/11/01 17:52:22  suz
# change recover message
#
# Revision 1.7  2001/10/04 19:29:59  suz
# add cvs cmd for version to file
#
#
#       The main program is expected to define the following globals:
##            ######### G L O B A L S ##################
##            status = 0 is success for odb
##       our $ODB_SUCCESS = 0;
##       our $EXPERIMENT = "";
##       our $FAILURE = $FALSE = 0;
##       our $SUCCESS = $TRUE = 1;
##            FOUT # file handle 
##       our $ANSWER = " ";   #reply from odb command
##       our @ARRAY ;   #array contents used by get_array
##       our $COMMAND = " "; # copy of command sent be odb_cmd (for error handling)
# run states:  CONSTANTS
##       our $STATE_STOPPED = 1; # Run state is stopped
##       our $STATE_RUNNING = 3; # Run state is running
############################################################################
use strict;
# globals
our ($COMMAND,$EXPERIMENT,$ANSWER,$ODB_SUCCESS);
our ( $TRUE, $FALSE, $FAILURE,  $SUCCESS);
our ( $DEBUG, @ARRAY);
our ( $STATE_STOPPED, $STATE_RUNNING);
# for odb  msg cmd:
our ($MERROR, $MINFO, $MTALK);

sub set_rn_type2
{
#
# Set the run number for Type 2 experiment. This is VERY COMPLICATED since
# it is supposed to handle all possible cases, so users are not assumed to be sensible.
#
#  input parameters:
#
#   $rn         supplied run number (0 for dummy, else present run number )
#   $eqp_name   equipment name
#   $next_run   next run as received from run_number_check
#   $run_number current run number as read from odb
#   $saved_dir  saved directory

#   output parameters:
#   status           SUCCESS or FAILURE
#   subroutine returns 1 for SUCCESS,  0 for FAILURE

# Input parameters:
    my ( $rn, $eqp_name, $next_run,  $run_number, $saved_dir )  = @_;
    my $parameter_msg = "rn, equipment name, next run, current run number, saved dir  ";
    my $nparam=5;
    my $name="set_rn_type2";
    my ($run_state,$transition, $this_run);
    my ($status, $last_saved_run, $info,$msg_flag,$len,$debug);
#
#          Check the parameters
#
    $len = (@_);
    
    unless ($len == $nparam ) 
    {
        print FOUT "$name: Not all parameters are supplied\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Not all parameters are supplied. " ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        print "Invoke this perl script $name with the parameters:\n";
        print "   $parameter_msg\n"; 
        die      "Too few parameters supplied";
    }
    print FOUT "$name: Supplied parameters: $parameter_msg\n";
    print FOUT "        Values     : @_\n";
    if ($debug)
    {
	print  "$name: Supplied parameters: $parameter_msg\n";
	print  "        Values     : @_\n";
    }
# 
# Now reset the run number if necessary
#
# ----------------------------------------------------------------------------
# Case 1
#      rn == 0  the tr_start in mdarc is a DUMMY - mdarc is just starting up. 
# ----------------------------------------------------------------------------
#
#   We have to check if the run is in progress or not.

#  
#   Case 1.1 :   (rn == 0  && run_in_progress ) 

#     if mdarc has been running earlier in this run, there may be saved files.
#     --> check last_saved_filename parameter 
#
#        Case 1.1A:  
#              Case 1.1A.1 There are saved runs for this run, run_number should be correctly set to 
#                          ($next_run-1). 
#              Case 1.1A.2 Run_number is not correct, this is not a simple restart of mdarc. Assume old run 
#                          is abandoned, so set run_number to $next_run. 
#        Case 1.1B:  There are no saved files for this run.  set run_number to $next_run
#
# 
#   Case 1.2 :   (rn == 0  && !run_in_progress ) 
#       if (rn == 0  &&  run is not in progress ) set run_number to $next_run-1 
#
#
# ----------------------------------------------------------------------------
# Case 2
#      rn !=0
# ----------------------------------------------------------------------------
#
#  if rn != 0 then this is a genuine run start and run number has already been incremented.
#  We know the state of the run.
#
#      set run_number to $next_run
#
# -------------------------------------------------------------------------------------------
# Case 3
#        toggle bit has been set 
# -------------------------------------------------------------------------------------------
#  If we are called after the toggle bit is set 
#       1. mdarc IS running therefore current run number has been checked previously as valid
#       2. the run IS in progress 
#       3. set run_number to $next_run  - mdarc must handle deleting old saved files
#           i.e. same as case 2 above.

# Case 1 :
if ($rn == 0)    # case 1.1
{               
    print FOUT " saved dir = $saved_dir\n";
    
    print FOUT "$name :  Detected a dummy tr_start from mdarc. Mdarc is just starting up\n";
    if ($debug) { print  "$name :  Detected a dummy tr_start from mdarc. Mdarc is just starting up\n"; }
    ($run_state, $transition) = get_run_state();
    if ($run_state == $STATE_STOPPED )    # case 1.2
    {
        print FOUT "$name: run is stopped ";
        $next_run = $next_run -1 ; # next start of run will increment the run number
    }
    else      # the run is going. Check for saved files
    { 
# Case 1.1  (rn==0 run is going)
        #  Check for saved files                   
        #  routine check_last_saved_file  will return  status = $SUCCESS if
        #    1. string last_saved_filename exists 
        #    2. saved directory matches directory in string last_saved_filename
        #    3. saved file exists
        #
        #    It also returns the run number of the last saved file and $final.
        #    $final = $TRUE  for  final saved file or  $FALSE an intermediate (version).
        #
        print FOUT "$name: run is NOT stopped... checking if last saved file is for this run. \n";
        
        ($status, $last_saved_run, $info) = check_last_saved_file( $saved_dir, $eqp_name );
        
        unless ($status )   # NO info on last_saved_file available or no file    1.1B
        {
            print FOUT "$name: check_last_saved_file did not find a saved file for this run \n";
            # run number will be set to next_run below
        }
        else    # mdarc parameter  last_saved_filename points to a file present on the disk
        {       # i.e. success from check_last_saved_file
            print FOUT "$name: Success received from check_last_saved_file\n";
            print FOUT "$name: last saved run file was for run $last_saved_run\n"; 

            # check $run_number is set to the expected value.... if it is not, do not continue with this
            # run, since it is not a simple restart of mdarc.
            $this_run = $next_run-1; # expected current run number
            if ($run_number != $this_run)
            {   # Case 1.1A.2    
                print FOUT ("$name: information - run number ($run_number) has changed since mdarc was last running\n");
                print FOUT ("           - it is not safe to continue the old run ($this_run) \n");
                # run number will be set to next_run below
            }
            else
            {   # Case 1.1A.1:   run_number has not been changed
                # check if last saved file is for current run
                if ( $last_saved_run == $run_number ) 
                {
                    print FOUT "$name: detected saved file for run $this_run\n"; 
#
#           Expect that this file must be intermediate
                    if ($info)     
                    {  # this shouldn't happen 
                        print FOUT "$name: info - unexpectedly detected final run file for run $this_run.\n";
                        # run number will be set to next_run below
                    }
                    else    #  case 1.1 A  - these files are for current run
                    {    # this is what we expect - intermediate file with version number
                        print FOUT "$name: successful check -   continuing with run $this_run\n";
                        print FOUT "$name  - intermediate saved files found for run $this_run";
                        print "* * * *   $name: continuing with run $this_run * * *\n";
                        $msg_flag = $TRUE; # suppress a later message 

                        ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO - continuing to save data for run $this_run after mdarc restart");
                        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
                        $next_run =$this_run;  # the run number will only be changed if it was not set to $this_run
                    }
                } # last saved run = run_number
            } # run number has not been changed
        }  # saved files found
    }  # run is going
    

} # rn == 0

# Case 2 and 3 :  Case 1 continues here
if ($run_number == $next_run )
{     
    unless ($msg_flag)  # suppress these messages if we are continuing with a run
    {
        print FOUT   "$name: Current run number $run_number is up-to-date\n";
        print        "$name: Current run number $run_number is up-to-date.\n";
#  send a message
        ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Current run number $run_number is up-to-date" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    }
}
else
{
# Run number is not correct.
    print FOUT   "$name: Resetting the current run number $run_number to $next_run \n";
    print        "* * * $name: Resetting the current run number $run_number to $next_run * * * \n";

    unless (set_run_number($next_run)) 
    { 
        print FOUT   "Error return detected from set_run_number. Failed to reset run number\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Could not reset $run_number to $next_run" ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        
        die "$name:  Error return from set_run_number. Failed to reset run number";
    }
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Current run number $run_number has been reset to $next_run" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    print FOUT  "$name: Successfully reset run number to $next_run\n";
    if($debug) { print       "$name: Successfully reset run number to $next_run\n"; }
}

    return ($SUCCESS);
}
#####################################################################################################################
#
#           Type 1
#
#####################################################################################################################
sub set_rn_type1
{
# Type 1  Midas logger
# 
#  Assumes mlogger is in action, and enabled in odb
# 
#
# This is not as fool-proof as Type 2.  Once run has started, mlogger opens file straight away and this
# check runs too late to change run number. Can only return a failure status and rely on program check_run
# to stop the run.
#
# Case 1 :
#    rn=0     DUMMY STARTUP (of check_run for example)
#      Case 1.1  Run is going - can't change run number if it's wrong, since mlogger ignores any changes
#                               anyway - return a failure status.
#
#      Case 1.2  Run is stopped - can change run number , but watch out for empty midas file if last begin-run failed
#                Midas handles empty run file - no need to delete.
# Case 2 :
#    rn != 0, genuine run start
#                Can't change run number (no effect on mlogger, file already opened) - warn users if run number
#                is incorrect & return failure status.
#
#
#
#  input parameters:
#
#   $rn         may be 0 or current run number
#   $next_run   next run as received from run_number_check
#   $run_number current run number as read from odb
#   $saved_dir  saved directory
#   $eqp_name   equipment name

#   output parameters:
#   status           SUCCESS or FAILURE
#   subroutine returns 1 for SUCCESS,  0 for FAILURE

# Input parameters:
    my ($rn, $eqp_name, $next_run,  $run_number, $saved_dir )  = @_;
    my $parameter_msg = " rn, equipment name, next run, current run number, saved dir  ";
    my $nparam = 5;

    my $name="set_rn_type1";
    my $debug;
    my ($run_state,$transition, $this_run);
    my ($status, $last_saved_run, $info,$msg_flag, $filename, $tmp, $len);
    my ($size,$err);
    my $filename2;
#
#          Check the parameters
#
    $len = (@_) ; #make sure it's an integer
    $nparam = $nparam + 0;  #make sure it's an integer

    unless ($len == $nparam ) 
    {
        print FOUT "$name: Not all parameters are supplied\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Not all parameters are supplied. " ) ;
        unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        print "Invoke this perl script $name with the parameters:\n";
        print "   $parameter_msg\n"; 
        die      "Too few parameters supplied";
    }
    if($debug)
    {
	print FOUT "$name: Supplied parameters: $parameter_msg\n";
	print FOUT "$name:       Values     : @_\n";
	print "$name: Supplied parameters: $parameter_msg\n";
	print "$name:       Values     : @_\n";
    }
# 
    $rn = $rn + 0; # make sure it's an integer

#
#   Dummy start
#
    if ($rn == 0)   
    {        
	($run_state, $transition) = get_run_state();
#
#   Dummy start : STOPPED
#
	if ($run_state == $STATE_STOPPED )    # case 1.2  Run stopped
	{
	    print FOUT "$name: INFO run is stopped \n";
	    if ($debug) { print      "$name: INFO run is stopped \n"; }
	    $next_run = $next_run -1 ; # next start of run will increment the run number
#  check for empty midas file ....  
	    $filename=sprintf("%06d.mid",$next_run);
	    print FOUT "checking if last saved midas file $filename is an empty midas file\n";
	    if ($debug) { print "checking if last saved midas file $filename is an empty midas file\n";}
	    if (-e $filename)
	    { 
		if (-z $filename)
		{ 
		    print "file $filename exists and has zero size\n"; # midas logger will overwrite this
		    print FOUT "file $filename exists and has zero size\n"; # midas logger will overwrite this

                    # check whether there is a .msr file for this run number
		    print FOUT "$name: calling check_for_msr\n";
		    ($status,$err)=check_for_msr($next_run,$saved_dir); # need to glob with .msr*
		    if ($status== $FAILURE)
		    { 
			print_3 ($name,"Failure status from check_for_msr",$MERROR,0);
			return ($FAILURE);
		    }
		    if($err==-1) 
		    { 
			print_3 ($name,"Found an existing msr file and (strangely) an empty mid file for run number ($run_number)",
				 $MINFO,0);
			print_3($name,"Midas logger may have an open filehandle on the empty mid file. Cannot proceed",$MERROR,0);
			print_3($name,"Delete either the msr file (if invalid) or the empty mid file for run $run_number",$MERROR,0);
			print_3($name,"Then press TEST/REAL button before starting next run",$MERROR,0);
			return ($FAILURE);
		    }
		    else
		    {
			print FOUT "$filename2 does not exist\n";
			$next_run = $next_run -1 ; # next start of run will increment the run number
			print_3($name,"File $filename should be overwritten by logger. Setting next_run to be $next_run",$MINFO,0); # midas logger will overwrite this			   
		    } # no msr file
		    
		}	    
		else
		{
		    $size = (-s $filename);
#		    if ($debug) { print "$name: file $filename exists (and has size $size)\n";}
		    print "$name: file $filename exists (and has size $size)\n";
		    print FOUT "$name: file $filename exists (and has size $size)\n";
		}
	    }
	    else
	    {
		if ($debug) { print "$name: file $filename does not exist; there may be  a msr file\n"; }
		print FOUT "$name: file $filename does not exist; there may be a msr file\n";
	    }
	    if ($run_number != $next_run   )
	    {
                 # Run number is not correct. Run is stopped so reset run number.
		print FOUT   "$name: Resetting the current run number $run_number to $next_run \n";
		print        "* * * $name: Resetting the current run number $run_number to $next_run * * * \n";

		unless (set_run_number($next_run)) 
		{ 
		    print FOUT   "Error return detected from set_run_number. Failed to reset run number\n";
		    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Could not reset $run_number to $next_run" ) ;
		    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
		    die "$name:  Error return from set_run_number. Failed to reset run number";
		}
		($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Current run number $run_number has been reset to $next_run" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
		print FOUT  "$name: Successfully reset run number to $next_run\n";
		if($debug) { print       "$name: Successfully reset run number to $next_run\n"; }
	    }
	    else
	    {
		print FOUT "$name: INFO - Current run number  $run_number IS correct.\n";
		print "$name: INFO - Current run number $run_number is correct.\n"; 
		($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Current run number $run_number is correct" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    }
	    return ($SUCCESS);
	}
#
#   Dummy start : RUNNING
#
	else  # RUNNING
	{                    # case 1.1 Run is going - but is run number correct ?
	    if ($run_number == $next_run -1  )
	    {     
		print FOUT   "$name: Current run number $run_number is up-to-date\n";
		print        "$name: Current run number $run_number is up-to-date.\n";
		($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Current run number $run_number is up-to-date" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	    }
	    else
	    {   
		if (mlogger_running() )
		{    # can't change run number if we are running - at least it wouldn't have any effect on the saved file
		    $tmp =  $next_run -1 ;
		    print FOUT "$name:  Current run number $run_number seems to be incorrect. Expect $tmp\n"; 
		    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Current run number seems to be incorrect. Expect $tmp" ) ;
		    die      "$name:  Current run number $run_number seems to be incorrect. Expect $tmp\n"; 
		}
		else
		{  
		    print FOUT "$name:  mlogger is not running. Run number not checked\n"; 
		    print      "$name:  mlogger is not running. Run number not checked\n"; 
		}
	    }
	}
    }
#
#   GENUINE START OF RUN
#
    else
    {
	#   Case 1 continues here (rn != 0 ) Real start-of-run
	if ( mlogger_running() )
	{
	    #   Type 1 run mlogger should already have incremented the run number (and opened mid file)
#            print FOUT "Mlogger (if running)  opens a file immediately - therefore compare current run number ($run_number) ";
#	    print FOUT "to next_run-1. ( Next_run = $next_run)\n";
	    print FOUT "midas logger is active and running - comparing run number with ($next_run-1)  \n";
	    $tmp = $next_run -1 ;	    
	    if ($run_number == $tmp )
	    {     
		print FOUT   "$name: Current run number $run_number is up-to-date\n";
		print        "$name: Current run number $run_number is up-to-date.\n";
		($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: Current run number $run_number is up-to-date" ) ;
		unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
		# there seems to be one more situation to test for in this case... an older msr file
		# already present for this run number (most likely from a previous Type 2 run).
		#
		# hopefully AVOID overwriting a MSR run by this test
		print FOUT "$name: calling check_for_msr\n";
		($status,$err)=check_for_msr($run_number,$saved_dir);
		if ($status== $FAILURE)
		{ 
		    print_3 ($name,"Failure status from check_for_msr",$MERROR,0);
		    return ($FAILURE);
		 
		    print_3 ($name,"PROBLEM: found an existing msr file for this run number ($run_number)",$MERROR,0);
#  there is probably also an empty midas file 
# ( an empty midas file and an msr file can happen if there was a nasty problem with run numbering)   
		    $filename=sprintf("%06d.mid",$run_number);
		    print FOUT "checking for empty midas file $filename \n";
		    print     "checking for empty midas file $filename \n";
		    if (-e $filename)
		    { 
			if (-z $filename)
			{ 
			    print "file $filename exists and has zero size\n"; # midas logger will overwrite this
			    print FOUT "file $filename exists and has zero size\n"; # midas logger will overwrite this
			    
			    print_3 ($name,"Delete empty midas file ($filename) or the msr file (if invalid)",$MINFO,0);
			    printf_3($name,"Then press TEST/REAL button  before retrying run start",
				     $MINFO,0);
			}
		    }
		    return($FAILURE);
		}
	    }
	    else
	    {
		# Run number is not as expected.
		#    if we get here is is due to  a failure at last BOR ....
		#    Previous to Midas 1.9 we had to stop the run from starting or we get data with incorrect run number in the file
		
		print_3($name,"* Last run start may have failed  or file handle may have been lost *",$MINFO,0);
		print_3($name,"* Expect logger (midas 1.9) to continue using run file $run_number *",$MINFO,0);
	    }
	}
	else
#       Mlogger NOT running or enabled; we don't care about the run number
	{
	    print FOUT "midas logger is NOT running. Run number is not checked\n";
	    print      "midas logger is NOT running. Run number is not checked\n";
	    ($status)=odb_cmd ( "msg","$MINFO","","$name", "INFO: midas logger is NOT running. Run number is not checked " ) ;
	}
    } 
    return($SUCCESS);
}

# this 1 is needed at the end of the file so require returns a true value   
1;   










