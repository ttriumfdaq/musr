#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from set_status button on custom Web page Custom_Status.html
# 
# invoke this script with cmd  e.g.
#              include                    experiment        beamline
#                path                                     
# set_status.pl /home/bnmr/online/perl       bnmr             bnmr
# set_status.pl /home/bnqr/online/perl       bnqr             bnqr
#
# toggles Status page between std Midas page and Custom Status page
# by either deleting odb key /Custom/Status
# or creating odb key /Custom/Status by loading a file /home/bn<mq>r/online/custom/status.odb
#
# ############################################################################################
#
#       Note status.odb and custom.odb are in CVS. 
# Custom.odb contains the keys needed in /custom for set_status.pl and custom web page to work.
#
# ############################################################################################
#
# $Log: set_status.pl,v $
# Revision 1.4  2008/07/21 22:30:12  suz
# changed message. Autorun_bnmr is gone. Replaced by CustomStatus
#
# Revision 1.3  2008/02/13 20:52:00  suz
# Status is now a link to CustomStatus&. Supply linkname for Status to odb_access
#
# Revision 1.2  2008/02/04 23:17:01  suz
# filename of custom script changed
#
# Revision 1.1  2007/09/17 18:33:02  suz
# initial version: toggles custom webpage for main status page
#
#
use strict;
##################### G L O B A L S ####################################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (set_status)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our ($inc_dir,$expt, $beamline ) = @ARGV;
our $len =  $#ARGV; # array length
our $name = "set_status"; # same as filename
our $nparam=3;
our $outfile = "set_status.txt"; #path supplied by file open
our $parameter_msg = "include_path, experiment;  beamline\n";
#######################################################################
# local variables
my ($path, $key, $status);
my $debug=$FALSE;
my $filename;
#######################################################################
#
$|=1; # flush output buffers


# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

#
# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; \n";
#
#
#
if( $beamline =~ /bn[qm]r/i )
{
# BNM/QR experiments use custom page
}
else
{ # no action
exit;
}

# Status is now a link; supply the expected name of the link or odb_cmd will flag and error 
($status) = odb_cmd ( "ls","/Custom","Status","","","CustomStatus&");

    print_2 ($name,"after odb_cmd, ANSWER=$ANSWER ",$CONT);
if  ($status)
{
    print_2 ($name,"found /Custom/Status",$CONT);

    ($status)=odb_cmd ( "rm","/Custom","Status","", "" ) ;
    print_2 ($name,"after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status)
       {    
         print_2 ($name,"removed /Custom/Status",$CONT);
       }
    else {
	print_2 ($name,"$name: after odb_cmd, ANSWER=$ANSWER ",$CONT);}
}
else
{
    print_2 ($name,"Creating /Custom/Status",$CONT);

    $filename = "/home/$beamline/online/custom/status.odb";
    print_2($name,"Filename=$filename",$CONT);
    unless (-e $filename)
    {
	print_3 ($name,"No such file as $filename",$DIE);
    }

    ($status)=odb_cmd ( "load","$filename","","", "" ) ;

    print_2 ($name,"$name: after odb_cmd, COMMAND=$COMMAND ",$CONT);
    if  ($status)
    {
	
	print_2($name, "loaded file to create /Custom/Status",$CONT);
    }
    else {
	print_2 ($name,"$name: after odb_cmd, ANSWER=$ANSWER ",$CONT);}

}    
exit;
