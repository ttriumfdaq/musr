#!/usr/bin/perl -w
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#  stop_now.pl
# 
# invoke this perlscript with cmd 
#                  include path          beamline  run     experiment     
# stop_now.pl  /home/bnmr/online/perl    bnmr     number    bnmr 
#
#   * include_path = /home/bnmr/online/perl 
#
#
# $Log: stop_now.pl,v $
# Revision 1.1  2004/11/17 00:05:30  suz
# original: send stop now cmd as deferred transition is now implemented on stop
#
#
#
use strict; 
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_PAUSED=2;  # Run state is paused
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
# init_check uses $inc_dir, $expt, $beamline from the input parameters
our($inc_dir, $beamline, $run_number,$expt) =@ARGV;
# note: for BNMR/BNQR saved directory in /script/kill is a link to mdarc's (type 2) or mlogger's (type 1). 
# These directories must be the same (this is checked)
# note also that toggle is not supported for BNMR type 1
our $len =  $#ARGV; # array length
our $name = "stop_now"; # same as filename
our $nparam = 4;
our $outfile = "stop_now.txt";
our $parameter_msg = "include_dir, expt, run number,   beamline";
our $suppress=$FALSE; # do not suppress message from open_output_file
#########################################################
my $status;
my ($old_run, $eqp_name);
my $mdarc_path = "/Equipment/FIFO_acq/mdarc/";
my ($transition, $run_state);
#########################################################

$|=1; # flush output buffers

# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
unless ($beamline) { die "$name: No beamline has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/check_params.pl";
# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

# Output will be sent to file given by $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null
#

print FOUT  "$name: Arguments supplied:  @ARGV\n";
print FOUT  "$name starting with parameters:  \n";
print FOUT  "Beamline=$beamline; Run number=%run_number; Experiment = $expt  \n";

if ($expt eq "") 
{
    print_3 ($name, "FAILURE: No experiment supplied ",$MERROR, $DIE); 
}
unless ($run_number) 
{
    print_3 ($name, "FAILURE: No run number supplied ",$MERROR, $DIE); 
}

# see if the run is stopped
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 
if ($run_state == $STATE_STOPPED)
{   # Run is stopped
    print_3($name,"Run is stopped. Can only stop when running",$MERROR,$DIE);
}

#
#      determine equipment name from beamline
#
if( ($beamline =~ /bn[mq]r/i) )
{
    # BNMR/BNQR experiments use equipment name of FIFO_ACQ
    $eqp_name = "FIFO_ACQ";
}
else
  {
    # All MUSR experiments use equipment name of MUSR_TD_ACQ
    $eqp_name = "MUSR_TD_ACQ";
} 
$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";


#   stop the run
print FOUT  "Attempting to stop run $run_number (run_state=$run_state) \n";

($status) = odb_cmd ( "stop now" ) ;
unless ($status) { exit_with_message($name); }
($run_state,$transition) = get_run_state();
get_run_state($transition,$run_state);
#if($transition) { print FOUT  "Run is in transition ($trans) \n"; }
if($run_state != $STATE_STOPPED ) 
{
    print_3($name," FAILURE: Can't stop the run ",$MERROR,$DIE);
}
print_3 ($name,"Stop now command has been sent; not waiting for data from last cycle",$MINFO,$CONT);
exit;

