#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
 
# invoke this script with cmd e.g.
#         include                      experiment  disable run    beamline ppg
#         path                                    numbering flag           mode
# test.pl /home/bnmr/online/mdarc/perl    bnmr       n             bnmr   2a
# test.pl /home/bnqr/online/mdarc/perl    bnqr       n             bnqr   2a
# test.pl /home/musrdaq/musr/mdarc/perl   musr       n             m20    2 ** always 2 for MUSR **
#
#
# Set a parameter (run type) in odb (mdarc area) for a test run
# and call get_next_run_number.pl to supply the next valid run number.
#
# $Log: test_run.pl,v $
# Revision 1.14  2004/06/10 16:50:41  suz
# include init_check.pl; change use vars to our; Rev 1.6 message is erroneous
#
# Revision 1.13  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.12  2004/04/21 19:07:21  suz
# add a space in search string to fix bug for beamlines m15/m20
#
# Revision 1.11  2004/03/30 22:51:27  suz
# add some timing info using write_time
#
# Revision 1.10  2004/03/29 18:27:47  suz
# allows only one copy to run; open_output_file now appends to message file
#
# Revision 1.9  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.8  2003/01/08 18:29:57  suz
# add polb
#
# Revision 1.7  2002/04/15 18:48:02  suz
# fix invoke script message for extra param include_path
#
# Revision 1.6  2002/04/12 21:45:48  suz
# add parameter include_path
#
# Revision 1.5  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.4  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.3  2001/09/14 19:29:23  suz
# add MUSR support and 1 param; use strict;imsg now msg
#
# Revision 1.2  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.1  2001/04/30 19:53:04  suz
# Initial version
#
#
#
use strict;
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
##########################################################################
#  parameters needed by init_check.pl (required code common to perlscripts) :
our ($inc_dir, $expt,$dis_rn_check, $beamline, $ppg_mode ) = @ARGV;# input parameters
our $len = $#ARGV; # array length
our $name = "test_run"; # same as filename
our $parameter_msg = "include_path, experiment , flag (disable automatic run numbering),  beamline, ppg mode";
our $nparam = 5;  # no. of input parameters
our $outfile = "test_run.txt";
################################################################################
# local variables
my ($transition, $run_state, $path, $key, $status);
my ($mdarc_path, $eqp_name);
my ($cmd);
my $debug=$FALSE;


$|=1; # flush output buffers


# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 

# Init_check.pl checks:
#   one copy of script running
#   no. of input parameters
#   opens output file:
#
require "$inc_dir/init_check.pl";

print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt; Flag to disable run number check = $dis_rn_check; beamline = $beamline \n";
#
#
# Does not matter whether data logger is running or not
# But automatic run numbering must be enabled
#
unless ($dis_rn_check eq "n") 
{
        print FOUT "FAILURE: Automatic run numbering is DISABLED \n"; 
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Automatic run numbering is DISABLED" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
        die  "FAILURE:  Automatic run numbering is DISABLED \n"; 
}
#

unless ($ppg_mode)
{
    print_3($name,"FAILURE: beamline not supplied",$MERROR,1);
}
unless ($ppg_mode =~/^[12]/)
{
    print_3 ($name, "FAILURE: invalid ppg mode ($ppg_mode) supplied", $MERROR, 1);
}

#
#      determine equipment name from beamline
#
if($beamline =~ /bn[qm]r/i)   
{
# BNMR experiments use equipment name of FIFO_ACQ
$eqp_name = "FIFO_ACQ";
}
else
{
# All MUSR experiments use equipment name of MUSR_TD_ACQ
$eqp_name = "MUSR_TD_ACQ";
} 
$mdarc_path= "/Equipment/$eqp_name/mdarc/";
print FOUT "mdarc path: $mdarc_path\n";


# check whether run is in progress

($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if ($run_state != $STATE_STOPPED)
{   # Run is going

# return
     if ($ppg_mode =~ /^2/i) # match 2 at beginning of string (e.g. 2a) 
    {
	print FOUT "Run in progress. Use Toggle button to change run type \n";
	($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - run is in progress. Use toggle button to change run type" ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die " Run is in progress. Use Toggle button to change the run type\n";
    }
    else  # Type 1
    {
	print FOUT "Run in progress.  Run type cannot be changed while run is in progress  \n";
	($status)=odb_cmd ( "msg","$MINFO", "","$name", "WARNING - Run type cannot be changed while run is in progress " ) ;
	unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
	die "  Run type cannot be changed while run is in progress\n";
    }
}
else
{       # run is stopped
        
    ($status) = odb_cmd ( "set","$mdarc_path","run type" ,"test") ;
    unless($status)
    { 
        print FOUT "$name: Failure from odb_cmd (set); Error setting run type\n";
        ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting key run type " ) ;
        unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
        die "$name: Failure setting key run type\n";
    }
    else 
    { 
        print FOUT "$name: Success -  key run type has been set to test in odb\n"; 
    }
    # now launch get_next_run_number
    print FOUT "Calling get_next_run_number with parameters: $inc_dir  $expt 0 $eqp_name 0 $beamline $ppg_mode\n";
##    $cmd = sprintf("/home/bnmr/online/mdarc/perl/get_next_run_number.pl %s 0 %s 0 %s",$expt,$eqp_name,$beamline,$ppg_mode);
    $cmd = sprintf("$inc_dir/get_next_run_number.pl %s %s 0 %s 0 %s %s",
		   $inc_dir,$expt,$eqp_name,$beamline,$ppg_mode);

    print "$name: sending command: $cmd\n";
    print FOUT "$name: sending command: $cmd\n";
    print      "Output from get_next_run_number is in /home/midas/musr/log/$beamline/get_run_number.txt\n";
    print FOUT "Output from get_next_run_number is in /home/midas/musr/log/$beamline/get_run_number.txt\n";
    $status=system "$cmd";
    if( $status == 0)
    {
	print FOUT "Success after system command, status=$status\n";
	print "Success after system command, status=$status\n";
    }
    else
    {
	print FOUT "Failure after system command, status=$status\n";
	print      "Failure after system command, status=$status\n";
	if ($status == -1 )
	{      # there is a message in errno
	    print FOUT "Error message: $!\n"; # errno is put in $! for system cmd
	    print      "Error message: $!\n";
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number due to: $! " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
	}
	else
	{
	    ($status)=odb_cmd ( "msg","$MERROR","","$name", "Failure return from get_next_run_number (status=$status) " ) ;
	    unless($status) { print FOUT "$name: Failure from odb_cmd (msg)";}
	}
    }
}
write_time($name, 3);
exit;








