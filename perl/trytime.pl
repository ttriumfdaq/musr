#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug -d warnings -w
###  !/usr/bin/perl -d
#
# Filename tester for shifts
# To test, convert the required date to unixtime in an xterm like this
# e.g.
# $ date +%s -d "1:36am Aug 1"
# 1406907360
#
# Then feed that number into this program
# [bnmr@isdaq06 perl]$ trytime.pl 1406907360
# Time now = 1406907360;  Filename is 1Aug2014_AMshift.txt

use warnings;
use strict;
my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
my $filename;
my $now;
#
# Parameter: unix time of required date (see above)
# No parameter supplied: uses present time
$now= shift or $now=time();

my $MONITOR_DIR="";

  my  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($now);
    $year+=1900; # year since 1900

# Determine which shift this is
  #    Owl = 0030-0829
  #    Day = 0830-1629
  #    Eve = 1630-0029


$hour +=0;
$min  +=0;
my $mtime;
my $beamline_dir=""; #no directory for test
# Look for "EVE" shift previous day
if (($hour == 0) && ($min < 30))
{ # EVE shift previous day
    $mtime = "EVE"; 
    my $yesterday = $now - 3600*24; # get date 24 hours ago
    
    my ($sec2,$min2,$hour2,$mday2,$mon2,$year2,$wday2,$yday2,$isdst2) = localtime($yesterday);
    $year2+=1900; # year since 1900

    print "mday=$mday and mday2=$mday2\n";
    $filename= $MONITOR_DIR.$mday2.$months[$mon2].$year2."_".$mtime.".txt";
    print "Time now = $now;  Filename is $filename\n";
    exit;
}
# All other times are today's shifts
if (($hour < 8) || ($hour == 8) && ($min <30))  { $mtime="OWL"; } # same day 
elsif  (($hour > 16) || ($hour == 16) && ($min > 29))  { $mtime="EVE"; } # same day 
else { $mtime = "DAY";}  # same day 

# assemble filename(s) for this shift
$filename= $MONITOR_DIR.$mday.$months[$mon].$year."_".$mtime.".txt";
 print "Time now = $now;  Filename is $filename\n";

exit;
