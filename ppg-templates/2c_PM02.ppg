// Header section

Clock Frequency = 10 Mhz;
ISA Card Address = 8000;   // VME address of the pulse programmer board (in hex)
Number of Flags = 24;      // Number of output bits

//************************************************************************************************************************************//

//************************************************************************************************************************************//

// !! USER SECTION !! //

  // Delay declarations

  d_scalerpulse=0;  	// pulse-length for the multiscaler-next pulse
  d_nxtfpulse=0;   	// pulse-length for the next freq pulse
  d_pulse=0;   		// pulse-length, used also as short pulse for script coherence (every program flow instruction need a corresponding delay
  d_RFpulse=0;          // irradiation for a single slice
  d_pre_beam_on=0;  	// beam on  cycle during beam build up
  d_pre_beam_off=0;  	// beam off cycle during beam build up

  d_ms_predelai=0;   	// delay between scaler next pulse and following enable counting-gate during the buildup    (RF on delay cycle)
  d_ms_postdelai=0;   // delay between disable counting-gate and following scaler next pulse during the buildup   (RF on delay cycle)

  d_countingtime=0;   	// counting enabled time                                                                    (case one, two or RF on delay cycle)

  d_ms_predelay=0;   	// delay between scaler next pulse (or rf-off in case 2) and following enable counting-gate (case one, two or three)
  d_ms_postdelay=0;   	// delay between disable counting-gate and following disable RF or pulse                    (case one, two or three)
  d_flip180=0;   	// delay after 180 pulse                                  (only when counting after second 180 pulse)
  d_flip360=0;   	// delay after second 180 pulse                           (only when counting after first  180 pulse)
  d_slice_internal=0;   // delay between multi-slice pulse trains

  d_dacs=0; 		// DAC Servicetime
  d_beam_off=0;   	// beam-off time between beam on and the first 180 pulse
  d_beam_on=0;          // beam-on time in pulsed beam mode

// !! END OF USER SECTION !! //

//************************************************************************************************************************************//

// Bit patterns ATTENTION!!: The bits are adressed from high to low, the position in the sum gives the number of the bit affected.

f_rf0_on       = 1,1;   // bit   1
f_rf0_off      = 0,1;
f_rf90_on      = 1,1;   // bit   2
f_rf90_off     = 0,1;
f_rf180_on     = 1,1;   // bit   3
f_rf180_off    = 0,1;
f_rf270_on     = 1,1;   // bit   4
f_rf270_off    = 0,1;
f_rfgate_on    = 1,1;   // bit   5
f_rfgate_off   = 0,1;

f_rf_on        = 1,1;   // bit   6
f_rf_off       = 0,1;

f_msnp_hi  = 1,1;   // bit   7 next dwelltime multiscaler pulse
f_msnp_lo  = 0,1;
f_counter_e    = 1,1;   // bit   8 multiscaler counter-gate
f_counter_d    = 0,1;

f_userbit1_on  = 1,1;   // bit   9 -> Userbit1
f_userbit1_off = 0,1;

f_userbit2_on  = 1,1;   // bit  10 -> Userbit2
f_userbit2_off = 0,1;

f_freqp_hi     = 1,1;   // bit  11 next FSC freq. ext. strobe
f_freqp_lo     = 0,1;

f_udctrl_u     = 1,1;   // bit  12 Strobe up down control
f_udctrl_d     = 0,1;

f_dacservp_hi  = 1,1;   // bit  13 Dac service pulse
f_dacservp_lo  = 0,1;

f_beam_on      = 1,1;   // bit 14
f_beam_off     = 0,1;

f_pol_pos      = 1,1;   // bit 15
f_pol_neg      = 0,1;

f_fsc_0        = 0,9;
f_fsc_1        = 1,9;

f_fsc_dummy    = 0,9;   // mask for channel 16 - 24 FSC memory select channels

//************************************************************************************************************************************//

// Program //--------------------------------------------------------------------------------------------------------------// Program //

// pre-Acquisition beam on to reach steady state condition

        d_pre_beam_on    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;

Loop Acquisition n_acq;
        d_pulse  	 f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;

 Loop rf_on n_rf_on;

    	d_beam_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
        d_beam_off       f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	d_scalerpulse    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

    loop sslices n_slices;

        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_slice_internal f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

    End loop sslices;

        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off  + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_flip180        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

    loop fslices n_slices;

        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_slice_internal f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

    End loop fslices;
        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_ms_predelay    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
	d_countingtime   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
	d_ms_postdelay   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

 End Loop rf_on;
 
 //Last dwelltime and (multi-slice?)freq step with rf_on: userbit 2 goes high
     	d_beam_on        f_fsc_dummy + f_pol_pos + f_beam_on  + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
        d_beam_off       f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	d_scalerpulse    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

    loop lsslices n_slices;

        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_slice_internal f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

    End loop lsslices;

        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_flip180        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;

    loop lfslices n_slices;

        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_slice_internal f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
        d_nxtfpulse     f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_hi + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
    
    End loop lfslices;
    
        d_RFpulse        f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_on  + f_rfgate_on  + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_on ;
        d_ms_predelay    f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
	d_countingtime   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_on  + f_userbit1_on  + f_counter_e + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
	d_ms_postdelay   f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_lo + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off;
 
 //Multiscalerpulse n+1 and DAC Servicetime

        d_pulse          f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_hi + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_hi + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;
	d_dacs           f_fsc_dummy + f_pol_pos + f_beam_off + f_dacservp_hi + f_freqp_lo + f_udctrl_u + f_userbit2_off + f_userbit1_off + f_counter_d + f_msnp_lo + f_rf_off + f_rfgate_off + f_rf270_off + f_rf180_off + f_rf90_off + f_rf0_off ;

End Loop Acquisition;
