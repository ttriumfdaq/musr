/*------------------------------------------------------------------*
 * trPPG.c 
 * Procedures for handling the Pulse Programmable board
 * This is a VME board specified by Syd Kreitzman - TRIUMF
 * designed and produced by Triumf electronics group - Hubert Hui
 *
 * CVS log information:
 *$Log: trPPG.c,v $
 *Revision 1.13  2009/08/17 20:28:35  suz
 *ppgload: need 3 clock cycles for stop cmd; read more info from external trigger register
 *
 *Revision 1.12  2005/05/16 19:13:59  suz
 *make sure beam is set to off in ppginit
 *
 *Revision 1.11  2005/02/04 18:31:05  suz
 *add new prototypes & mask for dual channel mode
 *
 *Revision 1.10  2005/01/26 00:10:29  suz
 *PPG hardware modified for dual channel mode; ppgEnable and ppgDisable changed to ppgStartSequencer and ppgStopSequencer; enable/disable external trigger is added
 *
 *Revision 1.9  2004/03/02 23:44:27  suz
 *add message about too many rpc processes
 *
 *Revision 1.8  2003/03/24 19:00:27  suz
 *Renee fixed memory leak by adding fclose
 *
 *Revision 1.6  2001/10/23 21:33:04  suz
 *add delay & input->ppginput
 *
 *Revision 1.5  2001/08/06 18:52:30  midas
 *New functions - beam control register(RP)
 *
 *Revision 1.4  2001/04/04 01:44:31  renee
 *Add error returns
 *
 *Revision 1.3  2000/12/01 00:33:22  renee
 *Reverse polz bit in software to match Epics readout
 *
 *Revision 1.2  2000/11/28 18:25:48  renee
 *Add procedure to test Power up state : ppgStartpatternWrite
 *Add procedures to change the Polarization control bit:
 *      ppgPolzSet, ppgPolzRead and ppgPolzFlip
 *Add a return state to ppgLoad
 *
 *------------------------------------------------------------------*/
#include "trPPG.h"
int ddd=0;
/*------------------------------------------------------------------*/
void ppg(void)
{
   printf("PPG function support\n");
   printf("inline: ppgInit(ppg_base) \n");
   printf("inline: ppgLoad(ppg_base, file)\n");
   printf("inline: ppgStopSequencer(ppg_base) \n");
   printf("inline: ppgStartSequencer(ppg_base) \n");
   printf("inline: ppgEnableExtTrig(ppg_base) \n");
   printf("inline: ppgDisableExtTrig(ppg_base) \n");
   printf("inline: ppgExtTrigRegRead(ppg_base) \n");
   printf("inline: ppgStatusRead(ppg_base) \n");
   printf("inline: ppgPolmskRead(ppg_base)\n");
   printf("inline: ppgPolmskWrite(ppg_base, polarity)\n");
   printf("inline: ppgRegWrite(ppg_base, offset, value)\n");
   printf("inline: ppgRegRead(ppg_base, offset)\n");
   printf("inline: ppgStartpatternWrite(ppg_base,value)\n");
   printf("inline: ppgPolzSet(ppg_base, value)\n");
   printf("inline: ppgPolzRead(ppg_base)\n");
   printf("inline: ppgPolzFlip(ppg_base)\n");
   printf("inline: ppgPolzCtlPPG(ppg_base)\n");
   printf("inline: ppgPolzCtlVME(ppg_base)\n");
   printf("inline: ppgBeamOn(ppg_base) \n");
   printf("inline: ppgBeamOff(ppg_base) \n");
   printf("inline: ppgBeamCtlPPG(ppg_base) \n");
   printf("inline: ppgBeamCtlRegRead(ppg_base) \n");

   printf("\n");
}
#ifdef TEMP
/* debugging only */
INLINE void  ppgReadAll(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  int data;
  DWORD value;

  ppgStatusRead(base_adr);
  ppgExtTrigRegRead(base_adr);
  ppgPolmskRead(base_adr);
  ppgPolzRead(base_adr);
  ppgBeamCtlRegRead(base_adr);

  printf("Reading McS Counter reg\n");
  value=0;
  data = ppgRegRead(base_adr, VME_MCS_COUNT_HI);
  data=data & 0xFF;
  /*  printf("mcs count hi: 0x%2.2x\n",data); */
  value = data << 24; 

  data = ppgRegRead(base_adr, VME_MCS_COUNT_MH);
  data=data & 0xFF00;
  /*  printf("mcs count mh: 0x%2.2x\n",data); */
  value = value || (data << 16); 

  data = ppgRegRead(base_adr, VME_MCS_COUNT_ML);
  data=data & 0xFF0000;
  /*  printf("mcs count ml: 0x%2.2x\n",data); */
  value = value || (data << 8);

  data = ppgRegRead(base_adr, VME_MCS_COUNT_LO);
  data=data & 0xFF000000;
  /*  printf("mcs count lo: 0x%2.2x\n",data); */
  value = value || data;
  printf("mcs value: 0x%x\n",value);

  printf("Reading VME Cycle Counter reg\n");
  value=0;
  data = ppgRegRead(base_adr, VME_CYC_COUNT_HI);
  data=data & 0xFF;
  value = data << 24; 

  data = ppgRegRead(base_adr, VME_CYC_COUNT_MH);
  data=data & 0xFF00;
  value = value || (data << 16); 

  data = ppgRegRead(base_adr, VME_CYC_COUNT_ML);
  data=data & 0xFF0000;
  value = value || (data << 8);

  data = ppgRegRead(base_adr, VME_CYC_COUNT_LO);
  data=data & 0xFF000000;
  value = value || data;
  printf("VME cycle counter: 0x%x\n",value);
  return;
}
#endif
/*------------------------------------------------------------------*/
/** ppgPolzSet
    Set Polarization bit to a given value.
    @memo Write PPG.
    @param base\_adr PPG VME base addroless
    @param value (8bit)
*/
INLINE void ppgPolzSet(const DWORD base_adr, BYTE value)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_POLZ_SET);
  /* flip the bit to be in synch with Epics readout */
  if(value) {
    value = 0;
  }
  else {
    value = 1;
  }
  /* end of flipping the bit */
  if (ddd) printf("Writing 0x%x to %p\n", value, spec_Adr);
  *spec_Adr = value;
}

/*------------------------------------------------------------------*/
/** ppgPolzRead
    Read Polarization bit.
    @memo Read PPG.
    @param base\_adr PPG VME base addroless
    @return value (8bit)
*/
INLINE BYTE  ppgPolzRead(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  volatile BYTE value;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_POLZ_SET);
  if (ddd) printf("Reading addr 0x%x \n",  spec_Adr);
  value = *spec_Adr;
  /* flip the bit to be in synch with Epics readout */
  if(value) {
    value = 0;
  }
  else {
    value = 1;
  }
  /* end of flip */
  return value;
}

/*------------------------------------------------------------------*/
/** ppgPolzFlip
    Flip the polarization bit.
    @memo Read PPG.
    @param base\_adr PPG VME base address
*/
INLINE BYTE  ppgPolzFlip(const DWORD base_adr)
{
  volatile BYTE value;
  value = ppgPolzRead(base_adr);
  if(value) {
    ppgPolzSet(base_adr, 0);
  }  
  else {
    ppgPolzSet(base_adr, 1);
  }
  return  ppgPolzRead(base_adr);
}

/*------------------------------------------------------------------*/
/** ppgRegWrite
    Write into PPG register.
    @memo Write PPG.
    @param base\_adr PPG VME base addroless
    @param reg\_offset PPG register
    @param value (8bit)
    @return status register
*/
INLINE BYTE ppgRegWrite(const DWORD base_adr, DWORD reg_offset, BYTE value)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + reg_offset);
  if (ddd) printf("Writing 0x%x to %p\n", value, spec_Adr);
  *spec_Adr = value;
  ss_sleep(20);
  return *spec_Adr;
}
/*------------------------------------------------------------------*/
/** ppgRegRead
    Read PPG register.
    @memo Read PPG.
    @param base\_adr PPG VME base addroless
    @param reg\_offset PPG register
    @return status register (8 bit)
*/
INLINE BYTE ppgRegRead(const DWORD base_adr, DWORD reg_offset)
{
  volatile BYTE * spec_Adr;
  volatile BYTE value;
  spec_Adr = (BYTE *)(A16 + base_adr + reg_offset);
  value = *spec_Adr;
  ss_sleep(20);
  return value;
}

/*------------------------------------------------------------------*/
/** ppgInit
    Initialize the PPG
    @memo Initialize PPG
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgInit(const DWORD base_adr)
{
  ppgPolmskWrite(base_adr, DEFAULT_PPG_POL_MSK);
  ppgPolzCtlVME(base_adr); /* by default VME controls Helicity (DRV POL) */
  ppgDisableExtTrig(base_adr); /* default.. external trigger input disabled */
  ppgBeamOff(base_adr); /* beam off (VME control) */
  return;
}

/*------------------------------------------------------------------*/
/** ppgStatusRead
    Read Status register.
    @memo Read status.
    @param base\_adr PPG VME base address
    @return status register
*/
INLINE BYTE ppgStatusRead(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  BYTE value,itmp;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_READ_STAT_REG);
  value = *spec_Adr;
  printf("PPG Status Reg: 0x%x\n",value);
   if (value & 1)
      printf("Pulseblaster IS reset\n");
    else
      printf("Pulseblaster is NOT  reset\n");
   if (value & 2)
     printf("Pulseblaster IS running \n");
   else
     printf("Pulseblaster NOT running \n");
   if (value & 4)
     printf("Pulseblaster IS stopped \n");
   else
     printf("Pulseblaster is NOT stopped \n");
 itmp =  (value & 0x18)>> 4;
     printf("VME polarization source control bits = 0x%x  i.e. ",itmp);
     if (itmp == 0)
       printf("VME control\n");
     else if (itmp == 1)
       printf("External control\n");
     else if (itmp == 2)
       printf("Pulseblaster control via ch 15\n");
     else
       printf("Not used; polarity driver pulled high\n");

   if (value & 20)
     printf("VME Polarization control: ACTIVE \n");
   else
     printf("VME Polarization control: OFF \n");
   /*   if (value & 40)
     printf("External clock IS present \n");
   else
     printf("External clock NOT present \n");
   */
   if (value & 80)
     printf("External  Polarization control: ACTIVE \n");
   else
     printf("External  Polarization control: OFF \n");



  return *spec_Adr;
}
/*------------------------------------------------------------------*/
/** ppgBeamOn
    Directly set Beam On signal.
    @memo Set Beam On (independent of ppg script)
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgBeamOn(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_BEAM_CTL);
  *spec_Adr = 0x1; /* channel 14 (beam on/off) set to ON; ignores ppg pulseblaster script */
  return;
}


/*------------------------------------------------------------------*/
/** ppgBeamOff
    Directly set Beam Off signal.
    @memo Set Beam Off (independent of PPG script)
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgBeamOff(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_BEAM_CTL);
  *spec_Adr = 0x0; /* channel 14 (beam on/off) set to OFF; ignores ppg pulseblaster script */
  return;
}

/*------------------------------------------------------------------*/
/** ppgBeamCtlPPG
    PPG controls the Beam On/Off signal.
    @memo Give PPG script control of Beam On/Off signal (ch14)
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgBeamCtlPPG(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_BEAM_CTL);
  *spec_Adr = 0x3; /* channel 14 (beam on/off) follows ppg pulseblaster script */ 
  return;
}

/*------------------------------------------------------------------*/
/** ppgPolzCtlVME
    VME controls the Pol On/Off signal for helicity
    @memo Give VME control of Pol On/Off signal (DRV POL) (PPG script ignored) 
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgPolzCtlVME(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr +  POLZ_SOURCE_CONTROL);
  *spec_Adr = 0x0; /* DRV POL (helicity) independent of ppg pulseblaster script */ 
  return;
}
/*------------------------------------------------------------------*/
/** ppgPolzCtlPPG
    PPG controls the Pol On/Off signal for helicity
    @memo Give PPG script control of Pol On/Off signal (DRV POL) which now follows ch15  
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgPolzCtlPPG(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr +  POLZ_SOURCE_CONTROL);
  *spec_Adr = 0x2; /* DRV POL (helicity) follows ch15 and ppg pulseblaster script */ 
  return;
}
/*------------------------------------------------------------------*/
/** ppgBeamCtlRegRead
    Read the Beam Control Register
    @memo Read the Beam Control Register
    @param base\_adr PPG VME base address
    @return void
*/
INLINE BYTE ppgBeamCtlRegRead(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_BEAM_CTL);
  return *spec_Adr;
}

/*------------------------------------------------------------------*/
/** ppgStartSequencer
    Start the PPG sequencer (internal trigger)
    @memo start the PPG sequencer.
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgStartSequencer(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  ppgPolmskWrite(base_adr, DEFAULT_PPG_POL_MSK);
  spec_Adr = (BYTE *)(A16 + base_adr + PPG_START_TRIGGER);
  *spec_Adr = 0x0;
  return;
}

/*------------------------------------------------------------------*/
/** ppgStopSequencer
    Stop the PPG sequencer.
    @memo Stop the PPG sequencer.
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgStopSequencer(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_RESET);
  *spec_Adr = 0x0;
  return;
}


/*------------------------------------------------------------------*/
/**  ppgEnableExtTrig(ppg_base)
    Enable front panel trigger input so external inputs can start the sequence
    @memo Enable external trigger  input so external inputs can start the sequence
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void  ppgEnableExtTrig(const DWORD base_adr)
{

  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_TRIG_CTL);
  *spec_Adr = 0x0;
   return;
}



/*------------------------------------------------------------------*/
/**  ppgDisableExtTrig(ppg_base)
    Disable front panel trigger input so external inputs cannot start the sequence
    @memo Disable external trigger  input so external inputs cannot start the sequence
    @param base\_adr PPG VME base address
    @return void
*/
INLINE void ppgDisableExtTrig(const DWORD base_adr)
{

  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_TRIG_CTL);
  *spec_Adr = 0x1;
  return ;
}



/*------------------------------------------------------------------*/
/** ppgExtTrigRegRead
    Read external trig register (bit 0 int/ext trigger is enabled bit 1 trigger active/inactive)
    @memo Read PPG.
    @param base\_adr PPG VME base address
    @return value (8bit)
*/
INLINE BYTE  ppgExtTrigRegRead(const DWORD base_adr)
{
  volatile BYTE * spec_Adr;
  BYTE value;
  spec_Adr = (BYTE *)(A16 + base_adr + VME_TRIG_CTL);
  value = *spec_Adr;
  
  if (value & 1)
    printf("External Trigger is Disabled\n");
  else
    printf("External Trigger is Enabled\n");
  if (value & 2)
    printf("External Trigger is in Active State \n");
  else
    printf("External Trigger is in InActive State  \n");
  
  return *spec_Adr;
}

/*------------------------------------------------------------------*/
/** ppgPolmskRead
    Read the Polarity mask.
    @memo Read polarity mask.
    @param base\_adr PPG VME base address
    @return polarity (24bit)
*/
INLINE DWORD ppgPolmskRead(const DWORD base_adr)
{
  volatile BYTE * spec_Adr, temp;
  DWORD  pol;
  pol = 0;
  spec_Adr = (BYTE *)(A16 + base_adr + OUTP_POL_MASK_HI);
  temp = *spec_Adr;
  pol |= temp << 16;
  spec_Adr = (BYTE *)(A16 + base_adr + OUTP_POL_MASK_MID);
  temp = *spec_Adr;
  pol |= temp <<  8;
  spec_Adr = (BYTE *)(A16 + base_adr + OUTP_POL_MASK_LO);
  temp = *spec_Adr;
  pol |= temp;
  return pol;
}

/*------------------------------------------------------------------*/
/** ppg\_polmsk\_write
    Write the Polarity mask.
    @memo Write and read back polarity mask.
    @param base\_adr PPG VME base address
    @param pol polarity (24bit)
    @return polarity (24bit)
*/
INLINE DWORD ppgPolmskWrite(const DWORD base_adr, const DWORD pol)
{
  volatile BYTE * spec_Adr;
  spec_Adr = (BYTE *)(A16 + base_adr + OUTP_POL_MASK_HI);
  *spec_Adr  = *((BYTE *) &pol + 1);
  spec_Adr = (BYTE *)(A16 + base_adr + OUTP_POL_MASK_MID);
  *spec_Adr  = *((BYTE *) &pol + 2);
  spec_Adr = (BYTE *)(A16 + base_adr + OUTP_POL_MASK_LO);
  *spec_Adr  = *((BYTE *) &pol + 3);
  return ppgPolmskRead(base_adr);
}
/*------------------------------------------------------------------*/
/** ppg\_startpattern\_write
    Write the Start(Power up or Reset)Pattern.
    @memo Write and read back startup pattern.
    @param base\_adr PPG VME base address
    @param pol polarity (24bit)
    @return polarity (24bit)
*/
INLINE DWORD ppgStartpatternWrite(const DWORD base_adr, const DWORD pol)
{
  volatile BYTE * spec_Adr;
  ppgRegWrite(base_adr,PPG_RESET_REG,0);
  ppgRegWrite(base_adr,BYTES_PER_WORD,0x0A);
  ppgRegWrite(base_adr,TOGL_MEM_DEVICE,0);
  ppgRegWrite(base_adr,CLEAR_ADDR_COUNTER,0x55);
  ppgRegWrite(base_adr,PROGRAMMING_FIN,7);
  ppgRegWrite(base_adr,PPG_START_TRIGGER,7);
  ppgRegWrite(base_adr,PPG_RESET_REG,1);
  ppgRegWrite(base_adr,BYTES_PER_WORD,3);
  ppgRegWrite(base_adr,CLEAR_ADDR_COUNTER,0);
  ppgRegWrite(base_adr,LOAD_MEM,*((BYTE *) &pol + 1));
  ppgRegWrite(base_adr,LOAD_MEM,*((BYTE *) &pol + 2));
  ppgRegWrite(base_adr,LOAD_MEM,*((BYTE *) &pol + 3));
  ppgRegWrite(base_adr,0x05,0);
  ppgRegWrite(base_adr,0x05,0);
  return ppgPolmskRead(base_adr);
}
/*------------------------------------------------------------------*/
/** byteOutputOrder
    byte swap for output
    @memo byte swap for PPG.
    @param data 
    @param array 
    @return void
*/
INLINE void byteOutputOrder(PARAM data, char *array)
{
#define LOW_MASK 	0x000000FF
#define LOW_MID_MASK 	0x0000FF00
#define HIGH_MID_MASK 	0x00FF0000
#define HIGH_MASK 	0xFF000000
#define BRANCH1_MASK	0X000FF000
#define BRANCH2_MASK	0X00000FF0
#define BRANCH3_MASK	0X0000000F
  
  unsigned long total_bits;
  unsigned int *test;
  
  /*total_bits = data.opcode_width + data.branch_width + data.delay_width  */
  /*			 + data.flag_width;            */
  
  array[0] = (char)( (data.flags & HIGH_MID_MASK) >> 16);
  array[1] = (char)( (data.flags & LOW_MID_MASK) >> 8);
  array[2] = (char)( (data.flags & LOW_MASK));
  
  if (ddd) printf("branch = %lx\n",data.branch_addr);

  array[3] = (char)( (data.branch_addr & BRANCH1_MASK) >> 12);
  array[4] = (char)( (data.branch_addr & BRANCH2_MASK) >> 4);
  array[5] = (char)((data.branch_addr & BRANCH3_MASK) << 4);
  array[5] = array[5] | (char)(data.opcode & BRANCH3_MASK);
  
  array[6] = (char)( (data.delay & HIGH_MASK) >> 24);
  array[7] = (char)( (data.delay & HIGH_MID_MASK) >> 16);
  array[8] = (char)( (data.delay & LOW_MID_MASK) >> 8);
  array[9] = (char)( (data.delay & LOW_MASK));
}

/*------------------------------------------------------------------*/
/** lineRead
    Read line of input file
    @memo read line.
    @param *input FILE pointer
    @return PARAM data structure
*/
INLINE PARAM lineRead(FILE *input)
{
  PARAM data_struct;
  unsigned int temp;
  
  fscanf(input,"%x",&temp);
  data_struct.opcode = (char)temp;
  
  fscanf(input,"%lx",&(data_struct.branch_addr));
  fscanf(input,"%lx",&(data_struct.delay));
  fscanf(input,"%lx",&(data_struct.flags));
  return(data_struct);
}

/*------------------------------------------------------------------*/
/** ppgLoad
    Load PPG file into sequencer.
    @memo Load file PPG.
    @param base\_adr PPG VME base address
    @return 1=SUCCESS, -1=file not found
*/
INLINE INT ppgLoad(const DWORD base_adr, char *file)
{
  /*  Local Variables  */
  long    j,i;
  int 	index = 0;
  char 	text[100];
  char  num_instructions;
  char  flag_size;
  char  delay_size;
  char  branch_size;
  char  opcode_size;
  unsigned char array[12];
  PARAM  command_info;
  unsigned int 	temp;
  unsigned char result;
  FILE  *ppginput;
  unsigned short port_address;
  
  printf("Opening file: %s   ...  ",file);
  ppginput = fopen(file,"r");
  ss_sleep(50);
  if(ppginput == NULL){
    printf("ppgLoad: Byte code file %s could not be opened. [%x]\n", file, ppginput);
    printf("  If file is present, problem may be too many rpc processes.  Reboot ppc and retry\n");
    return -1;
  }
  
  fscanf(ppginput,"%s",text);
  index = strcmp(text,"Op_Code");
  if(index == 0){
    fscanf(ppginput,"%s",text);
    index = strcmp(text,"Size");
    if(index == 0){
      fscanf(ppginput,"%s",text);
    }
    else {
      printf("ppgLoad: Input file has wrong format. Aborting procedure.\n");
      return -1;
    }
  }
  opcode_size = (char)atol(text);
  
  fscanf(ppginput,"%s",text);
  index = strcmp(text,"Branch");
  if(index == 0){
    fscanf(ppginput,"%s",text);
    index = strcmp(text,"Size");
    if(index == 0){
      fscanf(ppginput,"%s",text);
    }
    else {
      printf("ppgLoad: Input file has wrong format. Aborting procedure.\n");
      return -1;
    }
  }
  branch_size = (char)atol(text);
  
  fscanf(ppginput,"%s",text);
  index = strcmp(text,"Delay");
  if(index == 0){
    fscanf(ppginput,"%s",text);
    index = strcmp(text,"Size");
    if(index == 0){
      fscanf(ppginput,"%s",text);
    }
    else {
      printf("ppgLoad: Input file has wrong format. Aborting procedure.\n");
      return -1;
    }
  }
  delay_size = (char)atol(text);
  
  fscanf(ppginput,"%s",text);
  index = strcmp(text,"Flag");
  if(index == 0){
    fscanf(ppginput,"%s",text);
    index = strcmp(text,"Size");
    if(index == 0){
      fscanf(ppginput,"%s",text);
    }
    else {
      printf("ppgLoad: Input file has wrong format. Aborting procedure.\n");
      return -1;
    }
  }
  flag_size = (char)atol(text);
  
  fscanf(ppginput,"%s",text);
  index = strcmp(text,"Instruction");
  if(index == 0){
    fscanf(ppginput,"%s",text);
    index = strcmp(text,"Lines");
    if(index == 0){
      fscanf(ppginput,"%s",text);
    }
    else {
      printf("ppgLoad: Input file has wrong format. Aborting procedure.\n");
      return -1;
    }
  }
  num_instructions = (char)atol(text);
  if (ddd) printf("instruction lines = %d\n",num_instructions);
  
  fscanf(ppginput,"%s",text);
  index = strcmp(text,"Port");
  if(index == 0){
    fscanf(ppginput,"%s",text);
    index = strcmp(text,"Address");
    if(index == 0){
      fscanf(ppginput,"%x",&port_address);
    }
    else {
      printf("ppgLoad: Input file has wrong format. Aborting procedure.\n");
      return -1;
    }
  }
  if (ddd) printf("Base Address = %x\n",base_adr);
  
  if (ddd) printf("Reading byte code file...\n");
  
  
  command_info.opcode_width = opcode_size;
  command_info.branch_width = branch_size;
  command_info.delay_width = delay_size;
  command_info.flag_width = flag_size;
  
  /* init ISA BUS Controller  */
  ppgRegWrite(base_adr,PPG_RESET_REG,0x00);
  ppgRegWrite(base_adr,BYTES_PER_WORD,0x0A);
  ppgRegWrite(base_adr,TOGL_MEM_DEVICE,0x00);      /*Is this still active?*/
  ppgRegWrite(base_adr,CLEAR_ADDR_COUNTER,0x00);
  
  for(i=0;i<num_instructions;i++){
    command_info = lineRead(ppginput);
    if (ddd)
      {
	printf("%1x ",command_info.opcode);
	printf("%5lx ",command_info.branch_addr);
	printf("%8lx ",command_info.delay);
	printf("%6lx ",command_info.flags);
	printf("\n");
      }
    byteOutputOrder(command_info,array);
    
    for(j=0;j<10;j++){
      result = array[j];
      temp = (unsigned int) result;
      if (ddd) printf("%x ",temp);
    }
    if (ddd) printf("\n");
    
    for(j=0;j<10;j++)
      {
	ppgRegWrite(base_adr,LOAD_MEM,array[j]);
      }
  }
  
  /* arm controller        */
  ppgRegWrite(base_adr,LOAD_MEM,0x00);   /*writes the stop command */
  ppgRegWrite(base_adr,LOAD_MEM,0x00);
  ppgRegWrite(base_adr,LOAD_MEM,0x00);
  ppgRegWrite(base_adr,LOAD_MEM,0x00);
  ppgRegWrite(base_adr,LOAD_MEM,0x00);
  ppgRegWrite(base_adr,LOAD_MEM,0x01);
  ppgRegWrite(base_adr,LOAD_MEM,0x00);
  ppgRegWrite(base_adr,LOAD_MEM,0x00);
  ppgRegWrite(base_adr,LOAD_MEM,0x00);
  ppgRegWrite(base_adr,LOAD_MEM,0x04);   /* Need 4 clock cycles for stop cmd */
  
  
  ppgRegWrite(base_adr,PROGRAMMING_FIN,0x00);
  fclose(ppginput);
  if(ddd) printf("Programming ended, controller armed");
  return 1;
}











