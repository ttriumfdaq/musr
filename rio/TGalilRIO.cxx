#include "TGalilRIO.h"
#include <iostream>
#include <string.h>
#include <stdio.h>

TGalilRIO::TGalilRIO(std::string ip){

  std::cout << "Connecting to Galil device at IP " << ip << std::endl;
  fDataSocket = new KOsocket(ip.c_str(),23);
  fDataSocket->setSoTimeout(3000);
  
  fDataSocket->write("ID;",3);
  
  char tempbuffer[20000];
  int bufferSize = sizeof(tempbuffer);
  int bytes = 
    fDataSocket->read(tempbuffer, bufferSize); 
  
  if(bytes < 1 || (strchr(tempbuffer, ':') == NULL) || tempbuffer[0] == '?'){
    std::cerr << "Error talking to Galil-RIO. nbytes = "
	      << bytes << ", response = " << tempbuffer[0] << std::endl;
    exit(0);
  }

  std::cout << "Galil RIO describes itself as :" << std::endl;
  for(int i = 0; i < bytes-2; i++)
    std::cout << tempbuffer[i];
  std::cout << std::endl;
  
}
  
float TGalilRIO::GetAnalogInput(int ch){

  if(ch < 0 || ch > 7){
    std::cerr << "Invalid.  Only channels between 0-7; not ch = "
	      << ch << std::endl;  
    return -999.0;
  }
  char command[100];
  sprintf(command,"MG @AN[%i];",ch);

  fDataSocket->write(command,10);
  
  char tempbuffer[20000];
  int bufferSize = sizeof(tempbuffer);
  int bytes = 
    fDataSocket->read(tempbuffer, bufferSize); 
  
  if(bytes < 1 || (strchr(tempbuffer, ':') == NULL) || tempbuffer[0] == '?'){
    std::cerr << "Error on Galil-RIO analog read. nbytes = "
	      << bytes << ", response = " << tempbuffer[0] << std::endl;
    exit(0);
  }

  // I hate string manipulation in C/C++
  tempbuffer[bytes-2] = NULL; // Get rid of last two char = '\n:'
  std::string tmpstr(tempbuffer);
  std::string tmpstr2 = tmpstr.substr(1); // Get rid of first char (blank)
  float value = atof(tmpstr2.c_str());
  return value;

}

void  TGalilRIO::SetAnalogOutput(int ch, float value){

  if(ch < 0 || ch > 7){
    std::cerr << "Invalid.  Only channels between 0-7; not ch = "
	      << ch << std::endl;  
    return;
  }
  char command[100];
  sprintf(command,"AO %i,%f;",ch,value);
  int size = strlen(command);
  fDataSocket->write(command,size);
  
  char tempbuffer[20000];
  int bufferSize = sizeof(tempbuffer);
  int bytes = 
    fDataSocket->read(tempbuffer, bufferSize); 
  
  if(bytes < 1 || (strchr(tempbuffer, ':') == NULL) || tempbuffer[0] == '?'){
    std::cerr << "Error on Galil-RIO analog write. nbytes = "
	      << bytes << ", response = " << tempbuffer[0] << std::endl;
    exit(0);
  }


}
