#ifndef TGalilRIO_H
#define TGalilRIO_H

#include "KOsocket.h"

// A class for talking to Galil 47120 using TCP socket
// and setting DAC and reading ADCs.
class TGalilRIO {

  

 public:

  // Create the TGalilRIO and connect to the device.
  TGalilRIO(std::string ip);

  // Get a reading from one of the analog inputs.
  float GetAnalogInput(int ch);

  // Set the value for one of the analog outputs.
  void SetAnalogOutput(int ch, float value);

  ~TGalilRIO(){delete fDataSocket;};
  
 private:
  
  KOsocket *fDataSocket;


};

#endif
