/*-----------------------------------------------------------------------------
 *  Copyright (c) 1996      TRIUMF Data Acquistion Group
 *  Please leave this header in any reproduction of that distribution
 * 
 *  TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 *  Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *         amaudruz@triumf.ca
 * -----------------------------------------------------------------------------
 *  
 *  Description	: Support function library for the SIS3801 Multi scalers
 *                P.A. Amaudruz
 *  Requires 	: sis3801.h
 *  Application : VME
 *  Author      : Pierre-Andre Amaudruz Data Acquisition Group
    $Log: sis3801.c,v $
    Revision 1.3  2001/03/30 20:14:20  suz
    correct counter in HFIFO_read to MAX_FIFO_SIZE

    Revision 1.2  2000/11/03 22:45:08  midas
    remove sis3801_setup (no longer used)

 *
 *---------------------------------------------------------------------------*/
#include "sis3801.h"

void myStub_sis3801(void)
{
  static int sis3801_private_counter=0;
#ifdef OS_VXWORKS
  sis3801_int_source_disable(0,0);
  logMsg ("sis3801 IRQ#%i served\n",sis3801_private_counter++,0,0,0,0,0);
  sis3801_int_source_enable(0,0);
#endif
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_module_ID(const DWORD base_adr)
/**************************************************************\
 Purpose: return the Module ID number (I) version (V)
          IRQ level (L) IRQ vector# (BB)
 Input: 
  DWORD base_addr     : base address of the SIS3801
 Output:
    none
 Function value:
 DWORD                : 0xIIIIVLBB
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + MODULE_ID);
  return *spec_adr;
}

/************ INLINE function for General command ***********/
INLINE void sis3801_module_reset(const DWORD base_adr)
/**************************************************************\
 Purpose: General Module reset
 Input: 
  DWORD base_addr     : base address of the SIS3801
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + MODULE_RESET);
  *spec_adr = 0x0;
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_IRQ_REG_read(const DWORD base_adr)
/**************************************************************\
 Purpose: return the lower 12 bits corresponding to
          VME IRQ enable, levels, vector(lower 8bits)
 Input: 
  DWORD base_addr     : base address of the SIS3801
 Output:
    none
 Function value:
 DWORD                : 0xE(1)L(3)V(8)
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + IRQ_REG);
  return (*spec_adr & 0xFFF);
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_IRQ_REG_write(const DWORD base_adr, DWORD irq)
/**************************************************************\
 Purpose: write irq (ELV) to the register and read back 
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD irq           : reg 
 Output:
    none
 Function value:
 DWORD                : 0xE(1)L(3)V(8)
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + IRQ_REG);
  *spec_adr = irq;
  return (*spec_adr & 0xFFF);
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_input_mode(const DWORD base_adr, DWORD mode)
/**************************************************************\
 Purpose: Set input configuration mode
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD mode          : Mode 0-4
 Output:
    none
 Function value:
    DWORD             : current mode
\**************************************************************/
{
  volatile DWORD * spec_adr;
  
  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_WRITE);
  if (mode < 4)
    mode <<= 2;
  *spec_adr = mode;
  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_READ);
  return ((*spec_adr & GET_MODE) >> 2);
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_dwell_time(const DWORD  base_adr, DWORD dwell)
/**************************************************************\
 Purpose: Set dwell time in us
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD dwell         : dwell time in microseconds
 Output:
    none
 Function value:
    DWORD             : readback dwell time register
\**************************************************************/
{
  volatile DWORD * spec_adr, prescale;

  spec_adr = (DWORD *)(A32D24 + base_adr + PRESCALE_REG);
  prescale = (10 * dwell ) - 1;
  if ((prescale > 0) && (prescale < 2<<24))
    *spec_adr = prescale;
  return (*spec_adr);
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_ref1(const DWORD base_adr, DWORD endis)
/**************************************************************\
 Purpose: Enable/Disable Reference on Channel 1
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD endis         : action either ENABLE_REF_CH1
                                      DISABLE_REF_CH1
 Output:
    none
 Function value:
               : status of the ref1 1=enable, 0=disable
\**************************************************************/
{
  volatile DWORD * spec_adr;

  if ((endis == ENABLE_REF_CH1) || (endis == DISABLE_REF_CH1))
  {
    spec_adr = (DWORD *)(A32D24 + base_adr + endis);
    *spec_adr = 0x0;
  }
  else
    printf("sis3801_ref1: unknown command %d\n",endis);
 
  /* read back the status */
  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_READ);
  return ((*spec_adr & IS_REF1) ? 1 : 0);
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_next_logic(const DWORD base_adr, DWORD endis)
/**************************************************************\
 Purpose: Enable/Disable the next Logic signal
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD endis         : action either ENABLE_NEXT_CLK
                                      DISABLE_NEXT_CLK
 Output:
    none
 Function value:
 int        : status of the Next Logic  1=enable, 0=disable
\**************************************************************/
{
  volatile DWORD * spec_adr;

  if ((endis == ENABLE_NEXT_CLK) || (endis == DISABLE_NEXT_CLK))
  {
    spec_adr = (DWORD *)(A32D24 + base_adr + endis);
    *spec_adr = 0x0;
  }
  else
    printf("sis3801_next_logic: unknown command %d\n",endis);

  /* read back the status */
  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_READ);
  return ((*spec_adr & IS_NEXT_LOGIC_ENABLE) ? 1 : 0);
}

/************ INLINE function for General command ***********/
INLINE void sis3801_channel_enable(const DWORD base_adr, DWORD nch)
/**************************************************************\
 Purpose: Enable nch channel for acquistion.
          blind command! 1..24 or 32
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD nch           : number of channel to enable
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  volatile DWORD * spec_adr;
  spec_adr = (DWORD *)(A32D24 + base_adr + COPY_REG);
  if (nch > 24) nch = 32;
  *spec_adr = (1<<nch);
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_CSR_read(const DWORD  base_adr, const DWORD what)
/**************************************************************\
 Purpose: Read the CSR and return 1/0 based on what.
          except for what == CSR_FULL where it returns current CSR
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD what          : bitwise field (see sis3801.h)
 Output:
    none
 Function value:
 DWORD             : CSR 32bit current content
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_READ);
  if (what == CSR_FULL)
    return *spec_adr;
  if (what == GET_MODE)
    return ((*spec_adr & what) >> 2);
  return ((*spec_adr & what) ? 1 : 0);
}

/************ INLINE function for General command ***********/
INLINE DWORD sis3801_CSR_write(const DWORD  base_adr, const DWORD what)
/**************************************************************\
 Purpose: Write to the CSR and return CSR_FULL.
 Input: 
  DWORD base_addr     : base address of the SIS3801
  DWORD what          : bitwise field 
 Output:
    none
 Function value:
 DWORD             : CSR 32bit current content
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_WRITE);
  *spec_adr = what; 
  return sis3801_CSR_read(base_adr, CSR_FULL);
}

/************ INLINE function for General command ***********/
INLINE void sis3801_FIFO_clear(const DWORD base_adr)
/**************************************************************\
 Purpose: Clear FIFO and logic
 Input:
  DWORD base_addr     : base address of the SIS3801
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + FIFO_CLEAR);
  *spec_adr = 0x0;
}

/************ INLINE function for General command ***********/
INLINE int sis3801_HFIFO_read(const DWORD  base_adr, DWORD * pfifo)
/**************************************************************\
 Purpose: Reads 32KB (8K DWORD) of the FIFO
 Input:
  DWORD base_addr     : base address of the SIS3801
  DWORD *pfifo        : destination pointer
 Output:
    none
 Function value:
  int                 : -1 FULL
                         0 NOT 1/2 Full
			 number of words read
\**************************************************************/
{
  volatile DWORD * spec_adr;
  register int i;

  if (sis3801_CSR_read(base_adr, IS_FIFO_FULL))
    return -1;
  if (sis3801_CSR_read(base_adr, IS_FIFO_HALF_FULL) == 0)
    return 0;

  spec_adr = (DWORD *)(A32D24 + base_adr + FIFO_READ);
  for (i=0;i<HALF_FIFO;i++)
    *pfifo++ = *spec_adr;    

  return HALF_FIFO;
}

/************ INLINE function for General command ***********/
INLINE int sis3801_FIFO_flush(const DWORD  base_adr, DWORD * pfifo)
/**************************************************************\
 Purpose: Test and read FIFO until empty
          This is done using while, if the dwelling time is short
	  relative to the readout time, the pfifo can be overrun.
	  No test on the max read yet!
 Input:
  DWORD base_addr     : base address of the SIS3801
  DWORD *pfifo        : destination pointer
 Output:
    none
 Function value:
  int                 : -1 FULL
			 number of words read
\**************************************************************/
{
  volatile DWORD * spec_adr, * spec_adr_csr;
  register int counter=0;

  if (sis3801_CSR_read(base_adr, IS_FIFO_FULL))
    return -1;

  /* setup a fast CSR read */
  spec_adr_csr = (DWORD *)(A32D24 + base_adr + CSR_READ);

  /* setup FIFO address */
  spec_adr = (DWORD *)(A32D24 + base_adr + FIFO_READ);

/*  while (((*spec_adr_csr & IS_FIFO_EMPTY) == 0) && (counter < 0x4000))*/
  while (((*spec_adr_csr & IS_FIFO_EMPTY) == 0) && (counter < MAX_FIFO_SIZE))
  {
    counter++;
    *pfifo++ = *spec_adr;    
  }
  return counter;
}

/************ INLINE function for General command ***********/
INLINE void sis3801_int_source_enable (const DWORD base_adr, const int intnum)
/**************************************************************\
 Purpose: Enable the interrupt for the bitwise input intnum (0xff).
          The interrupt vector is then the VECTOR_BASE
 Input:
  DWORD * base_addr       : base address of the sis3801
  DWORD * intnum          : interrupt number (input 0:7)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  volatile DWORD * spec_adr;
  DWORD int_source;
  
  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_WRITE);
  switch (intnum)
  {
  case  0:
    int_source = ENABLE_IRQ_EN_CIP;
    break;
  case  1:
    int_source = ENABLE_IRQ_EN_FULL;
    break;
  case  2:
    int_source = ENABLE_IRQ_EN_HFULL;
    break;
  case  3:
    int_source = ENABLE_IRQ_EN_ALFULL;
    break;
  default:
    logMsg("Unknown interrupt source (%d)\n",int_source,0,0,0,0,0);
  }
  *spec_adr = int_source;
}

/************ INLINE function for General command ***********/
INLINE void sis3801_int_source_disable (const DWORD base_adr, const int intnum)
/**************************************************************\
 Purpose: Disable the interrupt for the bitwise input intnum (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:
  DWORD * base_addr       : base address of the sis3801
  int     level           : IRQ level (1..7)
  int   * intnum          : interrupt number (input 0..3)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  volatile DWORD * spec_adr;
  DWORD int_source;

  
  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_WRITE);
  switch (intnum)
  {
  case  0:
    int_source = DISABLE_IRQ_DI_CIP;
    break;
  case  1:
    int_source = DISABLE_IRQ_DI_FULL;
    break;
  case  2:
    int_source = DISABLE_IRQ_DI_HFULL;
    break;
  case  3:
    int_source = DISABLE_IRQ_DI_ALFULL;
    break;
  default:
    logMsg("Unknown interrupt source (%d)\n",int_source,0,0,0,0,0);
  }
  *spec_adr = int_source;
}

/************ INLINE function for General command ***********/
INLINE void sis3801_int_source (const DWORD base_adr, DWORD int_source)
/**************************************************************\
 Purpose: Enable the one of the 4 interrupt using the
          predefined parameters (see sis3801.h)
 Input:
  DWORD * base_addr       : base address of the sis3801
  DWORD * intnum          : interrupt number (input 0..3)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  volatile DWORD * spec_adr;

  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_WRITE);

  int_source &= (ENABLE_IRQ_EN_CIP   | ENABLE_IRQ_EN_FULL
	        |ENABLE_IRQ_EN_HFULL | ENABLE_IRQ_EN_ALFULL
		|DISABLE_IRQ_DI_CIP  | DISABLE_IRQ_DI_FULL
	        |DISABLE_IRQ_DI_HFULL| DISABLE_IRQ_DI_ALFULL);
  *spec_adr = int_source;
}

/************ INLINE function for General command ***********/
INLINE void sis3801_int_attach (const DWORD base_adr, DWORD base_vect, int level, void (*isr)(void))
/**************************************************************\
 Purpose: Book an ISR for a bitwise set of interrupt input (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:
  DWORD * base_addr      : base address of the sis3801
  DWORD base_vect        : base vector of the module
  int   level            : IRQ level (1..7)
  DWORD isr_routine      : interrupt routine pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  volatile DWORD * spec_adr;

  /* disable all IRQ sources */
  sis3801_int_source(base_adr
			     , DISABLE_IRQ_DI_CIP
			     | DISABLE_IRQ_DI_FULL
			     | DISABLE_IRQ_DI_HFULL
			     | DISABLE_IRQ_DI_ALFULL);
  
#ifdef OS_VXWORKS
  if ((level < 8) && (level > 0) && (base_vect < 0x100))
  {
    spec_adr = (DWORD *)(A32D24 + base_adr + IRQ_REG);
    *spec_adr = (level << 8) | VME_IRQ_ENABLE | base_vect;
    sysIntEnable(level);	        /* interrupt level */
  }
  intConnect(INUM_TO_IVEC (base_vect),(VOIDFUNCPTR) isr, 0); 
  printf("attach :%p level:%x vect:%x\n", isr, level, base_vect);
#else
  printf("Not implemented for this OS\n");
#endif
}

/************ INLINE function for General command ***********/
INLINE void sis3801_int_detach (const DWORD base_adr, DWORD base_vect, int level)
/**************************************************************\
 Purpose: Unbook an ISR for a bitwise set of interrupt input (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:
  DWORD * base_addr       : base address of the sis3801
  DWORD base_vect        : base vector of the module
  int   level            : IRQ level (1..7)
 Output:
    none
 Function value:
    none
\**************************************************************/
{

  /* disable all IRQ sources */
  sis3801_int_source(base_adr   , DISABLE_IRQ_DI_CIP
		    | DISABLE_IRQ_DI_FULL
		    | DISABLE_IRQ_DI_HFULL
		    | DISABLE_IRQ_DI_ALFULL);
   
#ifdef OS_VXWORKS
  intConnect(INUM_TO_IVEC (base_vect),(VOIDFUNCPTR) myStub_sis3801, 0); 
  sysIntDisable(level);	        /* interrupt level */
#else
  printf("vector : 0x%x\n",base_vect+intnum);
#endif
}

/************ INLINE function for General command ***********/
INLINE void sis3801_int_clear (const DWORD base_adr, const int intnum)
/**************************************************************\
 Purpose: Disable the interrupt for the bitwise input intnum (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:
  DWORD * base_addr       : base address of the sis3801
  int   * intnum          : interrupt number (input 0..3)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  volatile DWORD * spec_adr;
  DWORD int_source;

  spec_adr = (DWORD *)(A32D24 + base_adr + CSR_WRITE);
  switch (intnum)
  {
  case  0:
    int_source = DISABLE_IRQ_DI_CIP;
    *spec_adr = int_source;
    int_source = ENABLE_IRQ_EN_CIP;
    *spec_adr = int_source;
    break;
  case  1:
    int_source = DISABLE_IRQ_DI_FULL;
    *spec_adr = int_source;
    int_source = ENABLE_IRQ_EN_FULL;
    *spec_adr = int_source;
    break;
  case  2:
    int_source = DISABLE_IRQ_DI_HFULL;
    *spec_adr = int_source;
    int_source = ENABLE_IRQ_EN_HFULL;
    *spec_adr = int_source;
    break;
  case  3:
    int_source = DISABLE_IRQ_DI_ALFULL;
    *spec_adr = int_source;
    int_source = ENABLE_IRQ_EN_ALFULL;
    *spec_adr = int_source;
    break;
  default:
    logMsg("Unknown interrupt source (%d)\n",int_source,0,0,0,0,0);
  }
}


/************ INLINE function for General command ***********/
void sis3801(void)
/**************************************************************\
 Purpose: Display SIS3801 build-in commands
 Input:
    none
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  printf("\n---> SIS 3801 32 MultiScalers (sis3801.c) <---\n");
  printf("Inline : sis3801_module_ID (vmeBase);\n");
  printf("Inline : sis3801_IRQ_REG_read(vmeBase);\n");
  printf("Inline : sis3801_IRQ_REG_write(vmeBase, irq);\n");
  printf("Inline : sis3801_module_reset (vmeBase);\n");
  printf("Inline : sis3801_input_mode (vmeBase, mode);\n");
  printf("Inline : sis3801_dwell_time (vmeBase, dwell);\n");
  printf("Inline : sis3801_ref1 (vmeBase, enable/disable);\n");
  printf("Inline : sis3801_channel_enable (vmeBase, #channel);\n");
  printf("Inline : sis3801_CSR_read (vmeBase, what);\n");
  printf("Inline : sis3801_CSR_write (vmeBase, what);\n");
  printf("Inline : sis3801_FIFO_clear (vmeBase);\n");
  printf("Inline : sis3801_FIFO_read(vmeBase, pdata);\n");
  printf("Inline : sis3801_FIFO_flush (vmeBase, pdata);\n");
  printf("Inline : sis3801_int_source (vmeBase, enable/disable);\n");
  printf("Inline : sis3801_int_source_enable (vmeBase, source (0..3));\n");
  printf("Inline : sis3801_int_source_disable (vmeBase, source (0..3));\n");
  printf("Inline : sis3801_int_attach (vmeBase, vect, level, void (*isr)(void));\n");
  printf("Inline : sis3801_int_detach (vmeBase, vect, level);\n");
  printf("Inline : sis3801_int_clear (vmeBase, source (0..3));\n");
  printf("calls  : SIS3801_CSR_read( base_adr)\n");
 }

/**************************************************************/

void SIS3801_CSR_read(const DWORD base_adr)
{
  DWORD csr;
  
  csr = sis3801_CSR_read(base_adr,CSR_FULL);

  printf("Module Version   : %d\t", ((sis3801_module_ID (base_adr) & 0xf000) >> 12));
  printf("Module ID        : %4.4x\t", (sis3801_module_ID (base_adr) >> 16));
  printf("CSR contents     : 0x%8.8x\n", csr);
  printf("LED              : %s \t",(csr &     IS_LED) ? "Y" : "N");
  printf("FIFO test mode   : %s \t",(csr &        0x2) ? "Y" : "N");
  printf("Input mode       : %d \n",sis3801_input_mode(base_adr, 2));
  printf("25MHz test pulse : %s \t",(csr &   IS_25MHZ) ? "Y" : "N");
  printf("Input test mode  : %s \t",(csr &    IS_TEST) ? "Y" : "N");
  printf("10MHz to LNE     : %s \t",(csr &  IS_102LNE) ? "Y" : "N");
  printf("LNE prescale     : %s \n",(csr &     IS_LNE) ? "Y" : "N");
  printf("Reference pulse 1: %s \t",(csr &    IS_REF1) ? "Y" : "N");
  printf("Next Logic       : %s \n",(csr & IS_NEXT_LOGIC_ENABLE) ? "Y" : "N");
  printf("FIFO empty       : %s \t",(csr & IS_FIFO_EMPTY ) ? "Y" : "N");
  printf("FIFO almost empty: %s \t",(csr & IS_FIFO_ALMOST_EMPTY) ? "Y" : "N");
  printf("FIFO half full   : %s \t",(csr & IS_FIFO_HALF_FULL) ? "Y" : "N");
  printf("FIFO full        : %s \n",(csr & IS_FIFO_FULL) ? "Y" : "N");
  printf("External next    : %s \t",(csr & IS_EXTERN_NEXT) ? "Y" : "N");
  printf("External clear   : %s \t",(csr & IS_EXTERN_CLEAR) ? "Y" : "N");
  printf("External disable : %s \t",(csr & IS_EXTERN_DISABLE) ? "Y" : "N");
  printf("Software couting : %s \n",(csr & IS_SOFT_COUNTING) ? "N" : "Y");
  printf("IRQ enable CIP   : %s \t",(csr & IS_IRQ_EN_CIP) ? "Y" : "N");
  printf("IRQ enable FULL  : %s \t",(csr & IS_IRQ_EN_FULL) ? "Y" : "N");
  printf("IRQ enable HFULL : %s \t",(csr & IS_IRQ_EN_HFULL) ? "Y" : "N");
  printf("IRQ enable ALFULL: %s \n",(csr & IS_IRQ_EN_ALFULL) ? "Y" : "N");
  printf("IRQ CIP          : %s \t",(csr & IS_IRQ_CIP) ? "Y" : "N");
  printf("IRQ FIFO full    : %s \t",(csr & IS_IRQ_FULL) ? "Y" : "N");
  printf("IRQ FIFO 1/2 full: %s \t",(csr & IS_IRQ_HFULL) ? "Y" : "N");
  printf("IRQ FIFO almost F: %s \n",(csr & IS_IRQ_ALFULL) ? "Y" : "N");
  printf("internal VME IRQ : %s \t",(csr &  0x4000000) ? "Y" : "N");
  printf("VME IRQ          : %s \n",(csr &  0x8000000) ? "Y" : "N");
}

DWORD * p=NULL;

void test(DWORD mem)
{
  PART_ID part_id;

  if (p != NULL) free (p);
  (char *)p = malloc(mem);
}


