/*-----------------------------------------------------------------------------
 * Copyright (c) 1996      TRIUMF Data Acquistion Group
 * Please leave this header in any reproduction of that distribution
 * 
 * TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 * Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *        amaudruz@triumf.ca
 * ----------------------------------------------------------------------------
 *  
 * Description	: Header file for SIS3801 Multiscalers
 *
 * Author:  Pierre-Andre Amaudruz Data Acquisition Group
   $Log: sis3801.h,v $
   Revision 1.2  2001/02/12 18:13:43  suz
   Correct bug in size of HALF_FIFO & defin,e MAX an d SIS FIFO size in terms of HALF_FIFO

   Revision 1.1.1.1  2000/10/10 21:13:14  midas
   originals

 *---------------------------------------------------------------------------*/
#ifndef SIS3801_INCLUDE
#define SIS3801_INCLUDE

#include "stdio.h"
#include "string.h"

#ifdef OS_VXWORKS
#include "vme.h"
#include "vxWorks.h"
#include "intLib.h"
#include "sys/fcntlcom.h"
#ifdef PPCxxx
#include "arch/ppc/ivPpc.h"
#else
#include "arch/mc68k/ivMc68k.h"
#endif
#include "taskLib.h"
#endif

#if defined( _MSC_VER )
#define INLINE __inline
#elif defined(__GNUC__)
#define INLINE __inline__
#else 
#define INLINE
#endif

#define EXTERNAL extern 

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED

typedef unsigned short int WORD;

#ifdef __alpha
typedef unsigned int       DWORD;
#else
typedef unsigned long int  DWORD;
#endif

#endif /* MIDAS_TYPE_DEFINED */

#ifdef PPCxxx
#define A32D24	       0xfa000000              /* A32D24 camac base address */
#else
#define A32D24	       0xf0000000              /* A32D24 camac base address */
#endif

#define HALF_FIFO        16384        /* FIFO 64Kw -> 32Kw 1/2 FIFO
					2words/Data -> 16KData */ 
#define MAX_FIFO_SIZE    2*HALF_FIFO  /* FIFO size in words (16 bits) */
#define SIS_FIFO_SIZE    4*HALF_FIFO  /* FIFO size in bytes */
#define CSR_READ         0x0
#define CSR_FULL         0xffffffff  /* CSR Read */
#define CSR_WRITE        0x0

#define IS_LED           0x00000001  /* CSR read */
#define LED_ON           0x00000001  /* CSR write */
#define LED_OFF          0x00000100  /* CSR write */
#define GET_MODE         0x0000000C  /* CSR read */
#define MODE_0           0x00000C00  /* CSR write */
#define MODE_1           0x00000804  /* CSR write */
#define MODE_2           0x00000408  /* CSR write */
#define MODE_3           0x0000000C  /* CSR write */
#define IS_25MHZ         0x00000010  /* CSR read */
#define ENABLE_25MHZ     0x00000010  /* CSR write */
#define DISABLE_25MHZ    0x00001000  /* CSR write */
#define IS_TEST          0x00000020  /* CSR write */
#define ENABLE_TEST      0x00000020  /* CSR write */
#define DISABLE_TEST     0x00002000  /* CSR write */

#define IS_102LNE        0x00000040  /* CSR read */
#define ENABLE_102LNE    0x00000040  /* CSR write */
#define DISABLE_102LNE   0x00000400  /* CSR write */
#define IS_LNE           0x00000080  /* CSR read */
#define ENABLE_LNE       0x00000080  /* CSR write */
#define DISABLE_LNE      0x00000800  /* CSR write */
#define IS_FIFO_EMPTY          0x00000100  /* CSR Read */
#define IS_FIFO_ALMOST_EMPTY   0x00000200  /* CSR Read */
#define IS_FIFO_HALF_FULL      0x00000400  /* CSR Read */
#define IS_FIFO_FULL           0x00001000  /* CSR Read */
#define IS_REF1                0x00002000  /* CSR read */

#define IS_NEXT_LOGIC_ENABLE   0x00008000  /* CSR read */
#define IS_EXTERN_NEXT         0x00010000
#define IS_EXTERN_CLEAR        0x00020000
#define IS_EXTERN_DISABLE      0x00040000
#define IS_SOFT_COUNTING       0x00080000
#define ENABLE_EXTERN_NEXT     0x00010000
#define ENABLE_EXTERN_CLEAR    0x00020000
#define ENABLE_EXTERN_DISABLE  0x00040000
#define SET_SOFT_DISABLE       0x00080000
#define DISABLE_EXTERN_NEXT    0x01000000
#define DISABLE_EXTERN_CLEAR   0x02000000
#define DISABLE_EXTERN_DISABLE 0x04000000
#define CLEAR_SOFT_DISABLE     0x08000000

#define IS_IRQ_EN_CIP          0x00100000  /* CSR read */
#define IS_IRQ_EN_FULL         0x00200000  /* CSR read */
#define IS_IRQ_EN_HFULL        0x00400000  /* CSR read */
#define IS_IRQ_EN_ALFULL       0x00800000  /* CSR read */
#define IS_IRQ_CIP             0x10000000  /* CSR read */
#define IS_IRQ_FULL            0x20000000  /* CSR read */
#define IS_IRQ_HFULL           0x40000000  /* CSR read */
#define IS_IRQ_ALFULL          0x80000000  /* CSR read */
#define ENABLE_IRQ_EN_CIP      0x00100000  /* CSR write */
#define ENABLE_IRQ_EN_FULL     0x00200000  /* CSR write */
#define ENABLE_IRQ_EN_HFULL    0x00400000  /* CSR write */
#define ENABLE_IRQ_EN_ALFULL   0x00800000  /* CSR write */
#define DISABLE_IRQ_DI_CIP     0x10000000  /* CSR write */
#define DISABLE_IRQ_DI_FULL    0x20000000  /* CSR write */
#define DISABLE_IRQ_DI_HFULL   0x40000000  /* CSR write */
#define DISABLE_IRQ_DI_ALFULL  0x80000000  /* CSR write */
#define VME_IRQ_ENABLE         0x00000800  /* CSR write */

#define MODULE_ID        0x004
#define IRQ_REG          0x004
#define COPY_REG         0x00C
#define FIFO_WRITE       0x010
#define FIFO_CLEAR       0x020
#define VME_NEXT_CLK     0x024
#define ENABLE_NEXT_CLK  0x028
#define DISABLE_NEXT_CLK 0x02C
#define ENABLE_REF_CH1   0x050
#define DISABLE_REF_CH1  0x054
#define MODULE_RESET     0x060
#define SINGLE_TST_PULSE 0x068
#define PRESCALE_REG     0x080
#define FIFO_READ        0x100

#define SOURCE_CIP           0
#define SOURCE_FIFO_FULL     1
#define SOURCE_FIFO_HFULL    2
#define SOURCE_FIFO_ALFULL   3

#define SIS3801_VECT_BASE 0x7f

INLINE DWORD sis3801_module_ID(const DWORD b);
INLINE void  sis3801_module_reset(const DWORD b);
INLINE DWORD sis3801_IRQ_REG_read(const DWORD base_adr);
INLINE DWORD sis3801_IRQ_REG_write(const DWORD base_adr, DWORD irq);
INLINE DWORD sis3801_module_mode(const DWORD b, DWORD mode);
INLINE DWORD sis3801_dwell_time(const DWORD b, DWORD dwell);
INLINE DWORD sis3801_ref1(const DWORD b, DWORD endis);
INLINE DWORD sis3801_next_logic(const DWORD b, DWORD endis);
INLINE void  sis3801_channel_enable(const DWORD b, DWORD nch);
INLINE DWORD sis3801_CSR_read(const DWORD b, const DWORD what);
INLINE DWORD sis3801_CSR_write(const DWORD b, const DWORD what);
INLINE void  sis3801_FIFO_clear(const DWORD b);
INLINE int   sis3801_HFIFO_read(const DWORD b, DWORD *p);
INLINE int   sis3801_FIFO_flush(const DWORD b, DWORD *p);
INLINE void  sis3801_int_source (const DWORD base_adr, DWORD int_source);
INLINE void  sis3801_int_source_enable (const DWORD base_adr, const int intnum);
INLINE void  sis3801_int_source_disable (const DWORD base_adr, const int intnum);
INLINE void  sis3801_int_clear (const DWORD base_adr, const int intnum);
INLINE void  sis3801_int_attach (const DWORD base_adr, DWORD base_vect, int level, void (*isr)(void));
INLINE void  sis3801_int_detach (const DWORD base_adr, DWORD base_vect, int level);
void         sis3801_setup(const DWORD base_adr, int mode, int dsp);
void         myStub_sis3801(void);

void SIS3801_setup(const DWORD base_adr, int mode);
void SIS3801_CSR_read(const DWORD base_adr);

#endif
