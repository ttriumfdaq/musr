/*-----------------------------------------------------------------------------
 *  Copyright (c) 1996      TRIUMF Data Acquistion Group
 *  Please leave this header in any reproduction of that distribution
 * 
 *  TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 *  Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *         amaudruz@triumf.ca
 * -----------------------------------------------------------------------------
 *  
 *  Description	: Support function library for the SIS3803 200MHz 16ch. scalers
 *                P.A. Amaudruz
 *  Requires 	: sis3803.h
 *  Application : VME
 *  Author      : Pierre-Andre Amaudruz Data Acquisition Group
 *
 * gcc -g -O2 -Wall -g -DMAIN_ENABLE -I/home1/midas/midas/include
 *   -o sis3801 sis3801.c vmicvme.o -lm -lz -lutil -lnsl -lpthread -L/lib
 *   -lvme
 *
 *   $Id: sis3803.c,v 1.3 2015/03/19 22:38:00 suz Exp $
 *
 * Test modes:  (added/modified by SD)
 *
 * 1. 25MHz reference pulses go into all enabled channels
 * ------------------------------------------------------
 *    sis3803_test_mode_enable   enable test mode
 *    sis3803_25MHz_enable       enable 25MHz pulser
 *
 * 2. Single pulse into all enabled channels
 * ------------------------------------------
 *   sis3803_test_mode_enable   enable test mode,
 *   sis3803_test_pulse         successive writes generate single pulses
 *
 *    NOTE : disable 25MHz pulser
 *
 * Reference pulse into channel 1 (in NORMAL mode)
 * ------------------------------------------------
 *   sis3803_25MHz_enable       enable 25MHz ref pulser
 *   sis3803_ref1               enable Ref 1
 *
 *    Note: Disable test mode
 *
 *---------------------------------------------------------------------------
 * vme_peek -a VME_A24UD -d VME_D32 -A 0x383804    to read Module ID
 * VME_PEEK: vme_addr=0x383804 - am=0x39 - dw=0x4 - size=0x4 - phys_addr=0x0 - WPA=0xb383804
 *  38031000   <- Module ID 
 *---------------------------------------------------------------------------*/  


#include "sis3803.h"

#include <stdio.h>
#include <string.h>
#if defined(OS_LINUX)
#include <unistd.h>
#endif

int myinfo = VME_INTERRUPT_SIGEVENT;
extern INT_INFO int_info;

#ifdef MAIN_ENABLE
// For VMIC processor
#include "vmicvme.h"
#endif





/************  Function for General command ***********/
/*------------------------------------------------------------------*/
/** sis3803RegRead
    Read 32bit from register.
    @memo 32bit read.
    @param base\_adr SIS3803 VME base address
    @param reg\_offset register offset
    @return read back value
*/
DWORD sis3803RegRead(MVME_INTERFACE *mvme, DWORD base, DWORD reg_offset)
{
  DWORD data;
  
  mvme_set_am(mvme, MVME_AM_A24_ND); // A24
  mvme_set_dmode(mvme, MVME_DMODE_D32); // D32

  data = mvme_read_value(mvme, base + reg_offset);

  return data; 

}
/*------------------------------------------------------------------*/
/** sis3803RegWrite
    Write 32bit to register.
    @memo 32bit write.
    @param base\_adr sis3803 VME base address
    @param reg\_offset register offset
    @param value to be written 
    @return read back value
*/
 DWORD sis3803RegWrite(MVME_INTERFACE *mvme, DWORD base, const DWORD reg_offset,
                           const DWORD value)
{
  DWORD data;
  
  mvme_set_am(mvme, MVME_AM_A24_ND); // A24
  mvme_set_dmode(mvme, MVME_DMODE_D32); // D32
  
  mvme_write_value(mvme, base + reg_offset, value);
  data =  mvme_read_value(mvme, base + reg_offset);
  return data; 
}


 DWORD sis3803_module_ID(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: return the Module ID number (I) version (V)
          IRQ level (L) IRQ vector# (BB)
 Input: 
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base     : base address of the SIS3803
 Output:
    none
 Function value:
 DWORD                : 0xIIIIVLBB
\**************************************************************/
{
  DWORD id;

  id = sis3803RegRead(mvme, base,  SIS3803_MODULE_ID_RO);
  return (id);
}

/************  Function for General command ***********/
 void sis3803_module_reset(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: General Module reset
 Input:
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  mvme_set_am(mvme, MVME_AM_A24_ND); // A24
  mvme_set_dmode(mvme, MVME_DMODE_D32); // D32

  sis3803RegWrite(mvme, base, SIS3803_MODULE_RESET_WO, 0x0);
  return;
}

/************  Function for General command ***********/
 DWORD sis3803_IRQ_REG_read(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: return the lower 12 bits corresponding to
          VME IRQ enable, levels, vector(lower 8bits)
 Input: 
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 DWORD                : 0xE(1)L(3)V(8)
\**************************************************************/
{
  DWORD reg;

  reg = sis3803RegRead(mvme, base, SIS3803_IRQ_REG_RW );
  return (reg & 0xFFF);
}

/************  Function for General command ***********/
 DWORD sis3803_IRQ_REG_write(MVME_INTERFACE *mvme, DWORD base, DWORD vector)
/**************************************************************\
 Purpose: write irq (ELV) to the register and read back 
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic 
  DWORD base_addr     : base address of the SIS3803
  DWORD vector        : irq
 Output:
    readback vector
 Function value:
 DWORD                : 0xE(1)L(3)V(8)
\**************************************************************/
{
   
  DWORD reg;

  reg = sis3803RegWrite(mvme, base,  SIS3803_IRQ_REG_RW,  (vector & 0xFF));
 
  return (reg & 0xFFF);
}

/************  Function for General command ***********/
 DWORD sis3803_input_mode(MVME_INTERFACE *mvme, DWORD base, DWORD mode)
/**************************************************************\
 Purpose: Set input configuration mode (only modes 0,1 available)
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic 
  DWORD base_addr     : base address of the SIS3803
  DWORD mode          : Mode 0-1
 Output:
   read back of mode
 Function value:
    DWORD             : current mode
\**************************************************************/
{
  DWORD rmode;

  if (mode < 2)
    mode <<= 2;

  rmode = sis3803RegWrite(mvme, base,   SIS3803_CSR_RW, mode);

  return ((rmode & GET_MODE) >> 2);
}

/************  Function for General command ***********/
 DWORD sis3803_ref1(MVME_INTERFACE *mvme, DWORD base, DWORD endis)
/**************************************************************\
 Purpose: Enable/Disable Reference on Channel 1
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic 
  DWORD base_addr     : base address of the SIS3803
  DWORD endis         : action either SIS3803_ENABLE_REF_CH1_WO
                                      SIS3803_DISABLE_REF_CH1_WO
 Output:
    none
 Function value:
               : status of the ref1 1=enable, 0=disable
\**************************************************************/
{
  DWORD csr;


  mvme_set_am(mvme, MVME_AM_A24_ND); // A24
  mvme_set_dmode(mvme, MVME_DMODE_D32); // D32

  if ((endis == SIS3803_ENABLE_REF_CH1_WO) || (endis == SIS3803_DISABLE_REF_CH1_WO))
    {
      csr = sis3803RegWrite(mvme, base,  endis,  0x0);
    }
  else
    {
      printf("sis3801_ref1: unknown command %ld\n",endis);
      return 9; // error
    }

  return ((csr & IS_REF1) ? 1 : 0);

}

/************  Function for General command ***********/
 void sis3803_channel_enable(MVME_INTERFACE *mvme, DWORD base, DWORD enable_pat)
/**************************************************************\
 Purpose: Enable pat channels for acquisition
          blind command! 0x0..0xffff

 Note: for sis3803 this is a count DISABLE register.
       This function has enable_pat as the channels to be ENABLED, so pattern will be reversed.
       SIS3803 has 16 channels

 Input:  
  MVME_INTERFACE *mvme : pointer to vmic 
  DWORD base_addr     : base address of the SIS3803
  DWORD enable_pat    : enable channel pattern
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  DWORD pat = ~enable_pat & 0xffff;  // 16 channels max

  sis3803RegWrite(mvme, base, SIS3803_SELECT_COUNT_DIS_REG_WO, pat);
  return;
}


/************  Function for General command ***********/
 void sis3803_channel_disable(MVME_INTERFACE *mvme, DWORD base, DWORD disable_pat)
/**************************************************************\
 Purpose: Disable pat channel for acquisition.
          blind command! 0x0..0xffff
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic 
  DWORD base_addr     : base address of the SIS3803
  DWORD disable_pat   : disable channel pattern
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  DWORD pat = disable_pat & 0xffff;  // 16 channels max

  sis3803RegWrite(mvme, base,  SIS3803_SELECT_COUNT_DIS_REG_WO, pat );
  return;
}


/************  Function for General command ***********/
 DWORD sis3803_CSR_read(MVME_INTERFACE *mvme, DWORD base, const DWORD what)
/**************************************************************\
 Purpose: Read the CSR and return 1/0 based on what.
          except for what == CSR_FULL where it returns current CSR
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic 
  DWORD base_addr     : base address of the SIS3803
  DWORD what          : bitwise field (see sis3803.h)
 Output:
    none
 Function value:
 DWORD             : CSR 32bit current content
\**************************************************************/
{
  DWORD csr;


  if (what == CSR_FULL)
    {
      csr = sis3803RegRead(mvme, base,  SIS3803_CSR_RW);
    }
  else if (what == GET_MODE) 
    {
      csr = sis3803RegRead(mvme, base,  SIS3803_CSR_RW);
      csr = ((csr & what) >> 2);
    } 
  else
    {
      csr = sis3803RegRead(mvme, base,  SIS3803_CSR_RW);
      csr = ((csr & what) ? 1 : 0);
    }
  
  return (csr);
}


/************  Function for General command ***********/
DWORD sis3803_CSR_write(MVME_INTERFACE *mvme, DWORD base, const DWORD what)
/**************************************************************\
 Purpose: Write to the CSR and return CSR_FULL.
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic 
  DWORD base_addr     : base address of the SIS3803
  DWORD what          : bitwise field 
 Output:
    none
 Function value:
 DWORD             : CSR 32bit current content
\**************************************************************/
{
  int csr;

  sis3803RegWrite(mvme, base,  SIS3803_CSR_RW, what);
  csr =  sis3803_CSR_read(mvme, base, CSR_FULL);
  return (csr);
}


/************  Function for General command ***********/
 void sis3803_all_clear(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Clear all module + overflow bits
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,   SIS3803_ALL_CLEAR_WO, 0x0); 
  return;
}


/************  Function for General command ***********/
 void sis3803_test_pulse(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: generate a single test pulse into enabled channels
  NOTE: first call  sis3803_test_mode_enable(base) to enable test mode
        do not enable 25MHz test pulses
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,  SIS3803_SINGLE_TST_PULSE_WO, 0);
  return;
}


/************  Function for General command ***********/
 void sis3803_test_mode_enable(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Enable input test mode
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,  SIS3803_CSR_RW, ENABLE_TEST_MODE );
  return;
}


/************  Function for General command ***********/
 void sis3803_25MHz_enable(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Enable 25MHz test pulses
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,  SIS3803_CSR_RW,  ENABLE_25MHZ_PULSES );
  return;
}


/************  Function for General command ***********/
 void sis3803_test_mode_disable(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Disable input test mode
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,  SIS3803_CSR_RW,   DISABLE_TEST_MODE );
  return;
}

/************  Function for General command ***********/
 void sis3803_25MHz_disable(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Disable 25MHz test pulses
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base, SIS3803_CSR_RW,  DISABLE_25MHZ_PULSES );
  return;
}


/************  Function for General command ***********/
 void sis3803_all_enable(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Enable global counting
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,    SIS3803_ENABLE_GBL_COUNT_WO ,  0 );
  return;
}


/************  Function for General command ***********/
 void sis3803_all_disable(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Disable global counting
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,   SIS3803_DISABLE_GBL_COUNT_WO ,  0 );
  return;
}


/************  Function for General command ***********/
 void sis3803_grp1_clear(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Clear group 1 (1-8) channels + overflow bits
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base, SIS3803_COUNTER_GRP1_CLEAR_WO ,  0 );
  return;
}


/************  Function for General command ***********/
 void sis3803_grp2_clear(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: Clear group 2 (9-16) channels + overflow bits
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
  sis3803RegWrite(mvme, base,  SIS3803_COUNTER_GRP2_CLEAR_WO ,  0 );
  return;
}

/************  Function for General command ***********/
 void sis3803_single_clear(MVME_INTERFACE *mvme, DWORD base, const INT ch)
/**************************************************************\
 Purpose: Clear single scaler channel + overflow bits
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
  INT ch              : channel to clear
 Output:
    none
 Function value:
 none
\**************************************************************/
 {
   if (ch < 16) {
     sis3803RegWrite(mvme, base,  SIS3803_COUNTER_CLEAR_WO + (ch<<2)  ,  0 );
   }
   return;
 }

/************  Function for General command ***********/
 void sis3803_single_OVFL_clear(MVME_INTERFACE *mvme, DWORD base, INT ch)
/**************************************************************\
 Purpose: Clear  overflow bit of channel N
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    none
 Function value:
 none
\**************************************************************/
{
 
  if (ch < 16) {
    sis3803RegWrite(mvme, base,   SIS3803_OVERFLOW_CLEAR_WO + (ch<<2) ,  0 );
  }
  return;
}

/***********  Function for General command ***********/
 void sis3803_counter_read(MVME_INTERFACE *mvme, DWORD base, INT ch, DWORD * pvalue)
/**************************************************************\
 Purpose: Reads one individual scaler (DOES NOT CLEAR)
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
  INT   ch            : Channel to be read
  DWORD *pvalue       : destination pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{

  if (ch < 16) {
    *pvalue = sis3803RegRead(mvme, base,  SIS3803_COUNTER_READ_RO + (ch<<2) );
  }
  return;
}

/***********  Function for General command ***********/
 void sis3803_all_read(MVME_INTERFACE *mvme, DWORD base, DWORD ** pvalue)
/**************************************************************\
 Purpose: Reads all 16 channels scaler
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
  DWORD *pvalue       : destination pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  INT i;
 
 

  for (i=0;i<16;i++)
    *(*pvalue)++ = sis3803RegRead(mvme, base,  SIS3803_COUNTER_READ_RO );

  return;
}




/***********  Function for General command ***********/
 void sis3803_all_read1(MVME_INTERFACE *mvme, DWORD base, DWORD ** pvalue, const DWORD num_ch)
/**************************************************************\
 Purpose: Reads all 16 channels scaler
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
  DWORD *pvalue       : destination pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  INT   i;
  
  for (i=0;i<num_ch;i++)
    *(*pvalue)++ = sis3803RegRead(mvme, base,  SIS3803_COUNTER_READ_RO );

  return;
}

/***********  Function for General command ***********/
 void sis3803_grp1_read(MVME_INTERFACE *mvme, DWORD base, DWORD ** pvalue)
/**************************************************************\
 Purpose: Reads group 1 (1..8) channels scaler
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
  DWORD *pvalue       : destination pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  INT   i;

 
 
  
  for (i=0;i<8;i++)
    *(*pvalue)++ = sis3803RegRead(mvme, base,  SIS3803_COUNTER_READ_RO );

  return;
}


/***********  Function for General command ***********/
 void sis3803_grp2_read(MVME_INTERFACE *mvme, DWORD base, DWORD ** pvalue)
/**************************************************************\
 Purpose: Reads group 2 (9..16) channels scaler
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
  DWORD *pvalue       : destination pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  
 
 

    *(*pvalue)++ = sis3803RegRead(mvme, base,  SIS3803_COUNTER_READ_RO );

  return;
}

/***********  Function for General command ***********/
 void sis3803_all_read_clear(MVME_INTERFACE *mvme, DWORD base, DWORD ** pvalue)
/**************************************************************\
 Purpose: Reads & clear all 16 channels scaler
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
  DWORD *pvalue       : destination pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  INT   i;
  
 
 

  for (i=0;i<16;i++)
    *(*pvalue)++ = sis3803RegRead(mvme, base,  SIS3803_COUNTER_READ_CLEAR_RO );

  return;
}


/************  Function for General command ***********/
 void sis3803_OVFL_grp1_read(MVME_INTERFACE *mvme, DWORD base, DWORD *reg)
/**************************************************************\
 Purpose: Read Overflow reg for group 1 (1-8) 
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    reg               : pointer to overflow reg 
 Function value:
 none
\**************************************************************/
{
  
  *reg = sis3803RegRead(mvme, base,  SIS3803_OVERFLOW_REG1_8_RO );
  *reg >>= 24;

  return;
}

/************  Function for General command ***********/
 void sis3803_OVFL_grp2_read(MVME_INTERFACE *mvme, DWORD base, DWORD *reg)
/**************************************************************\
 Purpose: Read Overflow reg for group 2 (9-16) 
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD base_addr     : base address of the SIS3803
 Output:
    reg               : pointer to overflow reg 
 Function value:
 none
\**************************************************************/
{
 
 

  *reg = sis3803RegRead(mvme, base,  SIS3803_OVERFLOW_REG9_16_RO );
  *reg >>= 24;

  return;
}


/************  Function for General command ***********/
 void sis3803_int_source_enable (MVME_INTERFACE *mvme, DWORD base, const int intnum)
/**************************************************************\
 Purpose: Enable the interrupt for the bitwise input intnum (0xff).
          The interrupt vector is then the VECTOR_BASE
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD * base_addr       : base address of the sis3803
  DWORD * intnum          : interrupt number (input 0:7)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  DWORD int_source=0;


  switch (intnum)
  {
  case  0:
    int_source = ENABLE_IRQ_EN_BS0;
    break;
  case  1:
    int_source = ENABLE_IRQ_EN_BS1;
    break;
  case  2:
    int_source = ENABLE_IRQ_EN_BS2;
    break;
  default:
    //logMsg("Unknown interrupt source (%d)\n",int_source,0,0,0,0,0);
    printf("Unknown interrupt source (%ld)\n",int_source);
    return;
  }
 
 

  sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);

  return;
}


/************  Function for General command ***********/
 void sis3803_int_source_disable (MVME_INTERFACE *mvme, DWORD base, const int intnum)
/**************************************************************\
 Purpose: Disable the interrupt for the bitwise input intnum (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD * base_addr       : base address of the sis3803
  int     level           : IRQ level (1..7)
  int   * intnum          : interrupt number (input 0..3)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  DWORD int_source=0;


 
 

  switch (intnum)
  {
  case  0:
    int_source = DISABLE_IRQ_DI_BS0;
    break;
  case  1:
    int_source = DISABLE_IRQ_DI_BS1;
    break;
  case  2:
    int_source = DISABLE_IRQ_DI_BS2;
    break;
  default:
    // logMsg("Unknown interrupt source (%d)\n",int_source,0,0,0,0,0);
    printf("Unknown interrupt source (%ld)\n",int_source);
  }
  sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);
  return;
}


/************  Function for General command ***********/
void sis3803_int_source (MVME_INTERFACE *mvme, DWORD base, DWORD int_source)
/**************************************************************\
 Purpose: Enable the one of the 4 interrupt using the
          predefined parameters (see sis3803.h)
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD * base_addr       : base address of the sis3803
  DWORD * intnum          : interrupt number (input 0..3)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
 
 
 


  int_source &= (ENABLE_IRQ_EN_BS0   | ENABLE_IRQ_EN_BS1
	        |ENABLE_IRQ_EN_BS2   
		|DISABLE_IRQ_DI_BS0  | DISABLE_IRQ_DI_BS1
	        |DISABLE_IRQ_DI_BS2);

  sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);
 
  return;
}  

/************  Function for General command ***********/
 void sis3803_int_attach (MVME_INTERFACE *mvme, DWORD base, DWORD base_vect, int level, void (*isr)(void))
/**************************************************************\
 Purpose: Book an ISR for a bitwise set of interrupt input (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD * base_addr      : base address of the sis3803
  DWORD base_vect        : base vector of the module
  int   level            : IRQ level (1..7)
  DWORD isr_routine      : interrupt routine pointer
 Output:
    none
 Function value:
    none
\**************************************************************/
{
 
  /* disable all IRQ sources */

  /* this code copied from sis3801.c  */
  sis3803_int_source(mvme, base
			     , DISABLE_IRQ_DI_BS0
			     | DISABLE_IRQ_DI_BS1
			     | DISABLE_IRQ_DI_BS2);

     if ((level < 8) && (level > 0) && (base_vect < 0x100)) {

      
      

       sis3803RegWrite(mvme, base, SIS3803_IRQ_REG_RW, (level << 8) | VME_IRQ_ENABLE | base_vect);
       mvme_interrupt_attach(mvme, level, base_vect, (void *)isr, &myinfo);
     }
     return;
}



/************  Function for General command ***********/
 void sis3803_int_detach (MVME_INTERFACE *mvme, DWORD base, DWORD base_vect, int level)
/**************************************************************\
 Purpose: Unbook an ISR for a bitwise set of interrupt input (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD * base       : base address of the sis3803
  DWORD base_vect        : base vector of the module
  int   level            : IRQ level (1..7)
 Output:
    none
 Function value:
    none
\**************************************************************/
{

  /* disable all IRQ sources */
  //Should be done but not in detach 
  sis3803_int_source(mvme, base   , DISABLE_IRQ_DI_BS0
		    | DISABLE_IRQ_DI_BS1
		    | DISABLE_IRQ_DI_BS2);

   return;
}

/************  Function for General command ***********/
 void sis3803_int_clear (MVME_INTERFACE *mvme, DWORD base, const int intnum)
/**************************************************************\
 Purpose: Disable the interrupt for the bitwise input intnum (0xff).
          The interrupt vector is then the VECTOR_BASE+intnum
 Input:  
  MVME_INTERFACE *mvme : pointer to vmic
  DWORD * base_addr       : base address of the sis3803
  int   * intnum          : interrupt number (input 0..3)
 Output:
    none
 Function value:
    none
\**************************************************************/
{
  DWORD int_source=0;

  switch (intnum)
  {
  case  0:
    int_source = DISABLE_IRQ_DI_BS0;
    sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);
    int_source = ENABLE_IRQ_EN_BS0;
    sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);    
    break;
  case  1:
    int_source = DISABLE_IRQ_DI_BS1;
    sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);
    int_source = ENABLE_IRQ_EN_BS1;
    sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);
    break;
  case  2:
    int_source = DISABLE_IRQ_DI_BS2;
    sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);
    int_source = ENABLE_IRQ_EN_BS2;
    sis3803RegWrite(mvme, base, SIS3803_CSR_RW, int_source);
    break;
  default:
    //logMsg("Unknown interrupt source (%d)\n",int_source,0,0,0,0,0);
    printf("Unknown interrupt source (%ld)\n",int_source);
  }
}

/************  Function for General command ***********/
void sis3803_setup(MVME_INTERFACE *mvme, DWORD base, int mode, int dsp)
{
 
  switch(mode)
  {
  case 1 :
    /* */
    if (dsp)
    {
      printf("Setting          : %d \t", mode);
      printf("Info             : PAA/June 2000\n");
    }
    sis3803_module_reset(mvme, base);
    sis3803_input_mode(mvme, base, 1);            /* mode 1 ONLY */
    sis3803_all_enable(mvme, base);
    sis3803_channel_enable(mvme, base,0xf);          /* 4 channels */
    if (dsp) SIS3803_CSR_read(mvme, base);
    if (dsp) printf("# of active ch.  : 4\n");
    break;
  default :
    printf("Undefined mode %d (known mode :1, 2, 3, 4, 5)\n",mode);
    break;
  }
}


/**************************************************************/
void SIS3803_setup(MVME_INTERFACE *mvme, DWORD base, int mode)
{
  sis3803_setup(mvme, base, mode, 1);
}

void SIS3803_CSR_read(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD csr;
  
  csr = sis3803_CSR_read(mvme, base,  CSR_FULL);

  printf("Module Version   : %ld\t", ((sis3803_module_ID (mvme, base) & 0xf000) >> 12));
  printf("Module ID        : %4.4x\n",(unsigned int) (sis3803_module_ID (mvme, base) >> 16));
  printf("LED              : %s \t",(csr &     IS_LED) ? "Y" : "N");
  printf("Input mode       : %ld \n",sis3803_input_mode(mvme, base, 1));
  printf("Reference pulse 1: %s \t",(csr &    IS_REF1) ? "Y" : "N");
  printf("Global Enable    : %s \t",(csr & IS_GBL_ENABLE) ? "Y" : "N");
  printf("Global OVFL bit  : %s \n",(csr & IS_GBL_OVFL) ? "Y" : "N");
  printf("IRQ enable BS0   : %s \t",(csr & IS_IRQ_EN_BS0) ? "Y" : "N");
  printf("IRQ enable BS1   : %s \t",(csr & IS_IRQ_EN_BS1) ? "Y" : "N");
  printf("IRQ enable BS2   : %s \n",(csr & IS_IRQ_EN_BS2) ? "Y" : "N");
  printf("IRQ Bit Source 0 : %s \t",(csr & IS_IRQ_BS0) ? "Y" : "N");
  printf("IRQ Bit Source 1 : %s \t",(csr & IS_IRQ_BS1) ? "Y" : "N");
  printf("IRQ Bit Source 2 : %s \n",(csr & IS_IRQ_BS2) ? "Y" : "N");
  printf("internal VME IRQ : %s \t",(csr &  0x4000000) ? "Y" : "N");
  printf("VME IRQ          : %s \n",(csr &  0x8000000) ? "Y" : "N");
  printf("Test mode        : %s \t",(csr & IS_TEST_MODE) ? "Y" : "N");
  printf("25MHZ test pulses: %s \t",(csr & IS_25MHZ_PULSES) ? "Y" : "N");
  printf("CSR is           : %#X \n", (unsigned int)(csr &  0xffffffff));


}

void SIS3803_all_read(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD count;
  INT   i;
  
  for (i=0;i<16;i++) {
    sis3803_counter_read(mvme, base, i, &count);
    printf("Counter[%i] : %lu\n",i, count);
  }
}

void SIS3803_all_read_clear(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD count;
  INT   i;
  
  for (i=0;i<16;i++) {
    sis3803_counter_read(mvme, base, i, &count);
    sis3803_single_clear(mvme, base, i);
    printf("Counter[%i] : %lu\n",i, count);
  }
}

void SIS3803_all_bread(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD count[16];
  DWORD *pdata;
  INT   i;
  
  pdata = &count[0];
  sis3803_all_read(mvme, base, &pdata);
  for (i=0;i<16;i++) {
    printf("Counter[%i] : %lu 0x%lx\n",i, count[i], count[i]);
  }
}

void SIS3803_all_bcread(MVME_INTERFACE *mvme, DWORD base)
{
  DWORD count[16], *pdata;
  INT   i;
  
  pdata = &(count[0]);
  sis3803_all_read_clear(mvme, base, &pdata);
  for (i=0;i<16;i++) {
    printf("Counter[%i] : %lu\n",i, count[i]);
  }
}

#ifdef GONE
DWORD * p=NULL;

void test(DWORD mem)
{
  PART_ID part_id;

  if (p != NULL) free (p);
  (char *)p = malloc(mem);
}



/*  - the following copied from sis3801.c -  */
int intflag=0;

static void myisr(int sig, siginfo_t * siginfo, void *extra)
{

  //  fprintf(stderr, "interrupt: level:%d Vector:0x%x ...  "
  //          , int_info.level, siginfo->si_value.sival_int & 0xFF);

  if (intflag == 0) {
    intflag = 1;
    printf("interrupt: level:%d Vector:0x%x ...  "
     , int_info.level, siginfo->si_value.sival_int & 0xFF);
  }
}
#endif // GONE from SIS3801

/*****************************************************************/
/*-PAA- For test purpose only 
  
*/

#ifdef MAIN_ENABLE

#define IRQ_VECTOR_CIP        0x70
#define IRQ_LEVEL                5

int main (int argc, char* argv[]) {

 char cmd[]="hallo";
 int i,s;

  int status,imode;
  DWORD data;
  DWORD ival,jval;
  INT channel;
  INT group, offset;

  DWORD SIS3803_BASE = 0x383800;
  //  DWORD *pfifo=NULL, nwords;  not used

  MVME_INTERFACE *myvme;

  if (argc>1) {
    sscanf(argv[1],"%lx",&SIS3803_BASE);
  }

  // Test under vmic
  status = mvme_open(&myvme, 0);

  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);
  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  printf("\nSIS3803 Base Address: 0x%lx ; A24/D32\n",SIS3803_BASE);

  sis3803_module_reset(myvme, SIS3803_BASE);

   sis3803();

   while ( isalpha(cmd[0]) ||  isdigit(cmd[0])  )
    {
      printf("\nEnter command (A-Y) X to exit?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
         cmd[0]=toupper(cmd[0]);
      s=cmd[0];

      switch(s)
        {
        case ('A'):
           sis3803_module_reset(myvme, SIS3803_BASE);
          break;

        case ('B'):
          data = sis3803_module_ID (myvme,SIS3803_BASE ); 
          ival = data >> 16;
          jval = (data & 0xF000) >> 12;
          printf("SIS3803 Module ID : 0x%lx  Version : 0x%lx\n",ival,jval);
          break;  

        case ('C'):
	  sis3803_all_clear(myvme,SIS3803_BASE); // clear all module and overflow bits (void)
          break;

        case ('D'):
          printf("Enter BitPattern for DISABLED channels : 0x");
          scanf("%lx",&ival);
          sis3803_channel_disable(myvme, SIS3803_BASE, ival); // no readback
          break;


        case ('E'):
          data = sis3803_ref1 (myvme,SIS3803_BASE, SIS3803_ENABLE_REF_CH1_WO ); // Enable ref pulser ch 1
          printf("Wrote  0x%x; Read back : 0x%lx\n",SIS3803_ENABLE_REF_CH1_WO,data);
          if (data == 1)
	    printf("Reference pulser on Channel 1 is ENABLED\n");
	  else
	    printf("Reference pulser on Channel 1 is DISABLED\n");
          break;

        case ('F'):
          data = sis3803_ref1 (myvme,SIS3803_BASE, SIS3803_DISABLE_REF_CH1_WO ); // Disable ref pulser ch 1
          printf("Wrote  0x%x; Read back : 0x%lx\n", SIS3803_DISABLE_REF_CH1_WO, data);
          if (data == 1)
	    printf("Reference pulser on Channel 1 is ENABLED\n");
	  else
	    printf("Reference pulser on Channel 1 is DISABLED\n");
          break;



        case ('G'):
	  sis3803_test_pulse(myvme,SIS3803_BASE); // generate a test pulse onto enabled channels (void)
          break;

        case ('H'):
	  printf("LED Enter 1=ON or 0=OFF : ");
          scanf("%ld",&data);
          if(data > 0)
	    sis3803_CSR_write(myvme, SIS3803_BASE, LED_ON);
	  else
	    sis3803_CSR_write(myvme, SIS3803_BASE, LED_OFF);
          break;

        case ('I'):
	  sis3803_test_mode_enable(myvme,SIS3803_BASE); //  Enable input test mode
          break;

        case ('J'):
	  sis3803_test_mode_disable(myvme,SIS3803_BASE); //  Disable input test mode
          break;

        case ('K'):
	  sis3803_25MHz_enable (myvme,SIS3803_BASE); //  Enable 25MHz  test pulses
          break;

        case ('L'):
	  sis3803_25MHz_disable (myvme,SIS3803_BASE); //  Disable 25MHz  test pulses
          break;

        case ('M'):
	  sis3803_all_enable (myvme,SIS3803_BASE); //   Enable global counting
          break;

        case ('N'):
	  sis3803_all_disable (myvme,SIS3803_BASE); //   Disable global counting
          break;

        case ('O'):
	  printf("Enter Group to clear (overflow reg) : ");
          scanf("%d",&group);
	  if(group == 1)
	    sis3803_grp1_clear (myvme,SIS3803_BASE); //  Clear group 1 (1-8) channels + overflow bits
	  else if (group == 2)
	    sis3803_grp2_clear (myvme,SIS3803_BASE); //  Clear group  2 (9-16) channels + overflow bits
	  else
	    printf("Illegal group (%d). Group can be 1 or 2\n",group);
          break;

        case ('Q'):
	 
	  printf("Enter Group of overflow register to read : ");
          scanf("%d",&group);
	  if(group == 1)
	      sis3803_OVFL_grp1_read(myvme,SIS3803_BASE , &data);

	  else if (group == 2)
	    sis3803_OVFL_grp2_read(myvme,SIS3803_BASE ,  &data);
	  else
	    {
	      printf("Illegal group (%d). Group can be 1 or 2\n",group);
	      break;
	    }
	  
	  // data contains upper 8 bits of the register (already shifted)
	  printf("Upper 8 bits of Group %d overflow Register reads 0x%x\n",group,(unsigned int)data);
	  offset = (group * 8) -8; 
	  for (i=0; i<8; i++)
	    {
	      if(data & 1<<i)
		printf("Channel %d has overflowed\n",(i+1+offset));
	    }
	  break;

        case ('R'):
	  printf("Enter channel to clear (including Overflow bit) (0-15) : ");
          scanf("%d",&channel);
	  sis3803_single_clear (myvme,SIS3803_BASE,channel); //  Clear single channels + overflow bit
          break;

        case ('T'):
	  printf("Enter channel whose Overflow bit is to be cleared (0-15) : ");
          scanf("%d",&channel);
	  sis3803_single_OVFL_clear(myvme,SIS3803_BASE,channel); //  Clear  overflow bit of channel N
          break;


        case ('U'):
	  printf("Enter channel to read (does not clear) (0-15) : ");
          scanf("%d",&channel);
	  sis3803_counter_read(myvme,SIS3803_BASE,channel, &data); //   Reads one individual scaler (DOES NOT CLEAR)
          printf("Data read : 0x%lx\n",data);
          break;

        case ('V'):
	  SIS3803_all_read(myvme,SIS3803_BASE); //   Reads & displays all scalers (DOES NOT CLEAR)
          break;

        case ('W'):
	  SIS3803_all_read_clear(myvme,SIS3803_BASE); //   Reads, Clears & displays all scalers 
          break;

        case ('Y'):

	  printf("\n Mode 1 : cycle LEDs     Mode  2 : single pulse test\n");
	  printf(  " Mode 3 : 25MHz test     Mode  4 : real with Ref pulse ch 1\n");
	  printf("Enter test mode  (1-4) : ");
          scanf("%d",&imode);
          SIS3803_TEST(myvme,SIS3803_BASE,imode);  // Test 1 : cycle LEDs  
                                                   // Test 2 : single pulse test mode
                                                   // Test 3 : 25MHz test mode
                                                   // Test 4 : real with Ref pulse ch 1
	  break;

        case ('S'):
	  SIS3803_CSR_read(myvme, SIS3803_BASE); // status
          break;

        case ('P'):
	  sis3803(); // print command list
          break;

	case ('X'):
	  status = mvme_close(myvme);
          return 1;

	case ('a'):
	  break;

	case ('b'):
	  break;

	default:
	  printf("Unknown command %s .\n",&cmd[0]);
          break;


	}
    }

  status = mvme_close(myvme);
  return 1;
}



void SIS3803_TEST(MVME_INTERFACE *mvme, DWORD base, INT itest)
{
  // Select test mode
  //        itest= 1 : cycle LEDs  
  //               2 : single pulse test mode
  //               3 : 25MHZ Pulser test mode
  //               4 : real mode with ref pulse ch 1 enabled
  
  INT i;
  
  if(itest == 1)
    {
      printf("\nTest Mode 1 : cycle LEDs\n");
      printf("ID:%lx\n", sis3803_module_ID(mvme, base));
      SIS3803_CSR_read(mvme, base); // status
      
      for(i=0;i<4;i++) {
	sis3803_CSR_write(mvme, base, LED_ON);
	sleep(5);
	sis3803_CSR_write(mvme, base, LED_OFF);
	sleep(5);
      }
    }

  else if (itest == 2)
    {
      printf("\n\nTest Mode 2 : Single Pulse Test mode\n");
      // reset
      sis3803_module_reset(mvme, base);
      sleep(2); // sleep 2s

      // disable no channels
      sis3803_channel_disable(mvme, base, 0); // all channels enabled
      
      
      
      sis3803_test_mode_enable (mvme, base); //  Enable 25MHz  test pulses
      sis3803_all_enable (mvme, base); // enable global counting
      sis3803_test_pulse (mvme, base);// generate a single test pulse onto enabled channels (void)

      printf("CSR:0x%lx\n", sis3803_CSR_read(mvme, base, CSR_FULL));
      SIS3803_CSR_read(mvme, base); // status

      printf("Reading all scalers (no clear)\n");
      for(i=0;i<4;i++) {
	sleep(5);
	SIS3803_all_read (mvme, base);//   Reads & displays all scalers (DOES NOT CLEAR)
      }

      printf("\nReading all scalers (with clear)\n");
      for(i=0;i<4;i++) {
	sleep(5);
	SIS3803_all_read_clear (mvme, base);//   Reads & displays all scalers (DOES NOT CLEAR)
      }


    }
  else if (itest == 3)
    {
          printf("\n\nTest Mode 3 : test mode with 25MHz Pulser\n");
      // reset
      sis3803_module_reset(mvme, base);
      sleep(2);
      // disable no channels
      sis3803_channel_disable(mvme, base, 0); // all channels enabled
      
      sis3803_test_mode_enable (mvme, base); //  Enable 25MHz  test pulses
      sis3803_all_enable (mvme, base); // enable global counting
      sis3803_test_pulse (mvme, base);// generate a test pulse onto enabled channels (void)

      
      printf("CSR:0x%lx\n", sis3803_CSR_read(mvme, base, CSR_FULL));
      SIS3803_CSR_read(mvme, base); // status
      
      printf("\nReading all scalers (no clear)\n");
      for(i=0;i<4;i++) {
	sleep(5);
	SIS3803_all_read (mvme, base);
      }
      printf("\nReading all scalers (with clear)\n");
      for(i=0;i<4;i++) {
	sleep(5);
	SIS3803_all_read_clear (mvme, base);
      }
    }
  else if (itest == 4)
    {
      printf("\n\nTest Mode 4 : set into real mode and enable Ch 1 Ref pulser\n");
      // reset
      sis3803_module_reset(mvme, base);
      // disable no channels
      sis3803_channel_disable(mvme, base, 0); // all channels enabled
      // Use ch 1 as reference
      sis3803_ref1(mvme, base, SIS3803_ENABLE_REF_CH1_WO);
      
      // Set input mode to mode 1
      sis3803_input_mode(mvme, base, 1); // Mode 1
      
      sis3803_all_enable (mvme, base); // enable global counting
      
      printf("CSR:0x%lx\n", sis3803_CSR_read(mvme, base, CSR_FULL));
      SIS3803_CSR_read(mvme, base); // status
      
      printf("\nReading all scalers (no clear)\n");
      for(i=0;i<4;i++) {
	sleep(5);
	SIS3803_all_read (mvme, base);
      }
      printf("\nReading all scalers (with clear)\n");
      for(i=0;i<4;i++) {
	sleep(5);
	SIS3803_all_read_clear (mvme, base);
      }

    }
  else 
      printf("\nUnknown test mode\n");
  return;
}


void sis3803()
{
  printf("\nCommand List:\n");
  printf("A Reset             B Module ID\n");
  printf("C ClearAll+Ovf      D Disable Ch Bitpat\n");
  printf("Test Modes\n");
  printf("E EnRefCh1          F DisRefCh1 \n");
  printf("G GenTestPulse      H LED \n");
  printf("I EnTestMode        J DisTestMode  \n");
  printf("K En25MHz           L Dis25MHz  \n");
  printf("Enable/Disable Counting\n");
  printf("M EnCounting        N DisCounting  \n");
  printf("Clear Module  (See also C=ClrAll+Ovfl)\n");
  printf("      Grp1=Ch1-8 Grp2=9-16\n");
  printf("O Clr Grp Ovfl Reg   Q Read Grp Ovfl Reg\n");
  printf("R Clr Chan & Ovfl  T Clr Ovfl of 1Ch\n");

  printf("Read Module\n");
  printf("U Read1Ch  \n");
  printf("V Read All Ch     W Read & Clr All Ch  \n");
  printf("\n");
  printf("Y Test modes\n");
  printf("P CmdList         S Status  \n");
  printf("Q Quit           X Exit  \n");

  // don't need blockreads (only 1 module)
  // clear all ch + overflow (one register)
  // clear group1 + overflow (one register)
  // clear group2 + overflow (one register)

  // clear one ch + overflow  (16 registers)
  // clear overflow of 1ch    (16 registers)

  // read ch   ->     (16 registers) loops over cmd U
  // read/clear ch -> (16 registers) loops over cmd U and R

  // read overflow ch 1-8  (1 reg)
  // read overflow ch 9-16 (1 reg)


}


#endif

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
