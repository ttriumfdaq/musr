
/*-----------------------------------------------------------------------------
 * Copyright (c) 1996      TRIUMF Data Acquistion Group
 * Please leave this header in any reproduction of that distribution
 * 
 * TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 * Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *        amaudruz@triumf.ca
 * ----------------------------------------------------------------------------
 *  
 * Description	: Header file for SIS3803 Standard 200MHz 16 channels scaler
 *
 * Author:  Pierre-Andre Amaudruz Data Acquisition Group
 $Id: sis3803.h,v 1.2 2015/03/19 22:37:53 suz Exp $
*********************************************************************/
#ifndef __SIS3803_INCLUDE__
#define __SIS3803_INCLUDE__


#include <stdio.h>
#include <string.h>
#include <ctype.h>

#ifdef __cplusplus
extern "C" {
#endif
#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef long unsigned int  DWORD;
typedef long unsigned int  BOOL;
#define SUCCESS 1
#endif /* MIDAS_TYPE_DEFINED */
#include "mvmestd.h"
#include "vmicvme.h"

#define CSR_READ         0x0
#define CSR_FULL         0xffffffff  /* CSR Read */
#define CSR_WRITE        0x0
#define GET_MODE         0x0000000C  /* CSR read */

#define IS_REF1                0x00002000  /* CSR read */

#define IS_LED                 0x00000001  /* CSR read */
#define IS_IRQ_EN_BS0          0x00100000  /* CSR read */
#define IS_IRQ_EN_BS1          0x00200000  /* CSR read */
#define IS_IRQ_EN_BS2          0x00400000  /* CSR read */
#define IS_IRQ_BS0             0x10000000  /* CSR read */
#define IS_IRQ_BS1             0x20000000  /* CSR read */
#define IS_IRQ_BS2             0x40000000  /* CSR read */
#define IS_GBL_ENABLE          0x00008000  /* CSR read */
#define IS_GBL_OVFL            0x00004000  /* CSR read */
#define IS_TEST_MODE           0x00000020  /* CSR read */
#define IS_25MHZ_PULSES        0x00000010  /* CSR read */

#define LED_ON                 0x00000001  /* CSR write */
#define LED_OFF                0x00000100  /* CSR write */
#define ENABLE_TEST_MODE       0x00000020  /* CSR write */
#define ENABLE_25MHZ_PULSES    0x00000010  /* CSR write */
#define DISABLE_TEST_MODE      0x00002000  /* CSR write */
#define DISABLE_25MHZ_PULSES   0x00001000  /* CSR write */

#define ENABLE_IRQ_EN_BS0      0x00100000  /* CSR write */
#define ENABLE_IRQ_EN_BS1      0x00200000  /* CSR write */
#define ENABLE_IRQ_EN_BS2      0x00400000  /* CSR write */
#define DISABLE_IRQ_DI_BS0     0x10000000  /* CSR write */
#define DISABLE_IRQ_DI_BS1     0x20000000  /* CSR write */
#define DISABLE_IRQ_DI_BS2     0x40000000  /* CSR write */
#define VME_IRQ_ENABLE         0x00000800  /* CSR write */

#define SIS3803_CSR_RW                   0x000
#define SIS3803_MODULE_ID_RO             0x004
#define SIS3803_IRQ_REG_RW               0x004
#define SIS3803_SELECT_COUNT_DIS_REG_WO  0x00C
#define SIS3803_ALL_CLEAR_WO             0x020
#define SIS3803_SHADOW_CLK_REG_WO        0x024
#define SIS3803_ENABLE_GBL_COUNT_WO      0x028
#define SIS3803_DISABLE_GBL_COUNT_WO     0x02C
#define SIS3803_COUNTER_GRP1_CLEAR_WO    0x040
#define SIS3803_COUNTER_GRP2_CLEAR_WO    0x044
#define SIS3803_ENABLE_REF_CH1_WO        0x050
#define SIS3803_DISABLE_REF_CH1_WO       0x054
#define SIS3803_MODULE_RESET_WO          0x060
#define SIS3803_SINGLE_TST_PULSE_WO      0x068
#define SIS3803_PRESCALE_REG_WO          0x080
#define SIS3803_COUNTER_CLEAR_WO         0x100
#define SIS3803_OVERFLOW_CLEAR_WO        0x180
#define SIS3803_SHADOW_READ_RO           0x200
#define SIS3803_COUNTER_READ_RO          0x280
#define SIS3803_COUNTER_GRP2_READ_RO     0x2A0
#define SIS3803_COUNTER_READ_CLEAR_RO    0x300
#define SIS3803_OVERFLOW_REG1_8_RO       0x380
#define SIS3803_OVERFLOW_REG9_16_RO      0x3A0

#define SOURCE_CIP           0
#define SOURCE_FIFO_FULL     1
#define SOURCE_FIFO_HFULL    2
#define SOURCE_FIFO_ALFULL   3

#define SIS3803_VECT_BASE 0x6f

 DWORD sis3803RegWrite(MVME_INTERFACE *mvme, DWORD base, const DWORD reg_offset,
		       const DWORD value);
  DWORD sis3803RegRead(MVME_INTERFACE *mvme, DWORD base, DWORD reg_offset);
DWORD sis3803_module_ID(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_module_reset(MVME_INTERFACE *mvme, DWORD base_adr);
DWORD sis3803_IRQ_REG_read(MVME_INTERFACE *mvme, DWORD base_adr);
DWORD sis3803_IRQ_REG_write(MVME_INTERFACE *mvme, DWORD base_adr, DWORD irq);
DWORD sis3803_input_mode(MVME_INTERFACE *mvme, DWORD base_adr, DWORD mode);
DWORD sis3803_ref1(MVME_INTERFACE *mvme, DWORD base_adr, DWORD endis);
void  sis3803_channel_enable(MVME_INTERFACE *mvme, DWORD base_adr, DWORD pat);
void  sis3803_channel_disable(MVME_INTERFACE *mvme, DWORD base_adr, DWORD pat);
DWORD sis3803_CSR_read(MVME_INTERFACE *mvme, DWORD base_adr, const DWORD what);
DWORD sis3803_CSR_write(MVME_INTERFACE *mvme, DWORD base_adr, const DWORD what);
void  sis3803_test_mode_enable(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_test_mode_disable(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_25MHz_enable(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_25MHz_disable(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_test_pulse(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_all_clear(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_all_enable(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_all_disable(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_grp1_clear(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_grp2_clear(MVME_INTERFACE *mvme, DWORD base_adr);
void  sis3803_single_clear(MVME_INTERFACE *mvme, DWORD base_adr, const INT ch);
void  sis3803_single_OVFL_clear(MVME_INTERFACE *mvme, DWORD base_adr, const INT ch);
void  sis3803_counter_read(MVME_INTERFACE *mvme, DWORD base_adr, INT ch, DWORD *p);
void  sis3803_all_read(MVME_INTERFACE *mvme, DWORD base_adr, DWORD **p);
void  sis3803_grp1_read(MVME_INTERFACE *mvme, DWORD  base_adr, DWORD **p);
void  sis3803_grp2_read(MVME_INTERFACE *mvme, DWORD  base_adr, DWORD **p);
void  sis3803_all_read_clear(MVME_INTERFACE *mvme, DWORD base_adr, DWORD **p);
void  sis3803_OVFL_grp1_read(MVME_INTERFACE *mvme, DWORD base_adr, DWORD *p);
void  sis3803_OVFL_grp2_read(MVME_INTERFACE *mvme, DWORD base_adr, DWORD *p);
void  sis3803_int_source (MVME_INTERFACE *mvme, DWORD base_adr, DWORD int_source);
void  sis3803_int_source_enable (MVME_INTERFACE *mvme, DWORD base_adr, const int intnum);
void  sis3803_int_source_disable (MVME_INTERFACE *mvme, DWORD base_adr, const int intnum);
void  sis3803_int_clear (MVME_INTERFACE *mvme, DWORD base_adr, const int intnum);
void  sis3803_int_attach (MVME_INTERFACE *mvme, DWORD base_adr, DWORD base_vect, int level, void (*isr)(void));
void  sis3803_int_detach (MVME_INTERFACE *mvme, DWORD base_adr, DWORD base_vect, int level);
void  sis3803_setup(MVME_INTERFACE *mvme, DWORD base_adr, int mode, int dsp);
  //void myStub_sis3803(void);
void SIS3803_CSR_read(MVME_INTERFACE *mvme, DWORD base_adr);
void SIS3803_setup(MVME_INTERFACE *mvme, DWORD base_adr, int mode);
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]);
void SIS3803_TEST(MVME_INTERFACE *mvme, DWORD base, INT itest);
void sis3803(void);
#endif // MAIN_ENABLE

#ifdef __cplusplus
}
#endif
#endif //  __SIS3803_INCLUDE__
