/*------------------------------------------------------------------*
 * V680.c 
 * Procedures for handling the BNC/Highland V680 TDC board
 * This is a VME board with a custom MICROENGINE
 *    designed for Syd Kreitzman & MUSR group at TRIUMF
 *
 *
 * VMIC Version
 *
 *Id:$
 *------------------------------------------------------------------*/
#include "v680.h"

#include <stdio.h>
#include <string.h>
#if defined(OS_LINUX)
#include <unistd.h>
#endif

#ifdef MAIN_ENABLE
// For VMIC processor
#include "vmicvme.h"
INT v680_Display = 0;
INT disp_offset = 0;
INT last_message = 0;
INT debug=1;
#else
extern INT v680_Display ;  // these defined in musr_common.h
extern INT disp_offset ;
extern INT last_message ;
extern INT debug;
#endif

INT tf(WORD ival);



/*------------------------------------------------------------------*/
/** v680RegRead
    Read 16bit to register.
    @memo 16bit read.
    @param base\_adr V680 VME base address
    @param reg\_offset register offset
    @return read back value
*/
WORD v680RegRead(MVME_INTERFACE *mvme, DWORD base, DWORD reg_offset)
{
  INT csr;
  WORD data;
  

  mvme_set_am(mvme, MVME_AM_A16_ND); // set A16
  mvme_set_dmode(mvme, MVME_DMODE_D16); // set D16
  csr = mvme_read_value(mvme, base + reg_offset);

  data = (WORD)(csr & 0xFFFF);
  return data; 

}
/*------------------------------------------------------------------*/
/** v680RegWrite
    Write 16bit to register.
    @memo 16bit write.
    @param base\_adr V680 VME base address
    @param reg\_offset register offset
    @param value to be written 
    @return read back value
*/
 WORD v680RegWrite(MVME_INTERFACE *mvme, DWORD base, const DWORD reg_offset,
                           const WORD value)
{
  INT csr;
  WORD data;
  
  mvme_set_am(mvme, MVME_AM_A16_ND); // set A16
  mvme_set_dmode(mvme, MVME_DMODE_D16); // set D16

  mvme_write_value(mvme, base + reg_offset, value);
  csr = mvme_read_value(mvme, base + reg_offset);
  data = (WORD)(csr & 0xFFFF);
  return data; 
}


/************  function for General command ***********/
 void v680MUE_read(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: 
 Input: 
  DWORD base_addr     : base address of the v680
 Output:  none
 Function value:
 WORD                : none
\**************************************************************/
{
  WORD value,value1,value2,channel;
  DWORD time;
  value = v680RegRead (mvme, base,V680_CTL);
  printf("Control reg: 0x%x\n",value);

 value = v680RegRead (mvme, base,V680_HIT);
  printf("Hit reg: 0x%x\n",value);
  
  value= v680RegRead (mvme, base,V680_MUEREG);
  printf("MUE reg: 0x%x\n",value);
  // if (value & 0x8000)
  if(value <  0xfff0)  // looks like Bit 15 is Set except when hit is present, unlike Manual. The "zero" is to ignore the time data
  {
    printf("Hit present\n");
    value1 = v680RegRead (mvme, base,V680_MUEDATA_M);    
    value2 = v680RegRead (mvme, base,V680_MUEDATA_L);
    printf("Address/Time_Hi = 0x%x;   Time_Lo = 0x%x\n",value1,value2);
    channel = value1;
    channel = ( (channel >> 8 ) & 0xF);

    time = (value1 & 0x7) << 16;
    printf("shifted M =0x%x\n",(unsigned int)time);
    time = time | value2;

    
    printf("Channel: %d   Time 0x%x\n",channel,(unsigned int)time);


 value =  v680RegRead (mvme, base,V680_HIT);
 printf("\nHit Reg 5 offset 0xA  value 0x%4.4x\n",value);

 int i;
 for(i=0; i<9; i++)
   {
     if(value & 1<<i)
       printf("Hit on channel %d \n",i);
   }


  }
  else
    printf("MUE bit 15 is not set; no data\n");
  return;
}

void v680CntrlStatus(MVME_INTERFACE  *mvme, DWORD base)
{
  WORD value,me;
  int i;
  int hits[9];

  me =  v680RegRead (mvme, base,V680_MUEREG);
  printf("MUE reg = 0x%x\n",me);

  value =  v680RegRead (mvme, base,V680_CTL);
  printf("\nControl Reg 4 offset 0x8  value 0x%4.4x\n",value);
  
  printf("GATE  Enable Gates for Inputs 0-8:     %d\n",tf(value & 1)); // 1
  printf("FGATE Force GATE state to be true:     %d\n",tf(value & 1<<1)); // 2
  printf("POS   Force positive time measurement: %d\n",tf(value & 1<<2)); // 4
  printf("IRQ   Interrupt requested:             %d\n\n",tf(value & 1<<3)); // 8
 
  printf("SYNC  ET8 test to local clock:         %d\n",tf(value & 1<<4)); // 0x10
  printf("ET8   test enabled:                    %d\n",tf(value & 1<<5)); // 0x20
  printf("GTEST test gate threshold detector:    %d\n",tf(value & 1<<6)); // 0x40
  printf("TTEST test threshold detectors 0-8:    %d\n\n",tf(value & 1<<7)); // 0x80

 // bit 8 not used
  printf("GSTAT state of gate input:             %d\n",tf(value & 1<<9));  // 0x200
  printf("UENgine enabled:                       %d\n\n",tf(value & 1<<10)); // 0x400


  value =  v680RegRead (mvme, base,V680_HIT);
  printf("\nHit Reg 5 offset 0xA  value 0x%4.4x\n",value);

  int no_hits=1;
  for(i=0; i<9; i++)
    {
      if(value & 1<<i)
	{
	  no_hits=0;
	  hits[i]=i;
	}
      else
	hits[i]=0;
   }
  if(no_hits)
    printf("No hits detected");
  else
    {
      printf("Hits on channel(s) ");
      for(i=0; i<9; i++)
	printf("%d ",i);
    }
  printf("\n");


  printf("GATEF Gate fallen bit: %d\n",tf(value & 1<<9));
  printf("FLAG15 bit:            %d\n",tf(value & 1<<10));
  printf("Aux1 bit:              %d\n",tf(value & 1<<11));
  printf("OK bit:                %d\n",tf(value & 1<<12));
  printf("Z bit(s):            0x%x \n",value>>13);


 value =  v680RegRead (mvme, base,V680_SELECT);
 printf("\nSelect Reg 9 offset 0x12  value 0x%4.4x\n",value);

 WORD temp=0x8000 & (1 << 15);
 printf("\nMUE Reg 16 offset 0x20  value 0x%4.4x \n",me );


 if( temp)
   printf("MUE  valid event (hit)\n");
 else
   printf("MUE  no hit\n");


 return;


}

INT tf(WORD ival)
{
  if(ival)
    return 1;
  else
    return 0;
}

/************  function for General command ***********/
 INT v680MUERunning(MVME_INTERFACE *mvme, DWORD base)
/**************************************************************\
 Purpose: See if MicroEngine is running
 Input: 
  DWORD base_addr     : base address of the v680
 Output:
    none
 Function value:
  WORD               : 1 if MicroEngine is running
\**************************************************************/
{

  INT tmp=0;
  WORD value;
  
  value =  v680RegRead (mvme, base,V680_CTL);
  tmp = (value & (WORD)(V680_BMUEON));
  if (tmp)
  {
    if(v680_Display)
      ss_printf( 0,last_message+1+disp_offset,
               "v680: VCMD_READ_MUE - microengine bit is set. MUE is running          \n");
    else
      printf("v680: VCMD_READ_MUE - microengine bit is set. MUE is running          \n");
      
   tmp=1; /* return value */
  }
  else
    if(v680_Display)
      ss_printf( 0,last_message+1+disp_offset,
                 "v680: VCMD_READ_MUE - microengine bit is clear. MUE is not running    \n");
    else
      printf("v680: VCMD_READ_MUE - microengine bit is clear. MUE is not running    \n");
  return tmp;
}

/************  function for General command ***********/
 WORD v680DumpRegs(MVME_INTERFACE *mvme, DWORD base, WORD all)
/**************************************************************\
 Purpose: Dump the contents of the registers
 Input: 
  DWORD base_addr     : base address of the v680
  WORD  all           : 1 dump all regs, or 0 dump MUE regs (and control)
 Output:
    register values
 Function value:
  WORD               : 
\**************************************************************/
{

    
/* registers to read:  */
  WORD value,tmp1,tmp2,tmp3,tmp4;

  if(!v680_Display)
  {
    if(all)
    {
      printf("Reg. Offset Contents  Masked        Name  (note regs are MASKED if appropriate)\n"); 
      value = v680RegRead (mvme, base,V680_VXIMFR);
      printf("0    0x00   0x%4.4x                   VXI MFR (0xFEEE)\n",value);
      value = v680RegRead (mvme, base,V680_VXITYPE);
      printf("1    0x02   0x%4.4x                   VXI type (0x5989)\n",value);
      value = v680RegRead (mvme, base,V680_VXISTS);
      printf("2    0x04   0x%4.4x                   VXI status (0xFFFF)\n",value);
      value = v680RegRead (mvme, base,V680_IRQ);
      printf("3    0x06   0x%4.4x     0x%4.4x        IRQ vector\n\n",value,(value &0xFF));
      
      value = v680RegRead (mvme, base,V680_CTL);
      printf("4    0x08   0x%4.4x     0x%4.4x        Control\n",value,(value & 0x6FF));
      value = v680RegRead (mvme, base,V680_HIT);
      printf("5    0x0A   0x%4.4x                    Hit\n",value);
      value = v680RegRead (mvme, base,V680_DBL);
      printf("6    0x0C   0x%4.4x     0x%4.4x        Double Hit\n", value,(value & 0xFF));
      value = v680RegRead (mvme, base,V680_IE);
      printf("7    0x0E   0x%4.4x     0x%4.4x        Interrupt Enables\n\n",value,(value & 0x7FF));
      
      printf("8    0x10                            Reset (read only)\n");
      value = v680RegRead (mvme, base,V680_SELECT);
      printf("9    0x12   0x%4.4x     0x%4.4x        Readout Select\n",value,(value & 0x1F));
      value = v680RegRead (mvme, base,V680_DATA_H);
      printf("10   0x14   0x%4.4x                   Data High\n",value);
      value = v680RegRead (mvme, base,V680_DATA_M);
      printf("11   0x16   0x%4.4x                   Data Med\n",value);
      value = v680RegRead (mvme, base,V680_DATA_L);
      printf("12   0x18   0x%4.4x                   Data Low\n\n",value);
      
      printf(" Microengine Registers:\n");
      printf("13   0x1A                            not used\n");
      value = v680RegRead (mvme, base,V680_MUEDATA_M);
      printf("14   0x1C   0x%4.4x     0x%4.4x        MUE address,time High\n",value,(value & 0xF07));
      value = v680RegRead (mvme, base,V680_MUEDATA_L);
      printf("15   0x1E   0x%4.4x     0x%4.4x        MUE time Low\n",value,(value & 0xF07));
      value = v680RegRead (mvme, base,V680_MUEREG);
      printf("16   0x20   0x%4.4x     0x%4.4x        MUE Flag\n\n",value,(value & 0x8000));
    }
    else
    {  /* MUE only */
      printf("Reg. Offset Contents  Masked  Name  (note regs are MASKED if appropriate)\n");
      value = v680RegRead (mvme, base,V680_CTL);
      printf("4    0x08   0x%4.4x     0x%4.4x      Control\n",value,(value &
                                                                 0x6FF));
      printf(" Microengine Registers:\n");
      printf("13   0x1A                not used\n");
      value = v680RegRead (mvme, base,V680_MUEDATA_M);
      printf("14   0x1C   0x%4.4x     0x%4.4x      MUE address,time High\n",value,(value & 0xF07));
      value = v680RegRead (mvme, base,V680_MUEDATA_L);
      printf("15   0x1E   0x%4.4x     0x%4.4x      MUE time Low\n",value,(value & 0xF07));
      value = v680RegRead (mvme, base,V680_MUEREG);
      printf("16   0x20   0x%4.4x     0x%4.4x      MUE Flag\n\n",value,(value & 0x8000));
      
    }
  }
  else  /* v680 display is ON */
  {
    if(all)
    {
      tmp1 = v680RegRead (mvme, base,V680_VXIMFR);
      tmp2 = v680RegRead (mvme, base,V680_VXISTS);
      tmp3 = v680RegRead (mvme, base,V680_VXITYPE); 
      tmp4 = v680RegRead (mvme, base,V680_IRQ); 
      
      ss_printf( 0,last_message+3+disp_offset,
                 "vxi_mfr:0x%4.4x vxi_sts:0x%4.4x vxi_type:0x%4.4x irq   :0x%4.4x \n",tmp1,tmp2,tmp3,tmp4);

      ss_printf( 0,last_message+4+disp_offset,"v680_dump_registers: \n"); 
      tmp1 = v680RegRead (mvme, base,V680_CTL);
      tmp2 = v680RegRead (mvme, base,V680_SELECT);
      tmp3 = v680RegRead (mvme, base,V680_HIT); /* used in normal mode */
      ss_printf( 0,last_message+5+disp_offset,
                 "control:0x%4.4x select  :0x%4.4x hit   :0x%4.4x \n",tmp1,tmp2,tmp3);
      
      tmp1 = v680RegRead (mvme, base,V680_DATA_L);
      tmp2 = v680RegRead (mvme, base,V680_DATA_M);
      tmp3 = v680RegRead (mvme, base,V680_DATA_H); /* not really used */
      tmp4 = v680RegRead (mvme, base,V680_DBL); /* used in normal mode */
    
      ss_printf( 0,last_message+6+disp_offset,
                 "datL   :0x%4.4x datM   :0x%4.4x datH    :0x%4.4x double:0x%4.4x \n",tmp1,tmp2,tmp3,tmp4);
      
      
      
      tmp1 = v680RegRead (mvme, base,V680_IE); 
      tmp2 = v680RegRead (mvme, base,V680_MUEREG);  /* upper 16 bits used */
      ss_printf( 0,last_message+7+disp_offset,
                 "ie     :0x%4.4x mue    :0x%4.4x \n",tmp1,tmp2);
      
    
      tmp1 = v680RegRead (mvme, base,V680_MUEDATA_L);
      tmp2 = v680RegRead (mvme, base,V680_MUEDATA_M);
      ss_printf( 0,last_message+8+disp_offset,
                 "MUE Data Lo:0x%4.4x  Ch & Med    :  0x%4.4x   \n",tmp1,tmp2);    
    }
    else  
    {      /* MicroEngine Registers only */
      tmp1 = v680RegRead (mvme, base,V680_CTL);
      tmp2 = v680RegRead (mvme, base,V680_MUEREG);  /* upper 16 bits used */
      tmp3 = v680RegRead (mvme, base,V680_MUEDATA_L);
      tmp4 = v680RegRead (mvme, base,V680_MUEDATA_M);
      ss_printf( 0,last_message+3+disp_offset,
                 "ctl :0x%4.4x mue :0x%4.4x Data Lo:0x%4.4x  Ch & Med:  0x%4.4x   ",
                 tmp1,tmp2,tmp3,tmp4);
      
    }
  }
  return (0);
}
  
/*- v680 clock hang  ---------------------------------------------------*/
   WORD v680MakeClockHang(MVME_INTERFACE *mvme, DWORD base,INT type)
{
  /* Make the clock hang for testing!
     type = 0 for MUE switched off
     type = 1 for FGATE bit on          clock hang
     type = 2 for GTEST bit on          clock hang
     type = 3 for TTEST bit on          clock hang */

  WORD ctl;
  INT value;
  
      switch (type)
       

      {
      case 0 :
        if(v680_Display)
          ss_printf( 0,last_message+1+disp_offset,"v680: setting MUE off for clock hang... "); 
        else
          printf("v680: setting MUE off for clock hang...\n "); 
/* clear MUE bit  */
        ctl =  v680RegRead (mvme, base,V680_CTL);
        ctl =  ctl & (0xFFFFFFFF ^ (WORD)(V680_BMUEON));/* clear MUE bit */
        value = v680RegWrite(mvme, base, V680_CTL, ctl);
        break;
      case 1 :  
        if(v680_Display)
          ss_printf( 0,last_message+1+disp_offset,"v680: setting FGATE (bit 1) for clock hang... "); 
        else
          printf("v680: setting FGATE (bit 1) for clock hang...\n "); 

        /* set up FGATE  */
        ctl =  v680RegRead (mvme, base,V680_CTL);   /* get ctl reg */      
        ctl  = ctl | (WORD)(V680_BFGATE); /* set fgate on */
        value = v680RegWrite(mvme, base, V680_CTL, ctl);
        break;
      case 2 :
        if(v680_Display)
          ss_printf( 0,last_message+1+disp_offset,"v680: setting GTEST (bit 6) for clock hang... "); 
        else
          printf("v680: setting GTEST (bit 6) for clock hang...\n "); 
        /* set up GTEST  */
        ctl =  v680RegRead (mvme, base,V680_CTL);   /* get ctl reg */      
        ctl  = ctl | (WORD)(V680_BGTEST); /* set gtest on */
        value = v680RegWrite(mvme, base, V680_CTL, ctl);
        break;
      case 3 :
        if(v680_Display)
          ss_printf( 0,last_message+1+disp_offset,"v680: setting TTEST (bit 7) for clock hang... "); 
        else
          printf( "v680: setting TTEST (bit 7) for clock hang...\n "); 
         /* set up TTEST  */
        ctl =  v680RegRead (mvme, base,V680_CTL);   /* get ctl reg */      
        ctl  = ctl | (WORD)(V680_BTTEST); /* set ttest on */
        value = v680RegWrite(mvme, base, V680_CTL, ctl);
        break;
      default :
        if(v680_Display)
          ss_printf (0,last_message+1+disp_offset,"v680: unknown type (use one of 0,1,2,3)       ");
        else
          printf ("v680: unknown type (use one of 0,1,2,3)      \n ");
        value = 0;
        break;
      }
      return(value);
}
/*- v680 register clear ---------------------------------------------------*/
 WORD v680Clear(MVME_INTERFACE *mvme, DWORD base)
{
  /* Reset all channels + gate + uengine */
  WORD value;
  value = v680RegWrite(mvme, base, V680_RESET, V680_BRESET);
  return(value);
}

/*- v680 restart ---------------------------------------------------*/
 WORD v680Restart(MVME_INTERFACE *mvme, DWORD base)
{
  /* Re-enable TDC after a reset */
  WORD value;
  if(v680_Display)
    ss_printf( 0,last_message+1+disp_offset,"v680: V680 restart for MUE ON... "); 
  else
    printf( "v680: V680 restart for MUE ON...\n"); 
    /* init positive time + internal clock */
  value = v680RegWrite(mvme, base, V680_CTL,
                       (WORD)(V680_BPOS | V680_BSYNC | V680_BMUEON));
  return(value);
}


/*- v680 clear clock hang ------------------------------------------*/
 WORD v680ClearClockHang(MVME_INTERFACE *mvme, DWORD base)
{
  /* DE-hanging code for MicroEngine
     try to eliminate the "clock hang problem" where TDC gets cleared
     somehow and left in normal mode, so red light is on continuously. Also
     eliminate the other clock hang where FGATE bit gets set on
     
     Re-enable TDC after a clock hang  */
  WORD ctl,value;
  WORD mask;
  
  /* calculate de-hanging mask */ 
    mask =  ( 0xffff ^ ( (WORD)(V680_BFGATE) | (WORD)(V680_BGTEST)
                                               | (WORD)(V680_BTTEST) ) ); 
  
  
  ctl = v680RegRead(mvme, base, V680_CTL);     /* get ctl reg */
  if ( !(ctl & (WORD)(V680_BMUEON)) ) /* is MUE bit clear? */ 
  {
    value = v680RegWrite(mvme, base, V680_CTL,
                         (ctl | (WORD)(V680_BMUEON))); /* yes so enable microengine mode */
    if(v680_Display)
      ss_printf( 15,last_message+disp_offset,
                 " INFO: Clock Hang fixed (MUE bit) cntl reg: 0x%x                              ",ctl);
    else
      printf( " INFO: Clock Hang fixed (MUE bit) cntl reg: 0x%x\n",ctl); 
    
  }
    
  /* are any of FGATE/GTEST/TTEST bits set? */ 
  if ( ctl & ( (WORD)(V680_BFGATE) | (WORD)(V680_BGTEST)
               | (WORD)(V680_BTTEST) ) )
  {
    /* yes so clear all of FGATE, GTEST, TTEST  bits */
    value = v680RegWrite(mvme, base, V680_CTL, (ctl & mask));
    if(v680_Display)
      ss_printf( 15,last_message+disp_offset,
          " INFO: Clock Hang fixed (due to FGATE|GTEST|TTEST bit set); ctl reg : 0x%x",ctl);
    else
      printf(" INFO: Clock Hang fixed (due to FGATE|GTEST|TTEST bit set); ctl reg : 0x%x\n",ctl);

  }
  return (0);
}

#ifdef GONE  /* now in musr_common_subs */
void set_v680_display(INT display_enabled, INT display_offset, INT start_line)
{
  /* offset needed if more than 8 channels are enabled
     start_line indicates "last message:" line  */
  disp_offset = display_offset;
  v680_Display =  display_enabled;
  last_message   =  start_line;
  /* printf("set_v680_display: v680_Display=%d offset=%d last_message=%d\n",
     v680_Display,disp_offset,last_message); */
}

 DWORD GetMaxFree(void)
{
  DWORD ival;
  /* return the maximum free memory space */
  ival=memFindMax();
  return (ival);
}
#endif // GONE
/*****************************************************************/
/*-PAA- For test purpose only */

#ifdef MAIN_ENABLE
// ss_printf is provided by MIDAS 
void  ss_printf(INT x, INT y, const char *format, ...)
{
}

int main (int argc, char* argv[]) {

  INT status, i, data;
  char cmd[]="hallo";
  int s;
  INT ival,reg;

  MVME_INTERFACE *myvme;

  DWORD  V680_BASE= 0xC000; /* default */


  if (argc>1) {
    sscanf(argv[1],"%lx",&V680_BASE);
  }

  printf("\nV680 Base Address: 0x%lx ; A16/D16/D8\n",V680_BASE);

  // Test under vmic
  status = mvme_open(&myvme, 0);

  // Set am to A16 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A16_ND); // module supports A16 only

  // Set dmode to D16
  mvme_set_dmode(myvme, MVME_DMODE_D16); // D16 or D8 supported

  printf("\nCalling v680Clear...\n\n");
  v680Clear(myvme,V680_BASE);  /* Reset all channels + gate + uengine */
  v680Cmds();

   while ( isalpha(cmd[0]) |  isdigit(cmd[0]))
    {
      printf("\nEnter command (A-Y) X to exit?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];

      switch(s)
        {
        case ('A'):
          v680Clear(myvme, V680_BASE);    /* Reset all channels + gate + uengine */
          break;

        case ('B'):
          v680Restart(myvme, V680_BASE );  /* Re-enable TDC after a reset */
          break;  

        case ('C'):
          printf("Enter V680 Register to write : 0x");
          scanf("%x",&reg);
          printf("Enter value to write (16 bits) : 0x");
          scanf("%x",&ival);
          ival = ival & 0xFFFF;

          data = v680RegWrite(myvme, V680_BASE, reg, ival);
	  printf("Wrote: 0x%4.4x Read back: 0x%4.4x\n",ival,data);
          break;

        case ('D'):
          printf("Enter which registers to dump (1=All, 0=MUE and control)  : ");
          scanf("%d",&i);
          v680DumpRegs(myvme, V680_BASE,i );
          break;

        case ('E'):
          printf("Enter V680 Register to read : 0x");
          scanf("%x",&reg);
          data = v680RegRead(myvme, V680_BASE, reg);
	  printf("Read back: 0x%4.4x\n",data);
          break;

        case ('F'):
          data = v680MUERunning(myvme, V680_BASE );
          break;
        
        case ('G'):
          printf("Enter Clock Hang Type (0=MUE OFF, 1=FGATE ON, 2=GTEST ON, 3=TTEST ON)  : ");
          scanf("%d",&i);
          data = v680MakeClockHang(myvme, V680_BASE, i );
          printf("Returned value = %d or 0x%x\n",data,data);
          break;


        case ('I'):
          v680ClearClockHang(myvme, V680_BASE );
          break;

        case ('J'):
          v680MUE_read(myvme, V680_BASE );
          break;

     case ('S'):
          v680CntrlStatus(myvme, V680_BASE );
          break;

        case ('P'):
          v680Cmds();
          break;
        case ('H'):
          v680();
        case ('X'):
        case ('Q'):
	  status = mvme_close(myvme);
          return 1;
        default:
          cmd[1]='\0';
	  printf("Unknown command: %s\n",cmd);
          break;
        }
    }


  status = mvme_close(myvme);
  return 1;
}
void v680(void)
{
   printf("V680 TDC control ( base = 0xc000 for MUSR) \n");
   printf("  v680Clear           Reset all channels + Gate + MicroEngine\n");
   printf("  v680Restart         Re-enable TDC after a reset\n");
   printf("  v680RegWrite        Write register\n");
   printf("  v680RegRead         Read register\n");
   printf("  v680DumpRegs        Dump contents of registers\n");
   printf("  v680MUERunning      Check whether MicroEngine is running\n");
   printf("  v680MakeClockHang   Make the clock hang\n");
   printf("  v680ClearClockHang  Clear a clock hang\n");
   printf("  v680MUE_read        Read MicroEngine Registers\n");
}

void  v680Cmds(void)
{
  // print brief list of commands
  printf("A Clear              B Restart  \n");
  printf("C WriteReg           D DumpRegisters \n");
  printf("E ReadReg            F MUErunning?  \n"); 
  printf("G MakeClockHang      I ClearClockHang \n");
  printf("J ReadMUE            S Status\n");
  printf("H Help               P print \n");
  printf("X Exit               Q Quit\n");
  printf("\n");
}

#endif // MAIN
