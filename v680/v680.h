
/*-----------------------------------------------------------------------------
 * Copyright (c) 1996      TRIUMF Data Acquistion Group
 * Please leave this header in any reproduction of that distribution
 * 
 * TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 * Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *        amaudruz@triumf.ca
 * ----------------------------------------------------------------------------
 *  
 * Description	: Header file for V680 TDC  A16:D16:D08(EO)
 *
 * Author: Suzannah Daviel, TRIUMF
 *---------------------------------------------------------------------------*
 * CVS log information:
 *$Log: v680.h,v $
 *Revision 1.1  2015/03/19 22:00:13  suz
 *new to CVS; MUSR's TDC code (VMIC version of VxWorks V680.h)
 *
 *Revision 1.2  2004/02/09 20:16:18  suz
 *combined TD,Imusr; set_v680_display & GetMaxFree definitions moved to musr_common_subs.h
 *
 *Revision 1.1  2002/04/17 00:18:32  suz
 *original
 *
 *
 *---------------------------------------------------------------------------*/
#ifndef _V680_INCLUDE_
#define _V680_INCLUDE_

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#ifndef MAIN_ENABLE
#include "midas.h"  // ss_printf
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef long unsigned int  DWORD;
typedef long unsigned int  BOOL;
#define SUCCESS 1
#endif /* MIDAS_TYPE_DEFINED */

#include "mvmestd.h"
#include "vmicvme.h"


/* V680 register offset defines */

#define V680_VXIMFR   0x00    /* R   Manufacturer ID 0xFEEE */
#define V680_VXITYPE  0x02    /* R   Module Type     0x5989 */
#define V680_VXISTS   0x04    /* R   VXI status Red  0xFFFF */
#define V680_IRQ      0x06    /* RW  VME interrupt vector Reg */
#define V680_CTL      0x08    /* RW  Control Reg */
#define V680_HIT      0x0a    /* R   Hit Flags Reg */
#define V680_DBL      0x0c    /* R   Double Hit Flags Reg */
#define V680_IE       0x0e    /* RW  Interrupt Enable Mask Reg */
#define V680_RESET    0x10    /* W   Channel/Module reset bits */
#define V680_SELECT   0x12    /* RW  Data readout select Reg */
#define V680_DATA_H   0x14    /* R   T0 MostSig  16 bits of time */
#define V680_DATA_M   0x16    /* R   T1 mid      16 bits of time */
#define V680_DATA_L   0x18    /* R   T2 LeastSig 16 bits of time */
/*   M I C R O E N G I N E */
#define V680_MUEDATA_M  0x1c  /* R   MicroEngine MostSig & channel */
#define V680_MUEDATA_L  0x1e  /* R   MicroEngine LeastSig */
#define V680_MUEREG     0x20  /* RW? MicroEngine Register */


/* shifts or masks */
#define V680_BGATE   1<<0
#define V680_BFGATE  1<<1
#define V680_BPOS    1<<2
#define V680_BSYNC   1<<4
#define V680_BET8    1<<5
#define V680_BGTEST   1<<6
#define V680_BTTEST   1<<7
#define V680_BMUEON   1<<10

#define V680_BRESET  0xFFF /* was 1FF now include gateflag reset, clrctr */



/* V680 prototypes */

void  v680MUE_read(MVME_INTERFACE *mvme, DWORD base_adr);
WORD  v680RegRead(MVME_INTERFACE *mvme, DWORD base_adr, DWORD reg_offset);
WORD  v680RegWrite(MVME_INTERFACE *mvme, DWORD base_adr, DWORD reg_offset, const WORD value);
INT   v680MUERunning(MVME_INTERFACE *mvme, DWORD base_adr);
WORD  v680DumpRegs(MVME_INTERFACE *mvme, DWORD base_adr, WORD all);
WORD  v680MakeClockHang(MVME_INTERFACE *mvme, DWORD base_adr, INT type);
WORD  v680Clear(MVME_INTERFACE *mvme, DWORD base_adr);
WORD  v680Restart(MVME_INTERFACE *mvme, DWORD base_adr);
WORD v680ClearClockHang(MVME_INTERFACE *mvme, DWORD base_adr);

/* now in musr_common_subs.h 
   DWORD GetMaxFree(void);
   void   set_v680_display(INT display_enabled, INT display_offset, INT start_line);
*/
#ifdef MAIN_ENABLE
void  ss_printf(INT x, INT y, const char *format, ...);
int main (int argc, char* argv[]);
void  v680Cmds(void);
void v680(void);
#endif // MAIN
#ifdef __cplusplus
}
#endif
#endif //  _V680_INCLUDE_
