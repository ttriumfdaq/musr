/*-----------------------------------------------------------------------------
 *  Copyright (c) 1996      TRIUMF Data Acquistion Group
 *  Please leave this header in any reproduction of that distribution
 * 
 *  TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 *  Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *         amaudruz@triumf.ca
 * -----------------------------------------------------------------------------
 *  
 *  Description : Support function library for the vmeIO board developed at TRIUMF
 *                for the MuSR group
 *  Requires    : 
 *  Application : VME
 *  Author      :  Pierre-Andre Amaudruz Data Acquisition Group
 *
 * Convert to VMIC
 *
 * $Id: musrvmeio.c,v 1.6 2015/03/19 22:08:52 suz Exp $
 *
 *---------------------------------------------------------------------------*/
#include "musrvmeio.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#if defined(OS_LINUX)
#include <unistd.h>
#endif

#ifdef MAIN_ENABLE
// For VMIC processor
#include "vmicvme.h"
BOOL debug=0;
#endif


/* Book structure */
MUSR_VMEIO uvio={0};

/*-----------------------------------------------*/
DWORD bitSet(DWORD d, INT c)
{
  d |= (1<<c);
  return d;
}

/*-----------------------------------------------*/
DWORD bitClr(DWORD d, INT c)
{
  d &= ~(1<<c);
  return d;
}

/*-----------------------------------------------*/
INT bitRead(DWORD d, INT c)
{
  DWORD dd=0;
  dd = d & (1<<c);
  return (dd>>c);
}



/*-----------------------------------------------*/
void musrVmeioInit(DWORD base)
{
  
  /* Set VMEIO base address  */
  uvio.base = base;
  printf("Base address: 0x%lx\n",base);
  return;
    
}

/*-----------------------------------------------*/
void musrVmeioReset(void)
{
  memset(&uvio, 0, sizeof(MUSR_VMEIO));
  printf("Structure cleared\n");
}


/* -------------Outputs section----------------- */

INT musrVmeioPulseSet(MVME_INTERFACE *mvme, INT ch)
{
  if (uvio.base) {
      
    /* Check if channel is in range */
    if ((ch >= FIRST_OUTPUT) && (ch < MAX_N_OUTPUT)) {
      
      /* Record new setting */
      uvio.output_mode = bitSet(uvio.output_mode, ch);
      uvio.output_pulse_enable = bitSet(uvio.output_pulse_enable, ch);
      
      mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
      mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

      mvme_write_value(mvme, uvio.base + VMEIO_SET_PULSE_WO,  uvio.output_mode); // OUTSET Register
  
      return UVIO_SUCCESS;
    }
    return UVIO_INVALID_CH;
  }
  return UVIO_NOT_INITIALIZED;
}

void musrVmeio_msg(INT status)
{

  if (status == UVIO_SUCCESS)
    return; // no error
  else if  (status == UVIO_INVALID_CH)
    printf("Error: invalid Channel\n");
  else if  (status == UVIO_NOT_INITIALIZED)
    printf("Error: not Initialized\n");
  else if  (status == UVIO_DISABLE_CH)
    printf("Error: channel is Disabled\n");
  else if  (status == UVIO_NO_STROBE)
    printf("Error: no Strobe\n");
  else
    printf("Unknown error %d (0x%x)\n",status,status);
  return;
}

/*-----------------------------------------------*/
INT musrVmeioLatchSet(MVME_INTERFACE *mvme, INT ch)
{

  /* Check if channel is in range */
  if ((ch >= FIRST_OUTPUT) && (ch < MAX_N_OUTPUT)) {
    
    /* Record new setting */
    uvio.output_mode = bitClr(uvio.output_mode, ch);
    uvio.output_latch_enable = bitSet(uvio.output_latch_enable, ch);
    
    /* Set module */
      mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
      mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

      mvme_write_value(mvme, uvio.base + VMEIO_SET_PULSE_WO,  uvio.output_mode);

    
    return UVIO_SUCCESS;
  }
  return UVIO_INVALID_CH;
}

/*-----------------------------------------------*/
INT musrVmeioPulse(MVME_INTERFACE *mvme, INT ch)
{

  /* Check if channel is in range */
  if ((ch >= FIRST_OUTPUT) && (ch < MAX_N_OUTPUT)) {
    
    /* Check for enable */
    if (bitRead(uvio.output_pulse_enable, ch)) {

 /* Set module */
      mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
      mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

      mvme_write_value(mvme, uvio.base +  VMEIO_WRITE_PULSE_WO, (1<<ch)  );
 
      return UVIO_SUCCESS;
    }
    return UVIO_DISABLE_CH;
  }
  return UVIO_INVALID_CH;
}

/*-----------------------------------------------*/
INT musrVmeioOn(MVME_INTERFACE *mvme, INT ch)
{

    /* Check if channel is in range */
    if ((ch >= FIRST_OUTPUT) && (ch < MAX_N_OUTPUT)) {
    
	/* Check for enable */
	if (bitRead(uvio.output_latch_enable, ch)) {

	  /* Set module */
	  mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
	  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

	  /* Record change */
	  uvio.output_state = bitSet(uvio.output_state, ch);
	  mvme_write_value(mvme, uvio.base +  VMEIO_WRITE_LATCH_WO ,  uvio.output_state );

	  return UVIO_SUCCESS;
	}
	return UVIO_DISABLE_CH;
    }
    return UVIO_INVALID_CH;
}

/*-----------------------------------------------*/
INT musrVmeioOff(MVME_INTERFACE *mvme, INT ch)
{

  /* Check if channel is in range */
  if ((ch >= FIRST_OUTPUT) && (ch < MAX_N_OUTPUT)) {
    
    /* Check for enable */
    if (bitRead(uvio.output_latch_enable, ch)) {

      /* Set module */
      mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
      mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

        /* Record change */
        uvio.output_state = bitClr(uvio.output_state, ch);
        mvme_write_value(mvme, uvio.base +  VMEIO_WRITE_LATCH_WO ,  uvio.output_state );
	 
      return UVIO_SUCCESS;
    }
    return UVIO_DISABLE_CH;
  }
  return UVIO_INVALID_CH;
}

/*-----------------------------------------------*/
INT musrVmeioClear(MVME_INTERFACE *mvme)
{
  if (uvio.base) {
    
    /* Set module */
    mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
    mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32
    
    mvme_write_value(mvme, uvio.base +  VMEIO_CLEAR_VMEIO_WO,  0 );
    
    return UVIO_SUCCESS;
  }
  return UVIO_NOT_INITIALIZED;
}

/*-----------------------------------------------*/
DWORD musrVmeioOutputState(void)
{
  return uvio.output_state;
}

/*-----------------------------------------------*/
DWORD musrVmeioOutputMode(void)
{
  return uvio.output_mode;
}

/* -------------Inputs section----------------- */

/*-----------------------------------------------*/
INT musrVmeioAsyncSet (INT ch)
{
  /* Check if channel is in range */
  if ((ch >= FIRST_INPUT) && (ch < MAX_N_INPUT)) {
    
    /* Record change */
    uvio.input_async_enable = bitSet(uvio.input_async_enable, ch);
    return UVIO_SUCCESS;
  }
  return UVIO_INVALID_CH;
}

/*-----------------------------------------------*/
INT musrVmeioStrobeSet (INT ch)
{
  /* Check if channel is in range */
  if ((ch >= FIRST_INPUT) && (ch < MAX_N_INPUT)) {
    
    /* Record change */
    uvio.input_strobe_enable = bitSet(uvio.input_strobe_enable, ch);
    return UVIO_SUCCESS;
  }
  return UVIO_INVALID_CH;
}

/*-----------------------------------------------*/
INT musrVmeioRead  (MVME_INTERFACE *mvme, INT ch)
{
  // Added value so return value is always the status
  INT csr;
  DWORD data;
  
  /* Handles Async and Strobe read */
  /* Check if channel is in range */
  if ((ch >= FIRST_INPUT) && (ch < MAX_N_INPUT))
    {

      /* Set module */
      mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
      mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

      /* Select mode of operation (async/Strobe) */
      if (bitRead(uvio.input_async_enable, ch)) 
	{
	  /* Set => Async */
	  csr = mvme_read_value(mvme, uvio.base + VMEIO_READ_ASYNC_RO);
	  data = csr & OUTPUT_MASK;
	  return  ((data >> ch) & 0x1);
	}

      else if (bitRead(uvio.input_strobe_enable, ch)) 
	{
	  /* Set => Strobe */
	  /* Read status first */
	  csr = mvme_read_value(mvme, uvio.base + VMEIO_READ_STATUS_RO);
	  if (csr & 0xf) {
	    /* Read Data */
	    csr = mvme_read_value(mvme, uvio.base + VMEIO_READ_SYNC_RO);
	    data = csr & OUTPUT_MASK; // 0x3FFFF
	    return ((data >> ch) & 0x1);
	  }
	  else
	    {
	      return UVIO_NO_STROBE; // 0x80000004
	    }
	}
      else
	{
	  return UVIO_DISABLE_CH; // 0x80000003
	}
    
      /* Record change */  // do we actually get here?
      uvio.input_strobe_enable = bitSet(uvio.input_strobe_enable, ch);
      return UVIO_SUCCESS; // 0x80000000
    }
  return UVIO_INVALID_CH; // 0x80000002
}





/*-----------------------------------------------*/
DWORD musrVmeioInputState(MVME_INTERFACE *mvme)
{
  INT csr;

  /* Set module */
  mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

  csr = mvme_read_value(mvme, uvio.base + VMEIO_READ_ASYNC_RO);

  return (csr & 0xFFFFFF);
}

/*-----------------------------------------------*/
DWORD musrVmeioInputMode(void)
{
  DWORD mode=0;
  
  mode |= uvio.input_strobe_enable;
  mode |= (uvio.input_async_enable << 4);
  return mode;
}

/*-----------------------------------------------*/
INT   musrVmeioStatus(MVME_INTERFACE *mvme)
{
  INT csr;

  /* Set module */
  mvme_set_am(mvme, MVME_AM_A24_ND); // set A24
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32
  
  csr = mvme_read_value(mvme, uvio.base + VMEIO_READ_STATUS_RO);

  return (csr &0xFF);
}
  
void  musrVmeio_ShowStatus(INT status)
{
  status = status & 0xff;
  printf("musrVmeio_ShowStatus: Status = 0x%x\n",status);

  printf("Status from RDCNTL Register :\n");
  if ( (status & 0xf) == 0xf) 
    printf("STROBE has been received\n");
  else
    printf("STROBE has NOT been received\n");

  if ( ((status & 0xf0) >> 4) == 0xf) 
    printf("Interrupt source: SYNC\n");
  else
    printf("Interrupt source: ASYNC\n");

  return;
}

/*-----------------------------------------------*/
/*-----------------------------------------------*/
/*****************************************************************/
/*-PAA- For test purpose only */

#ifdef MAIN_ENABLE


int main (int argc, char* argv[]) {

  INT status,itest;
  DWORD value;
  INT iloop;
  char cmd[]="hallo";
  int s;
  INT channel;

  MVME_INTERFACE *myvme;

  DWORD VMEIO_BASE  =  0x780000; /* default */

 

  if (argc>1) {
    sscanf(argv[1],"%lx",&VMEIO_BASE);
  }

  // Test under vmic
  status = mvme_open(&myvme, 0);
  if(status != SUCCESS)
    {
      printf("failure after mvme_open, status = %d\n",status);
      return status;
    }

  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND); // set A32/A24 for VMEIO (jumpers)

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32); // D32 only for VMEIO

  printf("\nCalling musrVmeioReset to clear structure \n\n");
  musrVmeioReset();

  printf("\nCalling  musrVmeioInit to set VMEIO_BASE in structure\n\n");
  musrVmeioInit(VMEIO_BASE);

  printf("\nCalling  musrVmeioStatus to read status of VMEIO\n"); 
  status = musrVmeioStatus(myvme);
  musrVmeio_ShowStatus(status);
  musrVmeioCmds();
  while ( isalpha(cmd[0]) ||  isdigit(cmd[0]) )
    {
      printf("\nEnter command (A-Y) X to exit?  ");
      scanf("%s",cmd);
        printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];

      switch(s)
        {
        case ('A'):
          musrVmeioReset();
          musrVmeioInit(VMEIO_BASE); // set up Base
          break;
        case ('B'):
          printf("Enter Channel (0-17) : ");
          scanf("%d",&channel);
	  status = musrVmeioPulseSet(myvme, channel);
	  musrVmeio_msg(status);
          break;
         case ('C'):
           printf("Enter Channel  (0-17) : ");
           scanf("%d",&channel);
	   status = musrVmeioLatchSet(myvme, channel);
	   musrVmeio_msg(status);
           break;
         case ('D'):
           if(debug) {
	     debug=0;
	     printf("debug is OFF\n");
	   }
	   else {
	     debug=1;
	     printf("debug is ON\n");
	   }
           break;
        case ('E'):
          printf("Enter Channel  (0-17) : ");
          scanf("%d",&channel);
	  status = musrVmeioPulse(myvme, channel);
	  musrVmeio_msg(status);
          break;
        case ('F'):
          printf("Enter Channel (0-17) : ");
          scanf("%d",&channel);
	  status = musrVmeioOn(myvme, channel);
	  musrVmeio_msg(status);
          break;
        case ('G'):
          printf("Enter Channel (0-17) : ");
          scanf("%d",&channel);
	  status = musrVmeioOff(myvme, channel);
	  musrVmeio_msg(status);
          break;
        case ('I'):
	  status = musrVmeioOutputState();
          printf("Output State from structure: 0x%x or %d\n",status,status);
          break;
        case ('J'):
          status = musrVmeioOutputMode();
          printf("Output Mode from structure: 0x%x or %d\n",status,status);
          break;
        case ('K'):
          printf("Enter Channel (0-3) : ");
          scanf("%d",&channel);
          status = musrVmeioAsyncSet (channel);
	  musrVmeio_msg(status);
          break;
        case ('L'):
          printf("Enter Channel (0-3) : ");
          scanf("%d",&channel);
          status = musrVmeioStrobeSet (channel);
	  musrVmeio_msg(status);
          break;
        case ('M'):
          printf("Enter Channel (0-3) : ");
          scanf("%d",&channel);
          value = musrVmeioRead (myvme, channel);
          if(status & STATUS_MASK)
	    musrVmeio_msg(status);
	  else
	    printf("Value read: 0x%lx\n",value);
          break;
        case ('N'):
	  status = musrVmeioClear(myvme);
	  musrVmeio_msg(status);
          break;
        case ('R'):
	  status = musrVmeioInputState(myvme);
          printf("Input States: 0x%x or %d\n",status,status);
          break;
        case ('S'):
          status = musrVmeioStatus(myvme);
	  musrVmeio_ShowStatus(status);
          break;
        case ('U'):
          status = musrVmeioInputMode();
          printf("Input modes: 0x%x i.e.  Strobe:  0x%4.4x  Async:  0x%4.4x\n", 
		 status, (status & 0xF),((status & 0xF0)>>4));
          break;

        case ('T'):
	  printf("Test Modes: 1 Latch outputs\n");
	  printf("            2 Pulse outputs\n"); 
          printf("            3 Asynch inputs\n"); 
          printf("            4 Strobe inputs\n"); 
	   printf("Enter test mode (1-4): ");
          scanf("%d",&itest);
	   printf("Enter number of loops : ");
          scanf("%d",&iloop);
	  musrVmeioTest(myvme, VMEIO_BASE, itest, iloop);
          break;

       case ('H'):
	 musrVmeioHelp();
         break;
        case ('P'):
          musrVmeioCmds();
          break;
        case ('X'):
        case ('Q'):
          status = mvme_close(myvme);
          return 1;
        default:
          printf("Unknown command\n");
          break;
	}
    }
  status = mvme_close(myvme);
  return 1;
}

void musrVmeioTest (MVME_INTERFACE *myvme, DWORD base, INT test_mode, INT loop_count)
{
  /* Input Test Mode
     1  Latch
     2  Pulse
     3  Async
     4  Strobe
  */
  
  if (test_mode == 1)
    tlatch(myvme, base, loop_count);
  else if (test_mode == 2)
    tpulse(myvme, base, loop_count);
  else if (test_mode == 3)
    tasync(myvme, base, loop_count);
  else if (test_mode == 4)
    tstrobe(myvme, base, loop_count);
  else
    printf("Unknown test mode %d\n",test_mode);
  return;
}


/*-----------------------------------------------*/
void musrVmeioHelp(void)
{
  printf("\n");
  printf("musrVmeio functions  \n");
  printf("-------------------\n");
  printf("A musrVmeioReset              : Reset all definitions to zero (except VME base address)/n");
  printf("\nOutput section:\n");
  printf("B musrVmeioPulseSet(channel)  : Set output channel to PULSE mode; OUTSET reg \n");
  printf("C musrVmeioLatchSet(channel)  : Set output channel to LATCH mode; OUTSET reg\n");
  printf("D debug                       : Toggles debug flag\n");
  printf("E musrVmeioPulse(channel)     : Pulses output channel (if set in PULSE mode); OUTPULSE reg \n");
  printf("F musrVmeioOn(channel)        : Latch output channel  ON (if set in LATCH mode); OUTLATCH reg\n");
  printf("G musrVmeioOff(channel)       : Latch output channel OFF (if set in LATCH mode); OUTLATCH reg \n");
  printf("H Help                        : prints this list \n");
  printf("I musrVmeioOutputState        : prints output states from structure (bitwise)\n");
  printf("J musrVmeioOutputMode         : prints output mode settings from structure (bitwise)\n");
  printf("Input section:\n");
  printf("K musrVmeioAsyncSet(channel)  : Set Input channel in structure to Async mode\n");
  printf("L musrVmeioStrobeSet(channel) : Set Input channel in structure to Strobe mode\n");
  printf("M musrVmeioRead(channel)      : Read Input channel (handling Async and Strobe); RDSYNC reg\n");
  printf("N musrVmeioClear              : Clear Strobe - re-enables synchr inputs;  CLSTR reg\n");
  printf("P print                       : prints list of commands \n");
  printf("Q Quit                        : exit\n");
  printf("R musrVmeioInputState         : Read all Inputs states (bitwise); RDASYNC reg \n");
  printf("S musrVmeioStatus             : Read Status Register 0x0F:Strobe\n");
  printf("                                                         0xF0:A/S Int source\n");
  printf("T musrVmeioTest               : Test modes 1=latch 2=pulse 3=async 4=strobe \n");
  printf("U musrVmeioInputMode          : Read bitwise all Inputs modes from structure \n");
  printf("                                    (Strobe:0..3, Async:4..7)\n\n");
  printf("X Exit                        : exit\n");
  printf("\n");
}

void  musrVmeioCmds(void)
{
  // print brief list of commands
  printf("musrVmeio Command List:\n");
  printf("    A Reset            S Status \n");  
  printf("    T TestMode         D Debug \n");
  printf("Output section:\n");
  printf("    B *PulseSet        C *LatchSet\n");
  printf("    E *Pulse           F *LatchOn\n");
  printf("    G *LatchOff        I OutputState\n");
  printf("    J OutputMode\n");
  printf("Input Section: \n");
  printf("    K AsyncSet         L StrobeSet \n");
  printf("    M *Read            N *Clear \n");
  printf("    R InputState       U InputMode   \n");
  printf("General Commands");   
  printf("    H Help             P print \n");
  printf("    X Exit             Q Quit\n");
  printf("\n");
}

#endif // MAIN_ENABLE

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
