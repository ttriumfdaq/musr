/*-----------------------------------------------------------------------------
 *  Copyright (c) 1996      TRIUMF Data Acquistion Group
 *  Please leave this header in any reproduction of that distribution
 * 
 *  TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 *  Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *         amaudruz@triumf.ca
 * -----------------------------------------------------------------------------
 *  
 *  Description : Support function library for the vmeIO board developed at TRIUMF
 *                for the MuSR group
 *  Requires    : 
 *  Application : VME
 *  Author      :  Pierre-Andre Amaudruz Data Acquisition Group
 *
 *  Converted to VMIC
 *  $Id: musrvmeio.h,v 1.3 2015/03/19 22:08:52 suz Exp $
 *
 *
 *---------------------------------------------------------------------------*/

#ifndef MUSR_VMEIO_INCLUDE
#define MUSR_VMEIO_INCLUDE

#include "stdio.h"
#include "string.h"

#ifdef __cplusplus
extern "C" {
#endif



#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef long unsigned int       DWORD;
typedef long unsigned int  BOOL;
#define SUCCESS 1
#endif /* MIDAS_TYPE_DEFINED */

#include "mvmestd.h"
#include "vmicvme.h"
#include "tuvio.h"

// #define VMEIO_BASE    0x780000

/* Define the number of channels */
#define FIRST_OUTPUT       0
#define MAX_N_OUTPUT      18
#define OUTPUT_MASK  0x3FFFF
#define STATUS_MASK  0x80000000  // check for status/error code

#define FIRST_INPUT        0
#define MAX_N_INPUT        4

/* Define the possible command */
#define VMEIO_ENABLE_INT_WO      0x00
#define VMEIO_SET_INT_SYNC_WO    0x04
#define VMEIO_SET_PULSE_WO       0x08
#define VMEIO_WRITE_PULSE_WO     0x0c
#define VMEIO_WRITE_LATCH_WO     0x10
#define VMEIO_READ_SYNC_RO       0x14
#define VMEIO_READ_ASYNC_RO      0x18
#define VMEIO_READ_STATUS_RO     0x1c
#define VMEIO_CLEAR_VMEIO_WO     0x1c

#define UVIO_SUCCESS         0x80000000
#define UVIO_NOT_INITIALIZED 0x80000001
#define UVIO_INVALID_CH      0x80000002
#define UVIO_DISABLE_CH      0x80000003
#define UVIO_NO_STROBE       0x80000004

/**
musrVmeio Internal data structure
 */
typedef struct {
    DWORD base;
    DWORD output_pulse_enable;
    DWORD output_latch_enable;
    DWORD output_mode;
    DWORD output_state;
    DWORD input_async_enable;
    DWORD input_strobe_enable;
    DWORD input_mode;
    DWORD input_state;
} MUSR_VMEIO;


/** 
@memo Initialize vmeio structure
 */
void musrVmeioInit(DWORD base);

/** 
@memo Reset ALL definition, need musrVmeioInit as next step.
 */
void musrVmeioReset(void);

/*--- Output section */

/** 
@memo Set Output Pulse mode for given channel
@return status SUCCESS, ERROR
 */
INT musrVmeioPulseSet(MVME_INTERFACE *mvme, INT ch);

/** 
@memo Set Output Latch mode for given channel
@return status SUCCESS, ERROR
 */
INT musrVmeioLatchSet(MVME_INTERFACE *mvme, INT ch);

/** 
@memo Write Pulse for given channel
@return status SUCCESS, ERROR
 */
INT musrVmeioPulse(MVME_INTERFACE *mvme, INT ch);

/** 
@memo Write Latch ON for given channel
@return status SUCCESS, ERROR
 */
INT musrVmeioOn(MVME_INTERFACE *mvme, INT ch);

/** 
@memo Write Latch OFF for given channel
@return status SUCCESS, ERROR
 */
INT musrVmeioOff(MVME_INTERFACE *mvme, INT ch);

/** 
@memo Read all 18 outputs states
@return status SUCCESS, ERROR
 */
DWORD musrVmeioOutputState(void);

/** 
@memo Read all 18 output mode settings
@return status SUCCESS, ERROR
 */
DWORD musrVmeioOutputMode(void);

/*--- Input section */

/**
@memo Set input channel to Async mode
@return status SUCCESS, ERROR
 */
INT musrVmeioAsyncSet(INT);

/**
@memo Set input channel to Strobe mode
@return status SUCCESS, ERROR
 */
INT musrVmeioStrobeSet(INT);

/**
@memo Read input channel
@return status SUCCESS, ERROR
 */
//INT musrVmeioRead(MVME_INTERFACE *mvme, INT ch);
INT musrVmeioRead(MVME_INTERFACE *mvme, INT ch);
/**
@memo Clear Strobe
@return status SUCCESS, ERROR
 */
INT musrVmeioClear(MVME_INTERFACE *mvme);

/** 
@memo Read all 4 inputs states
@return 4bits of 4 input states
 */
DWORD musrVmeioInputState(MVME_INTERFACE *mvme);

/**
   
@memo Read all 4 input mode settings
@return 4bits 0=Async, 1=Strobe
 */
DWORD musrVmeioInputMode(void);


INT   musrVmeioStatus(MVME_INTERFACE *mvme);

void musrVmeio_msg(INT status); // write status error
void  musrVmeio_ShowStatus(INT status); // interpret status word returned by  musrVmeioStatus
void musrVmeioHelp(void);
void  musrVmeioCmds(void);
  void  musrVmeioTest (MVME_INTERFACE *myvme, DWORD base, INT test_mode, INT loop_count);
#ifdef __cplusplus
}
#endif
#endif
