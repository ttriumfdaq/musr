/*-----------------------------------------------------------------------------
 *  Copyright (c) 1996      TRIUMF Data Acquistion Group
 *  Please leave this header in any reproduction of that distribution
 * 
 *  TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 *  Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *         amaudruz@triumf.ca
 * -----------------------------------------------------------------------------
 *  
 *  Description : Support function library for the vmeIO board developed at TRIUMF
 *                for the MuSR group
 *  Requires    : 
 *  Application : VME
 *  Author      :  Pierre-Andre Amaudruz Data Acquisition Group
 *  Converted to VMIC : SD
 *  $Id: tuvio.c,v 1.3 2015/03/19 22:08:37 suz Exp $
 *---------------------------------------------------------------------------*/
#include "musrvmeio.h"
#include "tuvio.h" // prototypes
#include <stdio.h>
#include <string.h>
#if defined(OS_LINUX)
#include <unistd.h>
#endif

extern BOOL debug;


void tlatch(MVME_INTERFACE *mvme, DWORD base, INT loop)
{
  INT i, j;


  if(loop > 99)loop=100;
  if(debug)printf("tlatch: debug=%d  loop=%d\n",(INT)debug, loop);
  musrVmeioInit(base);
  for (i=0 ; i < MAX_N_OUTPUT ; i++) {
    musrVmeioLatchSet(mvme,i);
  }
  printf("State:0x%lx\n",musrVmeioOutputState());
  printf("Mode:0x%lx\n",musrVmeioOutputMode());

  if (loop) {
    for (i=0 ; i < loop ; i++) {
      if (debug)printf("Loop %d\n",i);
      for (j=0 ; j < MAX_N_OUTPUT ; j++) {
	musrVmeioOn(mvme, j);
        if(debug)
	  printf("Output %d Set On  -> State:0x%lx ..... ",j, musrVmeioOutputState()); 
	usleep(200);
	musrVmeioOff(mvme,j);
	usleep(200);
	if(debug)printf("   Set Off -> State:0x%lx\n", musrVmeioOutputState()); 
      }
    }
  }
  else
    printf("Loop count not specified\n");
  printf("Test done\n");
}

void tpulse(MVME_INTERFACE *mvme,  DWORD base, INT loop)
{
  INT i, j;
  if(loop > 99)loop=100;
  if(debug)printf("tpulse: debug=%d  loop=%d\n",(INT)debug, loop);


  musrVmeioInit(base);
  for (i=0 ; i < MAX_N_OUTPUT ; i++) {
    musrVmeioPulseSet(mvme, i);
  }
  printf("State:0x%lx\n",musrVmeioOutputState());
  printf("Mode:0x%lx\n",musrVmeioOutputMode());

  if (loop) {
    for (i=0;i<loop;i++) {
      if (debug)printf("Loop %d\n",i);
      for (j=0 ; j < MAX_N_OUTPUT ; j++) {
	musrVmeioPulse(mvme, j);
	usleep(200); 
	printf("Output %d Pulsed  -> State:0x%lx \n",j, musrVmeioOutputState()); 
      }
    }
  }
  else
    printf("Loop count not specified\n");
  printf("Test done\n");
}

void tasync(MVME_INTERFACE *mvme,  DWORD base, INT loop)
{
  INT i, j;
  DWORD status;

  if(loop > 99)loop=100;
  if(debug)printf("tasync: debug=%d  loop=%d\n",(INT)debug, loop);

  musrVmeioInit(base);

  for (i=0 ; i < MAX_N_INPUT ; i++) {
    musrVmeioAsyncSet(i);
  }
  if(debug)printf("Set all Inputs into Async Mode\n");

  musrVmeioLatchSet(mvme, 1);
  if(debug)printf("Set Output 1 into Latch Mode\n\n");

  printf("Reading Input States and Modes: \n");
  printf("State:0x%lx\n",musrVmeioInputState(mvme));
  printf("Mode:0x%lx\n",musrVmeioInputMode()); // from structure

  if (loop) {
    for (i=0 ; i < loop ; i++) {
      if (debug)printf("Loop %d\n",i);

      for (j=0 ; j < MAX_N_INPUT ; j++) {

	status =  musrVmeioRead(mvme,j) ;
	if (status & STATUS_MASK)
	  musrVmeio_msg(status); // return was a status message
	else 
	  {
	    if (status) // return is the value
	      {
		if(debug)
		  printf("Input %d Read %d State:0x%lx -> latching output 1 ON",j, (INT)status,  musrVmeioOutputState()); 
		musrVmeioOn(mvme, 1); // latch output ON
	      }
	    else
	      {
		if(debug)
		  printf("Input %d Read %d State:0x%lx -> latching output 1 OFF",j, (INT)status, musrVmeioOutputState());
		musrVmeioOff(mvme, 1);// latch output OFF
	      }
	  }
    
        
	usleep(200);
	if(debug)printf("State:0x%lx\n", musrVmeioInputState(mvme));
      }
    }
  }
  else
    printf("Loop count not specified\n");
  printf("Test done\n");
}

void tstrobe(MVME_INTERFACE *mvme,  DWORD base, INT loop)
{
  INT status, i, j;

  if(loop > 99)loop=100;
  if(debug)printf("tstrobe: debug=%d  loop=%d\n",(INT)debug, loop);

  musrVmeioInit(base);
  for (i=0 ; i < MAX_N_INPUT ; i++) {
    musrVmeioStrobeSet(i);
  }
  if(debug)printf("Set all Inputs into Strobe Mode\n");

  musrVmeioPulseSet(mvme, 0);
  if(debug)printf("Set Output 0 into Pulse Mode\n");
  musrVmeioLatchSet(mvme, 1);
  if(debug)printf("Set Output 1 into Latch Mode\n\n");

  printf("I State:0x%lx\n",musrVmeioInputState(mvme));
  printf("I Mode:0x%lx\n",musrVmeioInputMode());
  printf("O State:0x%lx\n",musrVmeioOutputState());
  printf("O Mode:0x%lx\n",musrVmeioOutputMode());

  if (loop) {
    for (i=0 ; i < loop ; i++) {
      if (debug)printf("Loop %d\n",i);

      /* Generate Strobe and level */
      musrVmeioOn(mvme, 1);
      if(debug)printf("Set Output 1 Latch ON\n");
      musrVmeioPulse(mvme, 0);
      if(debug)printf("Pulsed  Output 0 \n");
      musrVmeioOff(mvme, 1);
      if(debug)printf("Set Output 1 Latch OFF\n");

      printf("\nStrobe Read: ");

      if(debug)
	{
	  printf("Input ");
	  for (j=0 ; j < MAX_N_INPUT ; j++)
	    printf(" %d  ",j);
	}

      for (j=0 ; j < MAX_N_INPUT ; j++) {
	status = musrVmeioRead(mvme,j); // Reading all inputs

	if (status == UVIO_NO_STROBE)
	  printf("no ");
	else
	  printf("%d  ", (INT)status);
      }
      usleep(200);
      musrVmeioClear(mvme);
    }
  }
  else
    printf("Loop count not specified\n");
  printf("Test done\n");
}
  
