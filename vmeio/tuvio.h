/*-----------------------------------------------------------------------------
 *  Copyright (c) 1996      TRIUMF Data Acquistion Group
 *  Please leave this header in any reproduction of that distribution
 * 
 *  TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 *  Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *         amaudruz@triumf.ca
 * -----------------------------------------------------------------------------
 *  
 *  Description : Header file for support function library for the vmeIO board 
 *                developed at TRIUMF for the MuSR group
 *  Requires    : 
 *  Application : VME
 *  Converted to VMIC : SD
 *  $Id: tuvio.h,v 1.1 2015/03/19 22:08:23 suz Exp $
 *---------------------------------------------------------------------------*/

#ifndef MUSR_TUVIO_INCLUDE
#define MUSR_TUVIO_INCLUDE

#include "stdio.h"
#include "string.h"

#ifdef __cplusplus
extern "C" {
#endif

  void tstrobe(MVME_INTERFACE *mvme, DWORD base, INT loop);
  void tasync(MVME_INTERFACE *mvme, DWORD base, INT loop);
  void tpulse(MVME_INTERFACE *mvme, DWORD base, INT loop);
  void tlatch(MVME_INTERFACE *mvme, DWORD base, INT loop);

#ifdef __cplusplus
}
#endif
#endif // MUSR_TUVIO_INCLUDE 
